/*jshint esversion: 6 */
import Align from "../util/Align";
import {AlignGrid} from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene
{
    constructor()
    {
        super("SceneMain");
    }
    preload()
    {
      
        this.load.image("face","src/assets/face.png");

        this.load.image("zero","src/assets/0.png");
        this.load.image("one","src/assets/1.png");
    }
    create()
    {
        let face=this.add.image(0,0,"face");

        //make a grid
        let config={scene:this,rows:11,cols:11};
        this.grid=new AlignGrid(config);
        this.grid.showNumbers();

        //place the face on the grid
        this.grid.placeAtIndex(25,face);

        //scale the face to 10% (.1) of the game's width
        //pass in object, percent,screen
        Align.scaleToGameW(face,.1,this);

        //UIBlock -container example

        //add images
        let zero=this.add.image(0,0,"zero");
        let one=this.add.image(100,0,"one");

        //make a block
        let block=new UIBlock();
        //add images to block
        block.add(zero);
        block.add(one);

        //tween the block- the children will move as one object
        this.tweens.add({targets: block,duration: 1000,y:200,x:200});

    }
    update()
    {

    }
}
export default SceneMain