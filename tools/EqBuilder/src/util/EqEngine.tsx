import { DStoryVo } from "../dataObjs/DStoryVo";
import { ExportDVo } from "../dataObjs/ExportDVo";
import { ExportMVo } from "../dataObjs/ExportMVo";
import { ExportVo } from "../dataObjs/ExportVo";
import { FractStepsVo } from "../dataObjs/FractStepsVo";
import { FractVo } from "../dataObjs/FractVo";
import { MStoryVo } from "../dataObjs/MStoryVo";
import { NumVo } from "../dataObjs/NumVo";
import { SpanVo } from "../dataObjs/SpanVo";
import { StepVo } from "../dataObjs/StepVo";
import { StoryVo } from "../dataObjs/StoryVo";
import { DivideStepDisplay } from "../ui/DivideStepDisplay";
import { FractDisplay } from "../ui/FractDisplay";
import { TowerDisplay } from "../ui/TowerDisplay";
import { TowerStepDisplay } from "../ui/TowerStepDisplay";
import { FractionUtil } from "./FractionUtil";
import { PlacesUtil } from "./PlacesUtil";
import { StoryUtil } from "./StoryUtil";
//import { HtmlUtil } from "./HtmlUtil";

export class EqEngine {
    private expression: string;
    private orignal: string;

    private stepVo: StepVo | null = null;

    private answer: string = "";

    private styleEl: HTMLStyleElement | null = null;
    private maxDec: number = 0;
    private maxWhole: number = 0;

    public useInline: boolean = false;

    private showFirstTrail: boolean = false;

    private autoCalc: string = "calc";

    private classArray: string[][] = [];

    public step: number = 0;

    public useRemain: boolean = true;
    public zeroLimit: number = 2;

    public exportString: string = "";

    public spanMap: Map<number, SpanVo[]> = new Map<number, SpanVo[]>();

    public allSpans: SpanVo[] = [];
    public spans: SpanVo[] = [];
    public editMode: boolean;

    constructor(expression: string, editMode: boolean = false) {
        ////console.log("ED="+editMode);

        this.expression = expression;
        this.orignal = expression;
        this.editMode = editMode;
        this.styleEl = document.getElementById("customCSS") as HTMLStyleElement;

        (window as any).engine = this;
        this.classArray[0] = [];
        this.classArray[1] = [];

        // this.allSpans = [new SpanVo(2, -1, 0, 1, 'red'), new SpanVo(2, 1, 1, 2, 'blue'), new SpanVo(1, 1, 1, 2, 'blue'),new SpanVo(3, -1, 0, 2, 'red'), new SpanVo(3, 1, 2, 2, 'green'),]

        //  this.mapSpans();

        //  this.spanMap.set(2,[new SpanVo(2,0,0,1,'red'),new SpanVo(2,1,1,2,'blue')]);

        // this.spans=[new SpanVo(0,0,3,'red')];
        ////console.log("ED2="+this.editMode);
    }
    public resetSpans(allSpans: SpanVo[]) {
        //console.log(allSpans);
        (window as any).engine2 = this;
        this.spanMap.clear();
        this.allSpans = allSpans;
        this.mapSpans();
        //this.applySpanStyles();
    }
    public setSpanStep(step: number) {
        this.spans = this.spanMap.get(step) || [];


    }
    public mapSpans() {
        for (let i: number = 0; i < this.allSpans.length; i++) {
            let spanVo: SpanVo = this.allSpans[i];

            let spans2: SpanVo[] = this.spanMap.get(spanVo.step) || [];
            spans2.push(spanVo);

            this.spanMap.set(spanVo.step, spans2);
        }
    }
    public applySpanStyles() {
        for (let i: number = 0; i < this.spans.length; i++) {
            ////console.log(i);
            this.spans[i].applyStyles();
        }
    }
    private makeAnswer() {
        let answerArray: string[] = this.expression.split("=");

        if (answerArray.length > 2) {
            return "Invalid!";
        }
        if (answerArray.length > 0) {
            //console.log(answerArray);
            this.answer = answerArray[1];
            this.expression = answerArray[0];
        }
    }
    getHtml() {
        //basic expressions

        //console.log("get HTML");

        if (this.expression === "") {
            return "enter an expression";
        }

        this.makeAnswer();
        let output: string[] = [];

        if (this.expression.includes("_")) {
            return this.getFractions();
        }
        //console.log("no fractions");

        if (this.useInline === true) {
            return this.getInline();
        }
        if (this.expression.includes("sqrt")) {
            return this.getInline();
        }
        let add: boolean = this.expression.includes("+");
        let sub: boolean = this.expression.includes("-");
        let mul: boolean = this.expression.includes("*");
        let division: boolean = this.expression.includes("/");

        let ops: string[] = this.extractOps();

        let c: number = ((add === true) ? 1 : 0) + ((sub === true) ? 1 : 0) + ((mul === true) ? 1 : 0) + ((division === true) ? 1 : 0);

        if (c !== 1) {
            if (this.answer === "" || this.answer === undefined) {

                let exp: string = this.expression;
                exp = this.fixSpecial(exp);

                return exp;
            }
            return this.expression + "=" + this.getFormattedAnswer();
            // return "invalid";
        }

        if (c === 1) {
            if (division === false) {
                let symbol: string = "+";
                if (sub === true) {
                    symbol = "-";
                }
                if (mul === true) {
                    symbol = "*";
                }
                /**
                 * Simple
                 */

                let nums: string[] = this.expression.split(symbol);
                ////////////console.log("NUMS");
                ////////////console.log(nums);

                this.maxDec = this.findMaxDec(nums);
                this.maxWhole = this.findMaxWhole(nums);

                let answer = this.answer;

                if (answer === this.autoCalc) {
                    answer = this.calcAnswer(this.orignal);
                }



                if (this.useInline === false) {

                    ////console.log("engine edit mode "+this.editMode);

                    let td: TowerDisplay = new TowerDisplay(nums, symbol, answer, "", [], this.editMode);

                    output.push(td.toHtml());

                }
                else {

                    output.push("<div id='mathLine'>");
                    for (let i: number = 0; i < nums.length; i++) {
                        let num: string = nums[i];

                        if (num !== undefined) {
                            // num = this.fixDec(num);
                            num = this.fixSpecial(num);
                            if (i !== nums.length - 1) {
                                output.push("<span></span><span>" + num + "</span>");
                            }
                            else {
                                let op: string | undefined = ops.shift();
                                if (op === undefined) {
                                    op = "";
                                }
                                op = (mul === true) ? "\u00D7" : op;
                                output.push("<span class='fop'>" + op + "</span><span>" + num + "</span>");
                            }
                        }
                    }
                    if (answer !== "") {
                        output.push("<span id='answerLine'>");
                        output.push("<span></span>");
                        output.push("<span>" + answer + "</span>");
                        output.push("</span>");
                    }
                    output.push("</div>");
                }
            }
            else {
                /**
                 * Division
                 */
                let nums: string[] = this.expression.split("/");

                let dividend: string = nums[0] || "?";
                let divisor: string = nums[1] || "?";


                let nums2: string[] = [this.answer, dividend];

                if (!this.answer) {
                    nums2 = [dividend];
                }

                this.maxDec = this.findMaxDec(nums2);
                this.maxWhole = this.findMaxWhole(nums2);

                if (this.findDecPlaces(this.answer) > this.maxDec) {
                    this.maxDec = this.findDecPlaces(this.answer);
                }

                divisor = this.fixDec(divisor, "decp2");

                dividend = this.addZeroLead(dividend);
                dividend = this.addZeroTrail(dividend);
                dividend = this.fixDec(dividend);


                let answer: string = this.getFormattedAnswer();

                output.push("<div id='divGrid'>");
                output.push("<div></div>");
                output.push("<div id='quotient'>" + answer + "</div>");
                output.push("<div id='divisor'>" + divisor + "</div>");
                output.push("<div id='dividend'>" + dividend + "</div>");
                output.push("</div>");

            }
        }
        else {
            //complex
            return this.getInline();
        }

        return output.join("");
    }
    /*  private makeClassString(num: string, classes: string[]) {
         let numString: string = "";
         ////////////console.log(classes);
 
         for (let i: number = 0; i < num.length; i++) {
             if (classes[i]) {
                 numString += "<span class='" + classes[i] + "'>" + num.substring(i, i + 1) + "</span>";
             }
             else {
                 numString += "<span>" + num.substring(i, i + 1) + "</span>";
             }
         }
         ////////////console.log(numString);
         return numString;
     } */
    private getInline() {

        ////////////////console.log("GET INLINE");

        let output: string[] = [];
        let answer: string = this.answer;

        let nums: string[] = this.expression.split(/[=*+-//]/);
        let ops: string[] = this.extractOps();

        ////////////////console.log("Nums");
        ////////////////console.log(nums);


        output.push("<div id='mathLine'>");

        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            ////////////////console.log(num);

            if (num !== undefined) {
                // num = this.fixDec(num);
                num = this.fixSpecial(num);
                if (i !== nums.length - 1) {
                    output.push("<span>" + num + "</span>");
                }
                else {
                    let op: string | undefined = ops.shift();
                    if (op === undefined) {
                        op = "";
                    }
                    op = (op === "*") ? "\u00D7" : op;
                    output.push("<span class='fop'>" + op + "</span><span>" + num + "</span>");
                }
            }
        }
        if (answer !== "" && answer !== undefined) {

            if (answer === this.autoCalc) {
                answer = this.calcAnswer(this.orignal);
            }


            output.push("<span id='answerLine'>");
            output.push("<span></span>");
            output.push("<span>=" + answer + "</span>");
            output.push("</span>");
        }
        else {
            ////////////////console.log(ops);

            let len: number = this.orignal.length;
            if (this.orignal.substring(len - 1, len) === "=") {
                output.push("<span id='answerLine'>");
                output.push("<span></span>");
                output.push("<span>=</span>");
                output.push("</span>");
            }
        }

        output.push("</div>");

        return output.join("");
    }

    public getStep(step: number) {
        this.step = step;

        if (this.spanMap.has(step)) {
            this.spans = this.spanMap.get(step) || [];
        }
        if (this.expression.includes("_")) {
            //return this.getFractions();
            if (this.answer === "") {
                this.makeAnswer();
            }
            let fractionUtil: FractionUtil = new FractionUtil();
            let ops: string[] = this.extractOps();

            console.log(ops);

            let fracts: FractStepsVo[] = fractionUtil.stepFracts(this.expression, ops.slice());

            let displayAnswerAt:number=0;

            for (let j:number=0;j<fracts.length;j++)
            {
                if (fracts[j].fracts.length===1)
                {
                    displayAnswerAt=j-1;
                }
            }

            let answer: FractVo = fractionUtil.fractAnswer;
            //  this.answer=fractionUtil.answer;
            //  //console.log("answer!="+this.answer);
            console.log(ops);
            if (!fracts[step]) {
                return "Invalid";
            }
            console.log("displayAnswerAt="+displayAnswerAt);

            let fd: FractDisplay;

            if (step>displayAnswerAt)
            {
                fd = new FractDisplay(fracts[step-1], ops,answer);
            }
            else
            {
                fd = new FractDisplay(fracts[step], ops,null);
            }
           
            return fd.getHtml();

            //return outputString;
            //step here
        }

        let add: boolean = this.expression.includes("+");
        let sub: boolean = this.expression.includes("-");
        let mul: boolean = this.expression.includes("*");
        let division: boolean = this.expression.includes("/");

        let output: string[] = [];
        //output.push(this.getHtml());

        //let ops: string[] = this.extractOps();

        let c: number = ((add === true) ? 1 : 0) + ((sub === true) ? 1 : 0) + ((mul === true) ? 1 : 0) + ((division === true) ? 1 : 0);

        if (c !== 1) {

            return "Not Supported (YET!)";
        }

        let nums: string[] = [];

        let tsd: TowerStepDisplay = new TowerStepDisplay(this.expression, this.editMode);


        if (add === true) {

            let nums: string[] = this.expression.split("+");
            let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);


            let stepVo: StepVo = new StepVo(numsObj);

            let stories: StoryVo[] = stepVo.doStepsAdd();
            output.push(tsd.displayAdd(nums, stories, step));

            let exportVo: ExportVo = new ExportVo(this.expression, stories, this.allSpans);
            this.exportString = JSON.stringify(exportVo);

        }

        /**
         * SUBTRACTION
         */

        if (sub === true) {

            let nums: string[] = this.expression.split("-");
            let stories: StoryVo[] = StoryUtil.getSubStory(this.expression);

            let exportVo: ExportVo = new ExportVo(this.expression, stories, this.allSpans);
            this.exportString = JSON.stringify(exportVo);

            output.push(tsd.displaySub(nums, stories, step));
        }

        if (mul === true) {


            nums = this.expression.split("*");
            if (nums.length > 2) {
                return "NOT SUPPORTED";
            }


            ////////////console.log(nums);
            let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);

            ////////////console.log(numsObj);

            let stepVo: StepVo = new StepVo([numsObj[1], numsObj[0]]);
            this.stepVo = stepVo;

            let stories: MStoryVo[] = stepVo.doStepsMultiply(step, this.editMode);


            output = [tsd.getMultiStep(nums, stories, this.step)];
            // output.unshift(this.getHtml());

            let exportMVo: ExportMVo = new ExportMVo(this.expression, stories, this.allSpans);
            this.exportString = JSON.stringify(exportMVo);

        }

        if (division === true) {

            nums = this.expression.split("/");
            let stories: DStoryVo[] = StoryUtil.getDivStory(this.expression, this.zeroLimit, this.useRemain, this.editMode);
            let dsd: DivideStepDisplay = new DivideStepDisplay(this.zeroLimit, this.useRemain, this.editMode);
            output = dsd.getStepDisplay(nums, stories, this.step);

            let exportDVo: ExportDVo = new ExportDVo(this.expression, stories, this.allSpans);
            this.exportString = JSON.stringify(exportDVo);

        }


        return output.join("");
    }
    public getExport() {
        return this.exportString;
    }
    /* private makeNumberObjs(nums: string[]) {
        let numsObj: NumVo[] = [];

        for (let i: number = 0; i < nums.length; i++) {
            numsObj.push(new NumVo(nums[i]));
        }
        return numsObj;
    } */

    private getFractions() {
        //console.log("GET FRACTIONS");
        if (this.answer === "") {
            this.makeAnswer();
        }
        let output: string[] = [];
        let exArray: string[] = this.expression.split(/[=*+-//]/);
        let ops: string[] = this.extractOps();

        let answer: string = this.answer;

        let fracts: FractVo[] = [];



        for (let i: number = 0; i < exArray.length; i++) {


            let fArray: string[] = exArray[i].split("_");

            if (fArray.length > 3) {
                //console.log("INVALID");
                return "Invalid";
            }
            if (fArray.length === 2) {
                fracts.push(new FractVo((fArray[0]), fArray[1]));
            }
            else {
                fracts.push(new FractVo(fArray[1], fArray[2], fArray[0]));
            }
        }
        //console.log(answer);

        if (answer) {
            ops.push("=");

            if (answer === "calc") {

                let fractUtil: FractionUtil = new FractionUtil();

                if (fracts.length < 2) {
                    return "Invalid";
                }

                let currentFract: FractVo = fracts[0];
                let fIndex: number = 1;

                for (let i: number = 0; i < ops.length - 1; i++) {
                    let fract2: FractVo = fracts[fIndex];

                    let op2: any = fractUtil.opToWord(ops[i]);

                    let fract3: FractVo = fractUtil.computeFractions(currentFract.t1, currentFract.b1, fract2.t1, fract2.b1, currentFract.w1, fract2.w1, op2);

                    currentFract = fract3;
                    fIndex++;
                }

                if (currentFract.w1 === 0) {
                    answer = currentFract.top + "_" + currentFract.bottom;
                }
                else {
                    answer = currentFract.whole + "_" + currentFract.top + "_" + currentFract.bottom;
                }
                this.answer = answer;
            }


            if (answer.includes("_")) {
                let answerArray = answer.split("_");


                //Fraction only
                if (answerArray.length === 2) {
                    fracts.push(new FractVo((answerArray[0]), answerArray[1]));

                }
                //Whole Number and Fraction
                else {
                    // fracts.push(new FractVo((answerArray[0]), "0"));
                    fracts.push(new FractVo((answerArray[1]), answerArray[2], answerArray[0]));

                }
            }
            else {
                //Whole Number Only
                fracts.push(new FractVo("0", "0", answer));

            }
        }
        ////////////////console.log(fracts);

        for (let i: number = 0; i < fracts.length; i++) {

            output.push(fracts[i].toHtml());


            let op: string | undefined = ops.shift();

            if (op) {
                op = (op === "/") ? "÷" : op;
                output.push("<span class='fop'>" + op + "</span>");
            }
        }

        return output.join("");
    }

    private extractOps() {
        let opArray: string[] = ["+", "-", "*", "/", "="];
        let ops: string[] = [];

        for (let i: number = 0; i < this.expression.length; i++) {
            let char: string = this.expression.charAt(i);
            if (opArray.includes(char)) {
                ops.push(char);
            }
        }
        return ops;
    }
    public addStyleByID(elementID: string, style: CSSStyleDeclaration) {
        if (this.styleEl) {
            let cRule = "#" + elementID + "{" + style.cssText + "}";

            this.styleEl.append(cRule);
        }
    }
    public addStyle2(element: HTMLStyleElement, elementID: string, style: CSSStyleDeclaration) {
        if (element) {
            let cRule = "#" + elementID + "{" + style.cssText + "}";

            element.append(cRule);
        }
    }
    private fixDec(num: string, cn: string = "decp") {
        num = num.replace(".", "<span class='" + cn + "'>.</span>");

        return num;
    }
    private fixSpecial(num: string) {

        if (num.includes("sqrt(")) {
            let sqArray: string[] = num.split("(");

            if (sqArray.length === 2) {
                let num2: string = sqArray[1].replace(")", "");

                return "<span class='root'><span class='radix1'></span><span class='radix2'><span class='radicand'>" + num2 + "</span></span></span>";

                // return "<span style='white-space: nowrap; font-size:larger'>&radic;</span><span class='rootNum'>"+num2+"</span>";

            }
        }
        return num;
    }
    public convertToSqrt(q: string) {
        //let sqArray:string[]=q.split(\[(.*?)\]);

        let sqArray: string[] = q.split("sqrt(");
        ////////////////console.log(sqArray);

        for (let i: number = 0; i < sqArray.length; i++) {
            let s: string = sqArray[i];
            let s2: string[] = s.split(")");

            let num2: string = s2[0];
            if (num2 !== "") {
                if (!isNaN(parseFloat(num2))) {
                    let ss: string = "sqrt(" + num2 + ")";
                    let num3: number = Math.sqrt(parseFloat(num2));
                    q = q.replace(ss, num3.toString());
                }
                else {
                    return "invalid";
                }
            }
        }
        return q;
    }
    private addLeadingZeros(nums: string[]) {

        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            //  //////////////console.log(num);
            nums[i] = this.addZeroLead(num);
        }
        return nums;
    }
    private addTrailDecZeros(nums: string[]) {


        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];

            nums[i] = this.addZeroTrail(num);

        }
        return nums;
    }
    private addZeroLead(num: string) {

        ////////////console.log("add to "+num);

        let wholeLen: number = this.findWholePlaces(num);
        let zeroCount: number = this.maxWhole - wholeLen;


        if (zeroCount < 1) {
            return num;
        }
        let zeros: string = "";


        zeros = "<span class='invis0'>0</span>".repeat(zeroCount);
        num = zeros + num;


        return num;
    }
    private addZeroTrail(num: string) {

        let decLen: number = this.findDecPlaces(num);
        let zeroCount: number = this.maxDec - decLen;


        if (zeroCount < 1) {
            return num;
        }

        let zeros: string;
        if (this.showFirstTrail === true && decLen < 1) {

            zeros = "<span class='invis0'>0</span>".repeat(zeroCount - 1);
            zeros = "0" + zeros;
        }
        else {

            zeros = "<span class='invis0'>0</span>".repeat(zeroCount);
        }

        if (decLen === 0) {

            if (this.showFirstTrail === true) {
                num += "." + zeros;
            }
            else {
                num += zeros;
            }
        }
        else {
            num += zeros;
        }
        //////////console.log(num);
        return num;
    }
    private findDecPlaces(num: string) {
        if (num === undefined) {
            return 0;
        }
        let decArray = num.split(".");
        let dec: string = decArray[1];
        if (dec) {
            return dec.length;
        }
        return 0;
    }
    private findWholePlaces(num: string) {
        if (num === undefined) {
            return 0;
        }
        let wholeArray = num.split(".");
        let whole: string = wholeArray[0];

        if (whole) {
            return whole.length;
        }
        return 0;
    }
    private findMaxDec(nums: string[]) {
        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            let decCount: number = this.findDecPlaces(num);
            ////////////console.log(num,decCount);

            if (decCount > max) {
                max = decCount;
            }
        }
        return max;
    }
    private findMaxWhole(nums: string[]) {
        //////////////console.log(nums);

        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            if (!isNaN(parseFloat(num))) {
                let wholeCount: number = this.findWholePlaces(num);
                //////////////console.log(num,wholeCount);

                if (wholeCount > max) {
                    max = wholeCount;
                }
            }
        }
        return max;
    }
    private getFormattedAnswer() {
        let answer: string = this.answer;
        if (answer === this.autoCalc) {
            answer = this.calcAnswer(this.orignal);
        }
        if (answer !== "" && answer !== undefined) {
            answer = this.addZeroLead(answer);
            answer = this.addZeroTrail(answer);

            answer = this.fixDec(answer);
        }

        if (answer === undefined) {
            answer = "";
        }
        return answer;
    }
    private calcAnswer(answerString: string) {
        let answerArray: string[] = answerString.split("=");
        let q: string = answerArray[0];
        let answer: number = 0;
        if (q) {

            q = this.convertToSqrt(q);


            if (q.includes("sqrt()")) {
                return "invalid";
            }

            // eslint-disable-next-line no-eval
            answer = eval(q);
            answer = this.trimDec(answer.toString());

        }
        return answer.toString();
    }
    private trimDec(num: string) {
        let num2: number = parseFloat(num);
        let dec: number = num2 - Math.floor(num2);
        if (dec.toString().length > 5) {
            return Math.floor(num2 * 10000) / 10000;
        }
        return num2;


    }
    private decToFraction(num: number) {

        let len: number = num.toString().length - 2;

        let denominator: number = Math.pow(10, len);
        let numerator = num * denominator;

        let divisor = this.calcGcd(numerator, denominator);

        ////////////////////console.log("d=" + divisor);

        numerator = numerator / divisor;
        denominator = denominator / divisor;

        numerator = Math.floor(numerator);
        denominator = Math.floor(denominator);

        ////////////////////console.log(numerator + "/" + denominator);

        return numerator.toString() + "/" + denominator.toString();
    }
    private calcGcd(a: number, b: number): number {
        if (b === 0) return a;
        //a = parseInt(a);
        // b = parseInt(b);
        return this.calcGcd(b, a % b);
    };
}