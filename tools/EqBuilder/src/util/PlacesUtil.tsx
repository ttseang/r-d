import { NumVo } from "../dataObjs/NumVo";

export class PlacesUtil
{
    public static findMaxWhole(nums: string[]) {
        //////////console.log(nums);

        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            if (!isNaN(parseFloat(num))) {
                let wholeCount: number = PlacesUtil.findWholePlaces(num);               

                if (wholeCount > max) {
                    max = wholeCount;
                }
            }
        }
        return max;
    }
    public static findMaxWholeByObj(nums: NumVo[]) {
        //////////console.log(nums);

        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num2: string = nums[i].num;
            if (!isNaN(parseFloat(num2))) {
                let wholeCount: number = PlacesUtil.findWholePlaces(num2);               

                if (wholeCount > max) {
                    max = wholeCount;
                }
            }
        }
        return max;
    }
    public static findWholePlaces(num: string) {
        if (num === undefined) {
            return 0;
        }
        let wholeArray = num.split(".");
        let whole: string = wholeArray[0];

        if (whole) {
            return whole.length;
        }
        return 0;
    }
    public static findMaxDec(nums: string[]) {
        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            let decCount: number = PlacesUtil.findDecPlaces(num);
            ////////console.log(num,decCount);

            if (decCount > max) {
                max = decCount;
            }
        }
        return max;
    }
    public static findMaxDecByObj(nums: NumVo[]) {
        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num2: string = nums[i].num;
            let decCount: number = PlacesUtil.findDecPlaces(num2);
            ////////console.log(num,decCount);

            if (decCount > max) {
                max = decCount;
            }
        }
        return max;
    }
    public static findDecPlaces(num: string) {
        if (num === undefined) {
            return 0;
        }
        let decArray = num.split(".");
        let dec: string = decArray[1];
        if (dec) {
            return dec.length;
        }
        return 0;
    }
    public static addLeadingZeros(num:string,max:number)
    {
        while(num.length<max)
        {
            num="0"+num;
        }
        return num;
    }
    public static makeNumberObjs(nums: string[]) {
        let numsObj: NumVo[] = [];

        for (let i: number = 0; i < nums.length; i++) {
            numsObj.push(new NumVo(nums[i]));
        }
        return numsObj;
    }
}