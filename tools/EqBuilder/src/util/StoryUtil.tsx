import { DStoryVo } from "../dataObjs/DStoryVo";
import { NumVo } from "../dataObjs/NumVo";
import { StepVo } from "../dataObjs/StepVo";
import { StoryVo } from "../dataObjs/StoryVo";
import { PlacesUtil } from "./PlacesUtil";

export class StoryUtil {

    public static getSubStory(expression: string) {
        let nums: string[] = expression.split("-");
        let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);

        let maxWhole: number = PlacesUtil.findMaxWhole(nums);
        let maxDec: number = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            numsObj[i].formatDigits(maxWhole, maxDec);
        }

        let stepVo: StepVo = new StepVo(numsObj);

        let stories: StoryVo[] = stepVo.doStepSub();
        return stories;
    }
    public static getDivStory(expression: string, zeroLimit: number, useRemain: boolean,editMode:boolean=false) {
        let nums: string[] = expression.split("/");

        let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);

        let stepVo: StepVo = new StepVo(numsObj, zeroLimit);

        let stories: DStoryVo[] = stepVo.doStepDiv(useRemain,editMode);
        return stories;
    }
}