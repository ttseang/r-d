
export class RepeatChecker {
    private primeNumbers: number[];

    public static NO_REPEAT: number = 0;
    public static REPEATING_DEC: number = 1;
    public static INF_NUMBER: number = 2;

    constructor() {
        // All the prime numbers under 1,000
        this.primeNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997];

    }



    // Finds all the prime factors of a non-zero integer
    // a = integer
    private primeFactors(a: number) {
        let primeFactors: number[] = []

        // Trial division algorithm
        for (let i = 0, p = this.primeNumbers[i]; i < this.primeNumbers.length && p * p <= a; i++, p = this.primeNumbers[i]) {
            while (a % p === 0) {
                primeFactors.push(p);

                a /= p;
            }
        }

        if (a > 1) {
            primeFactors.push(a);
        }

        return primeFactors;
    }


    // Converts a fraction to a decimal
    // i = number
    // n = numerator
    // d = denominator
    public fractionToDecimal(n: number = 0, d: number) {

      

        let pFS = this.primeFactors(d);
        for (let i = 0; i < pFS.length; i++) { // Go through each of the denominators prime factors

            if (pFS[i] !== 2 && pFS[i] !== 5) { // We have a repeating decimal

                let output: string[] = [];
                let ns: number[] = [];

                // Let's find the repeating decimal
                // Repeating decimal algorithm - uses long division
                for (let i: number = 0; i < 20; i++) {

                    // For now find 20 spots, ideally this should stop after it finds the repeating decimal
                    // How many times does the denominator go into the numerator evenly

                    let temp2 = n / d;

                    if (ns[n] === undefined) {
                        ns[n] = i;
                    } else {

                        let repNumber: number = parseFloat(output.slice(0, 1).join(''));
                        repNumber = repNumber - Math.floor(repNumber);

                        for (let j: number = 0; j < 10; j++) {
                            if (repNumber.toString().includes("0." + j.toString().repeat(5))) {
                                ////////console.log("Repeat Dec Number")
                                return RepeatChecker.REPEATING_DEC;
                            }
                        }
                        //   //////console.log("Infinate number");
                        return RepeatChecker.INF_NUMBER;

                        /* return "Repeating decimal: " +
                            output.slice(0, 1).join('') +
                            '.' +
                            output.slice(1, ns[n]).join('') +
                            '[' + output.slice(ns[n]).join('') + ']'
                            ; */
                    }

                    output.push(temp2.toString());

                    n = n % d;

                    // n2 += "0";
                }



                // return "Repeating decimal: " + output;
            }
        }

        return RepeatChecker.NO_REPEAT;

        // Terminating decimal
        //return "Terminating decimal: " + parseFloat(n2) / d;
    }

}