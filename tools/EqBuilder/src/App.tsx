import React from 'react';
import './App.css';
import './MathStyles.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import BaseScreen from './screens/BaseScreen';
function App() {
  return (
    <div><style id="customCSS"></style>
      <style id="fractcss"></style>
      <div id="base"><BaseScreen></BaseScreen></div>
    </div>
  );
}
export default App;