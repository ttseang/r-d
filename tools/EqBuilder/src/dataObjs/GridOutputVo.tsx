export class GridOutputVo
{
    public top:string;
    public rows:string[];
    public operator:string;
    public answer:string;
    public classes:string[][];

    constructor(top:string,rows:string[],operator:string,answer:string,classes:string[][])
    {
        this.top=top;
        this.rows=rows;
        this.operator=operator;
        this.answer=answer;
        this.classes=classes;
    }
}