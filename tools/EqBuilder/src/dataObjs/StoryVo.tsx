export class StoryVo
{
    
    public top:string;
    public answer:string;
    public classes:string[];

    constructor(top:string="",answer:string="",classes:string[]=[])
    {
        this.top=top;
        this.answer=answer;
        this.classes=classes;
    }
    public fromObj(obj:any)
    {
        this.top=obj.top;
        this.answer=obj.answer;
        this.classes=obj.classes;
    }
}