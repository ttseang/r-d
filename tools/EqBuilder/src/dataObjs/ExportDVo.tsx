import { DStoryVo } from "./DStoryVo";
import { SpanVo } from "./SpanVo";

export class ExportDVo {
    public expression: string;
    public stories: DStoryVo[];
    public spanVo: SpanVo[] = [];

    constructor(expression: string = "", stories: DStoryVo[] = [], spanVo: SpanVo[] = []) {
        this.expression = expression;
        this.stories = stories;
        this.spanVo = spanVo;
    }
    fromObj(obj: any) {
        this.expression = obj.expression;
        let stories: string[] = obj.stories;
        this.stories = [];

        let spans:any=obj.spanVo;
        this.spanVo=[];

        for (let i: number = 0; i < stories.length; i++) {
            ////////console.log(stories[i]);
            let story: DStoryVo = new DStoryVo();
            story.fromObj(stories[i]);
            this.stories.push(story);
        }

        for (let j:number=0;j<spans.length;j++)
        {
            this.spanVo.push(new SpanVo(parseInt(spans[j].step),parseInt(spans[j].row),parseInt(spans[j].start),parseInt(spans[j].end),spans[j].className))
        }
    }
}