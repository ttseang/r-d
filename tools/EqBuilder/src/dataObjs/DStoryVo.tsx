import { SolutionVo } from "./SolutionVo";

export class DStoryVo {
    public solution: SolutionVo[];
    public answer: string;
    public classes: string[];
    public divs: string;
    public remain: string;

    constructor(solution: SolutionVo[] = [], answer: string = "", divs: string = "", remain: string = "", classes: string[] = []) {
        this.solution = solution;
        this.answer = answer;
        this.divs = divs;
        this.remain = remain;
        this.classes = classes;
    }
    fromObj(obj: any) {
        this.answer = obj.answer;
        this.classes = obj.classes;
        this.divs = obj.divs;
        this.remain = obj.remain;
        let solutions: string[] = obj.solution;
        if (solutions) {
            for (let i: number = 0; i < solutions.length; i++) {
               // //////console.log(solutions[i]);
                let solutionVo:SolutionVo=new SolutionVo();
                solutionVo.fromObj(solutions[i]);
                this.solution.push(solutionVo);
            }
        }
    }
}