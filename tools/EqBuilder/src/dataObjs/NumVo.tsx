

export class NumVo {
    public num: string;
    public val: number = 0;

    public whole: number = 0;
    public wholeString: string = "";
    public wholeLen: number = 0;

    public row: number = 0;

    public editMode: boolean = false;

    public isWholeNumber: boolean = true;

    public dec: number = 0;
    public decString: string = "";
    public decLen: number = 0;
    public decPlaces: number = 0;

    public all: string = "";
    public classes: string[] = [];

    public digits: number[] = [];
    public decs: number[] = [];
    public mdigits: number[] = [];
    public allDigits: number[] = [];

    public allChars: string[] = [];

    public callback: Function = () => { };

    private nString: string = "";

    constructor(num: string, editMode: boolean = false) {
        this.num = num;
        this.editMode = editMode;
        this.setUp();
    }
    setUp() {
        this.allDigits = [];
        this.allChars = [];
        this.digits = [];


        this.val = parseFloat(this.num);

        if (!this.num.includes(".")) {
            this.isWholeNumber = true;
        }
        else {
            this.isWholeNumber = false;
        }
        let decimalArray: string[] = this.num.split(".");
        this.decPlaces = decimalArray.length;

        this.decString = decimalArray[1] || "";
        this.whole = parseInt(decimalArray[0]);
        this.dec = parseFloat("." + decimalArray[1]);

        if (isNaN(this.whole)) {
            this.whole = 0;
        }
        if (isNaN(this.dec)) {
            this.dec = 0;
        }

        this.wholeString = this.whole.toString();
        this.wholeLen = this.wholeString.length;

        this.allChars = this.num.split("");
        this.decLen = this.decString.length;
        this.all = this.wholeString + "." + this.decString;


        let dArray: string[] = this.whole.toString().split("");
        for (let i: number = 0; i < dArray.length; i++) {

            this.digits.push(parseInt(dArray[i]));
            this.allDigits.push(parseInt(dArray[i]));
        }

        if (this.decString === "NaN") {
            this.decString = "";
        }

        if (this.decString.length !== 0) {
            let decArray: string[] = this.decString.split("");
            for (let i: number = 0; i < decArray.length; i++) {

                let decVal: number = parseInt(decArray[i]);

                if (!isNaN(decVal)) {
                    this.decs.push(decVal);
                    this.allDigits.push(decVal);
                }

            }
        }

        let digits2: number[] = this.digits.slice().reverse();


        for (let i: number = 0; i < digits2.length; i++) {
            let mval: number = digits2[i];

            mval = mval * (Math.pow(10, i));

            this.mdigits.push(mval);
        }
        ////////console.log(this);
    }
    addDecPlace() {
        if (this.isWholeNumber === true) {
            this.num += ".0";
        }
        else {
            this.num += "0";
        }
        this.setUp();
    }
    formatDigits(maxWhole: number, maxDec: number) {
        let wholeDif: number = maxWhole - this.wholeLen;
        let decDif: number = maxDec - this.decLen;

        let leadingZeros: string = "";
        let trailingZeros: string = "";

        if (wholeDif > 0) {
            leadingZeros = "0".repeat(wholeDif);
        }
        if (decDif > 0) {
            trailingZeros = "0".repeat(decDif);
        }

        let all: string = leadingZeros + this.num + trailingZeros;
        all = all.replace(".", "");

        let allArray: string[] = all.split("");
        //////////console.log(allArray);

        this.allDigits = [];
        for (let i: number = 0; i < allArray.length; i++) {
            let char: string = allArray[i];
            this.allDigits.push(parseInt(char));
        }
    }
    formatGridDigits(maxWhole: number, maxDec: number,hideDec:boolean=true) {
        let wholeDif: number = maxWhole - this.wholeLen;
        let decDif: number = maxDec - this.decLen;

        console.log(this.val,wholeDif,decDif);

        let leadingZeros: string = "";
        let trailingZeros: string = "";

        if (this.whole===0)
        {
            wholeDif=maxWhole;
        }

        if (wholeDif > 0) {
            leadingZeros = "0".repeat(wholeDif);
        }
        if (decDif > 0) {
            trailingZeros = "0".repeat(decDif);
        }
        
       
        let chars2:string=leadingZeros + this.wholeString;
        let chars3:string=this.decString+ trailingZeros;

        let nLenWhole=chars2.length;
        let nLenDec=chars3.length;

        console.log(chars2+"."+chars3,nLenWhole,nLenDec);

        let wholeDifBlank: number = 10-nLenWhole;
        let decDifBlank: number = 10-nLenDec;

        console.log(this.val,wholeDifBlank,decDifBlank);

        let leadingBlanks: string = "";
        let trailingBlanks: string = "";

        if (this.whole===0)
        {
            wholeDifBlank=9;
        }

        if (wholeDifBlank > 0) {
            leadingBlanks = "_".repeat(wholeDifBlank);
        }
        if (decDifBlank > 0) {
            trailingBlanks = "_".repeat(decDifBlank);
        }
        

        let all: string =leadingBlanks+ leadingZeros + this.num + trailingZeros+trailingBlanks;
        if (hideDec===true)
        {
            all = all.replace(".", "");
        }
      

        let allArray: string[] = all.split("");
        //////////console.log(allArray);

        this.allDigits = [];
        for (let i: number = 0; i < allArray.length; i++) {
            let char: string = allArray[i];
            this.allDigits.push(parseInt(char));
        }
        return all;
    }
    fixDigits(maxWhole: number, maxDec: number) {

        //fix whole
        let wholeDigits: number[] = this.digits.slice();
        while (wholeDigits.length < maxWhole) {
            wholeDigits.unshift(0);
        }

        //fix dec
        let decDigits: number[] = this.decs.slice();
        while (decDigits.length < maxDec) {
            decDigits.push(0);
        }
        //concat arrays
        /*   ////////console.log(maxWhole,maxDec);
          ////////console.log(wholeDigits,decDigits);
          ////////console.log("FIXED");
          ////////console.log(wholeDigits.concat(decDigits)); */
        this.allDigits = wholeDigits.concat(decDigits);
    }
    format(maxWhole: number, maxDec: number, hideTrail: boolean = true, trimDec: boolean = false) {

        if (this.whole === 0 && trimDec === true) {
            let trimmedDec: string = this.getTrimmedDec();
            this.allChars=trimmedDec.split("");
           // trimmedDec= this.getClassFormat();
            trimmedDec = trimmedDec.replaceAll("@", "<span class='hid'>0</span>");
            trimmedDec = trimmedDec.replace(".", "<span class='decp'>.</span>");
            return trimmedDec;
        }

        ////////console.log("MW="+maxWhole);
        //  //////console.log("MD="+maxDec);

        let wholeDif: number = maxWhole - this.wholeLen;
        let decDif: number = maxDec - this.decLen;

        let leadingZeros: string = "";
        let trailingZeros: string = "";

        if (wholeDif > 0) {
            leadingZeros = "@".repeat(wholeDif);
        }
        if (decDif > 0) {
            if (hideTrail === true) {
                trailingZeros = "@".repeat(decDif);
                // trailingZeros = "<span class='hid'>0</span>".repeat(decDif);
            }
            else {
                trailingZeros = "0".repeat(decDif);
            }
        }
        let fString: string = leadingZeros + this.num + trailingZeros;

        this.allChars = fString.split("");
        fString = this.getClassFormat();
        /* if (this.classes.length > 0) {
            fString = this.getClassFormat();
            //fString = leadingZeros + this.getClassFormat() + trailingZeros;
        }
        else {
            fString = fString.replace(".", "<span class='decp'>.</span>");
        } */
        fString = fString.replaceAll("@", "<span class='hid'>0</span>");

        return fString;
    }
    public getTrimmedDec() {
        this.nString = this.num;
        this.hideLeadingDec();
        ////////console.log(this.nString);
        return this.nString;
    }
    private hideLeadingDec() {
        for (let i: number = 10; i > 0; i--) {
            let searchString: string = "." + "0".repeat(i);
            this.nString = this.nString.replace(searchString, "@".repeat(i));
        }
    }
    private doClick() {
        alert("click");
    }
    private getClassFormat() {
        let chars: string = "";

        let classes: string[] = this.classes.slice().reverse();

        


        for (let i: number = this.allChars.length - 1; i > -1; i--) {
            let char: string = this.allChars[i];
            let i2: number = this.allChars.length - i - 1;
            let id: string = this.row.toString() + "_" + i2.toString();
           
            if (this.editMode === true) {
                if (char === ".") {
                    char = "<button id='btn" + id + "'><span id='" + id + "' class='decp'>.</span></button>";
                    classes.shift();
                }
                else {
                    //let className: string | undefined = classes.shift();
                    let className: string | undefined = classes[i];

                    //console.log(char);
                    if (char !== "@") {
                        if (className) {
                            char = "<button id='btn" + id + "'><span  id='" + id + "' class='" + className + "'>" + char + "</span></button>";
                        }
                        else {
                            char = "<button id='btn" + id + "'><span id='" + id + "'>" + char + "</span></button>";
                        }
                    }
                    else {
                        char = "<span id='" + id + "'>" + char + "</span>";
                    }
                }
            }
            else {
                if (char === ".") {
                    char = "<span  id='" + id + "' class='decp'>.</span>";
                    classes.shift();
                }
                else {
                    //let className: string | undefined = classes.shift();
                    let className: string | undefined = classes[i];


                    if (className) {
                        char = "<span  id='" + id + "' class='" + className + "'>" + char + "</span>";
                    }
                    else {
                        char = "<span id='" + id + "'>" + char + "</span>";
                    }
                }
            }

            chars = char + chars;
        }
        return chars;
    }
}