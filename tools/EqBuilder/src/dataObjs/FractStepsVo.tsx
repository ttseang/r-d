import { FractVo } from "./FractVo";

export class FractStepsVo
{
   public fracts:FractVo[];
   public ops:string[];

    constructor(fracts:FractVo[],ops:string[])
    {
        this.fracts=fracts;
        this.ops=ops;
    }
}