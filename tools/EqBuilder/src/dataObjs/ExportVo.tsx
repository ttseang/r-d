import { SpanVo } from "./SpanVo";
import { StoryVo } from "./StoryVo";

export class ExportVo {
    public expression: string;
    public solution: StoryVo[] = [];
    public spanVo:SpanVo[]=[];

    constructor(expression: string = "", solution: StoryVo[] = [],spanVo:SpanVo[]=[]) {
        this.expression = expression;
        this.solution = solution;
        this.spanVo=spanVo;
    }
    fromObj(obj: any) {
        //////console.log(obj);
        this.expression = obj.expression;
        let soultions: any = obj.solution;

        let spans:any=obj.spanVo;
        this.spanVo=[];

        ////console.log(spans);
        //////console.log(soultions);
        for (let i: number = 0; i < soultions.length; i++) {
            this.solution.push(new StoryVo(soultions[i].top, soultions[i].answer, soultions[i].class));
        }
        for (let j:number=0;j<spans.length;j++)
        {
            this.spanVo.push(new SpanVo(parseInt(spans[j].step),parseInt(spans[j].row),parseInt(spans[j].start),parseInt(spans[j].end),spans[j].className))
        }
    }
}