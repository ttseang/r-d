import { NumVo } from "../dataObjs/NumVo";
import { PlacesUtil } from "../util/PlacesUtil";

export class DivideDisplay {
    public divisor: NumVo;
    public dividend: NumVo;
    public solution: string[];
    public quotient: NumVo;
    public remain:string;
    public editMode:boolean;

    constructor(divisor: NumVo, dividend: NumVo, quotient: NumVo, solution: string[],remain:string,editMode:boolean=false) {
        
        //////console.log(divisor);
        //////console.log(dividend);
        
        this.divisor = divisor;
        this.dividend = dividend;
        this.quotient = quotient;
        this.solution = solution;       

        this.remain=remain;
        this.editMode=editMode;

        this.divisor.editMode=this.editMode;
        this.divisor.row=-1;
        this.dividend.editMode=this.editMode;
        this.dividend.row=-2;
        this.quotient.editMode=this.editMode;
        this.quotient.row=-3;
        

        (window as any).dd=this;
    }
    public getOutput() {
        let output: string[] = [];

        let maxDec:number=PlacesUtil.findMaxDecByObj([this.quotient,this.dividend]);
        let maxWhole:number=PlacesUtil.findMaxWholeByObj([this.quotient,this.dividend]);

      //  //////console.log("maxDec="+maxDec);
      //  //////console.log("maxWhole="+maxWhole);

        let rs:string="";

        if (this.remain)
        {
            rs="<span id='remain'>R"+this.remain+"</span>";
        }

        output.push("<div id='solutionGrid'>");
        output.push("<div></div>");
        output.push("<div id='quotient'>" + this.quotient.format(maxWhole, maxDec) +rs+ "</div>");
        output.push("<div id='divisor'>" + this.divisor.format(0,0) + "</div>");
        output.push("<div id='dividend'>" + this.dividend.format(maxWhole,maxDec) + "</div>");
        
        //  output.push("<div id='solutionGrid'>");
        for (let j: number = 0; j < this.solution.length; j++) {
            output.push(this.solution[j]);
        }

        output.push("</div>");


        return output;
    }
}