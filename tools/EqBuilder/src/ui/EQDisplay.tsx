import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

interface MyProps { fontSize: number,cardClass:string }
interface MyState { fontSize: number,cardClass:string }
class EQDisplay extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { fontSize: this.props.fontSize,cardClass:this.props.cardClass };

        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({fontSize:this.props.fontSize,cardClass:this.props.cardClass});
        }
    }
    render() {
        return (<Card className={this.state.cardClass}><Card.Body><div id="mathDisplay" style={{fontSize:this.state.fontSize.toString()+"px"}} className="equation">Enter an Expression</div></Card.Body></Card>)
    }
}
export default EQDisplay;