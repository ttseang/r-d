import { SpanVo } from "../dataObjs/SpanVo";

export class MainStorage {

  private static instance: MainStorage;

  public allSpans: SpanVo[] = [];
  public step: number = 0;
  public useRemainder:boolean=true;
  public zeroLimit:number=2;
  

  constructor() {

    (window as any).ms = this;
  }


  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }

    return this.instance;
  }

}
export default MainStorage;
