import React, { ChangeEvent, Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import { SpanVo } from '../dataObjs/SpanVo';
import EQDisplay from '../ui/EQDisplay';
import { EqEngine } from '../util/EqEngine';
interface MyProps { formula: string, callback: Function }
interface MyState { formula: string, step: number,paint:string }
class DesignScreen extends Component<MyProps, MyState>
{
    private eqEngine: EqEngine | null = null;
    private compMounted: boolean = false;

    private isDown: boolean = false;

    private ms: MainStorage = MainStorage.getInstance();
    private cArray:string[]=['erase','red','orange','blue','green','blink'];

    private dspanMap: Map<number, SpanVo> = new Map<number, SpanVo>();

    constructor(props: MyProps) {
        super(props);
        this.state = { formula: this.props.formula, step: this.ms.step,paint:this.cArray[0] };
    }
    componentDidMount() {
        setTimeout(() => {
            this.updateEq();
        }, 500);
        this.compMounted = true;

        this.mapSpans();

    }
    componentWillUnmount() {
        this.compMounted = false;
    }
    mapSpans() {
        for (let i: number = 0; i < this.ms.allSpans.length; i++) {
            let spanVo: SpanVo = this.ms.allSpans[i];
            let step: number = spanVo.step;
            let row: number = spanVo.row;
            let start: number = spanVo.start;
            //  let end:number=spanVo.end;
            let id3: number = step * 1000 + row * 100 + start;
            this.dspanMap.set(id3, spanVo);
        }

    }
    updateSpans() {
        let allSpans: SpanVo[] = [];
        this.dspanMap.forEach((s: SpanVo) => {
            allSpans.push(s);

        })
        //this.props.designCallback(allSpans);

        this.ms.allSpans = allSpans;

        ////console.log(allSpans);
        if (this.eqEngine) {
            this.eqEngine.resetSpans(allSpans);
            this.eqEngine.setSpanStep(this.state.step);
            // this.eqEngine.applySpanStyles();
        }

        /* setTimeout(() => {
            this.updateEq();
        }, 500); */
    }
    onDown(e: React.PointerEvent<HTMLElement>) {

        if (this.compMounted === true) {
            if (e.target) {
                //console.log((e.target as any).id);
                let id: string = (e.target as any).id;


                if (id) {
                    id = id.replace("btn", "");
                    //console.log(id);

                    let addressArray: string[] = id.split("_");
                    ////console.log(addressArray);

                    if (addressArray.length > 0) {
                        let step: number = this.state.step;
                        let row: number = parseInt(addressArray[0]);
                        let col: number = parseInt(addressArray[1]);

                        let id3: number = step * 1000 + row * 100 + col;
                        ////console.log(id3);
                        if (!isNaN(id3)) {
                            if (this.state.paint!=="erase")
                            {
                                let spanVo: SpanVo = new SpanVo(step, row, col, col, this.state.paint);
                                this.dspanMap.set(id3, spanVo);
                            }
                            else
                            {
                                this.dspanMap.delete(id3);
                            }
                          
                            //this.updateSpans();
                            setTimeout(() => {
                                this.updateEq();
                            }, 500);
                        }
                    }

                }

            }

        }
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ formula: this.props.formula });
        }
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    changeStep(e: ChangeEvent<HTMLInputElement>) {
        let step: number = parseInt(e.currentTarget.value);
        this.setState({ step: step });
        this.ms.step=step;
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    updateEq() {
        let eqEng: EqEngine = new EqEngine(this.state.formula, true);
        this.eqEngine = eqEng;
        
           eqEng.useRemain = this.ms.useRemainder;
           eqEng.zeroLimit = this.ms.zeroLimit;

        let html: string;
        if (this.state.step === 0) {
            html = eqEng.getHtml() || "error";
        }
        else {
            html = eqEng.getStep(this.state.step);
        }
        let div: HTMLDivElement = document.getElementById("mathDisplay") as HTMLDivElement;
        ////console.log(div);
        ////console.log(html);

        if (div) {
            div.innerHTML = html;
        }
        this.updateSpans();
        eqEng.applySpanStyles();
    }
    changePaint(e:ChangeEvent<HTMLSelectElement>)
    {
        let index:number=e.target.selectedIndex;
        let pClass:string=this.cArray[index];
        this.setState({paint:pClass});
    }
    getPaintDropDown()
    {
        let dd:JSX.Element[]=[];
        for (let i:number=0;i<this.cArray.length;i++)
        {
            let key:string="paint"+i.toString();

            dd.push(<option key={key}>{this.cArray[i]}</option>)
        }
        return (<select id='paintSelect' onChange={this.changePaint.bind(this)}>{dd}</select>)
    }
    render() {
        return (<div>           
            <Row><Col onPointerDown={this.onDown.bind(this)}><EQDisplay fontSize={32} cardClass={'blackboard'}></EQDisplay></Col></Row>
            <Card><Card.Body>
            <Row><Col className='tac'>{this.getPaintDropDown()}</Col></Row>
            <Row><Col className='tac'><input id="stepper1" min={0} onChange={this.changeStep.bind(this)} type='number' value={this.state.step} /></Col></Row>
            </Card.Body></Card>
        </div>)
    }
}
export default DesignScreen;