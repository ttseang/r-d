import React, { ChangeEvent, Component } from 'react';
import { Alert, Button, ButtonGroup, Card, Col, Row } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import CommonTab from '../tabs/CommonTab';
import FractTab from '../tabs/FractTab';
import TestTab from '../tabs/TestTab';
import EQDisplay from '../ui/EQDisplay';
import { EqEngine } from '../util/EqEngine';
import DesignScreen from './DesignScreen';
import DisplayScreen from './DisplayScreen';
interface MyProps { }
interface MyState { tabMode: number, tabMode2: number, fontSize: number, step: number, text: string, msg: string, zeroLimit: number, useRemain: boolean }
class BaseScreen extends Component<MyProps, MyState>
{
    private tabArray: string[] = ["Common", "Fractions", "Tests"];
    private tabArray2: string[] = ["Engine", "Design", "Import"];

    private ms: MainStorage = MainStorage.getInstance();

    private eqEngine: EqEngine | null = null;


    constructor(props: MyProps) {
        super(props);
        this.state = { tabMode: 2, tabMode2: 0, text: "", msg: "", fontSize: 26, step: this.ms.step, zeroLimit: this.ms.zeroLimit, useRemain: this.ms.useRemainder };
    }
    componentDidMount() {
        this.updateEq();
    }
    addToEq(s: string) {
        ////console.log(s);
        let t: string = this.state.text;
        t += s;
        this.setState({ text: t });
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    setEq(s: string) {
        this.setState({ text: s });
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    getTab() {
        switch (this.state.tabMode) {
            case 0:
                return <CommonTab callback={this.addToEq.bind(this)}></CommonTab>

            case 1:
                return <FractTab callback={this.addToEq.bind(this)}></FractTab>

            case 2:
                return <TestTab callback={this.setEq.bind(this)}></TestTab>
        }
        return "";
    }
    toggleTab(index: number) {
        this.setState({ tabMode: index });
    }
    toggleTab2(index: number) {
        this.setState({ tabMode2: index, step: this.ms.step });
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    getTabButtons() {
        let buttons: JSX.Element[] = [];
        for (let i: number = 0; i < this.tabArray.length; i++) {
            let key: string = "tab" + i.toString();
            buttons.push(<Button key={key} size="lg" variant='light' onClick={() => { this.toggleTab(i) }}>{this.tabArray[i]}</Button>)

        }
        return (<ButtonGroup>{buttons}</ButtonGroup>);
    }
    getTabButtons2() {
        let buttons: JSX.Element[] = [];
        for (let i: number = 0; i < this.tabArray2.length; i++) {
            let key: string = "tab" + i.toString();
            buttons.push(<Button key={key} size="lg" variant='light' onClick={() => { this.toggleTab2(i) }}>{this.tabArray2[i]}</Button>)

        }
        return (<ButtonGroup>{buttons}</ButtonGroup>);
    }
    setText(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.currentTarget.value })
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    updateEq() {
        let eqEng: EqEngine = new EqEngine(this.state.text);
        this.eqEngine = eqEng;

        eqEng.useRemain = this.state.useRemain;
        eqEng.zeroLimit = this.state.zeroLimit;

        this.eqEngine.allSpans = this.ms.allSpans;
        this.eqEngine.mapSpans();

        let html: string;
        html = eqEng.getStep(this.state.step);
        /* if (this.state.step === 0) {
            html = eqEng.getHtml() || "error";
        }
        else {
            html = eqEng.getStep(this.state.step);
        } */
        let div: HTMLDivElement = document.getElementById("mathDisplay") as HTMLDivElement;

        if (div) {
            div.innerHTML = html;
        }
        eqEng.applySpanStyles();
    }
    doCopy() {
        const copy = require("clipboard-copy");

        // eslint-disable-next-line no-control-regex
        // text=text.replaceAll(/[^\x00-\x7F]/g, "");
        // text = text.replace(/[\u{0080}-\u{FFFF}]/gu, "");
        copy(this.state.text);
        this.setState({ msg: "Text Copied" });

        setTimeout(() => {
            this.setState({ msg: "" });
            setTimeout(() => {
                this.updateEq();
            }, 200);
        }, 1000);
    }
    clearDisplay() {
        if (this.eqEngine) {
            this.ms.allSpans = [];
            this.eqEngine.resetSpans([]);
        }


        this.setState({ text: "", step: 0, fontSize: 26 });
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    getDisplay() {
        if (this.state.msg === "") {
            return (<Row><Col><EQDisplay cardClass='displayCard' fontSize={this.state.fontSize}></EQDisplay></Col></Row>)
        }
        else {
            return (<Card className="displayCard"><Card.Body><Row><Col><Alert variant='success'>{this.state.msg}</Alert></Col></Row></Card.Body></Card>)
        }
    }
    changeFontSize(e: ChangeEvent<HTMLInputElement>) {
        let fs: number = parseInt(e.currentTarget.value);
        this.setState({ fontSize: fs });
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    changeStep(e: ChangeEvent<HTMLInputElement>) {
        let step: number = parseInt(e.currentTarget.value);
        this.setState({ step: step });
        this.ms.step = step;
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    changeZeroLimit(e: ChangeEvent<HTMLInputElement>) {
        let zeroLimit: number = parseInt(e.currentTarget.value);
        this.ms.zeroLimit = zeroLimit;
        this.setState({ zeroLimit: zeroLimit });
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    changeRemain(e: React.ChangeEvent<HTMLInputElement>) {
        ////console.log(e.currentTarget.value);
        this.ms.useRemainder = !this.state.useRemain;
        this.setState({ useRemain: !this.state.useRemain });

        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    getDivTools() {
        if (!this.state.text.includes("/")) {
            return "";
        }
        let offOn: string = (this.state.useRemain === true) ? "on" : "off";

        return (<Card>
            <Card.Body>
                <Row><Col sm={4}>Zero Limit:</Col><Col className="tal"><input id="stepper1" min={0} onChange={this.changeZeroLimit.bind(this)} type='number' value={this.state.zeroLimit} /></Col></Row>
                <Row><Col sm={4}>Use Remainder:</Col><Col className='tal'><input type='checkbox' value={offOn} onChange={this.changeRemain.bind(this)}></input></Col></Row>
            </Card.Body>
        </Card>)
    }
    exportFile() {
        if (this.eqEngine) {
            let filename = "text.json";

            //  let text: string = this.ms.getExport();
            let text: string = this.eqEngine.getExport();
            ////console.log(text);

            if (text !== "") {
                var element = document.createElement('a');
                element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
                element.setAttribute('download', filename);
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            }
        }
    }

    getTopTabContent() {
        if (this.state.tabMode2 === 2) {
            return (<DisplayScreen callback={() => { this.setState({ tabMode2: 0 }) }}></DisplayScreen>)
        }
        if (this.state.tabMode2 === 1) {
            return <DesignScreen formula={this.state.text} callback={() => { this.setState({ tabMode2: 0 }); }}></DesignScreen>
        }
        return (<div>
            {this.getDisplay()}
            <hr />
            <Row>
                <Col className='tac' sm={8}><input id="text2" type='text' value={this.state.text} onChange={this.setText.bind(this)} /></Col>
                <Col>
                    <Row><Col sm={4}><Button variant='primary' onClick={this.clearDisplay.bind(this)}>Clear</Button></Col><Col sm={4}><span className="smText">Step:</span></Col><Col sm={2}><input id="stepper1" min={0} onChange={this.changeStep.bind(this)} type='number' value={this.state.step} /></Col></Row>
                    <Row> <Col sm={4}><Button className='m10' variant='success' onClick={() => { this.doCopy() }}>Copy</Button></Col><Col sm={4}><span className="smText">Text Size:</span></Col><Col sm={2}><input id="stepper1" min={0} onChange={this.changeFontSize.bind(this)} type='number' value={this.state.fontSize} /></Col></Row>
                    <Row><Col sm={4}><Button className='m10' onClick={this.exportFile.bind(this)}>Export</Button></Col></Row>
                </Col>
            </Row>
            <hr />
            {this.getDivTools()}
            <hr />
            <Row><Col>{this.getTabButtons()}</Col></Row>
            {this.getTab()}
        </div>)
    }
    render() {
        if (this.state.tabMode2 === 2) {
            return this.getTopTabContent();
        }
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Equation Builder</span>
                </Card.Header>
                <Card.Body>
                    <Row><Col>{this.getTabButtons2()}</Col></Row>
                    {this.getTopTabContent()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;