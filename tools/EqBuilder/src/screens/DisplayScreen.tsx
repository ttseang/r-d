import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { ExportDVo } from '../dataObjs/ExportDVo';
import { ExportMVo } from '../dataObjs/ExportMVo';
import { ExportVo } from '../dataObjs/ExportVo';
import { SpanVo } from '../dataObjs/SpanVo';

import { DivideStepDisplay } from '../ui/DivideStepDisplay';
import { TowerStepDisplay } from '../ui/TowerStepDisplay';
interface MyProps { callback: Function }
interface MyState { operator: string, mode: number, step: number }
class DisplayScreen extends Component<MyProps, MyState>
{
    private exportVo: ExportVo | null = null;
    private exportMVo: ExportMVo | null = null;
    private exportDVo: ExportDVo | null = null;
    private expression: string = "";
    private html: string = "";

    private spanMap: Map<number, SpanVo[]> = new Map<number, SpanVo[]>();

    //current spans classes
    public spans: SpanVo[] = [];
    public allSpans: SpanVo[] = [];

    constructor(props: MyProps) {
        super(props);
        this.state = { operator: "", mode: 0, step: 0 };
    }


    doUpload(e: ChangeEvent<HTMLInputElement>) {
        if (e.currentTarget) {
            if (e.currentTarget.files) {
                // let file:string=URL.createObjectURL(e.currentTarget.files[0]);
                let fileList: FileList | null = e.currentTarget.files;

                if (fileList.length > 0) {
                    const reader = new FileReader();
                    reader.onloadend = () => {
                        //  //////console.log(reader.result);
                        let result: string = reader.result as string;

                        let obj: any = JSON.parse(result);
                        //   //////console.log(obj);
                        //   //////console.log(obj.expression);
                        if (obj) {
                            if (obj['expression']) {
                                this.makeStory(obj);
                            }
                        }
                        //  this.props.callback(reader.result);
                    }
                    reader.readAsText(fileList[0]);
                }
                //   //////console.log(file);
                // this.setState({file:file});

            }
        }
    }
    makeStory(obj: any) {
        //////console.log(obj);

        if (obj['expression']) {
            //////console.log(obj.expression);

            let exp: string = obj.expression as string;
            this.expression = exp;

            if (exp.includes("/")) {
                let exportDVo: ExportDVo = new ExportDVo();
                exportDVo.fromObj(obj);
                this.exportDVo = exportDVo;
                this.allSpans = exportDVo.spanVo;
                this.setState({ mode: 4 });

            }

            if (exp.includes("*")) {
                let exportMVo: ExportMVo = new ExportMVo();
                exportMVo.fromObj(obj);
                this.exportMVo = exportMVo;
                this.allSpans = exportMVo.spanVo;

                this.setState({ mode: 3 })
            }
            if (exp.includes("-")) {
                let exportVo: ExportVo = new ExportVo();
                exportVo.fromObj(obj);
                this.exportVo = exportVo;
                this.allSpans = exportVo.spanVo;
                this.setState({ mode: 2 });
            }
            if (exp.includes("+")) {
                let exportVo: ExportVo = new ExportVo();
                exportVo.fromObj(obj);
                this.exportVo = exportVo;
                this.allSpans = exportVo.spanVo;
                this.setState({ mode: 1 });
            }
        }
        this.mapSpans();
    }
    private mapSpans() {
        for (let i: number = 0; i < this.allSpans.length; i++) {
            let spanVo: SpanVo = this.allSpans[i];

            let spans2: SpanVo[] = this.spanMap.get(spanVo.step) || [];
            spans2.push(spanVo);

            this.spanMap.set(spanVo.step, spans2);
        }
    }
    getDisplay() {


        let tsd: TowerStepDisplay = new TowerStepDisplay(this.expression);
        let dsd: DivideStepDisplay = new DivideStepDisplay(2, false);

        let nums: string[] = [];

        switch (this.state.mode) {
            case 0:
                return (<Row><Col sm={6} className="tar">Upload File</Col><Col><input type='file' onChange={this.doUpload.bind(this)} /></Col></Row>)

            case 1:
                nums = this.expression.split("+");
                if (this.exportVo) {
                    this.html = "<div  class='md2 equation'>" + tsd.displayAdd(nums, this.exportVo.solution, this.state.step) + "</div>";
                }
                break;

            // return "Tower Display +";
            case 2:
                nums = this.expression.split("-");
                if (this.exportVo) {
                    this.html = "<div  class='md2 equation'>" + tsd.displaySub(nums, this.exportVo.solution, this.state.step) + "</div>";
                }
                break;

            case 3:
                nums = this.expression.split("*");
                if (this.exportMVo) {
                    this.html = "<div  class='md2 equation'>" + tsd.getMultiStep(nums, this.exportMVo.stories, this.state.step) + "</div>";
                }
                break;

            case 4:
                nums = this.expression.split("/");
                if (this.exportDVo) {
                    this.html = "<div  class='md2 equation'>" + dsd.getStepDisplay(nums, this.exportDVo.stories, this.state.step).join("") + "</div>";

                }

        }
        setTimeout(() => {
            this.updateEq();
        }, 100);
        return "";
    }
    updateEq() {

        let div: HTMLDivElement = document.getElementById("mathDisplay2") as HTMLDivElement;
        if (div) {
            div.innerHTML = this.html;
        }
        if (this.spanMap.has(this.state.step)) {
            this.spans = this.spanMap.get(this.state.step) || [];
        }
        this.applySpanStyles();
    }
    applySpanStyles() {
        for (let i: number = 0; i < this.spans.length; i++) {
            ////console.log(i);
            this.spans[i].applyStyles();
        }
    }
    prevStep() {
        let step: number = this.state.step;
        this.setState({ step: step - 1 });
    }
    nextStep() {
        let step: number = this.state.step;
        this.setState({ step: step + 1 });
    }
    gotKey(e: React.KeyboardEvent<HTMLDivElement>) {
        //////console.log(e.key);
        let num: number = parseInt(e.key);

        if (!isNaN(num)) {
            //////console.log(num);
            this.setState({ step: num });
        }
        if (e.key === "Escape") {
            this.props.callback();
        }
        if (e.key === "ArrowRight") {
            this.nextStep();
        }
        if (e.key === "ArrowLeft") {
            this.prevStep();
        }
    }

    render() {
        return (<div tabIndex={0} onKeyDown={this.gotKey.bind(this)}>

            {this.getDisplay()}
            <div id="mathDisplay2"></div>

        </div>)
    }
}
export default DisplayScreen;