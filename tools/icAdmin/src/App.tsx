import React from 'react';
import './App.css';
import './IceCreamStyles.css';
import './TextAnimationStyles.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import BaseScreen from './screens/BaseScreen';
function App() {
  return (
    <div id="base"><BaseScreen></BaseScreen></div>
  );
}
export default App;