import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { ButtonConstants } from '../classes/constants/ButtonConstants';
import { AnimationVo } from '../classes/dataObjs/AnimationVo';
import { ButtonVo } from '../classes/dataObjs/ButtonVo';
import { ElementAnimationVo } from '../classes/dataObjs/ElementAnimationVo';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import ButtonLine from '../classes/ui/ButtonLine';
import AnimationBox from '../comps/AnimationBox';
interface MyProps { element: ElementVo | null }
interface MyState { element: ElementVo | null, mode: number, filterButtons: ButtonVo[], showIn: boolean }
class AnimationScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { element: this.props.element, showIn: true, mode: 0, filterButtons: ButtonConstants.ANIMATION_FILTERS.slice() }
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ element: this.props.element });
        }
    }
    checkAnimation(animationVo: AnimationVo, checked: boolean) {
        let value: number = animationVo.id;

        if (checked === false) {
            value = -1;
        }
        if (this.ms.selectedElement) {

            let element: ElementVo = this.ms.selectedElement;

            let anim: ElementAnimationVo = element.animationOut;

            if (this.state.showIn === true) {
                anim = element.animationIn;
            }

            switch (animationVo.subType) {

                case AnimationVo.SUB_TYPE_SPEED:
                    anim.speed = value;
                    break;

                case AnimationVo.SUB_TYPE_FADE:
                    anim.fade = value;
                    break;

                case AnimationVo.SUB_TYPE_MOVE:
                    anim.move = value;
                    break;

                case AnimationVo.SUB_TYPE_SIZE:
                    anim.grow = value;
                    break;
            }

            if (this.state.showIn === true) {
                element.animationIn = anim;
            }
            else {
                element.animationOut = anim;
            }
            this.ms.selectedElement = element;

            this.setState({ element: element })
        }
    }
    getFiltered(subtype: string) {
        let fboxes: JSX.Element[] = [];
        if (this.state.element) {
            let inString: string = (this.state.showIn === true) ? AnimationVo.DIR_IN : AnimationVo.DIR_OUT;
            let animationVos: AnimationVo[] = this.ms.getAnimations(this.state.element.type, subtype, inString);

            console.log(animationVos);

            for (let i: number = 0; i < animationVos.length; i++) {

                let animationVo: AnimationVo = animationVos[i];

                let key: string = subtype + i.toString();
                let check: boolean = false;
                if (this.ms.selectedElement) {
                    if (this.state.showIn) {
                        check = this.ms.selectedElement.animationIn.isChecked(animationVo.id);
                    }
                    else {
                        check = this.ms.selectedElement.animationOut.isChecked(animationVo.id);
                    }
                }

                fboxes.push(<Col sm={3} key={key}><AnimationBox animationVo={animationVos[i]} callback={this.checkAnimation.bind(this)} checked={check}></AnimationBox></Col>)
            }

        }
        let rowKey: string = "row" + subtype;

        return (<Row key={rowKey}>{fboxes}</Row>);
    }
    getAnimationBoxes() {

        let boxes: JSX.Element[] = [];

        if (this.state.mode === 0 || this.state.mode === 4) {
            boxes.push(<h2 key="speedHead">Speed</h2>);
            boxes = boxes.concat(this.getFiltered(AnimationVo.SUB_TYPE_SPEED));
            boxes.push(<hr key="hr3" />);
        }

        if (this.state.mode === 0 || this.state.mode === 1) {
            boxes.push(<h2 key="fadeHead">Fade</h2>);
            boxes = boxes.concat(this.getFiltered(AnimationVo.SUB_TYPE_FADE));
            boxes.push(<hr key="hr1" />);
        }

        if (this.state.mode === 0 || this.state.mode === 2) {
            boxes.push(<h2 key="moveHead">Move</h2>);
            boxes = boxes.concat(this.getFiltered(AnimationVo.SUB_TYPE_MOVE));
            boxes.push(<hr key="hr2" />);
        }

        if (this.state.mode === 0 || this.state.mode === 3) {
            boxes.push(<h2 key="sizeHead">Size</h2>);
            boxes = boxes.concat(this.getFiltered(AnimationVo.SUB_TYPE_SIZE));
        }
        // console.log(boxes);

        return boxes;
    }
    doInOutAction(index: number, action: number) {
        console.log(action);
        switch (action) {
            case 0:
                this.setState({ showIn: true });
                break;
            case 1:
                this.setState({ showIn: false });
                break;
        }
    }
    doFilter(index: number, action: number) {
        let buttons: ButtonVo[] = this.state.filterButtons.slice();

        for (let i: number = 0; i < buttons.length; i++) {
            buttons[i].variant = "light";

            if (i === action) {
                buttons[i].variant = "dark";
            }
        }
        this.setState({ mode: action, filterButtons: buttons });
    }
    render() {
        return (<Card>
            <Card.Body>
                <div id="inOutLine"><ButtonLine buttonArray={ButtonConstants.IN_OUT_BUTTONS} actionCallback={this.doInOutAction.bind(this)} useVert={false}></ButtonLine></div>
                <div id="animFilterLine"><ButtonLine buttonArray={ButtonConstants.ANIMATION_FILTERS} actionCallback={this.doFilter.bind(this)} useVert={false}></ButtonLine></div>
                <div className="scroll1">{this.getAnimationBoxes()}</div>
            </Card.Body>
            <Card.Footer className="tac"><Button onClick={() => { this.mc.changeScreen(0) }}>Done</Button></Card.Footer>
        </Card>)
    }
}
export default AnimationScreen;