import React, { ChangeEvent, Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { ButtonConstants } from '../classes/constants/ButtonConstants';
import { FormElementVo } from '../classes/dataObjs/FormElementVo';
import { MainController } from '../classes/MainController';

import MainStorage from '../classes/MainStorage';
import ButtonLine from '../classes/ui/ButtonLine';
interface MyProps { formElements: FormElementVo[] }
interface MyState { formElements: FormElementVo[], formValues: string[] }
class FormScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);

        let fv: string[] = [];
        for (let i: number = 0; i < props.formElements.length; i++) {
            fv.push("");
        }

        this.state = { formElements: this.props.formElements, formValues: fv };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ formElements: this.props.formElements })
        }
    }
    componentDidMount() {
        this.getValues();
    }
    onChange(e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {

        let index: number = parseInt(e.currentTarget.id.replace("f", ""));
        let formValues: string[] = this.state.formValues.slice();
        formValues[index] = e.currentTarget.value;
        this.setState({ formValues: formValues });
    }
    getValues() {
        let allValues: string[] = [];
        for (let i: number = 0; i < this.state.formElements.length; i++) {
            let value: string = this.ms.selectedElement?.content[i] || "?";
            allValues.push(value);
        }
        this.setState({ formValues: allValues });
    }
    getForm() {
        let form: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.formElements.length; i++) {
            let type: number = this.state.formElements[i].type;
            let label: string = this.state.formElements[i].label;
            let key: string = "formEl" + i.toString();
            let id: string = "f" + i.toString();

            switch (type) {
                case FormElementVo.TYPE_NUMBER:
                    form.push(<div key={key}>{label}<input type="number" /></div>);
                    break;

                case FormElementVo.TYPE_STRING:
                    form.push(<Row key={key}><Col className="tar" sm={2}>{label}</Col><Col><input id={id} type='text' onChange={this.onChange.bind(this)} value={this.state.formValues[i]} /></Col></Row>)
                    break;

                case FormElementVo.TYPE_BLOB:
                form.push(<Row key={key}><Col className="tar" sm={2}>{label}</Col><Col><textarea id={id} style={{width:"100%"}} rows={7} onChange={this.onChange.bind(this)} value={this.state.formValues[i]}></textarea></Col></Row>)
                    
            
                }
        }
        return form;
    }
    doAction(index: number, action: number) {
        console.log(index,action);

        switch(action)
        {
            case 0:
                this.mc.changeScreen(0);
            break;

            case 1:
                if (this.ms.selectedElement)
                {
                    this.ms.selectedElement.content=this.state.formValues.slice();
                }                
                this.mc.changeScreen(0);

        }
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    <div style={{ width: "70%", margin: "auto" }}>
                        <Card>
                            <Card.Body>
                                {this.getForm()}
                            </Card.Body>
                        </Card>
                    </div>
                </Card.Body>
                <Card.Footer><div style={{ width: "50%", margin: "auto" }}><ButtonLine buttonArray={ButtonConstants.DONE_CANCEL} actionCallback={this.doAction.bind(this)} useVert={false}></ButtonLine></div></Card.Footer>
            </Card>
        </div>)
    }
}
export default FormScreen;