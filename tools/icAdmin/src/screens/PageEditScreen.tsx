import React, { Component } from 'react';
import { Row, Col} from 'react-bootstrap';
import { AlignUtil } from '../classes/AlignUtil';
import { FormConstants } from '../classes/constants/FormConstants';
import { ButtonIconVo } from '../classes/dataObjs/ButtonIconVo';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../classes/dataObjs/ExtrasVo';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { StepVo } from '../classes/dataObjs/StepVo';

import { MainController } from '../classes/MainController';
import { MainStorage } from '../classes/MainStorage';
import ElementBar from '../comps/ElementBar';
import PreviewPage from '../comps/PreviewPage';
import PropBox from '../comps/rightsidepanel/PropBox';
import StepPanel from '../comps/StepPanel';
import TopBar from '../comps/TopBar';


interface MyProps { steps: StepVo[], doneCallback: Function }
interface MyState { steps: StepVo[], sideMode: number, step: StepVo | null, selectedElement: ElementVo | null }

class PageEditScreen extends Component<MyProps, MyState>
{
    private addElements: ButtonIconVo[] = [];
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private editElement: ElementVo | null = null;
    private isDown: boolean = false;
    private lineCircleIndex: number = 0;
    private changeFlag: boolean = false;
    private pageMounted:boolean=false;

    constructor(props: MyProps) {
        super(props);

        this.addElements.push(new ButtonIconVo("Image", "fas fa-image", "light", 0, "sm"));
        this.addElements.push(new ButtonIconVo("Text", "fas fa-font", "light", 1, "sm"));
        this.addElements.push(new ButtonIconVo("Card", "far fa-address-card", "light", 5, "sm"));
     //   this.addElements.push(new ButtonIconVo("Line", "fas fa-horizontal-rule", "light", 6, "sm"));
    

        this.mc.undo = this.goBack.bind(this);
        this.mc.redo = this.goForward.bind(this);

        this.mc.copyElement=this.copyElementToClipboard.bind(this);
        this.mc.pasteElement=this.pasteElement.bind(this);
        
        this.mc.doKeyboardShortCut = this.doShortCut.bind(this);

        let firstStep: StepVo | null = null;
        if (this.props.steps.length > 0) {
            firstStep = this.props.steps[0];
        }

        let selectedStep: StepVo | null = this.ms.selectedStep;

        if (selectedStep === null) {
            selectedStep = firstStep;
        }


        this.state = { steps: this.props.steps, step: selectedStep, sideMode: 0, selectedElement: null };
    }
    componentDidMount() {
        if (this.ms.selectedElement !== null) {
            this.setState({ selectedElement: this.ms.selectedElement, step: this.ms.selectedStep });
        }
        this.pageMounted=true;
    }
    componentWillUnmount()
    {
        this.pageMounted=false;
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ steps: this.props.steps });
        }
    }
    copyElementToClipboard()
    {
        if (this.ms.selectedElement)
        {
            this.ms.copyElement=this.ms.selectedElement.clone2();
        }        
    }
    pasteElement()
    {
        if (this.ms.copyElement)
        {
            let pasteElement:ElementVo=this.ms.copyElement.clone2();
            
            if (this.pageHasElement(this.ms.copyElement.eid))
            {
               this.ms.eidIndex++;
               pasteElement.eid=this.ms.eid;
            }

            pasteElement.id=ElementVo.getNewID();

            let stepVo: StepVo = this.ms.steps[this.ms.currentStep];
            stepVo.addElement(pasteElement);

            this.ms.addHistory();
            

            this.ms.selectedElement = pasteElement;

            this.setState({step:this.ms.steps[this.ms.currentStep]});
        }        
    }
    pageHasElement(eid:string)
    {
        if (this.state.step)
        {
            for (let i:number=0;i<this.state.step.elements.length;i++)
            {
                let element:ElementVo=this.state.step.elements[i];
                if (element.eid===eid)
                {
                    return true;
                }
            }
        }
        return false;
      
    }
    doShortCut(key: string, ctr: boolean, alt: boolean) {
        /* console.log(key);
        console.log(ctr);
        console.log(alt); */
        //console.log(this.pageMounted);
        if (this.pageMounted===false)
        {
            return;
        }
        let cString: string = (ctr === false) ? "0" : "1";
        let aString: string = (alt === false) ? "0" : "1";

        let keyString: string = cString + aString + key;
        console.log(keyString);

        switch (keyString) {
            case "10z":
                this.goBack();
                break;

            case "10y":
                this.goForward();
                break;

            case "10c":
                this.copyElementToClipboard();
                break;
            case "10v":
                this.pasteElement();
                break;

            case "00Delete":
                if (this.state.selectedElement)
                {
                    this.deleteElement(this.state.selectedElement);
                    this.ms.addHistory();
                    this.setState({selectedElement:null});
                }
               
                break;
        }


    }
    onMove(e: React.PointerEvent<SVGElement>) {
        e.preventDefault();

        if (this.isDown === true && this.state.selectedElement) {

            let selected: ElementVo = this.state.selectedElement;

            /**
             * convert movement in pixels to percentages
             */
            let perMoveX: number = Math.floor((e.movementX / this.ms.pageW) * 10000) / 100;
            let perMoveY: number = Math.floor((e.movementY / this.ms.pageH) * 10000) / 100;


            if (selected.type === ElementVo.TYPE_LINE) {
                this.moveLine(perMoveX, perMoveY);
                return;
            }

            /**
             * update position
             */
            selected.x += perMoveX;
            selected.y += perMoveY;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }

    }
    releaseElement(elementVo: ElementVo) {
        this.isDown = false;
        console.log("element released");
    }
    onElementDown(elementVo: ElementVo) {

        this.selectElement(elementVo);
        this.isDown = true;
    }
    onLineDown(elementVo: ElementVo, index: number) {
        console.log("line=" + index);
        this.selectElement(elementVo);
        this.isDown = true;
        this.lineCircleIndex = index;
    }
    moveLine(perMoveX: number, perMoveY: number) {
        let selected: ElementVo | null = this.state.selectedElement;
        if (selected) {
            switch (this.lineCircleIndex) {
                case 0:
                    selected.x += perMoveX;
                    selected.y += perMoveY;
                    break;

                case 1:
                    selected.w += perMoveX;
                    selected.h += perMoveY;
                    break;

                case 2:
                    selected.x += perMoveX;
                    selected.y += perMoveY;
                    selected.w += perMoveX;
                    selected.h += perMoveY;

            }
            this.changeFlag=true;
            this.setState({ selectedElement: selected });
        }
    }
    onLineUp(elementVo: ElementVo, index: number) {
        this.isDown = false;
        //  console.log("line up");
    }
    toggleVis(elementVo: ElementVo) {
        elementVo.visible = !elementVo.visible;
        this.ms.selectedElement = elementVo;

        this.setState({ selectedElement: elementVo });
    }
    toggleLock(elementVo: ElementVo) {
        elementVo.locked = !elementVo.locked;
        this.ms.selectedElement = elementVo;

        this.setState({ selectedElement: elementVo });
    }
    selectElement(elementVo: ElementVo) {


        this.ms.suggested = elementVo.suggested;
        this.ms.selectedElement = elementVo;

        this.setState({ selectedElement: elementVo, sideMode: 0 });
    }
    editTheElement(elementVo: ElementVo | null) {
        if (elementVo === null) {
            return;
        }

        this.editElement = elementVo;
        if (this.editElement.type === ElementVo.TYPE_IMAGE || this.editElement.type === ElementVo.TYPE_BACK_IMAGE) {
            this.mc.mediaChangeCallback = this.updateElementImage.bind(this);
            this.mc.openImageBrowse();
        }
        if (this.editElement.type === ElementVo.TYPE_TEXT) {
            this.ms.editText = this.editElement.content[0];
            this.mc.textEditCallback = this.updateElementText.bind(this);
            this.mc.openTextEdit();
        }

        if (this.editElement.type === ElementVo.TYPE_CARD) {
            switch (this.editElement.subType) {
                case ElementVo.SUB_TYPE_GREEN_CARD:

                    this.mc.openForm(FormConstants.GREEN_CARD_FORM);
                    break;

                case ElementVo.SUB_TYPE_WHITE_CARD:
                    this.mc.openForm(FormConstants.WHITE_CARD_FORM);

            }

        }
    }
    updateElementImage(libItem: LibItemVo) {
        if (this.editElement) {
            this.editElement.content[0] = libItem.image;
            this.mc.closeImageBrowse();
            this.ms.addHistory();
        }
    }
    updateElementText(text: string) {
        if (this.editElement) {
            this.editElement.content[0] = text;
            this.mc.closeTextEdit();
            this.ms.addHistory();
        }
    }
    updateProps(xx: number, yy: number, ww: number) {


        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.x = xx;
            selected.y = yy;
            selected.w = ww;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    updateLine(xx: number, yy: number, ww: number, hh: number) {


        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.x = xx;
            selected.y = yy;
            selected.w = ww;
            selected.h = hh;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    updateBackgroundProps(xx: number, yy: number, ww: number, hh: number) {
        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.extras.backgroundPosX = xx;
            selected.y = yy;
            selected.extras.backgroundSizeW = ww;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    updateInstanceName(eid:string)
    {
        if (this.state.selectedElement)
        {
            let selected: ElementVo = this.state.selectedElement;
            selected.eid=eid;
            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    getSideContent() {

        let allElements: ElementVo[] = [];
        if (this.state.step) {
            allElements = this.state.step.elements;
        }
        return (<ElementBar addElements={this.addElements} sideMode={this.state.sideMode} elements={allElements} selectedElement={this.state.selectedElement} addNewElement={this.mc.addNewElement} updateElementRows={this.updateElementRows.bind(this)} selectElement={this.selectElement.bind(this)} toggleVis={this.toggleVis.bind(this)} toggleLock={this.toggleLock.bind(this)}>
        </ElementBar>
        );
    }
   
    updateElementRows(pos: number[]) {


        let allElements: ElementVo[] = [];
        if (this.state.step) {
            allElements = this.state.step.elements;
        }
        let nElements: ElementVo[] = [];
        for (let i: number = 0; i < allElements.length; i++) {
            nElements.push(allElements[pos[i]]);
        }

        this.ms.steps[this.ms.currentStep].elements = nElements;

        this.ms.addHistory();

    }
    doPropAction(action: number, param: number = 0) {


        if (this.state.selectedElement) {

            let elementVo: ElementVo | null = this.state.selectedElement;

            switch (action) {
                case 0:
                    //  this.changeElementPage();
                    break;
                case 1:
                    let flippedH: boolean = !elementVo.extras.flipH;
                    elementVo.extras.flipH = flippedH;
                    this.ms.addHistory();
                    break;

                case 2:
                    let flippedV: boolean = !elementVo.extras.flipV;
                    elementVo.extras.flipV = flippedV;
                    this.ms.addHistory();
                    break;

                case 3:
                    let alignUtil: AlignUtil = new AlignUtil();
                    alignUtil.alignElement(param, elementVo);
                    this.ms.addHistory();
                    break;

                case 4:
                    elementVo.extras.orientation = param;
                    this.ms.addHistory();
                    break;

                case 5:
                    this.editTheElement(this.state.selectedElement);
                    break;

                case 12:
                    elementVo = this.copyElement(elementVo);
                    this.ms.addHistory();
                    break;
                case 13:
                    this.deleteElement(elementVo);
                    elementVo = null;
                    this.ms.addHistory();
                    break;
            }

            if (action > 0) {
                this.setState({ selectedElement: elementVo });
            }
        }
    }
    deleteElement(delElement: ElementVo) {
        let stepVo: StepVo = this.ms.steps[this.ms.currentStep];

        let elements: ElementVo[] = stepVo.deleteElement(delElement);
        this.ms.steps[this.ms.currentStep].elements = elements;

    }
    copyElement(copyElementVo: ElementVo) {
        let elementVo: ElementVo = copyElementVo.clone();

        let stepVo: StepVo = this.ms.steps[this.ms.currentStep];

        stepVo.addElement(elementVo);

        this.ms.selectedElement = elementVo;
        return elementVo;
    }
    updateExtras(extrasVo: ExtrasVo) {
        if (this.state.selectedElement) {
            this.changeFlag = true;
            let elementVo: ElementVo = this.state.selectedElement;
            elementVo.extras = extrasVo;
            this.setState({ selectedElement: elementVo });
        }
    }
    getPropBox() {
        return (<PropBox key="propBox" elementVo={this.state.selectedElement} step={this.state.step} updateLine={this.updateLine.bind(this)} callback={this.updateProps.bind(this)} actionCallback={this.doPropAction.bind(this)} updateExtras={this.updateExtras.bind(this)} updateBackgroundProps={this.updateBackgroundProps.bind(this)} updateInstance={this.updateInstanceName.bind(this)}></PropBox>)
    }

    getPreview() {
        return (<div id="preview"><PreviewPage editCallback={this.onElementDown.bind(this)} onUp={this.releaseElement.bind(this)} stepVo={this.state.step} onMove={this.onMove.bind(this)} lineDown={this.onLineDown.bind(this)} lineUp={this.onLineUp.bind(this)}></PreviewPage></div>)
    }
      
    selectStep(stepVo: StepVo) {
        this.ms.selectedElement=null;
        this.setState({ step: this.ms.selectedStep, selectedElement: null });
       
    }
    goBack() {
        this.ms.goBack();
        this.ms.selectedElement=null;
        this.setState({ step: this.ms.selectedStep, selectedElement: null });
        this.mc.stepsUpdated();
    }
    goForward() {
        this.ms.goForward();
        this.ms.selectedElement=null;
        this.setState({ step: this.ms.selectedStep, selectedElement: null });
        this.mc.stepsUpdated();
    }
    doSaveAction(action: number) {
        switch (action) {
            case 1:
               // alert("TODO:Save");
                this.mc.saveZoom();
                break;

            case 0:
               // alert("TODO:Load");
                this.mc.openZoom();
                break;
            case 2:
                this.mc.changeScreen(3);
                break;

            case 3:
                this.mc.exportFile();
                break;

            case 10:
                this.mc.duplicateFrame();
                this.ms.addHistory();
                break;

            case 11:
                this.mc.addBlankFrame();
                this.ms.addHistory();
                break;

            case 12:
                this.mc.delFrame();
                this.ms.addHistory();
                break;

            case 20:
                this.mc.undo();
                break;

            case 21:
                this.mc.redo();
                break;

            case 22:
                this.copyElementToClipboard();
                break;
            case 23:
                this.pasteElement();
                break;
        }
    }
    onUp() {
        this.isDown = false;
        console.log("on up");
        if (this.changeFlag === true) {
            this.changeFlag = false;
            this.ms.addHistory();
        }
    }
    render() {

        return (<div onPointerUp={(e: React.PointerEvent<HTMLDivElement>) => { this.onUp() }}>
            <TopBar callback={this.doSaveAction.bind(this)}></TopBar>
            <Row><Col sm={3}><Row><Col>{this.getSideContent()}</Col></Row></Col><Col sm={6}><Row><Col><StepPanel selectCallback={this.selectStep.bind(this)}></StepPanel></Col></Row><hr /><Row><Col><div id="pholder">{this.getPreview()}</div></Col></Row></Col><Col sm={3}>{this.getPropBox()}</Col></Row>

        </div>)
    }
}
export default PageEditScreen;