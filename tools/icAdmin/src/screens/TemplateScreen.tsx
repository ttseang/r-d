import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { TemplateVo } from '../classes/dataObjs/TemplateVo';
import MainStorage from '../classes/MainStorage';
import TemplateBox from '../comps/TemplateBox';
interface MyProps { }
interface MyState {}
class TemplateScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    getBoxes()
    {
        let templates:TemplateVo[]=this.ms.pageTemplates;

        let boxes:JSX.Element[]=[];
        for (let i:number=0;i<templates.length;i++)
        {
            let template:TemplateVo=templates[i];
            let key="pageTemplate"+template.id.toString();
            boxes.push(<Col key={key} sm={3}><TemplateBox  templateVo={template}></TemplateBox></Col>);
        }
        return (<Row>{boxes}</Row>);
    }
    render() {
        return (<div>{this.getBoxes()}</div>)
    }
}
export default TemplateScreen;