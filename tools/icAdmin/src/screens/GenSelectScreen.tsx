import React, { Component } from 'react';
import { GenVo } from '../classes/dataObjs/GenVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import GenMenu from '../classes/ui/GenMenu';
interface MyProps {callback:Function}
interface MyState {menu:GenVo[]}
class GenSelectScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {menu:this.ms.menu};
    }

    getMenu()
    {
        return <GenMenu genVo={this.state.menu} callback={this.selectCard.bind(this)}></GenMenu>
    }
    selectCard(genVo:GenVo)
    {
        console.log(genVo);
        this.props.callback(genVo);
    }
    render() {
        return (<div>{this.getMenu()}</div>)
    }
}
export default GenSelectScreen;