import { LibItemVo } from "./dataObjs/LibItemVo";

export class LibUtil {
    public library: LibItemVo[] = [];
    public audioLib:LibItemVo[]=[];
    public backgroundAudio:LibItemVo[]=[];

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        //TO DO move to JSON

    }
    getFilter(filter: string) {

        //console.log.log(filter);

        let filteredItems: LibItemVo[] = [];
        for (let i: number = 0; i < this.library.length; i++) {
            let current: LibItemVo = this.library[i];
            if (current.tags.includes(filter)) {
                //console.log.log(current);
                filteredItems.push(current);
            }
        }
        return filteredItems;
    }
    getAudioFilter(filter: string,background:boolean=false) {

        //console.log.log(filter);

        let lib:LibItemVo[]=this.audioLib;

        if (background===true)
        {
            lib=this.backgroundAudio;
        }

        let filteredItems: LibItemVo[] = [];
        for (let i: number = 0; i < lib.length; i++) {
            let current: LibItemVo = lib[i];
            if (current.tags.includes(filter)) {
                //console.log.log(current);
                filteredItems.push(current);
            }
        }
        return filteredItems;
    }
}