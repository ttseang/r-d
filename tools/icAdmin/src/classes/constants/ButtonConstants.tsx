import { ButtonIconVo } from "../dataObjs/ButtonIconVo";
import { ButtonVo } from "../dataObjs/ButtonVo";
import { GenVo } from "../dataObjs/GenVo";

export class ButtonConstants
{
    //static Type BUTTON_VARIANTS:"primary"|"success"|"warning"|"info"|"light"|"dark";

    static PAGE_SELECT:ButtonVo[]=[new ButtonVo("Left","dark",0),new ButtonVo("Right","dark",1)];
    static EDIT_TABS:ButtonVo[]=[new ButtonVo("Name","light",0),new ButtonVo("Element","light",1),new ButtonVo("Options","light",3)];

    static FLIP_BUTTONS:ButtonVo[]=[new ButtonVo("Flip H",'primary',1),new ButtonVo("Flip V",'primary',2)];
    static PANEL_BUTTONS:ButtonVo[]=[new ButtonVo("Content",'light',0),new ButtonVo("Position",'light',1),new ButtonVo("Animation",'light',18),new ButtonVo("Align",'light',2),new ButtonVo("Flip",'light',3),new ButtonVo("Orientation",'light',4),new ButtonVo("Border",'light',5),new ButtonVo("Advanced",'light',6)];

    static TEXT_PANEL_BUTTONS:ButtonVo[]=[new ButtonVo("Content",'light',0),new ButtonVo("Position",'light',1),new ButtonVo("Text Styles","light",19),new ButtonVo("Animation",'light',18),new ButtonVo("Font",'light',7),new ButtonVo("Align",'light',2),new ButtonVo("Flip",'light',3),new ButtonVo("Orientation",'light',4),new ButtonVo("Border",'light',5),new ButtonVo("Advanced",'light',6)];
    static LINE_PANEL_BUTTONS:ButtonVo[]=[new ButtonVo("Position",'light',15),new ButtonVo("Style","light",16)];

    static BGIMG_PANEL_BUTTONS:ButtonVo[]=[new ButtonVo("Content",'light',0),new ButtonVo("BG Position",'light',8)];

    static POPUP_PANEL_BUTTONS:ButtonVo[]=[new ButtonVo("Content","light",0),new ButtonVo("Position","light",1),new ButtonVo("Flip",'light',3),new ButtonVo("Pop UP","light",14)];

    static LIB_PANEL_BUTTONS:ButtonVo[]=[new ButtonVo("Suggested",'success',0),new ButtonVo("All",'primary',1)];
    static COPY_BUTTONS:ButtonVo[]=[new ButtonVo("Duplicate",'success',12,"sm"),new ButtonVo("Delete",'danger',13,"sm")];

    static PAGE_ADD_MENU:ButtonVo[]=[new ButtonVo("Edit Covers","info",3),new ButtonVo("Add A Page","primary",1),new ButtonVo("Add A Popup","warning",4),new ButtonVo("Preview","success",2),new ButtonVo("SAVE","primary",5)]
    static POPUP_ADD_MENU:ButtonVo[]=[new ButtonVo("Add A Popup","primary",1),new ButtonVo("Preview","success",2)];

    static OLD_NEW_MENU:GenVo[]=[new GenVo("Load A Book","fas fa-upload",0,"Load","primary",true),new GenVo("Make A New Book","fas fa-book",1,"Make","success",true),new GenVo("From Template","fas fa-books",2,"Make","warning",true)];
    
    static STEP_BUTTONS:ButtonVo[]=[new ButtonVo("Duplicate Frame","success",0,"sm"),new ButtonVo("Insert Blank Frame","warning",2,"sm"),new ButtonVo("Delete Frame","danger",1,"sm")]
    
    static SAVE_LOAD_PREVIEW:ButtonVo[]=[new ButtonVo("SAVE","primary",0,"sm"),new ButtonVo("Load","warning",1,"sm"),new ButtonVo("Preview","success",2,"sm")];
    static DONE_CANCEL:ButtonVo[]=[new ButtonVo("Cancel","danger",0),new ButtonVo("Done","success",1)];

    static IN_OUT_BUTTONS:ButtonVo[]=[new ButtonVo("IN","primary",0,"sm"),new ButtonVo("OUT","warning",1,"sm")];
    static ANIMATION_FILTERS:ButtonVo[]=[new ButtonVo("All","dark",0,"sm"),new ButtonVo("Fade","light",1,"sm"),new ButtonVo("Movement","light",2,"sm"),new ButtonVo("Size","light",3,"sm")]
    static FRAME_BUTTONS:ButtonVo[]=[new ButtonVo("Audio","light",0,"sm"),new ButtonVo("BG Audio","light",1,"sm")];

    static STEP_BUTTONS2:ButtonIconVo[]=[new ButtonIconVo("Copy Frame","far fa-copy","success",0,"sm"),new ButtonIconVo("Blank Frame","far fa-file","warning",1,"sm"),new ButtonIconVo("Template","fas fa-file-exclamation","info",4,"sm"),new ButtonIconVo("Delete Frame","far fa-trash","danger",2,"sm"),]
    static STEP_MOVE_BUTTONS:ButtonIconVo[]=[new ButtonIconVo("Move First","far fa-arrow-alt-from-right","light",0,"sm"),new ButtonIconVo("Move Left","far fa-arrow-alt-left","light",1,"sm"),new ButtonIconVo("Move Right","far fa-arrow-alt-right","light",2,"sm"),new ButtonIconVo("Move Last","far fa-arrow-alt-from-left","light",3,"sm")];

    static FILE_SORT_BUTTONS:ButtonVo[]=[new ButtonVo("LTI","light",2,"sm"),new ButtonVo("Name","light",0,"sm"),new ButtonVo("Modified","light",1,"sm")]
}   