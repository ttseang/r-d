import { AnimationVo } from "./dataObjs/AnimationVo";
import { FontVo } from "./dataObjs/FontVo";
import { StyleVo } from "./dataObjs/StyleVo";
import { TextStyleVo } from "./dataObjs/TextStyleVo";
import { GridVo } from "./GridVo";


export class StyleUtil
{
    public styles:StyleVo[]=[];
    public orientations:GridVo[]=[];
    public aligns:GridVo[]=[];
    public textStyles:TextStyleVo[]=[];

    public fonts:FontVo[]=[];

    public animations:AnimationVo[]=[];

    constructor()
    {
        //TO DO move to JSON        
        this.styles.push(new StyleVo(1,"top left",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"1.jpg",0,0,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(2,"top center",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"2.jpg",50,0,0,StyleVo.ADJUST_HALF,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(3,"top right",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"3.jpg",100,0,0,StyleVo.ADJUST_FULL,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(4,"center left",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"4.jpg",0,50,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_HALF));
        this.styles.push(new StyleVo(5,"center",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,["transformCenter"],"5.jpg",50,50,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(6,"center right",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"6.jpg",100,50,0,StyleVo.ADJUST_FULL,StyleVo.ADJUST_HALF));
        this.styles.push(new StyleVo(7,"bottom left",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"7.jpg",0,100,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_FULL));
        this.styles.push(new StyleVo(8,"bottom center",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"8.jpg",50,100,0,StyleVo.ADJUST_HALF,StyleVo.ADJUST_FULL));
        this.styles.push(new StyleVo(9,"bottom right",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"9.jpg",100,100,0,StyleVo.ADJUST_FULL,StyleVo.ADJUST_FULL));


        this.styles.push(new StyleVo(10,"size 1",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size1.jpg",0,0,10));
        this.styles.push(new StyleVo(11,"size 2",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size2.jpg",0,0,20));
        this.styles.push(new StyleVo(12,"size 3",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size3.jpg",0,0,30));
        this.styles.push(new StyleVo(13,"size 4",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size4.jpg",0,0,40));
        this.styles.push(new StyleVo(14,"size 5",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size5.jpg",0,0,50));
        this.styles.push(new StyleVo(15,"size 6",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size6.jpg",0,0,60));
        this.styles.push(new StyleVo(16,"size 7",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size7.jpg",0,0,70));
        this.styles.push(new StyleVo(17,"size 8",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size8.jpg",0,0,80));
        this.styles.push(new StyleVo(18,"size 9",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size9.jpg",0,0,90));



        this.orientations.push(new GridVo(0,"top left","orientations/00.png",4));
        this.orientations.push(new GridVo(1,"top center","orientations/05.png",4));
        this.orientations.push(new GridVo(2,"top right","orientations/01.png",4));
        this.orientations.push(new GridVo(3,"center left","orientations/50.png",4));
        this.orientations.push(new GridVo(4,"center","orientations/55.png",4));
        this.orientations.push(new GridVo(5,"center right","orientations/51.png",4));
        this.orientations.push(new GridVo(6,"bottom left","orientations/10.png",4));
        this.orientations.push(new GridVo(7,"bottom center","orientations/15.png",4));
        this.orientations.push(new GridVo(8,"bottom right","orientations/11.png",4));
        //
        //
        //
        this.aligns.push(new GridVo(0,"top left","aligns/0.jpg",3));
        this.aligns.push(new GridVo(1,"top center","aligns/1.jpg",3));
        this.aligns.push(new GridVo(2,"top right","aligns/2.jpg",3));
        this.aligns.push(new GridVo(3,"center left","aligns/3.jpg",3));
        this.aligns.push(new GridVo(4,"center","aligns/4.jpg",3));
        this.aligns.push(new GridVo(5,"center right","aligns/5.jpg",3));
        this.aligns.push(new GridVo(6,"bottom left","aligns/6.jpg",3));
        this.aligns.push(new GridVo(7,"bottom center","aligns/7.jpg",3));
        this.aligns.push(new GridVo(8,"bottom right","aligns/8.jpg",3));
        //
        //
        //
        this.fonts.push(new FontVo("Arial","Arial"));
        this.fonts.push(new FontVo("Times New Roman","Times New Roman"));
        this.fonts.push(new FontVo("Cursive","Cursive"));

       /*  this.animations.push(new AnimationVo(18,"Slow Fade In",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_FADE,AnimationVo.DIR_IN,["fadeIn"]));
        this.animations.push(new AnimationVo(19,"Slow Fade Out",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_FADE,AnimationVo.DIR_OUT,["fadeOut"]));


        this.animations.push(new AnimationVo(0,"Fade In",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_FADE,AnimationVo.DIR_IN,["fadeIn"]));
        this.animations.push(new AnimationVo(1,"Fade Out",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_FADE,AnimationVo.DIR_OUT,["fadeOut"]));

        this.animations.push(new AnimationVo(2,"Slide In 1",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide1"]));
        this.animations.push(new AnimationVo(3,"Slide In 2",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide2"]));
        this.animations.push(new AnimationVo(4,"Slide In 3",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide3"]));
        this.animations.push(new AnimationVo(5,"Slide In 4",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide4"]));
        this.animations.push(new AnimationVo(6,"Slide In 5",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide5"]));
        this.animations.push(new AnimationVo(7,"Slide In 6",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide6"]));
        this.animations.push(new AnimationVo(8,"Slide In 7",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide7"]));
        this.animations.push(new AnimationVo(9,"Slide In 8",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_IN,["slide8"]));

        this.animations.push(new AnimationVo(10,"Slide Out 1",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut1"]));
        this.animations.push(new AnimationVo(11,"Slide Out 2",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut2"]));
        this.animations.push(new AnimationVo(12,"Slide Out 3",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut3"]));
        this.animations.push(new AnimationVo(13,"Slide Out 4",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut4"]));
        this.animations.push(new AnimationVo(14,"Slide Out 5",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut5"]));
        this.animations.push(new AnimationVo(15,"Slide Out 6",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut6"]));
        this.animations.push(new AnimationVo(16,"Slide Out 7",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut7"]));
        this.animations.push(new AnimationVo(17,"Slide Out 8",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_MOVE,AnimationVo.DIR_OUT,["slideOut8"]));
        

        this.animations.push(new AnimationVo(20,"Grow",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_SIZE,AnimationVo.DIR_BOTH,["grow"]));
        this.animations.push(new AnimationVo(21,"Srhink",AnimationVo.TYPE_ALL,AnimationVo.SUB_TYPE_SIZE,AnimationVo.DIR_BOTH,["shrink"]));
        */

    }

    public filterStyles(type:string,subtype:string)
    {
        let fstyles:StyleVo[]=[];
        for (let i:number=0;i<this.styles.length;i++)
        {
            if (this.styles[i].type===type && this.styles[i].subType===subtype)
            {
                fstyles.push(this.styles[i]);
            }            
        }
        return fstyles;
    }
}