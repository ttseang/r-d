import { AnimationVo } from "./dataObjs/AnimationVo";
import { ElementVo } from "./dataObjs/ElementVo";
import { PresentationVo } from "./dataObjs/exports/PresentationVo";
import { StepSaveVo } from "./dataObjs/exports/saver/StepSaveVo";
import { ZoomSaveVo } from "./dataObjs/exports/saver/ZoomSaveVo";
import { StepExportVo } from "./dataObjs/exports/StepExportVo";
import { FileVo } from "./dataObjs/FileVo";
import { FormElementVo } from "./dataObjs/FormElementVo";
import { GenVo } from "./dataObjs/GenVo";
import { HistoryVo } from "./dataObjs/HistoryVo";
import { LibItemVo } from "./dataObjs/LibItemVo";
import { PopUpVo } from "./dataObjs/PopUpVo";
import { StepVo } from "./dataObjs/StepVo";
import { StyleVo } from "./dataObjs/StyleVo";
import { TemplateVo } from "./dataObjs/TemplateVo";
import { TextStyleVo } from "./dataObjs/TextStyleVo";
import { ZoomVo } from "./dataObjs/ZoomVo";
import { LibUtil } from "./libUtil";
import { StyleUtil } from "./styleUtil";

export class MainStorage {
  private static instance: MainStorage | null = null;


  public styleMap: Map<number, TextStyleVo> = new Map<number, TextStyleVo>();
  public pageTemplates: TemplateVo[] = [];

  public steps: StepVo[] = [];
  private _currentStep: number = 0;

  public framePanelIndex: number = 0;

  public selectedStep: StepVo;

  public selectedElement: ElementVo | null = null;
  public pageW: number = 640;
  public pageH: number = 480;

  public LtiFileName:string="";
  public fileName:string="";
  public files:FileVo[]=[];
  
  public eidIndex: number = 0;
  private _eid: string = "instance0";

  public libUtil: LibUtil;
  public library: LibItemVo[] = [];
  public suggested: string = "";
  public zoomLocked: boolean = false;

  public zoomVo: ZoomVo = new ZoomVo(20, 20, 80, 80);

  public linkedCount: number = 0;

  public popups: PopUpVo[] = [];
  public styles: StyleVo[] = [];
  public styleUtil: StyleUtil;

  public editText: string = "";
  public tempID: number = 0;

  public menu: GenVo[] = [];
  public formData: FormElementVo[] = [];

  public exportCode: string = "";
  public saveCode: string = "";

  private historyIndex: number = 0;
  private history: HistoryVo[] = [];

  public keyLock: boolean = false;

  public copyElement: ElementVo | null = null;

  public backgroundAudioSwitch: boolean = false;
  public appType:string="IC";

  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor() {
    //media items
    this.libUtil = new LibUtil();
    this.library = this.libUtil.library;

    this.styleUtil = new StyleUtil();

    this.styles = this.styleUtil.styles;

    this.steps.push(new StepVo(0, []));
    this.selectedStep = this.steps[0];

    (window as any).ms = this;
  }
  public get eid(): string {
    this._eid = "instance" + this.eidIndex.toString();
    return this._eid;
  }
  public get currentStep(): number {
    return this._currentStep;
  }
  public set currentStep(value: number) {
    this._currentStep = value;
    this.selectedStep = this.steps[this._currentStep];
  }
  public addHistory() {
    if (this.historyIndex !== this.history.length - 1) {
      this.history = this.history.slice(0, this.historyIndex);
    }
    this.history.push(new HistoryVo(this.currentStep, this.steps));
    this.historyIndex = this.history.length - 1;
  }
  public goBack() {
    if (this.historyIndex > 0) {
      this.historyIndex--;
      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }
  public goForward() {
    if (this.historyIndex < this.history.length - 1) {
      this.historyIndex++;

      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }

  getPageH() {
    return this.pageH;
  }
  getPageW() {
    return this.pageW;
  }
  public static getInstance(): MainStorage {
    if (this.instance === null) {
      this.instance = new MainStorage();
    }
    return this.instance;
  }
  getStepIndex(id: number) {
    for (let i: number = 0; i < this.steps.length; i++) {
      let step: StepVo = this.steps[i];
      if (step.id === id) {
        return i;
      }
    }
    return -1;
  }
  getAudioPath(audio: string) {
    return "https://ttv5.s3.amazonaws.com/william/audio/"+audio;
  }
  getImagePath(image: string, path: number = 0) {

    if (path === 1) {
     // return "./images/appImages/" + image;
      return "https://ttv5.s3.amazonaws.com/william/images/appImages/"+image;
    }
    return "https://ttv5.s3.amazonaws.com/william/images/bookimages/"+image;
   // return "./images/bookimages/" + image;
  }
  getPopUpByID(id: number) {
    for (let i: number = 0; i < this.popups.length; i++) {
      if (this.popups[i].id === id) {
        return this.popups[i];
      }
    }
    return this.popups[0];
  }

  getAnimations(type: string, subType: string, inOut: string) {
    let filteredAnimation: AnimationVo[] = [];
    for (let i: number = 0; i < this.styleUtil.animations.length; i++) {
      let animationVo: AnimationVo = this.styleUtil.animations[i];
      if (animationVo.type === AnimationVo.TYPE_ALL || animationVo.type === type) {
        if (animationVo.subType === subType) {
          if (animationVo.inOut === AnimationVo.DIR_BOTH || animationVo.inOut === inOut) {
            filteredAnimation.push(animationVo);
          }
        }
      }
    }
    return filteredAnimation;
  }

  getExport() {
    let allSteps: StepExportVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }

    let presentationVo: PresentationVo = new PresentationVo(allSteps, this.pageW, this.pageH);

    console.log(presentationVo);
    this.exportCode = JSON.stringify(presentationVo);
    return this.exportCode;
  }
  getExportJson()
  {
    let allSteps: StepExportVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }

    let presentationVo: PresentationVo = new PresentationVo(allSteps, this.pageW, this.pageH);

    return presentationVo;
  }
  saveZoom() {
    let allSteps: StepSaveVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }
    let zoomSave: ZoomSaveVo = new ZoomSaveVo(allSteps, this.pageW, this.pageH);
    this.saveCode = JSON.stringify(zoomSave);
    return this.saveCode;
  }
  saveZoom2() {
    let allSteps: StepSaveVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }
    let zoomSave: ZoomSaveVo = new ZoomSaveVo(allSteps, this.pageW, this.pageH);
    return zoomSave;
  }
  
  loadZoom(data: any) {

    console.log(data);
    this.steps = [];

    this.pageW = parseInt(data.pageW);
    this.pageH = parseInt(data.pageH);

    let steps: any[] = data.steps;

    for (let i: number = 0; i < steps.length; i++) {
      let stepVo: StepVo = new StepVo();
      stepVo.fromObj(steps[i]);
      this.steps.push(stepVo);
    }

    this.selectedStep = this.steps[0];
  }
}
export default MainStorage;