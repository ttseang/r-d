import { AnimationMapLoader } from "./AnimationMapLoader";
import ApiConnect from "./ApiConnect";
import { FileVo } from "./dataObjs/FileVo";
import { LibLoader } from "./LibLoader";
import MainStorage from "./MainStorage";
import { TemplateMapLoader } from "./TemplateMapLoader";
import { TextStyleMapLoader } from "./TextStyleMapLoader";

export class StartData {
    private callback: Function;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(callback: Function) {
        this.callback = callback;
    }
    start() {
       let libLoader:LibLoader=new LibLoader(this.getAnims.bind(this));
       libLoader.load("./jsonFiles/lib.json");
    }
    private getAnims()
    {
        let animationMapLoader: AnimationMapLoader = new AnimationMapLoader(this.gotMap.bind(this));
        animationMapLoader.load("./jsonFiles/animationMap.json");
    }
    private gotMap() {
        let textStyleMapLoader: TextStyleMapLoader = new TextStyleMapLoader(this.gotStyles.bind(this));
        textStyleMapLoader.load("./jsonFiles/textStyleMap.json");
    }
    private gotStyles() {
       let templateMapLoader:TemplateMapLoader=new TemplateMapLoader(this.gotTemplates.bind(this));
       templateMapLoader.load("./jsonFiles/templateMap.json");
    }
    private gotTemplates()
    {
        this.getFileList();
    }
    private getFileList()
    {
        let apiConnect:ApiConnect=new ApiConnect();
        apiConnect.getFileList(this.gotFileList.bind(this),this.ms.appType);
    }
    private gotFileList(data:any[])
    {       
        let files:FileVo[]=[];
        let filter="IC";

        for (let i:number=0;i<data.length;i++)
        {
          //  console.log(data[i]);
            let d:any=data[i];
            let t:number=Date.parse(data[i].lastmod);
           // console.log(t);
            
            /* let ltiArray:string=d.lti.split(".");
            let mod:string=ltiArray[2];
            if (mod.includes(filter))
            { */
                let fileVo:FileVo=new FileVo(d.name,d.lti,t,d.lastmod);
                files.push(fileVo);
          //  }          
        }
        this.ms.files=files;
        this.callback();
    }
}