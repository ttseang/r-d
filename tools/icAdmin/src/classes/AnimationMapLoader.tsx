import { AnimationVo } from "./dataObjs/AnimationVo";
import MainStorage from "./MainStorage";

export class AnimationMapLoader
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    load(path:string)
    {
       // console.log("load map");
        fetch(path)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any)
    {
       // console.log(data);
        let animations:any[]=data.animations;
        let dirArray:string[]=[AnimationVo.DIR_IN,AnimationVo.DIR_OUT,AnimationVo.DIR_BOTH];

        for (let i:number=0;i<animations.length;i++)
        {
            let anim:any=animations[i];
          //  console.log(anim);
            
            let dir:string=dirArray[anim.dirIn];

             let animationVo:AnimationVo=new AnimationVo(parseInt(anim.id),anim.name,anim.elementType,anim.animationType,dir,anim.initClass,anim.animClass);
          //  console.log(animationVo);
            this.ms.styleUtil.animations.push(animationVo);
           // this.ms.animationMap.set(animationVo.id,animationVo); 
        }
        this.callback();
    }
}