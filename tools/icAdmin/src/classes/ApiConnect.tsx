//import { MainStorage } from "./MainStorage";

export class ApiConnect {
    private authKey: string = "DQho4TTbytnJc31puZNDxfhY";

    public save(data: any, callback: Function) {

        let url: string = "https://tthq.me/api/dv/endpointX";
        fetch(url, {
            method: "post",
            body: data,
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
    public getFileList(callback: Function,type:string) {

        let url: string = "https://tthq.me/api/dv/listX?type="+type
        fetch(url, {
            method: "post",
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
    public getFileContent(lti:string,callback:Function)
    {
        let url: string = "https://tthq.me/api/dv/dataX/"+lti;
        fetch(url, {
            method: "post",
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
    public deleteFile(lti:string,callback:Function)
    {
        let url: string = "https://tthq.me/api/dv/deleteX/"+lti;
        fetch(url, {
            method: "post",
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
}
export default ApiConnect;