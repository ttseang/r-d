import { ElementVo } from "./dataObjs/ElementVo";
import { PosVo } from "./dataObjs/PosVo";

export class AlignUtil {
    private pos: PosVo[] = [];
    // private orIndex:number=[0,]
    constructor() {
        this.pos = [new PosVo(0, 0), new PosVo(50, 0), new PosVo(100, 0), new PosVo(0, 50), new PosVo(50, 50), new PosVo(100, 50), new PosVo(0, 100), new PosVo(50, 100), new PosVo(100, 100)];

    }
    alignElement(alignIndex: number, elementVo: ElementVo) {
        let pos: PosVo = this.pos[alignIndex];


        elementVo.extras.orientation = alignIndex;

        console.log(pos.x, pos.y);
        elementVo.x = pos.x;
        elementVo.y = pos.y;
    }
}