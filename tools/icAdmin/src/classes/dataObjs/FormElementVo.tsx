export class FormElementVo
{
    static TYPE_STRING:number=0;
    static TYPE_NUMBER:number=1;
    static TYPE_BLOB:number=2;
    
    public label:string;
    public type:number;

    constructor(label:string,type:number)
    {
        this.label=label;
        this.type=type;
    }
}