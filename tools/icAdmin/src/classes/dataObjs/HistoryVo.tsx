import { StepVo } from "./StepVo";

export class HistoryVo
{
    public currentStep:number;
    public steps:StepVo[]=[];

    constructor(currentStep:number,steps:StepVo[])
    {
        this.currentStep=currentStep;
       // this.steps=steps;

       for (let i:number=0;i<steps.length;i++)
       {
           this.steps.push(steps[i].clone2());
       }

    }
}