export class TemplateVo
{
    public id:number;
    public name:string;
    public file:string;

    constructor(id:number,name:string,file:string)
    {
        this.id=id;
        this.name=name;
        this.file=file;
    }
}