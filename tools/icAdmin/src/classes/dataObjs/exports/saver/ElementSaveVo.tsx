import { ExtrasVo } from "../../ExtrasVo";

export class ElementSaveVo
{
    public type:string;
    public subType:string;
    public classes:string[];
    public content:string[];
    public x:number;
    public y:number;
    public w:number;
    public h:number;
    public eid:string;
    public extras:ExtrasVo;

    constructor(type:string,eid:string,subType:string,classes:string[],content:string[],x:number,y:number,w:number,h:number,extras:ExtrasVo)
    {
        this.type=type;
        this.eid=eid;
        this.subType=subType;
        this.classes=classes;
        this.content=content;
        this.x=x;
        this.y=y;
        this.w=w;
        this.h=h;
        this.extras=extras;
    }
}