
import { ElementSaveVo } from "./ElementSaveVo";

export class StepSaveVo
{
    public id:number;
    public elements:ElementSaveVo[];


    constructor(id:number,elements:ElementSaveVo[])
    {
        this.id=id;
        this.elements=elements;
    }
}