
import { ElementExportVo } from "./ElementExportVo";

export class StepExportVo
{
    public id:number;
    public elements:ElementExportVo[];
    public audio:string;
    public bgAudio:string;

    constructor(id:number,elements:ElementExportVo[],audio:string,bgAudio:string)
    {
        this.id=id;
        this.elements=elements;
        this.audio=audio;
        this.bgAudio=bgAudio;
    }
}