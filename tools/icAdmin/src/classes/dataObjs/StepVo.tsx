import MainStorage from "../MainStorage";
import { ElementVo } from "./ElementVo";
import { StepSaveVo } from "./exports/saver/StepSaveVo";
import { StepExportVo } from "./exports/StepExportVo";
import { ZoomVo } from "./ZoomVo";

export class StepVo
{
    public static ElementID:number=0;
    public static StepCount:number=0;

    public id:number;
    public elements:ElementVo[];
    public label:string="";
    public zoom:ZoomVo=new ZoomVo(0,0,100,100);
    public audio:string="";
    public backgroundAudio:string="";
    
    public audioName:string="no audio set";
    public backgroundAudioName:string="no audio set";

    constructor(id:number=-1,elements:ElementVo[]=[])
    {        
        if (id===-1)
        {
            StepVo.StepCount++;
            id=StepVo.StepCount;
        }
        this.id=id;
        this.elements=elements;
        this.label=id.toString();
    }
    public makeID()
    {
        StepVo.StepCount++;
        this.id=StepVo.StepCount;
    }
    public fromObj(obj:any)
    {
        console.log("OBJ");
        console.log(obj);

        let id:number=parseInt(obj.id);
        this.id=id;

        if (obj.audio)
        {
            this.audio=obj.audio;
            this.audioName=this.audio;
        }
        if (obj.bgAudio)
        {
            this.backgroundAudio=obj.bgAudio;
            this.backgroundAudioName=this.backgroundAudio;
        }

        if (id===-1)
        {
            StepVo.StepCount++;
            this.id=StepVo.StepCount;
        }


        let elements:any[]=obj.elements;
        for (let i:number=0;i<elements.length;i++)
        {
            let elementVo:ElementVo=new ElementVo();
            elementVo.fromObj(elements[i]);

            StepVo.ElementID++;
            elementVo.id=StepVo.ElementID;
            elementVo.eid="element+"+elementVo.id.toString();
            
            this.elements.push(elementVo);
        }
    }
    
    public addElement(elementVo:ElementVo)
    {
        StepVo.ElementID++;
        
        elementVo.id=StepVo.ElementID;
        
        //console.log("ID="+elementVo.id);
        
        this.elements.push(elementVo);
    }
    public deleteElement(elementVo:ElementVo)
    {
        let index:number=-1;
        let elements2:ElementVo[]=this.elements.slice();

        for (let i:number=0;i<elements2.length;i++)
        {
            if (elements2[i].id===elementVo.id)
            {
                index=i;
            }
        }
        if (index!==-1)
        {
            elements2.splice(index,1);
        }
        return elements2;
    }
    public toExport()
    {
        let stepExportVo:StepExportVo=new StepExportVo(this.id,[],this.audio,this.backgroundAudio);
        for (let i:number=0;i<this.elements.length;i++)
        {
            stepExportVo.elements.push(this.elements[i].getExport());
        }
        return stepExportVo;
    }
    public toSave()
    {
        let stepSaveVo:StepSaveVo=new StepSaveVo(this.id,[]);
        for (let i:number=0;i<this.elements.length;i++)
        {
            stepSaveVo.elements.push(this.elements[i].getSave())
        }
    }
    getFirstImage()
    {
        let ms:MainStorage=MainStorage.getInstance();

        for (let i:number=0;i<this.elements.length;i++)
        {
            let element:ElementVo=this.elements[i];
            if (element.type===ElementVo.TYPE_IMAGE)
            {
                return ms.getImagePath(element.content[0]);
            }
        }
        return "./images/appImages/noImage.jpg";
    }
    public clone()
    {
        
        let stepVo:StepVo=new StepVo(-1,[]);

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepVo.elements.push(this.elements[i].clone(1));
        }
        return stepVo;
    }
    public clone2()
    {
        let stepVo:StepVo=new StepVo(this.id,[]);

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepVo.elements.push(this.elements[i].clone2());
        }
        return stepVo;   
    }
}