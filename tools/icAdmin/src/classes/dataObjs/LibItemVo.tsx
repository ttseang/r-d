export class LibItemVo
{
    public title:string;
    public image:string;
    public tags:string[];

    constructor(title:string,image:string,tags:string[]=[])
    {
        this.title=title;
        this.image=image;
        this.tags=tags;
    }
}