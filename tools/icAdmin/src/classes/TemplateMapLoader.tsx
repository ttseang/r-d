import { TemplateVo } from "./dataObjs/TemplateVo";
import MainStorage from "./MainStorage";

export class TemplateMapLoader
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(callback:Function)
    {
        this.callback=callback;
    }

    public load(path:string)
    {
        fetch(path)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any)
    {
        let templates:any[]=data.templates;
        for (let i:number=0;i<templates.length;i++)
        {
            let template:any=templates[i];
            let templateVo:TemplateVo=new TemplateVo(parseInt(template.id),template.name,template.file);
            this.ms.pageTemplates.push(templateVo);
        }
        this.callback();
    }
}