import React, { Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { ButtonIconVo } from '../classes/dataObjs/ButtonIconVo';
interface MyProps { buttonData:ButtonIconVo, callback: Function }
interface MyState { }
class AddBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state={};
    }
    render() {
        return (<div><Card onClick={()=>{this.props.callback()}}><Card.Body className='tac'>
            <Row><Col><i className={this.props.buttonData.icon+" addIcon"}></i></Col></Row>
            <Row><Col>{this.props.buttonData.text}</Col></Row>           
        </Card.Body></Card></div>)
    }
}
export default AddBox;