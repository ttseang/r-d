import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { ArrayUtil } from '../classes/ArrayUtil';
import { ButtonConstants } from '../classes/constants/ButtonConstants';
import { StepVo } from '../classes/dataObjs/StepVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import ButtonIconLine from '../classes/ui/ButtonIconLine';
import StepBox from './StepBox';
interface MyProps { selectCallback: Function }
interface MyState { steps: StepVo[]}
class StepPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private cMounted:boolean=false;

    constructor(props: MyProps) {
        super(props);
        this.state = { steps: this.ms.steps };
        //
        //
        //
        this.mc.duplicateFrame = this.cloneFrame.bind(this);
        this.mc.addBlankFrame = this.addBlankFrame.bind(this);
        this.mc.delFrame = this.delFrame.bind(this);
        this.mc.stepsUpdated=this.stepsUpdated.bind(this);
    }
    componentDidMount()
    {
        this.cMounted=true;
    }
    componentWillUnmount()
    {
        this.cMounted=false;
    }
    getSteps() {
        let stepCards: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.steps.length; i++) {
            let key: string = "stepBox" + i.toString();

            stepCards.push(<StepBox key={key} stepVo={this.state.steps[i]} selectCallback={this.selectStep.bind(this)}></StepBox>)
        }
        return stepCards;
    }
    selectStep(stepVo: StepVo) {
        let id: number = stepVo.id;
        let index: number = this.ms.getStepIndex(id);
        this.ms.currentStep = index;
        this.props.selectCallback();
    }
    private cloneFrame() {
        let spIndex: number = this.ms.getStepIndex(this.ms.selectedStep.id);

        let cloneStep: StepVo = this.ms.selectedStep.clone();
        console.log(cloneStep);

        let steps: StepVo[] = this.ms.steps.slice();
        steps.splice(spIndex + 1, 0, cloneStep);

        this.ms.steps = steps;
        this.setState({ steps: this.ms.steps });
        this.selectStep(cloneStep);

       
    }
    delFrame() {

        let spIndex2: number = this.ms.getStepIndex(this.ms.selectedStep.id);
        console.log(spIndex2);

        let steps2: StepVo[] = this.ms.steps.slice();
        steps2.splice(spIndex2, 1);


        this.ms.steps = steps2;

        if (steps2.length > 0) {
            let nstep: number = spIndex2 - 1;
            if (nstep < 0) {
                nstep = 0;
            }

            this.selectStep(this.ms.steps[nstep]);

            this.setState({ steps: this.ms.steps });
        }
        else {
            this.addBlankFrame();
        }

    }
    stepsUpdated()
    {
        this.setState({steps:this.ms.steps});
    }
    addBlankFrame() {
        let nstep: StepVo = new StepVo();
        console.log(nstep);
        if (this.ms.steps.length === 0) {
            this.ms.steps.push(nstep);
            this.setState({ steps: this.ms.steps });
            this.selectStep(this.ms.steps[0]);
            return;
        }

        let spIndex: number = this.ms.getStepIndex(this.ms.selectedStep.id);

        let steps: StepVo[] = this.ms.steps.slice();
        steps.splice(spIndex + 1, 0, nstep);

        this.ms.steps = steps;

        this.setState({ steps: this.ms.steps });
        this.selectStep(this.ms.steps[spIndex + 1]);
    }
    doStepAction(action: number) {
        console.log("action=" + action);
        switch (action) {
            case 0:
                // console.log(this.ms.selectedStep);
                this.cloneFrame();
                this.ms.addHistory();
                break;

            case 2:
                this.delFrame();
                this.ms.addHistory();
                break;

            case 1:
                this.addBlankFrame();
                this.ms.addHistory();
                break;

            case 4:
                this.mc.changeScreen(8);
                break;
        }
    }
    moveStep(index:number,action:number)
    {
        console.log(action);
        let spIndex: number = this.ms.getStepIndex(this.ms.selectedStep.id);

        let steps: StepVo[] = this.ms.steps.slice();
        
        switch(action)
        {
            case 0:
                steps=ArrayUtil.moveItem(steps,spIndex,0);
            break;
            case 1:
                if (spIndex>0)
                {
                steps=ArrayUtil.moveItem(steps,spIndex,spIndex-1);
                }
               
            break;

            case 2:

                steps=ArrayUtil.moveItem(steps,spIndex,spIndex+1);
        

            break;

            case 3:

                steps=ArrayUtil.moveItem(steps,spIndex,steps.length-1);
                break;
                
        }

        this.setState({ steps:steps });
        this.ms.steps=steps;
        spIndex = this.ms.getStepIndex(this.ms.selectedStep.id);
        this.selectStep(this.ms.steps[spIndex]);

    }
    render() {
        return (<div>
            <Card><Card.Body>
                <div id="stepButtons">
                    <ButtonIconLine buttonArray={ButtonConstants.STEP_BUTTONS2} actionCallback={this.doStepAction.bind(this)} useVert={false} useText={false}></ButtonIconLine>
                    </div>
                <div id="stepScroll">
                    <div id="steps">
                        {this.getSteps()}
                    </div>
                </div>
                <div id="stepMoveButtons">
                <ButtonIconLine buttonArray={ButtonConstants.STEP_MOVE_BUTTONS} actionCallback={this.moveStep.bind(this)} useVert={false} useText={false}></ButtonIconLine>
                </div>
                </Card.Body></Card>
        </div>)
    }
}
export default StepPanel;