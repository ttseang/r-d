import { Component } from 'react';
import { Navbar, NavDropdown } from 'react-bootstrap';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
interface MyProps { callback: Function }
interface MyState {}
class TopBar extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    
    render() {

        return (<div>

            <Navbar>
                <Navbar.Brand>Presentation Admin</Navbar.Brand>
                <NavDropdown id='mainNav' title='File'>
                    <NavDropdown.Item onClick={() => { this.props.callback(0) }}>Open</NavDropdown.Item>
                    <NavDropdown.Item onClick={() => { this.props.callback(1) }}>Save</NavDropdown.Item>
                    <NavDropdown.Item onClick={() => { this.props.callback(3) }}>Export</NavDropdown.Item>
                    <NavDropdown.Item onClick={() => { this.props.callback(2) }}>Preview</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown id="editNav" title="Edit">
                <NavDropdown.Item onClick={() => { this.props.callback(20) }}>Undo</NavDropdown.Item>
                <NavDropdown.Item onClick={() => { this.props.callback(21) }}>Redo</NavDropdown.Item>
                <NavDropdown.Item onClick={() => { this.props.callback(22) }}>Copy Element</NavDropdown.Item>
                <NavDropdown.Item onClick={() => { this.props.callback(23) }}>Paste Element</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown id="frameNav" title="Frames">
                    <NavDropdown.Item onClick={() => { this.props.callback(10) }}>Duplicate Frame</NavDropdown.Item>
                    <NavDropdown.Item onClick={() => { this.props.callback(11) }}>Add Blank Frame</NavDropdown.Item>
                    <NavDropdown.Item onClick={() => { this.props.callback(12) }}>Delete Frame</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown id="frameNav" title="Add">
                    <NavDropdown.Item>Add Image</NavDropdown.Item>
                    <NavDropdown.Item>Add Text</NavDropdown.Item>
                    <NavDropdown.Item>Add Card</NavDropdown.Item>
                    <NavDropdown.Item>Add Line</NavDropdown.Item>
                </NavDropdown>
                
            </Navbar>
        </div>)
    }
}
export default TopBar;