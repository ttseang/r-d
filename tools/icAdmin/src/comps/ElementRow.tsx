import { Component } from 'react';
import { Col,Row, ListGroupItem, Button } from 'react-bootstrap';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import MainStorage from '../classes/MainStorage';
interface MyProps {selected:boolean,callback:Function,toggleVis:Function,toggleLock:Function, index: number, elementVo: ElementVo }
interface MyState {selected:boolean,elementVo:ElementVo}
class ElementRow extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {selected:this.props.selected,elementVo:this.props.elementVo};
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({selected:this.props.selected,elementVo:this.props.elementVo});
        }
    }
    getImageName()
    {
       return this.props.elementVo.content[0].split("/")[0];
    }
    getContent() {
        //<Col sm={3}><Button size="sm" variant="dark" onClick={()=>{this.props.callback(this.props.index,this.props.elementVo)}}>Change</Button></Col>
        let selectClass:string=(this.state.selected===true)?"selectedItem":"";

        if (this.props.elementVo.type === ElementVo.TYPE_TEXT) {
            let text:string=this.props.elementVo.content[0].substring(0,10);
            if (this.props.elementVo.content.length>10)
            {
                text+="...";
            }
            return (<ListGroupItem onClick={()=>{this.props.callback(this.props.elementVo)}} className={selectClass}><Row><Col sm={4} className="tal fitText">{text}</Col><Col sm={3}>{this.getLock()}</Col><Col sm={3}>{this.getEye()}</Col></Row></ListGroupItem>);
        }
        if (this.props.elementVo.type === ElementVo.TYPE_IMAGE || this.props.elementVo.type===ElementVo.TYPE_BACK_IMAGE) {
            let image: string = this.ms.getImagePath(this.props.elementVo.content[0],0); 

            return (<ListGroupItem  onClick={()=>{this.props.callback(this.props.elementVo)}} className={selectClass}><Row><Col sm={4}><img src={image} alt='book' className='editThumb' /></Col><Col sm={3}>{this.getLock()}</Col><Col sm={3}>{this.getEye()}</Col></Row></ListGroupItem>)
        }
        if (this.props.elementVo.type===ElementVo.TYPE_CARD)
        {
            return (<ListGroupItem  onClick={()=>{this.props.callback(this.props.elementVo)}} className={selectClass}><Row><Col sm={4}><i className="far fa-address-card"></i></Col><Col sm={3}>{this.getLock()}</Col><Col sm={3}>{this.getEye()}</Col></Row></ListGroupItem>)
       
        }
        if (this.props.elementVo.type===ElementVo.TYPE_LINE)
        {
            return (<ListGroupItem  onClick={()=>{this.props.callback(this.props.elementVo)}} className={selectClass}><Row><Col sm={4}><i className="fas fa-horizontal-rule"></i></Col><Col sm={3}>{this.getLock()}</Col><Col sm={3}>{this.getEye()}</Col></Row></ListGroupItem>)
        
        }
        return "unknown";
    }
    getLock()
    {
        if (this.state.elementVo.locked===false)
        {
            return (<Button variant='light' onClick={()=>{this.props.toggleLock(this.state.elementVo)}}><i className="fas fa-lock-open"></i></Button>)     
        }
        return (<Button variant='light' onClick={()=>{this.props.toggleLock(this.state.elementVo)}}><i className="fas fa-lock"></i></Button>) 
    }
    getEye()
    {
      //  return (<i className="fas fa-eye-slash"></i>);
        if (this.state.elementVo.visible===false)
        {
            return (<Button variant='light' onClick={()=>{this.props.toggleVis(this.state.elementVo)}}><i className="fas fa-eye-slash"></i></Button>)     
        }
        return (<Button variant='light' onClick={()=>{this.props.toggleVis(this.state.elementVo)}}><i className="fas fa-eye"></i></Button>)
    }
    getHeader() {
       /*  if (this.props.elementVo.type === ElementVo.TYPE_TEXT) {
            let index2: string = (this.props.index + 1).toString();
            return (index2 + ".");
        } */
        return (<span>Element #{this.props.index + 1}</span>)
    }
    render() {
        return (this.getContent())
    }
}
export default ElementRow;