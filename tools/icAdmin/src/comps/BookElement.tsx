import React, { Component } from 'react';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { TextStyleVo } from '../classes/dataObjs/TextStyleVo';
import MainStorage from '../classes/MainStorage';
interface MyProps { elementVo: ElementVo, callback: Function, onUp: Function, lineUp: Function, lineDown: Function, preview: boolean }
interface MyState { }
class BookElement extends Component<MyProps, MyState>
{
    public ref: React.RefObject<any>;
    private ms: MainStorage = MainStorage.getInstance();
    private origins: string[];

    constructor(props: MyProps) {
        super(props);
        this.state = {};
        this.ref = React.createRef();
        this.props.elementVo.el = this;

        this.origins = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
    }

    getStyle(elementVo: ElementVo) {
        let style: React.CSSProperties = {};

        style.left = elementVo.x.toString() + "%";
        style.top = elementVo.y.toString() + "%";

        style.width = elementVo.w.toString() + "%";

        if (elementVo.type === ElementVo.TYPE_BACK_IMAGE) {
            let image: string = "url(" + this.ms.getImagePath(this.props.elementVo.content[0]) + ")";
            style.backgroundImage = image;
            style.backgroundPositionX = elementVo.extras.backgroundPosX + "px";
            style.backgroundPositionY = elementVo.extras.backgroundPosY + "px";


        }
        if (elementVo.type === ElementVo.TYPE_TEXT && elementVo.textStyle === -1) {

            style.color = elementVo.extras.fontColor;
            style.fontFamily = elementVo.extras.fontName;
        }

        if (elementVo.type === ElementVo.TYPE_TEXT) {
            style.fontSize = elementVo.extras.fontSize;
        }

        if (elementVo.extras.alpha !== 100) {
            style.opacity = elementVo.extras.alpha.toString() + "%";
        }

        let transformString: string = "translate(" + this.origins[elementVo.extras.orientation] + ")";

        if (elementVo.extras.rotation > 0) {
            let angle: number = ((elementVo.extras.rotation) / 100) * 360;
            angle = Math.floor(angle);
            ////console.log(angle);
            transformString += " rotate(" + angle.toString() + "deg)";
        }
        if (elementVo.extras.skewY !== 0) {
            let skewAngleY: number = ((elementVo.extras.skewY) / 100) * 360;
            skewAngleY = Math.floor(skewAngleY);
            ////console.log(skewAngleY);
            transformString += " skewY(" + skewAngleY.toString() + "deg)";
        }
        if (elementVo.extras.skewX !== 0) {
            let skewAngleX: number = ((elementVo.extras.skewX) / 100) * 360;
            skewAngleX = Math.floor(skewAngleX);
            ////console.log(skewAngleX);
            transformString += " skewX(" + skewAngleX.toString() + "deg)";
        }
        if (elementVo.extras.flipV === true) {
            transformString += " scaleY(-1)";
        }
        if (elementVo.extras.flipH === true) {
            transformString += " scaleX(-1)";
        }

        style.transform = transformString;

        if (elementVo.extras.borderThick > 0) {
            style.border = "solid";
            style.borderWidth = elementVo.extras.borderThick;
            style.borderColor = elementVo.extras.borderColor;
        }

        return style;
    }

    getHtml() {
        let ukey: string = "element" + this.props.elementVo.id.toString();
        let elementVo: ElementVo = this.props.elementVo;
        let style: React.CSSProperties = this.getStyle(elementVo);

        let classArray: string[] = this.props.elementVo.classes.slice();

       // console.log("textStyle=" + elementVo.textStyle);

        if (elementVo.textStyle !== -1) {

            if (this.ms.styleMap.has(elementVo.textStyle)) {
                let textStyleVo: TextStyleVo | undefined = this.ms.styleMap.get(elementVo.textStyle);
                console.log(textStyleVo);

                if (textStyleVo) {
                    for (let i: number = 0; i < textStyleVo.classes.length; i++) {
                        classArray.push(textStyleVo.classes[i]);
                    }
                }
            }
        }
       // console.log(classArray);

        if (elementVo.visible === false) {
            classArray.push("hid");
        }
        if (elementVo.locked === true) {
            classArray.push("locked");
        }
        if (this.props.elementVo.type === ElementVo.TYPE_CARD) {

            classArray.push("infoCard");
            switch (this.props.elementVo.subType) {
                case ElementVo.SUB_TYPE_GREEN_CARD:
                    classArray.push("green");
                    break;

                case ElementVo.SUB_TYPE_WHITE_CARD:
                    classArray.push("white");
                    break;
            }

        }

        let classes: string = classArray.concat(['abPos']).join(" ");


        if (this.props.elementVo.type === ElementVo.TYPE_LINE) {
            let x1: number = (elementVo.x / 100) * this.ms.pageW;
            let y1: number = (elementVo.y / 100) * this.ms.pageH;
            let x2: number = (elementVo.w / 100) * this.ms.pageW;
            let y2: number = (elementVo.h / 100) * this.ms.pageH;

            let x3: number = x1 + ((x2 - x1) / 2);
            let y3: number = y1 + ((y2 - y1) / 2);


            if (elementVo !== this.ms.selectedElement || this.props.preview === true) {
                return (<svg className='noPoint' key={ukey} x={0} y={0} width={this.ms.pageW} height={this.ms.pageH} style={{ position: "relative" }}>

                    <line x1={x1} y1={y1} x2={x2} y2={y2} style={{ stroke: elementVo.extras.borderColor, strokeWidth: elementVo.extras.borderThick.toString() + "px" }} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }}></line>
                </svg>)
            }

            return (<svg key={ukey} x={0} y={0} width={this.ms.pageW} height={this.ms.pageH} style={{ position: "relative" }}>

                <line x1={x1} y1={y1} x2={x2} y2={y2} style={{ stroke: elementVo.extras.borderColor, strokeWidth: elementVo.extras.borderThick.toString() + "px" }}></line>
                <circle cx={x1} cy={y1} r={10} className="dragCircle" onPointerUp={() => { this.props.lineUp(this.props.elementVo, 0) }} onPointerDown={() => { this.props.lineDown(this.props.elementVo, 0) }}></circle>
                <circle cx={x2} cy={y2} r={10} className="dragCircle" onPointerUp={() => { this.props.lineUp(this.props.elementVo, 1) }} onPointerDown={() => { this.props.lineDown(this.props.elementVo, 1) }}></circle>
                <circle cx={x3} cy={y3} r={10} className="dragCircle" onPointerUp={() => { this.props.lineUp(this.props.elementVo, 2) }} onPointerDown={() => { this.props.lineDown(this.props.elementVo, 2) }}></circle>

            </svg>)
        }

        if (this.props.elementVo.type === ElementVo.TYPE_CARD) {
            switch (this.props.elementVo.subType) {
                case ElementVo.SUB_TYPE_GREEN_CARD:
                    return (<div className={classes} style={style} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} >
                        <div id="title">{this.props.elementVo.content[0]}</div>
                        <div id="content">{this.props.elementVo.content[1]}</div>
                    </div>)
                case ElementVo.SUB_TYPE_WHITE_CARD:
                    return (<div className={classes} style={style} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} >
                        <div id="content">{this.props.elementVo.content[0]}</div>
                    </div>)
            }
        }
        if (this.props.elementVo.type === ElementVo.TYPE_TEXT) {

            return (<article ref={this.ref} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} style={style}>{this.props.elementVo.content[0]}</article>);
        }
        return (<img key={ukey} id={ukey} ref={this.ref} className={classes} style={style} onPointerDown={() => { this.props.callback(this.props.elementVo) }} src={this.ms.getImagePath(this.props.elementVo.content[0])} alt="bookpart" />)
    }
    render() {
        return (this.getHtml())
    }
}
export default BookElement;