import React, { Component, CSSProperties } from 'react';
import Card from 'react-bootstrap/esm/Card';
import { StepVo } from '../classes/dataObjs/StepVo';
import MainStorage from '../classes/MainStorage';
interface MyProps { stepVo: StepVo, selectCallback: Function }
interface MyState { stepVo: StepVo }
class StepBox extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepVo: this.props.stepVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (this.props !== oldProps) {
            this.setState({ stepVo: this.props.stepVo });
        }
    }
    getImage() {

       // let cssStyle: CSSStyleDeclaration = document.createElement("span").style;

        let cssStyle:CSSProperties={};

        cssStyle.backgroundImage="url("+this.state.stepVo.getFirstImage()+")";
        cssStyle.backgroundSize="cover";

    //    console.log(cssStyle);

        return cssStyle;
    }
    render() {

        let classString: string = "stepCard";

        if (this.ms.selectedStep) {
            if (this.ms.selectedStep.id === this.props.stepVo.id) {
                classString = "stepCard selected";
            }
        }

        return (<Card className={classString} style={this.getImage()} onClick={() => { this.props.selectCallback(this.state.stepVo) }}><span id="stepLabel">{this.state.stepVo.label}</span></Card>)
    }
}
export default StepBox;