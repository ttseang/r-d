import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { MainController } from '../../classes/MainController';
interface MyProps { }
interface MyState { }
class AnimationPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state={};
        }
    render() {
        return (<div className='tac' style={{marginTop:"10px",marginBottom:"10px"}}><Button size="sm" onClick={()=>{this.mc.changeScreen(4)}}>Set Animations</Button></div>)
    }
}
export default AnimationPanel;