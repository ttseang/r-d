import React, { ChangeEvent, Component } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../../classes/dataObjs/ExtrasVo';
import { PopUpVo } from '../../classes/dataObjs/PopUpVo';

import MainStorage from '../../classes/MainStorage';
interface MyProps {element:ElementVo,callback:Function}
interface MyState {element:ElementVo}
class PopUpSelect extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state={element:this.props.element};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (this.props!==oldProps)
        {
            this.setState({element:this.props.element});
        }
    }
    listChanged(e:ChangeEvent<HTMLSelectElement>)
    {
        let extras:ExtrasVo=this.state.element.extras;
        extras.popupID=this.ms.popups[e.currentTarget.selectedIndex].id;

        this.props.callback(extras);
    }
    getPopUpList()
    {
        let popups:PopUpVo[]=this.ms.popups;

        let selectePopUp:PopUpVo=this.ms.getPopUpByID(this.state.element.extras.popupID);

        let ddArray:JSX.Element[]=[];
        for (let i:number=0;i<popups.length;i++)
        {
            let key:string="poplist"+i.toString();
          //  let selected:boolean=(popups[i].fontName===this.state.elementVo.extras.fontName)?true:false;

            ddArray.push(<option key={key}>{popups[i].name}</option>)
        }
        return (<select onChange={this.listChanged.bind(this)} value={selectePopUp.name}>{ddArray}</select>)
    }
    render() {
        return (<Card>
            <Row><Col className='tac'>Pop Up: {this.getPopUpList()}</Col></Row>
        </Card>)
    }
}
export default PopUpSelect;