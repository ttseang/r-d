import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import MainStorage from '../../classes/MainStorage';
import ValBox from './ValBox';
interface MyProps {elementVo:ElementVo,callback:Function}
interface MyState {elementVo:ElementVo}
class LinePosBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {elementVo:this.props.elementVo};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({elementVo:this.props.elementVo});
        }
    }
    onXChange(xx:number)
    {
        let elementVo:ElementVo=this.props.elementVo;
        elementVo.x=xx;
        this.setState({elementVo:elementVo});
        this.props.callback(elementVo.x,elementVo.y,elementVo.w,elementVo.h);       
    }
    onYChange(yy:number)
    {
        let elementVo:ElementVo=this.props.elementVo;
        elementVo.y=yy;
        this.setState({elementVo:elementVo});
        this.props.callback(elementVo.x,elementVo.y,elementVo.w,elementVo.h);
    }
    onWChange(ww:number)
    {
        let elementVo:ElementVo=this.props.elementVo;
        elementVo.w=ww;
        this.setState({elementVo:elementVo});
        this.props.callback(elementVo.x,elementVo.y,elementVo.w,elementVo.h);
    }
    onHChange(hh:number)
    {
        let elementVo:ElementVo=this.props.elementVo;
        elementVo.h=hh;
        this.setState({elementVo:elementVo});
        this.props.callback(elementVo.x,elementVo.y,elementVo.w,elementVo.h);
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    <ValBox val={this.state.elementVo.x} min={0} max={100} callback={this.onXChange.bind(this)} label="X1" multiline={false}></ValBox>
                    <ValBox val={this.state.elementVo.y} min={0} max={100} callback={this.onYChange.bind(this)} label="Y1" multiline={false}></ValBox>
                    <ValBox val={this.state.elementVo.w} callback={this.onWChange.bind(this)} label="X2" multiline={false}></ValBox>
                    <ValBox val={this.state.elementVo.h} callback={this.onHChange.bind(this)} label="Y1" multiline={false}></ValBox>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default LinePosBox;