import React, { Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../../classes/dataObjs/ExtrasVo';
import ColorPicker from '../../classes/ui/ColorPicker';
import ValBox from './ValBox';
interface MyProps {elementVo:ElementVo,callback:Function}
interface MyState {elementVo:ElementVo}
class LineStylebox extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {elementVo:this.props.elementVo};
        }
        colorCallback(color:string)
        {
           // console.log(color);
            let extras:ExtrasVo=this.state.elementVo.extras;
            extras.borderColor=color;
            this.props.callback(extras);
        }
    onThickChange(val:number)
    {
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.borderThick=val;
        this.props.callback(extras);
    }
    render() {
        return (<Card>
            <Card.Body>
            <Row>
                <Col className='tac'><ColorPicker myColor={this.state.elementVo.extras.borderColor} callback={this.colorCallback.bind(this)}></ColorPicker>
                </Col>                
            </Row>
            <Row><Col> <ValBox val={this.state.elementVo.extras.borderThick} min={1} max={20} callback={this.onThickChange.bind(this)} label="X1" multiline={false}></ValBox>
                   </Col></Row>
            </Card.Body>
        </Card>)
    }
}
export default LineStylebox;