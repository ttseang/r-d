import React, { Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { TextStyleVo } from '../classes/dataObjs/TextStyleVo';
import CheckBox from './CheckBox';
interface MyProps { textStyle: TextStyleVo, checked: boolean, callback: Function }
interface MyState { textStyle: TextStyleVo, checked: boolean }
class TextStyleBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props)
        this.state = { textStyle: this.props.textStyle, checked: this.props.checked };
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({textStyle:this.props.textStyle,checked:this.props.checked});
        }
    }
    clickBox(value: boolean) {
        this.setState({ checked: value });
        this.props.callback(this.state.textStyle, value);
    }
    render() {

        let cssClass: string = "tac pvtext " + this.state.textStyle.classes.join(" ");


        return (<Card className="animationCard">
            <Card.Body>
                <Card.Header>{this.state.textStyle.name}</Card.Header>
                <div className={cssClass}>TEXT</div>
            </Card.Body>
            <Card.Footer><CheckBox checked={this.state.checked} callback={this.clickBox.bind(this)}></CheckBox></Card.Footer>
        </Card>)
    }
}
export default TextStyleBox;