import React, { Component } from 'react';
import { StepVo } from '../classes/dataObjs/StepVo';
import MainStorage from '../classes/MainStorage';
import { TemplateData } from '../classes/TemplateData';
interface MyProps { stepVo: StepVo | null,editCallback:Function,onUp:Function,onMove:Function,lineUp:Function,lineDown:Function}
interface MyState { stepVo: StepVo | null}
class PreviewPage extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepVo: this.props.stepVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepVo: this.props.stepVo });
        }
    }
    getPath(image: string) {
        return this.ms.getImagePath(image,0);
    }

    getPage() {       

        let td: TemplateData = new TemplateData();
        return td.getPage2(this.state.stepVo,this.props.editCallback,this.props.onUp,this.props.lineUp,this.props.lineDown);
       // return td.getPage(pageVo, texts, images,left);
    }


    render() {

        let w:number=this.ms.pageW;
        let h:number=this.ms.pageH;
        let vb:string="0 0 "+w.toString()+" "+h.toString();

        return (<svg viewBox={vb} height={h} width={w} onPointerMove={(e:React.PointerEvent<SVGElement>)=>{this.props.onMove(e)}}>
            {this.getPage()}
           
            </svg>)
    }
}
export default PreviewPage;