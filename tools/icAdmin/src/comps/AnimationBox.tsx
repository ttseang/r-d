import React, { Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { AnimationVo } from '../classes/dataObjs/AnimationVo';
import CheckBox from './CheckBox';
interface MyProps { animationVo: AnimationVo, callback: Function, checked: boolean }
interface MyState { animationVo: AnimationVo,checked:boolean }
class AnimationBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { animationVo: this.props.animationVo,checked:this.props.checked }
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ animationVo: this.props.animationVo,checked:this.props.checked });
        }
    }
    /* clickCheck(e:ChangeEvent<HTMLInputElement>)
    {
        let checked:boolean=!this.state.checked;
        this.props.callback(this.state.animationVo,checked);
        
    } */
    clickBox(value:boolean)
    {
        this.setState({checked:value});
        this.props.callback(this.state.animationVo,value);
    }
    getImage()
    {
        return "./images/appImages/animations/"+this.state.animationVo.id.toString()+".jpg";
    }
    render() {
        return (<Card className="animationCard">
            <Card.Header><Row><Col sm={1}></Col><Col sm={9}><span className='cardTitle'>{this.state.animationVo.name}</span></Col></Row></Card.Header>
            <Card.Body>
            <div className='tac'><img src={this.getImage()} width={50} height={50} alt="animation" /></div>
            </Card.Body>
            <Card.Footer><CheckBox checked={this.state.checked} callback={this.clickBox.bind(this)}></CheckBox></Card.Footer>
        </Card>)
    }
}
export default AnimationBox;