import { Component } from 'react';
import { Button } from 'react-bootstrap';
import { ButtonConstants } from '../../classes/constants/ButtonConstants';
import { ButtonVo } from '../../classes/dataObjs/ButtonVo';
import { StepVo } from '../../classes/dataObjs/StepVo';
import MainStorage from '../../classes/MainStorage';
import AudioPanel from './AudioPanel';
interface MyProps { stepVo: StepVo | null, callback: Function, actionCallback: Function }
interface MyState { stepVo: StepVo | null, panelIndex: number }
class FramePropBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepVo: this.props.stepVo, panelIndex: this.ms.framePanelIndex };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepVo: this.props.stepVo });
        }
    }
    getButtons() {
        let buttonData: ButtonVo[] = ButtonConstants.FRAME_BUTTONS;

        let buttons: JSX.Element[] = [];

        for (let i: number = 0; i < buttonData.length; i++) {
            let key: string = "frameButtons" + i.toString();

            buttons.push(<Button key={key} variant={buttonData[i].variant} className="fw" onClick={() => { this.changePanel(buttonData[i].action) }}>{buttonData[i].text}</Button>)
            if (buttonData[i].action === this.state.panelIndex) {
                buttons.push(this.getPanel());
            }
        }
        return buttons;
    }
    changePanel(index: number) {
        this.ms.framePanelIndex=index;
        this.setState({ panelIndex: index });
    }
    getPanel(): JSX.Element {
        /* if (this.state.elementVo === null) {
            return (<div>Loading</div>);
        } */
        // console.log("panelIndex=" + this.state.panelIndex);

        switch (this.state.panelIndex) {
            case 0:

                return <AudioPanel key="mainAudio" stepVo={this.state.stepVo} useBackground={false}></AudioPanel>

            case 1:

                return <AudioPanel key="backgroundAudio" stepVo={this.state.stepVo} useBackground={true}></AudioPanel>


        }

        return (<div>Select a Frame</div>)
    }

    render() {
        return (<div>{this.getButtons()}</div>)
    }
}
export default FramePropBox;