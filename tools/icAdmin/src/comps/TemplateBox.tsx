import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { TemplateVo } from '../classes/dataObjs/TemplateVo';
import { MainController } from '../classes/MainController';
interface MyProps { templateVo: TemplateVo }
interface MyState { templateVo: TemplateVo }
class TemplateBox extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { templateVo: this.props.templateVo };
    }
    selectTemplate() {
        this.mc.addFromTemplate(this.state.templateVo);
    }
    render() {
        let imagePath:string="./images/appImages/templates/"+this.state.templateVo.id.toString()+".jpg";

        return (<Card>
            <Card.Title>{this.state.templateVo.name}</Card.Title>
            <Card.Body><img className="pageTemplateImage" src={imagePath} alt="templateCard" /></Card.Body>
            <Card.Footer><Button onClick={this.selectTemplate.bind(this)}>Select</Button></Card.Footer>
        </Card>)
    }
}
export default TemplateBox;