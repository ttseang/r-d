import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { StyleVo } from '../classes/dataObjs/StyleVo';
import { MainController } from '../classes/MainController';
interface MyProps { styleVo: StyleVo}
interface MyState { }
class StyleBox extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    private getImagePath(image: string) {
        console.log(image);
        let folder:string="decor";
        if (this.props.styleVo.subType===StyleVo.SUBTYPE_POSITION)
        {
            folder="positions";
        }
        if (this.props.styleVo.subType===StyleVo.SUBTYPE_SIZE)
        {
            folder="sizes";
        }
        let path:string="images/pageTemplates/styles/"+folder+"/"+image;
        console.log(path);

        return path;
    }
    render() {
        return (<Card onClick={()=>{this.mc.styleChangeCallback(this.props.styleVo)}}>
            <Card.Body>
                <Card.Title className="tac">{this.props.styleVo.name}</Card.Title>
                <div className="tac"><img className='styleImage' src={this.getImagePath(this.props.styleVo.image)} alt="styleChoice" /></div>
            </Card.Body>
        </Card>)
    }
}
export default StyleBox;