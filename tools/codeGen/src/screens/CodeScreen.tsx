import React, { Component } from 'react';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { }
interface MyState {htmlString:string,cssString:string}
class CodeScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = { htmlString: this.ms.html, cssString: this.ms.css };
    }
    private copyText(id: string) {
        var copyText = document.getElementById(id) as HTMLInputElement;
        let text: string = copyText.value;
        navigator.clipboard.writeText(text);
    }
    changeHtml(event: any) {
        let html: string = event.target.value;
        this.ms.html = html;
    }
    changeCss(event: any) {
        let css: string = event.target.value;
        this.ms.css = css;
    }
    render() {
        return (<div className="container">
            <div className="text-area">
                <h2>HTML</h2>
                <textarea id="textarea1" rows={10} cols={50} defaultValue={this.state.htmlString} onChange={this.changeHtml.bind(this)}></textarea>
                <button onClick={() => { this.copyText("textarea1") }}>Copy Text</button>
            </div>
            <div className="text-area">
                <h2>CSS</h2>
                <textarea id="textarea2" rows={10} cols={50} defaultValue={this.state.cssString} onChange={this.changeCss.bind(this)}></textarea>
                <button onClick={() => { this.copyText("textarea2") }}>Copy Text</button>
            </div>
        </div>)
    }
}
export default CodeScreen;