import { Component } from 'react';
import CodeScreen from './CodeScreen';
import DesignScreen from './DesignScreen';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = { mode: -1 };
    }
    componentDidMount(): void {
        this.ms.loadFromLocalStorage();
        //wait 2 seconds before showing the screen
        setTimeout(() => {
            this.setState({ mode: 0 });
        }, 2000);
    }
    getScreen() {
        switch (this.state.mode) {
            case -1:
                return <div className="loading">Loading...</div>
            case 0:
                return <DesignScreen />
            case 1:
                return <CodeScreen />

            default:
                return "No screen found";
        }
    }
    getDesignScreen() {
        if (this.ms.loading===true) {
            return;
        }
        this.setState({ mode: 0 });
    }
    getCodeScreen() {
        if (this.ms.loading===true) {
            return;
        }
        this.setState({ mode: 1 });
    }
    getButton() {
        if (this.state.mode === 0) {
            return <button className='switch-button' onClick={this.getCodeScreen.bind(this)}>Code Screen</button>
        }
        else {
            return <button className='switch-button' onClick={this.getDesignScreen.bind(this)}>Design Screen</button>
        }
    }
    makeNewCode() {
        this.ms.clearLocalStorage();
        window.location.reload();
    }
    render() {
        return (<div id="base">
            <header>
                <h1 className="title">Text To Code</h1>
                <div className='button-container'>
                {this.getButton()}
                <button className='reset-button' onClick={this.makeNewCode.bind(this)}>New Code</button>
                </div>
            </header>
            {this.getScreen()}
        </div>)
    }
}
export default BaseScreen;