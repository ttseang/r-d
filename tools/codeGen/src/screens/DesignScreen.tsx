import React, { Component } from 'react';
import { DataCenter } from '../classes/DataCenter';
import { MainStorage } from '../classes/MainStorage';
import { ButtonGroup, Button } from 'react-bootstrap';
interface MyProps { }
interface MyState { htmlString: string, cssString: string, prompt: string, loading: boolean }
class DesignScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = { htmlString: this.ms.html, cssString: this.ms.css, prompt: "", loading: false };
    }
    sendMessage() {
        if (this.state.prompt === "") {
            return;
        }
        let prompt: string = this.state.prompt;
        this.ms.prompt = prompt;
        prompt = prompt.replace(/(?:\r\n|\r|\n)/g, '<br>');
        prompt = prompt.replace(/(?:\t)/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
        prompt = prompt.replace(/(?: )/g, '&nbsp;');
        prompt = prompt.replace(/(?:\n)/g, '<br>');
        prompt = prompt.replace(/(?:\r)/g, '<br>');

        //if prompt doesn't end with a period, add one
        if (prompt[prompt.length - 1] !== ".") {
            prompt += ".";
        }
        this.ms.loading = true;
        this.setState({ loading: true, prompt: "" });
       // let codeObj: any = { "html": this.state.htmlString, "css": this.state.cssString };
       // let codeString: string = JSON.stringify(codeObj);
        let codeString: string = this.state.htmlString + this.state.cssString;
        DataCenter.getCode(codeString, prompt, this.gotResponse.bind(this), this.onError.bind(this));

    }
    onError(data: any) {
       // alert("Error");
        this.setState({ loading: false });
        this.ms.loading = false;
    }
    gotResponse(data: any) {
        // console.log(data);
        let choices: any[] = data.choices;
        let first: any = choices[0];
        console.log(first);
        let message: any = first.message;
        console.log(message);

        let content: any = message.content;
        console.log(content);
        data = JSON.parse(content);
        console.log(data);
        let html: string = data.html;
        let css: string = data.css;
        this.ms.html = html;
        this.ms.css = css;
        this.ms.addHistory();
        this.setState({ htmlString: html, cssString: css, loading: false });
        this.ms.loading = false;
    }
    gotCode(data: any) {

        let isValid:boolean = this.isValidJSON(data);

        if (!isValid)
        {
            //try splitting the string on the first : and take the second part
            let index:number = data.indexOf(":");
            if (index>-1)
            {
                data=data.substring(index+1);
                isValid = this.isValidJSON(data);
                if (!isValid)
                {
                    alert("Invalid JSON");
                    this.setState({ loading: false });
                    this.ms.loading = false;
                    return;
                }
            }

        }
        
        data = JSON.parse(data);
        console.log(data);
        let html: string = data.html;
        let css: string = data.css;
        this.ms.html = html;
        this.ms.css = css;
        this.ms.addHistory();
        this.setState({ htmlString: html, cssString: css, loading: false });
        this.ms.loading = false;
    }
    
    getRenderedCode()
    {
        //make an html document
        const doc:HTMLElement = document.createElement("html");
        //set the inner html to the html string
        doc.innerHTML=this.state.htmlString;
        //get the head element
        const head:HTMLElement = doc.getElementsByTagName("head")[0];
        //create a style element
        const style:HTMLElement = document.createElement("style");
        //set the inner html to the css string
        style.innerHTML=this.state.cssString;
        //append the style element to the head
        head.appendChild(style);
        //return the html of the document
        let html= doc.outerHTML;
        //encode the html
        html = encodeURIComponent(html);
        html = `data:text/html;charset=utf-8,${html}`;
        return html;
    }
    checkForEnter(e: any) {
        if (e.keyCode === 13) {
            this.sendMessage();
        }
    }
    onTextChange(e: any) {
        //set prompt to the value of the text area
        this.setState({ prompt: e.target.value });
    }
    undo() {
        this.ms.undo();
        this.setState({ htmlString: this.ms.html, cssString: this.ms.css });
    }
    redo() {
        this.ms.redo();
        this.setState({ htmlString: this.ms.html, cssString: this.ms.css });
    }
    getDefaultHTML() {
        //some sample html
        let html: string = `<html>
        <head>
        <style>
        body {
            background-color: linen;
            }
            </style>
            </head>
            <body>
            <p>Hello World!</p>
            </body>
            </html>`

        html = `data:text/html;charset=utf-8,${html}`;

        //take out any characters that might cause problems
        html = html.replace(/(?:\r\n|\r|\n)/g, '');
        html = html.replace(/(?:\t)/g, '');
        html = html.replace(/(?: )/g, '');
        html = html.replace(/(?:\n)/g, '');
        html = html.replace(/(?:\r)/g, '');


        return html;
    }
    getInput() {
        if (this.state.loading) {
            return "Loading...";
        }
        return <div className="input-container">
            <textarea id="message" placeholder="Type your instructions here..." defaultValue={this.state.prompt} onChange={this.onTextChange.bind(this)} onKeyDown={this.checkForEnter.bind(this)}></textarea>
            <button onClick={() => { this.sendMessage() }}>Send</button>
        </div>
    }
    getEditButtons() {
        if (this.state.loading) {
            return;
        }
        return  <ButtonGroup className='editButtons'>
        <Button id="undo-btn" onClick={this.undo.bind(this)}><i className="fas fa-undo"></i></Button>
        <Button id="redo-btn" onClick={this.redo.bind(this)}><i className="fas fa-redo"></i></Button>
        <Button id="last-prompt-btn" onClick={() => { this.setState({ prompt: this.ms.prompt }) }}><i className="fas fa-history"></i></Button>
    </ButtonGroup>
    }

    private isValidJSON(str: string): boolean {
        try {
            JSON.parse(str);
            return true;
        } catch (error) {
            return false;
        }
    }
    render() {

        //place the sample html in the iframe
        return (<div className="designcontainer">
            <div className="iframe-container">
                <iframe title='codePreview' id="frame1" src={this.getRenderedCode()}></iframe>
            </div> 
            
            {this.getInput()}
            {this.getEditButtons()}
        </div>)
    }
}
export default DesignScreen;

