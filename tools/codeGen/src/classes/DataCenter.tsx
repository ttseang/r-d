import ApiConnect from "./ApiConnect";
import { ChatVo } from "./dataObjs/ChatVo";

export class DataCenter {

    public static getCode(existingCode: string, prompt: string, callback: Function, errorCallback: Function) {
        //1300
        const apiConnect: ApiConnect = new ApiConnect();
        let messages: ChatVo[] = [];
        messages.push(new ChatVo("system", "Provide the user with html and css code in a JSON object based on the prompt. Keep the html and css separate. If there is not already head and body tag create them. Do not explain what you are doing. Just give the JSON object. Use the following format for the JSON object: {\"html\":\"<div>hello</div>\",\"css\":\"div{color:red;}\"}"));

        if (existingCode === "") {
            messages.push(new ChatVo("user", prompt));
        } else {
            messages.push(new ChatVo("system", "The existing code is: " + existingCode));
            messages.push(new ChatVo("user", prompt));
        }

        // apiConnect.addParam(existingCode);
        // apiConnect.addParam(prompt);
        let msgObj: any = JSON.stringify(messages);
        apiConnect.sendTT(msgObj, callback, errorCallback);

    }
}

/* $messasge = array();
$messasge[0]['role'] = "system";
$messasge[0]['content'] = "Provide the user with html and css code in a JSON object based on the prompt. Keep the html and css separate. If there is not already head and body tag create them. Do not explain what you are doing. Just give the JSON object. Use the following format for the JSON object: {\"html\":\"<div>hello</div>\",\"css\":\"div{color:red;}\"}";

if ($code == "") {
    $messasge[1]['role'] = "user";
    $messasge[1]['content'] = $prompt2;
} else {
    $messasge[1]['role'] = "system";
    $messasge[1]['content'] = "The existing code is: " . $code;

    $messasge[2]['role'] = "user";
    $messasge[2]['content'] = $prompt2;
}
 */