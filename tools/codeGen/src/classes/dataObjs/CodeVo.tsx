export class CodeVo
{
    public htmlString:string="";
    public cssString:string="";
    public prompt:string="";
    
    constructor(htmlString:string,cssString:string,prompt:string)
    {
        this.htmlString=htmlString;
        this.cssString=cssString;
        this.prompt=prompt;
    }
}