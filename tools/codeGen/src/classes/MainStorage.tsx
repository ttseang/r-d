import { CodeVo } from "./dataObjs/CodeVo";


/* eslint-disable @typescript-eslint/no-useless-constructor */
export class MainStorage
{
    private static instance:MainStorage | null=null;
    public html:string="";
    public css:string="";
    public prompt:string="";
    public history:CodeVo[]=[];
    
    public historyIndex:number=0;
    public loading:boolean=false;
    
    constructor()
    {
        //
    }

    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new MainStorage();
        }
        return this.instance;
    }
    public saveToLocalStorage()
    {
        localStorage.setItem("html",this.html);
        localStorage.setItem("css",this.css);
        localStorage.setItem("prompt",this.prompt);
    }
    public loadFromLocalStorage()
    {
        this.html=localStorage.getItem("html")||"";
        this.css=localStorage.getItem("css")||"";
        this.prompt=localStorage.getItem("prompt")||"";
    }
    public clearLocalStorage()
    {
        localStorage.removeItem("html");
        localStorage.removeItem("css");
        localStorage.removeItem("prompt");
    }
    public addHistory()
    {
        let codeVo:CodeVo=new CodeVo(this.html,this.css,this.prompt);
        this.history.push(codeVo);
        this.historyIndex=this.history.length;
        this.saveToLocalStorage();
    }
    public undo()
    {
        if (this.historyIndex>0)
        {
            this.historyIndex--;
            let codeVo:CodeVo=this.history[this.historyIndex];
            this.html=codeVo.htmlString
            this.css=codeVo.cssString;
            this.prompt=codeVo.prompt;
        }
    }
    public redo()
    {
        if (this.historyIndex<this.history.length-1)
        {
            this.historyIndex++;
            let codeVo:CodeVo=this.history[this.historyIndex];
            this.html=codeVo.htmlString
            this.css=codeVo.cssString;
            this.prompt=codeVo.prompt;
        }
    }
}