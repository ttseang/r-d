import './App.css';
import BaseScreen from './screens/BaseScreen';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/main.css';
import './styles/codescreen.css';
import './styles/designscreen.css';

function App() {
  return (
    <div className="App">
      <BaseScreen />
    </div>
  );
}

export default App;
