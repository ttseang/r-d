import React, { Component } from 'react';
import { ListVo } from './ListVo';
interface MyProps { children: any, isDragging: boolean, onDown: Function, onUp: Function, index: number, y: number }
interface MyState { index: number, y: number }
class DragListItem extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { index: this.props.index, y: this.props.y };
        }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ y: this.props.y, index: this.props.index });

            
        }
    }
    getStyle() {
        let style: React.CSSProperties = {};
        style.top = this.state.y.toString() + "px";
        if (this.props.isDragging === true) {
            style.zIndex = 100;
        }
        else {
            style.zIndex = 0;
        }
        return style;
    }
    render() {

        return (<div className="draggableListItem" style={this.getStyle()} onPointerUp={() => { this.props.onUp(this.props.index) }} onPointerDown={() => { this.props.onDown(this.props.index) }}>{this.props.children}</div>)
    }
}
export default DragListItem;