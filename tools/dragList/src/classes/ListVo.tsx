export class ListVo
{
    public index:number;
    public name:string;
    public y:number;

    constructor(index:number,name:string)
    {
        this.index=index;
        this.name=name;
        this.y=index*50;
    }
}