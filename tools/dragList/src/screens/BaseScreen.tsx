import { Component } from 'react';
import { Card, ListGroupItem } from 'react-bootstrap';
import DragList from '../classes/DragList';


import { ListVo } from '../classes/ListVo';
interface MyProps { }
interface MyState {listData:ListVo[]}
class BaseScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {listData:[new ListVo(0, "fish"), new ListVo(1, "bread"), new ListVo(2, "pasta"),new ListVo(0, "bird"), new ListVo(1, "cattle"), new ListVo(2, "pencil"),new ListVo(0, "Spanish"), new ListVo(1, "French"), new ListVo(2, "Thai")]};
        }
    
    getList() {
       // let listData: ListVo[] = [new ListVo(0, "fish"), new ListVo(1, "bread"), new ListVo(2, "pasta")];
        let listItems: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.listData.length; i++) {
            let key = "listItem" + i.toString();
            // listItems.push(<DragListItem key={key} listVo={listData[i]} upCallback={this.onUp.bind(this)}></DragListItem>)
            listItems.push(<ListGroupItem key={key} className="usn">{this.state.listData[i].name}</ListGroupItem>)
        }
        return listItems;
    }
    onUpdate(pos: number[]) {
        console.log(pos);
        let nlist:ListVo[]=[];
        for (let i:number=0;i<this.state.listData.length;i++)
        {
            let p:number=pos[i];
            console.log(p);

            nlist.push(this.state.listData[p]);
        }
        console.log(nlist);

        this.setState({listData:nlist});
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header>Drag Test</Card.Header>
                <div id="holder">
            <div id="myList">
                <DragList itemHeight={50} onUpdate={this.onUpdate.bind(this)}>
                    {this.getList()}
                </DragList>
            </div>
            </div>
            </Card>
        </div>)
    }
}
export default BaseScreen;