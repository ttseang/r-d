import html2canvas from 'html2canvas';
import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { ImageUtil } from '../util/ImageUtil';
interface MyProps { }
interface MyState {imageArray:string[]}
class BaseScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {imageArray:[]};
        }
    componentDidMount()
    {
        this.getRandImages();
    }
    getRandImages()
    {
        let imageArrayLocal:string[]=[];
        for (let i:number=0;i<9;i++)
        {
            let picIndex:number=Math.floor(Math.random()*10)+1;

            imageArrayLocal.push("./images/"+picIndex.toString()+".svg");
        }
        console.log(imageArrayLocal);
        this.setState({imageArray:imageArrayLocal})
    }
    saveImage() {

        let element: HTMLElement | null = document.getElementById("mdesign");
        if (element) {
            html2canvas(element).then((canvas) => {
                //  this.downloadImage(canvas);
                console.log(canvas);
                let base: HTMLDivElement | null = document.getElementById("base") as HTMLDivElement;
                if (base) {

                    //convert svg
                    
                  //  let svgElements:HTMLCollectionOf<SVGElement> = base.getElementsByTagName("svg");
                    


                    //set your thumbnail size here
                    ImageUtil.resizedataURL(canvas.toDataURL("image/png"), 192, 192, (data: string) => {

                        let oldImage:HTMLImageElement | null=document.getElementById("thumbTest") as HTMLImageElement;
                        if (oldImage)
                        {
                            oldImage.remove();
                        }

                        let image: HTMLImageElement = document.createElement("img");
                        image.src = "data:image/png;" + data;
                        image.id="thumbTest";

                        document.body.append(image);



                    })

                }

            })
        }
    }
    getImageDisplay()
    {
        let images:JSX.Element[]=[];
        for (let i:number=0;i<this.state.imageArray.length;i++)
        {
            let alt:string="test"+i.toString();

            images.push(<Col sm={4}><img src={this.state.imageArray[i]} alt={alt} style={{width:"100%"}}></img></Col>)
        }
        return (<Row>{images}</Row>)
    }
        render()
        {
            return (<div id="base">
                <Card>
                    <Card.Header className="head1">
                        <span className="titleText">Title Here</span>
                    </Card.Header>
                    <Card.Body>
                       <div id="mdesign">{ this.getImageDisplay()}</div>
                    </Card.Body>
                    <Card.Footer><Button onClick={this.getRandImages.bind(this)}>Refresh</Button><Button variant='success' onClick={this.saveImage.bind(this)}>Save</Button></Card.Footer>
                </Card>
                
            </div>)
        }
    }
export default BaseScreen;