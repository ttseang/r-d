
import { GameObjects } from "phaser";
import Controller from "../classes/Controller";
import { CompLayout } from "../comps/CompLayout";
import { CompManager } from "../comps/CompManager";
import { CompVo } from "../dataObjs/CompVo";
import { PosVo } from "../dataObjs/PosVo";
import { IComp } from "../interfaces/IComp";
import { IGameObj } from "../interfaces/IGameObj";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private controller:Controller=Controller.getInstance();

    private compLayout:CompLayout | undefined;
    private cm:CompManager=CompManager.getInstance();

    private selectedItem:IGameObj | null =null;
    private selectedComp:IComp | null =null;

    constructor() {
        super("SceneMain");        
    }
    preload() {
        this.load.image("holder", "assets/holder.jpg");
        this.load.image("face","assets/face.png");

        for (let i:number=0;i<this.cm.icons.length;i++)
        {
            let icon:string=this.cm.icons[i];
            this.load.image(icon,"./assets/icons/"+icon+".png");
        }
       /*  this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("arrows", "./assets/arrows.png"); */
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.showPos();
        
        this.compLayout=new CompLayout(this);
              
       
        this.controller.updateCanvas=this.doBuild.bind(this);

        this.input.on("gameobjectdown",this.selectItem.bind(this));
        this.input.on("pointerup",this.onUp.bind(this));
        this.input.on("pointermove",this.dragItem.bind(this));
    }
    selectItem(p:Phaser.Input.Pointer,obj:any)
    {
        obj.alpha=0.5;
        this.selectedItem=obj;
        this.selectedComp=obj;
    }
    dragItem(p:Phaser.Input.Pointer)
    {
        if (this.selectedItem)
        {
            this.selectedItem.x=p.x;
            this.selectedItem.y=p.y;
        }
    }
    onUp(p:Phaser.Input.Pointer)
    {
        if (this.selectedItem && this.selectedComp)
        {
            let pos:PosVo=this.grid.findNearestGridXY(this.selectedItem.x,this.selectedItem.y);
            //console.log(pos);

            let ww:number=this.selectedItem.displayWidth;
            let hh:number=this.selectedItem.displayHeight;

            //console.log("ww="+ww);
            //console.log("hh="+hh);

            this.selectedComp.setPos(pos.x,pos.y);

            //console.log(this.cm);
            
            this.cm.currentComp=this.selectedComp.key;

            let compVo:CompVo | null=this.cm.getCompVo(this.selectedComp.key);

            if (compVo)
            {
                compVo.x=pos.x;
                compVo.y=pos.y;
                this.controller.setPropsPos(pos.x,pos.y);

                if (compVo.type==="btn" || compVo.type==="text")
                {
                    this.controller.setText(compVo.text);

                }
                this.controller.setStyle(compVo.style);
                this.controller.setBackgroundStyle(compVo.backstyle);
            }
            
            this.selectedItem.alpha=1;
        }
        this.selectedItem=null;
    }
    doBuild()
    {
        if (this.compLayout)
        {
            this.compLayout.clear();
            this.compLayout.build();
        }      
    }
    update() {
       
    }
}
export default SceneMain;