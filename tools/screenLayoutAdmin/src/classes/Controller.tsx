
export class Controller {

    private static instance: Controller;
    // public dc: DC | null = null;
    //
    //
   
    public updateCanvas: Function = () => {};   
    public updatePos:Function=()=>{};
    public setPropsPos:Function=()=>{};
    public prevGame:Function=()=>{};
    public setText:Function=()=>{};
    public setBackgroundStyle:Function=()=>{};
    public setStyle:Function=()=>{};
    public setTextStyle:Function=()=>{};
    public setComp:Function=()=>{};
    
    /* constructor() {
      //
    } */
    static getInstance(): Controller {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new Controller();
        }
        return this.instance;
    }
}
export default Controller;