import { CompManager } from "../comps/CompManager";
import { BackStyleVo } from "../dataObjs/BackStyleVo";
import { ButtonStyleVo } from "../dataObjs/ButtonStyleVo";
import { TextStyleVo } from "../dataObjs/TextStyleVo";

export class SetUpData
{
    private cm:CompManager=CompManager.getInstance();

    constructor()
    {
        this.cm.regBackStyle("default",this.cm.defBackStyle);
        this.cm.regBackStyle("blue",new BackStyleVo(0x00ffff,4,0xffffff,0xff0000,0xff0000));
        this.cm.regBackStyle("red",new BackStyleVo(0xff0000, 2, 0xffff00, 0x00000, 0xf0f0f0));
        this.cm.regBackStyle("white",new BackStyleVo(0xffffff, 2, 0x00000, 0x00000, 0xf0f0f0));
        this.cm.regBackStyle("green",new BackStyleVo(0x00ff00, 2, 0x00000, 0x00000, 0xf0f0f0));


        this.cm.regButtonStyle("default",this.cm.defButtonStyle);
        this.cm.regButtonStyle("green-white",new ButtonStyleVo("green",0.08,0.08,"white"));
        this.cm.regButtonStyle("red-white",new ButtonStyleVo("white",0.08,0.08,"red"));
        this.cm.regButtonStyle("test1",new ButtonStyleVo("red-tnr",0.08,0.08,"white"));

        this.cm.regButtonStyle("iconText1",new ButtonStyleVo("black-tnr",0.15,0.15,"blue"));


        this.cm.regTextStyle("default",this.cm.defTextStyle);
        this.cm.regTextStyle("red",new TextStyleVo("Arial","#ff0000",40));
        this.cm.regTextStyle("green",new TextStyleVo("Arial","#00ff00",40));
        this.cm.regTextStyle("white",new TextStyleVo("Times New Roman","#ffffff",40));
        this.cm.regTextStyle("red-tnr",new TextStyleVo("Times New Roman","#ff0000",40));
        this.cm.regTextStyle("black-tnr",new TextStyleVo("Times New Roman","#000000",40));
    }
}