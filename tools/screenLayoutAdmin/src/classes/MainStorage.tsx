import IconButtonVo from "../dataObjs/IconButtonVo";



export class MainStorage {
    public sent: string;
    private static instance: MainStorage;
    public gameConfig:any;
    //
    //
    
    public selectedFile:number=0;
    public prevData:string="";
    
    public makeNewFlag:boolean=true;
    public prevMode:boolean=false;

    public addBarIcons:IconButtonVo[]=[];

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        this.sent="";
        this.addBarIcons.push(new IconButtonVo("Add Window","dark","far fa-window-maximize",0));
        this.addBarIcons.push(new IconButtonVo("Add Button","dark","far fa-square",1));
        this.addBarIcons.push(new IconButtonVo("Add Text","dark","fas fa-font",2));
        this.addBarIcons.push(new IconButtonVo("Add Icon Button","dark","far fa-square",3));
        this.addBarIcons.push(new IconButtonVo("Add Icon Text Button","dark","far fa-square",4));
    }
    static getInstance(): MainStorage {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new MainStorage();
        }
        return this.instance;
    }
}
export default MainStorage;