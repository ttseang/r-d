export class IconButtonVo
{
    public text:string;
    public variant:string;
    public icon:string;
    public action:number;

    constructor(text:string,variant:string,icon:string,action:number)
    {
        this.text=text;
        this.variant=variant;
        this.icon=icon;
        this.action=action;
    }
}
export default IconButtonVo;