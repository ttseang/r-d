//import { BackStyleVo } from "./BackStyleVo";

export class ButtonStyleVo
{
    
    public textStyle:string;
    public hsize:number;
    public vsize:number;
    
    public backStyle:string;

    constructor(textStyle:string,hsize:number,vsize:number,backStyle:string)
    {
        this.textStyle=textStyle;
        this.hsize=hsize;
        this.vsize=vsize;
        this.backStyle=backStyle;
    }
  
}