import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { SetUpData } from '../classes/SetUpData';
import { CompManager } from '../comps/CompManager';
import LayoutScreen from './LayoutScreen';
interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    private cm:CompManager=CompManager.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = { mode: 0 };

            let setUpData:SetUpData=new SetUpData();
            this.cm.icons= ["check","question_mark","triangle","arrows"];
            
        }
    getScreen() {
        switch (this.state.mode) {
            case 0:

                return (<LayoutScreen></LayoutScreen>);
        }
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">TT - Screen Builder</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;