import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import Controller from '../classes/Controller';
import { MainStorage } from '../classes/MainStorage';
import AddBar from '../comps/AddBar';
import { CompManager } from '../comps/CompManager';

import PropsBar from '../comps/PropsBar';
import { CompVo } from '../dataObjs/CompVo';
import SceneMain from '../scenes/SceneMain';
interface MyProps { }
interface MyState { }
class LayoutScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private cm: CompManager = CompManager.getInstance();
    private controller: Controller = Controller.getInstance();

    private index: number = 0;

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
        const config: any = {
            mode: Phaser.AUTO,
            width: 800,
            height: 600,
            parent: 'phaser-game',
            backgroundColor: 0xcccccc,
            scene: [SceneMain]
        }
        this.ms.gameConfig = config;

        new Phaser.Game(config);
    }
    AddItem(itemIndex: number) {
        //console.log(itemIndex);

        let type: string = "";
        let x: number = 0;
        let y: number = 0;
        let w: number = 0.1;
        let h: number = 0.1;

        let style: string = "default";
        let backStyle: string = "default";
        let text: string = "";
        let icon: string = "";

        /* if (itemIndex!==1)
        {
            return;
        } */
        //console.log("ItemIndex="+itemIndex);

        switch (itemIndex) {
            case 0:
                type = "window";
                h = 0.8;
                w = 0.8;
                x = 5;
                y = 5;
                break;

            case 1:
                type = "btn";
                text = "Hello";
                h = 0.08;
                w = 0.08;
                break;

            case 2:
                type = "text";
                text = "Some Text Here";
                w = 300;
                x = 3;
                y = 1;
                break;

            case 3:

                type = "iconbutton";
                w = 0.8;
                icon = this.cm.icons[0];

                break;

            case 4:
                type = "icontextbutton";
                w = 1.8;
                h=1.8;
                icon = this.cm.icons[0];
                text = "My Text";
                backStyle="iconText1";
                break;
        }

        this.index++;
        let id: string = "comp" + this.index.toString();
        //console.log("id="+id);

        let compVo: CompVo = new CompVo(id, type, text, x, y, w, h, style, backStyle);
        compVo.icon = icon;

        this.cm.compDefs.push(compVo);

        //console.log(this.cm);
        this.cm.currentComp = id;

        /*  this.controller.setPropsPos(compVo.x,compVo.y);
 
         if (compVo.type==="btn" || compVo.type==="text")
         {
             this.controller.setText(compVo.text);
 
         }
         this.controller.setStyle(compVo.style);
         if (compVo.type==="text")
         {
             this.controller.setTextStyle(compVo.style);
         }
         this.controller.setBackgroundStyle(compVo.backstyle); */

        this.controller.setComp(compVo);

        this.controller.updateCanvas();
    }
    render() {
        return (<div>
            <Row><Col sm="1"><AddBar icons={this.ms.addBarIcons} callback={this.AddItem.bind(this)}></AddBar></Col>
                <Col sm="8">
                    <div className="tac">
                        <div id="phaser-game">
                        </div></div></Col>
                <Col sm="3">
                    <PropsBar></PropsBar>
                </Col>
            </Row>
        </div>)
    }
}
export default LayoutScreen;