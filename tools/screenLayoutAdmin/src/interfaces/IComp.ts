/**
 * allows us to pass a sprite or image
 * as an IComp
 */
 export interface IComp
 {
     visible:boolean;
     alpha:number;
     displayWidth:number;
     displayHeight:number;
     scaleX:number;
     scaleY:number;
     x:number;
     y:number;
     doResize():void;
     setPos(xx:number,yy:number):void;
     key:string;
 }