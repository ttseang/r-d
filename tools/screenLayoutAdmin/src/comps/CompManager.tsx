import { BackStyleVo } from "../dataObjs/BackStyleVo";
import { ButtonStyleVo } from "../dataObjs/ButtonStyleVo";
import { CompVo } from "../dataObjs/CompVo";
import { PosVo } from "../dataObjs/PosVo";
import { ShadowVo } from "../dataObjs/ShadowVo";
import { StrokeVo } from "../dataObjs/StrokeVo";
import { TextStyleVo } from "../dataObjs/TextStyleVo";
import { IComp } from "../interfaces/IComp";


export class CompManager {
    private static instance: CompManager | null = null;

    public comps: IComp[] = [];
    public compDefs: CompVo[] = [];

    public icons:string[]=[];

    public currentComp:string="";
    
    public compMap: Map<string, IComp> = new Map<string, IComp>();

    public backstyles: Map<string, BackStyleVo> = new Map<string, BackStyleVo>();
    public buttonStyles: Map<string, ButtonStyleVo> = new Map<string, ButtonStyleVo>();
    public defBackStyle: BackStyleVo = new BackStyleVo(0xff0000, 2, 0xffff00, 0x00000, 0xf0f0f0);
    public defButtonStyle: ButtonStyleVo = new ButtonStyleVo("#ffffff", 0.08, 0.08, "default");
    public textStyles: Map<string, TextStyleVo> = new Map<string, TextStyleVo>();

    public defTextStyle:TextStyleVo;

    constructor() {
        this.comps = [];
        //  window['cm'] = this;
        //Palanquin Dark
        //Andika
        this.defTextStyle = new TextStyleVo("font1", "#000000", 50);
        this.defTextStyle.strokeVo = new StrokeVo(4, "#ffffff");
        this.defTextStyle.shadowVo = new ShadowVo(5, 5, "#000000",4,true,false);
    }
    public static getInstance(): CompManager {
        if (this.instance === null) {
            this.instance = new CompManager();
        }
        return this.instance;
    }
    public getCompVo(key:string)
    {
        for (let i:number=0;i<this.compDefs.length;i++)
        {
            if (this.compDefs[i].key===key)
            {
                return this.compDefs[i];
            }
        }
        return null;
    }
    public getCompIndex(key:string)
    {
        for (let i:number=0;i<this.compDefs.length;i++)
        {
            if (this.compDefs[i].key===key)
            {
                return i;
            }
        }
        return -1;
    }
    public updatePos(key:string,pos:PosVo)
    {
        let compVo:CompVo | null=this.getCompVo(key);
        if (compVo)
        {
            compVo.x=pos.y;
            compVo.y=pos.y;
        }
    }
    
    public regTextStyle(key: string, textStyle: TextStyleVo) {
        this.textStyles.set(key, textStyle);
    }
    public getTextStyle(key: string) {
        if (this.textStyles.has(key)) {
            return this.textStyles.get(key);
        }
        return this.defTextStyle;
    }
    public regBackStyle(key: string, backstyle: BackStyleVo) {
        this.backstyles.set(key, backstyle);
    }
    public regButtonStyle(key: string, buttonStyle: ButtonStyleVo) {
        this.buttonStyles.set(key, buttonStyle);
    }
    public getBackStyle(key: string): BackStyleVo {
        if (this.backstyles.has(key)) {

            return this.backstyles.get(key) || this.defBackStyle;
        }
        return this.defBackStyle;
    }
    public getButtonStyle(key: string) {
        if (this.buttonStyles.has(key)) {
            return this.buttonStyles.get(key) || this.defButtonStyle;
        }
        return this.defButtonStyle;
    }
    doResize() {
        for (let i: number = 0; i < this.comps.length; i++) {
            this.comps[i].doResize();
        }
    }
}