import React, { ChangeEvent, Component } from "react";
import { Row, Col, Button, ButtonGroup } from "react-bootstrap";

interface MyProps {
  callback: Function;
  cancelCallback: Function;
  showCancel: boolean;
  buttonText: string;
  text: string;
  clearOnEnter: boolean;
}
interface MyState {
  text: string;
}
class TextInputButton extends Component<MyProps, MyState> {
  static defaultProps = { text: "", clearOnEnter: true, showCancel: false };
  constructor(props: MyProps) {
    super(props);
    this.state = { text: this.props.text };
  }
  onChange(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ text: e.target.value });
  }
  setText() {
    this.props.callback(this.state.text);
  }
  doInput() {
    if (this.state.text === "") {
      return;
    }
    this.props.callback(this.state.text);
    if (this.props.clearOnEnter === true) {
      this.setState({ text: "" });
    }
  }
  doCancel() {
    this.props.cancelCallback();
  }
  enterPressed(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === "Enter") {
      this.doInput();
    }
  }
  getCancelButton() {
    if (this.props.showCancel === false) {
      return "";
    }
    return (<Col>
      <Button onClick={this.doCancel.bind(this)}>{this.props.buttonText}</Button>
    </Col>)
  }
  getButtons() {
    if (this.props.showCancel === false) {
      return (<Button className="fl" variant="success" onClick={this.doInput.bind(this)}>{this.props.buttonText}</Button>);
    }
    return (<ButtonGroup className="fl"><Button variant="success" onClick={this.doInput.bind(this)}>{this.props.buttonText}</Button><Button variant="danger" onClick={this.doCancel.bind(this)}>Cancel</Button></ButtonGroup>)
  }
  getInput() {
    return (
      <Row>
        <Col sm="8">
          <input
            type="text"
            className="form-control"
            onChange={this.onChange.bind(this)}
            onKeyDown={this.enterPressed.bind(this)}
            value={this.state.text}
          />
        </Col>
        <Col>
          {this.getButtons()}
        </Col>
      </Row>
    );
  }
  render() {
    return (
      <div>
        {this.getInput()}
      </div>
    )
  }
}
export default TextInputButton;
