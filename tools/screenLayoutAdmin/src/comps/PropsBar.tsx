import React, { Component } from 'react';
import Controller from '../classes/Controller';
import { CompVo } from '../dataObjs/CompVo';
import { CompManager } from './CompManager';
import ArrangeBox from './edit/ArrangeBox';
import BackStyleBox from './edit/BackStyleBox';
import ButtonStyleBox from './edit/ButtonStyleBox';
import IconBox from './edit/IconBox';
import PosBox from './edit/PosBox';
import TextEditBox from './edit/TextEditBox';
import TextStyleBox from './edit/TextStyleBox';
interface MyProps { }
interface MyState { x: number, y: number, w: number, h: number, text: string, backstyle: string, style: string, textStyle: string,icon:string }
class PropsBar extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();
    private cm: CompManager = CompManager.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { x: 0, y: 0, w: 2, h: 2, text: "", backstyle: "", style: "", textStyle: "",icon:"" };
        this.controller.setPropsPos = this.updatePos.bind(this);
        this.controller.setText = this.setText.bind(this);
        this.controller.setBackgroundStyle = this.setBack.bind(this);
        this.controller.setStyle = this.setStyle.bind(this);
        this.controller.setTextStyle = this.setTextStyle.bind(this);
        this.controller.setComp=this.setComp.bind(this);

    }
    setComp(compVo:CompVo)
    {
        this.setState({x:compVo.x,y:compVo.y,w:compVo.w,h:compVo.h,text:compVo.text,backstyle:compVo.backstyle,style:compVo.style,icon:compVo.icon});

    }
    setStyle(style: string) {
        this.setState({ style: style });
    }
    setBack(backstyle: string) {
        this.setState({ backstyle: backstyle });
    }
    updatePos(x: number, y: number) {
        this.setState({ x: x, y: y });
    }
    setText(text: string) {
        this.setState({ text: text });
    }
    setTextStyle(ts: string) {
        this.setState({ textStyle: ts });
    }
    updateText(text: string) {
        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);

        if (compVo) {
            compVo.text = text;
            this.controller.updateCanvas();
        }
    }
    updateBackStyle(backstyle: string) {
        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);

        if (compVo) {
            compVo.backstyle = backstyle;
            //console.log(compVo);
            this.controller.updateCanvas();
        }
    }
    updateButtonStyle(buttonStyle: string) {
        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);

        if (compVo) {
            compVo.style = buttonStyle;
            //console.log(compVo);
            this.controller.updateCanvas();
        }
    }
    updateTextStyle(textStyle: string) {
        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);

        if (compVo) {
            compVo.style = textStyle;
            //console.log(compVo);
            this.controller.updateCanvas();
        }
    }
    
    changePos(x: number, y: number) {
        //console.log(x, y);

        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);

        if (compVo) {
            compVo.x = x;
            compVo.y = y;
            this.controller.updateCanvas();
        }
    }
    public changeArrange(action: number) {

        console.log("action=" + action);
        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);
        if (compVo) {
            let compIndex: number = this.cm.getCompIndex(this.cm.currentComp);

            switch (action) {
                case 0:
                    //move down


                    if (compIndex !== -1 && compIndex > 0) {
                        let index2: number = compIndex - 1;

                        let def1: CompVo = this.cm.compDefs[compIndex];
                        let def2: CompVo = this.cm.compDefs[index2];

                        this.cm.compDefs[index2] = def1;
                        this.cm.compDefs[compIndex] = def2;

                    }
                    this.controller.updateCanvas();
                    break;
                case 1:
                    //move down

                    console.log("down");
                    if (compIndex !== -1 && compIndex < this.cm.compDefs.length - 1) {
                        let index2: number = compIndex + 1;

                        let def1: CompVo = this.cm.compDefs[compIndex];
                        let def2: CompVo = this.cm.compDefs[index2];

                        this.cm.compDefs[index2] = def1;
                        this.cm.compDefs[compIndex] = def2;

                    }
                    this.controller.updateCanvas();
                    break;

                case 2:
                    //delete
                    console.log("delete");

                    compVo.deleted = true;

                    /* if (this.cm.compMap.has(this.cm.currentComp)) {
                        this.cm.compMap.delete(this.cm.currentComp);
                    }
                    console.log(this.cm.compMap); */

                    this.controller.updateCanvas();

                    break;
            }
        }
    }
    changeIcon(icon:string)
    {
        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);

        if (compVo) {
            compVo.icon=icon;
            this.controller.updateCanvas();
        }
    }
    changeSize(w: number, h: number) {
        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);

        if (compVo) {
            compVo.w=w;
            compVo.h=h;
        }
        this.controller.updateCanvas();
    }
    render() {

        let textDis: boolean = true;
        let backDis: boolean = true;
        let buttonDis: boolean = true;
        let buttonStyleDis: boolean = true;
        let arrangeDis: boolean = true;
        let iconDis:boolean=true;

        let compVo: CompVo | null = this.cm.getCompVo(this.cm.currentComp);
        //  console.log(compVo);

        if (compVo) {

            arrangeDis = false;

            if (compVo.type === "btn") {
                buttonDis = false;
                textDis = false;
            }
            if (compVo.type === "iconbutton") {
                buttonDis = false;
                iconDis=false;
            }
            if (compVo.type === "text") {
                textDis = false;
                buttonStyleDis = false;
            }
            if (compVo.type === "window") {
                backDis = false;
            }
            if (compVo.type==="icontextbutton")
            {
                buttonDis=false;
                iconDis=false;
                textDis=false;
                //backDis=false;
            }
        }
        
        return (<div>
            <PosBox x={this.state.x} y={this.state.y} w={this.state.w} h={this.state.h} disabled={false} posCallback={this.changePos.bind(this)} sizeCallback={this.changeSize.bind(this)}></PosBox>
            <ArrangeBox disabled={arrangeDis} callback={this.changeArrange.bind(this)}></ArrangeBox>
            <TextEditBox disabled={textDis} text={this.state.text} textCallback={this.updateText.bind(this)}></TextEditBox>
            <IconBox disabled={iconDis} icon={this.state.icon} callback={this.changeIcon.bind(this)}></IconBox>
            <BackStyleBox disabled={backDis} callback={this.updateBackStyle.bind(this)} backstyle={this.state.backstyle}></BackStyleBox>
            <ButtonStyleBox disabled={buttonDis} callback={this.updateButtonStyle.bind(this)} buttonStyle={this.state.style}></ButtonStyleBox>
            <TextStyleBox disabled={buttonStyleDis} callback={this.updateTextStyle.bind(this)} textStyle={this.state.textStyle}></TextStyleBox>

        </div>)
    }
}
export default PropsBar;