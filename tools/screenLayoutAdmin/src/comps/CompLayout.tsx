
import { AnchorVo } from "../dataObjs/AnchorVo";
import { BackStyleVo } from "../dataObjs/BackStyleVo";
import { ButtonStyleVo } from "../dataObjs/ButtonStyleVo";
import { CompVo } from "../dataObjs/CompVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IComp } from "../interfaces/IComp";
import { IGameObj } from "../interfaces/IGameObj";
import { CompManager } from "./CompManager";
import { BorderButton } from "./ui/BorderButton";
import { CheckBox } from "./ui/CheckBox";
import { IconButton } from "./ui/IconButton";
import { IconTextButton } from "./ui/IconTextButton";
import { ResponseImage } from "./ui/ResponseImage";
import { TextComp } from "./ui/TextComp";
import { TriangleButton } from "./ui/TriangleButton";
import UISlider from "./ui/UISlider";
import { UIWindow } from "./ui/UIWindow";

export class CompLayout {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;

    private allComps: IGameObj[] = [];

    private cm: CompManager = CompManager.getInstance();

    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
    }
    clear() {
        console.log(this.allComps);

        let len: number = this.allComps.length;
        for (let i: number = 0; i < len; i++) {
            this.allComps[i].destroy();
        }
        this.allComps = [];
    }
    build() {
        //console.log("build");

        for (let i: number = 0; i < this.cm.compDefs.length; i++) {
            let def: CompVo = this.cm.compDefs[i];

            let key: string = def.key;

            //console.log("KEY="+key);

            let type: string = def.type;
            let backStyle: string = def.backstyle;
            let style: string = def.style;

            let ww: number = def.w;
            let hh: number = def.h;
            let xx: number = def.x;
            let yy: number = def.y;

            let text: string = def.text;
            let icon:string=def.icon;

            let anchorVo: AnchorVo | null = def.anchorVo;

            let angle: number = def.angle;

            let buttonVo: ButtonStyleVo = this.cm.getButtonStyle(style);

            if (def.deleted === false) {
                switch (type) {
                    case "window":
                        let uiWindow: UIWindow = new UIWindow(this.bscene, key, ww, hh, backStyle);
                        uiWindow.setPos(xx, yy);

                        this.allComps.push(uiWindow);

                        break;

                    case "btn":

                        let button: BorderButton = new BorderButton(this.bscene, key, text, buttonVo);
                        button.setPos(xx, yy);

                        if (anchorVo) {
                            //console.log(anchorVo);

                            if (this.cm.compMap.has(anchorVo.anchorTo)) {
                                let comp: IComp | undefined = this.cm.compMap.get(anchorVo.anchorTo);
                                if (comp) {
                                    button.anchorTo(comp, anchorVo.anchorX, anchorVo.anchorY, anchorVo.anchorInside);
                                }
                            }

                        }
                        this.allComps.push(button);

                        break;

                    case "img":
                        let image: ResponseImage = new ResponseImage(this.bscene, key, hh, text);
                        image.setPos(xx, yy);
                        image.flipX = def.flipX;
                        image.flipY = def.flipY;

                        this.allComps.push(image);

                        break;

                    case "slider":
                        let sliderStyle: BackStyleVo = this.cm.getBackStyle(backStyle);
                        let slider: UISlider = new UISlider(this.bscene, key, hh, ww, sliderStyle.backColor, sliderStyle.borderColor);
                        slider.setPos(xx, yy);

                        this.allComps.push(slider);

                        break;

                    case "checkbox":
                        let checkBox: CheckBox = new CheckBox(this.bscene, key, hh, backStyle);
                        checkBox.setPos(xx, yy);

                        this.allComps.push(checkBox);

                        break;

                    case "tributton":

                        let triButton: TriangleButton = new TriangleButton(this.bscene, key, buttonVo);
                        triButton.setPos(xx, yy);
                        triButton.setAngle(angle);

                        this.allComps.push(triButton);

                        break;

                    case "iconbutton":

                        let btnIcon: IconButton = new IconButton(this.bscene, key, icon, buttonVo);
                        btnIcon.setPos(xx, yy);

                        this.allComps.push(btnIcon);

                        break;

                    case "icontextbutton":

                        let btnIconTextButton:IconTextButton=new IconTextButton(this.bscene,key,text,icon,"","",buttonVo);
                        btnIconTextButton.setPos(xx,yy);
                        this.allComps.push(btnIconTextButton);
                        
                        break;

                    case "text":

                        let textComp: TextComp = new TextComp(this.bscene, key, text, ww, style);
                        textComp.setPos(xx, yy);
                        this.allComps.push(textComp);

                        break;
                }

            }
        }
    }

}