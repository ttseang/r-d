import React, { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import IconButtonVo from '../dataObjs/IconButtonVo';
interface MyProps { icons: IconButtonVo[], callback: Function }
interface MyState { }
class AddBar extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getIcons() {
        // let icons:string[]=["far fa-window-maximize","far fa-square","fas fa-font"];

        let boxes: JSX.Element[] = [];

        for (let i: number = 0; i < this.props.icons.length; i++) {
            let key:string="addButton"+i.toString();

            boxes.push(<Button className="btnAdd" key={key} variant={this.props.icons[i].variant} size="lg" title={this.props.icons[i].text} onClick={()=>{this.props.callback(this.props.icons[i].action)}}><i className={this.props.icons[i].icon}></i></Button>);
        }
        return (<ButtonGroup vertical={true}>{boxes}</ButtonGroup>);
    }
    render() {
        return (<div className="ma tac">{this.getIcons()}</div>)
    }
}
export default AddBar;