import { GameObjects } from "phaser"
import { BackStyleVo } from "../../dataObjs/BackStyleVo";
import IBaseScene from "../../interfaces/IBaseScene";


export class CompBack extends GameObjects.Graphics
{
    private backStyle:BackStyleVo;
    private ww:number=0;
    private hh:number=0;
    constructor(bscene:IBaseScene,ww:number,hh:number,backStyle:BackStyleVo)
    {
        super(bscene.getScene());

        this.backStyle=backStyle;
        this.ww=ww;
        this.hh=hh;

        this.lineStyle(backStyle.borderThick, backStyle.borderColor);
        this.strokeRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.stroke();

        this.fillStyle(backStyle.backColor, 1);
        this.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.fillPath();

        this.scene.add.existing(this);        
    }

    doResize(ww:number,hh:number)
    {
        this.ww=ww;
        this.hh=hh;

        this.clear();

        this.lineStyle(this.backStyle.borderThick, this.backStyle.borderColor);
        this.strokeRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.stroke();

        this.fillStyle(this.backStyle.backColor, 1);
        this.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.fillPath();
    }
}