import { GameObjects } from "phaser";
import IBaseScene from "../../interfaces/IBaseScene";
import { IComp } from "../../interfaces/IComp";

import { BaseComp } from "./BaseComp";

export class UISlider extends BaseComp implements IComp {
    private back: GameObjects.Image;
    private hscale: number;
    private vscale: number;
    private knob: GameObjects.Image;
    private color: number=0;

    constructor(bscene: IBaseScene,key:string, hscale: number, vscale: number, color: number, knobColor: number) {
        super(bscene,key);
        //
        //
        //
        this.vscale = vscale;
        this.hscale = hscale;

        this.back = this.scene.add.image(0, 0, "holder");
        this.back.setTint(color);
        this.add(this.back);

        this.knob = this.scene.add.image(0, 0, "holder");
        this.knob.setTint(knobColor);
        this.knob.setInteractive();
        this.knob.on("pointerdown", this.knobDown.bind(this))
        this.add(this.knob);

        this.doResize();

        this.scene.add.existing(this);
    }
    knobDown(p: Phaser.Input.Pointer) {
        this.knob.x = p.x - this.x;
        this.bscene.getScene().input.on("pointermove", this.dragKnob.bind(this));
        this.bscene.getScene().input.once("pointerup", this.knobUp.bind(this));
    }
    dragKnob(p: Phaser.Input.Pointer) {
        this.knob.x = p.x - this.x;
        if (this.knob.x < -this.back.displayWidth / 2) {
            this.knob.x = -this.back.displayWidth / 2;
        }
        if (this.knob.x > this.back.displayWidth / 2) {
            this.knob.x = this.back.displayWidth / 2;
        }
    }
    knobUp() {
        this.bscene.getScene().input.off("pointermove");
    }
    doResize() {
        this.back.displayWidth = this.bscene.getW() * this.hscale;
        this.back.displayHeight = this.bscene.getH() * this.vscale;

        this.knob.displayWidth = this.back.displayWidth / 20;
        this.knob.displayHeight = this.back.displayHeight;
        super.doResize();
    }

}
export default UISlider;