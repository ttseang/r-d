import { GameObjects } from "phaser";
import { GM } from "../../classes/GM";
import { ButtonStyleVo } from "../../dataObjs/ButtonStyleVo";
import { PosVo } from "../../dataObjs/PosVo";
import { TextStyleVo } from "../../dataObjs/TextStyleVo";
import IBaseScene from "../../interfaces/IBaseScene";
import { IComp } from "../../interfaces/IComp";

import { BaseComp } from "./BaseComp";
import { CompBack } from "./CompBack";

export class BorderButton extends BaseComp implements IComp {
    
    private buttonVo: ButtonStyleVo;
    private textStyleVo:TextStyleVo | undefined;

    private back: CompBack;
   

    private text1: GameObjects.Text;

    private ww: number;
    private hh: number;

    private gm:GM=GM.getInstance();
   

    public posVo:PosVo=new PosVo(0,0);

    constructor(bscene: IBaseScene,key:string,text:string, buttonVo: ButtonStyleVo) {
        super(bscene,key);    
        this.buttonVo=buttonVo;
       // let ww: number = this.bscene.getW() * buttonVo.hsize;
        let hh: number = this.bscene.getH()*buttonVo.vsize;
        let ww:number=hh*2;

        console.log(buttonVo);
        console.log(ww,hh);
        this.ww = ww;
        this.hh = hh;

        this.backStyleVo=this.cm.getBackStyle(buttonVo.backStyle);
        this.textStyleVo=this.cm.getTextStyle(buttonVo.textStyle);
        console.log(this.textStyleVo);
        
        if (this.textStyleVo===undefined)
        {
            this.textStyleVo=this.cm.defTextStyle;
        }
        this.text1 = this.scene.add.text(0, 0, text, { color: this.textStyleVo.textColor,fontFamily:this.textStyleVo.fontName, fontSize: "40px"});
        this.text1.setOrigin(0.5, 0.5);
        this.text1.setFontFamily(this.textStyleVo.fontName);
        //  hh=this.text1.displayHeight*3;

        this.back = new CompBack(bscene,this.ww,this.hh,this.backStyleVo);
    
      
        this.add(this.back);

        this.add(this.text1);
        
        this.scene.add.existing(this);
        this.setSize(ww, hh);


        this.setInteractive();

        this.on("pointerdown",()=>{
            this.setBorder(this.backStyleVo.borderPress);
        })
        this.on("pointerup",()=>{
            this.resetBorder();
        });
        this.on("pointerover",()=>{
            this.setBorder(this.backStyleVo.borderOver);
        });
        this.on("pointerout",()=>{
            this.resetBorder();
        });

        this.sizeText();

       // window['btn']=this;
    }
    sizeText()
    {
        this.text1.setFontSize(40);
        
        let fs:number=parseInt(this.text1.style.fontSize.split("px")[0]);
        while(this.text1.displayHeight>this.hh*0.95 || this.text1.displayWidth>this.ww*0.95)
        {
            fs--;
            this.text1.setFontSize(fs);
        }
    }
    setText(text:string)
    {
        this.text1.setText(text);
    }
    doResize()
    {
      //  let ww: number = this.bscene.getW() * this.buttonVo.hsize;
        let hh: number = this.bscene.getH()*this.buttonVo.vsize;
        let ww:number=hh*2;
        this.ww = ww;
        this.hh = hh;

       
        /* this.back.clear();


        this.back.fillStyle(this.backStyleVo.backColor, 1);
        this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.back.fillPath(); */

        this.back.doResize(this.ww,this.hh);

        this.setSize(ww,hh);        
       
        super.doResize();

        this.sizeText();
    }
    
    resetBorder() {
        this.back.lineStyle(this.backStyleVo.borderThick, this.backStyleVo.borderColor);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    }
    setBorder(color: number) {
        this.back.lineStyle(this.backStyleVo.borderThick, color);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    }
}