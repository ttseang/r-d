import { ChangeEvent, Component } from 'react';
import { Card } from 'react-bootstrap';
import { BackStyleVo } from '../../dataObjs/BackStyleVo';
import { CompManager } from '../CompManager';
interface MyProps { backstyle: string, callback: Function, disabled: boolean }
interface MyState { backstyle: string, disabled: boolean }
class BackStyleBox extends Component<MyProps, MyState>
{
    private cm: CompManager = CompManager.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { backstyle: this.props.backstyle, disabled: this.props.disabled };
    }
    componentDidUpdate(prevProps: MyProps) {
        if (prevProps.backstyle !== this.props.backstyle) {
            this.setState({ backstyle: this.props.backstyle });
        }
        if (prevProps.disabled !== this.props.disabled) {
            this.setState({ disabled: this.props.disabled });
        }
    }
    getList() {
        let list: JSX.Element[] = [];
        let index: number = 0;
        this.cm.backstyles.forEach((value: BackStyleVo, key: string) => {
            let indexKey: string = "back" + index.toString();

            list.push(<option key={indexKey} value={key}>{key}</option>);
            index++;

        });

        return (<select onChange={this.onChange.bind(this)} className="selectBox" value={this.state.backstyle}>{list}</select>)
    }
    onChange(e: ChangeEvent<HTMLSelectElement>) {
        this.setState({ backstyle: e.currentTarget.value });
        this.props.callback(e.currentTarget.value);
    }
    render() {
        if (this.state.disabled === true) {
            return <div></div>
        }
        return (<div><Card>
            <Card.Header>Back Style</Card.Header>
            <Card.Body>
                {this.getList()}
            </Card.Body>
        </Card></div>)
    }
}
export default BackStyleBox;