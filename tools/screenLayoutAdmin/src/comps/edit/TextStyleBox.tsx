import { ChangeEvent, Component } from 'react';
import { Card } from 'react-bootstrap';

import { TextStyleVo } from '../../dataObjs/TextStyleVo';
import { CompManager } from '../CompManager';
interface MyProps { textStyle: string, callback: Function, disabled: boolean }
interface MyState { textStyle: string, disabled: boolean }
class TextStyleBox extends Component<MyProps, MyState>
{
    private cm: CompManager = CompManager.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { textStyle: this.props.textStyle, disabled: this.props.disabled };
    }
    componentDidUpdate(prevProps: MyProps) {
        if (prevProps.textStyle !== this.props.textStyle) {
            this.setState({ textStyle: this.props.textStyle });
        }
        if (prevProps.disabled !== this.props.disabled) {
            this.setState({ disabled: this.props.disabled });
        }
    }
    getList() {
        let list: JSX.Element[] = [];
        let index: number = 0;
        this.cm.textStyles.forEach((value: TextStyleVo, key: string) => {
            let indexKey: string = "textStyle" + index.toString();

            list.push(<option key={indexKey} value={key}>{key}</option>);
            index++;

        });

        return (<select onChange={this.onChange.bind(this)} className="selectBox" value={this.state.textStyle}>{list}</select>)
    }
    onChange(e: ChangeEvent<HTMLSelectElement>) {
        this.setState({ textStyle: e.currentTarget.value });
        this.props.callback(e.currentTarget.value);
    }
    render() {
        if (this.state.disabled === true) {
            return <div></div>
        }
        return (<div><Card>
            <Card.Header>Text Style</Card.Header>
            <Card.Body>
                {this.getList()}
            </Card.Body>
        </Card></div>)
    }
}
export default TextStyleBox;