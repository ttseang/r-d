import React, { ChangeEvent, Component } from 'react';
import { Card, Row, Col, Form } from 'react-bootstrap';
interface MyProps {text:string,textCallback:Function,disabled:boolean }
interface MyState { text: string, disabled: boolean }
class TextEditBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { text: this.props.text, disabled: this.props.disabled };
    }
    componentDidUpdate(prevProps:MyProps)
    {
        if (prevProps.text!==this.props.text)
        {
            this.setState({text:this.props.text});
        }
        if (prevProps.disabled!==this.props.disabled)
        {
            this.setState({disabled:this.props.disabled});
        }
    }
    onTextChanged(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.currentTarget.value });
        this.props.textCallback(e.currentTarget.value);
    }
    render() {
        if (this.state.disabled===true)
        {
            return <div></div>
        }
        return (
        <div>
            <div>
                <Card>
                    <Card.Header>Text</Card.Header>
                    <Card.Body>
                        <Row>
                            <Col>
                                <Form.Control type="text" size="sm" value={this.state.text} disabled={this.state.disabled} onChange={this.onTextChanged.bind(this)} />
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </div>
        </div>
        )
    }
}
export default TextEditBox;