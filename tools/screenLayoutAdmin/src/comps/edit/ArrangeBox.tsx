import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
interface MyProps { disabled: boolean, callback: Function }
interface MyState { }
class ArrangeBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        if (this.props.disabled===true)
        {
            return <div></div>
        }
        return (<div>
            <Card>
                <Card.Header>Arrange</Card.Header>
                <Card.Body>
            <Row>
                <Col>
                    <Button><i className="fas fa-arrow-up" onClick={()=>{this.props.callback(1)}}></i></Button>
                </Col>
                <Col>
                    <Button><i className="fas fa-arrow-down" onClick={()=>{this.props.callback(0)}}></i></Button>
                </Col>
                <Col>
                    <Button><i className="fas fa-trash" onClick={()=>{this.props.callback(2)}}></i></Button>
                </Col>
            </Row>
            </Card.Body>
            </Card>
        </div>)
    }
}
export default ArrangeBox;