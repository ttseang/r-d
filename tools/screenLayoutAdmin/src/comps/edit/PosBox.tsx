import React, { ChangeEvent, Component } from 'react';
import { Row, Col, Card, Form } from 'react-bootstrap';
interface MyProps { posCallback: Function, sizeCallback: Function, x: number, y: number, w: number, h: number, disabled: boolean }
interface MyState { x: number, y: number, w: number, h: number, disabled: boolean }
class PosBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { x: this.props.x, y: this.props.y, w: this.props.w, h: this.props.h, disabled: this.props.disabled };
    }
    componentDidUpdate(prev: MyProps) {
        if (prev !== this.props) {
            this.setState({ x: this.props.x, y: this.props.y, w: this.props.w, h: this.props.h, disabled: this.props.disabled })
        }
    }
    changeX(e: ChangeEvent<HTMLInputElement>) {
        let x: number = parseFloat(e.currentTarget.value);
        this.setState({ x: x });
        this.props.posCallback(x, this.state.y);
    }
    changeY(e: ChangeEvent<HTMLInputElement>) {
        let y: number = parseFloat(e.currentTarget.value);
        this.setState({ y: y });
        this.props.posCallback(this.state.x, y);
    }
    changeH(e: ChangeEvent<HTMLInputElement>) {
        let h: number = parseFloat(e.currentTarget.value);
        if (!isNaN(h)) {
            this.setState({ h: h });
            this.props.sizeCallback(this.state.w, h);
        }
    }
    changeW(e: ChangeEvent<HTMLInputElement>) {
        let w: number = parseFloat(e.currentTarget.value);
        if (!isNaN(w)) {
            this.setState({ w: w });
            this.props.sizeCallback(w, this.state.h);
        }

    }
    render() {
        return (<div>
            <Card>
                <Card.Header>Position</Card.Header>
                <Card.Body>
                    <Row>
                        <Col>X:<Form.Control min="0" max="10" step={0.1} type="number" size="sm" value={this.state.x} disabled={this.state.disabled} onChange={this.changeX.bind(this)} />
                        </Col>
                        <Col>Y:<Form.Control min="0" max="10" step={0.1} type="number" size="sm" value={this.state.y} disabled={this.state.disabled} onChange={this.changeY.bind(this)} /></Col></Row>
                    <Row>
                        <Col>W:<Form.Control min="0" max="10" step={0.1} type="number" size="sm" value={this.state.w} disabled={this.state.disabled} onChange={this.changeW.bind(this)} /></Col>
                        <Col>H:<Form.Control min="0" max="10" step={0.1} type="number" size="sm" value={this.state.h} disabled={this.state.disabled} onChange={this.changeH.bind(this)} /></Col>

                    </Row>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default PosBox;