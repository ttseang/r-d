import React, { ChangeEvent, Component } from 'react';
import { Card } from 'react-bootstrap';
import { CompManager } from '../CompManager';
interface MyProps { callback: Function, disabled: boolean, icon: string }
interface MyState { icon: string, disabled: boolean }
class IconBox extends Component<MyProps, MyState>
{
    private cm: CompManager = CompManager.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { icon: this.props.icon, disabled: this.props.disabled };
    }
    componentDidUpdate(prevProps:MyProps)
    {
        if (prevProps.icon!==this.props.icon)
        {
            this.setState({icon:this.props.icon});
        }
        if (prevProps.disabled!==this.props.disabled)
        {
            this.setState({disabled:this.props.disabled});
        }
    }
    getIcons() {
        let list: JSX.Element[] = [];
        let index: number = 0;
        this.cm.icons.forEach((icon: string) => {
            let indexKey: string = "icon" + index.toString();

            list.push(<option key={indexKey} value={icon}>{icon}</option>);
            index++;
        });

        return (<select onChange={this.onChange.bind(this)} className="selectBox" value={this.state.icon}>{list}</select>)
    }
    onChange(e: ChangeEvent<HTMLSelectElement>) {
        this.setState({ icon: e.currentTarget.value });
        this.props.callback(e.currentTarget.value);
    }
    render() {
        if (this.state.disabled === true) {
            return <div></div>
        }
        return (<div><Card>
            <Card.Header>Icon</Card.Header>
            <Card.Body>
                {this.getIcons()}
            </Card.Body>
        </Card></div>)
    }
}
export default IconBox;