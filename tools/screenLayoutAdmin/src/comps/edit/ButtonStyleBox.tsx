import { ChangeEvent, Component } from 'react';
import { Card } from 'react-bootstrap';
import { ButtonStyleVo } from '../../dataObjs/ButtonStyleVo';
import { CompManager } from '../CompManager';
interface MyProps { buttonStyle: string, callback: Function, disabled: boolean }
interface MyState { buttonStyle: string, disabled: boolean }
class ButtonStyleBox extends Component<MyProps, MyState>
{
    private cm: CompManager = CompManager.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { buttonStyle: this.props.buttonStyle, disabled: this.props.disabled };
    }
    componentDidUpdate(prevProps: MyProps) {
        if (prevProps.buttonStyle !== this.props.buttonStyle) {
            this.setState({ buttonStyle: this.props.buttonStyle });
        }
        if (prevProps.disabled !== this.props.disabled) {
            this.setState({ disabled: this.props.disabled });
        }
    }
    getList() {
        let list: JSX.Element[] = [];
        let index: number = 0;
        this.cm.buttonStyles.forEach((value: ButtonStyleVo, key: string) => {
            let indexKey: string = "buttonStyle" + index.toString();

            list.push(<option key={indexKey} value={key}>{key}</option>);
            index++;

        });

        return (<select onChange={this.onChange.bind(this)} className="selectBox" value={this.state.buttonStyle}>{list}</select>)
    }
    onChange(e: ChangeEvent<HTMLSelectElement>) {
        this.setState({ buttonStyle: e.currentTarget.value });
        this.props.callback(e.currentTarget.value);
    }
    render() {
        if (this.state.disabled === true) {
            return <div></div>
        }
        return (<div><Card>
            <Card.Header>Button Style</Card.Header>
            <Card.Body>
                {this.getList()}
            </Card.Body>
        </Card></div>)
    }
}
export default ButtonStyleBox;