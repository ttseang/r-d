import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import ApiConnect from '../classes/ApiConnect';
import { ChatVo } from '../dataObjects/ChatVo';
import { EqStepVo } from '../dataObjects/EqStepVo';
import EquationScreen from './EquationScreen';
//import InputScreen from './InputScreen';
interface MyProps { }
interface MyState { mode: number,eqSteps:EqStepVo[] }
class BaseScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { mode: 0,eqSteps:[] };
        }
    componentDidMount(): void {
        this.loadTestJson();
    }
    loadTestJson()
    {
       fetch("./sample3.json")
       .then(response => response.json())
       .then(data => this.process(data ));
    }
    process(data: any)
    {
        console.log(data);
        let steps: EqStepVo[] = [];
        for (let i = 0; i < data.steps.length; i++) {
            let step = data.steps[i];
            let eqStep:EqStepVo=new EqStepVo(step.instruction,step.lines,step.carry);
            steps.push(eqStep);
        }
        console.log(steps);
        this.setState({ mode: 1,eqSteps:steps });
    }
    getScreen() {
        switch (this.state.mode) {
            case 0:
                return "loading"
              //  return <InputScreen callback={this.getSteps.bind(this)}></InputScreen>
            case 1:
                return <EquationScreen steps={this.state.eqSteps}></EquationScreen>
        }
    }
    getSteps(text:string)
    {
        //if the text contains text other than numbers and operators, return
        if (text.match(/[^0-9+\-*/]/)) {
            alert("Please enter a valid equation");
            return;
        }
        //if the text contains more than one operator, return
        if (text.match(/[+\-*/]{2,}/)) {
            alert("Please enter a simple equation");
            return;
        }
        //if the text contains an operator at the end, return
        if (text.match(/[+\-*/]$/)) {
            alert("Please enter a valid equation");
            return;
        }
        const systemMessage:ChatVo=new ChatVo("system","You are a math genius!");
        const messages:ChatVo[]=[systemMessage];
        const prompt: string = 'provide the step by steps needed to solve'+text+'. With each step include lines for each part of the equation. Put the data in this format: {steps:[{"instruction"..,"lines":[..],"carry":..}]}';
        const apiConnect:ApiConnect=new ApiConnect();
        messages.push(new ChatVo("user",prompt));
        apiConnect.sendTT(JSON.stringify(messages),this.gotSteps.bind(this),()=>{});
       // apiConnect.getSteps(prompt,this.gotSteps.bind(this),()=>{});
    }
    gotSteps(data:any)
    {
        console.log(data);
        let stepData:string=data.choices[0].message;
        console.log(stepData);
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Title Here</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;
//provide the visual steps needed to solve 255+77. Put the data in this format: {steps:[{"instruction"..,"lines":[..],"carry":..}]}