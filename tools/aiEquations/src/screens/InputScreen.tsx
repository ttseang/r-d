import React, { Component } from 'react';
interface MyProps {callback:Function}
interface MyState {text:string}
class InputScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {text:""};
        }
    onTextChange(event: any) {
        this.setState({ text: event.target.value });
    }
    getInputBox()
    {
        //return a card with text box and button
        return (<div className='eqInput'>
            <input type="text" id="inputBox" onChange={this.onTextChange.bind(this)} value={this.state.text}></input>
            <button id="inputButton" onClick={()=>{this.props.callback(this.state.text)}}>Go</button>
        </div>)
    }
    render() {
        return (<div>
            {this.getInputBox()}
        </div>)
    }
}
export default InputScreen;