import ApiConnect from "./ApiConnect";

export class DataCenter {
    public static sendChat(allMessages: string, callback: Function) {
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(allMessages);
        apiConnect.sendTT(allMessages, callback, (error: string) => { alert("Error: " + error); });
    }
}