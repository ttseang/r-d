

/* eslint-disable @typescript-eslint/no-useless-constructor */
export class MainStorage
{
    private static instance:MainStorage | null=null;

    
    constructor()
    {
        //
    }

    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new MainStorage();
        }
        return this.instance;
    }

}