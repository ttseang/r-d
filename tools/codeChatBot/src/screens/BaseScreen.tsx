import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

import { MainStorage } from '../classes/MainStorage';
//import { StartData } from '../classes/StartData';
import ChatScreen from './ChatScreen';

interface MyProps { }
interface MyState {mode:number}
class BaseScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private catID:number=0;
    private listID:number=0;
    

        constructor(props: MyProps) {
            super(props);
            this.state={mode:0}
        }
    componentDidMount()
    {
       // let sd:StartData=new StartData(this.gotCats.bind(this));
       // sd.getCats();
        
    }
    gotCats()
    {
        this.setState({mode:1});
    }
  
    goBack()
    {
       /*  switch(this.state.mode)
        {
            case 2:
                this.setState({mode:1});
            break;

            case 3:
                this.setState({mode:2});
            break;
        } */
    }
    getScreen()
    {
        switch(this.state.mode)
        {
            case 0:

             return <ChatScreen></ChatScreen>

            
        }
    }
    render() {
        return (<div id="base"><Card><Card.Header>Lovelace Code Assistant</Card.Header><Card.Body>{this.getScreen()}</Card.Body></Card></div>)
    }
}
export default BaseScreen;