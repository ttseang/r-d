import React, { Component, createRef } from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import { ChatVo } from '../blogData/ChatVo';
import { DataCenter } from '../classes/DataCenter';
import ChatBar from '../ui/ChatBar';
import { SpeechUtteranceChunker } from '../util/SpeechUtteranceChunker';
interface MyProps { }
interface MyState { messsages: ChatVo[], sending: boolean, text: string, code: string, imageFolder: string, avatarIndex: number, voiceIndex: number }
class ChatScreen extends Component<MyProps, MyState>
{
    private myDivRef = createRef<HTMLDivElement>();
    private isSpeaking: boolean = false;
    constructor(props: MyProps) {
        super(props);
        this.state = { messsages: [new ChatVo("system", "You are a helpful coding assistant. You are a flatterer,flirty and give praise. Introduce yourself. Show all code as formatted. Surround any code examples with ticks `.")], sending: true, text: "", code: "", imageFolder: "eyes_closed", avatarIndex: 1, voiceIndex: this.getVoiceSetting() };
    }
    componentDidMount() {
        let messages = this.state.messsages.slice();
        let messageString: string = JSON.stringify(messages);
        DataCenter.sendChat(messageString, this.gotMessages.bind(this));
    }
    getMessages() {
        let items: JSX.Element[] = [];
        //loop through the messages and place them on list items
        for (let i = 0; i < this.state.messsages.length; i++) {
            let key: string = "chatItem" + i.toString();
            let chatVo: ChatVo = this.state.messsages[i];
            this.extractList(i);
            this.extractAllEquations(i);
            //make a list item for each message
            /*  let variant: string = "light";
             if (chatVo.role === "assistant") {
                 variant = "success";
             } */
            if (chatVo.role !== "system") {

                items.push(<ChatBar key={key} chatVo={chatVo} extractCode={() => { this.extractCode(i); }} doDownload={() => { this.doDownload(i) }} doCopy={() => { this.doCopy(i) }} doSpeak={() => { this.doSpeak(i) }}></ChatBar>)

                //   items.push(<ListGroupItem variant={variant} key={key} className="chatLine" onClick={()=>{this.extractCode(i)}}>{chatVo.content}</ListGroupItem>)

            }

        }
        return <ListGroup>{items}</ListGroup>
    }
    extractList(index: number) {
        let chatVo: ChatVo = this.state.messsages[index];
        let text: string = chatVo.content;
        // const text = "Hi here is a list. 1. Apples. 2. cook the apples. 3. get some more food.";

        const listRegex = /\d+\.\s+([^\d]+)/g;
        const matches = text.matchAll(listRegex);

        const listItems = [];

        for (const match of matches) {
            listItems.push(match[1]);
        }

        console.log(listItems);
    }
    extractAllEquations(i:number) {
        let chatVo: ChatVo = this.state.messsages[i];
        let text: string = chatVo.content;
        //get all text between single back ticks
        //there may be more than one so use a loop and an array
        let equations: string[] = [];
        let start: number = text.indexOf("`");
        let end: number = text.indexOf("`", start + 1);
        while (start > -1 && end > -1) {
            let equation: string = text.substring(start + 1, end);
            equations.push(equation);
            start = text.indexOf("`", end + 1);
            end = text.indexOf("`", start + 1);
        }
        console.log(equations);
        //now we have all the equations
        
      }
    extractCode(index: number) {
        let chatVo: ChatVo = this.state.messsages[index];
        let content: string = chatVo.content;
        let start: number = content.indexOf("```");
        let end: number = content.indexOf("```", start + 3);
        if (start > -1 && end > -1) {
            let code: string = content.substring(start + 3, end);
            console.log(code);
            //format the code
            code = code.replace(/&lt;/g, "<");
            code = code.replace(/&gt;/g, ">");
            code = code.replace(/&amp;/g, "&");
            code = code.replace(/&quot;/g, "\"");
            //replace all ; with a ; and a new line
            //    code=code.replace(/;/g,";\r\n");
            code = code.trim();
            this.setState({ code: code });
        }
    }
    doDownload(index: number) {
        let chatVo: ChatVo = this.state.messsages[index];
        let content: string = chatVo.content;
        //download content
        //let blob = new Blob([content], {type: "text/plain;charset=utf-8"});
        console.log(content);

        const blob = new Blob([content], { type: 'text/plain' });

        // Create a URL object from the Blob object
        const url = URL.createObjectURL(blob);

        // Create a new anchor element to download the file
        const downloadLink = document.createElement('a');
        downloadLink.href = url;
        downloadLink.download = "chat.txt";

        // Append the anchor element to the document body
        document.body.appendChild(downloadLink);

        // Click the anchor element to trigger the file download
        downloadLink.click();

        // Remove the anchor element from the document body
        document.body.removeChild(downloadLink);

        // Revoke the URL object to free up memory
        URL.revokeObjectURL(url);

    }



    doCopy(index: number) {
        let chatVo: ChatVo = this.state.messsages[index];
        let content: string = chatVo.content;
        navigator.clipboard.writeText(content).then(function () {
            /* clipboard successfully set */
        });
    }
    doSpeak(index: number) {
        console.log("doSpeak");
        console.log(this.isSpeaking);
        if (this.isSpeaking === true) {
            return;
        }
        this.isSpeaking = true;
        let chatVo: ChatVo = this.state.messsages[index];
        let content: string = chatVo.content;
        console.log(content);

        //remove the ticks
        content = content.replace(/`/g, "");
        //remove the ...
        content = content.replace(/\.\.\./g, "");



        let utt: SpeechSynthesisUtterance = new SpeechSynthesisUtterance(content);
        utt.voice = speechSynthesis.getVoices()[this.state.voiceIndex];
        utt.volume = 1;
        utt.rate = 1;

        const chunker: SpeechUtteranceChunker = new SpeechUtteranceChunker(speechSynthesis.getVoices()[this.state.voiceIndex]);
        chunker.doSpeak(utt, { chunkLength: 120 }, () => {
            console.log("done");
            this.isSpeaking = false;
        });

        /*  // Create a new instance of SpeechSynthesisUtterance
         const speech = new SpeechSynthesisUtterance();
 
         // Set the text to be spoken
         speech.text = content;
 
         // Set the voice to use (optional)
         speech.voice = speechSynthesis.getVoices()[this.state.voiceIndex];
 
         // Set the rate of speech (optional)
         speech.rate = 1;
 
         // Set the pitch of speech (optional)
         speech.pitch = 1;
 
         // Set the volume of speech (optional)
         speech.volume = 1;
 
         // Speak the text
         window.speechSynthesis.cancel();
         speechSynthesis.speak(speech); */


    }
    onTextChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.target.value });
    }
    getVoices() {
        //put all browser voices in a list on a dropdown
        let voices: JSX.Element[] = [];
        let voiceList: SpeechSynthesisVoice[] = speechSynthesis.getVoices();
        for (let i = 0; i < voiceList.length; i++) {
            let voice: SpeechSynthesisVoice = voiceList[i];
            voices.push(<option key={i} value={i}>{voice.name}</option>)
        }
        return <select value={this.state.voiceIndex} onChange={(e) => { this.setVoice(e.target.selectedIndex) }}>{voices}</select>
    }
    setVoice(index: number) {
        this.setState({ voiceIndex: index }, () => {
            this.rememberVoiceSetting();
        });
    }
    addMessage() {
        if (this.state.text === "") {
            return;
        }
        let messages = this.state.messsages.slice();
        messages.push(new ChatVo("user", this.state.text));
        let messageString: string = JSON.stringify(messages);

        let imageIndex: number = Math.floor(Math.random() * 11) + 1;
        this.setState({ messsages: messages, text: "", sending: true, imageFolder: "eyes_closed", avatarIndex: imageIndex });
        console.log(messageString);
        DataCenter.sendChat(messageString, this.gotMessages.bind(this));
    }
    gotMessages(messageObject: any) {
        console.log(messageObject);

        (window as any).messageObject = messageObject;
        if (messageObject.error) {
            alert(messageObject.error);
            this.setState({ sending: false });
            return;
        }
        let choice: any = messageObject.choices[0];
        if (choice) {
            let message: any = choice.message;
            console.log(message);

            let newMsg: ChatVo = new ChatVo(message.role, message.content);
            let messages: ChatVo[] = this.state.messsages.slice();
            messages.push(newMsg);

            let imageIndex: number = Math.floor(Math.random() * 8) + 1;

            this.setState({ messsages: messages, sending: false, imageFolder: "smile", avatarIndex: imageIndex }, () => {
                this.scrollToBottom();
            });
            //play the audio
            const audio = new Audio("ding.mp3");
            audio.play();
        }
        else {
            console.log("no choice");

        }
    }
    getInputBox() {
        if (this.state.sending) return (<div className="chatInput">Loading...</div>);
        return <div className="chatInput">
            <input type="text" id="chatInputText" value={this.state.text} onKeyDown={this.checkForEnter.bind(this)} onChange={this.onTextChange.bind(this)} />
            <Button id="chatInputButton" variant='success' onClick={() => { this.addMessage() }}>Send</Button>
        </div>;
    }
    checkForEnter(e: React.KeyboardEvent<HTMLInputElement>) {
        if (e.key === "Enter") {
            this.addMessage();
        }
    }
    getAvatar() {

        let avatarPath: string = "avatar/" + this.state.imageFolder + "/" + this.state.avatarIndex.toString() + ".png";
        return <img className='avatar' src={avatarPath} alt="avatar"></img>
    }
    copyCode() {
        let code: string = this.state.code;
        navigator.clipboard.writeText(code).then(function () {
            /* clipboard successfully set */
            console.log("copied");
        });

    }
    rememberVoiceSetting() {
        let voiceIndex: number = this.state.voiceIndex;
        //use local storage to remember the voice setting

        localStorage.setItem("voiceIndex", voiceIndex.toString());


    }
    scrollToBottom() {
        const myDiv = this.myDivRef.current;
        if (myDiv) {
            myDiv.scrollTop = myDiv.scrollHeight;
        }
    }
    getVoiceSetting() {
        let voiceIndex: number = parseInt(localStorage.getItem("voiceIndex") || "0");
        return voiceIndex;
    }
    render() {
        //make a ui for the chat
        //it will be a list of messages and a text box with a send button
        //the messages will be a list of ChatVo objects
        return (<div>
            <div className='chatGrid'>
                <div className="chatScreen">
                    <div className="chatMessages" ref={this.myDivRef}>
                        {this.getMessages()}
                    </div>
                </div>
                <div className='sideBar'>
                    <div className='avatarArea'>
                        {this.getAvatar()}
                        {this.getVoices()}
                    </div>
                    <div className='codeSection'>
                        <h2>Code</h2>
                        <div className='codeArea'><textarea readOnly value={this.state.code}></textarea></div>
                        <Button variant="primary" onClick={() => { this.copyCode() }}>Copy</Button>
                    </div>
                </div>

                {this.getInputBox()}

            </div>
        </div>)

    }
}
export default ChatScreen;