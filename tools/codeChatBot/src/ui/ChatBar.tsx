import React, { Component } from 'react';
import { Col, ListGroupItem, Row } from 'react-bootstrap';
import { ChatVo } from '../blogData/ChatVo';
interface MyProps { chatVo: ChatVo,extractCode:Function,doDownload:Function,doCopy:Function,doSpeak:Function}
interface MyState { }
class ChatBar extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {  };
        }
    getIconBar()
    {
        if (this.props.chatVo.role !== "system" && this.props.chatVo.role !== "user") {
            return (
                <Row>
                <Col><i className="fas fa-code" onClick={()=>{this.props.extractCode()}}></i></Col>
                <Col><i className="fas fa-copy" onClick={()=>{this.props.doCopy()}}></i></Col>
                <Col><i className="fas fa-file" onClick={()=>{this.props.doDownload()}}></i></Col>
                <Col><i className="fas fa-volume-up" onClick={()=>{this.props.doSpeak()}}></i></Col>                
                </Row>
            )
        }
    }
    render() {
        let chatVo: ChatVo = this.props.chatVo;
        let variant: string = "light";
        if (chatVo.role === "assistant") {
            variant = "success";
        }

        return (<ListGroupItem variant={variant} className="chatLine">
            <Row>
            <Col>{chatVo.content}</Col>
            </Row>
            {this.getIconBar()}           
            </ListGroupItem>)
    }
}
export default ChatBar;