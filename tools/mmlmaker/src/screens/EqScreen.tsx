
import { MathJax } from 'better-react-mathjax';
import React, { Component } from 'react';

interface MyProps { }
interface MyState {text:string}
class EqScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {text:"2^2"};
        }
    componentDidMount(): void {
        let mjax=(window as any).MathJax;
        console.log(mjax);
        
    }
    convertLatexToMML(latex:string)
    {
        //don't use mathjax

        
    }
    makeMML()
    {
      //  return  <MathJax>{"`frac(10)(4x) approx 2^(12)`"}</MathJax>
       //return <MathJax>{"\\(\\frac{10}{4x} \\approx 2^{12}\\)"}</MathJax>
       return <MathJax>{this.state.text}</MathJax>
       //return  <MathJax>{"`frac(10)(4x) approx 2^(12)`"}</MathJax>
       
       //return <MathJax>{`\\$${this.state.text}$\\`}</MathJax>
    }
    render() {
        return (<div>
            <input type="text" onChange={(e)=>{this.setState({text:e.target.value})}} value={this.state.text} />
            {this.makeMML()}
        </div>)
    }
}
export default EqScreen;