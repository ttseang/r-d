import { MathJaxContext } from 'better-react-mathjax';
import './App.css';
import BaseScreen from './screens/BaseScreen';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const config = {
    loader: { load: ["[tex]/html"] },
    tex: {
      packages: { "[+]": ["html"] },
      inlineMath: [
        ["$", "$"],
        ["\\(", "\\)"]
      ],
      displayMath: [
        ["$$", "$$"],
        ["\\[", "\\]"]
      ]
    }
  };
  return (
    <MathJaxContext config={config} version={3}>
    <div className="App">
      <BaseScreen />
    </div>
    </MathJaxContext>
  );
}

export default App;
