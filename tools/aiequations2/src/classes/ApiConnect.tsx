//import { MainStorage } from "./MainStorage";

export class ApiConnect
{
   // private ms:MainStorage=MainStorage.getInstance();
    public static GET_IDEA:number=100;
   
    private params:string[]=[];

    
    public sendTT(messages:string, callback:Function, errorCallback:Function)
    {
        const url:string="https://customtrack.org/api/davinci.php";
        
        //make post variables
        
        const data = new FormData();
        //set to no cors
        data.append('cors', 'no');
        data.append('chat', messages);
        fetch(url, {
            method: 'POST',
            body: data
        }).then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            callback(data);
        }).catch((error) => {
            console.log(error);
            console.error('Error:', error);
           // errorCallback(error);
        });
    }
    public getSteps(prompt:string,callback:Function, errorCallback:Function)
    {
        const url:string="https://customtrack.org/api/davinci.php";
        
        //make post variables

        const data = new FormData();
        //set to no cors
        data.append('cors', 'no');
        data.append('prompt',prompt);
        fetch(url, {
            method: 'POST',
            body: data
        }).then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            callback(data);
        }).catch((error) => {
            console.log(error);
            console.error('Error:', error);
              // errorCallback(error);
        });
    }
    public clear()
    {
        this.params=[];
    }
    public addParam(data:string)
    {
        this.params.push(data);
    }
}
export default ApiConnect;
/**
 * 900 getbuildercats
901 getbuilderclosings
902 getbuilderopens
903 getbuildersents
904 getbuildersubcats
905 getrandbuildersent
906 addbuildercat
907 addbuildersent
908 addbuildersubcat
 */