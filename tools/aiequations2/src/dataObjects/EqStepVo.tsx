export class EqStepVo {
    public instruction: string;
    public lines: string[];
    public carry:string;
    constructor(instruction: string, lines: string[],carry:string) {
        this.instruction = instruction;
        this.lines = lines;
        this.carry=carry;
    }
}