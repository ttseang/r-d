import React, { Component } from 'react';
import { ButtonGroup } from 'react-bootstrap';
import { EqStepVo } from '../dataObjects/EqStepVo';
interface MyProps { steps: EqStepVo[] }
interface MyState { steps: EqStepVo[], currentStep: number }
class EquationScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { steps: props.steps, currentStep: 0 };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (this.props.steps !== prevProps.steps) {
            this.setState({ steps: this.props.steps });
        }
    }
    getGrid() {
        let step: EqStepVo = this.state.steps[this.state.currentStep];
        let grid: JSX.Element[][] = [];
        //add the carry string
        let carryRow: JSX.Element[] = [];
        for (let i = 0; i < step.carry.length; i++) {
            let char: string = step.carry[i];
            if (char === "@") {
                char = " ";
            }
            let key: string = "carry_" + i;
            carryRow.push(<td key={key}>{char}</td>);
        }
        grid.push(carryRow);
        //add the lines
        for (let i = 0; i < step.lines.length; i++) {
            let line: string = step.lines[i];

            console.log(line);
            let row: JSX.Element[] = [];

            for (let j = 0; j < line.length; j++) {
                let char: string = line[j];
                if (char === "@") {
                    char = " ";
                }
                let key: string = i + "_" + j;
                row.push(<td key={key}>{char}</td>);
               
                
            }
            grid.push(row);
             //if the line starts with an operator, add a line of dashes
            if (line[0] === "+" || line[0] === "-") {
                let dashRow: JSX.Element[] = [];
                for (let j = 0; j < line.length; j++) {
                    let key: string = "dash_" + j;
                    dashRow.push(<td key={key}>-</td>);
                }
                grid.push(dashRow);
            }

           
        }
        let jsx: JSX.Element[] = [];
        //add the tbody
        for (let i = 0; i < grid.length; i++) {
            let key: string = "row_" + i;
            jsx.push(<tr key={key}>{grid[i]}</tr>);
        }
        return <table><tbody>{jsx}</tbody></table>;
    }
    prev() {
        let currentStep: number = this.state.currentStep;
        if (currentStep > 0) {
            currentStep--;
            this.setState({ currentStep: currentStep });
        }

    }
    next() {
        let currentStep: number = this.state.currentStep;
        if (currentStep < this.state.steps.length - 1) {
            currentStep++;
            this.setState({ currentStep: currentStep });
        }

    }
    getButtons() {
        let buttons: JSX.Element[] = [];
        buttons.push(<button key="prev" onClick={() => this.prev()}>Prev</button>);
        buttons.push(<button key="next" onClick={() => this.next()}>Next</button>);
        return <ButtonGroup>{buttons}</ButtonGroup>
    }
    render() {
        return (<div>{this.getGrid()}
            <hr />
            {this.getButtons()}
        </div>)
    }
}
export default EquationScreen;