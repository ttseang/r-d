
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    
    constructor() {
        super("SceneMain");
        
    }
    preload() {
        this.load.image("holder", "assets/holder.jpg");
        this.load.image("face","assets/face.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.show();
       
        this.placeImage("face",60,0.1);
    }
   
    update() {
       
    }
}
export default SceneMain;