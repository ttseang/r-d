import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { MainStorage } from '../classes/MainStorage';
import SceneMain from '../scenes/SceneMain';
interface MyProps { }
interface MyState { }
class LayoutScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
        componentDidMount()
        {
            const config: any = {
                mode: Phaser.AUTO,
                width: 640,
                height: 480,
                parent: 'phaser-game',
                scene: [SceneMain]
              }
              this.ms.gameConfig = config;
          
              new Phaser.Game(config);
        }
    render() {
        return (<div>
            <Row><Col>
            <div className="tac">
            <div id="phaser-game">
            </div></div></Col>
            </Row>
        </div>)
    }
}
export default LayoutScreen;