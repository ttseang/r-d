export class ButtonVo
{
    public text:string;
    public variant:string;
    public action:number;

    constructor(text:string,variant:string,action:number)
    {
        this.text=text;
        this.variant=variant;
        this.action=action;
    }
}