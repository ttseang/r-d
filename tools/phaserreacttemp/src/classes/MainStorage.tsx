


export class MainStorage {
    public sent: string;
    private static instance: MainStorage;
    public gameConfig:any;
    //
    //
    
    public selectedFile:number=0;
    public prevData:string="";
    
    public makeNewFlag:boolean=true;
    public prevMode:boolean=false;

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        this.sent="";
    }
    static getInstance(): MainStorage {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new MainStorage();
        }
        return this.instance;
    }
}
export default MainStorage;