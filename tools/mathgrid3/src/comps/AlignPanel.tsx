import React, { Component } from 'react';
interface MyProps {}
interface MyState {}
class AlignPanel extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
this.state={};
}
render()
{
    //make align image buttons
    //align top
    //align middle
    //align bottom
    //align left
    //align center
    //align right

return (<div>
    <button className='cellButton'>
        <i className="fas fa-align-left"></i>
    </button>
    <button className='cellButton'>
        <i className="fas fa-align-center"></i>
    </button>
    <button className='cellButton'>
        <i className="fas fa-align-right"></i>
    </button>
    <button className='cellButton'>
        <i className="fas fa-align-top"></i>
    </button>
    <button className='cellButton'>
        <i className="fas fa-align-middle"></i>
    </button>
    <button className='cellButton'>
        <i className="fas fa-align-bottom"></i>
    </button>


</div>)
}
}
export default AlignPanel;