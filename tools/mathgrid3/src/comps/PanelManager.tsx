import React, { Component } from 'react';
import { PanelType } from '../mc/Constants';
import DragPanel from './DragPanel';
import { PanelPosVo } from '../dataObjs/PanelPosVo';
import { MainController } from '../mc/MainController';
import { MainStorage } from '../mc/MainStorage';
interface MyProps { }
interface MyState { panels: JSX.Element[] }
class PanelManager extends Component<MyProps, MyState>
{
    public panelMap: Map<string, PanelPosVo> = new Map<string, PanelPosVo>();
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { panels: [] };
        this.mc.addPanel=this.addPanel.bind(this);
        this.mc.removePanel=this.removePanel.bind(this);
       
    }
    componentDidMount(): void {
        this.loadPanelPosFromStorage();
        this.reopenPanels();
    }
    removePanel(panelType: PanelType) {
        let panels: JSX.Element[] = this.state.panels.slice();
        let key: string = 'panel' + panelType.toString();

        let panelPos: PanelPosVo = this.getPanelPos(panelType);
        panelPos.open = false;
        this.updatePanelPos(panelPos.type, panelPos.posX, panelPos.posY, panelPos.open);

        for (let i: number = 0; i < panels.length; i++) {
            if (panels[i].key === key) {
                panels.splice(i, 1);
                this.setState({ panels: panels });
                return;
            }
        }

    }
    reopenPanels() {
        let panels: JSX.Element[] = [];
        let panelVo: PanelPosVo[] = this.getAllOpenPanels();
        // ////////console.log(panelVo);
        for (let i: number = 0; i < panelVo.length; i++) {
            panels.push(this.getPanel(panelVo[i]));
        }
        this.setState({ panels: panels });
    }
    addPanel(panelType: string) {
        let panels: JSX.Element[] = this.state.panels.slice();
        let key: string = 'panel' + panelType;
        ////////console.log('add panel ' + key);
        //if the panel already exists then return
        for (let i: number = 0; i < panels.length; i++) {
            if (panels[i].key === key) {
                return;
            }
        }
        let panelPos: PanelPosVo = this.getPanelPos(panelType);
        panelPos.open = true;
        let panel: JSX.Element = this.getPanel(panelPos);
        panels.push(panel);
        this.setState({ panels: panels });
    }
    getPanel(panelPos: PanelPosVo) {
        let key: string = 'panel' + panelPos.type;
        let width: number = 30;
        let height: number = 30;

        switch (panelPos.type) {
            case PanelType.adjust:
            case PanelType.move:
            case PanelType.insert:
            case PanelType.delete:
            case PanelType.shift:
            case PanelType.templates:
            case PanelType.colors:
            case PanelType.scratchpad:
                height = 40;
                break;

            case PanelType.lines:
            case PanelType.clipboard:

                height = 50;
        }
        let index: number = this.state.panels.length;
        this.updatePanelPos(panelPos.type, panelPos.posX, panelPos.posY, true);
        //<DragPanel posX={20} posY={20} width={30} height={30} type={PanelType.colors}></DragPanel>
        let panel: JSX.Element = <DragPanel key={key} posX={panelPos.posX} posY={panelPos.posY} width={width} height={height} type={panelPos.type} callback={this.panelSelect.bind(this)} index={index} updateCallback={this.updatePanelPos.bind(this)}></DragPanel>;
        return panel;
    }
    panelSelect(panelType: PanelType) {
        let panels: JSX.Element[] = this.state.panels.slice();
        let key: string = 'panel' + panelType;
        let index: number = -1;
        //if the panel already exists then return
        for (let i: number = 0; i < panels.length; i++) {
            if (panels[i].key === key) {
                index = i;
                break;
            }
        }
        //move the panel to the end of the array
        if (index > -1) {
            let panel: JSX.Element = panels[index];
            panels.splice(index, 1);
            panels.push(panel);
            this.setState({ panels: panels });
        }


    }
    updatePanelPos(type: string, posX: number, posY: number, open: boolean = false) {
        //check if the panel is already in the map
        let panelPos: PanelPosVo | undefined = this.panelMap.get(type);
        if (panelPos) {
            panelPos.posX = posX;
            panelPos.posY = posY;
            panelPos.open = open;
        }
        else {
            panelPos = new PanelPosVo(type, posX, posY, open);
            this.panelMap.set(type, panelPos);
        }
        //save the panel map to local storage
        //get only the value from the map
        let panelMapString: string = JSON.stringify(Array.from(this.panelMap.values()));
        localStorage.setItem("panelMap", panelMapString);
    }
    getAllOpenPanels(): PanelPosVo[] {
        let panels: PanelPosVo[] = [];
        this.panelMap.forEach((panel: PanelPosVo) => {
            if (panel.open) {
                panels.push(panel);
            }
        });
        return panels;
    }
    getPanelPos(type: string): PanelPosVo {

        let panelPos: PanelPosVo | undefined = this.panelMap.get(type);
        if (!panelPos) {
            panelPos = new PanelPosVo(type, 100, 100, true);
            this.panelMap.set(type, panelPos);
        }
        return panelPos;

    }
    loadPanelPosFromStorage() {
        let panelMapString: string | null = localStorage.getItem("panelMap");
        if (panelMapString) {
            let panelMap: PanelPosVo[] = JSON.parse(panelMapString);
            for (let i: number = 0; i < panelMap.length; i++) {
                this.panelMap.set(panelMap[i].type, panelMap[i]);
            }
        }
    }
    render() {
        return (<div>
            {this.state.panels}
        </div>)
    }
}
export default PanelManager;