import React, { Component } from 'react';
import Layout from './Layout';
import TopMenu from './TopMenu';
import PanelManager from './PanelManager';
interface MyProps { }
interface MyState { }
class Editor extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div>
                <TopMenu />
                <PanelManager />
                <div className='editor'>
                   
                    <div className="gridHolder">
                        <Layout active={true} />
                    </div>
                </div></div>)
    }
}
export default Editor;