import React, { Component } from 'react';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
import { MainController } from '../mc/MainController';
import { MainStorage } from '../mc/MainStorage';
import { PanelType } from '../mc/Constants';

interface MyProps { }
interface MyState { }
class TopMenu extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    private doAction(menuIndex: number, actionIndex: number) {
        
        switch (actionIndex) {
            case 0:
                
                localStorage.setItem("saveData", "");
                (window as any).location.reload();
                break;

            case 2:
                
               // this.mc.changeScreen(2);
                break;
            case 1:
                
              //  this.mc.changeScreen(1);
                break;
        }
    }
    clickCheck(e: React.ChangeEvent<HTMLInputElement>) {
       // let checked: boolean = e.target.checked;
       
      //  this.ms.applyAdjustToCell = checked;
       
    }
    showOverlays()
    {
      //  this.mc.addPanel(PanelType.overlays);
      //  this.mc.toggleOverlay();
    }
    render() {
        return (<div className='topMenu'>
            <ButtonGroup>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        File
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => { this.doAction(0, 0) }}>New</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 1) }}>Save</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 2) }}>Open</Dropdown.Item>
                    </Dropdown.Menu>
                    <button className='btnEye' onClick={() => {this.mc.toggleGrid()}}><i className="fas fa-eye"></i></button>
                    <button className='btnEye' onClick={() => { }}><i className="fas fa-grip-horizontal"></i></button>
                    <button className='btnEye' onClick={() => { }}><i className="fas fa-grip-vertical"></i></button>
                    <button className='btnEye' onClick={() => { }}><i className="fas fa-border-all"></i></button>
                    <button className='btnEye' title='symbols' onClick={() => { this.mc.addPanel(PanelType.symbols) }}><i className="fas fa-square-root-alt"></i></button>
                    <button className='btnEye' title='overlays' onClick={() => { this.mc.addPanel(PanelType.overlays) }}><i className="fas fa-layer-group"></i></button>
                    
                    <button className='btnEye' title='line' onClick={() => { this.mc.addLine() }}><i className="fas fa-ruler-horizontal"></i></button>
                    <button className='btnEye' title='text' onClick={() => { this.mc.addPanel(PanelType.text) }}><i className="fas fa-font"></i></button>
                    <span>||</span>
                    
                </Dropdown>
                <Dropdown>
                </Dropdown>
            </ButtonGroup>
        </div>)
    }
}
export default TopMenu;