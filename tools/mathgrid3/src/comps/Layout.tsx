import React, { Component } from 'react';

import { MainController } from '../mc/MainController';
import ElementVo from '../dataObjs/ElementVo';
import { GridConfig } from '../mc/GridConfig';
interface MyProps { active: boolean; }
//xx and yy are percentages it is the cursor position on the grid

interface MyState { active: boolean, elements: ElementVo[], startX: number, startY: number, currentX: number, currentY: number, dragSelect: boolean, xx: number, yy: number,showGrid:boolean }
class ImageOverlay extends Component<MyProps, MyState>
{

    private moveListener: any = this.mouseMove.bind(this);
    private mouseUpListener: any = this.mouseUp.bind(this);
    private keyListener: any = this.onKeyDown.bind(this);
    private upKeyListener: any = this.onKeyUp.bind(this);
    private mc: MainController = MainController.getInstance();
    private textLocked: boolean = false;


    constructor(props: MyProps) {
        super(props);

        let startElements: ElementVo[] = [];
     //   startElements.push(new ElementVo("image", 10, 10, 10, 10, "./overlay/square-root.svg"));
     //   startElements.push(new ElementVo("line", 25, 25, 10, 10, ""));
    //    startElements.push(new ElementVo("line", 30, 60, 10, 0, ""));
        //text elements
        // startElements.push(new ElementVo("text",5,5,10,10,"hello"));

        // startOverlays.push(new OverlayVo(0,0,10,10,"square-root"));

        this.state = { active: this.props.active, elements: startElements, startX: -1, startY: -1, currentX: 0, currentY: 0, dragSelect: false, xx: 0, yy: 0,showGrid:true };

        this.mc.addImage = this.addImage.bind(this);

        document.addEventListener("mouseup", this.mouseUpListener);
        // document.addEventListener("mousemove", this.moveListener);
        document.addEventListener("keydown", this.keyListener);
        document.addEventListener("keyup", this.upKeyListener);

        this.mc.setText = this.addTextLine.bind(this);
        this.mc.addOverlay = this.addImage.bind(this);
        this.mc.addLine=this.addLine.bind(this);
        this.mc.toggleGrid=this.toggleGrid.bind(this);
        this.mc.setTextLock=this.setTextLock.bind(this);
    }
    componentDidMount(): void {
        // this.addTextLine("hello");
        //add maths formula
        this.setPosition(5, 10, this.initTestData.bind(this));
        let gridElement: HTMLElement | null = document.getElementsByClassName("gridLayout")[0] as HTMLElement;
        if (gridElement !== null) {
            GridConfig.setGridSize(gridElement);
        }
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.active !== this.props.active) {
            this.setState({ active: this.props.active });
        }
    }
    setTextLock(locked:boolean){

        if (locked) {
            this.textLocked = true;
            document.removeEventListener("keydown", this.keyListener);
            document.removeEventListener("keyup", this.upKeyListener);
            document.removeEventListener("mouseup", this.mouseUpListener);
        }
        else {
            this.textLocked = false;
            document.removeEventListener("keydown", this.keyListener);
            document.removeEventListener("keyup", this.upKeyListener);           


             document.addEventListener("keydown", this.keyListener);
             document.addEventListener("keyup", this.upKeyListener);
                document.addEventListener("mouseup", this.mouseUpListener);
        }
    }
    toggleGrid() {
        this.setState({showGrid:!this.state.showGrid});
    }
    onKeyDown(e: React.KeyboardEvent) {
        console.log("key down");
        if (this.textLocked) {
            return;
        }
        this.setState({ dragSelect: e.shiftKey });
        let key: string = e.key;
        if (key === "Backspace") {
            //console.log("backspace");
            return;
        }
        if (key === "Enter") {
            //this.addTextLine("\n");
            this.setState({ yy: this.state.yy + GridConfig.cellHeight, xx: 0 });
            return;
        }
        if (key === "ArrowLeft") {
            this.setState({ xx: this.state.xx - GridConfig.cellWidth });
            return;
        }
        if (key === "ArrowRight") {
            this.setState({ xx: this.state.xx + GridConfig.cellWidth });
            return;
        }
        //tab
        if (key === "Tab") {
            e.preventDefault();
            let xx: number = this.state.xx;
            xx += GridConfig.cellWidth * 4;
            if (xx>100) 
            {
                xx=0;
            }
            this.setState({ xx: xx });
            return;
        }

        if (key === "ArrowUp") {
            this.setState({ yy: this.state.yy - GridConfig.cellHeight });
            return;
        }
        if (key === "ArrowDown") {
            this.setState({ yy: this.state.yy + GridConfig.cellHeight });
            return;
        }
        if (key === "Escape") {
            this.setState({ xx: 0, yy: 0 });
            return;
        }
        if (key === "Delete") {
            //delete the current element
            this.deleteSelected();
            return;
        }
        //if the key contains an f then we presume it is a function
        if (key.toLowerCase().indexOf("f") > -1 && key.length === 2) {
            return;
        }
        if (key.toLowerCase()==="shift" || key.toLowerCase()==="control" || key.toLowerCase()==="alt") {
            return;
        }
        //if we get here we have a letter or number
        this.addTextLine(key);

    }
    onKeyUp(e: React.KeyboardEvent) {
        this.setState({ dragSelect: false });
    }
    deleteSelected() {
        let elements: ElementVo[] = this.state.elements;
        let newElements: ElementVo[] = [];
        for (let i: number = 0; i < elements.length; i++) {
            let element: ElementVo = elements[i];
            if (!element.selected) {
                newElements.push(element);
            }
        }
        this.setState({ elements: newElements });
    }
    initTestData() {
       // this.addTextLine("x^2+2x+1=0");
    }
    setPosition(x: number, y: number, callback: Function) {
        //get the cell position at x,y and convert to %

        x=Math.floor(x/5)*5;
        y=Math.floor(y/5)*5;
        x++;
        y++;

        let x2: number = x * GridConfig.cellWidth;
        let y2: number = y * GridConfig.cellHeight;

      /*   x2 += GridConfig.cellWidth / 4;
        y2 -= GridConfig.cellHeight / 4; */

        this.setState({ xx: x2, yy: y2 }, () => {
            callback();
        });
    }
    addLine()
    {
        let elements: ElementVo[] = this.state.elements;
        elements.push(new ElementVo("line", 5, 5, 5, 0, ""));
        this.setState({ elements: elements });
    }
    addImage(path: string) {
        let elements: ElementVo[] = this.state.elements;
        elements.push(new ElementVo("image", 0, 0, 10, 10, path));
        this.setState({ elements: elements });
    }
    addSingleText(text: string) {
        let elements: ElementVo[] = this.state.elements;
        elements.push(new ElementVo("text", 0, 0, 100, 100, text));
        this.setState({ elements: elements });
    }
    addTextLine(text: string) {
        let textArray: string[] = text.split("");
        let elements: ElementVo[] = this.state.elements;

        let xx: number = this.state.xx;
        let yy: number = this.state.yy;

        xx=Math.floor(xx/5)*5;
        yy=Math.floor(yy/5)*5;

        xx+=2.5;    



        let x2: number = xx;

        for (let i: number = 0; i < textArray.length; i++) {
            // //////console.log("add text "+textArray[i]);
            if (textArray[i] === "\n") {
                xx = x2;
                yy += GridConfig.cellHeight;
                continue;
            }
            elements.push(new ElementVo("text", xx, yy-GridConfig.cellHeight/2, GridConfig.cellWidth / 2, GridConfig.cellHeight / 2, textArray[i]));
            xx += GridConfig.cellWidth;
            if (xx > 100) {
                xx = GridConfig.cellWidth / 4;
                yy += GridConfig.cellHeight;
            }
        }
        this.setState({ elements: elements, xx: xx, yy: yy });
    }
    getSelectCount()
    {
        let count:number=0;
        for (let i: number = 0; i < this.state.elements.length; i++) {
            if (this.state.elements[i].selected === true) {
                count++;
            }
        }
        return count;
    }
    overlayClick(index: number) {
        if (this.state.dragSelect===false)
        {
            if (this.getSelectCount()<2)
            {
                this.unSelectAll();
            }
           
        }
        let element: ElementVo = this.state.elements[index];
        element.selected = true;
        this.setState({ elements: this.state.elements });
        document.addEventListener("mousemove", this.moveListener);
    }
    mouseMove(e: React.MouseEvent<SVGSVGElement, MouseEvent>) {
        //  if (e.target === null) return;
        (window as any).getSelection().removeAllRanges();

        if (this.state.dragSelect === false) {
            let parentObj: any = (e.target as any).parentElement;
            if (parentObj === null) return;
            let parentRect = parentObj.getBoundingClientRect();
            let moveX: number = e.movementX;
            let moveY: number = e.movementY;
            for (let i: number = 0; i < this.state.elements.length; i++) {
                let currentElement: ElementVo = this.state.elements[i];

                if (currentElement.selected === false) continue;
                let x: number = currentElement.x + (moveX / parentRect.width * 100);
                let y: number = currentElement.y + (moveY / parentRect.height * 100);

                if (x < 0) x = 0;
                if (x > 100 - currentElement.width) x = 100 - currentElement.width;
                if (y < 0) y = 0;
                if (y > 100 - currentElement.height) y = 100 - currentElement.height;

                currentElement.x = x;
                currentElement.y = y;
            }

            this.setState({ elements: this.state.elements });
        }
        else {
            let parentObj: any = (e.target as any).parentElement;
            if (parentObj === null) return;
            let parentRect = parentObj.getBoundingClientRect();

            let currentX: number = e.clientX - parentRect.left;
            let currentY: number = e.clientY - parentRect.top;

            //convert to %
            currentX = currentX / parentRect.width * 100;
            currentY = currentY / parentRect.height * 100;


            this.setState({ currentX: currentX, currentY: currentY });
        }
    }
    mouseUp(e: React.MouseEvent<SVGSVGElement, MouseEvent>) {
        (window as any).getSelection().removeAllRanges();
        let target: any = e.target;
        //check for drag
        if (this.state.dragSelect === true) {
            let layoutEl: any | null = document.getElementsByClassName("gridLayout")[0];
            if (layoutEl === null) return;

            let parentObj: any = (e.target as any).parentElement;
            if (parentObj === null) return;
            let parentRect = parentObj.getBoundingClientRect();

            let endX: number = e.clientX - parentRect.left;
            let endY: number = e.clientY - parentRect.top;

            //convert to %
            endX = endX / parentRect.width * 100;
            endY = endY / parentRect.height * 100;

            //select all elements in between start and end
            let startX: number = this.state.startX;
            let startY: number = this.state.startY;

            if (startX > endX) {
                let temp: number = startX;
                startX = endX;
                endX = temp;
            }
            if (startY > endY) {
                let temp: number = startY;
                startY = endY;
                endY = temp;
            }

            for (let i: number = 0; i < this.state.elements.length; i++) {
                let el: ElementVo = this.state.elements[i];
                if (el.x > startX && el.x < endX && el.y > startY && el.y < endY) {
                    el.selected = true;
                } else {
                    el.selected = false;
                }
            }
        }
        else {
            console.log(target.id);
            //if the id doesn't include the word element, then unselect all
            if (target.id.indexOf("element") === -1)
            {
                this.unSelectAll();
            }
           
        }

        this.setState({ elements: this.state.elements, startX: -1, startY: -1, currentX: -1, currentY: -1 });

        document.removeEventListener("mousemove", this.moveListener);

        // document.removeEventListener("mouseup", this.mouseUpListener);
    }
    unSelectAll() {
        for (let i: number = 0; i < this.state.elements.length; i++) {
            let el: ElementVo = this.state.elements[i];
            el.selected = false;
        }
    }
    getElements() {
        let elements: JSX.Element[] = [];
        for (let i = 0; i < this.state.elements.length; i++) {
            let key: string = "elements" + i;
            elements.push(this.state.elements[i].getHtml(i, key, this.overlayClick.bind(this)));
        }
        return elements;
    }
    getGridLines() {
        if (this.state.showGrid === false) return null;
        let rows: number = GridConfig.gridRows;
        let cellHeight: number = GridConfig.cellHeight;

        let cols: number = GridConfig.gridCols;
        let cellWidth: number = GridConfig.cellWidth;


        let lines: JSX.Element[] = [];

        //make horizontal lines
        for (let i = 0; i < rows; i++) {
            let y: number = i * cellHeight;
            let key: string = "h" + i;
            lines.push(<line key={key} x1="0" y1={y} x2="100" y2={y} stroke="black" strokeWidth="0.1" />);
        }
        //make vertical lines
        for (let i = 0; i < cols; i++) {
            let x: number = i * cellWidth;
            let key: string = "v" + i;
            lines.push(<line key={key} x1={x} y1="0" x2={x} y2="100" stroke="black" strokeWidth="0.1" />);
        }
        return lines;
    }
    onCellClick(e: React.MouseEvent<SVGSVGElement, MouseEvent>) {

        let target: any = e.target;
       // console.log(target);

        if (this.state.active) {
            let rect: any = (e.target as any).getBoundingClientRect();
            let startX: number = e.clientX - rect.left;
            let startY: number = e.clientY - rect.top;

            let gridPos = GridConfig.getGridCell(startX, startY);
            let xx: number = gridPos.x * GridConfig.cellWidth;
            let yy: number = gridPos.y * GridConfig.cellHeight;

            let startPers: any = GridConfig.convertToPercentages(startX, startY);
            let startXPer = startPers.x;
            let startYPer = startPers.y;

            if (target.id.indexOf("element") !== -1 || this.state.dragSelect === true) {
                xx = this.state.xx;
                yy = this.state.yy;
            }

           // console.log("update xx");
            this.setState({ startX: startXPer, startY: startYPer, currentX: startXPer, currentY: startYPer, xx: xx, yy: yy });
            document.addEventListener("mousemove", this.moveListener);
        }
    }
    getDragRect() {

        if (this.state.dragSelect === false) return null;

        let startX: number = this.state.startX;
        let startY: number = this.state.startY;
        let currentX: number = this.state.currentX;
        let currentY: number = this.state.currentY;

        if (startX > currentX) {
            let temp: number = startX;
            startX = currentX;
            currentX = temp;
        }
        if (startY > currentY) {
            let temp: number = startY;
            startY = currentY;
            currentY = temp;
        }

        let height: number = currentY - startY;
        let width: number = currentX - startX;

        if (this.state.startX === -1 || this.state.startY === -1 || this.state.currentX === -1 || this.state.currentY === -1) {
            height = 0;
            width = 0;
        }

        let startXPercent: string = startX + "%";
        let startYPercent: string = startY + "%";

        return (<rect x={startXPercent} y={startYPercent} width={width} height={height} fill="rgba(0,0,0,0.2)" stroke="black" strokeWidth="0.1" />);
    }

    getCurrentSquare() {

        let gridX: number = Math.floor(this.state.xx /GridConfig.cellWidth)* GridConfig.cellWidth;
        let gridY: number = (Math.floor(this.state.yy / GridConfig.cellHeight)-1) * GridConfig.cellHeight;

        /* console.log("gridX", gridX);
        console.log("gridY", gridY); */


        let width: number = GridConfig.cellWidth;
        let height: number = GridConfig.cellHeight;
        return <rect x={gridX} y={gridY} width={width} height={height} fill="yellow" stroke="black" strokeWidth="0.1" />;
    }
    render() {

        //set pointer events to none if not active or pointer events to all if active
        let pointerEvents = this.state.active ? "all" : "none";
        let style: any = { "pointerEvents": pointerEvents };
        //background color
        //light green if active, transparent if not
        style.backgroundColor = this.state.active ? "rgba(0,255,0,0.2)" : "transparent";
        return (
            <svg width="100%" height="100%" style={style} viewBox="0 0 100 100" preserveAspectRatio="none" className='gridLayout' onMouseDown={this.onCellClick.bind(this)}>
                {this.getCurrentSquare()}
                {this.getGridLines()}
                {this.getElements()}
                {this.getDragRect()}
            </svg>
        )
    }
}
export default ImageOverlay;