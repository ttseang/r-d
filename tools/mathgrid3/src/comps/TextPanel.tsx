import React, { Component } from 'react';
import { MainController } from '../mc/MainController';
import { Button } from 'react-bootstrap';
interface MyProps { }
interface MyState { text: string }
class TextPanel extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = { text: '' };
        }
    onTextFocus() {
        this.mc.setTextLock(true);
    }
    onTextBlur() {
        this.mc.setTextLock(false);
    }
    onTextAreaChange(e: any) {
        this.setState({ text: e.target.value });
    }
    setTextAsSvg()
    {
        this.mc.setText(this.state.text);
    }
    render() {
        return (<div className='textPanel'>
            <textarea className='textArea' value={this.state.text} onChange={(e) => { this.onTextAreaChange(e) }} onFocus={() => { this.onTextFocus() }} onBlur={() => { this.onTextBlur() }} />
            <Button onClick={this.setTextAsSvg.bind(this)}>OK</Button>
        </div>)
    }
}
export default TextPanel;