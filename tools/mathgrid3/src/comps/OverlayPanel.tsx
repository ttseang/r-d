import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState { }
class OverlayPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private overlays: string[] = [];
    constructor(props: MyProps) {
        super(props);
        this.state = {};
        this.overlays.push('square-root');
        this.overlays.push('square-root2');
        this.overlays.push('arrow-left');
        this.overlays.push('arrow-right');
        this.overlays.push('arrow-up');
        this.overlays.push('arrow-down');
    }

    getButtons() {
        let overlays: string[] = this.overlays;
        let buttons: JSX.Element[] = [];
        for (let i = 0; i < overlays.length; i++) {
            let key: string = 'symbol_' + overlays[i];
            let src: string = 'overlay/' + overlays[i] + '.svg';
            buttons.push(<button key={key} className='cellButton' onClick={() => {
                this.mc.addOverlay(src);
            }}>
                <img src={src} className='overlayIcon' alt={overlays[i]} />
            </button>)
        }
        return buttons;
    }
    render() {
        return (<div className='symButtons'>
            {this.getButtons()}
        </div>)
    }
}
export default OverlayPanel;