import React, { Component } from 'react';
import Editor from '../comps/Editor';
import { GridConfig } from '../mc/GridConfig';
interface MyProps { }
interface MyState {mode:number}
class BasceScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {mode:0}

            //this is to init the grid config values
            new GridConfig();
        }

    getScreen()
    {
        switch(this.state.mode)
        {
            case 0:
                return <Editor/>
        }
    }
    render() {
        return (<div>
            {this.getScreen()}
        </div>)
    }
}
export default BasceScreen;