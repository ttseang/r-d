//type,x,y,width,height,data(either path or text)
export default class ElementVo
{
    type:string;
    x:number;
    y:number;
    width:number;
    height:number;
    data:string;
    selected:boolean=false;
    constructor(type:string,x:number,y:number,width:number,height:number,data:string)
    {
        this.type=type;
        this.x=x;
        this.y=y;
        this.width=width;
        this.height=height;
        this.data=data;
    }
    getHtml(index:number,key:string,clickHandler:Function)
    {
        //return the svg element needed to display this element
        //with the click handler
        switch(this.type)
        {
            case "line":
            return <line key={key} id={key} x1={this.x+"%"} y1={this.y+"%"} x2={(this.x+this.width)+"%"} y2={(this.y+this.height)+"%"} stroke="black" strokeWidth=".5" onMouseDown={()=>{clickHandler(index)}}></line>
           
            case "image":

            return <image href={this.data} key={key} id={key} x={this.x+"%"} y={this.y+"%"} width={this.width+"%"} height={this.height+"%"} onMouseDown={()=>{clickHandler(index)}}></image>

            case "text":
            let fill:string="black";
            if (this.selected)
            {
                fill="red";
            }
            return <text key={key} id={key} fill={fill} fontSize="5px" x={this.x+"%"} y={this.y+"%"} textAnchor="middle" dominantBaseline="middle" onMouseDown={()=>{clickHandler(index)}}>{this.data}</text>

        }
        return <div></div>
    }
}