
export class PanelPosVo
{
    public posX:number=0;
    public posY:number=0;
    public type:string;
    public open:boolean=false;
    constructor(type:string,posX:number,posY:number,open:boolean=false)
    {
        this.posX=posX;
        this.posY=posY;
        this.type=type;
        this.open=open;
    }
}