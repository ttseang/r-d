export class GridConfig {
    public static gridRows: number;
    public static gridCols: number;
    public static cellWidth: number;
    public static cellHeight: number;
    public static gridWidth: number;
    public static gridHeight: number;
    public static pxCellWidth: number;
    public static pxCellHeight: number;
    constructor() {
        GridConfig.gridRows = 20;
        GridConfig.gridCols = 20;
        GridConfig.cellWidth = 100 / GridConfig.gridCols;
        GridConfig.cellHeight = 100 / GridConfig.gridRows;
        GridConfig.gridWidth = 100;
        GridConfig.gridHeight = 100;
        GridConfig.pxCellWidth = 0;
        GridConfig.pxCellHeight = 0;

      //  console.log("cellWidth " + GridConfig.cellWidth + " cellHeight " + GridConfig.cellHeight);
    }
    public static setGridSize(element: HTMLElement) {
        //get rect of element
        let rect: DOMRect = element.getBoundingClientRect();
        GridConfig.gridWidth = rect.width;
        GridConfig.gridHeight = rect.height;
        GridConfig.pxCellWidth = GridConfig.gridWidth / GridConfig.gridCols;
        GridConfig.pxCellHeight = GridConfig.gridHeight / GridConfig.gridRows;
        // console.log("gridWidth "+GridConfig.gridWidth+" gridHeight "+GridConfig.gridHeight);
    }
    public static convertToPercentages(x: number, y: number) {
      //  console.log("convertToPercentages " + x + " " + y);
        //convert x,y to percentages
        let xPercent: number = (x / GridConfig.gridWidth) * 100;
        let yPercent: number = (y / GridConfig.gridHeight) * 100;
     //   console.log("xPercent " + xPercent + " yPercent " + yPercent);
        return { x: xPercent, y: yPercent };
    }
    public static convertToPixels(x: number, y: number) {
        //convert x,y to percentages
        let xPixels: number = (x / 100) * GridConfig.gridWidth;
        let yPixels: number = (y / 100) * GridConfig.gridHeight;
        return { x: xPixels, y: yPixels };

    }
    public static getGridCell(x: number, y: number) {

        //convert x,y to percentages
        let pers = GridConfig.convertToPercentages(x, y);
        let xPercent: number = pers.x;
        let yPercent: number = pers.y;

        //get grid cell
        let gridCellX: number = Math.floor(xPercent / GridConfig.cellWidth) + 0.25;
        let gridCellY: number = Math.floor(yPercent / GridConfig.cellHeight) + 1;

        return { x: gridCellX, y: gridCellY };

        
    }
}