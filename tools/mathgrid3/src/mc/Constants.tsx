export enum PanelType {
    sizes="sizes",
    colors="colors",
    fonts="fonts",
    style = "style",
    adjust = "adjust",
    color = "color",
    font = "font",
    lines="lines",
    move="moves",
    shift="shifts",
    insert="inserts",
    delete="deletes",
    symbols="symbols",
    templates="templates",
    debug="debug",
    clipboard="clipboard",
    scratchpad="scratchpad",
    overlays="overlays",
    text="text"
}
export enum GridActions
{
    MoveUp,
    MoveDown,
    MoveLeft,
    MoveRight,
    DeleteRow,
    DeleteCol,
    InsertRowAtStart,
    InsertColAtStart,
    InsertRowAtEnd,
    InsertColAtEnd,
    InsertRowAbove,
    InsertColLeft,
    InsertRowBelow,
    InsertColRight,
    ShiftColUp,
    ShiftColDown,
    ShiftRowLeft,
    ShiftRowRight,
    GoUp,
    GoDown,
    GoLeft,
    GoRight,
    SectionUp,
    SectionDown,
    SectionLeft,
    SectionRight
}
export enum PanelActions
{
    Move,
    Shift,
    Insert,
    Delete,
    Adjust
}
export enum StyleType {
    size = "size",
    color = "color",
    font = "font",
    cell="cell",
    other = "other"
}