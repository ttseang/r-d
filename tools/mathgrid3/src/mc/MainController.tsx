export class MainController
{
    public static instance:MainController | null = null;

    public static getInstance():MainController
    {
        if(MainController.instance == null)
        {
            MainController.instance = new MainController();
        }
        return MainController.instance;
    }

    public addImage:Function=()=>{};
    public addPanel:Function=()=>{};
    public setText:Function=()=>{};
    public removePanel:Function=()=>{};
    public addOverlay:Function=()=>{};
    public addLine:Function=()=>{};
    public toggleGrid:Function=()=>{};
    public setTextLock:Function=()=>{};
}