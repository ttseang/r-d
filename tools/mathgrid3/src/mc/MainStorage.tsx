export class MainStorage
{
    public static instance:MainStorage | null = null;

    public static getInstance():MainStorage
    {
        if(MainStorage.instance == null)
        {
            MainStorage.instance = new MainStorage();
        }
        return MainStorage.instance;
    }
}