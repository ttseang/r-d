import { Component } from 'react';
import FGrid from '../comps/FGrid';
import { GridVo } from '../dataObjs/GridVo';
import DGrid from '../comps/DGrid';
import EditPanels from '../panels/EditPanels';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import TopMenu from '../comps/ui/TopMenu';
import { Button, Card } from 'react-bootstrap';
import QuickMenu from '../panels/QuickMenu';
import QuickMenuBtn  from '../panels/QuickMenuBtn ';
interface MyProps { menuCallback: Function }
interface MyState { gridVo: GridVo, stepIndex: number, subscript: string, superscript: string }
class GridEdit extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { gridVo: this.ms.steps[0], stepIndex: 0, subscript: '', superscript: '' };
        this.mc.changeStep = this.changeStep.bind(this);
    }
    private changeStep(index: number) {
        this.ms.currentStep = index;
        this.setState({ gridVo: this.ms.steps[index], stepIndex: index });
    }
   
    render() {
        
        return (<div>
            <TopMenu callback={this.props.menuCallback}></TopMenu>
            <Card>
            <QuickMenuBtn></QuickMenuBtn>
            <QuickMenu></QuickMenu>
                <Card.Body>
                    <div className='editArea'>
                        
                        <div>
                            <FGrid rows={this.state.gridVo.rowCount} cols={this.state.gridVo.colCount} gridVo={this.state.gridVo} onChange={(grid: GridVo) => { this.setState({ gridVo: grid }) }}></FGrid>
                            <Button variant='danger' className='btnClear' onClick={()=>{this.mc.adjustGrid(14)}}>Clear</Button>
                        </div>
                        <DGrid grid={this.state.gridVo}></DGrid>
                    </div>
                </Card.Body>
            </Card>
            <hr />
            <EditPanels gridVo={this.state.gridVo}></EditPanels>
        </div>)
    }
}
export default GridEdit;