import { Component } from 'react';
import GridEdit from './GridEdit';
import { MainStorage } from '../mc/MainStorage';
import FileOpenScreen from './FileOpenScreen';
import FileNameScreen from './FileNameScreen';
import { MainController } from '../mc/MainController';
import ApiConnect from '../util/ApiConnect';
import { Payload } from '../dataObjs/Payload';
import { FileVo } from '../dataObjs/FileVo';
import { CellVo } from '../dataObjs/CellVo';
import { GridVo } from '../dataObjs/GridVo';
import { RowVo } from '../dataObjs/RowVo';

interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0 };

        //  this.ms.steps.push(GridUtil.getDefaultGrid());
        //  this.ms.steps.push(GridUtil.getDefaultGrid());
        //   this.ms.steps.push(GridUtil.getDefaultGrid());
    }
    componentDidMount(): void {
        // //console.log("BaseScreen.componentDidMount");
        // this.calc.doCalc("123+459");
    }
    getScreen() {
        switch (this.state.mode) {
            case 0:
                return <GridEdit menuCallback={this.menuSelected.bind(this)}></GridEdit>

            case 1:
                return <FileOpenScreen goBack={() => { this.setState({ mode: 0 }) }} openCallback={this.openFile.bind(this)}></FileOpenScreen>

            case 2:
                return <FileNameScreen callback={this.setFileName.bind(this)} cancelCallback={() => { this.setState({ mode: 0 }) }}></FileNameScreen>
        }
    }
    menuSelected(menuIndex: number, actionIndex: number) {
        switch (actionIndex) {

            case 1:
                this.setState({ mode: 2 });
                break;
            case 2:
                this.setState({ mode: 1 });
                break;
        }
    }
    setFileName(name: string) {
        //console.log("BaseScreen.setFileName:"+name);
        this.ms.fileName = name;
        this.ms.prepSaveData();

        //console.log(this.ms.saveData);
        // this.setState({mode:0});
        const apiConnect: ApiConnect = new ApiConnect();

        let payload: Payload = new Payload(this.ms.LtiFileName, "addition", "1+1", this.ms.saveData);
        apiConnect.Save(JSON.stringify(payload), this.saveDone.bind(this));
    }
    saveDone(data: any) {
        //console.log(data);

        this.setState({ mode: 0 });
        let loadData: any = this.ms.saveData.steps[0] || [];
        setTimeout(() => {
            this.mc.loadFromData(loadData);
        }, 500);
    }
    openFile(file: FileVo) {
        this.setState({ mode: 0 });
        this.ms.LtiFileName = file.lti;

        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getFileContent(file.lti, this.loadDone.bind(this));

    }
    loadDone(data: any) {
        //console.log("BaseScreen.loadDone");
        //console.log(data);
        // console.log("Steps");
        //  console.log(data.data.steps);

        this.ms.steps = [];
        let steps: any[] = data.data.steps;

        // console.log("ROWS");
        for (let i: number = 0; i < steps.length; i++) {
            let grid: any = steps[i];
            console.log(grid);
            let rowCount: number = grid.length;
            let rowVos: RowVo[] = [];

            for (let j: number = 0; j < rowCount; j++) {
                let row: any = grid[j];
                let colunmCount: number = row.length;
                let cellVos: CellVo[] = [];
                for (let k: number = 0; k < colunmCount; k++) {
                    let cell: any = row[k];
                    console.log(cell);
                    let textClasses: string = cell.textClasses || "";
                    let subTextClasses: string = cell.subTextClasses || "";
                    let superTextClasses: string = cell.superTextClasses || "";
                    let cellTextClasses: string = cell.cellTextClasses || "";

                    let superText: string = cell.superText || "";
                    let subText: string = cell.subText || "";
                    

                    let textClassArray: string[] = textClasses.split(",");
                    let subTextClassArray: string[] = subTextClasses.split(",");
                    let superTextClassArray: string[] = superTextClasses.split(",");
                    let cellTextClassArray: string[] = cellTextClasses.split(",");
                    

                    let cellVo: CellVo = new CellVo(cell.text, []);
                    cellVo.textClasses = textClassArray;
                    cellVo.subTextClasses = subTextClassArray;
                    cellVo.superTextClasses = superTextClassArray;
                    cellVo.cellClasses = cellTextClassArray;
                    cellVo.supertext = superText;
                    cellVo.subtext = subText;
                    cellVos.push(cellVo);
                }
                rowVos.push(new RowVo(cellVos, []));
            }
            this.ms.steps.push(new GridVo(rowVos));
        }
        this.mc.stepsUpdated();
        this.mc.changeStep(0);
        // this.mc.loadFromData(steps[0]);
        // this.mc.loadFromData(data.data);
    }
    render() {
        return (

            this.getScreen()

        )
    }
}
export default BaseScreen;