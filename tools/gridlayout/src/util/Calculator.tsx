import { GridVo } from "../dataObjs/GridVo";
import { MainController } from "../mc/MainController";
import { MainStorage } from "../mc/MainStorage";
import { AddCalc } from "./calc/AddCalc";

export class Calculator {
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage=MainStorage.getInstance();
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        this.mc.doCalc = this.doCalc.bind(this);
    }
    public doCalc(expression: string) {
        //decide what type of calc to do

        //check for +, -, *, /
        if (expression.indexOf("+") !== -1) {
            //do add
            const addCalc: AddCalc = new AddCalc();
            let grid: GridVo[] = addCalc.doAdd(expression);
            //console.log(grid);
            this.ms.steps=grid;
            this.ms.currentStep=0;
            this.mc.changePanels(0);
           
            //this.mc.addStepsFromCalc(grid);
        }


        return null;
    }
}