import { CellVo } from "../dataObjs/CellVo";
import { GridVo } from "../dataObjs/GridVo";
import { RowVo } from "../dataObjs/RowVo";

export class GridUtil {
    public static getDefaultGrid() {

        let rows: RowVo[] = [];
        for (let i: number = 0; i < 10; i++) {
            let cells: CellVo[] = [];
            for (let i: number = 0; i < 10; i++) {
                cells.push(new CellVo(" ", []));
            }
            rows.push(new RowVo(cells, []));
        }

        let grid: GridVo = new GridVo(rows);
        return grid;

    }
   
}
            