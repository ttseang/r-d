import { CellVo } from "../../dataObjs/CellVo";
import { GridVo } from "../../dataObjs/GridVo";
import { NumVo } from "../../dataObjs/NumVo";
import { RowVo } from "../../dataObjs/RowVo";
import { GridUtil } from "../GridUtil";
import { PlacesUtil } from "../PlacesUtil";

/* eslint-disable @typescript-eslint/no-useless-constructor */
export class AddCalc {
    private numsObj: NumVo[] = [];
    private maxWhole: number = 0;
    private maxDec: number = 0;
    private totalSteps: number = 0;
    private carryArray: number[][] = [];
    private sumArray: number[] = [];
    private sumString: string = "";

    private cells: CellVo[][] = [];
    private testGrid: string[][] = [];

    private row: number = 2;
    private col: number = 8;
    private gridSize: number = 10;
    private steps: GridVo[] =[];

    constructor() {
        this.carryArray[0] = [];
        this.carryArray[0].push(-1);
       this.makeGrid();
    }
    private makeGrid()
    {
        for (let i: number = 0; i < this.gridSize; i++) {
            this.testGrid[i] = [];
            this.cells[i] = [];
            for (let j: number = 0; j < this.gridSize; j++) {
                this.testGrid[i][j] = " ";
                this.cells[i][j] = new CellVo(" ", []);
            }
        }
    }
    public doAdd(expression: string):GridVo[] {

        let nums: string[] = expression.split("+");

        this.numsObj = PlacesUtil.makeNumberObjs(nums);

        this.maxWhole = PlacesUtil.findMaxWhole(nums);
        this.maxDec = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            this.numsObj[i].formatDigits(this.maxWhole, this.maxDec);
        }

        /**
         * Initial
         */


        this.totalSteps = 0;

        for (let i: number = 0; i < this.numsObj.length; i++) {
            /* if (this.ms.lineUpDec === true) {
                this.numsObj[i].fixDigits(this.maxWhole, this.maxDec);
            } */

            if (this.numsObj[i].allDigits.length > this.totalSteps) {
                this.totalSteps = this.numsObj[i].allDigits.length + 1;
            }
        }
        ////console.log("totalSteps: " + this.totalSteps);
       // //console.log(this.numsObj);

        let cols: number[][] = [];

        for (let i: number = 0; i < this.numsObj.length; i++) {
            let digits: number[] = this.numsObj[i].allDigits.slice();


            for (let j: number = 0; j < digits.length; j++) {

                if (!cols[j]) {
                    cols[j] = [];
                }
                cols[j].push(digits[j]);
            }
        }

        cols = cols.reverse();
      // // //console.log(cols);

        let carryOver: number = 0;


        this.sumArray = [];

        for (let i: number = 0; i < cols.length; i++) {

            let col: number[] = cols[i];

            let sum: number = col.reduce((partialSum, a) => partialSum + a, 0);



            let c: number = Math.floor(sum / 10);
            let r: number = sum - (c * 10);

            if (c === 0) {
                this.carryArray[0].unshift(-1);
            }
            else {
                this.carryArray[0].unshift(c);
            }

            this.sumArray.unshift(r + carryOver);

            carryOver = c;

           // //console.log(this.sumArray)
            this.addStep();
        }


        if (carryOver > 0) {
            this.sumArray.unshift(carryOver);
        }

          this.addStep();

       // //console.log(this.steps);
        return this.steps;
    }
    addDigits(digits: string[]) {
        digits=digits.reverse();
        for (let i: number = 0; i < digits.length; i++) {
            if (digits[i] !== "-1") {
               //// //console.log(this.row + " " + this.col + " " + digits[i]);
               //// //console.log(this.testGrid);
                this.testGrid[this.row][this.col] = digits[i];
                this.cells[this.row][this.col].text = digits[i];
            }
            this.col--;
            if (this.col < 0) {
                this.col = this.gridSize - 1;
            }
        }
    }
    getBody() {
        this.row++;
        this.col = 9;
        let body: CellVo[][] = [];

        for (let i: number = 0; i < this.numsObj.length; i++) {
            let digits: string[] = this.numsObj[i].allChars.slice();


            if (this.numsObj[i].isCurrency === true) {
                digits.unshift("$");
            }
           // //console.log(digits);
            this.addDigits(digits);
            this.row++;
            this.col = 9;
            /* let bodyCells: CellVo[] = GridUtil.arrayToCells2(digits, []);

            body.push(bodyCells); */
        }

        //  body = this.fixBody(body);
        return body;
    }
    getAnswer() {
        let sum: number = parseInt(this.sumArray.join(""));
        sum = sum / (Math.pow(10, this.maxDec));

        this.sumString = sum.toString();
       // //console.log(this.sumString);
        this.addDigits(this.sumString.split(""));
        /* let sums: string[] = this.sumString.split("");

        let answer: CellVo[] = [];
        for (let i: number = 0; i < sums.length; i++) {
            answer.push(new CellVo(sums[i].toString(), []));
        }
        return answer; */
    }
    addStep()
    {
        this.makeGrid();
        this.row=2;
        this.col=9;
        this.carryArray[0].reverse();       
        this.addDigits(this.carryArray[0].map(String));
        this.getBody();
        this.getAnswer();
       // //console.log(this.cells);
        let rows:RowVo[]=[];
        for(let i:number=0;i<this.cells.length;i++)
        {
            rows.push(new RowVo(this.cells[i],[]));
        }
        let gridVo:GridVo=new GridVo(rows);
        this.steps.push(gridVo);
    }
}