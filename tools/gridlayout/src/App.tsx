import "./styles/main.css";
import "./styles/grid.css";
import "./styles/math.css";
import "./styles/panels.css";
import "./styles/screens.css";
import "./styles/quickMenu.css";

import BaseScreen from './screens/BaseScreen';
//import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <div className="App">
      <BaseScreen></BaseScreen>
    </div>
  );
}

export default App;
