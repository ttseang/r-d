import React, { Component } from 'react';
import { GridVo } from '../dataObjs/GridVo';
import { RowVo } from '../dataObjs/RowVo';
import { MainController } from '../mc/MainController';
import { Card } from 'react-bootstrap';
interface MyProps { grid: GridVo,index:number,clickCallback:Function,selected:boolean }
interface MyState { grid: GridVo,selected:boolean }
class SGrid extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { grid: props.grid,selected:this.props.selected };
    }
    componentDidMount(): void {
        //add event listener for keypress using React.KeyboardEvent<HTMLInputElement>
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.grid !== this.props.grid) {
            this.setState({ grid: this.props.grid });
        }
        if (prevProps.selected!==this.props.selected)
        {
            this.setState({selected:this.props.selected});
        }
    }
    clickCell(e: React.MouseEvent<Element, MouseEvent>) {
        //get the row and column of the cell that was clicked
        //call MainController.getInstance().selectCell(row,col)
        let cell: HTMLTableCellElement = e.target as HTMLTableCellElement;
        //console.log(cell);
        let id: string = cell.id;
        let parts: string[] = id.split('_');
        let row: number = parseInt(parts[0]);
        let col: number = parseInt(parts[1]);
        if (!isNaN(row) && !isNaN(col)) {
            this.mc.selectCell(row, col);
        }
    }
    getGrid() {
        //render this.state.grid as table
        let cells: JSX.Element[] = [];
        for (let r = 0; r < this.state.grid.rows.length; r++) {
            let row: RowVo = this.state.grid.rows[r];
            let rowCells: JSX.Element[] = [];
            for (let c = 0; c < row.cells.length; c++) {
                let cell = row.cells[c];
                let text = cell.text;
                let id: string = r + '_' + c;
                let classes = cell.textClasses.join(' ');
                if (classes !== '') {
                    rowCells.push(<td key={r + '_' + c} id={id} className={classes}>{text}</td>);

                }
                else {
                    rowCells.push(<td key={r + '_' + c} id={id}>{text}</td>);
                }
            }
            let rowClasses: string = row.rowClasses.join(' ');
            if (rowClasses !== '') {
                cells.push(<tr key={r} className={rowClasses}>{rowCells}</tr>);
            }
            else {

                cells.push(<tr key={r}>{rowCells}</tr>);
            }
        }
        return (<table className='stepTable'><tbody>{cells}</tbody></table>);
    }
    selectMe()
    {
       // this.mc.changeStep(this.props.index);
       this.props.clickCallback(this.props.index);
    }
    render() {
        let classes:string[]=["stepCard"]
        if (this.state.selected===true)
        {
            classes.push("selected");
        }
        let classString:string=classes.join(" ");
        return (<Card className={classString} onClick={this.selectMe.bind(this)}><Card.Body>{this.getGrid()}</Card.Body></Card>)
    }
}
export default SGrid;