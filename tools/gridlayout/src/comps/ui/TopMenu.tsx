import React, { Component } from 'react';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
interface MyProps {callback:Function}
interface MyState { }
class TopMenu extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    private doAction(menuIndex: number, actionIndex: number) {
       // //console.log("TopMenu.doAction", menuIndex, actionIndex);
        this.props.callback(menuIndex,actionIndex);
    }
    render() {
        return (<div className='topMenu'>
            <ButtonGroup>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        File
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => { this.doAction(0, 0) }}>New</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 1) }}>Save</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 2) }}>Open</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown>
                </Dropdown>
            </ButtonGroup>
        </div>)
    }
}
export default TopMenu;