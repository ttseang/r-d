import React, { Component } from 'react';
import { RowVo } from '../dataObjs/RowVo';
import { CellVo } from '../dataObjs/CellVo';
import { GridVo } from '../dataObjs/GridVo';
import { MainController } from '../mc/MainController';
import { StyleVo } from '../dataObjs/StyleVo';
import { MathSymbols, StyleType } from '../mc/Constants';
import { MainStorage } from '../mc/MainStorage';
import { GridUtil } from '../util/GridUtil';

interface MyProps { rows: number, cols: number, onChange: Function, gridVo: GridVo }
interface MyState { rows: number, cols: number, rowClasses: string[][], gridCells: CellVo[][], currentRow: number, currentCol: number }
class FGrid extends Component<MyProps, MyState>
{

    private ignoreNextFocus: boolean = false;
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();
    private altDown: boolean = false;
    private shiftDown: boolean = false;
    private ctrlDown: boolean = false;

    constructor(props: MyProps) {
        super(props);

        let cells: CellVo[][] = this.props.gridVo.getCells();

        this.state = { rows: props.rows, cols: props.cols, gridCells: cells, rowClasses: [], currentRow: 0, currentCol: 0 };
        (window as any).fgrid = this;

        this.mc.updateStyle = this.updateStyle.bind(this);
        this.mc.selectCell = this.setCellFocus.bind(this);
        this.mc.adjustGrid = this.adjustGrid.bind(this);
        //
        this.mc.addColumn = this.addColumn.bind(this);
        this.mc.addRow = this.addRow.bind(this);
        this.mc.removeColumn = this.removeColumn.bind(this);
        this.mc.removeRow = this.removeRow.bind(this);

        this.mc.insertSymbol = this.insertText.bind(this);

       // this.mc.prepSaveData = this.prepSaveData.bind(this);
        this.mc.loadFromData = this.loadExport.bind(this);
    }
    componentDidMount(): void {
        //add event listener for keypress using React.KeyboardEvent<HTMLInputElement>


    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.gridVo !== this.props.gridVo) {
            ////console.log('gridVo changed');
            let cells: CellVo[][] = this.props.gridVo.getCells();
            this.setState({ gridCells: cells });
        }
    }
    updateStyle(styleVo: StyleVo) {
        //console.log('updateStyle ' + styleVo.styleValue);
        let cells: CellVo[][] = this.state.gridCells;
        if (styleVo.applyToRow === true) {

            let rowClasses: string[][] = this.state.rowClasses;
            if (rowClasses[this.state.currentRow] === undefined) {
                rowClasses[this.state.currentRow] = [];
            }
            //toggle class
            if (rowClasses[this.state.currentRow].indexOf(styleVo.styleValue) > -1) {
                rowClasses[this.state.currentRow].splice(rowClasses[this.state.currentRow].indexOf(styleVo.styleValue), 1);
            }
            else {
                rowClasses[this.state.currentRow].push(styleVo.styleValue);
            }
        }
        else {

            let cell: CellVo = cells[this.state.currentRow][this.state.currentCol];
            if (cell.text === '' || cell.text === ' ') {
                // //console.log('cell is empty');
                return;
            }

            //if the styleVo is of type color remove all other color classes
            if (styleVo.type === StyleType.color) {


                if (cell.hasClass(styleVo.styleValue)) {
                    //console.log('cell has class ' + styleVo.styleValue);
                    cell.removeClass(styleVo.styleValue);
                    this.setState({ gridCells: cells }, this.updatePreview);
                    return;
                }

                let colorStyles: StyleVo[] = this.ms.getStyleByType(StyleType.color);
                for (let i = 0; i < colorStyles.length; i++) {
                    cell.removeClass(colorStyles[i].styleValue);
                }
                cell.addClass(styleVo.styleValue);
                //console.log(cell);
                this.setState({ gridCells: cells }, this.updatePreview);
                return;
            }

            //if the styleVo is of type size remove all other size classes
            if (styleVo.type === StyleType.size) {

                if (cell.hasClass(styleVo.styleValue)) {
                    cell.removeClass(styleVo.styleValue);
                    this.setState({ gridCells: cells }, this.updatePreview);
                    return;
                }

                let sizeStyles: StyleVo[] = this.ms.getStyleByType(StyleType.size);
                for (let i = 0; i < sizeStyles.length; i++) {
                    cell.removeClass(sizeStyles[i].styleValue);
                }
                cell.addClass(styleVo.styleValue);
                this.setState({ gridCells: cells }, this.updatePreview);
                return;
            }

            if (styleVo.type===StyleType.cell)
            {
                cell.toggleCellClass(styleVo.styleValue);
                this.setState({ gridCells: cells }, this.updatePreview);
                return;
            }

            cell.toggleClass(styleVo.styleValue);
            this.setState({ gridCells: cells });
        }
        this.updatePreview();
    }
    updateGrid(gridVo: GridVo) {
        this.setState({ gridCells: gridVo.getCells() });
    }
    getCurrentCell(): CellVo {
        return this.state.gridCells[this.state.currentRow][this.state.currentCol];
    }

    getRows(gridText: string[][]) {
        let rows: RowVo[] = [];
        for (let r = 0; r < gridText.length; r++) {
            let row: string[] = gridText[r];
            let cells: CellVo[] = [];
            for (let c = 0; c < row.length; c++) {
                let cell: string = row[c];
                cells.push(new CellVo(cell, []));
            }
            rows.push(new RowVo(cells, []));
        }
        return rows;
    }
    onKeyPress(e: React.KeyboardEvent<HTMLInputElement>) {
        //////console.log(e.key);
        //////console.log(e.currentTarget.id);
        e.preventDefault();
        this.ignoreNextFocus = false;
        if (!e.currentTarget.id || e.currentTarget.id === '') {
            return;
        }

        let row: number = this.state.currentRow;
        let col: number = this.state.currentCol;

        let gridCells: CellVo[][] = this.state.gridCells;
        //check for alt key

        this.altDown = false;
        this.ctrlDown = false;
        this.shiftDown = false;

        if (e.ctrlKey) {
            this.ctrlDown = true;
        }
        if (e.shiftKey) {
            this.shiftDown = true;
        }
        if (e.altKey) {
            this.altDown = true;

        }

        console.log('altDown ' + this.altDown);
        console.log('key ' + e.key);
        //alt + the letter u
        if (e.key === 'u' && this.altDown === true) {
            const underLineStyle: StyleVo | null = this.ms.getStyleByName('bottom line');
            if (underLineStyle) {
                this.updateStyle(underLineStyle);
                this.goNextCell();
            }

            return;
        }

        if (e.key === 't' && this.altDown === true) {
            const topLineStyle: StyleVo | null = this.ms.getStyleByName('top line');
            if (topLineStyle) {
                this.updateStyle(topLineStyle);
                this.goNextCell();
            }

            return;
        }
        //check for alt + the letter x
        if (e.key === 'x' && this.altDown === true) {
            //update the cell value
            let cell: CellVo = gridCells[row][col];
            cell.text = MathSymbols['*'];
            this.setState({ gridCells: gridCells }, this.updatePreview);
            this.goNextCell();
        }

        if (e.key === 'Enter') {

            let nextRow = row;
            nextRow++;
            let nextCol = 0;
            if (nextRow >= this.state.rows) {
                nextRow = 0;
            }
            let nextId = nextRow + '_' + nextCol;
            let nextInput = document.getElementById(nextId);
            if (nextInput) {
                nextInput.focus();
            }
        }
        if (e.key === 'ArrowUp') {

            if (this.shiftDown === true && this.ctrlDown === true) {
                this.adjustGrid(8);
                return;
            }
            if (this.shiftDown === true) {
                this.adjustGrid(0);
                return;
            }
            if (this.ctrlDown === true) {
                this.adjustGrid(4);
                return;
            }

            let nextRow = row - 1;
            let nextCol = col;
            if (nextRow < 0) {
                nextRow = this.state.rows - 1;
            }
            this.setCellFocus(nextRow, nextCol);

        }
        if (e.key === 'ArrowDown') {
            //console.log('arrow down');
            if (this.shiftDown === true && this.ctrlDown === true) {
                this.adjustGrid(9);
                return;
            }
            if (this.shiftDown === true) {
                this.adjustGrid(1);
                return;
            }
            if (this.ctrlDown === true) {
                this.adjustGrid(5);
                return;
            }
            let nextRow = row + 1;
            let nextCol = col;
            if (nextRow >= this.state.rows) {
                nextRow = 0;


            }
            this.setCellFocus(nextRow, nextCol);
        }
        if (e.key === 'ArrowLeft') {

            if (this.shiftDown === true && this.ctrlDown === true) {
                this.adjustGrid(10);
                return;
            }
            if (this.shiftDown === true) {
                this.mc.adjustGrid(2);
                return;
            }
            if (this.ctrlDown === true) {
                this.mc.adjustGrid(6);
                return;
            }
            let nextRow = row;
            let nextCol = col - 1;
            if (nextCol < 0) {
                nextCol = this.state.cols - 1;
            }
            this.setCellFocus(nextRow, nextCol);
        }
        if (e.key === 'ArrowRight') {

            if (this.shiftDown === true && this.ctrlDown === true) {
                this.adjustGrid(11);
                return;
            }
            if (this.shiftDown === true) {
                this.mc.adjustGrid(3);
                return;
            }
            if (this.ctrlDown === true) {
                this.mc.adjustGrid(7);
                return;
            }
            let nextRow = row;
            let nextCol = col + 1;
            if (nextCol >= this.state.cols) {
                nextCol = 0;
            }
            this.setCellFocus(nextRow, nextCol);
        }
        //backspace
        if (e.key === 'Backspace') {
            //console.log('backspace');
            e.preventDefault();
            this.ignoreNextFocus = true;


            gridCells[row][col].text = '';


            let nextRow = this.state.currentRow;
            let nextCol = this.state.currentCol - 1;
            if (nextCol < 0) {
                nextCol = this.state.cols - 1;
                nextRow--;
                if (nextRow < 0) {
                    nextRow = this.state.rows - 1;
                }
            }
            if (gridCells[nextRow][nextCol].text === '' || gridCells[nextRow][nextCol].text === ' ') {
                this.setCellFocus(nextRow, nextCol);
                this.updatePreview();
            }
            else {
                this.setState({ gridCells: gridCells }, () => {
                    this.setCellFocus(nextRow, nextCol);
                    this.updatePreview();
                })
            }
            // this.setCellFocus(nextRow, nextCol);

            return;
        }
        //delete
        if (e.key === 'Delete') {

            let gridCells: CellVo[][] = this.state.gridCells;
            gridCells[row][col].text = '';
            this.setState({ gridCells: gridCells });
        }
        this.ignoreNextFocus = false;
    }
    setCellFocus(row: number, col: number) {
        let id = row + '_' + col;
        let input = document.getElementById(id);
        if (input) {
            input.focus();
        }
    }
    onFocus(e: React.FocusEvent<HTMLInputElement>) {

        let row = parseInt(e.target.id.split('_')[0]);
        let col = parseInt(e.target.id.split('_')[1]);
        if (!isNaN(row) && !isNaN(col)) {
            this.setState({ currentCol: col, currentRow: row });
            //console.log("onFocus " + row + " " + col);
            let cell: CellVo = this.state.gridCells[row][col];
            this.mc.updateSelectedCell(cell);
        }
    }
    insertText(symbol: string) {
        let gridCells: CellVo[][] = this.state.gridCells;
        let row = this.state.currentRow;
        let col = this.state.currentCol;
        gridCells[row][col].text = symbol;
        this.setState({ gridCells: gridCells }, this.updatePreview);
        this.goNextCell();
    }
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        let gridCells: CellVo[][] = this.state.gridCells;
        ////console.log(gridCells);

        //check for backspace
        if (e.target.value === '') {
            return;
        }
        this.ignoreNextFocus = false;

        let row = parseInt(e.target.id.split('_')[0]);
        let col = parseInt(e.target.id.split('_')[1]);

        let text: string = e.target.value;
        text = text.trim();
        //console.log("changing cell " + row + " " + col + " to " + text);

        if (text.length > 1) {
            //if the last character is not a decimal then reduce the text to the last character
            let lastChar = text.substring(text.length - 1);
            //console.log("last char is " + lastChar);
            if (lastChar !== '.' && lastChar !== ',') {
                text = text.substring(text.length - 1);
                e.currentTarget.value = text;
            }
        }


        gridCells[row][col].text = text;
        this.setState({ gridCells: gridCells });

        this.updatePreview();


        if (this.ignoreNextFocus) {
            //console.log("ignoring next focus");
            this.ignoreNextFocus = false;
            return;
        }
        //set focus to next cell
        let nextRow = row;
        let nextCol = col + 1;
        if (nextCol >= this.state.cols) {
            nextCol = 0;
            nextRow++;
        }
        if (nextRow >= this.state.rows) {
            nextRow = 0;
        }
        let nextId = nextRow + '_' + nextCol;
        let nextInput = document.getElementById(nextId);
        if (nextInput) {
            nextInput.focus();
        }
    }
    getGridVo() {
        let gridCells: CellVo[][] = this.state.gridCells;
        let rowsVo: RowVo[] = [];
        for (let r = 0; r < gridCells.length; r++) {
            let rowClasses = this.state.rowClasses[r];
            if (!rowClasses) {
                rowClasses = [];
            }

            let rowVo: RowVo = new RowVo([], rowClasses);
            for (let c = 0; c < gridCells[r].length; c++) {
                let cellVo: CellVo = gridCells[r][c];
                rowVo.cells.push(cellVo);
            }
            rowsVo.push(rowVo);
        }
        //make gridVo
        let gridVo: GridVo = new GridVo(rowsVo);
        return gridVo;
    }
    goNextCell() {
        // let gridCells: CellVo[][] = this.state.gridCells;
        let row = this.state.currentRow;
        let col = this.state.currentCol;
        let nextRow = row;
        let nextCol = col + 1;
        if (nextCol >= this.state.cols) {
            nextCol = 0;
            nextRow++;
        }
        if (nextRow >= this.state.rows) {
            nextRow = 0;
        }
        this.setCellFocus(nextRow, nextCol);
    }
    updatePreview() {
        let gridVo: GridVo = this.getGridVo();
        this.props.onChange(gridVo);
    }
    addToRow(row: number, classToAdd: string) {
        let gridCells: CellVo[][] = this.state.gridCells;
        let cells: CellVo[] = gridCells[row];
        for (let c = 0; c < cells.length; c++) {
            let cellVo: CellVo = cells[c];
            cellVo.toggleClass(classToAdd);
        }
        gridCells[row] = cells;
        this.setState({ gridCells: gridCells }, this.updatePreview);
    }
    getGrid() {
        //make a grid of editable cells with input boxes
        let grid: JSX.Element[] = [];

        //add top row of column headers
        let headerRow: JSX.Element[] = [];
        for (let c = 0; c < this.state.cols; c++) {
            let key: string = "header" + c;
            if (c === this.state.currentCol) {
                headerRow.push(<th key={key} className='redText'>{c}</th>);
            }
            else {
                headerRow.push(<th key={key}>{c}</th>);
            }
        }
        headerRow.unshift(<th key="headerRowNum"></th>);

        for (let r = 0; r < this.state.rows; r++) {
            let row: JSX.Element[] = [];
            //push row number
            let rowKey2: string = "rownum" + r;
            if (r === this.state.currentRow) {
                row.push(<td key={rowKey2} className='redText'><strong>{r}</strong></td>);
            }
            else {
                row.push(<td key={rowKey2}><strong>{r}</strong></td>);
            }

            for (let c = 0; c < this.state.cols; c++) {
                let id: string = r + '_' + c;
                let text: string = this.state.gridCells[r][c].text;
                let key: string = "cell" + r + '_' + c;
                if (r === this.state.currentRow && c === this.state.currentCol) {
                    row.push(<td key={key}><input type="text" id={id} className='inputCell selected' onKeyUp={this.onKeyPress.bind(this)} value={text} size={4} onFocus={this.onFocus.bind(this)} onChange={this.onChange.bind(this)}></input></td>);
                }
                else {
                    row.push(<td key={key}><input type="text" id={id} className='inputCell' onKeyUp={this.onKeyPress.bind(this)} value={text} size={4} onFocus={this.onFocus.bind(this)} onChange={this.onChange.bind(this)}></input></td>);
                }
            }
            let rowKey: string = "row" + r;
            grid.push(<tr key={rowKey}>{row}</tr>);
        }
        return (<table><thead><tr>{headerRow}</tr></thead><tbody>{grid}</tbody></table>);
    }
    adjustGrid(direction: number) {
        let gridVo: GridVo = this.getGridVo();


        // let rowClasses: string[][] = this.state.rowClasses;
        switch (direction) {
            //up
            case 0:
                gridVo.shiftUp();
                break;
            //down
            case 1:
                gridVo.shiftDown();
                break;
            //left
            case 2:
                gridVo.shiftLeft();
                break;
            //right
            case 3:
                gridVo.shiftRight();
                break;

            case 4:
                gridVo.shiftColumnUp(this.state.currentCol);
                break;
            case 5:
                gridVo.shiftColumnDown(this.state.currentCol);
                break;
            case 6:
                gridVo.shiftRowLeft(this.state.currentRow);
                break;
            case 7:
                gridVo.shiftRowRight(this.state.currentRow);
                break;
            case 8:
                gridVo.insertRowAbove(this.state.currentRow);
                break;
            case 9:
                gridVo.insertRowBelow(this.state.currentRow);
                break;
            case 10:
                gridVo.insertColumnLeft(this.state.currentCol);
                break;
            case 11:
                gridVo.insertColumnRight(this.state.currentCol);
                break;
            case 12:
                gridVo.deleteRow(this.state.currentRow);
                break;
            case 13:
                gridVo.deleteColumn(this.state.currentCol);
                break;
            case 14:
                //clear all
                gridVo = GridUtil.getDefaultGrid();

        }
        //get row classes from gridVo
        let rowClasses: string[][] = [];
        for (let r = 0; r < gridVo.rowCount; r++) {
            let rowVo: RowVo = gridVo.rows[r];
            //for each rowVo, get the classes
            //take the array of classes and put them in the rowClasses array
            rowClasses.push(rowVo.rowClasses);

        }

        this.ms.steps[this.ms.currentStep] = gridVo;
        let cells: CellVo[][] = gridVo.getCells();

        let rowCount: number = gridVo.rows.length;
        let colCount: number = gridVo.rows[0].cells.length;

        this.setState({ gridCells: cells, rowClasses: rowClasses, rows: rowCount, cols: colCount }, this.updatePreview);
    }
    addColumn(addAtStart: boolean) {
        let gridVo: GridVo = this.getGridVo();
        gridVo.addColumn(addAtStart);

        this.setState({ gridCells: gridVo.getCells(), cols: gridVo.colCount }, this.updatePreview);
    }
    addRow(addAtStart: boolean) {
        let gridVo: GridVo = this.getGridVo();
        gridVo.addRow(addAtStart);
        this.setState({ gridCells: gridVo.getCells(), rows: gridVo.rowCount }, this.updatePreview);
    }
    removeColumn(removeAtStart: boolean) {
        let gridVo: GridVo = this.getGridVo();
        gridVo.removeColumn(removeAtStart);
        this.setState({ gridCells: gridVo.getCells(), cols: gridVo.colCount }, this.updatePreview);
    }
    removeRow(removeAtStart: boolean) {
        let gridVo: GridVo = this.getGridVo();
        gridVo.removeRow(removeAtStart);
        this.setState({ gridCells: gridVo.getCells(), rows: gridVo.rowCount }, this.updatePreview);
    }

    debugGrid() {
        let gridCells: CellVo[][] = this.state.gridCells;
        for (let r = 0; r < gridCells.length; r++) {
            let row: CellVo[] = gridCells[r];
            let cells: string = '';
            for (let c = 0; c < row.length; c++) {
                let cellVo: CellVo = row[c];
                cells += cellVo.text + ' ';
            }
            console.log(cells);
        }
    }
    getSubSuper() {
        let currentCell: CellVo = this.state.gridCells[this.state.currentRow][this.state.currentCol];
        let superscript: string = currentCell.supertext;
        let subscript: string = currentCell.subtext;

        return (<div className='subSuper'>
            <label>Superscript</label>
            <input type='text' value={superscript} onChange={this.onSuperscriptChange.bind(this)}></input>
            <label>Subscript</label>
            <input type='text' value={subscript} onChange={this.onSubscriptChange.bind(this)}></input>
        </div>)
    }
    onSubscriptChange(e: React.ChangeEvent<HTMLInputElement>) {
        let currentCell: CellVo = this.state.gridCells[this.state.currentRow][this.state.currentCol];
        currentCell.subtext = e.target.value;
        this.setState({ gridCells: this.state.gridCells }, this.updatePreview);
    }
    onSuperscriptChange(e: React.ChangeEvent<HTMLInputElement>) {

        let currentCell: CellVo = this.state.gridCells[this.state.currentRow][this.state.currentCol];
        currentCell.supertext = e.target.value;
        this.setState({ gridCells: this.state.gridCells }, this.updatePreview);
    }
    /* private prepSaveData() {
        this.ms.saveData=this.getExport();
    } */
    getExport()
    {
        let gridVo: GridVo = this.getGridVo();
        let data:any[][] = gridVo.getExportData();
        return data;
        /* let exportString:string = JSON.stringify(data);
        return exportString; */
    }
    getExportString()
    {
        let exportString:string = JSON.stringify(this.getExport());
        return exportString;
    }
    loadExportString(exportString:string)
    {
        let data:any[][] = JSON.parse(exportString);
        this.loadExport(data);
    }
    loadExport(data:any[][])
    {
        //console.log("Loading export data");
        let gridCells: CellVo[][] =[];      

        
        this.ms.saveData = data;

        for (let r = 0; r < data.length; r++) {
            let row: any[] = data[r];
            gridCells[r] = [];
            for (let c = 0; c < row.length; c++) {
                let cell: any = row[c];
                //console.log(cell);
                let cellVo:CellVo = new CellVo(cell.text,[]);
                cellVo.subtext = cell.subtext;
                cellVo.supertext = cell.supertext;
                cellVo.subTextClasses = (cell.subTextClasses!==undefined)?cell.subTextClasses.split():[];
                cellVo.superTextClasses = (cell.superTextClasses!==undefined)?cell.superTextClasses.split():[];
                cellVo.textClasses = (cell.textClasses!==undefined)?cell.textClasses.split(","):[];
                cellVo.cellClasses = (cell.cellClasses!==undefined)?cell.cellClasses.split():[];
                gridCells[r].push(cellVo);
            }            
        }
        //console.log(gridCells);
        this.setState({gridCells:gridCells},this.updatePreview);
        /* {
            "text": " ",
            "supertext": "",
            "subtext": "",
            "classArray": "",
            "subTextClasses": "blueText",
            "superTextClasses": "redText"
        } */
    }
    render() {
        return (<div>
            {this.getSubSuper()}
            {this.getGrid()}</div>)
    }
}
export default FGrid;