import React, { Component } from 'react';
import { GridVo } from '../dataObjs/GridVo';
import { RowVo } from '../dataObjs/RowVo';
import { MainController } from '../mc/MainController';
import { CellVo } from '../dataObjs/CellVo';
interface MyProps { grid: GridVo }
interface MyState { grid: GridVo }
class DGrid extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { grid: props.grid };
    }
    componentDidMount(): void {
        //add event listener for keypress using React.KeyboardEvent<HTMLInputElement>
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.grid !== this.props.grid) {
            this.setState({ grid: this.props.grid });
        }
    }
    clickCell(e:React.MouseEvent<Element, MouseEvent>)
    {
        //get the row and column of the cell that was clicked
        //call MainController.getInstance().selectCell(row,col)
        let cell:HTMLTableCellElement=e.target as HTMLTableCellElement;
        //console.log(cell);
        let id:string=cell.id;
        let parts:string[]=id.split('_');
        let row:number=parseInt(parts[0]);
        let col:number=parseInt(parts[1]);
        if (!isNaN(row) && !isNaN(col))
        {
            this.mc.selectCell(row,col);
        }
    }
    getText(cellVo:CellVo)
    {
        let textElement:JSX.Element[]=[];
        let text:string=cellVo.text;
        if (cellVo.subtext)
        {
            let subClasses:string=cellVo.subTextClasses.join(' ');
            //console.log("subClasses:"+subClasses);
            if (subClasses!=='')
            {
                textElement.push(<sub key='sub' className={subClasses}>{cellVo.subtext}</sub>);
            }
            else
            {
                textElement.push(<sub key='sub'>{cellVo.subtext}</sub>);
            }
           
        }
        if (cellVo.supertext)
        {
            let supClasses:string=cellVo.superTextClasses.join(' ');
            //console.log("supClasses:"+supClasses);
            if (supClasses!=='')
            {
                textElement.push(<sup key='sup' className={supClasses}>{cellVo.supertext}</sup>);
            }
            else
            {
                textElement.push(<sup key='sup'>{cellVo.supertext}</sup>);
            }           
        }
        let classes = cellVo.textClasses.join(' ');
        if (classes !== '') {
            textElement.push(<span key='text' className={classes}>{text}</span>);
        }
        else
        {
            textElement.push(<span key='text'>{text}</span>);
        }      
        
        return textElement;
    }
    getGrid() {
        //render this.state.grid as table
        let cells: JSX.Element[] = [];
        for (let r = 0; r < this.state.grid.rows.length; r++) {
            let row: RowVo = this.state.grid.rows[r];
            let rowCells: JSX.Element[] = [];
            for (let c = 0; c < row.cells.length; c++) {
                let cell = row.cells[c];
                //let text = cell.text;
                let id: string = r + '_' + c;

                let cellClasses: string = cell.cellClasses.join(' ');
                if (cellClasses !== '') {
                    rowCells.push(<td key={r + '_' + c} id={id} className={cellClasses} onClick={this.clickCell.bind(this)}>{this.getText(cell)}</td>);
                }
                else
                {
                    rowCells.push(<td key={r + '_' + c} id={id} onClick={this.clickCell.bind(this)}>{this.getText(cell)}</td>);
                }
               // rowCells.push(<td key={r + '_' + c} id={id} onClick={this.clickCell.bind(this)}>{this.getText(cell)}</td>);
                /* let classes = cell.classArray.join(' ');
                if (classes !== '') {
                    rowCells.push(<td key={r + '_' + c} id={id} className={classes} onClick={this.clickCell.bind(this)}>{this.getText(cell)}</td>);

                }
                else {
                    rowCells.push(<td key={r + '_' + c} id={id} onClick={this.clickCell.bind(this)}>{this.getText(cell)}</td>);
                } */
            }
            let rowClasses: string = row.rowClasses.join(' ');
            if (rowClasses !== '') {
                cells.push(<tr key={r} className={rowClasses}>{rowCells}</tr>);
            }
            else {

                cells.push(<tr key={r}>{rowCells}</tr>);
            }
        }
        return (<table className='previewTable'><tbody>{cells}</tbody></table>);
    }
    render() {
        return (<div className='tableBorder'>{this.getGrid()}</div>)
    }
}
export default DGrid;