import { StyleType } from "../mc/Constants";

export class StyleVo {
    public styleName: string;
    public styleValue: string;
    public applyToRow: boolean;
    public type: StyleType;
    constructor(styleName: string, styleValue: string, applyToRow: boolean, type: StyleType) {
        this.styleName = styleName;
        this.styleValue = styleValue;
        this.applyToRow = applyToRow;
        this.type = type;

    }
}