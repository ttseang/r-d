export class CellVo {
    public text: string;
    public textClasses: string[];
    public subtext: string = '';
    public supertext: string = '';
    public subTextClasses: string[] = [];
    public superTextClasses: string[] = [];
    public cellClasses: string[] = [];

    constructor(text: string, classArray: string[]) {
        this.text = text;
        this.textClasses = classArray;
    }
    public addClass(className: string) {
        if (this.textClasses.indexOf(className) === -1) {
            this.textClasses.push(className);
        }
    }
    public removeClass(className: string) {
        if (this.textClasses.indexOf(className) !== -1) {
            this.textClasses.splice(this.textClasses.indexOf(className), 1);
        }
    }
    public hasClass(className: string) {
        return this.textClasses.indexOf(className) !== -1;
    }
    public addCellClass(className: string) {
        if (this.cellClasses.indexOf(className) === -1) {
            this.cellClasses.push(className);
        }
    }
    public removeCellClass(className: string) {
        if (this.cellClasses.indexOf(className) !== -1) {
            this.cellClasses.splice(this.cellClasses.indexOf(className), 1);
        }
    }
    public hasCellClass(className: string) {
        return this.cellClasses.indexOf(className) !== -1;
    }
    public addSubTextClass(className: string) {
        if (this.subTextClasses.indexOf(className) === -1) {
            this.subTextClasses.push(className);
        }
    }
    public removeSubTextClass(className: string) {
        if (this.subTextClasses.indexOf(className) !== -1) {
            this.subTextClasses.splice(this.subTextClasses.indexOf(className), 1);
        }
    }
    public hasSubTextClass(className: string) {
        return this.subTextClasses.indexOf(className) !== -1;
    }
    public addSuperTextClass(className: string) {
        if (this.superTextClasses.indexOf(className) === -1) {
            this.superTextClasses.push(className);
        }
    }
    public removeSuperTextClass(className: string) {
        if (this.superTextClasses.indexOf(className) !== -1) {
            this.superTextClasses.splice(this.superTextClasses.indexOf(className), 1);
        }
    }
    public hasSuperTextClass(className: string) {
        return this.superTextClasses.indexOf(className) !== -1;
    }
    public toggleClass(className: string) {
        if (this.textClasses.indexOf(className) === -1) {
            this.textClasses.push(className);
        }
        else {
            this.textClasses.splice(this.textClasses.indexOf(className), 1);
        }
    }
    public toggleCellClass(className: string) {
        if (this.cellClasses.indexOf(className) === -1) {
            this.cellClasses.push(className);
        }
        else {
            this.cellClasses.splice(this.cellClasses.indexOf(className), 1);
        }
    }
    public getExport() {
        let exportObj = {
            text: this.text,
            supertext: this.supertext,
            subtext: this.subtext,
            textClasses: this.textClasses.join(","),
            cellClasses: this.cellClasses.join(","),
            subTextClasses: this.subTextClasses.join(","),
            superTextClasses: this.superTextClasses.join(",")
        };
        return exportObj;
    }
}