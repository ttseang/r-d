import { CellVo } from "./CellVo";

export class RowVo
{
    public cells:CellVo[];
    public rowClasses:string[];
    constructor(cells:CellVo[],rowClasses:string[])
    {
        this.cells =cells;
        this.rowClasses = rowClasses;
    }
    public addClassToRow(className:string):void
    {
        if (this.rowClasses.indexOf(className)===-1)
        {
            this.rowClasses.push(className);
        }
    }
    public removeClassFromRow(className:string):void
    {
        let index:number = this.rowClasses.indexOf(className);
        if (index!==-1)
        {
            this.rowClasses.splice(index,1);
        }
    }
}