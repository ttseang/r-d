export class Payload
{
    public lti:string;
    public type:string;
    public eqstring:string;
    public data:any;

    constructor(lti:string,type:string,eqstring:string,data:any)
    {
        this.lti=lti;
        this.type=type;
        this.eqstring=eqstring;
        this.data=data;
    }
}