export class MainController
{
    public updateGrid:Function=()=>{};
    public updateStyle:Function=()=>{};
    public updateSelectedCell:Function=()=>{};
    public selectCell:Function=()=>{};
    public changeStep:Function=()=>{};
    public stepsUpdated:Function=()=>{};
    //public addStep:Function=()=>{};
    public adjustGrid:Function=()=>{};

    public addColumn:Function=()=>{};
    public addRow:Function=()=>{};
    public removeColumn:Function=()=>{};
    public removeRow:Function=()=>{};

    public doCalc:Function=()=>{};
    public addStepsFromCalc:Function=()=>{};
    public changePanels:Function=()=>{};
    public insertSymbol:Function=()=>{};

    public prepSaveData:Function=()=>{};
    public loadFromData:Function=()=>{};
    public quickMenuToggle:Function=()=>{};
    public toggleFlyMenu:Function=()=>{};
    
    private static _instance:MainController;
    public static getInstance():MainController
    {
        if (!MainController._instance)
        {
            MainController._instance = new MainController();
        }
        return MainController._instance;
    }
    constructor()
    {
        (window as any).mc = this;
        if (MainController._instance)
        {
            throw new Error("Error: Instantiation failed: Use SingletonDemo.getInstance() instead of new.");
        }
    }
}