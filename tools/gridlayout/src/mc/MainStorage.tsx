import { GridVo } from "../dataObjs/GridVo";
import { StyleVo } from "../dataObjs/StyleVo";
import { GridUtil } from "../util/GridUtil";
import { StyleType } from "./Constants";

export class MainStorage
{
    private static _instance:MainStorage;
    public styles:StyleVo[]=[];
    public steps:GridVo[]=[];
    public currentStep:number=0;

    public LtiFileName:string='';
    public fileName:string='';
    public saveData:any={};

    constructor()
    {
        if (MainStorage._instance)
        {
            throw new Error("Error: Instantiation failed: Use SingletonDemo.getInstance() instead of new.");
        }
        (window as any).ms=this;
        this.steps.push(GridUtil.getDefaultGrid());
     //   this.steps.push(GridUtil.getDefaultGrid());
        //set styles
        this.styles.push(new StyleVo('bottom line','underlineRow',false,StyleType.cell));
        this.styles.push(new StyleVo('top line','division-line',false,StyleType.cell));


        this.styles.push(new StyleVo('small text','smallText',false,StyleType.size));
        this.styles.push(new StyleVo('large text','largeText',false,StyleType.size));
        this.styles.push(new StyleVo('bold text','boldText',false,StyleType.font));
        this.styles.push(new StyleVo('italic text','italicText',false,StyleType.font));
        this.styles.push(new StyleVo('borrowed','strikethrough',false,StyleType.font));

        this.styles.push(new StyleVo('half-circle','halfCircle',false,StyleType.font));
       
        //colors
        this.styles.push(new StyleVo('red text','redText',false,StyleType.color));
        this.styles.push(new StyleVo('blue text','blueText',false,StyleType.color));
        this.styles.push(new StyleVo('green text','greenText',false,StyleType.color));
        this.styles.push(new StyleVo('yellow text','yellowText',false,StyleType.color));
    }
    public static getInstance():MainStorage
    {
        if (!MainStorage._instance)
        {
            MainStorage._instance = new MainStorage();
        }
        return MainStorage._instance;
    }
    public getStyleByName(name:string):StyleVo | null
    {
        for(let i=0;i<this.styles.length;i++)
        {
            if (this.styles[i].styleName===name)
            {
                return this.styles[i];
            }
        }
        return null;
    }
    public getStyleByType(type:StyleType):StyleVo[]
    {
        let styles:StyleVo[]=[];
        for(let i=0;i<this.styles.length;i++)
        {
            if (this.styles[i].type===type)
            {
                styles.push(this.styles[i]);
            }
        }
        return styles;
    }
    public prepSaveData():void
    {
        this.saveData   =   {};
        let rowArray:any[]=[];
        for (let i=0;i<this.steps.length;i++)
        {
            rowArray.push(this.steps[i].getExportData());
        }
        this.saveData.steps=rowArray;
    }
}