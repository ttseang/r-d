import React, { Component } from 'react';
import { GridVo } from '../dataObjs/GridVo';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import SGrid from '../comps/SGrid';
import { Button, ButtonGroup } from 'react-bootstrap';
import { GridUtil } from '../util/GridUtil';
interface MyProps { }
interface MyState { steps: GridVo[], selectedIndex: number }
class StepPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { steps: this.ms.steps, selectedIndex: this.ms.currentStep };
        this.mc.stepsUpdated = this.stepsUpdated.bind(this);
        this.mc.addStepsFromCalc = this.addStepsFromCalc.bind(this);
    }
    componentDidMount(): void {
        this.mc.changeStep(this.ms.currentStep);
        let steps: GridVo[] = this.ms.steps;
        if (steps.length === 0) {
            steps.push(GridUtil.getDefaultGrid());
        }
        this.setState({ steps: steps });
    }
    
    private stepsUpdated() {
        this.setState({ steps: this.ms.steps });
    }
    private cardClicked(index: number) {
        this.mc.changeStep(index);
        this.setState({ selectedIndex: index });
    }
    private getSteps() {
        //console.log("steps=" + this.state.steps.length);
        let steps: JSX.Element[] = [];
        for (let i = 0; i < this.state.steps.length; i++) {
            const key = "step" + i;
            const selected: boolean = (i === this.state.selectedIndex);
            steps.push(<SGrid key={key} selected={selected} grid={this.state.steps[i]} index={i} clickCallback={this.cardClicked.bind(this)}></SGrid>)
        }
        return steps;
    }
    private addStep() {
        this.ms.steps.push(GridUtil.getDefaultGrid());
        this.setState({ steps: this.ms.steps })
    }
    private cloneStep() {

        let grid: GridVo = this.ms.steps[this.ms.currentStep];
        let grid2: GridVo = grid.clone();
        this.ms.steps.push(grid2);
        this.setState({ steps: this.ms.steps });
    }
    private deleteStep()
    {
        if (this.ms.steps.length===0)
        {
            return;
        }
        this.ms.steps.splice(this.ms.currentStep,1);
        if (this.ms.currentStep>=this.ms.steps.length)
        {
            this.ms.currentStep=this.ms.steps.length-1;
        }
        this.setState({ steps: this.ms.steps,selectedIndex:this.ms.currentStep });
    }
    moveStepLeft() {
        if (this.ms.currentStep > 0) {
            let temp: GridVo = this.ms.steps[this.ms.currentStep - 1];
            this.ms.steps[this.ms.currentStep - 1] = this.ms.steps[this.ms.currentStep];
            this.ms.steps[this.ms.currentStep] = temp;
            this.setState({ steps: this.ms.steps });
            if(this.ms.currentStep>0)
            {
                let nstep:number=this.ms.currentStep-1;
                this.setState({ selectedIndex: nstep });
                this.ms.currentStep=nstep;
            }
        }
    }
    moveStepRight() {
        if (this.ms.currentStep < this.ms.steps.length - 1) {
            let temp: GridVo = this.ms.steps[this.ms.currentStep + 1];
            this.ms.steps[this.ms.currentStep + 1] = this.ms.steps[this.ms.currentStep];
            this.ms.steps[this.ms.currentStep] = temp;
            this.setState({ steps: this.ms.steps });
            if(this.ms.currentStep<this.ms.steps.length-1)
            {
                let nstep:number=this.ms.currentStep+1;
                this.setState({ selectedIndex: nstep });
                this.ms.currentStep=nstep;
            }
            
        }
    }
    addStepsFromCalc(steps: GridVo[]) {
        //add to current steps
        //console.log("addStepsFromCalc");
        //console.log(this.ms.steps);
        for (let i = 0; i < steps.length; i++) {
            this.ms.steps.push(steps[i]);

        }
        this.setState({ steps: this.ms.steps });
    }
    render() {
        return (<div className='stepPanel'>
            <ButtonGroup>
                <Button variant='light' onClick={this.addStep.bind(this)}>Add Step</Button>
                <Button variant='light' onClick={this.cloneStep.bind(this)}>Clone Step</Button>
                <Button variant='light' onClick={this.deleteStep.bind(this)}>Delete Step</Button>
                <Button variant='light' onClick={this.moveStepLeft.bind(this)}>Move Left</Button>
                <Button variant='light' onClick={this.moveStepRight.bind(this)}>Move Right</Button>
            </ButtonGroup>
            <div className='stepLine'>
                {this.getSteps()}
            </div>
        </div>)
    }
}
export default StepPanel;