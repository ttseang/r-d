import React, { Component, MouseEvent } from 'react';
import Hotkeys from "@teachingtextbooks/hotkeys";
import { MainController } from '../mc/MainController';
import SymbolPanel from './SymbolPanel';
interface MyProps { }
interface MyState {visible:boolean}
class QuickMenu extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {visible:false};
            Hotkeys.on("control+q", () => this.toggleMenu());

            this.mc.quickMenuToggle = this.handleMouseDown.bind(this);
            this.toggleMenu = this.toggleMenu.bind(this);
        }

        handleMouseDown(e:MouseEvent) {
            this.toggleMenu();
         
            console.log("clicked");
            e.stopPropagation();
          }

        public toggleMenu(){
            this.setState(
                {visible:!this.state.visible}
            )

            console.log('TOGGLED: ' + this.state.visible);
        }

        render() {
            var visibility:string = "hide";

            if(this.state.visible){
                visibility = "show";
            }

            return(
                <div id="flyoutMenu" className={visibility}>
                <SymbolPanel></SymbolPanel>
                </div> 
            )
        }
    
}
export default QuickMenu;