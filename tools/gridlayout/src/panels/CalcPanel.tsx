import React, { Component } from 'react';
import { MainController } from '../mc/MainController';
interface MyProps {}
interface MyState { expression: string }
class CalcPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { expression: "123+459" };
    }
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ expression: e.target.value });
    }
    onPreCalc()
    {
        this.mc.doCalc(this.state.expression);
    }
    render() {
        return (<div className='calcPanel'>
            <h2>Expression</h2>
            <input id="expressionText" type="text" value={this.state.expression} onChange={this.onChange.bind(this)} />
            <button id="calcButton" onClick={this.onPreCalc.bind(this)}>Calc</button>
        </div>)
    }
}
export default CalcPanel;