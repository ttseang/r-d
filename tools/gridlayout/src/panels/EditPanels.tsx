import { Component } from 'react';
import { Button } from 'react-bootstrap';
import { GridVo } from '../dataObjs/GridVo';
import StylePanel from './StylePanel';
import StepPanel from './StepPanel';
import LayoutPanel from './LayoutPanel';
import GridSizePanel from './GridSizePanel';

import { MainController } from '../mc/MainController';
import SymbolPanel from './SymbolPanel';
interface MyProps {gridVo:GridVo}
interface MyState {selectedIndex:number,gridVo:GridVo}
class EditPanels extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = {selectedIndex:0,gridVo:props.gridVo};
        }
    componentDidMount(): void {
        this.mc.changePanels=(index:number)=>{
            this.setState({selectedIndex:index});
        }
    }
    getTabs()
    {
        //return tabs
        const tabNames:string[]=['Steps','Styles','Layout','Sizes','Symbols'];
        let tabs:JSX.Element[]=[];
        for(let i=0;i<tabNames.length;i++)
        {
            let variant:string='light';
            if(i===this.state.selectedIndex)
            {
                variant='dark';
            }
            tabs.push(<Button key={i} className='tab' variant={variant} onClick={()=>{this.setState({selectedIndex:i})}}>{tabNames[i]}</Button>);
        }
        return <div className='tabs'>{tabs}</div>;
    }
    getPanel()
    {
        switch(this.state.selectedIndex)
        {
            case 0:
                return <StepPanel></StepPanel>
            case 1:
               // return <GridSizePanel gridVo={this.state.gridVo} sizeCallback={this.props.sizeCallback}></GridSizePanel>
               return <StylePanel></StylePanel>

            case 2:
                return <LayoutPanel></LayoutPanel>

            case 3:
                return <GridSizePanel></GridSizePanel>
            case 4:
                return <SymbolPanel></SymbolPanel>
           
                
            default:
                return <div>Not found</div>
        }
    }
    render() {
        return (<div className='toolArea'>{this.getTabs()}
            <div>{this.getPanel()}</div>
        </div>)
    }
}
export default EditPanels;