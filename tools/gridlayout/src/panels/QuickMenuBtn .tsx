import React, { Component } from "react";
import { MainController } from "../mc/MainController";
interface MyState {visible:boolean}
 
class QuickMenuBtn extends React.Component<{},{visible:boolean}> {

    private mc:MainController=MainController.getInstance();
    
    constructor(state:MyState) {
        super(state);
        this.state = {visible:false};
    }

  render() {
    var visibility:string = "hide";

    if(this.state.visible){
        visibility = "show";
    }
    return (
      <button id="flyMenuButton" onClick={event => this.mc.quickMenuToggle(event, "zoomTool")}>Symbols</button>
    );
  }
}
 
export default QuickMenuBtn;