import { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState { mode: number }
class GridSizePanel extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0 };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        /*  if (prevProps.gridVo.rowCount !== this.props.gridVo.rowCount || prevProps.gridVo.colCount !== this.props.gridVo.colCount) {
             this.setState({ rows: this.props.gridVo.rowCount, cols: this.props.gridVo.colCount });
         } */
    }
    getPanel() {
        switch (this.state.mode) {
            case 0:
                return (
                    <div className='sizeNav'>
                        <div></div>
                        <div><span className='navLabel'>Top Row</span><br /><Button className='btn-success' onClick={() => { this.mc.addRow(true) }}>Add</Button><Button className='btn-danger' onClick={() => { this.mc.removeRow(true); }}>Remove</Button></div>
                        <div></div>
                        <div><span className='navLabel'>Left Column</span><br /><Button className='btn-success' onClick={() => { this.mc.addColumn(true) }}>Add</Button><Button className='btn-danger' onClick={() => { this.mc.removeColumn(true) }}>Remove</Button></div>
                        <div className='navLabel'>Add or Remove <br />Rows and Columns</div>
                        <div><span className='navLabel'>Right Column</span><br /><Button className='btn-success' onClick={() => { this.mc.addColumn() }}>Add</Button><Button className='btn-danger' onClick={() => { this.mc.removeColumn() }}>Remove</Button></div>
                        <div></div>
                        <div><span className='navLabel'>Bottom Row</span><br /><Button className='btn-success' onClick={() => { this.mc.addRow() }}>Add</Button><Button className='btn-danger' onClick={() => { this.mc.removeRow() }}>Remove</Button></div>
                        <div></div>
                    </div>
                )
               

            case 1:
                return (<div className='insertNav'>
                    <div></div>
                    <div><Button className='btn-success' onClick={() => { this.mc.adjustGrid(8) }}>Insert Above</Button></div>
                    <div></div>
                    <div><Button className='btn-success' onClick={() => { this.mc.adjustGrid(10) }}>Insert Left</Button></div>
                    <div className='navLabel'>Insert Rows and Columns <br />At Selected Cell</div>
                    <div><Button className='btn-success' onClick={() => { this.mc.adjustGrid(11) }}>Insert Right</Button></div>
                    <div></div>
                    <div><Button className='btn-success' onClick={() => { this.mc.adjustGrid(9) }}>Insert Below</Button></div>
                    <div></div>
                </div>)

            case 2:
                return (<div className='insertNav'>
                    <div></div>
                    <div><Button className='btn-danger' onClick={() => { this.mc.adjustGrid(12) }}>Delete Row</Button></div>
                    <div></div>
                    <div><Button className='btn-danger' onClick={() => { this.mc.adjustGrid(13) }}>Delete Column</Button></div>
                    <div className='navLabel'>Delete Rows and Columns <br />At Selected Cell</div>
                    
                    <div></div>
                </div>);
        }
    }
    getControls() {
        let gridVariant: string = (this.state.mode === 0) ? 'dark' : 'light';
        let insertVariant: string = (this.state.mode === 1) ? 'dark' : 'light';
        let deleteVariant: string = (this.state.mode === 2) ? 'dark' : 'light';
        //make 2 number inputs for rows and cols
        //make a button to set the grid size
        return (
            <div>
                <ButtonGroup>
                    <Button variant={gridVariant} onClick={() => { this.setState({ mode: 0 }) }}>Grid Size</Button>
                    <Button variant={insertVariant} onClick={() => { this.setState({ mode: 1 }) }}>Insert</Button>
                    <Button variant={deleteVariant} onClick={() => { this.setState({ mode: 2 }) }}>Delete</Button>
                </ButtonGroup>
                {this.getPanel()}
            </div>
        )
    }
    render() {
        return (<div>{this.getControls()}</div>)
    }
}
export default GridSizePanel;