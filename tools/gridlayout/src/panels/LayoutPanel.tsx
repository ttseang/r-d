import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState { }
class LayoutPanel extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    private adjustGrid(direction: number) {
        this.mc.adjustGrid(direction);
    }
    render() {
        return (<div>
            <div className='buttonNav'>
                <div></div>
                <div><Button onClick={() => { this.adjustGrid(0) }}>Up</Button></div>
                <div></div>
                <div><Button onClick={() => { this.adjustGrid(2) }}>Left</Button></div>
                <div className='navLabel'>Move All Content</div>
                <div><Button onClick={() => { this.adjustGrid(3) }}>Right</Button></div>
                <div></div>
                <div> <Button onClick={() => { this.adjustGrid(1) }}>Down</Button></div>
                <div></div>
            </div>
            <hr/>
            <div className='buttonNav'>
                <div></div>
                <div><Button onClick={() => { this.adjustGrid(4) }}>Column Up</Button></div>
                <div></div>
                <div><Button onClick={() => { this.adjustGrid(6) }}>Row Left</Button></div>
                <div className='navLabel'>Move Row or Column</div>
                <div><Button onClick={() => { this.adjustGrid(7) }}>Row Right</Button></div>
                <div></div>
                <div> <Button onClick={() => { this.adjustGrid(5) }}>Column Down</Button></div>
                <div></div>
            </div>
        </div>)
    }
}
export default LayoutPanel;