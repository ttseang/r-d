import React, { Component } from 'react';
import { MathSymbols } from '../mc/Constants';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState {}
class SymbolPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    getSymbolList() {
        //maths symbols
        let symbols: string[] = [MathSymbols["+"], MathSymbols["-"], MathSymbols["*"], MathSymbols['/'], MathSymbols.sqrt, MathSymbols.infinity, MathSymbols.integral, MathSymbols.sum, MathSymbols.product, MathSymbols.delta, MathSymbols.epsilon, MathSymbols.theta, MathSymbols.pi, MathSymbols.sigma, MathSymbols.phi, MathSymbols.alpha, MathSymbols.beta, MathSymbols.gamma, MathSymbols.lambda, MathSymbols.mu, MathSymbols.rho]
        let symbolList: JSX.Element[] = [];
        for (let i = 0; i < symbols.length; i++) {
            symbolList.push(<button key={'button'+i} className="symbol" onClick={()=>{
                this.mc.insertSymbol(symbols[i]);
            }}>{symbols[i]}</button>);
        }
        return symbolList;
    }
    render() {
        return (<div>
            <div className="symbolList">
                {this.getSymbolList()}
            </div>
        </div>)
    }
}
export default SymbolPanel;