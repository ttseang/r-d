import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import { CellVo } from '../dataObjs/CellVo';
import { StyleVo } from '../dataObjs/StyleVo';
import { StyleType } from '../mc/Constants';
interface MyProps { }
interface MyState { selectedCell: CellVo | null }
class StylePanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { selectedCell: null };
        this.mc.updateSelectedCell = this.cellSelected.bind(this);
    }
    onStyleClick(styleVo: StyleVo) {
       
        this.mc.updateStyle(styleVo);
    }
    cellSelected(cellVo: CellVo) {
        this.setState({ selectedCell: cellVo });
    }
    getButtons(style: StyleType) {
        let styles: StyleVo[] = this.ms.getStyleByType(style);
        //console.log(styles);

        let buttons: JSX.Element[] = [];
        for (let i = 0; i < styles.length; i++) {
            let key: string = styles[i].styleName;
            let name: string = styles[i].styleName;
            let style: string = styles[i].styleValue;
            if (this.state.selectedCell) {
                //if the selected cell has the same style as the button, then add a checkmark
                if (this.state.selectedCell.textClasses.includes(style)) {
                    name += ' ✓';
                }
                else {
                    name += ' ✗';
                }
            }

            buttons.push(<button key={key} onClick={() => {
                this.onStyleClick(styles[i]);
            }}>{name}</button>)
        }
        return buttons;
    }
    render() {
        return (<div>
            <div className="styleButtons">
                <div><h2>Rows</h2>{this.getButtons(StyleType.cell)}</div>
                <div><h2>Fonts</h2>{this.getButtons(StyleType.font)}</div>
                <div><h2>Colors</h2>{this.getButtons(StyleType.color)}</div>
                <div><h2>Size</h2>{this.getButtons(StyleType.size)}</div>
            </div>
        </div>)
    }
}
export default StylePanel;