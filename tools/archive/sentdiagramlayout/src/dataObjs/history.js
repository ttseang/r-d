import { HistoryVo } from "./historyVo";

export class History {
    constructor() {
        //
        //
        //
        History.TYPE_MOVE = "move";
        History.TYPE_MULTI_MOVE = "multi_move";
        History.TYPE_ADD = "added";
        History.TYPE_UPDATED_BOX = "updatedBox";
        //
        //
        //
        this.historyArray = [];
        // this.historyIndex=0;
        this.future = [];
        this.inital = null;
    }
    addHistory(snap) {
        if (this.inital == null) {
            this.inital = snap;
        }
        
        this.historyArray.push(snap);
        this.future = [];

    }
    undo() {
        if (this.historyArray.length == 0) {
            return this.inital;
        }
        let snap = this.historyArray.pop();
        this.future.push(snap);
        let current = this.historyArray[this.historyArray.length - 1];
        return current;
    }
    redo() {
        let snap = this.future.pop();
        if (snap) {
            this.historyArray.push(snap);
        }       
        return snap;
    }
}
export default History;