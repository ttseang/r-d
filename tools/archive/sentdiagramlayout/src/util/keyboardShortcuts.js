export class KeyboardShortcuts
{
    constructor(scene)
    {
        this.scene=scene;
        this.scene.input.keyboard.on('keydown',this.getKey.bind(this));
        this.scene.input.keyboard.on('keyup',this.keyUp.bind(this));
        this.keylock=false;
    }
    getKey(e)
    {
        if (this.keylock==true)
        {
            return;
        }
       // console.log(e);
        if (e.key=="Delete")
        {
            this.scene.deleteBox();
        }
        if (e.key=="ArrowRight")
        {
            this.scene.nudge(1.01,0);
        }
        if (e.key=="ArrowLeft")
        {
            this.scene.nudge(-1,0);
        }
        if (e.key=="ArrowUp")
        {
            this.scene.nudge(0,-1);
        }
        if (e.key=="ArrowDown")
        {
            this.scene.nudge(0,1);
        }
        if (e.ctrlKey==true)
        {
           
            if (e.key=="z")
            {
                this.keylock=true;
                this.scene.undo();
            }
            if (e.key=="y")
            {
                this.keylock=true;
                this.scene.redo();
            }
        }
    }
    keyUp()
    {
        
        this.keylock=false;
    }
}