//import Phaser from 'phaser';
import {SceneMain} from "./scenes/sceneMain";
import {SceneFonts} from "./scenes/sceneFont";


var isMobile = navigator.userAgent.indexOf("Mobile");
 if (isMobile == -1) {
     isMobile = navigator.userAgent.indexOf("Tablet");
 }
 var w = 640;
 var h = 480;
 //
 //
 if (isMobile != -1) {
     w = window.innerWidth;
     h = window.innerHeight;
 }

const config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: w,
    height: h,
    scene: [SceneFonts,SceneMain]
};

const game = new Phaser.Game(config);
