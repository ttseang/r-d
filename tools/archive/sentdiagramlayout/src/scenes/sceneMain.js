import {
    SelectBox
} from "../comps/selectBox";
import WordBox from "../comps/wordBox";
import {
    AlignGrid
} from "../util/alignGrid";
import History from "../dataObjs/history";

import { BoxUtil } from "../util/boxUtil";
import { KeyboardShortcuts } from "../util/keyboardShortcuts";
export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
        this.SINGLE_SELECT = 0;
        this.MULTI_SELECT = 1;
        this.DRAG_SELECT = 2;
        this.MULTI_DRAG = 3;
        this.selectMode = this.SINGLE_SELECT;
    }
    preload() {
        this.load.image("holder", "./assets/images/holder.jpg");
        this.load.image("tri", "./assets/images/tri2.png");
    }
    create() {
        window.parent.document.getElementById('bridge').scene = this;
        this.updateReact = null;
        //let d:any= window.parent.document.getElementById('checker');
        this.gw = this.sys.game.config.width;
        this.gh = this.sys.game.config.height;
        this.back = this.add.image(0, 0, "holder").setOrigin(0, 0);
        this.back.displayWidth = this.gw;
        this.back.displayHeight = this.gh;
        this.back.setInteractive();
        this.back.alpha = 0.1;
        this.history = new History();

        //
        //
        //
        this.allBoxes = [];
        this.mSelect = [];
        //
        //
        //
        this.mStartY = 0;
        this.mStartY = 0;
        //
        //
        //
        this.graphics = this.add.graphics();
        this.graphics.lineStyle(5, 0x27ae60, 1);
        //
        //
        //
        this.grid = new AlignGrid({
            scene: this,
            rows: 22,
            cols: 44
        });
        this.grid.show();
        //
        //
        //
        this.cw = this.grid.cw;
        this.ch = this.grid.ch;
        this.cd = this.grid.cd;
        //
        //
        //
        window.scene = this;
        this.currentBox = null;
        //  this.allBoxes = [];
        let wordString = window.parent.document.getElementById('words').innerText;
        this.words = wordString.split(" ");
        for (let i = 0; i < this.words.length; i++) {
            let yy = (this.grid.rows - 2) - (this.words.length - i);
            let word = this.words[i];
            word = word.replace("_", " ");
            console.log(word);
            let box = this.makeBox(word, 2, yy, 6, "h");
            this.snapBoxToGrid(box);
        }
        //    this.makeBox("test", 2, 2, 4, "h");
        //  this.selectSquare=this.add.image(0,0,"holder").setOrigin(0,0);
        this.selectSquare = new SelectBox(this, "holder").setOrigin(0, 0);
        this.selectSquare.alpha = 0.1;
        this.selectSquare.setTint(0x2ecc71);
        this.selectSquare.visible = false;
        this.setSelectMode(this.SINGLE_SELECT);
        this.saveHistory();
        //
        //
        //
        this.keyboardShortcuts = new KeyboardShortcuts(this);
    }
    setSelectMode(mode) {
        this.selectMode = mode;
        this.input.off('pointermove');
        this.input.off('pointerup');
        this.input.off('pointerdown');
        if (this.updateReact) {
            this.updateReact("1", "modechanged|" + mode);
        }
        switch (mode) {
            case this.SINGLE_SELECT:
                this.selectSquare.visible = false;
                this.mSelect = [];
                this.deselectAll();
                this.back.on('pointerdown', this.unselect.bind(this));
                break;
            //
            //
            //
            case this.MULTI_SELECT:
                this.deselectAll();
                this.back.on('pointerdown', this.startMultiSelect.bind(this));
                break;
            //
            //
            //
            case this.DRAG_SELECT:
                this.back.on('pointerup', this.selectMultiEnd.bind(this));
                break;
            //
            //
            //
            case this.MULTI_DRAG:
                this.selectSquare.visible = true;
                this.back.on('pointerdown', this.endMulti.bind(this));
                this.selectSquare.on('pointerup', this.endMulti.bind(this));
                break;
        }
    }
    unselect() {
        this.deselectAll();
        this.currentBox = null;
    }
    deselectAll() {

        this.allBoxes.forEach((child) => {
            if (child) {
                child.setSelected(false);
            }
        });
        this.currentBox = null;
        if (this.updateReact) {
            this.updateReact("2", "");
        }
    }
    startMultiSelect(pointer) {
        //console.log(obj);
        this.back.off('pointerdown');
        this.mStartX = pointer.x;
        this.mStartY = pointer.y;
        this.setSelectMode(this.DRAG_SELECT);
    }
    endMulti(pointer) {
        this.selectSquare.off('pointerup');
        this.back.off('pointerdown');
        this.selectSquare.unselect();
        this.selectSquare.visible = false;
        this.setSelectMode(this.SINGLE_SELECT);
        this.deselectAll();
        this.snapAll();
        this.saveHistory();
    }
    snapAll() {
        this.allBoxes.forEach((box) => { this.snapBoxToGrid(box); });
    }
    selectMultiEnd(pointer) {
        this.mEndX = pointer.x;
        this.mEndY = pointer.y;
        console.log(pointer);
        this.back.off('pointerup');
        this.findBoxesUnderSelect();
        this.selectSquare.setUp();
        this.setSelectMode(this.MULTI_DRAG);
    }
    findBoxesUnderSelect() {
        this.mSelect = [];

        let corners = BoxUtil.findEdges(this.mStartX, this.mStartY, this.mEndX, this.mEndY);

        let len = this.allBoxes.length;
        for (let i = 0; i < len; i++) {
            let box = this.allBoxes[i];
            if (box) {
                box.setSelected(false);
                if (box.right > corners.x1 - 1 && box.x < corners.x2 + 1) {
                    if (box.bottom > corners.y1 - 1 && box.y < corners.y2 + 1) {
                        box.setSelected(true);
                        this.mSelect.push(box);
                    }
                }
            }
        }
        if (this.mSelect.length == 0) {
            this.setSelectMode(this.SINGLE_SELECT);
            this.selectSquare.visible = false;
            return;
        }
        this.selectSquare.setChildren(this.mSelect);
    }
    findUnderDragging() {
        let corners = BoxUtil.findEdges(this.mStartX, this.mStartY, this.input.activePointer.x, this.input.activePointer.y);
        let len = this.allBoxes.length;
        for (let i = 0; i < len; i++) {
            let box = this.allBoxes[i];
            if (box) {
                box.setSelected(false);
                if (box.right > corners.x1 - 1 && box.x < corners.x2 + 1) {
                    if (box.bottom > corners.y1 - 1 && box.y < corners.y2 + 1) {
                        box.setSelected(true);
                    }
                }
            }
        }
    }
    selectCurrent(box, pointer) {
        //  console.log(this.input.activePointer);
        if (this.selectMode != this.SINGLE_SELECT) {
            return;
        }
        if (this.currentBox) {
            this.currentBox.setSelected(false);
        }
        this.graphics.clear();
        this.currentBox = box;
        //  this.currentBox.alpha = 0.5;
        this.currentBox.setSelected(true);

        this.diffX = pointer.downX - this.currentBox.x;
        this.diffY = pointer.downY - this.currentBox.y;

        this.input.on('pointermove', this.dragBox.bind(this));
        this.input.on('pointerup', this.snapToGrid.bind(this));
    }
    dragBox(pointer) {
        if (this.currentBox) {
            this.currentBox.x = this.input.activePointer.x - this.diffX;
            this.currentBox.y = this.input.activePointer.y - this.diffY;
        }
    }
    snapToGrid() {
        if (!this.currentBox) {
            return;
        }
        let xx = this.currentBox.x;
        let yy = this.currentBox.y;
        let coord = this.grid.findNearestGridXY(xx, yy);
        this.grid.placeAt2(coord.x, coord.y, this.currentBox);
        this.currentBox.config.xPos = coord.x;
        this.currentBox.config.yPos = coord.y;
        this.currentBox.alpha = 1;
        //
        //
        this.input.off('pointermove');
        this.input.off('pointerup');
        if (this.updateReact) {
            this.updateReact("0", this.currentBox.config);
        }
        this.saveHistory();
        // this.history.addHistory(History.TYPE_MOVE,this.oldVal,newVal);
        // this.updateGraphics();
    }
    snapBoxToGrid(box) {
        let xx = box.x;
        let yy = box.y;
        let coord = this.grid.findNearestGridXY(xx, yy);
        box.config.xPos = coord.x;
        box.config.yPos = coord.y;
        this.grid.placeAt2(coord.x, coord.y, box);
    }
    makeBox(word, xx, yy, len = 1, line = "h") {
        if (isNaN(len)) {
            len = 1;
        }
        let wordBox = new WordBox({
            scene: this,
            word: word,
            len: len,
            line: line,
            dragCallback: this.selectCurrent.bind(this)
        });

        //,dragCallback:this.selectCurrent.bind(this) });
        this.grid.placeAt(xx, yy, wordBox);
        window.wordBox = wordBox;
        wordBox.index = this.allBoxes.length;
        this.allBoxes.push(wordBox);
        return wordBox;
    }
    updateBoxProp(prop, value) {
        if (this.currentBox == null) {
            return;
        }
        let box = this.currentBox;
        let config = box.config;
        console.log(config);
        switch (prop) {
            case "len":
                config.len = value;
                break;
            case "word":
                config.word = value;
                break;
            case "line":
                config.line = value;
                break;
        }
        box.destroy();
        let wordBox = new WordBox(config);
        wordBox.index = this.allBoxes.length;
        this.allBoxes.push(wordBox);
        this.grid.placeAt2(config.xPos, config.yPos, wordBox);
        this.currentBox = wordBox;
        //this.updateGraphics();
    }
    updateAllProps(configdata) {
        if (!this.currentBox) {
            return;
        }

        let config = JSON.parse(configdata);

        this.currentBox.destroy();
        config.scene = this;
        config.dragCallback = this.selectCurrent.bind(this);
        let wordBox = new WordBox(config);
        wordBox.index = this.allBoxes.length;
        this.allBoxes.push(wordBox);
        this.grid.placeAt2(config.xPos, config.yPos, wordBox);
        this.currentBox = wordBox;
        this.saveHistory();
    }

    updateGraphics() {
        this.graphics.clear();
        this.children.list.forEach((child) => {
            if (child.type == "wordBox") {
                console.log(child);
                child.drawGraphics(child.config.line);
            }
        });
    }
    getInfo() {
        let infoArray = [];
        this.children.list.forEach((child) => {
            if (child.type == "wordBox") {
                //  console.log(child.getInfo());
                infoArray.push(child.getInfo());
            }
        });
        return infoArray;
    }
    saveHistory() {
        console.log("save history");
        let snap = this.getInfo();
        this.history.addHistory(snap);

    }
    undo() {
        if (this.history.historyArray.length < 1) {
            return;
        }
        let historyObj = this.history.undo();
        if (historyObj) {
            this.destroyAllWordBoxes();
            this.rebuild(historyObj);
        }
    }
    redo() {
        let historyObj = this.history.redo();
        console.log(historyObj);
        if (historyObj) {
            this.destroyAllWordBoxes();
            this.rebuild(historyObj);
        }
    }
    rebuild(historyObj) {
        for (let i = 0; i < historyObj.length; i++) {
            let config = historyObj[i];
            //
            //
            //
            config.scene = this;
            config.dragCallback = this.selectCurrent.bind(this);
            //
            //
            //           
            let wordBox = new WordBox(config);
            wordBox.index = this.allBoxes.length;
            this.allBoxes.push(wordBox);
            this.grid.placeAt2(config.xPos, config.yPos, wordBox);
            this.currentBox = wordBox;
        }
    }
    destroyAllWordBoxes() {
        this.allBoxes.forEach((box) => { box.destroy(); });
        this.allBoxes = [];
    }
    destroySingle(index) {
        let box = this.allBoxes[index];
        console.log(box);
        box.destroy();
        this.allBoxes.splice(index, 1);
        this.renumberBoxes();
        this.saveHistory();
    }
    deleteBox() {

        if (this.selectMode == this.SINGLE_SELECT) {
            if (this.currentBox) {
                this.destroySingle(this.currentBox.index);
                this.currentBox = null;
            }
        }
        else {
            this.destroyMulti();
        }
    }
    copyBox()
    {
        if (this.currentBox) {
            let config=this.currentBox.config;
            console.log(config);

            let copyBox=this.makeBox(config.word,config.xPos+1,config.yPos+1,config.len,config.line);
            this.snapBoxToGrid(copyBox);
            
            this.currentBox.setSelected(false);
            copyBox.setSelected(true);
            this.currentBox=copyBox;            
        }
    }
    renumberBoxes() {
        for (let i = 0; i < this.allBoxes.length; i++) {
            this.allBoxes[i].index = i;
        }
    }
    destroyMulti() {
        for (let i = 0; i < this.mSelect.length; i++) {
            let index = this.mSelect[i].index;
            this.mSelect[i].destroy();
            this.allBoxes.splice(index, 1);
        }
        this.renumberBoxes();
        this.saveHistory();
        this.setSelectMode(this.SINGLE_SELECT);
    }
    nudge(xx, yy) {
        if (this.selectMode != this.SINGLE_SELECT) {

            let corners = BoxUtil.findCorners(this.mSelect);

            let x1 = corners.x1 + this.cw * xx;
            let x2 = corners.x2 + this.cw * xx;
            let y1 = corners.y1 + this.ch * yy;
            let y2 = corners.y2 + this.ch * yy;

            if (x1 < 0 || x2 > this.gw || y1 < 0 || y2 > this.gh) {
                return;
            }

            for (let i = 0; i < this.mSelect.length; i++) {
                let box = this.mSelect[i];
                box.x += this.cw * xx;
                box.y += this.ch * yy;
                this.snapBoxToGrid(box);
            }
            this.selectSquare.x += this.cw * xx;
            this.selectSquare.y += this.ch * yy;

            return;
        }
        //
        //
        //
        if (this.currentBox == null) {
            return;
        }
        let x1 = this.currentBox.x + this.cw * xx;
        let x2 = this.currentBox.right + this.cw * xx;
        let y1 = this.currentBox.y + this.ch * yy;
        let y2 = this.currentBox.bottom + this.ch * yy;
        //
        //
        //
        if (x1 < 0 || x2 > this.gw || y1 < 0 || y2 > this.gh) {
            return;
        }


        this.currentBox.x += this.cw * xx;
        this.currentBox.y += this.ch * yy;

        this.snapBoxToGrid(this.currentBox);
        this.saveHistory();
    }
    update() {
        if (this.selectMode == this.DRAG_SELECT) {
            this.selectSquare.draw(this.mStartX, this.mStartY, this.input.activePointer.x, this.input.activePointer.y);
            this.findUnderDragging();
            this.selectSquare.visible = true;
        }
        if (this.selectMode == this.MULTI_DRAG) {
            this.selectSquare.update();
        }
    }
}