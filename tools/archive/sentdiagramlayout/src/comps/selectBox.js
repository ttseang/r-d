import { BoxUtil } from "../util/boxUtil";

export class SelectBox extends Phaser.GameObjects.Sprite {
    constructor(scene, key) {
        super(scene, 0, 0, key);
        scene.add.existing(this);
        this.scene = scene;
        this.diffX = 0;
        this.diffY = 0;
        this.children = [];
    }

    setDragMode(val) {
        this.dragMode = val;
        if (val == true) {

        }
    }
    setUp() {
        this.on('pointerdown', this.selectMe.bind(this));
        this.setInteractive();

    }
    selectMe() {
        this.off('pointerdown');
        this.diffX = this.scene.input.activePointer.x - this.x;
        this.diffY = this.scene.input.activePointer.y - this.y;
        this.on('pointerup', this.mouseUp.bind(this));
        this.setDragMode(true);
    }
    inBound(xx, yy) {
        let endX = this.x + this.displayWidth;
        let endY = this.y + this.displayHeight;

        if (xx > this.x - 1 && this.x < endX + 1) {
            if (yy > this.y - 1 && this.y < endY + 1) {
                return true;
            }
        }
        return false;
    }
    setChildren(children) {
        this.children = children;


        let corners = BoxUtil.findCorners(children);

        this.x = corners.x1;
        this.y = corners.y1;

        let ww = corners.x2 - corners.x1;
        let hh = corners.y2 - corners.y1;

        this.displayWidth = ww;
        this.displayHeight = hh;

        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            child.diffX = child.x - this.x;
            child.diffY = child.y - this.y;
        }
    }
    draw(startX,startY,endX,endY)
    {
        //console.log(startX,startY,endX,endY);
        let corners=BoxUtil.findEdges(startX,startY,endX,endY);
        //console.log(corners);
        this.x = corners.x1;
        this.y = corners.y1;

        let ww = corners.x2 - corners.x1;
        let hh = corners.y2 - corners.y1;

        this.displayWidth = ww;
        this.displayHeight = hh;
    }
    updateChildren() {
        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];
            child.x = this.x + child.diffX;
            child.y = this.y + child.diffY;
        }
    }
    mouseUp() {
        this.on('pointerdown', this.selectMe.bind(this));
        this.dragMode = false;
    }
    unselect() {
        this.off('pointerdown');
        this.off('pointerup');
        this.dragMode = false;
    }
    update() {
        if (this.dragMode == true) {
            this.x = this.scene.input.activePointer.x - this.diffX;
            this.y = this.scene.input.activePointer.y - this.diffY;
            this.updateChildren();
        }
    }
}