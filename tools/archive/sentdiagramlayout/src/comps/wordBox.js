import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";

export class WordBox extends Phaser.GameObjects.Container {
    constructor(config) {
        super(config.scene);
        // console.log(config);
        //
        //
        this.scene = config.scene;
        this.config = config;
        this.word = config.word;
        this.type = "wordBox";
        this.lines = [];
        //
        //
        this.back = this.scene.add.sprite(0, 0, "holder").setOrigin(0, 0);
        this.back.displayHeight = this.scene.ch * 1.5;

        if (config.line !== "d") {
            this.back.displayWidth = this.scene.cw * config.len;
        }
        else {
            this.back.displayWidth = this.scene.cd * config.len;
        }
        if (config.line == "c") {
            this.back.displayHeight = this.scene.ch * config.len;
            this.back.displayWidth = this.scene.cw * 1.5;

        }
        //this.back.setTint(0xff0000);
        this.back.alpha = 0.01;
        //  this.back.visible = false;
        this.add(this.back);

        this.selectBox = this.scene.add.image(0, 0, "holder").setOrigin(0, 0);
        this.selectBox.displayHeight = this.back.displayHeight;
        this.selectBox.displayWidth = this.back.displayWidth;
        this.selectBox.setTint(0x45aaf2);
        this.selectBox.alpha = 0.3;
        this.add(this.selectBox);
        this.selectBox.visible = false;


        //
        //
        //
        if (config.word) {
            this.textObj = this.scene.add.text(0, 0, config.word, { color: '#ffffff', fontSize: '26px' });
            this.add(this.textObj);
            //
            //
            //
            this.textObj.y = this.back.displayHeight - this.textObj.displayHeight - this.scene.ch * 0.15;
            this.textObj.x = this.back.displayWidth / 2 - this.textObj.displayWidth / 2;
            if (config.line == "c") {
                this.textObj.y = this.back.displayHeight / 2;
                this.textObj.x = this.back.displayWidth / 2;
                this.textObj.setOrigin(0.5, 0.5);
                this.textObj.setAngle(-90);
            }
            if (this.textObj.displayWidth > this.back.displayWidth) {
                this.back.displayWidth = this.textObj.displayWidth;
                this.selectBox.displayWidth = this.textObj.displayWidth;
            }
        }

        //
        //
        //
        this.back.setInteractive();
        this.back.on("pointerdown", this.clickMe.bind(this));

        this.drawLine(config.line);
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.scene.add.existing(this);
    }
    clickMe(pointer) {
        console.log(pointer);
        this.config.dragCallback(this, pointer);
    }
    changeLineLen(len) {
        this.lines.forEach((line) => { line.destroy(); });

        this.config.len = len;
        let config = this.config;

        if (config.line !== "d") {
            this.back.displayWidth = this.scene.cw * config.len;
        }
        else {
            this.back.displayWidth = this.scene.cd * config.len;
        }
        if (config.line == "c") {
            this.back.displayHeight = this.scene.ch * config.len;
            this.back.displayWidth = this.scene.cw * 1.5;

        }

        this.drawLine(this.config.lineStyle);
    }
    drawLine(lineStyle) {

        if (lineStyle == "h" || lineStyle == "tvsep" || lineStyle == "vsep") {


            let lineObj = this.scene.add.image(0, 0, "holder").setOrigin(0, 0);
            lineObj.displayHeight = this.scene.ch * 0.1;
            lineObj.displayWidth = this.scene.cw * this.config.len;
            lineObj.y = this.back.displayHeight - lineObj.displayHeight / 2;
            lineObj.setTint(0x27ae60);
            this.lines.push(lineObj);
            this.add(lineObj);
        }

        if (lineStyle == "tvsep") {
            let lineObj2 = this.scene.add.image(0, 0, "holder").setOrigin(0.5, 0);
            lineObj2.displayHeight = this.scene.ch * 1.5;
            lineObj2.displayWidth = this.scene.cw * 0.125;
            //this.back.visible = true;
            lineObj2.x = this.back.displayWidth / 2;
            lineObj2.y=this.back.displayHeight/3;
            lineObj2.setTint(0x27ae60);
            this.lines.push(lineObj2);
            this.add(lineObj2);
        }
        if (lineStyle == "vsep") {
            let lineObj2 = this.scene.add.image(0, 0, "holder").setOrigin(0.5, 0);
            lineObj2.displayHeight = this.scene.ch;
            lineObj2.displayWidth = this.scene.cw * 0.125;
            //this.back.visible = true;
            lineObj2.x = this.back.displayWidth / 2;
            lineObj2.y=this.back.displayHeight/3;
            lineObj2.setTint(0x27ae60);
            this.lines.push(lineObj2);
            this.add(lineObj2);
        }

        if (lineStyle == "d") {
            let lineObj = this.scene.add.image(0, 0, "holder").setOrigin(0, 0);
            lineObj.displayHeight = this.scene.ch * 0.1;
            lineObj.displayWidth = this.scene.cd * this.config.len;
            lineObj.y = this.back.displayHeight - lineObj.displayHeight / 2;
            lineObj.x -= this.scene.ch;

            lineObj.setTint(0x27ae60);
            this.add(lineObj);
            this.lines.push(lineObj);
            this.setAngle(45);
        }
        if (lineStyle == "c") {
            let lineObj4 = this.scene.add.image(0, 0, "tri").setOrigin(0, 0);
            lineObj4.displayHeight = this.scene.ch * 6;
            lineObj4.displayWidth = this.scene.cw * 5;
            Align.centerToObj(lineObj4, this.back);
            this.add(lineObj4);
            this.lines.push(lineObj4);
        }
        if (lineStyle == "cend") {
            let lineObj4 = this.scene.add.image(0, 0, "tri").setOrigin(0, 0);

            lineObj4.displayHeight = this.scene.ch * 6;
            lineObj4.displayWidth = this.scene.cw * 5;
            Align.centerToObj(lineObj4, this.back);
            lineObj4.flipX = true;
            lineObj4.y += this.scene.ch / 2;

            this.add(lineObj4);
            this.lines.push(lineObj4);
        }
    }

    drawGraphics(lineStyle) {
        console.log(lineStyle);
        let graphics = this.scene.graphics;
        if (lineStyle == "c") {

            let h1 = this.scene.ch * this.config.len;

            let sx1 = this.x - this.scene.cw;
            let sy1 = this.y + h1 / 2;

            let sx2 = this.x + this.scene.cw;
            let sy2 = this.y;

            let sx3 = this.x + this.scene.cw;
            let sy3 = this.y + h1;



            graphics.beginPath();
            graphics.lineBetween(sx1, sy1, sx2, sy2);
            graphics.lineBetween(sx1, sy1, sx3, sy3);

            graphics.closePath();
            graphics.strokePath();

        }
        if (lineStyle == "cend") {
            let h1 = this.scene.ch * this.config.len;

            let sx1 = this.x + this.scene.cw;
            let sy1 = this.y + h1 / 2;

            let sx2 = this.x - this.scene.cw;
            let sy2 = this.y;

            let sx3 = this.x - this.scene.cw;
            let sy3 = this.y + h1;



            graphics.beginPath();
            graphics.lineBetween(sx1, sy1, sx2, sy2);
            graphics.lineBetween(sx1, sy1, sx3, sy3);

            graphics.closePath();
            graphics.strokePath();
        }

    }
    turnNormal() {
        if (this.textObj) {
            this.textObj.setColor("#ffffff");
        }
    }
    turnSelected() {
        if (this.textObj) {
            this.textObj.setColor("#ff0000");
        }
    }
    hideWord() {
        if (this.textObj) {
            this.ox = this.textObj.x;
            this.textObj.x = this.config.len * this.scene.cw / 2;
            this.textObj.setText("?");
        }
    }
    setSelected(val) {
        this.selectBox.visible = val;
    }
    get right() {
        return this.x + this.displayWidth;
    }
    get bottom() {
        return this.y + this.displayHeight;
    }
    showWord() {
        if (this.textObj) {
            this.textObj.x = this.ox;
            this.textObj.setText(this.word);
            this.turnNormal();
        }
    }
    getInfo() {
        return { word: this.config.word, len: this.config.len, line: this.config.line, xPos: this.config.xPos, yPos: this.config.yPos }
    }
}
export default WordBox;