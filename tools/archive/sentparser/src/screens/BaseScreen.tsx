import React, { Component } from "react";
import { Card } from "react-bootstrap";
import { StartData } from "../util/StartData";

import InputScreen from "./InputScreen";
//import OutputScreen from "./OutputScreen";
interface MyProps {}
interface MyState {
  mode:number
}
class BaseScreen extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);
    this.state = { mode:0 };
    
  }
  componentDidMount()
  {
      let startData:StartData=new StartData(this.dataloaded.bind(this));
      startData.loadVerbs(); 
  }
 getScreen()
 {
     switch(this.state.mode)
     {
         case 0:

         return "loading";

         case 1:

         return (<InputScreen modeCallback={this.setMode.bind(this)}></InputScreen>);

        /*  case 2:

         return (<OutputScreen></OutputScreen>); */
     }
 }
 dataloaded()
 {
     this.setState({mode:1});
 }
 setMode(mode:number)
 {
    this.setState({mode:mode});
 }
  render() {
    return (
      <div id="base">
        <Card>
            <Card.Header>App Name</Card.Header>
            <Card.Body>
              {this.getScreen()}
            </Card.Body>
        </Card>
      </div>
    );
  }
}
export default BaseScreen;
