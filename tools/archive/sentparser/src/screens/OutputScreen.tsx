import React, { Component } from "react";
import { SentenceVo } from "../dataObjects/SentenceVo";
import { MainStorage } from "../util/MainStorage";
import { SentParser } from "../util/SentParser";
interface MyProps {}
interface MyState {output:string}
class OutputScreen extends Component<MyProps, MyState> {
  private ms:MainStorage=MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);

    let sentParser:SentParser=new SentParser();
    sentParser.parse(this.ms.sentence);
    let data:SentenceVo=sentParser.getData();
    let sentData:string=JSON.stringify(data);

    this.state = {output:sentData};
  }
  componentDidMount()
  {
    
  }
  render() {
    return (
      <div>
        {this.ms.sentence}
        {this.state.output}
      </div>
    );
  }
}
export default OutputScreen;
