import React, { Component } from "react";
import { Form } from "react-bootstrap";
import TextInput from "../comps/TextInput";
import { SentenceVo } from "../dataObjects/SentenceVo";
import { MainStorage } from "../util/MainStorage";
import { SentParser } from "../util/SentParser";
interface MyProps {modeCallback:Function}
interface MyState {output:string,errors:string}
class InputScreen extends Component<MyProps, MyState> {
 private ms:MainStorage=MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = {output:"",errors:""};
  }
  setSentence(text:string)
  {
    this.ms.sentence=text;

    let sentParser:SentParser=new SentParser();
    sentParser.parse(this.ms.sentence);
    let data:SentenceVo=sentParser.getData();    
    this.setState({output:JSON.stringify(data),errors:sentParser.errors});
   
  }
  getOutput(data:any)
  {
    return(Object.keys(data).map((key:string,i:number)=>
      <div key={i}><strong>{key}</strong>:{data[key]},</div>
     ));
  }
  render() {
    return (
      <div>
         <TextInput callback={this.setSentence.bind(this)}></TextInput>
         <div className="redText">{this.state.errors}</div>
         <Form.Control as="textarea" rows={5} className="mylist" value={this.state.output} readOnly={true}/>
        
      </div>
    );
  }
}
export default InputScreen;