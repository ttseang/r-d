export class WordVo
{
    
    public word:string;
    public partOfSpeech:string;

    constructor(word:string,partOfSpeech:string)
    {
        this.word=word;
        this.partOfSpeech=partOfSpeech;
    }
}