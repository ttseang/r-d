export class SentenceVo
{
    public sentence:string;
    public subject:string;
    public directObject:string;
    public verb:string;
    public subjectMods:string[]=[];
    public directObjectMods:string[]=[];
    public verbMods:string[]=[];
    public predicate:string="";
    public fullSubject:string="";

    constructor(sentence:string,subject:string,verb:string,directObject:string)
    {
        this.sentence=sentence;
        this.subject=subject;
        this.verb=verb;
        this.directObject=directObject;        
    }
}