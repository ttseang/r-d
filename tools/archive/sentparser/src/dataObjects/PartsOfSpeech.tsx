export class PartsOfSpeech
{
    static UNKNOWN:string="unknown";
    static NOUN:string="noun";
    static VERB:string="verb";
    static DETERMINER:string="deteminer"
    static ADJECTIVE:string="adjective"
    static ADVERB:string="adverb";
    static CONJ:string="conjunction";

}