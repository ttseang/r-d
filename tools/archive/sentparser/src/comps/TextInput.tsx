import React, { ChangeEvent, Component } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
interface MyProps {
  callback: Function;
}
interface MyState {
  text: string;
}
class TextInput extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);
    this.state = { text: "" };
  }

  enterPressed(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === "Enter") {
      this.doDone();
    }
  }
  onChange(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ text: e.target.value });
  }
  doDone() {
    this.props.callback(this.state.text);
  }
  render() {
    return (
      <div>
        <Row className="tal">
          <Col>
            <Form.Control
              size="lg"
              type="text"
              placeholder="Enter Your Text"
              value={this.state.text}
              onChange={this.onChange.bind(this)}
              onKeyDown={this.enterPressed.bind(this)}
            />
          </Col>
        </Row>
        <Row className="marginTop">
        <Col></Col><Col></Col><Col className="fr tar"><Button onClick={this.doDone.bind(this)}>Parse</Button></Col>            
        </Row>
      </div>
    );
  }
}
export default TextInput;
