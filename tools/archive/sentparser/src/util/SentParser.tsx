import { PartsOfSpeech } from "../dataObjects/PartsOfSpeech";
import { SentenceVo } from "../dataObjects/SentenceVo";
import { WordVo } from "../dataObjects/WordVo";
import { MainStorage } from "./MainStorage";

export class SentParser {
  private sent: string;
  private verb: string = "";
  private verbIndex: number = 0;
  private words: WordVo[] = [];
  public errors: string = "";

  private ms: MainStorage = MainStorage.getInstance();

  private directObject: string = "";

  private predicateString: string = "";
  private predicate: WordVo[] = [];

  private subjectString: string = "";
  private subject: WordVo[] = [];
  private simpleSubject: string = "";

  private predAdjectives: string[] = [];
  private subAdjectives: string[] = [];

  constructor() {
    this.sent = "";
  }
  public parse(sent: string) {
    this.sent = sent;
    let words = sent.split(" ");

    for (let i: number = 0; i < words.length; i++) {
      let text: string = words[i].toLowerCase();
      let word: WordVo = new WordVo(text, this.idWord(text));
      this.words.push(word);
    }
    this.fixVerbVsNoun();
    this.findVerb();
    this.makePredicate();
    this.makeSubject();
    this.findDirectObject();
    this.findSubjectMods();
    this.findPredMods();

    console.log(this.words);
  }
  private idWord(word: string) {
    if (this.ms.verbs.includes(word)) {
      return PartsOfSpeech.VERB;
    }
    if (this.ms.nouns.includes(word)) {
      return PartsOfSpeech.NOUN;
    }
    if (word.substr(word.length - 1) === "s") {
      if (this.ms.nouns.includes(word.substr(0, word.length - 1))) {
        return PartsOfSpeech.NOUN;
      }
    }
    if (this.ms.nouns.includes(word)) {
      return PartsOfSpeech.NOUN;
    }
    if (this.ms.arts.includes(word)) {
      return PartsOfSpeech.DETERMINER;
    }
    if (this.ms.adjs.includes(word)) {
      return PartsOfSpeech.ADJECTIVE;
    }
    if (this.ms.advs.includes(word)) {
      return PartsOfSpeech.ADVERB;
    }
    this.errors += "unkown word:" + word + ", ";

    return PartsOfSpeech.UNKNOWN;
  }
  private fixVerbVsNoun() {
    for (let i: number = 1; i < this.words.length; i++) {
      let word: WordVo = this.words[i];
      if (
        word.partOfSpeech === PartsOfSpeech.VERB &&
        this.words[i - 1].partOfSpeech === PartsOfSpeech.DETERMINER
      ) {
        word.partOfSpeech = PartsOfSpeech.NOUN;
      }
    }
  }
  private findVerb() {
    for (let i: number = 1; i < this.words.length; i++) {
      let word: WordVo = this.words[i];
      if (word.partOfSpeech === PartsOfSpeech.VERB) {
        this.verb = word.word;
        this.verbIndex = i;
      }
    }
  }
  private makePredicate() {
    for (let i: number = this.verbIndex; i < this.words.length; i++) {
      let word: WordVo = this.words[i];
      this.predicate.push(word);
      this.predicateString += word.word + " ";
    }
  }
  private makeSubject() {
    for (let i: number = 0; i < this.verbIndex; i++) {
      let word: WordVo = this.words[i];

      if (word.partOfSpeech === PartsOfSpeech.NOUN) {
        this.simpleSubject = word.word;
      }
      this.subject.push(word);
      this.subjectString += word.word + " ";
    }
  }
  private findDirectObject() {
    for (let i: number = 0; i < this.predicate.length; i++) {
      let wordVo = this.predicate[i];
      if (wordVo.partOfSpeech === PartsOfSpeech.NOUN) {
        this.directObject = wordVo.word;
      }
    }
  }
  private findSubjectMods() {
    let mods:string[]=[];
    for (let i: number = 0; i < this.subject.length; i++) {
      let wordVo = this.subject[i];
      if (
        wordVo.partOfSpeech === PartsOfSpeech.NOUN ||
        wordVo.partOfSpeech === PartsOfSpeech.ADJECTIVE
      ) {
        if (this.simpleSubject !== wordVo.word) {
          mods.push(wordVo.word);
        }
      }
    }
    return mods;
  }
  private findVerbMods() {
    let mods:string[]=[];
    for (let i: number = 0; i < this.words.length; i++) {
      let wordVo = this.words[i];
      if (wordVo.partOfSpeech === PartsOfSpeech.ADVERB) {
        if (this.simpleSubject !== wordVo.word) {
          mods.push(wordVo.word);
        }
      }
    }
    return mods;
    // wordVo.partOfSpeech === PartsOfSpeech.ADJECTIVE ||
  }
  private findPredMods() {
    let mods:string[]=[];
    for (let i: number = 0; i < this.predicate.length; i++) {
      let wordVo = this.predicate[i];
      console.log("WORD="+wordVo.word);
      if (
        wordVo.partOfSpeech === PartsOfSpeech.NOUN ||
        wordVo.partOfSpeech === PartsOfSpeech.ADJECTIVE
      ) {
        if (this.directObject !== wordVo.word) {
          mods.push(wordVo.word);
        }
      }
    }
    return mods;
  }
  public getData() {
    let sentence = new SentenceVo(
      this.sent,
      this.simpleSubject,
      this.verb,
      this.directObject
    );
    sentence.directObjectMods = this.findPredMods();
    sentence.verbMods=this.findVerbMods();
    sentence.subjectMods = this.findSubjectMods();
    sentence.fullSubject = this.subjectString;
    sentence.predicate = this.predicateString;
    
    return sentence;
  }
}
