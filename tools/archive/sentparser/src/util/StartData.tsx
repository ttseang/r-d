import { MainStorage } from "./MainStorage";

export class StartData {
  private ms: MainStorage = MainStorage.getInstance();
  private callback: Function;

  constructor(callback: Function) {
    this.callback = callback;
  }
  public loadVerbs() {
    fetch("./data/verbs.txt")
      .then((response) => response.text())
      .then((data) => this.verbsLoaded(data));
  }
  private verbsLoaded(data: any) {
    this.ms.verbs = data.split(",");
    this.loadNouns();
  }
  private loadNouns() {
    fetch("./data/nouns.txt")
      .then((response) => response.text())
      .then((data) => this.nounsLoaded(data));
  }
  nounsLoaded(data: any) {
    this.ms.nouns = data.split(",");
    this.loadArts();
  }
  private loadArts() {
    fetch("./data/arts.txt")
      .then((response) => response.text())
      .then((data) => this.artsLoaded(data));
  }
  private artsLoaded(data: any) {
    this.ms.arts = data.split(",");
    this.loadAdj();
  }
  private loadAdj() {
    fetch("./data/adjectives.txt")
      .then((response) => response.text())
      .then((data) => this.adjLoaded(data));
  }
  private adjLoaded(data: any) {
    this.ms.adjs = data.split(",");
    this.loadAdv();
  }
  private loadAdv() {
    fetch("./data/adverbs.txt")
      .then((response) => response.text())
      .then((data) => this.advLoaded(data));
  }
  private advLoaded(data: any) {
    this.ms.advs = data.split(",");
    this.callback();
  }
}
