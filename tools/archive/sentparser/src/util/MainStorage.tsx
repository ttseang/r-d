

export class MainStorage {
  private static instance: MainStorage;
  public sentence: string;
  public verbs:string[]=[];
  public nouns:string[]=[];
  public arts:string[]=[];
  public adjs:string[]=[];
  public advs:string[]=[];

  constructor() {
    this.sentence = "";
  }
  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }
    return this.instance;
  }
  
}
