export class ConfigObj
{
    public word:string="";
    public xPos:number=0;
    public yPos:number=0;
    public line:string="";
    public len:number=4;

    constructor(obj:any=null)
    {
        if (obj)
        {
            console.log(obj);
            this.word=obj.word;
            this.xPos=parseFloat(obj.xPos);
            this.yPos=parseFloat(obj.yPos);
            this.line=obj.line;
            this.len=parseFloat(obj.len);
        }
       
    }
}