import React, { ChangeEvent, Component } from "react";
import { Row, Col, Card, Form } from "react-bootstrap";
import MainStorage from "../classes/mainStorage";
import { ConfigObj } from "../dataobjects/ConfigObj";
import LineSelect from "./LineSelect";
interface MyProps {
  configObj: ConfigObj;
}
interface MyState {
  x: number;
  y: number;
  len: number;
  line:string;
  word:string;
  disabled:boolean;
}
class PropBox extends Component<MyProps, MyState> {
    private ms:MainStorage=MainStorage.getInstance();
    private lineStyles:string[]=["h","d","c"];

  constructor(props: MyProps) {
    super(props);
  //  let dis:boolean=(this.props.configObj.word!=="null")?false:true;

    this.state = {
      x: this.props.configObj.xPos,
      y: this.props.configObj.yPos,
      len: this.props.configObj.len,
      line:this.props.configObj.line,
      word:this.props.configObj.word,
      disabled:true
    };
  }
  componentDidUpdate(prevProps:MyProps) {
    if (prevProps.configObj!==this.props.configObj)
    {
       let dis:boolean=(this.props.configObj.word!=="null")?false:true;

        this.setState({
            x: this.props.configObj.xPos,
            y: this.props.configObj.yPos,
            len: this.props.configObj.len,
            line:this.props.configObj.line,
            word:this.props.configObj.word,
            disabled:dis
          })
    }
  }
  
  setX(e: ChangeEvent<HTMLInputElement>) {     
    let val:number=parseFloat(e.target.value) || 0;
    console.log("x="+val);
    if (isNaN(val))
    {
      val=0;
    }
    this.setState({ x: val});
    let config:ConfigObj=this.getConfig();
    config.xPos=val;
    this.commitData(config);
  }
 
  setY(e: ChangeEvent<HTMLInputElement>) {
      let val:number=parseFloat(e.target.value) || 0;
      if (isNaN(val))
    {
      val=0;
    }
       console.log("y="+val);
    this.setState({ y: val });
    let config:ConfigObj=this.getConfig();
    config.yPos=val;
    this.commitData(config);
  }
  setLen(e: ChangeEvent<HTMLInputElement>)
  {
    let val:number=parseFloat(e.target.value) || 0;
    if (isNaN(val))
    {
      val=0;
    }
    console.log("len="+val);
    this.setState({ len: val });
    let config:ConfigObj=this.getConfig();
    config.len=val;
    this.commitData(config);
  }
  
  changeLine(val:number)
  {
    if (isNaN(val))
    {
      val=0;
    }
     let lineStyle:string=this.lineStyles[val];
     console.log(lineStyle);

     this.setState({line:lineStyle});

     let config:ConfigObj=this.getConfig();
     config.line=lineStyle;
     this.commitData(config);
  }
  getLineIndex()
  {
     for(let i:number=0;i<this.lineStyles.length;i++)
     {
         if (this.state.line===this.lineStyles[i])
         {
             return i;
         }
     }
     return 0;
  }
  getConfig()
  {
    let config:ConfigObj=new ConfigObj();
    config.xPos=this.state.x;
    config.yPos=this.state.y;
    config.line=this.state.line;
    config.len=this.state.len;
    config.word=this.state.word;
    return config;
  }
  commitData(config:ConfigObj)
  {   

     console.log(config);
     
     this.ms.scene.updateAllProps(JSON.stringify(config));
  }
  render() {
    return (
        <div>
      <Card>
          <Card.Body>
              <Row><Col>{this.state.word}</Col></Row>
        <Row>
          <Col sm={6}>
            x:
            <Form.Control type="number" size="sm" value={this.state.x} disabled={this.state.disabled} onChange={this.setX.bind(this)} />
            
          </Col>
          <Col sm={6}>
            y:
            <Form.Control  type="number" size="sm" value={this.state.y} disabled={this.state.disabled} onChange={this.setY.bind(this)} />
          </Col>
        </Row>
        <Row>
          <Col>len:<Form.Control  type="number" size="sm" value={this.state.len} disabled={this.state.disabled} onChange={this.setLen.bind(this)}  /></Col>
          <Col></Col>
        </Row>
        <Row>
          <Col></Col>
          <Col></Col>
        </Row></Card.Body>
      </Card>

      <Card>
          <Card.Body>
              <LineSelect callback={this.changeLine.bind(this)} lineStyle={this.getLineIndex()} disabled={this.state.disabled}></LineSelect>
          </Card.Body>
      </Card>
      </div>
    );
  }
}
export default PropBox;
