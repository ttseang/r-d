import React, { Component } from 'react';
import { ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
interface MyProps {lineStyle:number,callback:Function,disabled:boolean}
interface MyState {selected:number}
class LineSelect extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
this.state={selected:this.props.lineStyle};
}
handleChange(val:number)
{
    console.log(val);
    this.setState({selected:val});
    this.props.callback(val);
}
componentDidUpdate(prevProps:MyProps) {
    if (prevProps.lineStyle!==this.props.lineStyle)
    {
        this.setState({selected:this.props.lineStyle});
    }
}
render()
{
return (<div>
    <ToggleButtonGroup name="linestyle" type="radio" value={this.state.selected} defaultValue={this.props.lineStyle} onChange={this.handleChange.bind(this)} >
      <ToggleButton size="sm" value={0}  disabled={this.props.disabled}>Horiz.</ToggleButton>
      <ToggleButton size="sm" value={1}  disabled={this.props.disabled}>Diag</ToggleButton>
      <ToggleButton size="sm" value={2}  disabled={this.props.disabled}>Conj</ToggleButton>      
    </ToggleButtonGroup>
</div>)
}
}
export default LineSelect;