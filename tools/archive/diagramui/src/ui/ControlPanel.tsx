import React, { Component } from "react";
import { Card, Row, Col } from "react-bootstrap";
import { ButtonConstants } from "../classes/ButtonConstants";
import MainStorage from "../classes/mainStorage";
import ButtonLine from "./ButtonLine";
interface MyProps {}
interface MyState {}
class ControlPanel extends Component<MyProps, MyState> {
  private ms:MainStorage=MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  buttonAction(index:number,action:number){
    console.log(action);
    let scene:any=this.ms.scene;
    switch(action)
    {
      case 1:
      scene.makeBox("",0,0,1,"tvsep");
      break;

      case 2:
        scene.makeBox("",0,0,1,"vsep");
      break;
      case 3:
        scene.makeBox("",0,0,1,"cend");
      break;

      case 4:
        scene.makeBox("",0,0,4,"d");
        break; 
    }
  }
  render() {
    return (
      <div>
        <Card>
          <Row>
            <Col><ButtonLine buttonArray={ButtonConstants.ADD_BUTTONS} actionCallback={this.buttonAction.bind(this)}></ButtonLine></Col>
          </Row>
        </Card>
      </div>
    );
  }
}
export default ControlPanel;
