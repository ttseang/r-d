import React, { Component } from "react";
import { Button, ButtonGroup, Card } from "react-bootstrap";
import MainStorage from "../classes/mainStorage";
interface MyProps {}
interface MyState {}
class HistoryPanel extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  redo() {
    this.ms.scene.redo();
  }
  undo() {
    this.ms.scene.undo();
  }
  render() {
    return (
      <Card>
        <Card.Body>
          <ButtonGroup>
            <Button onClick={this.undo.bind(this)}>
              <i className="fas fa-undo"></i>
            </Button>
            <Button onClick={this.redo.bind(this)}>
              <i className="fas fa-redo"></i>
            </Button>
          </ButtonGroup>
        </Card.Body>
      </Card>
    );
  }
}
export default HistoryPanel;
