import React, { Component } from "react";
import { ToggleButton, ToggleButtonGroup } from "react-bootstrap";
import MainStorage from "../classes/mainStorage";

interface MyProps {}
interface MyState {
  selected: number;
}
class Sidebar extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = { selected: 0 };
  }
  setSingle() {
    this.ms.scene.setSelectMode(0);
  }
  setMulti() {
    // window.scene=this.ms.scene;
    this.ms.scene.setSelectMode(1);
    //  console.log(this.ms.scene);
  }
  handleChange(val: number) {
    this.setState({ selected: val });
    switch (val) {
      case 0:
        this.setSingle();
        break;
      case 1:
        this.setMulti();
        break;

      case 2:
        this.ms.scene.makeBox("", 16,8, 1, "tvsep");
        break;

      case 3:
        this.ms.scene.makeBox("", 16,8, 1, "vsep");
        break;
      case 4:
        this.ms.scene.makeBox("", 16,8, 1, "cend");
        break;

      case 5:
        this.ms.scene.makeBox("", 16,8, 4, "d");
        break;
    }
  }
  render() {
    return (
      <div>
        <ToggleButtonGroup
          name="toolbar"
          vertical={true}
          type="radio"
          value={this.state.selected}
          defaultValue={0}
          onChange={this.handleChange.bind(this)}
        >
          <ToggleButton variant="secondary" size="lg" value={0}>
            <i className="fas fa-mouse-pointer"></i>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={1}>
            <i className="far fa-object-ungroup"></i>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={2}>
            <img
              src="./layout/assets/images/tvsep.png"
              width="25px"
              alt="tvsep"
            ></img>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={3}>
            <img
              src="./layout/assets/images/vsep.png"
              width="25px"
              alt="tvsep"
            ></img>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={4}>
            <i className="fas fa-chevron-left"></i>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={5}>
            <i className="fas fa-slash"></i>
          </ToggleButton>
        </ToggleButtonGroup>
      </div>
    );
  }
}
export default Sidebar;
