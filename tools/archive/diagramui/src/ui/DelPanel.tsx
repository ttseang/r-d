import React, { Component } from "react";
import { Button, ButtonGroup, Card } from "react-bootstrap";
import MainStorage from "../classes/mainStorage";
interface MyProps {}
interface MyState {}
class DelPanel extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  copyBox() {
    this.ms.scene.copyBox();
  }
  delBox() {
    this.ms.scene.deleteBox();
  }
  render() {
    return (
      <Card>
        <Card.Body>
          <ButtonGroup>
            <Button variant="success">
              <i className="far fa-clone" onClick={this.copyBox.bind(this)}></i>
            </Button>
            <Button variant="danger" onClick={this.delBox.bind(this)}>
              <i className="fas fa-trash-alt"></i>
            </Button>
          </ButtonGroup>
        </Card.Body>
      </Card>
    );
  }
}
export default DelPanel;
