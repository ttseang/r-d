export class MainStorage {
  private static instance: MainStorage;
  public scene: any;
  constructor() {
    this.scene = null;
  }
  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }
    return this.instance;
  }
}
export default MainStorage;
