import { ButtonVo } from "../dataobjects/ButtonVo";

export class ButtonConstants {
  static ADD_BUTTONS: ButtonVo[] = [
    new ButtonVo("Add tv seperator", "primary", 1),
    new ButtonVo("Add v seperator", "warning", 2),
    new ButtonVo("Add Conjunction End", "primary", 3),
    new ButtonVo("Add Blank Diagonal", "warning", 4)
  ];
}
