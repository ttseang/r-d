import React, { Component } from "react";
import { Card } from "react-bootstrap";
import MainStorage from "../classes/mainStorage";
import LayoutScreen from "./LayoutScreen";
import SentenceScreen from "./SentenceScreen";
interface MyProps {}
interface MyState {
  mode: number;
  sentence:string;
}
class BaseScreen extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = { mode: 0,sentence:"" };
  }

  getScreen() {
    switch (this.state.mode) {
      case 0:
        return (
          <SentenceScreen callback={this.setText.bind(this)}></SentenceScreen>
        );

      case 1:
        return <LayoutScreen></LayoutScreen>;
    }
  }
  setText(text:string) {
    this.setState({sentence:text,mode:1});
  }
  render() {
    return (
      <div id="base">
        <div id="words" className="hideMe">{this.state.sentence}</div>
        <Card>
          <Card.Header id="top">App Name</Card.Header>
          <Card.Body id="main">{this.getScreen()}</Card.Body>
        </Card>
      </div>
    );
  }
}
export default BaseScreen;
