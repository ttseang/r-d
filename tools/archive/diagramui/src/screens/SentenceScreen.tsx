import React, { Component } from "react";
import TextInputButton from "../ui/TextInputButton";
interface MyProps {
  callback: Function;
}
interface MyState {}
class SentenceScreen extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  setText(text:string)
  {
    this.props.callback(text);
  }
  render() {
    return (
      <div>
          <h2 className="tal">Enter a sentence.</h2>
       <TextInputButton buttonText="Make Sentence" callback={this.setText.bind(this)} cancelCallback={()=>{}} showCancel={false} text={""} clearOnEnter={false}></TextInputButton>
      </div>
    );
  }
}
export default SentenceScreen;