import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import MainStorage from "../classes/mainStorage";
import { ConfigObj } from "../dataobjects/ConfigObj";
import ControlPanel from "../ui/ControlPanel";
import DelPanel from "../ui/DelPanel";
import HistoryPanel from "../ui/HistoryPanel";
import PropBox from "../ui/PropBox";
import Sidebar from "../ui/Sidebar";
interface MyProps {}
interface MyState {
  configObj: ConfigObj;
}
class LayoutScreen extends Component<MyProps, MyState> {
  private phaserFrameRef: React.RefObject<HTMLIFrameElement> = React.createRef();
  private ms: MainStorage = MainStorage.getInstance();
  private lastKey: string = "";

  constructor(props: MyProps) {
    super(props);
    this.state = { configObj: new ConfigObj() };
  }
  componentDidMount() {
    document.addEventListener("keydown", this.handleKey.bind(this));
  }
  handleKey(e: KeyboardEvent) {
    if (e.key === this.lastKey) {
      return;
    }
    if (e.ctrlKey === true && e.key === "c") {
      console.log("copy");
    } 
    if (e.ctrlKey===true && e.key==="z")
    {
      console.log("undo");
    }
    if (e.ctrlKey===true && e.key==="y")
    {
      console.log("redo");
    }
    this.lastKey = e.key;
  }
  setSize() {
    let h: number =
      this.phaserFrameRef.current?.contentDocument?.body.offsetHeight || 0;

    this.phaserFrameRef.current?.setAttribute("height", h.toString());

    setTimeout(this.getScene.bind(this), 500);
    //  this.phaserFrameRef.width=scene.game.config.width;
    //
  }
  getScene() {
    let bridge: any = document.getElementById("bridge");
    let scene: any = bridge.scene;
    if (scene) {
      //console.log(scene);
      this.ms.scene = scene;
      scene.updateReact = this.updatedItem.bind(this);
    } else {
      setTimeout(this.getScene.bind(this), 500);
    }
  }
  updatedItem(type: string, config: any) {
    /*  console.log("updated item");
    console.log(type);
    console.log(config); */

    switch (type) {
      case "0":
        let configObj: ConfigObj = new ConfigObj(config);
        this.setState({ configObj: configObj });
        break;

      case "1":
        console.log("select mode changed");
        break;

      case "2":
        this.setState({
          configObj: new ConfigObj({
            word: "null",
            len: 0,
            line: "h",
            xPos: 0,
            yPos: 0,
          }),
        });
        break;
    }
  }
  render() {
    return (
      <div>
        <Row>
          <Col sm="1">
            <Sidebar></Sidebar>
          </Col>
          <Col sm="8">
            <iframe
              ref={this.phaserFrameRef}
              id="gameframe"
              width="100%"
              title="phaser"
              onLoad={this.setSize.bind(this)}
              src="layout/index.html"
              scrolling="no"
            ></iframe>
          </Col>
          <Col sm="3">
            <PropBox configObj={this.state.configObj}></PropBox>
            <HistoryPanel></HistoryPanel>
            <DelPanel></DelPanel>
          </Col>
        </Row>
      </div>
    );
  }
}
export default LayoutScreen;
