import React, { ChangeEvent, Component } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import { ProblemTypes } from '../classes/Constants';
import MainStorage from '../classes/MainStorage';
import DivDisplay from '../comps/displays/DivDisplay';
import FractDisplay from '../comps/displays/FractDisplay';
import RMTower from '../comps/displays/RMTower';
import RTower from '../comps/displays/RTower';
import { CellGridOutputVo } from '../dataObjs/outputs/CellGridOutputVo';
import { DCellOutput } from '../dataObjs/outputs/DCellOutput';
import { FractGridOutput } from '../dataObjs/outputs/FractGridOutput';
import { MCellGridOutputVo } from '../dataObjs/outputs/MCellGridOutputVo';
import { EqEngine2 } from '../util/EqEngine2';
interface MyProps { callback: Function }
interface MyState { text: string, useRemain: boolean, zeroLimit: number, step: number, fontSize: number, msg: string, steps: CellGridOutputVo[] | null, msteps: MCellGridOutputVo[] | null,dsteps:DCellOutput[]|null,fractSteps:FractGridOutput[]|null }
class DebugEquationScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private problemType: ProblemTypes = ProblemTypes.UNSUPORTED;
    constructor(props: MyProps) {
        super(props);
        this.state = { text: this.getSaved(), useRemain: false, zeroLimit: 3, step: 0, fontSize: 26, msg: "", steps: null, msteps: null,dsteps:null,fractSteps:null }
    }
    goNext() {
        localStorage.setItem("eqtext", this.state.text);

        let eqEngine: EqEngine2 = new EqEngine2(this.state.text);

        this.problemType = eqEngine.problemType;

        switch (this.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:
                let data: CellGridOutputVo[] | null = eqEngine.getStepsTower();
                let msg = "";
                if (data === null) {
                    msg = "Invalid";
                }
                this.setState({ steps: data, msg: msg });
                break;

            case ProblemTypes.MULTIPLICATION:
                let data2: MCellGridOutputVo[] | null = eqEngine.getStepsMTower();
                let msg2 = "";
                if (data2 === null) {
                    msg2 = "Invalid";
                }
                this.setState({ msteps: data2, msg: msg2 });

                break;

            case ProblemTypes.DIVISION:
                let data3:DCellOutput[] | null =eqEngine.getStepsD();
                let msg3 = "";
                if (data3 === null) {
                    msg3 = "Invalid";
                }
                this.setState({ dsteps: data3, msg: msg3 });

                break;
            case ProblemTypes.FRACTIONS:

                
                let data4:FractGridOutput[] | null=eqEngine.getFractSteps();
                
                let msg4:string="";
                if (data4===null)
                {
                    msg4="Invalid";
                }
                this.setState({fractSteps:data4,msg:msg4});

           
        }
        /* let data: CellGridOutputVo[]  | null = eqEngine.getSteps();
        let msg = "";
        if (data === null) {
            msg = "Invalid";
        }
        this.setState({ steps: data, msg: msg }); */
    }
    getSaved() {
        let saveText: string = localStorage.getItem("eqtext") || "";
        return saveText;
    }
    getDisplay() {


        if (this.problemType === ProblemTypes.ADDITION || this.problemType === ProblemTypes.SUBTRACTION) {
            if (this.state.steps) {
                return <RTower cellInfo={this.state.steps[this.state.step]} fontSize={this.state.fontSize} idString={'mathDisplayRel'}></RTower>
            }

        }
        if (this.problemType === ProblemTypes.MULTIPLICATION) {
            if (this.state.msteps) {
                return <RMTower cellInfo={this.state.msteps[this.state.step]}  fontSize={26} idString={'mathDisplayRel'}></RMTower>
            }
        }
        if (this.problemType===ProblemTypes.DIVISION)
        {
            if (this.state.dsteps)
            {
                return <DivDisplay cellInfo={this.state.dsteps[this.state.step]} fontSize={26}></DivDisplay>
            }
        }
        if (this.problemType===ProblemTypes.FRACTIONS)
        {
            if (this.state.fractSteps)
            {
                return <FractDisplay cells={this.state.fractSteps[this.state.step]} fontSize={26}></FractDisplay>
            }
        }
        return <div className='tac'><h2>Invalid</h2></div>
    }
    setText(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.currentTarget.value })

    }
    clearDisplay() {

        this.setState({ text: "", step: 0, fontSize: 26 });

    }
    changeStep(e: ChangeEvent<HTMLInputElement>) {
        let step: number = parseInt(e.currentTarget.value);
        this.setState({ step: step });
        this.ms.step = step;

    }
    changeFontSize(e: ChangeEvent<HTMLInputElement>) {
        let fs: number = parseInt(e.currentTarget.value);
        this.setState({ fontSize: fs });

    }
    render() {
        return (<div className='mcontainer'>
            {this.getDisplay()}
            <hr />
            <Card><Card.Body>
                <Row>
                    <Col className='tac' sm={8}><input id="text2" type='text' value={this.state.text} onChange={this.setText.bind(this)} /></Col>
                    <Col>

                        <Row>
                            <Col sm={4}><span className="smText">Step:</span></Col>
                            <Col sm={2}><input id="stepper1" min={0} onChange={this.changeStep.bind(this)} type='number' value={this.state.step} /></Col>
                        </Row>

                        <Row>

                            <Col sm={4}><Button variant='primary' onClick={this.clearDisplay.bind(this)}>Clear</Button></Col>
                            <Col sm={4}><Button variant='success' onClick={this.goNext.bind(this)}>Next</Button></Col>
                        </Row>

                    </Col>
                </Row>
            </Card.Body></Card>
        </div>)
    }
}
export default DebugEquationScreen;