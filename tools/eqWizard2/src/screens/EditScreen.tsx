import { Component } from 'react';
import { Row, Col, ButtonGroup, Button } from 'react-bootstrap';
import { ProblemTypes, SidePanels } from '../classes/Constants';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
import ColumnBox from '../comps/sidePanels/ColumnBox';
import CssScroll from '../comps/sidePanels/CssScroll';
import MStepCard from '../comps/stepcards/MStepCard';
import MTowerEdit from '../comps/editComps/MTowerEdit';
import RMTower from '../comps/displays/RMTower';
import RowBox from '../comps/sidePanels/RowBox';
import RTower from '../comps/displays/RTower';
import StepCard from '../comps/stepcards/StepCard';
import TowerEdit from '../comps/editComps/TowerEdit';
import { CellGridOutputVo } from '../dataObjs/outputs/CellGridOutputVo';
import { MCellGridOutputVo } from '../dataObjs/outputs/MCellGridOutputVo';
import { DCellOutput } from '../dataObjs/outputs/DCellOutput';
import DStepCard from '../comps/stepcards/DStepCard';
import DivDisplay from '../comps/displays/DivDisplay';
import DivideEdit from '../comps/editComps/DivideEdit';
import { FractGridOutput } from '../dataObjs/outputs/FractGridOutput';
import FractDisplay from '../comps/displays/FractDisplay';
import FStepCard from '../comps/stepcards/FStepCard';
import FractionEdit from '../comps/editComps/FractionEdit';
import MRowBox from '../comps/sidePanels/MRowBox';
import MColumnBox from '../comps/sidePanels/MColumnBox';
import DColumnBox from '../comps/sidePanels/DColumnBox';
import DRowBox from '../comps/sidePanels/DRowBox';
import ToolPanel from '../comps/sidePanels/ToolPanel';
import { CssStyleUtil } from '../util/editUtils/CssStyleUtil';
import CellTools from '../comps/sidePanels/CellTools';
import DropDownMenu from '../ui/DropDownMenu';

interface MyProps { changeScreen: Function, saveInfo: Function, cells: CellGridOutputVo[], mcells: MCellGridOutputVo[], dcells: DCellOutput[], fcells: FractGridOutput[] }
interface MyState { saveMode: boolean; cells: CellGridOutputVo[], mcells: MCellGridOutputVo[], dcells: DCellOutput[], fcells: FractGridOutput[], step: number, sidePanel: SidePanels, editMode: number }
class EditScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private controller: Controller = Controller.getInstance();
    private problemType: ProblemTypes = ProblemTypes.UNSUPORTED;
    private stepTimer: any = {};
    private timeStep: number = 0;

    constructor(props: MyProps) {
        super(props);
        this.state = { saveMode: false, cells: this.props.cells, mcells: this.props.mcells, dcells: this.props.dcells, fcells: this.props.fcells, step: 0, sidePanel: SidePanels.CSS, editMode: 0 };


        this.problemType = this.ms.problemType;
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ step: 0, cells: this.props.cells, mcells: this.props.mcells, dcells: this.props.dcells, fcells: this.props.fcells })
        }
    }
    componentDidMount() {
        //  this.cycleSteps();
        this.controller.setSidePanel = this.setPanel.bind(this);
        this.controller.setEditMode = this.setMode.bind(this);
        this.controller.saveData = this.startSave.bind(this);
        this.controller.setSaveScreen = this.setSaveMode.bind(this);
        this.controller.showHide = this.showHide.bind(this);
    }
    componentWillUnmount() {
        this.controller.setSidePanel = () => { }
        this.controller.setEditMode = () => { };
        this.controller.saveData = () => { };
        this.controller.setSaveScreen = () => { };
        this.controller.showHide = () => { };
    }
    showHide(val: boolean) {

        let cssUtil: CssStyleUtil = CssStyleUtil.getInstance();
        let rule: CSSStyleDeclaration | undefined = cssUtil.cssMap.get(".tteq .hid");
        console.log(rule);

        if (rule !== undefined) {
            let visible: string = (val === true) ? "visible" : "hidden";

            rule.visibility = visible;
            rule.opacity = "0.5";
            rule.color = "blue";
        }

    }
    setSaveMode(val: boolean) {
        this.setState({ saveMode: val });
    }
    startSave() {
        this.ms.sizes = [];
        this.setState({ saveMode: true });
        this.cycleSteps();
    }
    cycleSteps() {
        this.timeStep++;
       
        let v: HTMLElement | null = document.getElementById("mathDisplayRel2");
        if (v) {
            let h: number = v.clientHeight;
          
            this.ms.sizes.push(h);
            if (h > this.ms.maxSize) {
                this.ms.maxSize = h;
            }
        }

        if (this.timeStep < this.getLength()) {
            this.setState({ step: this.timeStep });

            setTimeout(() => {
                this.cycleSteps();
            }, 200);
        }
        else {
            this.setState({ step: 0 });
            this.prepSaveContent();
        }
    }
    getLength() {
        switch (this.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:

                return this.state.cells.length;

            case ProblemTypes.MULTIPLICATION:
                return this.state.mcells.length;

            case ProblemTypes.DIVISION:
                return this.state.dcells.length;

            case ProblemTypes.FRACTIONS:
                return this.state.fcells.length;
        }
        return 0;
    }
    getCards() {
        let cards: JSX.Element[] = [];

        let length: number = this.getLength();

        for (let i: number = 0; i < length; i++) {
            let key: string = "card" + i.toString();
            let selected: boolean = (this.state.step === i) ? true : false;
            switch (this.problemType) {
                case ProblemTypes.ADDITION:
                case ProblemTypes.SUBTRACTION:
                    cards.push(<StepCard key={key} index={i} selected={selected} stepData={this.state.cells[i]} callback={this.selectCard.bind(this)}></StepCard>)

                    break;

                case ProblemTypes.MULTIPLICATION:

                    cards.push(<MStepCard key={key} index={i} selected={selected} stepData={this.state.mcells[i]} callback={this.selectCard.bind(this)}></MStepCard>)
                    break;

                case ProblemTypes.DIVISION:
                    cards.push(<DStepCard key={key} index={i} stepData={this.state.dcells[i]} callback={this.selectCard.bind(this)} selected={selected}></DStepCard>)

                    break;

                case ProblemTypes.FRACTIONS:
                   
                    cards.push(<FStepCard key={key} index={i} stepData={this.state.fcells[i]} callback={this.selectCard.bind(this)} selected={selected}></FStepCard>)

            }
        }
        return cards;
    }
    getStepButtons() {
        return <ButtonGroup><Button onClick={this.copyUp.bind(this)}>Copy</Button><Button variant='danger' onClick={this.deleteStep.bind(this)}>Delete</Button></ButtonGroup>
    }
    selectCard(index: number) {
       
        this.ms.step = index;
        this.setState({ step: index })
    }
    setPanel(panel: SidePanels) {
        /* if (panel === this.state.sidePanel) {
            panel = SidePanels.None;
        } */
        this.setState({ sidePanel: panel })
    }
    setMode(mode: number) {
        this.ms.editMode=mode;
        this.setState({ editMode: mode });
    }
    copyUp() {

        let cells: CellGridOutputVo[] = this.state.cells.slice();
        let mcells: MCellGridOutputVo[] = this.state.mcells.slice();
        let dcells: DCellOutput[] = this.state.dcells.slice();
        let fcells: FractGridOutput[] = this.state.fcells.slice();

        switch (this.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:
                let copyCell: CellGridOutputVo = this.state.cells[this.state.step].clone();

                if (this.state.step === 0) {
                    cells.splice(0, 0, copyCell);
                }
                else {
                    cells.splice(this.state.step, 0, copyCell);
                }
                break;

            case ProblemTypes.MULTIPLICATION:
                let mcopyCell: MCellGridOutputVo = this.state.mcells[this.state.step].clone();

                if (this.state.step === 0) {
                    mcells.splice(0, 0, mcopyCell);
                }
                else {
                    mcells.splice(this.state.step, 0, mcopyCell);
                }

                break;

            case ProblemTypes.DIVISION:
                let dcopyCell: DCellOutput = this.state.dcells[this.state.step].clone();
                if (this.state.step === 0) {
                    dcells.splice(0, 0, dcopyCell);
                }
                else {
                    dcells.splice(this.state.step, 0, dcopyCell);
                }
                break;

            case ProblemTypes.FRACTIONS:
                let fcopyCell: FractGridOutput = this.state.fcells[this.state.step].clone();

                if (this.state.step === 0) {
                    fcells.splice(0, 0, fcopyCell);
                }
                else {
                    fcells.splice(this.state.step, 0, fcopyCell);
                }

        }

        this.setState({ cells: cells, mcells: mcells, fcells: fcells, dcells: dcells });
    }
    deleteStep() {
        if (this.getLength() === 1) {
            return;
        }


        let cells: CellGridOutputVo[] = this.state.cells.slice();
        let mcells: MCellGridOutputVo[] = this.state.mcells.slice();
        let dcells: DCellOutput[] = this.state.dcells.slice();
        let fcells: FractGridOutput[] = this.state.fcells.slice();


        let step: number = this.state.step;

        switch (this.problemType) {

            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:

                cells.splice(this.state.step, 1);

                while (step > cells.length - 1 && step > -1) {
                    step--;
                }
                break;


            case ProblemTypes.MULTIPLICATION:
                mcells.splice(this.state.step, 1);

                while (step > mcells.length - 1 && step > -1) {
                    step--;
                }
                break;

            case ProblemTypes.DIVISION:


                dcells.splice(this.state.step, 1);

                while (step > dcells.length - 1 && step > -1) {
                    step--;
                }
                this.ms.dcells = dcells;
                break;


            case ProblemTypes.FRACTIONS:

                fcells.splice(this.state.step, 1);
                while (step > fcells.length - 1 && step > -1) {
                    step--;
                }

                this.ms.fcells = fcells;

        }

        this.setState({ cells: cells, step: step, mcells: mcells, fcells: fcells, dcells: dcells });
    }
    getDisplay() {
        if (this.state.cells === null) {
            return;          
        }       
        switch (this.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:
                return <RTower cellInfo={this.state.cells[this.state.step]} fontSize={26} idString={'mathDisplayRel2'}></RTower>


            case ProblemTypes.MULTIPLICATION:

                return <RMTower cellInfo={this.state.mcells[this.state.step]} fontSize={26} idString={'mathDisplayRel2'}></RMTower>

            case ProblemTypes.DIVISION:
                return <DivDisplay cellInfo={this.state.dcells[this.state.step]} fontSize={26}></DivDisplay>

            case ProblemTypes.FRACTIONS:
                return <div className="fractPos"><FractDisplay fontSize={26} cells={this.state.fcells[this.state.step]}></FractDisplay></div>
        }
        return "INVALID PROBLEM TYPE";
    }
    getEditComp() {
        switch (this.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:
                return <TowerEdit callback={this.updateStep.bind(this)} stepData={this.state.cells[this.state.step]}></TowerEdit>

            case ProblemTypes.MULTIPLICATION:

                return <MTowerEdit stepData={this.state.mcells[this.state.step]} callback={this.updateMStep.bind(this)}></MTowerEdit>

            case ProblemTypes.DIVISION:
                return <DivideEdit stepData={this.state.dcells[this.state.step]} callback={this.updateDStep.bind(this)}></DivideEdit>

            case ProblemTypes.FRACTIONS:
                return <FractionEdit stepData={this.state.fcells[this.state.step]} callback={this.updateFStep.bind(this)}></FractionEdit>
        }
    }
    updateFStep(cell: FractGridOutput) {
        let tempCells: FractGridOutput[] = this.state.fcells;
        tempCells[this.state.step] = cell;
        this.setState({ fcells: tempCells })
    }
    updateStep(cell: CellGridOutputVo) {
        let tempCells: CellGridOutputVo[] = this.state.cells;
        tempCells[this.state.step] = cell;
        this.setState({ cells: tempCells })
    }
    updateMStep(cell: MCellGridOutputVo) {
        let tempCells: MCellGridOutputVo[] = this.state.mcells;
        tempCells[this.state.step] = cell;
        this.setState({ mcells: tempCells })
    }
    updateDStep(cell: DCellOutput) {
        let tempCells: DCellOutput[] = this.state.dcells;
        tempCells[this.state.step] = cell;
        this.setState({ dcells: tempCells })
    }
    getSidePanel() {
        switch (this.state.sidePanel) {
            case SidePanels.None:
                return "";

            case SidePanels.CellTools:
                return <CellTools></CellTools>

            case SidePanels.Cols:
                if (this.ms.problemType === ProblemTypes.ADDITION || this.ms.problemType === ProblemTypes.SUBTRACTION) {
                    return <ColumnBox></ColumnBox>
                }
                if (this.ms.problemType === ProblemTypes.MULTIPLICATION) {
                    return <MColumnBox></MColumnBox>
                }
                if (this.ms.problemType === ProblemTypes.DIVISION) {
                    return <DColumnBox></DColumnBox>
                }
                break;

            case SidePanels.Rows:
                if (this.ms.problemType === ProblemTypes.ADDITION || this.ms.problemType === ProblemTypes.SUBTRACTION) {
                    return <RowBox></RowBox>
                }
                if (this.ms.problemType === ProblemTypes.MULTIPLICATION) {
                    return <MRowBox></MRowBox>
                }
                if (this.ms.problemType === ProblemTypes.DIVISION) {
                    return <DRowBox></DRowBox>
                }
                break;

            case SidePanels.CSS:
                return <CssScroll></CssScroll>

            case SidePanels.Tools:
                return <ToolPanel></ToolPanel>

            default:
                return "";
        }
        return "";
    }
    getContent() {
        switch (this.state.editMode) {
            case 0:
                return this.getDisplay();
            case 1:
                return this.getEditComp();
        }
        return <h5>Unknown Mode</h5>
    }

    prepSaveContent() {
        let saveObj: CellGridOutputVo[] | MCellGridOutputVo[] | DCellOutput[] | FractGridOutput[] | null = null;


        switch (this.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:

                saveObj = this.state.cells;
                break;

            case ProblemTypes.MULTIPLICATION:
                saveObj = this.state.mcells;

                break;

            case ProblemTypes.DIVISION:
                saveObj = this.state.dcells;

                break;

            case ProblemTypes.FRACTIONS:

                saveObj = this.state.fcells;

                break;
        }
        this.ms.saveObj = saveObj;
        if (this.ms.LtiFileName === "" || this.ms.fileName === "") {
            this.props.changeScreen(2);
            return
        }
        this.props.saveInfo();
        //  this.setState({saveMode:true});
    }

    getSaveCover() {
        if (this.state.saveMode === false) {
            return "";
        }
        return (<div className='saveCover'>
            <div className='saveText'>SAVING</div>
        </div>)
    }
    render() {
        return (<div>
            {this.getSaveCover()}
            <div>
                
           
                <Row>
                    <Col sm={3} className="silverBack colHeader">STEPS</Col>
                    <Col sm={6} className="silverBack"><DropDownMenu></DropDownMenu></Col>
                    <Col sm={3} className="silverBack colHeader">Controls</Col>
                </Row>
                <Row>
                    <Col sm={3} className="greyBack">
                        <div className="scroll2">
                            {this.getCards()}
                        </div>
                        {this.getStepButtons()}
                    </Col>
                    <Col sm={6} className="editArea">{this.getContent()}</Col>
                    <Col sm={3} className="greyBack">{this.getSidePanel()}</Col>
                </Row>
            </div>
        </div>)
    }
}
export default EditScreen;