import { Component } from 'react';
import { Card, ListGroup, ListGroupItem } from 'react-bootstrap';
interface MyProps { callback: Function }
interface MyState { }
class TestTab extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }

    getItems() {
        let snippets: string[] = ["2+1_3+1_3=calc", "1+1_3=calc", "1_1_2+1_1_2=calc", "1_3+1_3=calc", "1_1_3+2_1_3=?_?_3", "1_1_3+1_3=1_2_3", "1+2+3+4=calc", "1_3+1_3", "1_1_3+2_1_3=3_2_3", "22/7=calc"];
        snippets.unshift("20+1_3+1_3=calc");
        snippets.unshift("sqrt(4)+10=calc");
        snippets.unshift("sqrt(9)+10=calc");
        snippets.unshift("sqrt(4)");
        snippets.unshift("1.5+2.5+3.2=calc");
        snippets.unshift("10+9+338+423");

        snippets.unshift("567*7");
        snippets.unshift("567*5127");
        snippets.unshift("999+999");
        snippets.unshift("305-299");
        snippets.unshift("400-237");

        snippets.unshift("40000000-123");
        snippets.unshift("1.2+2.3+4.4");
        snippets.unshift("1.01+10.2");
        snippets.unshift("1.005+1.05");
        snippets.unshift("1.007+3.4+.7");
        snippets.unshift("1.07+3.4+.7");
        snippets.unshift("345-299");
        snippets.unshift("30.5-2.555");
        snippets.unshift("22/7");
        snippets.unshift("1782/11");
        snippets.unshift("10000/30");
        snippets.unshift("10/3");
        snippets.unshift("12/5");
        snippets.unshift("3.5/2");
        
        snippets.unshift("345-299");
        snippets.unshift("1_7+2_9");
        snippets.unshift("1_2+1_3");
        snippets.unshift("1_4+1_8");
        snippets.unshift("8_9*1_6");
        snippets.unshift("1_8*1_6");
        snippets.unshift("1_5*2_5");
        snippets.unshift("3_8*6_9");
        snippets.unshift("8_9+1_6");
        snippets.unshift("1_8+1_6");
        snippets.unshift("1_8+1_3");
        snippets.unshift("1_5+2_5");
        snippets.unshift("3_8+6_9");     


        snippets.unshift("1_2+1_3+1_4+1_5");
        snippets.unshift("1.007+3.4+.7");
        

        let items: JSX.Element[] = [];
        for (let i: number = 0; i < snippets.length; i++) {
            let key: string = "snip" + i.toString();
            items.push(<ListGroupItem key={key} onClick={() => { this.props.callback(snippets[i]) }}>{snippets[i]}</ListGroupItem>)
        }
        return (<div className="scroll1"><ListGroup>{items}</ListGroup></div>);
    }
    render() {
        return (<Card><Card.Body>{this.getItems()}</Card.Body></Card>)
    }
}
export default TestTab;