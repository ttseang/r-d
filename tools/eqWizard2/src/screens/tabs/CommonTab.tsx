import React, { Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import NumPad from '../../ui/NumPad';
import OpPad from '../../ui/OpPad';

interface MyProps { callback: Function }
interface MyState { }
class CommonTab extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state={};
        }

    render() {
        return (<Card>
            <Card.Body>
                <Row>
                    <Col sm={6}><NumPad callback={this.props.callback}></NumPad></Col><Col sm={6}><OpPad callback={this.props.callback}></OpPad></Col>
                </Row>
            </Card.Body>
        </Card>)
    }
}
export default CommonTab;