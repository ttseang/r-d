import React, { Component } from 'react';
import { ListGroupItem, Row, Col, ListGroup, Card, Button } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
interface MyProps {callback:Function,goBack:Function}
interface MyState { }
class HelpIndexScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getHelpList() {
        let fileRows: JSX.Element[] = [];

        for (let i: number = 0; i < this.ms.helps.length; i++) {

            let key:string="helpItem"+i.toString();
            fileRows.push(<ListGroupItem key={key} onClick={() => { this.props.callback(i) }}><Row><Col>{this.ms.helps[i].question}</Col><Col>{this.ms.fileList[i].eqstr}</Col></Row></ListGroupItem>)
            
        }
        return (<ListGroup style={{ width: "100%" }}>{fileRows}</ListGroup>);
    }
    render() {
        
        return (<div> 
            <Button onClick={()=>{this.props.goBack()}}>Back</Button>
            <Row><Col xs={12}>
            <Card>
                <Card.Body>
                    <div className='scroll2'>
                        {this.getHelpList()}
                    </div>
                </Card.Body>
            </Card></Col></Row></div>)
    }
}
export default HelpIndexScreen;