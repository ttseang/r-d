import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { CSSTypes, ProblemTypes } from '../classes/Constants';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
import { CSSVo } from '../dataObjs/CSSVo';
import { CellGridOutputVo } from '../dataObjs/outputs/CellGridOutputVo';
import { DCellOutput } from '../dataObjs/outputs/DCellOutput';
import { FractGridOutput } from '../dataObjs/outputs/FractGridOutput';
import { MCellGridOutputVo } from '../dataObjs/outputs/MCellGridOutputVo';
import { EqSaver } from '../util/api/EqSaver';
import DivCalculator from '../util/calcs/DivCalculator';
import { FractCalculator } from '../util/calcs/FractCalculator';
import { EqEngine2 } from '../util/EqEngine2';
import { StartData } from '../util/StartData';
import DebugEquationScreen from './DebugEquationScreen';
//import DebugEquationScreen from './DebugEquationScreen';
import EditScreen from './EditScreen';
import EquationScreen from './EquationScreen';
import FileNameScreen from './FileNameScreen';
import FileOpenScreen from './FileOpenScreen';
import HelpIndexScreen from './HelpIndexScreen';
import HelpScreen from './HelpScreen';
interface MyProps { }
interface MyState { mode: number, msg: string }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: Controller = Controller.getInstance();

    private dCalc: DivCalculator = new DivCalculator();
    private fcalc: FractCalculator = new FractCalculator();
    private helpIndex:number=0;

    /**
     * 
     * 
     * TODO:file open screen
     * 
     */

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0, msg: "" };
    }
    componentDidMount() {
        this.getCSSFromJson();
        this.mc.newProblem = () => {
            this.setState({ mode: 0 });
        }
        this.mc.doCalc = this.doCalc.bind(this);
        this.mc.openFile = () => { this.setState({ mode: 3 }) }
        this.mc.showHelp=()=>{this.setState({mode:4})};
    }
    getScreen() {
        switch (this.state.mode) {
            case -1:
                return "loading";
            case 0:
                if (this.ms.debug === true) {
                    return <DebugEquationScreen callback={this.showDisplay.bind(this)}></DebugEquationScreen>
                }

                return <EquationScreen openCallback={() => { this.setState({ mode: 3 }) }}></EquationScreen>

            case 1:
                //cells: this.ms.towerGrid2, mcells: this.ms.mTowerGrid, dcells: this.ms.dcells, fcells: this.ms.fcells,
                return <EditScreen changeScreen={this.changeScreen.bind(this)} saveInfo={this.doSaveInfo.bind(this)} cells={this.ms.towerGrid2} mcells={this.ms.mTowerGrid} dcells={this.ms.dcells} fcells={this.ms.fcells}></EditScreen>

            case 2:
                return <FileNameScreen callback={this.doSaveInfo.bind(this)} cancelCallback={() => { this.setState({ mode: 1 }) }}></FileNameScreen>

            case 3:
                return <FileOpenScreen goBack={() => { this.setState({ mode: 0 }); } } openCallback={()=>{this.setState({mode:1})}}></FileOpenScreen>

            case 4:
                return <HelpIndexScreen callback={this.setHelp.bind(this)} goBack={()=>{this.setState({mode:1})}}></HelpIndexScreen>
            
            case 5:
                return <HelpScreen helpIndex={this.helpIndex} goBack={()=>{this.setState({mode:4})}}></HelpScreen>
        }
    }
    setHelp(index:number)
    {
        this.helpIndex=index;
        this.setState({mode:5})
    }
    changeScreen(screenMode: number) {
        this.setState({ mode: screenMode });
    }
    doSaveInfo() {
        if (this.ms.saveObj !== null) {
            /* let fileVo: FileVo = new FileVo(this.ms.LtiFileName, this.ms.fileName, this.ms.expression, this.ms.maxSize, this.ms.sizes, this.ms.getSimpleSaveObjs());

            this.ms.fileVo = fileVo; */

            let eqSaver: EqSaver = new EqSaver(this.saveDone.bind(this));
            eqSaver.save();
        }
    }
    saveDone() {
        this.mc.setSaveScreen(false);
        this.setState({ mode: 1 });
    }
    showDisplay() {
        this.setState({ mode: 1 });
    }
    getCSSFromJson() {
        fetch("./cssList.json")
            .then(response => response.json())
            .then(data => this.gotCss(data));
    }
    gotCss(data: any) {

        let cssList: any = data.cssList;
        for (let i: number = 0; i < cssList.length; i++) {
            let css: any = cssList[i];

            let type: string = css.type;

            let cssType: CSSTypes = CSSTypes.Color;

            switch (type) {
                case "size":
                    cssType = CSSTypes.Size;
                    break;

                case "decor":
                    cssType = CSSTypes.Decor;
                    break;
            }


            let cssVo: CSSVo = new CSSVo(cssType, css.name, css.value);
            if (cssList[i].showAnimOnList === false) {
                cssVo.showAnimOnList = false;
            }
            this.ms.addCssType(cssVo);
        }
        //this.setState({mode:0});
        let sd: StartData = new StartData(this.gotStartData.bind(this));
        sd.getProblems();
    }
    gotStartData() {
        this.setState({ mode: 0 });
    }
    doCalc(text: string) {
        let eqArray: string[] = text.split("=");

        let eq2: string = eqArray[0];

        eq2 = eq2.replaceAll("÷", "/");
        eq2 = eq2.replaceAll("x", "*")

        let eqEngine: EqEngine2 = new EqEngine2(eq2);
        this.ms.problemType = eqEngine.problemType;

        //
        //
        //
        let data: CellGridOutputVo[] | null = null;
        let mData: MCellGridOutputVo[] | null = null;
        let dData: DCellOutput[] | null = null;
        let fData: FractGridOutput[] | null = null;

        let msg = "";
        //
        //
        //

        switch (this.ms.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:
                data = eqEngine.getStepsTower();

                if (data === null) {
                    msg = "Invalid";
                }
                else {
                    this.ms.towerGrid2 = data;
                    this.setState({ mode: 1 })
                    return;
                }
                break;

            case ProblemTypes.MULTIPLICATION:

                mData = eqEngine.getStepsMTower();

                if (mData === null) {
                    msg = "Invalid";
                }
                else {
                    this.ms.mTowerGrid = mData;
                    this.setState({ mode: 1 })
                    return;
                }
                break;
            case ProblemTypes.DIVISION:

                dData = eqEngine.getStepsD();

                if (dData === null) {
                    msg = "Invalid";
                }
                else {
                    this.ms.dcells = dData;
                    this.setState({ mode: 1 })
                    return;
                }
                break;

            case ProblemTypes.FRACTIONS:

                fData = eqEngine.getFractSteps();

                if (fData === null) {
                    msg = "Invalid";
                }
                else {
                    this.ms.fcells = fData;
                    this.setState({ mode: 1 })
                    return;
                }
        }
        this.setState({ msg: msg });
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Equation Tool</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;
