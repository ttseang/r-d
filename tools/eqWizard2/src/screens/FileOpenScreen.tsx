import React, { ChangeEvent, Component } from 'react';
import { Button, Card, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import { FileVo } from "../dataObjs/outputs/FileVo";
import { EqOpen } from '../util/api/EqOpen';
import ApiConnect from '../util/ApiConnect';

interface MyProps { goBack: Function, openCallback: Function }
interface MyState { selectedIndex: number, ltiFilter: string, files: FileVo[],noResultsFlag:boolean }
class FileOpenScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { selectedIndex: 0, ltiFilter: "", files: [],noResultsFlag:false };
    }
    getFileList() {
        let fileRows: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.files.length; i++) {
            let key: string = "fileItem" + i.toString();

            if (i === this.state.selectedIndex) {
                fileRows.push(<ListGroupItem key={key} variant='success'><Row><Col>{this.state.files[i].lti}</Col><Col>{this.state.files[i].eqstr}</Col></Row></ListGroupItem>)
            }
            else {
                fileRows.push(<ListGroupItem key={key} onClick={() => { this.setState({ selectedIndex: i }) }}><Row><Col>{this.state.files[i].lti}</Col><Col>{this.state.files[i].eqstr}</Col></Row></ListGroupItem>)
            }
        }
        return (<ListGroup style={{ width: "100%" }}>{fileRows}</ListGroup>);
    }
    onChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ ltiFilter: e.currentTarget.value, files: [] });
    }

    doSearch() {
        let api: ApiConnect = new ApiConnect();
        api.getFileList(this.gotSearch.bind(this), this.state.ltiFilter);
    }
    gotSearch(data: any) {
        
        if (data.length===0)
        {
            this.setState({noResultsFlag:true});
        }
        
        let files: FileVo[] = [];

        for (let i: number = 0; i < data.length; i++) {
            //console.log(data[i]);
            let fileVo: FileVo = new FileVo(data[i].lti, data[i].type, data[i].eqstring, []);
            //console.log(fileVo);
            files.push(fileVo);
        }
        this.setState({ files: files });
    }
    doOpen() {
        let file: FileVo = this.state.files[this.state.selectedIndex];
       // console.log(file);
        let eqOpen: EqOpen = new EqOpen(this.props.openCallback);
        eqOpen.openFile(file);
    }
    getNoResultsBox()
    {
        return (<Card>
            <Card.Body>
                <Row><Col className='tac'><h4>No Results Found</h4></Col></Row>
                <Row><Col className='tac'><Button onClick={()=>{this.setState({noResultsFlag:false})}}>OK</Button></Col></Row>
            </Card.Body>
        </Card>)
    }
    render() {
        if (this.state.noResultsFlag===true)
        {
            return this.getNoResultsBox();
        }
        if (this.state.files.length === 0) {
            return (<Row><Col sm={2}></Col><Col className="tar" sm={3}>LTI:</Col><Col sm={5}><input type='text' value={this.state.ltiFilter} onChange={this.onChange.bind(this)}></input></Col><Col sm={2}><Button onClick={() => { this.doSearch() }}>Fetch</Button></Col></Row>)
        }
        return (<div>
            <Button onClick={() => { this.props.goBack() }}>Back</Button>
            <hr />
            <Row><Col xs={12}>
                <Card>
                    <Card.Body>
                        <div className='scroll2'>
                            {this.getFileList()}
                        </div>
                    </Card.Body>
                </Card></Col></Row>
            <hr />
            <Row><Col><Button variant='success' onClick={() => { this.doOpen() }}>Open</Button><Button variant='danger'>Delete</Button></Col></Row>
        </div>)
    }
}
export default FileOpenScreen;