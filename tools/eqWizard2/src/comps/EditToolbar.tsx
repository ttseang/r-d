import { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { SidePanels } from '../classes/Constants';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
import { EditButtonVo } from '../dataObjs/EditButtonVo';
interface MyProps { }
interface MyState {selected:number[],mode:number}
class EditToolbar extends Component<MyProps, MyState>
{
    private controller:Controller=Controller.getInstance();
    private ms:MainStorage=MainStorage.getInstance();

        private buttonData:EditButtonVo[]=[];
        
        constructor(props: MyProps) {
            super(props);
            this.state = {selected:[2,0,0,0],mode:0};
            this.buttonData.push(new EditButtonVo("new","far fa-file",4,0));

            this.buttonData.push(new EditButtonVo("","fas fa-grip-lines-vertical",-1,0));

            this.buttonData.push(new EditButtonVo("cols","fas fa-columns",0,0));
            this.buttonData.push(new EditButtonVo("rows","fas fa-grip-lines",0,1));
            
            this.buttonData.push(new EditButtonVo("cells","far fa-minus-square",0,3));            
            this.buttonData.push(new EditButtonVo("CSS","fab fa-css3-alt",0,2));

            this.buttonData.push(new EditButtonVo("","fas fa-grip-lines-vertical",-1,0));

            this.buttonData.push(new EditButtonVo("view","fas fa-desktop",1,0));
            this.buttonData.push(new EditButtonVo("edit","fas fa-edit",1,1));
           

            this.buttonData.push(new EditButtonVo("","fas fa-grip-lines-vertical",-1,0));
            this.buttonData.push(new EditButtonVo("tools","fas fa-tools",4,1));
            this.buttonData.push(new EditButtonVo("showhide","far fa-eye",5,0));
            this.buttonData.push(new EditButtonVo("save","far fa-save",3,3));

            
        }
    
    doAction(cat:number,action:number)
    {
        switch(cat)
        {
            case 0:
            
            let panels:SidePanels[]=[SidePanels.Cols,SidePanels.Rows,SidePanels.CSS,SidePanels.CellTools];
            this.controller.setEditMode(1);
            this.controller.setSidePanel(panels[action]);
            break;

            case 1:
                if (action===0)
                {
                    this.controller.setSidePanel(0);
                }
                this.controller.setEditMode(action);
                break;
            
            case 3:
                this.controller.saveData();

                break;

            case 4:
                if (action===0)
                {
                    this.controller.newProblem();
                }
                if (action===1)
                {
                   // this.controller.doCalc(this.ms.expression);
                   this.controller.setSidePanel(SidePanels.Tools);
                }
                break;

            case 5:
                if (action===0)
                {
                    this.ms.showHidden=!this.ms.showHidden;
                    this.controller.showHide(this.ms.showHidden);
                  
                }
        }
        let selectArray:number[]=this.state.selected.slice();
        selectArray[cat]=action;
        this.setState({selected:selectArray});
    }
    makeButtons()
    {
        let buttons:JSX.Element[]=[];
        for (let i:number=0;i<this.buttonData.length;i++)
        {  
            let data:EditButtonVo=this.buttonData[i];
            let key:string="tbButton"+i.toString();

            if (data.buttonCat>-1)
            {

            let variantString:string="light";
            if (this.state.selected[data.buttonCat]===data.action)
            {
                variantString="dark";
            }
            if (data.buttonCat===3)
            {
                variantString="success";
            }
            if (data.buttonCat===4)
            {
                variantString="warning";
            }
            if (data.buttonCat===5)
            {
                if (this.ms.showHidden===true)
                {
                    data.icon="far fa-eye-slash";
                }
                else
                {
                    data.icon="far fa-eye";
                }
                variantString="info";
            }
            buttons.push(<Button key={key} title={data.name} variant={variantString} onClick={()=>{this.doAction(data.buttonCat,data.action)}}><i className={data.icon}></i></Button>)
        }
        else
        {
            buttons.push(<div key={key} className='editButtonSep'></div>)
        }
        }
        return (<ButtonGroup>{buttons}</ButtonGroup>)
    }
    setMode(panel:SidePanels)
    {
      
       this.controller.setSidePanel(panel);
    }
    getButtons()
    {
        return (<ButtonGroup><Button variant='dark' onClick={()=>{this.setMode(SidePanels.Cols)}}>Cols</Button><Button variant='light' onClick={()=>{this.setMode(SidePanels.Rows)}}>Rows</Button><Button onClick={()=>{this.setMode(SidePanels.CSS)}}>Css</Button></ButtonGroup>)
    }
    render() {
        return (<div>
            {this.makeButtons()}
        </div>)
    }
}
export default EditToolbar;