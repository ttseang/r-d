import React, { Component } from 'react';
import { CellGridOutputVo } from '../../dataObjs/outputs/CellGridOutputVo';
import { CellVo } from '../../dataObjs/CellVo';
import MainStorage from '../../classes/MainStorage';
import { Unicodes } from '../../classes/Constants';
import { GridUtil } from '../../util/GridUtil';
interface MyProps { cellInfo: CellGridOutputVo, fontSize: number, idString: string }
interface MyState { cellInfo: CellGridOutputVo,colCount:number }
class RTower extends Component<MyProps, MyState>
{
    //private colCount: number = 0;

    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        let  colCount:number = this.props.cellInfo.getMaxCols() + 1;
        this.state = { cellInfo: this.props.cellInfo,colCount:colCount};
       
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            let  colCount:number = this.props.cellInfo.getMaxCols() + 1;
            this.setState({ cellInfo: this.props.cellInfo,colCount:colCount });
        }
    }
    makeCols(index: number, cells: CellVo[], addOp: boolean = false, operator: string = "+") {

        cells = GridUtil.fixDec(cells);
        
        let cols: JSX.Element[] = [];

        if (addOp === true) {
            let key2: string = "op" + this.ms.columnIndex.toString();

            cols.push(<span key={key2}>&nbsp;{Unicodes[operator]}&nbsp;</span>);
        }

        for (let i: number = 0; i < cells.length; i++) {
            cols.push(cells[i].toHtml(i));
        }

        while (cols.length < this.state.colCount) {
            let key: string = "col" + this.ms.columnIndex.toString();
            this.ms.columnIndex++;
            cols.unshift(<span key={key}>&nbsp;</span>);

        }

        return cols;//`<span>${cols.join(" ")}</span>`;
    }
    getRows() {
        let rowArray: JSX.Element[] = [];
        const { rows, operator } = this.state.cellInfo;

        if (rows) {
            rows.forEach((row: CellVo[], index: number) => {

                const addOp: boolean = (index === rows.length - 1);
               
                row = row.slice();

                /* if (index===rows.length-1)
                {
                    row.unshift(new CellVo(' '+Unicodes['x']+' ',[]));
                } */

                // row=this.stuffCols(row);


                let cols = this.makeCols(index, row, addOp, operator);

                let rowKey: string = "mrow" + index.toString();

                const spacing: string = " 1fr".repeat(this.state.colCount);

                rowArray.push(<div className="ttrow" key={rowKey} style={{ gridTemplateColumns: spacing }}>{cols}</div>);
            });
        }

        return rowArray;
    }
    getTop() {
        let topRows: JSX.Element[] = [];
        const top = this.state.cellInfo.top;

        if (top) {
            top.forEach((t: CellVo[], index: number) => {
                //   t=this.stuffCols(t);
                const cols: JSX.Element[] = this.makeCols(index, t);
                const spacing: string = " 1fr".repeat(this.state.colCount);
                let key: string = "topRow" + index.toString();
                topRows.push(<div className="ttrow small" key={key} style={{ gridTemplateColumns: spacing }}>{cols}</div>);
            });
        }
        return topRows;
    }
    getAnswer() {
        const cols = this.makeCols(20, this.state.cellInfo.answer);
        const spacing: string = " 1fr".repeat(this.state.colCount);
        return <div className="ttrow answer" style={{ gridTemplateColumns: spacing }}>{cols}</div>
    }
    render() {
        return <div className="tteq"><div className="equation addition">
            {this.getTop()}
            {this.getRows()}
            <hr />
            {this.getAnswer()}
        </div></div>

    }
}
export default RTower;