import React, { Component } from 'react';
import { CellVo } from '../../dataObjs/CellVo';
import { DCellOutput } from '../../dataObjs/outputs/DCellOutput';
interface MyProps { cellInfo: DCellOutput, fontSize: number }
interface MyState { cellInfo: DCellOutput }
class DivDisplay extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { cellInfo: this.props.cellInfo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ cellInfo: this.props.cellInfo });
        }
    }
    
  
    private makeCols(index: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];

        cells.forEach(cell => cols.push(cell.toHtml(index)));

        return cols;
    }
    private getProductRows() {
        let rows: JSX.Element[] = [];

        this.state.cellInfo.products.forEach((product:CellVo[], index:number) => {
            
            const bottom:boolean = !(index % 2);

            rows.push(<div></div>);
            if (bottom)
            {
                rows.push(<div>&nbsp;&nbsp;&nbsp;<span className="bottomBorder"> {this.makeCols(index, product)}</span></div>);
            }
            else
            {
                rows.push(<div>&nbsp;&nbsp;&nbsp;{this.makeCols(index, product)}</div>);
            }
           // rows.push(<div>&nbsp;&nbsp;&nbsp;{(bottom ? `<span class="bottomBorder">` : "") + this.makeCols(index, product) + (bottom ? "</span>" : "")}</div>);
        });

        return rows;
    }
   /*  getProductRows() {
        let rows: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.cellInfo.products.length; i++) {
            let key1:string="blank"+i.toString();
            let key2:string="prow"+i.toString();

            rows.push(<div key={key1}></div>)
            rows.push(<div key={key2}>{this.makeCols(i, this.state.cellInfo.products[i])}</div>);
        }
        return rows;
    } */
    render() {
       
        const { quotient, divisor, dividend } = this.state.cellInfo;

        return <div className='tteq'><div className="equation division">
                <div></div>
                <div className="quotient">&nbsp;&nbsp;&nbsp;{this.makeCols(0, quotient)}</div>
                <div className="divisor">{this.makeCols(0, divisor)}&nbsp;</div>
                <div className="dividend">
                    <div className="inner">
                        <i className="circle"></i>
                        &nbsp;&nbsp;&nbsp;{this.makeCols(0, dividend)}
                    </div>
                </div>
                {this.getProductRows()}
            </div></div>

    }
}
export default DivDisplay;