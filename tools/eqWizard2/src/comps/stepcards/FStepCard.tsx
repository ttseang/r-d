import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { FractGridOutput } from '../../dataObjs/outputs/FractGridOutput';
import FractDisplay from '../displays/FractDisplay';
interface MyProps { index: number, stepData: FractGridOutput, callback: Function, selected: boolean }
interface MyState { stepData: FractGridOutput, selected: boolean }
class FStepCard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData, selected: this.props.selected };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData, selected: this.props.selected });
        }
    }
    render() {
        let selectedClass: string = (this.state.selected === true) ? "selectedCard" : "unselectedCard";

        return (<Card className="stepCard" onClick={() => { this.props.callback(this.props.index) }}>
            <Card.Header className={selectedClass}>Step {this.props.index}</Card.Header>
            <Card.Body className='stepCardBody'>
                <FractDisplay cells={this.state.stepData} fontSize={22}></FractDisplay>
            </Card.Body>
        </Card>)
    }
}
export default FStepCard;