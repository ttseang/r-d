import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { MCellGridOutputVo } from '../../dataObjs/outputs/MCellGridOutputVo';
import RMTower from '../displays/RMTower';

interface MyProps { index: number, stepData: MCellGridOutputVo, callback: Function,selected:boolean }
interface MyState { stepData: MCellGridOutputVo,selected:boolean }
class MStepCard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData,selected:this.props.selected };
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({stepData:this.props.stepData,selected:this.props.selected})
        }
    }
    render() {
        
        let selectedClass:string=(this.state.selected===true)?"selectedCard":"unselectedCard";

        return (<Card className="stepCard" onClick={() => { this.props.callback(this.props.index) }}>
            <Card.Header className={selectedClass}>Step {this.props.index}</Card.Header>
            <Card.Body className='stepCardBody'>
                <RMTower cellInfo={this.state.stepData} fontSize={20} idString={'mathDisplayRel'}></RMTower>
            </Card.Body>
        </Card>)
    }
}
export default MStepCard;