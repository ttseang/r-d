import { Component } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { CSSTypes } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import { CSSVo } from '../../dataObjs/CSSVo';
interface MyProps { cssVo: CSSVo, selected: boolean }
interface MyState { selected: boolean }
class CssCard extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { selected: this.props.selected };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ selected: this.props.selected });
        }
    }
    private update() {
        this.controller.setStyle(this.props.cssVo);
    }
   private getText() {     

        if (this.props.cssVo.type === CSSTypes.Header) {
            
            return <h3 className='tac'>{this.props.cssVo.name}</h3>
        }
        if (this.props.cssVo.type !== CSSTypes.Size) {
            
            if (this.props.cssVo.showAnimOnList===false)
            {
                return <div>{this.props.cssVo.name}</div>
            }
            let fullClass:string="tteq "+this.props.cssVo.value;

            return <div className={fullClass} style={{ fontSize: "22px" }}>{this.props.cssVo.name}</div>
        }
      
       
        return <div className={this.props.cssVo.value}>{this.props.cssVo.name}</div>
    }
    private getCheck()
    {
        if (this.state.selected===false || this.props.cssVo.type===CSSTypes.Header)
        {
            return "";
        }
        return <i className='fas fa-check'></i>
    }
    render() {
        //<i class="fas fa-check"></i>
        return (<Card onClick={this.update.bind(this)}>
            <Card.Body>
                <Row><Col sm={10}>{this.getText()}</Col><Col>{this.getCheck()}</Col></Row>
            </Card.Body>
        </Card>)
    }
}
export default CssCard;