import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { RowPos, TowerPosition } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import AllStepSelect from './AllStepSelect';
interface MyProps { }
interface MyState { }
class RowBox extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div className='insertCard'>
            <Card>
                <Card.Body>
                    <h3>Top Rows</h3>
                    <Row>
                        <Col sm={6}>To Top</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addRow(TowerPosition.Top, RowPos.Top) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subRow(TowerPosition.Top, RowPos.Top) }}>-</Button></Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col sm={6}>To Bottom</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addRow(TowerPosition.Top, RowPos.Bottom) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subRow(TowerPosition.Top, RowPos.Bottom) }}>-</Button></Col>
                    </Row>
                    <hr />
                    <h3>Body Rows</h3>
                    <Row>
                        <Col sm={6}>To Top</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addRow(TowerPosition.Body, RowPos.Top) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subRow(TowerPosition.Body, RowPos.Top) }}>-</Button></Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col sm={6}>To Bottom</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addRow(TowerPosition.Body, RowPos.Bottom) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subRow(TowerPosition.Body, RowPos.Bottom) }}>-</Button></Col>
                    </Row>
                    <Row><Col><AllStepSelect></AllStepSelect></Col></Row>
                </Card.Body>
            </Card></div>)
    }
}
export default RowBox;