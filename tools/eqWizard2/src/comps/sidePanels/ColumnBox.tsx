import React, { Component } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import { TowerPosition, ColPos } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import AllStepSelect from './AllStepSelect';
interface MyProps { }
interface MyState { }
class ColumnBox extends Component<MyProps, MyState>
{
    private controller:Controller=Controller.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
        
    render() {
        return (
            <div className='insertCard'>
            <Card>
                <Card.Body>
                    <Card.Title>Columns</Card.Title>
                    <Row>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addColumn(TowerPosition.Top,ColPos.Left)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subColumn(TowerPosition.Top,ColPos.Left)}}>-</Button></Col>
                        <Col sm={4}>Top</Col>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addColumn(TowerPosition.Top,ColPos.Right)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subColumn(TowerPosition.Top,ColPos.Right)}}>-</Button></Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addColumn(TowerPosition.Body,ColPos.Left)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subColumn(TowerPosition.Body,ColPos.Left)}}>-</Button></Col>
                        <Col sm={4}>Body</Col>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addColumn(TowerPosition.Body,ColPos.Right)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subColumn(TowerPosition.Body,ColPos.Right)}}>-</Button></Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addColumn(TowerPosition.Answer,ColPos.Left)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subColumn(TowerPosition.Answer,ColPos.Left)}}>-</Button></Col>
                        <Col sm={4}>Answer</Col>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addColumn(TowerPosition.Answer,ColPos.Right)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subColumn(TowerPosition.Answer,ColPos.Right)}}>-</Button></Col>
                    </Row>
                    <Row><Col><AllStepSelect></AllStepSelect></Col></Row>
                </Card.Body>
            </Card></div>
        )
    }
}
export default ColumnBox;