import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { DividePosition, RowPos } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
interface MyProps { }
interface MyState { }
class DRowBox extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div className='insertCard'>
            <Card>
                <Card.Body>
                    <Card.Title>Rows</Card.Title>
                    
                    <h4>Product</h4>
                    <Row>
                        <Col sm={6}>Top</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addDRow(DividePosition.Products,RowPos.Top) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subDRow(DividePosition.Products,RowPos.Top) }}>-</Button></Col>
                    </Row>
                   
                    <Row>
                        <Col sm={6}>Bottom</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addDRow(DividePosition.Products,RowPos.Bottom) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subDRow(DividePosition.Products,RowPos.Bottom) }}>-</Button></Col>
                    </Row>
                </Card.Body>
            </Card></div>)
    }
}
export default DRowBox;