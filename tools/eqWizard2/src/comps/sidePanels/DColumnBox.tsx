import { Component } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import { ColPos, DividePosition } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
interface MyProps { }
interface MyState { }
class DColumnBox extends Component<MyProps, MyState>
{
    private controller:Controller=Controller.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
        
    render() {
        return (
            <div className='insertCard'>
            <Card>
                <Card.Body>
                    <Card.Title>Columns</Card.Title>
                    <Row>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Quotient,ColPos.Left)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Quotient,ColPos.Left)}}>-</Button></Col>
                        <Col sm={4}>Quotient</Col>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Quotient,ColPos.Right)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Quotient,ColPos.Right)}}>-</Button></Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Divisor,ColPos.Left)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Divisor,ColPos.Left)}}>-</Button></Col>
                        <Col sm={4}>Divisor</Col>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Divisor,ColPos.Right)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Divisor,ColPos.Right)}}>-</Button></Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Dividend,ColPos.Left)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Dividend,ColPos.Left)}}>-</Button></Col>
                        <Col sm={4}>Dividend</Col>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Dividend,ColPos.Right)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Dividend,ColPos.Right)}}>-</Button></Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Products,ColPos.Left)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Products,ColPos.Left)}}>-</Button></Col>
                        <Col sm={4}>Products</Col>
                        <Col sm={2}><Button size='sm' variant='success' onClick={()=>{this.controller.addDColumn(DividePosition.Products,ColPos.Right)}}>+</Button></Col>
                        <Col sm={2}><Button size='sm' variant='danger' onClick={()=>{this.controller.subDColumn(DividePosition.Products,ColPos.Right)}}>-</Button></Col>
                    </Row>                  
                </Card.Body>
            </Card></div>
        )
    }
}
export default DColumnBox;