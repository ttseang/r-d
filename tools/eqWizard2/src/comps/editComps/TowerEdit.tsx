import React, { ChangeEvent, Component } from 'react';
import { ColPos, RowPos, TowerPosition } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import MainStorage from '../../classes/MainStorage';

import { CellGridOutputVo } from '../../dataObjs/outputs/CellGridOutputVo';
import { CellVo } from '../../dataObjs/CellVo';
import { CSSVo } from '../../dataObjs/CSSVo';
import { TowerEditUtil } from '../../util/editUtils/TowerEditUtil';
import { CssStyleUtil } from '../../util/editUtils/CssStyleUtil';

interface MyProps { stepData: CellGridOutputVo, callback: Function }
interface MyState { stepData: CellGridOutputVo }
class TowerEdit extends Component<MyProps, MyState>
{

    private cellIndex: number = 0;
    private controller: Controller = Controller.getInstance();
    private currentCell: HTMLInputElement | null = null;
    private currentCellVo: CellVo | null = null;
    private currentX: number = -1;
    private currentY: number = -1;
    private currentPos: TowerPosition | null = null;

    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData };


    }

    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData });
        }
    }
    componentDidMount() {
        this.controller.addColumn = this.addColumn.bind(this);
        this.controller.subColumn = this.subColumn.bind(this);
        this.controller.addRow = this.addRow.bind(this);
        this.controller.subRow = this.subRow.bind(this);
        this.controller.setStyle = this.changeCss.bind(this);
        this.controller.shiftColLeft = this.shiftColsLeft.bind(this);
        this.controller.shiftColRight = this.shiftColsRight.bind(this);
        this.controller.insertColLeft = this.insertLeft.bind(this);
        this.controller.insertColRight = this.insertRight.bind(this);
        this.controller.deleteCell = this.deleteCell.bind(this);
    }
    componentWillUnmount() {
        this.controller.addColumn = () => { };
        this.controller.subColumn = () => { };
        this.controller.addRow = () => { };
        this.controller.subRow = () => { };
        this.controller.setStyle = () => { };
        this.controller.shiftColLeft = () => { };
        this.controller.shiftColRight = () => { };
        this.controller.insertColLeft = () => { };
        this.controller.insertColRight = () => { };
        this.controller.deleteCell = () => { };
    }
    getCell(tp: TowerPosition, x: number, y: number, cell: CellVo) {
        let key = "editCell_" + x.toString() + "_" + y.toString();
        this.cellIndex++;
        let id: string = "cell" + this.cellIndex.toString();
        let char: string = cell.text;

        if (cell.classArray.includes("hid") && char==="0")
        {
            char="@";
        }

        let classString: string = "cellEdit";
        if (char === "@") {
            classString = "cellEditFade";
        }
        return (<input key={key} id={id} maxLength={2} type='text' className={classString} value={char} onFocus={(e: React.FocusEvent<HTMLInputElement>) => { this.onFocus(tp, x, y, e) }} onChange={(e: ChangeEvent<HTMLInputElement>) => { this.updateGrid(tp, x, y, e) }}></input>)
    }
    onFocus(tp: TowerPosition, x: number, y: number, e: React.FocusEvent<HTMLInputElement>) {
        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;

        if (this.currentCell) {
            this.currentCell.classList.remove('inputSelected');
        }

        inputBox.select();
        inputBox.classList.add("inputSelected");
        this.currentCell = inputBox;
        this.currentPos = tp;
        this.currentY = y;
        this.currentX = x;

      
        this.currentCellVo = this.getCellVo(tp, x, y);
       

        if (this.currentCellVo) {
            this.controller.setSelectedCss(this.currentCellVo.classArray);
            this.controller.setTowerCell(tp, x, y, this.currentCellVo);
        }


    }
    changeCss(cssVo: CSSVo) {
        

        if (this.currentPos !== null) {
            let stepDataTemp: CellGridOutputVo = this.state.stepData;
            let cell: CellVo | null = this.getCellVo(this.currentPos, this.currentX, this.currentY);
          

            if (cell) {
                //cell.addCss(cssVo);
                CssStyleUtil.addCssToCell(cssVo,cell);
            }
            this.props.callback(stepDataTemp);
            this.setState({ stepData: stepDataTemp });
            if (cell) {
                this.controller.setSelectedCss(cell.classArray);
            }

        }
    }
    private getCellVo(tp: TowerPosition, x: number, y: number) {
        let stepDataTemp: CellGridOutputVo = this.state.stepData;
        let cell: CellVo | null = null;
        switch (tp) {
            case TowerPosition.Body:
                //  stepDataTemp.rows[x][y].text=char;
                cell = stepDataTemp.rows[y][x];
                break;

            case TowerPosition.Top:
                cell = stepDataTemp.top[y][x];
                break;

            case TowerPosition.Answer:
                cell = stepDataTemp.answer[x];
        }

        return cell;
    }
    updateGrid(tp: TowerPosition, x: number, y: number, e: ChangeEvent<HTMLInputElement>) {
      
        let char: string = e.currentTarget.value;
        let stepDataTemp: CellGridOutputVo = this.state.stepData;

        let cell: CellVo = this.getCellVo(tp, x, y);

        if (cell) {

            cell.setEditText(char);
        }

        this.props.callback(stepDataTemp);

        this.setState({ stepData: stepDataTemp });
        if (this.ms.jumpToNext === true) {
            let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;
            let cellIndex = parseInt(inputBox.id.replace("cell", ""));
            if (!isNaN(cellIndex)) {
                cellIndex++;
                let nBox: HTMLInputElement = document.getElementById("cell" + cellIndex.toString()) as HTMLInputElement;
                if (nBox) {
                    nBox.focus();
                }
            }
        }
    }
    shiftColsLeft() {
        let stepData: CellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case TowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentX];
                if (tcells) {
                    tcells.shift();
                    tcells.push(new CellVo("@", ['hid']));
                }


                break;

            case TowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    cells.shift();
                    cells.push(new CellVo("@", ['hid']));
                }



                break;

            case TowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.shift();
                acells.push(new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    shiftColsRight() {
        let stepData: CellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case TowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentY]
                tcells.pop();
                tcells.unshift(new CellVo("@", ['hid']));

                break;

            case TowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    cells.pop();
                    cells.unshift(new CellVo("@", ['hid']));
                }


                break;

            case TowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.pop();
                acells.unshift(new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    insertLeft() {
        let stepData: CellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case TowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentY];
                tcells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                // tcells.pop();
                //tcells.unshift(new CellVo("@",['hid']));

                break;

            case TowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    
                    cells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }


                break;

            case TowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.splice(this.currentX, 0, new CellVo("@", ['hid']));


                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    insertRight() {
        let stepData: CellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case TowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentY];
                tcells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));

                break;

            case TowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    cells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']))

                }


                break;

            case TowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    deleteCell() {
        let stepData: CellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case TowerPosition.Top:
                let tcells: CellVo[] = stepData.top[this.currentY];
                tcells.splice(this.currentX, 1);
                break;

            case TowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    cells.splice(this.currentX, 1);
                }
                break;

            case TowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.splice(this.currentX, 1);
                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    getCols(tp: TowerPosition, row: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];
        for (let i: number = 0; i < cells.length; i++) {
            let id: string = row.toString() + "_" + i.toString();

            cols.push(<span key={id} id={id}>{this.getCell(tp, i, row, cells[i])}</span>);
        }
        let rowKey: string = "editRow" + row.toString();

        return (<div key={rowKey}>{cols}</div>);
    }
    getTowerRows() {
        let dataRows: CellVo[][] = this.state.stepData.rows;
        let rows: JSX.Element[] = [];

        for (let i: number = 0; i < dataRows.length; i++) {
            rows.push(this.getCols(TowerPosition.Body, i, dataRows[i]));
        }
        return rows;
    }
    subRow(part: TowerPosition, pos: RowPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;
      
        if (this.ms.applyToAllSteps === true) {
            TowerEditUtil.subRowsFromAllSteps(part, pos);
            tempCells = this.ms.towerGrid2[this.ms.step];
        }
        else {
            tempCells = TowerEditUtil.subRow(part, pos, tempCells);
        }

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }
    addRow(part: TowerPosition, pos: RowPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;
        if (this.ms.applyToAllSteps === true) {
            TowerEditUtil.addRowsToAllSteps(part, pos);
            tempCells = this.ms.towerGrid2[this.ms.step];
        }
        else {
            tempCells = TowerEditUtil.addRow(part, pos, tempCells);
        }



        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }
    addColumn(part: TowerPosition, side: ColPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;
        if (this.ms.applyToAllSteps === true) {
            TowerEditUtil.addColsToAllSteps(part, side);
            tempCells = this.ms.towerGrid2[this.ms.step];
        }
        else {
            tempCells = TowerEditUtil.addCol(part, side, tempCells);
        }


        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }
    subColumn(part: TowerPosition, side: ColPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;

        if (this.ms.applyToAllSteps === true) {
            TowerEditUtil.subColsToAllSteps(part, side);
            tempCells = this.ms.towerGrid2[this.ms.step];
        }
        else {
            tempCells = TowerEditUtil.subColumn(part, side, tempCells);
        }

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }

    test() {
        this.addColumn(TowerPosition.Body, ColPos.Right)
    }
    getTop() {
        let tops: CellVo[][] = this.state.stepData.top;
        let topCells: JSX.Element[] = [];

        for (let i: number = tops.length - 1; i > -1; i--) {
            topCells.push(this.getCols(TowerPosition.Top, i, tops[i]));
        }
        return topCells;
    }
    render() {
        this.cellIndex = 0;

        return (<div><div id="towerEdit">
            <div className='labelText'>Top</div>
            {this.getTop()}
            <hr />
            <div className='labelText'>Body</div>
            {this.getTowerRows()}
            <hr />
            <div className='labelText'>Answer</div>
            {this.getCols(TowerPosition.Answer, 100, this.state.stepData.answer)}
        </div>

        </div>)
    }
}
export default TowerEdit;