import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { ColPos, DividePosition, MTowerPosition, RowPos } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import MainStorage from '../../classes/MainStorage';
import { CellVo } from '../../dataObjs/CellVo';
import { CSSVo } from '../../dataObjs/CSSVo';
import { DCellOutput } from '../../dataObjs/outputs/DCellOutput';
import { CssStyleUtil } from '../../util/editUtils/CssStyleUtil';
interface MyProps { stepData: DCellOutput, callback: Function }
interface MyState { stepData: DCellOutput }
class DivideEdit extends Component<MyProps, MyState>
{
    private cellIndex: number = 0;
    private controller: Controller = Controller.getInstance();
    private currentCell: HTMLInputElement | null = null;
    private currentCellVo: CellVo | null = null;
    private currentX: number = -1;
    private currentY: number = -1;
    private currentPos: DividePosition | null = null;
    private maxCols: number = 0;

    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData };

    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData });
        }

    }
    componentDidMount() {
        this.controller.addDColumn = this.addColumn.bind(this);
        this.controller.subDColumn = this.subColumn.bind(this);
        this.controller.addDRow = this.addRow.bind(this);
        this.controller.subDRow = this.subRow.bind(this);
        this.controller.setStyle = this.changeCss.bind(this);

        this.controller.shiftColLeft = this.shiftColsLeft.bind(this);
        this.controller.shiftColRight = this.shiftColsRight.bind(this);

        this.controller.insertColLeft = this.insertColsLeft.bind(this);
        this.controller.insertColRight = this.insertColsRight.bind(this);

        this.controller.deleteCell = this.deleteCell.bind(this);
    }
    componentWillUnmount() {
        this.controller.addDColumn = () => { };
        this.controller.subDColumn = () => { };
        this.controller.addDRow = () => { };
        this.controller.subDRow = () => { };
        this.controller.setStyle = () => { };

        this.controller.shiftColLeft = () => { };
        this.controller.shiftColRight = () => { };

        this.controller.insertColLeft = () => { };
        this.controller.insertColRight = () => { };

        this.controller.deleteCell = () => { };
    }
    private getCellVo(tp: DividePosition, x: number, y: number) {
        let stepDataTemp: DCellOutput = this.state.stepData;
        let cell: CellVo | null = null;
        switch (tp) {
            case DividePosition.Quotient:
                //  stepDataTemp.rows[x][y].text=char;
                cell = stepDataTemp.quotient[x];
                break;

            case DividePosition.Products:             

                cell = stepDataTemp.products[y][x];               

                break;

            case DividePosition.Divisor:
                cell = stepDataTemp.divisor[x];
                // cell = stepDataTemp.answer[y];
                break;

            case DividePosition.Dividend:
                cell = stepDataTemp.dividend[x];
                break;
        }

        return cell;
    }
    getCell(tp: DividePosition, x: number, y: number, cell: CellVo) {
        let key = "editCell_" + x.toString() + "_" + y.toString();
        this.cellIndex++;
        let id: string = "cell" + this.cellIndex.toString();
        let char: string = cell.text;
        let classString: string = "cellEdit";

        if (char === "0" && cell.classArray.includes("hid")) {
            char = "@";
        }

        if (char === "@") {
            classString = "cellEditFade";
        }
        return (<input key={key} id={id} maxLength={1} type='text' className={classString} value={char} onFocus={(e: React.FocusEvent<HTMLInputElement>) => { this.onFocus(tp, x, y, e) }} onChange={(e: ChangeEvent<HTMLInputElement>) => { this.updateGrid(tp, x, y, e) }}></input>)
    }
    updateGrid(tp: DividePosition, x: number, y: number, e: ChangeEvent<HTMLInputElement>) {
       
        let char: string = e.currentTarget.value;
        let stepDataTemp: DCellOutput = this.state.stepData;

        let cell: CellVo | null = this.getCellVo(tp, x, y);
       
        if (cell) {
            

            cell.setEditText(char);
        }

        this.props.callback(stepDataTemp);

        this.setState({ stepData: stepDataTemp });

        if (this.ms.jumpToNext === true) {
            let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;
            let cellIndex = parseInt(inputBox.id.replace("cell", ""));
            if (!isNaN(cellIndex)) {
                cellIndex++;
                let nBox: HTMLInputElement = document.getElementById("cell" + cellIndex.toString()) as HTMLInputElement;
                if (nBox) {
                    nBox.focus();
                }
            }
        }
    }
    getCols(tp: DividePosition, row: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];

        let len: number = cells.length;

        if (tp === DividePosition.Products) {
            if (len < this.maxCols) {
                len = this.maxCols;
            }
        }

        for (let i: number = 0; i < len; i++) {
            let id: string = row.toString() + "_" + i.toString();
            if (cells[i] === undefined) {
                cells[i] = new CellVo("@", []);
            }
            cols.push(<span key={id} id={id}>{this.getCell(tp, i, row, cells[i])}</span>);
        }
        let rowKey: string = "editRow" + row.toString();

        return (<div key={rowKey}>{cols}</div>);
    }
    onFocus(tp: DividePosition, x: number, y: number, e: React.FocusEvent<HTMLInputElement>) {
        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;

        if (this.currentCell) {
            this.currentCell.classList.remove('inputSelected');
        }

        inputBox.select();
        inputBox.classList.add("inputSelected");
        this.currentCell = inputBox;
        this.currentPos = tp;
        this.currentY = y;
        this.currentX = x;

       
        this.currentCellVo = this.getCellVo(tp, x, y);
     

        if (this.currentCellVo) {
            this.controller.setSelectedCss(this.currentCellVo.classArray);
            this.controller.setDTowerCell(tp, x, y, this.currentCellVo);
        }


    }
    getProductRows() {
        let dataRows: CellVo[][] = this.state.stepData.products;
        let rows: JSX.Element[] = [];

        let max: number = 0;
        for (let i: number = 0; i < dataRows.length; i++) {
            if (dataRows[i].length > max) {
                max = dataRows[i].length;
            }
        }

        this.maxCols = max;

        for (let i: number = 0; i < dataRows.length; i++) {
            rows.push(this.getCols(DividePosition.Products, i, dataRows[i]));
        }
        return rows;
    }
    changeCss(cssVo: CSSVo) {
       

        if (this.currentPos !== null) {
            let stepDataTemp: DCellOutput = this.state.stepData;
            let cell: CellVo | null = this.getCellVo(this.currentPos, this.currentX, this.currentY);
         

            if (cell) {
                CssStyleUtil.addCssToCell(cssVo,cell);
               // cell.addCss(cssVo);
                this.controller.setSelectedCss(cell.classArray);
            }
            this.props.callback(stepDataTemp);
            this.setState({ stepData: stepDataTemp });

        }
    }
    addColumn(part: DividePosition, side: ColPos) {
        let tempCells: DCellOutput = this.state.stepData;
       

        switch (part) {
            case DividePosition.Products:
                let dataRows: CellVo[][] = tempCells.products;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].push(new CellVo("@", []));
                    }
                    else {
                        dataRows[i].unshift(new CellVo("@", []));
                    }

                }
                break;

            case DividePosition.Quotient:

                

                let top: CellVo[] = tempCells.quotient;

                if (side === ColPos.Right) {
                    top.push(new CellVo("@", []));
                }
                else {
                    top.unshift(new CellVo("@", []));
                }


                break;

            case DividePosition.Divisor:
                let divisor: CellVo[] = tempCells.divisor;
                if (side === ColPos.Right) {
                    divisor.push(new CellVo("@", []));
                }
                else {
                    divisor.unshift(new CellVo("@", []));
                }
                break;

            case DividePosition.Dividend:
                let dividend: CellVo[] = tempCells.dividend;
                if (side === ColPos.Right) {
                    dividend.push(new CellVo("@", []));
                }
                else {
                    dividend.unshift(new CellVo("@", []));
                }
        }
        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }
    subColumn(part: DividePosition, side: ColPos) {
        let tempCells: DCellOutput = this.state.stepData;

        switch (part) {
            case DividePosition.Products:
                let dataRows: CellVo[][] = tempCells.products;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].pop();
                    }
                    else {
                        dataRows[i].shift();
                    }

                }
                break;

            case DividePosition.Quotient:
                let top: CellVo[] = tempCells.quotient;

                if (side === ColPos.Right) {
                    top.pop();
                }
                else {
                    top.shift();
                }


                break;

            case DividePosition.Divisor:
                let divisor: CellVo[] = tempCells.divisor;
                if (side === ColPos.Right) {
                    divisor.pop();
                }
                else {
                    divisor.shift();
                }
                break;

            case DividePosition.Dividend:
                let dividend: CellVo[] = tempCells.dividend;
                if (side === ColPos.Right) {
                    dividend.pop();
                }
                else {
                    dividend.shift();
                }
        }

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }
    subRow(part: MTowerPosition, pos: RowPos) {
        let tempCells: DCellOutput = this.state.stepData;
        let dataRows: CellVo[][] = tempCells.products;

        if (pos === RowPos.Bottom) {
            dataRows.pop();
        }
        else {
            dataRows.shift();
        }
        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }
    addRow(part: DividePosition, pos: RowPos) {
        let tempCells: DCellOutput = this.state.stepData;

        let dataRows: CellVo[][] = tempCells.products;


        let len: number = 4;
        if (tempCells.products.length > 0) {
            for (let j: number = 0; j < tempCells.products.length; j++) {
                if (tempCells.products[j].length > len) {
                    len = tempCells.products[j].length;
                }
            }

        }
        let cols: CellVo[] = [];
        for (let i: number = 0; i < len; i++) {
            cols.push(new CellVo("@", []));
        }
        if (pos === RowPos.Bottom) {
            dataRows.push(cols);
        }
        else {
            dataRows.unshift(cols);
        }

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }

    insertColsLeft() {
        

        let stepData: DCellOutput = this.state.stepData;

        switch (this.currentPos) {
            case DividePosition.Dividend:

                let dcells: CellVo[] = stepData.dividend;
                if (dcells) {
                    dcells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Divisor:

                let ddcells: CellVo[] = stepData.divisor;
                if (ddcells) {
                    ddcells.splice(this.currentX, 0, new CellVo("@", ['hid']));

                }



                break;

            case DividePosition.Products:
                let pcells: CellVo[] = stepData.products[this.currentX];
                if (pcells) {
                    pcells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }

                break;

            case DividePosition.Quotient:
                let ccells: CellVo[] = stepData.quotient;
                ccells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                break;

            case DividePosition.Remain:
                let acells: CellVo[] = stepData.remain;
                acells.splice(this.currentX, 0, new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
        if (this.currentCell) {
            this.currentCell.focus();
        }
    }
    insertColsRight() {
        let stepData: DCellOutput = this.state.stepData;
       

        switch (this.currentPos) {
            case DividePosition.Dividend:

                let dcells: CellVo[] = stepData.dividend;
                if (dcells) {
                    dcells.pop();
                    dcells.unshift(new CellVo("@", ['hid']));
                }


                break;

            case DividePosition.Divisor:

                let ddcells: CellVo[] = stepData.divisor;
                if (ddcells) {
                    ddcells.pop();
                    ddcells.unshift(new CellVo("@", ['hid']));
                }



                break;

            case DividePosition.Products:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.pop();
                    pcells.unshift(new CellVo("@", ['hid']));
                }

                break;

            case DividePosition.Quotient:
                let ccells: CellVo[] = stepData.quotient;
                ccells.pop();
                ccells.unshift(new CellVo("@", ['hid']));
                break;

            case DividePosition.Remain:
                let acells: CellVo[] = stepData.remain;
                acells.pop();
                acells.unshift(new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
        if (this.currentCell) {
            this.currentCell.focus();
        }
    }
    shiftColsLeft() {
        let stepData: DCellOutput = this.state.stepData;

        switch (this.currentPos) {
            case DividePosition.Dividend:

                let dcells: CellVo[] = stepData.dividend;
                if (dcells) {
                    dcells.shift();
                    dcells.push(new CellVo("@", ['hid']));
                }


                break;

            case DividePosition.Divisor:

                let ddcells: CellVo[] = stepData.divisor;
                if (ddcells) {
                    ddcells.shift();
                    ddcells.push(new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Products:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.shift();
                    pcells.push(new CellVo("@", ['hid']));
                }

                break;

            case DividePosition.Quotient:
                let ccells: CellVo[] = stepData.quotient;
                ccells.shift();
                ccells.push(new CellVo("@", ['hid']));
                break;

            case DividePosition.Remain:
                let acells: CellVo[] = stepData.remain;
                acells.shift();
                acells.push(new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
        if (this.currentCell) {
            this.currentCell.focus();
        }
    }
    shiftColsRight() {
      

        let stepData: DCellOutput = this.state.stepData;

        switch (this.currentPos) {
            case DividePosition.Dividend:

                let dcells: CellVo[] = stepData.dividend;
                if (dcells) {
                    dcells.pop();
                    dcells.unshift(new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Divisor:

                let ddcells: CellVo[] = stepData.divisor;
                if (ddcells) {
                    ddcells.pop();
                    ddcells.unshift(new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Products:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.pop();
                    pcells.unshift(new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Quotient:
                let ccells: CellVo[] = stepData.quotient;
                ccells.pop();
                ccells.unshift(new CellVo("@", ['hid']));
                break;

            case DividePosition.Remain:
                let acells: CellVo[] = stepData.remain;
                acells.pop();
                acells.unshift(new CellVo("@", ['hid']));
                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
        if (this.currentCell) {
            this.currentCell.focus();
        }
    }
    insertToRight() {
        let stepData: DCellOutput = this.state.stepData;

        switch (this.currentPos) {
            case DividePosition.Dividend:

                let dcells: CellVo[] = stepData.dividend;
                if (dcells) {
                    dcells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Divisor:

                let ddcells: CellVo[] = stepData.divisor;
                if (ddcells) {
                    ddcells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Products:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Quotient:
                let ccells: CellVo[] = stepData.quotient;
                ccells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));
                break;

            case DividePosition.Remain:
                let acells: CellVo[] = stepData.remain;
                acells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));
                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
        if (this.currentCell) {
            this.currentCell.focus();
        }
    }
    insertToLeft() {
        let stepData: DCellOutput = this.state.stepData;

        switch (this.currentPos) {
            case DividePosition.Dividend:

                let dcells: CellVo[] = stepData.dividend;
                if (dcells) {
                    dcells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Divisor:

                let ddcells: CellVo[] = stepData.divisor;
                if (ddcells) {
                    ddcells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Products:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }
                break;

            case DividePosition.Quotient:
                let ccells: CellVo[] = stepData.quotient;
                ccells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                break;

            case DividePosition.Remain:
                let acells: CellVo[] = stepData.remain;
                acells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                break;

        }
    }
    deleteCell() {
        let stepData: DCellOutput = this.state.stepData;

        switch (this.currentPos) {
            case DividePosition.Dividend:

                let dcells: CellVo[] = stepData.dividend;
                if (dcells) {
                    dcells.splice(this.currentX, 1);
                }
                break;

            case DividePosition.Divisor:

                let ddcells: CellVo[] = stepData.divisor;
                if (ddcells) {
                    ddcells.splice(this.currentX, 1);
                }
                break;

            case DividePosition.Products:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.splice(this.currentX, 1);
                }
                break;

            case DividePosition.Quotient:
                let ccells: CellVo[] = stepData.quotient;
                ccells.splice(this.currentX, 1);
                break;

            case DividePosition.Remain:
                let acells: CellVo[] = stepData.remain;
                acells.splice(this.currentX, 1);
                break;

        }

        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    render() {
        
        if (this.state === null) {
            return <h3>Invalid</h3>
        }
        if (!this.state.stepData) {
            return <h3>Invalid</h3>
        }
        return (<div id="divEdit" className="scroll2">
            <div className='labelText'>Quotient</div>
            {this.getCols(DividePosition.Quotient, 0, this.state.stepData.quotient)}
            <hr />
            <Row>
                <Col>
                    <div className='labelText'>Divisor</div>
                    {this.getCols(DividePosition.Divisor, 100, this.state.stepData.divisor)}
                </Col>
                <Col>
                    <div className='labelText'>Dividend</div>
                    {this.getCols(DividePosition.Dividend, 200, this.state.stepData.dividend)}
                </Col></Row>
            <hr />
            <Row><Col></Col><Col> <div className='labelText'>Products</div>
                {this.getProductRows()}</Col></Row>

        </div>)
    }
}
export default DivideEdit;