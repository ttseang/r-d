import React, { ChangeEvent, Component } from 'react';
import { ColPos, MTowerPosition, RowPos } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import MainStorage from '../../classes/MainStorage';

import { CellVo } from '../../dataObjs/CellVo';
import { CSSVo } from '../../dataObjs/CSSVo';
import { MCellGridOutputVo } from '../../dataObjs/outputs/MCellGridOutputVo';
import { CssStyleUtil } from '../../util/editUtils/CssStyleUtil';
import { MTowerEditUtil } from '../../util/editUtils/MTowerEditUtil';
interface MyProps { stepData: MCellGridOutputVo, callback: Function }
interface MyState { stepData: MCellGridOutputVo }

class MTowerEdit extends Component<MyProps, MyState>
{

    private cellIndex: number = 0;
    private controller: Controller = Controller.getInstance();
    private currentCell: HTMLInputElement | null = null;
    private currentCellVo: CellVo | null = null;
    private currentX: number = -1;
    private currentY: number = -1;
    private currentPos: MTowerPosition | null = null;

    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData };


    }
    componentDidMount() {
        this.controller.addMColumn = this.addColumn.bind(this);
        this.controller.subMColumn = this.subColumn.bind(this);
        this.controller.addMRow = this.addRow.bind(this);
        this.controller.subMRow = this.subRow.bind(this);
        this.controller.setStyle = this.changeCss.bind(this);
        this.controller.shiftColLeft = this.shiftColsLeft.bind(this);
        this.controller.shiftColRight = this.shiftColsRight.bind(this);
        this.controller.insertColLeft = this.insertLeft.bind(this);
        this.controller.insertColRight = this.insertRight.bind(this);
        this.controller.deleteCell=this.deleteCell.bind(this);
    }
    componentWillUnmount() {
        this.controller.addMColumn = () => { };
        this.controller.subMColumn = () => { };
        this.controller.addMRow = () => { };
        this.controller.subMRow = () => { };
        this.controller.setStyle = () => { };
        this.controller.shiftColLeft = () => { };
        this.controller.shiftColRight = () => { };
        this.controller.insertColLeft = () => { };
        this.controller.insertColRight = () => { };
        this.controller.deleteCell=()=>{};
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData });
        }
    }
    getCell(tp: MTowerPosition, x: number, y: number, cell: CellVo) {
        let key = "editCell_" + x.toString() + "_" + y.toString() + "_" + tp.toString() + this.cellIndex.toString();
       

        this.cellIndex++;
        let id: string = "cell" + this.cellIndex.toString();
        //   let id: string = "editCell_" + x.toString() + "_" + y.toString()+"_"+tp.toString()+ this.cellIndex.toString();
        let char: string = cell.text;

        if (cell.classArray.includes("hid") && char === "0") {
            char = "@";
        }

        let classString: string = "cellEdit";
        if (char === "@") {
            classString = "cellEditFade";
        }

      

        return (<input key={key} id={id} maxLength={2} type='text' className={classString} value={char} onFocus={(e: React.FocusEvent<HTMLInputElement>) => { this.onFocus(tp, x, y, e) }} onChange={(e: ChangeEvent<HTMLInputElement>) => { this.updateGrid(tp, x, y, e) }}></input>)
    }
    onFocus(tp: MTowerPosition, x: number, y: number, e: React.FocusEvent<HTMLInputElement>) {
        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;

        if (this.currentCell) {
            this.currentCell.classList.remove('inputSelected');
        }

        inputBox.select();
        inputBox.classList.add("inputSelected");
        this.currentCell = inputBox;
        this.currentPos = tp;
        this.currentY = y;
        this.currentX = x;

      
        this.currentCellVo = this.getCellVo(tp, x, y);
        if (this.currentCellVo)
        {
            this.controller.setSelectedCss(this.currentCellVo.classArray);
            this.controller.setMTowerCell(tp, x, y, this.currentCellVo);
        }
        

    }
    changeCss(cssVo: CSSVo) {
      

        if (this.currentPos !== null) {
            let stepDataTemp: MCellGridOutputVo = this.state.stepData;
            let cell: CellVo = this.getCellVo(this.currentPos, this.currentX, this.currentY);
          

            if (cell) {
                //cell.addCss(cssVo);
                CssStyleUtil.addCssToCell(cssVo,cell);
            }
            this.props.callback(stepDataTemp);
            this.setState({ stepData: stepDataTemp });
            this.controller.setSelectedCss(cell.classArray);
        }
    }
    private getCellVo(tp: MTowerPosition, x: number, y: number) {
       

        let stepDataTemp: MCellGridOutputVo = this.state.stepData;
        let cell: CellVo | null = null;
        switch (tp) {
            case MTowerPosition.Body:
                //  stepDataTemp.rows[x][y].text=char;
                cell = stepDataTemp.rows[y][x];
                break;

            case MTowerPosition.Top:
                cell = stepDataTemp.top[y][x];
                break;

            case MTowerPosition.Carry:
                cell = stepDataTemp.carryRow[x];

                break;

            case MTowerPosition.Product:
                cell = stepDataTemp.products[x][y];
                break;
            case MTowerPosition.Answer:
                cell = stepDataTemp.answer[x];
        }

        return cell;
    }
    updateGrid(tp: MTowerPosition, x: number, y: number, e: ChangeEvent<HTMLInputElement>) {
      
        let char: string = e.currentTarget.value;
        let stepDataTemp: MCellGridOutputVo = this.state.stepData;

        let cell: CellVo = this.getCellVo(tp, x, y);

        if (cell) {

            cell.setEditText(char);
            if (char !== "@") {
                cell.removeClass("hid");
            }
        }

        this.props.callback(stepDataTemp);

        this.setState({ stepData: stepDataTemp });
        if (this.ms.jumpToNext === true) {


            let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;
            let cellIndex = parseInt(inputBox.id.replace("cell", ""));
            if (!isNaN(cellIndex)) {
                cellIndex++;
                let nBox: HTMLInputElement = document.getElementById("cell" + cellIndex.toString()) as HTMLInputElement;
                if (nBox) {
                    nBox.focus();
                }
            }
        }
    }
    getCols(tp: MTowerPosition, row: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];
        for (let i: number = 0; i < cells.length; i++) {
            let id: string = row.toString() + "_" + i.toString();
            let cell: JSX.Element = this.getCell(tp, i, row, cells[i]);

            cols.push(<span key={id} id={id}>{cell}</span>);
        }
        let rowKey: string = "editRow" + row.toString();

        return (<div key={rowKey}>{cols}</div>);
    }
    getTowerRows() {
        let dataRows: CellVo[][] = this.state.stepData.rows;
        let rows: JSX.Element[] = [];

        for (let i: number = 0; i < dataRows.length; i++) {
            rows.push(this.getCols(MTowerPosition.Body, i, dataRows[i]));
        }
        return rows;
    }
    getProductRows() {
        let dataRows: CellVo[][] = this.state.stepData.products;
        let rows: JSX.Element[] = [];

        for (let i: number = 0; i < dataRows.length; i++) {
            rows.push(this.getCols(MTowerPosition.Product, i, dataRows[i]));
        }
        return rows;
    }

    getTop() {
        let tops: CellVo[][] = this.state.stepData.top;
        let topCells: JSX.Element[] = [];

        for (let i: number = tops.length - 1; i > -1; i--) {
            topCells.push(this.getCols(MTowerPosition.Top, i, tops[i]));
        }
        return topCells;
    }
    subRow(part: MTowerPosition, pos: RowPos) {
        let tempCells: MCellGridOutputVo = this.state.stepData;

        if (this.ms.applyToAllSteps === true) {
            MTowerEditUtil.subRowsFromAllSteps(part, pos);
            tempCells = this.ms.mTowerGrid[this.ms.step];
        }
        else {
            tempCells = MTowerEditUtil.subRow(part, pos, tempCells);
        }
        /*  let dataRows: CellVo[][] = tempCells.rows;
         if (part===MTowerPosition.Product)
         {
             dataRows=tempCells.products;
         }
         if (pos === RowPos.Bottom) {
             dataRows.pop();
         }
         else {
             dataRows.shift();
         } */
        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }
    addRow(part: MTowerPosition, pos: RowPos) {
        let tempCells: MCellGridOutputVo = this.state.stepData;

        if (this.ms.applyToAllSteps === true) {
            MTowerEditUtil.addRowsToAllSteps(part, pos);
            tempCells = this.ms.mTowerGrid[this.ms.step];
        }
        else {
            tempCells = MTowerEditUtil.addRow(part, pos, tempCells);
        }

        /*   let dataRows: CellVo[][] = tempCells.rows;
          if (part===MTowerPosition.Product)
          {
              dataRows=tempCells.products;
          }
          let len: number = 4;
          if (tempCells.rows.length > 0) {
              len = tempCells.rows[0].length;
          }
          let cols: CellVo[] = [];
          for (let i: number = 0; i < len; i++) {
              cols.push(new CellVo("@", []));
          }
          if (pos === RowPos.Bottom) {
              dataRows.push(cols);
          }
          else {
              dataRows.unshift(cols);
          } */

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }
    addColumn(part: MTowerPosition, side: ColPos) {
        let tempCells: MCellGridOutputVo = this.state.stepData;
        if (this.ms.applyToAllSteps === true) {
            MTowerEditUtil.addColsToAllSteps(part, side);
            tempCells = this.ms.mTowerGrid[this.ms.step];
        }
        else {
            tempCells = MTowerEditUtil.addColumn(part, side, tempCells);
        }

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }
    subColumn(part: MTowerPosition, side: ColPos) {
        let tempCells: MCellGridOutputVo = this.state.stepData;
        if (this.ms.applyToAllSteps === true) {
            MTowerEditUtil.subColsToAllSteps(part, side);
            tempCells = this.ms.mTowerGrid[this.ms.step];
        }
        else {
            tempCells = MTowerEditUtil.subColumn(part, side, tempCells);
        }

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }

    shiftColsLeft() {
        let stepData: MCellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case MTowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentX];
                if (tcells) {
                    tcells.shift();
                    tcells.push(new CellVo("@", ['hid']));
                }


                break;

            case MTowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    cells.shift();
                    cells.push(new CellVo("@", ['hid']));
                }



                break;

            case MTowerPosition.Product:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.shift();
                    pcells.push(new CellVo("@", ['hid']));
                }

                break;

            case MTowerPosition.Carry:
                let ccells: CellVo[] = stepData.carryRow;
                ccells.shift();
                ccells.push(new CellVo("@", ['hid']));
                break;

            case MTowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.shift();
                acells.push(new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
        if (this.currentCell) {
            this.currentCell.focus();
        }
    }
    shiftColsRight() {
        let stepData: MCellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case MTowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentY];
                if (tcells) {
                    tcells.pop();
                    tcells.unshift(new CellVo("@", ['hid']));
                }


                break;

            case MTowerPosition.Body:
                

                let cells: CellVo[] = stepData.rows[this.currentY];
                
                if (cells) {
                    cells.pop();
                    cells.unshift(new CellVo("@", ['hid']));
                }

                break;

            case MTowerPosition.Carry:
                let ccells: CellVo[] = stepData.carryRow;
                ccells.pop();
                ccells.unshift(new CellVo("@", ['hid']));
                break;

            case MTowerPosition.Product:
                let pcells: CellVo[] = stepData.products[this.currentY];
                if (pcells) {
                    pcells.pop();
                    pcells.unshift(new CellVo("@", ['hid']));
                }

                break;


            case MTowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.pop();
                acells.unshift(new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
        if (this.currentCell) {
            this.currentCell.focus();
        }
    }
    insertLeft() {
       
        let stepData: MCellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case MTowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentY];
                if (tcells) {
                    tcells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }

                // tcells.pop();
                //tcells.unshift(new CellVo("@",['hid']));

                break;

            case MTowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {

                    cells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }


                break;

            case MTowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                if (acells)
                {
                    acells.splice(this.currentX, 0, new CellVo("@", ['hid']));
                }
               
                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    insertRight() {

       

        let stepData: MCellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case MTowerPosition.Top:

                let tcells: CellVo[] = stepData.top[this.currentY];
                tcells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));

                break;

            case MTowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    cells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']))

                }


                break;

            case MTowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.splice(this.currentX + 1, 0, new CellVo("@", ['hid']));

                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    deleteCell() {
        let stepData: MCellGridOutputVo = this.state.stepData;

        switch (this.currentPos) {
            case MTowerPosition.Top:
                let tcells: CellVo[] = stepData.top[this.currentY];
                tcells.splice(this.currentX, 1);
                break;

            case MTowerPosition.Body:

                let cells: CellVo[] = stepData.rows[this.currentY];
                if (cells) {
                    cells.splice(this.currentX, 1);
                }
                break;

            case MTowerPosition.Answer:
                let acells: CellVo[] = stepData.answer;
                acells.splice(this.currentX, 1);
                break;

        }
        this.setState({ stepData: stepData });
        this.props.callback(stepData);
    }
    render() {
        //this.cellIndex = 0;

        return (<div className="scroll2">
            <div id="towerEdit">
                <div className='labelText'>Top</div>
                {this.getTop()}
                <hr />
                <div className='labelText'>Body</div>
                {this.getTowerRows()}
                <hr />
                <div className='labelText'>Carry</div>
                {this.getCols(MTowerPosition.Carry, 300, this.state.stepData.carryRow)}
                <div className='labelText'>Product</div>
                {this.getProductRows()}
                <hr />
                <div className='labelText'>Answer</div>
                {this.getCols(MTowerPosition.Answer, 100, this.state.stepData.answer)}
            </div>

        </div>)
    }
}
export default MTowerEdit;