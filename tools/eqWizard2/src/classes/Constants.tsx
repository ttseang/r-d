export enum ProblemTypes
{
    UNSUPORTED,
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION,
    FRACTIONS
}
export const Unicodes: {[type: string]: string} = {
    "+": "\u002B",
    "-": "\u2212",
    "x": "\u00D7",
    "*": "\u2022",
    "<": "\u003C",
    ">": "\u003E",
    "=": "\u003D"
};
export enum ColPos {
    Left,
    Right
}
export enum RowPos {
    Top,
    Bottom
}
export enum TowerPosition {
    Top,
    Body,
    Answer
}
export enum MTowerPosition
{
    Top,
    Body,
    Carry,
    Product,
    Answer
}
export enum FractPosition
{
    Whole,
    Top,
    Bottom
}
export enum DividePosition
{
    Quotient,
    Divisor,
    Dividend,
    Products,
    Remain
}
export enum CSSTypes {
    Color,
    Size,
    Decor,
    Header
}
export enum SidePanels
{
    None,
    Cols,
    Rows,
    CSS,
    Tools,
    CellTools

}