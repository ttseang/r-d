import { CellVo } from "../dataObjs/CellVo";
import { CSSVo } from "../dataObjs/CSSVo";
import { TowerPosition, ColPos, RowPos, SidePanels, MTowerPosition, DividePosition } from "./Constants";

export class Controller
{
    private static instance:Controller | null=null;

    public static getInstance()
    {
        if(this.instance===null)
        {
            this.instance=new Controller();
        }
        return this.instance;
    }
    public saveData:Function=()=>{};

    public newProblem:Function=()=>{};
    public doCalc:Function=()=>{};
    
    public addColumn:Function=(part: TowerPosition, side: ColPos)=>{};
    public subColumn:Function=(part: TowerPosition, side: ColPos)=>{};
    
    public addRow:Function=(part:TowerPosition,pos:RowPos)=>{};
    public subRow:Function=(part:TowerPosition,pos:RowPos)=>{};

    
    public addMColumn:Function=(part: MTowerPosition, side: ColPos)=>{};
    public subMColumn:Function=(part: MTowerPosition, side: ColPos)=>{};

    public addMRow:Function=(part:MTowerPosition,pos:RowPos)=>{};
    public subMRow:Function=(part:MTowerPosition,pos:RowPos)=>{};

    public addDColumn:Function=(part: DividePosition, side: ColPos)=>{};
    public subDColumn:Function=(part: DividePosition, side: ColPos)=>{};

    public addDRow:Function=(part:DividePosition,pos:RowPos)=>{};
    public subDRow:Function=(part:DividePosition,pos:RowPos)=>{};

    public setSidePanel:Function=(panel:SidePanels)=>{};
    public setStyle:Function=(cssVo:CSSVo)=>{};
    public setSelectedCss:Function=(classList:string[])=>{};
    public setEditMode:Function=()=>{};
    public openFile:Function=()=>{};
    public showHelp:Function=()=>{};

    public setSaveScreen:Function=()=>{};
    public showHide:Function=(show:boolean)=>{};

    public setTowerCell:Function=(tp:TowerPosition,x:number,y:number,cell:CellVo)=>{};
    public setMTowerCell:Function=(tp:MTowerPosition,x:number,y:number,cell:CellVo)=>{};
    public setDTowerCell:Function=(tp:DividePosition,x:number,y:number,cell:CellVo)=>{};
    
    public shiftColRight:Function=()=>{};
    public shiftColLeft:Function=()=>{};

    public insertColRight:Function=()=>{};
    public insertColLeft:Function=()=>{};

    public deleteCell:Function=()=>{};
}