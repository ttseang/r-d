import { CSSTypes } from "../classes/Constants";

export class CSSVo
{
    public type:CSSTypes;
    public name:string;
    public value:string;
    public showAnimOnList:boolean=true;

    constructor(type:CSSTypes,name:string,value:string)
    {
        this.type=type;
        this.name=name;
        this.value=value;
    }
}