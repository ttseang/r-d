import { NumUtil } from "../util/NumUtil";


export class NumVo {
    public num: string;
    public original:string;
    public val: number = 0;

    public whole: number = 0;
    public wholeString: string = "";
    public wholeLen: number = 0;

    public row: number = 0;

    public editMode: boolean = false;

    public isWholeNumber: boolean = true;

    public dec: number = 0;
    public decString: string = "";
    public decLen: number = 0;
    public decPlaces: number = 0;

    public all: string = "";
    public classes: string[] = [];

    public digits: number[] = [];
    public decs: number[] = [];
    public mdigits: number[] = [];
    public allDigits: number[] = [];

    public allChars: string[] = [];

    public callback: Function = () => { };
    public isCurrency:boolean=false;

    private nString: string = "";

    constructor(num: string, editMode: boolean = false) {
        this.original=num;
        this.num = NumUtil.takeOutStrings(num);
        this.editMode = editMode;
        this.setUp();
    }
    setUp() {

        if (this.original.startsWith("$"))
        {
            this.isCurrency=true;
        }

        this.allDigits = [];
        this.allChars = [];
        this.digits = [];



        this.val = parseFloat(this.num);

        if (!this.num.includes(".")) {
            this.isWholeNumber = true;
        }
        else {
            this.isWholeNumber = false;
        }
        let decimalArray: string[] = this.num.split(".");
       

        if (decimalArray.length<2)
        {
            this.decPlaces=0;
        }
        else
        {
            this.decPlaces = decimalArray[1].length;
        }
       

        this.decString = decimalArray[1] || "";
        this.whole = parseInt(decimalArray[0]);
        this.dec = parseFloat("." + decimalArray[1]);

        if (isNaN(this.whole)) {
            this.whole = 0;
        }
        if (isNaN(this.dec)) {
            this.dec = 0;
        }

        this.wholeString = this.whole.toString();
        this.wholeLen = this.wholeString.length;

        this.allChars = this.num.split("");
        this.decLen = this.decString.length;
        this.all = this.wholeString + "." + this.decString;


        let dArray: string[] = this.whole.toString().split("");
        for (let i: number = 0; i < dArray.length; i++) {
            if (!isNaN(parseInt(dArray[i])))
            {
            this.digits.push(parseInt(dArray[i]));
            this.allDigits.push(parseInt(dArray[i]));
            }
        }

        if (this.decString === "NaN") {
            this.decString = "";
        }

        if (this.decString.length !== 0) {
            let decArray: string[] = this.decString.split("");
            for (let i: number = 0; i < decArray.length; i++) {

                let decVal: number = parseInt(decArray[i]);

                if (!isNaN(decVal)) {
                    this.decs.push(decVal);
                    this.allDigits.push(decVal);
                }
            }
        }

        let digits2: number[] = this.digits.slice().reverse();


        for (let i: number = 0; i < digits2.length; i++) {
            let mval: number = digits2[i];

            mval = mval * (Math.pow(10, i));

            this.mdigits.push(mval);
        }
       
    }
    addDecPlace() {
      //  console.log("whole?="+this.isWholeNumber);
        if (this.isWholeNumber === true) {
            this.num += ".0";
        }
        else {
            this.num += "0";
        }
        this.setUp();
    }
    getZeroSpaces()
    {
        let zCount:number=0;
        let wholeArray:string[]=this.wholeString.split("");
        wholeArray=wholeArray.reverse();

        let nonZeroFound:boolean=false;

        for (let i:number=0;i<wholeArray.length;i++)
        {
            if (wholeArray[i]!=="0")
            {
                nonZeroFound=true;
            }
            if (wholeArray[i]==="0" && nonZeroFound===false)
            {
                zCount++;
            }
            
        }
        return zCount;
    }
    formatDigits(maxWhole: number, maxDec: number) {
        let wholeDif: number = maxWhole - this.wholeLen;
        let decDif: number = maxDec - this.decLen;

       /*  console.log("max="+maxDec);
        console.log("len="+this.decLen);
        console.log("decDif="+decDif); */

        let leadingZeros: string = "";
        let trailingZeros: string = "";

        if (wholeDif > 0) {
            leadingZeros = "0".repeat(wholeDif);
        }
        if (decDif > 0) {
            trailingZeros = "0".repeat(decDif);
        }

        let all: string = leadingZeros + this.num + trailingZeros;
        console.log("all="+all);
        //this.allChars=all.split("");

        all = all.replace(".", "");

        let allArray: string[] = all.split("");
        

        this.allDigits = [];
        for (let i: number = 0; i < allArray.length; i++) {
            let char: string = allArray[i];
            this.allDigits.push(parseInt(char));
        }
    }
    formatGridDigits(maxWhole: number, maxDec: number,hideDec:boolean=true) {
        let wholeDif: number = maxWhole - this.wholeLen;
        let decDif: number = maxDec - this.decLen;

       

        let leadingZeros: string = "";
        let trailingZeros: string = "";

        if (this.whole===0)
        {
            wholeDif=maxWhole;
        }

        if (wholeDif > 0) {
            leadingZeros = "@".repeat(wholeDif);
        }
        if (decDif > 0) {
            trailingZeros = "0".repeat(decDif);
        }
        
       
       
        

        let leadingBlanks: string = "";
        let trailingBlanks: string = "";

        

        let all: string =leadingBlanks+ leadingZeros + this.num + trailingZeros+trailingBlanks;
        if (hideDec===true)
        {
           // all = all.replace(".", "");
        }
      

        let allArray: string[] = all.split("");
       

        this.allDigits = [];
        for (let i: number = 0; i < allArray.length; i++) {
            let char: string = allArray[i];
            this.allDigits.push(parseInt(char));
        }
        return all;
    }
    fixDigits(maxWhole: number, maxDec: number) {

        //fix whole
        let wholeDigits: number[] = this.digits.slice();
        while (wholeDigits.length < maxWhole) {
            wholeDigits.unshift(0);
        }

        console.log("max="+maxDec);
        console.log("len="+this.decLen);
        console.log(this.decLen);

        //fix dec
        let decDigits: number[] = this.decs.slice();
        if (decDigits.length<maxDec && this.decLen===0)
        {
            this.allChars.push(".");
        }

       
        
        //console.log("decDif="+decDif);

        while (decDigits.length < maxDec) {
            decDigits.push(0);
            //this.allDigits.push(0);
            this.allChars.push("0");
        }
       
    }
    format(maxWhole: number, maxDec: number, hideTrail: boolean = true, trimDec: boolean = false) {

        if (this.whole === 0 && trimDec === true) {
            let trimmedDec: string = this.getTrimmedDec();
            this.allChars=trimmedDec.split("");
          
            return trimmedDec;
        }

       

        let wholeDif: number = maxWhole - this.wholeLen;
        let decDif: number = maxDec - this.decLen;

        let leadingZeros: string = "";
        let trailingZeros: string = "";

     

        if (wholeDif > 0) {
            leadingZeros = "@".repeat(wholeDif);
        }
        if (decDif > 0) {
            if (hideTrail === true) {
                trailingZeros = "@".repeat(decDif);
                // trailingZeros = "<span class='hid'>0</span>".repeat(decDif);
            }
            else {
                trailingZeros = "0".repeat(decDif);
            }
        }
        let fString: string = leadingZeros + this.wholeString+"."+this.decString + trailingZeros;

        this.allChars = fString.split("");
      
        fString = fString.replaceAll("@", "<span class='hid'>0</span>");

        return fString;
    }
    public getTrimmedDec() {
        this.nString = this.num;
        this.hideLeadingDec();
        
        return this.nString;
    }
    private hideLeadingDec() {
        for (let i: number = 10; i > 0; i--) {
            let searchString: string = "." + "0".repeat(i);
            this.nString = this.nString.replace(searchString, "@".repeat(i));
        }
    }
    private doClick() {
        alert("click");
    }
    private getClassFormat() {
        let chars: string = "";

        let classes: string[] = this.classes.slice().reverse();

        
        for (let i: number = this.allChars.length - 1; i > -1; i--) {
            let char: string = this.allChars[i];
            let i2: number = this.allChars.length - i - 1;
            let id: string = this.row.toString() + "_" + i2.toString();
           
            

            if (this.editMode === true) {
                if (char === ".") {
                    char = "<button id='btn" + id + "'><span id='" + id + "' class=' decimal'>.</span></button>";
                    classes.shift();
                }
                else {
                   
                    let className: string | undefined = classes[i];

                   
                    if (char !== "@") {
                        if (className) {
                            char = "<button id='btn" + id + "'><span  id='" + id + "' class='" + className + "'>" + char + "</span></button>";
                        }
                        else {
                            char = "<button id='btn" + id + "'><span id='" + id + "'>" + char + "</span></button>";
                        }
                    }
                    else {
                        char = "<span id='" + id + "'>" + char + "</span>";
                    }
                }
            }
            else {
                if (char === ".") {
                    char = "<span  id='" + id + "' class=' decimal'>.</span>";
                    classes.shift();
                }
                else {
                    //let className: string | undefined = classes.shift();
                    let className: string | undefined = classes[i];


                    if (className) {
                        char = "<span  id='" + id + "' class='" + className + "'>" + char + "</span>";
                    }
                    else {
                        char = "<span id='" + id + "'>" + char + "</span>";
                    }
                }
            }

            chars = char + chars;
        }
        return chars;
    }
}