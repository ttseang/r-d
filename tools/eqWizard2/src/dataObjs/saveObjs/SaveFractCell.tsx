import { SaveCellVo } from "./SaveCellVo";

export class SaveFractCell
{
    public top: SaveCellVo[];
    public bottom:SaveCellVo[];
    public whole:SaveCellVo[];

    constructor(top:SaveCellVo[]=[],bottom:SaveCellVo[]=[],whole:SaveCellVo[]=[])
    {
        this.top=top;
        this.bottom=bottom;
        this.whole=whole;
    }
}