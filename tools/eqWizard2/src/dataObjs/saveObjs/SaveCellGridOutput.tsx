import { SaveCellVo } from "./SaveCellVo";

export class SaveCellGridOutput
{
    public top: SaveCellVo[][];
    public rows: SaveCellVo[][];
    public operator: string;
    public answer: SaveCellVo[];

    constructor(top: SaveCellVo[][], rows: SaveCellVo[][], answer: SaveCellVo[], operator: string) {
        this.top=top;
        this.rows=rows;
        this.answer=answer;
        this.operator=operator;
    }
}