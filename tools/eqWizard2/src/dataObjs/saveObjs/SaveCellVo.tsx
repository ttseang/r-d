export class SaveCellVo {
    public classArray: string[];
    private text: string;

    constructor(text: string, classArray: string[]) {
        this.text = text;
        this.classArray = classArray;
    }
}