import { SaveCellVo } from "./SaveCellVo";

export class SaveMCellGridOutput
{
    public top: SaveCellVo[][];
    public rows: SaveCellVo[][];
    public products: SaveCellVo[][];
    public carryRows: SaveCellVo[];
    public operator: string;
    public answer: SaveCellVo[];

    constructor(top: SaveCellVo[][], rows: SaveCellVo[][],products:SaveCellVo[][],carryRows:SaveCellVo[], answer: SaveCellVo[], operator: string) {
        this.top=top;
        this.rows=rows;
        this.products=products;
        this.carryRows=carryRows;
        this.answer=answer;
        this.operator=operator;
    }
}