import { GridUtil } from "../../util/GridUtil";
import { CellVo } from "../CellVo";
import { SaveDCellGridOutput } from "../saveObjs/SaveDCellGridOutput";

export class DCellOutput {
    public quotient: CellVo[];
    public divisor: CellVo[];
    public dividend: CellVo[];
    public products: CellVo[][];
    public remain: CellVo[];

    constructor(quotient: CellVo[], divisor: CellVo[], dividend: CellVo[], products: CellVo[][], remain: CellVo[]) {
        this.quotient = quotient;
        this.divisor = divisor;
        this.dividend = dividend;
        this.products = products;
        this.remain = remain;
    }
    fromObj(data:any)
    {
        this.quotient=GridUtil.dataToCells(data.quotient);
        this.divisor=GridUtil.dataToCells(data.divisor);
        this.dividend=GridUtil.dataToCells(data.dividend);
        this.products=GridUtil.dataToGrid(data.products);
        this.remain=GridUtil.dataToCells(data.remain);
    }
    getOutput()
    {
       let saveGrid:SaveDCellGridOutput=new SaveDCellGridOutput(GridUtil.cellsToSave(this.quotient),GridUtil.cellsToSave(this.divisor),GridUtil.cellsToSave(this.dividend),GridUtil.mcellsToSave(this.products),GridUtil.cellsToSave(this.remain));
       return saveGrid;
    }
    clone() {
        let quotient2: CellVo[] = [];
        let divisor2: CellVo[] = [];
        let dividend2: CellVo[] = [];
        let products2: CellVo[][] = [];
        let remain2: CellVo[] = [];

        for (let i:number=0;i<this.quotient.length;i++)
        {
            quotient2.push(this.quotient[i].clone());
        }
        for (let i:number=0;i<this.divisor.length;i++)
        {
            divisor2.push(this.divisor[i].clone());
        }
        for (let i:number=0;i<this.dividend.length;i++)
        {
          dividend2.push(this.dividend[i].clone());
        }
        for (let i:number=0;i<this.remain.length;i++)
        {
            remain2.push(this.remain[i].clone());
        }
        for (let i:number=0;i<this.products.length;i++)
        {
            products2[i]=[];
            for (let j:number=0;j<this.products[i].length;j++)
            {
                products2[i].push(this.products[i][j].clone());
            }           
        }
        return new DCellOutput(quotient2,divisor2,dividend2,products2,remain2);
    }
} 