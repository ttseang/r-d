import { GridUtil } from "../../util/GridUtil";
import { CellVo } from "../CellVo";
import { SaveMCellGridOutput } from "../saveObjs/SaveMCellGridOutput";

export class MCellGridOutputVo {
    public top: CellVo[][];
    public rows: CellVo[][];
    public operator: string;
    public products: CellVo[][];
    public carryRow: CellVo[];
    public answer: CellVo[];

    constructor(top: CellVo[][], rows: CellVo[][], products: CellVo[][], carryRow: CellVo[], answer: CellVo[], operator: string) {
        this.top = top;
        this.rows = rows;
        this.products = products;
        this.carryRow = carryRow;
        this.operator = operator;
        this.answer = answer;
    }
    fromObj(data: any) {
        this.top = GridUtil.dataToGrid(data.top);
        this.rows = GridUtil.dataToGrid(data.rows);
        this.products = GridUtil.dataToGrid(data.products);
        this.carryRow = GridUtil.dataToCells(data.carryRow);
        this.answer = GridUtil.dataToCells(data.answer);
    }
    getMaxCols() {
        let maxCols: number = 0;
        
        this.top.forEach((cell: CellVo[]) => {
            maxCols=Math.max(cell.length,maxCols);           
        });
        this.rows.forEach((cell: CellVo[]) => {
            maxCols=Math.max(cell.length,maxCols);
        });
        this.products.forEach((cell: CellVo[]) => {
            maxCols=Math.max(cell.length,maxCols);
        });
        maxCols=Math.max(this.answer.length,maxCols);
        
        return maxCols;
    }

    getOutput() {
        return new SaveMCellGridOutput(GridUtil.mcellsToSave(this.top), GridUtil.mcellsToSave(this.rows), GridUtil.mcellsToSave(this.products), GridUtil.cellsToSave(this.carryRow), GridUtil.cellsToSave(this.answer), this.operator);
        //return {"top":this.top,"rows":this.rows,"products":this.products,"carryRow":this.carryRow,"answer":this.answer,"operator":this.operator};
    }
    clone() {

        let top2: CellVo[][] = [];

        for (let i: number = 0; i < this.top.length; i++) {
            top2[i] = [];
            for (let k: number = 0; k < this.top[i].length; k++) {
                top2[i].push(this.top[i][k].clone());
            }

        }


        let rows2: CellVo[][] = [];

        for (let j: number = 0; j < this.rows.length; j++) {
            let row: CellVo[] = this.rows[j];
            let row2: CellVo[] = [];

            for (let k: number = 0; k < row.length; k++) {
                row2.push(row[k].clone());
            }
            rows2.push(row2);
        }

        let products2: CellVo[][] = [];

        for (let i: number = 0; i < this.products.length; i++) {
            products2[i] = [];
            for (let k: number = 0; k < this.products[i].length; k++) {
                products2[i].push(this.products[i][k].clone());
            }

        }

        let carry2: CellVo[] = [];

        for (let m: number = 0; m < this.carryRow.length; m++) {
            carry2.push(this.carryRow[m].clone());
        }


        let answer2: CellVo[] = [];
        for (let m: number = 0; m < this.answer.length; m++) {
            answer2.push(this.answer[m].clone());
        }

        let cellGrid: MCellGridOutputVo = new MCellGridOutputVo(top2, rows2, products2, carry2, answer2, this.operator);
        return cellGrid;
    }
}