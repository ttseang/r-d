export class ProbVo {
    public lesson: number;
    public reference: number;
    public problemID: string;
    public problem: string;
    public instructions:string;

    constructor(lesson: number, reference: number, problemID: string,instructions:string, problem: string) {
        this.lesson = lesson;
        this.reference = reference;
        this.problemID = problemID;
        this.instructions=instructions;
        this.problem = problem;
    }
}