export class PasteVo
{
    public display:string;
    public paste:string;

    constructor(display:string,paste:string="")
    {
        this.display=display;
        this.paste=paste;
        if (paste==="")
        {
            this.paste=display;
        }
    }
}