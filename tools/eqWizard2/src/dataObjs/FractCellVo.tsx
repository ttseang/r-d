import { CellVo } from "./CellVo";
import {GridUtil} from "../util/GridUtil";

export class FractCellVo
{
    public top: CellVo[];
    public bottom:CellVo[];
    public whole:CellVo[];

    public t:number=0;
    public w:number=0;
    public b:number=0;

    constructor(top:CellVo[]=[],bottom:CellVo[]=[],whole:CellVo[]=[])
    {
        this.top=top;
        this.bottom=bottom;
        this.whole=whole;
    }
    clone()
    {
        let top2: CellVo[]=[];
        let bottom2:CellVo[]=[];
        let whole2:CellVo[]=[];


        for (let i:number=0;i<this.top.length;i++)
        {
            top2.push(this.top[i].clone());
        }
        for (let i:number=0;i<this.bottom.length;i++)
        {
            bottom2.push(this.bottom[i].clone());
        }
        for (let i:number=0;i<this.whole.length;i++)
        {
            whole2.push(this.whole[i].clone());
        }
        return new FractCellVo(top2,bottom2,whole2);
    }
    fromNum(t:number,b:number,w:number)
    {
        this.top=GridUtil.arrayToCells2(t.toString().split(""),[]);
        this.bottom=GridUtil.arrayToCells2(b.toString().split(""),[]);
        this.whole=GridUtil.arrayToCells2(w.toString().split(""),[]);
        

        this.t=t;
        this.b=b;
        this.w=w;
    }
    getTopLen()
    {
        if (this.w>0)
        {
            return this.whole.length+1;
        }
        return 1;
    }
    getLen():number
    {
        if (this.w===0)
        {
            return 1;
        }
        return 2;
    }
    getBottomLen()
    {
        return this.bottom.length;
    }
    getTop()
    {
        let html:JSX.Element[]=[];
        for (let i:number=0;i<this.top.length;i++)
        {
            this.top[i].addClass("numerator");
            html.push(this.top[i].toHtml(i));
        }
        return html;
    }
    getBottom()
    {
        let html:JSX.Element[]=[];
        for (let i:number=0;i<this.bottom.length;i++)
        {
            this.bottom[i].addClass("denominator");
            html.push(this.bottom[i].toHtml(i));
        }
        return html;
    }
    getWhole()
    {
        if (this.w===0)
        {
            return "";
        }
        let html:JSX.Element[]=[];
        for (let i:number=0;i<this.whole.length;i++)
        {
            this.whole[i].addClass("whole");
            html.push(this.whole[i].toHtml(i+100));
        }
        return html;
    }
    
}