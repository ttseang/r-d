import { Controller } from "../../classes/Controller";
import MainStorage from "../../classes/MainStorage";
import { Payload } from "../../dataObjs/outputs/Payload";
import ApiConnect from "../ApiConnect";


export class EqSaver
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();
    private mc:Controller=Controller.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    public save()
    {

        if (this.ms.LtiFileName === "") {
           // this.mc.changeScreen(9);
            return;
        }
        let data: any = this.ms.saveObj;

        let payload:Payload=new Payload(this.ms.LtiFileName,this.ms.problemType.toString(),this.ms.expression,data);

        
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.Save(JSON.stringify(payload), this.dataSent.bind(this));
    }
    private dataSent(response: any) {
        console.log("send message");
     //   this.mc.setAlert(new MsgVo("Book Saved","success",2000));
        //console.log(response);
        this.callback(response);
    }
}