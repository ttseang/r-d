export class NumUtil
{
    private calcAnswer(answerString: string) {
        let answerArray: string[] = answerString.split("=");
        let q: string = answerArray[0];
        let answer: number = 0;
        if (q) {

            q = this.convertToSqrt(q);


            if (q.includes("sqrt()")) {
                return "invalid";
            }

            // eslint-disable-next-line no-eval
            answer = eval(q);
            answer = this.trimDec(answer.toString());

        }
        return answer.toString();
    }
    public convertToSqrt(q: string) {
        //let sqArray:string[]=q.split(\[(.*?)\]);

        let sqArray: string[] = q.split("sqrt(");
       

        for (let i: number = 0; i < sqArray.length; i++) {
            let s: string = sqArray[i];
            let s2: string[] = s.split(")");

            let num2: string = s2[0];
            if (num2 !== "") {
                if (!isNaN(parseFloat(num2))) {
                    let ss: string = "sqrt(" + num2 + ")";
                    let num3: number = Math.sqrt(parseFloat(num2));
                    q = q.replace(ss, num3.toString());
                }
                else {
                    return "invalid";
                }
            }
        }
        return q;
    }
    private trimDec(num: string) {
        let num2: number = parseFloat(num);
        let dec: number = num2 - Math.floor(num2);
        if (dec.toString().length > 5) {
            return Math.floor(num2 * 10000) / 10000;
        }
        return num2;


    }
    private decToFraction(num: number) {

        let len: number = num.toString().length - 2;

        let denominator: number = Math.pow(10, len);
        let numerator = num * denominator;

        let divisor = this.calcGcd(numerator, denominator);

       

        numerator = numerator / divisor;
        denominator = denominator / divisor;

        numerator = Math.floor(numerator);
        denominator = Math.floor(denominator);


        return numerator.toString() + "/" + denominator.toString();
    }
    private calcGcd(a: number, b: number): number {
        if (b === 0) return a;
        //a = parseInt(a);
        // b = parseInt(b);
        return this.calcGcd(b, a % b);
    }
    /**
     * Take out all non-numbers
     * Yes, I know there is probably a regex to fix this
     * but I had trouble getting it working, so went old school
     * @param num 
     */
    public static takeOutStrings(num:string)
    {
        let accept:string[]=["0","1","2","3","4","5","6","7","8","9","."];
        let sstring:string="";
        for (let i:number=0;i<num.length;i++)
        {
            let d:string=num.substring(i,i+1);
            if (accept.includes(d))
            {
                sstring+=d;
            }
        }
        return sstring;
    }
}