import { CellVo } from "../dataObjs/CellVo";
import { FractCellVo } from "../dataObjs/FractCellVo";
import { SaveCellVo } from "../dataObjs/saveObjs/SaveCellVo";
import { SaveFractCell } from "../dataObjs/saveObjs/SaveFractCell";

export class GridUtil {
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {

    }
    public static arrayToCells(nums: number[], classArray: string[]) {
        let cells: CellVo[] = [];

        for (let i: number = 0; i < nums.length; i++) {
            let className: string = classArray[i] || "";
            let char: string = nums[i].toString();
            if (nums[i] === -1) {
                char = "@";
            }
            let cell: CellVo = new CellVo(char, [className]);
            if (char.length > 1) {
                cell.addClass("narrowText");
            }
            cells.push(cell);

        }
        return cells;
    }
    public static arrayToCells2(chars: string[], classArray: string[]) {
        let cells: CellVo[] = [];

        for (let i: number = 0; i < chars.length; i++) {
            let className: string = classArray[i] || "";

            cells.push(new CellVo(chars[i], [className]));

        }
        return cells;
    }
    public static compCells(cells1: CellVo[], cells2: CellVo[]) {
        if (cells1 === cells2) {
            return true;
        }
        if (cells1.length !== cells2.length) {
            return false;
        }
        for (let i: number = 0; i < cells1.length; i++) {
            if (cells1[i].text !== cells2[i].text) {
                return false;
            }
        }
        return true;
    }
    public static compCells2(cells1: CellVo[][], cells2: CellVo[][]) {
        if (cells1 === cells2) {
            return true;
        }
        if (cells1.length !== cells2.length) {
            return false;
        }
        for (let i: number = 0; i < cells1.length; i++) {
            if (cells1[i].length !== cells2[i].length) {
                return false;
            }
            for (let j: number = 0; j < cells1[i].length; j++) {
                if (cells1[i][j].text !== cells2[i][j].text) {
                    return false;
                }
            }

        }
        return true;
    }
    public static dataToCells(data:any)
    {
        let cells:CellVo[]=[];
        for (let i:number=0;i<data.length;i++)
        {
            let ncell:CellVo=new CellVo(data[i]._text,[]);
           
            let classArray:string[]=data[i].classArray;
            for (let k:number=0;k<classArray.length;k++)
                {
                    if (classArray[k]!=='')
                    {
                        console.log(classArray[k]);
                        ncell.addClass(classArray[k]);
                    }
                    
                }
            cells.push(ncell);

        }
        return cells;
    }
    public static dataToGrid(data:any)
    {
        let cells:CellVo[][]=[];
        for (let i:number=0;i<data.length;i++)
        {
            let dat:any=data[i];
            
            cells[i]=[];
            for (let j:number=0;j<dat.length;j++)
            {
                let ncell:CellVo=new CellVo(dat[j]._text,[]);
               
                let classArray:string[]=dat[j].classArray;
               // console.log(classArray);
                
                for (let k:number=0;k<classArray.length;k++)
                {
                    if (classArray[k]!=='')
                    {
                        console.log(classArray[k]);
                        ncell.addClass(classArray[k]);
                    }
                    
                }
                cells[i].push(ncell);
            }
        }
        return cells;
    }
    public static dataToFractCells(data:any)
    {
        let fcells:FractCellVo[]=[];

        for (let i:number=0;i<data.length;i++)
        {
            console.log(data[i]);
            let fcell:FractCellVo=new FractCellVo(this.dataToCells(data[i].top),this.dataToCells(data[i].bottom),this.dataToCells(data[i].whole));
            fcells.push(fcell);
        }
        return fcells;
    }
    public static fixDec(cells: CellVo[]) {
        let cells2: CellVo[] = [];

        for (let i: number = 0; i < cells.length; i++) {
            if (cells[i].text === ".") {
                if (i > 0) {
                    cells[i - 1].addClass("decimal");
                }
            }
            else {
                cells2.push(cells[i]);
            }
        }
        return cells2;
    }
    public static cellsToSave(cells:CellVo[])
    {      
        let saves:SaveCellVo[]=[];

        for (let i:number=0;i<cells.length;i++)
        {
            saves.push(cells[i].getOutput());
        }

        return saves;
    }
    public static mcellsToSave(cells:CellVo[][])
    {      
        let saves:SaveCellVo[][]=[];

        for (let i:number=0;i<cells.length;i++)
        {
            saves[i]=[];

            for (let j:number=0;j<cells[i].length;j++)
            {
                saves[i].push(cells[i][j].getOutput());
            }
          
        }

        return saves;
    }

    public static fcellsToSave(cells:FractCellVo[])
    {      
        let saves:SaveFractCell[]=[];

        for (let i:number=0;i<cells.length;i++)
        {
            
                let fcell:FractCellVo=cells[i]
                
                saves.push(new SaveFractCell(GridUtil.cellsToSave(fcell.top),GridUtil.cellsToSave(fcell.bottom),GridUtil.cellsToSave(fcell.whole)))
              
            
          
        }

        return saves;
    }
}