import { CSSTypes } from "../../classes/Constants";
import MainStorage from "../../classes/MainStorage";
import { CellVo } from "../../dataObjs/CellVo";
import { CSSVo } from "../../dataObjs/CSSVo";

export class CssStyleUtil
{
    private static instance:CssStyleUtil | null=null;
    public cssMap:Map<string,CSSStyleDeclaration >

    constructor()
    {
        this.cssMap=new Map<string,CSSStyleDeclaration >();
        (window as any).cssUtil=this;
        this.mapStyles();
    }
    public static getInstance():CssStyleUtil
    {
        if (this.instance===null)
        {
            this.instance=new CssStyleUtil();
        }
        return this.instance;
    }
    public static addCssToCell(cssVo:CSSVo,cellVo:CellVo)
    {
        if (cellVo.classArray.includes(cssVo.value))
        {
            cellVo.removeClass(cssVo.value);
            return;
        }
         if (cssVo.type===CSSTypes.Color)
        {           
            this.removeCssByType(CSSTypes.Color,cellVo);
        }
        if (cssVo.type===CSSTypes.Size)
        {
           this.removeCssByType(CSSTypes.Size,cellVo);
        }
        cellVo.addClass(cssVo.value);
        
    }
    public static removeCssByType(type:CSSTypes,cellVo:CellVo)
    {
        let ms:MainStorage=MainStorage.getInstance();

        let cssClasses:CSSVo[] | undefined=ms.getCssByType(type);
            if (cssClasses)
            {
                for (let i:number=0;i<cssClasses.length;i++)
                {
                    cellVo.removeClass(cssClasses[i].value);
                }
            }
    }
    private mapStyles()
    {
        let styleSheets:StyleSheetList=document.styleSheets;
        
        for (let i:number=0;i<styleSheets.length;i++)
        {
            let sheet:CSSStyleSheet=styleSheets[i];
            

            if (sheet.href===null)
            {
            let rules:CSSRuleList=sheet.cssRules;

            for (let j:number=0;j<rules.length;j++)
            {
                let rule:CSSRule=rules[j];               
               
                let selectorName:string=(rule as CSSStyleRule).selectorText;
                
                let rule2:CSSStyleDeclaration =(rule as any).style;
                
                this.cssMap.set(selectorName,rule2);
                
            }
        }
        }
    }
}