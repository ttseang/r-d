//import { MainStorage } from "./MainStorage";

import MainStorage from "../classes/MainStorage";



export class ApiConnect
{
  //  private authKey: string = "DQho4TTbytnJc31puZNDxfhY";
    private authKey:string="";
    private ms:MainStorage=MainStorage.getInstance();
    //
    public Save(data:any,callback:Function)
    {
               
        let url:string="https://tthq.me/api/pr/writeequation";
        fetch(url,{
            method:"post",
            body:data,
            headers:new Headers({'Authorization':this.authKey})          
        }).then(
           response=>{
            if (response.ok) {
                response.json().then(json => {
                    callback(json);
                  });
            }
           })        
    }
    public Send(data:any,callback:Function)
    {
               
        let url:string="https://tthq.me/api/dv/endpointX";
        fetch(url,{
            method:"post",
            body:data,
            headers:new Headers({'Authorization':this.authKey})          
        }).then(
           response=>{
            if (response.ok) {
                response.json().then(json => {
                    callback(json);
                  });
            }
           })        
    }
    public getFileList(callback: Function,filter:string) {
        //https://tthq.me/api/pr/listequations?q=TEST
        let url: string = "https://tthq.me/api/pr/listequations?q="+filter;
        fetch(url, {
            method: "post",
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
    public getFileContent(lti:string,callback:Function)
    {
        //https://tthq.me/api/pr/getequation/1
        //https://tthq.me/api/pr/getequation/X.TEST.01
        let url: string = "https://tthq.me/api/pr/getequation/"+lti;
        fetch(url, {
            method: "post"
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        console.log(json);                        
                        callback(json);
                    });
                }
            })
    }
    public deleteFile(lti:string,callback:Function)
    {
        let url: string = "https://tthq.me/api/dv/deleteX/"+lti;
        fetch(url, {
            method: "post",
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
}
export default ApiConnect;
