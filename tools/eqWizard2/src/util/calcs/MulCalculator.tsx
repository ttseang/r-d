
import { CellVo } from "../../dataObjs/CellVo";
import { MCellGridOutputVo } from "../../dataObjs/outputs/MCellGridOutputVo";
import { NumVo } from "../../dataObjs/NumVo";
import { GridUtil } from "../GridUtil";
import { PlacesUtil } from "../PlacesUtil";
import MainStorage from "../../classes/MainStorage";
import { ArrayUtil } from "../ArrayUtil";

export class MulCalculator {
    private totalSteps: number = 0;
    private stepIndex: number = -1;

    private lastStep: MCellGridOutputVo | null = null;

    private numsObj: NumVo[] = [];

    private bodyRows: CellVo[][] = [];

    private steps: MCellGridOutputVo[] = [];

    private body1: string[] = [];
    private body2: string[] = [];


    private carryStrings: string[][] = [];
    private carryStrings2: string[] = [];

    private currentVal: number[] = [];
    private products: number[][] = [];
    private answer: number = 0;
    private sumArray: number[] = [];
    private sumString: string = "";

    //for products
    private maxDec: number = 0;
    private maxWhole: number = 0;

    private smallestLen:number=0;
    private zeroPlaces:number=0;

    private ms: MainStorage = MainStorage.getInstance();

    constructor() {
        (window as any).mcalc = this;
    }
    public doStepMul(expression: string) {


        let nums: string[] = expression.split("*");

        this.numsObj = PlacesUtil.makeNumberObjs(nums);

        let maxWhole: number = PlacesUtil.findMaxWhole(nums);
        let maxDec: number = PlacesUtil.findMaxDec(nums);

        this.maxWhole = maxWhole;
        this.maxDec = maxDec;

        for (let i: number = 0; i < nums.length; i++) {
            this.numsObj[i].formatDigits(maxWhole, maxDec);
            

            if (this.ms.lineUpDec === true) {
               
                this.numsObj[i].fixDigits(maxWhole, maxDec);
            }

        }

        this.smallestLen=Math.min(this.numsObj[0].num.length,this.numsObj[1].num.length);
        
        if (this.smallestLen===1)
        {
            this.ms.hideAnswer=true;
        }
       
        let digits1: number[] = this.numsObj[0].allDigits.slice().reverse();
        let digits2: number[] = this.numsObj[1].allDigits.slice().reverse();
        
       

        

        this.body1 = this.numsObj[0].allChars.slice();
        this.body2 = this.numsObj[1].allChars.slice();

        if (this.numsObj[0].isCurrency === true) {
            this.body1.unshift("$");
        }

        if (this.numsObj[1].isCurrency === true) {
            this.body2.unshift("$");
        }

        this.answer = this.numsObj[0].val * this.numsObj[1].val;

        this.bodyRows.push(GridUtil.arrayToCells2(this.body1, []));
        this.bodyRows.push(GridUtil.arrayToCells2(this.body2, []));

        this.totalSteps = digits1.length * digits2.length;

        //     let vals:number[]=[];

        let carry: number = 0;
        let carryString: string = "";

        let product: number = 0;

        this.addStep(0);
        
        //console.log("D2",digits2);

        for (let i: number = 0; i < digits2.length; i++) {

            this.products[i] = [];
            this.carryStrings[i] = ["@"];
            
          

            for (let k: number = 0; k < this.zeroPlaces; k++) {
                this.products[i].push(0);
            }
           
            // this.addStep();
            carry = 0;


            for (let j: number = 0; j < digits1.length; j++) {


                let n1: number = digits2[i];
                let n2: number = digits1[j];

                product = n1 * n2;

                

                //add in the carry
                product += carry;
              

                if (product > 9) {
                    carry = Math.floor(product / 10);


                    product -= carry * 10;

                    carryString = carry.toString();

                }
                else {
                    carry = 0;
                    carryString = "@";
                }
                

                this.products[i].unshift(product);
               
                this.addStep(1);

                this.carryStrings[i].unshift(carryString);
                this.addStep(2);
                
            }
            if (carry > 0) {
                this.carryStrings[i].shift();
                this.products[i].unshift(carry);
                this.addStep(3);

            }
            
           
            //check for all zeros
            if (this.isAllZeros(this.products[i])===false)
            {
                this.zeroPlaces++;
            }
        }

        this.addCols();

       //console.log(this.steps);

        return this.steps;

    }
    isAllZeros(product:number[])
    {
        for (let i:number=0;i<product.length;i++)
        {
            if (product[i]!==0)
            {
                return false;
            }
        }
        return true;
    }
    /**
     * takes out all empty rows of zeros
     * 
     * @returns number[][]
     */
    public trimProducts() {
        let nProducts: number[][] = [];

        for (let i: number = 0; i < this.products.length; i++) {
            let product: number[] = this.products[i];
            let len: number = product.length;
            let check: string = "0".repeat(len);
            if (product.join("") !== check) {
              
                nProducts.push(product);
            }
        }
        return nProducts;
        //this.products=nProducts;
    }
    private isZeroString(checkString:string)
    {
        for (let i:number=1;i<6;i++)
        {
            let zeroString:string="0".repeat(i);
            if (checkString===zeroString)
            {
                return true;
            }
        }
        return false;
    }
    private addCols() {

        let productsStrings: string[] = [];

       // //console.log("prods",this.products);

        for (let i: number = 0; i < this.products.length; i++) {
            let product: string = this.products[i].join("");
           
        //    //console.log("prod",product);

            if (this.isZeroString(product)===false) {
                productsStrings.push(product);
            }

           

        }

        let productNums: NumVo[] = PlacesUtil.makeNumberObjs(productsStrings);
        //console.log("prod",productNums);

        let maxWholeP = PlacesUtil.findMaxWhole(productsStrings);
        let maxDecP = PlacesUtil.findMaxDec(productsStrings);

        for (let i: number = 0; i < productNums.length; i++) {
            productNums[i].formatDigits(maxWholeP, maxDecP);
        }
        let cols: number[][] = [];
        for (let i: number = 0; i < productNums.length; i++) {
            let digits: number[] = productNums[i].allDigits.slice();
            //console.log(digits);

            for (let j: number = 0; j < digits.length; j++) {
               
                if (!cols[j]) {
                    cols[j] = [];
                }
                cols[j].push(digits[j]);
            }
        }

        //console.log("cols",cols);

        if (productNums.length > 1) {
            cols = cols.reverse();

            

            let carryOver: number = 0;

            this.carryStrings2 = ["@"];

            this.sumArray = [];

            for (let i: number = 0; i < cols.length; i++) {

                let col: number[] = cols[i];

                let sum: number = col.reduce((partialSum, a) => partialSum + a, 0);
                

                let c: number = Math.floor(sum / 10);
                let r: number = sum - (c * 10);


                if (c === 0) {
                    this.carryStrings2.unshift("@");
                }
                else {
                    this.carryStrings2.unshift(c.toString());
                }

              
                this.sumArray.unshift(r + carryOver);
                
                //console.log("sums",this.sumArray);

                carryOver = c;
                this.addStep(4);
                
            }

            if (carryOver > 0) {
                this.sumArray.unshift(carryOver);
            }
        }
        this.addStep(5);

        return this.steps;
    }
    getZeroSpaces() {
        let spaceCount: number = 0;

        for (let i: number = 0; i < this.numsObj.length; i++) {
            let numObj: NumVo = this.numsObj[i];
            let zCount: number = numObj.getZeroSpaces();
            spaceCount += zCount;
        }
        return spaceCount;
    }
    getAnswer() {

        let sumArray: number[] = this.sumArray.slice();
        
        console.log("sm",sumArray);
       /*  while (sumArray[sumArray.length - 1] === 0 && sumArray.length > 0) {
            sumArray.pop();
        } */
        
        

        let zCount: number = this.getZeroSpaces();
        for (let i: number = 0; i < zCount; i++) {
            sumArray.push(0);
        }

        let sum: number = parseInt(sumArray.join(""));
        sum = sum / (Math.pow(10, this.maxDec));
        
        if (isNaN(sum)) {
            return [];
        }
        this.sumString = sum.toString();
        let sums: string[] = this.sumString.split("");

        

        let answer: CellVo[] = [];
        for (let i: number = 0; i < sums.length; i++) {
            answer.push(new CellVo(sums[i].toString(), []));
        }
        console.log("an",answer);

        return answer;
    }
    private checkEmptyTop(top: CellVo[]) {
        for (let i: number = 0; i < top.length; i++) {
            if (top[i].text !== "@" && top[i].text !== "0") {
                
                return false;
            }
        }
       

        return true;
    }
    checkEmptyCarray(c:string[])
    {
        
        for (let i: number = 0; i <c.length; i++) {
            if (this.carryStrings2[i]!=="@")
            {
               return false;
            }
        }
        return true;
    }
    compCells(cells1: CellVo[], cells2: CellVo[]) {
        return GridUtil.compCells(cells1, cells2);
    }
    compCells2(cells1: CellVo[][], cells2: CellVo[][]) {
        return GridUtil.compCells2(cells1, cells2);
    }
    private getProductRowCount()
    {
        let rowCount:number=0;
        for (let i:number=0;i<this.products.length;i++)
        {
            let allZeros:boolean=this.isAllZeros(this.products[i]);
           
            if (allZeros===false)
            {
                rowCount++;
            }
        }
        return rowCount;
    }
    private addStep(id: number = -1) {
        this.stepIndex++;
        //console.log("add step "+id);
       
        let productCells: CellVo[][] = [];
        let products: number[][] = this.trimProducts();
     

       
            for (let i: number = 0; i < products.length; i++) {
                productCells[i] = [];

                let product: number[] = products[i];
                let productVal:number=parseInt(product.join(""));
                productVal=productVal/ (Math.pow(10, this.maxDec));
               

                let prodStrings:string[]=productVal.toString().split("");

                

                for (let k: number = 0; k < prodStrings.length; k++) {
                    let product2: string = prodStrings[k];
                    productCells[i].push(new CellVo(product2, []));
                }
            }
     

        let topCells: CellVo[][] = [];
        for (let i: number = 0; i < this.carryStrings.length; i++) {
            let topStringRow: string[] = this.carryStrings[i];
            topCells[i] = [];

            for (let j: number = 0; j < topStringRow.length; j++) {
                let tString: string = topStringRow[j];
                let topCell: CellVo = new CellVo(tString, []);
                topCells[i].push(topCell);
            }
            if (this.checkEmptyTop(topCells[i])) {
                topCells[i] = [];
            }
        }
        topCells = ArrayUtil.removeAllEmpty(topCells);

        

        if (this.checkEmptyCarray(this.carryStrings2))
        {
            this.carryStrings2=[];
        }

        let carryCells: CellVo[] = [];
        for (let i: number = 0; i < this.carryStrings2.length; i++) {
            carryCells.push(new CellVo(this.carryStrings2[i], ["carry"]));
        }
       

        let bodyRows2: CellVo[][] = [];
        for (let i: number = 0; i < this.bodyRows.length; i++) {
            bodyRows2[i] = [];

            for (let j: number = 0; j < this.bodyRows[i].length; j++) {
                let cellVo: CellVo = this.bodyRows[i][j];
                let copy: CellVo = cellVo.clone();
                bodyRows2[i][j] = copy;
            }
        }

       // let prodRowCount:number=this.getProductRowCount();

    

        let answerObj:CellVo[]=this.getAnswer();
        if (this.ms.hideAnswer===true || this.products.length===1)
        {
            answerObj=[];
        }
        
        let step: MCellGridOutputVo = new MCellGridOutputVo(topCells, bodyRows2, productCells, carryCells, answerObj, "*");

        if (this.ms.hideProduct===true)
        {
            step= new MCellGridOutputVo(topCells, bodyRows2, [], carryCells, answerObj, "*");
        }

        if (this.lastStep) {
            if (GridUtil.compCells2(step.top, this.lastStep.top) && GridUtil.compCells2(step.rows, this.lastStep.rows) && GridUtil.compCells(step.answer, this.lastStep.answer)) {
                //ignore
            }
            else {
                this.steps.push(step);
            }
        }
        else {
            this.steps.push(step);
        }

        this.lastStep = step;



    }
}