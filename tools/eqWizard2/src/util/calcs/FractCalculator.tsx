import { FractVo } from "../../dataObjs/FractVo";
import { FractGridOutput } from "../../dataObjs/outputs/FractGridOutput";
import { FractionUtil } from "../FractUtil";

export class FractCalculator
{
    private expression:string="";
    private tops:string[]=[];
    private wholes:string[]=[];
    private bottoms:string[]=[];
    private currentFract:FractVo | null=null;

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor()
    {
        (window as any).fcalc=this;
    }
    public calcFraction(expression:string)
    {
        this.expression=expression;
        
        let fractUtil:FractionUtil=new FractionUtil();
        let steps:FractGridOutput[]=fractUtil.stepFracts(expression,this.extractOps());
        steps.pop();
       return steps;

        
    }
    private extractOps() {
        let opArray: string[] = ["+", "-", "*", "/", "="];
        let ops: string[] = [];

        for (let i: number = 0; i < this.expression.length; i++) {
            let char: string = this.expression.charAt(i);
            if (opArray.includes(char)) {
                ops.push(char);
            }
        }
        return ops;
    }
}