import { CellVo } from "../../dataObjs/CellVo";
import { NumVo } from "../../dataObjs/NumVo";
import { PlacesUtil } from "../PlacesUtil";
import { RepeatChecker } from "../RepeatChecker";
import { GridUtil } from "../GridUtil";
import { DCellOutput } from "../../dataObjs/outputs/DCellOutput";
import MainStorage from "../../classes/MainStorage";
import { NumUtil } from "../NumUtil";

export class DivCalculator {

    private numsObj: NumVo[] = [];

    private maxDec: number = 0;
    private maxWhole: number = 0;

    private ms:MainStorage=MainStorage.getInstance();

    private lastDivs:number=0;
    private digits1: number[] = [];
   

    private zeroLimit: number = 2;
    private useRemain: boolean = false;


    private productLine: string[] = [];

    private quotient: CellVo[] = [];

    private qs: number[] = [];
    private divisor: number = 0;
    private dividend: number = 0;
    private remain:number=0;

    public steps: DCellOutput[] = [];


    constructor() {
        (window as any).dcalc = this;
    }

    doDiv(expression: string, useRemain: boolean = false, zeroLimit: number = 2) {
       
        this.qs = [];

        let nums: string[] = expression.split("/");
        this.numsObj = PlacesUtil.makeNumberObjs(nums);

        let div1:string=NumUtil.takeOutStrings(nums[0]);
        this.dividend = parseFloat(div1);

        this.zeroLimit = zeroLimit;


        let dmax: number = PlacesUtil.findMaxDecByObj(this.numsObj);
        let wmax: number = PlacesUtil.findMaxWholeByObj(this.numsObj);

        this.maxDec = dmax;
        this.maxWhole = wmax;
        if (this.ms.lineUpDec === true) {
        this.numsObj[0].fixDigits(wmax, dmax);
        this.numsObj[1].fixDigits(wmax, dmax);
        }

        this.digits1 = this.numsObj[0].allDigits.reverse().slice();

        let divisor: number = this.numsObj[1].val;
        this.divisor = divisor;



        let divs: number = 0;
        let divDigits: number[] = [];

        let spaces: number = 0;
        let rowID: number = 99;

        let addZeroCount: number = 0;
        let lastProduct:number=-1;

        this.addStep();

        while (this.digits1.length > 0) {

            rowID++;
            let safe: number = 0;

            while (divs < divisor && safe < 10) {
                divDigits.push(this.digits1.pop() || 0);
                divs = parseFloat(divDigits.join(""));
                safe++;
            }


            let divNum: NumVo = new NumVo(divs.toString());
            divNum.row = rowID;

           // spaces++;

          

            //q for quanity - or the number that will be muliplied
            //by the divsor
            let q: number = Math.floor(divs / divisor);
            this.qs.push(q);

            this.quotient = GridUtil.arrayToCells(this.qs, []);

            //the difference between the length of the dividend
            //and the length of the divsior
            //let diff:number=divs.toString().length-divisor.toString().length;
            
           

            if (this.lastDivs!==0)
            {
                this.productLine.pop();
              //  spaces--;
              
            }
           
            lastProduct=divs;
            this.productLine.push("@".repeat(spaces) + divs.toString());
            this.addStep();

            let a2: number = q * divisor;
            this.productLine.push("@".repeat(spaces) + a2.toString());
            this.addStep();

            let a2Obj: NumVo = new NumVo(a2.toString());
            a2Obj.row = -10 * rowID;

        
            if (spaces < 0) {
                return [];
            }

            this.addStep();
            
           // spaces -= bumpDif;


            divs -= a2;
            divDigits = [divs];
           
            this.lastDivs=divs;

          /*   let a3: number = parseInt(this.qs.join(""));
            a3 /= Math.pow(10, addZeroCount); */

            if (divs===0)
            {
                let bump:number=lastProduct.toString().length;
              
                spaces+=bump;
            }



            if (useRemain === false) {
                spaces++;
                let divNum:NumVo=new NumVo(lastProduct.toString());
                let remainNum:NumVo=new NumVo(divs.toString());

                let pdmax: number = PlacesUtil.findMaxDecByObj([divNum,remainNum]);
                let pwmax: number = PlacesUtil.findMaxWholeByObj([divNum,remainNum]);
                
                let remainString:string=remainNum.formatGridDigits(pwmax,pdmax);
                this.productLine.push("@".repeat(spaces) + remainString);
                this.addStep();
                if (divs !== 0) {
                    if (addZeroCount < this.zeroLimit) {
                        this.numsObj[0].addDecPlace();
                        this.digits1.unshift(0);

                        addZeroCount++;
                    }
                    else {
                       
                        let rc: RepeatChecker = new RepeatChecker();
                       
                        rc.fractionToDecimal(this.numsObj[0].val, this.numsObj[1].val);
                      

                    }
                }
            }
            else {

               
                this.remain=divs;
                if (divs>0)
                {
                

                let divNum:NumVo=new NumVo(lastProduct.toString());
                let remainNum:NumVo=new NumVo(divs.toString());

                let pdmax: number = PlacesUtil.findMaxDecByObj([divNum,remainNum]);
                let pwmax: number = PlacesUtil.findMaxWholeByObj([divNum,remainNum]);
                
                let remainString:string=remainNum.formatGridDigits(pwmax,pdmax);
                
                this.productLine.push("@".repeat(spaces) + remainString);
                this.addStep();
                }
            }
          //  spaces++;
        }
        return this.steps;
    }
    addStep() {
        let products: CellVo[][] = [];
        let pline: string[] | undefined = this.productLine.slice();
        pline.shift();

        for (let i: number = 0; i < pline.length; i++) {
            let product: CellVo[];
            if (i/2===Math.floor(i/2))
            {
                product=GridUtil.arrayToCells2(pline[i].split(""), 'bb,'.repeat(10).split(","));
            }
            else
            {
                product=GridUtil.arrayToCells2(pline[i].split(""), []);
            }
             
            products.push(product);
        }

        let divisorString:string=this.divisor.toString();
        if (this.numsObj[1].isCurrency)
        {
            divisorString="$"+divisorString;
        }
        let divsorCells: CellVo[] = GridUtil.arrayToCells2(divisorString.split(""), []);

        let dividendString:string=this.dividend.toString();
        if (this.numsObj[0].isCurrency)
        {
            dividendString="$"+dividendString;
        }

        let dividendCells: CellVo[] = GridUtil.arrayToCells2(dividendString.split(""), []);

        let remainCells:CellVo[]=GridUtil.arrayToCells2(this.remain.toString().split(""),[]);
        

        this.steps.push(new DCellOutput(this.quotient, divsorCells, dividendCells, products,remainCells));
    }
}
export default DivCalculator;