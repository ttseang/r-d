
import { FractCellVo } from "../dataObjs/FractCellVo";
import { FractVo } from "../dataObjs/FractVo";
import { FractGridOutput } from "../dataObjs/outputs/FractGridOutput";

export class FractionUtil {

    public answer: string = "";

    private fractIndex: number = -1;
    private fractions: FractVo[] = [];
    private ops: string[] = [];
    public steps: FractGridOutput[] =[];

    private lastFracs:FractVo[]=[];


    public fractAnswer: FractVo | null = null;

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {

    }
    private gcd(a: number, b: number): number {
        return b ? this.gcd(b, a % b) : a;
    }
    reduce(numerator: number, denominator: number): number[] {

        let gcd = this.gcd(numerator, denominator);
        return [numerator / gcd, denominator / gcd];
    }


    private range(min: number, max: number) {
        let arr = [];
        for (let i = min; i <= max; i++) {
            arr.push(i);
        }
        return arr;
    }
    private lcm(a: number, b: number) {
        return (a * b) / this.gcd(a, b);
    }
    leastCommonMultiple(min: number, max: number) {
        let multiple = min;
        this.range(min, max).forEach((n) => {
            multiple = this.lcm(multiple, n);
        });
        return multiple;
    }
    public opToWord(symbol: string): "add" | "sub" | "mul" | "div" {
        switch (symbol) {
            case "+":
                return "add";
            case "-":
                return "sub";
            case "*":
                return "mul";
            case "/":
                return "div"
        }
        return "add";
    }
    /**
    * a  c
    * _ +_
    * b  d
    * @param {number} a
    * @param {number} b
    * @param {number} c
    * @param {number} d
    * @param {number} w1
    * @param {number} w2
    * @param {("add"|"sub"|"mul"|"div")} op
    * @return {*} 
    * @memberof FractionUtil
    */
    public computeFractions(a: number, b: number, c: number, d: number, w1: number, w2: number, op: "add" | "sub" | "mul" | "div") {


        

        /**
         * if only whole numbers...
         */
        if (a === 0 && b === 0 && c === 0 && d === 0) {
          
            let wholeSum: number = 0;

            switch (op) {
                case "add":
                    wholeSum = w1 + w2;
                    break;

                case "sub":
                    wholeSum = w1 - w2;
                    break;

                case "mul":
                    wholeSum = w1 * w2;
                    break;

                case "div":
                    wholeSum = w1 / w2;
                    break;
            }
            

            return new FractVo("0", "0", wholeSum.toString());

        }
        //add lone whole number to fraction
        if (a === 0 && b === 0) {
            //make a whole fraction (1/1) and borrow from the whole interger
            a = d;
            b = d;
            w1--;
            if (w1 < 0) {
                w1 = 0;
            }
        }
        if (c === 0 && d === 0) {
            //make whole into fraction
            c = b;
            d = b;
            w2--;
            if (w2 < 0) {
                w2 = 0;
            }
        }

        //let b1 = b * d;
        let t1 = a;
        let t2 = c;
        let b2 = b;
        if (b !== d && op !== "mul") {
           //find lowest common
            /*  t1 = a * d;
             t2 = c * b;
             b2 = b * d; */

            b2 = this.findLCD(b, d);

            t1 = a * (b2 / b);
            t2 = c * (b2 / d);

          /*   t1 += w1 * b2
            t2 += w2 * b2; */

            

            let nFract1: FractVo = new FractVo(t1.toString(), b2.toString(), w1.toString());
            let nFract2: FractVo = new FractVo(t2.toString(), b2.toString(), w2.toString());
            let fArray: FractVo[] = [nFract1, nFract2].concat(this.getRest());
         
            this.addStep(fArray);
           

        }



        let topSum = 0;
        let whole = 0;

        switch (op) {
            case "add":
               
                topSum = t1 + t2;
                whole = w1 + w2;
                break;

            case "sub":
                topSum = t1 - t2;
                whole = w1 - w2;
                break;

            case "mul":
                topSum = t1 * t2;
                whole = w1 * w2;
                t1 = a * c;
                b2 = b * d;
                this.addOneToStory(topSum, b2, whole);
                break;

            case "div":
                topSum = t1 / t2;
                whole = w1 / w2;
                break;
        }



       

        while (topSum > b2 && b2 > 0) {
            this.addOneToStory(topSum, b2, whole);
            whole++;
            topSum -= b2;
        }
        

        this.addOneToStory(topSum, b2, whole);
        let rArray = this.reduce(topSum, b2);

        let t3 = rArray[0];
        let b3 = rArray[1];
        if (whole === 0 && t3 === 0) {
            return new FractVo("0", "0");
        }
        if (t3 === b3) {
            whole++;
            t3 = 0;
            b3 = 0;
            this.addOneToStory(t3, b3, whole);
            //  return whole;
        }

       
        let fractVo: FractVo = new FractVo(t3.toString(), b3.toString());
        fractVo.w1 = whole;


        return fractVo;
        
    }
    addOneToStory(t1: number, b2: number, w1: number) {
        let nFract1: FractVo = new FractVo(t1.toString(), b2.toString(), w1.toString());
        
        let fArray: FractVo[] = [nFract1].concat(this.getRest());
        this.addStep(fArray);

       
    }
    addTwoToStory(w1: number, w2: number, t1: number, t2: number, b2: number) {
        let nFract1: FractVo = new FractVo(t1.toString(), b2.toString(), w1.toString());
        let nFract2: FractVo = new FractVo(t2.toString(), b2.toString(), w2.toString());
        let fArray: FractVo[] = [nFract1, nFract2].concat(this.getRest());
        this.addStep(fArray);
       
    }
    findLCD(n1: number, n2: number) {
        let gcd = this.findGCD([n1, n2]);

        //then calculate the lcm
        return (n1 * n2) / gcd;
    }
    findGCD(arr: number[]) {
        let len = arr.length;
        let result = arr[0];
        for (let i = 1; i < len; i++) {
            result = this.gcd(arr[i], result);

            if (result === 1) {
                return 1;
            }
        }
        return result;
    }
    stepFracts(expression: string, ops: string[]) {
        this.ops = ops;
        //    let output: string[] = [];
        let exArray: string[] = expression.split(/[=*+-//]/);

       

        this.fractions = [];

        for (let i: number = 0; i < exArray.length; i++) {


            let fArray: string[] = exArray[i].split("_");

            if (fArray.length === 2) {
                this.fractions.push(new FractVo((fArray[0]), fArray[1]));
            }
            else {
                this.fractions.push(new FractVo(fArray[1], fArray[2], fArray[0]));
            }
        }

        this.addStep(this.fractions);

        let safe: number = 0;

        while (this.fractions.length > 0 && safe < 10) {
            safe++;
            let fracts: FractVo[] = [];

            if (this.fractions.length > 1) {
                let currentFract: FractVo = this.fractions[0];
                let fract2: FractVo = this.fractions[1];
                let op2: any = this.opToWord(this.ops[0]);

                let fract3: FractVo = this.computeFractions(currentFract.t1, currentFract.b1, fract2.t1, fract2.b1, currentFract.w1, fract2.w1, op2);

                fracts.push(fract3);
            }
            else {
                this.fractAnswer = this.fractions[0];
            }

            fracts = fracts.concat(this.getRest());


            this.ops.shift();
            if (fracts.length > 0) {
                this.addStep(fracts);
               
            }

            
            this.fractions = fracts;
        }
                

        return this.steps;
    }


    getRest(start: number = 2) {
        let fracts: FractVo[] = [];
        for (let j: number = start; j < this.fractions.length; j++) {
            
            fracts.push(this.fractions[j]);
        }
        return fracts;
    }
    addStep(fArray:FractVo[]) {
       
        let fcells:FractCellVo[]=[];

        for(let i:number=0;i<fArray.length;i++)
        {
            let fract:FractVo=fArray[i];
            let fcell:FractCellVo=new FractCellVo();
            fcell.fromNum(fract.t1,fract.b1,fract.w1);
        

            fcells.push(fcell);
        }
        if (fArray.length===1)
        {
           
            let last:FractVo[]=this.lastFracs.slice().reverse();

            for (let i:number=0;i<last.length;i++)
            {
                let fract:FractVo=last[i];
                let fcell:FractCellVo=new FractCellVo();
                fcell.fromNum(fract.t1,fract.b1,fract.w1);
                fcells.unshift(fcell);
            }
            if (!this.ops.includes("="))
            {
                this.ops.push("=");
            }
            
        }
        if (fArray.length>1)
        {
            this.lastFracs=fArray;
        }
     

        let ops:string[]=this.ops.slice();

        while(ops.length>fcells.length-1)
        {
            ops.pop();
        }

        let step:FractGridOutput=new FractGridOutput(fcells,ops);
  
        
       
        this.steps.push(step);

       
    }
}