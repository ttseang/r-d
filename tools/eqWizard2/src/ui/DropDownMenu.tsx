import React, { Component } from 'react';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
import { SidePanels } from '../classes/Constants';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
interface MyProps { }
interface MyState { action: number, item: number }
class DropDownMenu extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { action: 0, item: 0 };
    }
    doAction(mainCat: number, itemID: number) {
        switch (mainCat) {
            case 0:
                switch (itemID) {
                    case 0:
                        console.log("new");
                        this.controller.newProblem();
                        break;
                    case 1:
                        console.log("save");
                        this.controller.saveData();
                        break;
                    case 2:
                        console.log("open");
                        this.controller.openFile();
                        break;
                }
                break;

            case 1:
                switch (itemID) {
                    case 0:
                        console.log("view mode");
                        this.controller.setSidePanel(0);
                        this.controller.setEditMode(0);
                        break;

                    case 1:
                        console.log("edit mode");
                        this.controller.setEditMode(1);
                        break;
                }

                break;

            case 2:

                this.controller.setEditMode(1);
                // this.controller.setSidePanel(panels[action]);
                switch (itemID) {

                    case 1:
                        console.log("css");
                        this.controller.setSidePanel(SidePanels.CSS);
                        break;
                    case 2:
                        console.log("rows");
                        this.controller.setSidePanel(SidePanels.Rows);
                        break;

                    case 3:
                        console.log("cols");
                        this.controller.setSidePanel(SidePanels.Cols);
                        break;

                    case 4:
                        console.log("cells");
                        this.controller.setSidePanel(SidePanels.CellTools);
                        break;

                }
                break;

            case 3:

                switch (itemID) {
                    case 0:
                        console.log("show hide hidden");
                        this.ms.showHidden = !this.ms.showHidden;
                        this.controller.showHide(this.ms.showHidden);
                        break;
                    case 1:
                        console.log("recalc");
                        this.controller.setSidePanel(SidePanels.Tools);
                        break;
                }

                break;

             case 4:
                this.controller.showHelp();
             break;
        }

        this.setState({action:mainCat,item:itemID});
    }
    render() {

        let hidVar:string="selectedMenu";

        if (this.ms.showHidden===false)
        {
            hidVar="unselected";
        }

        let viewClass:string="unselected";
        let editClass:string="unselected";

        if (this.ms.editMode===0)
        {
            viewClass="selectedMenu";
        }
        else
        {
            editClass="selectedMenu";
        }

        return (<div>
            <ButtonGroup>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        File
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => { this.doAction(0, 0) }}>New</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 1) }}>Save</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 2) }}>Open</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        Mode
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => { this.doAction(1, 0) }} className={viewClass}>View Mode</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(1, 1) }} className={editClass}>Edit Mode</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        Edit
                    </Dropdown.Toggle>

                    <Dropdown.Menu>

                        <Dropdown.Item onClick={() => { this.doAction(2, 1) }}>Style</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(2, 2) }}>Edit Rows</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(2, 3) }}>Edit Columns</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(2, 4) }}>Edit Cells</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        Tools
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => { this.doAction(3, 0) }} className={hidVar}>Hidden Chars</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(3, 1) }}>Recacluate</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        Help
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => { this.doAction(4, 0) }}>How Do I...?</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </ButtonGroup>
        </div>)
    }
}
export default DropDownMenu;