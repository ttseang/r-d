export class WordConfig
{
    public word:string;
    public len:number;
    public xPos:number;
    public yPos:number;
    public line:string;
   
    constructor( word:string,xPos:number,yPos:number,len:number,line:string)
    {
        this.word=word;
        this.xPos=xPos;
        this.yPos=yPos;
        this.len=len;
        this.line=line;
    }
    clone()
    {
        return new WordConfig(this.word,this.xPos,this.yPos,this.len,this.line);
    }
}