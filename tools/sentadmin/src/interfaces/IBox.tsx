import { WordConfig } from "../dataObjs/WordConfig"

export interface IBox
{
    getConfig():WordConfig;
    getObj():Phaser.GameObjects.Container;

}