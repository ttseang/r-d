import Phaser from "phaser";
import { SquareVo } from "../dataObjs/SquareVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BoxUtil } from "../util/boxUtil";
import WordBox from "./WordBox";

export class SelectSquare extends Phaser.GameObjects.Sprite {
    public scene:Phaser.Scene;
    private diffX:number=0;
    private diffY:number=0;
    private dragMode:boolean=false;
    private children:WordBox[]=[];
    constructor(bscene:IBaseScene, key:string) {
        super(bscene.getScene(), 0, 0, key);
        this.scene=bscene.getScene();
        //
        //
        //      
       
        this.diffX = 0;
        this.diffY = 0;
        this.children = [];

        this.scene.add.existing(this);
    }

    setDragMode(val:boolean) {
        this.dragMode = val;       
    }
    setUp() {
        this.off('pointerdown');
        this.on('pointerdown', this.selectMe.bind(this));
        this.setInteractive();
        
    }
    selectMe() {
        this.off('pointerdown');
        console.log("select me");
        
        this.diffX = this.scene.input.activePointer.x - this.x;
        this.diffY = this.scene.input.activePointer.y - this.y;
        this.on('pointerup', this.mouseUp.bind(this));
        this.setDragMode(true);
    }
  /*   inBound(xx, yy) {
        let endX = this.x + this.displayWidth;
        let endY = this.y + this.displayHeight;

        if (xx > this.x - 1 && this.x < endX + 1) {
            if (yy > this.y - 1 && this.y < endY + 1) {
                return true;
            }
        }
        return false;
    } */
    setChildren(children:WordBox[]) {
        this.children = children;


        let corners:SquareVo = BoxUtil.findCorners(children);
        
        this.x = corners.x1;
        this.y = corners.y1;

        let ww = corners.x2 - corners.x1;
        let hh = corners.y2 - corners.y1;

        this.displayWidth = ww;
        this.displayHeight = hh;

        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            child.diffX = child.x - this.x;
            child.diffY = child.y - this.y;
        }
    }
    draw(startX:number,startY:number,endX:number,endY:number)
    {
       
        let corners:SquareVo=BoxUtil.findEdges(startX,startY,endX,endY);
        

        this.x = corners.x1;
        this.y = corners.y1;

        let ww = corners.x2 - corners.x1;
        let hh = corners.y2 - corners.y1;
       
        
        this.displayWidth = ww;
        this.displayHeight = hh;
    }
    updateChildren() {
        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];
            child.x = this.x + child.diffX;
            child.y = this.y + child.diffY;
        }
    }
    mouseUp() {
        this.on('pointerdown', this.selectMe.bind(this));
        this.dragMode = false;
    }
    unselect() {
        this.off('pointerdown');
        this.off('pointerup');
        this.dragMode = false;
    }
    update() {
        if (this.dragMode === true) {
            this.x = this.scene.input.activePointer.x - this.diffX;
            this.y = this.scene.input.activePointer.y - this.diffY;
            this.updateChildren();
        }
    }
}