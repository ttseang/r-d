import { WordConfig } from "../dataObjs/WordConfig";
import IBaseScene from "../interfaces/IBaseScene";
import { IBox } from "../interfaces/IBox";


export class GameDropTarget extends Phaser.GameObjects.Container implements IBox
{
    private config:WordConfig;
    public scene:Phaser.Scene;
    private back:Phaser.GameObjects.Sprite;
    private bscene:IBaseScene;
    public textObj!: Phaser.GameObjects.Text;
    public ox:number=0;

    constructor(config: WordConfig, bscene: IBaseScene)
    {
        super(bscene.getScene());
        this.config=config;
        this.scene=bscene.getScene();
        this.bscene=bscene;
        //
        //
        //
        this.back=this.scene.add.sprite(0,0,"holder").setOrigin(0, 0);
        this.add(this.back);

        this.back.displayWidth=bscene.cw*config.len;
        this.back.displayHeight=bscene.ch*2;
        if (config.line !== "d") {
            this.back.displayWidth = this.bscene.cw * config.len;
            // this.back.x=-this.back.displayWidth/2;
        }
        else {
            this.back.displayWidth = this.bscene.cd / 2 * config.len;
        }
        if (config.line === "c") {
            this.back.displayHeight = this.bscene.ch * config.len;
            this.back.displayWidth = this.bscene.cw * 1.5;

        }
        if (config.line === "cend") {
            this.back.displayHeight = this.bscene.ch * config.len;
            this.back.displayWidth = this.bscene.cw * 1.5;
        }
        this.back.alpha = 0.01;
        //
        //
        //
        if (config.word) {
            this.textObj = this.scene.add.text(0, 0, config.word, {align:"center", color: '#ffffff', fontSize: '26px' });
            this.add(this.textObj);
            //
            //
            //
            this.textObj.y = this.back.displayHeight - this.textObj.displayHeight - this.bscene.ch * 0.15;
            this.textObj.x = this.back.displayWidth / 2 - this.textObj.displayWidth / 2;
            if (config.line === "c") {
                this.textObj.y = this.back.displayHeight / 2;
                this.textObj.x = this.back.displayWidth;
                this.textObj.setOrigin(0.5, 0.5);
                this.textObj.setAngle(-90);
            }

            if (config.line === "cend") {
                this.textObj.y = this.back.displayHeight / 2;
                this.textObj.x = this.back.displayWidth;
                this.textObj.setOrigin(0.5, 0.5);
                this.textObj.setAngle(90);
            }
            if (config.line==="d")
            {
                this.setAngle(45);
            this.textObj.setAngle(1);
            }
            if (this.textObj.displayWidth > this.back.displayWidth) {
                this.back.displayWidth = this.textObj.displayWidth;
               
            }
            this.hideWord();
        }
       
    }
    turnSelected()
    {
        if (this.textObj)
        {
            this.textObj.setColor("#ff0000");
        }
       
    }
    turnNormal()
    {
        if (this.textObj)
        {
            this.textObj.setColor("#ffffff");
        }       
    }
    hideWord()
    {
        this.textObj.setText("?");
        this.ox=this.textObj.x;
        this.textObj.x=this.config.len*this.bscene.cw/2;
    }
    showWord()
    {
        this.turnNormal();
        this.textObj.x=this.ox;
        this.textObj.setText(this.config.word);
    }
    getConfig(): WordConfig {
        return this.config;
    }
    getObj(): Phaser.GameObjects.Container {
       return this;
    }
}