import { WordConfig } from "../dataObjs/WordConfig";
import IBaseScene from "../interfaces/IBaseScene";
//import Align from "../util/align";
import { BoxLines } from "../util/boxLines";
//import { AlignGrid } from "../util/alignGrid";

export class WordBox extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    //
    //
    //
    private dragCallback: Function;
    //
    //
    //
    public ox: number = 0;
    public oy: number = 0;
    //
    //
    //
    public diffX: number = 0;
    public diffY: number = 0;
    //
    //
    //
    public config: WordConfig;
    public word: string;
    public index: number = 0;
    //
    //
    //
    public back: Phaser.GameObjects.Sprite;
    public selectBox: Phaser.GameObjects.Sprite;
    public textObj!: Phaser.GameObjects.Text;
    //
    //
    //
    private lines: Phaser.GameObjects.Sprite[] = [];
    private graphics: Phaser.GameObjects.Graphics;
    //
    //
    //
    private bscene: IBaseScene;

    constructor(config: WordConfig, bscene: IBaseScene, dragCallback: Function) {
        super(bscene.getScene());
        this.scene = bscene.getScene();

        this.dragCallback = dragCallback;
        this.bscene = bscene;
        this.graphics = this.scene.add.graphics();
        this.graphics.lineStyle(2, 0x2980b9);
        this.add(this.graphics);
        //
        //
        //        
        this.config = config;
        this.word = config.word;
        this.type = "wordBox";
        //
        //
        //
        this.back = this.scene.add.sprite(0, 0, "holder").setOrigin(0, 0);
        this.back.displayHeight = this.bscene.ch * 2;

        if (config.line !== "d") {
            this.back.displayWidth = this.bscene.cw * config.len;
            // this.back.x=-this.back.displayWidth/2;
        }
        else {
            this.back.displayWidth = this.bscene.cd / 2 * config.len;
        }
        if (config.line === "c") {
            this.back.displayHeight = this.bscene.ch * config.len;
            this.back.displayWidth = this.bscene.cw * 1.5;

        }
        if (config.line === "cend") {
            this.back.displayHeight = this.bscene.ch * config.len;
            this.back.displayWidth = this.bscene.cw * 1.5;
        }

        //this.back.setTint(0xff0000);
        this.back.alpha = 0.01;
        //  this.back.visible = false;
        this.add(this.back);

        this.selectBox = this.scene.add.sprite(0, 0, "holder").setOrigin(0, 0);
        this.selectBox.displayHeight = this.back.displayHeight;
        this.selectBox.displayWidth = this.back.displayWidth;
        this.selectBox.setTint(0x45aaf2);
        this.selectBox.alpha = 0.3;
        this.add(this.selectBox);
        this.selectBox.visible = false;


        //
        //
        //
        if (config.word) {
            this.textObj = this.scene.add.text(0, 0, config.word, { color: '#ffffff', fontSize: '26px' });
            this.add(this.textObj);
            //
            //
            //
            this.textObj.y = this.back.displayHeight - this.textObj.displayHeight - this.bscene.ch * 0.15;
            this.textObj.x = this.back.displayWidth / 2 - this.textObj.displayWidth / 2;
            if (config.line === "c") {
                this.textObj.y = this.back.displayHeight / 2;
                this.textObj.x = this.back.displayWidth;
                this.textObj.setOrigin(0.5, 0.5);
                this.textObj.setAngle(-90);
            }

            if (config.line === "cend") {
                this.textObj.y = this.back.displayHeight / 2;
                this.textObj.x = this.back.displayWidth;
                this.textObj.setOrigin(0.5, 0.5);
                this.textObj.setAngle(90);
            }

            if (this.textObj.displayWidth > this.back.displayWidth) {
                this.back.displayWidth = this.textObj.displayWidth;
                this.selectBox.displayWidth = this.textObj.displayWidth;
            }
        }

        //
        //
        //
        this.back.setInteractive();
        this.back.on("pointerdown", this.clickMe.bind(this));

        this.drawLine(config.line);
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.graphics.visible = false;
        // this.scene.add.existing(this);

        /*  this.graphics.moveTo(0,0);
         this.graphics.lineTo(this.back.displayWidth,this.back.displayHeight);
         this.graphics.strokePath(); */
    }
    clickMe(pointer: Phaser.Input.Pointer) {
        this.dragCallback(this, pointer);
    }

    showLines(val: boolean) {       
        this.graphics.visible = val;
        if (this.config.word === "...") {
            this.textObj.visible = val;
        }
    }
    drawLine(lineStyle: string) {

        BoxLines.drawOnBox(this.graphics, this.back, this.config, this.bscene);
        this.back.visible = true;
        //  this.back.alpha=0.1;

        if (lineStyle === "d") {
            this.setAngle(45);
            this.textObj.setAngle(1);
        }
    }


    turnNormal() {
        if (this.textObj) {
            this.textObj.setColor("#ffffff");
        }
    }
    turnSelected() {
        if (this.textObj) {
            this.textObj.setColor("#ff0000");
        }
    }
    hideWord() {
        if (this.textObj) {
            this.ox = this.textObj.x;
            this.textObj.x = this.config.len * this.bscene.cw / 2;
            this.textObj.setText("?");
        }
    }
    setSelected(val: boolean) {
        this.selectBox.visible = val;
    }
    get right() {
        return this.x + this.displayWidth;
    }
    get bottom() {
        return this.y + this.displayHeight;
    }
    showWord() {
        if (this.textObj) {
            this.textObj.x = this.ox;
            this.textObj.setText(this.word);
            this.turnNormal();
        }
    }
    getInfo() {
        return new WordConfig(this.config.word, this.config.xPos, this.config.yPos, this.config.len, this.config.line);
        //return this.config;
        //return { word: this.config.word, len: this.config.len, line: this.config.line, xPos: this.config.xPos, yPos: this.config.yPos }
    }
}
export default WordBox;