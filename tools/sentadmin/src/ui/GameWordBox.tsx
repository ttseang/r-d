
import { WordConfig } from "../dataObjs/WordConfig";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";

export class GameWordBox extends Phaser.GameObjects.Container implements IGameObj {
    private config: WordConfig;
    public scene: Phaser.Scene;
    private back: Phaser.GameObjects.Sprite;
    private bscene: IBaseScene;
    public textObj!: Phaser.GameObjects.Text;
    public ox: number = 0;
    public oy: number = 0;
    private callback:Function;

    constructor(config: WordConfig, bscene: IBaseScene, dragCallback: Function) {
        super(bscene.getScene());
        this.config = config;
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.callback=dragCallback;
        //
        //
        //
        this.back = this.scene.add.sprite(0, 0, "holder");
        this.add(this.back);

        //   this.back.displayWidth=bscene.cw*config.len;
        //    this.back.displayHeight=bscene.ch*2;
        this.back.setTint(0x3498db);
        this.back.setInteractive();
        this.back.on('pointerdown',this.clickMe.bind(this));
        //  this.back.alpha = 0.5;
        //
        //
        //
        if (config.word) {
            


            this.textObj = this.scene.add.text(0, 0, config.word, {fontSize:"20px",color:'#ffffff'});
            this.textObj.setOrigin(0.5, 0.5);
            this.add(this.textObj);

            this.back.displayWidth = this.textObj.displayWidth * 1.1;
            this.back.displayHeight = this.bscene.ch;
        }
        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.scene.add.existing(this);
    }
    clickMe()
    {
        this.callback(this);
    }
    getConfig(): WordConfig {
        return this.config;
    }
    getObj(): Phaser.GameObjects.Container {
        return this;
    }
}