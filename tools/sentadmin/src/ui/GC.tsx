import MainStorage from "../classes/MainStorage";
import { WordConfig } from "../dataObjs/WordConfig";
import IBaseScene from "../interfaces/IBaseScene";
import { IBox } from "../interfaces/IBox";
import { AlignGrid } from "../util/alignGrid";
import { BoxLines } from "../util/boxLines";
import { GameDropTarget } from "./GameDropTarget";
import { GameWordBox } from "./GameWordBox";


export class GC {
    private scene: Phaser.Scene;
    private bscene: IBaseScene;
    private grid: AlignGrid;

    private ms: MainStorage = MainStorage.getInstance();
    private boxLines!: BoxLines;
    //game height and width
    private gw: number = 0;
    private gh: number = 0;
    //
    //
    //
    //grid cell width,height,diagonal
    private cw: number = 0;
    private ch: number = 0;
    private cd: number = 0;

    public xtol:number=3;
    public ytol:number=2;

    private dropTargets: GameDropTarget[] = [];
    private infoArray: WordConfig[] = [];
    private wordArray: string[] = [];

    private selectedBlock!: GameWordBox | null;
    private clickLock: boolean = false;
    private dgraphics:Phaser.GameObjects.Graphics;
    
    constructor(bscene: IBaseScene, grid: AlignGrid) {
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.grid = grid;
        this.create();

        this.dgraphics=this.scene.add.graphics();
        this.dgraphics.lineStyle(2,0xff0000,1);
    }
    /**
     * set up game objects and variables
     */
    create() {

        this.gw = this.bscene.getW();
        this.gh = this.bscene.getH();
        this.cw = this.grid.cw;
        this.ch = this.grid.ch;
        this.cd = this.grid.cd;

        this.boxLines = new BoxLines(this.bscene);

    }

    loadFile(data: string) {
        let obj: any = JSON.parse(data);

        console.log(obj);

        for (let i: number = 0; i < obj.length; i++) {
            let box: IBox = this.makeBox(obj[i].word, obj[i].xPos, obj[i].yPos, obj[i].len, obj[i].line);
            this.infoArray.push(box.getConfig());
        }
        this.makeDragBoxes();
        this.showLines(true);
    }

    /**
     * make a word box by params
     * @param word - text
     * @param xx - grid x position
     * @param yy - grid y position
     * @param len - the length of the box in grid cells
     * @param line - the style of the line, "h","d","tvsep","vsep", "c","cend"
     * @returns 
     */
    makeBox(word: string, xx: number, yy: number, len: number = 1, line: string = "h") {
        if (isNaN(len)) {
            len = 1;
        }
        return this.makeBoxbyConfig(new WordConfig(word, xx, yy, len, line));
    }
    /**
    * create a box using a WordConfig Object
    * @param config WordConfig
    * @returns WordBox
    */
    makeBoxbyConfig(config: WordConfig) {
        //make a new word box with the information (wordConfig)
        //the scene, and the drag listener
        let wordBox = new GameDropTarget(
            config,
            this.bscene);

        //place the box on the grid
        this.grid.placeAt(config.xPos, config.yPos, wordBox);

        //snap the the box to the grid
        this.snapBoxToGrid(wordBox);

        this.wordArray.push(config.word);

        this.dropTargets.push(wordBox);
        if (config.line!=="vsep" && config.line!=="tvsep")
        {
            //this.showSpot(wordBox.x+wordBox.getConfig().len,wordBox.y);
        }
        
        //add the box to the scene
        this.scene.add.existing(wordBox);

        this.showLines(true);

        //return the wordBox
        return wordBox;
    }
    /**
     * snap a box to the grid
     * @param box 
     */
    snapBoxToGrid(ibox: IBox) {
        let box: Phaser.GameObjects.Container = ibox.getObj();

        let xx = box.x + this.cw * 0.1;
        let yy = box.y + this.ch * 0.1;
        let coord = this.grid.findNearestGridXY(xx, yy);
        // box.config.xPos = coord.x;
        // box.config.yPos = coord.y;
        this.grid.placeAt2(coord.x, coord.y, box);
    }
    showLines(val: boolean) {
        if (val === true) {
            this.boxLines.drawLinesOnBoxes(this.infoArray);
        }
        else {
            this.boxLines.clear();
        }
    }
    makeDragBoxes() {
        console.log(this.infoArray);
        let count=0;
        for (let i: number = 0; i < this.infoArray.length; i++) {

            let info: WordConfig = this.infoArray[i];
            if (info.line !== "tvsep" && info.line !== "vsep") {

                count++;
                let dragBox = new GameWordBox(this.infoArray[i],
                    this.bscene,
                    this.selectCurrent.bind(this)
                );
                
                this.grid.placeAt(count * 5, 17, dragBox);
                dragBox.ox=dragBox.x;
                dragBox.oy=dragBox.y;               
            }
        }
    }
    selectCurrent(box: GameWordBox) {
        this.selectedBlock = box;
        this.scene.input.on('pointermove', this.doDrag.bind(this));
        this.scene.input.once('pointerup', this.stopDrag.bind(this));
    }
    doDrag() {
        if (this.selectedBlock) {
            this.selectedBlock.x = this.scene.input.activePointer.x;
            this.selectedBlock.y = this.scene.input.activePointer.y;
            this.checkTargets();
        }

    }
    stopDrag() {
        if (this.selectedBlock) {
            //this.selectedBlock.alpha = 1;
            this.scene.input.off('pointermove');
            let dt: GameDropTarget | null = this.getDroppedTarget();
            console.log("dt=" + dt);

            if (dt) {

                let dropWord = dt.getConfig().word.toLowerCase();
                let selectWord = this.selectedBlock.getConfig().word.toLowerCase();
                //console.log("Drop word=" + dropWord);
                //console.log("selectWord=" + selectWord);

                if (dropWord !== selectWord) {
                    dt.turnNormal();
                    this.flyBack();
                }
                else {
                    this.selectedBlock.destroy();
                    this.selectedBlock = null;
                    dt.showWord();
                }
            }
            else {
                this.flyBack();
            }
        }
    }
    flyBack() {
        if (this.selectedBlock) {

            this.clickLock = true;
            this.scene.tweens.add({ targets: this.selectedBlock, duration: 250, y: this.selectedBlock.oy, x: this.selectedBlock.ox, onComplete: this.flyBackDone.bind(this) });
        }
    }
    flyBackDone() {
        this.clickLock = false;
    }
    getDroppedTarget() {
       
        if (this.selectedBlock) {
            for (let i = 0; i < this.dropTargets.length; i++) {
                let dt: GameDropTarget = this.dropTargets[i];
             
                let endX:number=dt.x+dt.getConfig().len*this.cw;
                let endY:number=dt.y+this.ch*2;

               
                if (this.selectedBlock.x>dt.x && this.selectedBlock.x<endX)
                {
                    if (this.selectedBlock.y>dt.y && this.selectedBlock.y<endY)
                    {
                        return dt;
                    }
                    
                }
                
               
            }
        }
        return null;
    }
       
    showSpot(xx:number,yy:number)
    {
        this.dgraphics.strokeRect(xx,yy,this.cw*this.xtol,this.ch*this.ytol);
        
    }
    checkTargets() {
        if (this.selectedBlock) {
            for (let i = 0; i < this.dropTargets.length; i++) {
                let dt: GameDropTarget = this.dropTargets[i];
             
                let endX:number=dt.x+dt.getConfig().len*this.cw;
                let endY:number=dt.y+this.ch*2;

               
                if (this.selectedBlock.x>dt.x && this.selectedBlock.x<endX)
                {
                    if (this.selectedBlock.y>dt.y && this.selectedBlock.y<endY)
                    {
                        dt.turnSelected();
                    }
                    else{
                        dt.turnNormal();
                    }
                }
                else
                {
                    dt.turnNormal();
                }
               
            }
        }
    }
}

export default GC;