import Controller from "../classes/Controller";
import MainStorage from "../classes/MainStorage";
import Sidebar from "../comps/Sidebar";
import { WordConfig } from "../dataObjs/WordConfig";

import IBaseScene from "../interfaces/IBaseScene";
import { AlignGrid } from "../util/alignGrid";
import { BoxLines } from "../util/boxLines";
import { BoxUtil } from "../util/boxUtil";
import HistoryObj from "../util/HistoryObj";
import { SelectSquare } from "./SelectSquare";
import WordBox from "./WordBox";

export class DC {
    private scene: Phaser.Scene;
    private bscene: IBaseScene;
    private grid: AlignGrid;
    private historyObj: HistoryObj = new HistoryObj();
    private ms: MainStorage = MainStorage.getInstance();
    private controller: Controller = Controller.getInstance();
    private boxLines!:BoxLines;
    private showTheLines:boolean=false;
    private isDragging:boolean=false;
    private words:string[]=[];
    //
    //
    //DRAG MODE CONSTANTS
    //
    //CAN ONLY SELECT ONE ITEM BY CLICK
    private static SINGLE_SELECT: number = 0;

    //USER HAS SELECTED MULTI SELECT
    private static MULTI_SELECT: number = 1;

    //USER IS DRAGGING THE CURSOR WITH THE BUTTON DOWN
    private static DRAG_SELECT: number = 2;

    //USER CAN DRAG SELECTED ITEMS AS A GROUP
    private static MULTI_DRAG: number = 3;

    //current select mode
    private selectMode: number = 0;
    //
    //
    //arrays

    //all word boxes
    private allBoxes: WordBox[] = [];

    //multi selected boxes
    private mSelect: WordBox[] = [];
    //
    //
    //game height and width
    private gw: number = 0;
    private gh: number = 0;
    //
    //
    //
    private diffX: number = 0;
    private diffY: number = 0;
    //
    //
    //
    //grid cell width,height,diagonal
    private cw: number = 0;
    private ch: number = 0;
    private cd: number = 0;
    //
    //
    //the start and end coordinates of the multiselect tool
    private mStartY: number = 0;
    private mStartX: number = 0;
    private mEndY: number = 0;
    private mEndX: number = 0;
    //
    //
    //Game Objects

    //an image used for clicking the canvas
    private back!: Phaser.GameObjects.Sprite;

    //currently selected box
    private currentBox!: WordBox | null;

    //multi select tool graphic
    private selectSquare!: SelectSquare | null;
    //
    //
    //pass in the scene and the align grid
    //
    constructor(bscene: IBaseScene, grid: AlignGrid) {
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.grid = grid;
        this.create();
    }
    
    /**
     * set up game objects and variables
     */
    create() {

        this.gw = this.bscene.getW();
        this.gh = this.bscene.getH();

        //make the back of the canvas
        //for click purposes
        this.back = this.scene.add.sprite(0, 0, "holder").setOrigin(0, 0);
        this.back.displayWidth = this.gw;
        this.back.displayHeight = this.gh;
        this.back.setInteractive();
        this.back.alpha = 0.1;
        //
        //
        //
        this.boxLines=new BoxLines(this.bscene);
        //
        //
        //get the measurments from the grid
        this.cw = this.grid.cw;
        this.ch = this.grid.ch;
        this.cd = this.grid.cd;
        //
        //
        //set the current box to null
        this.currentBox = null;
        //
        //
        //
        //set callbacks to react
        this.controller.updateCanvas = this.updateCurrent.bind(this);
        this.controller.setSelectMode = this.setSelectMode.bind(this);
        this.controller.redo = this.redo.bind(this);
        this.controller.undo = this.undo.bind(this);
        this.controller.makeBox = this.makeBox.bind(this);
        this.controller.delBox = this.deleteBox.bind(this);
        this.controller.copyBox = this.copyBox.bind(this);
        this.controller.saveHistory = this.saveHistory.bind(this);
        this.controller.showLines=this.toggleLines.bind(this);
        //
        //
        //make the select tool graphic
        this.selectSquare = new SelectSquare(this.bscene, "holder").setOrigin(0, 0);
        this.selectSquare.alpha = 0.5;
        this.selectSquare.setTint(0x2ecc71);
        this.selectSquare.visible = false;

        //set select mode to single select
        this.setSelectMode(DC.SINGLE_SELECT);

    }
    /**
     * set the select mode
     * @param mode 
     */
    setSelectMode(mode: number) {
        this.selectMode = mode;

        //turn off event listeners
        this.scene.input.off("pointermove");
        this.scene.input.off("pointerup");
        this.scene.input.off("pointerdown");


        switch (mode) {
            case DC.SINGLE_SELECT:
                if (this.selectSquare) {
                    this.selectSquare.visible = false;
                }
                //update react
                this.controller.updateSideBar(Sidebar.SELECT_ONE);

                //empty the multi selected array
                this.mSelect = [];

                //unselect all the word boxes
                this.deselectAll();

                //set up a listener for when the user clicks off of the current box
                this.back.on("pointerdown", this.unselect.bind(this));
                break;
            //
            //
            //set up for selecting multiple boxes
            case DC.MULTI_SELECT:
              
                //unselect all the word boxes
                this.deselectAll();

                //set up a listener for the user to start dragging
                this.back.on("pointerdown", this.startMultiSelect.bind(this));
                break;
            //
            //

            case DC.DRAG_SELECT:
               
                //set up a listener for then the user stops dragging
                //this.showLines(false);
                this.scene.input.once("pointerup", this.selectMultiEnd.bind(this));
                break;
            //
            //
            //
            case DC.MULTI_DRAG:
               
                //set up to drag multi objects
                if (this.selectSquare) {
                    //set listeners inside the square
                    this.selectSquare.setUp();
                    this.selectSquare.setDepth(this.words.length);
                    //show the select square
                    this.selectSquare.visible = true;

                    //add a listener to stop dragging the items
                    this.scene.input.on("pointerup", this.endMulti.bind(this));
                }
                break;
        }
    }
    /**
     * turn off all selected boxes
     * when user clicks the canvas
     */
    unselect() {
        this.deselectAll();
        this.currentBox = null;
    }
    /**
     * unselect all boxes
     */
    deselectAll() {
        this.allBoxes.forEach((child) => {
            if (child) {
                child.setSelected(false);
            }
        });
        this.currentBox = null;

        //let the controls know the word information has been updated
        this.controller.updateWord(this.ms.null_word);

    }
    /**
     * record the start position of the drag
     * @param pointer mouse or touch
     */
    startMultiSelect(pointer: Phaser.Input.Pointer) {
        this.back.off("pointerdown");
        this.mStartX = pointer.x;
        this.mStartY = pointer.y;
       // this.showLines(false);
        this.setSelectMode(DC.DRAG_SELECT);
    }
    /**
     * record the end postion of the drag
     * and find the boxes under the square
     * @param pointer 
     */
    selectMultiEnd(pointer: Phaser.Input.Pointer) {
        this.mEndX = pointer.x;
        this.mEndY = pointer.y;

        this.back.off("pointerup");
        this.findBoxesUnderSelect();
        this.controller.updateWord(this.ms.null_word);
       
        this.setSelectMode(DC.MULTI_DRAG);
    }
    /**
     * stop dragging the mulit selected boxes
     * @param pointer mouse or touch
     */
    endMulti(pointer: Phaser.Input.Pointer) {
        if (this.selectSquare) {
            this.selectSquare.off("pointerup");
            this.back.off("pointerdown");
            this.selectSquare.unselect();
            this.selectSquare.visible = false;
        }

        this.setSelectMode(DC.MULTI_SELECT);
        this.deselectAll();
        this.snapAll();
        this.showLines(true);
        this.saveHistory();
    }
    /**
     * snap the current box to the grid
     * 
     */
    snapToGrid() {
        if (!this.currentBox) {
            return;
        }
        this.snapBoxToGrid(this.currentBox);
        this.showLines(true);
        //
        //
        //
        this.scene.input.off("pointermove");
        this.scene.input.off("pointerup");
        this.isDragging=false;
      
        //let the controls know the word information has been updated
        this.controller.updateWord(this.currentBox.config);
        this.saveHistory();
    }
    /**
     * snap a box to the grid
     * @param box 
     */
    snapBoxToGrid(box: WordBox) {
        let xx = box.x + this.cw * 0.1;
        let yy = box.y + this.ch * 0.1;
        let coord = this.grid.findNearestGridXY(xx, yy);
        box.config.xPos = coord.x;
        box.config.yPos = coord.y;
        this.grid.placeAt2(coord.x, coord.y, box);
    }
    /**
     * let the controls know the word information has been updated
     * while the current box is being dragged
     */
    updatePosWhileDragging() {
        if (this.currentBox) {
            let xx = this.currentBox.x;
            let yy = this.currentBox.y;
            let coord = this.grid.findNearestGridXY(xx, yy);
            this.currentBox.config.xPos = coord.x;
            this.currentBox.config.yPos = coord.y;
            this.controller.updateWord(this.currentBox.config);
        }
    }
    /**
     * snap all boxes to the grid
     */
    snapAll() {
        this.allBoxes.forEach((box) => {
            this.snapBoxToGrid(box);
        });
    }
    /**
     * make a word box by params
     * @param word - text
     * @param xx - grid x position
     * @param yy - grid y position
     * @param len - the length of the box in grid cells
     * @param line - the style of the line, "h","d","tvsep","vsep", "c","cend"
     * @returns 
     */
    makeBox(word: string, xx: number, yy: number, len: number = 1, line: string = "h") {
        if (isNaN(len)) {
            len = 1;
        }
        return this.makeBoxbyConfig(new WordConfig(word, xx, yy, len, line));
    }
    /**
     * create a box using a WordConfig Object
     * @param config WordConfig
     * @returns WordBox
     */
    makeBoxbyConfig(config: WordConfig) {
        //make a new word box with the information (wordConfig)
        //the scene, and the drag listener
        let wordBox = new WordBox(
            config,
            this.bscene,
            this.selectCurrent.bind(this)
        );
        //place the box on the grid
        this.grid.placeAt(config.xPos, config.yPos, wordBox);

        //snap the the box to the grid
        this.snapBoxToGrid(wordBox);

        //set an index for the box so it can be accessed later
        wordBox.index = this.allBoxes.length;

        //add the box to the scene
        this.scene.add.existing(wordBox);

        //put the box on the allBoxes array
        this.allBoxes.push(wordBox);

        this.showLines(true);

        //return the wordBox
        return wordBox;
    }
    /**
     * when a box is clicked and the select mode is single select
     * make that box the current box
     * 
     * @param box WordBox
     * @param pointer mouse or touch 
     */
    selectCurrent(box: WordBox, pointer: Phaser.Input.Pointer) {

        if (this.selectMode !== DC.SINGLE_SELECT) {
            return;
        }
        //turn off the old current box
        if (this.currentBox) {
            this.currentBox.setSelected(false);
        }

        //make the clicked box the currentBox
        this.currentBox = box;

        //turn on the select graphic inside the box
        this.currentBox.setSelected(true);

        //get the local points of the clicked postions
        this.diffX = pointer.downX - this.currentBox.x;
        this.diffY = pointer.downY - this.currentBox.y;

       

        //set up listeners for moving and releasing
        this.scene.input.on("pointermove", this.dragBox.bind(this));
        this.scene.input.on("pointerup", this.snapToGrid.bind(this));

        this.isDragging=true;
        this.showLines2();
        //this.showLines(false);
       // this.showTempLines(true);

        //update the controls with the current word information
        this.controller.updateWord(this.currentBox.config);
    }
    /**
     * update the postion of the box
     *  
     * @param pointer mouse or touch
     */
    dragBox(pointer: Phaser.Input.Pointer) {
        if (this.currentBox) {
            this.currentBox.x = this.scene.input.activePointer.x - this.diffX;
            this.currentBox.y = this.scene.input.activePointer.y - this.diffY;
            this.updatePosWhileDragging();           
        }
    }
    /**
     * 
     * @param config 
     * @returns 
     */
    updateCurrent(config: WordConfig) {
        if (!this.currentBox) {
            return;
        }
        this.showLines(false);

        //console.log("update index "+this.currentBox.index);

        this.allBoxes.splice(this.currentBox.index, 1);

        this.currentBox.destroy();

        let wordBox = this.makeBoxbyConfig(config);
        this.renumberBoxes();
        this.currentBox = wordBox;
        this.controller.updateWord(config);

        this.showLines(true);
        this.saveHistory();
    }
    loadFile(data:string)
    {
        let obj:any=JSON.parse(data);
       
        for (let i:number=0;i<obj.length;i++)
        {           
            this.makeBox(obj[i].word,obj[i].xPos,obj[i].yPos,obj[i].len,obj[i].line);
        }
        this.showLines(true);
        this.saveHistory();
    }
    /**
     * 
     * @param sent 
     */
    public setWords(sent: string) {
        let words: string[] = sent.split(" ");
        this.words=words;

        for (let i = 0; i < words.length; i++) {

            let word = words[i];

            word = word.replace("_", " ");

            let box: WordBox = this.makeBox(word, 2, i, 6, "h");

            this.snapBoxToGrid(box);
        }
        if (this.selectSquare) {
            this.selectSquare.setDepth(words.length);
        }
        this.saveHistory();
    }
    /**
     * make an array of information
     * to rebuild the canvas
     * @returns array
     */
    getInfo(): WordConfig[] {
        let infoArray: WordConfig[] = [];
        this.allBoxes.forEach((child) => {
            infoArray.push(child.getInfo());
        });
        return infoArray;
    }
    /**
     * save the current information
     */
    saveHistory() {
        let snap: WordConfig[] = this.getInfo();
        this.ms.currentLayout=snap;
        this.historyObj.addHistory(snap);
       // console.log(this.historyObj);
    }
    /**
     * find the boxes under the square 
     * while the user is dragging the select tool
     * to show them what is being selected
     */
    findUnderDragging() {

        //take 4 points and make a sqaure
        //this will return a select area
        //regardless of the direction the user drags
        let corners = BoxUtil.findEdges(this.mStartX, this.mStartY, this.scene.input.activePointer.x, this.scene.input.activePointer.y);
        let len = this.allBoxes.length;
        for (let i = 0; i < len; i++) {
            let box = this.allBoxes[i];
            if (box) {
                box.setSelected(false);
                if (box.right > corners.x1 - 1 && box.x < corners.x2 + 1) {
                    if (box.bottom > corners.y1 - 1 && box.y < corners.y2 + 1) {
                        box.setSelected(true);
                    }
                }
            }
        }
    }
    /**
     * find the boxes under the square when the mouse is released
     * @returns 
     */
    findBoxesUnderSelect() {
        this.mSelect = [];

        let corners = BoxUtil.findEdges(this.mStartX, this.mStartY, this.mEndX, this.mEndY);

        let len = this.allBoxes.length;
        for (let i = 0; i < len; i++) {
            let box = this.allBoxes[i];
            if (box) {
                box.setSelected(false);
                if (box.right > corners.x1 - 1 && box.x < corners.x2 + 1) {
                    if (box.bottom > corners.y1 - 1 && box.y < corners.y2 + 1) {
                        box.setSelected(true);
                        this.mSelect.push(box);
                    }
                }
            }
        }
        if (this.mSelect.length === 0) {
            this.setSelectMode(DC.SINGLE_SELECT);
            if (this.selectSquare) {
                this.selectSquare.visible = false;
            }

            return;
        }
        if (this.selectSquare) {
            this.selectSquare.setChildren(this.mSelect);
        }

    }
    /**
     * restore the canvas to the previous history
     * 
     */
    undo() {

        let historyEvent: WordConfig[] | undefined = this.historyObj.undo();

        if (historyEvent) {
            this.destroyAllWordBoxes();
            this.rebuild(historyEvent);
        }
    }
    /**
     * restore the canvas to the next foward point
     * only avaiable after undo
     */
    redo() {
        let historyEvent: WordConfig[] | undefined = this.historyObj.redo();

        if (historyEvent) {
            this.destroyAllWordBoxes();
            this.rebuild(historyEvent);
        }
    }
    /**
     * build the canvas based on an array
     * of wordConfig objects
     * @param historyEvent 
     */
    rebuild(historyEvent: WordConfig[]) {

        for (let i = 0; i < historyEvent.length; i++) {
            let config: WordConfig = historyEvent[i].clone();

            let wordBox = new WordBox(config, this.bscene, this.selectCurrent.bind(this));
            wordBox.index = this.allBoxes.length;
            this.allBoxes.push(wordBox);
            this.grid.placeAt2(config.xPos, config.yPos, wordBox);
            this.scene.add.existing(wordBox);
            this.currentBox = wordBox;
        }
        this.showLines(true);
        //unlock the history object
        //this prevents history rewinds or fowards
        //going faster than the canvas is rebuilt
        this.historyObj.locked = false;
    }
    /**
     * destroy all of the word boxes
     */
    destroyAllWordBoxes() {
        this.allBoxes.forEach((box) => { box.destroy(); });
        this.allBoxes = [];
    }
    /**
     * destroy a box by its index
     * @param index 
     */
    destroySingle(index: number) {
        //find the box
        let box = this.allBoxes[index];

        //destroy the sprite
        box.destroy();

        //take the box off of the array
        this.allBoxes.splice(index, 1);

        //renumber the boxes in the array
        this.renumberBoxes();

        this.saveHistory();
    }
    /**
     * when delete is press either by button or shortcode
     * determine if we need to delete a single (currentBox)
     * or all selected boxes (mArray)
     */
    deleteBox() {
        if (this.selectMode === DC.SINGLE_SELECT) {
            if (this.currentBox) {
                this.destroySingle(this.currentBox.index);
                this.currentBox = null;
            }
        }
        else {
            this.destroyMulti();
        }
        this.showLines(true);
    }
    /**
     * copy the current box
     */
    copyBox() {
        if (this.currentBox) {

            //make a copy of the current box's inforamation
            let config = this.currentBox.config.clone();
            config.xPos++;
            config.yPos++;

            //make a new box
            // let copyBox = this.makeBox(config.word, config.xPos + 1, config.yPos + 1, config.len, config.line);
            let copyBox = this.makeBoxbyConfig(config);

            //snap to the grid
            this.snapBoxToGrid(copyBox);
          
            //turn off the current box select graphic
            this.currentBox.setSelected(false);

            //turn on the new box select graphic
            copyBox.setSelected(true);

            //make the new box the current box
            this.currentBox = copyBox;

        
        }
        this.showLines(true);
    }
    /**
     * set the index of each box based on array
     * makes it possbile to find the box in the array
     */
    renumberBoxes() {
        for (let i = 0; i < this.allBoxes.length; i++) {
            this.allBoxes[i].index = i;
        }
    }
    /**
     * destroy all selected boxes
     */
    destroyMulti() {
        for (let i = 0; i < this.mSelect.length; i++) {

            //get the allBox array index
            let index = this.mSelect[i].index;

            //destroy the sprite
            this.mSelect[i].destroy();

            //take the box off of the allBox array
            this.allBoxes.splice(index, 1);
        }
        //renumber the boxes in the array
        this.renumberBoxes();

        //save the new history
        this.saveHistory();

        //set the select mode to single
        this.setSelectMode(DC.SINGLE_SELECT);
    }
    /**
     * 
     * @param xx horizontal direction to nudge
     * @param yy vertical direction to nudge
     * @returns 
     */
    nudge(xx: number, yy: number) {
       // console.log("NUDGE");
        
        if (this.selectMode !== DC.SINGLE_SELECT) {

            this.showLines(false);

            let corners = BoxUtil.findCorners(this.mSelect);

            //update the positions based on the params
            //multiplied by the cell width and height

            let x1 = corners.x1 + this.cw * xx;
            let x2 = corners.x2 + this.cw * xx;
            let y1 = corners.y1 + this.ch * yy;
            let y2 = corners.y2 + this.ch * yy;

            //if the values are out of bounds
            //exit the function
            if (x1 < 0 || x2 > this.gw || y1 < 0 || y2 > this.gh) {
                return;
            }
            //update all selected squares

            for (let i = 0; i < this.mSelect.length; i++) {
                let box = this.mSelect[i];
                box.x += this.cw * xx;
                box.y += this.ch * yy;
                this.snapBoxToGrid(box);
            }
            //update the select square graphic
            if (this.selectSquare) {
                this.selectSquare.x += this.cw * xx;
                this.selectSquare.y += this.ch * yy;

            }
            this.showLines(true);
            return;
        }
        //
        //SINGLE SELECTED
        //
        if (this.currentBox == null) {
            return;
        }
        this.showLines2();
        //update the position of the current box based on the params
        //multiplied by the cell width and height

        let x1 = this.currentBox.x + this.cw * xx;
        let x2 = this.currentBox.right + this.cw * xx;
        let y1 = this.currentBox.y + this.ch * yy;
        let y2 = this.currentBox.bottom + this.ch * yy;

        //if the values are out of bounds
        //exit the function
        if (x1 < 0 || x2 > this.gw || y1 < 0 || y2 > this.gh) {
            return;
        }

        //update the position of the current box
        this.currentBox.x += this.cw * xx;
        this.currentBox.y += this.ch * yy;

        //snap to the grid
        this.snapBoxToGrid(this.currentBox);

        this.showLines(true);

        //save the current positions
        this.saveHistory();
    }
    showLines2()
    {
       // console.log("SHOW LINES 2");
        this.boxLines.clear();
        if (this.selectMode===DC.SINGLE_SELECT)
        {
            this.allBoxes.forEach((child) => {
                if (child) {
                   // child.setSelected(false);
                    if (this.currentBox!==child)
                    {
                        this.boxLines.drawLinesOnBox(child.config);
                    } 
                    else
                    {
                        child.showLines(true);
                    }                   
                   // child.showLines(!val);
                }
            });
        }
    }
    //
    //
    //
    showLines(val:boolean)
    {     

        this.allBoxes.forEach((child) => {
            if (child) {
               // child.setSelected(false);
                child.showLines(false);
            }
        });
        if (val===true)
        {
            this.boxLines.drawLinesOnBoxes(this.getInfo());
        }
        else
        {
            this.boxLines.clear();
        }
    }
    showTempLines(val:boolean)
    {
        this.allBoxes.forEach((child) => {
            if (child) {
                child.setSelected(false);
                child.showLines(val);
            }
        });
    }
    toggleLines()
    {
       // console.log("force lines");
       // this.showLines(true);
        console.log(this.getInfo());
        this.allBoxes.forEach((child) => {
            if (child) {
               console.log(child.index);
            }
        });
      //  this.showTheLines=!this.showTheLines;
       // this.showLines(this.showTheLines);
    }
    //
    //
    //
    update() {
        //if the user is dragging the mouse to select
        //multiple boxes then update the size of the square
        if (this.selectMode === DC.DRAG_SELECT) {
            if (this.selectSquare) {
                this.selectSquare.draw(this.mStartX, this.mStartY, this.scene.input.activePointer.x, this.scene.input.activePointer.y);
                this.findUnderDragging();
                this.selectSquare.visible = true;
            }
        }
        //if the user has selected mutliple boxes
        //update the position of the select square graphic
        if (this.selectMode === DC.MULTI_DRAG) {
            if (this.selectSquare) {
                this.selectSquare.update();
            }
        }
    }
}
export default DC;
