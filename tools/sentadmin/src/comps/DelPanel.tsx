
/**
 * Panel for the copy and delete buttons
 */
import React, { Component } from "react";
import { Button, ButtonGroup, Card } from "react-bootstrap";
import Controller from "../classes/Controller";
import MainStorage from "../classes/MainStorage";
import { WordConfig } from "../dataObjs/WordConfig";


interface MyProps { wordConfig: WordConfig }
interface MyState { disabled: boolean }
class DelPanel extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();
  private controller: Controller = Controller.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = { disabled: false };
  }
  componentDidUpdate(prevProps: MyProps) {
    if (this.props.wordConfig.word !== prevProps.wordConfig.word) {
      let dis: boolean = (this.props.wordConfig.word !== "") ? false : true;
      this.setState({ disabled: dis });
    }

  }
  copyBox() {
    this.controller.copyBox();
  }
  delBox() {
    this.controller.delBox();
  }
  testMe() {
    this.controller.showLines();
  }
  getTestButton() {
    return (<Button variant="primary" onClick={this.testMe.bind(this)}>
      <i className="fas fa-vial"></i>
    </Button>)
  }
  render() {
    return (
      <Card>
        <Card.Body>
          <ButtonGroup>
            <Button variant="success" disabled={this.state.disabled} onClick={this.copyBox.bind(this)}>
              <i className="far fa-clone"></i>
            </Button>
            <Button variant="danger" disabled={this.state.disabled} onClick={this.delBox.bind(this)}>
              <i className="fas fa-trash-alt"></i>
            </Button>
          {this.getTestButton()}
          </ButtonGroup>
        </Card.Body>
      </Card>
    );
  }
}
export default DelPanel;
