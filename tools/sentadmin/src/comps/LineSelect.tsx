import React, { Component } from 'react';
import { ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
interface MyProps {lineStyle:number,callback:Function,disabled:boolean}
interface MyState {selected:number,disabled:boolean}
class LineSelect extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
this.state={selected:this.props.lineStyle,disabled:false};
}
handleChange(val:number)
{
    console.log(val);
    this.setState({selected:val});
    this.props.callback(val);
}
componentDidUpdate(prevProps:MyProps) {
    if (prevProps.lineStyle!==this.props.lineStyle)
    {
        this.setState({selected:this.props.lineStyle});
    }
    if (prevProps.disabled!==this.props.disabled)
    {
        this.setState({disabled:this.props.disabled});
    }
}
render()
{
 const gt:string=">";
 const lt:string="<";

return (<div>
    <ToggleButtonGroup name="linestyle" type="radio" value={this.state.selected} defaultValue={this.props.lineStyle} onChange={this.handleChange.bind(this)} >
      <ToggleButton size="sm" value={0}  disabled={this.state.disabled}>_</ToggleButton>
      <ToggleButton size="sm" value={1}  disabled={this.state.disabled}>\</ToggleButton>
      <ToggleButton size="sm" value={2}  disabled={this.state.disabled}>{lt}</ToggleButton>  
      <ToggleButton size="sm" value={3}  disabled={this.state.disabled}>{gt}</ToggleButton>          
    </ToggleButtonGroup>
</div>)
}
}
export default LineSelect;