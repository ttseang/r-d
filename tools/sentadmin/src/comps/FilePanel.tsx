/**
 * Panel for the open, new and save buttons
 */

import React, { Component } from 'react';
import { Card, ButtonGroup, Button } from 'react-bootstrap';
import Controller from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
interface MyProps { screenCallback: Function }
interface MyState { }
class FilePanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private controller:Controller=Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    private saveFile() {
        let saveInfo: string = JSON.stringify(this.ms.currentLayout);
        console.log(saveInfo);
    }
    private openFile() {
        this.props.screenCallback(2);
    }
    private newSent() {
        this.props.screenCallback(0);
    }
    private preview()
    {
        let saveInfo: string = JSON.stringify(this.ms.currentLayout);
        console.log(saveInfo);
        this.ms.prevData=saveInfo;
        this.controller.prevGame();
    }
    render() {
        return (
            <Card>
                <Card.Body>
                    <ButtonGroup>
                    <Button variant="secondary" onClick={this.preview.bind(this)}>
                            <i className="fas fa-eye"></i>
                        </Button>
                        <Button variant="secondary" onClick={this.saveFile.bind(this)}>
                            <i className="fas fa-save"></i>
                        </Button>

                        <Button variant="primary" onClick={this.openFile.bind(this)}>
                            <i className="fas fa-folder-open"></i>
                        </Button>
                        <Button variant="secondary" onClick={this.newSent.bind(this)}>
                            <i className="far fa-file"></i>
                        </Button>
                    </ButtonGroup>
                </Card.Body>
            </Card>
        );
    }
}
export default FilePanel;