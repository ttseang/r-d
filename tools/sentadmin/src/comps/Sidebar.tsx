/**
 * The main tool bar to add or select boxes
 */
import React, { Component } from "react";
import { ToggleButton, ToggleButtonGroup } from "react-bootstrap";
import Controller from "../classes/Controller";
import MainStorage from "../classes/MainStorage";

interface MyProps { selectMode: number }
interface MyState {
  selected: number;
}
class Sidebar extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();
  private controller: Controller = Controller.getInstance();

  public static SELECT_ONE: number = 0;
  public static SELECT_MANY: number = 0;

  constructor(props: MyProps) {
    super(props);
    this.state = { selected: this.props.selectMode };
  }
  componentDidUpdate(prevProps: MyProps) {
    if (prevProps.selectMode !== this.props.selectMode) {
      console.log("new mode " + this.props.selectMode);
      if (this.props.selectMode === 0) {
        this.setState({ selected: 0 });
      }

    }
  }
  setSingle() {
    this.controller.setSelectMode(0);
  }
  setMulti() {
    this.controller.setSelectMode(1);
  }
  handleChange(val: number) {
    this.setState({ selected: val });
    switch (val) {
      case 0:
        this.setSingle();
        break;

      case 1:
        this.setMulti();
        break;

      case 2:
        this.controller.makeBox("", 16, 8, 1, "tvsep");
        this.setState({ selected: 0 });
        break;

      case 3:
        this.controller.makeBox("", 16, 8, 1, "vsep");
        this.setState({ selected: 0 });
        break;

      case 4:
        this.controller.makeBox("...", 16, 8, 6, "cend");
        this.setState({ selected: 0 });
        break;

      case 5:
        this.controller.makeBox("...", 16, 8, 4, "d");
        this.setState({ selected: 0 });
        break;

      case 6:
        this.controller.makeBox("word", 16, 8, 4, "h");
        break;
    }
  }
  render() {
    return (
      <div>
        <ToggleButtonGroup
          name="toolbar"
          vertical={true}
          type="radio"
          tabIndex={0}
          value={this.state.selected}
          defaultValue={0}>
          <ToggleButton variant="secondary" size="lg" value={0} onClick={() => { this.handleChange(0) }}>
            <i className="fas fa-mouse-pointer"></i>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={1} onClick={() => { this.handleChange(1) }}>
            <i className="far fa-object-ungroup"></i>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={6} onClick={() => { this.handleChange(6) }}>
            <i className="fas fa-font"></i>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={2} onClick={() => { this.handleChange(2) }}>
            <img
              src="./assets/tvsep.png"
              width="25px"
              alt="tvsep"
            ></img>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={3}>
            <img
              src="./assets/vsep.png"
              width="25px"
              alt="tvsep"
            ></img>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={4}>
            <i className="fas fa-chevron-left"></i>
          </ToggleButton>
          <ToggleButton variant="secondary" size="lg" value={5}>
            <i className="fas fa-slash"></i>
          </ToggleButton>
        </ToggleButtonGroup>
      </div>
    );
  }//<i class="fas fa-font"></i>
}
export default Sidebar;
