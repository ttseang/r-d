
/**
 * panel for word, x and y postions
 */
import React, { ChangeEvent, Component } from "react";
import { Row, Col, Card, Form } from "react-bootstrap";
import Controller from "../classes/Controller";
import MainStorage from "../classes/MainStorage";
import { WordConfig } from "../dataObjs/WordConfig";


import LineSelect from "./LineSelect";
interface MyProps {
  wordConfig: WordConfig;
}
interface MyState {
  x: number;
  y: number;
  len: number;
  line: string;
  word: string;
  disabled: boolean;
}
class PropBox extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();
  private controller: Controller = Controller.getInstance();

  private lineStyles: string[] = ["h", "d", "c","cend"];

  constructor(props: MyProps) {
    super(props);
    //  let dis:boolean=(this.props.wordConfig.word!=="null")?false:true;

    this.state = {
      x: this.props.wordConfig.xPos,
      y: this.props.wordConfig.yPos,
      len: this.props.wordConfig.len,
      line: this.props.wordConfig.line,
      word: this.props.wordConfig.word,
      disabled: true
    };
  }
  componentDidUpdate(prevProps: MyProps) {

    if (this.props.wordConfig.xPos !== this.state.x ||
      this.props.wordConfig.yPos !== this.state.y ||
      this.props.wordConfig.word !== this.state.word ||
      this.props.wordConfig.line !== this.state.line ||
      this.props.wordConfig.len !== this.state.len) {
      let dis: boolean = (this.props.wordConfig.word !== "") ? false : true;

      this.setState({
        x: this.props.wordConfig.xPos,
        y: this.props.wordConfig.yPos,
        len: this.props.wordConfig.len,
        line: this.props.wordConfig.line,
        word: this.props.wordConfig.word,
        disabled: dis
      })
    }
  }
  setWord(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ word: e.target.value });
    let config: WordConfig = this.getConfig();
    config.word = e.target.value;
    this.commitData(config);
  }
  setX(e: ChangeEvent<HTMLInputElement>) {
    let val: number = parseFloat(e.target.value) || 0;

    if (isNaN(val) || val < 0) {
      val = 0;
    }
    this.setState({ x: val });
    let config: WordConfig = this.getConfig();
    config.xPos = val;
    this.commitData(config);
  }

  setY(e: ChangeEvent<HTMLInputElement>) {
    let val: number = parseFloat(e.target.value) || 0;
    if (isNaN(val) || val < 0) {
      val = 0;
    }

    this.setState({ y: val });
    let config: WordConfig = this.getConfig();
    config.yPos = val;
    this.commitData(config);
  }
  setLen(e: ChangeEvent<HTMLInputElement>) {
    let val: number = parseFloat(e.target.value) || 1;
    if (isNaN(val) || val < 1) {
      val = 1;
    }

    this.setState({ len: val });
    let config: WordConfig = this.getConfig();
    config.len = val;
    this.commitData(config);
  }

  changeLine(val: number) {
    if (isNaN(val)) {
      val = 0;
    }
    let lineStyle: string = this.lineStyles[val];

    this.setState({ line: lineStyle });

    let config: WordConfig = this.getConfig();
    config.line = lineStyle;
    this.commitData(config);
  }
  getLineIndex() {
    for (let i: number = 0; i < this.lineStyles.length; i++) {
      if (this.state.line === this.lineStyles[i]) {
        return i;
      }
    }
    return 0;
  }
  getConfig(): WordConfig {
    return new WordConfig(this.state.word, this.state.x, this.state.y, this.state.len, this.state.line);

  }
  commitData(config: WordConfig) {

    //console.log(config);
    this.controller.updateCanvas(config);

  }
  render() {
    return (
      <div>
        <Card>
          <Card.Body>
            <Row><Col><Form.Control type="text" size="sm" value={this.state.word} disabled={this.state.disabled} onChange={this.setWord.bind(this)} />
            </Col></Row>
            <Row>
              <Col sm={6}>
                x:
            <Form.Control type="number" size="sm" value={this.state.x} disabled={this.state.disabled} onChange={this.setX.bind(this)} />

              </Col>
              <Col sm={6}>
                y:
            <Form.Control type="number" size="sm" value={this.state.y} disabled={this.state.disabled} onChange={this.setY.bind(this)} />
              </Col>
            </Row>
            <Row>
              <Col>len:<Form.Control type="number" size="sm" value={this.state.len} disabled={this.state.disabled} onChange={this.setLen.bind(this)} /></Col>
              <Col></Col>
            </Row>
            <Row>
              <Col></Col>
              <Col></Col>
            </Row></Card.Body>
        </Card>

        <Card>
          <Card.Body>
            <LineSelect callback={this.changeLine.bind(this)} lineStyle={this.getLineIndex()} disabled={this.state.disabled}></LineSelect>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
export default PropBox;
