/**
 * Panel for the undo and redo buttons
 */
import React, { Component } from "react";
import { Button, ButtonGroup, Card } from "react-bootstrap";
import MainStorage from "../classes/MainStorage";
import Controller from "../classes/Controller";

interface MyProps {}
interface MyState {}
class HistoryPanel extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();
  private controller:Controller=Controller.getInstance();
  
  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  redo() {
    this.controller.redo();
  }
  undo() {
   this.controller.undo();
  }
  render() {
    return (
      <Card>
        <Card.Body>
          <ButtonGroup>
            <Button onClick={this.undo.bind(this)}>
              <i className="fas fa-undo"></i>
            </Button>
            <Button onClick={this.redo.bind(this)}>
              <i className="fas fa-redo"></i>
            </Button>
          </ButtonGroup>
        </Card.Body>
      </Card>
    );
  }
}
export default HistoryPanel;
