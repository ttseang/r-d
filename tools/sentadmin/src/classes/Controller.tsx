
//import { DC } from "../ui/DC";

export class Controller {

    private static instance: Controller;
   // public dc: DC | null = null;
    //
    //
  
    public updateWord: Function = () => { };
    public updateSideBar: Function = () => { };
    public updateCanvas: Function = () => { };
    public undo:Function = () => { };
    public redo:Function = () => { };
    public setSelectMode:Function = () => { };
    public makeBox:Function = () => { };
    public copyBox:Function = () => { };
    public delBox:Function = () => { };
    public saveHistory:Function = () => { };
    public showLines:Function=()=>{};
    public prevGame:Function=()=>{};
    /* constructor() {
      //
    } */
    static getInstance(): Controller {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new Controller();
        }
        return this.instance;
    }
}
export default Controller;