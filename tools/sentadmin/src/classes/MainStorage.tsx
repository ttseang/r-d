import { FileVo } from "../dataObjs/FileVo";
import { WordConfig } from "../dataObjs/WordConfig";


export class MainStorage {
    public sent: string;
    private static instance: MainStorage;
    public gameConfig:any;
    //
    //
    public null_word: WordConfig = new WordConfig("", 0, 0, 0, "none");
    public currentLayout:WordConfig[]=[];
    public files:FileVo[]=[];
    public selectedFile:number=0;
    public prevData:string="";
    
    public makeNewFlag:boolean=true;
    public prevMode:boolean=false;

    constructor() {
        this.sent = "";
        this.files.push(new FileVo("the fish flew in the air",'[{"word":"the","len":6,"xPos":3,"yPos":5,"line":"h"},{"word":"fish","len":6,"xPos":12,"yPos":5,"line":"h"},{"word":"flew","len":6,"xPos":5,"yPos":10,"line":"h"},{"word":"in","len":6,"xPos":15,"yPos":8,"line":"h"},{"word":"the","len":6,"xPos":23,"yPos":5,"line":"h"},{"word":"air","len":6,"xPos":24,"yPos":10,"line":"h"}]'));
        this.files.push(new FileVo("Where are you going?",'[{"word":"are","len":6,"xPos":19,"yPos":2,"line":"h"},{"word":"you","len":6,"xPos":12,"yPos":2,"line":"h"},{"word":"going","len":6,"xPos":25,"yPos":2,"line":"h"},{"word":"","len":1,"xPos":18,"yPos":2,"line":"tvsep"},{"word":"Where","len":7,"xPos":29,"yPos":3,"line":"d"}]'));
        this.files.push(new FileVo("Tashonda sent cards and letters.",'[{"word":"sent","len":6,"xPos":13,"yPos":6,"line":"h"},{"word":"cards","len":6,"xPos":24,"yPos":3,"line":"h"},{"word":"letters","len":6,"xPos":24,"yPos":9,"line":"h"},{"word":"Tashonda","len":10,"xPos":2,"yPos":6,"line":"h"},{"word":"","len":1,"xPos":12,"yPos":6,"line":"tvsep"},{"word":"and","len":6,"xPos":20,"yPos":5,"line":"c"},{"word":"and","len":6,"xPos":31,"yPos":6,"line":"cend"}]'));
        this.files.push(new FileVo("test me",'[{"word":"me","len":6,"xPos":21,"yPos":9,"line":"h"},{"word":"test","len":13,"xPos":4,"yPos":1,"line":"c"}]'));
    }
    static getInstance(): MainStorage {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new MainStorage();
        }
        return this.instance;
    }
}
export default MainStorage;