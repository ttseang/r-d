import { IBaseScene } from "../interfaces/IBaseScene";
import Phaser from "phaser";
import { AlignGrid } from "../util/alignGrid";
import MainStorage from "../classes/MainStorage";
/**
 * The base scene includes extra information
 * as well as the standard scene
 * passed to other classes as the IBaseScene interface
 */
export class BaseScene extends Phaser.Scene implements IBaseScene {
  public gw: number;
  public gh: number;
  //private graphics!: Phaser.GameObjects.Graphics;
  //align grid
  public grid!: AlignGrid;
  /**
   * coordinates from align grid
   */
  public ch: number = 0;
  public cw: number = 0;
  public cd: number = 0;
  /**
   * main storage singleton
   */
  public ms:MainStorage=MainStorage.getInstance();

  constructor(sceneName: string) {
    super(sceneName);
    
    /**
     * game height and width
     */
    this.gw = this.ms.gameConfig.width;
    this.gh = this.ms.gameConfig.height;
    
  }
  /**
   * 
   * @returns alginGrid
   */
  getGrid(): AlignGrid {
    return this.grid;
  }
  /**
   * overridden in scene class
   */
  create() {
    
  }
  /**
   * make the align grid
   * @param r rows
   * @param c columns
   */
  makeGrid(r:number=11,c:number=11) {
    this.grid = new AlignGrid(this, r, c);
    this.ch=this.grid.ch;
    this.cw=this.grid.cw;
    this.cd=this.grid.cd;
   // this.grid.showNumbers();
  }
  /**
   * 
   * @returns the real scene
   */
  public getScene(): Phaser.Scene {
    return this;
  }
  /**
   * 
   * @returns the games width
   */
  public getW(): number {
    return this.gw;
  }
  /**
   * 
   * @returns the game height
   */
  public getH(): number {
    return this.gh;
  }
}
