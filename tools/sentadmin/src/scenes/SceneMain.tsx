


import Controller from "../classes/Controller";
import { WordConfig } from "../dataObjs/WordConfig";
import DC from "../ui/DC";
import { KeyboardShortcuts } from "../util/keyboardShortcuts";



import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private controller:Controller=Controller.getInstance();

    private dc!: DC;

    constructor() {
        super("SceneMain");
        
    }
    preload() {
        this.load.image("holder", "assets/holder.jpg");
        this.load.image("tri", "assets/tri2.png");
    }
    create() {
        this.makeGrid(22, 44);
        this.grid.show();
        this.controller.prevGame=this.previewGame.bind(this);
        //let test:Phaser.GameObjects.Sprite=this.add.sprite(100,100,"holder");

        //diagram canvas
        this.dc = new DC(this, this.grid);

        if (this.ms.makeNewFlag === true) {
            this.dc.setWords(this.ms.sent);
        }
        else {
            this.dc.loadFile(this.ms.files[this.ms.selectedFile].data);
        }

        // 
        new KeyboardShortcuts(this, this.dc);

    }
    previewGame()
    {
        this.ms.files[this.ms.selectedFile].data=this.ms.prevData;
        this.controller.updateWord(new WordConfig("",0,0,0,"h"));
        this.scene.start("ScenePrev");
    }
    update() {
        this.dc.update();
    }
}
export default SceneMain;