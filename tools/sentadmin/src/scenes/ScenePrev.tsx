


import Controller from "../classes/Controller";
import MainStorage from "../classes/MainStorage";
import GC from "../ui/GC";


import { BaseScene } from "./BaseScene";

export class ScenePrev extends BaseScene {

    private gc!: GC;
    private mc:MainStorage=MainStorage.getInstance();
    private controller:Controller=Controller.getInstance();
    
    constructor() {
        super("ScenePrev");
        
    }
    preload() {
     //   this.load.image("holder", "assets/holder.jpg");
       // this.load.image("tri", "assets/tri2.png");
    }
    create() {
        this.makeGrid(22, 44);
        this.grid.show();
        this.controller.prevGame=this.editGame.bind(this);

        //let test:Phaser.GameObjects.Sprite=this.add.sprite(100,100,"holder");

        //game canvas
        
        
        let gc:GC=new GC(this,this.grid);
        gc.loadFile(this.mc.prevData);

    }
    editGame()
    {
        this.scene.start("SceneMain");
    }
    update() {
        
    }
}
export default ScenePrev;