import React, { Component } from "react";
import { Card } from "react-bootstrap";
import MainStorage from "../classes/MainStorage";
import FileScreen from "./FileScreen";
import LayoutScreen from "./LayoutScreen";
import SentenceScreen from "./SentenceScreen";
interface MyProps {}
interface MyState {
  mode: number;
}
/**
 * This is the parent screen for all of the other screen
 * use this class to switch between main views
 */
class BaseScreen extends Component<MyProps, MyState> {
  private ms:MainStorage=MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = { mode: 0 };
  }
  getScreen() {
    switch (this.state.mode) {
      case 0:
        return <SentenceScreen callback={this.setSentence.bind(this)} screenCallback={this.setMode.bind(this)}></SentenceScreen>;
      case 1:
        return <LayoutScreen screenCallback={this.setMode.bind(this)}></LayoutScreen>;
      case 2:
        return <FileScreen callback={this.setSentence.bind(this)} screenCallback={this.setMode.bind(this)}></FileScreen>
    }
  }
  setMode(mode:number)
  {
    this.setState({mode:mode});
  }
  setSentence(sent:string)
  {
      this.ms.sent=sent;
      this.setState({mode:1});
  }
  render() {
    return (
      <div id="base">
        <Card id="card1">
          <Card.Header className="header">Sentence Diagram Admin</Card.Header>
          <Card.Body>{this.getScreen()}</Card.Body>
        </Card>
      </div>
    );
  }
}
export default BaseScreen;
