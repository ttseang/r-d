import React, { Component } from 'react';
import { ListGroupItem, Card, ListGroup, Button, Col, Row } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
interface MyProps { callback: Function,screenCallback:Function }
interface MyState { }
class FileScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    selectFile(index: number) {
        console.log(index);
        this.ms.makeNewFlag = false;
        this.ms.selectedFile = index;
        this.props.callback(this.ms.files[this.ms.selectedFile].sent);
    }
    getFileList() {
        let listArray: JSX.Element[] = [];
        const len: number = this.ms.files.length;
        for (let i: number = 0; i < len; i++) {
            let fileKey: string = "file" + i.toString();
            listArray.push(<ListGroupItem key={fileKey} className="tal" onClick={() => { this.selectFile(i) }}>{this.ms.files[i].sent}</ListGroupItem>)
        }
        return listArray;
    }
    makeNew()
    {
        this.props.screenCallback(0);
    }
    render() {
        return (<div> <Card>
            <Card.Header>Open File</Card.Header>
            <Card.Body>
                <ListGroup>{this.getFileList()}</ListGroup>
            </Card.Body>
        </Card><hr />
        <Row><Col><Button onClick={this.makeNew.bind(this)}>Make New</Button></Col></Row></div>)
    }
}
export default FileScreen;