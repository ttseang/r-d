import React, { Component } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import MainStorage from "../classes/MainStorage";
import TextInputButton from "../comps/TextInputButton";
;
interface MyProps {
  callback: Function;
  screenCallback:Function;
}
interface MyState { }
class SentenceScreen extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  setText(text: string) {
    this.ms.makeNewFlag = true;
    this.props.callback(text);
  }
  loadFile()
  {    
    this.props.screenCallback(2);
  }
  render() {
    return (
      <div id="sentScreen">
        <Card>
          <Card.Header>Enter a sentence</Card.Header>
          <Card.Body>
            <TextInputButton buttonText="Make Sentence" callback={this.setText.bind(this)} cancelCallback={() => { }} showCancel={false} text={""} clearOnEnter={false}></TextInputButton>
          </Card.Body></Card>
        <hr />
        <Row><Col><Button onClick={this.loadFile.bind(this)}>Load File</Button></Col></Row>
      </div>
    );
  }
}
export default SentenceScreen;