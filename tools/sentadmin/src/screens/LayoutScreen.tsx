import React, { Component } from "react";
import DelPanel from "../comps/DelPanel";
import HistoryPanel from "../comps/HistoryPanel";
import PropBox from "../comps/PropBox";
import Sidebar from "../comps/Sidebar";
import SceneMain from "../scenes/SceneMain";
import ScenePrev from "../scenes/ScenePrev";

import { Card, Row, Col } from "react-bootstrap";
import { WordConfig } from "../dataObjs/WordConfig";
import MainStorage from "../classes/MainStorage";
import Controller from "../classes/Controller";
import FilePanel from "../comps/FilePanel";

interface MyProps { screenCallback: Function }
interface MyState { configObj: WordConfig, sideBarIndex: number }
class LayoutScreen extends Component<MyProps, MyState> {
  private ms: MainStorage = MainStorage.getInstance();
  private controller: Controller = Controller.getInstance();

  constructor(props: MyProps) {
    super(props);

    this.state = { configObj: this.ms.null_word, sideBarIndex: 0 };
  }
  componentDidMount() {

    this.controller.updateSideBar = this.updateSideBar.bind(this);
    this.controller.updateWord = this.updateWord.bind(this);
    //
    //
    //
    const config: any = {
      mode: Phaser.AUTO,
      width: 640,
      height: 480,
      parent: 'phaser-game',
      scene: [SceneMain,ScenePrev]
    }
    this.ms.gameConfig = config;

    new Phaser.Game(config);
  }
  updateWord(wordConfig: WordConfig) {

    this.setState({ configObj: wordConfig });
  }
  updateSideBar(index: number) {
    console.log("index=" + index);

    this.setState({ sideBarIndex: index });
  }
  render() {

    return (
      <Card>
        <Row>
          <Col sm="1">
            <Sidebar selectMode={this.state.sideBarIndex}></Sidebar>
          </Col>
          <Col sm="8">
            <div id="phaser-game">
            </div>
          </Col>
          <Col sm="3">
            <FilePanel screenCallback={this.props.screenCallback}></FilePanel>
            <PropBox wordConfig={this.state.configObj}></PropBox>
            <HistoryPanel></HistoryPanel>
            <DelPanel wordConfig={this.state.configObj}></DelPanel>

          </Col>
        </Row>
      </Card>
    );
  }
}
export default LayoutScreen;
