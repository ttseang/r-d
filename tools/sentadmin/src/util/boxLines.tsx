import { PosVo } from "../dataObjs/PosVo";
import { WordConfig } from "../dataObjs/WordConfig";
import IBaseScene from "../interfaces/IBaseScene";
import { AlignGrid } from "./alignGrid";

export class BoxLines {
    private scene: Phaser.Scene;
    private bscene: IBaseScene;
    private ch: number = 0;
    private cw: number = 0;
    private cd: number = 0;
    private graphics: Phaser.GameObjects.Graphics;
    private grid: AlignGrid;

    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.ch = bscene.ch;
        this.cw = bscene.cw;
        this.cd = bscene.cd;
        this.grid = bscene.getGrid();

        this.graphics = this.scene.add.graphics();

    }
    public clear() {
        this.graphics.clear();
        this.graphics.lineStyle(2, 0x2ecc71);
    }
    public drawLinesOnBoxes(configs: WordConfig[]) {
        this.graphics.clear();
        this.graphics.lineStyle(2, 0x2ecc71);

        for (let i: number = 0; i < configs.length; i++) {
            let config = configs[i];
            this.drawLinesOnBox(config);
        }
    }
    public drawLinesOnBox(config: WordConfig) {

        //convert grid pos to screen x and y coordinates
        let startPos: PosVo = this.grid.getRealBottom(config.xPos, config.yPos);


        let endPos: PosVo = startPos;

        switch (config.line) {
            case "h":
                endPos = this.grid.getRealBottom(config.xPos + config.len, config.yPos);
                this.drawLine(startPos, endPos);
                break;

            case "d":
                startPos = this.grid.getRealBottom(config.xPos - 3, config.yPos - 1);
                endPos = this.grid.getRealBottom(config.xPos + Math.floor(config.len / 2) - 1, config.yPos + Math.floor(config.len / 2) - 1);
                this.drawLine(startPos, endPos);
                break;

            case "vsep":
                endPos = this.grid.getRealBottom(config.xPos + config.len, config.yPos);
                this.drawLine(startPos, endPos);

                startPos = this.grid.getRealMiddleBotton(config.xPos, config.yPos);
                endPos = this.grid.getRealMiddleBotton(config.xPos, config.yPos - 1);
                // endPos.y-=this.ch;
                this.drawLine(startPos, endPos);
                break;

            case "tvsep":

                endPos = this.grid.getRealBottom(config.xPos + config.len, config.yPos);
                this.drawLine(startPos, endPos);

                startPos = this.grid.getRealMiddleBotton(config.xPos, config.yPos);
                endPos = this.grid.getRealMiddleBotton(config.xPos, config.yPos - 1);
                this.drawLine(startPos, endPos, 0, -this.ch, 0, this.ch * 2);


                break;

            case "c":

                let start2: number = config.yPos + config.len / 2;

                //
                //
                startPos = this.grid.getRealXY(config.xPos, start2);
                endPos = this.grid.getRealXY(config.xPos + 4, start2 + config.len / 2);
                //  endPos=this.grid.getRealBottom(config.xPos+4,config.yPos+Math.floor(config.len/2));
                this.drawLine(startPos, endPos);
                //
                //
                endPos = this.grid.getRealXY(config.xPos + 4, start2 - config.len / 2);
                this.drawLine(startPos, endPos);

                break;

            case "cend":
                let start3: number = config.yPos + config.len / 2;

                //
                //
                startPos = this.grid.getRealXY(config.xPos + 4, start3);
                endPos = this.grid.getRealXY(config.xPos, start3 + config.len / 2);
                //  endPos=this.grid.getRealBottom(config.xPos+4,config.yPos+Math.floor(config.len/2));
                this.drawLine(startPos, endPos);
                //
                //
                endPos = this.grid.getRealXY(config.xPos, start3 - config.len / 2);
                this.drawLine(startPos, endPos);

                break;

        }
    }
    drawLine(startPos: PosVo, endPos: PosVo, offsetX1: number = 0, offsetY1: number = 0, offsetX2: number = 0, offsetY2: number = 0) {
        this.graphics.moveTo(startPos.x + offsetX1, startPos.y + offsetY1);
        this.graphics.lineTo(endPos.x + offsetX1, endPos.y + offsetY2);
        this.graphics.strokePath();
    }

    static drawOnBox(g: Phaser.GameObjects.Graphics, back: Phaser.GameObjects.Sprite, config: WordConfig, bscene: IBaseScene) {

        let endX: number = 0;
        let endY: number = 0;
        let startX: number = 0;
        let startY: number = 0

        switch (config.line) {
            case "h":
                startY = back.displayHeight;
                endY = startY;
                endX = back.displayWidth;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                g.strokePath();
                break;

            case "d":
                startY = back.displayHeight;
                endY = startY;
                endX = back.displayWidth;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                g.strokePath();
                break;


            case "c":
                startY = back.displayHeight / 2;
                endX = back.displayWidth;
                endY = startY + back.displayHeight / 2;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                //
                //
                startY = back.displayHeight / 2;
                endX = back.displayWidth;
                endY = startY - back.displayHeight / 2;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                //
                //
                g.strokePath();


                break;

            case "cend":
                startY = back.displayHeight / 2;
                startX = back.displayWidth;
                endX = 0;
                endY = startY + back.displayHeight / 2;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                //
                //
                startY = back.displayHeight / 2;
                endX = 0;
                endY = startY - back.displayHeight / 2;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                //
                //
                g.strokePath();
                //
                //

                break;

            case "tvsep":
                startY = back.displayHeight / 2;
                startX = back.displayWidth / 2;
                endX = startX;
                endY = startY + bscene.ch * 2;

                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                g.strokePath();

                //
                //
                //
                startY = back.displayHeight;
                startX = 0;
                endX = back.displayWidth;
                endY = startY;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                g.strokePath();

                break;

            case "vsep":
                startY = back.displayHeight / 2;
                startX = back.displayWidth / 2;
                endX = startX;
                endY = startY + bscene.ch;

                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                g.strokePath();

                startY = back.displayHeight;
                startX = 0;
                endX = back.displayWidth;
                endY = startY;
                g.moveTo(startX, startY);
                g.lineTo(endX, endY);
                g.strokePath();
                break;
        }

    }

}