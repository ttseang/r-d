import { SquareVo } from "../dataObjs/SquareVo";
import { IGameObj } from "../interfaces/IGameObj";


export class BoxUtil {
  static findCorners(boxes: IGameObj[]): SquareVo {
    let x1 = 10000;
    let x2 = 0;
    let y1 = 10000;
    let y2 = 0;

    for (let i = 0; i < boxes.length; i++) {
      let box: IGameObj = boxes[i];
      let right = box.x + box.displayWidth;
      let bottom = box.y + box.displayHeight;

      if (box.x < x1) {
        x1 = box.x;
      }
      if (right > x2) {
        x2 = right;
      }
      if (box.y < y1) {
        y1 = box.y;
      }
      if (bottom > y2) {
        y2 = bottom;
      }
    }
    return new SquareVo(x1, y1, x2, y2);
  }
  
  /* static findObjectCorners(obj)
    {
        return this.findEdges
    } */
  static findEdges(startX: number,startY: number,endX: number, endY: number): SquareVo {
    let x1 = 10000;
    let x2 = 0;
    let y1 = 10000;
    let y2 = 0;

    if (startX < x1) {
      x1 = startX;
    }
    if (startX > x2) {
      x2 = startX;
    }
    if (endX < x1) {
      x1 = endX;
    }
    if (endX > x2) {
      x2 = endX;
    }

    if (startY < y1) {
      y1 = startY;
    }
    if (startY > y2) {
      y2 = startY;
    }
    if (endY < y1) {
      y1 = endY;
    }
    if (endY > y2) {
      y2 = endY;
    }

   
    return BoxUtil.findCorners([{x:startX,y:startY,displayWidth:0,displayHeight:0,scaleX:1,scaleY:1},{x:endX,y:endY,displayWidth:0,displayHeight:0,scaleX:1,scaleY:1}]);
  }
}
