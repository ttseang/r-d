import { WordConfig } from "../dataObjs/WordConfig";


export class HistoryObj {
    public future:WordConfig[][]=[] || undefined;
    public historyArray:WordConfig[][]=[] || undefined;
    private inital:WordConfig[]=[] || undefined;
    public locked:boolean=false;

    constructor() {
        //
        //
        //        
       
        this.historyArray = [];
        // this.historyIndex=0;
       
    }
   public addHistory(snap:WordConfig[]) {
       if (this.locked===true)
       {
           return;
       }
        if (this.inital.length===0) {            
            this.inital = snap;
            return;
        }        
        this.historyArray.push(snap);
        this.future = [];

    }
   public undo() {      
       if (this.locked===true) 
       {
           return undefined;
       }
       this.locked=true;
       let snap:WordConfig[] | undefined=this.historyArray.pop();
        if (snap)
        {
            this.future.push(snap);
        }       

       let current = this.historyArray[this.historyArray.length - 1];
       console.log(current);
       if (current===undefined)
       {
           return this.inital;
       }
       console.log(this);
       return current;
       
    }
    public redo() {
        if (this.locked===true) 
        {
            return undefined;
        }
        let snap:WordConfig[] | undefined = this.future.pop();
        if (snap) {
            this.historyArray.push(snap);
        }       
        let current = this.historyArray[this.historyArray.length - 1];
        return current;
    }
}
export default HistoryObj;