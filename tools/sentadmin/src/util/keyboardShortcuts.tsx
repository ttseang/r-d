import Controller from "../classes/Controller";
import { IBaseScene } from "../interfaces/IBaseScene";
import DC from "../ui/DC";

export class KeyboardShortcuts
{
    private scene:Phaser.Scene;
    private keylock:boolean;
    private saveFlag:boolean=false;
    private dc:DC;
    private controller:Controller=Controller.getInstance();

    constructor(bscene:IBaseScene,diagramCanvas:DC)
    {
        this.scene=bscene.getScene();
        this.dc=diagramCanvas;

        this.scene.input.keyboard.on('keydown',this.getKey.bind(this));
        this.scene.input.keyboard.on('keyup',this.keyUp.bind(this));
        this.keylock=false;
    }
    getKey(e:KeyboardEvent)
    {
        if (this.keylock===true)
        {
            return;
        }
       // console.log(e);
        if (e.key==="Delete")
        {
            this.dc.deleteBox();
        }
        if (e.key==="ArrowRight")
        {
            this.dc.nudge(1.01,0);
            this.saveFlag=true;
        }
        if (e.key==="ArrowLeft")
        {
            this.dc.nudge(-1,0);
            this.saveFlag=true;
        }
        if (e.key==="ArrowUp")
        {
            this.dc.nudge(0,-1);
            this.saveFlag=true;
        }
        if (e.key==="ArrowDown")
        {
            this.dc.nudge(0,1);
            this.saveFlag=true;
        }
        if (e.ctrlKey===true)
        {
           
            if (e.key==="z")
            {
                this.keylock=true;
                this.dc.undo();
            }
            if (e.key==="y")
            {
                this.keylock=true;
                this.dc.redo();
            }
        }
    }
    keyUp()
    {        
        this.keylock=false;
        if (this.saveFlag===true)
        {
            this.controller.saveHistory();
        }
    }
}