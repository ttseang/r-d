import React, { Component } from 'react';
import FormulaScreen from './FormulaScreen';
import { Card } from 'react-bootstrap';
interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { mode: 0 };
        }
    getScreen()
    {
        switch (this.state.mode) {
            case 0:
                return <FormulaScreen></FormulaScreen>
            default:
                return "error";
        }
    }
    render() {
        return (<div>
            <Card>
                <Card.Header>
                    Title of the App
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;