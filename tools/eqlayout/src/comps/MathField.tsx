import * as React from "react";

import { useEffect, useMemo } from "react";
import { MathfieldElement, MathfieldOptions } from "mathlive";

export type MathfieldProps = {
  options?: Partial<MathfieldOptions>;

  value: string;
  onChange: (latex: string) => void;

  className?: string;
};

export const Mathfield = () => {
  const mathfieldRef = useRef<MathfieldElement>(null);

  useEffect(() => {
    // mathfieldRef.current.<option> = <value>;
  }, []);

  return (
    <math-field ref={mathfieldRef}/>
  );
};
export default Mathfield;