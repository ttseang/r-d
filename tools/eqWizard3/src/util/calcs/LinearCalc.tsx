import { Controller } from "../../classes/Controller";
import MainStorage from "../../classes/MainStorage";
import { CellVo } from "../../dataObjs/CellVo";
import { ChatVo } from "../../dataObjs/ChatVo";
import { LinearCellOutputVo } from "../../dataObjs/outputs/LinearCellOutputVo";
import { ApiConnect } from "../ApiConnect";
import { GridUtil } from "../GridUtil";

export class LinerCalc
{
    private messages:ChatVo[]=[];
    private callback:Function=()=>{};
   // private controller:Controller=Controller.getInstance();
   
    private ms:MainStorage=MainStorage.getInstance();
    constructor()
    {
        (window as any).lcalc=this;
        /* this.controller.aiTweak=this.tweakCalc.bind(this);
        this.controller.redoAi=this.redoCalc.bind(this); */
    }
    public calcLinear(expression:string,callback:Function)
    {
        this.ms.expression=expression;
        this.callback=callback;
        let instructions:string='Show me every possible detailed steps for '+expression+'; Make the content suitable for beginners. Make as many steps as possible. Put the steps into this JSON stuct {"steps":[{"instruction":.,"expression":"..}]}. I want you to only reply with the JSON  and nothing else. ';
        //Show me every possible detailed steps for 2(x + 3) = 10; Make the content suitable for beginners.
        
        this.messages.push(new ChatVo("system","You are a helpful math teacher. You explain things in great detail. You do not like to leave out steps. Write as if talking to a student who has never learned the topic."));
        this.messages.push(new ChatVo("user",instructions));
        this.ms.messages=this.messages;
        const messageString:string=JSON.stringify(this.messages);

        //console.log(instructions);

        const apiConnect:ApiConnect=new ApiConnect();
        apiConnect.sendTT(messageString,this.gotCalc.bind(this),()=>{});
               
    }
    redoCalc()
    {
        //console.log("redoCalc");
        this.messages=[];
        this.calcLinear(this.ms.expression,this.callback);
    }
    public tweakCalc(message:string,callback:Function)
    {
        this.callback=callback;
        let messages:ChatVo[]=this.ms.messages;
        let chatVo:ChatVo=new ChatVo("user",message);
        messages.push(chatVo);
        //console.log(messages);
        const messageString:string=JSON.stringify(messages);
        const apiConnect:ApiConnect=new ApiConnect();
        apiConnect.sendTT(messageString,this.gotCalc.bind(this),()=>{});
    }
    gotCalc(messageObject:any)
    {
       // //console.log(messageObject);
        let choice: any = messageObject.choices[0];
        if (choice) {
            let message: any = choice.message;
            //console.log(message);
            if (message==undefined)
            {
                alert("I am sorry. The ai misunderstood. I will try again.");
                this.redoCalc();
                return;
            }
            if (message.content.indexOf("sorry")>-1)
            {
                alert("I am sorry. The ai misunderstood. I will try again.");
                this.redoCalc();
                return;
            }
            this.messages.push(new ChatVo(message.role,message.content));
            //console.log(this.messages);
            const content:string=message.content;
            //console.log(content);
            let json: any = JSON.parse(content);
            
            //console.log(json);
            let steps:any=json.steps;
            
            let allCells:CellVo[][]=[];
            let linearData:LinearCellOutputVo[]=[];

            for (let i:number=0;i<steps.length;i++)
            {
                //console.log(steps[i].expression);
                let expression:string=steps[i].expression;
                //remove all spaces
                expression=expression.replace(/\s/g, '');
                //turn the expression to cellVo
                let cells:CellVo[]=GridUtil.stringToCells(expression);
                linearData.push(new LinearCellOutputVo(steps[i].instruction,cells));
                
            }

           
           /*  if (message) {
                //console.log(message);
                let json: any = JSON.parse(message);
                //console.log(json);
            } */
           // return new LinearCellOutputVo(allInstructions,allCells);
           this.callback(linearData);
        }
    }
}