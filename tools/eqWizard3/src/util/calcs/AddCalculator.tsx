import { CellGridOutputVo } from "../../dataObjs/outputs/CellGridOutputVo";
import { CellVo } from "../../dataObjs/CellVo";
import { NumVo } from "../../dataObjs/NumVo";
import { GridUtil } from "../GridUtil";
import { PlacesUtil } from "../PlacesUtil";
import MainStorage from "../../classes/MainStorage";

export class AddCalculator {

    private totalSteps: number = 0;
    private numsObj: NumVo[] = [];
    private carryArray: number[][] = [];
    private sumArray: number[] = [];
    private sumString: string = "";
    private steps: CellGridOutputVo[] = [];
    private maxDec: number = 0;
    private maxWhole: number = 0;
    private ms: MainStorage = MainStorage.getInstance();

    constructor() {

        this.carryArray[0] = [];
        this.carryArray[0].push(-1);

        (window as any).addcalc = this;
    }
    public doAdd(expression: string) {

        let nums: string[] = expression.split("+");

        this.numsObj = PlacesUtil.makeNumberObjs(nums);

        this.maxWhole = PlacesUtil.findMaxWhole(nums);
        this.maxDec = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            this.numsObj[i].formatDigits(this.maxWhole, this.maxDec);
        }

        /**
         * Initial
         */


        this.totalSteps = 0;

        for (let i: number = 0; i < this.numsObj.length; i++) {
            if (this.ms.lineUpDec === true) {
                this.numsObj[i].fixDigits(this.maxWhole, this.maxDec);
            }

            if (this.numsObj[i].allDigits.length > this.totalSteps) {
                this.totalSteps = this.numsObj[i].allDigits.length + 1;
            }
        }

        this.addStep();


        let cols: number[][] = [];

        for (let i: number = 0; i < this.numsObj.length; i++) {
            let digits: number[] = this.numsObj[i].allDigits.slice();


            for (let j: number = 0; j < digits.length; j++) {

                if (!cols[j]) {
                    cols[j] = [];
                }
                cols[j].push(digits[j]);
            }
        }

        cols = cols.reverse();

        let carryOver: number = 0;


        this.sumArray = [];

        for (let i: number = 0; i < cols.length; i++) {

            let col: number[] = cols[i];

            let sum: number = col.reduce((partialSum, a) => partialSum + a, 0);



            let c: number = Math.floor(sum / 10);
            let r: number = sum - (c * 10);

            if (c === 0) {
                this.carryArray[0].unshift(-1);
            }
            else {
                this.carryArray[0].unshift(c);
            }



            this.sumArray.unshift(r + carryOver);

            carryOver = c;
            this.addStep();
        }
        if (carryOver > 0) {
            this.sumArray.unshift(carryOver);
        }



        this.addStep();

        return this.steps;
    }
    addStep() {
        this.steps.push(new CellGridOutputVo(this.getTop(), this.getBody(), this.getAnswer(), "+"));
    }
    getAnswer() {
        let sum: number = parseInt(this.sumArray.join(""));
        sum = sum / (Math.pow(10, this.maxDec));

        if (isNaN(sum)) {
            return [new CellVo("@", [])];
        }
        this.sumString = sum.toString();
        let sums: string[] = this.sumString.split("");

        let answer: CellVo[] = [];
        for (let i: number = 0; i < sums.length; i++) {
            answer.push(new CellVo(sums[i].toString(), []));
        }
        return answer;
    }
    getTop() {
        let top: CellVo[][] = [];

        for (let i: number = 0; i < this.carryArray[0].length; i++) {
            let val: number = this.carryArray[0][i];
            let char: string = val.toString();

            if (val === -1) {
                char = "@";
            }
            if (top.length === 0) {
                top[0] = [];
            }
            top[0].push(new CellVo(char, []));
        }
        return top;
    }
    fixBody(body: CellVo[][]) {
        let maxCols: number = 0;
        for (let i: number = 0; i < this.numsObj.length; i++) {
            let len: number = this.numsObj[i].allChars.length
            if (len > maxCols) {
                maxCols = len;
            }
        }
        for (let i: number = 0; i < body.length; i++) {
            let bodyCells: CellVo[] = body[i];
            while (bodyCells.length < maxCols) {
                bodyCells.unshift(new CellVo("@", []));
            }
            //if the last row then add the operator
            if (i === body.length - 1) {
                bodyCells.unshift(new CellVo("+", []));
            }
        }
        return body;
    }
    getBody() {
        let body: CellVo[][] = [];

        for (let i: number = 0; i < this.numsObj.length; i++) {
            let digits: string[] = this.numsObj[i].allChars.slice();


            if (this.numsObj[i].isCurrency === true) {
                digits.unshift("$");
            }

            let bodyCells: CellVo[] = GridUtil.arrayToCells2(digits, []);

            body.push(bodyCells);
        }

        body = this.fixBody(body);
        return body;
    }
}