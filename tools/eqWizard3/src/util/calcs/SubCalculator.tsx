import { CellGridOutputVo } from "../../dataObjs/outputs/CellGridOutputVo";
import { CellVo } from "../../dataObjs/CellVo";
import { NumVo } from "../../dataObjs/NumVo";
import { GridUtil } from "../GridUtil";
import { PlacesUtil } from "../PlacesUtil";
import MainStorage from "../../classes/MainStorage";
import { Unicodes } from "../../classes/Constants";

export class SubCalculator {
    private digits1: number[] = [];
    private digits2: number[] = [];
    private body1: string[] = [];
    private body2: string[] = [];

    private topArray: number[][] = [];

    private borrowArray: number[] = [];
    private strikes: string[] = [];
    private results: number[] = [];
    private steps: CellGridOutputVo[] = [];
    private maxDec: number = 0;
    private maxWhole: number = 0;
    private ms: MainStorage = MainStorage.getInstance();
    private numObjects: NumVo[] = [];
    
    constructor() {
        (window as any).scalc = this;
    }
    public doSub(expression: string) {



        this.borrowArray = [-1];

        let nums: string[] = expression.split("-");

        let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);
        this.numObjects = numsObj;

        this.maxWhole = PlacesUtil.findMaxWhole(nums);
        this.maxDec = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            numsObj[i].formatDigits(this.maxWhole, this.maxDec);
        }


        if (this.ms.lineUpDec === true) {
            numsObj[0].fixDigits(this.maxWhole, this.maxDec);
            numsObj[1].fixDigits(this.maxWhole, this.maxDec);
        }

        this.digits1 = numsObj[0].allDigits.slice().reverse();
        this.digits2 = numsObj[1].allDigits.slice().reverse();

        this.body1 = numsObj[0].allChars.slice();
        this.body2 = numsObj[1].allChars.slice();

        let maxLen: number = Math.max(this.digits1.length, this.digits2.length);
       
        this.topArray[0] = [];
        this.topArray[1] = [];
        this.topArray[2] = [];

        for (let i: number = 0; i < maxLen; i++) {
            this.topArray[0].push(-1);
            this.topArray[1].push(-1);
            this.topArray[2].push(-1);
        }

       
        /**
         * Initial
         */

        let body1:string[]=this.body1.slice();
        let body2:string[]=this.body2.slice();
        if (this.numObjects[0].isCurrency === true) {
            body1.unshift("$");
        }

        if (this.numObjects[1].isCurrency === true) {
            body2.unshift("$");
        }

        let row1: CellVo[] = GridUtil.arrayToCells2(body1, []);
        let row2: CellVo[] = GridUtil.arrayToCells2(body2, []);

        while (row2.length<row1.length){
            row2.unshift(new CellVo("@",[]));
        }
       row2.unshift(new CellVo(Unicodes["-"],[]));

        this.steps.push(new CellGridOutputVo([], [row1, row2], [], "-"));

        let totalSteps: number = Math.max(this.digits1.length, this.digits2.length);
        

        this.results = [];

        this.strikes = ",".repeat(totalSteps).split(",");
        this.strikes.pop();



        for (let i: number = 0; i < totalSteps; i++) {


            let num1: number = this.digits1[i];
            let num2: number = this.digits2[i];


            let num3: number = this.digits1[i + 1];
            let num5: number = num3 - 1;

            if (num1 === undefined) {
                num1 = 0;
            }
            if (num2 === undefined) {
                num2 = 0;
            }
            if (num5 === -1) {
                num5 = 9;
                this.addToTop(i + 1, 10);

                this.addToTop(i + 1, 9);
            }

            if (num1 < num2) {
                //borrow because " + num1 + " is less than " + num2 + " and add " + num5 + " to the top"

                this.borrowFromNext(i);
                this.addStep();

                //in case changed
                num1 = this.digits1[i];
                // num1 += 10;
                let result: number = num1 - num2;
               
                this.results.unshift(result);

            }
            else {
                //"don't borrow because " + num1 + " is greater than " + num2
                // or don't borrow because " + num1 + " is the same as " + num2;
                this.borrowArray.unshift(-1);

                let result: number = num1 - num2;
                this.results.unshift(result);

            }

            this.addStep();

        }

        return this.steps;
    }

    private addStep() {
        let top: CellVo[][] = [];
        top[0] = GridUtil.arrayToCells(this.topArray[0].slice().reverse(), []);
        top[1] = GridUtil.arrayToCells(this.topArray[1].slice().reverse(), []);

        let body1: string[] = this.body1.slice();
        let body2: string[] = this.body2.slice();

        if (this.numObjects[0].isCurrency === true) {
            body1.unshift("$");
        }

        if (this.numObjects[1].isCurrency === true) {
            body2.unshift("$");
        }


        let bodyCells1: CellVo[] = GridUtil.arrayToCells2(body1, this.strikes.slice().reverse());
        let bodyCells2: CellVo[] = GridUtil.arrayToCells2(body2, []);

        let maxCols: number = Math.max(body1.length, body2.length);


        while (bodyCells1.length < maxCols) {
            bodyCells1.unshift(new CellVo("@", []));
        }
        while (bodyCells2.length < maxCols) {
            bodyCells2.unshift(new CellVo("@", []));
        }


        let total: number = parseInt(this.results.join(""));
        total = total / (Math.pow(10, this.maxDec));
        let totalObj: NumVo = new NumVo(total.toString());
        totalObj.formatDigits(this.maxWhole, this.maxDec);
       

        let answers: CellVo[] = GridUtil.arrayToCells2(totalObj.allChars, []);

        if (isNaN(total)) {
            answers = [new CellVo("@", [])];
        }

        let gridOutput: CellGridOutputVo = new CellGridOutputVo(top, [bodyCells1, bodyCells2], answers, "-");
        

        this.steps.push(gridOutput);
    }
    private addToTop(col: number, val: number) {
       
        let row: number = 0;
        while (this.topArray[row][col] !== -1 && row < 3) {
            row++;
        }
        if (row < 3) {
            this.topArray[row][col] = val;
        }

    }
    private borrowFromNext(index: number) {
        this.digits1[index + 1]--;
        this.digits1[index] += 10;
        this.strikes[index + 1] = "strike3";
        this.addStep();

        this.borrowArray.unshift(this.digits1[index + 1]);
        this.addToTop(index + 1, this.digits1[index + 1]);
        this.addToTop(index, this.digits1[index]);
        this.strikes[index] = "strike3";

        if (this.digits1[index + 1] === -1) {
            this.borrowFromNext(index + 1);
        }
        
    }
}