import { CellVo } from "../../dataObjs/CellVo";
import { NumVo } from "../../dataObjs/NumVo";
import { PlacesUtil } from "../PlacesUtil";
import { RepeatChecker } from "../RepeatChecker";
import { GridUtil } from "../GridUtil";
import { DCellOutput } from "../../dataObjs/outputs/DCellOutput";
import MainStorage from "../../classes/MainStorage";
import { NumUtil } from "../NumUtil";

export class DivCalculator {

    private numsObj: NumVo[] = [];

    private maxDec: number = 0;
    private maxWhole: number = 0;

    private ms: MainStorage = MainStorage.getInstance();

    private lastDivs: number = 0;
    private digits1: number[] = [];


    private zeroLimit: number = 2;
    private useRemain: boolean = false;


    private productLine: string[] = [];

    private quotient: CellVo[] = [];

    private qs: number[] = [];
    private divisor: number = 0;
    private dividend: number = 0;
    private remain: number = 0;

    public steps: DCellOutput[] = [];


    constructor() {
        (window as any).dcalc = this;
    }

    doDiv(expression: string, useRemain: boolean = false, zeroLimit: number = 2) {

        this.qs = [];

        let qString: string = "";

        let nums: string[] = expression.split("/");
        this.numsObj = PlacesUtil.makeNumberObjs(nums);

        let div1: string = NumUtil.takeOutStrings(nums[0]);
        this.dividend = parseFloat(div1);

        this.zeroLimit = zeroLimit;

        const answer: number = this.dividend / this.numsObj[1].val;
        const decPlace: number = Math.floor(answer).toString().length;
      //  console.log("decPlace=" + decPlace);
     //   console.log("answer=" + answer);


        let dmax: number = PlacesUtil.findMaxDecByObj(this.numsObj);
        let wmax: number = PlacesUtil.findMaxWholeByObj(this.numsObj);

        this.maxDec = dmax;
        this.maxWhole = wmax;
        if (this.ms.lineUpDec === true) {
            this.numsObj[0].fixDigits(wmax, dmax);
            this.numsObj[1].fixDigits(wmax, dmax);
        }

        this.digits1 = this.numsObj[0].allDigits.reverse().slice();

        let divisor: number = this.numsObj[1].val;
        this.divisor = divisor;



        let divs: number = 0;
        let divDigits: number[] = [];

        let spaces: number = 0;
        let rowID: number = 99;

        let addZeroCount: number = 0;
        let lastProduct: number = -1;

        this.addStep();
        let decAdded: boolean = false;
      

        while (this.digits1.length > 0) {

            rowID++;
            let safe: number = 0;

            while (divs < divisor && safe < 10) {
                divDigits.push(this.digits1.pop() || 0);
                divs = parseFloat(divDigits.join(""));
                safe++;
            }


            let divNum: NumVo = new NumVo(divs.toString());
            divNum.row = rowID;

            // spaces++;



            //q for quanity(quotient?) - or the number that will be muliplied
            //by the divsor
            let q: number = Math.floor(divs / divisor);
            qString = qString + q.toString();
           // console.log("q=" + q);
          //  console.log(qString);
            // console.log("q2="+q2);

           // this.qs.push(q);
            /* console.log("qs="+this.qs);
            console.log("decPlace="+decPlace); */
            //insert the decimal place into this.quitent at decPlace

            //this.quotient = GridUtil.arrayToCells(this.qs, []);
            this.quotient = GridUtil.arrayToCells2(qString.split(""), []);



            this.quotient.unshift(new CellVo("@", []));

            //the difference between the length of the dividend
            //and the length of the divsior
            //let diff:number=divs.toString().length-divisor.toString().length;



            if (this.lastDivs !== 0) {
                this.productLine.pop();
                //  spaces--;

            }

            lastProduct = divs;
            this.productLine.push(divs.toString());
            this.addStep();

            let a2: number = q * divisor;
            this.productLine.push(a2.toString());
            this.addStep();

            let a2Obj: NumVo = new NumVo(a2.toString());
            a2Obj.row = -10 * rowID;


            if (spaces < 0) {
                return [];
            }

            this.addStep();

            // spaces -= bumpDif;


            divs -= a2;
            divDigits = [divs];

            this.lastDivs = divs;


            if (useRemain === false) {

                spaces++;
                let divNum: NumVo = new NumVo(lastProduct.toString());
                let remainNum: NumVo = new NumVo(divs.toString());

                let pdmax: number = PlacesUtil.findMaxDecByObj([divNum, remainNum]);
                let pwmax: number = PlacesUtil.findMaxWholeByObj([divNum, remainNum]);

                let remainString: string = remainNum.formatGridDigits(pwmax, pdmax);
                //  this.productLine.push("@".repeat(spaces) + remainString);
                this.productLine.push(remainString);
                this.addStep();
                console.log("step=" + this.steps.length);
                
             
                console.log("-----");
                if (divs !== 0) {
                    

                    if (addZeroCount < this.zeroLimit) {

                        this.numsObj[0].addDecPlace();
                        this.digits1.unshift(0);
                      
                        addZeroCount++;
                    }
                    else {
                        let rc: RepeatChecker = new RepeatChecker();
                        rc.fractionToDecimal(this.numsObj[0].val, this.numsObj[1].val);
                    }
                   if (spaces===decPlace)
                   {
                          qString+=".";
                   }
                }
              

            }
            else {
                console.log("no remainders");

                this.remain = divs;
                if (divs > 0) {


                    let divNum: NumVo = new NumVo(lastProduct.toString());
                    let remainNum: NumVo = new NumVo(divs.toString());

                    let pdmax: number = PlacesUtil.findMaxDecByObj([divNum, remainNum]);
                    let pwmax: number = PlacesUtil.findMaxWholeByObj([divNum, remainNum]);

                    let remainString: string = remainNum.formatGridDigits(pwmax, pdmax);
                    this.productLine.push(remainString);


                    //  this.productLine.push("@".repeat(spaces) + remainString);
                    this.addStep();
                }
            }
        }
        return this.steps;
    }
   
    addStep() {
        ////console.log(this.productLine);
        let products: CellVo[][] = [];
        let pline: string[] | undefined = this.productLine.slice();
        pline.shift();

        console.log("divisor="+this.divisor);
        console.log("dividend="+this.dividend);

        //get a substring of the dividend that is the same length as the divisor
        let divString: string = this.dividend.toString().substring(0, this.divisor.toString().length);
        console.log("divString="+divString);

        let indent: number = 0;
        let indentTweak:number=0;

        //if the value of the dividend is less than the divisor
        //then we need to indent the first line of the product by one more space
        if (parseInt(divString) < this.divisor) {
            indent = 1;
            indentTweak=1;
        }


        for (let i: number = 0; i < pline.length; i++) {
            let product: CellVo[];
            let plineLen: number = pline[i].length - 1;
            if (i / 2 === Math.floor(i / 2)) {
                pline[i] = "-" + pline[i];

                let aboveLine: string = pline[i - 1];
                let diff: number = 0;
                if (aboveLine) {
                  //  console.log("aboveLine="+aboveLine);
                 //   console.log("pline[i]="+pline[i]);

                    aboveLine=aboveLine.replace("@","");
                    aboveLine=aboveLine.replace("|","");

                    if (aboveLine.length > pline[i].length) {
                        diff = aboveLine.length - pline[i].length;
                      //  console.log("diff="+diff);
                    }
                }
                
                if (indent > 0) {
                    pline[i] = "@".repeat(indent + diff - 1) + pline[i];
                }
                product = GridUtil.arrayToCells2(pline[i].split(""), 'bb,'.repeat(10).split(","));
            }
            else {

                pline[i] = "|" + pline[i];

                pline[i] = "@".repeat(indent) + pline[i];


                product = GridUtil.arrayToCells2(pline[i].split(""), []);
                indent += plineLen-indentTweak;
            }


            // product=GridUtil.arrayToCells2(pline[i].split(""), []);
            products.push(product);
        }

        let divisorString: string = this.divisor.toString();
        if (this.numsObj[1].isCurrency) {
            divisorString = "$" + divisorString;
        }
        let divsorCells: CellVo[] = GridUtil.arrayToCells2(divisorString.split(""), []);

        let dividendString: string = "|" + this.dividend.toString();
        if (this.numsObj[0].isCurrency) {
            dividendString = "$" + dividendString;
        }

        let dividendCells: CellVo[] = GridUtil.arrayToCells2(dividendString.split(""), []);

        let remainCells: CellVo[] = GridUtil.arrayToCells2(this.remain.toString().split(""), []);

        //if the last productLine is 0
        if (this.productLine[this.productLine.length-1]==="0")
        {
            this.productLine.pop();
            indent--;
        }
        this.steps.push(new DCellOutput(this.quotient, divsorCells, dividendCells, products, remainCells));
    }
}
export default DivCalculator;