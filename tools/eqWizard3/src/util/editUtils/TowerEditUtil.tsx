import { TowerPosition, RowPos, ColPos } from "../../classes/Constants";
import MainStorage from "../../classes/MainStorage";
import { CellVo } from "../../dataObjs/CellVo";
import { CellGridOutputVo } from "../../dataObjs/outputs/CellGridOutputVo";


export class TowerEditUtil
{
    public static addRow(part: TowerPosition, pos: RowPos, tempCells: CellGridOutputVo) {


        switch (part) {
            case TowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                let len: number = 4;
                if (tempCells.rows.length > 0) {
                    len = tempCells.rows[0].length;
                }
                let cols: CellVo[] = [];
                for (let i: number = 0; i < len; i++) {
                    cols.push(new CellVo("@", []));
                }
                if (pos === RowPos.Bottom) {
                    dataRows.push(cols);

                }
                else {
                    dataRows.unshift(cols);

                }

                break;

            case TowerPosition.Top:
                let topRows: CellVo[][] = tempCells.top;
                let tlen: number = 4;
                if (tempCells.top.length > 0) {
                    tlen = tempCells.top[0].length;
                }
                let cols2: CellVo[] = [];
                for (let i: number = 0; i < tlen; i++) {
                    cols2.push(new CellVo("@", []));
                }
                if (pos === RowPos.Bottom) {
                    topRows.unshift(cols2);                    
                }
                else {
                    topRows.push(cols2);                   
                }

                break;
        }


        return tempCells;
    }
    public static subRow(part:TowerPosition,pos: RowPos,tempCells:CellGridOutputVo) {

        //console.log("part="+part);
        //console.log("pos="+pos);
        
        switch(part)
        {
            case TowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                //console.log(dataRows);
                if (pos === RowPos.Bottom) {
                    dataRows.pop();
                }
                else {
                    dataRows.shift();
                }
            break;

            case TowerPosition.Top:
                let topRows: CellVo[][] = tempCells.top;
                if (pos === RowPos.Bottom) {
                   
                    topRows.shift();
                }
                else {
                    topRows.pop();
                }

                break;
        }
       
       return tempCells;
    }
    public static addRowsToAllSteps(part: TowerPosition, pos: RowPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:CellGridOutputVo[]=ms.towerGrid2;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:CellGridOutputVo=towerRows[i];
            TowerEditUtil.addRow(part,pos,tempCells);
        }
    }
    public static addColsToAllSteps(part:TowerPosition,side:ColPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:CellGridOutputVo[]=ms.towerGrid2;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:CellGridOutputVo=towerRows[i];
            TowerEditUtil.addCol(part,side,tempCells);
        }
    }
    public static subColsToAllSteps(part:TowerPosition,side:ColPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:CellGridOutputVo[]=ms.towerGrid2;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:CellGridOutputVo=towerRows[i];
            TowerEditUtil.subColumn(part,side,tempCells);
        }
    }
    public static subRowsFromAllSteps(part: TowerPosition, pos: RowPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:CellGridOutputVo[]=ms.towerGrid2;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:CellGridOutputVo=towerRows[i];
            TowerEditUtil.subRow(part,pos,tempCells);
        }
        
    }
    public static addCol(part: TowerPosition, side: ColPos,tempCells:CellGridOutputVo)
    {
        switch (part) {
            case TowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].push(new CellVo("@", []));
                    }
                    else {
                        dataRows[i].unshift(new CellVo("@", []));
                    }

                }
                break;

            case TowerPosition.Top:
                let top: CellVo[][] = tempCells.top;

                for (let i: number = 0; i < top.length; i++) {
                    if (side === ColPos.Right) {
                        top[i].push(new CellVo("@", []));
                    }
                    else {
                        top[i].unshift(new CellVo("@", []));
                    }
                }
                break;

            case TowerPosition.Answer:
                let answer: CellVo[] = tempCells.answer;
                if (side === ColPos.Right) {
                    answer.push(new CellVo("@", []));
                }
                else {
                    answer.unshift(new CellVo("@", []));
                }
        }
        return tempCells;
    }
    public static subColumn(part: TowerPosition, side: ColPos,tempCells:CellGridOutputVo) {
        

        switch (part) {
            case TowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].pop();
                    }
                    else {
                        dataRows[i].shift();
                    }
                }
                break;

            case TowerPosition.Top:
                let top: CellVo[][] = tempCells.top;
                for (let i: number = 0; i < top.length; i++) {
                    if (side === ColPos.Right) {
                        top[i].pop();
                    }
                    else {
                        top[i].shift();
                    }
                }
                break;

            case TowerPosition.Answer:
                let answer: CellVo[] = tempCells.answer;
                if (side === ColPos.Right) {
                    answer.pop();
                }
                else {
                    answer.shift();
                }
        }
      return tempCells;
    }
}