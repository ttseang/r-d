import { MTowerPosition, RowPos, ColPos } from "../../classes/Constants";
import MainStorage from "../../classes/MainStorage";
import { CellVo } from "../../dataObjs/CellVo";
import { MCellGridOutputVo } from "../../dataObjs/outputs/MCellGridOutputVo";

export class MTowerEditUtil {
    public static subRow(part: MTowerPosition, pos: RowPos, tempCells: MCellGridOutputVo) {

        let dataRows: CellVo[][] = tempCells.rows;
        if (part === MTowerPosition.Product) {
            dataRows = tempCells.products;
        }
        if (pos === RowPos.Bottom) {
            dataRows.pop();
        }
        else {
            dataRows.shift();
        }
        return tempCells;
    }
    public static addRow(part: MTowerPosition, pos: RowPos, tempCells: MCellGridOutputVo) {

        let dataRows: CellVo[][] = tempCells.rows;
        if (part === MTowerPosition.Product) {
            dataRows = tempCells.products;
        }
        let len: number = 4;
        if (tempCells.rows.length > 0) {
            len = tempCells.rows[0].length;
        }
        let cols: CellVo[] = [];
        for (let i: number = 0; i < len; i++) {
            cols.push(new CellVo("@", []));
        }
        if (pos === RowPos.Bottom) {
            dataRows.push(cols);
        }
        else {
            dataRows.unshift(cols);
        }

        return tempCells;
    }
    public static addColumn(part: MTowerPosition, side: ColPos, tempCells: MCellGridOutputVo) {

        switch (part) {
            case MTowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].push(new CellVo("@", []));
                    }
                    else {
                        dataRows[i].unshift(new CellVo("@", []));
                    }

                }
                break;

            case MTowerPosition.Top:
                let top: CellVo[][] = tempCells.top;
                for (let i: number = 0; i < top.length; i++) {
                    if (side === ColPos.Right) {
                        top[i].push(new CellVo("@", []));
                    }
                    else {
                        top[i].unshift(new CellVo("@", []));
                    }
                }

                break;

            case MTowerPosition.Answer:
                let answer: CellVo[] = tempCells.answer;
                if (side === ColPos.Right) {
                    answer.push(new CellVo("@", []));
                }
                else {
                    answer.unshift(new CellVo("@", []));
                }
        }
        return tempCells;
    }
    public static subColumn(part: MTowerPosition, side: ColPos, tempCells: MCellGridOutputVo) {


        switch (part) {
            case MTowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].pop();
                    }
                    else {
                        dataRows[i].shift();
                    }
                }
                break;

            case MTowerPosition.Top:
                let top: CellVo[][] = tempCells.top;
                for (let i: number = 0; i < top.length; i++) {
                    if (side === ColPos.Right) {
                        top[i].pop();
                    }
                    else {
                        top[i].shift();
                    }
                }
                break;

            case MTowerPosition.Answer:
                let answer: CellVo[] = tempCells.answer;
                if (side === ColPos.Right) {
                    answer.pop();
                }
                else {
                    answer.shift();
                }
        }
        return tempCells;
    }
    public static addRowsToAllSteps(part: MTowerPosition, pos: RowPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:MCellGridOutputVo[]=ms.mTowerGrid;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:MCellGridOutputVo=towerRows[i];
            MTowerEditUtil.addRow(part,pos,tempCells);
        }
    }
    public static addColsToAllSteps(part:MTowerPosition,side:ColPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:MCellGridOutputVo[]=ms.mTowerGrid;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:MCellGridOutputVo=towerRows[i];
            MTowerEditUtil.addColumn(part,side,tempCells);
        }
    }
    public static subColsToAllSteps(part:MTowerPosition,side:ColPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:MCellGridOutputVo[]=ms.mTowerGrid;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:MCellGridOutputVo=towerRows[i];
            MTowerEditUtil.subColumn(part,side,tempCells);
        }
    }
    public static subRowsFromAllSteps(part: MTowerPosition, pos: RowPos)
    {
        let ms:MainStorage=MainStorage.getInstance();
        let towerRows:MCellGridOutputVo[]=ms.mTowerGrid;
        for (let i:number=0;i<towerRows.length;i++)
        {
            let tempCells:MCellGridOutputVo=towerRows[i];
            MTowerEditUtil.subRow(part,pos,tempCells);
        }
        
    }
}