import { ProblemTypes } from "../../classes/Constants";
import MainStorage from "../../classes/MainStorage";
import { CellGridOutputVo } from "../../dataObjs/outputs/CellGridOutputVo";
import { DCellOutput } from "../../dataObjs/outputs/DCellOutput";
import { FileVo } from "../../dataObjs/outputs/FileVo";
import { FractGridOutput } from "../../dataObjs/outputs/FractGridOutput";
import { LinearCellOutputVo } from "../../dataObjs/outputs/LinearCellOutputVo";
import { MCellGridOutputVo } from "../../dataObjs/outputs/MCellGridOutputVo";
import ApiConnect from "../ApiConnect";

export class EqOpen
{
    private ms:MainStorage=MainStorage.getInstance();
    private callback:Function;

   
    constructor(callback:Function)
    {
        this.callback=callback;
    }
    public openFile(fileVo:FileVo)
    {
        let api:ApiConnect=new ApiConnect();
        api.getFileContent(fileVo.lti,this.gotFile.bind(this));
        this.ms.LtiFileName=fileVo.lti;
    }
    private gotFile(data:any)
    {
        //console.log(data);

        let type:number=parseInt(data.type);
      //  //console.log("type="+type);

        let content:any=data.data;
        //console.log(content);
        this.ms.problemType=type;
        this.ms.expression=data.eqstring;

         switch(type)
        {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:


            let op:string=(type===ProblemTypes.ADDITION)?"+":"-";

            let cells:CellGridOutputVo[]=[];

            for (let i:number=0;i<content.length;i++)
            {
                let cellInfo=new CellGridOutputVo([],[],[],op);
                cellInfo.fromObj(content[i])
                cells.push(cellInfo);
            }
           
            this.ms.towerGrid2=cells;
          
            this.callback();
            break;

            case ProblemTypes.MULTIPLICATION:

            let mcells:MCellGridOutputVo[]=[];
            
            for (let i:number=0;i<content.length;i++)
            {
                let cellInfo=new MCellGridOutputVo([],[],[],[],[],"*");
                cellInfo.fromObj(content[i])
                mcells.push(cellInfo);
            }
            
            this.ms.mTowerGrid=mcells;
            this.callback();

            break;

            case ProblemTypes.DIVISION:

            let dcells:DCellOutput[]=[];
            
            for (let i:number=0;i<content.length;i++)
            {
                let cellInfo=new DCellOutput([],[],[],[],[]);
                cellInfo.fromObj(content[i])
                dcells.push(cellInfo);
            }
            this.ms.dcells=dcells;
            this.callback();
            break;


            case ProblemTypes.FRACTIONS:
            
            let fcells:FractGridOutput[]=[];
            for (let i:number=0;i<content.length;i++)
            {
                let cellInfo=new FractGridOutput([],[]);
                cellInfo.fromObj(content[i])
                fcells.push(cellInfo);
            }
            this.ms.fcells=fcells;
            this.callback();
            break;

            case ProblemTypes.LINEARVARS:
            let lcells:LinearCellOutputVo[]=[];
            for (let i:number=0;i<content.length;i++)
            {
                let cellInfo=new LinearCellOutputVo("",[]);
                cellInfo.fromObj(content[i])
                lcells.push(cellInfo);
            }
            this.ms.lcells=lcells;
            this.callback();
            break;
        }
    }
}