import { ProblemTypes } from "../classes/Constants";
import MainStorage from "../classes/MainStorage";
import { AddCalculator } from "./calcs/AddCalculator";
import DivCalculator from "./calcs/DivCalculator";
import { DivCalculator2 } from "./calcs/DivCalculator2";
import { FractCalculator } from "./calcs/FractCalculator";
import { LinerCalc } from "./calcs/LinearCalc";
import { MulCalculator } from "./calcs/MulCalculator";
import { SubCalculator } from "./calcs/SubCalculator";

export class EqEngine2
{
    private expression: string;
    public orignal: string;
    public problemType:ProblemTypes=ProblemTypes.ADDITION;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(expression:string)
    {
        this.expression = expression;
        this.orignal = expression;
        this.ms.expression=expression;
        this.problemType=this.checkType();
       // //console.log("Problem Type: "+this.problemType);
    }
    private checkType()
    {
        let add: boolean = this.expression.includes("+");
        let sub: boolean = this.expression.includes("-");
        let mul: boolean = this.expression.includes("*");
        let division: boolean = this.expression.includes("/");

        //check for variables which can be any single letter
        let re = new RegExp("[a-z]");
        let match = this.expression.match(re);
        //console.log("match: "+match);
        //console.log(this.expression);

        if (match !== null) {
            return ProblemTypes.LINEARVARS;
        }
        

        if (this.expression.includes("_")) {
            return ProblemTypes.FRACTIONS;
        }

        let c: number = ((add === true) ? 1 : 0) + ((sub === true) ? 1 : 0) + ((mul === true) ? 1 : 0) + ((division === true) ? 1 : 0);

        if (c !== 1) {
            return ProblemTypes.UNSUPORTED;
           // return "Not Supported";
        }
        if (add===true)
        {
            return ProblemTypes.ADDITION;
        }
        if (sub===true)
        {
            return ProblemTypes.SUBTRACTION;
        }
        if (mul===true)
        {
            return ProblemTypes.MULTIPLICATION;
        }
        if (division===true)
        {
            return ProblemTypes.DIVISION;
        }

        return ProblemTypes.UNSUPORTED;
    }
    public getStepsTower()
    {
               
        if (this.problemType === ProblemTypes.ADDITION) {
            let addcalc:AddCalculator=new AddCalculator();
            return addcalc.doAdd(this.expression);
        }
        if (this.problemType===ProblemTypes.SUBTRACTION)
        {
            let subcal:SubCalculator=new SubCalculator();
            return subcal.doSub(this.expression);
        }
       
        return null;
    }
    public getStepsMTower()
    {
        if (this.problemType===ProblemTypes.MULTIPLICATION)
        {
            let mcal:MulCalculator=new MulCalculator();
            return mcal.doStepMul(this.expression);
        }
        return null;
    }
    public getStepsD()
    {
        if (this.problemType===ProblemTypes.DIVISION)
        {
            let dCal:DivCalculator=new DivCalculator();
            
            return dCal.doDiv(this.expression,this.ms.useRemainder,this.ms.zeroLimit);
        }
        return null;
    }
    public getFractSteps()
    {
        if (this.problemType===ProblemTypes.FRACTIONS)
        {
            let fCal:FractCalculator=new FractCalculator();
           return  fCal.calcFraction(this.expression);
        }
        return null;
    }
    public getLinearSteps(callback:Function)
    {
        if (this.problemType===ProblemTypes.LINEARVARS)
        {
            const linCalc:LinerCalc=new LinerCalc();
            linCalc.calcLinear(this.orignal,callback);
        }
        return null;
    }
}