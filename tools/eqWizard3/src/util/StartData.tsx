import MainStorage from "../classes/MainStorage";
import { ProbVo } from "../dataObjs/ProbVo";

export class StartData {
    private files:string[]=["math3","math4","math5"];
    private callback:Function;
    private currentFile:string="";
    private ms:MainStorage=MainStorage.getInstance();

    constructor(callback:Function) {
        this.callback=callback;
    }
    getProblems() {
        this.loadNextFile();
    }
    private loadNextFile()
    {
        if (this.files.length>0)
        {
            let file:string | undefined=this.files.shift();
            if (file)
            {
                this.currentFile=file;
                this.loadJson(file);
            }           
        }
        else
        {
           // this.callback();
            this.setOptions();
           // this.getFileListFromServer();
        }
    }
    private loadJson(file: string) {
        fetch(file + ".json")
            .then(response => response.json())
            .then(data => this.process({ data }));
    }
   private process(data: any) {
       
        let problems: ProbVo[] = [];

        data = data.data;
        for (let i: number = 0; i < data.length; i++) {
           
            problems.push(new ProbVo(data[i].lesson, data[i].reference, data[i].problemID, data[i].instructions, data[i].problem));
        }
        this.ms.problemMap.set(this.currentFile,problems);
       // this.setState({ probs: problems });
       this.loadNextFile();
    }
    private getFileListFromServer()
    {
        //let apiConnect:ApiConnect=new ApiConnect();
       // apiConnect.getFileList(this.gotFileList.bind(this),"zoomy");
    }
    private gotFileList(data:any)
    {
       
        //set options

            

        this.setOptions();
    }

    private setOptions()
    {
        let lineUpString:string=localStorage.getItem("lineUpDecs") || "1";
        
        if (lineUpString==="0")
        {
            this.ms.lineUpDec=false;
        }

        this.callback();
    }
}