import { CellGridOutputVo } from "../dataObjs/outputs/CellGridOutputVo";
import { CSSVo } from "../dataObjs/CSSVo";
import { DCellOutput } from "../dataObjs/outputs/DCellOutput";
import { MCellGridOutputVo } from "../dataObjs/outputs/MCellGridOutputVo";
import { CSSTypes, ProblemTypes } from "./Constants";
import { FractGridOutput } from "../dataObjs/outputs/FractGridOutput";
import { ProbVo } from "../dataObjs/ProbVo";
import { FileVo } from "../dataObjs/outputs/FileVo";
import { Helpvo } from "../dataObjs/helpVo";
import { LinearCellOutputVo } from "../dataObjs/outputs/LinearCellOutputVo";
import { ChatVo } from "../dataObjs/ChatVo";

export class MainStorage {

  private static instance: MainStorage;


  public step: number = 0;
  public useRemainder: boolean = false;
  public zeroLimit: number = 2;
  public towerGrid2: CellGridOutputVo[] = [];
  public mTowerGrid:MCellGridOutputVo[]=[];
  public dcells:DCellOutput[]=[];
  public fcells:FractGridOutput[]=[];
  public lcells:LinearCellOutputVo[] = [];

  public messages:ChatVo[]=[];

  public lineUpDec:boolean=true;
  public expression:string="";
  public hideAnswer:boolean=false;
  public hideProduct:boolean=false;

  public fileList:FileVo[]=[];
  
  public showHidden:boolean=false;

  //if adding or subtracting rows or columns this will apply it to all steps
  //instead of just the current
  public applyToAllSteps:boolean=false;
  public jumpToNext:boolean=false;
  
  public problemMap:Map<string,ProbVo[]>=new Map<string,ProbVo[]>();

  public saveObj: CellGridOutputVo[] | MCellGridOutputVo[] | DCellOutput[] | FractGridOutput[] | LinearCellOutputVo[] | null = null;

  public debug:boolean=false;

  public problemType:ProblemTypes=ProblemTypes.UNSUPORTED;

  public LtiFileName:string="";
  public fileName:string="";

  public sizes:number[]=[];
  public maxSize:number=0;

  public columns:number=5;

  public columnIndex:number=0;

  /* public towerGrid: GridOutputVo[] = [];
 
  public gridOutput: GridOutputVo | null = null; */
  public celloutput: CellGridOutputVo | null = null;

  public currentCssClasses: string[] = [];

  public fileVo:FileVo | null=null;
  
  public cssMap: Map<CSSTypes, CSSVo[]> = new Map<CSSTypes, CSSVo[]>();
  public editMode:number=0;
  public helps:Helpvo[]=[];
 

  constructor() {

    (window as any).ms = this;
    this.cssMap.set(CSSTypes.Color, []);
    this.cssMap.set(CSSTypes.Decor, []);
    this.cssMap.set(CSSTypes.Size, []);

    for (let i:number=0;i<100;i++)
    {
      //this.fileList.push(new FileVo("RD.M1.EQ"+i.toString(),"addition","test file"+i.toString(),"1+"+i.toString(),10,[1,2,3],[]));
      this.fileList.push(new FileVo("RD.M1.EQ"+i.toString(),"addition","1+"+i.toString(),[]));
    }
    

    this.helps.push(new Helpvo(1,"How do I make steps for an equation?"));
    this.helps.push(new Helpvo(2,"How do I edit an equation?"));
    this.helps.push(new Helpvo(3,"How do I change colors or font sizes?"));
    this.helps.push(new Helpvo(4,"How do I hide a number?"));
    this.helps.push(new Helpvo(5,"How to add more digits."));
   // this.helps.push(new Helpvo(7,"Remove empty spaces"));
    this.helps.push(new Helpvo(7,"Cell Data Tricks"));
    this.helps.push(new Helpvo(8,"Format Workarounds"));
  }
  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }

    return this.instance;
  }
  public addCssType(cssVo: CSSVo) {
    let cssArray: CSSVo[] | undefined = this.cssMap.get(cssVo.type);
    if (cssArray) {
      cssArray.push(cssVo);
      this.cssMap.set(cssVo.type, cssArray);
    }
  }
  public getCssByType(type: CSSTypes) {
    if (this.cssMap.has(type)) {
      return this.cssMap.get(type);
    }
    return [];
  }
  public getSimpleSaveObjs()
  {
      let simpleObjs:any[]=[];
      if (this.saveObj)
      {
        for (let i:number=0;i<this.saveObj.length;i++)
        {
           simpleObjs.push(this.saveObj[i].getOutput());
        }

        return simpleObjs;
      }
      
      return [];
  }
  public getCssArray() {
    let cssArray: CSSVo[] = [];
    let typeArray: CSSTypes[] = [CSSTypes.Color, CSSTypes.Size, CSSTypes.Decor];
    let headers: string[] = ["Colors", "Sizes", "Decoration"];


    for (let i: number = 0; i < typeArray.length; i++) {

      let cssVo: CSSVo[] | undefined = this.getCssByType(typeArray[i]);
     
      if (cssVo) {
        cssArray.push(new CSSVo(CSSTypes.Header, headers[i], ""));

        for (let j: number = 0; j < cssVo.length; j++) {
          cssArray.push(cssVo[j]);
        }
      }

    }
    return cssArray;
  }


}
export default MainStorage;
