import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { CellGridOutputVo } from '../../dataObjs/outputs/CellGridOutputVo';
import RTower from '../displays/RTower';
interface MyProps { index: number, stepData: CellGridOutputVo, callback: Function, selected: boolean }
interface MyState { stepData: CellGridOutputVo, selected: boolean }
class StepCard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData, selected: this.props.selected };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData, selected: this.props.selected })
        }
    }
    getTag()
    {
        if (this.state.stepData.tag === "") {
            return "Step " + this.props.index;
        }
        return "Step " + this.props.index+"|"+this.state.stepData.tag;
    }
    render() {

        let selectedClass: string = (this.state.selected === true) ? "selectedCard" : "unselectedCard";

        return (<Card className="stepCard" onClick={() => { this.props.callback(this.props.index) }}>
            <Card.Header className={selectedClass}>{this.getTag()}</Card.Header>
            <Card.Body className='stepCardBody'>
                <RTower cellInfo={this.state.stepData} fontSize={20} idString={'mathDisplayRel'}></RTower>
            </Card.Body>
        </Card>)
    }
}
export default StepCard;