import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { CellGridOutputVo } from '../../dataObjs/outputs/CellGridOutputVo';
import { LinearCellOutputVo } from '../../dataObjs/outputs/LinearCellOutputVo';
import LinearDisplay from '../displays/LinearDisplay';
import RTower from '../displays/RTower';
interface MyProps { index: number, stepData: LinearCellOutputVo, callback: Function, selected: boolean }
interface MyState { stepData: LinearCellOutputVo, selected: boolean }
class LStepCard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData, selected: this.props.selected };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData, selected: this.props.selected })
        }
    }
    getTag()
    {
        if (this.state.stepData.tag === "") {
            return "Step " + this.props.index;
        }
        return "Step " + this.props.index+"|"+this.state.stepData.tag;
    }
    render() {

        let selectedClass: string = (this.state.selected === true) ? "selectedCard" : "unselectedCard";

        return (<Card className="LstepCard" onClick={() => { this.props.callback(this.props.index) }}>
            <Card.Header className={selectedClass}>{this.getTag()}</Card.Header>
            <Card.Body className='stepCardBody'>
                <LinearDisplay cells={this.state.stepData}></LinearDisplay>
            </Card.Body>
        </Card>)
    }
}
export default LStepCard;