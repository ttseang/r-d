import { Component, CSSProperties } from 'react';
import MainStorage from '../../classes/MainStorage';
import { FractCellVo } from '../../dataObjs/FractCellVo';
import { FractGridOutput } from '../../dataObjs/outputs/FractGridOutput';
interface MyProps { cells: FractGridOutput,fontSize:number}
interface MyState { cells: FractGridOutput }
class FractDisplay extends Component<MyProps, MyState>
{
    private keyIndex: number = 0;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { cells: this.props.cells };
        (window as any).fd=this;
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ cells: this.props.cells });
            this.keyIndex = 0;
        }
    }
    getKey() {
        this.keyIndex++;
        return "key_" + this.keyIndex.toString();
    }
    getFracts() {
        
        let fArray: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.cells.fracts.length; i++) {
            let fract: FractCellVo = this.state.cells.fracts[i];
            if (fract.w > 0) {
                fArray.push(<div key={this.getKey()}>{fract.getWhole()}</div>);
            }

            fArray.push(<div key={this.getKey()}>{fract.getTop()}</div>);

            if (this.state.cells.ops[i]) {
                fArray.push(<div key={this.getKey()}>{this.state.cells.opCells[i].toHtml(i)}</div>);
            }
            
        }
        
        for (let j: number = 0; j < this.state.cells.fracts.length; j++) {
            let fract: FractCellVo = this.state.cells.fracts[j];
            if (fract.w > 0) {
                fArray.push(<div key={this.getKey()}></div>);
            }
            fArray.push(<div key={this.getKey()}>{fract.getBottom()}</div>);
            fArray.push(<div key={this.getKey()}></div>);
        }
        return fArray;
    }
    getLen2()
    {
       let len:number=0;

        for (let i: number = 0; i < this.state.cells.fracts.length; i++) {
            let fract: FractCellVo = this.state.cells.fracts[i];
            
            len+=fract.getLen();
        }
        len+=this.state.cells.fracts.length-1;
        return len;
    }
    getLen() {
        let bl: number = 0;
        let tl: number = 0;

        for (let i: number = 0; i < this.state.cells.fracts.length; i++) {
            let fract: FractCellVo = this.state.cells.fracts[i];
            bl += fract.getBottomLen();
            tl += fract.getTopLen();
        }
       

        tl += this.state.cells.ops.length;
        bl += this.state.cells.ops.length;

        if (bl > tl) {
            return bl;
        }
        return tl;
    }
    getStyle() {
        

        let len: number = this.getLen2();
       
        let cssProps: CSSProperties = {};

        cssProps.display = "grid";
        cssProps.gridTemplateColumns = " 1fr".repeat(len);
        
        //cssProps.textAlign = "center";
       // cssProps.fontSize=this.props.fontSize.toString()+"px";

        return cssProps;
    }
    render() {
        return  <div className="tteq"><div className="equation fraction" style={this.getStyle()}>
                {this.getFracts()}
            </div></div>       
    }
}
export default FractDisplay;