import React, { Component } from 'react';
import { CellVo } from '../../dataObjs/CellVo';
import { DCellOutput } from '../../dataObjs/outputs/DCellOutput';
interface MyProps { cellInfo: DCellOutput, fontSize: number }
interface MyState { cellInfo: DCellOutput }
class DivDisplay extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { cellInfo: this.props.cellInfo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ cellInfo: this.props.cellInfo });
        }
    }
    
  
    private makeCols(index: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];
        //DON'T CHANGE
        for (let i: number = 0; i < cells.length; i++) {
            let key:string="col"+index.toString()+"_"+i.toString();
            cols.push(cells[i].toHtml(index,key));
        }
       /*  cells.forEach(cell => cols.push(cell.toHtml(index))); */
        return cols;
    }
  
   getProductRows() {
        let rows: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.cellInfo.products.length; i++) {
            let key1:string="blank"+i.toString();
            let key2:string="prow"+i.toString();

            rows.push(<div key={key1}></div>)
            rows.push(<div key={key2}>{this.makeCols(i, this.state.cellInfo.products[i])}</div>);
        }
        return rows;
    }
    render() {
       
        const { quotient, divisor, dividend } = this.state.cellInfo;
        
        return <div className='tteq'>
            <div className="equation division">
                <div></div>
                <div className="quotient">{this.makeCols(0, quotient)}</div>
                <div className="divisor">{this.makeCols(0, divisor)}</div>
                <div className="dividend">
                    <div className="inner">
                        <i className="circle"></i>
                        {this.makeCols(0, dividend)}
                    </div>
                </div>
                {this.getProductRows()}
            </div></div>

    }
}
export default DivDisplay;