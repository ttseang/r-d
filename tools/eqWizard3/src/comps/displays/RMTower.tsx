import { Component } from 'react';
import { Unicodes } from '../../classes/Constants';
import MainStorage from '../../classes/MainStorage';
import { CellVo } from '../../dataObjs/CellVo';
import { MCellGridOutputVo } from '../../dataObjs/outputs/MCellGridOutputVo';
import { GridUtil } from '../../util/GridUtil';
interface MyProps { cellInfo: MCellGridOutputVo, fontSize: number, idString: string }
interface MyState { cellInfo: MCellGridOutputVo, colCount: number }
class RMTower extends Component<MyProps, MyState>
{


    private ms: MainStorage = MainStorage.getInstance();
    private lastCell: CellVo | null = null;

    constructor(props: MyProps) {
        super(props);
        let colCount: number = this.props.cellInfo.getMaxCols() + 1;
        this.state = { cellInfo: this.props.cellInfo, colCount: colCount };
    }
    componentDidMount(): void {

    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            let colCount: number = this.props.cellInfo.getMaxCols() + 1;
            this.setState({ cellInfo: this.props.cellInfo, colCount: colCount });
        }
    }

  

    makeCols(index: number, cells: CellVo[], addOp: boolean = false) {

       // cells = GridUtil.fixDec(cells);
       // //console.log(cells);

        let cols: JSX.Element[] = [];

       /*  if (addOp === true) {
            let key2: string = "op" + this.ms.columnIndex.toString();
            cols.push(<span key={key2}>&nbsp;{Unicodes["x"]}&nbsp;</span>);
        } */

        for (let i: number = 0; i < cells.length; i++) {

            cols.push(cells[i].toHtml(i));

            this.lastCell = cells[i];
        }

        while (cols.length < this.state.colCount) {
            let key: string = "col" + this.ms.columnIndex.toString();
            this.ms.columnIndex++;
            cols.unshift(<span key={key}>&nbsp;</span>);

        }

        return cols;//`<span>${cols.join(" ")}</span>`;
    }
    /* stuffCols(cells:CellVo[])
    {
        //////console.log("cc="+this.colCount);
        //////console.log("len="+cells.length);

        while(cells.length<this.state.colCount)
        {
            cells.unshift(new CellVo("@",[]));
        }
        //////console.log(cells);
        return cells;
    } */
    getRows() {
        let rowArray: JSX.Element[] = [];
        const rows = this.state.cellInfo.rows;

        if (rows) {
            rows.forEach((row: CellVo[], index: number) => {

                const addOp: boolean = (index === rows.length - 1);
              
                row = row.slice();

                /* if (index===rows.length-1)
                {
                    row.unshift(new CellVo(' '+Unicodes['x']+' ',[]));
                } */

                // row=this.stuffCols(row);


                let cols: JSX.Element[] = this.makeCols(index, row, addOp);

                let rowKey: string = "mrow" + index.toString();

                const spacing: string = " 1fr".repeat(this.state.colCount);

                rowArray.push(<div className="ttrow" key={rowKey} style={{ gridTemplateColumns: spacing }}>{cols}</div>);
            });
        }

        return rowArray;
    }
    /*  getRows(): JSX.Element[] {
         let opString: string = "\u00D7";
 
         let rows: JSX.Element[] = [];
         if (this.state.cellInfo) {
             if (this.state.cellInfo.rows) {
                 for (let i: number = 0; i < this.state.cellInfo.rows.length; i++) {
                     let rowKey: string = "mrow" + i.toString();
                     let bKey: string = "blank" + i.toString();
                     if (i === this.state.cellInfo.rows.length - 1) {
                         rows.push(<div key={bKey}>{opString}</div>)
                     }
                     else {
                         rows.push(<div key={bKey}></div>)
                     }
 
                     rows.push(<div key={rowKey}>{this.makeCols(i, this.state.cellInfo.rows[i])}</div>);
                 }
             }
         }
 
         return (rows)
     } */

    getTop() {
        let topRows: JSX.Element[] = [];
        const top = this.state.cellInfo.top;

        if (top) {
            top.forEach((t: CellVo[], index: number) => {
                //   t=this.stuffCols(t);
                const cols: JSX.Element[] = this.makeCols(index, t);
                const spacing: string = " 1fr".repeat(this.state.colCount);
                let key: string = "topRow" + index.toString();
                topRows.push(<div className="ttrow small" key={key} style={{ gridTemplateColumns: spacing }}>{cols}</div>);
            });
        }
        return topRows;
    }
    /*  getTop() {
 
         let topRows: JSX.Element[] = [];
         if (this.state.cellInfo) {
             if (this.state.cellInfo.top) {
                 for (let i: number = 0; i < this.state.cellInfo.top.length; i++) {
                     let key: string = "topRow" + i.toString();
                     let key2: string = "ph" + i.toString();
                     topRows.unshift(<div key={key2}></div>)
                     topRows.unshift(<div key={key}>{this.makeCols(i, this.state.cellInfo.top[i])}</div>)
 
                 }
             }
         }
 
         return (topRows);
     } */
    getProducts() {

        /*  let maxCols:number=0;
 
         this.state.cellInfo.products.forEach((prod:CellVo[])=>{
             if (prod.length>maxCols)
             {
                 maxCols=prod.length;
             }
         })
         //console.log("maxCols="+maxCols); */

        let productRows: JSX.Element[] = [];
        if (this.state.cellInfo) {
            if (this.state.cellInfo.products) {
                for (let i: number = 0; i < this.state.cellInfo.products.length; i++) {

                    let key: string = "productRows" + i.toString();
                    let product: CellVo[] = this.state.cellInfo.products[i];

                    //   product=this.stuffCols(product);  
                    const spacing: string = " 1fr".repeat(this.state.colCount);
                    productRows.push(<div key={key} className="ttrow" style={{ gridTemplateColumns: spacing }}>{this.makeCols(i, product)}</div>)
                }
            }
        }

        return (productRows);
    }
    getAnswer() {
        let { answer, products } = this.state.cellInfo;

        ////console.log(products);
        ////console.log(answer);

        if (answer) {
            if (answer.length === 0) {
                return "";
            }
            if (products) {

                /*   for (let i:number=0;i<products.length;i++)
                  {
                      products[i]=this.stuffCols(products[i]);
                  }
                  ////console.log("answer");
   */
                const cols = this.makeCols(20, answer);
                // const hrs:JSX.Element = (products.length ? (<hr />) : "");
                const spacing: string = " 1fr".repeat(this.state.colCount);
                if (products.length > 0) {
                    return (<div><div><hr /></div>
                        <div className="ttrow answer" style={{ gridTemplateColumns: spacing }}>
                            {cols}
                        </div></div>)
                }
                return (<div><div></div>
                    <div className="ttrow answer" style={{ gridTemplateColumns: spacing }}>
                        {cols}
                    </div></div>)
            }
        }
        return "";
    }
    /* getAnswer() {
        if (this.state.cellInfo) {
            if (this.state.cellInfo.answer) {


                if (this.state.cellInfo.answer.length === 0) {
                    return "";
                }
                if (this.state.cellInfo.products) {
                    if (this.state.cellInfo.products.length === 0) {
                        return (<div id="answerGrid4">
                            <div></div>
                            {this.makeCols(20, this.state.cellInfo.answer)}
                        </div>)
                    }
                    return (<div id="answerGrid3">
                        <div></div>
                        {this.makeCols(20, this.state.cellInfo.answer)}
                    </div>)
                }
            }
        }
        return "";

    } */
    getCarryRow() {
        //    const { carryRow } = this.data;
        const carryRow: CellVo[] = this.state.cellInfo.carryRow;

        if (carryRow.length) {
            const cols = this.makeCols(30, carryRow);
            const spacing: string = " 1fr".repeat(this.state.colCount);
            return <div className="ttrow" style={{ gridTemplateColumns: spacing }}>{cols}</div>
        }
        return "";
    }
    /* getCarryRow() {
        if (this.state.cellInfo) {
            if (this.state.cellInfo.carryRow) {
                return this.makeCols(30, this.state.cellInfo.carryRow)
            }
        }
        return "";
    } */
    render() {
        //return <span>{this.makeCols(0,this.state.cellInfo.carryRow)}</span>

        return <div className="tteq"><div className="equation multiply">
            {this.getTop()}
            {this.getRows()}
            <hr />
            {this.getCarryRow()}
            {this.getProducts()}
            {this.getAnswer()}
        </div>
        </div>
    }
}
export default RMTower;