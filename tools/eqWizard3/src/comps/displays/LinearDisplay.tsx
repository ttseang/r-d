import React, { Component } from 'react';
import { CellVo } from '../../dataObjs/CellVo';
import { LinearCellOutputVo } from '../../dataObjs/outputs/LinearCellOutputVo';
interface MyProps { cells: LinearCellOutputVo }
interface MyState { cells: LinearCellOutputVo }
class LinearDisplay extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { cells: this.props.cells };
        }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ cells: this.props.cells });
        }
    }
    getCells()
    {
        let cArray: JSX.Element[] = [];
        let cells: CellVo[] = this.state.cells.cells;
        for (let i: number = 0; i <cells.length; i++) {
            let cell: CellVo = cells[i];
            cArray.push(cell.toHtml(i));
        }
        return cArray;
    }
    render() {
        return (<div>{this.getCells()}</div>)
    }
}
export default LinearDisplay;