import React, { Component } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { DividePosition, ProblemTypes, TowerPosition } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import MainStorage from '../../classes/MainStorage';
import { CellVo } from '../../dataObjs/CellVo';
interface MyProps { }
interface MyState { x: number, y: number }
class CellTools extends Component<MyProps, MyState>
{
    private mc: Controller = Controller.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { x: 0, y: 0 };
    }
    componentDidMount() {
        this.mc.setTowerCell = this.setTowerCell.bind(this);
        this.mc.setMTowerCell=this.setMTowerCell.bind(this);
        this.mc.setDTowerCell=this.setDTowerCell.bind(this);
    }
    componentWillUnmount() {
        this.mc.setTowerCell = () => { };
        this.mc.setMTowerCell=()=>{};
        this.mc.setDTowerCell=()=>{};
    }
    setTowerCell(tp: TowerPosition, x: number, y: number, cell: CellVo) {
        this.setState({ x: x, y: y });
    }
    setMTowerCell(tp: TowerPosition, x: number, y: number, cell: CellVo) {
        this.setState({ x: x, y: y });
    }
    setDTowerCell(tp: DividePosition, x: number, y: number, cell: CellVo) {
        this.setState({ x: x, y: y });
    }
    getShiftPanel() {
        switch (this.ms.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:
            case ProblemTypes.MULTIPLICATION:
                case ProblemTypes.DIVISION:
                return (<div><Row><Col><Button size='sm' variant='success' onClick={() => { this.mc.shiftColLeft() }}>Left</Button></Col><Col>SHIFT</Col><Col><Button size='sm' variant='success' onClick={() => { this.mc.shiftColRight() }}>Right</Button></Col></Row></div>)



            case ProblemTypes.FRACTIONS:
                return "not supported";
              

        }
        return "";
    }
    getInsertPanel() {
        switch (this.ms.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:
            case ProblemTypes.MULTIPLICATION:
                case ProblemTypes.DIVISION:
                return (<div><Row><Col sm={3}><Button size='sm' variant='success' onClick={() => { this.mc.insertColLeft() }}>Left</Button></Col><Col sm={5} className="tac">INSERT</Col><Col sm={3}><Button size='sm' variant='success' onClick={() => { this.mc.insertColRight() }}>Right</Button></Col></Row></div>)


            case ProblemTypes.FRACTIONS:
                return "not supported";
                

        }
        return "";
    }
    cellChanged() {
        switch (this.ms.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:

                break;

            case ProblemTypes.MULTIPLICATION:

                break;

            case ProblemTypes.DIVISION:

                break;

            case ProblemTypes.FRACTIONS:

                break;

        }
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    <Row><Col sm={4}>x:{this.state.x}</Col><Col sm={4}>y:{this.state.y}</Col></Row>
                    <hr />
                    {this.getShiftPanel()}
                    <hr />
                    {this.getInsertPanel()}
                    <hr />
                    <Row><Col className='tac'><Button variant='danger' onClick={() => { this.mc.deleteCell() }}>Delete Current Cell</Button></Col></Row>
                </Card.Body>
            </Card>
        </div>)

    }
}
export default CellTools;