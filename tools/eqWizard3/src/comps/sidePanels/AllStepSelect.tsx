import React, { ChangeEvent, Component } from 'react';
import MainStorage from '../../classes/MainStorage';
interface MyProps { }
interface MyState {applyToAllSteps:boolean}
class AllStepSelect extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state={applyToAllSteps:this.ms.applyToAllSteps};
        }
    onChecked(e:ChangeEvent<HTMLInputElement>)
    {
        this.setState({applyToAllSteps:e.currentTarget.checked});
        this.ms.applyToAllSteps=e.currentTarget.checked;
    }
    render() {
        
        return (<div><hr/>Apply to all steps: <input type='checkbox' onChange={this.onChecked.bind(this)} checked={this.state.applyToAllSteps}></input></div>)
    }
}
export default AllStepSelect;