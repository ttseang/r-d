import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { MTowerPosition, RowPos } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import AllStepSelect from './AllStepSelect';
interface MyProps { }
interface MyState { }
class MRowBox extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div className='insertCard'>
            <Card>
                <Card.Body>
                    <Card.Title>Rows</Card.Title>
                    <h4>Body</h4>
                    <Row>
                        <Col sm={6}>Top</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addMRow(MTowerPosition.Body,RowPos.Top) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subMRow(MTowerPosition.Body,RowPos.Top) }}>-</Button></Col>
                    </Row>
                  
                    <Row>
                        <Col sm={6}>Bottom</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addMRow(MTowerPosition.Body,RowPos.Bottom) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subMRow(MTowerPosition.Body,RowPos.Bottom) }}>-</Button></Col>
                    </Row>
                    <hr />
                    <h4>Product</h4>
                    <Row>
                        <Col sm={6}>Top</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addMRow(MTowerPosition.Product,RowPos.Top) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subMRow(MTowerPosition.Product,RowPos.Top) }}>-</Button></Col>
                    </Row>
                   
                    <Row>
                        <Col sm={6}>Bottom</Col>
                        <Col sm={3}><Button size='sm' variant='success' onClick={() => { this.controller.addMRow(MTowerPosition.Product,RowPos.Bottom) }}>+</Button></Col>
                        <Col sm={3}><Button size='sm' variant='danger' onClick={() => { this.controller.subMRow(MTowerPosition.Product,RowPos.Bottom) }}>-</Button></Col>
                    </Row>
                    <Row><Col><AllStepSelect></AllStepSelect></Col></Row>
                </Card.Body>
            </Card></div>)
    }
}
export default MRowBox;