import React, { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { Controller } from '../../classes/Controller';
interface MyProps { }
interface MyState { }
class AIPanel extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();
        constructor(props: MyProps) {
            super(props)
        }
    redo()
    {
        this.controller.redoAi();
    }
    addMoreDetail()
    {
        this.controller.aiTweak("add more detail to the steps.");
    }
    getButtons()
    {
        let bArray: JSX.Element[] = [];
        bArray.push(<Button key="btndetails" variant='warning' onClick={this.addMoreDetail.bind(this)}>Add More Detail</Button>);
        bArray.push(<Button key="btnredo" variant='danger' onClick={this.redo.bind(this)}>Recalculate Steps</Button>);
        return <ButtonGroup vertical>{bArray}</ButtonGroup>;
    }
    render() {
        return (<div>
            <h3>Ai Options</h3>
            {this.getButtons()}
        </div>)
    }
}
export default AIPanel;