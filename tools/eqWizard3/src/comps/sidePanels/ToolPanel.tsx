import React, { ChangeEvent, Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Controller } from '../../classes/Controller';
import MainStorage from '../../classes/MainStorage';
interface MyProps { }
interface MyState {lineUp:boolean,useRemain:boolean,zeroLimit:number,hideAnswer:boolean,hideProduct:boolean }
class ToolPanel extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private mc:Controller=Controller.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {lineUp:this.ms.lineUpDec,useRemain:this.ms.useRemainder,zeroLimit:this.ms.zeroLimit,hideAnswer:this.ms.hideAnswer,hideProduct:this.ms.hideProduct};
        }

        changeLineUp()
        {
            let lineUp2:boolean=this.state.lineUp;
            lineUp2=!lineUp2;
            this.ms.lineUpDec=lineUp2;
    
            let lineUpString:string=(lineUp2===true)?"1":"0";
    
            localStorage.setItem("lineUpDecs",lineUpString);
    
            this.setState({lineUp:lineUp2});
        }
        changeHideAnswer()
        {
            let hideAnswer:boolean=this.state.hideAnswer;
            hideAnswer=!hideAnswer;
            this.ms.hideAnswer=hideAnswer;
            this.setState({hideAnswer:hideAnswer});
        }
        changeHideProduct()
        {
            let hideProduct:boolean=this.state.hideProduct;
            hideProduct=!hideProduct;
            this.ms.hideProduct=hideProduct;
            this.setState({hideProduct:hideProduct});
        }
        changeRemain()
        {
            let remain2:boolean=this.state.useRemain;
            remain2=!remain2;
            this.ms.useRemainder=remain2;
            this.setState({useRemain:remain2});
        }
        changeZeroLimit(e:ChangeEvent<HTMLInputElement>)
        {
            this.ms.zeroLimit=parseInt(e.currentTarget.value);
            this.setState({zeroLimit:parseInt(e.currentTarget.value)});
        }
    doRecalc()
    {
        this.mc.doCalc(this.ms.expression);
    }
    render() {
        return (<div>
             <Row><Col sm={12}>Line Up Decimals<input type='checkbox' className='check1' onChange={this.changeLineUp.bind(this)} checked={this.state.lineUp}></input></Col></Row>
           
           <hr/>
           <Row><Col sm={12}>Hide Answer<input type='checkbox' className='check1' onChange={this.changeHideAnswer.bind(this)} checked={this.state.hideAnswer}></input></Col></Row>
           <hr/>
           <Row><Col sm={12}>Hide Product<input type='checkbox' className='check1' onChange={this.changeHideProduct.bind(this)} checked={this.state.hideProduct}></input></Col></Row>
           
          
           <hr/>
           <Row><Col sm={12}>Zero Limit:<input type='number'  onChange={this.changeZeroLimit.bind(this)} value={this.state.zeroLimit}></input></Col></Row>
            <hr/>
           <Row><Col sm={12}>Use Remainder<input type='checkbox' onChange={this.changeRemain.bind(this)} className='check1' checked={this.state.useRemain}></input></Col></Row>
           <hr/>
            <Row><Col className='tac'><Button variant="success" onClick={this.doRecalc.bind(this)}>Recalculate Steps</Button></Col></Row>
       
        </div>)
    }
}
export default ToolPanel;