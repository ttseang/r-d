import React, { ChangeEvent, Component } from 'react';
import { Controller } from '../../classes/Controller';
import MainStorage from '../../classes/MainStorage';
import { CellVo } from '../../dataObjs/CellVo';
import { LinearCellOutputVo } from '../../dataObjs/outputs/LinearCellOutputVo';
import { CSSVo } from '../../dataObjs/CSSVo';
import { CellGridOutputVo } from '../../dataObjs/outputs/CellGridOutputVo';
import { CssStyleUtil } from '../../util/editUtils/CssStyleUtil';
interface MyProps { stepData: LinearCellOutputVo, callback: Function }
interface MyState { stepData: LinearCellOutputVo }
class LinearEdit extends Component<MyProps, MyState>
{
    private cellIndex: number = 0;
    private controller: Controller = Controller.getInstance();
    private currentCell: HTMLInputElement | null = null;
    private currentCellVo: CellVo | null = null;
    private currentIndex: number = 0;
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData };
    }
    componentDidMount() {
        this.controller.setStyle = this.changeCss.bind(this);
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData });
        }
    }
    componentWillUnmount(): void {
        this.controller.setStyle=()=>{};
    }
    public getCellVo(index: number): CellVo {
        return this.state.stepData.cells[index];
    }
    changeCss(cssVo: CSSVo) {

        let stepDataTemp: LinearCellOutputVo = this.state.stepData;
        let cell: CellVo | null = this.getCellVo(this.currentIndex);


        if (cell) {
            //cell.addCss(cssVo);
            CssStyleUtil.addCssToCell(cssVo, cell);
        }
        this.props.callback(stepDataTemp);
        
        if (cell) {
            this.controller.setSelectedCss(cell.classArray);
        }
        //check if in middle of rendering
        this.setState({ stepData: stepDataTemp });
    }
    getLine() {
        let cols: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.stepData.cells.length; i++) {
            let id: string = "cell_" + i.toString();
            let cell: JSX.Element = this.getCell(i, this.state.stepData.cells[i]);

            cols.push(<span key={id} id={id}>{cell}</span>);
        }

        return (<div>{cols}</div>);
    }
    getCell(index: number, cell: CellVo) {
        let key = "editCell_" + index.toString() + "_" + this.cellIndex.toString();


        this.cellIndex++;
        let id: string = "cell" + this.cellIndex.toString();
        //   let id: string = "editCell_" + x.toString() + "_" + y.toString()+"_"+tp.toString()+ this.cellIndex.toString();
        let char: string = cell.text;

        if (cell.classArray.includes("hid") && char === "0") {
            char = "@";
        }

        let classString: string = "cellEdit";
        if (char === "@") {
            classString = "cellEditFade";
        }

        if (this.currentCellVo) {
            this.controller.setSelectedCss(this.currentCellVo.classArray);

        }

        return (<input key={key} id={id} maxLength={2} type='text' className={classString} value={char} onFocus={(e: React.FocusEvent<HTMLInputElement>) => { this.onFocus(index, e) }} onChange={(e: ChangeEvent<HTMLInputElement>) => { this.updateGrid(index, e) }}></input>)
    }
    onFocus(index: number, e: React.FocusEvent<HTMLInputElement>) {
        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;

        if (this.currentCell) {
            this.currentCell.classList.remove('inputSelected');
        }

        inputBox.select();
        inputBox.classList.add("inputSelected");
        this.currentCell = inputBox;
        this.currentIndex = index;
        //this.currentPos = tp;
        //this.currentY = y;
        // this.currentX = x;


        this.currentCellVo = this.getCellVo(index);
        if (this.currentCellVo) {
            this.controller.setSelectedCss(this.currentCellVo.classArray);

        }
    }
    updateGrid(index: number, e: ChangeEvent<HTMLInputElement>) {

        let char: string = e.currentTarget.value;
        let stepDataTemp: LinearCellOutputVo = this.state.stepData;

        let cell: CellVo = this.getCellVo(index);

        if (cell) {

            cell.setEditText(char);
            if (char !== "@") {
                cell.removeClass("hid");
            }
        }

        stepDataTemp.cells[index] = cell;

        this.props.callback(stepDataTemp);

        this.setState({ stepData: stepDataTemp });
        if (this.ms.jumpToNext === true) {


            let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;
            let cellIndex = parseInt(inputBox.id.replace("cell", ""));
            if (!isNaN(cellIndex)) {
                cellIndex++;
                let nBox: HTMLInputElement = document.getElementById("cell" + cellIndex.toString()) as HTMLInputElement;
                if (nBox) {
                    nBox.focus();
                }
            }
        }
    }
    render() {
        return (<div className='linearDisplay'>{this.getLine()}</div>)
    }
}
export default LinearEdit;