import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { FractPosition } from '../../classes/Constants';
import { Controller } from '../../classes/Controller';
import { CellVo } from '../../dataObjs/CellVo';
import { CSSVo } from '../../dataObjs/CSSVo';
import { FractCellVo } from '../../dataObjs/FractCellVo';

import { FractGridOutput } from '../../dataObjs/outputs/FractGridOutput';
import { CssStyleUtil } from '../../util/editUtils/CssStyleUtil';
interface MyProps { stepData: FractGridOutput, callback: Function }
interface MyState { stepData: FractGridOutput }
class FractionEdit extends Component<MyProps, MyState>
{
    private cellIndex: number = 0;
    private controller: Controller = Controller.getInstance();
    private currentCell: HTMLInputElement | null = null;
    private currentCellVo: CellVo | null = null;
    private currentX: number = -1;
    private currentY: number = -1;
    private currentPos: FractPosition | null = null;

    private keyIndex:number=0;
    private rowIndex:number=-1;


    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData };
        /*    this.controller.addColumn = this.addColumn.bind(this);
           this.controller.subColumn = this.subColumn.bind(this);
           this.controller.addRow = this.addRow.bind(this);
           this.controller.subRow = this.subRow.bind(this); */
        this.controller.setStyle = this.changeCss.bind(this);

        (window as any).fe=this;
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData });
            this.rowIndex=0;
        }

    }
    private getRowNumber()
    {
        this.rowIndex++;
        return this.rowIndex;
    }
    public getCellVo(tp: FractPosition, x: number, y: number) {
        let stepDataTemp: FractGridOutput = this.state.stepData;
        let cell: CellVo | null = null;
        

        switch (tp) {
            case FractPosition.Top:
               
                cell = stepDataTemp.fracts[x].top[y];
                break;

            case FractPosition.Bottom:
               
                cell = stepDataTemp.fracts[x].bottom[y];
                break;

            case FractPosition.Whole:
                
                cell = stepDataTemp.fracts[x].whole[y];
                break;

            case FractPosition.Operator:
                cell = stepDataTemp.opCells[y];
        }
      
        return cell;
    }
    getCell(tp: FractPosition, x: number, y: number, cell: CellVo) {
        if (tp===FractPosition.Operator)
        {
            return this.getOpCell(y,cell);
        }
        let key = "editCell_" + x.toString() + "_" + y.toString();
        this.cellIndex++;
        let id: string = "cell" + this.cellIndex.toString();
        let char: string = cell.text;
        let classString: string = "cellEdit";

        if (char === "0" && cell.classArray.includes("hid")) {
            char = "@";
        }

        if (char === "@") {
            classString = "cellEditFade";
        }
        return (<input key={key} id={id} maxLength={1} type='text' className={classString} value={char} onFocus={(e: React.FocusEvent<HTMLInputElement>) => { this.onFocus(tp, x, y, e) }} onChange={(e: ChangeEvent<HTMLInputElement>) => { this.updateGrid(tp, x, y, e) }}></input>)
    }
    //get operator cell
    getOpCell(y: number, cell: CellVo) {
        let key = "editCell_op_" + y.toString();
        this.cellIndex++;
        let id: string = "cell" + this.cellIndex.toString();
        let char: string = cell.text;
        let classString: string = "cellEdit";

        if (char === "0" && cell.classArray.includes("hid")) {
            char = "@";
        }

        if (char === "@") {
            classString = "cellEditFade";
        }
      //  char="?";
        return (<input key={key} id={id} maxLength={1} type='text' className={classString} value={char} onFocus={(e: React.FocusEvent<HTMLInputElement>) => { this.onFocus(FractPosition.Operator, 0, y, e) }} onChange={(e: ChangeEvent<HTMLInputElement>) => { this.updateGrid(FractPosition.Operator, 0, y, e) }}></input>)
    }
    updateGrid(tp: FractPosition, x: number, y: number, e: ChangeEvent<HTMLInputElement>) {
        
        let char: string = e.currentTarget.value;
        let stepDataTemp: FractGridOutput = this.state.stepData;

        let cell: CellVo | null = this.getCellVo(tp, x, y);
        if (tp===FractPosition.Operator)
        {
            //console.log("op cell");
            //console.log("x="+x);
            //console.log("y="+y);
            //console.log("cell",cell);
            cell=stepDataTemp.opCells[y];
            stepDataTemp.opCells[y].text=char;
           //get the text of each cell in the opCells and set it to the op array
             for (let i=0;i<stepDataTemp.opCells.length;i++)
             {
                let cellText:string=stepDataTemp.opCells[i].text;
                stepDataTemp.ops[i]=cellText;
             }

              
        }
        if (cell) {
          

            cell.setEditText(char);
            //console.log(char);
            //console.log(cell);
            if (char!=="@")
            {
                cell.removeClass("hid");
            }
        }
        //console.log(stepDataTemp);
        this.props.callback(stepDataTemp);

        this.setState({ stepData: stepDataTemp });

        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;
        let cellIndex = parseInt(inputBox.id.replace("cell", ""));
        if (!isNaN(cellIndex)) {
            cellIndex++;
            let nBox: HTMLInputElement = document.getElementById("cell" + cellIndex.toString()) as HTMLInputElement;
            if (nBox) {
                nBox.focus();
            }
        }
    }
    getCols(tp: FractPosition, row: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];
        for (let i: number = 0; i < cells.length; i++) {
            let id: string = row.toString() + "_" + i.toString();
            
            cols.push(<span key={id} id={id}>{this.getCell(tp, row, i, cells[i])}</span>);
        }
        let rowKey: string = "editRow" + row.toString();

        return (<div key={rowKey}>{cols}</div>);
    }
    getOpCols(cell: CellVo,index:number) {
        let cols: JSX.Element[] = [];
        let id: string = "op_" + index.toString();
        cols.push(<span key={id} id={id}>{this.getOpCell(index,cell)}</span>);
        let rowKey: string = "editRow" + index.toString();

        return (<div key={rowKey}>{cols}</div>);
    }

    onFocus(tp: FractPosition, x: number, y: number, e: React.FocusEvent<HTMLInputElement>) {

    

        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;

        if (this.currentCell) {
            this.currentCell.classList.remove('inputSelected');
        }

        inputBox.select();
        inputBox.classList.add("inputSelected");
        this.currentCell = inputBox;
        this.currentPos = tp;
        this.currentY = y;
        this.currentX = x;

      
        this.currentCellVo = this.getCellVo(tp, x, y);
      

        if (this.currentCellVo) {
            this.controller.setSelectedCss(this.currentCellVo.classArray);
        }


    }

    changeCss(cssVo: CSSVo) {
        

        if (this.currentPos !== null) {
            let stepDataTemp: FractGridOutput = this.state.stepData;
            let cell: CellVo | null = this.getCellVo(this.currentPos, this.currentX, this.currentY);
           

            if (cell) {
              //  cell.addCss(cssVo);
              CssStyleUtil.addCssToCell(cssVo,cell);
                this.controller.setSelectedCss(cell.classArray);
            }
            this.props.callback(stepDataTemp);
            this.setState({ stepData: stepDataTemp });

        }
    }
    getKey()
    {
        this.keyIndex++;
        return "cell_"+this.keyIndex.toString();
    }
    getFracts() {
        

        let rows: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.stepData.fracts.length; i++) {
            let fract: FractCellVo = this.state.stepData.fracts[i];
            let ops:CellVo[]=this.state.stepData.opCells || [];

            let divKey:string="divider_"+i.toString();
            let rowKey:string="row_"+i.toString();

            rows.push(<div key={divKey}><hr/></div>);
            rows.push(<div key={rowKey}>
                <Row>
                    <Col><span>Whole Number</span>{this.getCols(FractPosition.Whole,i,fract.whole)}</Col>
                    <Col><span>numerator</span>{this.getCols(FractPosition.Top,i,fract.top)}</Col>
                </Row>
                <Row>
                    <Col></Col>
                    <Col><span>denominator</span>{this.getCols(FractPosition.Bottom,i,fract.bottom)}</Col>
                </Row>
                <hr/>
                <Row><Col></Col><Col> {this.getFractOpertor(ops,i)}</Col></Row>
               
                </div>)
         
        }
        return (<div>{rows}</div>)
        //return (<div><Row>{tcols}</Row><Row>{bcols}</Row><Row>{wcols}</Row></div>)
    }
    getFractOpertor(ops: CellVo[],index:number) {
        let opSlice: CellVo[] = ops.slice(index, index + 1);
        if (opSlice.length === 0) {
            return "";
        }
        return <Row><span>Operator</span>{this.getOpCols(opSlice[0],index)}</Row>
    }
    render() {
    
        if (this.state === null) {
            return <h3>Invalid</h3>
        }
        if (!this.state.stepData) {
            return <h3>Invalid</h3>
        }
        return (<div id="divEdit" className="scroll2">
            {this.getFracts()}

        </div>)
    }
}
export default FractionEdit;