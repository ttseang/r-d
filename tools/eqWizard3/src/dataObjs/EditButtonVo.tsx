export class EditButtonVo
{
    public name:string;
    public icon:string;
    public buttonCat:number;
    public action:number;

    constructor(name:string,icon:string,buttonCat:number,action:number)
    {
        this.name=name;
        this.icon=icon;
        this.buttonCat=buttonCat;
        this.action=action;
    }
}