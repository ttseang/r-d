export class Helpvo
{
    public helpID:number;
    public question:string;

    constructor(helpID:number,question:string)
    {
        this.helpID=helpID;
        this.question=question;
    }
}