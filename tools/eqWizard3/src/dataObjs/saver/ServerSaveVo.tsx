/* {
    "metadata": {"lti": "TT.RD.FB01", "name": "All About Marsupials"},
    "data": {}
    } */

//import { MetaDataVo } from "./MetaDataVo";

export class ServerSaveVo
{
  //  public metadata:MetaDataVo;
    public lti:string;
    public data:any;
    constructor(fileName:string,name:string,type:string,data:any)
    {
        this.lti=fileName;
        //this.metadata=new MetaDataVo(fileName,name,type);
        this.data=data;
    }
}