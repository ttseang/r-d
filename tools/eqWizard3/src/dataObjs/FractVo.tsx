export class FractVo {
    public top: string;
    public bottom: string;

    public whole: string;

    public t1: number;
    public b1: number;
    private _w1: number;

    constructor(top: string, bottom: string, whole: string = "0") {
        this.top = top;
        this.bottom = bottom;

        this.whole = whole;
        this._w1 = parseInt(whole);

        this.t1 = parseInt(top) | 0;
        this.b1 = parseInt(bottom) | 0;
    }
    set w1(val: number) {
        this._w1 = val;
        this.whole = this._w1.toString();
    }
    get w1() {
        return this._w1;
    }
    toText()
    {
        if (this._w1!==0)
        {
            return this._w1.toString()+"_"+this.t1+"_"+this.b1;
        }
        else
        {
            return this.t1+"_"+this.b1;
        }
    }
    public toCells()
    {
        
    }
}