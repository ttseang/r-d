import { SaveCellVo } from "./SaveCellVo";

export class SaveDCellGridOutput
{
    public quotient: SaveCellVo[];
    public divisor: SaveCellVo[];
    public dividend: SaveCellVo[];
    public products: SaveCellVo[][];
    public remain: SaveCellVo[];

    constructor(quoteint:SaveCellVo[],divisor:SaveCellVo[],dividend:SaveCellVo[],products:SaveCellVo[][],remain:SaveCellVo[])
    {
        this.quotient=quoteint;
        this.divisor=divisor;
        this.dividend=dividend;
        this.products=products;
        this.remain=remain;
    }
}