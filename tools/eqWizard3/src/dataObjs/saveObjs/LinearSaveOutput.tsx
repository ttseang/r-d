import { SaveCellVo } from "./SaveCellVo";
import { SaveFractCell } from "./SaveFractCell";

export class LinearSaveOutput
{
    public cells:SaveCellVo[]=[];

    constructor(cells:SaveCellVo[])
    {
        this.cells=cells;
    }
}