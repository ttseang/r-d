import { SaveFractCell } from "./SaveFractCell";

export class SaveFractOutput
{
    public fracts:SaveFractCell[];
    public ops:string[];
   // public opCells:SaveCellVo[]=[];

    constructor(fracts:SaveFractCell[],ops:string[])
    {
        this.fracts=fracts;
        this.ops=ops;
    }
}