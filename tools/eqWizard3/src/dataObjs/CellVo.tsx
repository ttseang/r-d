import { ArrayUtil } from "../util/ArrayUtil";
import { SaveCellVo } from "./saveObjs/SaveCellVo";

export class CellVo
{
    //public text:string;
    public classArray:string[];
    private _text:string;
    

    constructor(text:string,classArray:string[])
    {
        this._text=text;
        this.classArray=classArray;
     //   this.autoClass();
    }
   public getOutput()
    {
        return new SaveCellVo(this.text,this.classArray);
    }
    public clone()
    {
        let classes:string[]=this.classArray.slice();
        /* //console.log("classes");
        //console.log(this.classArray);
        //console.log(classes); */
       // //console.log(classes);
        let copy:CellVo= new CellVo(this.text,classes);
        //do this to bypass the autoClass
        copy.classArray=classes;
        return copy;
    }
    public get text()
    {
        return this._text;
    }
    public set text(val:string)
    {
        this._text=val;
        this.autoClass();
    }
    public setEditText(val:string)
    {
        this._text=val;
        if (this._text!=="@")
        {
            this.removeClass("hid");
        }
    }
    
    public autoClass()
    {
        switch(this.text)
        {
            case "@":
               // this.text="0";
                this.addClass("hid");
                break;

            /* case ".":               
                this.addClass(" decimal");
            break; */
            
            default:
                this.removeClass("hid");
        }
    }
   public setText(text:string)
    {
        this.text=text;
        this.autoClass();
    }
  
    addClass(classString:string)
    {
        

        if (!this.classArray.includes(classString))
        {
           
            this.classArray.push(classString);
        }
        this.classArray=ArrayUtil.makeUniqueValues(this.classArray);
    }
    removeClass(classString:string)
    {
       // this.classArray=ArrayUtil.makeUniqueValues(this.classArray);
        
        let index:number=-1;
        for (let i:number=0;i<this.classArray.length;i++)
        {
            if (this.classArray[i]===classString)
            {
                index=i;
            }
        }
        if (index>-1)
        {
            this.classArray.splice(index,1);
        }
       
        

    }
    toHtml(index:number,parentKey:string="",useDiv:boolean=false)
    {
        let char: string = this.text;
        let cssClasses: string[] = this.classArray.slice();

        //do special here

        if (char === "@") {
            char = "0";
            this.addClass("hid");
        }
        
        if (char==="|")
        {
            char="-";
            this.addClass("hid");
        }

        let html: JSX.Element | null = null;

        let key: string = parentKey+"_span_" + index.toString();

        if (cssClasses.length === 0) {
            html = (<span key={key}>{char}</span>)
            if (useDiv)
            {
                html=(<div key={key}>{char}</div>)
            }
        }
        else {
            let classString:string=cssClasses.join(" ");
            classString=classString.trim();

            html = (<span key={key} className={classString}>{char}</span>)
            if (classString==="")
            {
                html=(<span key={key}>{char}</span>)
            }
            if (useDiv)
            {
                html=(<div key={key} className={classString}>{char}</div>)
                if (classString==="")
                {
                    html=(<div key={key}>{char}</div>)
                }               
            }
        }
        return html;
    }
}