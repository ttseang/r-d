import { GridUtil } from "../../util/GridUtil";
import { CellVo } from "../CellVo";
import { FractCellVo } from "../FractCellVo";
import { SaveFractOutput } from "../saveObjs/SaveFractOutput";

export class FractGridOutput
{
    
    public fracts:FractCellVo[];
    public ops:string[];
    public opCells:CellVo[]=[];
    public tag:string="";

    constructor(fracts:FractCellVo[],ops:string[])
    {
        this.fracts=fracts;
        this.ops=ops.slice();

        for (let i:number=0;i<this.ops.length;i++)
        {
            this.opCells.push(new CellVo(this.ops[i],['fop']));
        }
    }
    fromObj(data:any)
    {
        this.fracts=GridUtil.dataToFractCells(data.fracts);
        this.opCells=GridUtil.dataToCells(data.opCells);
        this.ops=data.ops;
    }
    getOutput()
    {
       let fractSave:SaveFractOutput=new SaveFractOutput(GridUtil.fcellsToSave(this.fracts),this.ops);
       return fractSave;
    //   let saveGrid:FractGridOutput=new FractGridOutput()
    }
    public clone()
    {
        let fracts2:FractCellVo[]=[];
        let ops:string[]=this.ops.slice();
        for (let i:number=0;i<this.fracts.length;i++)
        {
            fracts2.push(this.fracts[i].clone())
        }
        //console.log("clone fracts");
        //console.log(fracts2);
        //console.log("clone ops");
        //console.log(ops);
        return new FractGridOutput(fracts2,ops);
       // let opCells:CellVo[]=this.opCells.slice();
        ///return new FractGridOutput(fracts2,ops);
    }
}