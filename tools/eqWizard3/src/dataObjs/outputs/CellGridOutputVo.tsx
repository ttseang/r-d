import { GridUtil } from "../../util/GridUtil";
import { CellVo } from "../CellVo";
import { SaveCellGridOutput } from "../saveObjs/SaveCellGridOutput";

export class CellGridOutputVo {
    public top: CellVo[][];
    public rows: CellVo[][];
    public operator: string;
    public answer: CellVo[];
    public tag:string="";

    constructor(top: CellVo[][], rows: CellVo[][], answer: CellVo[], operator: string) {
        this.top = top;
        this.rows = rows;
        this.operator = operator;
        this.answer = answer;
    }
    fromObj(data:any)
    {        
        this.top=GridUtil.dataToGrid(data.top);        
        this.rows=GridUtil.dataToGrid(data.rows);
        this.answer=GridUtil.dataToCells(data.answer);
    }
    getMaxCols()
    {
        let maxCols: number = 0;
        
        this.top.forEach((cell: CellVo[]) => {
            maxCols=Math.max(cell.length,maxCols);           
        });
        this.rows.forEach((cell: CellVo[]) => {
            maxCols=Math.max(cell.length,maxCols);
        });
        
        maxCols=Math.max(this.answer.length,maxCols);
        
        return maxCols;
    }
    getOutput() {              

        let saveGrid:SaveCellGridOutput=new SaveCellGridOutput(GridUtil.mcellsToSave(this.top),GridUtil.mcellsToSave(this.rows),GridUtil.cellsToSave(this.answer),this.operator);
        return saveGrid;
    }
    clone() {

        let top2: CellVo[][] = [];

        for (let i: number = 0; i < this.top.length; i++) {

            top2[i] = [];
            for (let j: number = 0; j < this.top[i].length; j++) {
                
                /* //console.log(i+" "+j);
                //console.log(this.top[i][j]);
                //console.log(this.top[i][j].clone());
                //console.log("----"); */
                top2[i].push(this.top[i][j].clone());
            }
        }

        let rows2: CellVo[][] = [];

        for (let j: number = 0; j < this.rows.length; j++) {
            let row: CellVo[] = this.rows[j];
            let row2: CellVo[] = [];

            for (let k: number = 0; k < row.length; k++) {
                row2.push(row[k].clone());
            }
            rows2.push(row2);
        }

        let answer2: CellVo[] = [];
        for (let m: number = 0; m < this.answer.length; m++) {
            answer2.push(this.answer[m].clone());
        }

        let cellGrid: CellGridOutputVo = new CellGridOutputVo(top2, rows2, answer2, this.operator);
        return cellGrid;
    }
}