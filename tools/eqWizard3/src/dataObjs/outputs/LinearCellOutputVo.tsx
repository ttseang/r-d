import { GridUtil } from "../../util/GridUtil";
import { CellVo } from "../CellVo";

export class LinearCellOutputVo
{
    public instructions:string="";
    public cells:CellVo[]=[];
    public tag:string="";

    constructor(instructions:string,cells:CellVo[])
    {
        this.instructions=instructions;
        this.cells=cells;
    }
    fromObj(data:any)
    {
        this.instructions=data.instructions;
        this.cells=GridUtil.dataToCells(data.cells);
    }
    getOutput()
    {
        return {instructions:this.instructions,cells:GridUtil.cellsToSave(this.cells)};

    }
    clone()
    {
        let cells2:CellVo[]=[];
        for(let i:number=0;i<this.cells.length;i++)
        {
            cells2.push(this.cells[i].clone());
        }
        return new LinearCellOutputVo(this.instructions,cells2);
        
    }
}