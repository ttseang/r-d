import { ChangeEvent, Component } from 'react';
import { Card, Row, Col, Button, ButtonGroup } from 'react-bootstrap';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
import RTower from '../comps/displays/RTower';
import { CellVo } from '../dataObjs/CellVo';
import { CellGridOutputVo } from '../dataObjs/outputs/CellGridOutputVo';
import CommonTab from './tabs/CommonTab';
import FractTab from './tabs/FractTab';
import OptionTab from './tabs/OptionTab';
import ProbTab from './tabs/ProbTab';
//import TestTab from './tabs/TestTab';
interface MyProps {openCallback:Function}
interface MyState { text: string,tabMode:number, useRemain: boolean, zeroLimit: number, step: number, fontSize: number, msg: string, steps: CellGridOutputVo[] | null }
class EquationScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:Controller=Controller.getInstance();

    private tabArray: string[] = ["Common", "Fractions", "Problems","Options"];

    constructor(props: MyProps) {
        super(props);

      /*   let test1:CellGridOutputVo=new CellGridOutputVo([[new CellVo("0",["hid"])]],[],[new CellVo("1",[])],"+");
        //console.log(test1);
        //console.log(test1.clone()); */

        let cell1:CellVo=new CellVo("1",['hid']);
        //console.log(cell1);
        //console.log(cell1.clone());


        this.state = { text: this.getSaved(),tabMode:0, useRemain: false, zeroLimit: 3, step: 0, fontSize: 26, msg: "", steps: null }
    }
    goNext() {
        localStorage.setItem("eqtext", this.state.text);
        this.mc.doCalc(this.state.text);
       
    }
    getDisplay() {
        if (this.state.steps === null) {
            return this.state.msg;
        }
       
        if (!this.state.steps[this.state.step]) {
            return <div className='tac'><h2>Invalid</h2></div>
        }
        return <RTower cellInfo={this.state.steps[this.state.step]} fontSize={this.state.fontSize} idString={'mathDisplayRel'}></RTower>
    }
    getSaved() {
        let saveText: string = localStorage.getItem("eqtext") || "";
        return saveText;
    }
    setText(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.currentTarget.value })
    }
    clearDisplay() {
        this.setState({ text: "", step: 0, fontSize: 26 });
    }
    changeStep(e: ChangeEvent<HTMLInputElement>) {
        let step: number = parseInt(e.currentTarget.value);
        this.setState({ step: step });
        this.ms.step = step;
    }
    changeFontSize(e: ChangeEvent<HTMLInputElement>) {
        let fs: number = parseInt(e.currentTarget.value);
        this.setState({ fontSize: fs });

    }
    toggleTab(index:number)
    {
        this.setState({tabMode:index})
    }
    getTabButtons() {
        let buttons: JSX.Element[] = [];
        for (let i: number = 0; i < this.tabArray.length; i++) {
            let key: string = "tab" + i.toString();
            buttons.push(<Button key={key} size="lg" variant='light' onClick={() => { this.toggleTab(i) }}>{this.tabArray[i]}</Button>)

        }
        return (<ButtonGroup>{buttons}</ButtonGroup>);
    }
    addToEq(s: string) {
        
        let t: string = this.state.text;
        t += s;
        this.setState({ text: t });
      
    }
    setEq(s: string) {
        this.setState({ text: s });
      
    }
    getTab() {
        switch (this.state.tabMode) {
            case 0:
                return <CommonTab callback={this.addToEq.bind(this)}></CommonTab>

            case 1:
                return <FractTab callback={this.addToEq.bind(this)}></FractTab>

            case 2:
               // return <TestTab callback={this.setEq.bind(this)}></TestTab>
               return <ProbTab callback={this.setEq.bind(this)}></ProbTab>

            case 3:
                return <OptionTab></OptionTab>
        }
        return "";
    }
    render() {
        return (<div className='mcontainer'>
            <ButtonGroup><Button variant='success' onClick={()=>{this.props.openCallback()}}>Open</Button>
            <Button variant='primary' onClick={()=>{this.mc.showHelp()}}>Help</Button></ButtonGroup>
            {this.getDisplay()}
            <hr />
            <Card><Card.Body>
                <Row>
                    <Col className='tac' sm={8}><input id="text2" type='text' value={this.state.text} onChange={this.setText.bind(this)} /></Col>
                    <Col>
                        <Row>
                            <Col sm={4}><Button variant='primary' onClick={this.clearDisplay.bind(this)}>Clear</Button></Col>
                            <Col sm={4}><Button variant='success' onClick={this.goNext.bind(this)}>Next</Button></Col>
                        </Row>

                    </Col>
                </Row>
                <hr />
            <Row><Col>{this.getTabButtons()}</Col></Row>
            {this.getTab()}
            </Card.Body></Card>
        </div>)
    }
}
export default EquationScreen;