import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
interface MyProps {helpIndex:number,goBack:Function}
interface MyState {helpIndex:number}
class HelpScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state={helpIndex:this.props.helpIndex};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({helpIndex:this.props.helpIndex});
        }       
    }
    render() {
        let file:string="./helpFiles/"+this.state.helpIndex.toString()+"/index.html";
        //console.log(file);

        return (<div>
            <Button onClick={()=>{this.props.goBack()}}>Back</Button>
            <Card>
                <Card.Body>
                    <iframe title='helpFile' width="100%" height="400px" src={file}></iframe>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default HelpScreen;