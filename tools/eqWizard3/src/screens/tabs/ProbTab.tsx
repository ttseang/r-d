import React, { ChangeEvent, Component } from 'react';
import { Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import MainStorage from '../../classes/MainStorage';
import { ProbVo } from '../../dataObjs/ProbVo';
interface MyProps { callback: Function }
interface MyState { probs: ProbVo[], probIndex: number }
class ProbTab extends Component<MyProps, MyState>
{
    private probCats: string[] = ["math3", "math4", "math5"];
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { probs: this.ms.problemMap.get("math3") || [], probIndex: 0 };
    }
    componentDidMount() {

    }

    getProbList() {
        let rows: JSX.Element[] = [];
      

        for (let i: number = 0; i < this.state.probs.length; i++) {
            let prob: ProbVo = this.state.probs[i];
            if (prob.problem !== "") {
                let key: string = "prob" + i.toString();

                rows.push(<ListGroupItem key={key} onClick={() => { this.props.callback(prob.problem) }}><Row><Col sm={2}>{prob.lesson}</Col><Col sm={2}>{prob.reference}</Col><Col sm={3}>{prob.problemID}</Col><Col sm={3}>{prob.problem}</Col></Row></ListGroupItem>);
            }
        }
        return <ListGroup>{rows}</ListGroup>;
    }
    getDropDown()
    {
        let dd:JSX.Element[]=[];

        for (let i:number=0;i<this.probCats.length;i++)
        {
            let key:string="choice_"+i;
            dd.push(<option key={key} value={i}>{this.probCats[i]}</option>)
           
        }
        return <select value={this.state.probIndex} onChange={this.changeMath.bind(this)}>{dd}</select>
    }
    changeMath(e:ChangeEvent<HTMLSelectElement>)
    {
        let index:number=parseInt(e.currentTarget.value);
        let probs:ProbVo[]=this.ms.problemMap.get(this.probCats[index]) || [];
        this.setState({probIndex:index,probs:probs});
    }
    render() {
        return (
            <div>
                {this.getDropDown()}
                <hr/>
                <Row><Col sm={2}>Lesson</Col><Col sm={2}>Reference</Col><Col sm={3}>Problem</Col><Col sm={3}>Equation</Col></Row>
                <div className='scroll3'>

                    {this.getProbList()}
                </div></div>)
    }
}
export default ProbTab;