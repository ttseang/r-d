import React, { Component } from 'react';
import { Col, Button, Row, Card } from 'react-bootstrap';
import { PasteVo } from '../../dataObjs/PasteVo';

interface MyProps { callback: Function }
interface MyState { }
class FractTab extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state={};
        }
        makeFract(fract:string)
        {  
            if (fract==="/")
            {
                this.props.callback("/");
                return;
            }
          
            this.props.callback(fract);
        }
        getButtons() {
            let fractArray: string[] = ["1/2", "1/3", "2/3","1/4","2/4","3/4","1/5","2/5","3/5","4/5","1/6","2/6","3/6","4/6","5/6","1/7","2/7","3/7","4/7","5/7","6/7","7/7","1/8"];
            
            let pasteArray:PasteVo[]=[];
            pasteArray.push(new PasteVo("+"));
            pasteArray.push(new PasteVo("-"));
            pasteArray.push(new PasteVo("*"));
            pasteArray.push(new PasteVo("/"));
            pasteArray.push(new PasteVo("=calc"));

            for (let i:number=0;i<fractArray.length;i++)
            {
                let fract:string=fractArray[i];
                let paste:string=fract.replace("/","_");

                pasteArray.push(new PasteVo(fract,paste));
            }
            pasteArray.push(new PasteVo("1 1/2","1_1_2"));
            pasteArray.push(new PasteVo("1 1/3","1_1_3"));
            pasteArray.push(new PasteVo("1 1/4","1_1_4"));
            pasteArray.push(new PasteVo("1 1/5","1_1_5"));
            pasteArray.push(new PasteVo("1 1/6","1_1_6"));
            pasteArray.push(new PasteVo("1 1/7","1_1_7"));
            pasteArray.push(new PasteVo("1 1/8","1_1_8"));
            pasteArray.push(new PasteVo("1 1/9","1_1_9"));
            
            let buttons: JSX.Element[] = [];
            for (let i: number = 0; i < pasteArray.length; i++) {
                let key: string = "fractButton" + i.toString();
                buttons.push(<Col key={key} sm={2}><Button size="lg" variant='light' className="buttonCol" onClick={()=>{this.makeFract(pasteArray[i].paste)}}>{pasteArray[i].display}</Button></Col>)
            }
            return (<div className='scroll1'><Row>{buttons}</Row></div>);
        }
    render() {
        return (<Card><Card.Body>{this.getButtons()}</Card.Body></Card>)
    }
}
export default FractTab;