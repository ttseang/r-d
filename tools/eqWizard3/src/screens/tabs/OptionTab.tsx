import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import MainStorage from '../../classes/MainStorage';
interface MyProps { }
interface MyState {lineUp:boolean,useRemain:boolean,zeroLimit:number}
class OptionTab extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state={lineUp:this.ms.lineUpDec,useRemain:this.ms.useRemainder,zeroLimit:this.ms.zeroLimit};
        }
    changeLineUp()
    {
        let lineUp2:boolean=this.state.lineUp;
        lineUp2=!lineUp2;
        this.ms.lineUpDec=lineUp2;

        let lineUpString:string=(lineUp2===true)?"1":"0";

        localStorage.setItem("lineUpDecs",lineUpString);

        this.setState({lineUp:lineUp2});
    }
    changeRemain()
    {
        let remain2:boolean=this.state.useRemain;
        remain2=!remain2;
        this.ms.useRemainder=remain2;
        this.setState({useRemain:remain2});
    }
    changeZeroLimit(e:ChangeEvent<HTMLInputElement>)
    {
        
        this.ms.zeroLimit=parseInt(e.currentTarget.value);
        this.setState({zeroLimit:parseInt(e.currentTarget.value)});
    }
    render() {
        return (<div>
            <hr/>
            <Row><Col sm={3}>Line Up Decimals<input type='checkbox' className='check1' onChange={this.changeLineUp.bind(this)} checked={this.state.lineUp}></input></Col></Row>
           
            <hr/>
            <Row><Col>Zero Limit</Col><Col><input type='number' className='check1' onChange={this.changeZeroLimit.bind(this)} value={this.state.zeroLimit}></input></Col></Row>
            <Row><Col>Use Remainder</Col><Col><input type='checkbox' onChange={this.changeRemain.bind(this)} className='check1' checked={this.state.useRemain}></input></Col></Row>
        </div>)
    }
}
export default OptionTab;