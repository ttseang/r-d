<?php
class openai
{
    private function secret_key()
    {
        return $secret_key = 'key_here';
    }

    public function request($engine = "text-davinci-003", $prompt = "", $max_tokens = 2000, $useSearch = false)
    {
        if ($prompt === "") {
            $prompt = "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\nHuman: Hello, who are you?\nAI: I am an AI created by OpenAI. How can I help you today?\n";
        }
        $request_body = [
            "prompt" => $prompt,
            "max_tokens" => $max_tokens,
            "temperature" => 0.7,
            "top_p" => 1,
            "presence_penalty" => 0.75,
            "frequency_penalty" => 0.75,
            "best_of" => 1,
            "stream" => false,
        ];
        $url = "https://api.openai.com/v1/engines/" . $engine . "/completions";
        if ($useSearch) {
            $url = "https://api.openai.com/v1/engines/" . $engine . "/search";
        }
        $postfields = json_encode($request_body);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: ' . $this->secret_key()
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "Error #:" . $err;
        } else {
            echo $response;
        }
    }
}