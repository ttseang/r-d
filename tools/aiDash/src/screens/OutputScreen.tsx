import React, { Component } from 'react';
import { MainStorage } from '../classes/MainStorage';
import { MainUtil } from '../util/MainUtil';
interface MyProps {outputText:string,closeCallback:Function,anotherCallback:Function,startOverCallback:Function}
interface MyState {outputText:string}
class OutputScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {outputText:this.props.outputText};
        }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.outputText !== this.props.outputText) {
            this.setState({ outputText: this.props.outputText });
        }
    }
    doCopy()
    {
        MainUtil.doCopy(this.state.outputText);
    }
    doDownload()
    {
        MainUtil.downloadText(this.state.outputText,this.ms.inputText.substring(0,10).replace(" ","_"));
    }
    getAnother()
    {
        this.props.anotherCallback();
    }
    startOver()
    {
        this.props.startOverCallback();
    }
    render() {
        //create a read only text area with 4 buttons below it
        //the buttons are: copy, clear, save, and load
        //the text area is read only
        //make responsive
        //make it look nice
        return (<div className='outputGrid'>
            <div className='outputbox'>
            <div className='btn-close' onClick={() => { this.props.closeCallback() }}></div>
            <textarea className='outputTextArea' readOnly value={this.state.outputText}></textarea>
            <div className='outputButtons'>
                <button className='btn btn-success' onClick={this.doCopy.bind(this)}>Copy</button>
                <button className='btn btn-primary' onClick={this.doDownload.bind(this)}>Download</button>
                <button className='btn btn-warning' onClick={this.getAnother.bind(this)}>Get Another</button>
                <button className='btn btn-danger' onClick={this.startOver.bind(this)}>Start Over</button>
                <button className='btn btn-primary' onClick={() => { this.props.closeCallback() }}>Close</button>
            </div>
            </div>
        </div>)
    }
}
export default OutputScreen;