import React, { ChangeEvent, Component } from 'react';
import AppVo from '../blogData/AppVo';
import { DashConstants } from '../blogData/DashConstants';
import MainController from '../classes/MainController';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { appVo: AppVo | null, closeCallback: Function, writeCallback: Function, formIndex: number }
interface MyState { formIndex: number, appVo: AppVo | null, inputText: string, inputText2: string }
class FormScreen extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { formIndex: this.props.formIndex, appVo: this.props.appVo, inputText: this.ms.inputText, inputText2: this.ms.inputText2 };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.appVo !== this.props.appVo) {
            this.setState({ appVo: this.props.appVo });
        }
    }

    onTextChange(e: ChangeEvent) {
        const target = e.target as HTMLInputElement;
        const text: string = target.value;
        if (this.state.appVo) {
            if (text.length < this.state.appVo.maxText) {
                this.setState({ inputText: text });
            }
            else {
                this.mc.showFlashMessage(DashConstants.FlashMessages.TEXT_TOO_LONG);
            }
        }
    }
    onTextChange2(e: ChangeEvent) {
        const target = e.target as HTMLInputElement;
        const text: string = target.value;
        if (this.state.appVo) {
            if (text.length < this.state.appVo.maxText) {
                this.setState({ inputText2: text });
            }
        }
    }
    getForm1() {
        if (this.state.appVo) {
            let formArray: string[] = this.state.appVo.formData.split("|");
            let buttonDisabled: boolean = this.state.inputText.length < this.state.appVo.minText;

            return <div className="form1 abCenter">
                <h2>{this.state.appVo.title}</h2>
                <h3>{formArray[0]}</h3>
                <div className="form-group">
                    <label className='required'>{formArray[1]}</label>
                    <small>{formArray[2]}</small>
                    <input type="text" onKeyDown={this.doCheckForEnter.bind(this)} name="name2" id="name2" placeholder={formArray[3]} onChange={this.onTextChange.bind(this)} value={this.state.inputText} />
                    <div className="textInfoGrid">
                        <small className="minText">Min Text:{this.state.appVo.minText} Characters</small>
                        <small className='howMany'>{this.state.inputText.length}/{this.state.appVo.maxText}</small>
                    </div>
                    <button className='btn btn-primary' onClick={this.sendText.bind(this)} disabled={buttonDisabled}>Write My Text</button>
                    <button className='btn btn-danger' onClick={() => { this.props.closeCallback() }}>Close</button>
                </div>
            </div>
        }
        else {
            return <div>App Not Found</div>
        }
    }
    getForm2() {
        if (this.state.appVo) {
            let formArray: string[] = this.state.appVo.formData.split("|");
            let buttonDisabled: boolean = this.state.inputText.length < this.state.appVo.minText;

            return <div className="form1 abCenter">
                <h2>{this.state.appVo.title}</h2>
                <h3>{formArray[0]}</h3>
                <div className="form-group">
                    <label className='required'>{formArray[1]}</label>
                    <small>{formArray[2]}</small>
                    <input type="text" name="name" id="name" placeholder={formArray[3]} value={this.state.inputText} onChange={this.onTextChange.bind(this)} />
                    <div className="textInfoGrid">
                        <small className="minText">Min Text:{this.state.appVo.minText} Characters</small>
                        <small className='howMany'>{this.state.inputText.length}/{this.state.appVo.maxText}</small>
                    </div>
                    <label>{formArray[4]}</label>
                    <small>{formArray[5]}</small>
                    <input className='input2' type="text" name="name" id="name" placeholder={formArray[6]} value={this.state.inputText2} onChange={this.onTextChange2.bind(this)} />
                    <button disabled={buttonDisabled} className='btn btn-primary' onClick={() => { this.sendText.bind(this) }}>Write My Text</button>
                    <button className='btn btn-danger' onClick={() => { this.props.closeCallback() }}>Close</button>
                </div>
            </div>
        }
        else {
            return <div>App Not Found</div>
        }
    }
    getForm3() {
        //make a form with a textarea and a button and a label
        if (this.state.appVo) {
            let formArray: string[] = this.state.appVo.formData.split("|");
            let buttonDisabled: boolean = this.state.inputText.length < this.state.appVo.minText;

            return <div className="form3 abCenter">
                <h2>{this.state.appVo.title}</h2>
                <h3>{formArray[0]}</h3>
                <div className="form-group">
                    <label className='required'>{formArray[1]}</label>
                    <small>{formArray[2]}</small>
                    <textarea rows={10} cols={50} name="name" id="name" placeholder={formArray[3]} value={this.state.inputText} onChange={this.onTextChange.bind(this)} />
                    <div className="textInfoGrid">
                        <small className="minText">Min Text:{this.state.appVo.minText} Characters</small>
                        <small className='howMany'>{this.state.inputText.length}/{this.state.appVo.maxText}</small>
                    </div>
                    <button disabled={buttonDisabled} className='btn btn-primary' onClick={() => { this.sendText() }}>Write My Text</button>
                    <button className='btn btn-danger' onClick={() => { this.props.closeCallback() }}>Close</button>
                </div>
            </div>
        }
        else {
            return <div>App Not Found</div>
        }
    }
    doCheckForEnter(e: React.KeyboardEvent) {
        if (e.key === "Enter") {
            // this.doAction();
            if (this.state.appVo) {
                if (this.state.inputText.length > this.state.appVo.minText) {
                    this.sendText();
                }
            }
        }
    }
    sendText() {
        //send the text to the server
        /* console.log("sendText");
        console.log(this.state.inputText);
        console.log(this.state.appVo); */
        if (this.state.appVo) {
            this.ms.inputText = this.state.inputText;
            this.ms.inputText2 = this.state.inputText2;

            let promptText: string = this.state.appVo.template;
            promptText = promptText.replace("[prompt]", this.state.inputText);
            promptText = promptText.replace("[xtra]", this.state.inputText2);
            console.log(promptText);
            this.ms.promptText = promptText;
            //alert(promptText);
            //this.mc.showFlashMessage(promptText, 5000);



            this.props.writeCallback(promptText, this.state.inputText2);
        }
    }
    onSelectionChange(e: React.ChangeEvent<HTMLSelectElement>) {
        this.setState({ inputText2: e.target.value });
    }
    getForm4()
    {
        //same as form 1 but with a dropdown
        if (this.state.appVo) {
            let formArray: string[] = this.state.appVo.formData.split("|");
            let buttonDisabled: boolean = this.state.inputText.length < this.state.appVo.minText;
            //make an array of options from formArray[6] if it exists
            let options: string[] = [];
            if (formArray.length > 5) {
                options = formArray[5].split(",");
            }
            let optionsArray: JSX.Element[] = [];
            for (let i = 0; i < options.length; i++) {
                optionsArray.push(<option key={i} value={options[i]}>{options[i]}</option>);
            }
            return <div className="form1 abCenter">
                <h2>{this.state.appVo.title}</h2>
                <h3>{formArray[0]}</h3>
                <div className="form-group">
                    <label className='required'>{formArray[1]}</label>
                    <small>{formArray[2]}</small>
                    <input type="text" name="name2" id="name2" placeholder={formArray[3]} value={this.state.inputText} onChange={this.onTextChange.bind(this)} />
                    <div className="textInfoGrid">
                        <small className="minText">Min Text:{this.state.appVo.minText} Characters</small>
                        <small className='howMany'>{this.state.inputText.length}/{this.state.appVo.maxText}</small>
                    </div>
                    <label>{formArray[4]}</label>
                    <select className='input2' value={this.state.inputText2} onChange={this.onSelectionChange.bind(this)}>
                        {optionsArray}
                    </select>
                    <button disabled={buttonDisabled} className='btn btn-primary' onClick={() => { this.sendText() }}>Write My Text</button>
                    <button className='btn btn-danger' onClick={() => { this.props.closeCallback() }}>Close</button>
                </div>
            </div>
        }
        else {
            return <div>App Not Found</div>
        }

    }
    getForm() {
        console.log("getForm", this.state.formIndex);
        switch (this.state.formIndex) {
            case 1:
                return this.getForm1();
            case 2:
                return this.getForm2();
            case 3:
                return this.getForm3();
            case 4:
                return this.getForm4();
            default:
                return <div>Form Not Found</div>
        }
    }
    render() {
        return (<div className='formGrid'><div className='btn-close' onClick={() => { this.props.closeCallback() }}></div>{this.getForm()}</div>)
    }
}
export default FormScreen;