import React, { Component } from 'react';
import AppVo from '../blogData/AppVo';
import { CatVo } from '../blogData/CatVo';
import { DashConstants } from '../blogData/DashConstants';
import { MainStorage } from '../classes/MainStorage';
import AppCard from '../ui/AppCard';



import TopButtons from '../ui/TopButtons';
interface MyProps { callback: Function,topCallback:Function }
interface MyState { appList: AppVo[], filterID: number }
class AppListScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { appList: this.ms.apps, filterID: 0 };
    }
    getCards() {

        let appList: AppVo[] = this.state.appList.slice();

        if (this.state.filterID > 0) {
            let catID: number = this.ms.cats[this.state.filterID].id;
            ////console.log(catID,this.ms.cats[this.state.filterID].catName);
            appList = appList.filter((item: AppVo) => item.catID === catID);
        }
        //console.log("appList", appList);
        let cards: JSX.Element[] = [];
        for (let i = 0; i < appList.length; i++) {
            const key: string = "card" + i;
            let catVo: CatVo = this.ms.getCatVo(appList[i].catID);
            cards.push(<AppCard key={key} callback={() => { this.props.callback(appList[i]); }} appVo={appList[i]} color={catVo.catColor}></AppCard>);
        }
        return cards;
    }
    topButtonClicked(index: number) {
        //console.log("topButtonClicked", index);
        if (index < 100) {
            this.setState({ filterID: index });
        }
        else {
            switch (index) {
                case 100:
                    this.props.topCallback(DashConstants.ScreenConstants.STATUS);
                    break;
                case 101:
                    this.props.topCallback(DashConstants.ScreenConstants.SAVED);
                    break;
                
            }
        }
    }
    render() {
        //font-awesome icon
        //console.log("AppListScreen render");
        return (<div>
            <TopButtons callback={this.topButtonClicked.bind(this)}></TopButtons>
            <div className='appListScroll'>
                <div className='applistscreen'>
                    {this.getCards()}
                </div>
            </div>
        </div>);
    }
}
export default AppListScreen;