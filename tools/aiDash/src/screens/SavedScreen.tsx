import React, { Component } from 'react';
import { Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import { TextSaveVo } from '../blogData/TextSaveVo';
import { MainStorage } from '../classes/MainStorage';
import { DataCenter } from '../util/DataCenter';
import { MainUtil } from '../util/MainUtil';
interface MyProps { closeCallback: Function, actionCallback: Function }
interface MyState { texts: TextSaveVo[], searchText: string, showFaves: boolean, page: number }
class SavedScreen extends Component<MyProps, MyState>
{
    private catChanged: boolean = false;
    private filterID: number = 0;
    private ms: MainStorage = MainStorage.getInstance();
    //use state for the button but private var for the data call

    constructor(props: MyProps) {
        super(props);
        this.state = { texts: [], searchText: this.ms.searchText, showFaves: this.ms.showFaves, page: 0 };
    }
    componentDidMount(): void {
        this.getSaves();
    }
    getSaves() {
        let filterType: string = "bycat";
        let page: number = this.state.page;
        if (this.catChanged === true) {
            page = 0;
        }
        if (this.state.searchText !== "") {
            DataCenter.searchSaves(this.ms.userCode, this.state.searchText, this.gotSaves.bind(this));
            return;
        }
        if (this.filterID === 0) {
            filterType = "all";
        }
        if (this.state.showFaves === true) {
            console.log("fav");
            DataCenter.getFavorites(this.ms.userCode, page, this.gotSaves.bind(this));
            return;
        }
        let catID: number = this.ms.cats[this.filterID].id;

        DataCenter.getSaves(this.ms.userCode, catID, page, filterType, this.gotSaves.bind(this));
    }
    gotSaves(data: any) {
        // console.log(data);
        //map the data to textsavevo objects
        let textArray: TextSaveVo[] = data.map((item: any) => {
            return new TextSaveVo(item.id, item.text, parseInt(item.fav));
        });
        //  console.log(textArray);

        if (this.catChanged === false) {
            let nArray: TextSaveVo[] = this.state.texts.concat(textArray);
            this.setState({ texts: nArray });
        }
        else {
            this.setState({ texts: textArray, page: 0 });
            this.catChanged = false;
        }
    }
    getText() {
        let textSavedEl: JSX.Element[] = [];
        for (let i = 0; i < this.state.texts.length; i++) {
            let key: string = 'textSaved' + i;
            let text: JSX.Element = (<span>{this.state.texts[i].text}</span>);
            //mark the search text
            if (this.state.searchText.length > 0) {
                text = MainUtil.getHighlightedText(this.state.texts[i].text, this.state.searchText);
            }
            textSavedEl.push(<ListGroupItem key={key} onClick={() => { this.props.actionCallback(this.state.texts[i]) }}>{text}</ListGroupItem>);
        }
        if (this.state.texts.length > 49) {
            textSavedEl.push(<ListGroupItem className='cardMore' key="cardMore" onClick={() => { this.setState({ page: this.state.page + 1 }, () => { this.getSaves() }) }}><Button variant='warning'>Load More</Button></ListGroupItem>);
        }
        return <ListGroup>{textSavedEl}</ListGroup>
    }
    searchTextChange(event: any) {
        this.setState({ searchText: event.target.value });
        this.ms.searchText = event.target.value;
        if (event.target.value.length > 2) {
            this.catChanged = true;
           this.getSaves();
        }
    }
    toggleFavorites() {
        this.catChanged = true;
        this.ms.showFaves = !this.state.showFaves;
        this.setState({ showFaves: !this.state.showFaves, page: 0 }, () => { this.getSaves() });

    }
    render() {
        //add search field
        let favVariant: string = (this.state.showFaves === true) ? 'danger' : 'outline-danger';

        return (<div className='searchGrid'>
            <div className='btn-close' onClick={() => { this.props.closeCallback() }}></div>
            <h2>Saved Text</h2>
            <Button variant={favVariant} onClick={this.toggleFavorites.bind(this)}><i className="fa fa-heart"></i></Button>
            <div className='searchText'>
                <input type='text' placeholder='Search' onChange={this.searchTextChange.bind(this)} value={this.state.searchText}></input>
            </div>
            <div className='searchResults'>
                {this.getText()}
            </div>
            <Button onClick={() => { this.props.closeCallback() }}>Close</Button>
        </div>)
    }
}
export default SavedScreen;