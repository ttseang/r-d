/* eslint-disable no-restricted-globals */
import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { DataCenter } from '../util/DataCenter';
interface MyProps { closeCallback: Function }
interface MyState { remaining: number, total: number, resetDate: string }
class StatusScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { remaining: 0, total: 0, resetDate: "unknown" };
    }
    componentDidMount(): void {
        this.getStats();
    }
    getStats() {
        DataCenter.getUsageStats(this.gotStats.bind(this));
    }
    gotStats(data: any) {
        console.log(data);
        if (data.remaining !== undefined && data.total !== undefined && data.next !== undefined) {
            this.setState({ remaining: parseInt(data.remaining), total: parseInt(data.total), resetDate: data.next });
        }
    }
    render() {
        const used: number = this.state.total - this.state.remaining;
        const percent: number = Math.round((used / this.state.total) * 100);
        const percentRemaining: number = Math.round((this.state.remaining / this.state.total) * 100);

        //show used space, free space, total space, and a progress bar
        return (<div className="statusScreen">
            <table className='table1'>
                <tbody>
                    <tr>
                        <td>Total Queries</td>
                        <td>{this.state.total}</td>
                    </tr>
                    <tr>
                        <td>Remaining Queries</td>
                        <td>{this.state.remaining.toString()}</td>
                        <td>{percentRemaining.toString()}%</td>
                    </tr>
                    <tr>
                        <td>Used Queries</td>
                        <td>{used.toString()}</td>
                        <td>{percent.toString()}%</td>
                    </tr>



                </tbody>
            </table>
            <div className="statusBar" title='Percent Used'><div className='statusBarLeft' style={{width:percent.toString()+"%"}}></div></div>
            <table className='table2'>
                <tbody>
                    <tr>
                        <td>Reset Date</td>

                        <td>{this.state.resetDate}</td>
                    </tr>
                    <tr>
                        <td>Screen Size:</td>

                        <td>{screen.width.toString()} x {screen.height.toString()} </td>
                    </tr>

                </tbody>
            </table>

            <Button onClick={() => { this.props.closeCallback() }}>Close</Button>
        </div>)


    }
}
export default StatusScreen;