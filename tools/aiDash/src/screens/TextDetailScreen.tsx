import React, { Component } from 'react';
import { TextSaveVo } from '../blogData/TextSaveVo';
import MainController from '../classes/MainController';
import { MainStorage } from '../classes/MainStorage';
import { DataCenter } from '../util/DataCenter';
import { MainUtil } from '../util/MainUtil';
interface MyProps { textSavedVo: TextSaveVo | null, closeCallback: Function }
interface MyState { textSavedVo: TextSaveVo | null }
class TextDetailScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { textSavedVo: props.textSavedVo };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (this.props.textSavedVo !== prevProps.textSavedVo) {
            this.setState({ textSavedVo: this.props.textSavedVo });
        }
    }
    doCopy() {
        if (this.state.textSavedVo !== null) {
            MainUtil.doCopy(this.state.textSavedVo.text);
        }
    }
    doDownload() {
        if (this.state.textSavedVo !== null) {
            MainUtil.downloadText(this.state.textSavedVo.text, this.state.textSavedVo.text.substring(0, 10).replaceAll(" ", "_"));
        }
    }
    doFav() {
        if (this.state.textSavedVo !== null) {
            if (this.state.textSavedVo.fav === 0) {
                DataCenter.markAsFavorite(this.ms.userCode, this.state.textSavedVo.id, this.favAdded.bind(this));
            }
            else {
                DataCenter.removeFavorite(this.ms.userCode, this.state.textSavedVo.id, this.favRemoved.bind(this));
            }

        }
    }
    doDelete() {
        if (this.state.textSavedVo !== null) {
            DataCenter.deleteSave(this.state.textSavedVo.id, this.deleteDone.bind(this));
        }
    }
    deleteDone(data: any) {
        this.mc.showFlashMessage("Text deleted");
        this.props.closeCallback();
    }
    favAdded(data: any) {
        this.mc.showFlashMessage("Text added to favorites");
        if (this.state.textSavedVo !== null) {
            let textSavedVo: TextSaveVo = this.state.textSavedVo;
            textSavedVo.fav = 1;
            this.setState({ textSavedVo: textSavedVo });
        }
    }
    favRemoved(data: any) {
        this.mc.showFlashMessage("Text removed from favorites");
        if (this.state.textSavedVo !== null) {
            let textSavedVo: TextSaveVo = this.state.textSavedVo;
            textSavedVo.fav = 0;
            this.setState({ textSavedVo: textSavedVo });
        }
    }
    render() {
        if (this.state.textSavedVo === null) {
            return <div></div>;
        }
        let favClass: string = (this.state.textSavedVo.fav) ? "btn-danger" : "btn-outline-danger";
        let buttonText: string = (this.state.textSavedVo.fav) ? "Remove from favorites" : "Add to favorites";
        return (<div className='outputGrid'>
            <div className='outputbox'>
                <div className='btn-close' onClick={() => { this.props.closeCallback() }}></div>
                <textarea className='outputTextArea' readOnly value={this.state.textSavedVo?.text}></textarea>
                <div className='outputButtons'>
                    <button className='btn btn-success' onClick={this.doCopy.bind(this)}>Copy</button>
                    <button className='btn btn-primary' onClick={this.doDownload.bind(this)}>Download</button>
                    <button className={favClass} onClick={this.doFav.bind(this)}><i className='fas fa-heart'></i> {buttonText}</button>
                    <button className='btn btn-danger' onClick={this.doDelete.bind(this)}>Delete</button>
                    <button className='btn btn-primary' onClick={() => { this.props.closeCallback() }}>Close</button>
                </div>
            </div>
        </div>)
    }
}
export default TextDetailScreen;