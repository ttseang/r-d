import { Component } from 'react';
import { Alert } from 'react-bootstrap';

import AppVo from '../blogData/AppVo';
import { DashConstants } from '../blogData/DashConstants';
import { TextSaveVo } from '../blogData/TextSaveVo';
import ApiConnect from '../classes/ApiConnect';
import MainController from '../classes/MainController';

import { MainStorage } from '../classes/MainStorage';
import { StartData } from '../classes/StartData';
//import { DataCenter } from '../util/DataCenter';
import AppListScreen from './AppListScreen';

import FormScreen from './FormScreen';
import FrameScreen from './FrameScreen';
import OutputScreen from './OutputScreen';
//import BlockScreen from './BlockScreen';
//import FormScreen from './FormScreen';
import SavedScreen from './SavedScreen';
import StatusScreen from './StatusScreen';
import TextDetailScreen from './TextDetailScreen';
import WritingScreen from './WritingScreen';


interface MyProps { }
interface MyState { mode: DashConstants.ScreenConstants, selectedApp: AppVo | null, outputText: string, hasError: boolean, flashMessage: string, waiting: boolean }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    private catID: number = 0;
    private listID: number = 0;
    private startApp: string = "";
    private saveTextVo: TextSaveVo | null = null;
    private lastForm: DashConstants.ScreenConstants = DashConstants.ScreenConstants.FORM1;

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: DashConstants.ScreenConstants.LOADING, selectedApp: null, outputText: "", flashMessage: "", waiting: false, hasError: false };
        this.mc.handleError = this.handleError.bind(this);
    }
    componentDidMount() {


        let sd: StartData = new StartData(this.gotAppInfo.bind(this));
        sd.getAppInfo();
    }

    gotAppInfo() {
        console.log("gotAppInfo");
        this.setState({ mode: DashConstants.ScreenConstants.APP_LIST });
        const startAppDiv: HTMLDivElement | null = window.parent.document.getElementById("startApp") as HTMLDivElement;
        let startApp: string = "";
        if (startAppDiv) {
            startApp = startAppDiv.innerText || "";
        }
        if (startApp.length > 0) {
            this.startApp = startApp;
            // this.setState({ selectedApp: this.ms.getAppBySlug(startApp) });
            let startAppVo: AppVo | null = this.ms.getAppBySlug(startApp);
            if (startAppVo) {
                this.appListCallback(startAppVo);
            }
        }
    }

    getScreen() {

        if (this.state.waiting === true) {
            return <WritingScreen appVo={this.state.selectedApp}></WritingScreen>
        }

        switch (this.state.mode) {

            case DashConstants.ScreenConstants.LOADING:
                return (<div className="loadingScreen">
                    <img src="./hourglass.gif" alt="loading" />
                    <h2>Loading...</h2>
                </div>);

            case DashConstants.ScreenConstants.APP_LIST:

                return <AppListScreen callback={this.appListCallback.bind(this)} topCallback={this.topCallback.bind(this)}></AppListScreen>


            case DashConstants.ScreenConstants.FORM1:
                this.lastForm = DashConstants.ScreenConstants.FORM1;
                return <FormScreen formIndex={1} closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }} writeCallback={this.sendText.bind(this)} appVo={this.state.selectedApp}></FormScreen>

            case DashConstants.ScreenConstants.FORM2:
                this.lastForm = DashConstants.ScreenConstants.FORM2;
                return <FormScreen formIndex={2} closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }} writeCallback={this.sendText.bind(this)} appVo={this.state.selectedApp}></FormScreen>

            case DashConstants.ScreenConstants.FORM3:
                this.lastForm = DashConstants.ScreenConstants.FORM3;
                return <FormScreen formIndex={3} closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }} writeCallback={this.sendText.bind(this)} appVo={this.state.selectedApp}></FormScreen>

            case DashConstants.ScreenConstants.FORM4:
                this.lastForm = DashConstants.ScreenConstants.FORM4;
                return <FormScreen formIndex={4} closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }} writeCallback={this.sendText.bind(this)} appVo={this.state.selectedApp}></FormScreen>

            case DashConstants.ScreenConstants.OUTPUT:
                return <OutputScreen closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }} outputText={this.state.outputText} anotherCallback={this.getAnother.bind(this)} startOverCallback={this.startFormOver.bind(this)}></OutputScreen>

            case DashConstants.ScreenConstants.SAVED:
                return <SavedScreen closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }} actionCallback={this.getTextDetail.bind(this)}></SavedScreen>

            case DashConstants.ScreenConstants.STATUS:
                return <StatusScreen closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }}></StatusScreen>

            case DashConstants.ScreenConstants.TEXT_DETAIL:
                return <TextDetailScreen closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.SAVED }); }} textSavedVo={this.saveTextVo}></TextDetailScreen>

            case DashConstants.ScreenConstants.FRAME:
                return <FrameScreen closeCallback={() => { this.setState({ mode: DashConstants.ScreenConstants.APP_LIST }); }} actionCallback={() => { }} appVo={this.state.selectedApp}></FrameScreen>



        }
    }
    handleError() {
        this.mc.showFlashMessage("Sorry, there was an error. Please try again.");
        this.setState({ mode: DashConstants.ScreenConstants.APP_LIST, selectedApp: null });
    }
    getTextDetail(textSaveVo: TextSaveVo) {
        this.ms.saveTextVo = textSaveVo;
        this.saveTextVo = textSaveVo;
        this.setState({ mode: DashConstants.ScreenConstants.TEXT_DETAIL });
    }
    appListCallback(appVo: AppVo) {
        console.log(appVo);

        this.ms.inputText = "";
        this.ms.inputText2 = "";

        //this.setState({ selectedApp: appVo, mode: 1 });
        if (appVo !== null) {
            console.log(appVo);

            let link: string = appVo.link;

            console.log("Link: " + link);

            if (link.startsWith("mod")) {
                switch (link) {
                    case "mod":
                    case "mod1":
                        this.setState({ selectedApp: appVo, mode: DashConstants.ScreenConstants.FORM1 });
                        break;
                    case "mod2":
                        this.setState({ selectedApp: appVo, mode: DashConstants.ScreenConstants.FORM2 });
                        break;
                    case "mod3":
                        this.setState({ selectedApp: appVo, mode: DashConstants.ScreenConstants.FORM3 });
                        break;
                    case "mod4":
                        this.setState({ selectedApp: appVo, mode: DashConstants.ScreenConstants.FORM4 });
                        break;

                    // return <FormScreen appVo={appVo} closeCallback= callback={() => { this.setState({ selectedApp: null }) }}></FormScreen>
                }
                return;
            }
            if (appVo.openInNewWindow === true) {
                window.open(link, "_blank");
                return;
            }
            // window.open(link, "_blank");
            this.setState({ selectedApp: appVo, mode: DashConstants.ScreenConstants.FRAME });
        }
    }
    topCallback(mode: DashConstants.ScreenConstants) {
        this.setState({ mode: mode });
    }

    showFlashMessage() {
        if (this.state.flashMessage !== "") {
            setTimeout(() => {
                this.setState({ flashMessage: "" });
            }, 3000)
            return <div className="flash-message"><Alert variant='success'>{this.state.flashMessage}</Alert></div>
        }
        return ""
    }
    startFormOver() {
        this.setState({ mode: this.lastForm });
    }
    getAnother() {
        if (this.state.selectedApp) {
            this.setState({ waiting: true });
            if (this.ms.inputText.length > 0) {
                this.sendText(this.ms.promptText, "");
               // DataCenter.sendText(this.ms.userCode, this.state.selectedApp.id, this.ms.inputText, this.ms.inputText2, this.textSent.bind(this));
            }
        }
    }
    sendText(text1: string, text2: string) {
        if (this.state.selectedApp) {
            this.setState({ waiting: true });
           
            //DataCenter.sendText(this.ms.userCode, this.state.selectedApp.id, text1, text2, this.textSent.bind(this));
            const apiConnect: ApiConnect = new ApiConnect();
            apiConnect.sendTT(text1, this.textSent.bind(this), this.handleError.bind(this));

        }
    }
    textSent(data: any) {
        console.log(data);

        const choices: string[] = data.choices;
        let errorText: string = "unknown";

        if (choices !== null && choices.length > 0) {

            let response: string = data.choices[0].text;

            console.log("Response: " + response);
            if (response === null || response.length === 0) {
                errorText = "No response from server";
            }
            else {
                response = response.replace(/\\n/g, "");
                response = response.replace(/\\r/g, "");
                response = response.trim();
            }

            this.ms.outputText = response;
            this.setState({ mode: DashConstants.ScreenConstants.OUTPUT, waiting: false, outputText: response, hasError: false });
            return;

        }
        else {
            errorText = "No choices";
        }
        this.setState({ mode: DashConstants.ScreenConstants.OUTPUT, waiting: false, outputText: "Error " + errorText, hasError: true });
    }


    /*  <div className='main-title'>{(this.state.mode===1)?"Pick an App below to get started":"Saved Text"}</div> */
    render() {
        return <div>
            {this.showFlashMessage()}
            {this.getScreen()}
        </div>
    }
}
export default BaseScreen;