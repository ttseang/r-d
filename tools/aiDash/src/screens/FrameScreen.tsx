import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import AppVo from '../blogData/AppVo';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { appVo: AppVo | null, closeCallback: Function, actionCallback: Function }
interface MyState { appVo: AppVo | null }
class FrameScreen extends Component<MyProps, MyState>
{
    private timer: any = null;
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { appVo: this.props.appVo };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.appVo !== this.props.appVo) {
            this.setState({ appVo: this.props.appVo });
        }
    }
    componentDidMount(): void {
        /*  this.timer= setInterval(() => {
              this.checkFrame();
          }, 1000); */
    }
    componentWillUnmount(): void {
        clearInterval(this.timer);
    }
    checkFrame() {
        console.log('checkFrame');
        let frameEl: HTMLIFrameElement | null = document.getElementById('appframe') as HTMLIFrameElement;
        if (frameEl) {
            console.log("height:" + frameEl.contentWindow?.document.body.scrollHeight);

            if (frameEl.scrollHeight > frameEl.clientHeight) {
                let nheight: number = frameEl.scrollHeight + 20;
                frameEl.style.height = nheight.toString() + 'px';
            }
        }
    }
    openAppInNewWindow() {
        if (this.state.appVo) {
            let link: string = this.state.appVo.link;

            if (this.ms.isMembershipActive === false || this.ms.isUserLoggedIn === false) {
                link = this.state.appVo.freeLink;
            }
            console.log('openAppInNewWindow:' + link);
            window.open(link, '_blank');
        }
    }
    render() {
        console.log(this.state.appVo);
        if (this.state.appVo) {
            let link: string = this.state.appVo.link;
            console.log('link:' + link);
            /* if (this.ms.isMembershipActive === false || this.ms.isUserLoggedIn === false) {
                link = this.state.appVo.freeLink;
            } */
            if (link.length === 0) {
                return <div>Sorry, this app is not available.<Button onClick={() => { this.props.closeCallback() }}>Close</Button></div>
            }
            return (<div className='frameGrid'>
                <div id="appID" style={{ display: 'none' }}>1</div>
                <iframe id="appframe" src={link} title='app1'></iframe>
                <Button variant='success' onClick={ this.openAppInNewWindow.bind(this)}>Open In New Window</Button>
                <Button onClick={() => { this.props.closeCallback() }}>Close</Button>
            </div>)
        }
        else {
            return <div>Sorry, this app is not available.<Button onClick={() => { this.props.closeCallback() }}>Close</Button></div>
        }
    }
}
export default FrameScreen;