import React, { Component } from 'react';
import AppVo from '../blogData/AppVo';
interface MyProps { appVo: AppVo | null }
interface MyState { appVo: AppVo | null }
class WritingScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { appVo: props.appVo };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (this.props.appVo !== prevProps.appVo) {
            this.setState({ appVo: this.props.appVo });
        }
    }
    render() {
        let text: string = "Writing Your Text";
        if (this.state.appVo !== null) {
            if (this.state.appVo.waitingText !== "") {
                text = this.state.appVo.waitingText;
            }
        }
        return (<div className='writingScreen'>
            <img src='./notebook.gif' alt='writing your text' />
            <h2>{text}</h2>
        </div>)
    }
}
export default WritingScreen;