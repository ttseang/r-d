import AppVo from "../blogData/AppVo";
import { CatVo } from "../blogData/CatVo";
import { TextSaveVo } from "../blogData/TextSaveVo";


/* eslint-disable @typescript-eslint/no-useless-constructor */
export class MainStorage
{
    private static instance:MainStorage | null=null;
    public cats:CatVo[]=[];

    //public catNames:string[]=['','Product Descriptions','Emails','Blog Posts','Social Media','Website Content','Writing Tools','Other'];
    public catColors:string[]=['#55efc4','#fdcb6e','#74b9ff','#fab1a0','#55efc4','#ffeaa7','#fab1a0','#ff7675','#fd79a8','#636e72'];
    public catIcons:string[]=['fas fa-pencil-alt','fas fa-envelope','fas fa-blog','fab fa-facebook','fas fa-globe','fas fa-tools','fas fa-tools'];

    public isUserLoggedIn:boolean=true;
    public isMembershipActive:boolean=false;
    

    
    public userCode:string="";
    public userName:string='Guest';
    public firstName:string='';
    public lastName:string='';
    public accountStatus:string='';
    public memberSince:string='';


    
    public apiURL:string='https://teachingtextboos.com/api/gateway.php';
    public appName:string="Teaching Textbooks AI Helper";
    public appVersion:string="1.0.0";

    public apps:AppVo[]=[];

    //
    //94a83e0684ef601f395ad3ac10a528c2$00001
    public getCatID:number=0;
    public getAppListID:number=0;
    public getUserInfoID:number=0;
    public getTextID:number=0;
    public getSavesID:number=0;
    public faveSavesID:number=0;
    public makeFaveID:number=0;
    public removeFaveID:number=0;
    public searchSavesID:number=0;
    public deleteSaveID:number=0;
    public getUsageStatsID:number=0;
    
    public inputText:string='';
    public inputText2:string='';
    public outputText:string='';
    public saveTextVo:TextSaveVo | null=null;
    public searchText:string='';
    public showFaves:boolean=false;
    
    public promptText:string='';
    
    constructor()
    {
        //
       /*  this.cats.push(new CatVo(0,'All',''));
        this.cats.push(new CatVo(1,'Product Descriptions',"#fdcb6e"));
        this.cats.push(new CatVo(2,'Emails',"#74b9ff"));    
       
        this.cats.push(new CatVo(4,'Website Content',"#55efc4"));
        this.cats.push(new CatVo(5,'Social Media',"#fab1a0"));
        this.cats.push(new CatVo(6,'Writing Tools',"#fd79a8"));
        this.cats.push(new CatVo(7,'Other',"#ff7675"));         */

        (window as any).mainStorage=this;
    }

    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new MainStorage();
        }
        return this.instance;
    }
    public getCatVo(catID:number):CatVo
    {
        let catVo:CatVo=new CatVo(0,'','','',0);
        for (let i=0;i<this.cats.length;i++)
        {
            if (this.cats[i].id===catID)
            {
                catVo=this.cats[i];
                break;
            }
        }
        return catVo;
    }
    public getAppBySlug(slug:string):AppVo | null
    {
        let appVo:AppVo | null=null;
        for (let i=0;i<this.apps.length;i++)
        {
            if (this.apps[i].appSlug===slug)
            {
                appVo=this.apps[i];
                break;
            }
        }
        return appVo;
    }
    sortApps()
    {
        //sort by the displayOrder of catID, then by dorder then by title
        this.apps.sort((a,b)=>
        {
            if (a.catID<b.catID) return -1;
            if (a.catID>b.catID) return 1;
            if (a.dorder<b.dorder) return -1;
            if (a.dorder>b.dorder) return 1;
            if (a.title<b.title) return -1;
            if (a.title>b.title) return 1;
            
            return 0;
        });
        //sort cats by displayOrder
        this.cats.sort((a,b)=>
        {
            if (a.displayOrder<b.displayOrder) return -1;
            if (a.displayOrder>b.displayOrder) return 1;
            return 0;
        });
    }
   /*  getUserID():number
    {
        let userID:number=0;
        if (this.userCode.length>0)
        {
            let userArray:string[]=this.userCode.split('$0000');
            if (userArray.length>1)
            {
                userID=parseInt(userArray[1]);
            }
        }
        return userID;
    } */
}