//controller signgleton
export default class MainController {
    private static instance: MainController;
    private constructor() { }
    public showFlashMessage:Function=(message:string)=>{}
    public handleError:Function=(error:any)=>{};
    
    public static getInstance(): MainController {
        if (!MainController.instance) {
            MainController.instance = new MainController();
        }
        (window as any).mc=MainController.instance;
        return MainController.instance;
    }
    
    //controller methods
    //...
    
}