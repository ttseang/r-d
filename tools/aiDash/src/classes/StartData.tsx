
import AppVo from "../blogData/AppVo";
import { CatVo } from "../blogData/CatVo";

import { MainStorage } from "./MainStorage";

/* eslint-disable @typescript-eslint/no-useless-constructor */
export class StartData {
    private ms: MainStorage = MainStorage.getInstance();
    private callback: Function;

    constructor(callback: Function) {
        this.callback = callback;
    }
    getAppInfo() {
        fetch("dashInfo.json")
            .then(response => response.json())
            .then(data => this.gotAppInfo(data));
    }
    gotAppInfo(data: any) {
       // console.log(data);
        this.ms.apiURL = data.dashInfo.apiURL;
        this.ms.appName = data.dashInfo.appName;
        this.ms.appVersion = data.dashInfo.appVersion;
        this.ms.getCatID = parseInt(data.dashInfo.getCatsID);
        this.ms.getAppListID = parseInt(data.dashInfo.getAppListID);
        this.ms.getUserInfoID = parseInt(data.dashInfo.getUserInfoID);
        this.ms.getTextID = parseInt(data.dashInfo.getTextID);
        this.ms.getSavesID = parseInt(data.dashInfo.getSavesID);
        this.ms.faveSavesID = parseInt(data.dashInfo.favSaveID);
        this.ms.removeFaveID = parseInt(data.dashInfo.unfavSaveID);
        this.ms.searchSavesID = parseInt(data.dashInfo.searchSavesID);
        this.ms.deleteSaveID = parseInt(data.dashInfo.deleteSaveID);
        this.ms.getUsageStatsID = parseInt(data.dashInfo.usageStatsID);
        //    DataCenter.getCategories(this.gotCats.bind(this));
        this.getCats();
    }
    getCats() {
        fetch("dashcats.json")
            .then(response => response.json())
            .then(data => this.gotCats(data ));
    }
    gotCats(data: any) {
        data=data.cats;
     //   console.log(data);
    //    console.log(data.length);
        this.ms.cats = [];
        //convert data into array of catVo objects
        for (let i: number = 0; i < data.length; i++) {
            let catVo: CatVo = new CatVo(parseInt(data[i].id), data[i].catName, data[i].color, data[i].icon, parseInt(data[i].displayOrder));
            //console.log(catVo);
            this.ms.cats.push(catVo);
        }
        //console.log(this.ms.cats);
        //  this.callback();


        this.getApps();
    }
    getApps() {
        fetch("dashapps.json").then(response => response.json()).then(data => this.gotAppList({ data }));
    }
    gotAppList(data: any) {
        console.log(data.data);
        //convert json data to array of AppVo
        let appList: AppVo[] = data.data.apps.map((item: any) => new AppVo(parseInt(item.id), item.title, item.description, parseInt(item.catID), parseInt(item.formID),item.template, item.icon, item.formData, item.buttonText, item.waitingText, parseInt(item.minText), parseInt(item.maxText), item.appSlug, item.link, item.freeLink, item.dorder, parseInt(item.paid), item.isNew, item.active, (parseInt(item.openInNew) === 1)));
    
        
    
        ////console.log(appList);
        this.ms.apps = appList;
        this.ms.sortApps();
        // this.setState({ appList: appList });

        // //console.log("userCode=" + this.ms.userCode);
        console.log(this.ms);
        //get titles for all apps
        /* let titleArray: string[] = [];
        for (let i: number = 0; i < this.ms.apps.length; i++) {
            titleArray.push(this.ms.apps[i].title);
        }
        console.log(titleArray.join(",")); */
        this.callback();
    }


}