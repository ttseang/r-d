//import { MainStorage } from "./MainStorage";

import { MainStorage } from "./MainStorage";

export class ApiConnect {
    private ms: MainStorage = MainStorage.getInstance();


    private params: string[] = [];

    public Send(callNumber: number, callback: Function, errorCallback: Function = () => { }) {
        // const obj:any={"data":this.params.join("|")};
        const params2 = this.params.join("|");
        //console.log(this.params);
        // const jsonObj:string=JSON.stringify(obj);
        

        fetch(this.ms.apiURL + "?callNumber=" + callNumber + "&z=" + params2, {
            method: "post"
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        
                        callback(json);
                    });
                }
            }).catch(
                error => {
                   
                    errorCallback(error);
                 })

    }
    public sendTT(prompt:string, callback:Function, errorCallback:Function)
    {
        const url:string="https://customtrack.org/api/davinci.php";
        //make post variables
        
        const data = new FormData();
        //set to no cors
        data.append('cors', 'no');
        data.append('prompt', prompt);
        fetch(url, {
            method: 'POST',
            body: data
        }).then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            callback(data);
        }).catch((error) => {
            console.error('Error:', error);
            errorCallback(error);
        });
    }
    public clear() {
        this.params = [];
    }
    public addParam(data: string) {
        this.params.push(data);
    }
}
export default ApiConnect;
