import React from 'react';
import "./styles/main.css";
import './styles/appScreens.css';
import './styles/appcard.css';
import './styles/form1.css';
import './styles/textcard.css';
import './styles/saved.css';
import './styles/status.css';
import './styles/frameScreen.css';
import './styles/blockScreen.css';
import './styles/writingScreen.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import BaseScreen from './screens/BaseScreen';


function App() {
  return (
    <div id="app"><BaseScreen></BaseScreen></div>
  );
}

export default App;
