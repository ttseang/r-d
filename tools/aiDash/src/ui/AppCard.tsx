import React, { Component } from 'react';
import AppVo from '../blogData/AppVo';
import { MainStorage } from '../classes/MainStorage';
interface MyProps {appVo:AppVo,color:string,callback:Function}
interface MyState {appVo:AppVo}
class AppCard extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    
    constructor(props: MyProps) {
        super(props);
        this.state = {appVo:this.props.appVo};
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if(prevProps.appVo!==this.props.appVo)
        {
            this.setState({appVo:this.props.appVo});
        }
    }
    render() {
        
        let icon: string = this.props.appVo.icon ? this.props.appVo.icon : "shrug";
      //  let color: string = this.ms.getCatColor();
        let iconPath:string="./icons/"+icon+".png";
       // let description: string = "This is the description of the app\nIt can be multiple lines long";

        return (<div className="appcard" style={{ "backgroundColor": this.props.color }} onClick={() => {this.props.callback() }}>
           <div className="appcard-icon"><img src={iconPath} alt="icon"></img></div>
            <div>
                <h2>{this.state.appVo.title}</h2>
                <p>{this.state.appVo.description}</p>
            </div>
        </div>)
    }
}
export default AppCard;