/* eslint-disable no-restricted-globals */
import React, { Component } from 'react';
import { MainStorage } from '../classes/MainStorage';
interface MyProps {callback:Function}
interface MyState {}
class TopButtons extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    getButtons()
    {
        let buttons:JSX.Element[]=[];
        console.log("getButtons",this.ms.cats);
      //  let icon:string[]=["fas fa-home","fas fa-plus","fas fa-cog","fas fa-question","fas fa-info","fas fa-user","fas fa-sign-out-alt","fas fa-sign-in-alt"];
        for(let i=0;i<this.ms.cats.length;i++)
        {
            const key:string="button"+i;
            buttons.push(<div key={key} title={this.ms.cats[i].catName} className="topButton" onClick={()=>{this.props.callback(i)}}><i className={this.ms.cats[i].icon}></i></div>);
        }        
     //   buttons.push(<div key="buttonStats" title='Usage' className="topButton" onClick={()=>{this.props.callback(100)}}><i className="fi fi-rr-percentage"></i></div>)
     //   buttons.push(<div key="buttonSaves" title='Saved Text' className="topButton" onClick={()=>{this.props.callback(101)}}><i className="fi fi-rr-disk"></i></div>)

        return buttons;
    }
    render() {
        //make a list of icon buttons for all, writing, products,email and saved


        return (
            <div className="topButtons">
                {this.getButtons()}
            </div>)
    }
}
export default TopButtons;