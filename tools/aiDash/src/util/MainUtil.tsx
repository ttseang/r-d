import MainController from "../classes/MainController";

export class MainUtil {
    public static doCopy(text: string): void {
        navigator.clipboard.writeText(text);
        const mc: MainController = MainController.getInstance();
        mc.showFlashMessage("Copied to clipboard");
    }
    public static downloadText(text: string, fileName: string): void {
        let blob = new Blob([text], { type: "text/plain;charset=utf-8" });
        let url = URL.createObjectURL(blob);
        let a = document.createElement("a");
        a.href = url;
        fileName=fileName.substring(0,20);
        fileName = fileName.replaceAll(" ", "_");
        a.download = fileName + ".txt";
        a.click();
    }
    public static markSearchText(text: string, searchText: string): string {
        let textLower: string = text.toLowerCase();
        let searchTextLower: string = searchText.toLowerCase();
        let index: number = textLower.indexOf(searchTextLower);
        if (index === -1) {
            return text;
        }
        let markedText: string = text.substring(0, index) + "<mark>" + text.substring(index, index + searchText.length) + "</mark>" + text.substring(index + searchText.length);
        return markedText;
    }
    public static getHighlightedText(text:string, highlight:string) {
        // Split text on highlight term, include term itself into parts, ignore case
        const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
        return <span>{parts.map(part => part.toLowerCase() === highlight.toLowerCase() ? <b className="yellow-highlight">{part}</b> : part)}</span>;
    }
}