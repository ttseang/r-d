import ApiConnect from "../classes/ApiConnect";
import MainController from "../classes/MainController";
import { MainStorage } from "../classes/MainStorage";

export class DataCenter {
    public static markAsFavorite(userCode: string, id: number, callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(userCode);
        apiConnect.addParam(id.toString());
        apiConnect.Send(ms.faveSavesID, callback,mc.handleError);

    }
    public static removeFavorite(userCode: string, id: number, callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(userCode);
        apiConnect.addParam(id.toString());
        apiConnect.Send(ms.removeFaveID, callback,mc.handleError);
    }
    public static getFavorites(userCode: string,page:number, callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(userCode);
        apiConnect.addParam("0");
        apiConnect.addParam(page.toString());
        apiConnect.addParam("fav");
        apiConnect.Send(ms.getSavesID, callback,mc.handleError);
    }

    public static sendText(userCode: string, appID: number, inputText: string, inputText2: string, callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.addParam(appID.toString());
        apiConnect.addParam(inputText);
        apiConnect.addParam(inputText2);
        apiConnect.Send(ms.getTextID, callback,mc.handleError);
    }
    public static getSaves(userCode: string, catID: number, page: number, filterType: string, callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.addParam(catID.toString());
        apiConnect.addParam(page.toString());
        apiConnect.addParam(filterType);
        apiConnect.Send(ms.getSavesID, callback,mc.handleError);
    }
    public static getUserInfo(callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.Send(ms.getUserInfoID, callback,mc.handleError);
    }
    public static getCategories(callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.Send(ms.getCatID, callback,mc.handleError);
    }
    public static getApps(callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.Send(ms.getAppListID, callback,mc.handleError);
    }
    public static searchSaves(userCode: string, searchStr: string, callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.addParam(searchStr);
        apiConnect.Send(ms.searchSavesID, callback,mc.handleError);
    }
    public static deleteSave(saveID: number, callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.addParam(saveID.toString());
        apiConnect.Send(ms.deleteSaveID, callback,mc.handleError);
    }
    public static getUsageStats(callback: Function) {
        const ms: MainStorage = MainStorage.getInstance();
        const mc: MainController = MainController.getInstance();
        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.addParam(ms.userCode);
        apiConnect.addParam("2");
        apiConnect.Send(ms.getUsageStatsID, callback,mc.handleError);
    }
}