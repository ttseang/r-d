
export default class AppVo
{
    public id:number=0;
    public title:string="";
    public description:string;
    public catID:number=0;
    public formID:number=0;
    public icon:string="";
    public link:string="";
    public dorder:number=0;
    public paid:number=0;
    public isNew:number=0;
    public active:number=0;
    public freeLink:string="";
    public formData:string="";
    public maxText:number;
    public minText:number;
    public appSlug:string="";
    public buttonText:string="";
    public waitingText:string="";
    public openInNewWindow:boolean;
    public template:string="";
    constructor(id:number,title:string,description:string,catID:number,formID:number,template:string,icon:string,formData:string,buttonText:string,waitingText:string,minText:number,maxText:number,appSlug:string,link:string,freeLink:string,dorder:number,paid:number,isNew:number,active:number,openInNewWindow:boolean)
    {
        this.id=id;
        this.title=title;
        this.description=description;
        this.catID=catID;
        this.formID=formID;
        this.icon=icon;
        this.dorder=dorder;
        this.paid=paid;
        this.isNew=isNew;
        this.active=active;
        this.formData=formData;
        this.maxText=maxText;
        this.minText=minText;
        this.appSlug=appSlug;
        this.buttonText=buttonText;
        this.waitingText=waitingText;
        this.openInNewWindow=openInNewWindow;
        this.template=template;
        this.link=link;
        this.freeLink=freeLink;
        
        console.log(this.formID);
    }
}