//category data
export class CatVo {
    public id: number;
    public catName: string;
    public catColor: string;
    public icon: string;
    public displayOrder: number;
    constructor(id: number, catName: string, catColor: string, icon: string, displayOrder: number) {
        this.id = id;
        this.catName = catName;
        this.catColor = catColor;
        this.icon = icon;
        this.displayOrder = displayOrder;
    }

}