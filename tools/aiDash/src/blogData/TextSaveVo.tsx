//make an object with an id and text property called textsavevo
export class TextSaveVo {
    public id: number;
    public text: string;
    public fav:number;
    constructor(id: number, text: string,fav:number) {
        this.id = id;
        this.text = text;
        this.fav=fav;
    }
}