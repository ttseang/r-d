export namespace DashConstants
{
    export enum ScreenConstants
    {
        Loading=-1,
        HOME=0,
        SAVED=1,
        STATUS=2,
        TEXT_DETAIL=3,
        FRAME=4,
        APP_LIST=5,
        OUTPUT=6,
        LOADING=7,
        FORM1=8,
        FORM2=9,
        FORM3=10,
        FORM4=12,
        BLOCKSCREEN=11
    }
    export namespace FlashMessages
    {
        export const Loading="Loading...";
        export const TEXT_TOO_LONG="Text is too long. Please shorten it.";
        export const ADDED_TO_FAVORITES="Added to favorites.";
        export const REMOVED_FROM_FAVORITES="Removed from favorites.";
        export const TEXT_DELETED="Text deleted.";

    }
}