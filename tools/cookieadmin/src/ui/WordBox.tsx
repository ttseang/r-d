import { WordConfig } from "../dataObjs/WordConfig";
import IBaseScene from "../interfaces/IBaseScene";
import Phaser from "phaser";

export class WordBox extends Phaser.GameObjects.Container
{
    public config:WordConfig;
    public scene:Phaser.Scene;
    private texts:Phaser.GameObjects.Text[]=[];
    private bscene:IBaseScene;
    public backs:Phaser.GameObjects.Sprite[]=[];
    private back:Phaser.GameObjects.Sprite;
    public index:number=0;
    
    constructor(bscene:IBaseScene,config:WordConfig)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.config=config;
        this.scene=bscene.getScene();
        this.makeLetters();
        this.scene.add.existing(this);
        this.back=this.scene.add.sprite(0,0,"holder").setOrigin(0,0);
         if (this.config.dir==="h")
        {
            this.back.displayHeight=this.bscene.ch;
            this.back.displayWidth=this.bscene.cw*this.config.word.length;
        }
        else
        {
            this.back.displayHeight=this.bscene.ch*this.config.word.length;
            this.back.displayWidth=this.bscene.cw;
        }
        this.add(this.back);
        //
        //
        //
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
        this.back.alpha=0;
       // this.textObj=this.scene.add.text(0,0,config.word);
    }
    makeLetters()
    {
        for (let i:number=0;i<this.config.word.length;i++)
        {
            
            let back=this.scene.add.sprite(0,0,"holder");//.setOrigin(0,0);
            back.displayWidth=this.bscene.cw*0.99;
            back.displayHeight=this.bscene.ch*0.99;
            back.setTint(0xe67e22);

            let letter=this.config.word[i];
            let textObj:Phaser.GameObjects.Text=this.scene.add.text(0,0,letter).setOrigin(0.5,0.5);

            if (this.config.dir==="h")
            {
                textObj.x=this.bscene.cw*i;
                back.x=this.bscene.cw*i;
            }
            else
            {
                textObj.y=this.bscene.ch*i;
                back.y=this.bscene.ch*i;
            }
            this.backs.push(back);
            this.texts.push(textObj);
            this.add(back);
            this.add(textObj);


        }
    }
}