import IBaseScene from "../interfaces/IBaseScene";

export class LetterBox extends Phaser.GameObjects.Container {
    public back: Phaser.GameObjects.Sprite;
    private shadow: Phaser.GameObjects.Sprite;
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private textObj: Phaser.GameObjects.Text;
    public selected: boolean = false;

    constructor(bscene: IBaseScene, letter: string, tint: number = 0xe67e22) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.scene=bscene.getScene();
        
        this.back = this.scene.add.sprite(0, 0, "holder");
        this.back.displayHeight = this.bscene.ch * 0.95;
        this.back.displayWidth = this.bscene.cw * 0.95;
        this.back.setTint(tint);

        this.shadow = this.scene.add.sprite(2, 2, "holder");
        this.shadow.displayHeight = this.bscene.ch * 0.95;
        this.shadow.displayWidth = this.bscene.cw * 0.95;
        // this.shadow.setTint(0xe67e22);

        this.add(this.shadow);
        this.add(this.back);
        let fs: number = bscene.getW() / 18;

        this.textObj = this.scene.add.text(0, 0, letter, { color: "#ffffff", fontFamily: "Dela Gothic One", fontSize: fs.toString() + "px" }).setOrigin(0.5, 0.5);


        this.add(this.textObj);

        this.scene.add.existing(this);
    }
    showLetter(val: boolean) {
        this.textObj.visible = val;
    }
}