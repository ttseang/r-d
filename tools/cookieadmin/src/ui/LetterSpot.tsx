import IBaseScene from "../interfaces/IBaseScene";

export class LetterSpot extends Phaser.GameObjects.Container
{
    public back:Phaser.GameObjects.Sprite;
    public letter:string="-";

    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private textObj:Phaser.GameObjects.Text;
    public callback:Function=()=>{};
    public selected:boolean=false;

    constructor(bscene:IBaseScene,letter:string,tint:number=0xe67e22)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.letter=letter;
        
        this.back=this.scene.add.sprite(0,0,"circle");
        this.back.displayHeight=this.bscene.ch*0.95;
        this.back.displayWidth=this.bscene.cw*0.95;
        this.back.setTint(tint);
        this.back.alpha=0.1;

        this.add(this.back);
        let fs:number=bscene.getW()/10;
        // console.log(fs);
         this.textObj=this.scene.add.text(0,0,letter,{color:"#000000",fontFamily:"Dela Gothic One",fontSize:fs.toString()+"px"}).setOrigin(0.5,0.5);
        
         this.setSize(this.back.displayWidth,this.back.displayHeight);
     
        this.add(this.textObj);

        this.scene.add.existing(this);

       // this.on('pointerover',this.onOver.bind(this));
     //   this.on('pointerdown',this.onOver.bind(this));
    }
    onOver()
    {
        if (this.callback)
        {
            this.callback(this);
        }
    }
    showLetter(val:boolean)
    {
        this.textObj.visible=val;
    }
}