import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { LetterSpot } from "./LetterSpot";

export class WordPad extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private word: string;
    private back: Phaser.GameObjects.Sprite;
    private tint: number;

    public callback:Function=()=>{};
    
    private activeLine: Phaser.GameObjects.Graphics;
    private spotLines: Phaser.GameObjects.Graphics;

    private allSpots:LetterSpot[]=[];
    private spots: LetterSpot[] = [];
    
    private lineColor:number=0x27ae60;

    constructor(bscene: IBaseScene, word: string, tint: number = 0xe67e22) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.word = word;
        this.tint = tint;
        //
        //
        //
        this.back = this.scene.add.sprite(0, 0, "circle").setOrigin(0, 0);
        Align.scaleToGameW(this.back, 0.4, this.bscene);
        this.back.setTint(tint);
        this.add(this.back);

        //
        //
        //
        this.setSize(this.back.displayWidth, this.back.displayHeight);

         this.activeLine = this.scene.add.graphics();
        this.spotLines = this.scene.add.graphics();

     
        //this.setInteractive();
        this.scene.add.existing(this);

        
        this.scene.input.once('gameobjectdown',this.addSpotOnClick.bind(this));
        this.scene.input.on('gameobjectover',this.addSpot.bind(this));
        this.scene.input.on('pointerup',this.onUp.bind(this));
        this.scene.input.on('pointermove',this.onMove.bind(this));
    }
    
    addSpotOnClick(pointer:Phaser.Input.Pointer,spot:LetterSpot)
    {
        this.scene.input.off('gameobjectover');
        if (spot.selected === false) {
            this.spots.push(spot);
            spot.selected = true;
            console.log(this.spots);
        }
        this.scene.input.on('gameobjectover',this.addSpot.bind(this));
       
    }
    addSpot(pointer:Phaser.Input.Pointer,spot: LetterSpot) {
               
        if (this.scene.input.activePointer.isDown === true) {
            if (spot.selected === false) {
                this.spots.push(spot);
                spot.selected = true;               
            }
        }        
        this.connectSpots();
    }

    onUp()
    {
        if (this.callback)
        {
            this.callback(this.getWord());
        }
        //
        //
        //
        this.spotLines.clear();
        this.activeLine.clear();
        this.spots=[];
        this.allSpots.forEach((spot:LetterSpot)=>{spot.selected=false});
        this.scene.input.off('gameobjectover');
        this.scene.input.once('gameobjectdown',this.addSpotOnClick.bind(this));      


    }
    getWord()
    {
        let word:string="";
        this.spots.forEach((spot:LetterSpot)=>{
            word+=spot.letter;
        })
        return word;
    }
    onMove(pointer:Phaser.Input.Pointer) {
        if (this.spots.length===0)
        {
            return;
        }    

        let last:LetterSpot=this.spots[this.spots.length-1];
        this.activeLine.clear();
        this.activeLine.lineStyle(6, this.lineColor);
        this.activeLine.lineBetween(last.x, last.y, pointer.x,pointer.y);
        this.activeLine.strokePath();
    }
    connectSpots()
    {
        if (this.spots.length<1)
        {
            return;
        }
        this.spotLines.clear();
        this.spotLines.lineStyle(6,this.lineColor);

        for(let i:number=1;i<this.spots.length;i++)
        {
            let prevSpot:LetterSpot=this.spots[i-1];
            let spot:LetterSpot=this.spots[i];
            this.spotLines.lineBetween(prevSpot.x,prevSpot.y,spot.x,spot.y);
        }
        this.spotLines.strokePath();
    }
   
    showLetters() {
        let agrid: AlignGrid = new AlignGrid(this.bscene, 11, 11, this.displayWidth, this.displayHeight, 0, 0);
        //  agrid.showNumbers();

        let spots: number[] = [1, 2, 3, 4, 5, 6, 7, 8];

         switch (this.word.length) {
            case 3:
                spots = [16, 56, 64];
                break;
            case 4:
                spots = [16, 56, 64, 93];
                break;
            case 5:
                spots = [16, 45, 53, 96, 90];
                break;
            case 6:
                spots = [16, 34, 42, 86, 78, 104];
                break;

            case 7:
                spots = [16, 34, 42, 75, 67, 91, 95];
                break;
        }

        for (let i: number = 0; i < this.word.length; i++) {
            let letterSpot: LetterSpot = new LetterSpot(this.bscene, this.word[i], this.tint);
            letterSpot.setInteractive();
            this.allSpots.push(letterSpot);         

            agrid.placeAtIndex(spots[i], letterSpot);
           
          

        }
    }
    fixSpots()
    {
        this.allSpots.forEach((spot:LetterSpot)=>{
            spot.x+=this.x;
            spot.y+=this.y;
        })
        this.activeLine = this.scene.add.graphics();
        this.spotLines = this.scene.add.graphics();
      
        
    }

}