import Controller from "../classes/Controller";
import { SaveObj } from "../dataObjs/saveObj";
import { WordConfig } from "../dataObjs/WordConfig";
import { WordBox } from "../ui/WordBox";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private controller: Controller = Controller.getInstance();
    private currentBox!: WordBox;
    private diffX: number = 0;
    private diffY: number = 0;
    private allBoxes:WordBox[]=[];

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "assets/images/holder.jpg");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.show();
        //this.controller.prevGame=this.previewGame.bind(this);
        //let test:Phaser.GameObjects.Sprite=this.add.sprite(100,100,"holder");

        this.controller.addWord = this.addWord.bind(this);
        this.controller.delBox=this.delBox.bind(this);
        this.controller.saveData=this.getInfo.bind(this);
        this.controller.prevGame=this.previewGame.bind(this);
        this.controller.makeNew=this.makeNew.bind(this);
       // this.addWord("fish", "h");

        if (this.ms.saveData!=="")
        {
            let levelString:string=this.ms.saveData;
            let levelData: any = JSON.parse(levelString);
            for (let i: number = 0; i < levelData.wordConfigs.length; i++) {
                let data: any = levelData.wordConfigs[i];
    
                
                this.addWord(data.word, data.dir, data.xPos, data.yPos);
            }
        }

        //
        //
        //
        this.input.on("gameobjectdown", this.selectBox, this);
    }
    makeNew()
    {
        this.ms.saveData="";
        this.allBoxes=[];
        this.scene.restart();
    }
    selectBox(pointer: Phaser.Input.Pointer, box: WordBox) {
        this.currentBox = box;
        //
        //
        //
        this.diffX = pointer.downX - this.currentBox.x;
        this.diffY = pointer.downY - this.currentBox.y;
        //
        //
        //
        this.input.on("pointermove", this.dragBox.bind(this));
        this.input.on("pointerup", this.snapToGrid.bind(this));

        this.controller.updateWord(this.currentBox.config);
    }

    addWord(word: string, dir: string,xx:number=0,yy:number=0) {
        let box: WordBox = new WordBox(this, new WordConfig(word, dir, 0, 0));
        this.grid.placeAt(xx, yy, box);

        box.config.xPos = xx;
        box.config.yPos =yy;

        this.allBoxes.push(box);
        box.index=this.allBoxes.length-1;
    }
    delBox()
    {
        if (this.currentBox===null)
        {
            return;
        }
        this.controller.updateWord(new WordConfig("","",0,0));
        this.allBoxes.splice(this.currentBox.index,1);
        this.currentBox.destroy();
    }
    getInfo()
    {
        let configs:WordConfig[]=[];

        for(let i:number=0;i<this.allBoxes.length;i++)
        {
            let box:WordBox=this.allBoxes[i];
            let config:WordConfig=box.config;
            configs.push(config);
        }
        let saveObj:SaveObj=new SaveObj(this.ms.word,configs);

        this.ms.saveData=JSON.stringify(saveObj);
        console.log(this.ms.saveData);
        //return JSON.stringify(configs);
    }
    /**
    * snap a box to the grid
    * @param box 
    */
    snapBoxToGrid(box: WordBox) {
        let xx = box.x + this.cw * 0.1;
        let yy = box.y + this.ch * 0.1;
        let coord = this.grid.findNearestGridXY(xx, yy);
        box.config.xPos = coord.x;
        box.config.yPos = coord.y;
        this.grid.placeAt(coord.x, coord.y, box);
    }
    snapToGrid() {
        if (!this.currentBox) {
            return;
        }
        this.snapBoxToGrid(this.currentBox);

        //
        //
        //
        this.input.off("pointermove");
        this.input.off("pointerup");
        //this.isDragging=false;

        //let the controls know the word information has been updated
        //  this.controller.updateWord(this.currentBox.config);

    }
    /**
     * update the postion of the box
     *  
     * @param pointer mouse or touch
     */
    dragBox(pointer: Phaser.Input.Pointer) {
        if (this.currentBox) {
            this.currentBox.x = this.input.activePointer.x - this.diffX;
            this.currentBox.y = this.input.activePointer.y - this.diffY;
            // this.updatePosWhileDragging();           
        }
    }
    previewGame() {
        this.getInfo();
        this.scene.start("SceneGame");
    }
    update() {

    }
}
export default SceneMain;