import React from 'react';

import './App.css';
import BaseScreen from './screens/BaseSceen';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <BaseScreen></BaseScreen>
    </div>
  );
}

export default App;
