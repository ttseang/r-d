import { WordConfig } from "./WordConfig";

export class SaveObj
{
    public word:string;
    public wordConfigs:WordConfig[];

    constructor(word:string,wordConfigs:WordConfig[])
    {
        this.word=word;
        this.wordConfigs=wordConfigs;
    }
}