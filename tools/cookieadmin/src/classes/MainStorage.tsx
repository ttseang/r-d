
export class MainStorage {
    
    private static instance: MainStorage;
    public word:string;
    public wordArray:string[]=[];
    public saveData:string=""
    //
    //
    

    constructor() {
        this.word="";
    }
    static getInstance(): MainStorage {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new MainStorage();
        }
        return this.instance;
    }
}
export default MainStorage;