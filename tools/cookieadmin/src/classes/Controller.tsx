
//import { DC } from "../ui/DC";

export class Controller {

    private static instance: Controller;

    //
    //
    public updateWord: Function = () => { };
    public addWord: Function = (word: string, dir: string) => { };
    public delBox:Function = () => { };
    public saveData:Function = () => { };
    public prevGame:Function=()=>{};
    public makeNew:Function=()=>{};
    /*  
     public updateSideBar: Function = () => { };
     public updateCanvas: Function = () => { };
     public undo:Function = () => { };
     public redo:Function = () => { };
     public setSelectMode:Function = () => { };
     public makeBox:Function = () => { };
     public copyBox:Function = () => { };
     
     public saveHistory:Function = () => { };
     public showLines:Function=()=>{};
      */
    /* constructor() {
      //
    } */
    static getInstance(): Controller {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new Controller();
        }
        return this.instance;
    }
}
export default Controller;