import React, { Component } from 'react';
import { Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import TextInputButton from '../comps/TextInputButton';
import { WordUtil } from '../util/WordUtil';
interface MyProps { callback: Function }
interface MyState { words: string[] }
class WordScreen extends Component<MyProps, MyState>
{
    private wordUtil: WordUtil = new WordUtil();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { words: [] };
        this.wordUtil.loadWords();
    }
    setText(text: string) {
        console.log(text);
        this.ms.word = text;
        this.setState({ words: this.wordUtil.checkWords(text) });
        // this.ms.wordArray=this.wordUtil.checkWords(text);
    }
    getWords() {
        let wordList: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.words.length; i++) {
            let key = "wl" + i.toString();
            wordList.push(<ListGroupItem key={key}>{this.state.words[i]}</ListGroupItem>);
        }
        return (<ListGroup className="wl">{wordList}</ListGroup>)
    }
    goNext()
    {
        this.ms.wordArray=this.state.words;
        this.props.callback(1);
    }
    getNextButton() {
        if (this.state.words.length===0)
        {
            return "";
        }
        return (<Button onClick={this.goNext.bind(this)}>Next</Button>)
    }
    render() {
        return (<div>
            <TextInputButton buttonText="Done" callback={this.setText.bind(this)} cancelCallback={() => { }} showCancel={false}></TextInputButton>
            <hr />
            {this.getWords()}
            {this.getNextButton()}
        </div>)
    }
}
export default WordScreen;