import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import LayoutScreen from './LayoutScreen';
import WordScreen from './WordScreen';

interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0 };
    }
    getScreen()
    {
        switch(this.state.mode)
        {
            case 0:
            return (<WordScreen callback={this.changeScreen.bind(this)}></WordScreen>)
            case 1:

            return (<LayoutScreen></LayoutScreen>)
        }
    }
    changeScreen(mode:number)
    {
        this.setState({mode:mode});
    }
    render() {
        return (
            <Card>
                <Card.Header className="header1">Word Game Admin</Card.Header>
                <Card.Body className="cardb">
                    {this.getScreen()}
                </Card.Body>
            </Card>
        )
    }
}
export default BaseScreen;