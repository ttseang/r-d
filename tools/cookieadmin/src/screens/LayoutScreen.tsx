import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import Controller from '../classes/Controller';
import DelPanel from '../comps/DelPanel';
import SaveBox from '../comps/SaveBox';
import WordList from '../comps/WordList';
import { WordConfig } from '../dataObjs/WordConfig';
import SceneMain from '../scenes/SceneMain';
import SceneGame from "../scenes/SceneGame";

interface MyProps { }
interface MyState { wordConfig: WordConfig }
class LayoutScreen extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { wordConfig: new WordConfig("", "", 0, 0) };
        this.controller.updateWord = this.updateWord.bind(this);
    }
    componentDidMount() {
        const config: any = {
            mode: Phaser.AUTO,
            width: 480,
            height: 640,
            parent: 'phaser-game',
            scene: [SceneMain,SceneGame]
        }

        new Phaser.Game(config);
    }
    updateWord(wordConfig: WordConfig) {
        this.setState({ wordConfig: wordConfig });
    }
    render() {
        return (<div>
            <Row>
                <Col sm="4">
                    <Row>
                        <Col><WordList></WordList></Col>
                    </Row>
                    <Row><Col><DelPanel wordConfig={this.state.wordConfig}></DelPanel></Col></Row>
                    <Row><Col><SaveBox></SaveBox></Col></Row>
                </Col>
                <Col sm="8"><div id="phaser-game"></div></Col>
            </Row>
        </div>)
    }
}
export default LayoutScreen;