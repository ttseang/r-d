import React, { Component } from 'react';
import { Card, ButtonGroup, Button } from 'react-bootstrap';
import Controller from '../classes/Controller';
interface MyProps { }
interface MyState { }
class SaveBox extends Component<MyProps, MyState>
{
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    saveData() {
        this.controller.saveData();
    }
    preview()
    {
        this.controller.prevGame();
    }
    makeNew()
    {
        this.controller.makeNew();
    }
    render() {
        return (<Card>
             <Card.Header>Save</Card.Header>
            <Card.Body>
                <ButtonGroup>
                <Button variant="secondary" onClick={this.makeNew.bind(this)}>
                        <i className="fas fa-file"></i>
                    </Button>
                    <Button variant="secondary" onClick={this.saveData.bind(this)}>
                        <i className="fas fa-save"></i>
                    </Button>
                    <Button variant="secondary" onClick={this.preview.bind(this)}>
                        <i className="fas fa-eye"></i>
                    </Button>
                </ButtonGroup>
            </Card.Body>
        </Card>)
    }
}
export default SaveBox;