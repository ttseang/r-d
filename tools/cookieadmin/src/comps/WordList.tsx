import React, { Component } from 'react';
import { Card,ListGroupItem, ListGroup, Button, Col, Row } from 'react-bootstrap';
import Controller from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
interface MyProps { }
interface MyState { words: string[] }
class WordList extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private controller:Controller=Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { words: this.ms.wordArray }
    }
    addVert(index:number)
    {
        this.controller.addWord(this.state.words[index],"v");
    }
    addHoriz(index:number)
    {
        this.controller.addWord(this.state.words[index],"h");
    }
    getWords() {
        let wordList: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.words.length; i++) {
            let key = "wll" + i.toString();
            wordList.push(<ListGroupItem key={key}><Row><Col>{this.state.words[i]}</Col><Col><Button size="sm" onClick={()=>{this.addVert(i)}}>|</Button></Col><Col><Button size="sm" onClick={()=>{this.addHoriz(i)}}>_</Button></Col></Row> </ListGroupItem >);
        }
        return (<ListGroup className="wl">{wordList}</ListGroup>)
    }
    render() {
        return (<Card>
            <Card.Header>Words</Card.Header>
            {this.getWords()}
        </Card>)
    }
}
export default WordList;