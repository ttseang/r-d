export class WordUtil
{
    private wordList:string[]=[];

    loadWords()
    {
       
       fetch("assets/words/all.txt").then(response => response.text())
        .then(data => {
            // Do something with your data
           // console.log(data);
            this.wordList=data.split(/\r?\n/)
        });
      
    }
    checkWords(word:string)
    {
       let words:string[]=[];
      //  console.log(this.wordList.length);
      //  console.log(this.checkSingle(word,"yellow"));
        for (let i:number=0;i<this.wordList.length;i++)
        {
            let current:string=this.wordList[i];
         //   console.log(current);

            if (this.checkSingle(word,current)===true)
            {
                if (this.checkDupLetters(word,current))
                {
                 words.push(current);
                }
            }
            
        }
        console.log(words);
        return words;
        //console.log(this.wordList);
    }
    checkDupLetters(word1:string,word2:string)
    {
                
        for (let i:number=0;i<word2.length;i++)
        {
            let letter:string=word2[i];
            let letterCount1:number=word1.split(letter).length-1;
            let letterCount2:number=word2.split(letter).length-1;
            
            if (letterCount1<letterCount2)
            {
              //  console.log("compare "+word1+" "+word2);
              //  console.log("not enough "+letter);
                return false;
            }
        }
        return true;
    }
    checkSingle(word1:string,word2:string)
    {
       let wArray1:string[]=word1.split('');
       let wArray2:string[]=word2.split('');

       wArray1.sort();
       wArray2.sort();

     //  console.log(wArray1);
     //  console.log(wArray2);

       const result = wArray2.every(val => wArray1.includes(val));

       return result;
    }
}