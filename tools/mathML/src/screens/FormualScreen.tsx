import React, { Component } from 'react';
interface MyProps { }
interface MyState { text: string }
class FormulaScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { text: "" };
        }
    onTextChange(e: any) {
        this.setState({ text: e.target.value },this.updateFormulaOutput.bind(this));
    }
    getMathML() {
        let text: string = this.state.text;
        //convert text to LaTeX
        
      


    }
    updateFormulaOutput() {
        let formulaOutput = document.getElementById("formulaOutput");
        if (formulaOutput) {
            formulaOutput.innerHTML = this.getMathML();
        }
    }
    render() {
        return (<div>
            <div className='formulaScreen'>
                <div className='formulaOutput'id="formulaOutput"></div>
                <input type='text' value={this.state.text} onChange={this.onTextChange.bind(this)} />
            </div>
        </div>)
    }
}

export default FormulaScreen;