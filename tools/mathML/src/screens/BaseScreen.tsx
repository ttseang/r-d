import React, { Component } from 'react';

import FormulaScreen from './FormualScreen';
interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0 };
    }

    getScreen() {
        switch (this.state.mode) {
            case 0:
                return <FormulaScreen></FormulaScreen>
        }
    }
    render() {
        return (<div>
            {this.getScreen()}
        </div>)
    }
}
export default BaseScreen;