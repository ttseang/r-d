import './App.css';
import BaseScreen from './screens/BaseScreen';

function App() {
  return (
    <div className="App">
      <BaseScreen />
    </div>
  );
}

export default App;
