import React, { Component } from 'react';
import { PageVo } from '../classes/dataObjs/PageVo';
import MainStorage from '../classes/MainStorage';
import { TemplateData } from '../classes/TemplateData';
interface MyProps { pageVo: PageVo }
interface MyState { pageVo: PageVo }
class PreviewPage extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { pageVo: this.props.pageVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ pageVo: this.props.pageVo });
        }
    }
    getPath(image: string) {
        return "./images/bookimages/" + image;
    }

    getPage() {

        let texts: string[] = this.state.pageVo.texts;

        let images: string[] = this.state.pageVo.images.slice();

        for (let i: number = 0; i < images.length; i++) {
            images[i] = this.getPath(images[i]);
        }

        console.log(images);
        let td: TemplateData = new TemplateData();
        return td.getPage(this.state.pageVo.templateID, texts, images);


    }


    render() {
        return (<svg viewBox='0 0 300 450' height={450} width={300}>{this.getPage()}</svg>)
    }
}
export default PreviewPage;