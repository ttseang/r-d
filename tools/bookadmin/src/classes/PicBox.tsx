import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { WordPicVo } from '../classes/dataObjs/WordPicVo';
import { MainController } from './MainController';
import MainStorage from './MainStorage';
interface MyProps { wordPicVo: WordPicVo }
interface MyState { }
class PicBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getPic() {
        return "./images/bookimages/" + this.props.wordPicVo.image;
    }
    changePic()
    {
        
        this.mc.mediaChangeCallback(this.props.wordPicVo);
    }
    render() {
        return (<div>
            <Card className='picCard'>
                <Card.Body>
                    <div className='cardImageHolder'>
                        <img className='cardImage' alt={this.props.wordPicVo.word} src={this.getPic()}  onClick={()=>{this.changePic()}} />
                    </div>
                </Card.Body>
                <Card.Footer className="cardHeader">{this.props.wordPicVo.word}</Card.Footer>
            </Card>
        </div>)
    }
}
export default PicBox;