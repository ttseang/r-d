
export class MainController
{
    private static instance:MainController;

    public openTextEdit:Function=()=>{};
    public closeTextEdit:Function=()=>{};
    public onTextEditClose:Function=()=>{};
    public textEditCallback:Function=()=>{};
    //
    //
    //
    public openImageBrowse:Function=()=>{};
    public closeImageBrowse:Function=()=>{};
    public onImageBrowseClose:Function=()=>{};
    public mediaChangeCallback:Function=()=>{};


    public static getInstance(): MainController {
        if (this.instance === undefined || this.instance === null) {
          this.instance = new MainController();
        }
    
        return this.instance;
      }

}