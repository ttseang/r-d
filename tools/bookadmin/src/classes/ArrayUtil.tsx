export class ArrayUtil {
  public static mixUp(myArray: Array<string>) {
    for (let i: number = 0; i < 50; i++) {
      let place1: number = Math.floor(Math.random() * myArray.length);
      let place2: number = Math.floor(Math.random() * myArray.length);

      let temp: string = myArray[place1];
      myArray[place1] = myArray[place2];
      myArray[place2] = temp;
    }
    return myArray;
  }
}
