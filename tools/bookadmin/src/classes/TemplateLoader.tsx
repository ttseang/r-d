import MainStorage from "./MainStorage";

export class TemplateLoader {
    private callback: Function;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(callback: Function) {
        this.callback = callback;
    }
    loadTemplates() {
        fetch("./templates/square.xml")
            .then(response => response.text())
            .then(data => this.templatesLoaded(data));
    }
    templatesLoaded(data: string) {
        //console.log(data);
        const parser:DOMParser = new DOMParser();
        const xml:Document = parser.parseFromString(data, "application/xml");
       // console.log(xml);
        let pages:HTMLCollectionOf<SVGForeignObjectElement>=xml.getElementsByTagName("foreignObject");
        console.log(pages);

        let pages2:SVGForeignObjectElement[]=Array.prototype.slice.call(pages);

        for (let i:number=0;i<pages2.length;i++)
        {
        
            //this.ms.templateMap.set("page"+pages[i].id,pages2[i]);
        }
        console.log(this.ms);
        this.callback();
    }
}