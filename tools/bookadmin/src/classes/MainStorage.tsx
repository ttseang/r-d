import { BookTempVo } from "./dataObjs/BookTempVo";
import { EditVo } from "./dataObjs/EditVo";
import { PageTempVo } from "./dataObjs/PageTempVo";
import { PageVo } from "./dataObjs/PageVo";
import { WordPicVo } from "./dataObjs/WordPicVo";


export class MainStorage {
  private static instance: MainStorage;

  public pageTemps: PageTempVo[] = [];
  public bookTemps: BookTempVo[] = [];


  public appType:string="zoomy";
  

  public bookInfo: BookTempVo | null = null;

  public pages: PageVo[] = [];
  public pageEditIndex: number = 0;
  public itemEditIndex: number = 0;

  public editText: string = "";
  public editVo: EditVo | null = null;

  public library: WordPicVo[] = [];
  public libTag:string="";
  //public lastTab: number = 0;
  

 
  //public templateMap:Map<string,SVGForeignObjectElement>=new Map<string,SVGForeignObjectElement>();

  public bookID: number = 1;

  constructor() {

    this.bookTemps.push(new BookTempVo(0, "Square Book", 300, 300, "cover2.jpg"));
    this.bookTemps.push(new BookTempVo(1, "Rectange Book", 300, 450, "cover1.jpg"));

    //TO DO move to JSON

    this.library.push(new WordPicVo("fairy cover", "fairy_cover.svg", false));
    this.library.push(new WordPicVo("fairy1", "fairy1.svg", false));
    this.library.push(new WordPicVo("fairy2", "fairy2.svg", false));
    this.library.push(new WordPicVo("fairy3", "fairy3.svg", false));
    this.library.push(new WordPicVo("frame1", "frame1.svg", false));
    this.library.push(new WordPicVo("greek1", "greek1.svg", false));
    this.library.push(new WordPicVo("greek2", "greek2.svg", false));
    this.library.push(new WordPicVo("greek3", "greek3.svg", false));
    this.library.push(new WordPicVo("greek4", "greek4.svg", false));

    this.library.push(new WordPicVo("test1", "test1.jpg", false));
    this.library.push(new WordPicVo("test2", "test2.jpg", false));
    this.library.push(new WordPicVo("brushes", "brushes.png", false));
    this.library.push(new WordPicVo("cars", "cars.png", false));
    this.library.push(new WordPicVo("flowers", "flowers.png", false));

    this.library.push(new WordPicVo("wienerdog", "wienerdog.png", false));
    this.library.push(new WordPicVo("sub", "sub.png", false));
    this.library.push(new WordPicVo("burger", "burger.png", false));
    

    this.library.push(new WordPicVo("bear", "bear.png", false));
    this.library.push(new WordPicVo("bread", "bread.svg", false));
    this.library.push(new WordPicVo("bun", "bun.svg", false));
    this.library.push(new WordPicVo("cat", "cat.svg", false));
    this.library.push(new WordPicVo("chair", "chair.svg", false));
    this.library.push(new WordPicVo("chicken", "chicken.png", false));
    this.library.push(new WordPicVo("cow", "cow.png", false));
    this.library.push(new WordPicVo("crab", "crab.png", false));
    this.library.push(new WordPicVo("croc", "croc.png", false));
    this.library.push(new WordPicVo("duck", "duck.png", false));
    this.library.push(new WordPicVo("glass", "glass.svg", false));
    this.library.push(new WordPicVo("hammerhead", "hammerhead.png", false));
    this.library.push(new WordPicVo("hat", "hat.svg", false));
  }


  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }

    return this.instance;
  }
  selectBookTemp(id: number) {
    for (let i: number = 0; i < this.bookTemps.length; i++) {
      if (this.bookTemps[i].id === id) {
        this.bookInfo = this.bookTemps[i];
        this.bookID=id;
      //  this.setDefaultData();
      }
    }
    console.log(this.bookInfo);
  }
  setDefaultData() {


    if (this.bookInfo === null) {
      return
    }

    let textDefaults: string[] = ["The Fairy girl", "Once upon a time there was a little fairy girl who loved to dance.", "She would dance around her tree, and sing to the birds, and make new friends with the flowers."]


    switch (this.bookInfo.id) {
      case 0:

        this.pageTemps.push(new PageTempVo(0, "Center Image", 0, 1, "1.jpg", textDefaults, ["fairy_cover.svg"]));
        this.pageTemps.push(new PageTempVo(1, "Left Image", 3, 1, "2.jpg", textDefaults, ["fairy3.svg"]));
        this.pageTemps.push(new PageTempVo(2, "Right Image", 3, 1, "3.jpg", ["The Tree",
          "But one day she left her tree. And she walked out of the forest...",
          "And when she came back the tree was gone!"], ["tree2.svg"]));
        this.pageTemps.push(new PageTempVo(3, "Small Image", 3, 1, "4.jpg", [
          "The Tree",
          "Don't be so silly said the little girl Trees can not walk. They have to stay in one place",
          "'Not so', said the 2nd fairy, 'They can walk, and they can talk. Trees know how to make the earth happy. If you want to be happy, then you should listen to trees talk.'"
        ], ["fairy2.svg"]));
        break;

        case 1:

          let ipsum:string[]=["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut iaculis eros. Morbi accumsan metus quis turpis mattis, nec bibendum tortor vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit."];
          
          this.pageTemps.push(new PageTempVo(0, "Left Edge", 1, 1, "1.jpg", ipsum, ["brushes.png"]));
          this.pageTemps.push(new PageTempVo(1, "Right Edge", 1, 1, "2.jpg", ipsum, ["brushes.png"]));

          this.pageTemps.push(new PageTempVo(4, "Cross Gutter Left", 1, 1, "6.jpg", ipsum, ["wienerdog.png"]));
          this.pageTemps.push(new PageTempVo(5, "Cross Gutter Right", 1, 1, "5.jpg", ipsum, ["wienerdog.png"]));

          this.pageTemps.push(new PageTempVo(2, "Bottom Images", 1, 2, "3.jpg", ipsum, ["test1.jpg","test2.jpg"]));
          this.pageTemps.push(new PageTempVo(3, "Bottom Images2", 3, 1, "4.jpg", ipsum, ["test3.jpg","test4.jpg"]));
         
        break;
    }
  }
  /* getHtmlTemplate(id:number)
  {
    let key:string="pagept"+id.toString();
    if (this.templateMap.has(key))
    {
      return this.templateMap.get(key);
    }
    else
    {
       return (<foreignObject x="0" y="0" width="300" height="300" id="pt4">Not Found</foreignObject>)
    }
  } */
  getTemplateByID(id: number) {
    for (let i: number = 0; i < this.pageTemps.length; i++) {
      if (this.pageTemps[i].id === id) {
        return this.pageTemps[i];
      }
    }
    return this.pageTemps[0];
  }
  
}
export default MainStorage;
