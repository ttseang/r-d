export class PageTempVo
{
    public id:number;
    public title:string;
    public textCount:number;
    public imageCount:number;
    public thumb:string;

    public defImages:string[];
    public defText:string[];

    constructor(id:number,title:string,textCount:number,imageCount:number,thumb:string,defText:string[],defImages:string[])
    {
        this.id=id;
        this.title=title;
        this.textCount=textCount;
        this.imageCount=imageCount;
        this.thumb=thumb;
        this.defImages=defImages;
        this.defText=defText;
    }
}