export class BookTempVo
{
    public id:number;
    public name:string;
    public w:number;
    public h:number;
    public image:string;

    constructor(id:number,name:string,w:number,h:number,image:string)
    {
        this.id=id;
        this.name=name;
        this.w=w;
        this.h=h;
        this.image=image;
    }
}