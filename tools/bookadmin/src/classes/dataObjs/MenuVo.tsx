export class MenuVo
{
    public text:string;
    public action:number;
    public variant:string;

    constructor(text:string,action:number,variant:string)
    {
        this.text=text;
        this.action=action;
        this.variant=variant;
    }
}