export class WordPicVo
{
    public word:string;
    public image:string;
    public hasSound:boolean;

    constructor(word:string,image:string,hasSound:boolean)
    {
        this.word=word;
        this.image=image;
        this.hasSound=hasSound;
    }
}