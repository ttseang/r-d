export class PageVo
{
    public templateID:number;
    public texts:string[];
    public images:string[];
    public title:string;

    constructor(title:string,templateID:number,texts:string[],images:string[])
    {
        this.templateID=templateID;
        this.texts=texts;
        this.images=images;
        this.title=title;
    }
    clone()
    {
        return new PageVo(this.title,this.templateID,this.texts,this.images);
    }
}