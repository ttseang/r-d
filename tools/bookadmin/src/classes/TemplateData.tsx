import MainStorage from "./MainStorage";

export class TemplateData {
    private ms: MainStorage = MainStorage.getInstance();

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {

    }
    public getPage(index: number, texts: string[], images: string[]) {
        switch (this.ms.bookID) {
            case 0:

                switch (index) {

                    case 0:

                        return (<foreignObject x="0" y="0" width="300" height="300" id="pt4">
                            <div className="pg5 right oldpaper HTMNHL">
                                <figure className="fullImage" style={{ backgroundImage: 'url(' + images[0] + ')' }}></figure>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div>
                        </foreignObject>)

                    case 1:

                        return (<foreignObject className="pageStart" x="0" y="0" width="300" height="300" id="pt1">
                            <div className="pg2 left oldpaper HTMNHL">

                                <figure className="pgbg" style={{ backgroundImage: 'url(' + images[0] + ')' }}></figure>
                                <h1 data-nhl="pg2h1">{texts[0]}</h1>
                                <article className="righthalf">
                                    <p data-nhl="pg2p1">{texts[1]}</p>
                                    <p data-nhl="pg2p2">{texts[2]}</p>
                                </article>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div>
                        </foreignObject>)
                    case 3:
                        return (<foreignObject x="0" y="0" width="300" height="300" id="pt3" className='pageStart'>
                            <div className="pg3 right oldpaper HTMNHL">
                                <h1 data-nhl="pg3h1">{texts[0]}</h1>
                                <article>
                                    <p data-nhl="pg3p1">{texts[1]}</p>
                                    <figure className="spotart" style={{ backgroundImage: 'url(' + images[0] + ')' }}></figure>
                                    <p data-nhl="pg3p2">{texts[2]}</p>
                                </article>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div>
                        </foreignObject>)
                    case 2:

                        return (<foreignObject className="pageStart" x="0" y="0" width="300" height="300" id="pt2">
                            <div className="pg4 left oldpaper HTMNHL">
                                <figure className="pgbg" style={{ backgroundImage: 'url(' + images[0] + ')' }}></figure>
                                <h1 data-nhl="pg2h1">{texts[0]}</h1>
                                <article className="lefthalf">
                                    <p data-nhl="pg2p1">{texts[1]}</p>
                                    <p data-nhl="pg2p2">{texts[2]}</p>
                                </article>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div>
                        </foreignObject>)

                }
                break;
            //BOOK 2 300x450
            case 1:

                switch (index) {
                    case 0:

                        return (<foreignObject className="pageStart2" x="0" y="0" width="300" height="450" id="pt1">
                            <div className="pg2 paper1 rbook">
                                <p className="middleText">{texts[0]} </p>
                                <div className="leftEdge" style={{ backgroundImage: 'url(' + images[0] + ')' }}></div>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div></foreignObject>)

                    case 1:
                        return (<foreignObject className="pageStart2" x="0" y="0" width="300" height="450" id="pt2">
                            <div className="pg2 paper1 rbook">
                                <p className="middleText2">{texts[0]}</p>
                                <div className="rightEdge flipped" style={{ backgroundImage: 'url(' + images[0] + ')' }}></div>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div>
                        </foreignObject>)

                    case 2:

                        return (<foreignObject className="pageStart2" x="0" y="0" width="300" height="450" id="pt3">
                            <div className="pg2 paper2 rbook">

                                <p className="middleText3">{texts[0]}</p>

                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                                <div className="rpic1" style={{ backgroundImage: 'url(' + images[0] + ')' }}></div>
                                <div className="rpic2" style={{ backgroundImage: 'url(' + images[1] + ')' }}></div>
                            </div>
                        </foreignObject>)

                    case 3:

                        return (<foreignObject className="pageStart2" x="0" y="0" width="300" height="450" id="pt4">
                            <div className="pg2 paper3 rbook">

                                <p className="middleText3">{texts[0]}</p>

                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                                <div className="rpic3" style={{ backgroundImage: 'url(' + images[0] + ')' }}></div>
                                <div className="rpic4" style={{ backgroundImage: 'url(' + images[1] + ')' }}></div>
                            </div>
                        </foreignObject>)

                    case 4:

                        return (<foreignObject className="pageStart2" x="0" y="0" width="300" height="450" id="pt5">
                            <div className="pg2 paper1 rbook">

                                <p className="middleText4">{texts[0]}</p>
                                <div className="crossGutter crossGutterL" style={{ backgroundImage: 'url(' + images[0] + ')' }}></div>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div>
                        </foreignObject>)

                    case 5:
                        return (<foreignObject className="pageStart2" x="0" y="0" width="300" height="450" id="pt6">
                            <div className="pg2 paper1 rbook">

                                <p className="middleText4">{texts[0]}</p>
                                <div className="crossGutter crossGutterR" style={{ backgroundImage: 'url(' + images[0] + ')' }}></div>
                                <figure className="guttershadow"></figure>
                                <div className="playpausebtn play"></div>
                            </div>
                        </foreignObject>)
                }




        }
        return (<div></div>)
    }
}
