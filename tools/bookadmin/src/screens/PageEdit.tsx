import { ChangeEvent, Component } from 'react';
import { Button, Card, Col, ListGroup, Row } from 'react-bootstrap';
import { EditVo } from '../classes/dataObjs/EditVo';
import { PageTempVo } from '../classes/dataObjs/PageTempVo';
import { PageVo } from '../classes/dataObjs/PageVo';
import { WordPicVo } from '../classes/dataObjs/WordPicVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import EditRow from '../comps/EditRow';
import PreviewPage from '../comps/PreviewPage';

interface MyProps { pageTemp: PageTempVo, page: PageVo, updateCallback: Function,doneCallback:Function }
interface MyState { pageTemp: PageTempVo, page: PageVo,tabIndex:number }
class PageEdit extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { pageTemp: props.pageTemp, page: props.page,tabIndex:this.ms.lastTab };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (this.props !== oldProps) {
            this.setState({ pageTemp: this.props.pageTemp, page: this.props.page });
        }

    }
    getTextRows() {
        if (this.state.pageTemp.textCount===0)
        {
            return (<div>This page has no text</div>)
        }
        let editRows: JSX.Element[] = [];
        //editRows.push(<h3 key="head0">Texts</h3>)
        for (let i: number = 0; i < this.state.pageTemp.textCount; i++) {
            let key: string = "text" + i.toString();

            let text: string = "";
            if (this.state.page.texts.length > i - 1) {
                text = this.state.page.texts[i];
            }
            let editVo: EditVo = new EditVo("text", text);

            editRows.push(<EditRow key={key} callback={this.changeItem.bind(this)} index={i} editVo={editVo}></EditRow>)
        }
        return (<div id="scroll2"><ListGroup>{editRows}</ListGroup></div>);
    }
    getImageRows() {
        if (this.state.pageTemp.imageCount===0)
        {
            return (<div>This page has no images</div>)
        }
        let editRows: JSX.Element[] = [];
        // editRows.push(<h3 key="head1">Images</h3>)
        for (let j: number = 0; j < this.state.pageTemp.imageCount; j++) {
            let key: string = "image" + j.toString();

            let image: string = "";
            if (this.state.page.images.length > j - 1) {
                image = this.state.page.images[j];

            }
            let editVo: EditVo = new EditVo("image", image);
            editRows.push(<Col key={key} sm={4}><EditRow callback={this.changeItem.bind(this)} index={j} editVo={editVo}></EditRow></Col>)
        }
        return (<Row>{editRows}</Row>);

    }
    changeItem(index: number, e: EditVo) {
        console.log(index, e);

        this.mc.mediaChangeCallback = this.updateImage.bind(this)
        this.mc.textEditCallback = this.updateText.bind(this);

        this.ms.itemEditIndex = index;
     //   this.ms.editVo = e;

        if (e.type === "text") {
            this.ms.editText=e.content;
            this.mc.openTextEdit();

        }
        else {
            this.mc.openImageBrowse();
        }
    }
    updateImage(wordPic: WordPicVo) {
        console.log(wordPic);

        let pageVo: PageVo = this.props.page;
        pageVo.images[this.ms.itemEditIndex] = wordPic.image;
        this.props.updateCallback(pageVo);
       // this.setState({page:pageVo});
    }
    updateText(text: string) {
        let pageVo: PageVo = this.state.page;
        pageVo.texts[this.ms.itemEditIndex] =text;
        //this.setState({page:pageVo});
        this.props.updateCallback(pageVo);
    }
    changePageName(e:ChangeEvent<HTMLInputElement>)
    {
        let pageVo: PageVo = this.state.page;
        pageVo.title=e.currentTarget.value;
        this.setState({page:pageVo});
      //  this.props.updateCallback(pageVo);
    }
    getTextInput()
    {
        return (
            <div>
            <input
            type="text"
            className="form-control"
            placeholder=""
            onChange={this.changePageName.bind(this)}
            value={this.state.page.title}
          /></div>
        )
    }
    getButtons()
    {
        return (<div className='tac'><Button onClick={()=>{this.props.doneCallback()}}>Done</Button></div>)
    }
    getTabs()
    {
        let tabArray:JSX.Element[]=[];

        tabArray.push(<Col key="tab1" sm={3}><Button variant='light' onClick={()=>{this.setTab(0)}}>Page Name</Button></Col>);
        tabArray.push(<Col key="tab2" sm={3}><Button variant='light' onClick={()=>{this.setTab(1)}}>Text</Button></Col>);
        tabArray.push(<Col key="tab3" sm={3}><Button variant='light' onClick={()=>{this.setTab(2)}}>Images</Button></Col>);

        return (<div><Row>{tabArray}</Row></div>)
    }
    setTab(index:number)
    {
        this.ms.lastTab=index;
        this.setState({tabIndex:index});
    }
    getTabContent()
    {
        switch(this.state.tabIndex)
        {
            case 0:
            return this.getTextInput();

            case 1:
            return (<div>{this.getTextRows()}</div>);

            case 2:
            return (<div>{this.getImageRows()}</div>)
        }
    }
    getPreview()
    {
        let pSizeClasses:string[]=['preview','preview2'];
        let sizeClass:string=pSizeClasses[this.ms.bookID];
        return (<div id={sizeClass}><PreviewPage pageVo={this.state.page}></PreviewPage></div>)
    }
    render() {
        return (<div>
            {this.getPreview()}
            <hr/>           
            {this.getTabs()}
            <Card><Card.Body>{this.getTabContent()}</Card.Body></Card>         
            <hr/>
            {this.getButtons()}
        </div>)
    }
}
export default PageEdit;