import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { MenuVo } from '../classes/dataObjs/MenuVo';
import MainStorage from '../classes/MainStorage';
import PageEditRow from '../comps/PageEditRow';
interface MyProps { actionCallback: Function,editCallback:Function }
interface MyState { }
class BookMenuScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    private menu: MenuVo[] = [];
    constructor(props: MyProps) {
        super(props);
        this.state = {};
        this.menu.push(new MenuVo("Add A Page", 1, "primary"));
        this.menu.push(new MenuVo("Preview", 2, "success"));
    }
    getPageList() {
        if (this.ms.pages.length === 0) {
            return "No pages yet";
        }
        let pageList: JSX.Element[] = [];

        for (let i: number = 0; i < this.ms.pages.length; i++) {
            let key: string = "pagerow" + i.toString();
            pageList.push(<PageEditRow key={key} pageVo={this.ms.pages[i]} index={i} upCallback={this.moveUp.bind(this)} downCallback={this.moveDown.bind(this)} editCallback={this.props.editCallback} ></PageEditRow>);
        }
        return pageList;
    }
   
    moveUp()
    {

    }
    moveDown()
    {

    }
    getButtons() {
        let buttonArray: JSX.Element[] = [];
        for (let i: number = 0; i < this.menu.length; i++) {
            let key: string = "mbutton" + i.toString();

            buttonArray.push(<Button key={key} className="mbutton" variant={this.menu[i].variant} onClick={() => { this.props.actionCallback(this.menu[i].action) }}>{this.menu[i].text}</Button>)
        }
        return buttonArray;
    }
    render() {
        return (<div><Card><Card.Title>Pages</Card.Title><Card.Body>{this.getPageList()}</Card.Body></Card><hr/><div className='tac'>{this.getButtons()}</div></div>)
    }
}
export default BookMenuScreen;