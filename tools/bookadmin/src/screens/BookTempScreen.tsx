import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { BookTempVo } from '../classes/dataObjs/BookTempVo';
import MainStorage from '../classes/MainStorage';
import BookTempCard from '../classes/ui/BookTempCard';
interface MyProps {tempSelected:Function}
interface MyState {}
class BooKTempScreen extends Component <MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props:MyProps){
super(props);
this.state={};
}
getTemplates() {
    let cards: JSX.Element[] = [];

    for (let i: number = 0; i < this.ms.bookTemps.length; i++) {
        let bookTemp:BookTempVo= this.ms.bookTemps[i];
        let key:string="template "+bookTemp.id.toString();

        cards.push(<Col sm={6} key={key}><BookTempCard selectCallback={this.selectBook.bind(this)} bookTempVo={bookTemp}></BookTempCard></Col>)
    }
    return cards;
}
selectBook(tempID:number)
{
    this.ms.selectBookTemp(tempID);
    this.props.tempSelected();
}
render()
{
return (<div><Row>{this.getTemplates()}</Row></div>)
}
}
export default BooKTempScreen;