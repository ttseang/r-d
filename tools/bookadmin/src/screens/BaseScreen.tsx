import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { PageTempVo } from '../classes/dataObjs/PageTempVo';
import { PageVo } from '../classes/dataObjs/PageVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import AddPageScreen from './AddPageScreen';
import BookMenuScreen from './BookMenuScreen';
import BooKTempScreen from './BookTempScreen';
import MediaScreen from './MediaScreen';
import PageEdit from './PageEdit';
import TextEditor from './TextEditor';
interface MyProps { }
interface MyState { mode: number, showTextEdit: boolean, showImageBrowse: boolean }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 3, showTextEdit: false, showImageBrowse: false };
        this.mc.openImageBrowse = this.openMeida.bind(this);
        this.mc.closeImageBrowse = this.closeMedia.bind(this);
        this.mc.openTextEdit = this.openText.bind(this);
        this.mc.closeTextEdit = this.closeText.bind(this);
    }

    getScreen() {
        if (this.state.showImageBrowse === true || this.state.showTextEdit === true) {
            return;
        }
        switch (this.state.mode) {
            case -1:
                return "loading";
            case 0:
                return <AddPageScreen addCallback={this.onPageAdded.bind(this)} cancelCallback={() => { this.setState({ mode: 2 }) }}></AddPageScreen>
            case 1:

                let editPage: PageVo = this.ms.pages[this.ms.pageEditIndex];
                let pageEditTemp: PageTempVo = this.ms.getTemplateByID(editPage.templateID);

                return <PageEdit pageTemp={pageEditTemp} page={editPage} updateCallback={this.updatePage.bind(this)} doneCallback={this.editDone.bind(this)}></PageEdit>

            case 2:
                return <BookMenuScreen actionCallback={this.doBookMenuAction.bind(this)} editCallback={this.editPage.bind(this)}></BookMenuScreen>

            case 3:
                return <BooKTempScreen tempSelected={this.tempSelected.bind(this)}></BooKTempScreen>
        }
        return;
    }

    tempSelected() {
        this.setState({ mode: 2 })
    }
    editPage(index: number) {
        this.ms.pageEditIndex = index;
        this.setState({ mode: 1 });
    }
    doBookMenuAction(action: number) {
        switch (action) {
            case 1:
                this.setState({ mode: 0 });
                break;

            case 2:
                alert("preview");

        }
    }
    onPageAdded() {
        this.setState({ mode: 1 });
    }
    editDone() {
        this.setState({ mode: 2 });
    }
    updatePage(pageVo: PageVo) {
        this.ms.pages[this.ms.pageEditIndex] = pageVo;
        this.closeText();
        this.closeMedia();
    }
    openText() {
        this.setState({ showTextEdit: true });
    }
    openMeida() {
        this.setState({ showImageBrowse: true });
    }
    closeMedia() {
        this.setState({ showImageBrowse: false });

    }
    closeText() {
        this.setState({ showTextEdit: false });
    }
    getTextEdit() {
        if (this.state.showTextEdit) {
            return (<TextEditor closeCallback={this.closeText.bind(this)}></TextEditor>)
        }
    }
    getMediaScreen() {
        if (this.state.showImageBrowse) {
            return (<MediaScreen library={this.ms.library} closeCallback={this.closeMedia.bind(this)}></MediaScreen>)
        }
        return "";
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Flipping Book Maker</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                    {this.getMediaScreen()}
                    {this.getTextEdit()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;