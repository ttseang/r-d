import React, { Component } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { PageTempVo } from '../classes/dataObjs/PageTempVo';
import { PageVo } from '../classes/dataObjs/PageVo';
import MainStorage from '../classes/MainStorage';
import TempCard from '../classes/ui/TempCard';
interface MyProps { addCallback: Function,cancelCallback:Function}
interface MyState { }
class AddPageScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getTemplates() {
        let cards: JSX.Element[] = [];

        for (let i: number = 0; i < this.ms.pageTemps.length; i++) {
            let pageTemp: PageTempVo = this.ms.pageTemps[i];
            
            let key:string="template "+pageTemp.id.toString();

            cards.push(<Col sm={6} key={key}><TempCard addCallback={this.addPage.bind(this)} pageTempVo={pageTemp}></TempCard></Col>)
        }
        return cards;
    }
    addPage(title:string,template: PageTempVo) {
      //  console.log("Temp ID="+template.id);      
        let page: PageVo = new PageVo(title,template.id, template.defText, template.defImages);
        this.ms.pages.push(page);
        this.ms.pageEditIndex = this.ms.pages.length - 1;
        this.props.addCallback();
    }
    render() {
        return (<div>
            <h4 className='tac'>Add New Page</h4>
            <hr/>
            <div className="scroll1"><Row>{this.getTemplates()}</Row></div>
            <Row><Col className="tac"><Button variant='danger' onClick={()=>{this.props.cancelCallback()}}>Cancel</Button></Col></Row>
            </div>)
    }
}
export default AddPageScreen;