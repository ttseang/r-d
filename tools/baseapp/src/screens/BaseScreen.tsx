import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
interface MyProps { }
interface MyState {mode:number}
class BaseScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {mode:0};
        }
    getScreen()
    {
        switch(this.state.mode)
        {
            case 0:
                return "Loading...";

            default:
                return <h3>content here</h3>
        }
        
    }
    render() {
        return (<div className='base'>
            <Card>
                <Card.Header>Header</Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
                <Card.Footer>Footer</Card.Footer>
            </Card>
        </div>)
    }
}
export default BaseScreen;