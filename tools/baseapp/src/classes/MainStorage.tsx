/* eslint-disable @typescript-eslint/no-useless-constructor */
export class MainStorage {
    private static instance: MainStorage;
    public static getInstance(): MainStorage {
        if (!MainStorage.instance) {
            MainStorage.instance = new MainStorage();
        }
        return MainStorage.instance;
    }

    constructor() {

    }
}