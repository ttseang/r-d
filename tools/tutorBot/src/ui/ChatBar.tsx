import React, { Component } from 'react';
import { Col, ListGroupItem, Row } from 'react-bootstrap';
import { ChatVo } from '../blogData/ChatVo';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { chatVo: ChatVo,extractCode:Function,doDownload:Function,doCopy:Function,doSpeak:Function}
interface MyState { }
class ChatBar extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
        constructor(props: MyProps) {
            super(props);
            this.state = {  };
        }
    getIconBar()
    {
        if (this.props.chatVo.role !== "system" && this.props.chatVo.role !== "user") {

            let icons:JSX.Element[]=[];
            if (this.ms.showCodeArea===true)
            {
                icons.push(<Col key="iconCode"><i className="fas fa-code" title='get code' onClick={()=>{this.props.extractCode()}}></i></Col>);
            }

            icons.push(<Col key="iconCopy"><i className="fas fa-copy" title='copy' onClick={()=>{this.props.doCopy()}}></i></Col>);
            icons.push(<Col key="iconDownload"><i className="fas fa-file" title='download' onClick={()=>{this.props.doDownload()}}></i></Col>);
            icons.push(<Col key="iconSpeak"><i className="fas fa-volume-up" title='speak' onClick={()=>{this.props.doSpeak()}}></i></Col>);

            return (
                <Row>
                {icons}        
                </Row>
            )
        }
    }
    
    getAvatarImage()
    {
        if (this.props.chatVo.role === "assistant") {
            return (<img src={this.ms.avatarImage} title={this.ms.avatarName} alt="Avatar" className="msgAvatar"></img>)
        }
    }
    render() {
        let chatVo: ChatVo = this.props.chatVo;
        let variant: string = "light";
        if (chatVo.role === "assistant") {
            variant = "success";
        }

        return (<ListGroupItem variant={variant} className="chatLine">
            <Row><Col>{this.getAvatarImage()}</Col></Row>
            <Row>
            <Col>{chatVo.content}</Col>
            </Row>
            {this.getIconBar()}           
            </ListGroupItem>)
    }
}
export default ChatBar;