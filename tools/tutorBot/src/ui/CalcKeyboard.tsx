import React, { Component } from 'react';
interface MyProps { callback: Function }
interface MyState { expression: string }
class CalcKeyboard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { expression: "" };
    }
    onButtonClicked(e: any) {
        console.log(e.target.value);
        let val: string = e.target.innerHTML;
        if (val === "/") {
            val = "÷";
        }
        if (val === "CE") {
            this.setState({ expression: "" });
        }
        else if (val === "Del") {
            this.setState({ expression: this.state.expression.substring(0, this.state.expression.length - 1) });
        }
        else if (val === "E") {
            // this.setState({expression:this.state.expression+"="});
        }
        else if (val === "Go") {
            console.log("Send");
            this.props.callback(this.state.expression);
            //this.setState({expression:this.state.expression+"-"});
        }
        else {
            this.setState({ expression: this.state.expression + val });
        }
    }
    render() {
        //calculator keyboard
        //put in enter back and clear buttons
        //each button should call onButtonClicked
        return (<div className='calHolder'>
            <input type="text" className="calText" value={this.state.expression} readOnly></input>

            <div className="calc">


                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>7</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>8</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>9</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>÷</div>


                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>4</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>5</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>6</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>*</div>


                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>1</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>2</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>3</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>-</div>


                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>0</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>.</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>=</div>
                <div className="cbutton" onClick={this.onButtonClicked.bind(this)}>+</div>

                <div className="cbutton smalltext" onClick={this.onButtonClicked.bind(this)}>/</div>
                <div className="cbutton smalltext" onClick={this.onButtonClicked.bind(this)}>%</div>
                <div className="cbutton smalltext" onClick={this.onButtonClicked.bind(this)}>.</div>
                <div className="cbutton smalltext" onClick={this.onButtonClicked.bind(this)}>?</div>

                
                <div className="cbutton smalltext" onClick={this.onButtonClicked.bind(this)}>CE</div>
                <div className="cbutton smalltext" onClick={this.onButtonClicked.bind(this)}>Del</div>
                <div className="cbutton smalltext greenButton" onClick={this.onButtonClicked.bind(this)}>Go</div>


            </div>
        </div>)
    }
}
export default CalcKeyboard;