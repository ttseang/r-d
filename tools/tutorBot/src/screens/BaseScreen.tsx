import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

import { MainStorage } from '../classes/MainStorage';
import { StartData } from '../classes/StartData';
//import { StartData } from '../classes/StartData';
import ChatScreen from './ChatScreen';
import StartScreen from './StartScreen';

interface MyProps { }
interface MyState {mode:number}
class BaseScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private catID:number=0;
    private listID:number=0;
    

        constructor(props: MyProps) {
            super(props);
            this.state={mode:0}
        }
    componentDidMount()
    {
       
        
    }
    dataLoaded()
    {
        this.setState({mode:2});
    }
  
    goBack()
    {
       /*  switch(this.state.mode)
        {
            case 2:
                this.setState({mode:1});
            break;

            case 3:
                this.setState({mode:2});
            break;
        } */
    }
    getScreen()
    {
        switch(this.state.mode)
        {
            case 0:
                return <StartScreen startChat={this.botSelected.bind(this)}></StartScreen>
            case 1:
                return <div>loading</div>
            case 2:

             return <ChatScreen></ChatScreen>

            
        }
    }
    botSelected()
    {
        let sd:StartData=new StartData(this.dataLoaded.bind(this));
        sd.getBotData();
    }
    render() {
        return (<div id="base"><Card><Card.Header>{this.ms.appTitle}</Card.Header><Card.Body>{this.getScreen()}</Card.Body></Card></div>)
    }
}
export default BaseScreen;