import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { BotSelection } from '../blogData/BotSelection';
import { MainStorage } from '../classes/MainStorage';
interface MyProps {startChat:Function}
interface MyState { bots: BotSelection[], selectedIndex: number }
class StartScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
        constructor(props: MyProps) {
            super(props);
            this.state = { bots: [], selectedIndex: 0 };
        }
    componentDidMount() {
        this.getListOfBots();
    }
    getListOfBots() {
        fetch("./bots/bots.json")
            .then(response => response.json())
            .then(data => this.gotList({ data }));
    }
    gotList(data: any) {
        console.log(data);
        console.log(data.data);
        let bots = [];
        for (let i = 0; i < data.data.bots.length; i++) {
            let bot = data.data.bots[i];
            console.log(bot);
            let botSelection: BotSelection = new BotSelection(bot.name, bot.avatar,bot.file);
            bots.push(botSelection);
        }

        this.setState({ bots: bots });
    }
    getBotDropDown() {
        let list = [];
        for (let i = 0; i < this.state.bots.length; i++) {
            let bot = this.state.bots[i];
            let key: string = "bot" + i;
            list.push(<option key={key} value={bot.name}>{bot.name}</option>);
        }
        return list;
    }
    //get the index from the react event
    //change event
    selectBot(event: any) {
        let index = event.target.selectedIndex;
        console.log(index);
        this.setState({ selectedIndex: index });
    }
    getAvatarImage() {
        if (this.state.bots.length === 0) {
            return <div></div>
        }
        console.log(this.state.selectedIndex);
        //return "";
         return <img src={this.state.bots[this.state.selectedIndex].image} className="selectAvatar" alt={this.state.bots[this.state.selectedIndex].name}></img>
    }
    startChat()
    {
        this.ms.bot_to_load=this.state.bots[this.state.selectedIndex].file;
        this.props.startChat();
    }
    render() {
        return (<div>
            <h2>Choose A Bot</h2>
            {this.getAvatarImage()}
                <select onChange={this.selectBot.bind(this)}>
                    {this.getBotDropDown()}
            </select>
            <div>
            <Button variant='success' onClick={this.startChat.bind(this)}>Go</Button>
            </div>
        </div>)
    }
}
export default StartScreen;