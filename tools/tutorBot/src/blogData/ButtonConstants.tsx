import { ButtonVo } from "../blogData/ButtonVo";

export class ButtonConstants {
  
  static LIST_BUTTONS: ButtonVo[] = [
    new ButtonVo("Default","dark",2),
    new ButtonVo("Sort A-Z", "primary", 0),
    new ButtonVo("Sort Z-A", "warning", 1),    
    new ButtonVo("Copy","success",4),
    new ButtonVo("Print","success",5),
    new ButtonVo("Advanced", "info", 3)
  ];
  static ADV_BUTTONS: ButtonVo[] = [
    new ButtonVo("Join ,", "secondary", 0),
    new ButtonVo("Join ;", "secondary", 1),
    new ButtonVo("Join |", "secondary", 4),
    new ButtonVo("Join ,(space)", "secondary",5),
  ];
  /*  <Button variant="primary" onClick={this.sortList.bind(this)}>Sort A-Z</Button>
            <Button variant="success" onClick={this.sortList2.bind(this)}>Sort Z-A</Button>
            <Button variant="danger" onClick={this.removeDupes.bind(this)}>Remove Duplicates</Button>
            <Button variant="info" onClick={this.toggleAdv.bind(this)}>Advanced</Button>
            
            <Button variant="secondary" onClick={this.doJoin.bind(this)}>Join ,</Button>
            <Button variant="secondary" onClick={this.doJoin2.bind(this)}>Join ;</Button>
            <Button variant="secondary" onClick={this.splitList.bind(this)}>Split ,</Button>
            <Button variant="secondary" onClick={this.splitList2.bind(this)}>Split ;</Button>
            
            
            */
}
