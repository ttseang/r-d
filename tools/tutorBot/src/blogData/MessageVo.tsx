export class MessageVo
{
    public message:string;
    public variant:string;
    public icon:string;
    public isError:boolean;

    constructor(message:string,variant:string,icon:string,isError:boolean)
    {
        this.message=message;
        this.variant=variant;
        this.icon=icon;
        this.isError=isError;
    }
}