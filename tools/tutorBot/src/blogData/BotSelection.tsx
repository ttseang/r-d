export class BotSelection
{
    public name:string;
    public image:string;
    public file:string;
    constructor(name:string,image:string,file:string)
    {
        this.name=name;
        this.image=image;
        this.file=file;
    }
}