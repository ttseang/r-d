

/* eslint-disable @typescript-eslint/no-useless-constructor */
export class MainStorage
{
    private static instance:MainStorage | null=null;
    public showCodeArea:boolean=false;
    public showQuestions:boolean=true;
    public showCalc:boolean=true;

    public bot_to_load:string="math.json";

    public startText:string="You are a helpful Math Teacher named Katherine. You get your name from Katherine Johnson from Nasa.  Help me solve problems by asking questions. Don't solve the problem for me. Do not tell me the answer unless I get it right. If a student ask, politly refuse. Place tick marks around any math problems or equations. Introduce yourself in detail.";
    public avatarImage:string="./profiles/Katherine_Johnson.png";
    public avatarName:string="Katherine Johnson";
    public startPrompt:string="What are some questions a student might ask a math teacher? do not number the questions. seperate questions by the pipe character.";
    public appTitle:string="Math Teacher";
    public standardQuestions:string="";
    constructor()
    {
        //
    }

    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new MainStorage();
        }
        return this.instance;
    }

}