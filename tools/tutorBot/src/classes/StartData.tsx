
import { MainStorage } from "./MainStorage";

/* eslint-disable @typescript-eslint/no-useless-constructor */
export class StartData
{
    private ms:MainStorage=MainStorage.getInstance();
    private callback:Function;

    constructor(callback:Function)
    {
        this.callback=callback;
    }
   /*  getBot()
    {
        fetch("./main.json")
        .then(response => response.json())
        .then(data => this.gotBot({ data }));
    } */
    getBotData()
    {
       
        let bot_to_load:string=this.ms.bot_to_load+".json";
        console.log(bot_to_load);
        if (bot_to_load)
        {
            fetch("./bots/"+bot_to_load)
            .then(response => response.json())
            .then(data => this.gotBotData({ data }));
        }
    }
    gotBotData(data:any)
    {
        console.log(data);
        let avatarName:string=data.data.avatarName;
        let avatarImage:string=data.data.avatarImage;
        let startText:string=data.data.startText;
        let startPrompt:string=data.data.startPrompt;
        let appTitle:string=data.data.appTitle;
        let useCalculator:boolean=data.data.useCalculator;
        let useCode:boolean=data.data.useCode;
        let useQuestions:boolean=data.data.useQuestions;
        let standardQuestions:string=data.data.standardQuestions;
        
        this.ms.standardQuestions=standardQuestions;
        this.ms.showCalc=useCalculator;
        this.ms.showCodeArea=useCode;
        this.ms.showQuestions=useQuestions;

        this.ms.avatarName=avatarName;
        this.ms.avatarImage=avatarImage;
        this.ms.startText=startText;
        this.ms.startPrompt=startPrompt;
        this.ms.appTitle=appTitle;
        this.callback();
    }
   /* {
   "avatarName":"Katherine Johnson",
    "avatarImage":"./profiles/Katherine_Johnson.png",
    "startText":"You are a helpful Math Teacher named Katherine. You get your name from Katherine Johnson from Nasa.  Help me solve problems by asking questions. Don't solve the problem for me. Do not tell me the answer unless I get it right. If a student ask, politly refuse. Place tick marks around any math problems or equations. Introduce yourself in detail and ask the student what they would like help with.",
    "startPrompt":"What are some questions a student might ask a math teacher? do not number the questions. seperate questions by the pipe character.",
    "useCalculator":true,
    "useCode":false,
    "useQuestions":true
} */
}