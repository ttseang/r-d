import React, { Component } from 'react';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { }
interface MyState { }
class ExportScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    render() {
        let data:string=JSON.stringify(this.ms.problems);

        return (<div>
            <textarea readOnly={true} value={data} rows={10} style={{width:"100%"}}></textarea>
        </div>)
    }
}
export default ExportScreen;