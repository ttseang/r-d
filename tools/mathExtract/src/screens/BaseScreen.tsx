import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { MainStorage } from '../classes/MainStorage';
import { ProbVo } from '../dataObjs/ProbVo';
import ExportScreen from './ExportScreen';
import SlideScreen from './SlideScreen';
interface MyProps { }
interface MyState {mode:number,probs:ProbVo[]}
class BaseScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {mode:0,probs:[]};
        }
        componentDidMount()
        {
            let probs:ProbVo[]=this.ms.getData();
           // console.log(probs);

            if (probs.length===0)
            {
              //  console.log("EMPTY");
                this.loadJson();
            }
            else
            {
                this.setState({probs:probs,mode:1})
            }
           // 
        }
        loadJson()
        {
          //  console.log("LOAD JSON");
            fetch("./math5.json")
            .then(response => response.json())
            .then(data => this.process({ data }));
        }
        process(data:any)
        {
           // console.log(data);
           let problems:ProbVo[]=[];

            data=data.data;
            for (let i:number=0;i<data.length;i++)
            {
              //  console.log(data[i]);
                problems.push(new ProbVo(parseInt(data[i].lesson),parseInt(data[i].ref),data[i].problemID,data[i].instructions,""));
            }
            this.setState({probs:problems,mode:1});
        }

    getScreen()
    {
        switch(this.state.mode)
        {
            case 0:
               // return this.getInputButton();
               return "Loading";
            
             case 1:
                return <SlideScreen probs={this.state.probs} callback={()=>{this.setState({mode:2})}}></SlideScreen>

            case 2:
                return <ExportScreen></ExportScreen>
        }
        return "no screen";
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Title Here</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;