import React, { ChangeEvent, Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Controller } from '../classes/Controller';
import { MainStorage } from '../classes/MainStorage';
import { ProbVo } from '../dataObjs/ProbVo';
import EditCard from '../ui/EditCard';
interface MyProps { probs: ProbVo[], callback: Function }
interface MyState { probs: ProbVo[], index: number }
class SlideScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: Controller = Controller.getInstance();
    private searchIndex:number=0;

    constructor(props: MyProps) {
        super(props);
        this.state = ({ probs: this.props.probs, index: 0 })
    }
    componentDidMount() {
        document.addEventListener("keydown", this.doKeyboardShortcut.bind(this))
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.doKeyboardShortcut.bind(this))
    }
    doKeyboardShortcut(e: KeyboardEvent) {
        console.log(e.key);
        switch (e.key) {
            case "n":
                this.goNext();
                break;

            case "e":
                this.mc.doExtract();
                break;

            case "b":
                this.goPrev();
                break;

            case "f":
                this.startFindNext();
                break;

            case "c":
                this.mc.doClear();
                break;

            case "t":
                this.mc.takeOffFirst();
                break;
            case "w":
                this.mc.takeOffLast();
                break;
        }
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ probs: this.props.probs })
        }
    }
    cardChange(probs: ProbVo) {
        console.log(this.state.probs[0].problem);
        this.ms.setData(this.state.probs);
    }
    getCard() {
        if (this.state.probs !== null && this.state.probs.length > -1) {

            let prob: ProbVo = this.state.probs[this.state.index];
            console.log(prob);

            if (prob) {
                return <EditCard prob={prob} callback={this.cardChange.bind(this)}></EditCard>
            }
        }
        return "No Data";
    }
    goNext() {
        let index: number = this.state.index;
        index++;
        if (index < this.state.probs.length) {
            this.setState({ index: index });
        }
    }
    goPrev() {
        let index: number = this.state.index;
        index--
        if (index > 0) {
            this.setState({ index: index });
        }
    }

    updateIndex(e: ChangeEvent<HTMLInputElement>) {
        let nindex: number = parseInt(e.currentTarget.value);
        if (nindex > 0 && nindex < this.state.probs.length) {
            this.setState({ index: nindex })
        }
    }
    startFindNext()
    {
        this.searchIndex=this.state.index+1;
        this.findNext();
    }
    findNext()
    {
        let current:ProbVo=this.state.probs[this.searchIndex];
        if (!this.hasOp(current.instructions))
        {
            this.searchIndex++;
            console.log(this.searchIndex);
            if (this.searchIndex<this.state.probs.length)
            {
                this.findNext();
            }
            return;
        }
        this.setState({index:this.searchIndex});
    }
    hasOp(s:string)
    {
        let ops:string[]=["+","-","x","/"];
        for (let i:number=0;i<ops.length;i++)
        {
            if (s.includes(ops[i]))
            {
                return true;
            }
        }
        return false;
    }
    findLast() {
        let last: number = 0;

        for (let i: number = this.state.probs.length - 1; i > 0; i--) {
            if (last === 0 && this.state.probs[i].problem !== "") {
                last = i;
            }
        }
        this.setState({ index: last })
    }
    render() {
        return (<div>
            <div>{this.state.index}/{this.state.probs.length}</div>
            <div><input type='number' value={this.state.index} onChange={this.updateIndex.bind(this)}></input></div>

            <Row><Col>{this.getCard()}</Col></Row>
            <hr />
            <Row><Col sm={2}><Button onClick={this.goPrev.bind(this)}>Previous</Button></Col><Col sm={2} style={{ textAlign: "right" }}><Button variant='success' onClick={() => { this.props.callback() }}>Export</Button></Col><Col sm={2} style={{ textAlign: "right" }}><Button onClick={this.goNext.bind(this)}>Next</Button></Col><Col sm={3} style={{ textAlign: "right" }}><Button variant='warning' onClick={this.startFindNext.bind(this)}>Find Next</Button></Col><Col sm={2} style={{ textAlign: "right" }}><Button variant='warning' onClick={this.findLast.bind(this)}>Last</Button></Col></Row>
        </div>)
    }
}
export default SlideScreen;