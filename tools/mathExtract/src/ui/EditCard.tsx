import React, { Component } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Controller } from '../classes/Controller';
import { ProbVo } from '../dataObjs/ProbVo';
interface MyProps { prob: ProbVo, callback: Function }
interface MyState { prob: ProbVo }
class EditCard extends Component<MyProps, MyState>
{
    private mc:Controller=Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { prob: this.props.prob };
    }
    componentDidMount()
    {
        this.mc.doExtract=this.doExtract.bind(this);
        this.mc.doClear=this.doClear.bind(this);
        this.mc.takeOffFirst=this.takeFirstOff.bind(this);
        this.mc.takeOffLast=this.takeOffLast.bind(this);
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ prob: this.props.prob });
        }

    }
    takeFirstOff()
    {
        let prob:ProbVo=this.state.prob;
        let problem:string=prob.problem;
        let chars:string[]=problem.split("");
        chars.shift();
        problem=chars.join("");
        prob.problem=problem;
        this.setState({prob:prob});
    }
    takeOffLast()
    {
        let prob:ProbVo=this.state.prob;
        let problem:string=prob.problem;
        let chars:string[]=problem.split("");
        chars.pop();
        problem=chars.join("");
        prob.problem=problem;
        this.setState({prob:prob});
    }
    getInput() {
        return <input value={this.state.prob.problem} onChange={this.onProbChange.bind(this)} style={{ width: "200px" }} />
    }
    onProbChange(e: React.ChangeEvent<HTMLInputElement>) {
        let prob: ProbVo = this.state.prob;
        prob.problem = e.currentTarget.value;
        this.setState({ prob: prob });
        this.props.callback(prob);
    }
    doClear()
    {
        let prob:ProbVo=this.state.prob;
        prob.problem="";
        this.setState({prob:prob});
    }
    doExtract()
    {
        let prob:ProbVo=this.state.prob;

        let instructions:string=prob.instructions;
        instructions=instructions.replace("divided by","÷");
        
        instructions=instructions.replaceAll("_","");
        instructions=instructions.replaceAll("/","_");

        instructions=instructions.replaceAll(/[^0-9+\-x÷=_.$]/g,'');
        
       // console.log(instructions);


        let half1:string=instructions.substring(0,instructions.length/2);
        let half2:string=instructions.substring(instructions.length/2,instructions.length);
       /*  console.log(half1);
        console.log(half2); */

        if (half1===half2)
        {
            instructions=half1;
        }
        instructions=instructions.replace("x.","");
        if (instructions.substring(0,1)===".")
        {
            instructions=instructions.substring(1,instructions.length);
        }
        if (instructions.substring(instructions.length-1,instructions.length)===".")
        {
            instructions=instructions.substring(0,instructions.length-1);
        }

        prob.problem=instructions;
        this.props.callback(prob);
        this.setState({prob:prob});
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                <Row><Col>Lesson:{this.state.prob.lesson}</Col><Col>Reference:{this.state.prob.reference}</Col><Col>ProblemID:{this.state.prob.problemID}</Col></Row>
                <hr/>
                <Row><Col>Instructions:{this.state.prob.instructions}</Col></Row>
                <hr/>
                <Row><Col>Equation:{this.getInput()}</Col></Row>
            </Card.Body>
            <Card.Footer><div style={{float:"right"}}><Button size='sm' variant='success' onClick={this.doExtract.bind(this)}>Extract</Button></div></Card.Footer>
            </Card>
        </div>)
    }
}
export default EditCard;