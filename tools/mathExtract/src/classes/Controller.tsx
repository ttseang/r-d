export class Controller
{
    private static instance:Controller | null=null;

    public doExtract:Function=()=>{};
    public doClear:Function=()=>{};
    public takeOffFirst:Function=()=>{};
    public takeOffLast:Function=()=>{};

    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new Controller();
        }
        return this.instance;
    }
}