import { ProbVo } from "../dataObjs/ProbVo";

export class MainStorage
{
    private static instance:MainStorage | null=null;

    public problems:ProbVo[]=[];
    //math3 key problems
    //math4 key math4
    //math5 key math5
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor()
    {
        
    }
    public static getInstance():MainStorage
    {
        if (this.instance===null)
        {
            this.instance=new MainStorage();
            (window as any).ms=this.instance;
        }
        return this.instance;
    }

    setData(probs:ProbVo[])
    {
        this.problems=probs;
        localStorage.setItem("math5",JSON.stringify(probs));
    }
    getData()
    {
        if (localStorage.getItem("math5"))
        {
            let dataString:string=localStorage.getItem("math5") || "";

            console.log(dataString);

            let data:any=JSON.parse(dataString);
            let problems:ProbVo[]=[];

            for (let i:number=0;i<data.length;i++)
            {
                console.log(data[i]);
                problems.push(new ProbVo(data[i].lesson,data[i].reference,data[i].problemID,data[i].instructions,data[i].problem));
            }
            this.problems=problems;
           return problems;
        }
        return []
    }

} 