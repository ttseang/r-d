import IBaseScene from "../interfaces/IBaseScene";
import UIBlock from "../util/UIBlock";

export class AnimationPlayer extends UIBlock
{
    //private t: Phaser.GameObjects.Text;
    private bscene: IBaseScene;
    private scene:Phaser.Scene;
    private image:Phaser.GameObjects.Sprite;
    private animKey:string;

    constructor(bscene: IBaseScene, key:string,w:number=-1)
    {
        super();

        console.log(key);
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.image=this.scene.add.sprite(0,0,key);

        if (w!==-1)
        {
            this.image.displayWidth=w;
            this.image.scaleY=this.image.scaleX;
        }

        this.add(this.image);
        this.animKey=key+"Play";     
        
        this.image.on('animationcomplete', this.animComplete.bind(this), this);
    }
    animComplete()
    {
        this.image.destroy();

    }
    play()
    {
       
     this.image.play(this.animKey);
      
    }
}