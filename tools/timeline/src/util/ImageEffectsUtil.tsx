import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";

export class ImageEffectUtil
{
    private bscene:IBaseScene;
    private image:GameObjects.Sprite | undefined;
    private scene:Phaser.Scene;
    private callback:Function=()=>{};

    constructor(bscene:IBaseScene)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        
    }
    doImageEffect(image:GameObjects.Sprite,index:number,callback:Function)
    {
        this.image=image;
        this.callback=callback;
        switch (index) {
            case 1:
                let ox: number = this.image.x;
                this.image.x = 1000;
                this.image.visible = true;
                this.scene.tweens.add({ targets: this.image, duration: 1000, x: ox, onComplete: this.effectDone.bind(this) });
                break;

            case 2:
                let ox2: number = this.image.x;
                this.image.x = -1000;
                this.image.visible = true;
                this.scene.tweens.add({ targets: this.image, duration: 1000, x: ox2, onComplete: this.effectDone.bind(this) });
                break;

            case 3:
                let oy: number = this.image.y;
                this.image.y = -1000;
                this.image.visible = true;
                this.scene.tweens.add({ targets: this.image, duration: 1000, y: oy, onComplete: this.effectDone.bind(this) });
                break;

            case 4:
                let oy2: number = this.image.y;
                this.image.y = 1000;
                this.image.visible = true;
                this.scene.tweens.add({ targets: this.image, duration: 1000, y: oy2, onComplete: this.effectDone.bind(this) });
                break;

            case 5:
                this.image.alpha=0;
                this.image.visible = true;
                this.scene.tweens.add({ targets: this.image, duration: 1000, alpha:1, onComplete: this.effectDone.bind(this) });

                break;

            case 6:
                
                let ow:number=this.image.displayWidth;
                let oh:number=this.image.displayHeight;

                this.image.displayHeight=10;
                this.image.displayWidth=10;
                this.image.visible = true;
                this.scene.tweens.add({ targets: this.image, duration: 1000, displayWidth:ow,displayHeight:oh, onComplete: this.effectDone.bind(this) });
               
                break;

           

            case 7:
                
                //let ox3: number = image.x;
                let oy3: number = this.image.y;

                this.image.y=this.image.y-this.image.displayWidth/2;

                this.image.visible = true;
                this.scene.tweens.add({ targets: this.image, duration: 1000,y:oy3,yoyo:true,loop:true, onComplete: this.effectDone.bind(this) });
               
                break;
        }
    }
    effectDone()
    {
        this.callback();
    }
}