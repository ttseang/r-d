
//import { Sound } from "phaser";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene {
   
    private loadingText:Phaser.GameObjects.Text | undefined;

    constructor() {
        super("SceneLoad");
      
    }
    preload() {
        super.create();

        this.sound.once(Phaser.Sound.Events.DECODED_ALL, this.soundDecoded.bind(this));

        
        this.loadingText=this.add.text(0,0,"0%",{fontSize:"20px"}).setOrigin(0.5,0.5);
        Align.center(this.loadingText,this);
        this.load.on('progress',this.onProgress.bind(this));
       // this.load.once(Phaser.Sound.Events.DECODED_ALL,this.soundDecoded.bind(this));
        
        this.load.image("holder", "assets/holder.jpg");
        this.load.image("face", "assets/face.png");
        this.load.image("bracket", "assets/corner.png");
        this.load.image("hand", "assets/hand.png");
        this.load.image("punch", "assets/punch.png");
        this.load.image("thumbsUp", "assets/thumbsUp.png");
        this.load.image("circle", "assets/circle.png");
        this.load.image("heart", "assets/heart.png");
        this.load.image("balloon", "assets/balloon.png");
        this.load.image("rocket", "assets/rocket2.png");
        this.load.image("rays3", "assets/rays3.png");
        this.load.image("donut","assets/donut.png");
        this.load.image("sunrise","assets/sunrise.png");

        this.load.atlas("halfRays","./assets/rays_half.png","./assets/rays_half.json");
        this.load.atlas("brackets","./assets/bracket.png","./assets/bracket.json");
        this.load.atlas("bubblePop","./assets/bubblePop.png","./assets/bubblePop.json");
        this.load.atlas("starBurst","./assets/starburst.png","./assets/starburst.json");
        this.load.atlas("fullRays","./assets/rays.png","./assets/rays.json");
        this.load.atlas("raysPaper","./assets/raysPaper.png","./assets/raysPaper.json");
        this.load.atlas("sparks","./assets/sparks.png","./assets/sparks.json");
        this.load.atlas("radial","./assets/radial.png","./assets/radial.json");

        this.load.image("paperTop","./assets/paperTop.png");
        this.load.image("paperBottom","./assets/paper1.png");
        this.load.audio("audio1","assets/audio/1.mp3");
        this.load.image("pimage1","assets/pageImages/1.jpg");
        this.load.audio("FreshAndCool","assets/audio/backgroundMusic/FreshAndCool.mp3");

        this.load.image("bg1","assets/bg1.jpg");
    }
    onProgress(per:number)
    {
        per=Math.floor(per*100);
        if (this.loadingText)
        {
            this.loadingText.setText(per.toString()+"%");
        }
       
    }
    create() {
        //let audio1:Sound.BaseSound=this.sound.add("audio1");
      ////  audio1.once(Phaser.Sound.Events.DECODED, this.soundDecoded.bind(this));
       // let am:Sound.WebAudioSoundManager=new Sound.WebAudioSoundManager(this.sys.game);
       // am.once(Phaser.Sound.Events.DECODED, this.soundDecoded.bind(this));
       
        setTimeout(() => {
            this.scene.start("SceneMain");
        }, 3000);
    }
    soundDecoded()
    {
       
    }
    update()
    {
        
    }
}
export default SceneLoad;