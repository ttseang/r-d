
import { GameObjects } from "phaser";
import { AudioPlayer } from "../classes/AudioPlayer";
import { LayoutClock } from "../classes/LayoutClock";
import { LayoutManager } from "../classes/LayoutManager";
import MainStorage from "../classes/MainStorage";
import { TestFile } from "../classes/testFile";
import Align from "../util/align";
import { PageLoader } from "../util/pageLoader";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private ms:MainStorage=MainStorage.getInstance();
    private layoutClock:LayoutClock | undefined;
    private layoutManager:LayoutManager | undefined;
    private audioPlayer:AudioPlayer;
    private bgImage:GameObjects.Sprite | undefined;
    
    constructor() {
        super("SceneMain");
        
        //load file
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
       //let tf:TestFile=new TestFile();       
        this.audioPlayer=AudioPlayer.getAudioPlayer(this);
    }
    
    preload() {

    }
    create() {
        super.create();
        this.makeGrid(11, 11);
      //  this.grid.showPos();
        this.game.canvas.id="thecanvas";
        
        this.game.events.addListener(Phaser.Core.Events.BLUR,this.onBlur.bind(this));
        this.game.events.addListener(Phaser.Core.Events.CONTEXT_LOST,this.onBlur.bind(this));
        this.game.events.addListener(Phaser.Core.Events.FOCUS,this.onFocus.bind(this));
        this.game.events.addListener(Phaser.Core.Events.CONTEXT_RESTORED,this.onFocus.bind(this));

        this.layoutManager=new LayoutManager(this);
        this.layoutClock=new LayoutClock(this,this.layoutManager);
        this.layoutClock.pageCallback=this.nextPage.bind(this);

        /* if (this.ms.backgroundImage!=="")
        {
            this.bgImage=this.add.sprite(0,0,this.ms.backgroundImage);
            Align.scaleToGameW(this.bgImage,1,this);
            this.grid.placeAtIndex(60,this.bgImage,true);
        } */
        let pl:PageLoader=new PageLoader(this.startClock.bind(this));
        pl.loadPage(this.ms.currentPage+".json");

       // this.placeImage("face",60,0.1);

      // this.startClock();
    }
    startClock()
    {
        if (this.ms.backgroundMusic)
        {
            this.audioPlayer.setBackgroundMusic(this.ms.backgroundMusic);
        }
        if (this.layoutClock)
        {
             this.layoutClock.start();
        }       
    }
    onBlur()
    {
        console.log("blurred");
        
        let audioPlayer:AudioPlayer=AudioPlayer.getAudioPlayer(this);
        audioPlayer.pause();
       if (this.layoutClock)
       {
           this.layoutClock.paused=true
       }
    }
    onFocus()
    {
        console.log("focus")
        let audioPlayer:AudioPlayer=AudioPlayer.getAudioPlayer(this);
        audioPlayer.resume();
        if (this.layoutClock)
        {
            this.layoutClock.paused=false;
        }
    }
    nextPage()
    {
        this.scene.restart();
    }
    update(time:number,delta:number) {
        if (this.layoutClock)
        {
            this.layoutClock.tick(time,delta);
        }
       
    }
}
export default SceneMain;