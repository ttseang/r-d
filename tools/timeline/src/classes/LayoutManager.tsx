import { GameObjects } from "phaser";
import { AttsVo } from "../dataObjs/AttsVo";
import { EffectDataVo } from "../dataObjs/EffectDataVo";
import { GameObj } from "../dataObjs/GameObj";
import { HighlightVo } from "../dataObjs/HighlightVo";
import { KeyframeVo } from "../dataObjs/KeyframeVo";
import { PosVo } from "../dataObjs/PosVo";
//import { ImageMask } from "../effects/imageMask";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { EffectUtil } from "../util/EffectUtil";
import { FormUtil } from "../util/formUtil";
import { ImageEffectUtil } from "../util/ImageEffectsUtil";
import EffectsDef from "./EffectsDef";
import { MaskedImage } from "./MaskedImage";

export class LayoutManager {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    private objMap: Map<string, GameObj> = new Map();
    private effectTexts: Map<string, GameObjects.Text> = new Map();
    private grid: AlignGrid;
    private formUtil: FormUtil;
    private effectUtil: EffectUtil;
    private imageEffects: ImageEffectUtil;
    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.grid = bscene.getGrid();
        this.formUtil = new FormUtil(bscene, this.grid.rows, this.grid.cols);
        
        this.effectUtil = new EffectUtil(bscene, this.formUtil);
        this.imageEffects = new ImageEffectUtil(bscene);
        this.effectUtil.loadAnimations();

        this.formUtil.alignGrid.showPos();

        this.makeTextHolder();
    }
    makeTextHolder() {
        if (document.getElementById('textholder') === null) {
            let h: HTMLElement = document.createElement('div');


            h.id = "textholder"
            h.style.position = "initial";

            let gameDiv: HTMLElement | null = document.getElementById('phaser-game');
            if (gameDiv) {
                gameDiv.appendChild(h);
            }
        }

    }
    clearAll() {
        let th: HTMLElement | null = document.getElementById('textholder');
        if (th !== null) {

            th.innerHTML = "";
        }
        this.objMap.clear();
        this.effectTexts.clear();
        /*   this.objMap.forEach((item:GameObj)=>{
              
          }) */
    }
    getObj(k: KeyframeVo) {
        if (this.objMap.has(k.itemName)) {
            let gameObj: GameObj | undefined = this.objMap.get(k.itemName);
            return gameObj;
        }
        else {
            ////console.log("create obj");
            ////console.log(k);

            if (k.atts) {
                if (k.atts.textKey) {
                    switch (k.atts.type) {
                        case "s":

                            let s: GameObjects.Sprite = this.scene.add.sprite(0, 0, k.atts?.textKey);
                            let gameObj = new GameObj(k.itemName, k.atts.type, s);
                            this.objMap.set(k.itemName, gameObj);
                            return gameObj;

                        case "t":

                            let t: GameObjects.Text = this.scene.add.text(0, 0, k.atts?.textKey);
                            let gameObjt = new GameObj(k.itemName, k.atts.type, t);
                            this.objMap.set(k.itemName, gameObjt);
                            return gameObjt;

                        case "im":

                            let im: MaskedImage = new MaskedImage(this.bscene, "circle", k.atts.textKey, k.atts.scale);
                            // im.setFullMask();
                            let gameObjIM = new GameObj(k.itemName, k.atts.type, im);
                            this.objMap.set(k.itemName, gameObjIM);
                            return gameObjIM;

                        case "h":
                            let h: HTMLElement = document.createElement('div');


                            h.id = k.itemName;
                            if (k.cssClass !== "") {
                                h.classList.add(k.cssClass);
                            }
                            else {
                                h.classList.add('titleText');
                            }


                            let gameDiv: HTMLElement | null = document.getElementById('textholder');
                            if (gameDiv) {
                                gameDiv.appendChild(h);
                            }



                            let gameObjh = new GameObj(k.itemName, k.atts.type, h);
                            this.objMap.set(k.itemName, gameObjh);

                            return gameObjh;
                    }
                }
            }
        }
    }
    setHtmlText(h: HTMLElement, k: KeyframeVo, effectText: GameObjects.Text | undefined) {
        //console.log("set html text");
        if (k.atts) {
            if (k.highlights) {
                let mainText: string = k.atts.textKey;
                let parts:string[]=mainText.split("]");
               // let segs: string[] = [];
                let htext:string="";
                let spanCount:number=0;

                console.log(parts);
                for (let i:number=0;i<parts.length;i++)
                {
                    let part:string=parts[i];
                   

                    let parts2:string[]=part.split("[");
                    if (parts2.length>1)
                    {
                        console.log(parts2);
                        htext+=parts2[0];
                        let spanName: string = k.itemName + "_high_" + i.toString();
                        let highlight:HighlightVo=k.highlights[spanCount];
                        highlight.scanned=parts2[1];
                        let spanText: string = "<span id='" + spanName + "' class='" + highlight.cssClass + "'>" + parts2[1] + "</span>";
                        htext+=spanText;
                        spanCount++;
                    }
                    else
                    {
                        htext+=part;
                    }
                }
                /* let segs: string[] = [];

                for (let i: number = 0; i < k.highlights.length; i++) {
                    let highlight: HighlightVo = k.highlights[i];


                    let seg: string = k.atts.textKey.substr(highlight.startPos, highlight.len);
                    segs.push(seg);
                    highlight.scanned = seg;
                    let end: number = highlight.startPos + highlight.len - 1;
                    //console.log(seg);
                    let mt: string[] = mainText.split("");
                    // mt[highlight.startPos]="^";
                    mt[end] = "^";
                    mainText = mt.join("");
                }
                // //console.log(mainText);

                for (let j: number = 0; j < segs.length; j++) {
                    let highlight: HighlightVo = k.highlights[j];

                    let seg: string = segs[j];
                    let segArray: string[] = seg.split("");
                    segArray[segArray.length - 1] = "^";
                    let search: string = segArray.join("");

                    // //console.log(search);
                    let spanName: string = k.itemName + "_high_" + j.toString();
                    let spanText: string = "<span id='" + spanName + "' class='" + highlight.cssClass + "'>" + seg + "</span>";
                    mainText = mainText.replace(search, spanText);
                } */
                ////console.log(mainText);

                h.innerHTML = htext;

            }

        }
    }
    placeObj(k: KeyframeVo) {
        let gameObj: GameObj | undefined = this.getObj(k);
        ////console.log(gameObj);

        if (gameObj) {
            if (gameObj.type === "h") {
                if (k.atts) {
                    let h: HTMLElement = gameObj.obj as HTMLElement;
                    //console.log(h);
                    let index: number = this.grid.getIndexByXY(k.atts.x, k.atts.y);
                    this.formUtil.placeElementAt(index, h.id, false, false);

                    let effectText: GameObjects.Text | undefined = this.getEffectText(gameObj.name);

                    this.setHtmlText(h, k, effectText);

                    /*   if (k.effectText)
                      {
                          k.effectText.setText(k.effectAtts.highlight);
                          //console.log("set to "+k.effectAtts.highlight);
                      } */

                    /*  if (k.centerText === true) {
                         this.formUtil.centerElement(h.id);
                     } */
                    if (k.atts.col !== -1) {
                        this.formUtil.columnElement(h.id, k.atts.col);
                    }
                }

            }
            if (gameObj.type === "s") {
                let s: GameObjects.Sprite = gameObj.obj as GameObjects.Sprite;
                if (k.atts) {
                    if (k.useTween === false) {
                        this.placeItem(s, k.atts);
                    }
                    else {
                        this.tweenItem(s, k.atts);
                    }

                    if (k.atts.loadEffect !== 0) {
                        this.imageEffects.doImageEffect(s, k.atts.loadEffect, this.imageEffectDone.bind(this));
                        k.atts.loadEffect = 0;
                    }
                }

            }
            if (gameObj.type === "t") {
                let t: GameObjects.Text = gameObj.obj as GameObjects.Text;
                if (k.atts) {
                    if (k.useTween === false) {
                        this.placeItem(t, k.atts);
                    }
                    else {
                        this.tweenItem(t, k.atts);
                    }
                }
            }
            if (gameObj.type === "im") {
                let im: MaskedImage = gameObj.obj as MaskedImage;

                if (k.atts) {
                    if (k.useTween === false) {
                        this.placeItem(im, k.atts);
                    }
                    else {
                        this.tweenItem(im, k.atts);
                    }

                    if (k.atts.loadEffect !== 0) {
                        this.imageEffects.doImageEffect(im.image, k.atts.loadEffect, this.imageEffectDone.bind(this));
                        k.atts.loadEffect = 0;
                    }
                }

            }
        }
    }
    private imageEffectDone() {

    }
    private placeItem(iGameObj: IGameObj, atts: AttsVo) {


        if (atts.type === "s") {
            Align.scaleToGameW(iGameObj, atts.scale, this.bscene);
            let s:GameObjects.Sprite=iGameObj as GameObjects.Sprite;

            iGameObj.alpha = atts.alpha;
            if (atts.tint!==-1)
            {
                s.setTint(atts.tint);
            }
        }
        if (atts.type === "t") {
            let t: GameObjects.Text = iGameObj as GameObjects.Text;
            t.setText(atts.textKey);
        }
        if (atts.type === "im") {
            let im: MaskedImage = iGameObj as MaskedImage;
            //console.log(iGameObj);
            //console.log(atts);
            im.image.alpha = atts.alpha;
            if (im.inPlace===false)
            {
                this.grid.placeAt(atts.x, atts.y, im.mask);
                im.inPlace=true;
            }
           
            this.grid.placeAt(atts.x, atts.y, im.image);
            return;
        }
        this.grid.placeAt(atts.x, atts.y, iGameObj);                   //
        //

        //iGameObj.x = atts.x;
        // iGameObj.y = atts.y;


    }
    private tweenItem(iGameObj: IGameObj, atts: AttsVo) {

        if (atts.type === "t") {
            let t: GameObjects.Text = iGameObj as GameObjects.Text;
            t.setText(atts.textKey);
        }
        if (atts.type === "im") {
            let im: MaskedImage = iGameObj as MaskedImage;
            let posVo: PosVo = this.grid.getRealXY(atts.x, atts.y);
            this.scene.tweens.add({ targets: im.image, duration: 1000, y: posVo.y, x: posVo.x, alpha: atts.alpha });
            return;
        }
        let posVo: PosVo = this.grid.getRealXY(atts.x, atts.y);
        //console.log(posVo);
        this.scene.tweens.add({ targets: iGameObj, duration: 1000, y: posVo.y, x: posVo.x, alpha: atts.alpha });
    }
    private getEffectText(objName: string, create: boolean = true) {
        if (this.effectTexts.has(objName)) {
            return this.effectTexts.get(objName);
        }
        if (create === false) {
            return undefined;
        }
        let t: Phaser.GameObjects.Text = this.scene.add.text(0, 0, "effect text");
        t.visible = false;
        this.effectTexts.set(objName, t);
        return t;
    }
    public doEffect(k: KeyframeVo) {
        let gameObj: GameObj | undefined = this.getObj(k);
        //console.log(gameObj);

        if (gameObj) {

            let fxData: EffectsDef = EffectsDef.getInstance();

            if (k.highlights) {
                for (let i: number = 0; i < k.highlights.length; i++) {
                    let hightlight: HighlightVo = k.highlights[i];
                    //console.log(hightlight.scanned);
                    let effectVo: EffectDataVo = fxData.getEffectData(hightlight.effectName);
                    //console.log(effectVo);
                    let spanName: string = k.itemName + "_high_" + i.toString();

                    if (effectVo.phaserEffect !== "none") {
                        let textName: string = k.itemName + "_high" + i.toString();
                        let t: GameObjects.Text | undefined = this.getEffectText(textName, true);
                        if (t) {
                            t.setText(hightlight.scanned);


                            if (effectVo.phaserEffect !== "none") {
                                this.effectUtil.doEffect(effectVo.phaserEffect, t, spanName, hightlight.duration);
                            }

                        }
                    }
                    if (effectVo.mainClass !== "noEffect") {
                        let h: HTMLElement = gameObj.obj as HTMLElement;
                        //console.log(h);
                        h.className = effectVo.mainClass;
                    }
                    if (effectVo.hClass !== "noEffect") {

                        let highSpan: HTMLElement | null = document.getElementById(spanName);
                        //console.log(highSpan);

                        if (highSpan) {
                            highSpan.className = effectVo.hClass;
                        }
                    }
                }
            }
            /* let fxData: EffectsDef = EffectsDef.getInstance();
            let effectVo: EffectDataVo = fxData.getEffectData(k.effectAtts.effectName);



            if (effectVo.phaserEffect !== "none") {
                let t: GameObjects.Text | undefined = this.getEffectText(gameObj.name, true);
                //  let t: GameObjects.Text | undefined = k.effectText;

                //console.log(t);
                //console.log(this.objMap);

                if (t) {
                    //console.log(k.effectAtts);

                    //console.log(k.effectAtts.effectName);
                    t.setText(k.effectAtts.highlight);
                    let spanName: string = "high" + k.itemName;
                    this.effectUtil.doEffect(k.effectAtts?.effectName, t, spanName, k.effectAtts.duration);

                }
            }

            if (effectVo.mainClass !== "noEffect") {
                let h: HTMLElement = gameObj.obj as HTMLElement;
                //console.log(h);
                h.className = effectVo.mainClass;
            }
            if (effectVo.hClass !== "noEffect") {
                let spanName: string = "high" + k.itemName;
                let highSpan: HTMLElement | null = document.getElementById(spanName);
                if (highSpan) {
                    highSpan.className = effectVo.hClass;
                }
            } */

        }


    }
}