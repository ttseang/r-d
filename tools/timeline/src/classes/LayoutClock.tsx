
import { KeyframeVo } from "../dataObjs/KeyframeVo";
import IBaseScene from "../interfaces/IBaseScene";

import { AlignGrid } from "../util/alignGrid";
import { AudioPlayer } from "./AudioPlayer";
import { LayoutManager } from "./LayoutManager";
import MainStorage from "./MainStorage";

export class LayoutClock {
    private ms: MainStorage = MainStorage.getInstance();
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    public timerOn: boolean = true;
    private grid: AlignGrid;
    // private mainTimer:Phaser.Time.TimerEvent;
    private keyframe: KeyframeVo | undefined;
    private nextTime: number = -1;
    public layoutManager: LayoutManager;
    public started: boolean = false;
    public paused: boolean = false;
    private myTime: number = 0;
    public pageCallback:Function=()=>{};
    private audioPlayer:AudioPlayer;
    private timerText:Phaser.GameObjects.Text;

    constructor(bscene: IBaseScene, layoutManager: LayoutManager) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.layoutManager = layoutManager;
        this.grid = bscene.getGrid();
        this.audioPlayer=AudioPlayer.getAudioPlayer(bscene);
        console.log(this.audioPlayer);

        this.timerText=this.scene.add.text(0,0,"0");
        this.grid.placeAt(9,10,this.timerText);
    }
    public start() {

        this.getNext();
        this.started = true;
    }
    tick(time: number, delta: number) {

        if (this.started === false || this.paused === true) {
            return;
        }
        if (this.ms.syncToAudio===true)
        {
            if (this.audioPlayer.sound)
            {
                if (this.audioPlayer.sound.isPlaying===true)
                {
                    this.myTime += delta;
                }
            }
            
        }
        else
        {
            this.myTime += delta;
        }
        
        if (this.timerText)
        {
            this.timerText.setText(this.myTime.toString());
        }
        if (this.myTime > this.nextTime) {
            if (this.keyframe) {
                if (this.keyframe.active === true) {
                    //console.log(this.keyframe);

                    this.doAction();
                    this.getNext();
                }
            }
        }
    }
    private doAction() {
        console.log("do action");

        if (this.keyframe) {
            if (this.keyframe.eventType === "update") {
                this.keyframe.active = false;
                //  console.log("do action");
                this.layoutManager.placeObj(this.keyframe);
                if (this.keyframe.highlights) {
                    this.layoutManager.doEffect(this.keyframe);
                }
                
            }
            if (this.keyframe.eventType==="playAudio")
            {
                this.audioPlayer.playSound(this.keyframe.itemName);
            }
            if (this.keyframe.eventType==="page")
            {
                this.ms.currentPage=this.keyframe.itemName;
                this.layoutManager.clearAll();
                this.pageCallback();

            }
        }
    }


    getNext() {
        this.keyframe = this.ms.getNextFrame();
        console.log(this.keyframe);
        if (this.keyframe) {
            this.nextTime = this.keyframe?.start;
        }
        else
        {
            this.started=false;
        }
    }
}