import { KeyframeVo } from "../dataObjs/KeyframeVo";



export class MainStorage {
   
    private static instance: MainStorage;
    public gameConfig: any;
    //
    //

    public selectedFile: number = 0;
    public prevData: string = "";

    public makeNewFlag: boolean = true;
    public prevMode: boolean = false;
    public keyFrames: KeyframeVo[] = [];
    public currentKeyFrame: KeyframeVo | undefined;
    public currentPage: string = "page3";
    public backgroundMusic: string = "";
    public backgroundImage: string = "";
    public syncToAudio:boolean=true;
    
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        
    }
    static getInstance(): MainStorage {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new MainStorage();
        }
        return this.instance;
    }
    public getNextFrame() {
        this.currentKeyFrame = this.keyFrames.shift();
        return this.currentKeyFrame;
    }
    public addKeyFrame(k: KeyframeVo) {
        this.keyFrames.push(k);
        // this.keyFrames.sort((a:KeyframeVo,b:KeyframeVo)=>(a.start<b.start)?1:0);
    }
}
export default MainStorage;