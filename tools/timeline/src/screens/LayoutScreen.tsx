import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { MainStorage } from '../classes/MainStorage';
import SceneMain from '../scenes/SceneMain';
import SceneFont from '../scenes/SceneFont';
import SceneLoad from '../scenes/SceneLoad';
interface MyProps { }
interface MyState { }
class LayoutScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
        const config: any = {
            mode: Phaser.AUTO,
            width: 640,
            height: 480,
            parent: 'phaser-game',
            scene: [SceneFont, SceneLoad, SceneMain]
        }
        this.ms.gameConfig = config;

        new Phaser.Game(config);
    }
    render() {
        return (          
                <div id="phaser-game">
                </div>)
    }
}
export default LayoutScreen;