

export class GameObj
{
    public name:string;
    public type:string;
    public obj:any;

    constructor(name:string,type:string,obj:any)
    {
        this.name=name;
        this.type=type;
        this.obj=obj;
    }
}