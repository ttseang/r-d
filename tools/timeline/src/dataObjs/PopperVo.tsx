export class PopperVo
{
    public key:string;
    public row:number;
    public col:number;
    public type:string;
    public scale:number;
    constructor(key:string,row:number,col:number,scale:number,type:string)
    {
        this.key=key;
        this.row=row;
        this.col=col;
        this.type=type;
        this.scale=scale;
    }
}