export class HighlightVo
{
  
    public cssClass:string;
    public effectName:string;
    public duration:number;
    public scanned:string="";
    
    constructor(cssClass:string,effectName:string,duration:number)
    {
               
        this.cssClass=cssClass;
        this.effectName=effectName;
        this.duration=duration;
    }
}