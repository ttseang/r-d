export class AttsVo
{
    public x:number;
    public y:number;
    public textKey:string;
    public angle:number;
    public scale:number;   
    public type:string;
    public alpha:number;
    public color:number=0xffffff;
    public col:number=-1;
    public loadEffect:number=0;
    public tint:number=-1;
    
    constructor(type:string,x:number=0,y:number=0,textKey:string="",scale:number=1,angle:number=0,alpha:number=1)
    {
        this.type=type;
        this.x=x;
        this.y=y;        
        this.textKey=textKey;
        this.angle=angle;
        this.scale=scale;      
        this.alpha=alpha;
    }
    fromSprite(s:Phaser.GameObjects.Sprite)
    {
        this.x=s.x;
        this.y=s.y;        
      //  this.textKey="";
        this.angle=s.angle;
       // this.scaleX=s.scaleX;
       // this.scaleY=s.scaleY;
        this.alpha=s.alpha;
    }
}