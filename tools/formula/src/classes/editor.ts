import { MathfieldElement } from 'mathlive';
export class Editor {
    private mfe: MathfieldElement;
    private mfeElement: HTMLElement;
    private btnClear: HTMLButtonElement;
    private text1: HTMLTextAreaElement;

    constructor() {
        console.log("Editor constructor");
        this.mfe = new MathfieldElement();

        // this.mfe.value = "x^2";
        this.mfeElement = document.getElementById("mfe") as HTMLElement;
        this.mfeElement.addEventListener("input", (event) => this.onInput(event));

        this.btnClear = document.getElementById("btnClear") as HTMLButtonElement;
        this.btnClear.addEventListener("click", (event) => this.clearEditor());
        (window as any).editor = this.mfe;

        document.addEventListener("keydown", (event) => this.onKeyDown(event));
        (this.mfeElement as any)._mathfield.onSelectionDidChange = this.selectionChanged.bind(this);

        this.text1 = document.getElementsByClassName("ltext")[0] as HTMLTextAreaElement;
        //on input event
        this.text1.addEventListener("input", (event) => this.textAreaInput(event));
        this.text1.value = this.mfe.getValue();

        const buttonLine = document.getElementsByClassName("btnLine")[0] as HTMLButtonElement;
        buttonLine.addEventListener("click", (event) => this.insertColor(event));

        setTimeout(() => {
            this.text1.value = (this.mfeElement as any).getValue();
        }, 2000);
    }
    insertColor(event: Event) {
        let color: string = (event.target as HTMLElement).innerText;
        color = color.toLowerCase();
        console.log(color);
        //get the selected range from text area
        let start: number = this.text1.selectionStart;
        let end: number = this.text1.selectionEnd;
        console.log(start, end);
        if (start == end) {
            return;
        }
        let selectedText: string = this.text1.value.substring(start, end);
        console.log(selectedText);
        //insert the color and then add /color{black} at the end of the selection
        let newText: string = `\\color{${color}}${selectedText}\\color{black}`;
        console.log(newText);
        this.text1.value = this.text1.value.substring(0, start) + newText + this.text1.value.substring(end);
        //set the cursor position
        this.text1.selectionStart = start + newText.length;
        this.text1.selectionEnd = start + newText.length;
        //set the focus
        this.text1.focus();
        (this.mfeElement as any).setValue(this.text1.value);


    }
    private textAreaInput(event: Event) {
        console.log("text area input");
        (this.mfeElement as any).setValue(this.text1.value);
    }
    private onKeyDown(event: KeyboardEvent) {
        // console.log(event);
        //check for alt+r
        if (event.altKey && event.key == "r") {
            const mcClasses = document.querySelectorAll('[class^="ML__"]');
            console.log(mcClasses);
        }
    }
    private selectionChanged(e: Event) {
        console.log("selection changed");
        //console.log((this.mfeElement as any)._mathfield.getCaretPoint());
        //console.log((this.mfeElement as any)._mathfield.getCaretPosition());
        console.log(window.getSelection());
    }
    private getSelectedText() {
        let selection = this.mfe.selection;
        console.log(selection);
        return selection;
    }
    private getContent() {
        return this.mfe.value;
    }
    private clearEditor() {
        (this.mfeElement as any).setValue("");
    }
    private onInput(event: Event) {
        console.log((this.mfeElement as any).getValue());
        this.text1.value = (this.mfeElement as any).getValue();
        //  let equation = (this.mfeElement as any).getValue();
        /*  const elements = equation.split(/([\+\-\=\/\*]|\\\\[a-z]+|\{[^\{\}]*\}|\^[^\^]*)/);
         // Remove any empty elements from the array
         const parsedElements = elements.filter((element: any) => element);
         console.log(parsedElements); */
    }
}