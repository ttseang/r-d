import ApiConnect from "../ApiConnect";
import { ServerSaveVo } from "./dataObjs/exports/saver/ServerSaveVo";
import { MsgVo } from "./dataObjs/MsgVo";
import { MainController } from "./MainController";
import MainStorage from "./MainStorage";

export class BookSaver
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    public saveBook()
    {

        if (this.ms.LtiFileName === "") {
            this.mc.changeScreen(9);
            return;
        }
        let data: any = this.ms.saveBook2();

        let serverData: ServerSaveVo = new ServerSaveVo(this.ms.LtiFileName, this.ms.fileName, this.ms.appType, data);

        console.log(serverData);

        this.mc.setAlert(new MsgVo("Saving...","warning",-1));

        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.save(JSON.stringify(serverData), this.dataSent.bind(this));
    }
    private dataSent(response: any) {
        console.log("send message");
        this.mc.setAlert(new MsgVo("Book Saved","success",2000));
        //console.log(response);
        this.callback(response);
    }
}