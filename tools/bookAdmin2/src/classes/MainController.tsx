import { MsgVo } from "./dataObjs/MsgVo";

export class MainController
{
    private static instance:MainController;


    public changeScreen:Function=()=>{};
    public addNewElement:Function=()=>{};

    public openTextEdit:Function=()=>{};
    public closeTextEdit:Function=()=>{};
    public onTextEditClose:Function=()=>{};
    public textEditCallback:Function=()=>{};
    //
    //
    //
    public openImageBrowse:Function=()=>{};
    public closeImageBrowse:Function=()=>{};
    public onImageBrowseClose:Function=()=>{};
    public mediaChangeCallback:Function=()=>{};
    //
    //
    //
    public saveBook:Function=()=>{};

    public setAlert:Function=(msg:MsgVo)=>{};
    

    public styleChangeCallback:Function=()=>{};
    public doKeyboardShortCut:Function=(key:string,ctr:boolean,alt:boolean)=>{}

    public static getInstance(): MainController {
        if (this.instance === undefined || this.instance === null) {
          this.instance = new MainController();
        }
    
        return this.instance;
      }

}