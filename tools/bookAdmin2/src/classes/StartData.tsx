import { FileListLoader } from "./FileListLoader";
import MainStorage from "./MainStorage";
import { TextStyleMapLoader } from "./TextStyleMapLoader";

export class StartData {
    private callback: Function;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(callback: Function) {
        this.callback = callback;
    }
    start() {
        let textStyleMapLoader: TextStyleMapLoader = new TextStyleMapLoader(this.gotStyles.bind(this));
        textStyleMapLoader.load("./textStyleMap.json");
    }
    private gotStyles() {
       this.getFileList();
    }
    private getFileList()
    {
        let fll:FileListLoader=new FileListLoader(this.callback);
        fll.getFileList();
    }
  }