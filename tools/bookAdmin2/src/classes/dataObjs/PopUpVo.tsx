import { ElementVo } from "./ElementVo";
import { PopUpSaveVo } from "./exports/saver/PopUpSaveVo";
import { SaveVo } from "./exports/saver/SaveVo";
import { ExportVo } from "./exports/viewer/ExportVo";
import { PopUpExportVo } from "./exports/viewer/PopUpExportVo";


export class PopUpVo {

    public id: number;
    public name: string;
    public ukey: string;
    public w: number;
    public h: number;
    public elements: ElementVo[] = [];

    private static POPUPID: number = 0;
    private static PElementID: number = 0;

    constructor(w: number = 0, h: number = 0) {
        PopUpVo.POPUPID++;
        this.id = PopUpVo.POPUPID;

        //unique string for the display
        this.ukey = "popup" + this.id.toString();

        //can be changed by the user
        this.name = "popup " + this.id.toString();
        this.w = w;
        this.h = h;
    }
    public fromObj(obj: any) {
        this.w = parseFloat(obj.w);
        this.h = parseFloat(obj.h);

        console.log("pop up elements");
        console.log(obj.elements);

        let elements: any = obj.elements;

        for (let i: number = 0; i < elements.length; i++) {
            let elementVo: ElementVo = new ElementVo();
            elementVo.fromObj(elements[i]);
            this.addElement(elementVo);
        }
    }
    public addElement(elementVo: ElementVo) {
        PopUpVo.PElementID++;

        elementVo.id = PopUpVo.PElementID;
        //console.log("ID="+elementVo.id);

        this.elements.push(elementVo);
    }
    public deleteElement(elementVo: ElementVo) {
        let index: number = -1;
        let elements2: ElementVo[] = this.elements.slice();

        for (let i: number = 0; i < elements2.length; i++) {
            if (elements2[i].id === elementVo.id) {
                index = i;
            }
        }
        if (index !== -1) {
            elements2.splice(index, 1);
        }
        return elements2;
    }
    public getExport() {
        let elementExports: ExportVo[] = [];

        for (let i: number = 0; i < this.elements.length; i++) {
            elementExports.push(this.elements[i].getExport());
        }
        return new PopUpExportVo(this.id, this.w, this.h, elementExports);
    }
    public getSave() {
        let elementExports: SaveVo[] = [];

        for (let i: number = 0; i < this.elements.length; i++) {
            elementExports.push(this.elements[i].getSave());
        }
        return new PopUpSaveVo(this.id, this.w, this.h, elementExports);
    }
}