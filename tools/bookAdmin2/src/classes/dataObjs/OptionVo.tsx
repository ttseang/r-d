export class OptionVo
{
    public key:string;
    public value:string;
    public type:string;
    public min:number;
    public max:number;
    public label:string;
    public static TYPE_SLIDER:string="typeSlider";

    constructor(type:string,label:string,key:string,value:string,min:number=0,max:number=100)
    {
        this.key=key;
        this.value=value;
        this.type=type;
        this.min=min;
        this.max=max;
        this.label=label;
    }
}