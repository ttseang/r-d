import { ElementVo } from "./ElementVo";
import { OptionVo } from "./OptionVo";

export class PageTempVo
{
    public id:number;
    public title:string;
    public textCount:number;
    public imageCount:number;
    public thumb:string;

    public defElements:ElementVo[];
    public options:OptionVo[];
    
    constructor(id:number,title:string,textCount:number,imageCount:number,thumb:string,defElements:ElementVo[],options:OptionVo[]=[])
    {
        this.id=id;
        this.title=title;
        this.textCount=textCount;
        this.imageCount=imageCount;
        this.thumb=thumb;
        this.defElements=defElements;        
        this.options=options;
    }
   
}