
export class GenVo
{
    public title:string;
    public image:string;
    public action:number;
    public buttonText:string;
    public variant:string;
    public useIcon:boolean;

    constructor(title:string="",image:string="",action:number=0,buttonText:string,variant:string,useIcon:boolean=false)
    {
        this.title=title;
        this.image=image;
        this.action=action;
        this.buttonText=buttonText;
        this.variant=variant;
        this.useIcon=useIcon;
    }
}