import { ElementVo } from "./ElementVo";
//import { PageTempVo } from "./PageTempVo";

export class DPageTempVo
{
    public left:ElementVo[];
    public right:ElementVo[];
    public id:number;
    public name:string;
    public image:string;

    constructor(id:number,name:string,left:ElementVo[],right:ElementVo[],image:string)
    {
        this.id=id;
        this.name=name;
        this.left=left;
        this.right=right;
        this.image=image;
    }
}