import { AudioVo } from "../../AudioVo";
import { SaveVo } from "./SaveVo";


export class PageSaveVo
{
    public name:string;
    public left:SaveVo[];
    public right:SaveVo[];
    public audioVo:AudioVo;
    constructor(name:string,left:SaveVo[],right:SaveVo[],audio:AudioVo)
    {
        this.name=name;
        this.left=left;
        this.right=right;
        this.audioVo=audio;
    }
}