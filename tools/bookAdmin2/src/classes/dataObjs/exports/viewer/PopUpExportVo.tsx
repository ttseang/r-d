import { ExportVo } from "./ExportVo";

export class PopUpExportVo
{
   
    public id:number;
    public w:number;
    public h:number;
    public elements:ExportVo[];

    constructor(id:number,w:number,h:number,elements:ExportVo[])
    {
        this.id=id;
        this.w=w;
        this.h=h;
        this.elements=elements;
    }
}