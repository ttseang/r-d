import { AudioVo } from "../../AudioVo";
import { ExportVo } from "./ExportVo";

export class PageExportVo
{
    public left:ExportVo[];
    public right:ExportVo[];
    public audioVo:AudioVo;
    constructor(left:ExportVo[],right:ExportVo[],audio:AudioVo)
    {
        this.left=left;
        this.right=right;
        this.audioVo=audio;
    }
}