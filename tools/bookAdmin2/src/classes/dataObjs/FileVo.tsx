export class FileVo
{
    public fileName:string;
    public lti:string;
    public lastMod:number;
    public lastModD:string;

    constructor(fileName:string,lti:string,lastMod:number,lastModD:string)
    {
        if (fileName==="")
        {
            fileName="untitled";
        }
        this.fileName=fileName;
        this.lti=lti;
        this.lastMod=lastMod;
        this.lastModD=lastModD;
    }
}