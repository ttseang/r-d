import { PageVo } from "./PageVo";


export class HistoryVo
{
    public currentStep:number;
    public pages:PageVo[]=[];

    constructor(currentStep:number,pages:PageVo[])
    {
        this.currentStep=currentStep;
       // this.pages=pages;

       for (let i:number=0;i<pages.length;i++)
       {
           this.pages.push(pages[i].clone());
       }

    }
}