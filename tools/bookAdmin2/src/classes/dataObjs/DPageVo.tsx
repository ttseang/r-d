import { AudioVo } from "./AudioVo";
import { PageSaveVo } from "./exports/saver/PageSaveVo";
import { PageExportVo } from "./exports/viewer/PageExportVo";

import { PageVo } from "./PageVo";

export class DPageVo
{
  
    public left:PageVo;
    public right:PageVo;
    public name:string;

    public audioVo:AudioVo=new AudioVo();
    public color:string="ffffff";

    constructor(name:string="",left:PageVo,right:PageVo)
    {
        this.left=left;
        this.right=right;
        this.name=name;
    }
    fromObj(obj:any)
    {
     //   console.log("DPage VO");
      //  console.log(obj);
        
        this.name=obj.name;

        let left:PageVo=new PageVo();
        left.fromObj(obj.left,true);

        let right:PageVo=new PageVo();
        right.fromObj(obj.right,false);

        this.left=left;
        this.right=right;
    }
    getExport()
    {
        return new PageExportVo(this.left.getExport(),this.right.getExport(),this.audioVo);
    }
    getSave()
    {
        return new PageSaveVo(this.name,this.left.getSave(),this.right.getSave(),this.audioVo);
    }
}