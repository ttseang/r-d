export class AudioVo
{
    public audio:string="";
    public audioName:string="";
    
    public backgroundAudioName:string="";
    public backgroundAudio:string="";

    constructor(audio:string="",audioName:string="",backgroundAudioName:string="",backgroundAudio:string="")
    {
        this.audio=audio;
        this.audioName=audioName;
        this.backgroundAudio=backgroundAudio;
        this.backgroundAudioName=backgroundAudioName;
    }
}