export class MsgVo
{
    public msg:string;
    public variant:string;
    public time:number;

    constructor(msg:string,variant:string="primary",time:number=5)
    {
        this.msg=msg;
        this.variant=variant;
        this.time=time;
    }
}