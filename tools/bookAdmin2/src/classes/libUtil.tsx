import { LibItemVo } from "./dataObjs/LibItemVo";

export class LibUtil {
    public library: LibItemVo[] = [];

    constructor() {
        //TO DO move to JSON

        this.library.push(new LibItemVo("fairy cover", "fairy_cover.svg", []));
        this.library.push(new LibItemVo("fairy1", "fairy1.svg", []));
        this.library.push(new LibItemVo("fairy2", "fairy2.svg", []));
        this.library.push(new LibItemVo("fairy3", "fairy3.svg", []));
        this.library.push(new LibItemVo("frame1", "frame1.svg", []));
        this.library.push(new LibItemVo("greek1", "greek1.svg", []));
        this.library.push(new LibItemVo("greek2", "greek2.svg", []));
        this.library.push(new LibItemVo("greek3", "greek3.svg", []));
        this.library.push(new LibItemVo("greek4", "greek4.svg", []));
        this.library.push(new LibItemVo("tree", "tree.svg", []));
        this.library.push(new LibItemVo("tree2", "tree2.svg", []));

      /*   this.library.push(new LibItemVo("test1", "test1.jpg", []));
        this.library.push(new LibItemVo("test2", "test2.jpg", []));
        this.library.push(new LibItemVo("test3", "test3.jpg", []));
        this.library.push(new LibItemVo("test4", "test4.jpg", []));
        this.library.push(new LibItemVo("fisherman", "fisherman.jpg", []));
        this.library.push(new LibItemVo("tower", "tower.jpg", [])); */

        this.library.push(new LibItemVo("explore button","btnexplore.svg",["popbutton"]));


       /*  this.library.push(new LibItemVo("art","art1.png",['art']));
        this.library.push(new LibItemVo("pallet","pallet.png",['gutter']));
        this.library.push(new LibItemVo("shape1","shape1.png",['shapes']));
        this.library.push(new LibItemVo("shape2","shape2.png",['shapes'])); */

        this.library.push(new LibItemVo("brushes", "brushes.png", ['edge']));
        this.library.push(new LibItemVo("cars", "cars.png", ['edge']));
        this.library.push(new LibItemVo("flowers", "flowers.png", ['edge']));

        this.library.push(new LibItemVo("wienerdog", "wienerdog.png", ['gutter']));
        this.library.push(new LibItemVo("sub", "sub.png", ['gutter']));
        this.library.push(new LibItemVo("burger", "burger.png", ['gutter']));
        this.library.push(new LibItemVo("whale", "whale.svg", ['gutter']));
        this.library.push(new LibItemVo("submarine", "submarine.svg", ['gutter']));
        this.library.push(new LibItemVo("balloon", "balloon.svg", ['gutter']));

        this.library.push(new LibItemVo("bear", "bear.png", []));
        this.library.push(new LibItemVo("bread", "bread.svg", []));
        this.library.push(new LibItemVo("bun", "bun.svg", []));
        this.library.push(new LibItemVo("cat", "cat.svg", []));
        this.library.push(new LibItemVo("chair", "chair.svg", []));
        this.library.push(new LibItemVo("chicken", "chicken.png", []));
        this.library.push(new LibItemVo("cow", "cow.png", []));
        this.library.push(new LibItemVo("crab", "crab.png", []));
        this.library.push(new LibItemVo("croc", "croc.png", []));
        this.library.push(new LibItemVo("duck", "duck.png", []));
        this.library.push(new LibItemVo("glass", "glass.svg", []));
        this.library.push(new LibItemVo("hammerhead", "hammerhead.png", []));
        this.library.push(new LibItemVo("hat", "hat.svg", []));
    }
    getFilter(filter: string) {

        //console.log.log(filter);

        let filteredItems: LibItemVo[] = [];
        for (let i: number = 0; i < this.library.length; i++) {
            let current: LibItemVo = this.library[i];
            if (current.tags.includes(filter)) {
                //console.log.log(current);
                filteredItems.push(current);
            }
        }
        return filteredItems;
    }
}