
import { BookTempVo } from "./dataObjs/BookTempVo";
import { DPageTempVo } from "./dataObjs/DPageTempVo";
import { DPageVo } from "./dataObjs/DPageVo";
import { EditVo } from "./dataObjs/EditVo";
import { ElementVo } from "./dataObjs/ElementVo";
import { BookSaveVo } from "./dataObjs/exports/saver/BookSaveVo";
import { PageSaveVo } from "./dataObjs/exports/saver/PageSaveVo";
import { PopUpSaveVo } from "./dataObjs/exports/saver/PopUpSaveVo";
import { BookExportVo } from "./dataObjs/exports/viewer/BookExportVo";
import { PageExportVo } from "./dataObjs/exports/viewer/PageExportVo";
import { PopUpExportVo } from "./dataObjs/exports/viewer/PopUpExportVo";
import { FileVo } from "./dataObjs/FileVo";
import { HistoryVo } from "./dataObjs/HistoryVo";
import { LibItemVo } from "./dataObjs/LibItemVo";

import { PageTempVo } from "./dataObjs/PageTempVo";
import { PageVo } from "./dataObjs/PageVo";

import { PopUpVo } from "./dataObjs/PopUpVo";
import { StyleVo } from "./dataObjs/StyleVo";
import { TextStyleVo } from "./dataObjs/TextStyleVo";
import { LibUtil } from "./libUtil";
import { StyleUtil } from "./styleUtil";
import { TemplateData } from "./TemplateData";


export class MainStorage {

  private static instance: MainStorage;

  public pageTemps: PageTempVo[] = [];
  public dPageTemps: DPageTempVo[] = [];

  public bookTemps: BookTempVo[] = [];

  public bookInfo: BookTempVo = new BookTempVo(0, "", 0, 0, "");

  public pages: DPageVo[] = [];
  public popups: PopUpVo[] = [];
  public cover: DPageVo;

  public popupEditIndex: number = 0;
  public pageEditIndex: number = 0;
  public itemEditIndex: number = 0;

  public editMode: "pages" | "popup" = "pages";
  public styleMap: Map<number, TextStyleVo> = new Map<number, TextStyleVo>();

  private historyIndex: number = 0;
  private history: HistoryVo[] = [];

  

  public keyLock: boolean = false;

  public copyElement: ElementVo | null = null;

  public editText: string = "";
  public editVo: EditVo | null = null;

  public libUtil: LibUtil;
  public library: LibItemVo[] = [];
  public suggested: string = "";

  public styles: StyleVo[] = [];
  public styleUtil: StyleUtil;

  public linkedCount: number = 0;
  //public lastTab: number = 0;

  public selectedElement: ElementVo | null = null
  public bookID: number = 1;

  public exportCode: string = "";
  public editCover: boolean = false;
  public LtiFileName: string="";
  public fileName:string="";
  public files:FileVo[]=[];
  public appType:string="book";

  constructor() {

    this.cover = new DPageVo("cover", new PageVo("front cover", [], []), new PageVo("back cover", [], []));

    //sizes of books
    this.bookTemps.push(new BookTempVo(0, "Square Book", 300, 300, "cover2.jpg"));
    this.bookTemps.push(new BookTempVo(1, "Rectangle Book", 300, 450, "cover1.jpg"));

    //media items
    this.libUtil = new LibUtil();
    this.library = this.libUtil.library;

    this.styleUtil = new StyleUtil();

    this.styles = this.styleUtil.styles;

    (window as any).ms = this;
  }


  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }

    return this.instance;
  }

  /* public addHistory() {
    if (this.historyIndex !== this.history.length - 1) {
      this.history = this.history.slice(0, this.historyIndex);
    }
    this.history.push(new HistoryVo(this.pageEditIndex, this.pages));
    this.historyIndex = this.history.length - 1;
  }
  public goBack() {
    if (this.historyIndex > 0) {
      this.historyIndex--;
      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }
  public goForward() {
    if (this.historyIndex < this.history.length - 1) {
      this.historyIndex++;

      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }

 */
  /**
   * set the information for the selected book size
   * @param id -book id
   */
  selectBookTemp(id: number) {
    for (let i: number = 0; i < this.bookTemps.length; i++) {
      if (this.bookTemps[i].id === id) {
        this.bookInfo = this.bookTemps[i];
        this.bookID = id;
        console.log(this.bookInfo);

        //this.setDefaultData();
      }
    }

  }
  /**
   * move a page up or down in the book
   * @param fromIndex
   * @param toIndex 
   */
  public movePage(fromIndex: number, toIndex: number) {
    // remove `from` item and store it
    let item: DPageVo = this.pages.splice(fromIndex, 1)[0];
    // insert stored item into position `toIndex`
    this.pages.splice(toIndex, 0, item);
  }
  /**
   * Remove pages from the book
   * @param index 
   */
  public removePage(index: number) {
    this.pages.splice(index, 1);
  }
  /**
   * Set double page info
   */
  private setDTemps() {
    let dtemps: DPageTempVo[] = [];

    switch (this.bookID) {
      case 0:
        dtemps.push(new DPageTempVo(0, "blank", this.getTemplateByID(-1), this.getTemplateByID(-1), "0.jpg"));
        dtemps.push(new DPageTempVo(1, "Style 1", this.getTemplateByID(1), this.getTemplateByID(2), "1.jpg"));
        dtemps.push(new DPageTempVo(2, "Style 2", this.getTemplateByID(1), this.getTemplateByID(3), "2.jpg"));

        break;

      case 1:
        dtemps.push(new DPageTempVo(0, "blank", this.getTemplateByID(-1), this.getTemplateByID(-1), "0.jpg"));
        dtemps.push(new DPageTempVo(2, "Gutter", this.getTemplateByID(4), this.getTemplateByID(5), "2.jpg"));
        dtemps.push(new DPageTempVo(1, "Edges", this.getTemplateByID(0), this.getTemplateByID(1), "1.jpg"));

        //   dtemps.push(new DPageTempVo(2, "Photos", this.getTemplateByID(2), this.getTemplateByID(3), "3.jpg"));
        break;
    }
    this.dPageTemps = dtemps;
  }
  getImagePath(image: string, path: number = 0) {

    if (path === 1) {
      return "https://ttv5.s3.amazonaws.com/william/images/pageTemplates/" + image;
    }

    return "https://ttv5.s3.amazonaws.com/william/images/bookimages/" + image;
  }

  getPageW() {
    if (this.bookInfo) {
      return this.bookInfo.w;
    }
    return 300;
  }
  getPageH() {
    if (this.bookInfo) {
      return this.bookInfo.h;
    }
    return 450;
  }
  setDefaultData() {
    if (this.bookInfo === null) {
      return
    }

    let templateData: TemplateData = new TemplateData();

    this.pageTemps = templateData.getDefaultData(this.bookInfo.id);

    this.setDTemps();



  }
  loadBook(data: any) {

    console.log(data);
    let w: number = parseInt(data.pwidth);
    let h: number = parseInt(data.pheight);

    this.bookInfo = new BookTempVo(0, "loaded book", w, h, "");

    this.cover = new DPageVo("", new PageVo(), new PageVo());
    this.cover.fromObj(data.covers);

    let pageData: any = data.pages;
    console.log(pageData);

    this.pages = [];

    for (let i: number = 0; i < pageData.length; i++) {
      let dPageVo: DPageVo = new DPageVo("", new PageVo(), new PageVo());
      dPageVo.fromObj(pageData[i]);
      this.pages.push(dPageVo);
    }

    let popUpData:any=data.popups;
    console.log("POP UPS");
    console.log(popUpData);

    this.popups=[];
    for (let i:number=0;i<popUpData.length;i++)
    {
       let popUpVo:PopUpVo=new PopUpVo();
       popUpVo.fromObj(popUpData[i]);
       this.popups.push(popUpVo);
    }

    // console.log(this.cover);

  }
  saveBook() {
    let allPages: PageSaveVo[] = [];
    for (let i: number = 0; i < this.pages.length; i++) {
      allPages.push(this.pages[i].getSave());
    }

    let coverExport: PageSaveVo = this.cover.getSave();

    let bookSave: BookSaveVo = new BookSaveVo(coverExport, allPages, this.bookInfo.w, this.bookInfo.h);

    let popupSaves: PopUpSaveVo[] = [];

    for (let j: number = 0; j < this.popups.length; j++) {
      popupSaves.push(this.popups[j].getSave());
    }
    bookSave.popups = popupSaves;

    return JSON.stringify(bookSave);
  }
  saveBook2() {
    let allPages: PageSaveVo[] = [];
    for (let i: number = 0; i < this.pages.length; i++) {
      allPages.push(this.pages[i].getSave());
    }

    let coverExport: PageSaveVo = this.cover.getSave();

    let bookSave: BookSaveVo = new BookSaveVo(coverExport, allPages, this.bookInfo.w, this.bookInfo.h);

    let popupSaves: PopUpSaveVo[] = [];

    for (let j: number = 0; j < this.popups.length; j++) {
      popupSaves.push(this.popups[j].getSave());
    }
    bookSave.popups = popupSaves;

    return bookSave;
  }
  getExport() {

    let allPages: PageExportVo[] = [];
    for (let i: number = 0; i < this.pages.length; i++) {
      allPages.push(this.pages[i].getExport());
    }

    let coverExport: PageExportVo = this.cover.getExport();

    let bookExport: BookExportVo = new BookExportVo(coverExport, allPages, this.bookInfo.w, this.bookInfo.h);

    let popupExports: PopUpExportVo[] = [];

    for (let j: number = 0; j < this.popups.length; j++) {
      popupExports.push(this.popups[j].getExport());
    }



    bookExport.popups = popupExports;
    console.log(bookExport);

    this.exportCode = JSON.stringify(bookExport);

    /*  console.log(allPages);
     this.exportCode=JSON.stringify(allPages); */
  }
  markSide(elements:ElementVo[],onLeft:boolean)
  {
    for (let i:number=0;i<elements.length;i++)
    {
      elements[i].onLeft=onLeft;
    }
    return elements;
  }
  getTemplateByID(id: number) {
    for (let i: number = 0; i < this.pageTemps.length; i++) {
      if (this.pageTemps[i].id === id) {
        return this.pageTemps[i].defElements;
      }
    }
    return this.pageTemps[0].defElements;
  }
  getDoublePagesByID(id: number) {
    for (let i: number = 0; i < this.dPageTemps.length; i++) {
      if (this.dPageTemps[i].id === id) {
        return this.dPageTemps[i];
      }
    }
    return this.dPageTemps[0];
  }
  getPopUpByID(id: number) {
    for (let i: number = 0; i < this.popups.length; i++) {
      if (this.popups[i].id === id) {
        return this.popups[i];
      }
    }
    return this.popups[0];
  }
}
export default MainStorage;
