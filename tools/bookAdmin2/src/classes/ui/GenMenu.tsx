import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { GenVo } from '../dataObjs/GenVo';
import GenCard from './GenCard';
interface MyProps {genVo:GenVo[],callback:Function}
interface MyState {}
class GenMenu extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
    this.state={};
}
getCards()
{
    let cards:JSX.Element[]=[];
    for (let i:number=0;i<this.props.genVo.length;i++)
    {
        let key:string="gencard"+i.toString();

        cards.push(<Col key={key}><GenCard data={this.props.genVo[i]} callback={this.props.callback}></GenCard></Col>)
    }
    return (<Row>{cards}</Row>);
}
render()
{
return (<div>{this.getCards()}</div>)
}
}
export default GenMenu;