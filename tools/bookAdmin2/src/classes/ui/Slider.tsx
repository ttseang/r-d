import React, { ChangeEvent, Component } from 'react';
import { KeyVo } from '../dataObjs/KeyVo';
import { OptionVo } from '../dataObjs/OptionVo';
interface MyProps {min:number,max:number, optionVo: OptionVo,currentVal:KeyVo,updateCallback:Function}
interface MyState {sliderVal:number}
class Slider extends Component<MyProps, MyState>
{
    public static defaultProps={min:0,max:100};
    constructor(props: MyProps) {
        super(props);
        this.state = {sliderVal:parseFloat(this.props.currentVal.value)};
    }
    onChange(e:ChangeEvent<HTMLInputElement>)
    {
        this.setState({sliderVal:parseFloat(e.currentTarget.value)});
        this.props.updateCallback(this.props.optionVo.key,this.state.sliderVal.toString());
    }
    render() {
        return (<div>
            <label htmlFor="customRange1" className="form-label">{this.props.optionVo.label}</label>
            <input type="range" className="form-range" id="slider" onChange={this.onChange.bind(this)} value={this.state.sliderVal} max={this.props.optionVo.max} min={this.props.optionVo.min}></input>
        </div>)
    }
}
export default Slider;