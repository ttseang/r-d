import { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { BookTempVo } from '../dataObjs/BookTempVo';
import MainStorage from '../MainStorage';
interface MyProps {selectCallback:Function, bookTempVo: BookTempVo }
interface MyState { }
class BookTempCard extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    tempClicked()
    {
        this.props.selectCallback(this.props.bookTempVo.id);
    }
    render() {
        let img: string = "./images/pageTemplates/" + this.props.bookTempVo.image;

        return (<Card>
            <Card.Title className='tac'>{this.props.bookTempVo.name}</Card.Title>
            <Card.Body><div className='thumb'><img className='tempThumb' src={img} alt='thumb' width={150} /></div>
                <div className='tac'><Button variant='success' onClick={()=>{this.tempClicked()}}>Select</Button></div></Card.Body>
        </Card>)
    }
}
export default BookTempCard;