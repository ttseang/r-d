import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { GenVo } from '../dataObjs/GenVo';
interface MyProps {data:GenVo,callback:Function}
interface MyState { }
class GenCard extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    getButton()
    {
        if (this.props.data.buttonText!=="")
        {
            return <Button variant={this.props.data.variant} onClick={()=>{this.props.callback(this.props.data.action)}}>{this.props.data.buttonText}</Button>
        }
        return "";
    }
    getImage()
    {
        if (this.props.data.image!=="")
        {
            if (this.props.data.useIcon===true)
            {
                let clss:string[]=[];
                clss.push("genIcon");
                clss.push(this.props.data.image);                
                return <i className={clss.join(" ")}></i>
            }
            return <img src={this.props.data.image} alt={this.props.data.title} />
        }
        return "";
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                <Row><Col className='tac'><h3>{this.props.data.title}</h3></Col></Row>
                    <Row><Col className='tac'>{this.getImage()}</Col></Row>
                    <Row><Col className='tac'>{this.getButton()}</Col></Row>                    
                </Card.Body>
            </Card>
        </div>)
    }
}
export default GenCard;