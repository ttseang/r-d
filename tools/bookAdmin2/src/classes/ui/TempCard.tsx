import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { PageTempVo } from '../dataObjs/PageTempVo';
import MainStorage from '../MainStorage';
interface MyProps {addCallback:Function, pageTempVo: PageTempVo }
interface MyState { }
class TempCard extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    tempClicked()
    {
        let len:number=this.ms.pages.length;
        let pageName:string="page"+len.toString();
        this.props.addCallback(pageName,this.props.pageTempVo);
    }
    render() {
        let img: string = "./images/pageTemplates/"+this.ms.bookID.toString()+"/"+ this.props.pageTempVo.thumb;
        
        let w:number=150;
        let h:number=150;

        if (this.ms.bookID===1)
        {
            h=225;
        }

        return (<Card>
            <Card.Title className='tac pageTitle'>{this.props.pageTempVo.title}</Card.Title>
            <Card.Body><div className='thumb'><img className='tempThumb' src={img} alt='thumb' width={w} height={h} /></div>
                <div className='tac'><Button variant='success' onClick={()=>{this.tempClicked()}}>ADD</Button></div></Card.Body>
        </Card>)
    }
}
export default TempCard;