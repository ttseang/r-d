import { DPageTempVo } from "./dataObjs/DPageTempVo";
import { ElementVo } from "./dataObjs/ElementVo";
//import { ElementVo } from "./dataObjs/ElementVo";
import { PageVo } from "./dataObjs/PageVo";
import MainStorage from "./MainStorage";

export class TemplateLoader
{
    private ms:MainStorage=MainStorage.getInstance();

    private pageData:any[]=[];
    private pageIDS:number[]=[];
    private bookID:number=0;
    private pageMap:Map<number,PageVo>=new Map<number,PageVo>();

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor()
    {
        
    }
    public getPageList(bookID:number)
    {
        this.bookID=bookID;

        let url:string="templates/pages/"+bookID.toString()+"/pageList.json";
        
        fetch(url)
        .then(response => response.json())
        .then(data => this.gotPageList(data));
    }
    private gotPageList(data:any)
    {
        console.log(data);
        let pageIDs:number[]=[];
        let pages:any=data.pageList;
        this.pageData=pages;
        console.log(pages);

        for (let i:number=0;i<pages.length;i++)
        {
            let left:number=parseInt(pages[i].left);
            let right:number=parseInt(pages[i].right);

            if (!pageIDs.includes(left))
            {
                pageIDs.push(left);
            }
           
            if (!pageIDs.includes(right))
            {
                pageIDs.push(right);
            }            
        }
        this.pageIDS=pageIDs;
        this.loadNextPage();
    }
    private loadNextPage()
    {
        if (this.pageIDS.length>0)
        {
            let id:number=this.pageIDS.pop() || 0;
             let url:string="templates/pages/"+this.bookID.toString()+"/page"+id+".json";
            fetch(url)
            .then(response => response.json())
            .then(data => this.pageLoaded(data));
        }
        else
        {
           // console.log("done");
            this.makeDoublePages();
        }
    }
    private pageLoaded(data:any)
    {
        //console.log(data);
        let pageVo:PageVo=new PageVo();
        let id:number=parseInt(data.page.id);
        pageVo.title="page"+id.toString();
        //console.log(id);
        pageVo.fromObj(data.elements);
        this.pageMap.set(id,pageVo);
        //console.log(pageVo);
        this.loadNextPage();
    }
    private makeDoublePages()
    {
        for (let i:number=0;i<this.pageData.length;i++)
        {
            console.log(this.pageData[i]);
            let leftID:number=this.pageData[i].left;
            let rightID:number=this.pageData[i].right;
            let name:string=this.pageData[i].name;
            let image:string=this.pageData[i].image;
            console.log(leftID,rightID);

           let rightPage:PageVo=this.pageMap.get(rightID) ||  new PageVo("",[]);
           let leftPage:PageVo=this.pageMap.get(leftID) ||  new PageVo("",[]);
           

           let rightElements:ElementVo[]=rightPage.elements;
           let leftElements:ElementVo[]=leftPage.elements;

           leftElements=this.ms.markSide(leftElements,true);
           rightElements=this.ms.markSide(rightElements,false);

           let dPage:DPageTempVo=new DPageTempVo(i,name,leftElements,rightElements,image);
           this.ms.dPageTemps.push(dPage);
        }
    }
}