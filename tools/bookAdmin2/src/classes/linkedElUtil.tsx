
import { DPageVo } from "./dataObjs/DPageVo";
import { ElementVo } from "./dataObjs/ElementVo";
import MainStorage from "./MainStorage";

export class LinkedElUtil {
    private pages: DPageVo;
    public linkMap: Map<string, ElementVo[]> = new Map<string, ElementVo[]>();
    private ms: MainStorage = MainStorage.getInstance();
    constructor(pages: DPageVo) {
        this.pages = pages;
        this.mapPages();

        (window as any).linkUtil=this;
    }
    mapPages() {
        let allElements: ElementVo[] = this.pages.left.elements.concat(this.pages.right.elements);
        for (let i: number = 0; i < allElements.length; i++) {
            let elementVo: ElementVo = allElements[i];
            if (elementVo.linkID !== "") {
                if (this.linkMap.has(elementVo.linkID)) {
                    this.linkMap.get(elementVo.linkID)?.push(elementVo);
                }
                else {
                    this.linkMap.set(elementVo.linkID, [elementVo]);
                }
            }
        }
    }
    public addNewLink(elementVo:ElementVo)
    {
        if (elementVo.linkID !== "") {
            if (this.linkMap.has(elementVo.linkID)) {
                this.linkMap.get(elementVo.linkID)?.push(elementVo);
            }
            else {
                this.linkMap.set(elementVo.linkID, [elementVo]);
            }
        }
    }
    getFirst(linkID: string): ElementVo | boolean {
        if (!this.linkMap.has(linkID)) {
            return false;
        }
        let elementVos: ElementVo[] | undefined = this.linkMap.get(linkID);
        if (elementVos === undefined) {
            return false;
        }
        return elementVos[0];
    }
    getLinkedEl(elementVo:ElementVo):ElementVo | null
    {
        if (elementVo.linkID === "") {
            return null;
        }
        if (!this.linkMap.has(elementVo.linkID)) {
            return null;
        }
        let elementVos: ElementVo[] | undefined = this.linkMap.get(elementVo.linkID);
        if (elementVos === undefined) {
            return null;
        }
        for (let i: number = 0; i < elementVos.length; i++) {
            let element2: ElementVo = elementVos[i];
            if (element2.id !== elementVo.id) {
                console.log("matched "+elementVo.id.toString()+" with "+element2.id.toString());
                return element2;
            }
        }
        return null;
    }
    checkLink(elementVo: ElementVo) {

        if (elementVo.linkID === "") {
            return;
        }
        if (!this.linkMap.has(elementVo.linkID)) {
            return false;
        }

        let elementVos: ElementVo[] | undefined = this.linkMap.get(elementVo.linkID);
        if (elementVos === undefined) {
            return false;
        }
        console.log("looking for "+elementVo.linkID);
        console.log(elementVos);
      //  //console.log.log(elementVo.onLeft);

       

        for (let i: number = 0; i < elementVos.length; i++) {
            let element2: ElementVo = elementVos[i];
            if (element2.id !== elementVo.id) {
                if (elementVo.subType === ElementVo.SUB_TYPE_GUTTER) {

                    console.log("linked el id="+elementVo.id);

                    let ww: number = this.ms.getPageW();

                    if (elementVo.onLeft === true) {
                        let xx: number = elementVo.extras.backgroundPosX;


                        let diff1: number = ww - xx;
                        element2.extras.backgroundPosX = -diff1;

                        //console.log.log(ww, xx, diff1);
                    }
                    else {
                        let xx2: number = elementVo.extras.backgroundPosX;

                        let diff2: number = xx2 - ww;
                        //console.log.log(ww, xx2, diff2);
                        element2.extras.backgroundPosX = -diff2;
                        element2.extras.backgroundPosY = elementVo.extras.backgroundPosY;
                    }

                    element2.extras.backgroundPosY = elementVo.extras.backgroundPosY;
                    element2.extras.backgroundSizeH = elementVo.extras.backgroundSizeH;
                    element2.extras.backgroundSizeW = elementVo.extras.backgroundSizeW;
                    element2.y = elementVo.y;

                    element2.content=elementVo.content;

                    //console.log.log(element2);
                    //console.log.log(elementVo);
                }
            }
        }
        return true;
    }
}