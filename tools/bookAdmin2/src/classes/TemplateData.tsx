import BookElement from "../comps/BookElement";
import { ElementVo } from "./dataObjs/ElementVo";
import { PageTempVo } from "./dataObjs/PageTempVo";
import { PageVo } from "./dataObjs/PageVo";
import { PopUpVo } from "./dataObjs/PopUpVo";
import MainStorage from "./MainStorage";

export class TemplateData {
    private ms: MainStorage = MainStorage.getInstance();

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {

    }
    public getPopUp(popupVo:PopUpVo,callback:Function,onUp:Function)
    {
        let elements:ElementVo[]=popupVo.elements;
        // let index:number=pageVo.templateID;
         
         let xx:number=0;
         let hh:number=popupVo.h;
         let ww:number=popupVo.w;
         let ww2:number=ww*2;
 
         let pageElements:JSX.Element[]=[];
         for (let i:number=0;i<elements.length;i++)
         {  
             let key:string="be"+i.toString();
             pageElements.push(<BookElement key={key} elementVo={elements[i]} callback={callback} onUp={onUp}></BookElement>);
         }
 
         let pageClass:string="pageStart";
 
         if (this.ms.bookInfo.h===450)
        {
            pageClass="pageStart2";
           
        }
 
         return (<foreignObject className={pageClass} x={xx} y="0" width={ww} height={this.ms.bookInfo.h} id="ptblank">
         <div className="pg2" style={{height:this.ms.bookInfo.h,width:this.ms.bookInfo.w}}>
             {pageElements}
             <figure className="guttershadow" style={{"height":"450px","backgroundSize":ww2.toString()+"px "+hh.toString()+"px"}}></figure>
            
         </div>
     </foreignObject>);
    }
    public getPage2(pageVo:PageVo,left:boolean,callback:Function,onUp:Function)
    {
        let elements:ElementVo[]=pageVo.elements;
       // let index:number=pageVo.templateID;
        
        let xx:number=(left===true)?-150:150;
        let hh:number=this.ms.getPageH();
        let ww:number=this.ms.getPageW();
        let ww2:number=ww*2;

        let pageElements:JSX.Element[]=[];
        for (let i:number=0;i<elements.length;i++)
        {  
            let key:string="be"+i.toString();
            pageElements.push(<BookElement key={key} elementVo={elements[i]} callback={callback} onUp={onUp}></BookElement>);
        }

        let pageClass:string="pageStart";
        let bookClass:string="pg2 paper1 rbook2";

        if (hh===450)
        {
            pageClass="pageStart2";
            bookClass="pg2 paper1 rbook"
        }

        return (<foreignObject className={pageClass} x={xx} y="0" width={ww} height={hh} id="ptblank">
        <div className={bookClass}>
            {pageElements}
            <figure className="guttershadow" style={{"height":"450px","backgroundSize":ww2.toString()+"px "+hh.toString()+"px"}}></figure>
           
        </div>
    </foreignObject>);

    }
    
    getDefaultPopUpData(templateID:number)
    {
        let defElements:ElementVo[]=[];

        switch(templateID)
        {
            case 0:

               
                let popback:ElementVo=new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,[],["popback.svg"]);
                popback.x=50;
                popback.y=50;
                popback.w=100;
                popback.h=100;
                popback.extras.orientation=4;
         
                defElements.push(popback);

                let textEl:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,["centerText"],["It was the best of times, it was the worst of times"],50,40,80);
                textEl.extras.fontColor="white";
                textEl.extras.orientation=4;
                 defElements.push(textEl);

               // defElements.push(new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,["centerText"],["Once upon a time there was a little girl"]));
               // defElements.push(new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,["image1"],["fairy3.svg"]));

            break;
        }
       
        return defElements;
    }
    getDefaultData(bookID: number) {
       // let textDefaults: string[] = ["The Fairy girl", "Once upon a time there was a little fairy girl who loved to dance.", "She would dance around her tree, and sing to the birds, and make new friends with the flowers."]

        let defElements:ElementVo[]=[];
        defElements.push(new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,["centerText"],["Once upon a time there was a little girl"]));
        defElements.push(new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,["image1"],["fairy3.svg"]));
        let pageTemps: PageTempVo[] = [];

        switch (bookID) {
            case 0:
                pageTemps.push(new PageTempVo(-1, "blank", 0, 0, "0.jpg", [], []));

                let centerImage:ElementVo=new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,[],["fairy3.svg"],50,50,50);
                centerImage.extras.orientation=4;

                let leftImage:ElementVo=new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,[],["fairy3.svg"],25,65,44);
                leftImage.extras.orientation=4;

                let rightImage:ElementVo=new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,[],["tree2.svg"],100,100,49);
                rightImage.onLeft=false;
                rightImage.extras.orientation=8;

                let leftText2:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,[],["Once upon a time there was a little girl who loved trees, and liked to talk to them whenever she could"],53,17,32);
                //But one day the tree wasn't there! Where could it have gone? Then suddenly out of the woods...
                
                let rightText2:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,[],["But one day the tree wasn't there! Where could it have gone? Then suddenly out of the woods..."],0,5,45);
                rightText2.onLeft=false;
                let smallImage:ElementVo=new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,[],["fairy2.svg"],50,50,15);
                smallImage.extras.orientation=4;

                //She could not believe her eyes! "Who are you?" asked the first fairy. 

                let smallText:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,[],["There was another fairy standing there! She had never seen another fairy before!"],45,20,90);
                smallText.extras.orientation=4;

                let smallText2:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,[],["She could not believe her eyes! 'Who are you?' asked the first fairy."],45,80,90);
                smallText2.extras.orientation=4;

                pageTemps.push(new PageTempVo(0, "Center Image", 0, 1, "1.jpg", [centerImage]));
                pageTemps.push(new PageTempVo(1, "Left Image", 3, 1, "2.jpg", [leftImage,leftText2]));
                pageTemps.push(new PageTempVo(2, "Right Image", 3, 1, "3.jpg", [rightImage,rightText2]));
                pageTemps.push(new PageTempVo(3, "Small Image", 3, 1, "4.jpg",[smallImage,smallText,smallText2]));
                break;

            case 1:


                let ipsum: string[] = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut iaculis eros. Morbi accumsan metus quis turpis mattis, nec bibendum tortor vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit."];
                pageTemps.push(new PageTempVo(-1, "blank", 0, 0, "0.jpg", [], []));

                let leftEdgeElements:ElementVo[]=[];
                let leftEdgeImage:ElementVo=new ElementVo(ElementVo.TYPE_BACK_IMAGE,ElementVo.SUB_TYPE_NONE,["fullHeight"],["brushes.png"],0,0,25)
                leftEdgeImage.suggested="edge";
                leftEdgeElements.push(leftEdgeImage);
                
                let leftText:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,[],[ipsum[0]],50,20,80);
                leftText.extras.orientation=4;
                leftText.extras.fontSize=16;
                leftText.extras.fontColor="#000000"
                
                leftEdgeElements.push(leftText);

                let rightEdgeElements:ElementVo[]=[];
                let edgeImage:ElementVo=new ElementVo(ElementVo.TYPE_BACK_IMAGE,ElementVo.SUB_TYPE_NONE,["flipped","fullHeight"],["brushes.png"],75,0,25);
                edgeImage.extras.flipH=true;
                edgeImage.suggested="edge";
                edgeImage.onLeft=false;
                
                rightEdgeElements.push(edgeImage);
                let rightText:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,[],[ipsum[0]],50,20);
                rightText.extras.orientation=4;
                rightText.extras.fontSize=18;
                rightText.extras.fontColor="#e22222";
                rightText.onLeft=false;
                rightEdgeElements.push(rightText);

                pageTemps.push(new PageTempVo(0, "Left Edge", 1, 1, "1.jpg", leftEdgeElements));
                pageTemps.push(new PageTempVo(1, "Right Edge", 1, 1, "2.jpg", rightEdgeElements));

                let crossGutterLElements:ElementVo[]=[];
                let gutter1:ElementVo=new ElementVo(ElementVo.TYPE_BACK_IMAGE,ElementVo.SUB_TYPE_GUTTER,["crossGutter2"],["wienerdog.png"]);
                gutter1.onLeft=true;
                gutter1.suggested="gutter";
                gutter1.linkID="dog1";
                gutter1.y=25;
                gutter1.w=300;
                gutter1.h=300;
                gutter1.extras.backgroundPosX=30;
                gutter1.extras.backgroundPosY=0;
                gutter1.extras.backgroundSizeW=540;
                gutter1.extras.backgroundSizeH=-1;
                crossGutterLElements.push(gutter1);


                let crossGutterRElements:ElementVo[]=[];
                let gutter2:ElementVo=new ElementVo(ElementVo.TYPE_BACK_IMAGE,ElementVo.SUB_TYPE_GUTTER,["crossGutter2"],["wienerdog.png"]);
                gutter2.linkID="dog1";
                gutter2.onLeft=false;
                gutter2.suggested="gutter";
                gutter2.y=25;
                gutter2.w=300;
                gutter2.h=300;
                gutter2.extras.backgroundPosX=-270;
                gutter2.extras.backgroundPosY=0;
                gutter2.extras.backgroundSizeW=540;
                gutter2.extras.backgroundSizeH=-1;
                crossGutterRElements.push(gutter2);

                pageTemps.push(new PageTempVo(4, "Cross Gutter Left", 1, 1, "6.jpg", crossGutterLElements));
                pageTemps.push(new PageTempVo(5, "Cross Gutter Right", 1, 1, "5.jpg", crossGutterRElements));

                pageTemps.push(new PageTempVo(2, "Bottom Images", 1, 2, "3.jpg", defElements));
                pageTemps.push(new PageTempVo(3, "Bottom Images2", 3, 1, "4.jpg", defElements));

               /*  pageTemps.push(new PageTempVo(0, "Left Edge", 1, 1, "1.jpg", ipsum.slice(), ["brushes.png"]));
                pageTemps.push(new PageTempVo(1, "Right Edge", 1, 1, "2.jpg", ipsum.slice(), ["brushes.png"]));

                pageTemps.push(new PageTempVo(4, "Cross Gutter Left", 1, 1, "6.jpg", ipsum.slice(), ["wienerdog.png"],[slider1]));
                pageTemps.push(new PageTempVo(5, "Cross Gutter Right", 1, 1, "5.jpg", ipsum.slice(), ["wienerdog.png"],[slider1]));

                pageTemps.push(new PageTempVo(2, "Bottom Images", 1, 2, "3.jpg", ipsum.slice(), ["test1.jpg", "test2.jpg"]));
                pageTemps.push(new PageTempVo(3, "Bottom Images2", 3, 1, "4.jpg", ipsum.slice(), ["test3.jpg", "test4.jpg"])); */

                break;
        }
        return pageTemps;
    }
}
