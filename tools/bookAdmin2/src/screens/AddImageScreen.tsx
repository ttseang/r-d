import React, { Component } from 'react';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { StyleVo } from '../classes/dataObjs/StyleVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import MediaScreen from './MediaScreen';
import StyleScreen from './StyleScreen';
interface MyProps { callback: Function }
interface MyState { mode: number, styleVos: StyleVo[] }
class AddImageScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private libVo: LibItemVo | null = null;
    private pos: StyleVo | null = null;
    private size: StyleVo | null = null;


    constructor(props: MyProps) {
        super(props)
        this.state = { mode: 0, styleVos: [] };
    }
    getScreen() {
        switch (this.state.mode) {
            case 0:
                this.mc.mediaChangeCallback = this.selectImage.bind(this)
                return <MediaScreen library={this.ms.library} closeCallback={() => { }}></MediaScreen>
            case 1:
                this.mc.styleChangeCallback = this.selectStyle.bind(this);
                return <StyleScreen styles={this.state.styleVos}></StyleScreen>
        }
    }
    selectImage(libVo: LibItemVo) {
        this.libVo = libVo;

        //   let styles: StyleVo[] = this.ms.styleUtil.filterStyles(StyleVo.TYPE_IMAGE, StyleVo.SUBTYPE_POSITION);
        let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_IMAGE, ElementVo.SUB_TYPE_NONE, [], [this.libVo.image], 0, 0, 20);


        elementVo.onLeft = true;
        this.ms.pages[this.ms.pageEditIndex].left.addElement(elementVo);
        this.ms.selectedElement=elementVo;

        this.props.callback();


        //this.setState({ mode: 1, styleVos: styles });
    }
    selectStyle(styleVo: StyleVo) {

        if (styleVo.subType === StyleVo.SUBTYPE_POSITION) {
            this.pos = styleVo;
            let styles: StyleVo[] = this.ms.styleUtil.filterStyles(StyleVo.TYPE_IMAGE, StyleVo.SUBTYPE_SIZE);
            this.setState({ styleVos: styles });
        }
        if (styleVo.subType === StyleVo.SUBTYPE_SIZE) {
            this.size = styleVo;



            if (this.libVo && this.pos && this.size) {

                let classes: string[] = this.pos.classes.concat(this.size.classes);

                let xx: number = this.pos.x;
                let yy: number = this.pos.y;


                switch (this.pos.xAdjust) {
                    case StyleVo.ADJUST_FULL:
                        xx = this.pos.x - this.size.w;

                        break;
                    case StyleVo.ADJUST_HALF:
                        xx = this.pos.x - this.size.w / 2;

                }
                switch (this.pos.yAdjust) {
                    case StyleVo.ADJUST_FULL:
                        yy = this.pos.y - this.size.w;
                        break;

                    case StyleVo.ADJUST_HALF:
                        yy = this.pos.y - this.size.w / 2;
                        break;
                }

                let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_IMAGE, ElementVo.SUB_TYPE_NONE, classes, [this.libVo.image], xx, yy, this.size.w);


                elementVo.onLeft = true;
                this.ms.pages[this.ms.pageEditIndex].left.addElement(elementVo);
                this.props.callback();
            }
        }
    }
    render() {
        return (<div>
            {this.getScreen()}

        </div>)
    }
}
export default AddImageScreen;