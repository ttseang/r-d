import React, { ChangeEvent, Component } from 'react';
import { Col, Row, Card, Button, ButtonGroup } from 'react-bootstrap';
import { AlignUtil } from '../classes/AlignUtil';
import { BookSaver } from '../classes/BookSaver';
import { ButtonIconVo } from '../classes/dataObjs/ButtonIconVo';
import { DPageVo } from '../classes/dataObjs/DPageVo';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../classes/dataObjs/ExtrasVo';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { PageVo } from '../classes/dataObjs/PageVo';
import { LinkedElUtil } from '../classes/linkedElUtil';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import ElementBar from '../comps/ElementBar';
import PreviewPage from '../comps/PreviewPage';
import PropBox from '../comps/rightsidepanel/PropBox';
interface MyProps { pages: DPageVo, doneCallback: Function }
interface MyState { pages: DPageVo, sideMode: number, selectedElement: ElementVo | null, currentPage: PageVo, pageIndex: number,  showLeft: boolean }

class PagesEdit extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private editElement: ElementVo | null = null;
    private linkedELUtil: LinkedElUtil | null = null;

    private isDown: boolean = false;
    private addElements: ButtonIconVo[] = [];

    constructor(props: MyProps) {
        super(props);        
        this.addElements.push(new ButtonIconVo("Image", "fas fa-image", "light", 0, "sm"));
        this.addElements.push(new ButtonIconVo("Text", "fas fa-font", "light", 1, "sm"));
        this.addElements.push(new ButtonIconVo("Gutter Image", "fas fa-columns", "light", 2, "sm"));
        this.addElements.push(new ButtonIconVo("Pop Up", "far fa-window", "light", 3, "sm"));
        this.addElements.push(new ButtonIconVo("Special", "far fa-smile", "light", 4, "sm"));

        this.mc.doKeyboardShortCut = this.doShortCut.bind(this);

        this.state = { selectedElement: null, sideMode: 0, currentPage: this.props.pages.left, pages: this.props.pages, pageIndex: 0, showLeft: true };
    }
    componentDidMount() {
        if (this.ms.selectedElement !== null) {
            this.setState({ selectedElement: this.ms.selectedElement });
        }
        this.linkedELUtil = new LinkedElUtil(this.state.pages);
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ pages: this.props.pages });
        }
    }
    copyElementToClipboard()
    {
        if (this.ms.selectedElement)
        {
            this.ms.copyElement=this.ms.selectedElement.clone();
        }        
    }
    pasteElement()
    {
        if (this.ms.copyElement)
        {
            let pasteElement:ElementVo=this.ms.copyElement.clone();
            
            this.copyElement(pasteElement);
           // this.ms.addHistory();
        }        
    }
   

    doShortCut(key: string, ctr: boolean, alt: boolean) {
        /* console.log(key);
        console.log(ctr);
        console.log(alt); */       

        let cString: string = (ctr === false) ? "0" : "1";
        let aString: string = (alt === false) ? "0" : "1";

        let keyString: string = cString + aString + key;
        console.log(keyString);

        switch (keyString) {
            case "10z":
             //   this.goBack();
                break;

            case "10y":
               // this.goForward();
                break;

            case "10c":
                this.copyElementToClipboard();
                break;
            case "10v":
                this.pasteElement();
                break;

            case "00Delete":
                if (this.state.selectedElement)
                {
                    this.deleteElement(this.state.selectedElement);
                  //  this.ms.addHistory();
                    this.setState({selectedElement:null});
                }
               
                break;
        }


    }
    onMove(e: React.PointerEvent<SVGElement>) {
        e.preventDefault();

        if (this.isDown === true && this.state.selectedElement) {


            let selected: ElementVo = this.state.selectedElement;

            /**
             * convert movement in pixels to percentages
             */
            let perMoveX: number = Math.floor((e.movementX / this.ms.bookInfo.w) * 10000) / 100;
            let perMoveY: number = Math.floor((e.movementY / this.ms.bookInfo.h) * 10000) / 100;

            

            if (this.state.selectedElement.subType === ElementVo.SUB_TYPE_GUTTER) {
                

                selected.extras.backgroundPosX += perMoveX;
                selected.y += perMoveY;

                if (this.linkedELUtil) {
                    this.linkedELUtil.checkLink(selected);
                }

                this.setState({ selectedElement: selected });
                return;
            }


            /**
             * update position
             */
            selected.x += perMoveX;
            selected.y += perMoveY;

            this.setState({ selectedElement: selected });

        }

    }
    releaseElement(elementVo: ElementVo) {
        this.isDown = false;
    }
    onElementDown(elementVo: ElementVo) {
        
        this.selectElement(elementVo);
        this.isDown = true;
    }
    toggleVis(elementVo:ElementVo)
    {
        elementVo.visible=!elementVo.visible;
        this.ms.selectedElement=elementVo;

        if (this.linkedELUtil)
        {
            let linkedEl:ElementVo | null=this.linkedELUtil.getLinkedEl(elementVo);
            if (linkedEl)
            {
                linkedEl.visible=elementVo.visible;
            }
        }

        this.setState({ selectedElement: elementVo});
    }
    toggleLock(elementVo:ElementVo)
    {
        elementVo.locked=!elementVo.locked;
        this.ms.selectedElement=elementVo;

        if (this.linkedELUtil)
        {
            let linkedEl:ElementVo | null=this.linkedELUtil.getLinkedEl(elementVo);
            if (linkedEl)
            {
                linkedEl.locked=elementVo.locked;
            }
        }

        this.setState({ selectedElement: elementVo});
    }
    selectElement(elementVo: ElementVo) {

        if (elementVo.linkID !== "") {
            if (this.linkedELUtil) {
                let element2: ElementVo | boolean = this.linkedELUtil.getFirst(elementVo.linkID);
                if (element2 !== false) {
                    elementVo = element2 as ElementVo;
                }
            }
        }
        this.ms.suggested = elementVo.suggested;
        this.ms.selectedElement = elementVo;        

        this.setState({ selectedElement: elementVo, sideMode: 0, showLeft: elementVo.onLeft });
    }
    editTheElement(elementVo: ElementVo | null) {
        if (elementVo === null) {
            return;
        }

        this.editElement = elementVo;
        if (this.editElement.type === ElementVo.TYPE_IMAGE || this.editElement.type === ElementVo.TYPE_BACK_IMAGE) {
            this.mc.mediaChangeCallback = this.updateElementImage.bind(this);
            this.mc.openImageBrowse();
        }
        if (this.editElement.type === ElementVo.TYPE_TEXT) {
            this.ms.editText = this.editElement.content[0];
            this.mc.textEditCallback = this.updateElementText.bind(this);
            this.mc.openTextEdit();
        }
    }
    updateElementImage(libItem: LibItemVo) {
              if (this.editElement) {
            this.editElement.content[0] = libItem.image;
            this.mc.closeImageBrowse();
            if (this.linkedELUtil) {
                let linkedEL: ElementVo | null = this.linkedELUtil.getLinkedEl(this.editElement);
                if (linkedEL) {
                    linkedEL.content[0] = libItem.image;
                }
            }
        }
    }
    updateElementText(text: string) {
        if (this.editElement) {
            this.editElement.content[0] = text;
            this.mc.closeTextEdit();
        }
    }
    updateProps(xx: number, yy: number, ww: number) {
        

        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.x = xx;
            selected.y = yy;
            selected.w = ww;
            if (this.linkedELUtil) {
                this.linkedELUtil.checkLink(selected);
            }
            this.setState({ selectedElement: selected });
        }
    }
    updateBackgroundProps(xx: number, yy: number, ww: number, hh: number) {
        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.extras.backgroundPosX = xx;
            selected.y = yy;
            selected.extras.backgroundSizeW = ww;
            if (this.linkedELUtil) {
                this.linkedELUtil.checkLink(selected);
            }
            this.setState({ selectedElement: selected });
        }
    }
    getPreview() {
        let pSizeClasses: string[] = ['preview', 'preview2'];
        let sizeClass: string = pSizeClasses[this.ms.bookID];
        return (<div id={sizeClass}><PreviewPage editCallback={this.onElementDown.bind(this)} onUp={this.releaseElement.bind(this)} pageVo={this.state.pages} onMove={this.onMove.bind(this)}></PreviewPage></div>)
    }
    changePageName(e: ChangeEvent<HTMLInputElement>) {
        let pages: DPageVo = this.state.pages;
        pages.name = e.currentTarget.value;
        this.setState({ pages: pages });
    }
    changeElementPage() {
        if (this.state.selectedElement) {
            let leftPages: ElementVo[] = this.state.pages.left.elements.slice();
            let rightPages: ElementVo[] = this.state.pages.right.elements.slice();
            let elementVo: ElementVo = this.state.selectedElement;

            if (elementVo.onLeft === true) {
                for (let i: number = 0; i < leftPages.length; i++) {
                    if (leftPages[i].id === elementVo.id) {
                        leftPages.splice(i, 1);
                        elementVo.onLeft = false;
                        rightPages.push(elementVo);
                    }
                }
            }
            else {
                for (let i: number = 0; i < rightPages.length; i++) {
                    if (rightPages[i].id === elementVo.id) {
                        rightPages.splice(i, 1);
                        elementVo.onLeft = true;
                        leftPages.push(elementVo);
                    }
                }
            }
            let pages: DPageVo = this.state.pages;
            pages.left.elements = leftPages;
            pages.right.elements = rightPages;
            this.setState({ pages: pages });
        }
    }
    
    
    updateElementRows(pos: number[]) {
        

        let allElements: ElementVo[] = (this.state.showLeft === true) ? this.state.pages.left.elements : this.state.pages.right.elements;

        let nElements: ElementVo[] = [];
        for (let i: number = 0; i < allElements.length; i++) {
            nElements.push(allElements[pos[i]]);
        }
       
        if (this.state.showLeft === true) {
            this.ms.pages[this.ms.pageEditIndex].left.elements = nElements;
        }
        else {
            this.ms.pages[this.ms.pageEditIndex].right.elements = nElements;
        }
        
    }
    showPageElements(showLeft: boolean) {
        this.setState({ showLeft: showLeft });
    }
   

    getTextInput() {
        return (
            <div>
                <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    onChange={this.changePageName.bind(this)}
                    value={this.state.pages.name}
                /></div>
        )
    }
    getSideContent() {

        let allElements: ElementVo[] = (this.state.showLeft === true) ? this.state.pages.left.elements : this.state.pages.right.elements;
        return (<div>
            <ElementBar addElements={this.addElements} sideMode={this.state.sideMode} elements={allElements} selectedElement={this.state.selectedElement} addNewElement={this.mc.addNewElement} updateElementRows={this.updateElementRows.bind(this)} selectElement={this.selectElement.bind(this)} toggleVis={this.toggleVis.bind(this)} toggleLock={this.toggleLock.bind(this)}>
            </ElementBar>
            <ButtonGroup><Button variant='light' disabled={(this.state.showLeft === true)} onClick={() => { this.showPageElements(true) }}>Left</Button><Button variant='light' disabled={(this.state.showLeft === false)} onClick={() => { this.showPageElements(false) }}>Right</Button></ButtonGroup>
        </div>);
    }
    doPropAction(action: number, param: number = 0) {
        

        if (this.state.selectedElement) {

            let elementVo: ElementVo | null = this.state.selectedElement;

            switch (action) {
                case 0:
                    this.changeElementPage();
                    break;
                case 1:
                    let flippedH: boolean = !elementVo.extras.flipH;
                    elementVo.extras.flipH = flippedH;
                    break;

                case 2:
                    let flippedV: boolean = !elementVo.extras.flipV;
                    elementVo.extras.flipV = flippedV;
                    break;

                case 3:
                    let alignUtil: AlignUtil = new AlignUtil();
                    alignUtil.alignElement(param, elementVo);

                    break;

                case 4:
                    elementVo.extras.orientation = param;
                    break;

                case 5:
                    this.editTheElement(this.state.selectedElement);
                    break;

                case 12:
                    elementVo = this.copyElement(elementVo);
                    break;
                case 13:
                    this.deleteElement(elementVo);
                    elementVo = null;
                    break;
            }

            if (action > 0) {
                this.setState({ selectedElement: elementVo });
            }
        }
    }
    deleteElement(delElement: ElementVo) {
        let dPageVo: PageVo = this.ms.pages[this.ms.pageEditIndex].right;
        if (delElement.onLeft === true) {
            dPageVo = this.ms.pages[this.ms.pageEditIndex].left;
        }

        let linkedElement: ElementVo | null = null;

        if (this.linkedELUtil) {
            linkedElement = this.linkedELUtil.getLinkedEl(delElement);
        }

        let elements: ElementVo[] = dPageVo.deleteElement(delElement);

        if (delElement.onLeft === true) {
            this.ms.pages[this.ms.pageEditIndex].left.elements = elements;
        }
        else {
            this.ms.pages[this.ms.pageEditIndex].right.elements = elements;
        }

        if (linkedElement) {
            linkedElement.linkID = "";
            this.deleteElement(linkedElement);
        }

    }
    copyElement(copyElementVo: ElementVo) {
        let elementVo: ElementVo = copyElementVo.clone();

        let linkedElement: ElementVo | null = null;

        if (this.linkedELUtil) {
            linkedElement = this.linkedELUtil.getLinkedEl(copyElementVo);
        }

        if (elementVo.onLeft === true) {
            this.ms.pages[this.ms.pageEditIndex].left.addElement(elementVo);
        }
        else {
            this.ms.pages[this.ms.pageEditIndex].right.addElement(elementVo);
        }

        if (linkedElement) {
            let linkedElement2: ElementVo = linkedElement.clone();
            linkedElement2.linkID = elementVo.linkID;

            if (linkedElement2.onLeft === true) {
                this.ms.pages[this.ms.pageEditIndex].left.addElement(linkedElement2);
            }
            else {
                this.ms.pages[this.ms.pageEditIndex].right.addElement(linkedElement2);
            }
            if (this.linkedELUtil) {
                this.linkedELUtil.addNewLink(elementVo);
                this.linkedELUtil.addNewLink(linkedElement2);
            }
        }

        this.ms.selectedElement = elementVo;
        return elementVo;
    }
    updateExtras(extrasVo: ExtrasVo) {
        
        if (this.state.selectedElement) {
            let elementVo: ElementVo = this.state.selectedElement;
            elementVo.extras = extrasVo;
            this.setState({ selectedElement: elementVo });
            if (this.linkedELUtil) {
                this.linkedELUtil.checkLink(elementVo);
            }
        }
    }
    getPropBox() {
        return (<PropBox key="propBox" dpage={this.state.pages} elementVo={this.state.selectedElement} callback={this.updateProps.bind(this)} actionCallback={this.doPropAction.bind(this)} updateExtras={this.updateExtras.bind(this)} updateBackgroundProps={this.updateBackgroundProps.bind(this)}></PropBox>)
    }
    
    doSave()
    {
        let bookSaver:BookSaver=new BookSaver(this.saveDataSent.bind(this));
        bookSaver.saveBook();
    }
    saveDataSent()
    {
        
    }
    
    render() {
        return (<div onPointerUp={(e: React.PointerEvent<HTMLDivElement>) => { this.isDown = false }}>
            <Row><Col></Col><Col className="tac">{this.getTextInput()}</Col><Col></Col></Row>
            <Row><Col>{this.getSideContent()}</Col><Col>{this.getPreview()}</Col><Col>{this.getPropBox()}</Col></Row>

            <Card>
                <Card.Body>
                    <Row><Col></Col><Col className='tac'><Button variant='success' onClick={() => { this.props.doneCallback() }}>Done</Button></Col><Col><Button variant="success" onClick={this.doSave.bind(this)}>Save</Button></Col></Row>
                </Card.Body>
            </Card>

        </div>)
    }
}
export default PagesEdit;