import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { DPageTempVo } from '../classes/dataObjs/DPageTempVo';
import { DPageVo } from '../classes/dataObjs/DPageVo';
import { PageVo } from '../classes/dataObjs/PageVo';
import MainStorage from '../classes/MainStorage';
import PagesSelectBox from '../comps/PagesSelectBox';
interface MyProps {addCallback:Function}
interface MyState { }
class AddDoubleScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    getTemplates()
    {
        let boxes:JSX.Element[]=[];
        for (let i:number=0;i<this.ms.dPageTemps.length;i++)
        {
            let key:string="tempbox"+i.toString();

            boxes.push(<Col key={key} sm={4}><PagesSelectBox selectCallback={this.addPage.bind(this)} pageData={this.ms.dPageTemps[i]}></PagesSelectBox></Col>)
        }
        return (<Row>{boxes}</Row>);
    }
    addPage(title:string,template: DPageTempVo) {
              

        let leftPage:PageVo=new PageVo(title, template.left);
        let rightPage:PageVo=new PageVo(title,template.right);

        let pageName:string="page"+this.ms.pages.length.toString();
        let dpage:DPageVo=new DPageVo(pageName,leftPage,rightPage);
        this.ms.pages.push(dpage);
        this.ms.pageEditIndex=this.ms.pages.length-1;
        this.props.addCallback();
      
    }
    render() {
        return (<div className='scroll1'>{this.getTemplates()}</div>)
    }
}
export default AddDoubleScreen;