import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
interface MyProps {editCallback:Function}
interface MyState { }
class PreviewScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {

        let loc:string=window.location.href;
        let ppath:string=loc+"preview/";
        console.log(ppath);

        return (<div>
            <div id="bookData" style={{display:"none"}}>{this.ms.exportCode} </div>
            <div className='tac'>
            <iframe src={ppath} title='book preview' width={620} height={480} frameBorder={0} scrolling="no" >
            </iframe>
            <hr/>
            <Button variant='success' onClick={()=>{this.props.editCallback()}}>EDIT</Button>
            </div>
        </div>)
    }
}
export default PreviewScreen;