import { Component } from 'react';
import { Alert, Card } from 'react-bootstrap';
import ApiConnect from '../ApiConnect';
import { BookSaver } from '../classes/BookSaver';
import { DPageVo } from '../classes/dataObjs/DPageVo';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { FileVo } from '../classes/dataObjs/FileVo';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { MsgVo } from '../classes/dataObjs/MsgVo';
import { PopUpVo } from '../classes/dataObjs/PopUpVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import { StartData } from '../classes/StartData';
import { TemplateData } from '../classes/TemplateData';
import { TemplateLoader } from '../classes/TemplateLoader';
import OldNewScreen from '../screens/OldNewScreen';
import AddDoubleScreen from './AddDouble';
import AddImageScreen from './AddImageScreen';
import BookMenuScreen from './BookMenuScreen';
import BooKTempScreen from './BookTempScreen';
import FileNameScreen from './FileNameScreen';
import LoadScreen from './LoadScreen';
import MediaScreen from './MediaScreen';
import PagesEdit from './PagesEdit';
import PopupEditScreen from './PopupEditScreen';
import PreviewScreen from './PreviewScreen';
import StyleScreen from './StyleScreen';
import TextEditor from './TextEditor';
import TextStyleScreen from './TextStyleScreen';

interface MyProps { }
interface MyState { mode: number, showTextEdit: boolean, showImageBrowse: boolean, showStyles: boolean,msg:MsgVo | null }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: -1, showTextEdit: false, showImageBrowse: false, showStyles: false,msg:null };
        this.mc.openImageBrowse = this.openMedia.bind(this);
        this.mc.closeImageBrowse = this.closeMedia.bind(this);
        this.mc.openTextEdit = this.openText.bind(this);
        this.mc.closeTextEdit = this.closeText.bind(this);
        this.mc.changeScreen = this.changeScreen.bind(this);
        this.mc.addNewElement = this.addNewElement.bind(this);
        this.mc.saveBook = this.saveBook.bind(this);
        this.mc.setAlert=this.setAlert.bind(this);


        document.addEventListener("keyup", (e: KeyboardEvent) => {
            if (this.ms.keyLock === false) {
                this.mc.doKeyboardShortCut(e.key, e.ctrlKey, e.altKey);
                this.ms.keyLock = true;

                setTimeout(() => {
                    this.ms.keyLock = false;
                }, 250);
            }
        })

    }
    componentDidMount() {
        let sd: StartData = new StartData(() => { this.setState({ mode: -1 }) });
        sd.start();
    }
    changeScreen(screenIndex: number) {
        this.setState({ mode: screenIndex });
    }
    setAlert(msg:MsgVo)
    {
        this.setState({msg:msg});
    }
    getScreen() {
        if (this.state.showImageBrowse === true || this.state.showTextEdit === true || this.state.showStyles === true) {
            return "";
        }
        switch (this.state.mode) {

            case -3:
                return "LOADING";

            case -1:
                return <OldNewScreen></OldNewScreen>

            case -2:
                return <LoadScreen openCallback={this.openFile.bind(this)} cancelCallback={this.doCancel.bind(this)}></LoadScreen>

            case 0:
                return <BooKTempScreen tempSelected={() => { this.setState({ mode: 1 }) }}></BooKTempScreen>

            case 1:
                return <BookMenuScreen actionCallback={this.doBookMenuAction.bind(this)} editCallback={this.editPage.bind(this)} popUpEditCallback={this.editPopUp.bind(this)}></BookMenuScreen>

            case 2:
                return <AddDoubleScreen addCallback={this.pageAdded.bind(this)}></AddDoubleScreen>

            case 3:

                let editPage: DPageVo;
                //  let pageEditTemp: DPageTempVo;

                if (this.ms.editCover === false) {
                    editPage = this.ms.pages[this.ms.pageEditIndex];

                }
                else {
                    //console.log("edit cover");
                    editPage = this.ms.cover;
                }
                //console.log(editPage);

                //   pageEditTemp = this.ms.getDoublePagesByID(editPage.templateID);

                //  pageEditTemp = this.ms.getDoublePagesByID(1);


                this.ms.editMode = "pages";
                return <PagesEdit pages={editPage} doneCallback={() => { this.setState({ mode: 1 }) }}></PagesEdit>

            case 4:
                return <AddImageScreen callback={() => { this.setState({ mode: 3 }) }}></AddImageScreen>

            case 5:
                return <PreviewScreen editCallback={() => { this.setState({ mode: 1 }) }}></PreviewScreen>

            case 6:

                let editPage2: DPageVo;
                //  let pageEditTemp: DPageTempVo;

                if (this.ms.editCover === false) {
                    editPage2 = this.ms.pages[this.ms.pageEditIndex];

                }
                else {
                    //console.log("edit cover");
                    editPage2 = this.ms.cover;
                }

                this.ms.editMode = "popup";
                return <PopupEditScreen doneCallback={() => { this.setState({ mode: 1 }); } } dpage={editPage2}></PopupEditScreen>

            case 7:
                return <TextStyleScreen element={this.ms.selectedElement}></TextStyleScreen>

            case 9:
                return <FileNameScreen callback={this.saveBook.bind(this)} cancelCallback={this.doCancel.bind(this)}></FileNameScreen>

        }
        return "Add Screen Here";
    }
    doCancel() {
        this.setState({ mode: -1});
    }
    saveBook() {
        let bookSaver:BookSaver=new BookSaver(this.saveDataSent.bind(this));
        bookSaver.saveBook();
       

    }
    saveDataSent(response: any) {
        //console.log(response);
        this.setState({ mode: 1 });
    }
    openFile(fileVo: FileVo) {
        let apiConnect: ApiConnect = new ApiConnect();
        this.ms.LtiFileName = fileVo.lti;
        this.ms.fileName = fileVo.fileName;

        apiConnect.getFileContent(fileVo.lti, this.gotFileData.bind(this))
    }
    gotFileData(response: any) {
        console.log(response);

        this.ms.loadBook(response);

        let tempID: number = 1;

        if (this.ms.bookInfo.h === 300) {
            tempID = 0;
        }
        this.ms.selectBookTemp(tempID);
        let tl: TemplateLoader = new TemplateLoader();
        tl.getPageList(tempID);

        this.mc.changeScreen(1);

    }
    addNewElement(index: number) {
        switch (index) {
            case 0:
                // this.setState({ mode: 4 });
                this.ms.suggested = "";
                this.mc.mediaChangeCallback = this.addNewImage.bind(this);
                this.mc.openImageBrowse();
                break;
            case 1:
                this.mc.textEditCallback = this.addNewText.bind(this);
                this.ms.editText = "Your Text Here";
                this.mc.openTextEdit();
                break;
            case 2:
                this.ms.suggested = "gutter";
                this.mc.mediaChangeCallback = this.addGutterImage.bind(this);
                this.mc.openImageBrowse();
                break;

            case 3:

                this.ms.suggested = "popbutton";
                this.mc.mediaChangeCallback = this.addPopUpLink.bind(this);
                this.mc.openImageBrowse();
                break;
        }
    }

    addNewImage(libVo: LibItemVo) {
        //console.log(libVo);

        let nextMode: number = 3;
        let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_IMAGE, ElementVo.SUB_TYPE_NONE, [], [libVo.image], 0, 0, 20);


        elementVo.onLeft = true;
        if (this.ms.editMode === "pages") {
            if (this.ms.editCover === false) {
                this.ms.pages[this.ms.pageEditIndex].left.addElement(elementVo);
            }
            else {
                this.ms.cover.left.addElement(elementVo);
            }
        }
        if (this.ms.editMode === "popup") {
            this.ms.popups[this.ms.popupEditIndex].addElement(elementVo);
            nextMode = 6;
        }

        this.ms.selectedElement = elementVo;
        this.closeMedia();


        this.setState({ mode: nextMode });
    }
    addPopUpLink(libVo: LibItemVo) {
        let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_IMAGE, ElementVo.SUB_TYPE_POPUP_LINK, [], [libVo.image], 0, 0, 20);
        elementVo.onLeft = true;
        elementVo.extras.popupID = this.ms.popups[0].id;


        this.ms.pages[this.ms.pageEditIndex].left.addElement(elementVo);
        this.ms.selectedElement = elementVo;
        this.closeMedia();
        this.setState({ mode: 3 });
    }
    addGutterImage(libVo: LibItemVo) {

        this.ms.linkedCount++;
        let linkID: string = "linkedImage" + this.ms.linkedCount.toString();
        let gutter1: ElementVo = new ElementVo(ElementVo.TYPE_BACK_IMAGE, ElementVo.SUB_TYPE_GUTTER, ["crossGutter2"], [libVo.image]);
        gutter1.onLeft = true;
        gutter1.suggested = "gutter";
        gutter1.linkID = linkID;
        gutter1.y = 25;
        gutter1.w = 300;
        gutter1.h = 300;
        gutter1.extras.backgroundPosX = 30;
        gutter1.extras.backgroundPosY = 0;
        gutter1.extras.backgroundSizeW = 540;
        gutter1.extras.backgroundSizeH = -1;
        //
        //
        //
        let gutter2: ElementVo = new ElementVo(ElementVo.TYPE_BACK_IMAGE, ElementVo.SUB_TYPE_GUTTER, ["crossGutter2"], [libVo.image]);
        gutter2.linkID = linkID;
        gutter2.onLeft = false;
        gutter2.suggested = "gutter";
        gutter2.y = 25;
        gutter2.w = 300;
        gutter2.h = 300;
        gutter2.extras.backgroundPosX = -270;
        gutter2.extras.backgroundPosY = 0;
        gutter2.extras.backgroundSizeW = 540;
        gutter2.extras.backgroundSizeH = -1;


        if (this.ms.editCover === false) {
            this.ms.pages[this.ms.pageEditIndex].left.addElement(gutter1);
            this.ms.pages[this.ms.pageEditIndex].right.addElement(gutter2);
        }
        else {
            this.ms.cover.left.addElement(gutter1);
            this.ms.cover.right.addElement(gutter2);
        }

        this.ms.selectedElement = gutter1;
        this.closeMedia();
        this.setState({ mode: 3 });
    }
    addNewText(text: string) {
        let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_TEXT, ElementVo.SUB_TYPE_NONE, [], [text], 50, 50, 20);
        elementVo.extras.orientation = 4;
        elementVo.onLeft = true;

        if (this.ms.editMode === "pages") {
            if (this.ms.editCover === false) {
                this.ms.pages[this.ms.pageEditIndex].left.addElement(elementVo);
            }
            else {
                this.ms.cover.left.addElement(elementVo);
            }
        }
        if (this.ms.editMode === "popup") {
            this.ms.popups[this.ms.popupEditIndex].addElement(elementVo);
        }

        this.ms.selectedElement = elementVo;
    }
    doBookMenuAction(action: number) {
        //console.log("action="+action);

        switch (action) {
            case 1:
                this.ms.editCover = false;
                this.setState({ mode: 2 });
                break;

            case 2:
                //  alert("preview");
                this.ms.getExport();
                this.setState({ mode: 5 });
                break;

            case 3:
                //console.log("edit cover")
                this.ms.editCover = true;
                this.setState({ mode: 3 });
                break;

            case 4:
                //  console.log("add popup");
                let td: TemplateData = new TemplateData();
                let popUp: PopUpVo = new PopUpVo(400, 300);

                let elements: ElementVo[] = td.getDefaultPopUpData(0);

                console.log(elements);

                for (let i: number = 0; i < elements.length; i++) {
                    popUp.addElement(elements[i]);
                }

                this.ms.popups.push(popUp);
                this.setState({ mode: 6 });
                break;

            case 5:
                this.saveBook();

        }
    }
    pageAdded() {
        this.setState({ mode: 3 });
    }
    editPage(index: number) {
        this.ms.editCover = false;
        this.ms.editMode = 'pages';
        this.ms.pageEditIndex = index;
        this.setState({ mode: 3 });
    }
    editPopUp(index: number) {
        this.ms.editMode = "popup";
        this.ms.popupEditIndex = index;
        this.setState({ mode: 6 });
    }
    getTextEdit() {
        if (this.state.showTextEdit) {
            return (<TextEditor closeCallback={this.closeText.bind(this)}></TextEditor>)
        }
    }
    getMediaScreen() {
        if (this.state.showImageBrowse) {
            return (<MediaScreen library={this.ms.library} closeCallback={this.closeMedia.bind(this)}></MediaScreen>)
        }
        return "";
    }
    getStyleScreen() {
        if (this.state.showStyles) {
            return (<StyleScreen styles={this.ms.styles}></StyleScreen>)
        }
    }
    openText() {
        this.setState({ showTextEdit: true });
    }
    openMedia() {
        this.setState({ showImageBrowse: true });
    }
    closeMedia() {
        this.setState({ showImageBrowse: false });

    }
    closeText() {
        this.setState({ showTextEdit: false });
    }
    getAlert()
    {
        if (this.state.msg===null)
        {
            return "";
        }
        if (this.state.msg.time>0)
        {
            setTimeout(() => {
                this.setState({msg:null});
            }, this.state.msg.time);
        }
        return (<div id="blockScreen"><Alert id="alert1" variant='success'>{this.state.msg.msg}</Alert></div>)
    }
    render() {
        return (<div id="base">
            {this.getAlert()}
            <Card>
                <Card.Body>
                    {this.getScreen()}
                    {this.getMediaScreen()}
                    {this.getTextEdit()}
                    {this.getStyleScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;