import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { TextStyleVo } from '../classes/dataObjs/TextStyleVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import TextStyleBox from '../comps/TextStyleBox';
interface MyProps { element: ElementVo | null }
interface MyState { element: ElementVo | null }
class TextStyleScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { element: this.props.element };
    }

    checkTextStyle(textStyle: TextStyleVo, check: boolean) {
        if (this.ms.selectedElement) {

            let element: ElementVo = this.ms.selectedElement;

            if (check === false) {
                element.textStyle = -1;
            }
            else {
                element.textStyle = textStyle.id;
            }

            this.ms.selectedElement = element;
            this.setState({ element: element });
        }
    }
    getTextStyleBoxes() {

        let boxes: JSX.Element[] = [];
        let textStyles: TextStyleVo[] = this.ms.styleUtil.textStyles;

        for (let i: number = 0; i < textStyles.length; i++) {
            let textStyle: TextStyleVo = textStyles[i];
            let checked: boolean = (textStyle.id === this.state.element?.textStyle) ? true : false;

            console.log(checked);
            let key: string = "textStyleBox" + i.toString();

            boxes.push(<Col key={key} sm={4}><TextStyleBox textStyle={textStyle} checked={checked} callback={this.checkTextStyle.bind(this)}></TextStyleBox></Col>)
        }

        return (<Row>{boxes}</Row>);
    }
    render() {
        return (<Card>
            <Card.Body><div className="scroll1">{this.getTextStyleBoxes()}</div></Card.Body>
            <Card.Footer className="tac"><Button onClick={() => { this.mc.changeScreen(3) }}>Done</Button></Card.Footer>
        </Card>)
    }
}
export default TextStyleScreen;