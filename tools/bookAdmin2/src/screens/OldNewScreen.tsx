import { Component } from 'react';

import { ButtonConstants } from '../classes/ButtonConstants';
import { MainController } from '../classes/MainController';
import GenMenu from '../classes/ui/GenMenu';
interface MyProps { }
interface MyState { }
class OldNewScreen extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    doAction(action: number) {
        console.log("action=" + action);
        switch (action) {
            case 0:
                this.mc.changeScreen(-2);
                break;

            case 1:
                this.mc.changeScreen(0);
                break;

        }
    }

    render() {
        return (<div>
            <GenMenu genVo={ButtonConstants.OLD_NEW_MENU} callback={this.doAction.bind(this)} ></GenMenu>
        </div>)
    }
}
export default OldNewScreen;