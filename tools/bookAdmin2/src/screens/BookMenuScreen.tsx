import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { ButtonConstants } from '../classes/ButtonConstants';
import { DPageVo } from '../classes/dataObjs/DPageVo';
import { PopUpVo } from '../classes/dataObjs/PopUpVo';
import MainStorage from '../classes/MainStorage';
import ButtonLine from '../classes/ui/ButtonLine';
import DragList from '../comps/dragListComps/DragList';
import PageEditRow from '../comps/PageEditRow';
import PopUpEditRow from '../comps/PopUpEditRow';
interface MyProps { actionCallback: Function, editCallback: Function, popUpEditCallback: Function }
interface MyState { pages: DPageVo[], showConfirm: boolean, popups: PopUpVo[], }
class BookMenuScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    private delIndex: number = -1;
    //
    //
    //
    constructor(props: MyProps) {
        super(props);
        this.state = { pages: this.ms.pages, popups: this.ms.popups, showConfirm: false };

    }
    getPageList() {
        if (this.state.pages.length === 0) {
            return "No pages yet";
        }
        let pageList: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.pages.length; i++) {
            let key: string = "pagerow" + i.toString();
            let isLast: boolean = (i === this.state.pages.length - 1);

            pageList.push(<PageEditRow key={key} isLast={isLast} pageVo={this.state.pages[i]} index={i} upCallback={this.moveUp.bind(this)} downCallback={this.moveDown.bind(this)} editCallback={this.props.editCallback} delCallback={this.delPage.bind(this)} ></PageEditRow>);
        }
        return (<div id="listHolder"><DragList itemHeight={60} onUpdate={this.updatePagePos.bind(this)}>{pageList}</DragList></div>)
    }
    updatePagePos(pos:number[])
    {
        console.log(pos);
         let pages:DPageVo[]=[];
        for (let i:number=0;i<pos.length;i++)
        {
            pages.push(this.state.pages[pos[i]]);
        }
        this.ms.pages=pages;
        this.setState({pages:pages});
    }
    getPopUpList() {
        if (this.state.popups.length === 0) {
            return "No Pop Ups Yet";
        }
        let popupList: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.popups.length; i++) {
            let key: string = "popuprow" + i.toString();
            let isLast: boolean = (i === this.state.popups.length - 1);

            popupList.push(<PopUpEditRow key={key} isLast={isLast} popup={this.state.popups[i]} index={i} editCallback={this.props.popUpEditCallback} delCallback={this.delPage.bind(this)} ></PopUpEditRow>);
        }
        return popupList;
    }
    getContent() {
        if (this.state.showConfirm === true) {
            return this.getConfirmBox();
        }
        return (<div> <Row><Col><Card><Card.Title>Pages</Card.Title><Card.Body><div className='scroll3'>{this.getPageList()}</div></Card.Body></Card></Col><Col><Card><Card.Title>Popups</Card.Title><Card.Body>{this.getPopUpList()}</Card.Body></Card></Col></Row>
            <hr />
            <div className='smButtonLine'><ButtonLine buttonArray={ButtonConstants.PAGE_ADD_MENU} actionCallback={this.props.actionCallback} useVert={false}></ButtonLine></div>
            <hr /></div>)
    }
    getConfirmBox() {
        return (<Card className="modBox">
            <Card.Title>Really Delete {this.state.pages[this.delIndex].name}?</Card.Title>
            <Card.Body>
                <Row><Col sm="6" className='tac'><Button variant='danger' onClick={this.confirmDel.bind(this)}>YES</Button></Col><Col sm={6} className='tac'><Button variant='success' onClick={this.denyDel.bind(this)}>NO</Button></Col></Row>
            </Card.Body>
        </Card>)
    }
    moveUp(index: number) {
        console.log("move up");
        console.log(index);
        this.ms.movePage(index, index - 1);
        this.setState({ pages: this.ms.pages });
    }
    moveDown(index: number) {
        console.log("move down");
        console.log(index);
        this.ms.movePage(index, index + 1);
        this.setState({ pages: this.ms.pages });
    }
    delPage(index: number) {
        console.log("delete " + index);
        this.delIndex = index;
        this.setState({ showConfirm: true })
    }
    confirmDel() {
        this.ms.removePage(this.delIndex);
        this.setState({ showConfirm: false });
    }
    denyDel() {
        this.setState({ showConfirm: false });
    }

    render() {
        return (<div>{this.getContent()}</div>)
    }
}
export default BookMenuScreen;