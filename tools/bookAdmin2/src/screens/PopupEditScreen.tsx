import React, { ChangeEvent, Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { AlignUtil } from '../classes/AlignUtil';
import { ButtonIconVo } from '../classes/dataObjs/ButtonIconVo';
import { DPageVo } from '../classes/dataObjs/DPageVo';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../classes/dataObjs/ExtrasVo';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { PopUpVo } from '../classes/dataObjs/PopUpVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import ElementBar from '../comps/ElementBar';
import PopupPreview from '../comps/PopupPreview';
import PropBox from '../comps/rightsidepanel/PropBox';
interface MyProps {dpage:DPageVo, doneCallback: Function }
interface MyState {dpage:DPageVo, popUp: PopUpVo, sideMode: number, selectedElement: ElementVo | null }
class PopupEditScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private isDown: boolean = false;
    private editElement: ElementVo | null = null;
    private addElements: ButtonIconVo[] = [];

    constructor(props: MyProps) {
        super(props);
        this.state = {dpage:this.props.dpage,popUp: this.ms.popups[this.ms.popupEditIndex], selectedElement: null, sideMode: 1 };

        this.addElements.push(new ButtonIconVo("Image", "fas fa-image", "light", 0, "sm"));
        this.addElements.push(new ButtonIconVo("Text", "fas fa-font", "light", 1, "sm"));
        this.addElements.push(new ButtonIconVo("Special", "far fa-smile", "light", 3, "sm"));
    }
    updateElementRows(pos: number[]) {
        console.log(pos);

        let allElements: ElementVo[] = this.state.popUp.elements;
        let nElements: ElementVo[] = [];
        for (let i: number = 0; i < allElements.length; i++) {
            nElements.push(allElements[pos[i]]);
        }
        this.ms.popups[this.ms.popupEditIndex].elements = nElements;

    }
    toggleVis(elementVo: ElementVo) {
        elementVo.visible = !elementVo.visible;
        this.ms.selectedElement = elementVo;
        this.setState({ selectedElement: elementVo });
    }
    toggleLock(elementVo: ElementVo) {
        elementVo.locked = !elementVo.locked;
        this.ms.selectedElement = elementVo;
        this.setState({ selectedElement: elementVo });
    }
    selectElement(elementVo: ElementVo) {

        this.ms.suggested = elementVo.suggested;
        this.ms.selectedElement = elementVo;

        this.setState({ selectedElement: elementVo, sideMode: 0 });
    }
    getSideContent() {

        let allElements: ElementVo[] = this.state.popUp.elements;

        return (<div>
            <ElementBar addElements={this.addElements} sideMode={this.state.sideMode} elements={allElements} selectedElement={this.state.selectedElement} addNewElement={this.mc.addNewElement} updateElementRows={this.updateElementRows.bind(this)} selectElement={this.selectElement.bind(this)} toggleVis={this.toggleVis.bind(this)} toggleLock={this.toggleLock.bind(this)}>
            </ElementBar>
        </div>);
    }
    onMove(e: React.PointerEvent<SVGElement>) {
        e.preventDefault();

        if (this.isDown === true && this.state.selectedElement) {


            let selected: ElementVo = this.state.selectedElement;

            /**
             * convert movement in pixels to percentages
             */
            let perMoveX: number = Math.floor((e.movementX / this.state.popUp.w) * 10000) / 100;
            let perMoveY: number = Math.floor((e.movementY / this.state.popUp.w) * 10000) / 100;



            /**
             * update position
             */

            selected.x += perMoveX;
            selected.y += perMoveY;


            this.setState({ selectedElement: selected });


        }

    }
    releaseElement(elementVo: ElementVo) {
        console.log("Released");
        this.isDown = false;
    }
    onElementDown(elementVo: ElementVo) {
        console.log("down");
        this.selectElement(elementVo);
        this.isDown = true;
    }
    editTheElement(elementVo: ElementVo | null) {
        if (elementVo === null) {
            return;
        }

        this.editElement = elementVo;
        if (this.editElement.type === ElementVo.TYPE_IMAGE || this.editElement.type === ElementVo.TYPE_BACK_IMAGE) {
            this.mc.mediaChangeCallback = this.updateElementImage.bind(this);
            this.mc.openImageBrowse();
        }
        if (this.editElement.type === ElementVo.TYPE_TEXT) {
            this.ms.editText = this.editElement.content[0];
            this.mc.textEditCallback = this.updateElementText.bind(this);
            this.mc.openTextEdit();
        }
    }
    updateElementImage(libItem: LibItemVo) {
        console.log("update element");
        if (this.editElement) {
            console.log(this.editElement);
            this.editElement.content[0] = libItem.image;
            this.mc.closeImageBrowse();

        }
    }
    updateElementText(text: string) {
        if (this.editElement) {
            this.editElement.content[0] = text;
            this.mc.closeTextEdit();
        }
    }
    updateProps(xx: number, yy: number, ww: number) {
        console.log("update props");

        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.x = xx;
            selected.y = yy;
            selected.w = ww;
            this.setState({ selectedElement: selected });
        }
    }
    updateBackgroundProps(xx: number, yy: number, ww: number, hh: number) {
        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.extras.backgroundPosX = xx;
            selected.y = yy;
            selected.extras.backgroundSizeW = ww;
            this.setState({ selectedElement: selected });
        }
    }
    getPreview() {
        let pSizeClasses: string[] = ['preview', 'preview2'];
        let sizeClass: string = pSizeClasses[this.ms.bookID];
        return (<div id={sizeClass}><PopupPreview editCallback={this.onElementDown.bind(this)} onUp={this.releaseElement.bind(this)} popupVo={this.state.popUp} onMove={this.onMove.bind(this)}></PopupPreview></div>)
    }

    getTextInput() {
        return (
            <div>
                <input
                    type="text"
                    className="form-control"
                    placeholder=""
                    onChange={this.changePageName.bind(this)}
                    value={this.state.popUp.name}
                /></div>
        )
    }
    changePageName(e: ChangeEvent<HTMLInputElement>) {
        let popup: PopUpVo = this.state.popUp;
        popup.name = e.currentTarget.value;
        this.setState({ popUp: popup });
    }
    getPropBox() {
        return (<PropBox key="propBox" elementVo={this.state.selectedElement} callback={this.updateProps.bind(this)} actionCallback={this.doPropAction.bind(this)} updateExtras={this.updateExtras.bind(this)} updateBackgroundProps={this.updateBackgroundProps.bind(this)} dpage={this.state.dpage}></PropBox>)
    }
    doPropAction(action: number, param: number = 0) {
        console.log(action, param);

        if (this.state.selectedElement) {

            let elementVo: ElementVo | null = this.state.selectedElement;

            switch (action) {
                
                case 1:
                    let flippedH: boolean = !elementVo.extras.flipH;
                    elementVo.extras.flipH = flippedH;
                    break;

                case 2:
                    let flippedV: boolean = !elementVo.extras.flipV;
                    elementVo.extras.flipV = flippedV;
                    break;

                case 3:
                    let alignUtil: AlignUtil = new AlignUtil();
                    alignUtil.alignElement(param, elementVo);

                    break;

                case 4:
                    elementVo.extras.orientation = param;
                    break;

                case 5:
                    this.editTheElement(this.state.selectedElement);
                    break;

                case 12:
                    console.log("COPY");
                    elementVo = this.copyElement(elementVo);
                    break;
                case 13:
                    console.log("DELETE");
                    this.deleteElement(elementVo);
                    elementVo = null;
                    break;
            }

            if (action > 0) {
                this.setState({ selectedElement: elementVo });
            }
        }
    }
    deleteElement(delElement: ElementVo) {
        let popup: PopUpVo = this.state.popUp;
        let elements: ElementVo[] = popup.deleteElement(delElement);
        this.ms.popups[this.ms.popupEditIndex].elements = elements;

    }
    copyElement(copyElementVo: ElementVo) {
        let elementVo: ElementVo = copyElementVo.clone();

        this.ms.popups[this.ms.popupEditIndex].addElement(elementVo);

        this.ms.selectedElement = elementVo;
        return elementVo;
    }
    updateExtras(extrasVo: ExtrasVo) {
        if (this.state.selectedElement) {
            let elementVo: ElementVo = this.state.selectedElement;
            elementVo.extras = extrasVo;
            this.setState({ selectedElement: elementVo });

        }
    }
    render() {
        return (<div onPointerUp={(e: React.PointerEvent<HTMLDivElement>) => { this.isDown = false }}>
            <Row><Col></Col><Col className="tac">{this.getTextInput()}</Col><Col></Col></Row>
            <Row><Col>{this.getSideContent()}</Col><Col>{this.getPreview()}</Col><Col>{this.getPropBox()}</Col></Row>

            <Card>
                <Card.Body>
                    <Row><Col className='tac'><Button variant='success' onClick={() => { this.props.doneCallback() }}>Done</Button></Col></Row>
                </Card.Body>
            </Card>

        </div>)
    }
}
export default PopupEditScreen;