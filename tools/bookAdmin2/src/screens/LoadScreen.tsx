import { Component } from 'react';
import { Button, Card, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import ApiConnect from '../ApiConnect';
import { ButtonConstants } from '../classes/ButtonConstants';
import { FileVo } from '../classes/dataObjs/FileVo';

import { FileListLoader } from '../classes/FileListLoader';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import ButtonLine from '../classes/ui/ButtonLine';
interface MyProps {openCallback:Function,cancelCallback:Function }
interface MyState { files: FileVo[],selected:FileVo | null }
class LoadScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private dir:number[]=[1,1,1];
    private lastSort:number=-1;

    constructor(props: MyProps) {
        super(props);
        this.state = { files: this.ms.files.slice(),selected:null };

    }

    componentDidMount() {
        
    }
    componentWillUnmount()
    {
       
    }
    loadTestFile() {
        fetch("testbook.json")
            .then(response => response.json())
            .then(data => this.loadBook(data));
    }
    loadBook(data: any) {
        this.ms.loadBook(data);
        this.mc.changeScreen(0);
    }
    loadJson(text: string) {
        console.log(text);
        let bookData: any = JSON.parse(text);
        console.log(bookData);

        this.ms.loadBook(bookData);
        this.mc.changeScreen(0);

    }
    getFileList() {
        let fileBoxes: JSX.Element[] = [];
        let files: FileVo[] = this.state.files;

        for (let i: number = 0; i < files.length; i++) {
            let file: FileVo = files[i];
            let key: string = "file" + i.toString();
            let variant:string="light";
            if (file===this.state.selected)
            {
                variant="success";
            }
            fileBoxes.push(<ListGroupItem variant={variant} key={key} onClick={()=>{this.selectFile(file)}}><Row><Col sm={3}>{file.lti}</Col><Col sm={6} className="tac">{file.fileName}</Col><Col sm={3}>{file.lastModD}</Col></Row></ListGroupItem>)
        }
        return (<ListGroup className="scroll1">{fileBoxes}</ListGroup>);
    }
    cancel() {
        this.mc.changeScreen(-1);
    }
    doSort(sortKey: number, dir: number) {

        console.log(sortKey+" "+dir);

        let files: FileVo[] = this.state.files.slice();

        if (sortKey === 0) {
            if (dir === 1) {
                files.sort(function (a: FileVo, b: FileVo) {
                    return (a.lti < b.lti) ? -1 : 1
                })
            }
            else {
                files.sort(function (a: FileVo, b: FileVo) {
                    return (a.lti > b.lti) ? -1 : 1
                })
            }
        }

        if (sortKey === 1) {
            if (dir === 1) {
                files.sort(function (a: FileVo, b: FileVo) {
                    return (a.fileName < b.fileName) ? -1 : 1
                })
            }
            else {
                files.sort(function (a: FileVo, b: FileVo) {
                    return (a.fileName > b.fileName) ? -1 : 1
                })
            }
        }
        if (sortKey === 2) {
            if (dir === 1) {
                files.sort(function (a: FileVo, b: FileVo) {
                    return (a.lastMod < b.lastMod) ? -1: 1
                })
            }
            else {
                files.sort(function (a: FileVo, b: FileVo) {
                    return (a.lastMod > b.lastMod) ? -1 : 1
                })
            }
        }
        console.log(files);

        this.setState({files:files});
    }
    doSortAction(index:number,action:number)
    {
        if (action===this.lastSort)
        {
            this.dir[index]=-this.dir[index];
        }
        console.log(this.dir);

        this.doSort(index,this.dir[index]);

        this.lastSort=action;
    }
    doDelete()
    {
        if (this.state.selected)
        {
            let apiConnect:ApiConnect=new ApiConnect();
            apiConnect.deleteFile(this.state.selected.lti,this.deleteDone.bind(this));
        }        
    }
    deleteDone()
    {
        let fll:FileListLoader=new FileListLoader(this.gotFileList.bind(this));
        fll.getFileList();
    }
    gotFileList()
    {
        this.setState({files:this.ms.files});
    }
    
    getSelectButtons()
    {
        let dis:boolean=(this.state.selected===null)?true:false;

        return (<Row><Col><Button disabled={dis} variant="success" onClick={this.openFile.bind(this)}>Open</Button></Col><Col><Button variant='danger' onClick={this.doDelete.bind(this)}>Delete</Button></Col><Col><Button variant='warning' onClick={this.doCancel.bind(this)}>Cancel</Button></Col></Row>)
    }
    selectFile(fileVo:FileVo)
    {
        this.setState({selected:fileVo});
    }
    openFile()
    {
        this.props.openCallback(this.state.selected);
    }
    doCancel()
    {
        this.props.cancelCallback();
    }    
    render() {
        return (<div><Card>
            <Card.Body>
                <ButtonLine buttonArray={ButtonConstants.FILE_SORT_BUTTONS} actionCallback={this.doSortAction.bind(this)} useVert={false}></ButtonLine>
                {this.getFileList()}
            </Card.Body>
            <Card.Footer className="tac">{this.getSelectButtons()}</Card.Footer>
        </Card>
        </div>)
    }
}
export default LoadScreen;