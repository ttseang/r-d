import React, { Component } from 'react';
import { PopUpVo } from '../classes/dataObjs/PopUpVo';
import MainStorage from '../classes/MainStorage';
import { TemplateData } from '../classes/TemplateData';
interface MyProps { popupVo:PopUpVo,editCallback:Function,onUp:Function,onMove:Function}
interface MyState { popupVo:PopUpVo }
class PopupPreview extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { popupVo: this.props.popupVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ popupVo: this.props.popupVo });
        }
    }
    getPath(image: string) {
        return "./images/bookimages/" + image;
    }

    getPopup() {

        

        let td: TemplateData = new TemplateData();
      //  return td.getPage2(pop,left,this.props.editCallback,this.props.onUp);
        return td.getPopUp(this.state.popupVo,this.props.editCallback,this.props.onUp);
       // return td.getPage(pageVo, texts, images,left);
    }


    render() {

        let sizeString="0 0 "+this.ms.bookInfo.w.toString()+" "+this.ms.bookInfo.h.toString();

        return (<svg viewBox={sizeString} onPointerMove={(e:React.PointerEvent<SVGElement>)=>{this.props.onMove(e)}}>
            {this.getPopup()}
            </svg>)
    }
}
export default PopupPreview;