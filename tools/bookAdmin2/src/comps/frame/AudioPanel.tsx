import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { DPageVo } from '../../classes/dataObjs/DPageVo';

import { MainController } from '../../classes/MainController';
interface MyProps { dpage: DPageVo | null,useBackground:boolean }
interface MyState { dpage: DPageVo | null }
class AudioPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { dpage: this.props.dpage };
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({dpage:this.props.dpage});
        }
    }
    getAudioName()
    {
        if (this.state.dpage)
        {
            let audio:string=this.state.dpage.audioVo.audioName;
            if (this.props.useBackground===true)
            {
                audio=this.state.dpage.audioVo.backgroundAudioName;
            }
            if (audio==="")
            {
                return "No Audio Set";
            }
            let nameArray:string[]=audio.split("/");
            let name:string=nameArray[nameArray.length-1];
            name=name.replace(".mp3","");
            return name;
        }
        return "";
    }
    openAudio()
    {
        if (this.props.useBackground===false)
        {
            this.mc.changeScreen(5);
        }
        else
        {
            this.mc.changeScreen(6);
        }
    }
    render() {
        return (<div className='tac'><h6 className='mt10'>{this.getAudioName()}</h6><div className='m20'><Button onClick={()=>{this.openAudio()}}>Set Audio</Button></div></div>)
    }
}
export default AudioPanel;