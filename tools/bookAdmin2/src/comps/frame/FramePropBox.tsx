import { Component } from 'react';
import { Button } from 'react-bootstrap';
import { ButtonConstants } from '../../classes/ButtonConstants';

import { ButtonVo } from '../../classes/dataObjs/ButtonVo';
import { DPageVo } from '../../classes/dataObjs/DPageVo';

import MainStorage from '../../classes/MainStorage';
import AudioPanel from './AudioPanel';
import PageBackgroundPanel from './PageBackgroundPanel';
interface MyProps { dpage: DPageVo | null, callback: Function, actionCallback: Function }
interface MyState { dpage: DPageVo | null, panelIndex: number }
class FramePropBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { dpage: this.props.dpage, panelIndex: this.ms.pageEditIndex };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ dpage: this.props.dpage });
        }
    }
    getButtons() {
        let buttonData: ButtonVo[] = ButtonConstants.FRAME_BUTTONS;

        let buttons: JSX.Element[] = [];

        for (let i: number = 0; i < buttonData.length; i++) {
            let key: string = "frameButtons" + i.toString();

            buttons.push(<Button key={key} variant={buttonData[i].variant} className="fw" onClick={() => { this.changePanel(buttonData[i].action) }}>{buttonData[i].text}</Button>)
            if (buttonData[i].action === this.state.panelIndex) {
                buttons.push(this.getPanel());
            }
        }
        return buttons;
    }
    changePanel(index: number) {
        //this.ms.pan=index;
        this.setState({ panelIndex: index });
    }
    getPanel(): JSX.Element {
        /* if (this.state.elementVo === null) {
            return (<div>Loading</div>);
        } */
        // console.log("panelIndex=" + this.state.panelIndex);

        switch (this.state.panelIndex) {
            case 0:

                return <AudioPanel key="mainAudio" dpage={this.state.dpage} useBackground={false}></AudioPanel>

            case 1:

                return <AudioPanel key="backgroundAudio" dpage={this.state.dpage} useBackground={true}></AudioPanel>

            case 2:
                return <PageBackgroundPanel dpage={this.state.dpage}></PageBackgroundPanel>
        }

        return (<div>Select a Frame</div>)
    }

    render() {
        return (<div>{this.getButtons()}</div>)
    }
}
export default FramePropBox;