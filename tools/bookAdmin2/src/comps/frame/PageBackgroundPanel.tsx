import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { DPageVo } from '../../classes/dataObjs/DPageVo';
import ColorPicker from '../../classes/ui/ColorPicker';
interface MyProps { dpage: DPageVo | null }
interface MyState { dpage: DPageVo | null}
class PageBackgroundPanel extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { dpage: this.props.dpage };
        }
        colorCallback(color:string)
        {
           // console.log(color);
            /* let extras:ExtrasVo=this.state.elementVo.extras;
            extras.fontColor=color;
            this.props.callback(extras); */
        }
    render() {
        let myColor:string="ffffff";

        if (this.state.dpage)
        {
            myColor=this.state.dpage.color;
        }
        return (<div>
            <Row>
                <Col className='tac'>
                    <ColorPicker myColor={myColor} callback={this.colorCallback.bind(this)}></ColorPicker>
                </Col>
            </Row>
        </div>)
    }
}
export default PageBackgroundPanel;