import React, { ChangeEvent, Component } from 'react';
interface MyProps {callback:Function}
interface MyState { }
class FileLoader extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    showFile(e: ChangeEvent<HTMLInputElement>) {

        e.preventDefault()

        if (e.target) {
            if (e.target.files) {
                let fileList: FileList | null = e.target.files;
                console.log(fileList);
                if (fileList.length > 0) {
                    const reader = new FileReader();
                    reader.onloadend = () => {
                       // console.log(reader.result);
                        this.props.callback(reader.result);
                    }
                    reader.readAsText(fileList[0]);

                }
            }

        }

        // reader.readAsText(e.currentTarget.value)

    }
    render() {
        return (<div>
            <input type="file" onChange={(e) => this.showFile(e)} />
        </div>)
    }
}
export default FileLoader;