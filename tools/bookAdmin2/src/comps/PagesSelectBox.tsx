import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { DPageTempVo } from '../classes/dataObjs/DPageTempVo';
import MainStorage from '../classes/MainStorage';
interface MyProps { pageData: DPageTempVo, selectCallback: Function }
interface MyState { pageData: DPageTempVo }

class PagesSelectBox extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { pageData: this.props.pageData };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ pageData: this.props.pageData });
        }
    }
    getImage() {
       
        let path: string = "./images/pageTemplates/" + this.ms.bookID.toString() + "/doubles/" + this.state.pageData.image;

        return (<img src={path} alt="template" className='dpagethumb'></img>);
    }
    render() {
        return (<Card>
            <Card.Title className="headTitle tac">{this.state.pageData.name}</Card.Title>
            <Card.Body>
                <Row className="tac"><Col>{this.getImage()}</Col></Row>
                <hr />
                <Row className="tac"><Col><Button onClick={() => { this.props.selectCallback(this.props.pageData.name,this.props.pageData) }}>Select</Button></Col></Row>
            </Card.Body>
        </Card>)
    }
}
export default PagesSelectBox;