import { Component } from 'react';
import { Button, Col, ListGroupItem, Row } from 'react-bootstrap';
import { DPageVo } from '../classes/dataObjs/DPageVo';
interface MyProps {index:number,isLast:boolean, pageVo: DPageVo,delCallback:Function,upCallback:Function,downCallback:Function,editCallback:Function }
interface MyState { }
class PageEditRow extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    getButtons()
    {
        let upDisabled:boolean=(this.props.index===0);
        let downDisabled:boolean=this.props.isLast
        let buttonArray:JSX.Element[]=[];
        buttonArray.push(<Col key="btnEdit" sm={1}><Button onClick={()=>{this.props.editCallback(this.props.index)}}><i className="fas fa-edit"></i></Button></Col>);
        buttonArray.push(<Col key="btnUp" sm={1}><Button disabled={upDisabled} onClick={()=>{this.props.upCallback(this.props.index)}}><i className="fas fa-arrow-up"></i></Button></Col>);
        buttonArray.push(<Col key="btnDown" sm={2}><Button disabled={downDisabled} onClick={()=>{this.props.downCallback(this.props.index)}}><i className="fas fa-arrow-down"></i></Button></Col>);
        buttonArray.push(<Col key="btnDel" sm={2}><Button variant='danger' onClick={()=>{this.props.delCallback(this.props.index)}}><i className="fas fa-trash-alt"></i></Button></Col>);
        return buttonArray;
    }
    
    render() {
        return (<ListGroupItem><Row><Col sm={4}>{this.props.pageVo.name}</Col>{this.getButtons()}</Row></ListGroupItem>)
    }
}
export default PageEditRow;


