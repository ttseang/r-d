import React, { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { ButtonVo } from '../classes/dataObjs/ButtonVo';
interface MyProps { tabs: ButtonVo[],callback:Function,tabClass:string }
interface MyState { selectedIndex: number }
class TabsRow extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { selectedIndex: 0 };
        }
    selectTab(index:number)
    {
        this.props.callback(this.props.tabs[index].action);
        this.setState({selectedIndex:index});
    }
    getButtons()
    {
        let buttons:JSX.Element[]=[];

        for (let i:number=0;i<this.props.tabs.length;i++)
        {
            let disabled:boolean=false;
            if (i===this.state.selectedIndex)
            {
                disabled=true;
            }
            let key="tab"+i.toString();
            buttons.push(<Button key={key} variant={this.props.tabs[i].variant} disabled={disabled} onClick={()=>{this.selectTab(i)}}>{this.props.tabs[i].text}</Button>)
        }

        if (this.props.tabClass!=="")
        {
            return (<ButtonGroup className={this.props.tabClass}>{buttons}</ButtonGroup>)
        }

        return (<ButtonGroup>{buttons}</ButtonGroup>)
    }
    render() {
        return (this.getButtons())
    }
}
export default TabsRow;