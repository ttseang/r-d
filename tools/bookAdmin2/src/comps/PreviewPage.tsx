import React, { Component } from 'react';
import { DPageVo } from '../classes/dataObjs/DPageVo';
import { PageVo } from '../classes/dataObjs/PageVo';
import MainStorage from '../classes/MainStorage';
import { TemplateData } from '../classes/TemplateData';
interface MyProps { pageVo: DPageVo,editCallback:Function,onUp:Function,onMove:Function}
interface MyState { pageVo: DPageVo }
class PreviewPage extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { pageVo: this.props.pageVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ pageVo: this.props.pageVo });
        }
    }
    getPath(image: string) {
        return "./images/bookimages/" + image;
    }

    getPage(left:boolean) {

        let pageVo:PageVo=this.state.pageVo.left;

        if (left===false)
        {
            pageVo=this.state.pageVo.right;
        }

        let td: TemplateData = new TemplateData();
        return td.getPage2(pageVo,left,this.props.editCallback,this.props.onUp);
       // return td.getPage(pageVo, texts, images,left);
    }


    render() {
        return (<svg viewBox='0 0 300 450' height={450} width={600} onPointerMove={(e:React.PointerEvent<SVGElement>)=>{this.props.onMove(e)}}>
            {this.getPage(true)}
            {this.getPage(false)}
            </svg>)
    }
}
export default PreviewPage;