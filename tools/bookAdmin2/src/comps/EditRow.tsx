import React, { Component } from 'react';
import { Button, Card, Col,Row, ListGroupItem } from 'react-bootstrap';
import { EditVo } from '../classes/dataObjs/EditVo';
interface MyProps {callback:Function, index: number, editVo: EditVo }
interface MyState { }
class EditRow extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getContent() {
        if (this.props.editVo.type === "text") {
            let text:string=this.props.editVo.content.substring(0,30);
            if (this.props.editVo.content.length>30)
            {
                text+="...";
            }
            return (<ListGroupItem><Row><Col sm={1}>{this.getHeader()}</Col><Col sm={4} className="tal">{text}</Col><Col sm={3}><Button variant="success" onClick={()=>{this.props.callback(this.props.index,this.props.editVo)}}>Change</Button></Col></Row></ListGroupItem>);
        }
        if (this.props.editVo.type === "image") {
            let image: string = "./images/bookimages/" + this.props.editVo.content;

            return (<ListGroupItem><Card><Card.Header>{this.getHeader()}</Card.Header><img src={image} alt='book' className='editThumb' /><Button variant="success" onClick={()=>{this.props.callback(this.props.index,this.props.editVo)}}>Change</Button></Card></ListGroupItem>)
        }
    }
    getHeader() {
        if (this.props.editVo.type === "text") {
            let index2: string = (this.props.index + 1).toString();
            return (index2 + ".");
        }
        return (<div>Image #{this.props.index + 1}</div>)
    }
    render() {
        return (<div>{this.getContent()}</div>)
    }
}
export default EditRow;