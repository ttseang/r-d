import React, { Component } from 'react';
interface MyProps {checked:boolean,callback:Function}
interface MyState {checked:boolean}
class CheckBox extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {checked:this.props.checked};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({checked:this.props.checked});
        }
    }
    toggle()
    {
        let checked:boolean=this.state.checked;
        checked=!checked;
        this.setState({checked:checked});
        this.props.callback(checked);
    }
    getIcon()
    {
        if (this.state.checked===true)
        {
        return <i className="far fa-check-square cb" onClick={this.toggle.bind(this)}></i>
        }
       return <i className="far fa-square-full cb" onClick={this.toggle.bind(this)}></i>
    }
    render() {
        return (<div className='tac'>{this.getIcon()}</div>)
    }
}
export default CheckBox;
//<i class="far fa-check-square"></i>