import React, { Component } from 'react';
import { KeyVo } from '../classes/dataObjs/KeyVo';
import { OptionVo } from '../classes/dataObjs/OptionVo';
import Slider from '../classes/ui/Slider';
interface MyProps { options: OptionVo[], currentVals: KeyVo[],updateCallback:Function}
interface MyState { options: OptionVo[], currentVals: KeyVo[] }
class OptionPanel extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { options: this.props.options, currentVals: this.props.currentVals }
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (this.props!==oldProps)
        {
            this.setState({options:this.props.options,currentVals:this.props.currentVals});
        }
    }
    getPanel(index:number)
    {
        console.log(this.state.options);

        if (this.state.options.length<1)
        {
            return "";
        }
        switch(this.state.options[index].type)
        {
            case OptionVo.TYPE_SLIDER:

            return <Slider optionVo={this.props.options[index]} currentVal={this.props.currentVals[index]} updateCallback={this.props.updateCallback} ></Slider>
        }
        return "control not found";
    }
    render() {
        return (<div>{this.getPanel(0)}</div>)
    }
}
export default OptionPanel;