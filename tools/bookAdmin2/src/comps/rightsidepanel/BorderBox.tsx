import React, { Component } from 'react';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../../classes/dataObjs/ExtrasVo';
import ColorPicker from '../../classes/ui/ColorPicker';
import ValBox from './ValBox';
interface MyProps {elementVo:ElementVo,callback:Function}
interface MyState {elementVo:ElementVo}
class BorderBox extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {elementVo:this.props.elementVo};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({elementVo:this.props.elementVo});
        }
    }
    colorCallback(color:string)
    {
        console.log(color);
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.borderColor=color;
        this.props.callback(extras);
    }
    sizeCallback(val:number)
    {
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.borderThick=val;
        this.props.callback(extras);
    }
    render() {
        return (<div>
            <ColorPicker myColor={this.state.elementVo.extras.borderColor} callback={this.colorCallback.bind(this)}></ColorPicker>
            <ValBox label="Border" min={0} max={30} val={this.state.elementVo.extras.borderThick} callback={this.sizeCallback.bind(this)} multiline={true}></ValBox>
        </div>)
    }
}
export default BorderBox;