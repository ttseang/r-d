import React, { Component } from 'react';
import { ButtonConstants } from '../../classes/ButtonConstants';
import ButtonLine from '../../classes/ui/ButtonLine';
interface MyProps { actionCallback: Function }
interface MyState { }
class FlipPanel extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    
    render() {
        return (<ButtonLine buttonArray={ButtonConstants.FLIP_BUTTONS} actionCallback={this.props.actionCallback} useVert={true}></ButtonLine>)
    }
}
export default FlipPanel;