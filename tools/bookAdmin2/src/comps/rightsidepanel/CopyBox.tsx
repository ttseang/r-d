import React, { Component } from 'react';
import { ButtonConstants } from '../../classes/ButtonConstants';
import ButtonLine from '../../classes/ui/ButtonLine';
interface MyProps {actionCallback:Function}
interface MyState { }
class CopyBox extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    render() {
        return (<div><ButtonLine buttonArray={ButtonConstants.COPY_BUTTONS} actionCallback={this.props.actionCallback} useVert={false}></ButtonLine></div>)
    }
}
export default CopyBox;