import { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { ButtonConstants } from '../../classes/ButtonConstants';
import { ButtonVo } from '../../classes/dataObjs/ButtonVo';
import { DPageVo } from '../../classes/dataObjs/DPageVo';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import MainStorage from '../../classes/MainStorage';
import StyleGrid from '../../classes/ui/StyleGrid';
import FramePropBox from '../frame/FramePropBox';
import AdvPanel from './AdvPanel';
import BgPosBox from './BgPosBox';
import BorderBox from './BorderBox';
import ContentPanel from './ContentPanel';
import CopyBox from './CopyBox';
import FlipPanel from './FlipPanel';
import FontPanel from './FontPanel';
import PopUpSelect from './PopUpSelect';
import PosBox from './PosBox';
import TextStylePanel from './TextStylePanel';
interface MyProps {dpage:DPageVo,elementVo: ElementVo | null, callback: Function, actionCallback: Function, updateExtras: Function, updateBackgroundProps: Function }
interface MyState {dpage:DPageVo, elementVo: ElementVo | null, panelIndex: number, }
class PropBox extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { elementVo: this.props.elementVo, panelIndex: 0,dpage:this.props.dpage };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ elementVo: this.props.elementVo });
        }
    }

    getPanel(): JSX.Element {
        if (this.state.elementVo === null) {
            return (<div>Loading</div>);
        }
        // console.log("panelIndex=" + this.state.panelIndex);

        switch (this.state.panelIndex) {
            case 0:
                return <ContentPanel key="contentPanel" elementVo={this.state.elementVo} callback={this.props.actionCallback}></ContentPanel>

            case 1:

                return this.getPropBox();

            case 2:
                return <StyleGrid key="alignGrid" gridItems={this.ms.styleUtil.aligns} callback={this.props.actionCallback}></StyleGrid>

            case 3:
                return <FlipPanel key="flipPanel" actionCallback={this.props.actionCallback}></FlipPanel>

            case 4:
                return <StyleGrid key="orgrid" gridItems={this.ms.styleUtil.orientations} callback={this.props.actionCallback}></StyleGrid>

            case 5:
                return <BorderBox key="borderBox" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></BorderBox>

            case 6:
                return <AdvPanel key="advancedPanel" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></AdvPanel>

            case 7:
                return <FontPanel key="fontPanel" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></FontPanel>
            case 8:
                return (<BgPosBox key="backgroundPosBox" elementVo={this.state.elementVo} callback={this.props.updateBackgroundProps.bind(this)}></BgPosBox>);

            case 14:
                return (<PopUpSelect element={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></PopUpSelect>)
       
                case 19:
                    return <TextStylePanel key="textStylePanel"></TextStylePanel>
                    
            }
        return (<div key="no panel">Panel Here</div>);
    }
    getButtons() {
        if (this.state.elementVo === null) {
            return (<FramePropBox dpage={this.state.dpage} callback={()=>{}} actionCallback={()=>{}}></FramePropBox>);
          //  return "Select an Element";
        }
        let buttonData: ButtonVo[] = ButtonConstants.PANEL_BUTTONS;

        if (this.state.elementVo.type === ElementVo.TYPE_TEXT) {
            buttonData = ButtonConstants.TEXT_PANEL_BUTTONS;
        }
        if (this.state.elementVo.subType===ElementVo.SUB_TYPE_GUTTER)
        {
            buttonData=ButtonConstants.BGIMG_PANEL_BUTTONS;
        }
        if (this.state.elementVo.subType===ElementVo.SUB_TYPE_POPUP_LINK)
        {
            buttonData=ButtonConstants.POPUP_PANEL_BUTTONS;
        }
        let buttons: JSX.Element[] = [];

        for (let i: number = 0; i < buttonData.length; i++) {
            let key: string = "propButton" + i.toString();

            buttons.push(<Button key={key} variant={buttonData[i].variant} className="fw" onClick={() => { this.changePanel(buttonData[i].action) }}>{buttonData[i].text}</Button>)
            if (buttonData[i].action === this.state.panelIndex) {
                buttons.push(this.getPanel());
            }
        }
        return buttons;
    }
    changePanel(panelIndex: number) {
        if (this.state.panelIndex === panelIndex) {
            panelIndex = -1;
        }
        this.setState({ panelIndex: panelIndex })
    }
    getContent() {
        if (this.state.elementVo) {
            return this.state.elementVo.content + " " + this.state.elementVo.type;
        }
        return "";
    }
    getPropBox() {
        if (this.state.elementVo) {
            return (<PosBox key="posbox" elementVo={this.state.elementVo} callback={this.props.callback}></PosBox>)
        }
        return (<div>Loading</div>);
    }
     getCopyBox()
     {
         if (this.state.elementVo)
         {
             return (<CopyBox actionCallback={this.props.actionCallback}></CopyBox>)
         }
         return "";
     }   
    render() {
        //IMAGES
        //TODO:delete, duplicate
        //TODO:content panel
        //TODO:style panel
        //TEXT
        //TODO:style panel
        return (<div>
            <Card>
                <Card.Header id="propHeader">Properties</Card.Header>
                <Card.Body>
                    {this.getCopyBox()}
                    <div className='scroll1' id="propScroll">
                        {this.getButtons()}
                    </div>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default PropBox;