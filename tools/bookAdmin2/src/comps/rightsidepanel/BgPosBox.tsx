import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import ValBox from './ValBox';
interface MyProps {elementVo:ElementVo,callback:Function}
interface MyState {elementVo:ElementVo}

class BgPosBox extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {elementVo:this.props.elementVo};
        }
        componentDidUpdate(oldProps:MyProps)
        {
            if (oldProps!==this.props)
            {
                this.setState({elementVo:this.props.elementVo});
            }
        }
        onXChange(xx:number)
        {
            let elementVo:ElementVo=this.props.elementVo;
            elementVo.extras.backgroundPosX=xx;
          //  this.setState({elementVo:elementVo});
             this.props.callback(elementVo.extras.backgroundPosX,elementVo.y,elementVo.extras.backgroundSizeW,elementVo.h);
           
        }
        onYChange(yy:number)
        {
            let elementVo:ElementVo=this.props.elementVo;
            elementVo.y=yy;
            this.setState({elementVo:elementVo});
            this.props.callback(elementVo.extras.backgroundPosX,elementVo.y,elementVo.extras.backgroundSizeW,elementVo.h);
        }
        onWChange(ww:number)
        {
            let elementVo:ElementVo=this.props.elementVo;
            elementVo.extras.backgroundSizeW=ww;
            this.setState({elementVo:elementVo});
            this.props.callback(elementVo.extras.backgroundPosX,elementVo.y,elementVo.extras.backgroundSizeW,elementVo.h);
        }
       /*  onHChange(hh:number)
        {
            let elementVo:ElementVo=this.props.elementVo;
            elementVo.h=hh;
            this.setState({elementVo:elementVo});
            this.props.callback(elementVo.extras.backgroundPosX,elementVo.extras.backgroundPosY,elementVo.w,elementVo.h);
        } */
    render() {
        return (<div>
             <Card>
                <Card.Body>
                    <ValBox val={this.state.elementVo.extras.backgroundPosX} min={-300} max={300} callback={this.onXChange.bind(this)} label="X" multiline={false}></ValBox>
                    <ValBox val={this.state.elementVo.y} min={-300} max={300} callback={this.onYChange.bind(this)} label="Y" multiline={false}></ValBox>
                    <ValBox val={this.state.elementVo.extras.backgroundSizeW} min={0} max={800} callback={this.onWChange.bind(this)} label="W" multiline={false}></ValBox>
                    {/* <ValBox val={this.state.elementVo.h} min={0} max={300} callback={this.onHChange.bind(this)} label="H" multiline={false}></ValBox> */}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BgPosBox;