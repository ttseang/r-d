import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import ListBuilder from '../comps/ListBuilder';
interface MyProps { }
interface MyState { }
class BaseScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Title Here</span>
                </Card.Header>
                <Card.Body>
                <ListBuilder words={["one","two","blue","red"]}></ListBuilder>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;