import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import TextInputButton from './TextInputButton';
import WordRow from './WordRow';
interface MyProps { words: string[] }
interface MyState { words: string[] }
class ListBuilder extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state={words:this.props.words}
        }
    addWord(text: string) {
        let words: string[] = this.state.words.slice();
        words.push(text);
        this.setState({ words: words });
    }
    getRows() {
        let wordRows: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.words.length; i++) {
            let key: string = "wordRow" + i.toString();

            wordRows.push(<WordRow key={key} word={this.state.words[i]} index={i} delCallback={this.deleteItem.bind(this)}></WordRow>)
        }
        return wordRows;
    }
    deleteItem(index:number)
    {
        let words: string[] = this.state.words.slice();
        words.splice(index,1);
        this.setState({ words: words });
    }
    render() {
        return (<div>
            <ListGroup>{this.getRows()}</ListGroup>
            <hr/>
            <TextInputButton callback={this.addWord.bind(this)} cancelCallback={() => { }} buttonText={"Add"}></TextInputButton>
        </div>)
    }
}
export default ListBuilder;