import React, { Component } from 'react';
import { Button, Col, ListGroupItem, Row } from 'react-bootstrap';
interface MyProps { word: string, index: number,delCallback:Function }
interface MyState { word: string, index: number }

class WordRow extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { word: this.props.word, index: this.props.index }
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ word: this.props.word });
        }
    }
    render() {
        return (<ListGroupItem>
            <Row>
                <Col>{this.state.index}</Col>
                <Col>{this.state.word}</Col>
                <Col><Button variant='danger' onClick={()=>{this.props.delCallback(this.state.index)}}>Delete</Button></Col>
            </Row></ListGroupItem>)
    }
}
export default WordRow;