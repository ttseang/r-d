import React, { ChangeEvent, Component } from 'react';
import { Alert, Button, Card, Col, Row } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
interface MyProps { callback: Function, cancelCallback: Function }
interface MyState { fileName: string, LtiFileName: string, msg: string }
class FileNameScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { "LtiFileName": this.ms.LtiFileName, "fileName": this.ms.fileName, "msg": "" }
    }
    onChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ LtiFileName: e.currentTarget.value });
    }
    onChangeName(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ fileName: e.currentTarget.value });
    }
    getInput() {
        return (<Row><Col sm={2}></Col><Col className="tar" sm={3}>LTI:</Col><Col sm={5}><input type='text' value={this.state.LtiFileName} onChange={this.onChange.bind(this)}></input></Col><Col sm={2}></Col></Row>)
    }
    getInputName() {
        return (<Row><Col sm={2}></Col><Col className='tar' sm={3}>File Name</Col><Col sm={5}><input type='text' value={this.state.fileName} onChange={this.onChangeName.bind(this)}></input></Col><Col sm={2}></Col></Row>)
    }
    setName() {
        if (this.state.LtiFileName==="")
        {
            this.setState({msg:"LTI can not be blank"});
            return;
        }
        
        if (this.state.fileName==="")
        {
            this.setState({msg:"File Name can not be blank"});
            return;
        }
        this.ms.LtiFileName = this.state.LtiFileName;
        this.ms.fileName = this.state.fileName;
        this.props.callback();
    }
    getMessage() {
        if (this.state.msg === "") {
            return "";
        }
        return <Alert variant='warning' className='tac'>{this.state.msg}</Alert>
    }
    render() {
        return (<Card>
            <Card.Body>
                <Row><Col>{this.getMessage()}</Col></Row>
                {this.getInput()}
                <br/>
                {this.getInputName()}
                <hr/>
                <Row><Col className='tac'><Button onClick={this.setName.bind(this)}>Done</Button></Col><Col className='tac'><Button variant='danger' onClick={() => { this.props.cancelCallback() }}>Cancel</Button></Col></Row>
            </Card.Body>
        </Card>)
    }
}
export default FileNameScreen;