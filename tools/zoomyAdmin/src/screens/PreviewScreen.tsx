import React, { Component } from 'react';
import { Button, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import { ScreenSizeVo } from '../classes/dataObjs/ScreenSizeVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
interface MyProps { editCallback: Function }
interface MyState {sizeIndex:number,w:number,h:number}
class PreviewScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {sizeIndex:0,w:this.ms.screenSizes[0].w,h:this.ms.screenSizes[0].h};
    }
    getSizeList()
    {
        let sizeBoxes:JSX.Element[]=[];
        for (let i:number=0;i<this.ms.screenSizes.length;i++)
        {
            let size:ScreenSizeVo=this.ms.screenSizes[i];
            let sizeString:string=size.w.toString()+"x"+size.h.toString();
            let key:string="sizes"+i.toString();
            let variant:string="light";
            if (this.state.sizeIndex===i)
            {
                variant="success";
            }
            sizeBoxes.push(<ListGroupItem key={key} variant={variant} onClick={()=>{this.selectSize(i)}}><Row><Col>{sizeString}</Col><Col sm={3}>{this.ms.screenSizes[i].description}</Col><Col sm={3}>{this.ms.screenSizes[i].per.toString()}%</Col></Row></ListGroupItem>)
        }
        return (<ListGroup id='scroll2'>{sizeBoxes}</ListGroup>)
    }
    selectSize(index:number)
    {
        let size:ScreenSizeVo=this.ms.screenSizes[index];
        this.setState({sizeIndex:index,w:size.w,h:size.h});
    }
    render() {

        let loc: string = window.location.href;
        let ppath: string = loc + "preview/";
        //console.log(ppath);

        return (<div>
            <div id="bookData" style={{ display: "none" }}>{this.ms.getExport()} </div>
            <div className='tac'>
                <iframe src={ppath} title='book preview' width={this.state.w} height={this.state.h} frameBorder={0} scrolling="no" >
                </iframe>
                <hr />
                <Row><Col>Size</Col><Col sm={3}>Example Phones</Col><Col sm={3}>% of market</Col></Row>
                {this.getSizeList()}
                <hr/>
                <Row><Col> <Button variant='success' onClick={() => { this.props.editCallback() }}>EDIT</Button></Col><Col> <Button variant='success' onClick={() => { this.mc.changeScreen(10) }}>Full Screen</Button></Col></Row>

            </div>
        </div>)
    }
}
export default PreviewScreen;