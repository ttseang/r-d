import React, { Component } from 'react';
import { Alert, Card } from 'react-bootstrap';
import ApiConnect from '../classes/ApiConnect';
import { MenuConstants } from '../classes/constants/MenuConstants';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { ServerSaveVo } from '../classes/dataObjs/exports/saver/ServerSaveVo';
import { FileVo } from '../classes/dataObjs/FileVo';
import { FormElementVo } from '../classes/dataObjs/FormElementVo';
import { GenVo } from '../classes/dataObjs/GenVo';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { MsgVo } from '../classes/dataObjs/MsgVo';
import { StepVo } from '../classes/dataObjs/StepVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import { StartData } from '../classes/StartData';
import { MediaSoundVo } from '../filebrowser/dataObjs/MediaSoundVo';
import SoundMediaBrowser from '../filebrowser/screens/SoundMediaBrowser';
//import AudioScreen from './AudioScreen';
import FileNameScreen from './FileNameScreen';
import FormScreen from './FormScreen';
import FullPreviewScreen from './FullPreviewScreen';

import GenSelectScreen from './GenSelectScreen';
import LoadScreen from './LoadScreen';
import MediaScreen from './MediaScreen';
import PageEditScreen from './PageEditScreen';
import PreviewScreen from './PreviewScreen';
import StyleScreen from './StyleScreen';
import TextEditor from './TextEditor';
import TextStyleScreen from './TextStyleScreen';
interface MyProps { }
interface MyState { mode: number,msg:MsgVo | null,showTextEdit: boolean, showImageBrowse: boolean, showStyles: boolean }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: -1, showTextEdit: false, showImageBrowse: false, showStyles: false,msg:null };

        this.mc.openImageBrowse = this.openMedia.bind(this);
        this.mc.closeImageBrowse = this.closeMedia.bind(this);
        this.mc.openTextEdit = this.openText.bind(this);
        this.mc.closeTextEdit = this.closeText.bind(this);
        this.mc.changeScreen = this.changeScreen.bind(this);
        this.mc.addNewElement = this.addNewElement.bind(this);
        this.mc.openForm = this.showForm.bind(this);
        this.mc.exportFile = this.exportFile.bind(this);
        this.mc.saveZoom = this.saveZoom.bind(this);
        this.mc.openZoom = this.openZoom.bind(this);

        this.mc.audioChangeCallback = this.chooseFrameAudio.bind(this);
        this.mc.backgroundAudioChange = this.chooseBackgroundAudio.bind(this);

        this.mc.setAlert=this.setAlert.bind(this);
        document.addEventListener("keydown",(e:KeyboardEvent)=>{
            if (e.key==="s" && e.ctrlKey===true)
            {
                e.preventDefault();
                this.mc.saveZoom();
            }
            if (e.key==="o" && e.ctrlKey===true)
            {
                e.preventDefault();
                this.mc.openZoom();
            }
            if (e.key==="d" && e.ctrlKey===true)
            {
                e.preventDefault();
              //  this.mc.toggleZoom();
            }
        })
        document.addEventListener("keyup", (e: KeyboardEvent) => {
            e.preventDefault();

            if (this.ms.keyLock === false) {
                this.mc.doKeyboardShortCut(e.key, e.ctrlKey, e.altKey);
                this.ms.keyLock = true;

                setTimeout(() => {
                    this.ms.keyLock = false;
                }, 250);
            }
        })
    }
    componentDidMount() {
        let sd: StartData = new StartData(this.gotStartData.bind(this));
        sd.start();
    }
    gotStartData() {
        this.setState({ mode: 0 });
    }
    exportFile() {
        let filename = "text.json";
        let text: string = this.ms.getExport();

        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
    getScreen() {
        if (this.state.showImageBrowse === true || this.state.showTextEdit === true || this.state.showStyles === true) {
            return "";
        }
        switch (this.state.mode) {

            case -2:
                return <LoadScreen openCallback={this.openFile.bind(this)} cancelCallback={this.doCancel.bind(this)}></LoadScreen>

            case -1:
                return "Loading";

            case 0:
                return <PageEditScreen doneCallback={() => { }} steps={this.ms.steps}></PageEditScreen>

            case 1:
                this.ms.menu = MenuConstants.cardSelectMenu;
                return <GenSelectScreen callback={this.addCard.bind(this)}></GenSelectScreen>
            case 2:
                return <FormScreen formElements={this.ms.formData}></FormScreen>

            case 3:
                return <PreviewScreen editCallback={() => { this.setState({ mode: 0 }) }}></PreviewScreen>

                case 5:
                this.ms.backgroundAudioSwitch = false;
                return <SoundMediaBrowser tag={'TT.RD.ZM'} path={'https://ttv5.s3.amazonaws.com/'} cancelCallback={()=>{this.setState({mode:0})}} callback={this.setAudioFile.bind(this)}></SoundMediaBrowser>
            
               // return <AudioScreen library={this.ms.libUtil.audioLib} closeCallback={this.closeAudio.bind(this)}></AudioScreen>

            case 6:
                this.ms.backgroundAudioSwitch = true;
                return <SoundMediaBrowser tag={'TT.RD.BM'} path={'https://ttv5.s3.amazonaws.com/'} cancelCallback={()=>{this.setState({mode:0})}} callback={this.setBackgroundMusic.bind(this)}></SoundMediaBrowser>
            //    return <AudioScreen library={this.ms.libUtil.backgroundAudio} closeCallback={this.closeAudio.bind(this)}></AudioScreen>
                

            case 7:
                return <TextStyleScreen element={this.ms.selectedElement}></TextStyleScreen>

            case 9:
                return <FileNameScreen callback={this.saveZoom.bind(this)} cancelCallback={this.doCancel.bind(this)}></FileNameScreen>

            case 10:
                return <FullPreviewScreen callback={() => { this.setState({ mode:0 }) }}></FullPreviewScreen>
        }
        return "Screen Not Found";
    }
    setBackgroundMusic(sound:MediaSoundVo)
    {
        //console.log(sound.mp4.path);
        this.ms.selectedStep.backgroundAudio = sound.mp4.path;
        this.ms.selectedStep.backgroundAudioName = sound.title;
        this.setState({mode:0});
    }
    setAudioFile(sound:MediaSoundVo)
    {
        this.ms.selectedStep.audio = sound.mp4.path;
        this.ms.selectedStep.audioName = sound.title;
        this.setState({mode:0});
    }
    setAlert(msg:MsgVo)
    {
        this.setState({msg:msg});
    }
    closeAudio() {
        this.setState({ mode: 0 })
    }
    doCancel() {
        this.setState({ mode: 0 });
    }

    chooseFrameAudio(libItem: LibItemVo) {
        if (this.ms.selectedStep) {
            this.ms.selectedStep.audio = libItem.image;
            this.ms.selectedStep.audioName = libItem.title;
        }
        this.setState({ mode: 0 });
    }
    chooseBackgroundAudio(libItem: LibItemVo) {
        if (this.ms.selectedStep) {

            let nameArray: string[] = libItem.image.split("/");
            let baseFile: string = nameArray[nameArray.length - 1];

            this.ms.selectedStep.backgroundAudio = baseFile;
            this.ms.selectedStep.backgroundAudioName = libItem.title;
        }
        this.setState({ mode: 0 });
    }

    changeScreen(screen: number) {
        this.setState({ mode: screen });
    }
    showForm(formData: FormElementVo[]) {
        this.ms.formData = formData;
        this.setState({ mode: 2 });
    }
    addCard(genVo: GenVo) {
        this.addNewCard(genVo.action);
        this.setState({ mode: 0 });

    }
    addNewElement(index: number) {
        switch (index) {
            case 0:
                // this.setState({ mode: 4 });
                this.ms.suggested = "";
                this.mc.mediaChangeCallback = this.addNewImage.bind(this);
                this.mc.openImageBrowse();
                break;
            case 1:
                this.mc.textEditCallback = this.addNewText.bind(this);
                this.ms.editText = "Your Text Here";
                this.mc.openTextEdit();
                break;
            case 2:
                /*  this.ms.suggested = "gutter";
                 this.mc.mediaChangeCallback = this.addGutterImage.bind(this);
                 this.mc.openImageBrowse(); */
                break;

            case 3:

                this.ms.suggested = "popbutton";
                this.mc.mediaChangeCallback = this.addPopUpLink.bind(this);
                this.mc.openImageBrowse();
                break;

            case 5:
                // this.addNewCard(["Title","Content here"]);
                this.setState({ mode: 1 });
                break;

            case 6:
                let lineElement: ElementVo = new ElementVo(ElementVo.TYPE_LINE, ElementVo.SUB_TYPE_NONE, [], [], 0, 0, 50);
                lineElement.h = 50;
                lineElement.extras.borderColor = "green";
                lineElement.extras.borderThick = 2;

                this.ms.eidIndex++;
                lineElement.eid = "line"+this.ms.eidIndex.toString();

                this.ms.steps[this.ms.currentStep].addElement(lineElement);
                this.ms.selectedElement = lineElement;

                this.setState({ mode: 0 });
        }
        this.ms.addHistory();
    }
    addNewCard(type: number = 0) {
        let elementVo: ElementVo

        switch (type) {
            case 0:
                elementVo = new ElementVo(ElementVo.TYPE_CARD, ElementVo.SUB_TYPE_GREEN_CARD, [], ["Title", "Content"], 0, 0, 30);
                elementVo.h=40;
                this.ms.eidIndex++;
                elementVo.eid = "card"+this.ms.eidIndex.toString();
                break;

            case 1:
                elementVo = new ElementVo(ElementVo.TYPE_CARD, ElementVo.SUB_TYPE_WHITE_CARD, [], ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis, nibh maximus bibendum aliquam, ligula enim mollis urna, sed pulvinar nibh mauris vel lorem. "], 0, 0, 44);
                elementVo.h=40;
                elementVo.extras.fontSize=11;
                this.ms.eidIndex++;
                elementVo.eid = "card"+this.ms.eidIndex.toString();
                break;

            default:
                elementVo = new ElementVo(ElementVo.TYPE_CARD, ElementVo.SUB_TYPE_GREEN_CARD, [], ["Title", "Content"], 0, 0, 30);
                elementVo.h=40;
                this.ms.eidIndex++;
                elementVo.eid = "card"+this.ms.eidIndex.toString();
        }


        this.ms.steps[this.ms.currentStep].addElement(elementVo);


        this.ms.selectedElement = elementVo;
        this.ms.addHistory();
        this.closeMedia();
    }
    addNewImage(libVo: LibItemVo) {
        ////console.log(libVo);

        //let nextMode: number = 3;
        let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_IMAGE, ElementVo.SUB_TYPE_NONE, [], [libVo.image], 0, 0, 20);
        this.ms.eidIndex++;
        elementVo.eid = "image"+this.ms.eidIndex.toString();

        //  elementVo.onLeft = true;
        this.ms.steps[this.ms.currentStep].addElement(elementVo);

        /*  if (this.ms.editMode === "popup") {
             this.ms.popups[this.ms.popupEditIndex].addElement(elementVo);
             nextMode = 6;
         } */

        this.ms.selectedElement = elementVo;
        this.closeMedia();


        //  this.setState({ mode: nextMode });
    }
    addPopUpLink(libVo: LibItemVo) {
        let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_IMAGE, ElementVo.SUB_TYPE_POPUP_LINK, [], [libVo.image], 0, 0, 20);
        //elementVo.onLeft = true;
        elementVo.extras.popupID = this.ms.popups[0].id;

        this.ms.eidIndex++;
        elementVo.eid = this.ms.eid;

        this.ms.steps[this.ms.currentStep].addElement(elementVo);
        this.ms.selectedElement = elementVo;
        this.closeMedia();
        this.setState({ mode: 3 });
    }

    addNewText(text: string) {
        let elementVo: ElementVo = new ElementVo(ElementVo.TYPE_TEXT, ElementVo.SUB_TYPE_NONE, [], [text], 50, 50, 20);
        elementVo.extras.orientation = 4;
        this.ms.eidIndex++;
        elementVo.eid = "text"+this.ms.eidIndex.toString();
        //elementVo.onLeft = true;
        this.ms.steps[this.ms.currentStep].addElement(elementVo);
        /* 
        if (this.ms.editMode === "popup") {
            this.ms.popups[this.ms.popupEditIndex].addElement(elementVo);
        } */

        this.ms.selectedElement = elementVo;
    }
    getTextEdit() {
        if (this.state.showTextEdit) {
            return (<TextEditor closeCallback={this.closeText.bind(this)}></TextEditor>)
        }
    }
    getMediaScreen() {
        if (this.state.showImageBrowse) {
            return (<MediaScreen library={this.ms.library} closeCallback={this.closeMedia.bind(this)}></MediaScreen>)
        }
        return "";
    }
    getStyleScreen() {
        if (this.state.showStyles) {
            return (<StyleScreen styles={this.ms.styles}></StyleScreen>)
        }
    }
    openText() {
        this.setState({ showTextEdit: true });
    }
    openMedia() {
        this.setState({ showImageBrowse: true });
    }
    closeMedia() {
        this.setState({ showImageBrowse: false });

    }
    closeText() {
        this.setState({ showTextEdit: false });
    }
    openZoom() {
        this.setState({ mode: -2 });
    }
    saveZoom() {
        if (this.ms.LtiFileName === "") {
            this.setState({ mode: 9 });
            return;
        }
        this.mc.setAlert(new MsgVo("Saving...","warning",-1));

        let data: any = this.ms.saveZoom2();

        let serverData: ServerSaveVo = new ServerSaveVo(this.ms.LtiFileName, this.ms.fileName, this.ms.appType, data);

        //console.log(serverData);

        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.Send(JSON.stringify(serverData), this.dataSent.bind(this));
    }
    dataSent(response: any) {
        //console.log(response);
        this.mc.setAlert(new MsgVo("Zoom Presentation Saved","success",2000));
        this.setState({ mode: 0 });
    }
    openFile(fileVo: FileVo) {
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getFileContent(fileVo.lti, this.gotFileData.bind(this));
        this.ms.LtiFileName=fileVo.lti;
        this.ms.fileName=fileVo.fileName;
    }
    gotFileData(response: any) {
        //console.log(response);
        StepVo.StepCount=0;
        this.ms.loadZoom(response);
        this.mc.changeScreen(0);

    }
    getAlert()
    {
        if (this.state.msg===null)
        {
            return "";
        }
        if (this.state.msg.time>0)
        {
            setTimeout(() => {
                this.setState({msg:null});
            }, this.state.msg.time);
        }
        return (<div id="blockScreen"><Alert id="alert1" variant='success'>{this.state.msg.msg}</Alert></div>)
    }
    render() {
        if (this.state.mode === 10) {
            return this.getScreen();
        }
        return (<div id="base">
            {this.getAlert()}
            <Card>
                <Card.Body>
                    {this.getScreen()}
                    {this.getMediaScreen()}
                    {this.getTextEdit()}
                    {this.getStyleScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;