import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { MediaSoundVo } from '../dataObjs/MediaSoundVo';
interface MyProps { sound: MediaSoundVo, path: string,callback:Function }
interface MyState { sound: MediaSoundVo }
class SoundCard extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props)
            this.state = { sound: this.props.sound }
        }
    getPlayer() {
        let path: string = this.props.path + this.state.sound.mp4.path;
        console.log(path);
        return (<audio controls><source src={path} type="audio/mpeg" /></audio>)
    }
    render() {
        return (<Card>
            <Card.Header id="soundHeader">{this.state.sound.title}</Card.Header>
            <Card.Body>
            <div style={{textAlign:"center"}}>  {this.getPlayer()}</div>
             <div style={{textAlign:"center"}}><Button onClick={()=>{this.props.callback(this.state.sound)}}>Select</Button></div>
            </Card.Body>
        </Card>)
    }
}
export default SoundCard;