import React, { Component, ReactNode } from 'react';
import DragListItem from './DragListItem';
import { ListPosVo } from './ListPosVo';
interface MyProps { children: ReactNode[], itemHeight: number, onUpdate: Function }
interface MyState { yPos: ListPosVo[], children: ReactNode[] }
class DragList extends Component<MyProps, MyState>
{
    private drags: JSX.Element[] = [];
    private currentDrag: number = -1;

    constructor(props: MyProps) {
        super(props)
        this.state = { yPos: [], children: this.props.children };
    }
    componentDidMount() {
        let ys: ListPosVo[] = [];

        for (let i: number = 0; i < this.props.children.length; i++) {
            ys.push(new ListPosVo(i * this.props.itemHeight, i));

        }
        this.setState({ yPos: ys });
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            let ys: ListPosVo[] = [];
            for (let i: number = 0; i < this.props.children.length; i++) {
                ys.push(new ListPosVo(i * this.props.itemHeight, i));

            }

            this.setState({ children: this.props.children, yPos: ys });
        }
    }
    getDragObjs() {
        let drags: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.children.length; i++) {
            let key: string = "dli" + i.toString();
            let y: number = 0;
            if (this.state.yPos[i]) {
                y = this.state.yPos[i].y;
            }
            let isDragging: boolean = (this.currentDrag === i) ? true : false;
            drags.push(<DragListItem key={key} isDragging={isDragging} index={i} y={y} onDown={this.onDown.bind(this)} onUp={this.onUp.bind(this)}>{this.state.children[i]}</DragListItem>)
        }
        this.drags = drags;
        return drags;
    }

    onDown(index: number) {
        this.currentDrag = index;
    }
    onUp(index: number) {

        this.currentDrag = -1;

        let ys: ListPosVo[] = this.state.yPos.slice();

        ys.sort((a: ListPosVo, b: ListPosVo) => {
            return (a.y - b.y)
        })

        //console.log(ys);

        let pos: number[] = [];
        for (let i: number = 0; i < ys.length; i++) {
            pos.push(ys[i].index);
        }
        this.props.onUpdate(pos);
    }
    onDragging()
    {
        
      //  this.props.onUpdate(pos);
    }
    onMove(e: React.PointerEvent<HTMLElement>) {
        e.preventDefault();

        if (this.currentDrag !== -1) {
            let ys: ListPosVo[] = this.state.yPos.slice();
            ys[this.currentDrag].y += e.movementY;
         //   //console.log(e.clientX);
            
            this.setState({ yPos: ys });
           // this.onDragging();
        }
    }
    render() {
        return (<div id="dragList" onPointerMove={this.onMove.bind(this)} onPointerUp={()=>{this.onUp.bind(this)}}>{this.getDragObjs()}</div>)
    }
}
export default DragList;