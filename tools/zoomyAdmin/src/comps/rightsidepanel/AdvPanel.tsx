import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import ValBox from './ValBox';
interface MyProps { elementVo: ElementVo, callback: Function }
interface MyState { elementVo: ElementVo }
class AdvPanel extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { elementVo: this.props.elementVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ elementVo: this.props.elementVo });
        }
    }
    onAlphaChange(alpha: number) {
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.alpha = alpha;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }
    onRotationChange(rot: number) {
        // //console.log(rot);
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.rotation = rot;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }
    onSkewYChange(skew: number) {
        // //console.log(rot);
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.skewY = skew;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }
    onSkewXChange(skew: number) {
        // //console.log(rot);
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.skewX = skew;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }

    render() {
        return (<div>
            <Card>
                <Card.Body>
                    <ValBox val={this.state.elementVo.extras.alpha} multiline={true} callback={this.onAlphaChange.bind(this)} label="Alpha"></ValBox>
                    <ValBox val={this.state.elementVo.extras.rotation} multiline={true} callback={this.onRotationChange.bind(this)} label="Angle"></ValBox>
                    <ValBox val={this.state.elementVo.extras.skewX} min={-100} max={100} multiline={true} callback={this.onSkewXChange.bind(this)} label="Skew X"></ValBox>
                    <ValBox val={this.state.elementVo.extras.skewY} min={-100} max={100} multiline={true} callback={this.onSkewYChange.bind(this)} label="Skew Y"></ValBox>

                </Card.Body>
            </Card>
        </div>)
    }
}
export default AdvPanel;