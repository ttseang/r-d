import React, { ChangeEvent, Component } from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../../classes/dataObjs/ExtrasVo';
import { FontVo } from '../../classes/dataObjs/FontVo';

import MainStorage from '../../classes/MainStorage';
import ColorPicker from '../../classes/ui/ColorPicker';
import ValBox from './ValBox';
interface MyProps {elementVo:ElementVo,callback:Function}
interface MyState {elementVo:ElementVo}
class FontPanel extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {elementVo:this.props.elementVo};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (this.props!==oldProps)
        {
            this.setState({elementVo:this.props.elementVo});
        }
    }
    fontChanged(e:ChangeEvent<HTMLSelectElement>)
    {
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.fontName=e.currentTarget.value;
        this.props.callback(extras);
    }
    getFontDropDown()
    {
        let fonts:FontVo[]=this.ms.styleUtil.fonts;
        let ddArray:JSX.Element[]=[];
        for (let i:number=0;i<fonts.length;i++)
        {
            let key:string="fs"+i.toString();
          //  let selected:boolean=(fonts[i].fontName===this.state.elementVo.extras.fontName)?true:false;

            ddArray.push(<option key={key}>{fonts[i].label}</option>)
        }
        return (<select onChange={this.fontChanged.bind(this)} value={this.state.elementVo.extras.fontName}>{ddArray}</select>)
    }
    colorCallback(color:string)
    {
       // //console.log(color);
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.fontColor=color;
        this.props.callback(extras);
    }
    sizeCallback(val:number)
    {
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.fontSize=val;
        this.props.callback(extras);
    }
    render() {
        return (<Card>
            <Row><Col className='tac'>{this.getFontDropDown()}</Col></Row>
            <Row>
                <Col className='tac'><ColorPicker myColor={this.state.elementVo.extras.fontColor} callback={this.colorCallback.bind(this)}></ColorPicker>
                </Col>
            </Row>
            <Row><Col><ValBox label="Font Size" min={0} max={100} val={this.state.elementVo.extras.fontSize} callback={this.sizeCallback.bind(this)} multiline={true}></ValBox></Col></Row>
            
        </Card>)
    }
}
export default FontPanel;