import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
interface MyProps {label:string,min:number,max:number, val: number,callback:Function,multiline:boolean }
interface MyState { val: number }
class ValBox extends Component<MyProps, MyState>
{
    public static defaultProps={min:0,max:100};
    
        constructor(props: MyProps) {
            super(props);
            this.state = { val: this.props.val };
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({val:this.props.val});
        }
    }
    onChange(e:ChangeEvent<HTMLInputElement>)
    {
        let val:number=parseFloat(e.currentTarget.value);
        if (isNaN(val))
        {
            val=0;
        }
       // this.setState({val:val});
        this.props.callback(val);
    }
    getSlider() {
        return (<input type='range' className='fw' min={this.props.min} max={this.props.max} value={this.state.val} onChange={this.onChange.bind(this)}></input>)
    }
    getInputBox() {
        return (<input type='text' className='fw' value={this.state.val} onChange={this.onChange.bind(this)}></input>)
    }
    render() {
        if (this.props.multiline===true)
        {
            return (<div>
                <Row><Col sm={12} className="tac">{this.props.label}</Col></Row>
                <Row><Col sm={12} className="fw">{this.getInputBox()}</Col></Row>
                <Row><Col>{this.getSlider()}</Col></Row>
            </div>)    
        }
        return (<div>
            <Row><Col sm={2}>{this.props.label}</Col><Col sm={8}>{this.getInputBox()}</Col></Row>
            <Row><Col>{this.getSlider()}</Col></Row>
        </div>)
    }
}
export default ValBox;