import { Component } from 'react';
import { Card, Button } from 'react-bootstrap';
import { ButtonIconVo } from '../classes/dataObjs/ButtonIconVo';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import AddBox from './AddBox';
import DragList from './dragListComps/DragList';
import ElementRow from './ElementRow';
interface MyProps { sideMode: number, elements: ElementVo[], selectedElement: ElementVo | null,addElements:ButtonIconVo[],zoomLocked:boolean,toggleZoomLock:Function, addNewElement: Function, updateElementRows: Function, selectElement: Function, toggleVis: Function, toggleLock: Function }
interface MyState { sideMode: number, elements: ElementVo[], selectedElement: ElementVo | null,addElements:ButtonIconVo[],zoomLocked:boolean }
class ElementBar extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { sideMode: 0, elements: [], selectedElement: null,addElements:this.props.addElements,zoomLocked:this.props.zoomLocked};
        }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ sideMode: this.props.sideMode,zoomLocked:this.props.zoomLocked, elements: this.props.elements, selectedElement: this.props.selectedElement,addElements:this.props.addElements });
        }
    }
    getSideContent() {
        switch (this.state.sideMode) {
            case 0:
                return this.getElementRows();

            case 1:
                return this.getAddBoxes();
        }
    }
    getElementRows() {

        // let allElements: ElementVo[] = (this.state.showLeft === true) ? this.state.pages.left.elements : this.state.pages.right.elements;
        let allElements: ElementVo[] = this.state.elements;
        let editRows: JSX.Element[] = [];

        let usedLinks: string[] = [];

        for (let j: number = 0; j < allElements.length; j++) {
            let key: string = "elvo" + j.toString();
            let selected: boolean = false;
            if (this.state.selectedElement) {
                if (this.state.selectedElement.id === allElements[j].id) {
                    selected = true;
                }
            }
            let skip: boolean = false;

            if (allElements[j].linkID !== "" && usedLinks.includes(allElements[j].linkID)) {
                skip = true;
            }

            if (skip === false) {
                if (selected === true) {
                    editRows.push(<div key={key}><ElementRow callback={this.props.selectElement} selected={true} index={j} elementVo={allElements[j]} toggleVis={this.props.toggleVis} toggleLock={this.props.toggleLock}></ElementRow></div>)
                }
                else {
                    editRows.push(<div key={key}><ElementRow callback={this.props.selectElement} selected={false} index={j} elementVo={allElements[j]} toggleVis={this.props.toggleVis} toggleLock={this.props.toggleLock}></ElementRow></div>)
                }
                if (allElements[j].linkID !== "") {
                    usedLinks.push(allElements[j].linkID);
                }
            }

        }
        // ////console.log(this.selectedRef);



        return (<div>
            <Card className='rsidePanel'>{this.getAddButton()}
                <div className='sideScroll'>
                    <div id="listHolder"><DragList itemHeight={60} onUpdate={this.props.updateElementRows}>{editRows}</DragList></div>
                </div>
            </Card>
        </div>);
    }
    getAddButton() {
        if (this.state.sideMode === 1) {
            return (<Button className="fw" variant='primary' onClick={() => { this.setState({ sideMode: 0 }) }}>Elements</Button>)
        }
        return (<Button className="fw" variant='success' onClick={() => { this.setState({ sideMode: 1 }) }}>ADD New</Button>)
    }
    getAddBoxes() {
        //     let addElements: string[] = ["Image", "Text","Gutter Image","Special"];
        

        let addRows: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.addElements.length; i++) {
            let key: string = "addBox" + i.toString();
            addRows.push(<AddBox key={key} buttonData={this.state.addElements[i]} callback={() => { this.props.addNewElement(this.state.addElements[i].action) }}></AddBox>)
        }
        return (<div>{this.getAddButton()}{addRows}</div>)
    }
    getLock()
    {
        if (this.state.zoomLocked===false)
        {
            return (<Button variant='light' onClick={()=>{this.props.toggleZoomLock()}}><i className="fas fa-lock-open"></i></Button>)     
        }
        return (<Button variant='light' onClick={()=>{this.props.toggleZoomLock()}}><i className="fas fa-lock"></i></Button>) 
    }
    render() {
        return (<div>{this.getSideContent()}</div>)
    }
}
export default ElementBar;