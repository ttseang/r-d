import React, { Component } from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import { ButtonIconVo } from "../dataObjs/ButtonIconVo";


interface MyProps {buttonArray:ButtonIconVo[],actionCallback:Function,useVert:boolean,useText:boolean}
interface MyState {
}
class ButtonIconLine extends Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getButtons() {
        let blist: JSX.Element[] = [];
        for (let i: number = 0; i < this.props.buttonArray.length; i++) {
          let text: string = this.props.buttonArray[i].text;
          let variant: string = this.props.buttonArray[i].variant;
          let action: number = this.props.buttonArray[i].action;
          let size:any=this.props.buttonArray[i].size;
          let icon:string=this.props.buttonArray[i].icon;
          //
          //
          let key: string = "bbutton" + i.toString();
          if (this.props.useText===true)
          {
          blist.push(
            <Button              
              key={key}
              variant={variant}
              size={size}
              onClick={() => {
                this.props.actionCallback(action,i);
              }}
            ><i className={icon}></i>
              {text}
            </Button>
          );
            }
            else
            {
                blist.push(
                    <Button              
                      key={key}
                      variant={variant}
                      size={size}
                      onClick={() => {
                        this.props.actionCallback(action,i);
                      }}
                    ><i className={icon}></i>
                    </Button>
                  );
            }
        }
        if (this.props.useVert===true)
        {
            return <ButtonGroup size="sm" className="fw" vertical>{blist}</ButtonGroup>;    
        }
        return <ButtonGroup className="fw">{blist}</ButtonGroup>;
      }
    render() {
        return (
           <div>{this.getButtons()}</div> 
        );
    }
}
export default ButtonIconLine;