import { ScreenSizeVo } from "./dataObjs/ScreenSizeVo";
import MainStorage from "./MainStorage";

export class ScreenSizeLoader
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    public getScreenSizes()
    {
        fetch("./jsonFiles/screenSizes.json")
        .then(response => response.json())
        .then(data => this.process( data ));
    }
    private process(data:any)
    {
      //  console.log(data);
        let sizes:any=data.sizes;
        for (let i:number=0;i<sizes.length;i++)
        {
            let screenSizeVo:ScreenSizeVo=new ScreenSizeVo(parseInt(sizes[i].w),parseInt(sizes[i].h),sizes[i].d,parseInt(sizes[i].per));
            this.ms.screenSizes.push(screenSizeVo);
        }
        this.callback();
    }
}