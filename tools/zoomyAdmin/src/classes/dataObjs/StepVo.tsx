import { ElementVo } from "./ElementVo";
import { StepSaveVo } from "./exports/saver/StepSaveVo";
import { StepExportVo } from "./exports/StepExportVo";
import { ZoomVo } from "./ZoomVo";

export class StepVo
{
    public static ElementID:number=0;
    public static StepCount:number=0;

    public audio:string="";
    public audioName:string="";
    
    public backgroundAudioName:string="";
    public backgroundAudio:string="";

    public id:number;
    public elements:ElementVo[];
    public label:string="";
    public zoom:ZoomVo=new ZoomVo(0,0,100,100);

    constructor(id:number=-1,elements:ElementVo[]=[])
    {        
        if (id===-1)
        {
            StepVo.StepCount++;
            id=StepVo.StepCount;
        }
        this.id=id;
        this.elements=elements;
        this.label=id.toString();
    }
    public fromObj(obj:any)
    {
        console.log(obj);

        let id:number=parseInt(obj.id);
        this.id=id;

        let zx1:number=parseFloat(obj.zoom.x1);
        let zx2:number=parseFloat(obj.zoom.x2);

        let zy1:number=parseFloat(obj.zoom.y1);
        let zy2:number=parseFloat(obj.zoom.y2);

        this.zoom=new ZoomVo(zx1,zy1,zx2,zy2);

        let elements:any[]=obj.elements;
        for (let i:number=0;i<elements.length;i++)
        {
            let elementVo:ElementVo=new ElementVo();
            elementVo.fromObj(elements[i]);
            this.elements.push(elementVo);
        }
        if (obj['audioName'])
        {
            this.audioName=obj.audioName;
        }
        if (obj['audio'])
        {
            this.audio=obj.audio;
        }
        if (obj['backgroundAudio'])
        {
            this.backgroundAudio=obj.backgroundAudio;
        }
        if (obj['backgroundAudioName'])
        {
            this.backgroundAudioName=obj.backgroundAudioName;
        }
    }
    public addElement(elementVo:ElementVo)
    {
        StepVo.ElementID++;
        
        elementVo.id=StepVo.ElementID;
        
        ////console.log("ID="+elementVo.id);
        
        this.elements.push(elementVo);
    }
    public deleteElement(elementVo:ElementVo)
    {
        let index:number=-1;
        let elements2:ElementVo[]=this.elements.slice();

        for (let i:number=0;i<elements2.length;i++)
        {
            if (elements2[i].id===elementVo.id)
            {
                index=i;
            }
        }
        if (index!==-1)
        {
            elements2.splice(index,1);
        }
        return elements2;
    }
    public toExport()
    {
        let stepExportVo:StepExportVo=new StepExportVo(this.id,this.zoom,[],this.label);

        stepExportVo.audio=this.audio;
        stepExportVo.audioName=this.audioName;

        stepExportVo.backgroundAudio=this.backgroundAudio;
        stepExportVo.backgroundAudioName=this.backgroundAudioName;

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepExportVo.elements.push(this.elements[i].getExport());
        }
        return stepExportVo;
    }
    public toSave()
    {
        let stepSaveVo:StepSaveVo=new StepSaveVo(this.id,this.zoom,[],this.label);

        stepSaveVo.audio=this.audio;
        stepSaveVo.audioName=this.audioName;

        stepSaveVo.backgroundAudio=this.backgroundAudio;
        stepSaveVo.backgroundAudioName=this.backgroundAudioName;

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepSaveVo.elements.push(this.elements[i].getSave());
        }
        return stepSaveVo;
    }
    public clone()
    {
        
        let stepVo:StepVo=new StepVo(-1,[]);

        stepVo.zoom=new ZoomVo(this.zoom.x1,this.zoom.y1,this.zoom.x2,this.zoom.y2);

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepVo.elements.push(this.elements[i].clone(1));
        }
        return stepVo;
    }
    public clone2()
    {
        let stepVo:StepVo=new StepVo(this.id,[]);

        stepVo.zoom=new ZoomVo(this.zoom.x1,this.zoom.y1,this.zoom.x2,this.zoom.y2);

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepVo.elements.push(this.elements[i].clone2());
        }
        return stepVo;   
    }
}