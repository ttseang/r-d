import { ZoomVo } from "../ZoomVo";
import { ElementExportVo } from "./ElementExportVo";



export class StepExportVo {
    public id: number;
    public elements: ElementExportVo[];
    public zoom: ZoomVo;

    public audio: string = "";
    public audioName: string = "";
    public backgroundAudioName: string = "";
    public backgroundAudio: string = "";
    public label: string;

    constructor(id: number, zoom: ZoomVo, elements: ElementExportVo[],label:string) {
        this.id = id;
        this.zoom = zoom;
        this.elements = elements;
        this.label=label;
    }
}