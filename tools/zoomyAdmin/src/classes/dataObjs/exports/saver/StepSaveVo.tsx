import { ZoomVo } from "../../ZoomVo";
import { ElementSaveVo } from "./ElementSaveVo";

export class StepSaveVo
{
    public id:number;
    public elements:ElementSaveVo[];
    public zoom:ZoomVo;
    public label:string;

    public audio:string="";
    public audioName:string="";
    
    public backgroundAudioName:string="";
    public backgroundAudio:string="";

    constructor(id:number,zoom:ZoomVo,elements:ElementSaveVo[],label:string)
    {
        this.id=id;
        this.zoom=zoom;
        this.elements=elements;
        this.label=label;
    }
}