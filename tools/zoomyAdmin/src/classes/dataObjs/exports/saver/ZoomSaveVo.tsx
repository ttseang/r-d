import { StepSaveVo } from "./StepSaveVo";

export class ZoomSaveVo
{
    public steps:StepSaveVo[];
    public pageW:number;
    public pageH:number;

    constructor(steps:StepSaveVo[],pageW:number,pageH:number)
    {
        this.steps=steps;
        this.pageW=pageW;
        this.pageH=pageH;
    }
}