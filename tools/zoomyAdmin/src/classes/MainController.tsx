import { MsgVo } from "./dataObjs/MsgVo";

export class MainController
{
    private static instance:MainController | null=null;

    public changeScreen:Function=()=>{};
    public addNewElement:Function=()=>{};

    public openTextEdit:Function=()=>{};
    public closeTextEdit:Function=()=>{};
    public onTextEditClose:Function=()=>{};
    public textEditCallback:Function=()=>{};
    //
    //
    //
    public openImageBrowse:Function=()=>{};
    public closeImageBrowse:Function=()=>{};
    public onImageBrowseClose:Function=()=>{};
    public mediaChangeCallback:Function=()=>{};
    public updateZoomControls:Function=()=>{};
    public lockZoom:Function=()=>{};
    public toggleZoom:Function=()=>{};
    //
    //
    //
    public openForm:Function=()=>{};
    public sendForm:Function=()=>{};

    //
    //
    //
    public saveZoom:Function=()=>{};
    public openZoom:Function=()=>{};
    public exportFile:Function=()=>{};

    public duplicateFrame:Function=()=>{};
    public delFrame:Function=()=>{};
    public addBlankFrame:Function=()=>{};

    public styleChangeCallback:Function=()=>{};
    public setAlert:Function=(msg:MsgVo)=>{};


    public audioChangeCallback:Function=()=>{};
    public backgroundAudioChange:Function=()=>{};

    public undo:Function=()=>{};
    public redo:Function=()=>{};

    public copyElement:Function=()=>{};
    public pasteElement:Function=()=>{};

    public doKeyboardShortCut:Function=(key:string,ctr:boolean,alt:boolean)=>{}
    public stepsUpdated:Function=()=>{};

    
    
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor()
    {
        (window as any).mc=this;
    }
    public static getInstance():MainController
    {
        if (this.instance===null)
        {
            this.instance=new MainController();
        }
        return this.instance;
    }
}