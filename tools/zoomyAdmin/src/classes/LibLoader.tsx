import { LibItemVo } from "./dataObjs/LibItemVo";
import MainStorage from "./MainStorage";

export class LibLoader
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();
    
    constructor(callback:Function)
    {
        this.callback=callback;
    }
    load(path:string)
    {
        //console.log("load Lib");
        fetch(path)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any[])
    {
        //console.log(data);
        
        for (let i:number=0;i<data.length;i++)
        {
            let item:any=data[i];
            let itemVo:LibItemVo=new LibItemVo(item.title,item.image,item.tags);
            this.ms.libUtil.library.push(itemVo)
        }
        this.loadAudio();
    }  
    private loadAudio()
    {
        fetch("./jsonFiles/audio.json")
        .then(response => response.json())
        .then(data => this.gotAudio(data));
    }
    private gotAudio(data:any)
    {
        for (let i:number=0;i<data.length;i++)
        {
            let item:any=data[i];
            let itemVo:LibItemVo=new LibItemVo(item.title,item.image,item.tags);
            this.ms.libUtil.audioLib.push(itemVo)
        }
        this.loadBackground();
    }
    private loadBackground()
    {
        fetch("./jsonFiles/backgroundAudio.json")
        .then(response => response.json())
        .then(data => this.getBackgroundAudio(data));
    }
    private getBackgroundAudio(data:any)
    {
        for (let i:number=0;i<data.length;i++)
        {
            let item:any=data[i];
            let itemVo:LibItemVo=new LibItemVo(item.title,item.image,item.tags);
            this.ms.libUtil.backgroundAudio.push(itemVo)
        }
        this.callback();
    }
}