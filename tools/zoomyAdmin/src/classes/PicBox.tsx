import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { LibItemVo } from './dataObjs/LibItemVo';
import { MainController } from './MainController';
import MainStorage from './MainStorage';
interface MyProps { libItem: LibItemVo }
interface MyState { }
class PicBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getPic() {
        return this.ms.getImagePath(this.props.libItem.image,0);
    }
    changePic()
    {        
        this.mc.mediaChangeCallback(this.props.libItem);
    }
    render() {
        return (<div>
            <Card className='picCard'>
                <Card.Body>
                    <div className="tac fitText">{this.props.libItem.title}</div>
                    <hr/>
                    <div className='cardImageHolder'>
                        <img className='cardImage' alt={this.props.libItem.title} src={this.getPic()}  onClick={()=>{this.changePic()}} />
                    </div>
                </Card.Body>
               
            </Card>
        </div>)
    }
}
export default PicBox;