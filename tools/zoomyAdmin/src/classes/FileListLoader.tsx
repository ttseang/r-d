
import ApiConnect from "./ApiConnect";
import { FileVo } from "./dataObjs/FileVo";
import MainStorage from "./MainStorage";

export class FileListLoader
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    public getFileList()
    {
        let apiConnect:ApiConnect=new ApiConnect();
        apiConnect.getFileList(this.gotFileList.bind(this),this.ms.appType);
    }
    private gotFileList(data:any[])
    {       
        let files:FileVo[]=[];
       // let filter="FB";

        for (let i:number=0;i<data.length;i++)
        {
            //console.log(data[i]);
            let d:any=data[i];
            let t:number=Date.parse(data[i].lastmod);
           // //console.log(t);

          /*   let ltiArray:string=d.lti.split(".");
            let mod:string=ltiArray[2];
            if (mod.includes(filter))
            { */
                let fileVo:FileVo=new FileVo(d.name,d.lti,t,d.lastmod);
                files.push(fileVo);
          //  }          
        }
        this.ms.files=files;
        this.callback();
    }
}