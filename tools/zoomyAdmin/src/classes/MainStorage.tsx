import { ElementVo } from "./dataObjs/ElementVo";
import { PresentationVo } from "./dataObjs/exports/PresentationVo";
import { StepSaveVo } from "./dataObjs/exports/saver/StepSaveVo";
import { ZoomSaveVo } from "./dataObjs/exports/saver/ZoomSaveVo";
import { StepExportVo } from "./dataObjs/exports/StepExportVo";
import { FileVo } from "./dataObjs/FileVo";
import { FormElementVo } from "./dataObjs/FormElementVo";
import { GenVo } from "./dataObjs/GenVo";
import { HistoryVo } from "./dataObjs/HistoryVo";
import { LibItemVo } from "./dataObjs/LibItemVo";
import { PopUpVo } from "./dataObjs/PopUpVo";
import { ScreenSizeVo } from "./dataObjs/ScreenSizeVo";
import { StepVo } from "./dataObjs/StepVo";
import { StyleVo } from "./dataObjs/StyleVo";
import { TextStyleVo } from "./dataObjs/TextStyleVo";
import { ZoomVo } from "./dataObjs/ZoomVo";
import { LibUtil } from "./libUtil";
import { StyleUtil } from "./styleUtil";

export class MainStorage {
  private static instance: MainStorage | null = null;

  public styleMap: Map<number, TextStyleVo> = new Map<number, TextStyleVo>();
  public steps: StepVo[] = [];
  private _currentStep: number = 0;

  public selectedStep: StepVo;

  public selectedElement: ElementVo | null = null;
  public pageW: number = 480;
  public pageH: number = 270;

  public eidIndex: number = 0;
  private _eid: string = "instance0";

  public libUtil: LibUtil;
  public library: LibItemVo[] = [];
  public suggested: string = "";
  public zoomLocked: boolean = true;

  public zoomVo: ZoomVo = new ZoomVo(20, 20, 80, 80);

  public linkedCount: number = 0;

  public popups: PopUpVo[] = [];
  public styles: StyleVo[] = [];
  public styleUtil: StyleUtil;

  public editText: string = "";
  public tempID: number = 0;

  public menu: GenVo[] = [];
  public formData: FormElementVo[] = [];

  public exportCode: string = "";
  public saveCode: string = "";

  private historyIndex: number = 0;
  private history: HistoryVo[] = [];

  public keyLock: boolean = false;

  public copyElement: ElementVo | null = null;
  public LtiFileName: string="";
  public fileName:string="";
  public files:FileVo[]=[];
  public appType:string="zoomy";

  public framePanelIndex:number=0;
  public backgroundAudioSwitch:boolean=false;
  public screenSizes:ScreenSizeVo[]=[];
  
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor() {
    //media items
    this.libUtil = new LibUtil();
    this.library = this.libUtil.library;

    this.styleUtil = new StyleUtil();

    this.styles = this.styleUtil.styles;

    this.steps.push(new StepVo(0, []));
    this.selectedStep = this.steps[0];

    (window as any).ms = this;
  }
  public get eid(): string {
    this._eid = "instance" + this.eidIndex.toString();
    return this._eid;
  }
  public get currentStep(): number {
    return this._currentStep;
  }
  public set currentStep(value: number) {
    this._currentStep = value;
    this.selectedStep = this.steps[this._currentStep];
  }
  public addHistory() {
    if (this.historyIndex !== this.history.length - 1) {
      this.history = this.history.slice(0, this.historyIndex);
    }
    this.history.push(new HistoryVo(this.currentStep, this.steps));
    this.historyIndex = this.history.length - 1;
  }
  public goBack() {
    if (this.historyIndex > 0) {
      this.historyIndex--;
      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }
  public goForward() {
    if (this.historyIndex < this.history.length - 1) {
      this.historyIndex++;

      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }
  getAudioPath(audio: string) {
    return "https://ttv5.s3.amazonaws.com/william/audio/"+audio;
  }
  getPageH() {
    return this.pageH;
  }
  getPageW() {
    return this.pageW;
  }
  public static getInstance(): MainStorage {
    if (this.instance === null) {
      this.instance = new MainStorage();
    }
    return this.instance;
  }
  getStepIndex(id: number) {
    for (let i: number = 0; i < this.steps.length; i++) {
      let step: StepVo = this.steps[i];
      if (step.id === id) {
        return i;
      }
    }
    return -1;
  }
  getImagePath(image: string, path: number = 0) {

    if (path === 1) {
      // return "./images/appImages/" + image;
      return "https://ttv5.s3.amazonaws.com/william/images/appImages/" + image;
    }
    return "https://ttv5.s3.amazonaws.com/william/images/bookimages/" + image;
    // return "./images/bookimages/" + image;
  }
  getPopUpByID(id: number) {
    for (let i: number = 0; i < this.popups.length; i++) {
      if (this.popups[i].id === id) {
        return this.popups[i];
      }
    }
    return this.popups[0];
  }
  getExport() {
    let allSteps: StepExportVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }

    let presentationVo: PresentationVo = new PresentationVo(allSteps, this.pageW, this.pageH);

    //console.log(presentationVo);
    this.exportCode = JSON.stringify(presentationVo);
    return this.exportCode;
  }
  getFullScreenPreview()
  {
    let allSteps: StepExportVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }

    let presentationVo: PresentationVo = new PresentationVo(allSteps, this.pageW, this.pageH);

    //console.log(presentationVo);
    return JSON.stringify(presentationVo);
    
  }
  saveZoom() {
    let allSteps: StepSaveVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toSave())
    }
    let zoomSave: ZoomSaveVo = new ZoomSaveVo(allSteps, this.pageW, this.pageH);
    this.saveCode = JSON.stringify(zoomSave);
    return this.saveCode;
  }
  saveZoom2()
  {
    let allSteps: StepSaveVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toSave())
    }
    let zoomSave: ZoomSaveVo = new ZoomSaveVo(allSteps, this.pageW, this.pageH);
    return zoomSave;
  }
  loadZoom(data: any) {

    //console.log(data);
    this.steps = [];

    this.pageW = parseInt(data.pageW);
    this.pageH = parseInt(data.pageH);

    let steps: any[] = data.steps;

    for (let i: number = 0; i < steps.length; i++) {
      let stepVo: StepVo = new StepVo();
      stepVo.fromObj(steps[i]);
      this.steps.push(stepVo);
    }

    this.selectedStep = this.steps[0];
  }
  showEid()
  {
    for (let i: number = 0; i < this.steps.length; i++) {
      let stepVo: StepVo = this.steps[i];
      console.log("step "+i.toString());
      for (let j:number=0;j<stepVo.elements.length;j++)
      {
         let elementVo=stepVo.elements[j];
         console.log(elementVo.type+" "+elementVo.eid+" id="+elementVo.id.toString());

      }

    }
  }
}
export default MainStorage;