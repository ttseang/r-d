export class DateStorage {
    public static storeInstallEpoch(epoch: string) {
        localStorage.setItem("installEpoch", epoch);
    }
    public static getInstallEpoch(): string {
        return localStorage.getItem("installEpoch") || "";
    }
    public static storeLastEpoch(epoch: string) {
        localStorage.setItem("lastEpoch", epoch);
    }
    public static getEpochList(): string {
        return localStorage.getItem("lastEpoch") || "";
    }
    public static clear() {
        localStorage.removeItem("installEpoch");
        localStorage.removeItem("lastEpoch");
    }
}