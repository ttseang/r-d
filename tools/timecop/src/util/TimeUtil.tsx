import { EpochVo } from "../dataObjs/EpochVo";

export class TimeUtil
{

    public static getCurrentUTCTime(callback:Function)
    {
        //get utc time from server
        //http://worldtimeapi.org/api/timezone/Etc/UTC
        const url = "https://worldtimeapi.org/api/timezone/Etc/UTC";
        fetch(url)
        .then(response => response.json())
        .then(data => {
            let utcDate = new Date(data.utc_datetime);
            callback(utcDate);
        });
    }
    public static getElapsedTime(date1:Date, date2:Date):number
    {
        let diff = date2.getTime() - date1.getTime();
        let diffMinutes = Math.floor(diff / 1000 / 60);
        return diffMinutes;
    }
    public static addElapsedTime(date:Date, minutes:number)
    {
        return new Date(date.getTime() + minutes * 60000);
    }
    public static unixEpochToJsDate(unixEpoch:number):Date
    {
        return new Date(unixEpoch * 1000);
    }
    public static jsDateToUnixEpoch(jsDate:Date):number
    {
        return Math.floor(jsDate.getTime() / 1000);
    }
    public static compareEpochs(epoch:EpochVo,lastEpoch:EpochVo)
    {
        ////console.log("compareEpochs");
        ////console.log(epoch);
        ////console.log(lastEpoch);
        if (epoch===undefined)
        {
            ////console.log("epoch undefined");
            return -1;
        }
        if (lastEpoch===undefined)
        {
            ////console.log("lastEpoch undefined");
            return -1;
        }
        //compare the two epochs and return the difference in seconds
        //if the local time is valid, use it, otherwise use the server time
        //if the server time is 0, use the local time

        console.log(epoch.getLocalTime().toString());
        console.log(lastEpoch.getLocalTime().toString());
       
        let time1 = epoch.localTimeValid ? epoch.localTime : epoch.serverTime;
        let time2 = lastEpoch.localTimeValid ? lastEpoch.localTime : lastEpoch.serverTime;

        if (time2===0)
        {
            time2 = lastEpoch.localTime;
        }

        console.log("time1: " + time1);
        console.log("time2: " + time2);

        let date1:Date = new Date(time1 * 1000);
        let date2:Date = new Date(time2 * 1000);
        console.log("date1: " + date1.toString());
        console.log("date2: " + date2.toString());
        //time 1 is the current time, time 2 is the last time
        //check to see is time1 is in the past
        if (time1 < time2)
        {
            //time1 is in the past, so the clock went backwards
            return 0;
        }
       

        return 1;
    }
    public static guestimateEpoch(epochNow:EpochVo,installEpoch:EpochVo):number
    {
        if (installEpoch.isLocalTimeValid()===false)
        {
            //if the install epoch is not valid, use the server time
            //return epochNow.serverTime - installEpoch.serverTime;
            //get the difference between the local times and add it to the server time
            let diff = epochNow.localTime - installEpoch.localTime;
            return installEpoch.serverTime + diff;
        }
        else
        {
            return epochNow.localTime;
        }
    }
    public static findLastValidEpoch(epochNow:EpochVo,epochList:EpochVo[]):EpochVo | null
    {
        for (let i=epochList.length-1;i>=0;i--)
        {
           let stat=TimeUtil.compareEpochs(epochNow,epochList[i]);
              if (stat===1)
              {
                    return epochList[i];
              }
        }
        return null;
    }
}