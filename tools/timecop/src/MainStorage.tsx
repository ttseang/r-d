import { DateStorage } from "./DateStorage";
import { EpochVo } from "./dataObjs/EpochVo";

export class MainStorage {

    private static _instance: MainStorage;
    public clientDateAtInstall: Date | null = null;
    public clientClock:Date|null = null;

    public installEpoch: EpochVo = new EpochVo();
    public isOnline: boolean = false;

    public epochList: EpochVo[] = [];

    constructor() {
        (window as any).mainStorage = this;
    }
    public static getInstance(): MainStorage {
        if (this._instance == null) {
            this._instance = new MainStorage();
        }
        return this._instance;
    }
    public getClientClock():Date
    {
        if (this.clientClock===null)
        {
            return new Date();
        }
        return this.clientClock;
    }
    public restoreEpochList() {
        let epochListString = DateStorage.getEpochList();
        if (epochListString) {
            let epochList = JSON.parse(epochListString);
            //console.log(epochList);
            for (let i = 0; i < epochList.length; i++) {
                let epoch = new EpochVo();
                console.log(epochList[i]);
                epoch.fromObject(epochList[i]);
                this.epochList.push(epoch);
            }
        }
        let installEpochString = DateStorage.getInstallEpoch();
        if (installEpochString) {
            console.log(installEpochString);
            this.installEpoch =new EpochVo();
            
            this.installEpoch.fromJson(installEpochString);
        }
    }

}