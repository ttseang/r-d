import React from 'react';
import './App.css';
import BaseScreen from './screens/BaseScreen';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <BaseScreen />
      </header>
    </div>
  );
}

export default App;
