import React, { Component } from 'react';
import SetTimeScreen from './SetTimeScreen';
import InstallScreen from './InstallScreen';
import { DateStorage } from '../DateStorage';
import LaunchAppScreen from './LaunchAppScreen';
import { MainStorage } from '../MainStorage';
import AppScreen from './AppScreen';
interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage = MainStorage.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = { mode: -1 };
    }

    componentDidMount(): void {
   //     DateStorage.clear();
        this.ms.restoreEpochList();
        let installEpochString = DateStorage.getInstallEpoch();
        ////console.log("installEpochString: " + installEpochString);
        if (installEpochString!=="") {
            //  this.setState({mode:1});
            ////console.log("app is installed");
            this.setState({ mode: 2 });
        }
        else {
            ////console.log("app is not installed");
            this.setState({ mode: 0 });
        }
    }
    getScreen() {
        switch (this.state.mode) {
            case 0:
                return <SetTimeScreen callback={() => { this.setState({ mode: 1 }) }} />;

            case 1:
                return <InstallScreen callback={()=>{this.setState({mode:2})}}/>;
            case 2:
                return <LaunchAppScreen callback={()=>{this.setState({mode:3})}}/>;
            case 3:
                return <AppScreen/>;
            default:
                return <div>sreen not found</div>;
        }
    }
    render() {
        return (<div>
            {this.getScreen()}
        </div>)
    }
}
export default BaseScreen;