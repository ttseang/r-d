import React, { Component } from 'react';
import { MainStorage } from '../MainStorage';
import { DateStorage } from '../DateStorage';
interface MyProps {callback:Function;}
interface MyState {
    date: Date;
    time: Date;
}
class LaunchAppScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = {date:new Date(), time:new Date()};
        }
        changeDate(event: any) {
            this.setState({ date: event.target.value });
        }
        changeTime(event: any) {
            this.setState({ time: event.target.value });
        }
    setClientClock()
    {
        //make a date from the date and time
        let date = new Date(this.state.date);
        let time = new Date(this.state.time);
        let newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), time.getSeconds());
        this.ms.clientClock = newDate;
    }
    launchAppOnline()
    {
        this.setClientClock();
        this.ms.isOnline=true;
        this.props.callback();
    }
    launchAppOffline()
    {
        this.setClientClock();
        this.ms.isOnline=false;
        this.props.callback();
    }
    uninstall()
    {
        DateStorage.clear();
        //relaunch the app
        window.location.reload();
    }
    render() {
        return (<div className='appScreen'>
            <div className="container">
                <p>Set the current time on the customer's device:</p>
                <label htmlFor="date">Date:</label>
                <input type="date" id="date" onChange={this.changeDate.bind(this)} />

                <label htmlFor="time">Time:</label>
                <input type="time" id="time" onChange={this.changeTime.bind(this)} />
                <button id='launch-app-online' onClick={()=>{this.launchAppOnline()}}>Launch App Online</button>

                <button id='launch-app-offline' onClick={()=>{this.launchAppOffline()}}>Launch App Offline</button>
                <button id='launch-app-offline' onClick={()=>{this.uninstall()}}>UNINSTALL</button>
            </div>            
        </div>)
    }
}
export default LaunchAppScreen;