import React, { Component } from 'react';
import { MainStorage } from '../MainStorage';
import { EpochVo } from '../dataObjs/EpochVo';
import { TimeUtil } from '../util/TimeUtil';
import { DateStorage } from '../DateStorage';
interface MyProps { }
interface MyState { messages: string[]; }
class AppScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private epochVo: EpochVo = new EpochVo();
    constructor(props: MyProps) {
        super(props);
        this.state = { messages: [] };
    }
    componentDidMount(): void {
        this.updateTimes();
    }
    private updateTimes() {
        let msg: string[] = [];
        if (this.ms.isOnline) {
            //update the server time
            alert("update server time");
        }
        else {
            //update the local time            
            this.epochVo.setLocalTime(this.ms.getClientClock());

            msg.push("client clock time: " + this.epochVo.getLocalTime().toString());

            let epochList: EpochVo[] = this.ms.epochList.slice();
            let lastEpoch: EpochVo | undefined = epochList[epochList.length - 1];

            if (lastEpoch === undefined) {
                //no epoch found, use the install epoch
                lastEpoch = this.ms.installEpoch;
                //console.log("lastEpoch undefined");
            }

            console.log(lastEpoch);

            let stat = TimeUtil.compareEpochs(this.epochVo, lastEpoch);
            if (stat === 0) {
                //clock went backwards, use the last epoch
                msg.push("clock went backwards, from last timestamp");

                //

                //msg.push("last timestamp was: " + lastEpoch.getLocalTime().toString());

                let lastValid: EpochVo | null = TimeUtil.findLastValidEpoch(this.epochVo, epochList);

                if (!lastValid) {
                    //no epoch found, use the install epoch
                    lastValid = this.ms.installEpoch;
                }

                if (lastValid) {
                    let lastValidDate: Date = new Date(lastValid.localTime * 1000);
                    msg.push("\nlast valid time was: " + lastValidDate.toString());
                }
            }
            else {
                msg.push("clock did not go backwards");
                if (this.ms.installEpoch.localTimeValid === false) {
                    msg.push("The time was not set correctly at install, ");
                    let time: number = TimeUtil.guestimateEpoch(this.epochVo, this.ms.installEpoch);
                    let guessDate: Date = new Date(time * 1000);
                    msg.push("Using the install server time, and difference between device times I am Guessing the time to be: " + guessDate);
                }
            }
            epochList.push(this.epochVo);

            if (epochList.length > 3) {
                epochList.shift();
            }

            this.ms.epochList = epochList;
            DateStorage.storeLastEpoch(JSON.stringify(epochList));

            this.setState({ messages: msg });
        }
    }
    getMessageList(): JSX.Element[] {
        let msgList: JSX.Element[] = [];
        for (let i = 0; i < this.state.messages.length; i++) {
            msgList.push(<p key={i}>{this.state.messages[i]}</p>);
        }
        return msgList;
    }
    getTimeList()
    {
        let timeList: JSX.Element[] = [];
        for (let i = 0; i < this.ms.epochList.length; i++) {
            timeList.push(<p key={i}>{this.ms.epochList[i].getLocalTime().toString()}</p>);
        }
        return timeList;
    }
    render() {

        return (<div>
            {this.getMessageList()}
            <h4>Time List</h4>
            {this.getTimeList()}
        </div>)
    }
}
export default AppScreen;