import React, { Component } from 'react';
import { TimeUtil } from '../util/TimeUtil';
import { EpochVo } from '../dataObjs/EpochVo';
import { MainStorage } from '../MainStorage';
import { DateStorage } from '../DateStorage';
interface MyProps { callback: Function; }
interface MyState { }
class InstallScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
        this.getServerTime();
    }
    getServerTime() {
        TimeUtil.getCurrentUTCTime(this.onServerTime.bind(this));
    }
    onServerTime(serverTime: Date) {
        ////console.log("server time: " + serverTime);

        let epochVo = new EpochVo();
        if (this.ms.clientDateAtInstall) {
            epochVo.setLocalTime(this.ms.clientDateAtInstall);
        }
        epochVo.setServerTime(serverTime);

        this.ms.installEpoch = epochVo;
        ////console.log(epochVo);

        DateStorage.storeInstallEpoch(epochVo.toJson());
        //delay for 2 seconds
        setTimeout(() => {
            this.props.callback();
        }, 2000);
    }
    render() {
        return (<div className='appScreen'>
            <p>
                Installing the app...
            </p>
        </div>)
    }
}
export default InstallScreen;