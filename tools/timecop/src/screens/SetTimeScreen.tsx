import React, { Component } from "react";
import { MainStorage } from "../MainStorage";
interface MyProps {callback:Function; }
interface MyState {
    date: Date;
    time: Date;
}
class TimeScreen extends Component<MyProps, MyState> {
    private ms:MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {
            date: new Date(),
            time: new Date()
        };
    }
    changeDate(event: any) {
        this.setState({ date: event.target.value });
    }
    changeTime(event: any) {
        this.setState({ time: event.target.value });
    }
    doInstall()
    {
        //make a date from the date and time
        let date = new Date(this.state.date);
        let time = new Date(this.state.time);
        let newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), time.getSeconds());
        this.ms.clientDateAtInstall = newDate;
        
        this.props.callback();
    }
    render() {
        return (
            <div className="appScreen">               
                <div className="container">
                <h3>App is Not Installed</h3>
                <p>Set the inital time on the customer's device:</p>
                    <label htmlFor="date">Date:</label>
                    <input type="date" id="date" onChange={this.changeDate.bind(this)} />

                    <label htmlFor="time">Time:</label>
                    <input type="time" id="time" onChange={this.changeTime.bind(this)} />
                    <button onClick={this.doInstall.bind(this)}>Install</button>
                </div>
            </div>
        );
    }
}
export default TimeScreen;
