export class EpochVo {
    public localTime: number;
    public serverTime: number;
    public localTimeValid: boolean = false;
    //all times are in seconds since epoch (1/1/1970)

    constructor(localTime: number=0, serverTime: number=0) {

        this.localTime = localTime;
        this.serverTime = serverTime;
        this.localTimeValid = this.isLocalTimeValid();
    }
    
    public getLocalTime():Date
    {
        return new Date(this.localTime * 1000);
    }
    public getServerTime():Date
    {
        return new Date(this.serverTime * 1000);
    }
    public setLocalTime(date:Date)
    {
        this.localTime = Math.floor(date.getTime() / 1000);
        if (this.serverTime!==0)
        {
            this.localTimeValid = this.isLocalTimeValid();
        }
    }
    public setServerTime(date:Date)
    {
        this.serverTime = Math.floor(date.getTime() / 1000);
        this.localTimeValid = this.isLocalTimeValid();
    }
    public getDiff():number
    {
        return this.serverTime - this.localTime;
    }
    public isLocalTimeValid():boolean
    {
        //if the local time is within 10 minutes of the server time, it's valid
        let diff = this.getDiff();
        if (Math.abs(diff) < 600)
        {
            return true;
        }
        return false;
    }
    public toJson():any
    {
        //return a json object with the local and server times stringified
        let obj:any = {};
        obj.localTime = this.localTime;
        obj.serverTime = this.serverTime;
        obj=JSON.stringify(obj);
        return obj;            
    }
    public fromJson(json:string)
    {
        //parse the json object and set the local and server times
        let obj:any = JSON.parse(json);
        this.localTime = obj.localTime;
        this.serverTime = obj.serverTime;
        this.localTimeValid = this.isLocalTimeValid();
    }
    public fromObject(obj:any)
    {
        //parse the json object and set the local and server times
        this.localTime = obj.localTime;
        this.serverTime = obj.serverTime;
        this.localTimeValid = this.isLocalTimeValid();
    }
}