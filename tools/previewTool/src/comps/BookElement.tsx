import React, { Component } from 'react';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { TextStyleVo } from '../classes/dataObjs/TextStyleVo';
import MainStorage from '../classes/MainStorage';
interface MyProps { elementVo: ElementVo, callback: Function,onUp:Function }
interface MyState { }
class BookElement extends Component<MyProps, MyState>
{
    public ref: React.RefObject<any>;
    private ms: MainStorage = MainStorage.getInstance();
    private origins: string[];

    constructor(props: MyProps) {
        super(props);
        this.state = {};
        this.ref = React.createRef();
        this.props.elementVo.el = this;

        this.origins = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
    }

    getStyle(elementVo: ElementVo) {
        let style: React.CSSProperties = {};

        style.left = elementVo.x.toString() + "%";
        style.top = elementVo.y.toString() + "%";

        style.width = elementVo.w.toString() + "%";    

        if (elementVo.type === ElementVo.TYPE_BACK_IMAGE) {
            let image: string = "url(" + this.ms.getImagePath(this.props.elementVo.content[0]) + ")";
            style.backgroundImage = image;
            style.backgroundPositionX=elementVo.extras.backgroundPosX+"px";
            style.backgroundPositionY=elementVo.extras.backgroundPosY+"px";

            if (elementVo.subType===ElementVo.SUB_TYPE_GUTTER)
            {
                let ww:number=elementVo.extras.backgroundSizeW;
              
                let bgs:string=ww.toString()+"px";
                style.backgroundSize=bgs;
                style.width=elementVo.w.toString()+"px";
                style.height=elementVo.h.toString()+"px";
               
            }
        }
        if (elementVo.type === ElementVo.TYPE_TEXT && elementVo.textStyle === -1) {

            style.color = elementVo.extras.fontColor;
            style.fontFamily = elementVo.extras.fontName;
        }

        if (elementVo.type === ElementVo.TYPE_TEXT) {
            style.fontSize = elementVo.extras.fontSize;
        }

        if (elementVo.extras.alpha !== 100) {
            style.opacity = elementVo.extras.alpha.toString() + "%";
        }
               
        let transformString:string="translate("+this.origins[elementVo.extras.orientation]+")";

        if (elementVo.extras.rotation>0)
        {
            let angle:number=((elementVo.extras.rotation)/100)*360;
            angle=Math.floor(angle);
            ////console.log(angle);
            transformString+=" rotate("+angle.toString()+"deg)";
        }
        if (elementVo.extras.skewY!==0)
        {
            let skewAngleY:number=((elementVo.extras.skewY)/100)*360;
            skewAngleY=Math.floor(skewAngleY);
            ////console.log(skewAngleY);
            transformString+=" skewY("+skewAngleY.toString()+"deg)";
        }
        if (elementVo.extras.skewX!==0)
        {
            let skewAngleX:number=((elementVo.extras.skewX)/100)*360;
            skewAngleX=Math.floor(skewAngleX);
            ////console.log(skewAngleX);
            transformString+=" skewX("+skewAngleX.toString()+"deg)";
        }
        if (elementVo.extras.flipV===true)
        {
            transformString+=" scaleY(-1)";
        }
        if (elementVo.extras.flipH===true)
        {
            transformString+=" scaleX(-1)";
        }

        ////console.log(transformString);

        style.transform = transformString;




        if (elementVo.extras.borderThick > 0) {
            style.border = "solid";
            style.borderWidth = elementVo.extras.borderThick;
            style.borderColor = elementVo.extras.borderColor;
        }
            

        return style;
    }
    getHtml() {
        let ukey: string = "element" + this.props.elementVo.id.toString();
        let elementVo: ElementVo = this.props.elementVo;
        let style: React.CSSProperties = this.getStyle(elementVo);

        let classArray:string[]=this.props.elementVo.classes.slice();

        if (elementVo.visible===false)
        {
            classArray.push("hid");
        }
        if (elementVo.locked===true)
        {
            classArray.push("locked");
        }

        if (elementVo.textStyle !== -1) {

            if (this.ms.styleMap.has(elementVo.textStyle)) {
                let textStyleVo: TextStyleVo | undefined = this.ms.styleMap.get(elementVo.textStyle);
                console.log(textStyleVo);

                if (textStyleVo) {
                    for (let i: number = 0; i < textStyleVo.classes.length; i++) {
                        classArray.push(textStyleVo.classes[i]);
                    }
                }
            }
        }
        console.log(classArray);


        let classes: string = classArray.concat(['abPos']).join(" ");

       

        ////console.log(elementVo.extras.flipH);




        /*   //console.log(classes);
          //console.log(style); */

        if (this.props.elementVo.type === ElementVo.TYPE_TEXT) {

            return (<article ref={this.ref} key={ukey} id={ukey} onPointerUp={()=>{this.props.onUp(this.props.elementVo)}} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} style={style}>{this.props.elementVo.content[0]}</article>);
        }
        if (this.props.elementVo.type === ElementVo.TYPE_BACK_IMAGE) {

            if (elementVo.subType !== ElementVo.SUB_TYPE_GUTTER) {
                return (<div key={ukey} id={ukey} ref={this.ref} style={style} onPointerUp={()=>{this.props.onUp(this.props.elementVo)}} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} ></div>)
            }
            else {
                return (<div key={ukey} id={ukey} ref={this.ref} style={style} onPointerUp={()=>{this.props.onUp(this.props.elementVo)}} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} ></div>)
            }
        }

        return (<img key={ukey} id={ukey} ref={this.ref} className={classes} style={style} onPointerDown={() => { this.props.callback(this.props.elementVo) }} src={this.ms.getImagePath(this.props.elementVo.content[0])} alt="bookpart" />)
    }
    render() {
        return (this.getHtml())
    }
}
export default BookElement;