import { BookTempVo } from "./dataObjs/BookTempVo";
import { DPageVo } from "./dataObjs/DPageVo";
import { PresentationVo } from "./dataObjs/exports/PresentationVo";
import { StepExportVo } from "./dataObjs/exports/StepExportVo";
import { BookExportVo } from "./dataObjs/exports/viewer/BookExportVo";
import { PageExportVo } from "./dataObjs/exports/viewer/PageExportVo";
import { PopUpExportVo } from "./dataObjs/exports/viewer/PopUpExportVo";
import { ZoomPresentationVo } from "./dataObjs/exports/ZoomPresentation";
import { ZoomStepExportVo } from "./dataObjs/exports/ZoomStepExportVo";
import { FileVo } from "./dataObjs/FileVo";
import { ICStepVo } from "./dataObjs/ICStepVo";
import { PageVo } from "./dataObjs/PageVo";
import { PopUpVo } from "./dataObjs/PopUpVo";
import { StepVo } from "./dataObjs/StepVo";
import { TextStyleVo } from "./dataObjs/TextStyleVo";

export class MainStorage
{
  private static instance:MainStorage | null=null;

  public files:FileVo[]=[];
  public pages: DPageVo[] = [];
  public popups: PopUpVo[] = [];
  public cover: DPageVo;
  public bookInfo: BookTempVo = new BookTempVo(0, "", 0, 0, "");

  public steps: StepVo[] = [];
  public icsteps:ICStepVo[]=[];

  public styleMap: Map<number, TextStyleVo> = new Map<number, TextStyleVo>();
  
  
  public selectedStep: StepVo=new StepVo(-1,[]);
  
  public pageW: number = 640;
  public pageH: number = 480;
  

  public ltiCode:string="";
  public selectedType:string="";
  public appType:string="IC"
  public exportCode:string="";


  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor()
  {
    this.cover = new DPageVo("cover", new PageVo("front cover", [], []), new PageVo("back cover", [], []));
    (window as any).ms=this;
  }
  public static getInstance()
  {
      if (this.instance===null)
      {
          this.instance=new MainStorage();
      }
      return this.instance;
  }
  getBookExport()
  {
    let allPages: PageExportVo[] = [];
    for (let i: number = 0; i < this.pages.length; i++) {
      allPages.push(this.pages[i].getExport());
    }

    let coverExport: PageExportVo = this.cover.getExport();

    let bookExport: BookExportVo = new BookExportVo(coverExport, allPages, this.bookInfo.w, this.bookInfo.h);

    let popupExports: PopUpExportVo[] = [];

    for (let j: number = 0; j < this.popups.length; j++) {
      popupExports.push(this.popups[j].getExport());
    }



    bookExport.popups = popupExports;
    console.log(bookExport);

    this.exportCode = JSON.stringify(bookExport);
    return this.exportCode;
  }
  getZoomExport()
  {
    console.log("GET Zoom Export");
    let allSteps: ZoomStepExportVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toZoomExport())
    }

    let presentationVo: ZoomPresentationVo = new ZoomPresentationVo(allSteps, this.pageW, this.pageH);

    console.log(presentationVo);
    this.exportCode = JSON.stringify(presentationVo);

    
    return this.exportCode;
  }
  getICExport() {
    console.log("Get IC Export");
    let allSteps: StepExportVo[] = [];
    for (let i: number = 0; i < this.icsteps.length; i++) {
      allSteps.push(this.icsteps[i].toExport())
    }

    let presentationVo: PresentationVo = new PresentationVo(allSteps, this.pageW, this.pageH);

    console.log(presentationVo);
    this.exportCode = JSON.stringify(presentationVo);

    
    return this.exportCode;
  }

  getImagePath(image: string, path: number = 0) {

    if (path === 1) {
     // return "./images/appImages/" + image;
      return "https://ttv5.s3.amazonaws.com/william/images/appImages/"+image;
    }
    return "https://ttv5.s3.amazonaws.com/william/images/bookimages/"+image;
   // return "./images/bookimages/" + image;
  }
  loadBook(data: any) {

    console.log(data);
    let w: number = parseInt(data.pwidth);
    let h: number = parseInt(data.pheight);

    this.bookInfo = new BookTempVo(0, "loaded book", w, h, "");

    this.cover = new DPageVo("", new PageVo(), new PageVo());
    this.cover.fromObj(data.covers);

    let pageData: any = data.pages;
    console.log(pageData);

    this.pages = [];

    for (let i: number = 0; i < pageData.length; i++) {
      let dPageVo: DPageVo = new DPageVo("", new PageVo(), new PageVo());
      dPageVo.fromObj(pageData[i]);
      this.pages.push(dPageVo);
    }

    let popUpData:any=data.popups;
    console.log("POP UPS");
    console.log(popUpData);

    this.popups=[];
    for (let i:number=0;i<popUpData.length;i++)
    {
       let popUpVo:PopUpVo=new PopUpVo();
       popUpVo.fromObj(popUpData[i]);
       this.popups.push(popUpVo);
    }

    // console.log(this.cover);

  }

  loadZoom(data: any) {
    console.log("LOAD ZOOM");
    console.log(data);
    this.steps = [];

    this.pageW = parseInt(data.pageW);
    this.pageH = parseInt(data.pageH);

    let steps: any[] = data.steps;

    for (let i: number = 0; i < steps.length; i++) {
      let stepVo: StepVo = new StepVo();
      stepVo.fromObjZoom(steps[i]);
      this.steps.push(stepVo);
    }

    this.selectedStep = this.steps[0];
  }
  loadPres(data: any) {
    console.log("LOAD IC");
    console.log(data);
    this.icsteps = [];

    /* this.pageW = parseInt(data.pageW);
    this.pageH = parseInt(data.pageH); */

    let hh:number=parseInt(data.pageH);
    let ww:number=parseInt(data.pageW);

    let ratio=ww/hh;

    this.pageW=window.innerWidth;
    this.pageH=window.innerHeight*ratio;

    let steps: any[] = data.steps;

    for (let i: number = 0; i < steps.length; i++) {
      let stepVo: ICStepVo = new ICStepVo();
      stepVo.fromObj(steps[i]);
      this.icsteps.push(stepVo);
    }

    this.selectedStep = this.steps[0];
  }
}
export default MainStorage;