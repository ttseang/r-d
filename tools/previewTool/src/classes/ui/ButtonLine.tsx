import React, { Component } from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import { ButtonVo } from "../dataObjs/ButtonVo";

interface MyProps {buttonArray:ButtonVo[],actionCallback:Function,useVert:boolean}
interface MyState {
}
class ButtonLine extends Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getButtons() {
        let blist: JSX.Element[] = [];
        for (let i: number = 0; i < this.props.buttonArray.length; i++) {
          let text: string = this.props.buttonArray[i].text;
          let variant: string = this.props.buttonArray[i].variant;
          let action: number = this.props.buttonArray[i].action;
          let size:any=this.props.buttonArray[i].size;
          //
          //
          let key: string = "bbutton" + i.toString();
          blist.push(
            <Button              
              key={key}
              variant={variant}
              size={size}
              onClick={() => {
                this.props.actionCallback(action,i);
              }}
            >
              {text}
            </Button>
          );
        }
        if (this.props.useVert===true)
        {
            return <ButtonGroup size="sm" className="fw" vertical>{blist}</ButtonGroup>;    
        }
        return <ButtonGroup className="fw">{blist}</ButtonGroup>;
      }
    render() {
        return (
           <div>{this.getButtons()}</div> 
        );
    }
}
export default ButtonLine;