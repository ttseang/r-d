import BookElement from "../../comps/BookElement";
import MainStorage from "../MainStorage";
import { ElementExportVo } from "./exports/ElementExportVo";
import { ElementSaveVo } from "./exports/saver/ElementSaveVo";
import { ExportVo } from "./exports/viewer/ExportVo";
//import { SaveVo } from "./exports/saver/SaveVo";
//import { ExportVo } from "./exports/viewer/ExportVo";
import { ExtrasVo } from "./ExtrasVo";
import { StepVo } from "./StepVo";
import { StyleVo } from "./StyleVo";

export class ElementVo {
    public type: string;
    public subType: string;

    public textStyle:number=-1;
    
    public classes: string[];

    public extras: ExtrasVo = new ExtrasVo();

    // public className:string;
    public content: string[];
    public id: number = -1;
    public eid:string="0";

    public el: BookElement | null = null;

    public linkID: string = "";
    public suggested: string = "";

    public visible: boolean = true;
    public locked: boolean = false;

    private _x: number = 0;
    private _y: number = 0;
    public w: number = 0;
    public h: number = 0;
    public onLeft: boolean = true;

    public static TYPE_IMAGE: string = "IMAGE";
    public static TYPE_TEXT: string = "TEXT";
    public static TYPE_BACK_IMAGE = "BACK_IMAGE";
    public static TYPE_CARD: string = "CARD";
    public static TYPE_LINE: string = "LINE";

    public static SUB_TYPE_NONE: string = "none";
    public static SUB_TYPE_GUTTER: string = "gutter";
    public static SUB_TYPE_POPUP_LINK: string = "popuplink";

    public static SUB_TYPE_GREEN_CARD: string = "greenCard";
    public static SUB_TYPE_WHITE_CARD: string = "whiteCard";

    private ms: MainStorage = MainStorage.getInstance();

    public contentType: "html" | "svg" = "html";

    constructor(type: string = "", subType: string = "", classes: string[] = [], content: string[] = [], x: number = 0, y: number = 0, w = 0) {
        this.type = type;
        this.subType = subType;
        this.classes = classes;
        this.content = content;
        this.x = x;
        this.y = y;
        this.w = w;

        if (this.type === ElementVo.TYPE_TEXT && this.w === 0) {
            this.w = 75;
        }
    }
    fromObj(obj: any) {
        this.type = obj.type;
        this.subType = obj.subType;
        this.classes = obj.classes;
        this.content = obj.content;
        this.x = parseFloat(obj.x);
        this.y = parseFloat(obj.y);
        this.w = parseFloat(obj.w);
        this.h = parseFloat(obj.h) || 0;
        console.log("EID="+obj.eid);
        
        this.eid=obj.eid;

        this.linkID = obj.linkID || "";

        this.extras.fromObj(obj.extras);
    }
    get x() {
        return this._x;
    }
    set x(val: number) {
        val = Math.floor(val * 100) / 100;
        this._x = val;
    }
    get y() {
        return this._y;
    }
    set y(val: number) {
        val = Math.floor(val * 100) / 100;
        this._y = val;
    }
    getStyle() {
        let myStyle: any = { "width": this.w.toString() + "%", "left": this.x.toString() + "%", "top": this.y.toString() + "%" };
        return myStyle;
    }
    setStyle(styleVo: StyleVo) {
        switch (styleVo.subType) {
            case StyleVo.SUBTYPE_POSITION:
                this.x = styleVo.x;
                this.y = styleVo.y;
                break;

            case StyleVo.SUBTYPE_SIZE:
                this.w = styleVo.w;
                break;
        }
    }
    addClass(classID: string) {
        if (!this.classes.includes(classID)) {
            this.classes.push(classID);
        }
    }
    removeClass(classID: string) {
        if (this.classes.includes(classID)) {
            let index: number = this.classes.indexOf(classID);
            this.classes.splice(index, 1);
        }
    }

    clone(variant: number = 0) {
        /* let extrasClone:ExtrasVo=this.extras.clone(); */
        let extrasClone: ExtrasVo = JSON.parse(JSON.stringify(this.extras));

        let cloneElement: ElementVo = new ElementVo(this.type, this.subType, this.classes, this.content.slice(), this.x, this.y, this.w);

        if (variant === 0) {
            if (this.subType !== ElementVo.SUB_TYPE_GUTTER) {
                cloneElement.x += 5;
                cloneElement.y += 5;
            }
            else {
                cloneElement.y -= 5;
            }
        }

        StepVo.ElementID++;

        cloneElement.id=StepVo.ElementID;

        cloneElement.extras = extrasClone;
        cloneElement.suggested = this.suggested;
        cloneElement.onLeft = this.onLeft;
        cloneElement.h = this.h;
        cloneElement.eid=this.eid;
        cloneElement.textStyle=this.textStyle;
        /* if (this.linkID !== "") {
            this.ms.linkedCount++;
            cloneElement.linkID = "linkedElement" + this.ms.linkedCount.toString();
        } */

        return cloneElement;
    }
    clone2() {
        /* let extrasClone:ExtrasVo=this.extras.clone(); */
        let extrasClone: ExtrasVo = JSON.parse(JSON.stringify(this.extras));

        let cloneElement: ElementVo = new ElementVo(this.type, this.subType, this.classes, this.content.slice(), this.x, this.y, this.w);

        cloneElement.id=this.id;
        cloneElement.extras = extrasClone;
        cloneElement.h = this.h;
        cloneElement.eid=this.eid;
        cloneElement.textStyle=this.textStyle;

        return cloneElement;
    }
    public static getNewID()
    {
        return StepVo.ElementID++;
    }
    getBookExport()
    {
        return new ExportVo(this.type,this.subType,this.classes,this.content,this.x,this.y,this.w,this.h,this.extras,this.textStyle);
    }
    getExport()
    {
        return new ElementExportVo(this.type,this.eid,this.subType,this.classes,this.content,this.x,this.y,this.w,this.h,this.extras,this.textStyle);
    }    
    getSave()
    {
        return new ElementSaveVo(this.type,this.eid,this.subType,this.classes,this.content,this.x,this.y,this.w,this.h,this.extras,this.textStyle);
    } 
}