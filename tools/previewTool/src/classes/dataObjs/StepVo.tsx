import MainStorage from "../MainStorage";
import { ElementVo } from "./ElementVo";

import { ZoomStepExportVo } from "./exports/ZoomStepExportVo";
import { ZoomVo } from "./ZoomVo";

export class StepVo
{
    public static ElementID:number=0;
    public static StepCount:number=0;

    public id:number;
    public elements:ElementVo[];
    public label:string="";
    public zoom:ZoomVo=new ZoomVo(0,0,100,100);
    public audio:string="";
    public backgroundAudio:string="";
    
    public audioName:string="no audio set";
    public backgroundAudioName:string="no audio set";

    constructor(id:number=-1,elements:ElementVo[]=[])
    {        
        if (id===-1)
        {
            StepVo.StepCount++;
            id=StepVo.StepCount;
        }
        this.id=id;
        this.elements=elements;
        this.label=id.toString();
    }
    public makeID()
    {
        StepVo.StepCount++;
        this.id=StepVo.StepCount;
    }
    public fromObjZoom(obj:any)
    {
        console.log(obj);
        
        let id:number=parseInt(obj.id);
        this.id=id;

        if (id===-1)
        {
            StepVo.StepCount++;
            this.id=StepVo.StepCount;
        }

        let zx1:number=parseFloat(obj.zoom.x1);
        let zx2:number=parseFloat(obj.zoom.x2);

        let zy1:number=parseFloat(obj.zoom.y1);
        let zy2:number=parseFloat(obj.zoom.y2);

        this.zoom=new ZoomVo(zx1,zy1,zx2,zy2);

        let elements:any[]=obj.elements;
        for (let i:number=0;i<elements.length;i++)
        {
            let elementVo:ElementVo=new ElementVo();
            elementVo.fromObj(elements[i]);

            StepVo.ElementID++;
            elementVo.id=StepVo.ElementID;
            //elementVo.eid=obj.eid;
            
            this.elements.push(elementVo);
        }

        if (obj['audioName'])
        {
            this.audioName=obj.audioName;
        }
        if (obj['audio'])
        {
            this.audio=obj.audio;
        }
        if (obj['backgroundAudio'])
        {
            this.backgroundAudio=obj.backgroundAudio;
        }
        if (obj['backgroundAudioName'])
        {
            this.backgroundAudioName=obj.backgroundAudioName;
        }
    }
    public fromObj(obj:any)
    {
        let id:number=parseInt(obj.id);
        this.id=id;

        if (id===-1)
        {
            StepVo.StepCount++;
            this.id=StepVo.StepCount;
        }


        let elements:any[]=obj.elements;
        for (let i:number=0;i<elements.length;i++)
        {
            let elementVo:ElementVo=new ElementVo();
            elementVo.fromObj(elements[i]);

            StepVo.ElementID++;
            elementVo.id=StepVo.ElementID;
           // elementVo.eid="element+"+elementVo.id.toString();
            
            this.elements.push(elementVo);
        }
    }
    public toZoom()
    {

    }
    public addElement(elementVo:ElementVo)
    {
        StepVo.ElementID++;
        
        elementVo.id=StepVo.ElementID;
        
        //console.log("ID="+elementVo.id);
        
        this.elements.push(elementVo);
    }
    public deleteElement(elementVo:ElementVo)
    {
        let index:number=-1;
        let elements2:ElementVo[]=this.elements.slice();

        for (let i:number=0;i<elements2.length;i++)
        {
            if (elements2[i].id===elementVo.id)
            {
                index=i;
            }
        }
        if (index!==-1)
        {
            elements2.splice(index,1);
        }
        return elements2;
    }
/*      public toExport()
    {
        let stepExportVo:StepExportVo=new StepExportVo(this.id,[],this.audio,this.backgroundAudio);
        for (let i:number=0;i<this.elements.length;i++)
        {
            stepExportVo.elements.push(this.elements[i].getExport());
        }
        return stepExportVo;
    } */
    public toZoomExport()
    {
        let stepExportVo:ZoomStepExportVo=new ZoomStepExportVo(this.id,this.zoom,[]);


        stepExportVo.audio=this.audio;
        stepExportVo.audioName=this.audioName;

        stepExportVo.backgroundAudio=this.backgroundAudio;
        stepExportVo.backgroundAudioName=this.backgroundAudioName;

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepExportVo.elements.push(this.elements[i].getExport());
            
        }
        return stepExportVo;
    }
  /*  public toSave()
    {
        let stepSaveVo:StepSaveVo=new StepSaveVo(this.id,[]);
        for (let i:number=0;i<this.elements.length;i++)
        {
            stepSaveVo.elements.push(this.elements[i].getSave())
        }
    } */
    getFirstImage()
    {
        let ms:MainStorage=MainStorage.getInstance();

        for (let i:number=0;i<this.elements.length;i++)
        {
            let element:ElementVo=this.elements[i];
            if (element.type===ElementVo.TYPE_IMAGE)
            {
                return ms.getImagePath(element.content[0]);
            }
        }
        return "./images/appImages/noImage.jpg";
    }
    public clone()
    {
        
        let stepVo:StepVo=new StepVo(-1,[]);

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepVo.elements.push(this.elements[i].clone(1));
        }
        return stepVo;
    }
    public clone2()
    {
        let stepVo:StepVo=new StepVo(this.id,[]);

        for (let i:number=0;i<this.elements.length;i++)
        {
            stepVo.elements.push(this.elements[i].clone2());
        }
        return stepVo;   
    }
}