import { ElementVo } from "./ElementVo";
import { ExportVo } from "./exports/viewer/ExportVo";

import { KeyVo } from "./KeyVo";

export class PageVo
{
    public static ElementID:number=0;

    //public templateID:number;
    public elements:ElementVo[]=[];
    public title:string;

    constructor(title:string="",elements:ElementVo[]=[],options:KeyVo[]=[])
    {
        //this.templateID=templateID;
        
        this.title=title;
    
        for (let i:number=0;i<elements.length;i++)
        {
            this.addElement(elements[i]);
        }
    }
    public fromObj(obj:any,onLeft:boolean=false)
    {
              
        let elements:ElementVo[]=[];
      
        for (let i:number=0;i<obj.length;i++)
        {
            let elementVo:ElementVo=new ElementVo();
            PageVo.ElementID++;        
       
            elementVo.fromObj(obj[i]);
            elementVo.id=PageVo.ElementID;
            elementVo.onLeft=onLeft;
            elements.push(elementVo);
        }
        this.elements=elements;
    }
    public addElement(elementVo:ElementVo)
    {
        PageVo.ElementID++;
        
        elementVo.id=PageVo.ElementID;
        //console.log("ID="+elementVo.id);
        
        this.elements.push(elementVo);
    }
    public deleteElement(elementVo:ElementVo)
    {
        let index:number=-1;
        let elements2:ElementVo[]=this.elements.slice();

        for (let i:number=0;i<elements2.length;i++)
        {
            if (elements2[i].id===elementVo.id)
            {
                index=i;
            }
        }
        if (index!==-1)
        {
            elements2.splice(index,1);
        }
        return elements2;
    }
    /* public updateOption(key:string,value:string)
    {
        let updated:boolean=false;

        for (let i:number=0;i<this.options.length;i++)
        {
            if (this.options[i].key===key)
            {
                this.options[i].value=value;
                updated=true;
            }
        }
        if (updated===false)
        {
            this.options.push(new KeyVo(key,value));
        }
    }
    getOptValue(key:string)
    {
        for (let i:number=0;i<this.options.length;i++)
        {
            if (this.options[i].key===key)
            {
                return this.options[i].value;
            }
        }
        return "";
    }
 */   
 getExport()
    {
        let exports:ExportVo[]=[];
        for (let i:number=0;i<this.elements.length;i++)
        {
            exports.push(this.elements[i].getBookExport());
        }
        return exports;
    }
 /*    getSave()
    {
        let saves:SaveVo[]=[];
        for (let i:number=0;i<this.elements.length;i++)
        {
            saves.push(this.elements[i].getSave());
        }
        return saves;
    }
    clone()
    {
        return new PageVo(this.title,this.elements);
    } */
}