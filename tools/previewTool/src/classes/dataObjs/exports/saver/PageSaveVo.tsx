import { SaveVo } from "./SaveVo";


export class PageSaveVo
{
    public name:string;
    public left:SaveVo[];
    public right:SaveVo[];

    constructor(name:string,left:SaveVo[],right:SaveVo[])
    {
        this.name=name;
        this.left=left;
        this.right=right;
    }
}