import { ZoomVo } from "../../ZoomVo";
import { ElementSaveVo } from "./ElementSaveVo";

export class StepSaveVo
{
    public id:number;
    public elements:ElementSaveVo[];
    public zoom:ZoomVo;

    constructor(id:number,zoom:ZoomVo,elements:ElementSaveVo[])
    {
        this.id=id;
        this.zoom=zoom;
        this.elements=elements;
    }
}