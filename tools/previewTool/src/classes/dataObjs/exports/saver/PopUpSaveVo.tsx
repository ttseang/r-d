import { SaveVo } from "./SaveVo";

export class PopUpSaveVo
{
   
    public id:number;
    public w:number;
    public h:number;
    public elements:SaveVo[];

    constructor(id:number,w:number,h:number,elements:SaveVo[])
    {
        this.id=id;
        this.w=w;
        this.h=h;
        this.elements=elements;
    }
}