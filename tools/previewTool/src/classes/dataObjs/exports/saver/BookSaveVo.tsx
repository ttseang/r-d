import { PageSaveVo } from "./PageSaveVo";
import { PopUpSaveVo } from "./PopUpSaveVo";


export class BookSaveVo
{
    public pwidth:number=300;
    public pheight:number=450;

    public covers:PageSaveVo;

    public pages:PageSaveVo[];

    public popups:PopUpSaveVo[]=[];
    
    constructor(covers:PageSaveVo,pages:PageSaveVo[],pageWidth:number,pageHeight:number)
    {
        this.covers=covers;
        this.pages=pages;
        this.pwidth=pageWidth;
        this.pheight=pageHeight;
    }
}