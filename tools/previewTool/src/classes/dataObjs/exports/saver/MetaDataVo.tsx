/* {
    "metadata": {"lti": "TT.RD.FB01", "name": "All About Marsupials"},
    "data": {}
    } */

    export class MetaDataVo
    {
        public lti:string;
        public name:string;
        public type:string;
        constructor(lti:string,name:string,type:string)
        {
            this.lti=lti;
            this.name=name;
            this.type=type;
        }
    }