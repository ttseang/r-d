import { ExtrasVo } from "../../ExtrasVo";


export class SaveVo
{
    public type:string;
    public subtype:string;
    public classes:string[];
    public content:string[];
    public x:number;
    public y:number;
    public w:number;
    public h:number;
    public linkID:string;
    public extras:ExtrasVo;
    public textStyle:number;

    constructor(type:string,subtype:string,classes:string[],content:string[],x:number,y:number,w:number,h:number,linkID:string,extras:ExtrasVo,textStyle:number)
    {
        this.type=type;
        this.subtype=subtype;
        this.classes=classes;
        this.content=content;
        this.x=x;
        this.y=y;
        this.w=w;
        this.h=h;
        this.extras=extras;
        this.linkID=linkID;
        this.textStyle=textStyle;
    }
}