import { ZoomVo } from "../ZoomVo";
import { ElementExportVo } from "./ElementExportVo";

export class ZoomStepExportVo
{
    public id:number;
    public elements:ElementExportVo[];
    public zoom:ZoomVo;

    public audio: string = "";
    public audioName: string = "";
    public backgroundAudioName: string = "";
    public backgroundAudio: string = "";

    constructor(id:number,zoom:ZoomVo,elements:ElementExportVo[])
    {
        this.id=id;
        this.zoom=zoom;
        this.elements=elements;
    }
}