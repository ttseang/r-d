import { ExportVo } from "./ExportVo";

export class PageExportVo
{
    public left:ExportVo[];
    public right:ExportVo[];

    constructor(left:ExportVo[],right:ExportVo[])
    {
        this.left=left;
        this.right=right;
    }
}