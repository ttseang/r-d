import { PageExportVo } from "./PageExportVo";
import { PopUpExportVo } from "./PopUpExportVo";

export class BookExportVo
{
    public pwidth:number=300;
    public pheight:number=450;

    public covers:PageExportVo;

    public pages:PageExportVo[];

    public popups:PopUpExportVo[]=[];
    
    constructor(covers:PageExportVo,pages:PageExportVo[],pageWidth:number,pageHeight:number)
    {
        this.covers=covers;
        this.pages=pages;
        this.pwidth=pageWidth;
        this.pheight=pageHeight;
    }
}