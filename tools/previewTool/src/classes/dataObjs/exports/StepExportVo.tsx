
import { ICElementExportVo } from "./ICElmentExportVo";

export class StepExportVo
{
    public id:number;
    public elements:ICElementExportVo[];
    public audio:string;
    public bgAudio:string;

    constructor(id:number,elements:ICElementExportVo[],audio:string,bgAudio:string)
    {
        this.id=id;
        this.elements=elements;
        this.audio=audio;
        this.bgAudio=bgAudio;
    }
}