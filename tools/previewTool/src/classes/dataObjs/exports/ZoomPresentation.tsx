import { ZoomStepExportVo } from "./ZoomStepExportVo";

export class ZoomPresentationVo
{
    public steps:ZoomStepExportVo[];
    public pageW:number;
    public pageH:number;

    constructor(steps:ZoomStepExportVo[],pageW:number,pageH:number)
    {
        this.steps=steps;
        this.pageW=pageW;
        this.pageH=pageH;
    }
}