import { ElementAnimationVo } from "../ElementAnimationVo";
import { ExtrasVo } from "../ExtrasVo";

export class ICElementExportVo
{
    public type:string;
    public subType:string;
    public classes:string[];
    public content:string[];
    public x:number;
    public y:number;
    public w:number;
    public h:number;
    public eid:string;
    public extras:ExtrasVo;
    public animationIn:ElementAnimationVo;
    public animationOut:ElementAnimationVo;  
    public textStyle:number;  

    constructor(type:string,eid:string,subType:string,classes:string[],content:string[],x:number,y:number,w:number,h:number,extras:ExtrasVo,animationIn:ElementAnimationVo,animationOut:ElementAnimationVo,textStyle:number)
    {
        this.type=type;
        this.eid=eid;
        this.subType=subType;
        this.classes=classes;
        this.content=content;
        this.x=x;
        this.y=y;
        this.w=w;
        this.h=h;
        this.extras=extras;
        this.animationIn=animationIn;
        this.animationOut=animationOut;
        this.textStyle=textStyle;
    }
}