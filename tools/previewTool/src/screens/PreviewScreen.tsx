import React, { Component } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
interface MyProps { callback: Function, fullCallback: Function }
interface MyState { }
class PreviewScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        let loc: string = window.location.href;
        let ppath: string = loc + "preview/";


        switch (this.ms.appType) {
            case "book":
                ppath = loc + "preview/book/";
                break;

            case "zoomy":
                ppath = loc + "preview/zoomy/";
                break;

            case "IC":
                ppath = loc + "preview/ic/";
        }

        // console.log(ppath);

        return (<div className='tac'>
            <div id="bookData" style={{ display: "none" }}>{this.ms.exportCode} </div>
            <div className='tac'>
                <iframe src={ppath} title='book preview' width={620} height={480} frameBorder={0} scrolling="no" >
                </iframe>
                <hr />
                <Row><Col><Button variant='primary' onClick={() => { this.props.fullCallback() }}>Full Screen</Button></Col><Col><Button variant='success' onClick={() => { this.props.callback() }}>Close</Button></Col></Row>
            </div>
        </div>)
    }
}
export default PreviewScreen;