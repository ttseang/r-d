import React, { ChangeEvent, Component } from 'react';
import { Card, Col, Row, Button } from 'react-bootstrap';
interface MyProps { callback: Function,cancelCallback:Function }
interface MyState { text: string }
class CodeScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { text: "" };
        }
    onChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.currentTarget.value });
    }
    render() {
        return (<Card><Card.Body>
            <Row><Col>Input Your Code</Col></Row>
            <Row><Col><input type='text' value={this.state.text} onChange={this.onChange.bind(this)} /></Col></Row>
            <hr/>
            <Row><Col><Button onClick={() => { this.props.callback(this.state.text) }}>Load</Button></Col><Col><Button variant="warning" onClick={() => { this.props.cancelCallback()}}>Cancel</Button></Col></Row>
        </Card.Body></Card>)
    }
}
export default CodeScreen;