import React, { Component } from 'react';
import { Alert, Card } from 'react-bootstrap';
import ApiConnect from '../ApiConnect';
import { FileVo } from '../classes/dataObjs/FileVo';
import { MsgVo } from '../classes/dataObjs/MsgVo';
import { FileListLoader } from '../classes/FileListLoader';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import ChooseScreen from './ChooseScreen';
import CodeScreen from './CodeScreen';
import FullPreviewScreen from './FullPreviewScreen';
import LoadScreen from './LoadScreen';
import PreviewScreen from './PreviewScreen';
interface MyProps { }
interface MyState { mode: number, msg: MsgVo | null }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0, msg: null };
    }
    getScreen() {
        switch (this.state.mode) {
            case -1:
                return "LOADING";

            case 0:
                return <ChooseScreen callback={this.selectFileType.bind(this)}></ChooseScreen>
            case 1:
                return <LoadScreen openCallback={this.openFile.bind(this)} cancelCallback={() => { this.setState({ mode: 0 }) }}></LoadScreen>

            case 2:
                return <PreviewScreen callback={() => { this.setState({ mode: 0 }); }} fullCallback={() => { this.setState({ mode: 4 }) }}></PreviewScreen>

            case 3:
                return <CodeScreen callback={this.loadCode.bind(this)} cancelCallback={() => { this.setState({ mode: 0 }) }}></CodeScreen>

            case 4:
                return <FullPreviewScreen callback={() => { this.setState({ mode: 0 }) }}></FullPreviewScreen>
        }
        return "That feature is not yet ready";
    }
    getAlert() {
        if (this.state.msg === null) {
            return "";
        }
        if (this.state.msg.time > 0) {
            setTimeout(() => {
                this.setState({ msg: null });
            }, this.state.msg.time);
        }
        return (<div id="blockScreen"><Alert id="alert1" variant='success'>{this.state.msg.msg}</Alert></div>)
    }
    loadCode(code: string) {
        code = code.trim();
        //console.log("Load Code=" + code);

        this.ms.ltiCode = code;
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getInfoByCode(code, this.gotFileInfo.bind(this));
    }
    gotFileInfo(data: any) {
        //console.log(data);
        if (data['error']) {
            this.setState({ msg: new MsgVo(data['error'], "warning", 2000) });
            return;
        }
        this.ms.appType = data[0].type;
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getFileContent(this.ms.ltiCode, this.gotFileData.bind(this));
    }

    selectFileType(fileIndex: number) {
        //console.log(fileIndex);

        if (fileIndex === 3) {
            this.setState({ mode: 3 });
            //alert("That feature is not yet ready");
            return;
        }

        let fileTypes: string[] = ["book", "IC", "zoomy"];
        //console.log(this);
        //console.log(this.ms);

        this.ms.appType = fileTypes[fileIndex];

        let fll: FileListLoader = new FileListLoader(() => { this.setState({ mode: 1 }) });
        fll.getFileList();
    }
    openFile(fileVo: FileVo) {
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getFileContent(fileVo.lti, this.gotFileData.bind(this))
    }
    gotFileData(response: any) {
        //console.log(response);

        switch (this.ms.appType) {
            case "book":
                this.ms.loadBook(response);
                this.ms.getBookExport();
                break;

            case "zoomy":
                this.ms.loadZoom(response);
                this.ms.getZoomExport();
                break;

            case "IC":
                this.ms.loadPres(response);
                this.ms.getICExport();
        }

        // this.ms.loadZoom(response);
        //  this.mc.changeScreen(0);
        this.setState({ mode: 4 });
    }
    render() {
        if (this.state.mode === 4) {
            return this.getScreen();
        }
        return (<div id="base">
            <Card>
                {this.getAlert()}
                <Card.Header className="head1">
                    <span className="titleText">Preview Tool</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;