import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
interface MyProps { callback: Function }
interface MyState { }
class FullPreviewScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        let loc: string = window.location.href;
        let ppath: string = loc + "preview/";


        switch (this.ms.appType) {
            case "book":
                ppath = loc + "preview/book/";
                break;

            case "zoomy":
                ppath = loc + "preview/zoomy/";
                break;

            case "IC":
                ppath = loc + "preview/ic/";
        }

        console.log(ppath);

        return (<div>
            <Button variant='success' id="btnExit" onClick={() => { this.props.callback() }}>Close</Button>
            <div id="bookData" style={{ display: "none" }}>{this.ms.exportCode} </div>
            <div>
                <iframe src={ppath} id="fullFrame" title='book preview' frameBorder={0} scrolling="no" >
                </iframe>
            </div>
        </div>)
    }
}
export default FullPreviewScreen;