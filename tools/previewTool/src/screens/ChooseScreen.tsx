import React, { Component } from 'react';
import { GenVo } from '../classes/dataObjs/GenVo';
import GenCard from '../classes/ui/GenCard';
interface MyProps {callback:Function}
interface MyState { }
class ChooseScreen extends Component<MyProps, MyState>
{
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(props: MyProps) {
        super(props);

    }
    getMenu() {
        let menu: GenVo[] = [new GenVo("Book Files", "", 0, "GET", "primary", false), new GenVo("Presentation Files", "", 1, "GET", "primary", false), new GenVo("Zoom Files", "", 2, "GET", "primary", false),new GenVo("I Have A Code!", "", 3, "GET", "primary", false)]

        let boxes: JSX.Element[] = [];
        for (let i: number = 0; i < menu.length; i++) {
            let key:string="box"+i.toString();

            boxes.push(<GenCard key={key} data={menu[i]} callback={()=>{this.props.callback(menu[i].action)}}></GenCard>)
        }
        return boxes;
    }
    render() {
        return (<div className="scroll2">{this.getMenu()}</div>)
    }
}
export default ChooseScreen;