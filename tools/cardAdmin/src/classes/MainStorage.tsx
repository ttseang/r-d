import { CardVo } from "./dataObjs/CardVo";


export class MainStorage {
  private static instance: MainStorage;

  public cards: CardVo[] = [];

  public editIndex: number = 0;
  public deleteID:number=0;
  
  constructor() {
    this.cards.push(new CardVo(1, "front 1", "back 1"));
    this.cards.push(new CardVo(2, "front 2", "back 2"));
    this.cards.push(new CardVo(3, "front 3", "back 3"));
    this.cards.push(new CardVo(4, "front 4", "back 4"));
  }

  public getCard(id: number) {
    for (let i: number = 0; i < this.cards.length; i++) {
      if (this.cards[i].id === id) {
        return this.cards[i];
      }
    }
    return new CardVo(-1, "", "");
  }
  public editCard(cardVo: CardVo) {
    for (let i: number = 0; i < this.cards.length; i++) {
      if (this.cards[i].id === cardVo.id) {
        this.cards[i] = cardVo;
      }
    }
  }
  public getCardIndex(id: number) {
    for (let i: number = 0; i < this.cards.length; i++) {
      if (this.cards[i].id === id) {
        return i;
      }
    }
    return -1;
  }
  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }

    return this.instance;
  }

}
export default MainStorage;
