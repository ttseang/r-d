import React, { ChangeEvent, Component } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { CardVo } from '../dataObjs/CardVo';
import MainStorage from '../MainStorage';
interface MyProps { buttonText: string, buttonText2: string, cardVo: CardVo, callback: Function, cancelCallback: Function }
interface MyState { frontText: string, backText: string }
class InputCard extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { frontText: this.props.cardVo.front, backText: this.props.cardVo.back };
    }
    onFrontChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ frontText: e.target.value });
    }
    onBackChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ backText: e.target.value });
    }
    doCancel() {
        this.props.cancelCallback();
    }
    doInput() {
        
        this.props.callback(new CardVo(this.ms.cards.length + 1, this.state.frontText, this.state.backText));
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    <Row className="formRow">
                        <Col sm="12" md="8">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="front text"
                                onChange={this.onFrontChange.bind(this)}
                                value={this.state.frontText}
                            />
                        </Col>
                    </Row>
                    <Row className="formRow">
                        <Col sm="12" md="8">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="back text"
                                onChange={this.onBackChange.bind(this)}
                                value={this.state.backText}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col className="searchButton" sm="4" md="4">
                            <Button className="searchButton" onClick={this.doCancel.bind(this)} variant="danger">{this.props.buttonText2}</Button>
                        </Col>
                        <Col className="searchButton" sm="4" md="4">
                            <Button className="searchButton" onClick={this.doInput.bind(this)}>{this.props.buttonText}</Button>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default InputCard;