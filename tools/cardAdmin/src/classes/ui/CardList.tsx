import React, { Component } from 'react';
import { Button, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import { CardVo } from '../dataObjs/CardVo';
interface MyProps { list: CardVo[],editCallback:Function,deleteCallback:Function }
interface MyState { list: CardVo[] }
class CardList extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { list: this.props.list }
        }
    componentDidUpdate(p: MyProps) {
        if (p.list !== this.props.list) {
            this.setState({ list: this.props.list });
        }
    }
    getList() {
        if (this.state.list.length === 0) {
            return (<div>When you make cards they will appear here</div>);
        }
        let cl: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.list.length; i++) {
            let card: CardVo = this.state.list[i];
            let key: string = "card" + i.toString();

            cl.push(<ListGroupItem key={key}>
                    <Row>
                    <Col sm="2">{card.front}</Col>
                    <Col sm="2">{card.back}</Col>
                    <Col sm="4"></Col>
                    <Col sm="2"><Button onClick={()=>{this.props.editCallback(card.id)}}>Edit</Button></Col>
                    <Col sm="2"><Button variant="danger" onClick={()=>{this.props.deleteCallback(card.id)}}>Delete</Button></Col>
                    </Row></ListGroupItem>)
        }
        return (<ListGroup>{cl}</ListGroup>)
    }
    render() {
        return (<div>{this.getList()}</div>)
    }
}
export default CardList;