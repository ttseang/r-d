export class CardVo
{
    public front:string;
    public back:string;
    public id:number;

    constructor(id:number,front:string,back:string)
    {
        this.id=id;
        this.front=front;
        this.back=back;
    }
}