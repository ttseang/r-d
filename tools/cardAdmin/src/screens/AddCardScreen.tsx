import React, { Component } from 'react';
import { CardVo } from '../classes/dataObjs/CardVo';
import MainStorage from '../classes/MainStorage';
import InputCard from '../classes/ui/InputCard';
interface MyProps {modeCallback:Function}
interface MyState { }
class AddCardScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    cancel()
    {
        this.props.modeCallback(0);
    }
    addCard(cv:CardVo)
    {
        this.ms.cards.push(cv);
        this.props.modeCallback(0);
    }
    render() {

        let nCard:CardVo=new CardVo(-1,"","");

        return (<div>
            <InputCard cardVo={nCard} buttonText="Add New" buttonText2="Cancel" callback={this.addCard.bind(this)} cancelCallback={this.cancel.bind(this)}></InputCard>
        </div>)
    }
}
export default AddCardScreen;