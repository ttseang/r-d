import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
interface MyProps {sayYes:Function,sayNo:Function}
interface MyState { }
class DeleteScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    <Row><Col sm="2"></Col><Col sm="4"><Button variant="primary" onClick={()=>{this.props.sayNo()}}>No</Button></Col><Col sm="2"></Col><Col sm="4"><Button variant="danger" onClick={()=>{this.props.sayYes()}}>Yes</Button></Col><Col sm="4"></Col></Row>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default DeleteScreen;