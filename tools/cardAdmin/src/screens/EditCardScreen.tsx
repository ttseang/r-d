import React, { Component } from 'react';
import { CardVo } from '../classes/dataObjs/CardVo';
import MainStorage from '../classes/MainStorage';
import InputCard from '../classes/ui/InputCard';
interface MyProps {modeCallback:Function}
interface MyState { }
class EditCardScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    cancel()
    {
        this.props.modeCallback(0);
    }
    editCard(cv:CardVo)
    {
        console.log(cv);
        cv.id=this.ms.editIndex;
        
        this.ms.editCard(cv);
        console.log(this.ms);
       // this.ms.cards[this.ms.editIndex]=cv;
        this.props.modeCallback(0);
    }
    render() {

        let nCard:CardVo=this.ms.getCard(this.ms.editIndex);

        return (<div>
            <InputCard cardVo={nCard} buttonText="Update" buttonText2="Cancel" callback={this.editCard.bind(this)} cancelCallback={this.cancel.bind(this)}></InputCard>
        </div>)
    }
}
export default EditCardScreen;