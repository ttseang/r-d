import React, { Component } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { CardVo } from '../classes/dataObjs/CardVo';
import MainStorage from '../classes/MainStorage';
import CardList from '../classes/ui/CardList';
interface MyProps { callback: Function, cards: CardVo[] }
interface MyState { cards: CardVo[] }
class StartScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { cards: this.props.cards };
    }
    componentDidUpdate(p: MyProps) {
        if (p.cards !== this.props.cards) {
            this.setState({ cards: this.props.cards });
        }
    }
    deleteCard(id: number) {
        console.log("delete " + id);
        this.ms.deleteID=id;/* 
        let index:number=this.ms.getCardIndex(id);
        this.ms.cards.splice(index,1); */
        this.props.callback(2);
    }
    editCard(index: number) {
        console.log("edit "+index);
        this.ms.editIndex = index;
       
        this.props.callback(1);
        
    }
    render() {
        return (<div>
            <CardList list={this.state.cards} editCallback={this.editCard.bind(this)} deleteCallback={this.deleteCard.bind(this)}></CardList>
            <hr />
            <Row><Col><Button onClick={() => { this.props.callback(0) }}>Add New Card</Button></Col></Row>
        </div>)
    }
}
export default StartScreen;