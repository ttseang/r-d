import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import AddCardScreen from './AddCardScreen';
import DeleteScreen from './DeleteScreen';
import EditCardScreen from './EditCardScreen';
import StartScreen from './StartScreen';
interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0 };
    }

    getScreen() {
        switch (this.state.mode) {
            case 0:
                return <StartScreen callback={this.doMenuAction.bind(this)} cards={this.ms.cards}></StartScreen>
            case 1:
                return <AddCardScreen modeCallback={this.doMode.bind(this)}></AddCardScreen>
            case 2:
                return <EditCardScreen modeCallback={this.doMode.bind(this)}></EditCardScreen>
            case 3:
                return <DeleteScreen sayYes={this.confirmDelete.bind(this)} sayNo={this.cancelDelete.bind(this)}></DeleteScreen>

        }
    }
    cancelDelete()
    {
        this.setState({mode:0});
    }
    confirmDelete()
    {        
        console.log("confirm delete");
        
        let index:number=this.ms.getCardIndex(this.ms.deleteID);
        this.ms.cards.splice(index,1);
        this.setState({mode:0});
    }
    doMode(mode: number) {
        this.setState({ mode: mode });
    }
    doMenuAction(action: number) {
        switch (action) {
            case 0:
                this.setState({ mode: 1 });
                break;

            case 1:
                this.setState({ mode: 2 });
                break;

            case 2:
                this.setState({ mode: 3 });
                break;

            
        }
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Title Here</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;