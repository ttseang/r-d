import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import EQDisplay from '../ui/EQDisplay';
import { TowerGridDisplay } from '../ui/TowerGridDisplay';
interface MyProps { }
interface MyState { step: number }
class DisplayScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { step: 0 };
    }
    componentDidMount()
    {
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    updateEq() {
        let towerGridDisplay: TowerGridDisplay = new TowerGridDisplay(this.ms.towerGrid);

        let html: string = towerGridDisplay.renderGrid(this.state.step);

        let div: HTMLDivElement = document.getElementById("mathDisplay") as HTMLDivElement;

        if (div) {
            div.innerHTML = html;
        }
    }
    changeStep(e: ChangeEvent<HTMLInputElement>) {
        let step: number = parseInt(e.currentTarget.value);
        this.setState({ step: step });
      //  this.ms.step = step;
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    getDisplay()
    {
        return (<Row><Col><EQDisplay cardClass='displayCard' fontSize={26}></EQDisplay></Col></Row>)
    }
    render() {
        return (<div>
            
            {this.getDisplay()}
            
            <Row>
                <Col sm={4}><span className="smText">Step:</span></Col>
                <Col sm={2}><input id="stepper1" min={0} onChange={this.changeStep.bind(this)} type='number' value={this.state.step} /></Col>
            </Row>
        </div>)
    }
}
export default DisplayScreen;