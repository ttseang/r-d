import { Component } from 'react';
import { Row, Col, ButtonGroup, Button } from 'react-bootstrap';
import { SidePanels } from '../classes/Constants';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
import ColumnBox from '../comps/ColumnBox';
import CssScroll from '../comps/CssScroll';
import EditToolbar from '../comps/EditToolbar';
import RowBox from '../comps/RowBox';
import StepCard from '../comps/StepCard';
import TowerEdit from '../comps/TowerEdit';
import { CellGridOutputVo } from '../dataObjs/CellGridOutputVo';
interface MyProps { }
interface MyState { cells: CellGridOutputVo[], step: number, sidePanel: SidePanels }
class EditScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { cells: this.ms.towerGrid2, step: 0, sidePanel: SidePanels.CSS };

        this.controller.setSidePanel = this.setPanel.bind(this);
    }
    getCards() {
        let cards: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.cells.length; i++) {
            let key: string = "card" + i.toString();
            let selected: boolean = (this.state.step === i) ? true : false;

            cards.push(<StepCard key={key} index={i} selected={selected} stepData={this.state.cells[i]} callback={this.selectCard.bind(this)}></StepCard>)
        }
        return cards;
    }
    getStepButtons()
    {
        return <ButtonGroup><Button onClick={this.copyUp.bind(this)}>Copy</Button><Button variant='danger' onClick={this.deleteStep.bind(this)}>Delete</Button></ButtonGroup>
    }
    selectCard(index: number) {
        this.setState({ step: index })
    }
    setPanel(panel: SidePanels) {
        /* if (panel === this.state.sidePanel) {
            panel = SidePanels.None;
        } */
        this.setState({ sidePanel: panel })
    }
    copyUp()
    {
       
        let copyCell:CellGridOutputVo=this.state.cells[this.state.step].clone();
        console.log(copyCell);
        let cells:CellGridOutputVo[]=this.state.cells.slice();
        if (this.state.step===0)
        {
            cells.splice(0,0,copyCell);
        }
        else
        {
            cells.splice(this.state.step,0,copyCell);
        }
        
        this.setState({cells:cells});
    }
    deleteStep()
    {
        if (this.state.cells.length===1)
        {
            return;
        }
        let cells:CellGridOutputVo[]=this.state.cells.slice();
        cells.splice(this.state.step,1);
        let step:number=this.state.step;
        while(step>cells.length-1 && step>-1){
            step--;
        }
        
        this.setState({cells:cells,step:step});
    }
    getEditComp() {
        return <TowerEdit callback={this.updateStep.bind(this)} stepData={this.state.cells[this.state.step]}></TowerEdit>
    }
    updateStep(cell: CellGridOutputVo) {
        let tempCells: CellGridOutputVo[] = this.state.cells;
        tempCells[this.state.step] = cell;
        this.setState({ cells: tempCells })
    }
    getSidePanel() {
        switch (this.state.sidePanel) {
            case SidePanels.None:
                return "";

            case SidePanels.Cols:

                return <ColumnBox></ColumnBox>

            case SidePanels.Rows:
                return <RowBox></RowBox>

            case SidePanels.CSS:
                return <CssScroll></CssScroll>

            default:
                return "";
        }
       
    }
    render() {
        return (<div>
            <div>
                <Row>
                    <Col sm={3} className="silverBack colHeader">STEPS</Col>
                    <Col sm={6} className="silverBack"><EditToolbar></EditToolbar></Col>
                    <Col sm={3} className="silverBack colHeader">Controls</Col>
                </Row>
                <Row>
                    <Col sm={3} className="greyBack">
                        <div className="scroll2">
                            {this.getCards()}
                        </div>
                        {this.getStepButtons()}
                    </Col>
                    <Col sm={6} className="editArea">{this.getEditComp()}</Col>
                    <Col sm={3} className="greyBack">{this.getSidePanel()}</Col>
                </Row>
            </div>
        </div>)
    }
}
export default EditScreen;