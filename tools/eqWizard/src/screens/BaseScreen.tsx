import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { CSSTypes } from '../classes/Constants';
import MainStorage from '../classes/MainStorage';
import { CSSVo } from '../dataObjs/CSSVo';
import { AddCalculator } from '../util/calcs/AddCalculator';
import { MulCalculator } from '../util/calcs/MulCalculator';
import { SubCalculator } from '../util/calcs/SubCalculator';
import { DivCalculator } from '../util/calcs'

import EditScreen from './EditScreen';
import EquationScreen from './EquationScreen';
interface MyProps { }
interface MyState {mode:number}
class BaseScreen extends Component<MyProps, MyState>
{
    private subCalc:SubCalculator=new SubCalculator();
    private addCalc:AddCalculator=new AddCalculator();
    private mulCalc:MulCalculator=new MulCalculator();
    private divCalc:DivCalculator=new DivCalculator();
    
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {mode:-1};
        }
    componentDidMount()
    {
        this.getCSSFromJson();
    }
    getScreen()
    {
        switch(this.state.mode)
        {
            case -1:
                return "loading";
            case 0:
                return <EquationScreen callback={this.showDisplay.bind(this)}></EquationScreen>
            case 1:
                return <EditScreen></EditScreen>
        }
    }
    getCSSFromJson()
    {
        fetch("./cssList.json")
        .then(response => response.json())
        .then(data => this.gotCss(data));
    }
    gotCss(data:any)
    {
        //console.log(data);
        let cssList:any=data.cssList;
        for (let i:number=0;i<cssList.length;i++)
        {
            let css:any=cssList[i];

            let type:string=css.type;

            let cssType:CSSTypes=CSSTypes.Color;

            switch(type)
            {
                case "size":
                cssType=CSSTypes.Size;
                break;

                case "decor":
                cssType=CSSTypes.Decor;
                break;

                /* case "header":
                cssType=CSSTypes.Header; */
            }
            //console.log(type);

            let cssVo:CSSVo=new CSSVo(cssType,css.name,css.value);
            this.ms.addCssType(cssVo);
        }
        this.setState({mode:0});
    }
    showDisplay()
    {
        this.setState({mode:1});
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Equation Wizard</span>
                </Card.Header>
                <Card.Body>
                {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;