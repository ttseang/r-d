import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import RTower from '../comps/RTower';
import { CellGridOutputVo } from '../dataObjs/CellGridOutputVo';
interface MyProps { }
interface MyState {step:number}
class DisplayScreen2 extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {step:3};
        }
        changeStep(e: ChangeEvent<HTMLInputElement>) {
            let step: number = parseInt(e.currentTarget.value);
            this.setState({ step: step });
          //  this.ms.step = step;
           
        }
    getInput()
    {
        return (<Row>
            <Col sm={4}><span className="smText">Step:</span></Col>
            <Col sm={2}><input id="stepper1" min={0} onChange={this.changeStep.bind(this)} type='number' value={this.state.step} /></Col>
        </Row>)
    }
    render() {
        if (!this.ms.towerGrid2[this.state.step])
        {
            return <div>{this.getInput()}<div>Invalid</div></div>
            
        }
        let currentStep:CellGridOutputVo=this.ms.towerGrid2[this.state.step];
       // //console.log(currentStep);

        return (<div>
            <RTower top={currentStep.top} rows={currentStep.rows} answer={currentStep.answer} fontSize={26} operator={currentStep.operator}></RTower>
            {this.getInput()}
        </div>)
    }
}
export default DisplayScreen2;