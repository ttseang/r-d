import React, { ChangeEvent, Component } from 'react';
import { Card, Row, Col, Button, Alert } from 'react-bootstrap';
import MainStorage from '../classes/MainStorage';
import EQDisplay from '../ui/EQDisplay';
import { EqEngine } from '../util/EqEngine';
interface MyProps {callback:Function}
interface MyState { text: string, useRemain: boolean, zeroLimit: number, step: number, fontSize: number, msg: string }
class EquationScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    private eqEngine: EqEngine | null = null;


    constructor(props: MyProps) {
        super(props);
        this.state = { useRemain: false, step: 0, zeroLimit: 2, text: "", fontSize: 26, msg: "" };
    }
    changeZeroLimit(e: ChangeEvent<HTMLInputElement>) {
        let zeroLimit: number = parseInt(e.currentTarget.value);
        this.ms.zeroLimit = zeroLimit;
        this.setState({ zeroLimit: zeroLimit });
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    updateEq() {
        let eqEng: EqEngine = new EqEngine(this.state.text, false);
        eqEng.useRemain = this.state.useRemain;
        eqEng.zeroLimit = this.state.zeroLimit;

        // this.eqEngine.allSpans = this.ms.allSpans;
        //  this.eqEngine.mapSpans();

        let html: string;
        html = eqEng.getStep(this.state.step);
        let div: HTMLDivElement = document.getElementById("mathDisplay") as HTMLDivElement;

        if (div) {
            div.innerHTML = html;
        }
    }
    goNext()
    {
        let eqEng: EqEngine = new EqEngine(this.state.text, false);
        eqEng.useRemain = this.state.useRemain;
        eqEng.zeroLimit = this.state.zeroLimit;

        for (let i:number=0;i<10;i++)
        {
           // let html: string = eqEng.getStep(i);
            eqEng.getStep(i);
            ////console.log(this.ms.gridOutput);

            if (this.ms.celloutput)
            {
                //this.ms.towerGrid.push(this.ms.gridOutput);
                this.ms.towerGrid2.push(this.ms.celloutput);
               // //console.log(this.ms.celloutput);
                
            }

            //this.ms.gridOutput=null;
            this.ms.celloutput=null;
        }
        //console.log(this.ms.towerGrid2);
        ////console.log(this.ms.towerGrid);
        this.props.callback();
    }
    changeRemain(e: React.ChangeEvent<HTMLInputElement>) {
        ////////console.log(e.currentTarget.value);
        this.ms.useRemainder = !this.state.useRemain;
        this.setState({ useRemain: !this.state.useRemain });

        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    getDivTools() {
        if (!this.state.text.includes("/")) {
            return "";
        }
        let offOn: string = (this.state.useRemain === true) ? "on" : "off";

        return (<Card>
            <Card.Body>
                <Row>
                    <Col sm={4}>Zero Limit:</Col>
                    <Col className="tal">
                        <input id="stepper1" min={0} onChange={this.changeZeroLimit.bind(this)} type='number' value={this.state.zeroLimit} />
                    </Col>
                </Row>
                <Row>
                    <Col sm={4}>Use Remainder:</Col>
                    <Col className='tal'><input type='checkbox' value={offOn} onChange={this.changeRemain.bind(this)}></input></Col>
                </Row>
            </Card.Body>
        </Card>)
    }
    setText(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.currentTarget.value })
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    clearDisplay() {
        if (this.eqEngine) {
            this.ms.allSpans = [];
            this.eqEngine.resetSpans([]);
        }


        this.setState({ text: "", step: 0, fontSize: 26 });
        setTimeout(() => {
            this.updateEq();
        }, 500);
    }
    changeStep(e: ChangeEvent<HTMLInputElement>) {
        let step: number = parseInt(e.currentTarget.value);
        this.setState({ step: step });
        this.ms.step = step;
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    changeFontSize(e: ChangeEvent<HTMLInputElement>) {
        let fs: number = parseInt(e.currentTarget.value);
        this.setState({ fontSize: fs });
        setTimeout(() => {
            this.updateEq();
        }, 200);
    }
    getDisplay() {
        if (this.state.msg === "") {
            return (<Row><Col><EQDisplay cardClass='displayCard' fontSize={this.state.fontSize}></EQDisplay></Col></Row>)
        }
        else {
            return (<Card className="displayCard"><Card.Body><Row><Col><Alert variant='success'>{this.state.msg}</Alert></Col></Row></Card.Body></Card>)
        }
    }
    render() {
        return (<div className='mcontainer'>
            {this.getDisplay()}
            <hr/>
            <Card><Card.Body>
            <Row>
                <Col className='tac' sm={8}><input id="text2" type='text' value={this.state.text} onChange={this.setText.bind(this)} /></Col>
                <Col>
                
                    <Row>

                        <Col sm={4}><span className="smText">Step:</span></Col>
                        <Col sm={2}><input id="stepper1" min={0} onChange={this.changeStep.bind(this)} type='number' value={this.state.step} /></Col>
                    </Row>
                    <Row>

                        <Col sm={4}><span className="smText">Text Size:</span></Col>
                        <Col sm={2}><input id="stepper1" min={0} onChange={this.changeFontSize.bind(this)} type='number' value={this.state.fontSize} /></Col>
                    </Row>
                    <Row>
                        
                        <Col sm={4}><Button variant='primary' onClick={this.clearDisplay.bind(this)}>Clear</Button></Col>
                        <Col sm={4}><Button variant='success' onClick={this.goNext.bind(this)}>Next</Button></Col>
                    </Row>
                   
                </Col>
            </Row>
            </Card.Body></Card>
        </div>)
    }
}
export default EquationScreen;