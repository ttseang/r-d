import { CellGridOutputVo } from "../dataObjs/CellGridOutputVo";
import { CSSVo } from "../dataObjs/CSSVo";
import { GridOutputVo } from "../dataObjs/GridOutputVo";
import { SpanVo } from "../dataObjs/SpanVo";
import { CSSTypes } from "./Constants";

export class MainStorage {

  private static instance: MainStorage;

  public allSpans: SpanVo[] = [];
  public step: number = 0;
  public useRemainder: boolean = false;
  public zeroLimit: number = 2;
  public towerGrid: GridOutputVo[] = [];
  public towerGrid2: CellGridOutputVo[] = [];


  public gridOutput: GridOutputVo | null = null;
  public celloutput: CellGridOutputVo | null = null;

  public currentCssClasses:string[]=[];

  public cssMap: Map<CSSTypes, CSSVo[]> = new Map<CSSTypes, CSSVo[]>();

  constructor() {

    (window as any).ms = this;
    this.cssMap.set(CSSTypes.Color, []);
    this.cssMap.set(CSSTypes.Decor, []);
    this.cssMap.set(CSSTypes.Size, []);
  }
  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }

    return this.instance;
  }
  public addCssType(cssVo: CSSVo) {
    let cssArray: CSSVo[] | undefined = this.cssMap.get(cssVo.type);
    if (cssArray) {
      cssArray.push(cssVo);
      this.cssMap.set(cssVo.type, cssArray);
    }
  }
  public getCssByType(type: CSSTypes) {
    if (this.cssMap.has(type)) {
      return this.cssMap.get(type);
    }
    return [];
  }
  public getCssArray() {
    let cssArray: CSSVo[] = [];
    let typeArray: CSSTypes[] = [CSSTypes.Color, CSSTypes.Size, CSSTypes.Decor];
    let headers:string[]=["Colors","Sizes","Decoration"];
    

    for (let i: number = 0; i < typeArray.length; i++) {

      let cssVo: CSSVo[] | undefined = this.getCssByType(typeArray[i]);
      //console.log(cssVo);
      if (cssVo) {
        cssArray.push(new CSSVo(CSSTypes.Header,headers[i],""));

          for (let j:number=0;j<cssVo.length;j++)
          {
            cssArray.push(cssVo[j]);
          }
      }

    }
    return cssArray;
  }
 

}
export default MainStorage;
