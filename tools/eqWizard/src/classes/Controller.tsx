import { CSSVo } from "../dataObjs/CSSVo";
import { TowerPosition, ColPos, RowPos, SidePanels } from "./Constants";

export class Controller
{
    private static instance:Controller | null=null;

    public static getInstance()
    {
        if(this.instance===null)
        {
            this.instance=new Controller();
        }
        return this.instance;
    }

    public addColumn:Function=(part: TowerPosition, side: ColPos)=>{};
    public subColumn:Function=(part: TowerPosition, side: ColPos)=>{};
    public addRow:Function=(pos:RowPos)=>{};
    public subRow:Function=(pos:RowPos)=>{};
    public setSidePanel:Function=(panel:SidePanels)=>{};
    public setStyle:Function=(cssVo:CSSVo)=>{};
    public setSelectedCss:Function=(classList:string[])=>{};
}