
export enum ColPos {
    Left,
    Right
}
export enum RowPos {
    Top,
    Bottom
}
export enum TowerPosition {
    Top,
    Body,
    Answer
}
export enum CSSTypes {
    Color,
    Size,
    Decor,
    Header
}
export enum SidePanels
{
    None,
    Cols,
    Rows,
    CSS
}