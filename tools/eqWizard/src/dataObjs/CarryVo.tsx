import { NumVo } from "./NumVo";

export class CarryVo {
    public carrys: string[];
    public answer: number;

    private answerObj: NumVo;
    private answerDigits: number[];

    constructor(carry: string, answer: number) {
        this.carrys = carry.split("");
        this.answer = answer;
        this.answerObj = new NumVo(answer.toString());
        this.answerDigits = this.answerObj.digits.slice();
    }
    toHtmlStep(step: number): string[] {

        //////////console.log(this.answerObj);

        let cArray: string[] = [];
        let answerArray: number[] = [];

        cArray.push("<span class='hid'>0</span>");

        for (let i: number = 0; i < step; i++) {
            if (i / 2 !== Math.floor(i / 2)) {
                cArray.push(this.carrys.pop() || "0");
            }
            else {
                answerArray.push(this.answerDigits.pop() || 0);
            }
        }

        return [cArray.reverse().join(""), answerArray.reverse().join("")]
    }
}