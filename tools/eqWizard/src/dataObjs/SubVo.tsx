import { NumVo } from "./NumVo";

export class SubVo {

    public borrowArray: number[];
    public borrow2: number[] = [];

    public answer: number;
    private answerObj: NumVo;
    private answerDigits: number[];

    

    constructor(borrowArray: number[], answer: number) {
        this.borrowArray = borrowArray;
        this.borrow2 = borrowArray.slice();

        this.answer = answer;
        this.answerObj = new NumVo(answer.toString());
        this.answerDigits = this.answerObj.digits.slice();

        (window as any).subVo = this;
    }
    toHtmlStep(step: number): string[] {

        let bArray: string[] = [];
        let answerArray: number[] = [];

        // bArray.push("0");

        for (let i: number = 0; i < step; i++) {
            if (i / 2 === Math.floor(i / 2)) {

                let b: number | undefined = this.borrowArray.pop();

                if (b && b !== 0) {
                    bArray.push(b.toString());
                }
                else {
                    answerArray.push(this.answerDigits.pop() || 0);
                }
            }
            else {
                answerArray.push(this.answerDigits.pop() || 0);
            }
        }

        //////////console.log("borrow");
        //////////console.log(bArray);

        //////////console.log("answer");
        //////////console.log(this.answer);
        //////////console.log(answerArray);



        return [bArray.reverse().join(""), answerArray.reverse().join("")]
    }
}