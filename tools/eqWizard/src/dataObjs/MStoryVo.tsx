export class MStoryVo
{
    
    public answer:string;
    public classes:string[];

    constructor(answer:string="",classes:string[]=[])
    {
        this.answer=answer;
        this.classes=classes;
    }
    fromObj(obj:any)
    {
        this.answer=obj.answer;
        this.classes=obj.classes;
    }
    getOutput()
    {
        let answer:string=this.answer.replaceAll("@", "<span class='hid'>0</span>");

        if (this.classes.length>0)
        {
            
            return "<div class='"+this.classes.join(" ")+"'>"+answer+"</div>";
        }
        return "<div>"+answer+"</div>";
    }
}