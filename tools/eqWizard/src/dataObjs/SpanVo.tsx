export class SpanVo
{
    public row:number;
    public start:number;
    public end:number;
    public className:string;
    public step:number;

    constructor(step:number,row:number,start:number,end:number,className:string)
    {
        this.row=row;
        this.start=start;
        this.end=end;
        this.className=className;
        this.step=step;
    }
    applyStyles()
    {
        for (let i:number=this.start;i<this.end+1;i++)
        {
            let id:string=this.row+"_"+i;
            let spanObj:HTMLSpanElement | null=document.getElementById(id) as HTMLSpanElement;
            //////console.log(spanObj);

            if (spanObj)
            {
                //////console.log("apply "+this.className+" to "+id);
                
                spanObj.classList.add(this.className);
            }
        }
    }
}