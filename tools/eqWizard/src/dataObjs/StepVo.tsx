//import { exit } from "process";
import { PlacesUtil } from "../util/PlacesUtil";
import { RepeatChecker } from "../util/RepeatChecker";
import { DStoryVo } from "./DStoryVo";
import { MStoryVo } from "./MStoryVo";
import { NumVo } from "./NumVo";
import { SolutionVo } from "./SolutionVo";
import { SpanVo } from "./SpanVo";
import { StoryVo } from "./StoryVo";

export class StepVo {
    //public numObject1: NumVo;
    //public numObject2: NumVo;
    public totalSteps: number = 0;

    public zeroLimit: number = 2;

    public numberObjs: NumVo[] = [];

    private digits1: number[] = [];
    private digits2: number[] = [];
    private borrowArray: number[] = [];

    public story: StoryVo[] = [];
    public dStory: DStoryVo[] = [];
    public mStory:MStoryVo[]=[];
    public classes: string[] = [];

    public spanVo:SpanVo[]=[];
    

    constructor(numberObjs: NumVo[], zeroLimit: number = 2) {
        //this.numObject1 = numObject1;
        //this.numObject2 = numObject2;
        this.numberObjs = numberObjs;
        this.zeroLimit = zeroLimit;
        //
        //
        //
        //
       // this.classes.push("");
        (window as any).stepVo = this;
    }

    public doStepDiv(useRemain: boolean,editMode:boolean=false) {

        let story: DStoryVo[] = [];

        let dmax: number = PlacesUtil.findMaxDecByObj(this.numberObjs);
        let wmax: number = PlacesUtil.findMaxWholeByObj(this.numberObjs);

        this.numberObjs[0].fixDigits(wmax, dmax);
        this.numberObjs[1].fixDigits(wmax, dmax);

        this.digits1 = this.numberObjs[0].allDigits.reverse().slice();

        let divisor: number = this.numberObjs[1].val;


        let qs: number[] = [];

        let divs: number = 0;
        let divDigits: number[] = [];
        let solution: SolutionVo[] = [];

        let spaces: number = -1;
        let rowID:number=99;

        let addZeroCount: number = 0;

        while (this.digits1.length > 0) {

            rowID++;
            let safe: number = 0;
            solution = [];

            while (divs < divisor && safe < 10) {
                divDigits.push(this.digits1.pop() || 0);
                divs = parseFloat(divDigits.join(""));
                safe++;
            }
           // //////console.log("DIVS="+divs);

            let divNum:NumVo=new NumVo(divs.toString(),editMode);
            divNum.row=rowID;
          // //////console.log(divNum.format(2,2));
            spaces++;
            //  solution.push("<div class='subAnswer'>" + "<span class='hid'>0</span>".repeat(spaces) + divs.toString() + "</div>");
            //solution.push(new SolutionVo("@".repeat(spaces) + divs.toString(), "subAnswer"));

            solution.push(new SolutionVo("@".repeat(spaces) + divNum.format(spaces,2), "subAnswer"));
            //q for quanity - or the number that will be muliplied
            //by the divsor
            let q: number = Math.floor(divs / divisor);
            qs.push(q);

            //the difference between the lenght of the dividend
            //and the length of the divsior
            //let diff:number=divs.toString().length-divisor.toString().length;

            let a2: number = q * divisor;
            let a2Obj:NumVo=new NumVo(a2.toString(),editMode);
            a2Obj.row=-10*rowID;

            let bumpDif: number = divs.toString().length - a2.toString().length;

            spaces += bumpDif;
            if (spaces<0)
            {
                return [new DStoryVo([new SolutionVo("","")],"","","",[])];
            }
            // solution.push("<div id='q'>" + "<span class='hid'>0</span>".repeat(spaces) + a2.toString() + "</div>");
           // solution.push(new SolutionVo("@".repeat(spaces) + a2.toString(), "", "q"));
            solution.push(new SolutionVo("@".repeat(spaces) + a2Obj.format(2,2), "", "q"));
            spaces -= bumpDif;


            divs -= a2;
            divDigits = [divs];

            let a3: number = parseInt(qs.join(""));
            a3 /= Math.pow(10, addZeroCount);

         //   //////////console.log("ur=" + useRemain);

            if (useRemain === false) {
                //  solution.push("<div class='subAnswer'>" + "<span class='hid'>0</span>".repeat(spaces + 1) + divs.toString() + "</div>");

                solution.push(new SolutionVo("@".repeat(spaces + 1) + divs.toString(), "subAnswer"));

                story.push(new DStoryVo(solution, a3.toString(), this.numberObjs[0].num, ""));

                if (divs !== 0) {
                    if (addZeroCount < this.zeroLimit) {
                        this.numberObjs[0].addDecPlace();
                        this.digits1.unshift(0);
                        addZeroCount++;
                    }
                    else
                    {
                        //////////console.log("ZERO LIMIT");
                        let rc:RepeatChecker=new RepeatChecker();
                     //  let stat:number=
                       rc.fractionToDecimal(this.numberObjs[0].val,this.numberObjs[1].val);
                       ////////console.log(stat);
                       
                    }
                }
            }
            else {
                ////////////console.log("Remain "+divs);

                // solution.push("<div class='subAnswer'>" + "<span class='hid'>0</span>".repeat(spaces + 1) + divs.toString() + "</div>");
                solution.push(new SolutionVo("@".repeat(spaces + 1) + divs.toString(), "subAnswer"));
                story.push(new DStoryVo(solution, a3.toString(), this.numberObjs[0].num, divs.toString()));
            }
        }

        this.dStory = story;

        return story;
    }

    public doStepSub() {


        //let borrowString: string = "<span class='hid'>0</span>";
        // let borrowArray: number[] = [];

        let dmax: number = PlacesUtil.findMaxDecByObj(this.numberObjs);
        let wmax: number = PlacesUtil.findMaxWholeByObj(this.numberObjs);

        this.numberObjs[0].fixDigits(wmax, dmax);
        this.numberObjs[1].fixDigits(wmax, dmax);

        this.digits1 = this.numberObjs[0].allDigits.reverse();
        this.digits2 = this.numberObjs[1].allDigits.reverse();

        ////////////////console.log("DIGITS");
        ////////////////console.log(this.digits1);
        ////////////////console.log(this.digits2);
        ////////////////console.log("______")


     //   let borrowClasses:string[]=[''];

        //   let answer: number = this.numberObjs[0].val - this.numberObjs[1].val;

        this.totalSteps = Math.max(this.digits1.length, this.digits2.length);
        let resultsString: string = "";

        //////////////////////console.log("total step=" + this.totalSteps);

        let results: number[] = [];
        this.classes=[''];

      /*   for (let i: number = 1; i < this.totalSteps; i++) {
            let num1: number = this.digits1[i];
            let num2: number = this.digits2[i];

            ////console.log("compare",num1,num2);
            borrowClasses.unshift("strike3");

            this.numberObjs[0].classes=borrowClasses;
        } */
        //console.log(this.classes);

        for (let i: number = 0; i < this.totalSteps; i++) {
            let num1: number = this.digits1[i];
            let num2: number = this.digits2[i];

         
            let num3: number = this.digits1[i + 1];
            let num5: number = num3 - 1;

            ////////////////////console.log(num1 + "-" + num2);
            if (num1 === undefined) {
                num1 = 0;
            }
            if (num2 === undefined) {
                num2 = 0;
            }
            if (num5 === -1) {
                num5 = 9;
                //num1=9;
            }

            if (num1 < num2) {
                //console.log("borrow because " + num1 + " is less than " + num2 + " and add " + num5 + " to the top");
                //  this.borrowArray.unshift(num5);  
                this.borrowFromNext(i);

                num1 += 10;

                resultsString = results.join("");
                resultsString = this.fixDec(resultsString, dmax);


                this.story.push(new StoryVo(this.borrowArray.join(""), resultsString))

                let result = num1 - num2;
                results.unshift(result);

                resultsString = results.join("");
                resultsString = this.fixDec(resultsString, dmax);

                this.story.push(new StoryVo(this.borrowArray.join(""), resultsString))

            }
            else {
              //  this.classes.unshift("");

                if (num1 !== num2) {
                    //console.log("don't borrow because " + num1 + " is greater than " + num2);
                }
                else {
                    //console.log("don't borrow because " + num1 + " is the same as " + num2);
                }

                let result = num1 - num2;

                //    result=result/(Math.pow(10,maxDec));
                results.unshift(result);

                resultsString = results.join("");
                resultsString = this.fixDec(resultsString, dmax);
                this.story.push(new StoryVo(this.borrowArray.join(""), resultsString,this.classes))

                //this.borrowArray.unshift(0);

            }


            //  //////////////////console.log(num1 + "-" + num2+"="+result);
        }

        //  //////////////////console.log(this.borrowArray);

        //////////////////console.log(this.story);
        return this.story;

    }
    private borrowFromNext(index: number) {
        ////console.log("Borrow from Next");

        this.digits1[index + 1]--;
        //num1+=10;
   //     this.classes.unshift("strike3");
     //   //console.log(this.classes);

        if (this.digits1[index + 1] === -1) {
            this.digits1[index + 1] = 9;
            this.borrowArray.unshift(9);
            this.borrowFromNext(index + 1);

            //  borrowArray.unshift(9);
        }
        else {
            this.borrowArray.unshift(this.digits1[index + 1]);
        }
    }
    public doStepsAdd() {
        //  let answer: number = 0;

        let dmax: number = PlacesUtil.findMaxDecByObj(this.numberObjs);
        let wmax: number = PlacesUtil.findMaxWholeByObj(this.numberObjs);

        //////////////////console.log("dMAX=" + dmax);
        //////////////////console.log("wMAX=" + wmax);


        if (this.numberObjs.length < 2) {
            //  return ["Invalid", ""];
        }
        this.totalSteps = 0;

        for (let i: number = 0; i < this.numberObjs.length; i++) {

            this.numberObjs[i].fixDigits(wmax, dmax);
            //  answer += this.numberObjs[i].val;

            if (this.numberObjs[i].allDigits.length > this.totalSteps) {
                this.totalSteps = this.numberObjs[i].allDigits.length + 1;
            }
        }
        //  this.totalSteps=this.totalSteps*2;

        /**
         * 
         */
        let cols: number[][] = [];

        for (let i: number = 0; i < this.numberObjs.length; i++) {
            let digits: number[] = this.numberObjs[i].allDigits.reverse();

            for (let j: number = 0; j < digits.length; j++) {
                if (!cols[j]) {
                    cols[j] = [];
                }
                cols[j].push(digits[j]);
            }
        }
       
        /**
         * 
         */
        //find carrys

        //  let count: number = 0;
        let carryString: string = "@";

        ////////////////////console.log(cols);

        this.story = [];

        // let answerArray=
        let sumArray: number[] = [];
        let carryOver = 0;
        let sumString: string = "";

        for (let i: number = 0; i < cols.length; i++) {

            let col: number[] = cols[i];

            let sum: number = col.reduce((partialSum, a) => partialSum + a, 0);

            let c: number = Math.floor(sum / 10);
            let r: number = sum - (c * 10);
            //////////////////console.log(col);
            //////////////////console.log("carry="+c);

            // //////////////////console.log(i, sum, c, r + carryOver);

            sumArray.unshift(r + carryOver);

            sumString = sumArray.join("");
            sumString = this.fixDec(sumString, dmax);

            if (c === 0) {
                carryString = "@" + carryString;
            }
            this.story.push(new StoryVo(carryString, sumString,this.classes));

            carryOver = c;
            if (c > 0) {
                carryString = c.toString() + carryString;
                this.story.push(new StoryVo(carryString, sumString,this.classes));
            }
        }

        if (carryOver > 0) {
            sumArray.unshift(carryOver);
            //carryString = carryOver.toString() + carryString;
            this.story.push(new StoryVo(carryString, sumString,this.classes));
        }
       
        return this.story;

    }
    private fixDec(sumString: string, dmax: number) {
        if (dmax > 0) {
            
            let end: number = sumString.length - dmax;

            let wholeString: string = sumString.substring(0, end);
            let decString: string = sumString.substring(end, end + dmax);

            decString = PlacesUtil.addLeadingZeros(decString, dmax);
            sumString = wholeString + "." + decString;
            
        }
        return sumString;
    }
    public doStepsMultiply(step: number,editMode:boolean=false) {
       
        let answer: number = 0;
        let mStories: MStoryVo[] = [];

        if (this.numberObjs.length !== 2) {
            return [new MStoryVo("Invalid",[])];
        }

        let digits1: number[] = this.numberObjs[0].digits.reverse();
        let digits2: number[] = this.numberObjs[1].digits.reverse();

        this.totalSteps = digits1.length * digits2.length;

        //////////////////////console.log("TS="+totalSteps);

      //  let count: number = 0;

        for (let i: number = 0; i < digits1.length; i++) {
            for (let j: number = 0; j < digits2.length; j++) {
                let n1: number = digits1[i];
                let n2: number = digits2[j];

                //////////////////////console.log(n1.toString()+"*"+n2.toString());

                answer = n1 * n2;
                
                let answerObj:NumVo=new NumVo(answer.toString(),editMode);
                answerObj.row=j*1000+i;
                
                //mStories.push(new MStoryVo(answer.toString()  + "@".repeat(i + j),['tar']))
                mStories.push(new MStoryVo(answerObj.format(0,0) + "@".repeat(i + j),[]))
              //  count++;

                /* if (count === step) {
                    this.mStory=mStories;
                    return mStories;
                } */

            }
        }

        let product:number=this.numberObjs[0].val*this.numberObjs[1].val;

        mStories.push(new MStoryVo("@"+product.toString(),[]));

        this.mStory=mStories;

        return mStories;
        //answerString.push("calc");
       // return answerString;
    }
}