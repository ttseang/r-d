export class SolutionVo {
    public text: string;
    public cssClass: string;
    public id: string;

    constructor(text: string="", cssClass: string="", id: string = "") {
        this.text = text;
        this.cssClass = cssClass;
        this.id = id;
    }
    fromObj(obj:any)
    {
        this.text=obj.text;
        this.cssClass=obj.cssClass;
        this.id=obj.id;
    }
    getOutput() {
        let output: string = "<div>" + this.text + "</div>";

        if (this.id !== "" && this.cssClass !== "") {
            output = "<div id='" + this.id + "' class='" + this.cssClass + "'>" + this.text + "</div>";
        }
        else {
            if (this.id !== "") {
                output = "<div id='" + this.id + "'>" + this.text + "</div>";
            }
            if (this.cssClass !== "") {
                output = "<div class='" + this.cssClass + "'>" + this.text + "</div>";
            }
        }

        output=output.replaceAll("@","<span class='hid'>0</span>");
        return output;
    }
}