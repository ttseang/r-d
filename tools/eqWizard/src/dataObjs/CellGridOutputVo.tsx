import { CellVo } from "./CellVo";

export class CellGridOutputVo {
    public top: CellVo[];
    public rows: CellVo[][];
    public operator: string;
    public answer: CellVo[];

    constructor(top: CellVo[], rows: CellVo[][], answer: CellVo[], operator: string) {
        this.top = top;
        this.rows = rows;
        this.operator = operator;
        this.answer = answer;
    }
    clone()
    {
        
        let top2:CellVo[]=[];



        for (let i:number=0;i<this.top.length;i++)
        {
           
            
            top2.push(this.top[i].clone());
        }

        let rows2:CellVo[][]=[];

        for (let j:number=0;j<this.rows.length;j++)
        {
            let row:CellVo[]=this.rows[j];
            let row2:CellVo[]=[];

            for (let k:number=0;k<row.length;k++)
            {
                row2.push(row[k].clone());
            }
            rows2.push(row2);  
        }

        let answer2:CellVo[]=[];
        for (let m:number=0;m<this.answer.length;m++)
        {
            answer2.push(this.answer[m].clone());
        }

        let cellGrid:CellGridOutputVo=new CellGridOutputVo(top2,rows2,answer2,this.operator);
        return cellGrid;
    }
}