import { CSSTypes } from "../classes/Constants";
import MainStorage from "../classes/MainStorage";
import { ArrayUtil } from "../util/ArrayUtil";
import { CSSVo } from "./CSSVo";

export class CellVo
{
    //public text:string;
    public classArray:string[];
    private _text:string;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(text:string,classArray:string[])
    {
        this._text=text;
        this.classArray=classArray;
        this.autoClass();
    }
    public clone()
    {
        return new CellVo(this.text,this.classArray.slice());
    }
    public get text()
    {
        return this._text;
    }
    public set text(val:string)
    {
        this._text=val;
        this.autoClass();
    }
    private autoClass()
    {
        switch(this.text)
        {
            case "@":
                this.addClass("hid");
                break;

            case ".":
                this.addClass("decp");
            break;
            
            default:
                this.removeClass("hid");
        }
    }
   public setText(text:string)
    {
        this.text=text;
        this.autoClass();
    }
    addCss(cssVo:CSSVo)
    {
        if (this.classArray.includes(cssVo.value))
        {
            this.removeClass(cssVo.value);
            return;
        }
         if (cssVo.type===CSSTypes.Color)
        {           
            this.removeAllType(CSSTypes.Color);
        }
        if (cssVo.type===CSSTypes.Size)
        {
           this.removeAllType(CSSTypes.Size);
        }
        this.addClass(cssVo.value);
        //console.log(this);
    }
    private removeAllType(type:CSSTypes)
    {
        let cssClasses:CSSVo[] | undefined=this.ms.getCssByType(type);
            if (cssClasses)
            {
                for (let i:number=0;i<cssClasses.length;i++)
                {
                    //console.log("remove "+cssClasses[i].value);
                    this.removeClass(cssClasses[i].value);
                }
            }
    }
    addClass(classString:string)
    {
        //console.log(this.classArray);

        if (!this.classArray.includes(classString))
        {
            //console.log("add "+classString);
            this.classArray.push(classString);
        }
        this.classArray=ArrayUtil.makeUniqueValues(this.classArray);
    }
    removeClass(classString:string)
    {
       // this.classArray=ArrayUtil.makeUniqueValues(this.classArray);
        
        let index:number=-1;
        for (let i:number=0;i<this.classArray.length;i++)
        {
            if (this.classArray[i]===classString)
            {
                index=i;
            }
        }
        if (index>-1)
        {
            this.classArray.splice(index,1);
        }
       
        ////console.log(this.classArray);

    }
}