import { CellGridOutputVo } from "../dataObjs/CellGridOutputVo";
import { CellVo } from "../dataObjs/CellVo";
import { GridOutputVo } from "../dataObjs/GridOutputVo";
import { NumVo } from "../dataObjs/NumVo";
import { StoryUtil } from "../util/StoryUtil";


export class TowerDisplay {
    public nums: string[];
    public operator: string;

    public answer: string;
    public answerObj: NumVo | null = null;

    public numberObjs: NumVo[] = [];

    public maxDecs: number = 0;
    public maxWhole: number = 0;

    public top: string;
    public topClass: string = "";
    public plainTop:string="";

    public classes: string[][] = [];

    public editMode: boolean;



    constructor(nums: string[], operator: string, answer: string = "", top: string = "", classes: string[][] = [], editMode: boolean = false) {
        this.nums = nums;
        this.operator = operator;
        this.answer = answer;

        this.classes = classes;

        this.editMode = editMode;

        ////////console.log("tower edit mode "+this.editMode);

        this.top = top;

        if (answer !== "") {
            //////console.log("editMode="+editMode);
            this.answerObj = new NumVo(answer, editMode);
        }

        for (let i: number = 0; i < nums.length; i++) {
            /**
             * make number object
             */
            let numObj: NumVo = new NumVo(nums[i], this.editMode);

            numObj.row = i;
            if (this.classes[i]) {
                numObj.classes = this.classes[i];
            }

            //    this.setSpans(i,numObj);

            /**
             * find max number of whole digits
             */
            if (numObj.wholeLen > this.maxWhole) {
                this.maxWhole = numObj.wholeLen;
            }
            /**
             * find max number of decimal digits
             */
            if (numObj.decLen > this.maxDecs) {
                this.maxDecs = numObj.decLen;
            }
            /**
             * 
             */
            this.numberObjs.push(numObj);
        }
        if (this.answerObj) {
            //////////console.log("ANSWER OBJ");
            //////////console.log(this.answerObj);


            if (this.answerObj.wholeLen > this.maxWhole) {
                this.maxWhole = this.answerObj.wholeLen;
            }
            if (this.answerObj.decLen > this.maxDecs) {
                this.maxDecs = this.answerObj.decLen;
            }

            /* if (this.answerObj.whole===0)
            {
                this.answerObj.getTrimmedDec();
            } */
        }
        (window as any).tower = this;
    }

    public toHtml() {
        let output: string[] = [];

        if (this.top !== "") {
            let top2 = this.top.replaceAll("@", "<span class='hid'>0</span>");

            if (this.topClass === "") {
                output.push(top2);
            }
            else {
                output.push("<div class='" + this.topClass + "'>" + top2 + "</div>");
            }
        }

        output.push("<div id='mathGrid'>");

        for (let i: number = 0; i < this.numberObjs.length; i++) {
            let numObj: NumVo = this.numberObjs[i];
            //     ////console.log(numObj);

            let formated: string = numObj.format(this.maxWhole, this.maxDecs, false);
            //    ////console.log(formated);

            if (i < this.numberObjs.length - 1) {
                output.push("<div></div><div>" + formated + "</div>");
            }
            else {
                let symbol: string = this.operator;
                symbol = (symbol === "*") ? "\u00D7" : symbol;
                output.push("<div>" + symbol + "</div><div>" + formated + "</div>");
            }
        }

        output.push("</div>");
        if (this.answerObj) {

            this.answerObj.row = -1;
            //////console.log("answer obj edit mode="+this.answerObj.editMode);

            let answerString: string = this.answerObj.format(this.maxWhole, this.maxDecs, true, true);
            //////////console.log("answerString=" + answerString);

            output.push(answerString);
          //  output.push("<div id='answerGrid'><div></div><div>" + answerString + "</div></div>");
        }
        //////////console.log(output);

        return output.join("");
    }
    public toGrid() {
        let rows: string[] = [];
        for (let i: number = 0; i < this.numberObjs.length; i++) {
            let numObj: NumVo = this.numberObjs[i];

            let formated: string = numObj.formatGridDigits(this.maxWhole, this.maxDecs, false);

            rows.push(formated);

        }

        let answerString: string = "";

        if (this.answerObj) {
            ////console.log(this.answerObj);

            answerString = this.answerObj.formatGridDigits(this.maxWhole, this.maxDecs, false);
            
        }
        //////////console.log(output);

        let topNum:NumVo=new NumVo(this.plainTop,false);
        let topString:string="";
        if (isNaN(parseFloat(this.plainTop)))
        {
            topNum=new NumVo("0",false);
            //topString="@".repeat(10);
        }
        else
        {
            topString=topNum.formatGridDigits(this.maxWhole,this.maxDecs,false);
        }
        ////console.log("plainTop="+this.plainTop);
        
        ////console.log(topString);

        /* if (this.plainTop==="@")
        {
            topString="";
        } */        

        return new GridOutputVo(topString, rows, this.operator, answerString,this.classes);
    }
    public toGrid2() {
        let rows: CellVo[][] = [];
        for (let i: number = 0; i < this.numberObjs.length; i++) {
            let numObj: NumVo = this.numberObjs[i];

            let formated: string = numObj.formatGridDigits(this.maxWhole, this.maxDecs, false);

            let myClasses:string[]=[];
            if (this.classes[i])
            {
                myClasses=this.classes[i];
            }
            let rowCells:CellVo[]=StoryUtil.stringToCells(formated,myClasses);

            rows.push(rowCells);
        }

        let answerString: string = "";

        if (this.answerObj) {
            ////console.log(this.answerObj);

            answerString = this.answerObj.formatGridDigits(this.maxWhole, this.maxDecs, false);
            
            
        }
        //////////console.log(output);

        let topNum:NumVo=new NumVo(this.plainTop,false);
        let topString:string="";
        if (isNaN(parseFloat(this.plainTop)))
        {
            topNum=new NumVo("0",false);
            //topString="@".repeat(10);
        }
        else
        {
            topString=topNum.formatGridDigits(this.maxWhole,this.maxDecs,false);
        }
        let topClasses:string[]='carryString|'.repeat(topString.length).split("|");
        topClasses.pop();

        let answerCells:CellVo[]=StoryUtil.stringToCells(answerString,[]);

        let topArray:CellVo[]=StoryUtil.stringToCells(topString,topClasses);

        return new CellGridOutputVo(topArray,rows,answerCells,this.operator);
    }
}