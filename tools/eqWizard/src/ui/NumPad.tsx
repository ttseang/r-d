import React, { Component } from 'react';
import { Col, Button, Card, Row } from 'react-bootstrap';
interface MyProps {callback:Function }
interface MyState { }
class NumPad extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getButtons() {
        let numArray: string[] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "=", "0", "."];
        let buttons: JSX.Element[] = [];
        for (let i: number = 0; i < numArray.length; i++) {
            let key: string = "numpad" + i.toString();
            buttons.push(<Col key={key} sm={4} className="buttonCol"><Button size="lg" variant='light' onClick={()=>{this.props.callback(numArray[i])}}>{numArray[i]}</Button></Col>)
        }
        return (<Row>{buttons}</Row>);
    }
    render() {
        return (<Card><Card.Body>{this.getButtons()}</Card.Body></Card>)
    }
}
export default NumPad;