import { DStoryVo } from "../dataObjs/DStoryVo";
import { NumVo } from "../dataObjs/NumVo";
import { StepVo } from "../dataObjs/StepVo";
import { DivideDisplay } from "./DivideDisplay";

export class DivideStepDisplay
{
    
    public stepVo:StepVo | null=null;
    public zeroLimit:number;
    public useRemain:boolean;
    public editMode:boolean;

    constructor(zeroLimit:number=2,useRemain:boolean=false,editMode:boolean=false)
    {
        this.zeroLimit=zeroLimit;
        this.useRemain=useRemain;
        this.editMode=editMode;
    }
    getStepDisplay(nums: string[], stories: DStoryVo[], step: number, classes: string[] = [])
    {
                
        //////////console.log("NUMS="+nums);
        
        let storyIndex: number = 0;

        let answer: string = "";

        let solutionOutput: string[] = [];

        let divs: string = "";
        let remain: string = "";
        let remain2: string = "";

        let subStep: number = 1;

        for (let i: number = 0; i < step; i++) {
            let story: DStoryVo = stories[storyIndex];

            if (story) {
                divs = story.divs;
                remain = story.remain;
                ////////////console.log("ss="+subStep);

                switch (subStep) {
                    case 0:
                        ////////////console.log("Bring Down");

                        solutionOutput.pop();
                        solutionOutput.pop();

                        solutionOutput.push("<div></div>");
                        solutionOutput.push(story.solution[0].getOutput());

                        //bring down digit
                        break;

                    case 1:
                        //place answer
                        answer = story.answer;
                        ////////////console.log("place answer");

                        break;

                    case 2:
                        //write product
                        ////////////console.log("write product");
                        solutionOutput.push("<div></div>");
                        solutionOutput.push(story.solution[1].getOutput());

                        break;

                    case 3:
                        //write difference
                        ////////////console.log("write difference");
                        solutionOutput.push("<div></div>");
                        solutionOutput.push(story.solution[2].getOutput());
                }

                subStep++;
                if (subStep === 4) {
                    subStep = 0;
                    storyIndex++;
                }
            }
            else {
                remain2 = remain;
            }
        }

        let dividend: string = nums[0] || "?";
        let divisor: string = nums[1] || "?";

        if (divs !== "") {
            dividend = divs;
        }

        let answerObj: NumVo = new NumVo(answer);

        let divGrid: DivideDisplay = new DivideDisplay(new NumVo(divisor), new NumVo(dividend), answerObj, solutionOutput, remain2,this.editMode);

        return divGrid.getOutput();
    }
}