import { FractStepsVo } from "../dataObjs/FractStepsVo";
import { FractVo } from "../dataObjs/FractVo";

export class FractDisplay {
    private story: FractStepsVo;
    private ops:string[];
    private answer:FractVo | null;


    constructor(story: FractStepsVo,ops:string[],answer:FractVo | null) {
        this.story = story;
        this.ops=ops;
        this.answer=answer;
        //////console.log(ops);
    }
    getHtml()
    {
        let html:string="";
        if (this.ops.length===0 || this.story.fracts.length===0)
        {
            return "Invalid";
        }
        ////console.log(this.story);
        ////console.log(this.ops);

        for (let i:number=0;i<this.story.fracts.length;i++)
        {
            let fractVo:FractVo=this.story.fracts[i];
            //////console.log(fractVo);

            if (fractVo.top===undefined || fractVo.bottom===undefined)
            {
                return "Invalid";
            }
           
            html+=fractVo.toHtml();
            if (this.ops[i] && i<this.story.fracts.length-1)
            {
                html+="<span class='fop'>"+this.ops[i]+"</span>";
            }         

        }
        if (this.answer)
        {
            html+="<span class='fop'>=</span>"+this.answer.toHtml();
        }
        return html;
    }
}