import { GridOutputVo } from "../dataObjs/GridOutputVo";

export class TowerGridDisplay {
    private output: GridOutputVo[];
    private rows:string[][]=[];

    constructor(output: GridOutputVo[]) {
        this.output = output;

       
    }
    
    renderGrid(step: number) {
        if (step>this.output.length-1)
        {
            return "<div id='mathGrid2'><div></div><div><strong>Invalid</strong></div></div>";
        }
        let grid: GridOutputVo = this.output[step];

        
        let html: string = "<div>";
       
        html+="<div id='carryString'>";
        html+="<div></div>";

        let topString:string=this.makeCols(grid.top);

        if (grid.top==="")
        {
            topString="<span class='hid'>0</span>".repeat(4);
            topString="<div>"+topString+"</div>";
        }
        
        html +=  topString;
        html+="</div>";

        html+="<div id='mathGrid'>";
        for (let i: number = 0; i < grid.rows.length; i++) {
            html+="<div></div>";
            html += this.makeCols(grid.rows[i]);
        }
        html+="</div>";

        html+="<div id='answerGrid2'>";
        html+="<div></div>";
        html += "<div>" + this.makeCols(grid.answer) + "</div>";
        html += "</div>"

        html += "</div>"
        html += "</div>";
        return html;
    }
    makeCols(colString: string,idName:string="") {
        let rowHtml: string = "";
        let cols: string[] = colString.split("");
       
        if (idName!=="")
        {
            rowHtml += "<div id="+idName+">"
        }
        else
        {
            rowHtml += "<div>"
        }
        for (let j: number = 0; j < cols.length; j++) {
            let char: string = cols[j];
            switch (char) {
                 case "@":
                    rowHtml += "<span class='hid'>0</span>";
                    break;

                case ".":
                    rowHtml += "<span class='decp'>.</span>";
                    break;

                default:

                    rowHtml += "<span>" + char + "</span>";

            }

        }
        rowHtml += "</div>";
        return rowHtml;
    }

}