import MainStorage from "../classes/MainStorage";
import { MStoryVo } from "../dataObjs/MStoryVo";
import { StoryVo } from "../dataObjs/StoryVo";
import { TowerDisplay } from "./TowerDisplay";

export class TowerStepDisplay {
    public expression: string;
    public editMode:boolean;
    private ms:MainStorage=MainStorage.getInstance();

    constructor(expression: string,editMode:boolean=false) {
        this.expression = expression;
        this.editMode=editMode;
    }

    public displayAdd(nums: string[], stories: StoryVo[], step: number) {

        if (step < 1) {
            let td2: TowerDisplay = new TowerDisplay(nums, "+", "","",[],this.editMode);
            this.ms.celloutput=td2.toGrid2();
            return td2.toHtml();
        }
        let story: StoryVo = stories[step - 1];


        if (step > stories.length) {
            return "Invalid";
        }


        let td: TowerDisplay = new TowerDisplay(nums, "+", story.answer, "<div id='carryString'><div></div><div>" + story.top + "</div></div>", [story.classes],this.editMode);
        td.plainTop=story.top;

     //   this.ms.gridOutput=td.toGrid();
        this.ms.celloutput=td.toGrid2();

        return td.toHtml();
    }
    public displaySub(nums: string[], stories: StoryVo[], step: number, classes: string[] = []) {

        let story: StoryVo = stories[step - 1];

        if (step > stories.length) {
            return "Invalid";
        }
        if (step < 1) {
            let td2: TowerDisplay = new TowerDisplay(nums, "-", "","",[],this.editMode);
            this.ms.celloutput=td2.toGrid2();
            return td2.toHtml();
        }

        let answerString: string = story.answer;
        
        let td: TowerDisplay = new TowerDisplay(nums, "-", answerString, "<div id='borrow'>" + story.top + "<span class='hid'>0</span></div>", [story.classes],this.editMode);
        td.plainTop=story.top;
        this.ms.celloutput=td.toGrid2();
        return td.toHtml();
    }
    public getMultiStep(nums: string[], stories: MStoryVo[], step: number, classes: string[] = []) {
        
        let output: string[] = [];

        for (let i: number = 0; i < step; i++) {
            if (stories[i]) {
                output.push(stories[i].getOutput());
            }
        }
        let td: TowerDisplay = new TowerDisplay(nums, "*",output.join(""),"",[],this.editMode);
        return td.toHtml();

    }
}