import React, { Component } from 'react';
import { Col, Button, Card, Row } from 'react-bootstrap';
interface MyProps {callback:Function}
interface MyState { }
class OpPad extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state={};
        }
    getButtons() {
        let numArray: string[] = ["+", "-", "*", "/","=calc"];
        let buttons: JSX.Element[] = [];
        for (let i: number = 0; i < numArray.length; i++) {
            let key: string = "OpPad" + i.toString();
            buttons.push(<Col key={key} sm={3} className="buttonCol"><Button size="lg" variant='light' onClick={()=>{this.props.callback(numArray[i])}}>{numArray[i]}</Button></Col>)
        }
        return (<Row>{buttons}</Row>);
    }
    render() {
        return (<Card><Card.Body>{this.getButtons()}</Card.Body></Card>)
    }
}
export default OpPad;