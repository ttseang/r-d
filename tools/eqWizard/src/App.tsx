import React from 'react';
import './App.css';
import './MathStyles.css';
//import 'bootstrap/dist/css/bootstrap.min.css';
import './sketchy-bootstrap.min.css';
//import './journal-bootstrap.min.css';
//import "./morph-bootstrap.min.css";
import BaseScreen from './screens/BaseScreen';
function App() {
  return (
    <div id="base"><BaseScreen></BaseScreen></div>
  );
}
export default App;