export class EqEngine2
{
    private expression: string;
    private orignal: string;

    constructor(expression:string)
    {
        this.expression = expression;
        this.orignal = expression;
    }

    public getSteps()
    {
        let add: boolean = this.expression.includes("+");
        let sub: boolean = this.expression.includes("-");
        let mul: boolean = this.expression.includes("*");
        let division: boolean = this.expression.includes("/");

        let c: number = ((add === true) ? 1 : 0) + ((sub === true) ? 1 : 0) + ((mul === true) ? 1 : 0) + ((division === true) ? 1 : 0);

        if (c !== 1) {

            return "Not Supported (YET!)";
        }
        

    }
}