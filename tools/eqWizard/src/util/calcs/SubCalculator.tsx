import { CellGridOutputVo } from "../../dataObjs/CellGridOutputVo";
import { CellVo } from "../../dataObjs/CellVo";
import { NumVo } from "../../dataObjs/NumVo";
import { GridUtil } from "../GridUtil";
import { PlacesUtil } from "../PlacesUtil";

export class SubCalculator {
    private digits1: number[] = [];
    private digits2: number[] = [];
    private borrowArray: number[] = [];
    private borrowClasses: string[] = [];
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        (window as any).scalc = this;
    }
    public doSub(expression: string) {
        let steps: CellGridOutputVo[] = [];


        this.borrowArray = [];

        let nums: string[] = expression.split("-");

        let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);

        let maxWhole: number = PlacesUtil.findMaxWhole(nums);
        let maxDec: number = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            numsObj[i].formatDigits(maxWhole, maxDec);
        }

        let dmax: number = PlacesUtil.findMaxDecByObj(numsObj);
        let wmax: number = PlacesUtil.findMaxWholeByObj(numsObj);

        numsObj[0].fixDigits(wmax, dmax);
        numsObj[1].fixDigits(wmax, dmax);

        this.digits1 = numsObj[0].allDigits.reverse();
        this.digits2 = numsObj[1].allDigits.reverse();

        /**
         * Initial
         */
        let row1: CellVo[] = GridUtil.arrayToCells(this.digits1, []);
        let row2: CellVo[] = GridUtil.arrayToCells(this.digits2, []);
        steps.push(new CellGridOutputVo([], [row1,row2], [], "-"));

        let totalSteps: number = Math.max(this.digits1.length, this.digits2.length);
        let results: number[] = [];

        for (let i: number = 0; i < totalSteps; i++) {
            let num1: number = this.digits1[i];
            let num2: number = this.digits2[i];


            let num3: number = this.digits1[i + 1];
            let num5: number = num3 - 1;

            ////////////////////console.log(num1 + "-" + num2);
            if (num1 === undefined) {
                num1 = 0;
            }
            if (num2 === undefined) {
                num2 = 0;
            }
            if (num5 === -1) {
                num5 = 9;
                //num1=9;
            }

            if (num1 < num2) {
                //console.log("borrow because " + num1 + " is less than " + num2 + " and add " + num5 + " to the top");
               
                this.borrowFromNext(i);

                num1 += 10;
                let result: number = num1 - num2;
                results.unshift(result);

            }
            else {
               
                if (num1 !== num2) {
                    //console.log("don't borrow because " + num1 + " is greater than " + num2);
                }
                else {
                    //console.log("don't borrow because " + num1 + " is the same as " + num2);
                }

                let result: number = num1 - num2;


                results.unshift(result);


            }

            let top: CellVo[] = GridUtil.arrayToCells(this.borrowArray, [])



            let body1: CellVo[] = GridUtil.arrayToCells(this.digits1, []);
            let body2: CellVo[] = GridUtil.arrayToCells(this.digits2, []);

            let answers: CellVo[] = GridUtil.arrayToCells(results, []);

            let gridOutput: CellGridOutputVo = new CellGridOutputVo(top, [body1, body2], answers, "-");
       
            steps.push(gridOutput);
           
        }
        
        return steps;
    }

    private borrowFromNext(index: number) {
           this.digits1[index + 1]--;

        if (this.digits1[index + 1] === -1) {
            this.digits1[index + 1] = 9;
            this.borrowArray.unshift(9);
            this.borrowFromNext(index + 1);
        }
        else {
            this.borrowArray.unshift(this.digits1[index + 1]);
        }
    }
}