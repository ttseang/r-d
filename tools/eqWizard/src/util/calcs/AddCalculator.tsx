import { CellGridOutputVo } from "../../dataObjs/CellGridOutputVo";
import { CellVo } from "../../dataObjs/CellVo";
import { NumVo } from "../../dataObjs/NumVo";
import { GridUtil } from "../GridUtil";
import { PlacesUtil } from "../PlacesUtil";

export class AddCalculator {
   
    private totalSteps: number = 0;
    private numsObj:NumVo[]=[];
    private carryArray: number[] = [-1];
    private sumArray: number[] = [];
    private steps: CellGridOutputVo[] = [];

    constructor() {
        (window as any).addcalc = this;
    }
    public doAdd(expression: string) {

        
        
        let nums: string[] = expression.split("+");

        this.numsObj = PlacesUtil.makeNumberObjs(nums);

        let maxWhole: number = PlacesUtil.findMaxWhole(nums);
        let maxDec: number = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            this.numsObj[i].formatDigits(maxWhole, maxDec);
        }

        let dmax: number = PlacesUtil.findMaxDecByObj(this.numsObj);
        let wmax: number = PlacesUtil.findMaxWholeByObj(this.numsObj);


        /**
         * Initial
         */
        /* let row1: CellVo[] = GridUtil.arrayToCells(this.digits1, []);
        let row2: CellVo[] = GridUtil.arrayToCells(this.digits2, []);
        steps.push(new CellGridOutputVo([], [row1,row2], [], "-")); */

        this.totalSteps = 0;

        for (let i: number = 0; i < this.numsObj.length; i++) {

            this.numsObj[i].fixDigits(wmax, dmax);

            if (this.numsObj[i].allDigits.length > this.totalSteps) {
                this.totalSteps = this.numsObj[i].allDigits.length + 1;
            }
        }

        let cols: number[][] = [];

        for (let i: number = 0; i < this.numsObj.length; i++) {
            let digits: number[] = this.numsObj[i].allDigits.reverse();
            //console.log(digits);

            for (let j: number = 0; j < digits.length; j++) {
                //  console.log(digits[j]);
                if (!cols[j]) {
                    cols[j] = [];
                }
                cols[j].push(digits[j]);
            }
        }

        let carryOver: number = 0;
        
       
        this.sumArray=[];

        for (let i: number = 0; i < cols.length; i++) {

            let col: number[] = cols[i];

            let sum: number = col.reduce((partialSum, a) => partialSum + a, 0);
         //   console.log(sum);


            let c: number = Math.floor(sum / 10);
            let r: number = sum - (c * 10);

            if (c === 0) {
                this.carryArray.unshift(-1);
            }
            else {
                this.carryArray.unshift(c);
            }

            //console.log(c, r);

            this.sumArray.unshift(r + carryOver);

            carryOver = c;
            this.addStep();
        }
        if (carryOver > 0) {
            this.sumArray.unshift(carryOver);
        }
        this.addStep();
             

       /*  console.log(body);
        console.log(sumArray);
        console.log(top);
 */
        //console.log(this.steps);
        return this.steps;
    }
    addStep()
    {
        this.steps.push(new CellGridOutputVo(this.getTop(),this.getBody(),this.getAnswer(),"+"));
    }
    getAnswer()
    {
        let answer:CellVo[]=[];
        for (let i:number=0;i<this.sumArray.length;i++)
        {
            answer.unshift(new CellVo(this.sumArray[i].toString(),[]));
        }
        return answer;
    }
    getTop()
    {
        let top:CellVo[]=[];

        for (let i:number=0;i<this.carryArray.length;i++)
        {
            let val:number=this.carryArray[i];
            let char:string=val.toString();

            if (val===-1)
            {
                char="@";
            }

            top.push(new CellVo(char,[]));
        }
        return top;
    }
    getBody()
    {
        let body: CellVo[][] = [];

        for (let i: number = 0; i < this.numsObj.length; i++) {
            let digits:number[]=this.numsObj[i].allDigits.reverse();
            console.log(digits);

            body.push(GridUtil.arrayToCells(this.numsObj[i].allDigits.reverse(), []));
        }
        return body;
    }
}