import { CellGridOutputVo } from "../../dataObjs/CellGridOutputVo";
import { CellVo } from "../../dataObjs/CellVo";
import { NumVo } from "../../dataObjs/NumVo";
import { GridUtil } from "../GridUtil";
import { PlacesUtil } from "../PlacesUtil";

export class MulCalculator
{
    private totalSteps: number = 0;
    private numsObj:NumVo[]=[];
    
    private body2:CellVo[][]=[];

    private steps: CellGridOutputVo[] = [];
    constructor()
    {
        (window as any).mcalc = this;
    }
    public doStepMul(expression:string)
    {
        let answer: number = 0;
        
        let nums: string[] = expression.split("*");

        this.numsObj = PlacesUtil.makeNumberObjs(nums);

        let maxWhole: number = PlacesUtil.findMaxWhole(nums);
        let maxDec: number = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            this.numsObj[i].formatDigits(maxWhole, maxDec);
        }

        
       

        let digits1: number[] = this.numsObj[0].digits.reverse();
        let digits2: number[] = this.numsObj[1].digits.reverse();

        this.body2.push(GridUtil.arrayToCells(digits1,[]));
        this.body2.push(GridUtil.arrayToCells(digits2,[]));

        this.totalSteps = digits1.length * digits2.length;

        let vals:number[]=[];
        let sum:number=0;

        for (let i: number = 0; i < digits1.length; i++) {
            sum=0;

            for (let j: number = 0; j < digits2.length; j++) {
                let n1: number = digits1[j];
                let n2: number = digits2[i];

               // console.log(n1.toString()+"*"+n2.toString());

                answer = n1 * n2;
                sum+=answer;
                vals.push(answer);

                this.addToBody(vals,sum);
                console.log(vals,sum);
               // console.log(answer);

              //  let answerObj:NumVo=new NumVo(answer.toString());

                this.steps.push(new CellGridOutputVo([],this.body2.slice(),[],"*"))
            }
            

            
        }

        let product:number=this.numsObj[0].val*this.numsObj[1].val;
        let productObj:NumVo=new NumVo(product.toString());

        this.steps.push(new CellGridOutputVo([],this.body2.slice(),GridUtil.arrayToCells(productObj.allDigits.reverse(),[]),"*"));

      //  console.log(product);

       // console.log(this.body2);
        return this.steps;
        
    }
    private addToBody(vals:number[],sum:number)
    {
        let valObjs:NumVo[]=PlacesUtil.makeNumberObjs2(vals);
        let sumObj:NumVo=new NumVo(sum.toString());
      

        for (let i:number=0;i<valObjs.length;i++)
        {
            let digits:number[]=valObjs[i].allDigits.reverse();
            this.body2.push(GridUtil.arrayToCells(digits,[]));
        }
        
        let sumDigits:number[]=sumObj.allDigits.reverse();
        let cArray:string[]=[];
        for (let j:number=0;j<sumDigits.length;j++)
        {
            cArray.push("answer");
        }
        this.body2.push(GridUtil.arrayToCells(sumDigits,cArray));
    }
}