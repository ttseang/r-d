import { CellVo } from "../dataObjs/CellVo";

export class GridUtil
{
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor()
    {
        
    }
    public static arrayToCells(nums:number[],classArray:string[])
    {
        let cells:CellVo[]=[];

        for (let i:number=0;i<nums.length;i++)
        {
            let className:string =classArray[i] || "";

            cells.push(new CellVo(nums[i].toString(),[className]));
            
        }
        return cells;
    }
}