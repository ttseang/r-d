import { CellVo } from "../dataObjs/CellVo";
import { DStoryVo } from "../dataObjs/DStoryVo";
import { NumVo } from "../dataObjs/NumVo";
import { StepVo } from "../dataObjs/StepVo";
import { StoryVo } from "../dataObjs/StoryVo";
import { PlacesUtil } from "./PlacesUtil";

export class StoryUtil {

    public static getSubStory(expression: string) {
        let nums: string[] = expression.split("-");
        let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);

        let maxWhole: number = PlacesUtil.findMaxWhole(nums);
        let maxDec: number = PlacesUtil.findMaxDec(nums);

        for (let i: number = 0; i < nums.length; i++) {
            numsObj[i].formatDigits(maxWhole, maxDec);
        }

        let stepVo: StepVo = new StepVo(numsObj);

        let stories: StoryVo[] = stepVo.doStepSub();
        return stories;
    }
    public static getDivStory(expression: string, zeroLimit: number, useRemain: boolean,editMode:boolean=false) {
        let nums: string[] = expression.split("/");

        let numsObj: NumVo[] = PlacesUtil.makeNumberObjs(nums);

        let stepVo: StepVo = new StepVo(numsObj, zeroLimit);

        let stories: DStoryVo[] = stepVo.doStepDiv(useRemain,editMode);
        return stories;
    }
    public static stringToCells(str:string,classArray:string[])
    {
        let strArray:string[]=str.split("");
        let cells:CellVo[]=[];

        for (let i:number=0;i<strArray.length;i++)
        {
            let cellClass:string=classArray[i] || "";

           
            let cell:CellVo=new CellVo(strArray[i],[cellClass]);
            if (classArray===[])
            {
                cell=new CellVo(strArray[i],[]);
            }
            if (cell.text==="@")
            {
                cell.classArray.push("hid");
            }
            cells.push(cell);
        }
        return cells;
    }
}