export class ArrayUtil
{
    public static makeUniqueValues(array:any[])
    {
            const result:any[] = [];
            for (const item of array) {
                if (!result.includes(item)) {
                    result.push(item);
                }
            }
            return result;        
    }
}