import React, { Component } from 'react';
import { CellVo } from '../dataObjs/CellVo';
interface MyProps { top: CellVo[], rows: CellVo[][], answer: CellVo[], fontSize: number, operator: string }
interface MyState { top: CellVo[], rows: CellVo[][], answer: CellVo[] }
class RTower extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { top: this.props.top, rows: this.props.rows, answer: this.props.answer };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ top: this.props.top, rows: this.props.rows, answer: this.props.answer });
        }
    }
    makeCols(index: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];
        let key: string = "";

        for (let j: number = 0; j < cells.length; j++) {
            let char: string = cells[j].text;
            let html: JSX.Element | null = null;

            key = "span_" + index.toString() + "_" + j.toString();

            if (cells[j].classArray.length === 0) {
                html = (<span key={key}>{char}</span>)
            }
            else {
                html = (<span key={key} className={cells[j].classArray.join(" ")}>{char}</span>)
            }
            cols.push(html);
        }

        return <span>{cols}</span>
    }
    getRows(): JSX.Element[] {
        let rows: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.rows.length; i++) {
            let rowKey: string = "mrow" + i.toString();
            let bKey: string = "blank" + i.toString();
            if (i === this.state.rows.length - 1) {
                rows.push(<div key={bKey}>{this.props.operator}</div>)
            }
            else {
                rows.push(<div key={bKey}></div>)
            }

            rows.push(<div key={rowKey}>{this.makeCols(i, this.state.rows[i])}</div>);
        }
        return (rows)
    }
    render() {
        return (<div id="mathDisplayRel" style={{ fontSize: this.props.fontSize.toString() + "px" }} className="equation">
            <div id="carryString2">
                <div></div>
                {this.makeCols(0, this.state.top)}
            </div>
            <div id="mathGrid">
                {this.getRows()}
            </div>
            <div id="answerGrid2">
                <div></div>
                {this.makeCols(20, this.state.answer)}
            </div>
        </div>)
    }
}
export default RTower;