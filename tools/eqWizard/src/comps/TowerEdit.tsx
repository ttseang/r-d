import React, { ChangeEvent, Component } from 'react';
import { ColPos, RowPos, TowerPosition } from '../classes/Constants';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';

import { CellGridOutputVo } from '../dataObjs/CellGridOutputVo';
import { CellVo } from '../dataObjs/CellVo';
import { CSSVo } from '../dataObjs/CSSVo';
interface MyProps { stepData: CellGridOutputVo, callback: Function }
interface MyState { stepData: CellGridOutputVo }
class TowerEdit extends Component<MyProps, MyState>
{

    private cellIndex: number = 0;
    private controller: Controller = Controller.getInstance();
    private currentCell: HTMLInputElement | null = null;
    private currentCellVo:CellVo | null=null;
    private currentX: number = -1;
    private currentY: number = -1;
    private currentPos: TowerPosition | null = null;

    private ms:MainStorage=MainStorage.getInstance();
    
    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData };

        this.controller.addColumn = this.addColumn.bind(this);
        this.controller.subColumn = this.subColumn.bind(this);
        this.controller.addRow = this.addRow.bind(this);
        this.controller.subRow = this.subRow.bind(this);
        this.controller.setStyle=this.changeCss.bind(this);
    }

    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepData: this.props.stepData });
        }
    }
    getCell(tp: TowerPosition, x: number, y: number, cell: CellVo) {
        let key = "editCell_" + x.toString() + "_" + y.toString();
        this.cellIndex++;
        let id: string = "cell" + this.cellIndex.toString();
        let char: string = cell.text;
        let classString: string = "cellEdit";
        if (char === "@") {
            classString = "cellEditFade";
        }
        return (<input key={key} id={id} maxLength={1} type='text' className={classString} value={char} onFocus={(e: React.FocusEvent<HTMLInputElement>) => { this.onFocus(tp, x, y, e) }} onChange={(e: ChangeEvent<HTMLInputElement>) => { this.updateGrid(tp, x, y, e) }}></input>)
    }
    onFocus(tp: TowerPosition, x: number, y: number, e: React.FocusEvent<HTMLInputElement>) {
        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;

        if (this.currentCell)
        {
            this.currentCell.classList.remove('inputSelected');
        }

        inputBox.select();
        inputBox.classList.add("inputSelected");
        this.currentCell = inputBox;
        this.currentPos=tp;
        this.currentY=y;
        this.currentX=x;

        //console.log(tp, x, y);
        this.currentCellVo=this.getCellVo(tp,x,y);

        this.controller.setSelectedCss(this.currentCellVo.classArray);
        
    }
    changeCss(cssVo: CSSVo) {
        //console.log(cssVo.name);
        //console.log(this.currentPos);

        if (this.currentPos!==null) {
            let stepDataTemp: CellGridOutputVo = this.state.stepData;
            let cell: CellVo = this.getCellVo(this.currentPos, this.currentX, this.currentY);
            ////console.log(cell);

            if (cell) {
                cell.addCss(cssVo);                
            }
            this.props.callback(stepDataTemp);
            this.setState({ stepData: stepDataTemp });
            this.controller.setSelectedCss(cell.classArray);
        }
    }
    private getCellVo(tp: TowerPosition, x: number, y: number) {
        let stepDataTemp: CellGridOutputVo = this.state.stepData;
        let cell: CellVo | null = null;
        switch (tp) {
            case TowerPosition.Body:
                //  stepDataTemp.rows[x][y].text=char;
                cell = stepDataTemp.rows[x][y];
                break;

            case TowerPosition.Top:
                cell = stepDataTemp.top[y];
                break;

            case TowerPosition.Answer:
                cell = stepDataTemp.answer[y];
        }

        return cell;
    }
    updateGrid(tp: TowerPosition, x: number, y: number, e: ChangeEvent<HTMLInputElement>) {
        //console.log(x, y);
        let char: string = e.currentTarget.value;
        let stepDataTemp: CellGridOutputVo = this.state.stepData;

        let cell: CellVo = this.getCellVo(tp, x, y);

        if (cell) {

            cell.setText(char);
        }

        this.props.callback(stepDataTemp);

        this.setState({ stepData: stepDataTemp });

        let inputBox: HTMLInputElement = e.currentTarget as HTMLInputElement;
        let cellIndex = parseInt(inputBox.id.replace("cell", ""));
        if (!isNaN(cellIndex)) {
            cellIndex++;
            let nBox: HTMLInputElement = document.getElementById("cell" + cellIndex.toString()) as HTMLInputElement;
            if (nBox) {
                nBox.focus();
            }
        }
    }
    getCols(tp: TowerPosition, row: number, cells: CellVo[]) {
        let cols: JSX.Element[] = [];
        for (let i: number = 0; i < cells.length; i++) {
            let id: string = row.toString() + "_" + i.toString();

            cols.push(<span key={id} id={id}>{this.getCell(tp, row, i, cells[i])}</span>);
        }
        let rowKey: string = "editRow" + row.toString();

        return (<div key={rowKey}>{cols}</div>);
    }
    getTowerRows() {
        let dataRows: CellVo[][] = this.state.stepData.rows;
        let rows: JSX.Element[] = [];

        for (let i: number = 0; i < dataRows.length; i++) {
            rows.push(this.getCols(TowerPosition.Body, i, dataRows[i]));
        }
        return rows;
    }
    subRow(pos: RowPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;
        let dataRows: CellVo[][] = tempCells.rows;
        if (pos === RowPos.Bottom) {
            dataRows.pop();
        }
        else {
            dataRows.shift();
        }
        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }
    addRow(pos: RowPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;
        let dataRows: CellVo[][] = tempCells.rows;
        let len: number = 4;
        if (tempCells.rows.length > 0) {
            len = tempCells.rows[0].length;
        }
        let cols: CellVo[] = [];
        for (let i: number = 0; i < len; i++) {
            cols.push(new CellVo("@", []));
        }
        if (pos === RowPos.Bottom) {
            dataRows.push(cols);
        }
        else {
            dataRows.unshift(cols);
        }

        this.props.callback(tempCells);
        this.setState({ stepData: tempCells });
    }
    addColumn(part: TowerPosition, side: ColPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;

        switch (part) {
            case TowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].push(new CellVo("@", []));
                    }
                    else {
                        dataRows[i].unshift(new CellVo("@", []));
                    }

                }
                break;

            case TowerPosition.Top:
                let top: CellVo[] = tempCells.top;
                if (side === ColPos.Right) {
                    top.push(new CellVo("@", []));
                }
                else {
                    top.unshift(new CellVo("@", []));
                }
                break;

            case TowerPosition.Answer:
                let answer: CellVo[] = tempCells.answer;
                if (side === ColPos.Right) {
                    answer.push(new CellVo("@", []));
                }
                else {
                    answer.unshift(new CellVo("@", []));
                }
        }
        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }
    subColumn(part: TowerPosition, side: ColPos) {
        let tempCells: CellGridOutputVo = this.state.stepData;

        switch (part) {
            case TowerPosition.Body:
                let dataRows: CellVo[][] = tempCells.rows;
                for (let i: number = 0; i < dataRows.length; i++) {
                    if (side === ColPos.Right) {
                        dataRows[i].pop();
                    }
                    else {
                        dataRows[i].shift();
                    }
                }
                break;

            case TowerPosition.Top:
                let top: CellVo[] = tempCells.top;
                if (side === ColPos.Right) {
                    top.pop();
                }
                else {
                    top.shift();
                }
                break;

            case TowerPosition.Answer:
                let answer: CellVo[] = tempCells.answer;
                if (side === ColPos.Right) {
                    answer.pop();
                }
                else {
                    answer.shift();
                }
        }
        this.props.callback(tempCells);
        this.setState({ stepData: tempCells })
    }

    test() {
        this.addColumn(TowerPosition.Body, ColPos.Right)
    }
    render() {
        this.cellIndex = 0;

        return (<div><div id="towerEdit">
            <div className='labelText'>Top</div>
            {this.getCols(TowerPosition.Top, 0, this.state.stepData.top)}
            <hr />
            <div className='labelText'>Body</div>
            {this.getTowerRows()}
            <hr />
            <div className='labelText'>Answer</div>
            {this.getCols(TowerPosition.Answer, 100, this.state.stepData.answer)}
        </div>

        </div>)
    }
}
export default TowerEdit;