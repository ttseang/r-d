import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { CellGridOutputVo } from '../dataObjs/CellGridOutputVo';
import RTower from './RTower';
interface MyProps { index: number, stepData: CellGridOutputVo, callback: Function,selected:boolean }
interface MyState { stepData: CellGridOutputVo,selected:boolean }
class StepCard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { stepData: this.props.stepData,selected:this.props.selected };
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({stepData:this.props.stepData,selected:this.props.selected})
        }
    }
    render() {
        
        let selectedClass:string=(this.state.selected===true)?"selectedCard":"unselectedCard";

        return (<Card className="stepCard" onClick={() => { this.props.callback(this.props.index) }}>
            <Card.Header className={selectedClass}>Step {this.props.index}</Card.Header>
            <Card.Body className='stepCardBody'>
                <RTower top={this.state.stepData.top} rows={this.state.stepData.rows} answer={this.state.stepData.answer} fontSize={20} operator={this.state.stepData.operator}></RTower>
            </Card.Body>
        </Card>)
    }
}
export default StepCard;