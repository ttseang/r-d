import { Component } from 'react';
import { Controller } from '../classes/Controller';
import MainStorage from '../classes/MainStorage';
import { CSSVo } from '../dataObjs/CSSVo';
import CssCard from './CssCard';
interface MyProps {}
interface MyState {cssVo:CSSVo[],selectedCss:string[]}
class CssScroll extends Component<MyProps, MyState>
{
   
    

    private ms:MainStorage=MainStorage.getInstance();
    private controller:Controller=Controller.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {cssVo:this.ms.getCssArray(),selectedCss:[]};
            this.controller.setSelectedCss=this.updateSelected.bind(this);
        }
        componentDidMount()
        {
           
        }
        private updateSelected(selected:string[])
        {
            this.setState({selectedCss:selected});
        }
        getCards()
        {
            let cards:JSX.Element[]=[];
            //console.log(this.state.cssVo);

            for (let i:number=0;i<this.state.cssVo.length;i++)
            {
                let key:string="cssCard"+i.toString();
                let selected:boolean=this.state.selectedCss.includes(this.state.cssVo[i].value);

                cards.push(<CssCard key={key} cssVo={this.state.cssVo[i]} selected={selected}></CssCard>)
            }
            return cards;           
        }
    render() {
        return (<div className='scroll3'>{this.getCards()}</div>)
    }
}
export default CssScroll;