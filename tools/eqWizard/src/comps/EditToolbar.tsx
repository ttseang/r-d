import { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { SidePanels } from '../classes/Constants';
import { Controller } from '../classes/Controller';
import { EditButtonVo } from '../dataObjs/EditButtonVo';
interface MyProps { }
interface MyState {selected:number[]}
class EditToolbar extends Component<MyProps, MyState>
{
    private controller:Controller=Controller.getInstance();
        private buttonData:EditButtonVo[]=[];
        
        constructor(props: MyProps) {
            super(props);
            this.state = {selected:[2,0,0,0]};
            this.buttonData.push(new EditButtonVo("cols","fas fa-columns",0,0));
            this.buttonData.push(new EditButtonVo("rows","fas fa-grip-lines",0,1));
            this.buttonData.push(new EditButtonVo("CSS","fab fa-css3-alt",0,2));

        }
    doAction(cat:number,action:number)
    {
        switch(cat)
        {
            case 0:
            
            let panels:SidePanels[]=[SidePanels.Cols,SidePanels.Rows,SidePanels.CSS]
            this.controller.setSidePanel(panels[action]);
            break;
        }
        let selectArray:number[]=this.state.selected.slice();
        selectArray[cat]=action;
        this.setState({selected:selectArray});
    }
    makeButtons()
    {
        let buttons:JSX.Element[]=[];
        for (let i:number=0;i<this.buttonData.length;i++)
        {  
            let data:EditButtonVo=this.buttonData[i];
            let key:string="tbButton"+i.toString();
            let variantString:string="light";
            if (this.state.selected[data.buttonCat]===data.action)
            {
                variantString="dark";
            }
            buttons.push(<Button key={key} title={data.name} variant={variantString} onClick={()=>{this.doAction(data.buttonCat,data.action)}}><i className={data.icon}></i></Button>)
        }
        return (<ButtonGroup>{buttons}</ButtonGroup>)
    }
    setMode(panel:SidePanels)
    {
       // //console.log(mode);
       this.controller.setSidePanel(panel);
    }
    getButtons()
    {
        return (<ButtonGroup><Button variant='dark' onClick={()=>{this.setMode(SidePanels.Cols)}}>Cols</Button><Button variant='light' onClick={()=>{this.setMode(SidePanels.Rows)}}>Rows</Button><Button onClick={()=>{this.setMode(SidePanels.CSS)}}>Css</Button></ButtonGroup>)
    }
    render() {
        return (<div>
            {this.makeButtons()}
        </div>)
    }
}
export default EditToolbar;