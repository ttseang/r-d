import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { NavVo } from '../dataObjs/NavVo';
import Slider from './Slider';
interface MyProps { callback: Function, value: number,label:string }
interface MyState { left: number, right: number, top: number, bottom: number, numLock: boolean }
class FourSlides extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { left: this.props.value, right: this.props.value, top: this.props.value, bottom: this.props.value, numLock: true }
    }
    sliderUpdated(index: number, value: number) {

        if (this.state.numLock === true) {
            this.setState({ top: value, right: value, bottom: value, left: value });

        }
        else {
            switch (index) {
                case 1:
                    this.setState({ top: value })
                    break;

                case 2:
                    this.setState({ right: value })
                    break;

                case 3:
                    this.setState({ bottom: value })
                    break;

                case 4:
                    this.setState({ left: value })
                    break;
            }
        }
        let obj: NavVo = new NavVo(this.state.top, this.state.right, this.state.bottom, this.state.left);
        this.props.callback(obj);
    }
    lockChanged() {
        
       this.setState({ numLock:!this.state.numLock });
    }
    render() {
        let msg:string=(this.state.numLock===true)?"locked":"unlocked";

        return (<div>
            <Card>
                <Card.Title>{this.props.label}</Card.Title>
                <Card.Body>
                    <div className="tal"><Button size="sm" onClick={this.lockChanged.bind(this)}>{msg}</Button></div>
                    <Slider min={50} max={800} index={1} label="top" callback={this.sliderUpdated.bind(this)} value={this.state.top} disabled={false}></Slider>
                    <Slider min={50} max={800} index={1} label="right" callback={this.sliderUpdated.bind(this)} value={this.state.right} disabled={this.state.numLock}></Slider>
                    <Slider min={50} max={800} index={1} label="bottom" callback={this.sliderUpdated.bind(this)} value={this.state.bottom} disabled={this.state.numLock}></Slider>
                    <Slider min={50} max={800} index={1} label="left" callback={this.sliderUpdated.bind(this)} value={this.state.left} disabled={this.state.numLock}></Slider>
                </Card.Body> </Card> </div>)
    }
}
export default FourSlides;