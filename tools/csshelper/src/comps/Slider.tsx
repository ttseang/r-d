import React, { ChangeEvent, Component } from 'react';
import { Form } from 'react-bootstrap';
interface MyProps {index:number,callback:Function,label:string,value:number,min:number,max:number,disabled:boolean}
interface MyState {value:number}
class Slider extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {value:props.value};
        }
    componentDidUpdate(prevProps:MyProps)
    {
        if (prevProps.value!==this.props.value)
        {
            this.setState({value:this.props.value});
        }
    }
    sliderUpdated(e:ChangeEvent<HTMLInputElement>)
    {
        this.setState({value:parseFloat(e.currentTarget.value)});
        this.props.callback(this.props.index,parseFloat(e.currentTarget.value));

    }
    render() {
        return (<div>
            <Form.Group controlId="formBasicRange">
                <Form.Label>{this.props.label}:{this.state.value}</Form.Label>
                <Form.Control disabled={this.props.disabled} type="range" max={this.props.max} min={this.props.min} value={this.state.value} onChange={this.sliderUpdated.bind(this)} />
            </Form.Group>
        </div>)
    }
}
export default Slider;