import React, { ChangeEvent, Component } from 'react';
import { Row, Col, ListGroupItem } from 'react-bootstrap';
interface MyProps {index:number,callback:Function,label:string}
interface MyState {file:string}
class ImageSelect extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
    this.state={file:"./noimage.jpg"};
}
handleChange(e:ChangeEvent<HTMLInputElement>)
{
    if (e.currentTarget)
    {
        if (e.currentTarget.files)
        {
            let file:string=URL.createObjectURL(e.currentTarget.files[0]);
            this.setState({file:file});
            this.props.callback(this.props.index,file);
        }
    }
}
render()
{
   let key:string="select"+this.props.index.toString();

return (<ListGroupItem key={key}>
        <Row><Col>{this.props.label}</Col><Col></Col></Row>
        <Row><Col><img width="100px" height="100px" src={this.state.file} alt="selected"></img></Col><Col><input type="file" onChange={this.handleChange.bind(this)}/></Col></Row>
    </ListGroupItem>)
}
}
export default ImageSelect;