import React, { Component, CSSProperties } from "react";
import { Card } from "react-bootstrap";
import Slider from "./Slider";
interface MyProps {
    code: JSX.Element;
}
interface MyState {
    code: JSX.Element;
    h: number;
    w: number;
}
class PrevWindow extends Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
        let el: JSX.Element = <h2>hello world</h2>;
        this.state = { code: el, h: 300, w: 300 };
    }
    componentDidUpdate(prevProps: MyProps) {
        if (prevProps.code !== this.props.code) {
            this.setState({ code: this.props.code });
        }
    }
    getHolderStyle() {
        let style: CSSProperties = {};
        style.height = this.state.h;
        style.width = this.state.w;
        return style;
    }
    sliderUpdated(index: number, value: number) {
        
        switch (index) {
            case 1:
                this.setState({ w: value })
                break;

            case 2:
                this.setState({ h: value })
                break;
        }
    }
    render() {
        return (
            <div>                
                <Card>
                    <Card.Title>Resize</Card.Title>
                    <Card.Body>
                        <Slider min={50} max={800} index={1} label="width" callback={this.sliderUpdated.bind(this)} value={300} disabled={false}></Slider>
                        <Slider min={50} max={800} index={2} label="height" callback={this.sliderUpdated.bind(this)} value={300} disabled={false}></Slider>
                    </Card.Body>
                </Card>
                <Card>
                    <Card.Header>Preview</Card.Header>
                    <Card.Body>
                        <div style={this.getHolderStyle()}>{this.props.code}</div>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
export default PrevWindow;
