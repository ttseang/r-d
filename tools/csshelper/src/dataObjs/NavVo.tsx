export class NavVo
{
    public top:number;
    public left:number;
    public right:number;
    public bottom:number;

    constructor(top:number,right:number,bottom:number,left:number)
    {
        this.top=top;
        this.right=right;
        this.bottom=bottom;
        this.left=left;
    }
    toString(usePx:boolean=false)
    {
        if (usePx===true)
        {
            return this.top.toString()+"px "+this.right.toString()+"px "+this.bottom.toString()+"px "+this.left.toString()+"px ";
        }
        return this.top.toString()+" "+this.right.toString()+" "+this.bottom.toString()+" "+this.left.toString();
    }
}