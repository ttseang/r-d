import React, { Component, CSSProperties } from 'react';
import { Button, ButtonGroup, Card } from 'react-bootstrap';
import FourSlides from '../comps/FourSlide';

import ImageSelect from '../comps/ImageSelect';
import PrevWindow from '../comps/PrevWindow';
import { NavVo } from '../dataObjs/NavVo';
interface MyProps { }
interface MyState { backgroundImage: string, borderImage: string, activeTab: number, sliced: NavVo, borderImageWidth: NavVo }
class BoarderScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        let defSlice: NavVo = new NavVo(260, 260, 260, 260);
        let defWidth: NavVo = new NavVo(133, 133, 133, 133);
        this.state = { backgroundImage: "", borderImage: "", activeTab: 0, sliced: defSlice, borderImageWidth: defWidth };
    }
    imageSelected(index: number, file: string) {
        switch (index) {
            case 1:
                this.setState({ backgroundImage: file });
                break;

            case 2:
                this.setState({ borderImage: file });
                break;
        }
    }
    slideChanged(navVo: NavVo) {
        this.setState({ sliced: navVo });
    }
    imageWidthChanged(navVo: NavVo) {
        this.setState({ borderImageWidth: navVo });
    }
    getHtml() {
        if (this.state.backgroundImage === "" || this.state.borderImage === "") {
            return (<div></div>);
        }
        //let css:string="background-image:url("+this.state.backgroundImage+")";

        let style: CSSProperties = {};
        style.background = "url(" + this.state.backgroundImage + ")";
        style.padding = "60px 20px 0 20px";
        style.height = "calc(100% - 100px)";
        style.width = "calc(100% - 80px)";
        style.backgroundColor = "#EDE1D1";
        style.backgroundPosition = "center";
        style.border = "20px solid #E2D1C1";
        style.borderImageSource = "url(" + this.state.borderImage + ")";
        //  style.borderImageWidth = "133px 133px 133px 133px";
        style.borderImageWidth = this.state.borderImageWidth.toString(true);
        //   style.borderImageSlice = "266 266 266 266";
        style.borderImageSlice = this.state.sliced.toString();
        style.borderImageRepeat = "round";
        style.borderImageOutset = "0px";
        style.textAlign = "center";

        return (<div style={style}></div>)

    }

    getTabs() {
        return (<ButtonGroup><Button variant="link" onClick={() => { this.setTab(0) }}>Images</Button><Button variant="link" onClick={() => { this.setTab(1) }}>Slice</Button><Button variant="link" onClick={() => { this.setTab(2) }}>Border Width</Button></ButtonGroup>)
    }
    setTab(index: number) {
        this.setState({ activeTab: index });
    }
    getTab(index: number) {

        switch (index) {
            case 0:

                return (<div><ImageSelect label="background" callback={this.imageSelected.bind(this)} index={1}></ImageSelect>
                    <ImageSelect label="border" callback={this.imageSelected.bind(this)} index={2}></ImageSelect></div>);

            case 1:
                return <FourSlides label="Border Image Slice" value={266} callback={this.slideChanged.bind(this)}></FourSlides>

            case 2:

                return <FourSlides label="Border Image Width" value={133} callback={this.imageWidthChanged.bind(this)}></FourSlides>
        }

    }
    render() {
        return (<div>

            <PrevWindow code={this.getHtml()}></PrevWindow>

            <Card>
                <Card.Body>
                    <Card.Title>Properties</Card.Title>
                    {this.getTabs()}
                    {this.getTab(this.state.activeTab)}

                </Card.Body>
            </Card>

        </div>)
    }
}
export default BoarderScreen;