import React, { Component } from 'react';
import {  Col, Button, ListGroupItem, ListGroup } from 'react-bootstrap';
interface MyProps {callback:Function}
interface MyState { }
class SelectScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state={};
        }
    
    render() {
        return (<div>
            <ListGroup>
            <ListGroupItem key="border">
                <Col><Button onClick={()=>{this.props.callback(0)}}>Make Border</Button></Col>
            </ListGroupItem>
            <ListGroupItem key="tile">
                <Col><Button onClick={()=>{this.props.callback(1)}}>Make Tile</Button></Col>
            </ListGroupItem>
            <ListGroupItem key="nine">
                <Col><Button onClick={()=>{this.props.callback(2)}}>Make 9-slice</Button></Col>
            </ListGroupItem>
            </ListGroup>
        </div>)
    }
}
export default SelectScreen;