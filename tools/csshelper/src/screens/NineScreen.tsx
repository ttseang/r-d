import React, { Component, CSSProperties } from 'react';
import { Button, ButtonGroup, Card } from 'react-bootstrap';
import FourSlides from '../comps/FourSlide';
import ImageSelect from '../comps/ImageSelect';
import PrevWindow from '../comps/PrevWindow';
import { NavVo } from '../dataObjs/NavVo';
interface MyProps { }
interface MyState { backgroundImage: string, borderImage: string, activeTab: number, sliced: NavVo, borderImageWidth: NavVo }
class NineScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        let defSlice: NavVo = new NavVo(260, 260, 260, 260);
        let defWidth: NavVo = new NavVo(133, 133, 133, 133);
        this.state = { backgroundImage: "", borderImage: "", activeTab: 0, sliced: defSlice, borderImageWidth: defWidth }
    }
    imageSelected(index: number, file: string) {
        switch (index) {
            case 1:
                this.setState({ backgroundImage: file });
                break;

            case 2:
                this.setState({ borderImage: file });
                break;
        }
    }
    setTab(index: number) {
        this.setState({ activeTab: index });
    }
    getTabs() {
        return (<ButtonGroup><Button variant="link" onClick={() => { this.setTab(0) }}>Images</Button><Button variant="link" onClick={() => { this.setTab(1) }}>Slice</Button><Button variant="link" onClick={() => { this.setTab(2) }}>Border Width</Button></ButtonGroup>)
    }
    getTab(index: number) {
        switch (index) {
            case 0:

                return (<div><ImageSelect label="background" callback={this.imageSelected.bind(this)} index={1}></ImageSelect>
                    <ImageSelect label="border" callback={this.imageSelected.bind(this)} index={2}></ImageSelect></div>);
            case 1:
                return <FourSlides label="Border Image Slice" value={266} callback={this.slideChanged.bind(this)}></FourSlides>

            case 2:

                return <FourSlides label="Border Image Width" value={133} callback={this.imageWidthChanged.bind(this)}></FourSlides>

        }
    }
    slideChanged(navVo: NavVo) {
        this.setState({ sliced: navVo });
    }
    imageWidthChanged(navVo: NavVo) {
        this.setState({ borderImageWidth: navVo });
    }
    getHtml() {
        if (this.state.backgroundImage === "" || this.state.borderImage === "") {
            return (<div></div>);
        }
        let style: CSSProperties = {};
        style.padding = "30px";
        style.height = "calc(100% - 220px)";
        style.width = "calc(100% - 220px)";
        style.backgroundColor = "#E2D1C0";
        style.backgroundImage = "url(" + this.state.backgroundImage + ")";
        style.border = "80px solid #936A19";
        style.borderImageSource = "url(" + this.state.borderImage + ")";
        style.borderImageWidth = this.state.borderImageWidth.toString(true);
        style.borderImageSlice = this.state.sliced.toString();

        /*  padding: 30px;
         height: calc(100% - 220px);
         width: calc(100% - 220px);
         background-color: #E2D1C0;
         background-image: url('./papertile.jpg');
         border: 80px solid #936A19;
         border-image-source: url('./woodsyborder.png');
         border-image-width: 163px 236px 247px 244px;
         border-image-slice: 163 236 247 244;
         border-image-outset: 0px;
         text-align: center; */

        return (<div style={style}></div>)
    }
    render() {
        return (<div><PrevWindow code={this.getHtml()}></PrevWindow>

            <Card>
                <Card.Body>
                    <Card.Title>Properties</Card.Title>
                    {this.getTabs()}
                    {this.getTab(this.state.activeTab)}
                </Card.Body>
            </Card></div>)
    }
}
export default NineScreen;