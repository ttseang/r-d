import React, { Component, CSSProperties } from 'react';
import { Card } from 'react-bootstrap';

import ImageSelect from '../comps/ImageSelect';
import PrevWindow from '../comps/PrevWindow';
interface MyProps {}
interface MyState {activeTab:number,overlayImage:string,backgroundImage:string}
class TileScreen extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
this.state={activeTab:0,backgroundImage:"",overlayImage:""}
}
imageSelected(index: number, file: string) {
    switch (index) {
        case 1:
            this.setState({ backgroundImage: file });
            break;

        case 2:
            this.setState({ overlayImage: file });
            break;
    }
}
getTabs()
{

}
getTab(index:number)
{
    switch(index)
    {
        case 0:

        return (<div><ImageSelect label="background" callback={this.imageSelected.bind(this)} index={1}></ImageSelect>
        <ImageSelect label="overlay" callback={this.imageSelected.bind(this)} index={2}></ImageSelect></div>);

    }
}
getHtml()
{
    if (this.state.backgroundImage === "" || this.state.overlayImage === "") {
        return (<div></div>);
    }
    let style: CSSProperties = {};
        style.height="100%";
		style.width="100%";
        style.backgroundColor="#FA709B";
        style.backgroundImage="url("+this.state.overlayImage+"),url("+this.state.backgroundImage+")";
        style.backgroundRepeat="no-repeat, auto";

        style.backgroundSize="100% 100%, auto";
	//	background-color: #FA709B;
	//	background-image: url('./centerlightshading.png'), url('./tiled_jelly_beans.png');
	//	background-repeat: no-repeat, auto;
	//	background-size: 100% 100%, auto;

     return (<div style={style}></div>)
}
render()
{
    return (<div>

        <PrevWindow code={this.getHtml()}></PrevWindow>

        <Card>
            <Card.Body>
                <Card.Title>Properties</Card.Title>
                {this.getTabs()}
                {this.getTab(this.state.activeTab)}
            </Card.Body>
        </Card>

    </div>)
}
}
export default TileScreen;