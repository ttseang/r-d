import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import BoarderScreen from './BorderScreen';
import NineScreen from './NineScreen';
import SelectScreen from './SelectScreen';
import TileScreen from './TileScreen';
interface MyProps { }
interface MyState { mode: number, screenIndex: number }
class BaseScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0, screenIndex: 0 }
    }
    setScreen(index:number)
    {
        this.setState({mode:1,screenIndex:index});
    }
    getScreen() {
        if (this.state.mode === 0) {
            //select screen
            return <SelectScreen callback={this.setScreen.bind(this)}></SelectScreen>
        }
        else {
            switch (this.state.screenIndex) {
                case 0:

                    return (<BoarderScreen></BoarderScreen>)

                case 1:
                    return (<TileScreen></TileScreen>)

                case 2:
                    return (<NineScreen></NineScreen>)
            }
        }
    }
    getTitle()
    {
        if (this.state.mode===0)
        {
            return (<h2>Background & Border Wizard</h2>);
        }
        let titles:string[]=["Border","Tiled Background","Nine Slice"];
        return (<Card.Title>{titles[this.state.screenIndex]}</Card.Title>);
    }
    render() {
        return (<div><Card>
            <Card.Body>
                <Card.Header>{this.getTitle()}</Card.Header>
                
                {this.getScreen()}
            </Card.Body>
        </Card></div>)
    }
}
export default BaseScreen;