export class EffectDataVo
{
    public name:string;
    public key:string;
    public mainClass:string;
    public hClass:string;   
    public phaserEffect:string;
    public secs:number;
    constructor(key:string,name:string,mainClass:string,hClass:string,phaserEffect:string,secs:number=0)
    {
        this.key=key;
        this.name=name;
        this.mainClass=mainClass;
        this.hClass=hClass;
        this.phaserEffect=phaserEffect;      
        this.secs=secs;
    }
}