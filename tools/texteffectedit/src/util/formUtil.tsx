import {
    AlignGrid
} from "./alignGrid";

import IBaseScene from "../interfaces/IBaseScene";


export class FormUtil {
    private gameWidth:number=0;
    private gameHeight:number=0;
    private scene:Phaser.Scene;
    private alignGrid:AlignGrid;

    constructor(bscene:IBaseScene,rows:11,cols:11) {
        //super();
        this.scene = bscene.getScene();
        //get the game height and width
        this.gameWidth = bscene.getW();
        this.gameHeight= bscene.getH();

        this.alignGrid = new AlignGrid(bscene,rows,cols,this.gameWidth,this.gameHeight);
      //  this.alignGrid.color=0xfff000;
    }
    showNumbers() {
        this.alignGrid.showNumbers();
    }
    scaleToGameW(elName:string, per:number) {
        var el:any = document.getElementById(elName);
        var w = this.gameWidth * per;
        el.style.width = w + "px";
    }
    scaleToGameH(elName:string, per:number) {
        var el:any = document.getElementById(elName);
        var h:number;
        if (el)
        {
            h = this.gameHeight * per;
            el.style.height = h + "px";
        }
        h = this.gameHeight * per;
        el.style.height = h + "px";
    }
    placeElementAt(index:number, elName:string, centerX = true, centerY = false) {
        //get the position from the grid
        var pos = this.alignGrid.getPosByIndex(index);
      //  //console.log(pos);
        //extract to local vars
        var x = pos.x;
        var y = pos.y;
        //console.log(x,y);
        //get the element
        var el:any = document.getElementById(elName);
        //set the position to absolute
        el.style.position = "absolute";
        //get the width of the element
        let  w1:string = el.style.width;
        //convert to a number
        let w:number = this.toNum(w1);
     //   //console.log("w=" + w);
        //
        //
        //center horizontal in square if needed
        if (centerX === true) {
            x -= w / 2;
        }
        //
        //get the height
        //        
        let h1 = el.style.height;
        //convert to a number
        let h:number = this.toNum(h1);
        //
        //center verticaly in square if needed
        //
        if (centerY === true) {
            y -= h / 2;
        }
       // //console.log("x=" + x);
        //set the positions
        el.style.top = y + "px";
        el.style.left = x + "px";
    }
    //changes 100px to 100
    toNum(s:string) {
        s = s.replace("px", "");
        let s2:number = parseInt(s);
        return s2;
    }
    //add a change callback
    addChangeCallback(elName:string, fun:Function, scope = null) {
        var el:any = document.getElementById(elName);
        el.onchange = fun;
        
    }
    blankElement(elName:string)
    {
        var el:any = document.getElementById(elName);
        el.style.visibility = "hidden";
    }
    unblankElement(elName:string)
    {
        var el:any = document.getElementById(elName);
        el.style.visibility = "visible";
    }
    hideElement(elName:string) {
        var el:any = document.getElementById(elName);
        el.style.display = "none";
    }
    showElement(elName:string) {
        var el:any = document.getElementById(elName);
        el.style.display = "block";
    }
    getTextAreaValue(elName:string) {
        var el = document.getElementById(elName) as HTMLTextAreaElement;
        return el.value;
    }
    getTextValue(elName:string) {
        var el:any = document.getElementById(elName);
        return el.innerText;
    }
    setTextValue(elName:string, val:string) {
        var el = document.getElementById(elName) as HTMLInputElement;
        //window.el=el;
        el.value = val;
    }
   /*  copyFrom(elName)
    {
         var el:any = document.getElementById(elName);
         el.select();
        document.execCommand('copy');
    } */
    addClickCallback(elName:string, fun:Function) {
        var el:any = document.getElementById(elName);       
            el.onclick = fun;        
    }
   /*  addOption(dropDown, text, item) {
        var select = document.getElementById(dropDown);
        var option = document.createElement('option');
        option.text = text;
        option.data = item;
        select.add(option, 0);
    }
    getSelectedItem(dropDown) {
        var e = document.getElementById(dropDown);
        return e.options[e.selectedIndex].data;
    }
    getSelectedIndex(dropDown) {
        var el = document.getElementById(dropDown);
        return el.selectedIndex;
    }
    getSelectedText(dropDown) {
        var e = document.getElementById(dropDown);
        return e.options[e.selectedIndex].text;
    } */
}