import IBaseScene from "../interfaces/IBaseScene";
import { AnimationPlayer } from "./animationPlayer";

export class AnimationOnText {
    private t: Phaser.GameObjects.Text;
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    public player: AnimationPlayer;

    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text, animation: string, placement: number = 0) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.t = t;
        console.log(animation);
        this.player = new AnimationPlayer(bscene, animation, t.displayWidth * 2);

        switch (placement) {
            case 0:
                this.player.x = t.x + t.displayWidth / 2;
                this.player.y = t.y - t.displayHeight / 2;
                break;

            case 1:
                this.player.x = t.x + t.displayWidth / 2;
                this.player.y = t.y + t.displayHeight / 2;
                break;
        }



        t.visible = true;

    }
}