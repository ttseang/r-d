import IBaseScene from "../interfaces/IBaseScene";
import { TextMask } from "./textMask";

export class Reveal
{
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private scale: number = 0;
   
    //   private corners: Phaser.GameObjects.Sprite[] = [];
    private timer1: Phaser.Time.TimerEvent | undefined;
   
    public callback: Function = () => { };
    private animationName: string;
    private t: Phaser.GameObjects.Text;
    private tm:TextMask;
    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text, animationName: string)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.t=t;
        this.animationName=animationName;
        this.tm=new TextMask(bscene,t,"holder");
        this.tm.setFullMask();      
        
        

        this.initPos();
        this.tm.x=t.x;
        this.tm.y=t.y;
    }
    private initPos()
    {
        switch(this.animationName)
        {
            case "revealLeft":
                this.tm.holder.x=this.t.displayWidth;
                break;

            case "revealRight":
                this.tm.holder.x=-this.t.displayWidth;
                break;

            case "revealUp":
                this.tm.holder.y=this.t.displayHeight;
                break;
            case "revealDown":
                this.tm.holder.y=-this.t.displayHeight;
                break;
        }
    }
    animate()
    {
       this.scene.tweens.add({ targets: this.tm.holder, duration: 1000, x: this.t.x,y:this.t.y,onComplete:this.destroy.bind(this) })
    }
    destroy()
    {
        this.tm.destroy();
        this.callback();
    }
}