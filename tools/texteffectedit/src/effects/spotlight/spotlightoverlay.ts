export class SpotlightOverlay {

    private elmt: HTMLElement | null;


    constructor() {
        //var SO = this;
        this.elmt = document.querySelector('.spotlightoverlay');
        console.log(this.elmt);
    }
    /*
        This is the element that actually renders the Spotlight Effect.
        It covers the entire window, and is layered on top of other elements.
        */
    fadeIn() {
        this.show();
        if (this.elmt) {
            this.elmt.style.opacity = '1';
        }
    }

    fadeOut() {
        setTimeout(this.hide.bind(this), 500);
        if (this.elmt) {
            this.elmt.style.opacity = '0';
        }
    };
    hide() {
        if (this.elmt) {
            this.elmt.style.display = 'none';
        }
    };
    setBackground(cx: number, cy: number, r:number) {
        var bg = 'radial-gradient(circle at '
            + String(cx) + 'px '
            + String(cy) + 'px, rgba(0,0,0,0) '
            + String(r - 5) + 'px, rgba(0,0,0,0.4) '
            + String(r + 5) + 'px)';
        console.log(this.elmt);
        if (this.elmt) {
            this.elmt.style.backgroundImage = bg;
        }
    };
    show() {
        if (this.elmt) {
            this.elmt.style.display = 'block';
        }
    };

};
