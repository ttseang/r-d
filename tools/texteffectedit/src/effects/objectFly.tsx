
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class ObjectFly
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private scale:number=0;
    private dir:number=1;
    private count:number=0;
    private secs:number=2;
    private obj: Phaser.GameObjects.Sprite;
    private timer1:Phaser.Time.TimerEvent | undefined;
    private effectName:string;
    private ox:number=0;
    private oy:number=0;
    private oc:string="#ff0000";

    private t:Phaser.GameObjects.Text;
    constructor(bscene:IBaseScene,t:Phaser.GameObjects.Text,key:string,secs:number,effectName:string)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.t=t;      
        this.ox=t.x;
        this.oy=t.y;
        this.oc=t.style.color;
        this.effectName=effectName;
        this.obj=this.scene.add.sprite(0,bscene.getH()+100,key).setOrigin(0.5,0);
        if (effectName==="thumb")
        {
            this.obj.x=bscene.getW();
        }
        Align.scaleToGameW(this.obj,0.1,bscene);
        this.secs=secs;
    }
    public animate()
    {
        //this.obj.x=this.t.x;
        //this.obj.y=this.t.y;
        let time: number = this.secs * 1000;
        let tx:number=this.t.x+this.t.displayWidth/2;
        let ty:number=this.t.y+this.t.displayHeight;

        this.scene.tweens.add({targets: this.obj,duration: time,y:ty,x:tx,onComplete:this.doEnd.bind(this)});
        this.scene.time.addEvent({ delay: time*2, callback: this.destroy.bind(this),  loop: false });
    }
    private doEnd()
    {
        if (this.effectName==="punch" || this.effectName==="thumb")
        {
            this.t.y-=10;
            this.t.setColor("#2ecc71");
        }
    }
    destroy()
    {
        this.t.x=this.ox;
        this.t.y=this.oy;
        this.t.setColor(this.oc);
        this.obj.destroy();
    }
}