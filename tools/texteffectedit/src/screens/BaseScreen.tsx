import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import LayoutScreen from './LayoutScreen';
interface MyProps {}
interface MyState {mode:number}
class BaseScreen extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
this.state={mode:0};
}
getScreen()
{
    switch(this.state.mode)
    {
        case 0:

        return (<LayoutScreen></LayoutScreen>);
    }
}
render()
{
return (<div id="base">
    <Card>
        <Card.Header className="head1">
           <span  className="titleText">Title Here</span>
        </Card.Header>
        <Card.Body>
        {this.getScreen()}
        </Card.Body>
    </Card>
</div>)
}
}
export default BaseScreen;