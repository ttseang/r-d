import React, { ChangeEvent, Component } from 'react';
import { Row, Col, Card, Button, Form } from 'react-bootstrap';
import Controller from '../classes/Controller';
import { MainStorage } from '../classes/MainStorage';
import { EffectDataVo } from '../dataObjs/EffectDataVo';
import { SceneFont } from '../scenes/SceneFont';
import SceneMain from '../scenes/SceneMain';
interface MyProps { }
interface MyState { mainText: string, highText: string, effectClass: string, heffectClass: string,effectName:string }
class LayoutScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private controller: Controller = Controller.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {effectName:"brackets", mainText: "You just put ED at the end of the word", highText: "ED", effectClass: "noEffect", heffectClass: "noEffect" };
    }
    componentDidMount() {
      //  const c2:Phaser.Types.Core.GameConfig={};
        
        const config: any = {
            mode: Phaser.AUTO,
            width: 640,
            height: 480,
            parent: 'phaser-game',
            scene: [SceneFont,SceneMain]
        }
        this.ms.gameConfig = config;

       let game:any= new Phaser.Game(config);
        this.ms.game=game;

       //console.log(game);
    }
    updateMainText(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ mainText: e.currentTarget.value });
    }
    updateHighText(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ highText: e.currentTarget.value });
    }
    getInner() {
        let main: string = this.state.mainText;
        let htext: string = this.state.highText;
        if (htext !== "") {
            let partArray: string[] = main.split(htext);
            //console.log(partArray);

            if (partArray.length === 2) {
                return (<div id="efxtext" className={this.state.effectClass}>{partArray[0]}<span id="htext" className={this.state.heffectClass}>{htext}</span>{partArray[1]}</div>)
            }
        }

        return (<div><div id="efxtext" className={this.state.effectClass}>{main}</div></div>)
    }
    setEffect() {
        let blank: EffectDataVo = new EffectDataVo("blank", "blank", "noEffect", "noEffect", "none");

        let effectData: EffectDataVo = this.ms.getEffectData(this.state.effectName) || blank;

        console.log(effectData);

        this.setState({ effectClass: effectData.mainClass, heffectClass: effectData.hClass });

        if (effectData.phaserEffect !== "none") {

            this.controller.showPhaserEffect(effectData, this.state.highText);
        }
    }
    getDropDown() {
        let ddArray: JSX.Element[] = [];

      /*   for (let i:number=0;i<this.ms.effectKeys.length;i++)
        {
            let effectData:EffectDataVo =this.ms.effectData.get(this.ms.effectKeys[i]) || new EffectDataVo("","","","","",0);

            if (effectData.name!=="")
            {
                ddArray.push(<option key={effectData.key} value={effectData.key}>{effectData.name.toLocaleUpperCase()}</option>);
            }
            
        }
 */

         let effectArray:EffectDataVo[]=this.ms.effectArray.slice();

         effectArray=effectArray.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);

     //   console.log(effectArray);

         effectArray.forEach((data: EffectDataVo) => {

            ddArray.push(<option key={data.key} value={data.key}>{data.name.toLocaleUpperCase()}</option>);

        });
        return (<Form.Control as="select" onChange={this.effectChanged.bind(this)}>{ddArray}</Form.Control>)
    }
    effectChanged(e:ChangeEvent<HTMLSelectElement>)
    {
        //console.log(e.currentTarget.value);
        this.setState({effectName:e.currentTarget.value});
    }
    resetEffect() {
        window.location.reload();
       // this.setState({ effectClass: "noEffect", heffectClass: "noEffect" });
       // this.controller.resetCanvas();
    }
    render() {
        //<div id="screen1" className="fscreen">screen</div>
        return (<div>
            <Row><Col>
            <div className="spotlightoverlay"></div>
                <div className="tac">
                    <div id="phaser-game">
                        {this.getInner()}
                    </div>
                    
                    </div></Col>
            </Row>
            <Card>
                <Card.Body>
                    <Row><Col sm="4" className="tar">Main Text</Col><Col sm="8"><input type="text" className="text1" value={this.state.mainText} onChange={this.updateMainText.bind(this)} /></Col></Row>
                    <Row><Col sm="4" className="tar">Hightlight Text</Col><Col sm="8"><input type="text" className="text1" value={this.state.highText} onChange={this.updateHighText.bind(this)} /></Col></Row>
                    <Row><Col sm="4" className="tar">Effect</Col><Col sm="4">{this.getDropDown()}</Col></Row>
                    <Row><Col sm="2"><Button onClick={this.resetEffect.bind(this)}>Reset</Button></Col><Col sm="2"><Button onClick={this.setEffect.bind(this)}>Run</Button></Col></Row>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default LayoutScreen;