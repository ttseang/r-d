import { EffectDataVo } from "../dataObjs/EffectDataVo";

export class Controller {

    private static instance: Controller;
    // public dc: DC | null = null;
    //
    //
    public showPhaserEffect:Function = (effect:EffectDataVo,text:string) => {};   
    public updateCanvas: Function = () => {};   
    public resetCanvas: Function = () => {};
    public prevGame:Function=()=>{};
    /* constructor() {
      //
    } */
    static getInstance(): Controller {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new Controller();
        }
        return this.instance;
    }
}
export default Controller;