
import Controller from "../classes/Controller";
import { EffectDataVo } from "../dataObjs/EffectDataVo";
import { EffectUtil } from "../util/EffectUtil";
import { FormUtil } from "../util/formUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private formUtil: FormUtil | undefined;
    private controller: Controller = Controller.getInstance();
    private effectUtil: EffectUtil | undefined;
    private effectText: Phaser.GameObjects.Text | undefined;

    constructor() {
        super("SceneMain");
        //this.formUtil=new FormUtil(this,11,11);
        // this.effectUtil=new EffectUtil(this,this.formUtil);

    }
    preload() {
        this.load.image("holder", "assets/holder.jpg");
        this.load.image("face", "assets/face.png");
        this.load.image("bracket", "assets/corner.png");
        this.load.image("hand", "assets/hand.png");
        this.load.image("punch", "assets/punch.png");
        this.load.image("thumbsUp", "assets/thumbsUp.png");
        this.load.image("circle", "assets/circle.png");
        this.load.image("heart", "assets/heart.png");
        this.load.image("balloon", "assets/balloon.png");
        this.load.image("rocket", "assets/rocket2.png");
        this.load.image("rays3", "assets/rays3.png");
        this.load.image("donut","assets/donut.png");
        this.load.image("sunrise","assets/sunrise.png");

        this.load.atlas("halfRays","./assets/rays_half.png","./assets/rays_half.json");
        this.load.atlas("brackets","./assets/bracket.png","./assets/bracket.json");
        this.load.atlas("bubblePop","./assets/bubblePop.png","./assets/bubblePop.json");
        this.load.atlas("starBurst","./assets/starburst.png","./assets/starburst.json");
        this.load.atlas("fullRays","./assets/rays.png","./assets/rays.json");
        this.load.atlas("raysPaper","./assets/raysPaper.png","./assets/raysPaper.json");
        this.load.atlas("sparks","./assets/sparks.png","./assets/sparks.json");
        this.load.atlas("radial","./assets/radial.png","./assets/radial.json");

        this.load.image("paperTop","./assets/paperTop.png");
        this.load.image("paperBottom","./assets/paper1.png");
    }
    create() {
        super.create();
        this.formUtil = new FormUtil(this, 11, 11);
        this.effectUtil = new EffectUtil(this, this.formUtil);
        this.makeGrid(11, 11);
        //  this.grid.showNumbers();

        this.effectUtil.loadAnimations();

        console.log(this.anims);

       // this.add.sprite(0,0,"halfRays");

        this.effectText = this.add.text(0, 0, "TEXT HERE");
        this.effectText.visible = false;

        // this.placeImage("face",34,0.1);

        this.formUtil.placeElementAt(13, "efxtext", false, false);
       // this.formUtil.placeElementAt(0,"screen1",false,false);
        this.controller.showPhaserEffect = this.doEffect.bind(this);
        this.controller.resetCanvas=this.reset.bind(this);

        /* let screen1:any=document.getElementById('screen1');
        screen1.style.width=this.getW();
        screen1.style.height=this.getH(); */
    }
    doEffect(effect: EffectDataVo, text: string) {
        //////////console.log(effect);

        if (this.effectText) {
            if (this.effectUtil) {
                this.effectText.setText(text);
                this.effectUtil.doEffect(effect.phaserEffect, this.effectText, 'htext', effect.secs);
            }
        }
    }
    reset() {
        if (this.formUtil) {
            this.formUtil.unblankElement('htext');
            this.formUtil.unblankElement('efxtext');
            if (this.effectText) {
                this.effectText.visible = false;

                this.effectText.setAngle(0);
                this.effectText.setScale(1, 1);
                console.log(this);
            }
        }
    }
    update() {

    }
}
export default SceneMain;