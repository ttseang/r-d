import React, { Component } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { MainStorage } from '../../classes/core/MainStorage';
import PicRow from '../../classes/picGame/PicRow';
import { MatchImageVo } from '../../dataObjs/picGames/MatchImageVo';


interface MyProps { matchRows: MatchImageVo[], editRowCallback: Function, addNewCallback: Function, prevGame: Function }
interface MyState { matchRows: MatchImageVo[] }

class RowScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { matchRows: this.props.matchRows };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ matchRows: this.props.matchRows });
        }
    }
    getRows() {
        let rowArray: JSX.Element[] = [];
        let matchRows: MatchImageVo[] = this.state.matchRows;
        for (let i: number = 0; i < matchRows.length; i++) {
            let key: string = "matchRow" + i.toString();
            rowArray.push(<PicRow editCallback={this.props.editRowCallback} index={i} key={key} matchImages={matchRows[i]}></PicRow>);
            
        }
        return rowArray;
    }
    render() {
        return (<div>
            {this.getRows()}
            <hr />
            <Row><Col sm="6"><Button onClick={() => { this.props.addNewCallback() }}>Add New</Button></Col><Col sm="6"><Button variant='success' onClick={() => { this.props.prevGame() }}>Preview</Button></Col></Row>

        </div>)
    }
}
export default RowScreen;