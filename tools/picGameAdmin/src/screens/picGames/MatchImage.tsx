import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { MainController } from '../../classes/core/MainController';
import MainStorage from '../../classes/core/MainStorage';
import PicCard from '../../classes/picGame/PicCard';

import { GameWordVo } from '../../dataObjs/picGames/GameWordVo';
import { MatchImageVo } from '../../dataObjs/picGames/MatchImageVo';
import { WordPicVo } from '../../dataObjs/picGames/WordPicVo';


interface MyProps { matchGameData: MatchImageVo,doneCallback:Function }
interface MyState { matchGameData: MatchImageVo, changeIndex: number,mediaOpen:boolean,textOpen:boolean }
/**
 * match to first image game
 */
class MatchImage extends Component<MyProps, MyState>
{

    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { matchGameData: this.props.matchGameData, changeIndex: -1,mediaOpen:false,textOpen:false }

    }

    getCards() {
        let gameWords: GameWordVo[] = this.state.matchGameData.cards.slice();

        if (gameWords.length < 3) {
            return "Not enough cards error";
        }

        let cards: JSX.Element[] = [];
        for (let i: number = 0; i < gameWords.length; i++) {
            let key: string = "picCard" + i.toString();
            let selected: boolean = (this.state.changeIndex === i);

            cards.push(<Col key={key} lg={4}><PicCard index={i} selected={selected} gamePic={gameWords[i]} selectCallback={this.changeCorrect.bind(this)} changePicCallback={this.changePic.bind(this)}></PicCard></Col>)
        }
        return cards;
    }
    changeCorrect(index:number)
    {
        let matchData: MatchImageVo = this.state.matchGameData;
        let matchGameData: GameWordVo[] = matchData.cards;
        matchGameData[index].correct =! matchGameData[index].correct;
        

        this.setState({ matchGameData: matchData });
    }
    changePic(index: number) {
        this.setState({ changeIndex: index,mediaOpen:true });
        this.mc.showMediaLib();
        this.mc.mediaChangeCallback = this.picSelected.bind(this);
        this.mc.closeMediaCallback = this.onMediaClose.bind(this);
    }
    picSelected(wordPicVo: WordPicVo) {
        let matchData: MatchImageVo = this.state.matchGameData;
        let matchGameData: GameWordVo[] = matchData.cards;
        matchGameData[this.state.changeIndex].image = wordPicVo.image;
        matchGameData[this.state.changeIndex].word = wordPicVo.word;

        this.mc.mediaClose();
        this.setState({ matchGameData: matchData,mediaOpen:false });
    }
    
    changeText()
    {
       // console.log("old text="+oldText);

        this.ms.editText=this.state.matchGameData.instructions;
        this.mc.textEditCallback=this.updateText.bind(this);
        this.mc.onTextEditClose=this.textEditOnClose.bind(this);
        this.mc.openTextEditor();
        this.setState({textOpen:true});
    }
    textEditOnClose()
    {
        this.setState({textOpen:false});
    }
    updateText(newText:string)
    {
        let matchGameData:MatchImageVo=this.state.matchGameData;
        matchGameData.instructions=newText;
        this.setState({matchGameData:matchGameData,textOpen:false});
    }
    onMediaClose() {
        this.setState({ changeIndex: -1,mediaOpen:false });
    }
    render() {
        if (this.state.mediaOpen===true || this.state.textOpen===true)
        {
            return "";
        }
        return (<div>
            <Row><Col lg={8} ><span className="headTitle">Instructions:</span>{this.props.matchGameData.instructions}</Col><Col lg={4}><Button onClick={this.changeText.bind(this)}>Change</Button></Col></Row>
           <hr/>
            <Row>
                {this.getCards()}
            </Row>
            <hr/>
            <div className="tac"><Button variant="success" onClick={()=>{this.props.doneCallback()}}>Done</Button></div>
        </div>)
    }
}
export default MatchImage;