import React, { Component } from 'react';
import { MainController } from '../../classes/core/MainController';
import MainStorage from '../../classes/core/MainStorage';
import { MatchImageVo } from '../../dataObjs/picGames/MatchImageVo';
import MatchImage from './MatchImage';
import MatchText from './MatchText';
import RowScreen from './RowScreen';
interface MyProps { previewGame: Function }
interface MyState { mode: number, matchRows: MatchImageVo[] }
class PicGameManager extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0, matchRows: this.ms.matchRows };
    }
    addNewRow() {
        this.ms.addBlank();
        this.setState({ matchRows: this.ms.matchRows });
    }
    editRow(index: number) {
        this.ms.matchGameData = this.ms.matchRows[index];
        this.setState({ mode: 1 });
    }
    editDone() {
        this.setState({ mode: 0 })
    }
    
    getScreen() {
        switch (this.state.mode) {

            case 0:
                return <RowScreen prevGame={()=>{this.props.previewGame()}} addNewCallback={this.addNewRow.bind(this)} editRowCallback={this.editRow.bind(this)} matchRows={this.state.matchRows}></RowScreen>
            case 1:
                if (this.ms.gameID === 2) {
                    return <MatchText doneCallback={this.editDone.bind(this)} matchGameData={this.ms.matchGameData}></MatchText>
                }
                return <MatchImage doneCallback={this.editDone.bind(this)} matchGameData={this.ms.matchGameData}></MatchImage>

        }

        return "";
    }
    render() {
        return (<div>{this.getScreen()}</div>)
    }
}
export default PicGameManager;