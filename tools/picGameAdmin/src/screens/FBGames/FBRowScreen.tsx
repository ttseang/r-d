import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import MainStorage from '../../classes/core/MainStorage';
import FBRow from '../../classes/fbGame/FBRow';
import FBRow2 from '../../classes/fbGame/FBRow2';
import FBRow3 from '../../classes/fbGame/FBRow3';
import { CallbackVo } from '../../dataObjs/core/CallbackVo';
import { FBRowVo } from '../../dataObjs/fibGames/FBRowVo';
interface MyProps { rowsData: FBRowVo[], callbackVo: CallbackVo, prevGame: Function }
interface MyState { rowsData: FBRowVo[] }
class FBRowScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { rowsData: this.props.rowsData };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (this.props !== oldProps) {
            this.setState({ rowsData: this.props.rowsData });
        }
    }
    getRows() {


        console.log("get rows game id="+this.ms.gameInfo.id);

        let rowArray: JSX.Element[] = [];
        let rowsData: FBRowVo[] = this.state.rowsData;

        for (let i: number = 0; i < rowsData.length; i++) {
            let key: string = "letterRow" + i.toString();

            switch(this.ms.gameInfo.id)
            {
                case 8:
                case 11:
               
                    rowArray.push(<FBRow3 key={key} index={i} editCallback={this.props.callbackVo.editCallback} deleteCallback={this.props.callbackVo.deleteCallback} rowData={rowsData[i]}></FBRow3>)
                break;

                case 9:
                case 10:
                case 13:
                    rowArray.push(<FBRow2 key={key} index={i} editCallback={this.props.callbackVo.editCallback} deleteCallback={this.props.callbackVo.deleteCallback} rowData={rowsData[i]}></FBRow2>)
                 break;

                 default:
                    rowArray.push(<FBRow key={key} index={i} editCallback={this.props.callbackVo.editCallback} deleteCallback={this.props.callbackVo.deleteCallback} rowData={rowsData[i]}></FBRow>)
                 break;
            }

          /*   if (this.ms.gameInfo.id === 9 || this.ms.gameInfo.id===10) {
                rowArray.push(<FBRow2 key={key} index={i} editCallback={this.props.callbackVo.editCallback} deleteCallback={this.props.callbackVo.deleteCallback} rowData={rowsData[i]}></FBRow2>)
            }
            else {
                rowArray.push(<FBRow key={key} index={i} editCallback={this.props.callbackVo.editCallback} deleteCallback={this.props.callbackVo.deleteCallback} rowData={rowsData[i]}></FBRow>)
                // rowArray.push(<LetterRow key={key} index={i} deleteCallback={this.props.callbackVo.deleteCallback} editCallback={this.props.callbackVo.editCallback} letterRowVo={rowData[i]}></LetterRow>);
            } */
        }
        return rowArray;
    }
    render() {
        return (<div>
            <h5>Levels</h5>
            <hr />
            <div>{this.getRows()}</div>
            <hr />
            <Row><Col sm="6"><Button onClick={() => { this.props.callbackVo.addNew() }}>Add New Level</Button></Col><Col sm="6"><Button variant='success' onClick={() => { this.props.prevGame() }}>Preview</Button></Col></Row>

        </div>)
    }
}
export default FBRowScreen;