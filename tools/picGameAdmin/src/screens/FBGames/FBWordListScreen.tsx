import React, { Component } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import ListBuilder from '../../comps/listComps/ListBuilder';
interface MyProps { words: string[], callback: Function, cancelCallback: Function }
interface MyState { words: string[] }
class FbWordListScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { words: this.props.words };
    }

    getButtons() {
        return (<div className='tac'><Row><Col sm={4}><Button onClick={()=>{this.props.callback(this.state.words)}}>Update</Button></Col><Col><Button onClick={()=>{this.props.cancelCallback()}}>Cancel</Button></Col></Row></div>)
    }
    updateWords(words:string[])
    {
        this.setState({words:words});
    }
    render() {
        return (<div>
            <Row><Col>
            <ListBuilder callback={this.updateWords.bind(this)} words={this.state.words}></ListBuilder>
            </Col></Row>
            <hr/>
            {this.getButtons()}
        </div>)
    }
}
export default FbWordListScreen;