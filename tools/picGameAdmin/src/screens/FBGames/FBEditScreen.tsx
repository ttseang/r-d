import { ChangeEvent, Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { ButtonConstants } from '../../classes/core/ButtonConstants';
import MainStorage from '../../classes/core/MainStorage';
import LetterBox from '../../classes/letterGame/LetterBox';
import LetterBox2 from '../../classes/letterGame/LetterBox2';
import ButtonLine from '../../comps/ButtonLine';
import { ButtonVo } from '../../dataObjs/core/ButtonVo';
import { CallbackVo } from '../../dataObjs/core/CallbackVo';
import { FBRowVo } from '../../dataObjs/fibGames/FBRowVo';
import { LetterVo } from '../../dataObjs/letterGames/LetterVo';
import ImageBox from '../core/ImageBox';
interface MyProps { rowData: FBRowVo, callbackVo: CallbackVo }
interface MyState { rowData: FBRowVo, editMode: number, buttons: ButtonVo[] }
class FBEditScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);

        let startButtons: ButtonVo[] = ButtonConstants.LETTER_EDIT_BUTTONS;
        if (this.ms.gameInfo.id === 13) {
            startButtons = ButtonConstants.LETTER_EDIT_BUTTONS2;
        }
        this.state = { rowData: this.props.rowData, editMode: 0, buttons: startButtons };
    }

    getLetters() {
        let letterArray: JSX.Element[] = [];

        let gameSwitchArray: number[] = [8, 9, 10, 11, 13];

        if (!gameSwitchArray.includes(this.ms.gameInfo.id)) {
            letterArray.push(<Col sm={12} key="levelImage"><ImageBox image={this.state.rowData.image} callback={this.props.callbackVo.changePic.bind(this)}></ImageBox></Col>)
        }
        for (let i: number = 0; i < this.props.rowData.letters.length; i++) {
            let letterVo: LetterVo = this.props.rowData.letters[i];
            let key: string = "letterBox" + i.toString();

            let colSize:number=4;

            if (this.ms.gameInfo.id===13)
            {
                colSize=12;
            }

            if (this.ms.gameInfo.id === 8 || this.ms.gameInfo.id === 11 || this.ms.gameInfo.id === 13) {
                letterArray.push(<Col sm={colSize} key={key}><LetterBox2 editMode={this.state.editMode} index={i} letterVo={letterVo} deleteCallback={this.props.callbackVo.deleteCallback} selectCallback={this.props.callbackVo.toggleCallback.bind(this)} textEditCallback={this.props.callbackVo.changeText}></LetterBox2></Col>)
            }
            else {

                letterArray.push(<Col sm={colSize} key={key}><LetterBox editMode={this.state.editMode} index={i} letterVo={letterVo} deleteCallback={this.props.callbackVo.deleteCallback} selectCallback={this.props.callbackVo.toggleCallback.bind(this)} textEditCallback={this.props.callbackVo.changeText}></LetterBox></Col>)
            }
        }
        return (<Row>{letterArray}</Row>);
    }
    doAction(index: number, action: number) {

        console.log(action);

        switch (action) {
            case 1:
                let rowData: FBRowVo = this.state.rowData;
                let letters: LetterVo[] = rowData.letters;
                letters.push(new LetterVo("A", false));

                this.setState({ rowData: rowData });
                break;

            case 2:
                this.props.callbackVo.doneCallback();
                break;

            case 3:

                this.setState({ editMode: 1, buttons: ButtonConstants.LETTER_EDIT_BUTTONS_DEL });
                break;

            case 4:
                if (this.ms.gameInfo.id === 13) {
                    this.setState({editMode:0,buttons:ButtonConstants.LETTER_EDIT_BUTTONS2});
                }
                this.setState({ editMode: 0, buttons: ButtonConstants.LETTER_EDIT_BUTTONS })
                break;
        }
    }
    onBlankChange(e: ChangeEvent<HTMLInputElement>) {
        let rowData: FBRowVo = this.state.rowData;
        rowData.blank = e.currentTarget.value;
        this.setState({ rowData: rowData });
    }

    getBlankInput() {
        return (<input type='text' value={this.state.rowData.blank} onChange={this.onBlankChange.bind(this)} />)
    }
    render() {
        return (<div>
            <div>Word (use an underscore _ where the blank should be)</div>
            {this.getBlankInput()}
            <hr />
            <div className="scroll1">{this.getLetters()}</div>

            <hr />
            <div><ButtonLine buttonArray={this.state.buttons} actionCallback={this.doAction.bind(this)}></ButtonLine></div>
        </div>)

    }
}
export default FBEditScreen;