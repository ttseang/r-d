import React, { Component } from 'react';
import { MainController } from '../../classes/core/MainController';
import MainStorage from '../../classes/core/MainStorage';
import WordListCard from '../../classes/fbGame/WordListCard';
import InstructCard from '../../classes/picGame/InstructCard';
import { CallbackVo } from '../../dataObjs/core/CallbackVo';
import { FBRowVo } from '../../dataObjs/fibGames/FBRowVo';
import { FillBlankGame } from '../../dataObjs/fibGames/FillBlankGame';
import { LetterVo } from '../../dataObjs/letterGames/LetterVo';
import { WordPicVo } from '../../dataObjs/picGames/WordPicVo';
import FBEditScreen from './FBEditScreen';
import FBRowScreen from './FBRowScreen';
import FbWordListScreen from './FBWordListScreen';
interface MyProps {previewGame: Function, showMediaLibrary: Function}
interface MyState {mode:number,gameData:FillBlankGame,textEditOpen: boolean, mediaOpen: boolean }
class FBGameMager extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();
    private editRowIndex: number = 0;
    private letterIndex: number = 0;

        constructor(props: MyProps) {
            super(props);
            this.state = {mode:0,gameData:this.ms.fillBlankGame,textEditOpen:false,mediaOpen:false};
        }
        componentDidMount()
        {
            /* this.ms.fillBlankGame.cards=[];
            this.ms.addBlank();
            this.ms.fbRowVo=this.ms.fillBlankGame.cards[0]; */

            this.mc.onTextEditClose=this.onTextClose.bind(this);
        }
        onTextClose()
        {
            this.setState({textEditOpen:false})
        }
        addNewRow() {
            this.ms.addBlank();
    
            this.setState({gameData:this.ms.fillBlankGame});
          //  this.setState({ letterRowsVo: this.ms.fillBlankGame.cards });
            // this.setState({ matchRows: this.ms.matchRows });
        }
        editRow(index: number) {
            this.editRowIndex = index;
            this.ms.fbRowVo = this.ms.fillBlankGame.cards[index];
            this.setState({ mode: 1 });
        }
        deleteRow(index:number)
        {
            let cards:FBRowVo[]=this.state.gameData.cards.slice();
            cards.splice(index,1);
            this.ms.fillBlankGame.cards=cards;
    
            this.setState({gameData:this.ms.fillBlankGame});
        }
        editDone() {
            this.setState({ mode: 0 })
        }
        changePic() {
            console.log("change pic");
            this.mc.mediaChangeCallback = this.updatePic.bind(this);
            this.mc.showMediaLib();
            this.setState({ mediaOpen: true });
        }
        updatePic(wordPicVo: WordPicVo) {
            let fbRowVo: FBRowVo = this.state.gameData.cards[this.editRowIndex];
            fbRowVo.image = wordPicVo.image;
    
            let letterRows: FBRowVo[] = this.state.gameData.cards.slice();
            letterRows[this.editRowIndex] = fbRowVo;
    
            this.ms.fillBlankGame.cards=letterRows;
    
            this.mc.mediaClose();
    
            this.setState({ gameData:this.ms.fillBlankGame, mediaOpen: false });
        }
        deleteBox(index: number) {
            let fbRowVo: FBRowVo = this.state.gameData.cards[this.editRowIndex];
            let letters: LetterVo[] = fbRowVo.letters.slice();
            console.log(letters);

            letters.splice(index, 1);
    
            fbRowVo.letters = letters;
    
            let letterRows: FBRowVo[] = this.state.gameData.cards.slice();
            letterRows[this.editRowIndex] = fbRowVo;
            this.ms.fillBlankGame.cards=letterRows;
    
            this.setState({ gameData:this.ms.fillBlankGame });
    
    
        }
        toggleSelected(index: number) {
            let fbRowVo: FBRowVo = this.state.gameData.cards[this.editRowIndex]
            let letter: LetterVo = fbRowVo.letters[index];
            letter.correct = !letter.correct;
    
            let letterRows: FBRowVo[] = this.state.gameData.cards.slice();
            letterRows[this.editRowIndex] = fbRowVo;
    
            this.ms.fillBlankGame.cards=letterRows;
    
            this.setState({ gameData:this.ms.fillBlankGame });
        }
        changeText(index: number) {
            console.log(index);
            let fbRowVo: FBRowVo = this.state.gameData.cards[this.editRowIndex]
            let letter: LetterVo = fbRowVo.letters[index];
            this.ms.editText = letter.letter;
    
            this.letterIndex = index;
            this.mc.textEditCallback = this.updateText.bind(this);
            this.mc.openTextEditor();
            this.setState({ textEditOpen: true });
        }
    
        updateText(text: string) {
            let fbRowVo: FBRowVo = this.state.gameData.cards[this.editRowIndex];
            let letter: LetterVo = fbRowVo.letters[this.letterIndex];
            letter.letter = text;
    
            let letterRows: FBRowVo[] = this.state.gameData.cards.slice();
            letterRows[this.editRowIndex] = fbRowVo;
            this.ms.fillBlankGame.cards=letterRows;
    
    
            this.mc.closeTextEditor();
            this.setState({ gameData:this.ms.fillBlankGame, textEditOpen: false });
        }
        editInstructions()
        {
            this.ms.editText=this.ms.fillBlankGame.instructions;
            this.mc.openTextEditor();
            this.mc.textEditCallback=this.updateInstruction.bind(this);
            this.setState({textEditOpen:true});
        }
        updateInstruction(text:string)
        {
            this.ms.fillBlankGame.instructions=text;
            this.mc.closeTextEditor();
            this.setState({ gameData:this.ms.fillBlankGame, textEditOpen: false });
        }
        getInstructions()
        {
            if (this.state.mode!==0 || this.state.textEditOpen===true || this.state.mediaOpen===true)
            {
                return "";
            }
            return <InstructCard text={this.state.gameData.instructions} editCallback={this.editInstructions.bind(this)}></InstructCard>
                
        }
        getWordList()
        {
            if (this.ms.gameInfo.id===12 && this.state.mode===0)
            {
                return (<WordListCard words={this.ms.fillBlankGame.wordList} editCallback={this.changeWordList.bind(this)}></WordListCard>)
               // return (<div><hr/><Row className="tac"><Col sm={2}>Word List</Col><Col sm={6}>{this.ms.fillBlankGame.wordList.join(",")}</Col><Col sm={2}><Button variant='warning' onClick={this.changeWordList.bind(this)}>Change</Button></Col></Row><hr/></div>)
            }
            return "";
        }
        changeWordList()
        {
            this.setState({mode:2});
        }
        getScreen() {
            if (this.state.mediaOpen === true || this.state.textEditOpen === true) {
                return "";
            }
    
            switch (this.state.mode) {
    
                case 0:

                    let cb2:CallbackVo=new CallbackVo();
                    cb2.addNew=this.addNewRow.bind(this);
                    cb2.deleteCallback=this.deleteRow.bind(this);
                    cb2.editCallback=this.editRow.bind(this)

                    return (<FBRowScreen rowsData={this.state.gameData.cards} callbackVo={cb2} prevGame={this.props.previewGame}></FBRowScreen>)
                    
                case 1:
                    let cb:CallbackVo=new CallbackVo();
                    cb.changePic=this.changePic.bind(this);
                    cb.doneCallback=this.editDone.bind(this);
                    cb.deleteCallback=this.deleteBox.bind(this);
                    cb.toggleCallback=this.toggleSelected.bind(this);
                    cb.changeText=this.changeText.bind(this);
                
                    return (<FBEditScreen rowData={this.ms.fbRowVo} callbackVo={cb}></FBEditScreen>)
                case 2:
                    return <FbWordListScreen words={this.ms.fillBlankGame.wordList} callback={this.updateWordList.bind(this)} cancelCallback={this.cancelWords.bind(this)}></FbWordListScreen>
            }
            return "";
        }
        updateWordList(words:string[])
        {
            this.ms.fillBlankGame.wordList=words;
            this.setState({mode:0});
            console.log(this.ms);
        }
        cancelWords()
        {
            this.setState({mode:0});
        }
    render() {
        return (<div>
            {this.getInstructions()}
            {this.getWordList()}
            {this.getScreen()}</div>)
    }
}
export default FBGameMager;