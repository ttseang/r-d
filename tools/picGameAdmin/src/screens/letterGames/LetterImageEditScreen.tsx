import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { ButtonConstants } from '../../classes/core/ButtonConstants';
import LetterBox from '../../classes/letterGame/LetterBox';
import ButtonLine from '../../comps/ButtonLine';
import { ButtonVo } from '../../dataObjs/core/ButtonVo';
import { CallbackVo } from '../../dataObjs/core/CallbackVo';

import { LetterRowVo } from '../../dataObjs/letterGames/LetterRowVo';
import { LetterVo } from '../../dataObjs/letterGames/LetterVo';
import ImageBox from '../core/ImageBox';

interface MyProps { letterRowVo: LetterRowVo, callbackVo:CallbackVo }
interface MyState { letterRowVo: LetterRowVo,editMode:number,buttons:ButtonVo[] }
class LetterImageEditScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { letterRowVo: this.props.letterRowVo,editMode:0,buttons:ButtonConstants.LETTER_EDIT_BUTTONS };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ letterRowVo: this.props.letterRowVo });
        }
    }

    getLetters() {
        let letterArray: JSX.Element[] = [];

        letterArray.push(<Col sm={12} key="levelImage"><ImageBox image={this.state.letterRowVo.image} callback={this.props.callbackVo.changePic.bind(this)}></ImageBox></Col>)

        for (let i: number = 0; i < this.props.letterRowVo.letters.length; i++) {
            let letterVo: LetterVo = this.props.letterRowVo.letters[i];
            let key: string = "letterBox" + i.toString();

            letterArray.push(<Col sm={4} key={key}><LetterBox editMode={this.state.editMode} index={i} letterVo={letterVo} deleteCallback={this.props.callbackVo.deleteCallback} selectCallback={this.props.callbackVo.toggleCallback.bind(this)} textEditCallback={this.props.callbackVo.changeText}></LetterBox></Col>)
        }
        return (<Row>{letterArray}</Row>);
    }

    doAction(index: number, action: number) {
        console.log(action);

        switch (action) {
            case 1:
                let letterRowVo: LetterRowVo = this.state.letterRowVo;
                let letters: LetterVo[] = letterRowVo.letters;
                letters.push(new LetterVo("A", false));

                this.setState({ letterRowVo: letterRowVo });
                break;

            case 2:
                this.props.callbackVo.doneCallback();
            break;

            case 3:

                this.setState({editMode:1,buttons:ButtonConstants.LETTER_EDIT_BUTTONS_DEL});
            break;
            
            case 4:
                this.setState({editMode:0,buttons:ButtonConstants.LETTER_EDIT_BUTTONS})
                break;
        }
    }
    render() {
        return (<div>
            <div className="scroll1">{this.getLetters()}</div>
            <hr />
            <div><ButtonLine buttonArray={this.state.buttons} actionCallback={this.doAction.bind(this)}></ButtonLine></div>
        </div>)
    }
}
export default LetterImageEditScreen;