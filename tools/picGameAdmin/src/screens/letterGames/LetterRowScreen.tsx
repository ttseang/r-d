import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';

import { MainStorage } from '../../classes/core/MainStorage';
import LetterRow from '../../classes/letterGame/LetterRow';
import { CallbackVo } from '../../dataObjs/core/CallbackVo';
import { LetterRowVo } from '../../dataObjs/letterGames/LetterRowVo';
interface MyProps { letterRowsData: LetterRowVo[],callbackVo:CallbackVo,prevGame:Function}
interface MyState { letterRowsData: LetterRowVo[] }
class LetterRowScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props)
        this.state = { letterRowsData: this.props.letterRowsData };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (this.props !== oldProps) {
            this.setState({ letterRowsData: this.props.letterRowsData });
        }
    }
    getRows() {
        let rowArray: JSX.Element[] = [];
        let rowData: LetterRowVo[] = this.state.letterRowsData;

        for (let i: number = 0; i < rowData.length; i++) {
            let key: string = "letterRow" + i.toString();
            rowArray.push(<LetterRow key={key} index={i} deleteCallback={this.props.callbackVo.deleteCallback} editCallback={this.props.callbackVo.editCallback} letterRowVo={rowData[i]}></LetterRow>);

        }
        return rowArray;
    }
    render() {
         return (<div>
             <h5>Levels</h5>
             <hr/>
            <div>{this.getRows()}</div>
            <hr />
            <Row><Col sm="6"><Button onClick={() => { this.props.callbackVo.addNew() }}>Add New Level</Button></Col><Col sm="6"><Button variant='success' onClick={() => { this.props.prevGame() }}>Preview</Button></Col></Row>

        </div>)
    }
}
export default LetterRowScreen;