import React, { Component } from 'react';
import { MainController } from '../../classes/core/MainController';
import MainStorage from '../../classes/core/MainStorage';
import InstructCard from '../../classes/picGame/InstructCard';
import { CallbackVo } from '../../dataObjs/core/CallbackVo';
import { LetterRowVo } from '../../dataObjs/letterGames/LetterRowVo';
import { LetterVo } from '../../dataObjs/letterGames/LetterVo';
import { SingleLetterGame } from '../../dataObjs/letterGames/SingleLetterGame';
import { WordPicVo } from '../../dataObjs/picGames/WordPicVo';
import LetterImageEditScreen from './LetterImageEditScreen';
import LetterRowScreen from './LetterRowScreen';
interface MyProps { previewGame: Function, showMediaLibrary: Function }
interface MyState { mode: number,gameData:SingleLetterGame, textEditOpen: boolean, mediaOpen: boolean }
class LetterGameManager extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private editRowIndex: number = 0;
    private letterIndex: number = 0;
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0,gameData:this.ms.letterGameData, textEditOpen: false, mediaOpen: false };
        // /letterRowsVo: this.ms.letterGameData.cards,
    }
    componentDidMount()
    {
        this.mc.onTextEditClose=this.onTextClose.bind(this);
    }
    onTextClose()
    {
        this.setState({textEditOpen:false})
    }
    addNewRow() {
        this.ms.addBlank();

        this.setState({gameData:this.ms.letterGameData});
      //  this.setState({ letterRowsVo: this.ms.letterGameData.cards });
        // this.setState({ matchRows: this.ms.matchRows });
    }
    editRow(index: number) {
        this.editRowIndex = index;
        this.ms.letterRowVo = this.ms.letterGameData.cards[index];
        this.setState({ mode: 1 });
    }
    deleteRow(index:number)
    {
        let cards:LetterRowVo[]=this.state.gameData.cards.slice();
        cards.splice(index,1);
        this.ms.letterGameData.cards=cards;

        this.setState({gameData:this.ms.letterGameData});
    }
    editDone() {
        this.setState({ mode: 0 })
    }
    changePic() {
        console.log("change pic");
        this.mc.mediaChangeCallback = this.updatePic.bind(this);
        this.mc.showMediaLib();
        this.setState({ mediaOpen: true });
    }
    updatePic(wordPicVo: WordPicVo) {
        let letterRowVo: LetterRowVo = this.state.gameData.cards[this.editRowIndex];
        letterRowVo.image = wordPicVo.image;

        let letterRows: LetterRowVo[] = this.state.gameData.cards.slice();
        letterRows[this.editRowIndex] = letterRowVo;

        this.ms.letterGameData.cards=letterRows;

        this.mc.mediaClose();

        this.setState({ gameData:this.ms.letterGameData, mediaOpen: false });
    }
    deleteBox(index: number) {
        let letterRowVo: LetterRowVo = this.state.gameData.cards[this.editRowIndex];
        let letters: LetterVo[] = letterRowVo.letters.slice();

        letters.splice(index, 1);

        letterRowVo.letters = letters;

        let letterRows: LetterRowVo[] = this.state.gameData.cards.slice();
        letterRows[this.editRowIndex] = letterRowVo;
        this.ms.letterGameData.cards=letterRows;

        this.setState({ gameData:this.ms.letterGameData });


    }
    toggleSelected(index: number) {
        let letterRowVo: LetterRowVo = this.state.gameData.cards[this.editRowIndex]
        let letter: LetterVo = letterRowVo.letters[index];
        letter.correct = !letter.correct;

        let letterRows: LetterRowVo[] = this.state.gameData.cards.slice();
        letterRows[this.editRowIndex] = letterRowVo;

        this.ms.letterGameData.cards=letterRows;

        this.setState({ gameData:this.ms.letterGameData });
    }
    changeText(index: number) {
        console.log(index);
        let letterRowVo: LetterRowVo = this.state.gameData.cards[this.editRowIndex]
        let letter: LetterVo = letterRowVo.letters[index];
        this.ms.editText = letter.letter;

        this.letterIndex = index;
        this.mc.textEditCallback = this.updateText.bind(this);
        this.mc.openTextEditor();
        this.setState({ textEditOpen: true });
    }

    updateText(text: string) {
        let letterRowVo: LetterRowVo = this.state.gameData.cards[this.editRowIndex];
        let letter: LetterVo = letterRowVo.letters[this.letterIndex];
        letter.letter = text;

        let letterRows: LetterRowVo[] = this.state.gameData.cards.slice();
        letterRows[this.editRowIndex] = letterRowVo;
        this.ms.letterGameData.cards=letterRows;


        this.mc.closeTextEditor();
        this.setState({ gameData:this.ms.letterGameData, textEditOpen: false });
    }
    editInstructions()
    {
        this.ms.editText=this.ms.letterGameData.instructions;
        this.mc.openTextEditor();
        this.mc.textEditCallback=this.updateInstruction.bind(this);
        this.setState({textEditOpen:true});
    }
    updateInstruction(text:string)
    {
        this.ms.letterGameData.instructions=text;
        this.mc.closeTextEditor();
        this.setState({ gameData:this.ms.letterGameData, textEditOpen: false });
    }
    getInstructions()
    {
        if (this.state.mode===1 || this.state.mediaOpen===true || this.state.textEditOpen===true)
        {
            return "";
        }
        return <InstructCard text={this.state.gameData.instructions} editCallback={this.editInstructions.bind(this)}></InstructCard>
            
    }
    getScreen() {
        if (this.state.mediaOpen === true || this.state.textEditOpen === true) {
            return "";
        }

        switch (this.state.mode) {

            case 0:
                let cb2:CallbackVo=new CallbackVo();
                cb2.addNew=this.addNewRow.bind(this);
                cb2.deleteCallback=this.deleteRow;
                cb2.editCallback=this.editRow.bind(this)

                return <LetterRowScreen letterRowsData={this.state.gameData.cards} prevGame={this.props.previewGame} callbackVo={cb2}></LetterRowScreen>
            // return <RowScreen prevGame={()=>{this.props.previewGame()}} addNewCallback={this.addNewRow.bind(this)} editRowCallback={this.editRow.bind(this)} matchRows={this.state.matchRows}></RowScreen>
            case 1:

               // let cb:CallbackVo=new CallbackVo(this.changePic.bind(this),this.editDone.bind(this),this.deleteBox.bind(this),this.toggleSelected.bind(this),this.changeText.bind(this));
                let cb:CallbackVo=new CallbackVo();
                cb.changePic=this.changePic.bind(this);
                cb.doneCallback=this.editDone.bind(this);
                cb.deleteCallback=this.deleteBox;
                cb.toggleCallback=this.toggleSelected.bind(this);
                cb.changeText=this.changeText.bind(this);

                return <LetterImageEditScreen letterRowVo={this.ms.letterRowVo} callbackVo={cb}></LetterImageEditScreen>
            // return <MatchImage doneCallback={this.editDone.bind(this)} matchGameData={this.ms.matchGameData} showMediaLibCallback={this.props.showMediaLibrary.bind(this)}></MatchImage>

        }

        return "";
    }
    render() {
        return (<div>
            {this.getInstructions()}
            {this.getScreen()}</div>)
    }
}
export default LetterGameManager;