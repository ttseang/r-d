import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { MainStorage } from '../../classes/core/MainStorage';
import PicBox from '../../classes/picGame/PicBox';

import { WordPicVo } from '../../dataObjs/picGames/WordPicVo';


interface MyProps { library: WordPicVo[], closeCallback: Function }
interface MyState { library: WordPicVo[] }
class MediaScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props)
        this.state = { library: this.props.library };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (this.props !== oldProps) {
            this.setState({ library: this.props.library });
        }
    }
    private getCards() {
        let cardArray: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.library.length; i++) {
            let key:string="mediaImage"+i.toString();

            cardArray.push(<Col key={key} sm={6} md={3} lg={4} ><PicBox wordPicVo={this.state.library[i]}></PicBox></Col>);
        }
        return cardArray;
    }
    render() {
        return (<div>
            <Card id="mediaLib">
                <Card.Header className="headTitle">Media Library<Button className="btnClose" onClick={() => { this.props.closeCallback() }}>X</Button></Card.Header>
                <Card.Body>
                    <div className='scroll1'>
                        <Row>
                            {this.getCards()}
                        </Row>
                    </div>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default MediaScreen;