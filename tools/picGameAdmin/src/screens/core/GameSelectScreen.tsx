import { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import GameRow from '../../classes/core/GameRow';

import MainStorage from '../../classes/core/MainStorage';

interface MyProps {selectCallback:Function,infoCallback:Function }
interface MyState { }
class GameSelectScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getCards() {
        let cards: JSX.Element[] = [];
        for (let i: number = 0; i < this.ms.gameArray.length; i++) {
            let key:string="gameRow"+i.toString();

            cards.push(<Col key={key} sm={3}><GameRow selectCallback={this.props.selectCallback} gameVo={this.ms.gameArray[i]} infoCallback={this.props.infoCallback}></GameRow></Col>)
        }
        return cards;
    }
    render() {
        return (<div className='scroll1'>
            <Row>
            {this.getCards()}
            </Row>
        </div>)
    }
}
export default GameSelectScreen;