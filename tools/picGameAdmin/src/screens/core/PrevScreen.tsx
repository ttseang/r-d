import { Component } from 'react';
import { Button } from 'react-bootstrap';
import MainStorage from '../../classes/core/MainStorage';
interface MyProps { editCallback: Function }
interface MyState { }
class PrevScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }

    getGamePath() {
        //let path:string="./assets/games/soundslike/index.html";
        //let path:string="./assets/games/rhyme/index.html";
        //  let path:string="./assets/games/textpics/index.html";

       // let paths: string[] = ["./assets/games/soundslike/index.html", "./assets/games/rhyme/index.html", "./assets/games/textpics/index.html"];

        //let path: string = paths[this.ms.gameID];
        let path=this.ms.gameInfo.prevLink;
        return path;
    }
    doDownload() {

        let text:string=this.ms.getGameData();
        let filename = "game.json";
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
    render() {
        return (<div>
            <div id="gameData">{this.ms.getGameData()}</div>
            <iframe src={this.getGamePath()} id='gamePrev' title='game preview'></iframe>
            <hr />
            <div className='tac' onClick={() => { this.props.editCallback() }}><Button>Edit</Button></div>
            <hr/>
            <div className='tac' onClick={() => { this.doDownload() }}><Button>Download</Button></div>
        </div>)
    }
}
export default PrevScreen;