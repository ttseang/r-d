import React, { Component } from 'react';
import { Card, Button, ButtonGroup } from 'react-bootstrap';
import { GameVo } from '../../dataObjs/core/GameVo';
interface MyProps { gameVo: GameVo, selectCallback: Function, closeCallback: Function }
interface MyState { }
class GameInfoScreen extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    render() {
        return (<div>
            <Card className='gameCard'>
                <Card.Header>
                    {this.props.gameVo.gameName}</Card.Header>
                <Card.Body>

                   <div className='tac'> <img src={this.props.gameVo.image}  width='80%' alt={this.props.gameVo.gameName} /></div>
                    <hr/>
                    <div>{this.props.gameVo.description}</div>
                </Card.Body>
                <Card.Footer className="tac">
                    <ButtonGroup>
                    <Button variant='success' onClick={() => { this.props.selectCallback(this.props.gameVo.id) }}>Make This</Button>
                    <Button variant='primary' onClick={()=>{this.props.closeCallback()}}>Back</Button>
                    </ButtonGroup>
                </Card.Footer>
            </Card>
        </div>)
    }
}
export default GameInfoScreen;