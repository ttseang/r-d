import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
interface MyProps { image: string, callback: Function }
interface MyState { image: string }
class ImageBox extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { image: this.props.image };
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({image:this.props.image});
        }
    }
    getPic()
    {
        return "./assets/pics/" + this.state.image;
    }
    changePic()
    {
        this.props.callback();
    }
    render() {
        return (<div>
            <Card className='picCard'>
                <Card.Body>
                    <div className='cardImageHolder2'>
                        <img className='cardImage2' alt='levelpic' src={this.getPic()}   />
                    </div>
                </Card.Body>
                <Card.Footer className="cardHeader"><Button variant='success' onClick={()=>{this.changePic()}}>Change</Button></Card.Footer>
            </Card>
        </div>)
    }
}
export default ImageBox;