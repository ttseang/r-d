import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { MainController } from '../../classes/core/MainController';
import MainStorage from '../../classes/core/MainStorage';
interface MyProps {closeCallback:Function}
interface MyState { myText: string }
class TextEditor extends Component<MyProps, MyState>
{
   // private textVal:string="";

    private ms:MainStorage=MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();
   

        constructor(props: MyProps) {
            super(props);
            this.state = { myText: this.ms.editText };
        }
    componentDidMount()
    {
        //this.textRef.setFocus();
    }
    onTextChange()
    {
        let el: HTMLInputElement = document.getElementById(
            "textinput1"
          ) as HTMLInputElement;
         this.setState({myText:el.value});
          //this.myText = el.value;
          //console.log(this.myText);
    }
    editDone()
    {
        this.mc.textEditCallback(this.state.myText);
        this.mc.closeTextEditor();
    }
    cancelEdit()
    {
        this.mc.closeTextEditor();
    }
    enterPressed(e: React.KeyboardEvent<HTMLInputElement>) {
        if (e.key === "Enter") {
           this.editDone();
        }
      }
    render() {
        return (<div>
           <Card>
               <Card.Header>Edit Text</Card.Header>
               <Card.Body>
              <Row><Col>
            <input type='text'
              id="textinput1"
              name="textinput"
              autoFocus
              defaultValue={this.state.myText}
              className="form-control input-md"
              onKeyDown={this.enterPressed.bind(this)}
              onChange={() => this.onTextChange()}
            />
          </Col></Row>
          </Card.Body>
          <Card.Footer>
          <Row><Col className='tac'><Button variant='danger' onClick={this.cancelEdit.bind(this)}>cancel</Button></Col><Col className='tac'><Button variant='success' onClick={this.editDone.bind(this)}>Done</Button></Col></Row>
          </Card.Footer>
          </Card>
        </div>)
    }
}
export default TextEditor;