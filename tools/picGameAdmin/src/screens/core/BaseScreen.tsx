import { Component } from 'react';
import { Card } from 'react-bootstrap';
import GameInfoScreen from './GameInfoScreen';
import { MainController } from '../../classes/core/MainController';
import MainStorage from '../../classes/core/MainStorage';
import { GameVo } from '../../dataObjs/core/GameVo';
import LetterGameManager from '../letterGames/LetterGameManager';

import PicGameManager from '../picGames/PicGameManager';
import GameSelectScreen from './GameSelectScreen';
import MediaScreen from './MediaScreen';
import PrevScreen from './PrevScreen';
import TextEditor from './TextEditor';
import FBGameMager from '../FBGames/FBGameManager';

interface MyProps { }
interface MyState { mode: number, showMediaLib: boolean,showTextEdit:boolean }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 1,  showMediaLib: false,showTextEdit:false };
        this.mc.mediaClose=this.closeMediaLibrary.bind(this);
        this.mc.openTextEditor=this.showTextEditor.bind(this);
        this.mc.closeTextEditor=this.closeTextEdit.bind(this);
        this.mc.showMediaLib=this.showMediaLibrary.bind(this);
    }
   
    getGameManager()
    {
        
       if (this.ms.gameInfo.type===GameVo.TYPE_PIC_MATCH)
       {
       return <PicGameManager previewGame={this.previewGame.bind(this)}></PicGameManager>
       }
       if (this.ms.gameInfo.type===GameVo.TYPE_SINGLE_LETTER)
       {
        return <LetterGameManager previewGame={this.previewGame.bind(this)} showMediaLibrary={this.showMediaLibrary.bind(this)}></LetterGameManager>
       }
       if (this.ms.gameInfo.type===GameVo.TYPE_FILL_IN_THE_BLANK)
       {
           return <FBGameMager previewGame={this.previewGame.bind(this)} showMediaLibrary={this.showMediaLibrary.bind(this)}></FBGameMager>
       }
    }
    getScreen() {
        switch (this.state.mode) {

            case 0:
                return this.getGameManager();
           
            case 1:
                return <GameSelectScreen selectCallback={this.selectGame.bind(this)} infoCallback={this.getGameInfo.bind(this)}></GameSelectScreen>

            case 2:
                return <PrevScreen editCallback={this.editGame.bind(this)}></PrevScreen>

            case 3:
                return <GameInfoScreen gameVo={this.ms.infoGame} selectCallback={this.selectGame.bind(this)} closeCallback={()=>{ this.setState({mode:1})}}></GameInfoScreen>
        }

        return "";
    }
    editGame()
    {
        this.setState({mode:0})
    }
    selectGame(index: number) {
      
        this.ms.selectGame(index);
        this.setState({ mode: 0 })
    }
    getGameInfo(gameVo:GameVo)
    {
        this.ms.infoGame=gameVo;
        this.setState({mode:3});
    }
    previewGame()
    {
        this.setState({mode:2});
    }
    getMediaLib() {
        if (this.state.showMediaLib === false) {
            return "";
        }
        return <MediaScreen library={this.ms.library} closeCallback={this.closeMediaLibrary.bind(this)}></MediaScreen>
    }
    getTextEdit()
    {
        if (this.state.showTextEdit===false)
        {
            return "";
        }
        return <TextEditor closeCallback={this.closeTextEdit.bind(this)}></TextEditor>
    }
    showTextEditor()
    {
        console.log("open editor");
        this.setState({showTextEdit:true})
    }
    closeTextEdit()
    {
        this.mc.onTextEditClose();
        this.setState({showTextEdit:false});
    }
    showMediaLibrary() {
        this.setState({ showMediaLib: true });
    }
    closeMediaLibrary() {
        this.setState({ showMediaLib: false });
        this.mc.closeMediaCallback();
    }
   
    render() {
        return (<div id="base">
            <Card>
                <Card.Header className="head1">
                    <span className="titleText">Game Manager</span>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                    {this.getMediaLib()}
                    {this.getTextEdit()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;