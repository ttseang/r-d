import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';

interface MyProps {text:string,editCallback:Function}
interface MyState { }
class InstructCard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>            
            <Row><Col sm={2}>Instructions</Col><Col sm={6} className='tac'>{this.props.text.substring(0,100)}</Col>
            <Col sm={2} className='tac'><Button variant='warning' onClick={()=>{this.props.editCallback(this.props.text)}}>Change</Button></Col></Row>
            </Card.Body>
            </Card>
        </div>)
    }
}
export default InstructCard;