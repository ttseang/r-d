import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { GameWordVo } from '../../dataObjs/picGames/GameWordVo';
import MainStorage from '../core/MainStorage';


interface MyProps {index:number, gamePic: GameWordVo,changePicCallback:Function,selectCallback:Function,selected:boolean }
interface MyState {gamePic: GameWordVo }
class PicCard extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {gamePic:this.props.gamePic};
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({gamePic:this.props.gamePic});
        }
    }
    getPic() {
        return "./assets/pics/" + this.props.gamePic.image;
    }
    changeCorrect()
    {
        //do callback here
    }
    changePic()
    {
        console.log("Click");
        this.props.changePicCallback(this.props.index);
    }
    
    getButton()
    {
       if (this.props.index===0 && this.ms.gameID===1)
       {
           return "Question Image";
       }
       if (this.state.gamePic.correct===true)
       {

           return (<Button variant='success' onClick={()=>{this.props.selectCallback(this.props.index)}}><i className="far fa-smile-beam"></i></Button>)
       }
       return (<Button variant='danger' onClick={()=>{this.props.selectCallback(this.props.index)}}><i className="far fa-frown"></i></Button>)
    }
    render() {

        let sclass:string="picCardBody";
        if (this.props.selected===true)
        {
            sclass="picCardBodySelected";
        }

        return (<div>
            <Card className='picCard'>
                <Card.Header className="cardHeader">{this.props.gamePic.word}</Card.Header>
                <Card.Body className={sclass}>
                    <div className='cardImageHolder'>
                        <img className='cardImage' alt={this.props.gamePic.word} src={this.getPic()} onClick={()=>{this.changePic()}}/>
                    </div>
                </Card.Body>
                <Card.Footer className="tac">{this.getButton()}</Card.Footer>
            </Card>
        </div>)
    }
}
export default PicCard;