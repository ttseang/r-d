import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { MatchImageVo } from '../../dataObjs/picGames/MatchImageVo';
import { MainStorage } from '../core/MainStorage';

interface MyProps { index: number, matchImages: MatchImageVo,editCallback:Function }
interface MyState { }
class PicRow extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getImage(index: number) {
        return this.props.matchImages.cards[index];
    }
    getSClass(index:number)
    {
        if (this.props.matchImages.cards[index].correct===true)
        {
            return "picThumb";
        }
        return "picThumbWrong";
    }
    getRowContent()
    {
        

        if (this.ms.gameID===2)
        {
            return (
                <Row className="picRow">
                <Col lg={1}>{this.props.index}</Col> 
                <Col lg={2}>{this.props.matchImages.instructions.substring(0,20)}</Col>
                <Col lg={2}><img className={this.getSClass(0)} src={"./assets/pics/" + this.getImage(0).image} alt={this.getImage(0).word} height={50} width={50} /></Col>
                <Col lg={2}><img className={this.getSClass(1)} src={"./assets/pics/" + this.getImage(1).image} alt={this.getImage(1).word} height={50} width={50} /></Col>
                <Col lg={2}><Button onClick={()=>{this.props.editCallback(this.props.index)}}>Edit</Button></Col>
                <Col lg={2}><Button variant='danger'>Delete</Button></Col>
            </Row>
            )
        }
        return (
            <Row className="picRow">
            <Col lg={1}>{this.props.index}</Col> <Col lg={2}><img className={this.getSClass(0)} src={"./assets/pics/" + this.getImage(0).image} alt={this.getImage(0).word} height={50} width={50} /></Col>
            <Col lg={2}><img className={this.getSClass(1)} src={"./assets/pics/" + this.getImage(1).image} alt={this.getImage(1).word} height={50} width={50} /></Col>
            <Col lg={2}><img className={this.getSClass(2)} src={"./assets/pics/" + this.getImage(2).image} alt={this.getImage(2).word} height={50} width={50} /></Col>
            <Col lg={2}><Button onClick={()=>{this.props.editCallback(this.props.index)}}>Edit</Button></Col>
            <Col lg={2}><Button variant='danger'>Delete</Button></Col>
        </Row>
        )
    }
    render() {
        return (<div>
            <Card>
                {this.getRowContent()}                
            </Card></div>
        )
    }
}
export default PicRow;