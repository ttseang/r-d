import { GameVo } from "../../dataObjs/core/GameVo";
import { FBRowVo } from "../../dataObjs/fibGames/FBRowVo";
import { FillBlankGame } from "../../dataObjs/fibGames/FillBlankGame";
import { LetterRowVo } from "../../dataObjs/letterGames/LetterRowVo";
import { LetterVo } from "../../dataObjs/letterGames/LetterVo";
import { SingleLetterGame } from "../../dataObjs/letterGames/SingleLetterGame";
import { GameWordVo } from "../../dataObjs/picGames/GameWordVo";
import { MatchImageVo } from "../../dataObjs/picGames/MatchImageVo";
import { WordPicVo } from "../../dataObjs/picGames/WordPicVo";


export class MainStorage {

    private static instance: MainStorage;
    private gameMap: Map<number, GameVo> = new Map<number, GameVo>();

    public infoGame: GameVo;

    public word: string;
    public wordArray: string[] = [];
    public saveData: string = "";
    //
    //

    public library: WordPicVo[] = [];
    public matchGameData: MatchImageVo;
    public matchRows: MatchImageVo[] = [];
    public gameArray: GameVo[] = [];


    public letterGameData: SingleLetterGame;
    public letterRowVo: LetterRowVo;

    public fillBlankGame:FillBlankGame;
    public fbRowVo:FBRowVo;

    /* public mediaChangeCallback: Function = () => { };
    public closeMediaCallback: Function = () => { };
    public mediaClose: Function = () => { };


    public textEditCallback: Function = () => { };
    public openTextEditor: Function = () => { };
    public closeTextEditor: Function = () => { };
    public onTextEditClose: Function = () => { }; */

    public editText: string = "";

    public gameID: number = 0;
    public gameInfo:GameVo=new GameVo(0,"",0,"","","");

    // public defLetterPic:letterPicVo;

    constructor() {
        this.word = "";

        // this.defLetterPic=new letterPicVo("b")

        this.library.push(new WordPicVo("bear", "bear.png", false));
        this.library.push(new WordPicVo("bread", "bread.svg", false));
        this.library.push(new WordPicVo("bun", "bun.svg", false));
        this.library.push(new WordPicVo("cat", "cat.svg", false));
        this.library.push(new WordPicVo("chair", "chair.svg", false));
        this.library.push(new WordPicVo("chicken", "chicken.png", false));
        this.library.push(new WordPicVo("cow", "cow.png", false));
        this.library.push(new WordPicVo("crab", "crab.png", false));
        this.library.push(new WordPicVo("croc", "croc.png", false));
        this.library.push(new WordPicVo("duck", "duck.png", false));
        this.library.push(new WordPicVo("glass", "glass.svg", false));
        this.library.push(new WordPicVo("hammerhead", "hammerhead.png", false));
        this.library.push(new WordPicVo("hat", "hat.svg", false));


        /*  this.matchGameData=new MatchImageVo("do this",[]);
         this.matchGameData.pics.push(new GameWordVo("hat","hat.svg",false));
         this.matchGameData.pics.push(new GameWordVo("duck","duck.png",false));
         this.matchGameData.pics.push(new GameWordVo("cat","cat.svg",true)); */


        /**
         * DEFAULT DATA
         */

        let miv: MatchImageVo = this.addMatchImageVo("do something");
        this.addMatchPic(miv, new GameWordVo("hat", "hat.svg", false));
        this.addMatchPic(miv, new GameWordVo("duck", "duck.png", false));
        this.addMatchPic(miv, new GameWordVo("cat", "cat.svg", true));
        this.matchGameData = this.matchRows[0];

        let letterRows: LetterRowVo[] = [];
        letterRows.push(new LetterRowVo("hat.svg", [new LetterVo("A", true), new LetterVo("B", false), new LetterVo("C", false)]));
        this.letterGameData = new SingleLetterGame("do something", letterRows);
        this.letterRowVo = this.letterGameData.cards[0];
        

        this.fillBlankGame=new FillBlankGame("match something",[new FBRowVo("hat.svg","H_T",[new LetterVo("A", true), new LetterVo("B", false), new LetterVo("C", false)])]);
        this.fbRowVo=this.fillBlankGame.cards[0];
        this.fillBlankGame.wordList=["one","fish","blue","dish"];


        this.gameArray.push(new GameVo(0, "Select Cards", GameVo.TYPE_PIC_MATCH, "./assets/gameThumbs/0.png", "Pick x number of cards. For example pick the two cards that start with the same letter","./assets/games/soundslike/index.html"));
        this.gameArray.push(new GameVo(1, "Match To First Card", GameVo.TYPE_PIC_MATCH, "./assets/gameThumbs/1.png", "Pick the card which matches the first card. For example pick the card that rhymes with the first one","./assets/games/rhyme/index.html"));
        this.gameArray.push(new GameVo(2, "Pick by Text", GameVo.TYPE_PIC_MATCH, "./assets/gameThumbs/2.png", "Pick the card as described in the instructions. For example, pick the cards with the letters in the text.","./assets/games/textpics/index.html"));
        this.gameArray.push(new GameVo(3, "Single Letter Drag", GameVo.TYPE_SINGLE_LETTER, "./assets/gameThumbs/3.png", "Drag the right letter","./assets/games/singledrag/index.html"));
        this.gameArray.push(new GameVo(4, "Single Letter Click", GameVo.TYPE_SINGLE_LETTER, "./assets/gameThumbs/4.png", "Click the right letter","./assets/games/singleletterclick/index.html"));
        this.gameArray.push(new GameVo(5, "Single Word Click", GameVo.TYPE_SINGLE_LETTER, "./assets/gameThumbs/5.png", "Click the right word","./assets/games/singlewordclick/index.html"));
        this.gameArray.push(new GameVo(6,"Drag Fill-in-the-blank",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/6.png","",""));
        this.gameArray.push(new GameVo(7,"Drag Fill-in-the-blank 2",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/7.png","",""));
        this.gameArray.push(new GameVo(8,"Drag F-i-b text only",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/8.png","",""));
        this.gameArray.push(new GameVo(9,"Drag F-i-b Word",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/9.png","",""));
        this.gameArray.push(new GameVo(10,"Drag F-i-b Word",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/10.png","",""));
        this.gameArray.push(new GameVo(11,"Build A Word",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/11.png","",""));
        this.gameArray.push(new GameVo(12,"Drag f-i-b Word List",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/12.png","fill in the blank with word list",""));
        this.gameArray.push(new GameVo(13,"Find Hidden Word",GameVo.TYPE_FILL_IN_THE_BLANK,"./assets/gameThumbs/13.png","",""));



        this.infoGame = this.gameArray[0];

        //map games
        for (let i: number = 0; i < this.gameArray.length; i++) {
            let gameVo: GameVo = this.gameArray[i];
            this.gameMap.set(gameVo.id, gameVo);
        }
    }
    static getInstance(): MainStorage {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new MainStorage();
        }
        return this.instance;
    }
    public selectGame(gameID:number)
    {
        this.gameID=gameID;
        this.gameInfo=this.gameMap.get(gameID) || new GameVo(0,"not found",0,"","","");
        this.initRow();
    }
    public setRow(row: number) {
        this.matchGameData = this.matchRows[row];
    }
    private initRow()
    {
        let type:number=this.gameInfo.type;
        switch(type)
        {
            case GameVo.TYPE_PIC_MATCH:

                break;

            case GameVo.TYPE_SINGLE_LETTER:
            this.letterGameData.cards=[];
            this.addBlank();
            this.letterRowVo=this.letterGameData.cards[0];
            
            break;

            case GameVo.TYPE_FILL_IN_THE_BLANK:
                this.fillBlankGame.cards=[];
                this.addBlank();
                this.fbRowVo=this.fillBlankGame.cards[0];

            break;
        }
    }
    public addBlank() {

//        let type:number=this.gameMap.get(this.gameID)?.type || -1;
        let type:number=this.gameInfo.type;
        switch(type)
        {

        
        case GameVo.TYPE_PIC_MATCH:
            let miv: MatchImageVo = this.addMatchImageVo("do something");
            this.addMatchPic(miv, new GameWordVo("hat", "hat.svg", false));
            this.addMatchPic(miv, new GameWordVo("duck", "duck.png", false));
            this.addMatchPic(miv, new GameWordVo("cat", "cat.svg", true));
        break;

        case GameVo.TYPE_SINGLE_LETTER:
            switch(this.gameInfo.id)
            {
                case 5:
                    this.letterGameData.cards.push(new LetterRowVo("cat.svg", [new LetterVo("Cat", true), new LetterVo("Sat", false), new LetterVo("Jam", false)]));
                break;

                default:
                    this.letterGameData.cards.push(new LetterRowVo("hat.svg", [new LetterVo("A", true), new LetterVo("B", false), new LetterVo("C", false)]));
        
            }
          break;

        case GameVo.TYPE_FILL_IN_THE_BLANK:

            switch(this.gameInfo.id)
            {
                 case 8:
                    this.fillBlankGame.cards.push(new FBRowVo("hat.svg","beg", [new LetterVo("b", true), new LetterVo("e", true)]));
        
                break;
     
               case 9:
                    this.fillBlankGame.cards.push(new FBRowVo("","cap", [new LetterVo("nap", true), new LetterVo("sat", false)]));
        
                break;

                case 10:
                    this.fillBlankGame.cards.push(new FBRowVo("","Example:pen, marker, crayon", [new LetterVo("spicy", false), new LetterVo("city", false),new LetterVo("pencil", true), new LetterVo("price", false),new LetterVo("face", false), new LetterVo("cider", false)]));
                
                break;

                case 11:
                    this.fillBlankGame.cards.push(new FBRowVo("","JAM", [new LetterVo("M",true),new LetterVo("J",true),new LetterVo("A",true)]));
                
                break;

                case 13:
                    this.fillBlankGame.cards.push(new FBRowVo("","Tim", [new LetterVo("jepTim",true)]));
                
                break;

                default:

                    this.fillBlankGame.cards.push(new FBRowVo("hat.svg","H_T", [new LetterVo("A", true), new LetterVo("B", false), new LetterVo("C", false)]));
        
            }

           
            
        break;
    }
    }

    public addMatchImageVo(instructions: string) {
        let miv: MatchImageVo = new MatchImageVo(instructions, []);
        this.matchRows.push(miv);
        return miv;
    }
    public addMatchPic(miv: MatchImageVo, gw: GameWordVo) {
        miv.cards.push(gw);
    }
    private getTextRows() {
        let rowArray: any[] = [];
        for (let i: number = 0; i < this.matchRows.length; i++) {
            rowArray.push(this.matchRows[i].getTextFormat());
        }
        return rowArray;
    }
    public getGameData() {

        let gameType:number | undefined=this.gameMap.get(this.gameID)?.type;

        switch(gameType)
        {
            case GameVo.TYPE_PIC_MATCH:
                if (this.gameID === 2) {
                    return JSON.stringify({ "instructions": this.matchGameData.instructions, "levels": this.getTextRows() });
                }
                return JSON.stringify({ "instructions": this.matchGameData.instructions, "levels": this.matchRows });
        

            case GameVo.TYPE_SINGLE_LETTER:

              return JSON.stringify({"instructions":this.letterGameData.instructions,"levels":this.letterGameData.cards});
                

            case GameVo.TYPE_FILL_IN_THE_BLANK:

            if (this.gameInfo.id===12)
            {
                return JSON.stringify({"instructions":this.fillBlankGame.instructions,"levels":this.fillBlankGame.cards,"wordList":this.fillBlankGame.wordList});    
            }

            return JSON.stringify({"instructions":this.fillBlankGame.instructions,"levels":this.fillBlankGame.cards});
        }
        return "";      

    }
}
export default MainStorage;