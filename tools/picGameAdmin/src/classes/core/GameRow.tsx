import { Component } from 'react';

import { Button, Card } from 'react-bootstrap';
import { GameVo } from '../../dataObjs/core/GameVo';

interface MyProps { gameVo: GameVo, selectCallback: Function,infoCallback:Function }
interface MyState { }
class GameRow extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div>
            <Card className='gameCard'>
                <Card.Header className="ft">
                    {this.props.gameVo.gameName} <i className="fas fa-info-circle infoIcon" onClick={() => { this.props.infoCallback(this.props.gameVo) }}></i>
                </Card.Header>
                <Card.Body>

                    <img src={this.props.gameVo.image} width='100%' alt={this.props.gameVo.gameName} />
                </Card.Body>
                <Card.Footer className="tac"><Button variant='success' onClick={() => { this.props.selectCallback(this.props.gameVo.id) }}>Make This</Button></Card.Footer>
            </Card>
        </div>)
    }
}
export default GameRow;