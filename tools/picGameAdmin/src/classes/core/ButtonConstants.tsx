import { ButtonVo } from "../../dataObjs/core/ButtonVo";



export class ButtonConstants {
  static ADD_BUTTONS: ButtonVo[] = [
    new ButtonVo("Add tv seperator", "primary", 1),
    new ButtonVo("Add v seperator", "warning", 2),
    new ButtonVo("Add Conjunction End", "primary", 3),
    new ButtonVo("Add Blank Diagonal", "warning", 4)
  ];
  static LETTER_EDIT_BUTTONS:ButtonVo[]=[
    new ButtonVo("Add New Letter","primary",1),
    new ButtonVo("Delete Letters","danger",3),
    new ButtonVo("Done","success",2)
  ];
  static LETTER_EDIT_BUTTONS2:ButtonVo[]=[
    new ButtonVo("Done","success",2)
  ];
  
  static LETTER_EDIT_BUTTONS_DEL:ButtonVo[]=[
    new ButtonVo("Exit Delete Mode","success",4)
  ];
}
