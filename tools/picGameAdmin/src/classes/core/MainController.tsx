export class MainController
{
    private static instance:MainController | null=null;

    public mediaChangeCallback: Function = () => { };
    public closeMediaCallback: Function = () => { };
    public mediaClose: Function = () => { };
    public showMediaLib:Function=()=>{};

    public textEditCallback: Function = () => { };
    public openTextEditor: Function = () => { };
    public closeTextEditor: Function = () => { };
    public onTextEditClose: Function = () => { };
    
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor()
    {

    }
    public static getInstance():MainController
    {
        if (this.instance==null)
        {
            this.instance=new MainController();
        }
        return this.instance;
    }
}