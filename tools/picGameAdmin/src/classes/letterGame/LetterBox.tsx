import React, { Component } from 'react';
import { Button, ButtonGroup, Card } from 'react-bootstrap';
import { LetterVo } from '../../dataObjs/letterGames/LetterVo';
import { MainStorage } from '../core/MainStorage';



interface MyProps {index:number,editMode:number, letterVo: LetterVo, textEditCallback: Function,selectCallback:Function,deleteCallback:Function }
interface MyState {editMode:number}
class LetterBox extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {editMode:this.props.editMode};
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({editMode:this.props.editMode});
        }
    }
    private getButton() {

        

        let variant: string = "success";
       // let text: string = "Right";
        let icon:JSX.Element=(<i className="far fa-smile-beam"></i>)

        if (this.props.letterVo.correct === false) {
            variant = "warning";
          //  text = "Wrong";
            icon=(<i className="far fa-frown"></i>)
        }
       
        return (<Button variant={variant} onClick={()=>{this.props.selectCallback(this.props.index)}}>{icon}</Button>)
    }
    getButtonGroup()
    {
        if (this.state.editMode===1)
        {
            return (<Button variant="danger" onClick={()=>{this.props.deleteCallback(this.props.index)}}><i className="fas fa-times-circle"></i></Button>)
        }
        return <ButtonGroup><Button onClick={()=>{this.props.textEditCallback(this.props.index)}}><i className="fas fa-edit"></i></Button>{this.getButton()}</ButtonGroup>
    }
    private getDelButton()
    {
        return (<Button><i className="fas fa-trash"></i></Button>);
    }
    render() {
        return (<div>
            <Card className='picCard'>
                <Card.Body>
                    <div className='sLetter'>
                        {this.props.letterVo.letter}
                    </div>
                </Card.Body>
                <Card.Footer className="cardHeader">{this.getButtonGroup()}</Card.Footer>
            </Card>
        </div>)
    }
}
export default LetterBox;