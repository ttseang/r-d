import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { LetterRowVo } from '../../dataObjs/letterGames/LetterRowVo';


interface MyProps {index:number,editCallback:Function,deleteCallback:Function,letterRowVo:LetterRowVo}
interface MyState {}
class LetterRow extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
this.state={};
}
getImage(index: number) {
    return this.props.letterRowVo.image;
}
getLetters()
{
    let letters:JSX.Element[]=[];
    for (let i:number=0;i<this.props.letterRowVo.letters.length;i++)
    {
        let cn:string=(this.props.letterRowVo.letters[i].correct===true)?"rightLetter":"wrongLetter";
        let key="letter"+i.toString();
        letters.push(<span className={cn} key={key}>{this.props.letterRowVo.letters[i].letter}</span>)
    }
    return letters;
}
getRowContent()
{
    return (
        <Row className="picRow">
        <Col lg={1}>{this.props.index}</Col> 
        <Col lg={5}>{this.getLetters()}</Col>
        <Col lg={2}><img src={"./assets/pics/" + this.props.letterRowVo.image} alt={this.props.letterRowVo.letters[0].letter} height={50} width={50} /></Col>
        <Col lg={2}><Button onClick={()=>{this.props.editCallback(this.props.index)}}>Edit</Button></Col>
        <Col lg={2}><Button variant='danger' onClick={()=>{this.props.deleteCallback(this.props.index)}}>Delete</Button></Col>
    </Row>
    )
}
render()
{
return (<div>{this.getRowContent()}</div>)
}
}
export default LetterRow;