import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { FBRowVo } from '../../dataObjs/fibGames/FBRowVo';



interface MyProps { index: number, editCallback: Function, deleteCallback: Function, rowData:FBRowVo }
interface MyState { }
class FBRow2 extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
   
    getLetters() {
        let letters: JSX.Element[] = [];
        for (let i: number = 0; i < this.props.rowData.letters.length; i++) {
            let cn: string = (this.props.rowData.letters[i].correct === true) ? "rightLetter2" : "wrongLetter2";
            let key = "letter" + i.toString();
            letters.push(<li className={cn} key={key}>{this.props.rowData.letters[i].letter}</li>)
        }
        return (<ul className='wordList'>{letters}</ul>);
    }
    getRowContent() {
        return (
            <Row className="picRow">
                <Col lg={1}>{this.props.index}</Col>
                <Col lg={3}>{this.props.rowData.blank}</Col>
                <Col lg={4}>{this.getLetters()}</Col>
                <Col lg={2}><Button onClick={() => { this.props.editCallback(this.props.index) }}>Edit</Button></Col>
                <Col lg={2}><Button variant='danger' onClick={() => { this.props.deleteCallback(this.props.index) }}>Delete</Button></Col>
            </Row>
        )
    }
    render() {
        return (<div>{this.getRowContent()}</div>)
    }
}
export default FBRow2;