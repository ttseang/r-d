import React, { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';

interface MyProps {words:string[],editCallback:Function}
interface MyState { }
class WordListCard extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>            
            <Row><Col sm={2}>Word List</Col><Col sm={6} className='tac'>{this.props.words.join(",")}</Col>
            <Col sm={2} className='tac'><Button variant='warning' onClick={()=>{this.props.editCallback()}}>Change</Button></Col></Row>
            </Card.Body>
            </Card>
        </div>)
    }
}
export default WordListCard;