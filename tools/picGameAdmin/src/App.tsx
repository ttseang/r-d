import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BaseScreen from './screens/core/BaseScreen';

function App() {
  return (
    <div id="base"><BaseScreen></BaseScreen></div>
  );
}
export default App;