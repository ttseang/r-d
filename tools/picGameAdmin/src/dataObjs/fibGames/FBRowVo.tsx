import { LetterVo } from "../letterGames/LetterVo";


export class FBRowVo
{
    public letters:LetterVo[];
    public image:string;
    public blank:string;

    constructor(image:string,blank:string,letters:LetterVo[])
    {
        this.image=image;
        this.blank=blank;
        this.letters=letters;
    }
}