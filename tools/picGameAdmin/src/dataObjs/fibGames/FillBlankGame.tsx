import { FBRowVo } from "./FBRowVo";


export class FillBlankGame
{
    public cards:FBRowVo[];
    public instructions:string;
    public wordList:string[]=[];
    
    constructor(instructions:string,cards:FBRowVo[])
    {
        this.instructions=instructions;
        this.cards=cards;
    }
}