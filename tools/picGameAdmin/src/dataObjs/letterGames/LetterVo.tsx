export class LetterVo
{
    public letter:string;
    public correct:boolean;

    constructor(letter:string,correct:boolean)
    {
        this.letter=letter;
        this.correct=correct;
    }
}