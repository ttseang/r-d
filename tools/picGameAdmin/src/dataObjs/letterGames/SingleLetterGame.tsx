import { LetterRowVo } from "./LetterRowVo";

export class SingleLetterGame
{
    public cards:LetterRowVo[];
    public instructions:string;
    
    constructor(instructions:string,cards:LetterRowVo[])
    {
        this.instructions=instructions;
        this.cards=cards;
    }
}