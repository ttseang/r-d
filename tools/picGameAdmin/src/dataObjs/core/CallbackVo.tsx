export class CallbackVo {
    public changePic: Function=()=>{};
    public doneCallback: Function=()=>{};
    public deleteCallback: Function=()=>{};
    public toggleCallback: Function=()=>{};
    public changeText: Function=()=>{};
    public addNew:Function=()=>{};
    public editCallback:Function=()=>{};
}