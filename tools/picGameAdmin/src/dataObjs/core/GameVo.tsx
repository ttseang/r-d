export class GameVo {
    public id: number
    public gameName: string;
    public type:number;
    public image: string;
    public description: string;
    public prevLink:string;

    public static TYPE_PIC_MATCH:number=1;
    public static TYPE_SINGLE_LETTER:number=2;
    public static TYPE_FILL_IN_THE_BLANK:number=3;

    constructor(id: number, gameName: string,type:number, image: string, description: string,prevLink:string) {
        this.id = id;
        this.gameName = gameName;
        this.type=type;
        this.image = image;
        this.description = description;
        this.prevLink=prevLink;
    }
}