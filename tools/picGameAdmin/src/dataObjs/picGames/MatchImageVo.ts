import { GameWordVo } from "./GameWordVo";

export class MatchImageVo
{
    public cards:GameWordVo[];
    public instructions:string;

    constructor(instructions:string,cards:GameWordVo[])
    {
        this.instructions=instructions;
        this.cards=cards;
    }
    getTextFormat()
    {
        return {text:this.instructions,cards:this.cards.slice(0,2)}
    }
}