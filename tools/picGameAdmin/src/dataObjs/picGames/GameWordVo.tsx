export class GameWordVo
{
    public word:string;
    public image:string;
    public correct:boolean;

    constructor(word:string,image:string,correct:boolean)
    {
        this.word=word;
        this.image=image;
        this.correct=correct;
    }
}