import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import AudioBox from '../comps/AudioBox';
interface MyProps {library: LibItemVo[], closeCallback: Function}
interface MyState {library: LibItemVo[],filteredLib:LibItemVo[],showFiltered:boolean }
class AudioScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {library: this.props.library,filteredLib:[],showFiltered:false};
        }
        componentDidMount()
        {
            let libItemVo:LibItemVo[]=this.getFilter();
            let show:boolean=true;
            if (libItemVo.length===0)
            {
                show=false;
            }
            this.setState({filteredLib:libItemVo,showFiltered:show});
        }
        componentDidUpdate(oldProps: MyProps) {
            if (this.props !== oldProps) {
                this.setState({ library: this.props.library });
            }
        }
        private getFilter()
        {        
            return this.ms.libUtil.getAudioFilter(this.ms.suggested);
        }
        private showFilter()
        {
            this.setState({showFiltered:true});
        }
        private showAll()
        {
            this.setState({showFiltered:false});
        }
    getAudioBoxes()
    {
        let boxes:JSX.Element[]=[];
        for (let i:number=0;i<this.state.library.length;i++)
        {
            let key:string="audioBox"+i.toString();
            boxes.push(<Col key={key} sm={4}><AudioBox  libItemVo={this.state.library[i]}></AudioBox></Col>)
        }
        return (<Row>{boxes}</Row>);
    }
    render() {
        return (<div className='scroll1'>{this.getAudioBoxes()}</div>)
    }
}
export default AudioScreen;