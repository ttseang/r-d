import React, { Component } from 'react';
import { ZoomVo } from '../classes/dataObjs/ZoomVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
interface MyProps {locked:boolean,updateZoom:Function}
interface MyState { x1: number, y1: number, x2: number, y2: number, locked:boolean}
class ZoomFrame extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    private isDown:boolean=false;

    constructor(props: MyProps) {
        super(props);
        this.state = {x1: 20, y1: 20, x2: 80, y2: 80, locked:this.props.locked};
        this.mc.updateZoomControls=this.updateZoomControls.bind(this);
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({locked:this.props.locked});
        }
    }
    private updateZoomControls(zoomVo:ZoomVo)
    {
        this.setState({x1:zoomVo.x1,y1:zoomVo.y1,x2:zoomVo.x2,y2:zoomVo.y2});
    }
    moveMiddle(e: React.PointerEvent<HTMLDivElement>)
    {
        if (this.isDown===false)
        {
            return;
        }
        e.preventDefault();
        let perMoveX: number = Math.floor((e.movementX / this.ms.pageW) * 10000) / 100;
        let perMoveY: number = Math.floor((e.movementY / this.ms.pageH) * 10000) / 100;

        let x1:number=this.state.x1;
        let y1:number=this.state.y1;
        let x2:number=this.state.x2;
        let y2:number=this.state.y2;

        x1+=perMoveX;
        x2+=perMoveX;
        y1+=perMoveY;
        y2+=perMoveY;

        this.setState({x1:x1,x2:x2,y1:y1,y2:y2});
        this.updateZoom();
    }
    
    moveCorner(e: React.PointerEvent<HTMLDivElement>)
    {
        if (this.isDown===false)
        {
            return;
        }
        e.preventDefault();
        
        let perMoveX: number = Math.floor((e.movementX / this.ms.pageW) * 10000) / 100;
        let perMoveY: number = Math.floor((e.movementY / this.ms.pageH) * 10000) / 100;

        let x1:number=this.state.x1;
        let y1:number=this.state.y1;
        let x2:number=this.state.x2;
        let y2:number=this.state.y2;

        let id:string=e.currentTarget.id;

        switch(id)
        {
            case "leftTopCorner":
            x1+=perMoveX;
            y1+=perMoveY;
            break;

            case "rightBottomCorner":
            x2+=perMoveX;
            y2+=perMoveY;
            break;

            case "rightTopCorner":
            y1+=perMoveY;
            x2+=perMoveX;
            break;

            case "leftBottomCorner":
            y2+=perMoveY;
            x1+=perMoveX;
            break;
        }


      /*   x1+=perMoveX;
        x2+=perMoveX;
        y1+=perMoveY;
        y2+=perMoveY;
 */
        this.setState({x1:x1,x2:x2,y1:y1,y2:y2});
        this.updateZoom();
    
    }
    updateZoom()
    {
        let zoomVo:ZoomVo=new ZoomVo(this.state.x1,this.state.y1,this.state.x2,this.state.y2);
        
        this.props.updateZoom(zoomVo);
    }
  
    render() {

        let hx: number = this.state.x1+(this.state.x2 - this.state.x1) / 2;
        let hy: number = this.state.y1+(this.state.y2 - this.state.y1) / 2;
       // ////console.log(hx,hy);

        let ph: number = this.ms.pageH;
        let pw: number = this.ms.pageW;

        
        let cornerClasses:string[]=[];
        cornerClasses.push("zoomCorner");

        let moveClasses:string[]=[];
        moveClasses.push("zoomCorner");
        moveClasses.push("centerTrans");

        let mainClass:string="";

        if (this.state.locked===true)
        {
            cornerClasses.push("zoomLocked");
            moveClasses.push("zoomLocked");
            mainClass="noPoint";
        }

        let cornerClass:string=cornerClasses.join(" ");
        let moveClass:string=moveClasses.join(" ");

      
        

        return (<div id="zoomFrame" onPointerDown={()=>{this.isDown=true}} onPointerUp={()=>{this.isDown=false}} className={mainClass} style={{ "width": pw.toString() + "px", "height": ph.toString() + "px" }}>
            <div id="leftTopCorner" onPointerMove={this.moveCorner.bind(this)}  className={cornerClass} style={{ "left": this.state.x1.toString() + "%", "top": this.state.y1.toString() + "%" }}><img className='zoomFrameImage r90' src='./images/pageImages/png/corner.png' alt="left corner" /></div>
            <div id="leftBottomCorner" onPointerMove={this.moveCorner.bind(this)} className={cornerClass} style={{ "left": this.state.x1.toString() + "%", "top": this.state.y2.toString() + "%" }}><img className='zoomFrameImage r0' src='./images/pageImages/png/corner.png' alt="left corner" /></div>
            <div id="rightBottomCorner" onPointerMove={this.moveCorner.bind(this)} className={cornerClass} style={{ "left": this.state.x2.toString() + "%", "top": this.state.y2.toString() + "%" }}><img className='zoomFrameImage r270' src='./images/pageImages/png/corner.png' alt="left corner" /></div>
            <div id="rightTopCorner" onPointerMove={this.moveCorner.bind(this)} className={cornerClass} style={{ "left": this.state.x2.toString() + "%", "top": this.state.y1.toString() + "%" }}><img className='zoomFrameImage r180' src='./images/pageImages/png/corner.png' alt="left corner" /></div>
            <div id="centerMove" onPointerMove={this.moveMiddle.bind(this)} className={moveClass} style={{ "left": hx.toString() + "%", "top": hy.toString() + "%" }}><img className='zoomFrameImage' src='./images/pageImages/png/move.png' alt="left corner" /></div>

        </div>)
    }
}
export default ZoomFrame;