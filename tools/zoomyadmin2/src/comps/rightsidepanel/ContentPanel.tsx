import React, { Component } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import MainStorage from '../../classes/MainStorage';

interface MyProps { elementVo: ElementVo,callback:Function }
interface MyState { elementVo: ElementVo }
class ContentPanel extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = { elementVo: this.props.elementVo };
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({elementVo:this.props.elementVo});
        }
    }
    getContent()
    {
        if (this.state.elementVo.type === ElementVo.TYPE_TEXT) {
            let text:string=this.state.elementVo.content[0].substring(0,30);
            if (this.state.elementVo.content.length>30)
            {
                text+="...";
            }
            return text;
        }
        if (this.state.elementVo.type === ElementVo.TYPE_IMAGE || this.state.elementVo.type===ElementVo.TYPE_BACK_IMAGE) {
            let image: string =this.ms.getImagePath(this.state.elementVo.content[0],0);            
            return (<img src={image} alt='book' className='contentThumb' />)
        }
    }
    render() {
        return (<Card>
            <Card.Body>
                <Row><Col className="tac">{this.getContent()}</Col></Row>
                <Row><Col className="tac"><Button size="sm" onClick={()=>{this.props.callback(5)}}>Change</Button></Col></Row>
                </Card.Body>
        </Card>)
    }
}
export default ContentPanel;