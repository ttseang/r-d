import { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { ButtonConstants } from '../../classes/constants/ButtonConstants';

import { ButtonVo } from '../../classes/dataObjs/ButtonVo';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { StepVo } from '../../classes/dataObjs/StepVo';

import MainStorage from '../../classes/MainStorage';
import StyleGrid from '../../classes/ui/StyleGrid';
import FramePropBox from '../frame/FramePropBox';
import AdvPanel from './AdvPanel';
import BgPosBox from './BgPosBox';
import BorderBox from './BorderBox';
import ContentPanel from './ContentPanel';
import CopyBox from './CopyBox';
import FlipPanel from './FlipPanel';
import FontPanel from './FontPanel';
import InstanceBox from './InstanceBox';
import LinePosBox from './LinePosBox';
import LineStylebox from './LineStyleBox';
import PopUpSelect from './PopUpSelect';
import PosBox from './PosBox';
import PosBox2 from './PosBox2';
import TextStylePanel from './TextStylePanel';
interface MyProps { elementVo: ElementVo | null,step:StepVo | null, callback: Function, actionCallback: Function, updateExtras: Function, updateBackgroundProps: Function,updateLine:Function,updateInstance:Function }
interface MyState { elementVo: ElementVo | null,step:StepVo | null, panelIndex: number }
class PropBox extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { elementVo: this.props.elementVo,step:this.props.step, panelIndex: 0 };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ elementVo: this.props.elementVo,step:this.props.step });
        }
    }

    getPanel(): JSX.Element {
        if (this.state.elementVo === null) {
            return (<div>Loading</div>);
        }
        // ////console.log("panelIndex=" + this.state.panelIndex);

        switch (this.state.panelIndex) {
            case 0:
                return <ContentPanel key="contentPanel" elementVo={this.state.elementVo} callback={this.props.actionCallback}></ContentPanel>

            case 1:

                return this.getPropBox();

            case 2:
                return <StyleGrid key="alignGrid" gridItems={this.ms.styleUtil.aligns} callback={this.props.actionCallback}></StyleGrid>

            case 3:
                return <FlipPanel key="flipPanel" actionCallback={this.props.actionCallback}></FlipPanel>

            case 4:
                return <StyleGrid key="orgrid" gridItems={this.ms.styleUtil.orientations} callback={this.props.actionCallback}></StyleGrid>

            case 5:
                return <BorderBox key="borderBox" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></BorderBox>

            case 6:
                return <AdvPanel key="advancedPanel" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></AdvPanel>

            case 7:
                return <FontPanel key="fontPanel" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></FontPanel>
            case 8:
                return (<BgPosBox key="backgroundPosBox" elementVo={this.state.elementVo} callback={this.props.updateBackgroundProps.bind(this)}></BgPosBox>);

            case 14:
                return (<PopUpSelect key="popupselect" element={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></PopUpSelect>)

            case 15:
                return (<LinePosBox key="lineposbox" elementVo={this.state.elementVo} callback={this.props.updateLine}></LinePosBox>)
       
            case 16:
                return (<LineStylebox key="linestylebox" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></LineStylebox>)
           
             //instance box
            case 17:
                return <InstanceBox key="instanceBox" element={this.state.elementVo} callback={this.props.updateInstance}></InstanceBox>

            case 19:
                return <TextStylePanel></TextStylePanel>

            case 20:
                return this.getPropBox2();
            }
        return (<div key="no panel">Panel Here</div>);
    }
    doFrameActions()
    {

    }
    getButtons() {
        
        if (this.state.elementVo === null) {
            // return "Select an Element";
             return <FramePropBox stepVo={this.state.step} callback={()=>{}} actionCallback={this.doFrameActions.bind(this)}></FramePropBox>
         }
        let buttonData: ButtonVo[] = ButtonConstants.PANEL_BUTTONS;

        if (this.state.elementVo.type === ElementVo.TYPE_TEXT) {
            buttonData = ButtonConstants.TEXT_PANEL_BUTTONS;
        }
       /*  if (this.state.elementVo.subType===ElementVo.SUB_TYPE_GUTTER)
        {
            buttonData=ButtonConstants.BGIMG_PANEL_BUTTONS;
        } */
        if (this.state.elementVo.type===ElementVo.TYPE_CARD)
        {
            buttonData=ButtonConstants.CARD_PANEL_BUTTONS;
        }
        if (this.state.elementVo.subType===ElementVo.SUB_TYPE_POPUP_LINK)
        {
            buttonData=ButtonConstants.POPUP_PANEL_BUTTONS;
        }
        if (this.state.elementVo.type===ElementVo.TYPE_LINE)
        {
            buttonData=ButtonConstants.LINE_PANEL_BUTTONS;
        }
        let buttons: JSX.Element[] = [];

        for (let i: number = 0; i < buttonData.length; i++) {
            let key: string = "propButton" + i.toString();

            buttons.push(<Button key={key} variant={buttonData[i].variant} className="fw" onClick={() => { this.changePanel(buttonData[i].action) }}>{buttonData[i].text}</Button>)
            if (buttonData[i].action === this.state.panelIndex) {
                buttons.push(this.getPanel());
            }
        }
        return buttons;
    }
    changePanel(panelIndex: number) {
        if (this.state.panelIndex === panelIndex) {
            panelIndex = -1;
        }
        this.setState({ panelIndex: panelIndex })
    }
    getContent() {
        if (this.state.elementVo) {
            return this.state.elementVo.content + " " + this.state.elementVo.type;
        }
        return "";
    }
    getPropBox() {
        if (this.state.elementVo) {
            return (<PosBox key="posbox" elementVo={this.state.elementVo} callback={this.props.callback}></PosBox>)
        }
        return (<div>Loading</div>);
    }
    /**
     * 
     * Includes height box
     */
    getPropBox2() {
        if (this.state.elementVo) {
            return (<PosBox2 key="posbox2" elementVo={this.state.elementVo} callback={this.props.callback}></PosBox2>)
        }
        return (<div>Loading</div>);
    }
     getCopyBox()
     {
         if (this.state.elementVo)
         {
             return (<CopyBox key="copybox" actionCallback={this.props.actionCallback}></CopyBox>)
         }
         return "";
     }   
    render() {
        //TODO:save file
        //TODO:load file
        //TODO:instance name
        //TEXT
        //TODO:style panel
        return (<div>
            <Card>
                <Card.Header id="propHeader">Properties</Card.Header>
                <Card.Body>
                    {this.getCopyBox()}
                    <div className='scroll1' id="propScroll">
                        {this.getButtons()}
                    </div>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default PropBox;