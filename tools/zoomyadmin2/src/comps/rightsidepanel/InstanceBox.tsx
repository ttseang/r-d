import { ChangeEvent, Component } from 'react';
import { Card } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
interface MyProps { element: ElementVo, callback: Function }
interface MyState { eidText:string,msg:string }
class InstanceBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props)
        this.state = { eidText:this.props.element.eid,msg:"" };
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({eidText:this.props.element.eid});
        }
    }
    onChange(e:ChangeEvent<HTMLInputElement>)
    {
        let instanceText:string=e.currentTarget.value;
        this.setState({eidText:instanceText});

        if (instanceText!=="")
        {
            this.props.callback(instanceText);
            this.setState({msg:""});
        }  
        else{
            this.setState({msg:"Instance cannot be blank"});
        }      
    }
    render() {
        return (
            <div>
            <Card>
                <Card.Body>
                    <input type='text' className='instanceText' onChange={this.onChange.bind(this)} value={this.state.eidText}></input>
                    <div id="errorText">{this.state.msg}</div>
                </Card.Body>
                </Card>
                </div>
        )
    }
}
export default InstanceBox;