import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
interface MyProps { libItemVo: LibItemVo }
interface MyState { libItemVo: LibItemVo }
class AudioBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = { libItemVo: this.props.libItemVo };
        }
    selectAudio()
    {
        if (this.ms.backgroundAudioSwitch===true)
        {
            this.mc.backgroundAudioChange(this.state.libItemVo);
        }
        else
        {
            this.mc.audioChangeCallback(this.state.libItemVo);
        }
    }
    getPlayer()
    {
        let path:string=this.ms.getAudioPath(this.state.libItemVo.image);
        //console.log(path);
        return(<audio controls><source src={path} type="audio/mpeg" /></audio>)
    }
    render() {
        return (<Card>
            <Card.Title>{this.state.libItemVo.title}</Card.Title>
            <Card.Body>{this.getPlayer()}</Card.Body>
            <Card.Footer className='tac'><Button onClick={this.selectAudio.bind(this)}>Select</Button></Card.Footer>
        </Card>)
    }
}
export default AudioBox;