import React, { Component } from 'react';
import { StepVo } from '../classes/dataObjs/StepVo';
import { ZoomVo } from '../classes/dataObjs/ZoomVo';
import MainStorage from '../classes/MainStorage';
import { TemplateData } from '../classes/TemplateData';
interface MyProps { stepVo: StepVo | null,zoomVo:ZoomVo}
interface MyState { stepVo: StepVo | null,zoomVo:ZoomVo}
class ZoomPreviewPage extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepVo: this.props.stepVo,zoomVo:this.props.zoomVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepVo: this.props.stepVo,zoomVo:this.props.zoomVo });
        }
    }
    getPath(image: string) {
        return "./images/bookimages/" + image;
    }

    getPage() {       

        let td: TemplateData = new TemplateData();
        return td.getZoomPage(this.state.stepVo);
    }

    render() {

        let zoomVo:ZoomVo=this.state.zoomVo;
        
        //turn percentages into pixels
        let xx:number=(zoomVo.x1/100)*this.ms.pageW;
        let yy:number=(zoomVo.y1/100)*this.ms.pageH;

        //mesuare the distance between the points
        let distX:number=(zoomVo.x2-zoomVo.x1)/100;
        let distY:number=(zoomVo.y2-zoomVo.y1)/100;
        
        //cut off the extra decimal places past 2 points
        distX=Math.round(distX*100)/100;
        distY=Math.round(distY*100)/100;

        

        let zoomX:number=(this.ms.pageW*distX);
        let zoomY:number=(this.ms.pageH*distY);       

        let posString=xx.toString()+" "+yy.toString();
        posString+=" "+zoomX+" "+zoomY

        console.log("posString="+posString);

        let hh:number=this.ms.pageH/2.5;
        let ww:number=this.ms.pageW/2.5;

        return (<svg viewBox={posString} height={hh} width={ww}>
            {this.getPage()}
           
            </svg>)
    }
}
export default ZoomPreviewPage;