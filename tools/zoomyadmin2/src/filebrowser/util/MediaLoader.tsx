//import { MainStorage } from "./MainStorage";


export class MediaLoader
{
   
    public getFileList(callback: Function,tags:string) {

        let url: string = "https://tthq.me/api/tag/find?t[]="+tags;
        fetch(url, {
            method: "get"
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
    
}
export default MediaLoader;
