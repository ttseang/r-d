import React, { ChangeEvent, Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { MediaSoundVo } from '../dataObjs/MediaSoundVo';
import { MPVo } from '../dataObjs/MPVo';
import SoundCard from '../ui/SoundCard';
import MediaLoader from '../util/MediaLoader';
interface MyProps { tag: string, path: string, callback: Function, cancelCallback: Function }
interface MyState { tag: string, media: MediaSoundVo[] }
class SoundMediaBrowser extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { tag: this.props.tag, media: [] };
    }
    componentDidMount() {
        this.loadFileInfo(this.props.tag);
        ////console.log("mount");
    }
    loadFileInfo(tag: string) {
        let ml: MediaLoader = new MediaLoader();
        ml.getFileList(this.gotFileInfo.bind(this), tag);
    }
    private gotFileInfo(data: any) {
        //console.log(data);
        let sounds: any[] = data.sounds;

        let media: MediaSoundVo[] = [];

        for (let i: number = 0; i < sounds.length; i++) {
            let sound: any = sounds[i];
            //console.log(sound);

            let mp3: MPVo = new MPVo(parseInt(sound.mp3.bytes), parseFloat(sound.mp3.dur), sound.mp3.etag, parseInt(sound.mp3.id), sound.mp3.path);
            let mp4: MPVo = new MPVo(parseInt(sound.mp4.bytes), parseFloat(sound.mp4.dur), sound.mp4.etag, parseInt(sound.mp4.id), sound.mp4.path);

            let soundObj: MediaSoundVo = new MediaSoundVo(sound.name, sound.title, sound.kind, mp3, mp4, sound.tags);
            media.push(soundObj);
        }
        this.setState({ media: media })
    }
    tagChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ tag: e.currentTarget.value });
        this.loadFileInfo(e.currentTarget.value);
    }
    getCards() {
        let cards: JSX.Element[] = [];

        for (let i: number = 0; i < this.state.media.length; i++) {
            let key: string = "soundCard" + i.toString();

            cards.push(<Col key={key} sm={4}><SoundCard callback={this.props.callback} sound={this.state.media[i]} path={this.props.path}></SoundCard></Col>)
        }
        return (<Row>{cards}</Row>);
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    <Row><Col><Button style={{float:"right"}} onClick={()=>{this.props.cancelCallback()}}>X</Button></Col></Row>
                    <Row><Col></Col><Col style={{ textAlign: "center" }}>Tag:<input type="text" value={this.state.tag} onChange={this.tagChange.bind(this)} /></Col><Col></Col></Row>
                    <hr />
                    {this.getCards()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default SoundMediaBrowser;