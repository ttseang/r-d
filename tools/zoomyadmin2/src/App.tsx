import React from 'react';
import './App.css';
import './Zoomy.css';
import './TextAnimationStyles.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import BaseScreen from './screens/BaseScreen';
function App() {
  return (
    <BaseScreen></BaseScreen>
  );
}
export default App;