export class GridVo
{
    public id:number;
    public text:string;
    public image:string;
    public action:number;

    constructor(id:number,text:string,image:string,action:number)
    {
        this.id=id;
        this.text=text;
        this.image=image;
        this.action=action;
    }
}