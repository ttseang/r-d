import { FileListLoader } from "./FileListLoader";
import { LibLoader } from "./LibLoader";
import MainStorage from "./MainStorage";
import { ScreenSizeLoader } from "./ScreenSizeLoader";
import { TextStyleMapLoader } from "./TextStyleMapLoader";

export class StartData {
    private callback: Function;
    private ms: MainStorage = MainStorage.getInstance();

    constructor(callback: Function) {
        this.callback = callback;
    }
    start() {
        let textStyleMapLoader: TextStyleMapLoader = new TextStyleMapLoader(this.gotStyles.bind(this));
        textStyleMapLoader.load("./jsonFiles/textStyleMap.json");
    }
    private gotStyles() {
        let libLoader: LibLoader = new LibLoader(this.gotLib.bind(this));
        libLoader.load("./jsonFiles/lib.json")
    }
    private gotLib() {
        this.getFileList();
    }
    private getFileList() {
        let fll: FileListLoader = new FileListLoader(this.getScreenSized.bind(this));
        fll.getFileList();
    }
    private getScreenSized() {
        let ssl: ScreenSizeLoader = new ScreenSizeLoader(this.gotScreenSizes.bind(this));
        ssl.getScreenSizes();
    }
    private gotScreenSizes() {
        this.callback();
    }
}