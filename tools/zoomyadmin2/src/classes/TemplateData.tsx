import BookElement from "../comps/BookElement";
import { ElementVo } from "./dataObjs/ElementVo";
import { PopUpVo } from "./dataObjs/PopUpVo";
import { StepVo } from "./dataObjs/StepVo";

import MainStorage from "./MainStorage";

export class TemplateData {
    private ms: MainStorage = MainStorage.getInstance();

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {

    }
    public getPopUp(popupVo:PopUpVo,callback:Function,onUp:Function)
    {
        let elements:ElementVo[]=popupVo.elements;
        // let index:number=pageVo.templateID;
         
         let xx:number=0;
         let hh:number=popupVo.h;
         let ww:number=popupVo.w;
         let ww2:number=ww*2;
 
         let pageElements:JSX.Element[]=[];
         for (let i:number=0;i<elements.length;i++)
         {  
             let key:string="be"+i.toString();
             pageElements.push(<BookElement key={key} elementVo={elements[i]} preview={true} callback={callback} onUp={onUp} lineDown={()=>{}} lineUp={()=>{}}></BookElement>);
         }
 
         let pageClass:string="pageStart";
 
         
 
         return (<foreignObject className={pageClass} x={xx} y="0" width={ww} height={hh} id="ptblank">
         <div className="pg2">
             {pageElements}
             <figure className="guttershadow" style={{"height":"450px","backgroundSize":ww2.toString()+"px "+hh.toString()+"px"}}></figure>
            
         </div>
     </foreignObject>);
    }
    public getPage2(stepVo:StepVo | null,callback:Function,onUp:Function,lineUp:Function=()=>{},lineDown:Function=()=>{})
    {
        let elements:ElementVo[]=[];
        if (stepVo)
        {
            elements=stepVo.elements;
        }
    
        let xx:number=0;
        let hh:number=this.ms.getPageH();
        let ww:number=this.ms.getPageW();
        let ww2:number=ww*2;

        let pageElements:JSX.Element[]=[];
        for (let i:number=0;i<elements.length;i++)
        {  
            let key:string="be"+i.toString();
           // //console.log(elements[i].type+" "+elements[i].subType);
            pageElements.push(<BookElement key={key} elementVo={elements[i]} preview={false} callback={callback} onUp={onUp} lineDown={lineDown} lineUp={lineUp}></BookElement>);
        }

        let pageClass:string="pageStart";

        if (hh===450)
        {
            pageClass="pageStart2";
        }

        return (<foreignObject className={pageClass} x={xx} y="0" width={ww} height={hh} id="ptblank">
        <div className="pg2 paper1 rbook">
            {pageElements}
            <figure className="guttershadow" style={{"height":"450px","backgroundSize":ww2.toString()+"px "+hh.toString()+"px"}}></figure>
           
        </div>
    </foreignObject>);

    }
    getZoomPage(stepVo:StepVo | null)
    {
        let elements:ElementVo[]=[];
        if (stepVo)
        {
            elements=stepVo.elements;
        }
    
        let xx:number=0;
        let hh:number=this.ms.getPageH();
        let ww:number=this.ms.getPageW();
        let ww2:number=ww*2;

        let pageElements:JSX.Element[]=[];
        for (let i:number=0;i<elements.length;i++)
        {  
            let key:string="be"+i.toString();
            pageElements.push(<BookElement key={key} elementVo={elements[i]} callback={()=>{}} preview={true} onUp={()=>{}} lineUp={()=>{}} lineDown={()=>{}}></BookElement>);
        }

        let pageClass:string="pageStart";

        if (hh===450)
        {
            pageClass="pageStart2";
        }

        return (<foreignObject className={pageClass} x={xx} y="0" width={ww} height={hh} id="ptblank">
        <div className="pg2 paper1 rbook">
            {pageElements}
            <figure className="guttershadow" style={{"height":"450px","backgroundSize":ww2.toString()+"px "+hh.toString()+"px"}}></figure>
           
        </div>
    </foreignObject>);

    }
    getDefaultPopUpData(templateID:number)
    {
        let defElements:ElementVo[]=[];

        switch(templateID)
        {
            case 0:

               
                let popback:ElementVo=new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,[],["popback.svg"]);
                popback.x=50;
                popback.y=50;
                popback.w=100;
                popback.h=100;
                popback.extras.orientation=4;
         
                defElements.push(popback);

                let textEl:ElementVo=new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,["centerText"],["It was the best of times, it was the worst of times"],50,40,80);
                textEl.extras.fontColor="white";
                textEl.extras.orientation=4;
                 defElements.push(textEl);

               // defElements.push(new ElementVo(ElementVo.TYPE_TEXT,ElementVo.SUB_TYPE_NONE,["centerText"],["Once upon a time there was a little girl"]));
               // defElements.push(new ElementVo(ElementVo.TYPE_IMAGE,ElementVo.SUB_TYPE_NONE,["image1"],["fairy3.svg"]));

            break;
        }
       
        return defElements;
    }
    
}
