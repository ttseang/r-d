export class ScreenSizeVo
{
    public w:number;
    public h:number;
    public description:string;
    public per:number;

    constructor(w:number,h:number,description:string,per:number)
    {
        this.w=w;
        this.h=h;
        this.description=description;
        this.per=per;
    }
}