export class ButtonVo
{
    public text:string;
    public variant:string;
    public action:number;
    public size:string;
    constructor(text:string,variant:string,action:number,size:string="lg")
    {
        this.action=action;
        this.variant=variant;
        this.text=text;
        this.size=size;
    }
}