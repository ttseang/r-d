export class StyleVo
{
    public id:number;
    public name:string;
    public type:string;
    public subType:string;
    public classes:string[];
    public image:string;

    public x:number;
    public y:number;
    public w:number;

    public xAdjust:string;
    public yAdjust:string;
    //
    //
    //
    public static TYPE_IMAGE:string="typeImage";
    public static TYPE_TEXT:string="typeText";
    public static SUBTYPE_POSITION:string="typePosition";
    public static SUBTYPE_STYLE:string="typeStyle";
    public static SUBTYPE_SIZE:string="typeSize";

    public static ADJUST_NONE:string="adjust_none";
    public static ADJUST_FULL:string="adjustFull";
    public static ADJUST_HALF:string="adjustHalf";

    constructor(id:number,name:string,type:string,subType:string,classes:string[],image:string,x:number=-1000,y:number=-1000,w:number=-1000,adjustX:string="adjust_none",adjustY:string="adjust_none")
    {
        this.id=id;
        this.name=name;
        this.type=type;
        this.subType=subType;
        this.classes=classes;
        this.image=image;
        
        this.x=x;
        this.y=y;
        this.w=w;
        this.xAdjust=adjustX;
        this.yAdjust=adjustY;
    }
}