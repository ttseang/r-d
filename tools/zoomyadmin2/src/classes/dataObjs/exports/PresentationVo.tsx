import { StepExportVo } from "./StepExportVo";

export class PresentationVo
{
    public steps:StepExportVo[];
    public pageW:number;
    public pageH:number;

    constructor(steps:StepExportVo[],pageW:number,pageH:number)
    {
        this.steps=steps;
        this.pageW=pageW;
        this.pageH=pageH;
    }
}