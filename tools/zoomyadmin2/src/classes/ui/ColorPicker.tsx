import React, { ChangeEvent, Component } from 'react';
interface MyProps {myColor:string,callback:Function}
interface MyState {myColor:string}
class ColorPicker extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {myColor:this.props.myColor};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({myColor:this.props.myColor});
        }
    }
    onChange(e:ChangeEvent<HTMLInputElement>)
    {
        let color:string=e.currentTarget.value;
        this.setState({myColor:color});
        this.props.callback(color);
    }
    render() {
        return (<div>
            <div className='tac'>Color</div>
            <div className='tac'><input type='color' value={this.state.myColor} onChange={this.onChange.bind(this)} /></div>
        </div>)
    }
}
export default ColorPicker;