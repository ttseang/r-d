import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { GridVo } from '../GridVo';

import MainStorage from '../MainStorage';
interface MyProps { gridVo: GridVo, callback: Function }
interface MyState { gridVo: GridVo }
class GridItem extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { gridVo: this.props.gridVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ gridVo: this.props.gridVo });
        }
    }
    getImage() {
        return this.ms.getImagePath(this.state.gridVo.image, 1);
    }
    render() {
        return (<div>
            <Row>
                <Col>
                    <img onClick={()=>{this.props.callback(this.props.gridVo.action,this.props.gridVo.id)}} src={this.getImage()} width="50" alt={this.state.gridVo.text} title={this.state.gridVo.text} />
                </Col>
            </Row>
        </div>)
    }
}
export default GridItem;