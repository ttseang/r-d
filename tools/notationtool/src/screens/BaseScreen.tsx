import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import InputScreen from './InputScreen';
import OutputScreen from './OutputScreen';

interface MyProps { }
interface MyState { mode: number, value: number }
class BaseScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0, value: 0 };
    }
    getScreen() {
        switch (this.state.mode) {
            case 0:
                return <InputScreen callback={this.setNumber.bind(this)}></InputScreen>
            case 1:
                return <OutputScreen value={this.state.value}></OutputScreen>

        }
        return "something went wrong";
    }
    setNumber(value: number) {
        this.setState({ mode: 1,value:value });
    }
    render() {
        return (<div>
            <Card>
                <Card.Header>
                    <h1>Notation Builder</h1>
                </Card.Header>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;