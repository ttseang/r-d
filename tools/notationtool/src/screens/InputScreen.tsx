import React, { ChangeEvent, Component } from 'react';
interface MyProps { callback: Function }
interface MyState { value: number,text:string }
class InputScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { value: 0,text:"" };
    }
    onChange(e: ChangeEvent<HTMLInputElement>) {
        let text: string = e.target.value;
        console.log(text);
        if (!text) {
            //if text is empty then set the state to 0
        //    console.log("empty");
            this.setState({ value: 0 });
            return;
        }
        //if text has more than one decimal point, remove the last one
        //count the number of decimal points
        let decimalCount: number = 0;
        for (let i = 0; i < text.length; i++) {
            if (text[i] === ".") {
                decimalCount++;
            }
        }
       // console.log(decimalCount);
        if (decimalCount > 1) {
            //remove the last decimal point
         //   console.log("more than one decimal point");
            text = text.substring(0, text.length - 1);
        }
        //console.log(text);
        if (isNaN(parseFloat(text))) {
            console.log("not a number");
            return;
        }
        //if text has numbers and decimal points then set the state
        if (text.match(/^[0-9.]+$/)) {
         //   console.log("number");
        //    console.log(parseFloat(text));
            this.setState({ value: parseFloat(text),text:text});
        }
        
        
    }
    addCommas(text:string) {
        console.log("comma text "+text);
        let wholeNumber: string = text.split(".")[0];
        let decimalNumber: string = text.split(".")[1];
         
           //split the whole number into an array of numbers
            let wholeNumberArray: string[] = wholeNumber.split("");
            //reverse the array
            wholeNumberArray.reverse();
            //console.log(wholeNumberArray);
            //add commas every 3 numbers
            let commaArray: string[] = [];
            for (let i = 0; i < wholeNumberArray.length; i++) {
                if (i % 3 === 0 && i !== 0) {
                    commaArray.push(",");
                    console.log("comma");
                }
                console.log(wholeNumberArray[i]);
                commaArray.push(wholeNumberArray[i]);
            }
            console.log(commaArray);
            //reverse the array
            wholeNumberArray = commaArray.reverse();
            console.log(wholeNumberArray);
          //   wholeNumberArray=wholeNumberArray.reverse();

            //join the array into a string
            wholeNumber = wholeNumberArray.join("");

    
        if (decimalNumber) {
            return wholeNumber + "." + decimalNumber;
        }
        return wholeNumber;
    }
    


    render() {
        //create a form with a number input and a submit button

        return (<div className='inputScreen'>
            
                <label>
                    Number:
                    <input type="text" max={12} name="number" value={this.state.text} onChange={this.onChange.bind(this)} />
                </label>
                <button onClick={() => { this.props.callback(this.state.value) }}>Submit</button>
         

        </div>)
    }
}
export default InputScreen;