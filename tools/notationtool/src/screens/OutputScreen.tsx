import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
interface MyProps { value: number }
interface MyState { }
class OutputScreen extends Component<MyProps, MyState>
{
    private wholeNumber: string[] = [];
    private decimalNumber: string[] = [];

    constructor(props: MyProps) {
        super(props);
        this.state = {};
        (window as any).outputScreen = this;
    }
    componentDidMount(): void {
        //console.log("component did mount");
        //  console.log(this.props.value);
        //set css variables
        let digits: string[] = this.props.value.toString().split("");
        let digitCount: number = digits.length;
        let decPos: number = digits.indexOf(".");
        //if not a whole number
        if (decPos !== -1) {
            digitCount--;
        }
        
        let digitCountString: string = digitCount.toString();
        document.documentElement.style.setProperty('--dcols', digitCountString);


    }
    getDigits() {

        //get the whole number and decimal number
        let numberParts: string[] = this.props.value.toString().split(".");
        
        let whole:string=parseInt(numberParts[0]).toLocaleString();
        //whole=this.addCommas(whole);
        //split the whole number into an array of numbers
        let wholeNumber:string[] = whole.split("");

        
    
        let decimalNumber:string[] = [];
        
        if (numberParts.length > 1) {
            decimalNumber = numberParts[1].split("");
        }
        
        
      
        if (decimalNumber.length > 0)
        {
            decimalNumber.unshift(".");
        }
       //for debugging
       this.wholeNumber = wholeNumber;
         this.decimalNumber = decimalNumber;


        //combine the whole number and decimal number
        let allDigits:string[] = wholeNumber.concat(decimalNumber);
        
        let listItems: JSX.Element[] = [];
        //loop through the digits in reverse order
        for (let i: number = 0; i < allDigits.length; i++) {
            let digit: string = allDigits[i];
           
            //get next digit
            let nextDigit: string = allDigits[i + 1];
            //if the next digit is a decimal point or comma then add it to the current digit
            switch (nextDigit) {
                case ".":
                    digit = digit + ".";
                    break;
                case ",":
                    digit = digit + ",";
                    break;

            }
            //if the digit is not a decimal point or comma then add it to the list
            //otherwise skip it
            if (digit !== "." && digit !== ",") {
                let key: string = "digit" + i;
                listItems.push(<li key={key}>{digit}</li>);
            }
        }
        return listItems
    }
    getNumberPlaceNames() {

        const placeNames: string = "ones,tens,hundreds,thousands,ten thousands,hundred thousands, millions,ten millions,hundred millions, billions,ten billions,hundred billions, trillions,ten trillions,hundred trillions, quadrillions,ten quadrillions,hundred quadrillions, quintillions,ten quintillions,hundred quintillions, sextillions,ten sextillions,hundred sextillions, septillions,ten septillions,hundred septillions, octillions,ten octillions,hundred octillions, nonillions,ten nonillions,hundred nonillions, decillions,ten decillions,hundred decillions, undecillions,duodecillions,tredecillions,quattuordecillions,quindecillions,sexdecillions,septendecillions,octodecillions,novemdecillions,vigintillions";
        const decimalPlaceNames: string = "tenths,hundredths,thousandths,ten thousandths,hundred thousandths, millionths,ten millionths,hundred millionths, billionths,ten billionths,hundred billionths, trillionths,ten trillionths,hundred trillionths, quadrillionths,ten quadrillionths,hundred quadrillionths, quintillionths,ten quintillionths,hundred quintillionths, sextillionths,ten sextillionths,hundred sextillionths, septillionths,ten septillionths,hundred septillionths, octillionths,ten octillionths,hundred octillionths, nonillionths,ten nonillionths,hundred nonillionths, decillionths,ten decillionths,hundred decillionths, undecillionths,duodecillionths,tredecillionths,quattuordecillionths,quindecillionths,sexdecillionths,septendecillionths,octodecillionths,novemdecillionths,vigintillionths";
        let placeNamesArray: string[] = placeNames.split(",");
        let decimalPlaceNamesArray: string[] = decimalPlaceNames.split(",");
        let placeNamesList: JSX.Element[] = [];

        //convert the number to a string
        let numberString: string = this.props.value.toString();
        //console.log(numberString);
        let numberArray: string[] = numberString.split(".");

        //get the whole number and the decimal part
        let wholeNumberString: string = numberArray[0];
        let decimalPartString: string = numberArray[1] || "";

        //convert the whole number and the decimal part to arrays of digits
        let wholeNumberDigits: string[] = wholeNumberString.split("");
        let decimalPartDigits: string[] = decimalPartString.split("");


        //reverse the arrays
        wholeNumberDigits.reverse();
        decimalPartDigits.reverse();
       

        //if the decimal part is not zero
        if (decimalPartDigits[0] !== "0" || decimalPartDigits.length > 1) {

            //get the place names for the decimal part
            for (let i = 0; i < decimalPartDigits.length; i++) {

                //make a key for the list item
                let key: string = "decimal" + i.toString();

                placeNamesList.push(<li className='word' key={key}>{decimalPlaceNamesArray[i]}</li>)
            }
        }
        //get the place names for the whole number
        for (let i = 0; i < wholeNumberDigits.length; i++) {
            let key: string = "whole" + i.toString();
            placeNamesList.unshift(<li className='word' key={key}>{placeNamesArray[i]}</li>)
        }
        //return the place names as list items
        return placeNamesList;


    }
   
    render() {
        return (<div><div className='outputScreen'>
            <ul className="numberGrid">
                {this.getDigits()}
                {this.getNumberPlaceNames()}
            </ul>
           
        </div>
        <div className='btnStartOver'>
            <Button onClick={()=>{window.location.reload()}}>Start Over</Button>
            </div>
        </div>
        )
    }
}
export default OutputScreen;