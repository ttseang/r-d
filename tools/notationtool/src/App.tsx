import './App.css';
import BaseScreen from './screens/BaseScreen';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./styles/main.css";
import "./styles/screens.css";
//import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <BaseScreen></BaseScreen>
  );
}

export default App;
