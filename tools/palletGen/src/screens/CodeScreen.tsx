import React, { Component } from 'react';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { }
interface MyState { htmlString: string, cssString: string }
class CodeScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = { htmlString: this.ms.htmlString, cssString: this.ms.cssString };
    }
    private copyText(id: string) {
        var copyText = document.getElementById(id) as HTMLInputElement;
        let text: string = copyText.value;
        navigator.clipboard.writeText(text);
    }
    changeHtml(event: any) {
        let html: string = event.target.value;
        this.ms.htmlString = html;
    }
    changeCss(event: any) {
        let css: string = event.target.value;
        this.ms.cssString = css;
    }
    render() {

        let bodyString = "body{--text:" + this.ms.colorVo.textColor + ";--background:" + this.ms.colorVo.backgroundColor + ";--accent:" + this.ms.colorVo.accentColor + ";--primary:" + this.ms.colorVo.primaryColor + ";--secondary:" + this.ms.colorVo.secondaryColor + ";--link:" + this.ms.colorVo.linkColor + "}";

        //split the css string into an array by ; and then join it with a ; and a new line
        bodyString = bodyString.split(";").join(";\n");

        return (<div className="container">
            <div className="text-area">
                <h2>HTML</h2>
                <textarea id="textarea1" rows={10} cols={50} defaultValue={this.state.htmlString} onChange={this.changeHtml.bind(this)}></textarea>
                <button onClick={() => { this.copyText("textarea1") }}>Copy Text</button>
            </div>
            <div className="text-area">
                <h2>CSS</h2>
                <textarea id="textarea2" rows={10} cols={50} defaultValue={this.state.cssString} onChange={this.changeCss.bind(this)}></textarea>
                <button onClick={() => { this.copyText("textarea2") }}>Copy Text</button>
            </div>
            <div className='text-area'>
                <h2>Css Variables</h2>
                <textarea id="textarea3" rows={10} cols={50} defaultValue={bodyString} readOnly></textarea>
                <button onClick={() => { this.copyText("textarea3") }}>Copy Text</button>
            </div>
        </div>)
    }
}
export default CodeScreen;