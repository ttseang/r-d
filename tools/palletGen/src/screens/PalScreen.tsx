import React, { Component } from 'react';
import { MainStorage } from '../classes/MainStorage';
import { ColorVo } from '../dataObjs/ColorVo';
import { Alert } from 'react-bootstrap';
import { DataCenter } from '../classes/DataCenter';
interface MyProps { }
interface MyState {htmlString: string; cssString: string; colorVo: ColorVo, cssVars: string[], message: string, loading: boolean, prompt: string, palletteName: string }
class PalScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
       // let colorVo: ColorVo = new ColorVo("#FFFFFF", "#FF0000", "#FF8C00", "#FF4500", "#FF6347", "#FF69B4");
        //let colorVo:ColorVo= new ColorVo("#FFFFF", "#FF9F00", "#FFD700", "#FF4500", "#FFA500", "#00BFFF");
     
        this.state = { htmlString: this.ms.htmlString, cssString: this.ms.cssString, colorVo: this.ms.colorVo, cssVars: [], message: "", loading: false, prompt: "", palletteName: "" };
    }
    handleChange = (event: any) => {

        this.setState({ prompt: event.target.value });
    }
    getRenderedCode() {
        //make an html document
        const doc: HTMLElement = document.createElement("html");
        //set the inner html to the html string
        doc.innerHTML = this.state.htmlString;
        //get the head element
        const head: HTMLElement = doc.getElementsByTagName("head")[0];
        //create a style element

        //body variables
        //body{--text:#FFFFFF;--background:#FF0000;--accent:#FF8C00;--primary:#FF4500;--secondary:#FF6347;--link:#FF69B4}
        const bodyString = "body{--text:" + this.state.colorVo.textColor + ";--background:" + this.state.colorVo.backgroundColor + ";--accent:" + this.state.colorVo.accentColor + ";--primary:" + this.state.colorVo.primaryColor + ";--secondary:" + this.state.colorVo.secondaryColor + ";--link:" + this.state.colorVo.linkColor + "}";


        const style: HTMLElement = document.createElement("style");

        //set the inner html to the css string
        const cssString = bodyString + this.state.cssString;
        style.innerHTML = cssString;

        //append the style element to the head
        head.appendChild(style);

        //return the html of the document
        let html = doc.outerHTML;

        //encode the html
        html = encodeURIComponent(html);
        html = `data:text/html;charset=utf-8,${html}`;
        return html;
    }
    getCSSVars() {
        let cssVars: string[] = [];
        let colorValues: string[] = [this.state.colorVo.textColor, this.state.colorVo.backgroundColor, this.state.colorVo.accentColor, this.state.colorVo.primaryColor, this.state.colorVo.secondaryColor, this.state.colorVo.linkColor];
        let colorNames: string[] = ["Text", "Background", "Accent", "Primary", "Secondary", "Link"];
        for (let i = 0; i < 6; i++) {
            cssVars.push("--" + colorNames[i].toLocaleLowerCase() + ": " + colorValues[i] + ";");
        }
        return cssVars;
    }
    getColorBoxes() {
        let colorValues: string[] = [this.state.colorVo.textColor, this.state.colorVo.backgroundColor, this.state.colorVo.accentColor, this.state.colorVo.primaryColor, this.state.colorVo.secondaryColor, this.state.colorVo.linkColor];
        let colorNames: string[] = ["Text", "Background", "Accent", "Primary", "Secondary", "Link"];

        let boxes: JSX.Element[] = [];
        for (let i = 0; i < 6; i++) {
            let key: string = "box" + i;
            let textColor: string = "black";
            if (this.isColorDark(colorValues[i])) {
                textColor = "white";
            }
            let style: React.CSSProperties = { width: "100px", height: "100px", backgroundColor: colorValues[i], color: textColor };
            boxes.push(<div className='colorBox' onClick={() => { this.copyColor(colorValues[i]) }} key={key} style={style}><div>{colorNames[i]}</div><div>{colorValues[i]}</div></div>)
        }
        return <div className='colorBoxes'>{boxes}</div>;
    }
    copyColor(color: string) {
        navigator.clipboard.writeText(color);
        this.showMessage("Copied: " + color);
    }
    isColorDark(color: string) {
        let rgb: number[] = this.hexToRgb(color);
        let brightness: number = Math.round(((rgb[0] * 299) +
            (rgb[1] * 587) +
            (rgb[2] * 114)) / 1000);
        return brightness < 125;

    }
    hexToRgb(hex: string) {
       // console.log(hex);
        let r: number = parseInt(hex.substring(1, 3), 16);
        let g: number = parseInt(hex.substring(3, 5), 16);
        let b: number = parseInt(hex.substring(5, 7), 16);
        return [r, g, b];
    }
    displayCSSVars() {
        let cssBoxes: JSX.Element[] = [];
        let cssVars: string[] = this.getCSSVars();
        for (let i = 0; i < cssVars.length; i++) {
            let key: string = "box" + i;
            cssBoxes.push(<div className='cssBox' key={key}>{cssVars[i]}</div>)
        }
        return <div className='cssBoxes' onClick={this.copyCSSVars.bind(this)}>{cssBoxes}</div>;
    }
    copyCSSVars() {
        navigator.clipboard.writeText(this.getCSSVars().join("\n"));
        this.showMessage("Copied CSS Vars");
    }
    onErrorMessage() {
        this.setState({ loading: false });
        alert("Error");
    }
    showMessage(message: string) {
        this.setState({ message: message });
        //reset message after 5 seconds
        setTimeout(() => {
            this.setState({ message: "" });
        }
            , 5000);

    }
    getInput() {
        if (this.state.loading) {
            return <Alert variant="info">Loading...</Alert>
        }
        return <div className="input-container">
            <input type='text' id="message" placeholder="Thai Sunset" defaultValue={this.state.prompt} onChange={this.handleChange.bind(this)} onKeyDown={this.checkForEnter.bind(this)}></input>
            <button onClick={() => { this.sendMessage() }}>Send</button>
        </div>
    }
    checkForEnter(event: any) {
        if (event.keyCode === 13) {
            this.sendMessage();
        }
    }
     sendMessage() {
        this.setState({ loading: true });
        DataCenter.getColors(this.state.prompt, this.gotColors.bind(this), this.onErrorMessage.bind(this));
    }
    gotColors(json: any) {
        console.log(json);
       
        this.setState({ loading: false });
       /*  let colorVo: ColorVo = ColorVo.fromJson(json);
        let pname: string = this.state.prompt;
        //capitalize first letter
        pname = pname.charAt(0).toUpperCase() + pname.slice(1);
        this.setState({ colorVo: colorVo, palletteName: pname, prompt: "" }); */
        let choices: any[] = json.choices;
        let pallette: any = choices[0].message.content;
        console.log(choices);
        console.log(pallette);
        let colorVo: ColorVo = ColorVo.fromJson(pallette);
        let pname: string = this.state.prompt;
        //capitalize first letter
        pname = pname.charAt(0).toUpperCase() + pname.slice(1);
        this.setState({ colorVo: colorVo, palletteName: pname, prompt: "" });

        this.ms.colorVo = colorVo;
        this.ms.prompt = pname;
        this.ms.saveToLocalStorage();
         /* if (!this.isValidJSON(json)) {
            this.onErrorMessage();
            return;
        } */
    }
    /* private isValidJSON(str: string): boolean {
        try {
            JSON.parse(str);
            return true;
        } catch (error) {
            return false;
        }
    } */
    render() {
        return (<div className='designcontainer'>
            <h2>{this.ms.prompt}</h2>
            {this.getColorBoxes()}
            {this.getInput()}
            <div className="iframe-container">
                <iframe title='codePreview' id="frame1" src={this.getRenderedCode()}></iframe>
            </div>
        </div>)
    }
}
export default PalScreen;