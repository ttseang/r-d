import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import PalScreen from './PalScreen';
import CodeScreen from './CodeScreen';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { }
interface MyState {mode:number}
class BaseScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage = MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {mode:-1};
        }
    componentDidMount(): void {
        this.ms.loadFromLocalStorage();
        this.setState({mode:0});
    }
    getScreen()
    {
        switch(this.state.mode)
        {

            case -1:
                return <div className='loading'>Loading...</div>
            case 0:
                return <PalScreen/>

            case 1:
                return <CodeScreen/>
            default:
                return <h3>content here</h3>
        }
        
    }
    getCodeScreen() {
        this.setState({mode:1});
    }
    getDesignScreen() {
        this.setState({mode:0});
    }
    getButton() {
        if (this.state.mode === 0) {
            return <button className='switch-button' onClick={this.getCodeScreen.bind(this)}>Code Screen</button>
        }
        else {
            return <button className='switch-button' onClick={this.getDesignScreen.bind(this)}>Design Screen</button>
        }
    }
    makeNewCode() {
        this.ms.clearLocalStorage();
        //reload the page
        window.location.reload();
    }
    render() {
        return (<div id="base">
            <header>
                <h1 className="title">Color Palette Generator</h1>
                <div className='button-container'>
                {this.getButton()}
                <button className='reset-button' onClick={this.makeNewCode.bind(this)}>New Code</button>
                </div>
            </header>
            {this.getScreen()}
        </div>)
    }
}
export default BaseScreen;