import { ChatVo } from "../dataObjs/ChatVo";
import ApiConnect from "./ApiConnect";


export class DataCenter {

    public static getColors( prompt: string, callback: Function, errorCallback: Function) {
        if (prompt === "") {
            alert("Please enter a prompt");
            return;
        }
        //1300
        const apiConnect: ApiConnect = new ApiConnect();
        let messages: ChatVo[] = [];
        messages.push(new ChatVo("system", "Provide the user with a color palette in a JSON object based on the prompt. There object should include a text color, background color, accent color, primary color, and secondary color, and a link color. Do not explain what you are doing. Just give the JSON object"));
        messages.push(new ChatVo("user", "make a pallette base on: " + prompt));


        // apiConnect.addParam(existingCode);
        // apiConnect.addParam(prompt);
        let msgObj: any = JSON.stringify(messages);
        console.log(msgObj);
        apiConnect.sendTT(msgObj, callback, errorCallback);

    }
}

/* $messasge = array();
$messasge[0]['role'] = "system";
$messasge[0]['content'] = "Provide the user with html and css code in a JSON object based on the prompt. Keep the html and css separate. If there is not already head and body tag create them. Do not explain what you are doing. Just give the JSON object. Use the following format for the JSON object: {\"html\":\"<div>hello</div>\",\"css\":\"div{color:red;}\"}";

if ($code == "") {
    $messasge[1]['role'] = "user";
    $messasge[1]['content'] = $prompt2;
} else {
    $messasge[1]['role'] = "system";
    $messasge[1]['content'] = "The existing code is: " . $code;

    $messasge[2]['role'] = "user";
    $messasge[2]['content'] = $prompt2;
}
 */