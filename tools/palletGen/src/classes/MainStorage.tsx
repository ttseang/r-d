import { ColorVo } from "../dataObjs/ColorVo";

/* eslint-disable @typescript-eslint/no-useless-constructor */
export class MainStorage {
    private static instance: MainStorage;
    public static getInstance(): MainStorage {
        if (!MainStorage.instance) {
            MainStorage.instance = new MainStorage();
        }
        return MainStorage.instance;
    }
    public colorVo: ColorVo = new ColorVo("#333333", "#f1f1f1", "#ffcc00", "#007bff", "#6c757d", "#ff3366");

    public prompt: string = "Ai Basic";

    public htmlString: string = `<!DOCTYPE html>
    <html>
    
    <head>
        <title>CSS Color Variables Test</title>
    </head>
    
    <body>
        <h1>Welcome to CSS Color Variables Test</h1>
        <p>This is a sample paragraph to test the text color.</p>
        <button class="primary-button">Primary Button</button>
        <button class="secondary-button">Secondary Button</button>
        <a href="#">This is a link</a>
    </body>
    
    </html>`;


//CSS CODE
    public cssString: string = `body {
        background-color: var(--background);
        color: var(--text);
        font-family: Arial, sans-serif;
    }
    
    h1 {
        color: var(--accent);
    }
    
    .primary-button {
        background-color: var(--primary);
        color: white;
        padding: 10px 20px;
        border-radius: 5px;
        text-decoration: none;
    }
    
    .secondary-button {
        background-color: var(--secondary);
        color: white;
        padding: 10px 20px;
        border-radius: 5px;
        text-decoration: none;
    }
    
    a {
        color: var(--link);
        text-decoration: none;
    }`;
    constructor() {
        (window as any).ms = this;
    }

    public saveToLocalStorage()
    {
        console.log("saving to local storage");
        console.log(this.htmlString);
        localStorage.setItem("html",this.htmlString);
        localStorage.setItem("css",this.cssString);
        localStorage.setItem("prompt",this.prompt);
        localStorage.setItem("pallette",JSON.stringify(this.colorVo));
    }
    public loadFromLocalStorage()
    {
        this.htmlString=localStorage.getItem("html") || this.htmlString;

        console.log(this.htmlString);

        this.cssString=localStorage.getItem("css")|| this.cssString;
        this.prompt=localStorage.getItem("prompt")|| this.prompt;
        let palletteString:string=localStorage.getItem("pallette")||"";

        if(palletteString!=="")
        {
            let palletteObj:ColorVo=JSON.parse(palletteString);
            this.colorVo=palletteObj;
        }
    }
    public clearLocalStorage()
    {
        localStorage.removeItem("html");
        localStorage.removeItem("css");
        localStorage.removeItem("prompt");
        localStorage.removeItem("pallette");
    }
}