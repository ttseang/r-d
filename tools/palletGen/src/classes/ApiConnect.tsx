//import { MainStorage } from "./MainStorage";

export class ApiConnect {
    // private ms:MainStorage=MainStorage.getInstance();
    public static GET_IDEA: number = 100;

    private params: string[] = [];

    public sendTT(messages:string, callback:Function, errorCallback:Function)
    {
        const url:string="https://customtrack.org/api/davinci.php";
        
        //make post variables
        
        const data = new FormData();
        //set to no cors
        data.append('cors', 'no');
        data.append('chat', messages);
        fetch(url, {
            method: 'POST',
            body: data
        }).then(response => response.json())
        .then(data => {
            ////////////console.log('Success:', data);
            callback(data);
        }).catch((error) => {
            ////////////console.log(error);
            console.error('Error:', error);
           // errorCallback(error);
        });
    }
    public clear() {
        this.params = [];
    }
    public addParam(data: string) {
        this.params.push(data);
    }
}
export default ApiConnect;
