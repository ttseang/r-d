//"{\n \"textColor\": \"#FFFFFF\",\n \"backgroundColor\": \"#FF0000\",\n \"accentColor\": \"#FF8C00\",\n \"primaryColor\": \"#FF4500\",\n \"secondaryColor\": \"#FF6347\",\n \"linkColor\": \"#FF69B4\"\n}"
export class ColorVo {
    public textColor: string;
    public backgroundColor: string;
    public accentColor: string;
    public primaryColor: string;
    public secondaryColor: string;
    public linkColor: string;
    constructor(textColor: string, backgroundColor: string, accentColor: string, primaryColor: string, secondaryColor: string, linkColor: string) {
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
        this.accentColor = accentColor;
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.linkColor = linkColor;
    }
    public static fromJson(json: string): ColorVo {
        let obj = JSON.parse(json);
        return new ColorVo(obj.textColor, obj.backgroundColor, obj.accentColor, obj.primaryColor, obj.secondaryColor, obj.linkColor);
    }
    
}