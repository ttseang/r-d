import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BaseScreen from './screens/BaseScreen';
import "./styles/codescreen.css";

function App() {
  return (
    <div className="App">
     <BaseScreen/>
    </div>
  );
}

export default App;
