import React from 'react';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import BaseScreen from './screens/BaseScreen';


function App() {
  return (
    <div id="app"><BaseScreen></BaseScreen></div>
  );
}

export default App;
