import React, { ChangeEvent, Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { callback: Function }
interface MyState { file: string }
class PicFileScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { file: "" };
    }
    getInputButton() {
        if (this.state.file !== "") {
            //  return this.getButtons();
        }
        return (<input type="file" onChange={this.handleChange.bind(this)} />)
    }
    handleChange(e: ChangeEvent<HTMLInputElement>) {
        if (e.currentTarget) {
            if (e.currentTarget.files) {
                let file: string = URL.createObjectURL(e.currentTarget.files[0]);
                console.log(file);
                let img: HTMLImageElement = new Image();
                img.onload = () => {
                   
                    this.ms.img = img;

                    this.props.callback();
                }
                img.src = file;

                // let fr:FileReader=new FileReader();

                /*   this.setState({ file: file });
                  this.props.callback(file); */
            }
        }
    }
    render() {
        return (<div>
            <Row><Col>{this.getInputButton()}</Col></Row>
        </div>)
    }
}
export default PicFileScreen;