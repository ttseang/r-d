import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { MainStorage } from '../classes/MainStorage';
interface MyProps { }
interface MyState {colorString:string,r:number,g:number,b:number,alpha:number,locked:boolean}
class PickScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private ctx: CanvasRenderingContext2D | null = null;
    
    constructor(props: MyProps) {
        super(props);
        this.state = {colorString:"rgba(10,10,10,1)",r:0,g:0,b:0,alpha:0,locked:false};
    }
    componentDidMount() {
        this.drawImage();
    }
    drawImage() {
        var myCanvas: HTMLCanvasElement | null = document.getElementById('myCanvas') as HTMLCanvasElement;
        if (myCanvas) {
            this.ctx = myCanvas.getContext('2d');
            if (this.ms.img) {

                if (this.ctx) {

                    let w: number = this.ms.img.width;
                    let h: number = this.ms.img.height;
                    if (w > 400) {
                        let ratio = 400 / w;
                        w = 400;
                        h = h * ratio;
                    
                    }

                    this.ctx.drawImage(this.ms.img, 0, 0,w,h); // Or at whatever offset you like
                }

            }
        }
    }
    toggleLock()
    {
        this.setState({locked:!this.state.locked});
    }
    onOver(e: React.PointerEvent<HTMLCanvasElement>) {
        if (this.state.locked===true)
        {
            return false;
        }
       // console.log(e.clientX, e.clientY);
       if (this.ctx)
       {
           let xx:number=e.clientX-e.currentTarget.getBoundingClientRect().left;
           let yy:number=e.clientY-e.currentTarget.getBoundingClientRect().top;

           let colors:ImageData | undefined=this.ctx.getImageData(xx,yy,1,1);
           //console.log(colors);
           let red:number=colors.data[0];
           let green:number=colors.data[1];
           let blue:number=colors.data[2];
           let alpha:number=colors.data[3];
           let cs:string='rgba('+red.toString()+','+green.toString()+', '+blue.toString()+','+alpha.toString()+')';
           
           this.setState({colorString:cs,r:red,g:green,b:blue,alpha:alpha});
       }    
       
    }
    doCopy(text:string)
    {
        const copy = require('clipboard-copy')
        copy(text);
    }
    componentToHex(c:number) {
        var hex = c.toString(16);
        return hex.length ===1 ? "0" + hex : hex;
      }
    rgbToHex() {
        return "#" + this.componentToHex(this.state.r) + this.componentToHex(this.state.g) + this.componentToHex(this.state.b)+this.componentToHex(this.state.alpha);
      }
    render() {
        let w: number = 100;
        let h: number = 100;
        if (this.ms.img) {
            w = this.ms.img.width;
            h = this.ms.img.height;
        }
                    if (w > 400) {
                        let ratio = 400 / w;
                        w = 400;
                        h = h * ratio;
                    
                    }

        //let rString:string="red:"+this.state.r.toString()+" blue:"+this.state.b.toString()+" green:"+this.state.g.toString()+" alpha:"+this.state.alpha.toString();
       let msg:string="Click Image to lock color selection";
       if (this.state.locked===true)
       {
           msg="Click Image AGAIN to unlock selection";
       }
        return (<div className='tac'>
            <canvas id="myCanvas" width={w} height={h} onPointerMove={this.onOver.bind(this)} onClick={this.toggleLock.bind(this)}></canvas>
            <hr/>
            <Row>
                <Col sm={2} className="tac"><div id="block" style={{backgroundColor:this.state.colorString}}></div></Col><Col sm={8}>{msg}</Col>
            </Row>
            <hr/>
            <Row><Col sm="3">RGBA</Col><Col sm="6">{this.state.colorString}</Col><Col><Button size="sm" onClick={()=>{this.doCopy(this.state.colorString)}}>Copy</Button></Col></Row>
            <hr/>
            <Row><Col sm="3">Hex</Col><Col sm="6">{this.rgbToHex()}</Col><Col><Button size='sm' onClick={()=>{this.doCopy(this.rgbToHex())}}>Copy</Button></Col></Row>    
        </div>)
    }
}
export default PickScreen;