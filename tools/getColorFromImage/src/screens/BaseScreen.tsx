import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

import { MainStorage } from '../classes/MainStorage';
import PicFileScreen from './PicFileScreen';
import PickScreen from './PickScreen';
//import { StartData } from '../classes/StartData';

interface MyProps { }
interface MyState {mode:number}
class BaseScreen extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();
    private catID:number=0;
    private listID:number=0;
    

        constructor(props: MyProps) {
            super(props);
            this.state={mode:0}
        }
    componentDidMount()
    {
      
        //this.getImage();
    }
   
  
    goBack()
    {
       /*  switch(this.state.mode)
        {
            case 2:
                this.setState({mode:1});
            break;

            case 3:
                this.setState({mode:2});
            break;
        } */
    }
    getImage()
    {
        var img = new Image();
           
            img.onload=()=>{                
                this.ms.img=img;
                this.setState({mode:1});
            }
            img.src = "./desktop.png";
    }
    getScreen()
    {
        switch(this.state.mode)
        {

            case 0:

            return <PicFileScreen callback={()=>{this.setState({mode:1})}}></PicFileScreen>
            case 1:

             return <PickScreen></PickScreen>

            
        }
    }
    render() {
        return (<div id="base">
            <Card>
                <Card.Header>Title here</Card.Header>
                <Card.Body>{this.getScreen()}</Card.Body>
            </Card>
                </div>)
    }
}
export default BaseScreen;