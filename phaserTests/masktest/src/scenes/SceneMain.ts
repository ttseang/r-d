import { GameObjects } from "phaser";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private graphics:Phaser.GameObjects.Graphics;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.image("heart","./assets/heart.png");
        this.load.image("num5","./assets/num5.png");
    }
    create() {
        super.create();
        this.makeGrid(640, 480);
        //this.grid.showNumbers();
        console.log("create!");

        let guide:GameObjects.Sprite=this.add.sprite(0,0,"num5").setOrigin(0,0);
       // guide.x=150;
       // guide.y=150;
       Align.scaleToGameW(guide,0.4,this);

        let letterGrid:AlignGrid=new AlignGrid(this,22,22,guide.displayWidth,guide.displayHeight);
        letterGrid.show();
       // 
      //  this.grid.placeAt(150,150,guide);
        /* this.grid.placeAt(4,4,guide);

        guide.displayWidth=this.cw*4;
        guide.displayHeight=this.ch*4;

        let high:GameObjects.Sprite=this.add.sprite(0,0,"num5").setOrigin(0,0);

        high.x=guide.x;
        high.y=guide.y;
        high.setTint(0xff0000);
        high.scaleX=guide.scaleX;
        high.scaleY=guide.scaleY;
        this.graphics=this.add.graphics(); */

        
        /* let con:Phaser.GameObjects.Container=this.add.container(0,0);

        for (let i: number = 0; i < 3; i++) {
            for (let j: number = 0; j < 3; j++) {
                let face: Phaser.GameObjects.Image = this.add.image(100, 200, "face");

                Align.scaleToGameW(face, 0.1, this);
                

                this.grid.placeAt(j, i, face);
              //  face.alpha=0.01;
                face.setInteractive();
                con.add(face);
            }
        }
        this.grid.placeAtIndex(36,con);

        let heart:GameObjects.Sprite=this.add.sprite(0,0,"heart");
        this.grid.placeAtIndex(48,heart);

        con.mask=new Phaser.Display.Masks.BitmapMask(this,heart);
        heart.visible=false;

        this.input.on('gameobjectover',this.lightUp.bind(this)) */
    }
    lightUp(pointer,gameObj)
    {
        gameObj.alpha=1;
    }

}