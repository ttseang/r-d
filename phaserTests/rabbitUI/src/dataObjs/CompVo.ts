import { AnchorVo } from "./AnchorVo";

export class CompVo
{
    public key:string;
    public type:string;
    public x:number;
    public y:number;
    public w:number;
    public h:number;
    public style:string;
    public backstyle:string;

    public anchorVo:AnchorVo=null;
    
    constructor(key:string,type:string,x:number,y:number,w:number,h:number,style:string,backstyle:string)
    {
        this.key=key;
        this.type=type;
        this.x=x;
        this.y=y;
        this.w=w;
        this.h=h;
        this.style=style;
        this.backstyle=backstyle;
    }
}