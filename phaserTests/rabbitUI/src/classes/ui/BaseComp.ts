import { GameObjects } from "phaser";
import { BackStyleVo } from "../../dataObjs/BackStyleVo";
import { PosVo } from "../../dataObjs/PosVo";
import IBaseScene from "../../interfaces/IBaseScene";
import { IComp } from "../../interfaces/IComp";
import { CompManager } from "../CompManager";

export class BaseComp extends GameObjects.Container implements IComp {
    public posVo: PosVo = new PosVo(0, 0);
    public bscene: IBaseScene;
    protected cm: CompManager = CompManager.getInstance();

    private anchorObj: IComp | null = null;
    private anchorX: number = 0;
    private anchorY: number = 0;
    private anchorInside: boolean = false;
    protected backStyleVo:BackStyleVo;

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.cm.comps.push(this);
    }
    doResize() {
        this.bscene.getGrid().placeAt(this.posVo.x, this.posVo.y, this);
        this.reAnchor();
    }
    setPos(xx: number, yy: number) {
        this.posVo = new PosVo(xx, yy);
        this.bscene.getGrid().placeAt(xx, yy, this);
    }
    anchorTo(obj: IComp, anchorX: number, anchorY: number, inside: boolean) {
        this.anchorObj = obj;
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.anchorInside = inside;
        this.reAnchor();
    }
    reAnchor() {
        if (this.anchorObj === null) {
            return;
        }
        if (this.anchorX != -1) {
            if (this.anchorInside == false) {


                switch (this.anchorX) {
                    case 0:

                        this.x = this.anchorObj.x - this.anchorObj.displayWidth / 2 + this.displayWidth / 2;
                        break;

                    case 0.5:

                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2;

                        break;

                    case 1:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2 - this.displayWidth / 2;
                        break;
                }
            }
            else
            {
                switch (this.anchorX) {
                    case 0:

                        this.x = this.anchorObj.x - this.anchorObj.displayWidth / 2 + this.displayWidth*2;
                        break;

                    case 0.5:

                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2;

                        break;

                    case 1:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2-this.displayWidth/1.5;
                        break;
                }
            }
        }

        if (this.anchorY!=-1)
        {
            if (this.anchorInside == false) {


                switch (this.anchorY) {
                    case 0:

                        this.y = this.anchorObj.y - this.anchorObj.displayHeight / 2 + this.displayHeight / 2;
                        break;

                    case 0.5:

                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2;

                        break;

                    case 1:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2 - this.displayHeight / 2;
                        break;
                }
            }
            else
            {
                switch (this.anchorY) {
                    case 0:

                        this.y = this.anchorObj.y - this.anchorObj.displayHeight / 2 + this.displayHeight;
                        break;

                    case 0.5:

                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2;

                        break;

                    case 1:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2-this.displayHeight/1.5;
                        break;
                }
            }            
        }
    }
}