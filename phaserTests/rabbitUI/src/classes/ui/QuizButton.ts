import { GameObjects } from "phaser";
import { QuizButtonVo } from "../../dataObjs/QuizButtonVo";
import IBaseScene from "../../interfaces/IBaseScene";
import Align from "../../util/align";
import { GM } from "../GM";

export class QuizButton extends GameObjects.Container {
    private bscene: IBaseScene;
    private quizButtonVo: QuizButtonVo;
    private back: GameObjects.Graphics;
    private border: GameObjects.Graphics;

    private text1: GameObjects.Text;

    private ww: number;
    private hh: number;

    private gm:GM=GM.getInstance();

    constructor(bscene: IBaseScene, quizButtonVo: QuizButtonVo) {
        super(bscene.getScene());

        this.bscene = bscene;
        this.quizButtonVo = quizButtonVo;

        let ww: number = this.bscene.getW() * 0.35;
        let hh: number = ww / 5;

        this.ww = ww;
        this.hh = hh;

        this.text1 = this.scene.add.text(0, 0, quizButtonVo.text, { color: quizButtonVo.textColor,fontFamily:this.gm.mainFont, fontSize: "22px", fontStyle: "bold", wordWrap: { useAdvancedWrap: true, width: ww * 0.8 } });
        //.setOrigin(0.5, 0.5);

        //  hh=this.text1.displayHeight*3;

        this.back = this.scene.add.graphics();
        this.border = this.scene.add.graphics();

        this.border.lineStyle(2, quizButtonVo.borderColor);
        this.border.strokeRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.border.stroke();

        this.back.fillStyle(quizButtonVo.backColor, 1);
        this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.back.fillPath();

        this.add(this.back);
        this.add(this.border);


        this.add(this.text1);
        
        this.text1.x=-ww*0.4;
        this.text1.y=-this.text1.displayHeight/2;


        this.scene.add.existing(this);
        this.setSize(ww, hh);

    }
    resetBorder() {
        this.border.lineStyle(2, this.quizButtonVo.borderColor);
        this.border.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.border.stroke();
    }
    setBorder(color: number) {
        this.border.lineStyle(2, color);
        this.border.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.border.stroke();
    }
}