import { CompManager } from "../classes/CompManager";
import { BorderButton } from "../classes/ui/BorderButton";
import { CheckBox } from "../classes/ui/CheckBox";
import { IconButton } from "../classes/ui/IconButton";
import { ResponseImage } from "../classes/ui/ResponseImage";
import { TriangleButton } from "../classes/ui/TriangleButton";
import UISlider from "../classes/ui/UISlider";
import { UIWindow } from "../classes/ui/UIWindow";
import { BackStyleVo } from "../dataObjs/BackStyleVo";
import { ButtonStyleVo } from "../dataObjs/ButtonStyleVo";
import { IComp } from "../interfaces/IComp";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private compManager:CompManager=CompManager.getInstance();

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("rabbit","./assets/rabbit.png");
        this.load.image("check","./assets/check.png");
        this.load.image("keyboard","./assets/keyboard.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);
      //  this.grid.showPos();

        window['scene']=this;

        let uiWindow:UIWindow=new UIWindow(this,.8,.7,0x375982,0xffffff);
        uiWindow.setPos(5,5);

       // Align.center(uiWindow,this);
        let bstyle1:BackStyleVo=new BackStyleVo(0x5881BB,4,0xffffff,0xff00ff,0x0ff00);
        let bstyle2:BackStyleVo=new BackStyleVo(0xff0000,4,0xffffff,0xff00ff,0x0ff00);

        this.compManager.regBackStyle("blue",bstyle1);
        this.compManager.regBackStyle("red",bstyle2);



        let buttonVo:ButtonStyleVo=new ButtonStyleVo("#ffffff",0.016,0.08,"blue");
        let buttonVo2:ButtonStyleVo=new ButtonStyleVo("#ffffff",0.016,0.08,"red");
        


        let btnHome:BorderButton=new BorderButton(this,"Home",buttonVo);
        btnHome.setPos(1.25,0.5);
        btnHome.anchorTo(uiWindow,0,-1,false);


        let btnMenu:BorderButton=new BorderButton(this,"Menu",buttonVo);
        btnMenu.setPos(9,0.5);
        btnMenu.anchorTo(uiWindow,1,-1,false);
        
       

        let btnHint:BorderButton=new BorderButton(this,"Hint",buttonVo2);
        btnHint.setPos(8.5,2);
        btnHint.anchorTo(uiWindow,1,0,true);
         
        let rabbit:ResponseImage=new ResponseImage(this,0.15,"rabbit");
        rabbit.flipX=true;
        rabbit.setPos(1,8);

        let triButtonVo:ButtonStyleVo=new ButtonStyleVo("#ffffff",0.045,0.045,"blue");

        let btnTri:TriangleButton=new TriangleButton(this,triButtonVo,-90);
        btnTri.setPos(2.5,9.5);

        let btnTri2:TriangleButton=new TriangleButton(this,triButtonVo,90);
        btnTri2.setPos(7.5,9.5);
        
        let slider:UISlider=new UISlider(this,.4,0.04,0xcccccc,0xff0000);
        slider.setPos(5,9.5);

        let btnCheck:CheckBox=new CheckBox(this,0.08,"blue");
        btnCheck.setPos(8,9.5);
        
        let iconButton:IconButton=new IconButton(this,"keyboard",buttonVo);
        iconButton.setPos(9,9.5);

        
       // this.grid.placeAt(5,5,btnBorder);

        window.onresize=this.doResize.bind(this);
    }

    doResize()
    {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();

        this.compManager.doResize();
    }
      
    }
