
import { CompManager, TextStyleVo, BackStyleVo } from "ttphasercomps";
import { ButtonStyleVo } from "ttphasercomps/dataObjs/ButtonStyleVo";
import { CompGame } from "../classes/CompGame";
import { ListBox } from "../classes/ListBox";
import { SylBox } from "../classes/SylBox";

import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private cm:CompManager=CompManager.getInstance();
    private compGame:CompGame;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("qmark","./assets/questionmark.png");
        this.load.image("right","./assets/right.png");
        this.load.image("wrong","./assets/wrong.png");
        this.load.image("triangle","./assets/triangle.png");
    }
    create() {

        super.create();
        this.makeGrid(11,11);
        this.grid.showPos();

        
        window.onresize=()=>{

            let w:number = window.outerWidth;
            let h:number = window.outerHeight;
            this.resetSize(w, h);
            this.scale.resize(w, h);
            this.grid.hide();
            this.makeGrid(11, 11);

            console.log(this.getW(),this.getH());

            this.cm.doResize();
            
            if (this.compGame)
            {
                this.compGame.doResize();
            }
            
        }


        this.cm.regTextStyle("white",new TextStyleVo("Arial","#ffffff",30));
        this.cm.regBackStyle("blue",new BackStyleVo(0x5C83B7,3,0xffffff,0x00000,0xffffff));
        this.cm.regButtonStyle("bb",new ButtonStyleVo("white",0.08,0.08,"blue"));

        this.compGame=new CompGame(this);
        this.compGame.start();
    }
}