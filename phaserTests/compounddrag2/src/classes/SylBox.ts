import { GameObjects } from "phaser";
import { IBaseScene } from "ttphasercomps";



export class SylBox extends GameObjects.Container
{
    private text1:GameObjects.Text;
    public back:GameObjects.Image;

    private ox:number=0;
    private oy:number=0;
    public text:string;
    public index:number;

    constructor(bscene:IBaseScene,text:string,index:number)
    {
        super(bscene.getScene());

        this.text=text;
        this.index=index;
        
        this.back=this.scene.add.image(0,0,"holder");

        this.text1=this.scene.add.text(0,0,text,{fontSize:"24px",color:"black"}).setOrigin(0,0.5);

        this.back.displayWidth=this.text1.displayWidth*1.1;
        this.back.displayHeight=this.text1.displayHeight*1.5;

        this.text1.x=this.back.x+this.back.displayWidth/2-this.text1.displayWidth;


        this.add(this.back);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.scene.add.existing(this);
    }
    setFontSize(size:number)
    {
        this.text1.setFontSize(size);
        this.back.displayWidth=this.text1.displayWidth*1.1;
        this.back.displayHeight=this.text1.displayHeight*1.5;

        this.text1.x=this.back.x+this.back.displayWidth/2-this.text1.displayWidth;
        this.setSize(this.back.displayWidth,this.back.displayHeight);
    }
    setText(text:string)
    {
        this.text=text;
        this.text1.setText(text);
        this.back.displayWidth=this.text1.displayWidth*1.1;
        this.back.displayHeight=this.text1.displayHeight*1.5;

        this.text1.x=this.back.x+this.back.displayWidth/2-this.text1.displayWidth;
        this.setSize(this.back.displayWidth,this.back.displayHeight);
    }
    resetPos()
    {
        this.x=this.ox;
        this.y=this.oy;
        this.visible=true;
    }
    setBackSize(ww:number)
    {
        this.back.displayWidth=ww;

        this.text1.x=this.back.x+this.back.displayWidth/2-this.text1.displayWidth;
    }
    initPos()
    {
        this.ox=this.x;
        this.oy=this.y;
    }
    public snapBack()
    {
        this.scene.add.tween({targets:[this],duration:500,x:this.ox,y:this.oy})       
    }
}