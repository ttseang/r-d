import { Loader } from "phaser";
import { IBaseScene, BlankComp, IconButton, CompManager, ResponseImage, ButtonController, TextStyleVo, UIWindow, PosVo, CompLayout } from "ttphasercomps";
import { ButtonStyleVo } from "ttphasercomps/dataObjs/ButtonStyleVo";

import { WordPartVo } from "../dataObjs/WordPartVo";

import { ListBox } from "./ListBox";
import { QBox } from "./QBox";
import { SylBox } from "./SylBox";

export class CompGame {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private words: WordPartVo[] = [];
    private wordList: string[] = [];
    private boxes: SylBox[] = [];
    private picks: number[] = [];

    private box: SylBox;
    private qbox: QBox;


    private listHolder: BlankComp;
    private qBoxHolder: BlankComp;

    private btnHelp: IconButton;
    private btnPlay: IconButton;
    private btnCheck: IconButton;

    private cm: CompManager = CompManager.getInstance();

    private rightImage: ResponseImage;
    private wrongImage: ResponseImage;

    private correct: number = 0;
    private selected: number = 0;

    private lb: ListBox;
    private clickLock: boolean = false;
    private uiWindow:UIWindow;

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;
        window['cg']=this;
    }
    start() {

        this.scene.load.json("words", "./assets/words.json");
        this.scene.load.loadComplete = this.dataLoaded.bind(this);
        this.scene.load.start();
    }
    dataLoaded() {
        let words = this.scene.cache.json.get("words").words;
        console.log(words);

        for (let i: number = 0; i < words.length; i++) {
            let word: any = words[i];
            let wordVo: WordPartVo = new WordPartVo(word.word, word.p1, word.p2);
            this.words.push(wordVo);

            this.wordList.push(word.word);
        }
        this.makeUI();
        this.create();

        let bc: ButtonController = ButtonController.getInstance();
        bc.callback = this.buttonAction.bind(this);
    }
    buttonAction(action: string, params: string) {
        console.log(action);
        console.log(params);
        if (action === "check") {
            this.checkRight();
        }
    }
    makeUI() {

        
        let textStyle: TextStyleVo = new TextStyleVo("Arial", "#ff0000", 40);

        let blueButton: ButtonStyleVo = new ButtonStyleVo("red", 0.7, 0.7, "blue");

        let uiWindow: UIWindow = new UIWindow(this.bscene, "backwindow", 0.85, 0.7, "default");
        uiWindow.setPos(5, 5);
        this.uiWindow=uiWindow;



        this.btnHelp = new IconButton(this.bscene, "btnHelp", "qmark", "help", "", this.cm.getButtonStyle("bb"));
        this.btnHelp.setPos(4, 9.5);

        this.btnPlay = new IconButton(this.bscene, "btnPlay", "triangle", "play", "", this.cm.getButtonStyle("bb"));
        this.btnPlay.setPos(5, 9.5);

        this.btnCheck = new IconButton(this.bscene, "btnCheck", "check", "check", "", this.cm.getButtonStyle("bb"));
        this.btnCheck.setPos(6, 9.5);

        this.rightImage = new ResponseImage(this.bscene, "rightImage", 0.05, "right");
        this.rightImage.setPos(6.5, 7);
        this.rightImage.visible = false;

        this.wrongImage = new ResponseImage(this.bscene, "wrong", 0.05, "wrong");
        this.wrongImage.setPos(6.5, 7);
        this.wrongImage.visible = false;

        this.lb = new ListBox(this.bscene, this.wordList, 3);
        this.lb.x = -this.lb.displayWidth / 2 + (this.lb.displayWidth * 0.05);
        this.lb.y = -this.lb.displayHeight / 2;

        this.listHolder = new BlankComp(this.bscene, "listHolder", this.lb, "default");
        this.listHolder.setPos(3, 5);
    }
    doResize()
    {
        let pos: PosVo = this.bscene.getGrid().getRealXY(7.5, 5);
        let xx: number = pos.x;
        let yy: number = pos.y;

        let ww:number=0;

        let fs:number=(this.bscene.getW()/300)+30;
        let fs2:number=(this.bscene.getW()/300)+20;
        console.log("fs="+fs);
        console.log("fs2="+fs2);
        console.log("w="+this.bscene.getW());

        for (let i: number = 0; i < this.boxes.length; i++) {

            this.boxes[i].setFontSize(fs);

            if (this.boxes[i].displayWidth>ww)
            {
                ww=this.boxes[i].displayWidth;
            }
        }

        for (let i: number = 0; i < this.boxes.length; i++) {
            this.boxes[i].setBackSize(ww);
            this.boxes[i].x = xx;
            this.boxes[i].y = yy;
            this.boxes[i].initPos();
            xx += ww * 1.3;
            //this.bscene.getGrid().placeAt(6+(i*.5),5,this.boxes[i]);
        }
        this.lb.doResize(fs2);
        this.lb.x = -this.lb.displayWidth / 2 + (this.lb.displayWidth * 0.05);
        this.lb.y = -this.lb.displayHeight / 2;
        
        this.listHolder.doResize();
       // this.listHolder.placeInside(this.uiWindow,.05,.25);

       this.qbox.setFontSize(fs,ww);
       this.qBoxHolder.doResize();
    }
    create() {

        console.log(this.words);


        let picks: number[] = this.pickThree();
        this.picks = picks;
        this.correct = Math.floor(Math.random() * 3);


        for (let j: number = 0; j < picks.length; j++) {
            // let index: number = picks[j];

            let sb: SylBox = new SylBox(this.bscene, "", j);
            // sb.setInteractive();
            this.boxes.push(sb);

        }
        this.pickRand();

        this.scene.input.on("gameobjectdown", this.startDrag.bind(this));
        this.scene.input.on("pointerup", this.onUp.bind(this));
        this.scene.input.on("pointermove", this.onMove.bind(this));

        setTimeout(() => {
            this.doResize();
        }, 1000);
    }
    pickRand() {
        let picks: number[] = this.pickThree();
        this.picks = picks;
        this.correct = Math.floor(Math.random() * 3);

        let ww: number = 0;

        for (let j: number = 0; j < picks.length; j++) {
            let index: number = picks[j];

            let sb: SylBox = this.boxes[j];
            sb.visible = true;
            sb.setText(this.words[index].part1)
            sb.setInteractive();
            if (sb.displayWidth > ww) {
                ww = sb.displayWidth * 1.1;
            }
        }

        let pos: PosVo = this.bscene.getGrid().getRealXY(7.5, 5);
        let xx: number = pos.x;
        let yy: number = pos.y;

        for (let i: number = 0; i < picks.length; i++) {
            this.boxes[i].setBackSize(ww);
            this.boxes[i].x = xx;
            this.boxes[i].y = yy;
            this.boxes[i].initPos();
            xx += ww * 1.3;
            //this.bscene.getGrid().placeAt(6+(i*.5),5,this.boxes[i]);
        }


        if (this.qBoxHolder) {
            this.qBoxHolder.destroy();
        }
        if (this.qbox) {
            this.qbox.destroy();
        }

        let qBox: QBox = new QBox(this.bscene, this.words[picks[this.correct]], ww);
        this.qbox = qBox;


        console.log(this.words[this.correct]);

        this.qBoxHolder = new BlankComp(this.bscene, "qbox", qBox, "defualt");
        this.qBoxHolder.setPos(8, 7);

    }
    startDrag(p: Phaser.Input.Pointer, box: SylBox) {
        if (this.clickLock == true) {
            return;
        }
        if (box instanceof (SylBox) == false) {
            return;
        }
        this.box = box;
        this.scene.children.bringToTop(this.box);
    }
    onMove(p: Phaser.Input.Pointer) {
        if (this.box) {
            this.box.x = p.x;
            this.box.y = p.y;
        }
    }
    onUp() {
        if (!this.qbox || !this.box) {
            return;
        }

        if (this.box instanceof (SylBox) == false) {
            return;
        }


        let distX: number = Math.abs(this.box.x - this.qBoxHolder.x);
        let distY: number = Math.abs(this.box.y - this.qBoxHolder.y);

        this.selected = this.box.index;

        // console.log(distX, distY);

        if (distX < this.qbox.displayWidth / 2 && distY < this.qbox.displayHeight / 2) {
            this.box.visible = false;

            this.qbox.setText(this.box.text);
            this.qbox.setTint(0xcccccc);
            this.rightImage.visible = false;
            this.wrongImage.visible = false;

            for (let i: number = 0; i < this.boxes.length; i++) {
                if (this.boxes[i] != this.box) {
                    this.boxes[i].resetPos();
                }
            }
        }
        else {
            this.box.snapBack();
        }
        // this.checkRight();
        this.box = null;
    }
    checkRight() {
        if (this.selected == this.correct) {
            // console.log("right");

            let word: string = this.words[this.picks[this.correct]].word;
            //   console.log(word);
            this.lb.strikeFromList(word);


            // this.words.splice(this.picks[this.correct],1);

            this.rightImage.visible = true;
            this.wrongImage.visible = false;
            this.qbox.setTint(0xC8E6C9);
            this.clickLock = true;
            setTimeout(() => {
                this.resetGame();
            }, 1000);
        }
        else {
            this.wrongImage.visible = true;
            this.rightImage.visible = false;
            this.qbox.setTint(0xff0000);
            console.log("wrong");
        }
    }
    resetGame() {
        this.rightImage.visible = false;
        this.wrongImage.visible = false;
        this.qbox.setTint(0xcccccc);
        this.pickRand();
        this.clickLock = false;
    }
    pickThree() {
        let indexes: number[] = [];
        let len: number = this.words.length;
        for (let i: number = 0; i < len; i++) {
            indexes.push(i);
        }

        for (let j: number = 0; j < 100; j++) {
            let r1: number = Math.floor(Math.random() * len);
            let r2: number = Math.floor(Math.random() * len);

            let temp: number = indexes[r1];
            indexes[r1] = indexes[r2];
            indexes[r2] = temp;
        }

        return indexes.slice(0, 3);
    }
}