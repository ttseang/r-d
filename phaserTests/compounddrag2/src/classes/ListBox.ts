
import { BaseComp, IComp } from "ttphasercomps";
import { IBaseScene } from "ttphasercomps/interfaces/IBaseScene";



export class ListBox extends BaseComp implements IComp
{
    private listText:Phaser.GameObjects.Text[]=[];
    private itemList:string[]=[];
    private cols:number=0;
    private colWidths:number[]=[];
    private fs:number=36;

    constructor(bscene:IBaseScene,itemList:string[],cols:number=3)
    {
        super(bscene,"listBox");

        this.itemList=itemList;
        this.cols=cols;       
        
        this.buildList();

        this.scene.add.existing(this);
    }
    doResize()
    {
        
        this.buildList();
        super.doResize();
    }
    public strikeFromList(word:string)
    {
        if (!this.itemList.includes(word))
        {
            return;
        }
        let ck:number=this.itemList.indexOf(word);

        console.log("ck="+ck);
        console.log(this.itemList.slice());
        this.itemList.splice(ck,1);
        console.log(this.itemList.slice());
       

        this.buildList();
    }
    destroyList()
    {
        for (let i:number=0;i<this.listText.length;i++)
        {
            this.listText[i].destroy();
        }
        this.listText=[];
    }
    setPos2(xx:number,yy:number)
    {
        super.setPos(xx,yy);
    }
    buildList()
    {
        this.destroyList();
        for (let i:number=0;i<this.cols;i++)
        {
            this.colWidths[i]=0;
        }

        
        let col:number=0;
        let th:number=0;
        let ww:number=0;
        let hh:number=0;

        for (let i:number=0;i<this.itemList.length;i++)
        {
            
            let tb:Phaser.GameObjects.Text=this.scene.add.text(0,0,this.itemList[i],{fontSize:"36px","color":"black"});
            tb.setFontSize(this.fs);
            this.add(tb);
            this.listText.push(tb);

            if (this.colWidths[i]<tb.displayWidth)
            {
                this.colWidths[i]=tb.displayWidth;
            }
            col++;
            if (col==this.cols)
            {
                col=0;
            } 
            if (tb.displayHeight>th)
            {
                th=tb.displayHeight*1.1;
            }          
        }

        let xx:number=0;
        let yy:number=0;
        let col2:number=0;
        let yMod:number=1.6;
        let xMod:number=1.3;

        for (let i:number=0;i<this.itemList.length;i++)
        {
            this.listText[i].x=xx;
            this.listText[i].y=yy;

            xx+=this.colWidths[col2]*xMod;
            col2++;

            if (col2===this.cols)
            {
                col2=0;
                yy+=th*yMod;
                xx=0;
                hh+=th*yMod;
            }
        }

        for (let i:number=0;i<this.cols;i++)
        {
            console.log(this.colWidths[i]);

            ww+=this.colWidths[i]*xMod;
        }
        console.log(ww,hh);

        this.setSize(ww,hh);
    }

}