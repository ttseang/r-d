export class WordPartVo
{
    public word:string;
    public part1:string;
    public part2:string;

    public used:boolean=false;

    constructor(word:string,part1:string,part2:string)
    {
        this.word=word;
        this.part1=part1;
        this.part2=part2;
    }
}