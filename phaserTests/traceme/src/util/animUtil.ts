export class AnimationUtil {
    AnimationUtil() {}
    makeChar(scene, key, char) {
        console.log(char);
        if (char.indexOf("_fairy") > -1) {
            this.makeFairy(scene, key);
            return;
        }
        if (char.indexOf("_wizard") > -1) {
            this.makeWizard(scene, key);
            return;
        }
        if (char.indexOf("_elf") > -1) {
            let findex = char.split("_elf")[1];
            this.makeElf(scene, key, findex);
            return;
        }
        if (char.indexOf("_archer") > -1) {
            let aindex = parseInt(char.split("_archer")[1]) - 1;
            this.makeArcher(scene, key, aindex);
        }
        if (char.indexOf("_dwarf") > -1) {
            let dindex = parseInt(char.split("_dwarf")[1]);
            this.makeDwarf(scene, key, dindex);
        }
    }
    makeEnemy(scene, key, char) {
        console.log(char);
        if (char.indexOf("_shamans") > -1) {
         //   this.makeShamans(scene, key);
        }
        if (char.indexOf("assassin")>-1)
        {
            
            let aindex = parseInt(char.split("assassin")[1]);
            this.makeAssassin(scene,key,aindex);
        }
        if (char.indexOf("demon")>-1)
        {
           let aindex2 = parseInt(char.split("demon")[1]);
            this.makeDemon(scene,key,aindex2);   
        }
        if (char.indexOf('monster')>-1)
        {
            this.makeMonster(scene,key);
        }
    }
    makeAnims(scene, key) {
        console.log("anim key=" + key);
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Attack__',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'slide',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Slide__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpAttack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Attack__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpThrow',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Throw__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Idle__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Dead__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Run__',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: -1
        });
    }
    makeAnims2(scene, key) {
        //console.log("anim key=" + key);
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Attack (',
                suffix: ').png'
            }),
            frameRate: 32,
            repeat: -1
        });
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'slide',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Slide (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpAttack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Attack (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpThrow',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 0,
                prefix: 'JumpShoot (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 0,
                prefix: 'Idle (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 0,
                prefix: 'Dead (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        let frames = scene.anims.generateFrameNames(key, {
            start: 0,
            end: 10,
            zeroPad: 0,
            prefix: 'Run (',
            suffix: ').png'
        });
        //console.log(frames);
        scene.anims.create({
            key: 'run',
            frames: frames,
            frameRate: 8,
            repeat: -1
        });
    }
    makeDog(scene, key) {
        console.log("anim key=" + key);
        
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 7,
                zeroPad: 3,
                prefix: 'jump_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'walk',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'walk_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
       /*  scene.anims.create({
            key: 'slide',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'slide_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        }); */
        
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'idle',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
      
    }
    makeFlyingMonster(scene, key) {
        let frames = scene.anims.generateFrameNames(key, {
            start: 0,
            end: 17,
            zeroPad: 3,
            prefix: 'Idle Blinking_',
            suffix: '.png'
        });
        //console.log(frames);
        scene.anims.create({
            key: 'flying' + key,
            frames: frames,
            frameRate: 8,
            repeat: -1
        });
    }
    makeElf(scene, key, index) {
        let elfKey = "Elf_0" + index;
        console.log(elfKey);
        //Elf_01_Walking_
        //Elf_01_Idle Blinking_000
        //Elf_01_Shooting_000
        //
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: elfKey + '_Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: elfKey + '_Walking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: elfKey + '_Shooting_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: elfKey + '_Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
    }
    makeFairy(scene, key) {
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 14,
                zeroPad: 3,
                prefix: 'Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Moving Forward_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Casting Spells_',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
    }
    makeArcher(scene, key, index) {
        let archerKey = "0_Archer_";
        console.log(archerKey);
        console.log(key);
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 17,
                zeroPad: 3,
                prefix: archerKey + 'Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 14,
                zeroPad: 3,
                prefix: archerKey + 'Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: archerKey + 'Running_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: archerKey + 'Shooting in The Air_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
    }
    makeWizard(scene, key) {
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 14,
                zeroPad: 3,
                prefix: 'Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 17,
                zeroPad: 3,
                prefix: 'Walking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Casting Spells_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
    }
    makeDwarf(scene, key, index) {
        let dwarfKey = "Dwarf_0" + index + "_";
        console.log(dwarfKey);
        console.log(key);
        //
        //
        //
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 17,
                zeroPad: 3,
                prefix: dwarfKey + 'Idle Blink_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 14,
                zeroPad: 3,
                prefix: dwarfKey + 'Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: dwarfKey + 'Walking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        // Dwarf_01_Attacking_009.png
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: dwarfKey + 'Attacking_',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
    }
    makeShamans(scene, key, char) {
        scene.anims.create({
            key: key + 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: '0_Shamans_Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: key + 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: '0_Shamans_Walking_',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: -1
        });
        scene.anims.create({
            key: key + 'die',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: '0_Shamans_Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: key + 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: '0_Shamans_Slashing_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
    }
    makeMonster(scene,key)
    {
        scene.anims.create({
            key: key + 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        let walkpre='Walking_';
        console.log(walkpre);

        scene.anims.create({
            key: key + 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 17,
                zeroPad: 3,
                prefix:  walkpre,
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: -1
        });
        scene.anims.create({
            key: key + 'die',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 14,
                zeroPad: 3,
                prefix: 'Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: key + 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Attacking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
    }
    makeDemon(scene,key,index)
    {
        this.makeCommon(scene,key,index,"Demon");
    }
   
    makeCommon(scene,key,index,char)
    {
        scene.anims.create({
            key: key + 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: char+'_0'+index+'_Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        let walkpre=char+'_0'+index+'_Walking_';
        console.log(walkpre);

        scene.anims.create({
            key: key + 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 17,
                zeroPad: 3,
                prefix:  walkpre,
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: -1
        });
        scene.anims.create({
            key: key + 'die',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 14,
                zeroPad: 3,
                prefix: char+'_0'+index+'_Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: key + 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: char+'_0'+index+'_Attacking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
    }
    makeAssassin(scene,key,index)
    {
        //Assassin_01_Idle Blink_000
        console.log(index);
        scene.anims.create({
            key: key + 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Assassin_0'+index+'_Idle Blink_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        let walkpre='Assassin_0'+index+'_Walking_';
        console.log(walkpre);

        scene.anims.create({
            key: key + 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 17,
                zeroPad: 3,
                prefix:  walkpre,
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: -1
        });
        scene.anims.create({
            key: key + 'die',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 14,
                zeroPad: 3,
                prefix: 'Assassin_0'+index+'_Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: key + 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Assassin_0'+index+'_Attacking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
    }
    makeBoss(scene, key) {
        scene.anims.create({
            key: 'bossattack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Attacking_',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
        scene.anims.create({
            key: 'bossidle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'bosstaunt',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Taunt_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'bossdie',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Dying_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'bosshurt',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Hurt_',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
    } //
}