import { BoardVo } from "../dataObjs/boardVo";
import { DrawVo } from "../dataObjs/drawVo";
import { GridVo } from "../dataObjs/gridVo";

export class TraceDefs
{
    private EAST:number=120;
    private SOUTH:number=90;
    private WEST:number=300;
    private SW:number=15;

    constructor()
    {

    }
    getDef(key:string)
    {
        let board:BoardVo;
        
        switch(key)
        {
            case "5":
                return this.get5();

            case "3":
                return this.get3();

            case "4":
                return this.get4();

            case "2":
                return this.get2();

        }

        return new BoardVo("num5",2,2,0.5);
    }
    private get2()
    {
        let board: BoardVo = new BoardVo("num2", 5, 3, 0.5);
        let draw: DrawVo = new DrawVo();
        let draw2: DrawVo = new DrawVo();
        let draw3: DrawVo = new DrawVo();

        draw.gridSquares.push(new GridVo(0, 2, 9, 5, 180, 3, 5));
        draw.gridSquares.push(new GridVo(3, 0, 7, 5, 90, 8, 1));
        draw.gridSquares.push(new GridVo(9, 0, 6, 5, 0, 12, 1));
        draw.gridSquares.push(new GridVo(14, 0, 8, 5, 60, 16, 2));
        draw.gridSquares.push(new GridVo(14, 5, 8, 5, 90, 17, 8));
        draw.gridSquares.push(new GridVo(12, 9, 10, 5, 140, 15, 11));
        draw.gridSquares.push(new GridVo(10, 10, 10, 6, -1, 8, 16));
        draw.gridSquares.push(new GridVo(8, 11, 10, 6, -1, 10, 14));
        draw.gridSquares.push(new GridVo(2, 12, 10, 6, -1, 4, 17,true));
       
        draw2.gridSquares.push(new GridVo(0, 18, 10, 4, 120, 3, 19,true));
        draw2.gridSquares.push(new GridVo(6, 18, 10, 4, 120, 9, 19,true));
        draw2.gridSquares.push(new GridVo(12, 18, 10, 4, 120, 12, 19,true));
        

        board.draws.push(draw);
        board.draws.push(draw2);
        return board;
    }
    private get4()
    {
        let board: BoardVo = new BoardVo("num4", 5, 3, 0.5);
        let draw: DrawVo = new DrawVo();
        let draw2: DrawVo = new DrawVo();
        let draw3: DrawVo = new DrawVo();

        for (let i: number = 0; i < 24; i += 4) {
            draw.gridSquares.push(new GridVo(14.2, i, 3.5, 4, 90, 15.5, i + 1));
        }

        draw2.gridSquares.push(new GridVo(9.5, 0, 5, 6, 15, 12, 2));
        draw2.gridSquares.push(new GridVo(8, 4, 5, 5, 15, 8, 6));
        draw2.gridSquares.push(new GridVo(6, 6, 5, 5, 15, 6, 8));
        draw2.gridSquares.push(new GridVo(4, 8, 5, 5, 15, 4, 11));
        draw2.gridSquares.push(new GridVo(0, 10, 5, 4, 15, 2, 13));
       
        for (let i:number=0;i<22;i+=5)
        {
        draw3.gridSquares.push(new GridVo(i, 12, 5, 5, 120, i+1, 15,true));
        }
        board.draws.push(draw);
        board.draws.push(draw2);
        board.draws.push(draw3);
        return board;
    }
    private get3()
    {
        
        let board: BoardVo = new BoardVo("num3", 5, 3, 0.5);
        let draw: DrawVo = new DrawVo();
        let draw2: DrawVo = new DrawVo();
        //let draw3: DrawVo = new DrawVo();

        draw.gridSquares.push(new GridVo(0, 0, 4, 5, 90, 1, 2));
        draw.gridSquares.push(new GridVo(4, -1, 4, 5, 120, 5, 1));
        draw.gridSquares.push(new GridVo(8, -1, 4, 5, 240, 9, 0.5));
        draw.gridSquares.push(new GridVo(12, -1, 4, 5, 250, 13, 1));
        draw.gridSquares.push(new GridVo(16, 0, 5, 5, 280, 17, 2));
        draw.gridSquares.push(new GridVo(16, 5, 5, 5, 227, 18, 6));
        draw.gridSquares.push(new GridVo(11, 7, 5, 4, 300, 13, 9));
        draw.gridSquares.push(new GridVo(5, 7, 7, 6, 300, 7, 9));


      //  draw2.gridSquares.push(new GridVo(5, 11, 5, 3, 120, 5, 10));
        draw2.gridSquares.push(new GridVo(7, 8, 5, 5, 120, 12, 10));
        draw2.gridSquares.push(new GridVo(12, 9, 6, 5, 160, 17, 12));
        draw2.gridSquares.push(new GridVo(16, 11, 7, 6, 90, 19, 16));
        draw2.gridSquares.push(new GridVo(14, 17, 8, 5,this.SW, 18, 19,true));

        draw2.gridSquares.push(new GridVo(11, 19, 6, 4, 300, 13, 20));
        draw2.gridSquares.push(new GridVo(6, 18, 6, 6, 300, 7, 20.5));
        draw2.gridSquares.push(new GridVo(0, 18, 6, 5, 300, 2, 20));

        board.draws.push(draw);
        board.draws.push(draw2);
        return board;
    }
    private get5()
    {
        let board: BoardVo = new BoardVo("num5", 14, 3, 0.5);
        let draw: DrawVo = new DrawVo();
        let draw2: DrawVo = new DrawVo();
        let draw3: DrawVo = new DrawVo();

        

        for (let i: number = 6; i < 21; i+=4) {
            draw.gridSquares.push(new GridVo(i, -1, 4, 4, 0, i, 1));
        }

        for (let i: number = 0; i < 12; i += 2) {
            draw2.gridSquares.push(new GridVo(0, i, 7, 2, 90, 3, i));
        }

        draw3.gridSquares.push(new GridVo(6, 6, 5, 7, 0, 6, 8));
        draw3.gridSquares.push(new GridVo(11, 6, 5, 7, 227, 12, 8));
     
        draw3.gridSquares.push(new GridVo(16, 7, 8, 7, 300, 17, 10));


        for (let j: number = 12; j < 17; j+=3) {
            draw3.gridSquares.push(new GridVo(15, j, 8, 3, 90, 18, j));
        }
        draw3.gridSquares.push(new GridVo(12, 18, 9, 5, 180, 12, 20));

        for (let k: number = 11; k > 7; k-=3) {
            draw3.gridSquares.push(new GridVo(k, 17, 3, 7, 180, k + 1, 20));
        }

        draw3.gridSquares.push(new GridVo(4, 16, 4, 7, 240, 4, 18));
        draw3.gridSquares.push(new GridVo(0, 16, 4, 7, 240, 2, 16));
       

        board.draws.push(draw2);
        board.draws.push(draw3);

        board.draws.push(draw);
        return board;
    }
    testData() {

        let board: BoardVo = new BoardVo("num2", 5, 3, 0.25);
        let draw: DrawVo = new DrawVo();
        let draw2: DrawVo = new DrawVo();
        let draw3: DrawVo = new DrawVo();

        draw.gridSquares.push(new GridVo(0, 2, 9, 5, 180, 3, 5));
        draw.gridSquares.push(new GridVo(3, 0, 7, 5, 90, 8, 1));
        draw.gridSquares.push(new GridVo(9, 0, 6, 5, 0, 12, 1));
        draw.gridSquares.push(new GridVo(14, 0, 8, 5, 60, 16, 2));
        draw.gridSquares.push(new GridVo(14, 5, 8, 5, 90, 17, 8));
        draw.gridSquares.push(new GridVo(12, 9, 10, 5, 140, 15, 11));
        draw.gridSquares.push(new GridVo(10, 10, 10, 6, -1, 8, 16));
        draw.gridSquares.push(new GridVo(8, 11, 10, 6, -1, 10, 14));
        draw.gridSquares.push(new GridVo(2, 12, 10, 6, -1, 4, 17, true));
        //

        //board.draws.push(draw3);
        // //console.log(JSON.stringify(board));
        draw2.gridSquares.push(new GridVo(0, 18, 10, 4, 120, 3, 19, true));
        draw2.gridSquares.push(new GridVo(6, 18, 10, 4, 120, 9, 19, true));
        draw2.gridSquares.push(new GridVo(12, 18, 10, 4, 120, 12, 19, true));


        board.draws.push(draw);
        board.draws.push(draw2);
       // this.buildBoard(board);
        return board;
        //    this.showDraw(draw2);
        /*  this.showDraw(draw);
        this.showDraw(draw2);
        this.showDraw(draw3);  */
        //this.showDraw(draw2);
        //this.showDrags(draw2);
    }
}