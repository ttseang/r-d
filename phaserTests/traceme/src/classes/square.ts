import { GameObjects } from "phaser"
import { GridVo } from "../dataObjs/gridVo";
import IBaseScene from "../interfaces/IBaseScene"
import { AlignGrid } from "../util/alignGrid";

export class Square extends GameObjects.Sprite
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    
    public row:number=0;
    public col:number=0;
    
    public prev:Square;
    public grid:AlignGrid;
    public dataProvider:GridVo;

    constructor(bscene:IBaseScene,grid:AlignGrid)
    {
        super(bscene.getScene(),0,0,"square");
        this.bscene=bscene;
        this.grid=grid;
        this.scene.add.existing(this);
        this.setOrigin(0,0);
        
    }
    public setDataProvider(gridVo:GridVo)
    {
        this.dataProvider=gridVo;
        let grid:AlignGrid=this.grid;
        this.displayWidth=this.grid.cw*gridVo.sw;
        this.displayHeight=this.grid.ch*gridVo.sh;
        grid.placeAt(gridVo.col,gridVo.row,this);
        this.x-=this.grid.cw/2;
        this.y-=this.grid.ch/2;
        this.row=gridVo.row;
        this.col=gridVo.col;
        this.setInteractive();
       // this.setTint(0xff0000);
    }
}