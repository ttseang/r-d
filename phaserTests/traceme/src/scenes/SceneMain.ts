import { GameObjects } from "phaser";
import { GM } from "../classes/GM";
import { LayoutManager } from "../classes/layoutManager";
import { Square } from "../classes/square";
import { TraceDefs } from "../classes/traceDefs";
import { BoardVo } from "../dataObjs/boardVo";
import { DrawVo } from "../dataObjs/drawVo";
import { GridVo } from "../dataObjs/gridVo";
import { MaskVo } from "../dataObjs/maskVo";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { AnimationUtil } from "../util/animUtil";

import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private currentCon: Phaser.GameObjects.Container;
    private currentMask: Phaser.GameObjects.Sprite;
    private arrow: Phaser.GameObjects.Sprite;
    private guideImage: Phaser.GameObjects.Sprite;
    private outlineImage: Phaser.GameObjects.Sprite;

    private drawIndex: number = -1;
    private board: BoardVo;
    private letterGrid: AlignGrid;
    private dragArrow: GameObjects.Sprite;
    private defs: TraceDefs = new TraceDefs();

    private boards: BoardVo[] = [];
    private boardIndex: number = -1;
    private boardNumbers: string[] = ["5", "4", "3", "2"];
    private allSquares: Square[] = [];
    private maskContainers: Phaser.GameObjects.Container[] = [];
    private gm: GM = GM.getInstance();
    private layoutManager: LayoutManager;
    private background: GameObjects.Sprite;
    private toneIndex: number = 0;
    private moveLock:boolean=false;
    private dog:GameObjects.Sprite;

    constructor() {
        super("SceneMain");
    }
    preload() {

        this.load.image("num5", "./assets/num5.png");
        this.load.image("num4", "./assets/num4.png");
        this.load.image("num2", "./assets/num2.png");
        this.load.image("num3", "./assets/num3.png");
        this.load.image("square", "./assets/square.jpg");
        this.load.image("dragArrow", "./assets/dragArrow.png");
        this.load.image("background", "./assets/background.jpg");
        this.load.audio("note", "./assets/audio/yip.wav");
        this.load.audio("levelup", "./assets/audio/happybark.wav");

        this.load.atlas("dog","./assets/dog.png","./assets/dog.json");

    }
    create() {
        super.create();
        this.makeGrid(22, 22);



        this.background = this.add.sprite(0, 0, "background").setOrigin(0, 0);
        this.background.displayWidth = this.gw;
        this.background.displayHeight = this.gh;
        // this.grid.showNumbers();

        this.layoutManager = new LayoutManager(this);
        this.layoutManager.callback = this.redraw.bind(this);
        /*  let board2: BoardVo | null = this.defs.getDef("4");
        if (board2) {
           // this.buildBoard(board2);
         //  this.showDraw(board2.draws[1]);
          // this.showDrags(board2.draws[1]);
        } */
        window['scene'] = this;


        this.dog=this.add.sprite(20,20,"dog");

        let animUtil:AnimationUtil=new AnimationUtil();
        animUtil.makeDog(this,"dog");

        this.dog.play("idle");


        this.nextBoard();
        this.input.on('gameobjectdown', this.onGameObjDown.bind(this));
        this.input.on('pointerdown', this.onDown.bind(this));
        // this.testData();

        

        
    }

    nextBoard() {
        this.boardIndex++;
        if (this.boardIndex >= this.boardNumbers.length) {
            this.moveLock=true;
            return;
        }
        let key: string = this.boardNumbers[this.boardIndex];
        this.board = this.defs.getDef(key);
        this.drawIndex = -1;

        if (this.board) {
            this.buildBoard(this.board);
        }
        this.moveLock=false;
    }

    showDrags(drawVo: DrawVo) {
        let gridSquares: GridVo[] = drawVo.gridSquares;
        for (let i: number = 0; i < gridSquares.length; i++) {
            let sqData: GridVo = drawVo.gridSquares[i];
            let arrow: GameObjects.Sprite = this.add.sprite(0, 0, "dragArrow");
            Align.scaleToGameW(arrow, this.layoutManager.getScale("arrow"), this);
            this.placeOnLetterGrid(sqData.arrowX, sqData.arrowY, arrow);
            arrow.setAngle(sqData.arrowAngle);
            if (sqData.debug === true) {
                // arrow.setTint(0xff0000);
                //   arrow.alpha=0.1;
            }
        }

    }
    showDraw(drawVo: DrawVo) {
        let testColors: number[] = this.getTestColors();
        let gridSquares: GridVo[] = drawVo.gridSquares;
        for (let i: number = 0; i < gridSquares.length; i++) {
            let sqData: GridVo = drawVo.gridSquares[i];
            let sq: Square = new Square(this, this.letterGrid);
            this.allSquares.push(sq);
            //sq.setTint(0x4cd137);
            sq.setTint(testColors.pop());
            if (testColors.length === 0) {
                testColors = this.getTestColors();
            }
            sq.alpha = 0.25;
            if (sqData.debug) {
                sq.setTint(0xff0000);
                sq.alpha = 1;
            }
            //sq.alpha=0.01;
            sq.setDataProvider(sqData);

            sq.setInteractive(false);
            sq.x += this.guideImage.x;
            sq.y += this.guideImage.y;
        }
    }
    loadBoard() {

    }
    onBoardLoaded(data: any) {

    }
    buildBoard(board: BoardVo) {
        this.board = board;
        console.log("BUILD BOARD");
        this.dog.setAngle(0);
        
        while (this.maskContainers.length > 0) {
            let con: GameObjects.Container = this.maskContainers.pop();
            con.removeAll(true);
            con.destroy();
            // this.maskContainers.pop().destroy()

        }
        while (this.allSquares.length > 0) {
            let sq: Square = this.allSquares.pop();
            sq.setInteractive(false);
            sq.destroy();
        }
        if (this.guideImage) {
            this.guideImage.destroy();
        }
        let scale: number = this.layoutManager.getScale("number");
        console.log("SCALE=" + scale);



        this.guideImage = this.placeImage2(board.imageKey, board.col, board.row, scale).setOrigin(0, 0);
        this.guideImage.setPosition(0, 0);

        this.letterGrid = new AlignGrid(this, 22, 22, this.guideImage.displayWidth, this.guideImage.displayHeight);

        if (this.dragArrow) {
            this.dragArrow.destroy();
        }
        this.dragArrow = this.add.sprite(0, 0, "dragArrow");
        window['dragArrow'] = this.dragArrow;
        Align.scaleToGameW(this.dragArrow, this.layoutManager.getScale("arrow"), this);
        //this.letterGrid.showPos();
        // this.grid.placeAt2(0, 0, this.guideImage);
        // this.grid.placeAt2(this.board.col,this.board.row,this.guideImage);

        let pos: number = this.layoutManager.getPos("number");
        console.log("POS=" + pos);

        if (pos === -1) {
            Align.center2(this.guideImage, this);
        }
        else {
            this.grid.placeAtIndex(pos, this.guideImage);
        }


        //Align.scaleToGameH(this.guideImage,0.8,this);
        //this.guideImage.visible=false;

        this.drawNext();

        /* this.outlineImage=this.placeImage2("outline5",0,0,board.scale).setOrigin(0,0);
        this.outlineImage.x=this.guideImage.x;
        this.outlineImage.y=this.guideImage.y; */



        /*  this.input.on('gameobjectdown', this.onGameObjDown.bind(this));
         this.input.on('pointerdown', this.onDown.bind(this)); */

         Align.scaleToGameW(this.dog,this.layoutManager.getScale("dog"),this);
         this.grid.placeAtIndex(this.layoutManager.getPos("dog"),this.dog);
         
    }
    redraw() {
        console.log("redraw");
        // this.grid.showNumbers();
        this.background.displayWidth = this.gw;
        this.background.displayHeight = this.gh;
        if (this.board) {
            this.drawIndex = -1;
            this.buildBoard(this.board);
        }
    }
    drawNext() {
        this.drawIndex++;
        if (this.drawIndex < this.board.draws.length) {
            let currentDraw: DrawVo = this.board.draws[this.drawIndex];

            this.buildDraw(currentDraw);

            // this.currentMask.alpha=.5;
            this.setConMask(this.currentMask, this.currentCon);
            if (this.drawIndex > 0) {
                let s: Phaser.Sound.BaseSound = this.sound.add('note');
                s.play();
                this.dog.play("jump");
                setTimeout(() => {
                    this.dog.play("idle");
                }, 1000);
            }


        }
        else {
            let levelUP: Phaser.Sound.BaseSound = this.sound.add("levelup");
            levelUP.play();

            this.dragArrow.visible = false;
            this.moveLock=true;
            console.log("LEVEL UP");

            this.dog.play("jump");
            this.tweens.add({targets: this.dog,duration: 1000,angle:350});
            setTimeout(() => {
                this.dog.play("idle");
                this.dog.setAngle(0)
            }, 1000);
            setTimeout(() => {
                this.nextBoard();
            }, 1500);
          
            // this.guideImage.setTint(0x4cd137);
        }
    }
    getTestColors() {
        let testColors: number[] = [0xff000, 0x00a8ff, 0xccff00, 0x9c88ff, 0xe84118, 0xe1b12c];
        return testColors.slice();
    }
    placeOnLetterGrid(col: number, row: number, obj: GameObjects.Sprite) {
        this.letterGrid.placeAt(col, row, obj);
        obj.x += this.guideImage.x;
        obj.y += this.guideImage.y;
        // obj.x+=obj.displayWidth/2;
        // obj.y+=obj.displayHeight/2;
    }
    buildDraw(drawObj: DrawVo) {

        if (this.arrow) {
            this.arrow.destroy();
        }

        let first: GridVo = drawObj.gridSquares[0];
        this.placeOnLetterGrid(first.arrowX, first.arrowY, this.dragArrow);

        if (first.arrowAngle != -1) {
            // //console.log(first.arrowAngle);
            this.dragArrow.setAngle(first.arrowAngle);
        }

        // / //console.log(first);
        let testColors: number[] = this.getTestColors();
        let last: Square | null = null;

        ////console.log(drawObj);

        let con: GameObjects.Container = this.add.container(0, 0);
        this.maskContainers.push(con);
        this.currentCon = con;

        if (this.currentMask) {
            this.currentMask.destroy();
        }
        this.currentMask = this.placeImage2(this.board.imageKey, 0, 0, this.board.scale).setOrigin(0, 0);
        this.currentMask.x = this.guideImage.x;
        this.currentMask.y = this.guideImage.y;
        this.currentMask.displayWidth = this.guideImage.displayWidth;
        this.currentMask.displayHeight = this.guideImage.displayHeight;

        //   return;
        //  this.currentMask.alpha=0.5;
        let gridSquares: GridVo[] = drawObj.gridSquares;
        for (let i: number = 0; i < gridSquares.length; i++) {
            let sqData: GridVo = drawObj.gridSquares[i];
            let sq: Square = new Square(this, this.letterGrid);
            sq.setTint(0x4cd137);
            /*  sq.setTint(testColors.pop());
            if (testColors.length===0)
            {
                testColors=this.getTestColors();
            }  */
            // sq.alpha=0.5;
            sq.alpha = 0.01;
            sq.setDataProvider(sqData);
            sq.x += this.guideImage.x;
            sq.y += this.guideImage.y;

            this.currentCon.add(sq);
            if (last) {
                sq.prev = last;
            }
            last = sq;
        }
    }
    onDown(pointer: Phaser.Input.Pointer) {
        if (this.moveLock===true)
        {
            return;
        }
        this.input.on('gameobjectover', this.overSquare.bind(this));
        this.input.once('pointerup', this.onUp.bind(this));

        let pos: PosVo = this.letterGrid.findNearestGridXY(pointer.x, pointer.y);

        //console.log(pos);

    }
    onGameObjDown(pointer: Phaser.Input.Pointer, sq: Square) {
        if (this.moveLock===true)
        {
            return;
        }
        if (sq.prev) {
            return;
        }
        sq.alpha = 1;

        this.children.bringToTop(this.dragArrow);

        this.input.on('gameobjectover', this.overSquare.bind(this));
        this.input.once('pointerup', this.onUp.bind(this));
    }
    onUp(pointer: Phaser.Input.Pointer) {
        if (this.moveLock===true)
        {
            return;
        }
        this.input.off('gameobjectover');
        ////console.log(this.checkCon(this.currentCon));
        if (this.checkCon(this.currentCon) === false) {
            // this.resetCon(this.currentCon);
        }
        else {

            this.drawNext();
        }
    }
    playTone() {

    }
    overSquare(pointer: Phaser.Input.Pointer, sq: Square) {
        ////console.log(sq);
        if (this.moveLock===true)
        {
            return;
        }
        if (!sq) {
            return;
        }
        if (sq.scene === undefined) {
            sq.destroy();
            //console.log("bad square");
            //console.log(sq);
            window['sq'] = sq;
            return;
        }
        if (sq.prev) {
            if (sq.prev.alpha != 1) {
                return;
            }
        }

        ////console.log(sq.col,sq.row);

        if (sq.alpha < 1) {
            this.placeOnLetterGrid(sq.dataProvider.arrowX, sq.dataProvider.arrowY, this.dragArrow);
            if (sq.dataProvider.arrowAngle != -1) {
                this.dragArrow.setAngle(sq.dataProvider.arrowAngle);
            }
        }
        sq.alpha = 1;
        this.playTone();
        //console.log(sq);
        sq.setInteractive(false);
        this.children.bringToTop(this.dragArrow);
        if (this.checkCon(this.currentCon) === true) {
            //console.log("NEXT!");

            this.drawNext();
        }
    }

    setConMask(image: GameObjects.Sprite, con: Phaser.GameObjects.Container) {
        con.mask = new Phaser.Display.Masks.BitmapMask(this, image);
        image.visible = false;

    }
    resetCon(con: Phaser.GameObjects.Container) {
        //con.setAll('alpha','0.01');
        ////console.log('reset');

        let all: GameObjects.GameObject[] = con.getAll();
        for (let i: number = 0; i < all.length; i++) {
            let s: GameObjects.Sprite = all[i] as Square;
            s.alpha = 0.01;
        }
    }
    checkCon(con: Phaser.GameObjects.Container) {
        let aCount: number = con.getAll('alpha', 1).length;
        let tCount: number = con.getAll().length;

        if (aCount === tCount) {
            return true;
        }
        return false;
    }
}