import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";

export class Hat extends GameObjects.Image
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    public index:number;

    public ox:number=0;
    
    constructor(bscene:IBaseScene,index:number)
    {
        super(bscene.getScene(),0,0,"hat");

        this.index=index;
        this.setInteractive();
        this.scene.add.existing(this);
    }
}