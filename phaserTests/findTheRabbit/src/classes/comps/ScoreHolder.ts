import { GameObjects } from "phaser";
import IBaseScene from "../../interfaces/IBaseScene";

export class ScoreHolder extends GameObjects.Container {
    private bscene: IBaseScene;
    private back: GameObjects.Graphics;
    private scoreText:GameObjects.Text;

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());

        this.bscene = bscene;

        let w: number = this.bscene.getW() * 0.25;
        let h: number = this.bscene.getH() * 0.2;

        this.back = this.scene.add.graphics();
        this.back.fillStyle(0xff0000);
        this.back.lineStyle(4, 0xffffff);
        this.back.strokeRoundedRect(-w / 2, -h / 2, w, h , 8);
        this.back.fillRoundedRect(-w / 2, -h / 2, w, h, 8);
        this.back.fill();
        this.add(this.back);

        this.scoreText=this.scene.add.text(0,0,"Score: 0",{fontFamily:"Arial",fontSize:"30px",color:"white"}).setOrigin(0.5,0.5);
        this.add(this.scoreText);

        this.scene.add.existing(this);
    }
    setScore(s:number)
    {
        this.scoreText.setText("Score: "+s.toString());
    }
}