import { Game, GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import { ScoreHolder } from "../classes/comps/ScoreHolder";
import { Hat } from "../classes/Hat";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private hats:Hat[]=[];
    private positions:PosVo[]=[];
    private rabbit:GameObjects.Image;
    private correct:number=-1;
    private last:PosVo[]=[];
    private mixCount:number=0;
    private mainText:GameObjects.Text;

    private btnStart:Button;
    private cdSecs:number=4;
    private selectedIndex:number=0;
    private scoreHolder:ScoreHolder;
    private score:number=0;

    constructor() {
        super("SceneMain");
    }
    preload() {
        
        let keys:string[]=["rabbit","hat","star","table"];
        for (let i:number=0;i<keys.length;i++)
        {
            this.load.image(keys[i],"./assets/"+keys[i]+".png");
        }
        this.load.image("bg","./assets/bg.jpg");
        this.load.image("holder","./assets/holder.jpg");
        this.load.image("button","./assets/ui/buttons/1/4.png");

        this.load.audio("whoosh","./assets/audio/whoosh.mp3");
        this.load.audio("tada","./assets/audio/tada.wav");
        this.load.audio("aww","./assets/audio/aww.mp3");

    }
    create() {
        super.create();
        this.makeGrid(11,11);
        
        this.hats=[];
        this.score=0;

        this.positions=[new PosVo(3,7),new PosVo(5,7),new PosVo(7,7)];
       
        let bg:GameObjects.Image=this.add.image(0,0,"bg");

        bg.displayWidth=this.gw;
        bg.scaleY=bg.scaleX;

        if (bg.displayHeight<this.gh)
        {
            bg.displayHeight=this.gh;
            bg.scaleX=bg.scaleY;
        }

         Align.center(bg,this);

         let table:GameObjects.Image=this.add.image(0,0,"table");
         Align.scaleToGameW(table,0.8,this);
         this.grid.placeAt(5,8,table);

         for (let i:number=0;i<3;i++)
         {
             let hat:Hat=new Hat(this,i);
             Align.scaleToGameW(hat,0.15,this);
           
             this.hats.push(hat);
         }


         this.scoreHolder=new ScoreHolder(this);
         this.grid.placeAt(5,1,this.scoreHolder);

         this.mainText=this.add.text(0,0,"GET READY",{fontFamily:"Arial",fontSize:"32px",color:"white",align:"center"}).setOrigin(0.5,0.5);
         this.grid.placeAt(5,9,this.mainText);
         this.mainText.visible=false;

         //this.grid.showPos();

         this.startHats();


         this.btnStart=new Button(this,"button","SHUFFLE");
         this.grid.placeAt(5,9,this.btnStart);
         this.btnStart.setCallback(this.startCountDown.bind(this));

         window['scene']=this;
    }
    playSound(key:string)
    {
        let s:Phaser.Sound.BaseSound=this.sound.add(key);
        s.play();
    }
    startCountDown()
    {
        this.btnStart.visible=false;

        this.cdSecs=3;
        this.grid.placeAt(5,9,this.mainText);

        setTimeout(() => {
            this.countDown();
        }, 1000);
    }
    countDown()
    {
        this.cdSecs--;
        this.mainText.setText("Get Ready\n"+this.cdSecs.toString());
        this.mainText.visible=true;

        if (this.cdSecs>0)
        {
            setTimeout(() => {
                this.countDown();
            }, 1000);
        }
        else
        {
            this.mainText.visible=false;
            this.dropHats();
        }
    }
    startHats()
    {
        this.placeRabbit();
        this.placeHats();

        setTimeout(() => {
           // this.dropHats();
        }, 2000);
    }
    placeRabbit()
    {
        
        if (this.correct==-1)
        {
            this.correct=1;
        }
        else
        {
            this.correct=Math.floor(Math.random()*3);
        }
        this.rabbit=this.add.image(0,0,"rabbit");
        Align.scaleToGameW(this.rabbit,0.1,this);  

        //this.hats[this.correct].alpha=.5;

        this.grid.placeAt(this.positions[this.correct].x,this.positions[this.correct].y,this.rabbit);
    }
    cheat()
    {
        this.hats[this.correct].alpha=.5;
        this.rabbit.visible=true;
    }
    placeHats()
    {
        for (let i:number=0;i<3;i++)
         {
             let hat:Hat=this.hats[i];
             this.children.bringToTop(hat);
             this.grid.placeAt(this.positions[i].x,this.positions[i].y-3,hat);
             hat.ox=hat.x;
         }
    }
    dropHats()
    {
        for (let i:number=0;i<3;i++)
         {
             let hat:Hat=this.hats[i];

             let pos2:PosVo=this.grid.getRealXY(this.positions[i].x,this.positions[i].y);
             console.log(pos2);

             this.tweens.add({targets:hat,duration:500,x:pos2.x+this.cw/2,y:pos2.y+this.ch/2});
             
         }
         setTimeout(() => {
             this.startMix();
         }, 1000);
    }
    raiseHats()
    {
        
        for (let i:number=0;i<3;i++)
        {
            let hat:Hat=this.hats[i];           

            this.tweens.add({targets:hat,duration:500,y:this.gh*0.3});
            
        }

        setTimeout(() => {
            for (let i:number=0;i<this.hats.length;i++)
            {
                this.hats[i].x=this.hats[i].ox;
            }
        }, 600);
    }
    startMix()
    {
        this.rabbit.visible=false;
        this.mixCount=0;
        this.mixHats();
    }
    mixHats()
    {
        let positions:PosVo[]=this.getRandPos();

        while(positions==this.last)
        {
            positions=this.getRandPos();
            
        }

        this.last=positions;

        for (let i:number=0;i<3;i++)
         {
             let hat:Hat=this.hats[i];

             let pos2:PosVo=this.grid.getRealXY(positions[i].x,positions[i].y);
             

             this.tweens.add({targets:hat,duration:500,x:pos2.x+this.cw/2,y:pos2.y+this.ch/2});
             
         }

         if (this.mixCount/2==Math.floor(this.mixCount/2))
         {
            this.playSound("whoosh");
         }
         

         this.mixCount++;
         if (this.mixCount<10)
         {
             setTimeout(() => {
                 this.mixHats();
             }, 600);
         }
         else
         {
             setTimeout(() => {
                this.rabbit.x=this.hats[this.correct].x;
                this.rabbit.y=this.hats[this.correct].y;
               // this.rabbit.visible=true;
                this.prepGuess();
               // this.raiseHats();    
             }, 700);
             
         }
    }
    prepGuess()
    {
        this.grid.placeAt(5,4,this.mainText);
        this.mainText.setText("Where is the rabbit?").visible=true;
        this.input.once("gameobjectdown",this.clickHat.bind(this))
    }
    clickHat(p:Phaser.Input.Pointer,hat:Hat)
    {
        this.mainText.visible=false;
        this.selectedIndex=hat.index;
        this.tweens.add({targets:hat,loop:4,duration:50,x:hat.x-20,yoyo:true,onComplete:this.shakeDone.bind(this)})
    }
    shakeDone()
    {
        this.raiseHats();
        this.rabbit.visible=true;
        setTimeout(() => {
            this.checkCorrect();
        }, 1000);
    }
    checkCorrect()
    {
        if (this.correct==this.selectedIndex)
        {
            console.log("correct");
            this.score++;
            this.scoreHolder.setScore(this.score);

            setTimeout(() => {
                this.startCountDown();
            }, 2000);
            this.playSound("tada");
        }
        else
        {
            setTimeout(() => {
                this.scene.start("SceneOver");
            }, 2000);
            console.log("wrong");
            this.playSound("aww");
        }
    }
    getRandPos()
    {
        let positions=this.positions.slice();

        for (let i:number=0;i<20;i++)
        {
            let r1:number=Math.floor(Math.random()*3);
            let r2:number=Math.floor(Math.random()*3);

            let pos1:PosVo=positions[r1];
            let pos2:PosVo=positions[r2];

            positions[r2]=pos1;
            positions[r1]=pos2;

        }
        return positions;
    }
}