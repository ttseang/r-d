import 'phaser/plugins/spine/dist/SpinePlugin';
import Align from '../util/Align';

export class SpineAnim extends SpinePlugin.SpineGameObject {
    constructor(scene, key, scale) {

        super(scene, scene.spine, 0, 0, key);
        this.scene = scene;
        scene.sys.displayList.add(this);
        scene.sys.updateList.add(this);

        this.scale = scale;

        Align.scaleToGameW(this, scale, scene);

    }
    setOffSet(xx, yy) {
        let root = this.getRootBone();
        root.x = xx;
        root.y = yy;
    }
    fixCenter() {
        this.x -= this.displayWidth / 2;
        this.y -= this.displayHeight / 2;
    }
    centerMe() {
        Align.center(this, this.scene);
    }
    showDebugPos() {
        if (!this.graphics) {
            this.graphics = this.scene.add.graphics();

            this.graphics.fillStyle(0xffffff, 0.4);
        }
        else {
            this.graphics.clear();
            this.graphics.fillStyle(0xffffff, 0.4);
        }

        this.graphics.fillRect(this.x, this.y, this.displayWidth, this.displayHeight);
    }
}
