/*jshint esversion: 6 */
import { SpineAnim } from "../classes/SpineAnim";
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");

       // this.load.spine("me", 'me_01.json', ['me_01.atlas'], false);
        this.load.spine("miss", 'miss.json', ['miss.atlas'], false);
    }
    create() {
        this.makeGrid();

       /*  this.spineAnim = new SpineAnim(this, "me", 0.25);
        this.aGrid.placeAtIndex(60, this.spineAnim);
        this.spineAnim.showDebugPos(); */


        this.miss=new SpineAnim(this, "miss", 0.5);
        this.miss.setOffSet(820,-1400);
      //  this.aGrid.placeAtIndex(27, this.miss);
        this.placeChildren();
        this.miss.play("animation");
        window.scene = this;

        window.onresize=this.onResize.bind(this);
    }
    placeChildren()
    {
        Align.center(this.miss,this);
        
        this.miss.fixCenter();
        this.miss.showDebugPos();
    }
    onResize()
    {
        console.log("resize");
        let width = window.innerWidth/window.devicePixelRatio;
        let height = window.innerHeight/window.devicePixelRatio;

        

        scene.game.scale.resize(width,height); 
        this.cameras.resize(width, height);
        
        this.sys.game.config.width=width;
        this.sys.game.config.height=height;

        this.makeGrid();
        Align.scaleToWindowW(this.miss,0.5,this);
        this.placeChildren();

/* 
        if (this.graphics)
        {
            this.graphics.clear();
        } */
    }
    makeGrid() {
        if (this.aGrid) {
            console.log("try to clear");
            this.aGrid.destroy();
        }
        this.aGrid = new AlignGrid({ scene: this, rows: 11, cols: 11, width: this.sys.canvas.width, height: this.sys.canvas.height });
      //  this.aGrid.showNumbers();

    }
    

}
export default SceneMain;