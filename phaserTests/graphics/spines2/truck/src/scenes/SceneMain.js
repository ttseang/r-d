/*jshint esversion: 6 */
import { SpineAnim } from "../classes/SpineAnim";
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");

        // this.load.spine("me", 'me_01.json', ['me_01.atlas'], false);
        this.load.spine("truck", 'truck.json', ['truck.atlas'], false);
    }
    create() {
        this.makeGrid();

        /*  this.spineAnim = new SpineAnim(this, "me", 0.25);
         this.aGrid.placeAtIndex(60, this.spineAnim);
         this.spineAnim.showDebugPos(); */


        this.truck = new SpineAnim(this, "truck");       
        this.truck.setReal(2714,1243);
        this.truck.setOffSet(1515,-1074);
        this.truck.play("animation",true);
        this.placeChildren();

        window.scene = this;
        
       window.onresize = this.onResize.bind(this);
    }
    placeChildren() {
        this.truck.scaleToGameW(0.5);
        this.truck.centerMe();
       
        //this.truck.showDebugPos();
    }
    onResize() {
       
        let width = window.innerWidth / window.devicePixelRatio;
        let height = window.innerHeight / window.devicePixelRatio;



        scene.game.scale.resize(width, height);
        this.cameras.resize(width, height);

        this.sys.game.config.width = width;
        this.sys.game.config.height = height;

        this.makeGrid();
       
        this.placeChildren();
        //837,534
        /* 
                if (this.graphics)
                {
                    this.graphics.clear();
                } */
    }
    makeGrid() {
        if (this.aGrid) {
         
            this.aGrid.destroy();
        }
        this.aGrid = new AlignGrid({ scene: this, rows: 11, cols: 11, width: this.sys.canvas.width, height: this.sys.canvas.height });
        //  this.aGrid.showNumbers();

    }


}
export default SceneMain;