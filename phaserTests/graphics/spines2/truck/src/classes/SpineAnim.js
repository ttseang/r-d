import 'phaser/plugins/spine/dist/SpinePlugin';
import Align from '../util/Align';

export class SpineAnim extends SpinePlugin.SpineGameObject {
    constructor(scene, key) {

        super(scene, scene.spine, 0, 0, key);
        this.scene = scene;
        scene.sys.displayList.add(this);
        scene.sys.updateList.add(this);

        this._realWidth = -1;
        this._realHeight = -1;
    }
    scaleToGameW(per) {
        let offsetScale = this.displayWidth / this.realWidth;
        this.displayWidth = this.scene.sys.game.config.width * per * offsetScale;
        this.scaleY = this.scaleX;
        //1440/679
    }
    setReal(w, h) {
        this._realWidth = w;
        this._realHeight = h;
    }
    get realHeight() {
        if (this._realHeight != -1) {
            return this._realHeight * this.scaleY;
        }
        return super.displayHeight;
    }
    get realWidth() {
        if (this._realWidth != -1) {
            return this._realWidth * this.scaleX;
        }
        return super.displayWidth;
    }

    setOffSet(xx, yy) {
        let root = this.getRootBone();
        root.x = xx;
        root.y = yy;
    }
    fixCenter() {
        this.x -= this.displayWidth / 2;
        this.y -= this.displayHeight / 2;
    }
    resetPos() {
        this.x = 0;
        this.y = 0;
    }
    centerMe() {
        Align.centerSpine(this, this.scene);
    }
    showDebugPos() {
        if (!this.graphics) {
            this.graphics = this.scene.add.graphics();

            this.graphics.fillStyle(0xffffff, 0.4);
        }
        else {
            this.graphics.clear();
            this.graphics.fillStyle(0xffffff, 0.4);
        }

        this.graphics.fillRect(this.x, this.y, this.displayWidth, this.displayHeight);
    }
}
