/*jshint esversion: 6 */
export class Align
{
	
	static scaleToGameW(obj,per,scene)
	{
		obj.displayWidth=scene.sys.game.config.width*per;
		obj.scaleY=obj.scaleX;
	}
	static scaleToGameH(obj,per,scene)
	{
		obj.displayHeight=scene.sys.game.config.width*per;
		obj.scaleX=obj.scaleY;
	}
	static scaleToWindowW(obj,per)
	{
		let dw=window.innerWidth*per;
		console.log(dw);
		obj.displayWidth=dw;
		obj.scaleY=obj.scaleX;
	}
	static centerH(obj,scene)
	{
		obj.x=scene.sys.game.config.width/2-obj.displayWidth/2;
	}
	static centerV(obj,scene)
	{
		obj.y=scene.sys.game.config.height/2-obj.displayHeight/2;
	}
	static center2(obj,scene)
	{
		obj.x=scene.sys.game.config.width/2-obj.displayWidth/2;
		obj.y=scene.sys.game.config.height/2-obj.displayHeight/2;
	}
	static centerSpine(spine,scene)
	{
		spine.x=scene.sys.game.config.width/2-spine.realWidth/2;
		spine.y=scene.sys.game.config.height/2-spine.realHeight/2;
	}
	static center(obj,scene)
	{
		obj.x=scene.sys.game.config.width/2;
		obj.y=scene.sys.game.config.height/2;
	}
}
export default Align;