import Phaser from 'phaser';
import {SceneMain} from "./scenes/SceneMain";
import 'phaser/plugins/spine/dist/SpinePlugin';
 let isMobile = navigator.userAgent.indexOf("Mobile");
 if (isMobile == -1) {
     isMobile = navigator.userAgent.indexOf("Tablet");
 } 

 let width = window.innerWidth*0.98 / window.devicePixelRatio;
 let height = window.innerHeight*0.97 / window.devicePixelRatio;
 //
 //
/* if (isMobile != -1) {
     w = window.innerWidth;
     h = window.innerHeight;
 } */

const config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: width,
    height: height,
    scene: SceneMain,
    physics: {
        default: 'arcade',
        arcade: {
            debug: true
        }
    },
    plugins: {
        scene: [
            { key: 'SpinePlugin', plugin: window.SpinePlugin, mapping: 'spine' }
        ]
    }
};

const game = new Phaser.Game(config);
