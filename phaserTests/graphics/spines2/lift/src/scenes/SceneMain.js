/*jshint esversion: 6 */
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");
      
        //this.load.spine('set1', 'demos.json', [ 'atlas1.atlas', 'atlas2.atlas', 'heroes.atlas' ], true);
        this.load.spine("lift", 'lift.json', ['lift.atlas'], false);
    }
    create() {
        this.aGrid = new AlignGrid({ scene: this, rows: 11, cols: 11 });
        // this.aGrid.showNumbers();

        //add a spine to the canvas
        this.lift= this.add.spine(0, 0, 'lift');
        window.lift=this.lift;
        //resize the lift
        Align.scaleToGameW(this.lift, 0.25, this);

        //postion on the canvas
        this.aGrid.placeAtIndex(60, this.lift);

        //play the animation
        this.lift.play("animation", true);

        
        
    }
   
}
export default SceneMain;