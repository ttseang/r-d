//import Phaser from 'phaser';
import {SceneMain} from "./scenes/SceneMain";
import 'phaser/plugins/spine/dist/SpinePlugin';

const config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: 800,
    height: 600,
    scene: SceneMain,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    plugins: {
        scene: [
            { key: 'SpinePlugin', plugin: window.SpinePlugin, mapping: 'spine' }
        ]
    }
};

const game = new Phaser.Game(config);
