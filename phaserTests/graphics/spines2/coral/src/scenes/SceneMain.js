/*jshint esversion: 6 */
import { SpineAnim } from "../classes/SpineAnim";
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");

        // this.load.spine("me", 'me_01.json', ['me_01.atlas'], false);
        this.load.spine("coral", 'coral reef.json', ['coral reef.atlas'], false);
    }
    create() {
        this.makeGrid();

        /*  this.spineAnim = new SpineAnim(this, "me", 0.25);
         this.aGrid.placeAtIndex(60, this.spineAnim);
         this.spineAnim.showDebugPos(); */


        this.coral = new SpineAnim(this, "coral");       
        this.coral.setReal(1666.79,1048.97);
        this.coral.setOffSet(785,-1040);
        this.coral.play("animation",true);
        this.placeChildren();

        window.scene = this;
        
       window.onresize = this.onResize.bind(this);
    }
    placeChildren() {
        this.coral.scaleToGameW(1);
        this.coral.centerMe();
       
        //this.coral.showDebugPos();
    }
    onResize() {
       
        let width = window.innerWidth;// / window.devicePixelRatio;
        let height = window.innerHeight;// / window.devicePixelRatio;

        if (window['visualViewport'])
        {
            width=window.visualViewport.width; // /window.devicePixelRatio;
            height=window.visualViewport.height; // /window.devicePixelRatio;
        }


        scene.game.scale.resize(width, height);
        this.cameras.resize(width, height);

        this.sys.game.config.width = width;
        this.sys.game.config.height = height;

        this.makeGrid();
       
        this.placeChildren();
        //837,534
        /* 
                if (this.graphics)
                {
                    this.graphics.clear();
                } */
    }
    makeGrid() {
        if (this.aGrid) {
         
            this.aGrid.destroy();
        }
        this.aGrid = new AlignGrid({ scene: this, rows: 11, cols: 11, width: this.sys.canvas.width, height: this.sys.canvas.height });
        //  this.aGrid.showNumbers();

    }


}
export default SceneMain;