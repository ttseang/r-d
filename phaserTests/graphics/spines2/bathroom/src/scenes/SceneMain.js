/*jshint esversion: 6 */
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");
      
        //this.load.spine('set1', 'demos.json', [ 'atlas1.atlas', 'atlas2.atlas', 'heroes.atlas' ], true);
        this.load.spine("bathroom", 'bathroom.json', ['bathroom.atlas'], false);
    }
    create() {
        this.makeGrid();
        this.bathroom= this.add.spine(0, 0, 'bathroom');
        window.bathroom=this.bathroom;

        this.placeChildren();
         window.scene=this;
        //add a spine to the canvas
        window.onresize=this.onResize.bind(this);
    }
    makeGrid()
    {
        if (this.aGrid)
        {
            console.log("try to clear");
            this.aGrid.destroy();
        }
        this.aGrid = new AlignGrid({ scene: this, rows: 22, cols: 22,width:this.sys.canvas.width,height:this.sys.canvas.height });
       // this.aGrid.showNumbers();
        
    }
    placeChildren()
    {   
        
        //resize the bathroom
        Align.scaleToWindowW(this.bathroom, 1.1, this);
       // Align.center(this.bathroom,this);
        //postion on the canvas
        this.aGrid.placeAt(10,22, this.bathroom);

        //play the animation
        this.bathroom.play("animation", true);        
              
        
    }
    onResize() {
        
        console.log("resize");
        let width = window.innerWidth/window.devicePixelRatio;
        let height = window.innerHeight/window.devicePixelRatio;

        scene.game.scale.resize(width,height); 
        this.cameras.resize(width, height);

        this.makeGrid();
        this.placeChildren();
       
    }
   
}
export default SceneMain;