/*jshint esversion: 6 */
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");
      
        //this.load.spine('set1', 'demos.json', [ 'atlas1.atlas', 'atlas2.atlas', 'heroes.atlas' ], true);
        this.load.spine("kiss", 'kiss.json', ['kiss.atlas'], false);
        this.load.image("circle","circle.png");
    }
    create() {
        this.makeGrid();
        this.circle=this.add.image(0,0,"circle");
        this.spineAnim= this.add.spine(0, 0, 'kiss');
        this.physics.add.existing(this.spineAnim);
        this.adjust();

        window.spineAnim=this.spineAnim;
        
        this.placeChildren();
         window.scene=this;
        //add a spine to the canvas
        window.onresize=this.onResize.bind(this);
    }
    adjust()
    {
        let bone=this.spineAnim.findBone("root");
        bone.x=this.spineAnim.width/2;
        bone.y=-this.spineAnim.height;
    }
    makeGrid()
    {
        if (this.aGrid)
        {
            console.log("try to clear");
            this.aGrid.destroy();
        }
        this.aGrid = new AlignGrid({ scene: this, rows: 22, cols: 22,width:this.sys.canvas.width,height:this.sys.canvas.height });
        //this.aGrid.showNumbers();
        
    }
    placeChildren()
    {   
        
       
        Align.scaleToWindowW(this.circle, 0.25, this);
        //resize the spineAnim
        Align.scaleToWindowW(this.spineAnim, 0.25, this);
       // this.spineAnim.setScale(0.125,0.125);
        //Align.center2(this.spineAnim,this);
        Align.center(this.circle,this);
        //postion on the canvas
        this.aGrid.placeAt(10,8,this.circle);
        this.aGrid.placeAt(10,8, this.spineAnim);

        this.drawSquare(this.spineAnim.x,this.spineAnim.y,this.spineAnim.displayWidth,this.spineAnim.displayHeight);

      //  let bounds=this.spineAnim.getBounds();
      //  this.drawSquare(bounds.offset.x,bounds.offset.y,bounds.size.x,bounds.size.y);
        //play the animation
        this.spineAnim.play("animation", true);        
              
        
    }
    onResize() {
        
        console.log("resize");
        let width = window.innerWidth/window.devicePixelRatio;
        let height = window.innerHeight/window.devicePixelRatio;

        scene.game.scale.resize(width,height); 
        this.cameras.resize(width, height);

        this.makeGrid();
        this.placeChildren();
        if (this.graphics)
        {
            this.graphics.clear();
        }
        
    }
    drawSquare(x, y, w, h) {
        if (!this.graphics)
        {
            this.graphics = this.add.graphics();

            this.graphics.fillStyle(0xffffff, 0.4);
        }
        this.graphics.fillRect(x, y, w, h);
    }
}
export default SceneMain;