/*jshint esversion: 6 */
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");
        this.load.image("box", "images/boxAlt.png");
        this.load.image("grass", "images/grass.png");
        //this.load.spine('set1', 'demos.json', [ 'atlas1.atlas', 'atlas2.atlas', 'heroes.atlas' ], true);
        this.load.spine("set1", 'spines/dragon/dragon-ess.json', ['spines/dragon/dragon.atlas'], false);
    }
    create() {
        this.aGrid = new AlignGrid({ scene: this, rows: 11, cols: 11 });
        // this.aGrid.showNumbers();

        //add a spine to the canvas
        this.dragon = this.add.spine(0, 0, 'set1');

        //resize the dragon
        Align.scaleToGameW(this.dragon, 0.15, this);

        //postion on the canvas
        this.aGrid.placeAtIndex(34, this.dragon);

        //play the animation
        this.dragon.play("flying", true);

        //add the dragon to physics
        this.physics.add.existing(this.dragon);

        //add a bounce to the dragon
        this.dragon.body.setBounce(1, 1);

        //make the dragon bounce against the walls
        this.dragon.body.collideWorldBounds = true;

       
        //make collision groups
        this.boxGroup = this.physics.add.group();
        this.grassGroup = this.physics.add.group();

        //make the ground and blocks
        this.makeBlocks();
        this.makeGrass();

        //add collisions between the objects
        this.physics.add.collider(this.dragon, this.boxGroup);
        this.physics.add.collider(this.boxGroup);
        this.physics.add.collider(this.grassGroup, this.boxGroup);
        this.physics.add.collider(this.grassGroup, this.dragon);

        //add input listener
        this.input.on('pointerdown', this.onMouseDown.bind(this));
    }
    //make the ground
    makeGrass() {
        for (let i = 110; i < 121; i++) {
            let grass = this.physics.add.sprite(0, 0, "grass");
            this.grassGroup.add(grass);
            Align.scaleToGameW(grass,0.1,this);
            this.aGrid.placeAtIndex(i, grass);
            grass.setImmovable(true);
        }
    }
    //make the blocks
    makeBlocks() {        
        for (let i = 20; i < 99; i += 11) {
            let block = this.physics.add.sprite(0, 0, "box");
            this.boxGroup.add(block);
            Align.scaleToGameW(block, 0.05, this);
            this.aGrid.placeAtIndex(i, block);
            block.setGravityY(20);
        }
    }
    onMouseDown(pointer) {

        //record the pointer position
        //we don't need it for flying by we do for stopping
        this.targetX = pointer.x;
        this.targetY = pointer.y;
        
        //fly the dragon to the click point
        this.physics.moveTo(this.dragon, pointer.x, pointer.y, 400);
    }
    //spines do not have a flipX property,
    //so we need to use some tricks to flip the image
    //and keep the collision box in the right place

    flipDragon(flip) {
        if (flip == true) {
            this.dragon.scaleX = -Math.abs(this.dragon.scaleX);
            this.dragon.body.setOffset(this.dragon.width, 0);
        }
        else {
            this.dragon.scaleX = Math.abs(this.dragon.scaleX);
            this.dragon.body.setOffset(0, 0);
        }
    }
    //if the dragon is close to the target click point
    //then stop the dragon

    checkTargetDist() {
        //check which way the dragon is flying
        //and flip if needed
        
        if (this.dragon.body.velocity.x < 1) {
            this.flipDragon(true);
        }
        else {
            this.flipDragon(false);
        }
        
        //get the distance between the target and dragon
        let distX = Math.abs(this.dragon.x - this.targetX);
        let distY = Math.abs(this.dragon.y - this.targetY);

        //if close to the target, then stop the dragon
        if (distX < 10 && distY < 10) {
            this.dragon.body.setVelocity(0, 0);
        }
    }
    update() {
        this.checkTargetDist();
    }
}
export default SceneMain;