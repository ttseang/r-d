import { EffectParamVo } from "../dataObjs/EffectParamVo";
import { PageVo } from "../dataObjs/PageVo";
import { IBaseScene } from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";

export class ImageEffects
{
    private scene:Phaser.Scene;
    private callback:Function;

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    doEffect(bscene:IBaseScene,image:IGameObj,page:PageVo) {

        this.scene=bscene.getScene();
        let index:number=page.effect;

        switch (index) {
            case 1:
                let ox: number = image.x;
                image.x = 1000;
                image.visible = true;
                this.scene.tweens.add({ targets: image, duration: 1000, x: ox, onComplete: this.effectDone.bind(this) });
                break;

            case 2:
                let ox2: number = image.x;
                image.x = -1000;
                image.visible = true;
                this.scene.tweens.add({ targets: image, duration: 1000, x: ox2, onComplete: this.effectDone.bind(this) });
                break;

            case 3:
                let oy: number = image.y;
                image.y = -1000;
                image.visible = true;
                this.scene.tweens.add({ targets: image, duration: 1000, y: oy, onComplete: this.effectDone.bind(this) });
                break;

            case 4:
                let oy2: number = image.y;
                image.y = 1000;
                image.visible = true;
                this.scene.tweens.add({ targets: image, duration: 1000, y: oy2, onComplete: this.effectDone.bind(this) });
                break;

            case 5:
                image.alpha=0;
                image.visible = true;
                this.scene.tweens.add({ targets: image, duration: 1000, alpha:1, onComplete: this.effectDone.bind(this) });

                break;

            case 6:
                
                let ow:number=image.displayWidth;
                let oh:number=image.displayHeight;

                image.displayHeight=10;
                image.displayWidth=10;
                image.visible = true;
                this.scene.tweens.add({ targets: image, duration: 1000, displayWidth:ow,displayHeight:oh, onComplete: this.effectDone.bind(this) });
               
                break;

            case 7:                
                let endX:number=(page.effectParams.endX/100)*bscene.getW();
                let endY:number=(page.effectParams.endY/100)*bscene.getH();
                let ww:number=bscene.getW()*page.effectParams.zoomEnd;

                let ow2:number=image.displayWidth;
                let oh2:number=image.displayHeight;
                
                image.displayWidth=ww;
                image.scaleY=image.scaleX;

                let hh:number=image.displayHeight;

                image.displayHeight=oh2;
                image.displayWidth=ow2;

                image.angle=page.effectParams.angleStart;

                image.visible=true;

                this.scene.tweens.add({ targets: image, duration: page.effectParams.duration,x:endX,y:endY, displayWidth:ww,displayHeight:hh,angle:page.effectParams.angleEnd, onComplete: this.effectDone.bind(this) });

            break;

            case 8:
                
                //let ox3: number = image.x;
                let oy3: number = image.y;

                image.y=image.y-image.displayWidth/2;

                image.visible = true;
                this.scene.tweens.add({ targets: image, duration: 1000,y:oy3,yoyo:true,loop:true, onComplete: this.effectDone.bind(this) });
               
                break;
        }
        
    }
    effectDone()
    {
        this.callback();
    }
}