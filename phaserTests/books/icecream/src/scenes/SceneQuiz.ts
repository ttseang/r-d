import {
    BaseScene
} from "./BaseScene";
/* import {
    Align
} from "../common/util/align"; */

import {
    FlatButton
} from "../ui/FlatButton";

/* import {
    ColorBurst
} from "../common/effects/colorBurst"; */
import { PageModel } from "../mc/pageModel";
import { ColorBurst } from "../effects/colorBurst";
import { Sparks } from "../effects/sparks";
import { Ripple } from "../effects/ripple";
import { Flare } from "../effects/flare";
import { StarBurst } from "../effects/starBurst";
//
//
//
export class SceneQuiz extends BaseScene {
    private buttonArray:FlatButton[];
    private clickLock:boolean=false;
    private index:number=0;
    private quizData:any;
    private model:PageModel=PageModel.getInstance();
    private questText:Phaser.GameObjects.Text;

    constructor() {
        super('SceneQuiz');
    }
    preload() {}
    create() {
        this.index = -1;
        this.buttonArray = [];
        this.clickLock = false;
        //set up the base scene
        super.create();
        //set the grid for the scene
        this.makeGrid(11, 11);
        //show numbers for layout and debugging 
        //
        this.grid.showNumbers();
        this.setBackground('background');
      //  window.scene=this;
        //
        //
        //
        this.makeUi();
        /* let sb = new ScoreBox({
            scene: this
        }); */
      //  this.placeAtIndex(5, sb);
        this.quizData = this.cache.json.get('quiz').results;
        this.quizData = this.mixUpArray(this.quizData);

        this.model.numberOfQuestions=this.quizData.length;
        //
        //make the quiz text
        //       
        this.questText = this.placeText("question", 27, "QUIZ_TEXT");
        //
        //make buttons
        //
        for (let i = 0; i < 4; i++) {
            let btnAnswer = new FlatButton(this,'BUTTON_STYLE',
                 "button",
                 "",
                this.pickAnswer.bind(this)
            );
            this.placeAtIndex(49 + (i * 11), btnAnswer);
            this.buttonArray.push(btnAnswer);
        }
        this.getNext();
    }
    mixUpArray(array) {
        let len = array.length;
        for (let i = 0; i < 10; i++) {
            let p1 = Phaser.Math.Between(0, len - 1);
            let p2 = Phaser.Math.Between(0, len - 1);
            let temp = array[p1];
            array[p1] = array[p2];
            array[p2] = temp;
        }
        return array;
    }
    getNext() {
        if (this.index==this.quizData.length-1)
        {
           // this.scene.start("SceneOver");
            return;
        }
        let currentQuestion = this.getNextQuestion();

        //
        //set the data
        //
        this.fillInQuizData(currentQuestion);
        this.clickLock = false;
    }
    fillInQuizData(questObj) {
        this.questText.setText(questObj.question);
        for (let i = 0; i < 4; i++) {
            this.buttonArray[i].setText(questObj.answers[i]);
        }
    }
    getNextQuestion() {
        this.index++;

        let question = this.quizData[this.index];
        console.log(question);

        let answers = question.incorrect_answers.slice();
        answers.push(question.correct_answer);
     //   answers.forEach(function(answer){console.log(answer)});
        answers = this.mixUpArray(answers);
        return {
            question: question.question,
            answers: answers
        };
    }
    pickAnswer(button:FlatButton) {
        if (this.clickLock == true) {
            return;
        }
        this.clickLock = true;
        let answer = button.text1.text;
        if (answer == this.quizData[this.index].correct_answer) {
            console.log("Right");
           /*  this.emitter.emit("UP_POINTS", 1);
            this.mm.playSound("correct");
            this.mm.playSound("sparkle"); */
           // let sparks:Sparks=new Sparks(this,button.x,button.y);
          //  let ripple:Ripple=new Ripple(this,button.x,button.y,40,2);
         // let flare:Flare=new Flare(this,button.x,button.y,0xff0000);
        //  let stars:StarBurst=new StarBurst(this,button.x,button.y,20);

             let colorStars = new ColorBurst(this,button.x,button.y,2,25);
        } else {
            console.log("Wrong");
            this.showCorrect();
           // this.mm.playSound("wrong");
        }
        this.time.addEvent({
            delay: 2000,
            callback: this.delayGoNext,
            callbackScope: this,
            loop: false
        });
    }
    showCorrect() {
        for (let i = 0; i < 4; i++) {
            if (this.buttonArray[i].text1.text != this.quizData[this.index].correct_answer) {
                this.buttonArray[i].alpha = .25;
            }
        }
    }
    delayGoNext() {
        for (let i = 0; i < 4; i++) {
            this.buttonArray[i].alpha = 1;
        }
        this.getNext();
    }
    makeUi() {
      //  super.makeSoundPanel();
       // super.makeGear();
    }
    update() {}
}