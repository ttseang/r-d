import { Popper } from "../classes/Popper";
import { PageVo } from "../dataObjs/PageVo";
import { PopperVo } from "../dataObjs/PopperVo";
import { Sparks } from "../effects/sparks";
import { PageModel } from "../mc/pageModel";
import { TextStyles } from "../ui/TextStyles";
import Align from "../util/align";
import { ImageEffects } from "../util/imageEffects";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private image: Phaser.GameObjects.Sprite;
    private clickLock: boolean = false;
    private narration: Phaser.Sound.BaseSound;
    private pageIndex: number = 0;
    private textObj: Phaser.GameObjects.Text;
    private titleText: Phaser.GameObjects.Text;
    private circle: Phaser.GameObjects.Sprite;

    private model: PageModel = PageModel.getInstance();
    private buttonArray: Phaser.GameObjects.Sprite[] = [];
    private imageEffect:ImageEffects;

    private backgroundMusic:string="";
    private backgroundSound:Phaser.Sound.BaseSound;
    private popperObjs:Popper[];

    constructor() {
        super("SceneMain");
        this.imageEffect=new ImageEffects(this.effectDone.bind(this));
    }
    preload() {

    }
    create() {
        super.create();
        this.makeGrid(11, 11);
       
        this.buttonArray=[];
        this.popperObjs=[];

        let background: Phaser.GameObjects.Image = this.add.image(0, 0, "background");
        Align.scaleToGameW(background, 1, this);
        this.grid.placeAtIndex(60, background);

        //
        //
        //
        let square: Phaser.GameObjects.Image = this.add.image(0, 0, "circle");
        Align.scaleToGameW(square, 0.8, this);
        this.grid.placeAt(5, 4, square);


        this.placeTextBack();
       // this.grid.showPos();

        let black=this.textStyles.getStyle("BLACK").style;
        let title=this.textStyles.getStyle("TITLE_TEXT").style;
       
        this.textObj = this.add.text(0, 0, "TEXT", black).setOrigin(0.5, 0.5);
        this.textObj.setWordWrapWidth(this.gw * 0.9);
        // this.textObj.setAlign("center");
        this.grid.placeAtIndex(104, this.textObj);

        this.titleText = this.add.text(0, 0, "TITLE",  title).setOrigin(0.5, 0.5);
        this.grid.placeAtIndex(5, this.titleText);

        this.placeButtons();

        this.showPage(this.model.currentPage);
       
    }
    placeTextBack() {


        let textBack: Phaser.GameObjects.Image = this.add.image(0, 0, "holder");
        textBack.setOrigin(0, 0);
        textBack.setTint(0x00b894);
        textBack.displayWidth = this.gw;
        textBack.displayHeight = this.ch * 3;
        this.grid.placeAtIndex(88, textBack);
        textBack.x = 0;

        let textBack2: Phaser.GameObjects.Image = this.add.image(0, 0, "holder");
        textBack2.setOrigin(0, 0);
        textBack2.setTint(0x00b894);
        textBack2.displayWidth = this.gw;
        textBack2.displayHeight = this.ch;

        textBack2.x = 0;
        textBack2.y = 0;
    }
    placeButtons() {
        let btnNext: Phaser.GameObjects.Sprite = this.add.sprite(0, 0, "btnNext");
        Align.scaleToGameW(btnNext, 0.1, this);
        this.grid.placeAt(8, 7.5, btnNext);
        btnNext.setInteractive();
        btnNext.on("pointerdown", this.nextPage.bind(this));

        let btnPrev: Phaser.GameObjects.Sprite = this.add.sprite(0, 0, "btnPrev");
        Align.scaleToGameW(btnPrev, 0.1, this);
        this.grid.placeAt(2, 7.5, btnPrev);
        btnPrev.setInteractive();
        btnPrev.on("pointerdown", this.prevPage.bind(this));

        let btnReplay: Phaser.GameObjects.Sprite = this.add.sprite(0, 0, "btnRepeat");
        Align.scaleToGameW(btnReplay, 0.1, this);
        this.grid.placeAt(5, 7.5, btnReplay);
        btnReplay.setInteractive();
        btnReplay.on("pointerdown", this.playNaration.bind(this));

        this.buttonArray = [btnNext, btnPrev, btnReplay];
    }
    buttonsOff() {
        for (let i: number = 0; i < this.buttonArray.length; i++) {
            this.buttonArray[i].visible = false;
        }
    }
    buttonsOn() {
        for (let i: number = 0; i < this.buttonArray.length; i++) {
            this.buttonArray[i].visible = true;
        }
    }
    playNaration() {
        this.buttonsOff();
        this.narration = this.sound.add("narration" + this.pageIndex.toString(),{volume:1});
        this.narration.on("complete", this.audioDone.bind(this));
        this.narration.play();       
    }
    nextPage() {
        if (this.clickLock === true) {
            return;
        }
        if (this.pageIndex < this.model.pages.length-1) {
            this.pageIndex++;
            this.showPage(this.pageIndex);
        }
        else{
            this.backgroundSound.stop();
            this.scene.start("SceneQuiz");
        }
    }
    prevPage() {
        if (this.clickLock === true) {
            return;
        }

        if (this.pageIndex > 0) {
            this.pageIndex--;
            this.showPage(this.pageIndex);
        }

    }
    showPage(pageIndex: number) {
        this.pageIndex = pageIndex;
        if (this.image) {
            this.image.destroy();
        }
        if (this.circle) {
            this.circle.destroy();
        }
        this.destroyPoppers();
        this.model.currentPage=pageIndex;
        //
        //
        //
        let pageVo:PageVo=this.model.pages[pageIndex];

        this.image = this.add.sprite(0, 0, "pageImage" + pageIndex.toString());
        Align.scaleToGameW(this.image, 0.75, this);
        this.grid.placeAt(5, 4, this.image);

        this.circle = this.make.sprite({ x: 0, y: 0, key: "circle" });
        this.circle.setTintFill(0xff00ff);
        Align.scaleToGameW(this.circle, 0.75, this);
        this.grid.placeAt(5, 4, this.circle);
        this.circle.visible = false;

        this.image.mask = new Phaser.Display.Masks.BitmapMask(this, this.circle);
        this.image.visible = false;

        this.setBackgroundMusic(pageVo.bgMusic);
        this.playNaration();

        let text: string = pageVo.text;        
        this.textObj.setText(text);

        this.titleText.setText(pageVo.title);
        this.buttonsOff();
        
        if (pageVo.effect!==0)
        {
            if (pageVo.effect===7)
            {
              this.image.x=this.gw*(pageVo.effectParams.startX/100);
              this.image.y=this.gh*(pageVo.effectParams.startY/100);

              let ww:number=this.gw*pageVo.effectParams.zoomStart;
              console.log("ww="+ww);

              this.image.displayWidth=ww;
              this.image.scaleY=this.image.scaleX;

              //this.image.visible=true;
            }
            this.imageEffect.doEffect(this,this.image,pageVo);
        }   
        else
        {
            this.image.visible=true;
        }           
        this.makePoppers();
    }
    setBackgroundMusic(music:string)
    {
        if (music===this.backgroundMusic)
        {
            return;
        }
        if (music==="")
        {
            return;
        }
        this.backgroundMusic=music;
        if (this.backgroundSound)
        {
            this.backgroundSound.stop();
            
        }
        this.backgroundSound=this.sound.add(music,{volume:0.1,loop:true});
        this.backgroundSound.play();
    }
    audioDone() {
        this.buttonsOn();
    }
    effectDone() {
     //   this.playNaration();
    }
    destroyPoppers()
    {
        while(this.popperObjs.length>0)
        {
            this.popperObjs.pop().destroy();
        }
    }
    makePoppers()
    {
        let pageVo:PageVo=this.model.pages[this.pageIndex];
        console.log(pageVo);

        let poppers:PopperVo[]=pageVo.poppers;
        console.log(poppers);

        for (let i:number=0;i<poppers.length;i++)
        {
            let popperVo:PopperVo=poppers[i];
            console.log(popperVo);
            
            let popper:Popper=new Popper(this,"popper"+popperVo.key,popperVo.type,this.popperClicked.bind(this));
            
            Align.scaleToGameW(popper,popperVo.scale,this);

            this.grid.placeAt(popperVo.col,popperVo.row,popper);
            this.popperObjs.push(popper);
        }
    }
    popperClicked(p:Popper)
    {
        console.log(p.type2);

        if (p.type2==="flyup")
        {
            this.tweens.add({targets: p,duration: 500,y:-1000});
        }
        if (p.type2==="flydown")
        {
            this.tweens.add({targets: p,duration: 500,y:2000});
        }
        if (p.type2==="fade")
        {
            this.tweens.add({targets: p,duration: 500,alpha:0});
        }
        if (p.type2==="twirl")
        {
            this.tweens.add({targets: p,duration: 500,angle:360*5});
        }
        if (p.type2==="pop")
        {
            let s:Sparks=new Sparks(this,p.x,p.y);
            let pop:Phaser.Sound.BaseSound=this.sound.add("pop");
            pop.play();

            p.destroy();
        }
        if (p.type2==="randplace")
        {
            let xx:number=Phaser.Math.Between(this.gw*0.1,this.gw*0.9);
            let yy:number=Phaser.Math.Between(this.gh*0.1,this.gh*0.9);
          //  p.x=xx;
           // p.y=yy;
            this.tweens.add({targets: p,duration: 100,x:xx,y:yy});
            p.clickCount++;
            if (p.clickCount==4)
            {
                p.type2="pop";
            }
        }
        if (p.type2==="slidergame")
        {
            this.scene.start("SceneSlider");
        }
    }
}