import { PageVo } from "../dataObjs/PageVo";
import { ColorBurst } from "../effects/colorBurst";
import { Flare } from "../effects/flare";
import { StarBurst } from "../effects/starBurst";
import { PageModel } from "../mc/pageModel";

export class SceneLoad extends Phaser.Scene {
    private model: PageModel = PageModel.getInstance();

    constructor() {
        super("SceneLoad");
    }
    preload() {
        this.load.audio("pop","./assets/audio/pop.mp3");


        let folder: string = this.model.folder;
        /*  for (let i: number = 1; i < 21; i++) {
             this.load.image("pageImage" + i.toString(), "./assets/images/" + i.toString() + ".jpg");
             this.load.audio("narration" + i.toString(), "./assets/audio/" + i.toString() + ".mp3");
         } */
        for (let i: number = 0; i < this.model.pages.length; i++) {
            let pageVo: PageVo = this.model.pages[i];
            this.load.image("pageImage" + i.toString(), folder + "images/" + pageVo.image);

            let key: string = "narration" + i.toString();
            this.load.audio(key, folder + "audio/" + pageVo.audio);

            for(let k:number=0;k<pageVo.poppers.length;k++)
            {
                let pkey:string=pageVo.poppers[k].key;
                let popperPath:string=folder + "images/poppers/"+pkey+".png";
                this.load.image("popper"+pkey,popperPath);
            }

            if (pageVo.bgMusic !== "") {
                let bgPath: string = folder + "/audio/backgroundMusic/" + pageVo.bgMusic + ".mp3";

              //  console.log(pageVo.bgMusic);

                if (!this.cache.audio.has(pageVo.bgMusic)) {
                    this.load.audio(pageVo.bgMusic, bgPath);
                }
            }

        }
        //this.load.image("button", "./assets/ui/buttons/1/3.png");
        this.load.image("background", "./assets/background.png");
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("circle", "./assets/circle.png");
        this.load.image("btnNext", "./assets/ui/btnNext.png");
        this.load.image("btnPlay", "./assets/ui/btnPlay.png");
        this.load.image("btnPrev", "./assets/ui/btnPrev.png");
        this.load.image("btnRepeat", "./assets/ui/btnRepeat.png");

        ColorBurst.preload(this, "./assets/effects/colorStars.png");
        Flare.preload(this, "./assets/effects/flare.png");
        StarBurst.preload(this, "./assets/effects/stars.png");
    }
    create() {

        this.scene.start("SceneMain");
    }
}