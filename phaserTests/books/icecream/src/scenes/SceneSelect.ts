import { PageModel } from "../mc/pageModel";
import { FlatButton } from "../ui/FlatButton";
import { BaseScene } from "./BaseScene";

export class SceneSelect extends BaseScene
{
    private model:PageModel=PageModel.getInstance();

    constructor()
    {
        super("SceneSelect");
    }
    preload()
    {
        console.log("preload");
        this.load.image("button", "./assets/ui/buttons/1/3.png");
    }
    create()
    {
        super.create();
        console.log("create");
        this.makeGrid(11,11);
       // this.grid.showNumbers();

         let buttonArray:string[]=['Ice Cream','Mars'];

        for(let i:number=0;i<buttonArray.length;i++)
        {
            let button:FlatButton=new FlatButton(this,"WHITE","button",buttonArray[i],this.selectBook.bind(this));
            button.index=i;
            this.grid.placeAt(5,2+i*2,button);
        } 
    }
    selectBook(button:FlatButton)
    {
        let folder:string[]=["icecream","mars"];
        this.model.folder="./assets/"+folder[button.index]+"/";
        this.scene.start("SceneData");
    }
}