import IBaseScene from "../interfaces/IBaseScene";

export class SliderSquare extends Phaser.GameObjects.Sprite
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    public index:number=0;
    public xx:number=0;
    public yy:number=0;
    public ox:number=0;
    public oy:number=0;
    
    public callback:Function;

    constructor(bscene:IBaseScene,key:string,index:number)
    {
        super(bscene.getScene(),0,0,key,index.toString());
        this.setOrigin(0,0);
        this.scene=bscene.getScene();

        this.setInteractive();
        this.index=index;
        this.scene.add.existing(this);

        this.on('pointerdown',()=>{this.callback(this)});
    }
}