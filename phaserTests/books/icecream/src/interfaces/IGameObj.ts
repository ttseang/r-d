/**
 * allows us to pass a sprite or image
 * as an IGameObj
 */
export interface IGameObj
{
    angle: number;
    alpha: number;
    visible: boolean;
    displayWidth:number;
    displayHeight:number;
    scaleX:number;
    scaleY:number;
    x:number;
    y:number;
}