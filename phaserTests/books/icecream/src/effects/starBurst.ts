import IBaseScene from "../interfaces/IBaseScene";

export class StarBurst {
    public scene:Phaser.Scene;

    constructor(bscene:IBaseScene,x:number=0,y:number=0,size:number=5,count:number=25,dist:number=150,duration:number=1000,maxDist:number=300,f:number=0,tint:number=0xffffff) {
        this.scene = bscene.getScene();
        
        for (var i = 0; i < count; i++) {
            var star = this.scene.add.sprite(x, y, "effectStars");
            //
            //
            //
            star.setTint(tint);
            star.setFrame(f);
            star.setOrigin(0.5, 0.5);
            var r = Phaser.Math.Between(50, maxDist);
            var s = Phaser.Math.Between(1, 100) / 100;
            star.scaleX = s;
            star.scaleY = s;
            var angle = i * (360 / count);
            var tx = x + r * Math.cos(angle);
            var ty = y + r * Math.sin(angle);
            //  star.x=tx;
            // star.y=ty;
            this.scene.tweens.add({
                targets: star,
                duration: duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                    scope: this
                }]
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    static preload(scene, path) {
        scene.load.spritesheet('effectStars', path, {
            frameWidth: 25,
            frameHeight: 25
        });
    }
}