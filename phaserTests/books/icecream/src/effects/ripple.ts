import IBaseScene from "../interfaces/IBaseScene";

export class Ripple {
    public scene: Phaser.Scene;
    private color: number;
    private size: number;

    constructor(bscene: IBaseScene, x: number = 0, y: number = 0, count: number = 25, size: number = 5, dist: number = 150, duration: number = 1000, color: number = 0xffffff) {
        this.scene = bscene.getScene();


        this.size = size;
        //
        //
        //
        this.color = color;

        for (var i = 0; i < count; i++) {
            var s = this.getSpark();
            s.x = x;
            s.y = y;
            //
            //
            //
            var angle = i * (360 / count);
            //  var r = game.rnd.integerInRange(50, 100);
            var tx = x + dist * Math.cos(angle);
            var ty = y + dist * Math.sin(angle);
            this.scene.tweens.add({
                targets: s,
                duration: duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                    scope: this
                }]
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    getSpark() {
        var s = this.scene.add.graphics();
        s.fillStyle(this.color, 1);
        s.fillCircle(0, 0, this.size);
        return s;
    }
}