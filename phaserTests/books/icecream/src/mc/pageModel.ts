import { PageVo } from "../dataObjs/PageVo";

let instance:PageModel=null;

export class PageModel
{
    public pages:PageVo[]=[];
    public numberOfQuestions:number=0;
    public folder:string="./assets/icecream/";
    public currentPage:number=0;
    
    constructor()
    {

    }
    static getInstance():PageModel
    {
        if (instance===null)
        {
            instance=new PageModel();
        }
        return instance;
    }
}