//import Phaser = require("phaser");
import Phaser from 'phaser';
import { SceneData } from './scenes/SceneData';
import { SceneLoad } from './scenes/SceneLoad';
import { SceneMain } from "./scenes/SceneMain";
import { SceneQuiz } from './scenes/SceneQuiz';
import { SceneSelect } from './scenes/SceneSelect';
import { SceneSlider } from './scenes/SceneSlider';
let isMobile = navigator.userAgent.indexOf("Mobile");
if (isMobile == -1) {
    isMobile = navigator.userAgent.indexOf("Tablet");
}
let w = 800;
let h = 600;
//
//
//if (isMobile != -1) {
    w = window.innerWidth;
    h = window.innerHeight;
//}
const config = {
    type: Phaser.AUTO,//
    width: w,
    height: h,
    parent: 'phaser-game',
    scene: [SceneSelect,SceneData,SceneLoad,SceneQuiz,SceneMain,SceneSlider]
};

new Phaser.Game(config);