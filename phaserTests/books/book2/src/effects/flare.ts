import IBaseScene from "../interfaces/IBaseScene";
import {
    Align
} from "../util/align";
export class Flare extends Phaser.GameObjects.Sprite {
    public scene: Phaser.Scene;

    constructor(bscene: IBaseScene, x: number = 0, y: number = 0, tint: number = 0xffffff) {

        super(bscene.getScene(), x, y, "flare");
        this.scene = bscene.getScene();

        let scale = .2;
        let direction = -1;
        let duration = 1000;
        if (scale) {
            scale = scale;
        }
        if (direction) {
            direction = direction;
        }
        if (duration) {
            duration = duration;
        }

        this.setTint(tint);

        let finalAngle = 270 * direction;
        Align.scaleToGameW(this, scale, bscene);

        this.scene.add.existing(this);
        this.scene.tweens.add({
            targets: this,
            duration: duration,
            alpha: 0,
            angle: finalAngle,
            onComplete: this.tweenDone.bind(this)

        });
    }
    tweenDone() {
        this.destroy();
    }
    static preload(scene: Phaser.Scene, path: string) {
        scene.load.image('flare', path);
    }
}