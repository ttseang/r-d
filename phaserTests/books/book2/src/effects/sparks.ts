import IBaseScene from "../interfaces/IBaseScene";

export class Sparks {
    public scene:Phaser.Scene;
    private color:number;
    private size:number;

    constructor(bscene:IBaseScene,x:number=0,y:number=0,size:number=1,color:number=0xffffff,count:number=25) {
        // super(config.scene);
        this.scene = bscene.getScene();
        this.color=color;
        this.size=size;
     
        for (var i = 0; i < count;i++) {
            var s = this.getSpark();
            s.x = x;
            s.y = y;
            //   this.add(s);
            var angle = i * (360 / count);
            var r = Phaser.Math.Between(50, 100);
            var tx = x + r * Math.cos(angle);
            var ty = y + r * Math.sin(angle);
            this.scene.tweens.add({
                targets: s,
                duration: 1000,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                    scope: this
                }]
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    getSpark() {
        var s = this.scene.add.graphics();
        s.fillStyle(this.color, 1);
        s.fillCircle(0, 0, this.size);
        return s;
    }
}