import { PageVo } from "../dataObjs/PageVo";
import { PopperVo } from "../dataObjs/PopperVo";
import TextVo from "../dataObjs/Textvo";
import { PageModel } from "../mc/pageModel";

export class SceneData extends Phaser.Scene {
    private model: PageModel = PageModel.getInstance();

    constructor() {
        super("SceneData");

    }
    preload() {
        this.load.json("pages", this.model.folder + "data/bookData.json");
        this.load.json('quiz', this.model.folder + 'data/quiz.json');
    }
    create() {
        let book: any = this.cache.json.get("pages").book;

        console.log(book);
        
        let options:any=book.options;

     //   console.log(options);

        let pages: PageVo[] = [];

        for (let i: number = 0; i < book.pages.length; i++) {
            let page: any = book.pages[i];
            console.log(page);
            let pageVo: PageVo = new PageVo(page.image, page.audio, page.bgmusic, page.title, page.text, page.effect);

            pageVo.effectParams.load(page.effectParams);

            let poppers: any = page.poppers;
            if (poppers) {
                if (poppers.images) {
                    for (let j: number = 0; j < poppers.images.length; j++) {
                        let pdata: any = poppers.images[j];
                        let popper: PopperVo = new PopperVo(pdata.key, parseFloat(pdata.row), parseFloat(pdata.col), parseFloat(pdata.scale), pdata.type);
                        pageVo.poppers.push(popper);
                    }
                }
            }
            if (page.subs)
            {
                console.log(page.subs);

                for(let m:number=0;m<page.subs.length;m++)
                {
                    let sub:any=page.subs[m];
                    console.log(sub);
                    let textVo:TextVo=new TextVo(sub.start,sub.end,sub.text);
                    pageVo.subs.push(textVo);
                }
               // pageVo.subs=page.subs;
            }
           // console.log(poppers);

            pages.push(pageVo);
        }
        console.log(pages);
        //console.log(this.model);

        this.model.pages = pages;
        this.scene.start("SceneLoad");
    }
}