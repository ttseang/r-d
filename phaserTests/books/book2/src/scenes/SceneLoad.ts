import { PageVo } from "../dataObjs/PageVo";
import { ColorBurst } from "../effects/colorBurst";
import { Flare } from "../effects/flare";
import { StarBurst } from "../effects/starBurst";
import { PageModel } from "../mc/pageModel";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene {
    private model: PageModel = PageModel.getInstance();
    private loadingText:Phaser.GameObjects.Text;

    constructor() {
        super("SceneLoad");
    }
    preload() {
        super.create();

        this.loadingText=this.add.text(0,0,"0%",{fontSize:"20px"}).setOrigin(0.5,0.5);
        Align.center(this.loadingText,this);
        this.load.on('progress',this.onProgress.bind(this));


        this.load.audio("pop","./assets/audio/pop.mp3");


        let folder: string = this.model.folder;
        /*  for (let i: number = 1; i < 21; i++) {
             this.load.image("pageImage" + i.toString(), "./assets/images/" + i.toString() + ".jpg");
             this.load.audio("narration" + i.toString(), "./assets/audio/" + i.toString() + ".mp3");
         } */
        for (let i: number = 0; i < this.model.pages.length; i++) {
            let pageVo: PageVo = this.model.pages[i];
            this.load.image("pageImage" + i.toString(), folder + "images/" + pageVo.image);

            let key: string = "narration" + i.toString();
            this.load.audio(key, folder + "audio/" + pageVo.audio);

            for(let k:number=0;k<pageVo.poppers.length;k++)
            {
                let pkey:string=pageVo.poppers[k].key;
                let popperPath:string=folder + "images/poppers/"+pkey+".png";
                this.load.image("popper"+pkey,popperPath);
            }

            if (pageVo.bgMusic !== "") {
                let bgPath: string = folder + "/audio/backgroundMusic/" + pageVo.bgMusic + ".mp3";

              //  console.log(pageVo.bgMusic);

                if (!this.cache.audio.has(pageVo.bgMusic)) {
                    this.load.audio(pageVo.bgMusic, bgPath);
                }
            }

           /*  if (pageVo.subs!="")
            {
                let subPath:string=folder+"/subs/"+pageVo.subs+".json";
                this.load.json("subs"+i.toString(),subPath);
            } */

        }
        //this.load.image("button", "./assets/ui/buttons/1/3.png");
        this.load.image("background", "./assets/background.png");
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("circle", "./assets/circle.png");
        this.load.image("btnNext", "./assets/ui/btnNext.png");
        this.load.image("btnPlay", "./assets/ui/btnPlay.png");
        this.load.image("btnPrev", "./assets/ui/btnPrev.png");
        this.load.image("btnRepeat", "./assets/ui/btnRepeat.png");

        ColorBurst.preload(this, "./assets/effects/colorStars.png");
        Flare.preload(this, "./assets/effects/flare.png");
        StarBurst.preload(this, "./assets/effects/stars.png");
    }
    onProgress(per:number)
    {
        per=Math.floor(per*100);
        this.loadingText.setText(per.toString()+"%");
    }
    create() {

        this.scene.start("SceneMain");
    }
}