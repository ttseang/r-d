import { Popper } from "../classes/Popper";
import { SubPlayer } from "../comps/subPlayer";
import { PageVo } from "../dataObjs/PageVo";
import { PopperVo } from "../dataObjs/PopperVo";
import { PosVo } from "../dataObjs/PosVo";
import { Sparks } from "../effects/sparks";
import { PageModel } from "../mc/pageModel";
import { TextStyles } from "../ui/TextStyles";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { FormUtil } from "../util/formUtil";
import { ImageEffects } from "../util/imageEffects";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private image: Phaser.GameObjects.Sprite;
    private clickLock: boolean = false;
    private narration: Phaser.Sound.BaseSound;
    private pageIndex: number = 0;
    private textObj: Phaser.GameObjects.Text;
    private titleText: Phaser.GameObjects.Text;
    private circle: Phaser.GameObjects.Sprite;

    private model: PageModel = PageModel.getInstance();
    private buttonArray: Phaser.GameObjects.Sprite[] = [];
    private imageEffect: ImageEffects;

    private backgroundMusic: string = "";
    private backgroundSound: Phaser.Sound.BaseSound;
    private popperObjs: Popper[];

    private circScale1: number = 0.55;
    private circScale2: number = 0.4;
    private imageScale: number = 0.5;

    private textSize1: number = 30;
    private textSize2: number = 22;
    private background: Phaser.GameObjects.Image;

    private btnNext: Phaser.GameObjects.Sprite;
    private btnPrev: Phaser.GameObjects.Sprite;
    private btnReplay: Phaser.GameObjects.Sprite;

    private formUtil: FormUtil;


    constructor() {
        super("SceneMain");
        this.imageEffect = new ImageEffects(this.effectDone.bind(this));
    }
    preload() {

    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.formUtil = new FormUtil(this, 11, 11);
        // this.formUtil.showNumbers();

        this.buttonArray = [];
        this.popperObjs = [];

        this.background = this.add.image(0, 0, "holder");


        //
        //
        //
        let square: Phaser.GameObjects.Image = this.add.image(0, 0, "circle");
        Align.scaleToGameW(square, this.circScale1, this);
        square.setTint(0x000000);
        square.visible = false;
        this.grid.placeAt(2.6, 4.5, square);


        // this.placeTextBack();
        //   this.grid.showPos();

        let black = this.textStyles.getStyle("BLACK").style;

        this.textObj = this.add.text(0, 0, "TEXT", black);
        this.textObj.setFontSize(this.textSize2);

        let title = this.textStyles.getStyle("TITLE_TEXT").style;

        this.titleText = this.add.text(0, 0, "TITLE", title);//.setOrigin(0.5, 0.5);
        this.titleText.setFontSize(this.textSize1);
        this.titleText.setWordWrapWidth(this.gw * 0.45);



        this.makeButtons();
        this.placeChildren();
        console.log(this.cw, this.ch);

        this.showPage(this.model.currentPage);
        // this.showPage(0);
        window.onresize = this.onResize.bind(this);
        window['scene'] = this;
    }

    onResize() {

        let width: number = window.innerWidth / window.devicePixelRatio;
        let height: number = window.innerHeight / window.devicePixelRatio;

        this.scale.resize(width, height);
        // this.scene.game.scale.resize(width, height);
        this.cameras.resize(width, height);
        this.gw = width;
        this.gh = height;

        //  this.sys.game.config.width = width;
        //  this.sys.game.config.height = height;
        this.grid.hide();
        this.makeGrid(11, 11);

        //  this.grid.showPos();

        // console.log(this.cw,this.ch);

        this.placeChildren();

    }
    placeChildren() {


        Align.scaleToGameW(this.background, 1, this);
        this.grid.placeAtIndex(60, this.background);

        this.formUtil.placeElementAt(29, 'dtext', false, false);

        this.titleText.setFontSize(this.textSize1);
        this.titleText.setWordWrapWidth(this.gw * 0.45);

        this.textObj.setFontSize(this.textSize2);
        this.textObj.setWordWrapWidth(this.gw * 0.4);

        this.grid.placeAt(5.5, 4.5, this.textObj);
        this.grid.placeAt(5.5, 2.5, this.titleText);
        //
        //
        //
        Align.scaleToGameW(this.btnReplay, 0.05, this);
        Align.scaleToGameW(this.btnPrev, 0.05, this);
        Align.scaleToGameW(this.btnNext, 0.05, this);
        this.grid.placeAt(8, 8, this.btnReplay);
        this.grid.placeAt(9, 8, this.btnNext);
        this.grid.placeAt(7, 8, this.btnPrev);

        if (this.image) {
            this.placePageElements();
        }
        // this.formUtil.showNumbers();
    }
    placePageElements() {
        Align.scaleToGameW(this.image, this.imageScale, this);
        this.grid.placeAt(2.6, 4.5, this.image);
        Align.scaleToGameW(this.circle, this.circScale2, this);
        this.grid.placeAt(2.6, 4.5, this.circle);

        for (let i: number = 0; i < this.popperObjs.length; i++) {
            let popper: Popper = this.popperObjs[i];
            Align.scaleToGameW(popper, popper.dataProvider.scale, this);

            this.grid.placeAt(popper.dataProvider.col, popper.dataProvider.row, popper);
        }

    }
    placeTextBack() {


        let textBack: Phaser.GameObjects.Image = this.add.image(0, 0, "holder");
        textBack.setOrigin(0, 0);
        textBack.setTint(0x00b894);
        textBack.displayWidth = this.gw;
        textBack.displayHeight = this.ch * 3;
        this.grid.placeAtIndex(88, textBack);
        textBack.x = 0;

        let textBack2: Phaser.GameObjects.Image = this.add.image(0, 0, "holder");
        textBack2.setOrigin(0, 0);
        textBack2.setTint(0x00b894);
        textBack2.displayWidth = this.gw;
        textBack2.displayHeight = this.ch;

        textBack2.x = 0;
        textBack2.y = 0;
    }
    makeButtons() {
        this.btnNext = this.add.sprite(0, 0, "btnNext");
        this.btnNext.setInteractive();
        this.btnNext.on("pointerdown", this.nextPage.bind(this));

        this.btnPrev = this.add.sprite(0, 0, "btnPrev");
        this.btnPrev.setInteractive();
        this.btnPrev.on("pointerdown", this.prevPage.bind(this));

        this.btnReplay = this.add.sprite(0, 0, "btnRepeat");



        this.btnReplay.setInteractive();
        this.btnReplay.on("pointerdown", this.playNaration.bind(this));

        this.buttonArray = [this.btnNext, this.btnPrev, this.btnReplay];
    }
    buttonsOff() {
        for (let i: number = 0; i < this.buttonArray.length; i++) {
            this.buttonArray[i].visible = false;
        }
    }
    buttonsOn() {
        for (let i: number = 0; i < this.buttonArray.length; i++) {
            this.buttonArray[i].visible = true;
        }
    }
    playNaration() {
        this.buttonsOff();
        let pageVo: PageVo = this.model.pages[this.pageIndex];
        console.log("subs=" + pageVo.subs);

        if (pageVo.subs.length>0) {
            let player = new SubPlayer(this, "subs" + this.pageIndex.toString(), "narration" + this.pageIndex.toString(),pageVo.subs);
            
            player.onTextChange = this.setHTMLText.bind(this);
            player.onComplete = this.audioDone.bind(this);
            player.play();
        }

        else {
            this.narration = this.sound.add("narration" + this.pageIndex.toString(), { volume: 1 });
            this.narration.on("complete", this.audioDone.bind(this));
            this.narration.play();
        }
    }
    setHTMLText(html: string) {
        console.log("h="+html);
        let pageVo: PageVo = this.model.pages[this.pageIndex];
        html = "<h2>" + pageVo.title + "</h2>" + html;
        document.getElementById('dtext').innerHTML = html;
    }
    nextPage() {
        if (this.clickLock === true) {
            return;
        }
        if (this.pageIndex < this.model.pages.length - 1) {
            this.pageIndex++;
            this.showPage(this.pageIndex);
        }
        else {
            this.backgroundSound.stop();
            this.scene.start("SceneQuiz");
        }
    }
    prevPage() {
        if (this.clickLock === true) {
            return;
        }

        if (this.pageIndex > 0) {
            this.pageIndex--;
            this.showPage(this.pageIndex);
        }

    }
    showPage(pageIndex: number) {
        this.pageIndex = pageIndex;
        if (this.image) {
            this.image.destroy();
        }
        if (this.circle) {
            this.circle.destroy();
        }
        this.destroyPoppers();
        this.model.currentPage = pageIndex;
        //
        //
        //
        let pageVo: PageVo = this.model.pages[pageIndex];

        this.image = this.add.sprite(0, 0, "pageImage" + pageIndex.toString());


        this.circle = this.make.sprite({ x: 0, y: 0, key: "circle" });
        this.circle.setTintFill(0xff00ff);

        if (pageVo.subs.length==0) {
            document.getElementById("dtext").style.display = "none";
            this.textObj.visible = true;
            this.titleText.visible = true;
        }
        else {
            document.getElementById("dtext").style.display = "block";
            this.textObj.visible = false;
            this.titleText.visible = false;
        }

        this.circle.visible = false;

        this.image.mask = new Phaser.Display.Masks.BitmapMask(this, this.circle);
        this.image.visible = false;

        this.placePageElements();

        this.setBackgroundMusic(pageVo.bgMusic);
        this.playNaration();

        let text: string = pageVo.text;
        this.textObj.setText(text);

        this.titleText.setText(pageVo.title);

        this.setHTMLText(text);




        this.buttonsOff();

        if (pageVo.effect !== 0) {
            if (pageVo.effect === 7) {
                this.image.x = this.gw * (pageVo.effectParams.startX / 100);
                this.image.y = this.gh * (pageVo.effectParams.startY / 100);

                let ww: number = this.gw * pageVo.effectParams.zoomStart;
                // console.log("ww="+ww);

                this.image.displayWidth = ww;
                this.image.scaleY = this.image.scaleX;

                //this.image.visible=true;
            }
            this.imageEffect.doEffect(this, this.image, pageVo);
        }
        else {
            this.image.visible = true;
        }
        this.makePoppers();
    }
    setBackgroundMusic(music: string) {
        if (music === this.backgroundMusic) {
            return;
        }
        if (music === "") {
            return;
        }
        this.backgroundMusic = music;
        if (this.backgroundSound) {
            this.backgroundSound.stop();

        }
        this.backgroundSound = this.sound.add(music, { volume: 0.1, loop: true });
        this.backgroundSound.play();
    }
    audioDone() {
        this.buttonsOn();
    }
    effectDone() {
        //   this.playNaration();
    }
    destroyPoppers() {
        while (this.popperObjs.length > 0) {
            this.popperObjs.pop().destroy();
        }
    }
    makePoppers() {
        let pageVo: PageVo = this.model.pages[this.pageIndex];
        console.log(pageVo);

        let poppers: PopperVo[] = pageVo.poppers;
        console.log(poppers);


        for (let i: number = 0; i < poppers.length; i++) {
            let popperVo: PopperVo = poppers[i];
            console.log(popperVo);

            let popper: Popper = new Popper(this, "popper" + popperVo.key, popperVo.type, this.popperClicked.bind(this));
            popper.dataProvider = popperVo;

            Align.scaleToGameW(popper, popperVo.scale, this);

            this.grid.placeAt(popperVo.col, popperVo.row, popper);
            this.popperObjs.push(popper);
        }
    }
    popperClicked(p: Popper) {
        console.log(p.type2);

        if (p.type2 === "flyup") {
            this.tweens.add({ targets: p, duration: 500, y: -1000 });
        }
        if (p.type2 === "flydown") {
            this.tweens.add({ targets: p, duration: 500, y: 2000 });
        }
        if (p.type2 === "fade") {
            this.tweens.add({ targets: p, duration: 500, alpha: 0 });
        }
        if (p.type2 === "twirl") {
            this.tweens.add({ targets: p, duration: 500, angle: 360 * 5 });
        }
        if (p.type2 === "pop") {
            let s: Sparks = new Sparks(this, p.x, p.y);
            let pop: Phaser.Sound.BaseSound = this.sound.add("pop");
            pop.play();

            p.destroy();
        }
        if (p.type2 === "randplace") {
            let xx: number = Phaser.Math.Between(1, this.grid.cols - 1);
            let yy: number = Phaser.Math.Between(1, this.grid.rows - 1);
            p.dataProvider.row = xx;
            p.dataProvider.col = yy;

            let posVo: PosVo = this.grid.getRealXY(xx, yy);
            //  p.x=xx;
            // p.y=yy;
            this.tweens.add({ targets: p, duration: 100, x: posVo.x, y: posVo.y });
            p.clickCount++;
            if (p.clickCount == 4) {
                p.type2 = "pop";
            }
        }
        if (p.type2 === "slidergame") {
            this.scene.start("SceneSlider");
        }
    }
}