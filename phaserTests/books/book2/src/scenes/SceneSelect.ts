import { PageModel } from "../mc/pageModel";
import { FlatButton } from "../ui/FlatButton";
import { BaseScene } from "./BaseScene";

export class SceneSelect extends BaseScene
{
    private model:PageModel=PageModel.getInstance();
    private flatButtons:FlatButton[]=[];
    constructor()
    {
        super("SceneSelect");
    }
    preload()
    {
        console.log("preload");
        this.load.image("button", "./assets/ui/buttons/1/3.png");
    }
    create()
    {
        super.create();
        console.log("create");
        this.makeGrid(11,11);
       // this.grid.showNumbers();

         let buttonArray:string[]=['Ice Cream','Mars'];

        for(let i:number=0;i<buttonArray.length;i++)
        {
            let button:FlatButton=new FlatButton(this,"WHITE","button",buttonArray[i],this.selectBook.bind(this));
            button.index=i;
            this.flatButtons.push(button);
           // this.grid.placeAt(5,2+i*2,button);
        } 
        this.placeChildren();
        window.onresize=this.onResize.bind(this);
    }
    onResize() {
        let width: number = window.innerWidth / window.devicePixelRatio;
        let height: number = window.innerHeight / window.devicePixelRatio;

        this.scale.resize(width, height);
       
        this.cameras.resize(width, height);
        this.gw = width;
        this.gh = height;

        
        this.grid.hide();
        this.makeGrid(11, 11);  
       

        this.placeChildren();
    }
    placeChildren()
    {
        for (let i:number=0;i<this.flatButtons.length;i++)
        {
            this.grid.placeAt(5,2+i*2,this.flatButtons[i]);
        }
    }
    selectBook(button:FlatButton)
    {
        console.log(button.index);
        
        if (button.index==1)
        {
            this.model.useCircle=true;
        }
        let folder:string[]=["icecream","mars"];
        this.model.folder="./assets/"+folder[button.index]+"/";
        this.scene.start("SceneData");
    }
}