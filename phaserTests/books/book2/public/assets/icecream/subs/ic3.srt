﻿1
00:00:00,000 --> 00:00:06,266
Experimenters in China and India discover that adding salt 

3
00:00:06,266 --> 00:00:12,266
to snow makes a slush that gets very cold. They use this trick to cool drinks quickly. 

4
00:00:12,266 --> 00:00:17,199
Italians rediscover it around 1500.

