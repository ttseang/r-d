export class ActionVo {
    public action: string;
    public item: string;
    public delay: number;
    constructor(action: string, item: string, delay: number) {
        this.action = action;
        this.item = item;
        this.delay = delay;
    }
}