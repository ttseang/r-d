import { GameObjects } from "phaser";
import { LayoutManager } from "../classes/layoutManager";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { AnimationUtil } from "../util/animUtil";
import { FormUtil } from "../util/formUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private fox:GameObjects.Sprite;
    private ninja:GameObjects.Sprite;
    private bubble:GameObjects.Sprite;
    private bubbleText:GameObjects.Text;
    private formUtil:FormUtil;
    private layoutMananger:LayoutManager;
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.atlas("fox","./assets/fox.png","./assets/fox.json");
        this.load.atlas("ninja","./assets/ninja_girl.png","./assets/ninja_girl.json");
        this.load.image("bubble","./assets/bubble2.png");
    }
    create() {
        super.create();
        this.makeGrid(22,22);
        this.layoutMananger=new LayoutManager(this);

        this.formUtil=new FormUtil(this);
        this.formUtil.alignGrid=this.grid;
        console.log("create!");
        this.ninja=this.add.sprite(100,200,"ninja");
      //  this.fox=this.add.sprite(100, 200, "fox");

       // Align.scaleToGameW(this.fox,0.25,this);
        
     //   this.grid.showNumbers();
        this.grid.placeAtIndex(244,this.ninja);

        let animUtil:AnimationUtil=new AnimationUtil();
        //animUtil.makeAnims(this,"fox");
        animUtil.makeAnims2(this,"ninja");

        this.ninja.play("idle");

        //this.moveIn();
        //this.fox.play("idle");
        this.bubble=this.add.sprite(0,0,"bubble");
        Align.scaleToGameW(this.bubble,0.25,this);
        this.bubble.visible=false;
        
        this.bubbleText=this.add.text(0,0,"TEST",{color:"#000000",fontSize:"26px"}).setOrigin(0.5,0.5);
        this.bubbleText.visible=false;
      //  this.moveNinjaTo(2,2);
        //this.input.on('pointerdown',this.onDown.bind(this));

        this.formUtil.addClickCallback("btnWrong",this.showWrong.bind(this));
        this.formUtil.addClickCallback("btnRight",this.showRight.bind(this));
        this.formUtil.addClickCallback("btnFly",()=>{this.flyToDiv('testdiv')});
        this.formUtil.addClickCallback("btnFly2",()=>{this.flyToDiv('testdiv2')});
        this.formUtil.addClickCallback("btnFly3",()=>{this.flyToDiv("testdiv3")});
        this.formUtil.addClickCallback("btnFly4",()=>{this.flyToDiv("testdiv4")});
    }
    flyToTest()
    {
        this.flyToDiv("testdiv");
    }
    flyToBottomDiv()
    {
        this.flyToDiv("testdiv2");
    }
    showWrong()
    {
        this.ninja.play("dead");
        setTimeout(() => {
            this.ninja.play("idle");
        }, 1000);
    }
    showRight()
    {
        this.ninja.play("attack");
        setTimeout(() => {
            this.ninja.play("idle");
        }, 1000);
    }
    onDown(pointer)
    {
        this.tweens.add({targets: this.ninja,duration: 1000,onComplete:this.moveDone.bind(this),y:pointer.y,x:pointer.x});
        this.ninja.play("run");   
    }
    flyToDiv(divname:string)
    {
        console.log(divname);
        let div:HTMLElement=document.getElementById(divname);
        let pos:PosVo=this.formUtil.getElPos(div);
        console.log(pos);
        if (pos.x<this.ninja.x)
        {
            this.ninja.flipX=true;
        }
        else
        {
            this.ninja.flipX=false;
        }
        this.tweens.add({targets: this.ninja,duration: 1000,onComplete:this.moveDone.bind(this),y:pos.y,x:pos.x});
        this.ninja.play("run");
    }
    moveIn()
    {
        let sx:number=this.fox.scaleX;
        let sy:number=this.fox.scaleY;
        this.ninja.play("run");
        this.ninja.setScale(0.1,0.1);
        this.tweens.add({targets: this.ninja,duration: 2000,onComplete:this.moveInDone.bind(this),scaleX:sx,scaleY:sy});
        
    }
    moveInDone()
    {
        this.ninja.play("idle");
        this.addBubble("HELLO!");
        setTimeout(() => {
            this.doFly();
        }, 2000);
    }
    doFly()
    {
        this.hideBubble();
        this.moveNinjaTo(18,10);
    }
    moveNinjaTo(col:number,row:number)
    {
        let pos:PosVo=this.grid.getRealXY(col,row);
        this.tweens.add({targets: this.ninja,duration: 1000,onComplete:this.moveDone.bind(this),y:pos.y,x:pos.x});
        this.ninja.play("run");
    }
    moveDone()
    {
        this.formUtil.unblankElement("testdiv")
        this.ninja.play("idle");
    }
    addBubble(text:string)
    {
        this.grid.placeAtIndex(96,this.bubble);
        this.grid.placeAtIndex(96,this.bubbleText);

        this.bubbleText.setText(text);

        this.bubbleText.visible=true;
        this.bubble.visible=true;
    }
    hideBubble()
    {
        this.bubbleText.visible=false;
        this.bubble.visible=false;
    }
}