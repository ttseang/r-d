export class TextUtil
{
    static wordsIntoChunks(text:string,len:number=50)
    {
        let words:string[]=text.split(" ");

        let chunks:string[]=[];
      

        let chunk:string="";

        for (let i:number=0;i<words.length;i++)
        {
            let word:string=words[i];

            if (word.length+chunk.length>len)
            {
                chunks.push(chunk);

                chunk=word+" ";

            }
            else
            {
                chunk+=word+" ";

            }
        }
        if (chunk.length>0)
        {
            chunks.push(chunk);
        }

        return chunks;
    }
}