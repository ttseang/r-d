import { GameObjects } from "phaser";
import { Animal } from "../classes/Animal";
import { Bus } from "../classes/Bus";
import { SBubble } from "../classes/SBubble";
import { BubblePosVo } from "../dataObjs/BubblePosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private bus: Bus
    private cat: Animal
    private bg: GameObjects.TileSprite;

    private busStopSign: GameObjects.Image;
    private shelter: GameObjects.Image;

    public speed: number = 10;
    public slowDown: boolean = false;
    public speedUp: boolean = false;


    private animals: Animal[] = [];

    private currentAnimal: Animal | null = null;
    private waitingAnimals: Animal[] = [];
    private swapingAnimals: Animal[] = [];

    public isMoving: boolean = true;
    private sessionIndex: number = 0;

    public bubble: SBubble;
    private bubblePos: BubblePosVo[] = [];

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("bus", "./assets/bus/bus.png");
        this.load.image("wheel", "./assets/bus/wheel.png");
        this.load.image("window", "./assets/bus/window.png");
        this.load.image("stopSign", "./assets/bus/stopsign.png");
        this.load.image("holder", "./assets/holder.jpg");

        this.load.atlas("animals", "./assets/animals.png", "./assets/animals.json ");
        this.load.atlas("bubbles", "./assets/bubbles2.png", "./assets/bubbles2.json");

        this.load.image("busStopSign", "./assets/busStopSign.png");
        this.load.image("shelter", "./assets/shelter.png");

        this.load.image("bg", "./assets/bg.jpg");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);

        this.bg = this.add.tileSprite(0, 0, 0, 0, "bg");

        Align.center(this.bg, this);
        this.bg.y = -this.gh * 0.1;

        //  this.grid.showPos();

        this.bus = new Bus(this);

        Align.scaleToGameW(this.bus, 0.7, this);
        //  Align.center(this.bus,this);

        this.grid.placeAt(8, 15, this.bus);

        this.bus.y = this.gh - this.bus.displayHeight / 2 - this.gh * 0.1;

        //  this.cat = new Animal(this, "pig");
        //this.cat.visible=false;

        // this.grid.placeAt(17, 19.5, this.cat);
        // this.cat.visible = false;
        // this.bus.placeAnimalInWindow(4,this.cat);



        this.busStopSign = this.add.image(0, 0, "busStopSign");
        Align.scaleToGameW(this.busStopSign, 0.05, this);
        this.grid.placeAt(17, 21, this.busStopSign);
        this.busStopSign.y = this.gh - this.busStopSign.displayHeight / 2;

        this.shelter = this.add.image(0, 0, "shelter");
        Align.scaleToGameW(this.shelter, 0.2, this);
        this.grid.placeAt(20, 18, this.shelter);
        this.shelter.y = this.gh - this.shelter.displayHeight / 2;

        //   this.cat.y=this.gh-this.cat.displayHeight/2;

        this.shelter.x = -300;
        this.busStopSign.x = -300;

        //this.addAnimals(2, "pig");

        // this.children.bringToTop(this.cat);

        window['bus'] = this.bus;

        window['scene'] = this;

        setTimeout(() => {
            this.startSlowDown();
        }, 2000);

        this.bubble = new SBubble(this, "", 26, 1);

        this.bus.moveCallback = this.allAboard.bind(this);

        this.input.on("gameobjectdown", this.startDragAnimal.bind(this));
        this.setBubblePos();
        this.showBubble(6, "Testing");
        this.nextAnimals();
    }
    setBubblePos() {
        let windowY:number=9;

        this.bubblePos.push(new BubblePosVo(17, 17, "Any Seat Is Fine", 1, 20));
        this.bubblePos.push(new BubblePosVo(3.5, windowY, "I Want To Change Seats!", 1, 20));
        this.bubblePos.push(new BubblePosVo(5, windowY, "I Want To Change Seats!", 1, 20));
        this.bubblePos.push(new BubblePosVo(7, windowY, "I Want To Change Seats!", 1, 20));
        this.bubblePos.push(new BubblePosVo(8.5, windowY, "I Want To Change Seats too!", 1, 20));
        this.bubblePos.push(new BubblePosVo(11, windowY, "I Want To Change Seats???!", 1, 20));
        this.bubblePos.push(new BubblePosVo(12, windowY, "I Want To Change Seats???!", 1, 20));
    }
    showBubble(index: number, text: string) {

        console.log("INDEX=" + index);

        let bubblePosVo: BubblePosVo = this.bubblePos[index];

        this.bubble.setFrame2(bubblePosVo.bubbleFrame);
        this.bubble.setFontSize(bubblePosVo.fontSize);
        this.bubble.setBubbleText(text, 70);

        this.grid.placeAt(bubblePosVo.x, bubblePosVo.y, this.bubble);

        this.bubble.autoSize();
        this.bubble.visible = false;
    }
    nextAnimals() {

        console.log("next animals");
        this.sessionIndex++;

        switch (this.sessionIndex) {
            case 1:
                this.addAnimals(2, "pig", "any");
                break;

            case 2:
                this.addAnimals(2, "cat", "blue");
                break;


        }
    }
    onBusStop() {
        switch (this.sessionIndex) {
            case 3:
                this.bus.takeOffAll("pig");
                this.askToSwap("cat", "red");
                break;

            case 4:
                this.bubble.visible = false;
                this.bus.takeOffAll("cat");
                break;
        }
    }
    askToSwap(type: string, color: string) {
        let randAnimal: Animal = this.bus.pickOne(type);
        randAnimal.colorPref = color;
        randAnimal.setOriginalPos();
        randAnimal.setInteractive();
        //  randAnimal.alpha = .5;

        window['randAnimal'] = randAnimal;
        if (randAnimal.busWindow) {
            let index: number = randAnimal.busWindow.index + 1;

            this.showBubble(index, "I want to switch to a " + color + " seat!");
        }
        this.swapingAnimals.push(randAnimal);
        this.bubble.visible=true;
    }
    addAnimals(count: number, type: string, colorPref: string) {
        for (let i: number = 0; i < count; i++) {
            let animal: Animal = new Animal(this, type);
            this.grid.placeAt(17 - i * 1.5, 19.5, animal);

            animal.colorPref = colorPref;
            animal.setOriginalPos();
            animal.setInteractive();
            animal.visible = false;

            this.waitingAnimals.push(animal);
            this.animals.push(animal);
        }
        this.showBubble(0, colorPref + " seat please!");
    }
    allAboard() {
        this.nextAnimals();
        this.startMoving();
        setTimeout(() => {
            this.startSlowDown();
        }, 5000);
    }
    startDragAnimal(p: Phaser.Input.Pointer, obj: any) {
        /* console.log(animal);
        if (animal !instanceof Animal)
        {
            return;
        } */
        if (obj == this.bus) {

            return;
        }
        // console.log("DRAG");
        this.bubble.visible = false;

        this.currentAnimal = obj;
        this.children.bringToTop(this.currentAnimal);

        this.input.once("pointerup", this.onUp.bind(this));
        this.input.on("pointermove", this.doDragAnimal.bind(this));
    }
    doDragAnimal(p: Phaser.Input.Pointer) {
        if (this.currentAnimal) {
            this.currentAnimal.x = p.x;
            this.currentAnimal.y = p.y;
        }
    }
    onUp() {
        if (this.currentAnimal) {
            let foundWindow: boolean = this.bus.findNearestWindow(this.currentAnimal);

            if (this.currentAnimal.onBus == false && foundWindow == true) {
                let index: number = this.waitingAnimals.indexOf(this.currentAnimal);
                console.log("waiting index=" + index);

                if (index != -1) {
                    this.bus.busAnimals.push(this.currentAnimal);
                    this.currentAnimal.onBus = true;
                    this.waitingAnimals.splice(index, 1);
                    if (this.waitingAnimals.length == 0) {
                        this.allAboard();
                    }
                }
            }
            else {
                if (this.currentAnimal.onBus == true && foundWindow == true) {
                    let index: number = this.swapingAnimals.indexOf(this.currentAnimal);
                    console.log("swap index=" + index);

                    if (index != -1) {

                        this.swapingAnimals.splice(index, 1);
                        if (this.swapingAnimals.length == 0) {
                            this.allAboard();
                        }
                    }
                }
            }
        }
        this.currentAnimal = null;



        this.input.once("gameobjectdown", this.startDragAnimal.bind(this));
    }
    startSlowDown() {
        this.slowDown = true;
        this.speedUp = false;
        this.busStopSign.x = this.gw + this.speed * 10;
        this.shelter.x = this.busStopSign.x + this.shelter.displayWidth;
    }
    startMoving() {
        this.bubble.visible = false;
        this.isMoving = true;
        this.speed = 0;
        this.slowDown = false;
        this.speedUp = true;
    }
    showWaiting() {
        for (let i: number = 0; i < this.waitingAnimals.length; i++) {
            this.waitingAnimals[i].visible = true;
        }
        if (this.waitingAnimals.length > 0) {
            this.bubble.visible = true;
        }

    }
    update() {
        if (this.isMoving == true) {
            this.bus.moveWheels(this.speed);
            this.bg.tilePositionX += this.speed;

            if (this.shelter.x > -300) {
                this.shelter.x -= this.speed;
            }
            if (this.busStopSign.x > -300) {
                this.busStopSign.x -= this.speed;
            }
            if (this.speedUp == true) {
                this.speed += 0.1;
                if (this.speed > 10) {
                    this.speed = 10;
                    this.speedUp = false;
                }
            }
            if (this.slowDown == true) {
                this.speed -= 0.1;
                if (this.speed < 0.1) {
                    this.speed = 0;
                    this.isMoving = false;
                    this.onBusStop();
                    //this.cat.visible=true;
                    this.showWaiting();
                }
            }
        }
    }
}