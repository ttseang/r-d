import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import { TextUtil } from "../util/TextUtil";

export class SBubble extends GameObjects.Container
{
    private bscene:IBaseScene;
    public text:string;
    public frame:number;
    public scene:Phaser.Scene;
    //
    //
    //
    private back:GameObjects.Sprite;
    private text1:GameObjects.Text;

    private chunkIndex:number=0;

    public chunks:string[]=[];

    public doneCallback:Function=()=>{};
    
    constructor(bscene:IBaseScene,text:string,fontSize:number=26,frame:number=0)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.bscene=bscene;
        this.text=text;
        this.frame=frame;

       

       

        this.back=this.scene.add.sprite(0,0,"bubbles");
        this.add(this.back);
        this.setBubbleFrame(frame);
        //
        //
        //
        let fs:string=fontSize.toString()+"px";
        this.text1=this.scene.add.text(0,0,"",{fontSize:fs,color:"black",fontFamily:"Comic Sans MS",wordWrap:{useAdvancedWrap:true,width:300}}).setOrigin(0.5,0.5);
        this.add(this.text1);
        //
        //
        //
        this.scene.add.existing(this);
        this.setBubbleText(text);
        
    }
    public setFrame2(frame:number)
    {
        this.frame=frame;
        let frameName:string="bubble ("+frame.toString()+").png";
        this.back.setFrame(frameName);
    }
    public setBubbleFrame(frame:number)
    {
        console.log("frame="+frame);
        this.frame=frame;
        let positions:PosVo[]=[];
     //   positions[0]=new PosVo(0,0);
        positions[1]=new PosVo(5,3);
        positions[2]=new PosVo(4,6);
        positions[3]=new PosVo(3,8);
        positions[4]=new PosVo(5,8);
        positions[5]=new PosVo(1.5,7);
        positions[6]=new PosVo(2,7);
        positions[7]=new PosVo(4.5,8);
        positions[8]=new PosVo(2,5.5);
        positions[9]=new PosVo(6,1.5);
        positions[10]=new PosVo(4,1);
        positions[11]=new PosVo(4,1);

        let frameName:string="bubble ("+frame.toString()+").png";
        this.back.setFrame(frameName);

        let xx:number=positions[frame].x;
        let yy:number=positions[frame].y;

        console.log(xx,yy);

        this.bscene.getGrid().placeAt(xx,yy,this);
        
    }
    public setFontSize(fs:number)
    {
        this.text1.setFontSize(fs);
        this.autoSize();
    }
    public setBubbleText(text:string,cs:number=50)
    {
        this.chunks=TextUtil.wordsIntoChunks(text,cs);
        this.chunkIndex=0;
        this.setText();
    }
    private setText()
    {
        this.text1.setText(this.chunks[this.chunkIndex]);
        this.autoSize();
    }
    public nextChunk()
    {
        if (this.chunkIndex<this.chunks.length-1)
        {
            this.chunkIndex++;
            this.setText();
        }
        else
        {
            this.doneCallback();
        }
    }
    public prevChunk()
    {
        if (this.chunkIndex>0)
        {
            this.chunkIndex--;
            this.setText();
        }
    }
    public autoSize()
    {
        let offsetsX:number[]=[0];
        let offsetsY:number[]=[0];
        let scalesX:number[]=[1,1.1,1.5,1.5];
        let scalesY:number[]=[1,2,1.5,1.5];

        offsetsX[1]=0;
        offsetsY[1]=.25;
        scalesX[1]=1.1;
        scalesY[1]=2.5;

        offsetsX[2]=.10;
        offsetsY[2]=0;
        scalesX[2]=1.5;
        scalesY[2]=1.5;

        offsetsX[3]=0;
        offsetsY[3]=-.5;
        scalesX[3]=2;
        scalesY[3]=3;
       
        offsetsX[4]=0;
        offsetsY[4]=-.2;
        scalesX[4]=1.5;
        scalesY[4]=2;

        offsetsX[5]=.2;
        offsetsY[5]=-.1;
        scalesX[5]=2;
        scalesY[5]=1.8;

        offsetsX[6]=.1;
        offsetsY[6]=0;
        scalesX[6]=1.5;
        scalesY[6]=1.8;

        offsetsX[7]=0;
        offsetsY[7]=-.25;
        scalesX[7]=1.5;
        scalesY[7]=1.8;

        offsetsX[8]=.1;
        offsetsY[8]=0;
        scalesX[8]=1.5;
        scalesY[8]=1.5;

        offsetsX[9]=0;
        offsetsY[9]=0;
        scalesX[9]=1.5;
        scalesY[9]=1.8;

        offsetsX[10]=0;
        offsetsY[10]=.1;
        scalesX[10]=1.5;
        scalesY[10]=1.8;

        offsetsX[11]=0;
        offsetsY[11]=.25;
        scalesX[11]=1.5;
        scalesY[11]=2.2;

        this.back.displayHeight=this.text1.displayHeight*scalesY[this.frame];
        this.back.displayWidth=this.text1.displayWidth*scalesX[this.frame];

        let offsetX:number=offsetsX[this.frame]*this.text1.displayWidth;
        this.back.x=offsetX;

        let offsetY=offsetsY[this.frame]*this.text1.displayHeight;
        this.back.y=offsetY;

        console.log(offsetX,offsetY);

    }
}