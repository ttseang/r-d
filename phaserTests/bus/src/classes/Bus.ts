import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { Animal } from "./Animal";
import { BusWindow } from "./BusWindow";

export class Bus extends GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private back: GameObjects.Image;

    private backWheel: GameObjects.Image;
    private frontWheel: GameObjects.Image;
    private stopSign: GameObjects.Image;

    public windows: BusWindow[] = [];
    // public shades:GameObjects.Image[]=[];

    private distX: number = 1000;
    private distY: number = 1000;
    private nearWindow: number = -1;

    public moveCallback: Function = () => { };

    public busAnimals: Animal[] = [];

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());

        this.bscene = bscene;
        this.scene = bscene.getScene();

        this.back = this.scene.add.image(0, 0, "bus");
        this.frontWheel = this.scene.add.image(204, 60, "wheel");
        this.backWheel = this.scene.add.image(-155, 60, "wheel");
        this.stopSign = this.scene.add.image(85, 10, "stopSign");

        this.add(this.back);
        this.add(this.frontWheel);
        this.add(this.backWheel);
        this.add(this.stopSign);

        //  this.setInteractive();

        for (let i: number = 0; i < 6; i++) {

            /* let window:GameObjects.Image=this.scene.add.image(-220+i*60,-40,"window");
            let shade:GameObjects.Image=this.scene.add.image(window.x,window.y,"holder");

            shade.displayHeight=window.displayHeight*1.2;
            shade.displayWidth=window.displayWidth*1.2;
           

            if (i<3)
            {
                shade.setTint(0xff0000);
            }
            else
            {
                shade.setTint(0x3498db);
            }

            this.add(shade);
            this.add(window); */
            let color: string = "red";
            if (i > 2) {
                color = "blue";
            }
            let window: BusWindow = new BusWindow(this.bscene, i, color);
            window.x = -220 + i * 60;
            window.y = -40;
            this.add(window);
            this.windows.push(window);
            // this.shades.push(shade);
        }

        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.scene.add.existing(this);
        //  this.setInteractive();
    }
    placeAnimalInWindow(index: number, obj: Animal) {
        let pos: PosVo = this.getWindowPos(index);
        console.log(pos);
        Align.scaleToGameW(obj, 0.05, this.bscene);

        this.windows[index].animal = obj;
        if (obj.busWindow != null) {
            obj.busWindow.animal = null;
        }
        obj.busWindow = this.windows[index];

        obj.x = this.x + pos.x * this.scaleX;
        obj.y = this.y + pos.y * this.scaleY;
        obj.setOriginalPos();
        //  obj.removeInteractive();
        //  this.moveCallback();
    }
    findNearestWindow(obj: Animal) {


        this.nearWindow = -1;
        this.distX = 1000;
        this.distY = 1000;


        /*   let testSpot:GameObjects.Image=this.scene.add.image(localX,localY,"holder");
         testSpot.setTint(0xff0000);
         Align.scaleToGameW(testSpot,0.05,this.bscene);
         this.add(testSpot);
         return; */

        for (let i: number = 0; i < this.windows.length; i++) {
            let myWindow: BusWindow = this.windows[i];
            let globalX: number = myWindow.getWorldTransformMatrix().tx;
            let globalY: number = myWindow.getWorldTransformMatrix().ty;

            let distX: number = Math.round(Math.abs(globalX - obj.x));
            let distY: number = Math.round(Math.abs(globalY - obj.y));

            // console.log(distX,distY,this.distX,this.distY);

            if (distX <= this.distX && distY <= this.distY) {
                this.distX = distX;
                this.distY = distY;
                this.nearWindow = i;

            }
        }
        //console.log("near="+this.nearWindow);

        if (this.nearWindow != -1) {
            if (this.distY < obj.displayHeight / 2 && this.distX < obj.displayWidth / 2) {

                console.log(this.windows[this.nearWindow].color, obj.colorPref);

                if (this.windows[this.nearWindow].color !== obj.colorPref) {
                    if (obj.colorPref != "any") {
                        obj.snapBack();
                        return;
                    }

                }
                if (this.windows[this.nearWindow].animal != null) {
                    obj.snapBack();
                    return;
                }
                obj.showFace();

                this.placeAnimalInWindow(this.nearWindow, obj);
                return true;
            }

        }
        return false;
    }
    pickOne(type: string) {
        let picked: Animal[] = [];
        for (let i: number = 0; i < this.busAnimals.length; i++) {
            let animal: Animal = this.busAnimals[i];
            if (animal.type == type) {
                picked.push(animal);
            }
        }
        let r: number = Math.floor(Math.random() * picked.length);
        return picked[r];
    }
    takeOffAll(type: string) {
        let keep: Animal[] = [];

        for (let i: number = 0; i < this.busAnimals.length; i++) {
            let animal: Animal = this.busAnimals[i];
            if (animal.type == type) {
                if (animal.busWindow) {
                    animal.busWindow.animal = null;
                    animal.busWindow = null;
                }

                this.remove(animal);

                animal.jumpOff();
            }
            else {
                keep.push(animal);
            }
        }
        this.busAnimals = keep;
    }
    getWindowPos(index: number) {
        let window: BusWindow = this.windows[index];
        return new PosVo(window.x, window.y);
    }
    moveWheels(speed: number) {
        this.backWheel.angle += speed / 2;
        this.frontWheel.angle += speed / 2;

    }
}