import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";
import { BusWindow } from "./BusWindow";

export class Animal extends GameObjects.Sprite
{
    public scene:Phaser.Scene;
    public type:string;
    private bscene:IBaseScene;

    public colorPref:string="any";
    public onBus:boolean=false;
    public canDrag:boolean=true;

    private ox:Number=0;
    private oy:number=0;

    public busWindow:BusWindow | null=null;
    
    constructor(bscene:IBaseScene,type:string)
    {
        super(bscene.getScene(),0,0,"animals",type+"Full.png");
        this.type=type;
        this.bscene=bscene;

        Align.scaleToGameW(this,0.05,bscene);

        this.scene=bscene.getScene();
        this.scene.add.existing(this);
        this.setInteractive();
        
    }
    public setOriginalPos()
    {
        this.ox=this.x;
        this.oy=this.y;
    }
    public jumpOff()
    {
        Align.scaleToGameW(this,0.1,this.bscene);
        this.setFrame(this.type+"Full.png");
        this.scene.tweens.add({targets:this,y:this.bscene.getH()*.95,duration:500,onComplete:()=>{ this.walkOff();}})
    }
    public walkOff()
    {
        this.scene.tweens.add({targets:this,x:-100,duration:500,onComplete:()=>{ this.destroy();}})
    }
    public snapBack()
    {
        this.scene.add.tween({targets:[this],duration:1000,x:this.ox,y:this.oy});
    }
    public showFace()
    {
        this.setFrame(this.type+".png");
    }
}