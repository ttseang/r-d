import { GameObjects } from "phaser"
import IBaseScene from "../interfaces/IBaseScene";
import { Animal } from "./Animal";

export class BusWindow extends GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    public color:string;

    public animal:Animal | null=null;
    public index:number;

    constructor(bscene:IBaseScene,index:number,color:string)
    {
        super(bscene.getScene());

        this.index=index;
        this.scene=bscene.getScene();
        this.bscene=bscene;
        this.color=color;
        //
        //
        //
        let back:GameObjects.Image=this.scene.add.image(0,0,"window");
        let shade:GameObjects.Image=this.scene.add.image(0,0,"holder");
        //
        //
        //
        shade.displayHeight=back.displayHeight*1.2;
        shade.displayWidth=back.displayWidth*1.2;
        //
        //
        //
        this.add(shade);
        this.add(back);      
        //
        //
        //
        let tintColor:number=0xff0000;

        if (color=="blue")
        {
            tintColor=0x3498db;
        }

        shade.setTint(tintColor);

        this.scene.add.existing(this);
    }
}