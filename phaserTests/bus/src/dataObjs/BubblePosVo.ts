export class BubblePosVo
{
    public x:number;
    public y:number;
    public text:string;
    public bubbleFrame:number;
    public fontSize:number;

    constructor(x:number,y:number,text:string,bubbleFrame:number,fontSize:number)
    {
        this.x=x;
        this.y=y;
        this.text=text;
        this.bubbleFrame=bubbleFrame;
        this.fontSize=fontSize;
    }
}