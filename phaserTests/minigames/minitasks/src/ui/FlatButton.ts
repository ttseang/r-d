import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class FlatButton extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private textStyle: string;
    public onDown: Function;
    public onUp:Function=()=>{};
    private back:Phaser.GameObjects.Image;

    public text1: Phaser.GameObjects.Text
    public extraData:string="";
    public index:number=0;
    constructor(bscene: IBaseScene, textStyle: string, key: string, text: string,callback:Function) {
        super(bscene.getScene());
        
        this.back = this.scene.add.image(0, 0, key);
        this.back.displayWidth=bscene.getW()*0.4;
        this.add(this.back);

        this.text1 = this.scene.add.text(0, 0, text).setOrigin(0.5, 0.5);
        this.add(this.text1);

        this.scene.add.existing(this);
        this.back.setInteractive();
        this.onDown=callback;

        this.back.on('pointerdown', this.clickMe.bind(this));
        
        
    }
    setOnUp(callback:Function)
    {
        this.onUp=callback;
        this.back.on('pointerup', this.release.bind(this));
    }
    clickMe() {
        if (this.onDown)
        {
            this.onDown(this);
        }      
    }
    release()
    {
        if (this.onUp)
        {
            this.onUp();
        }
    }
    setText(text: string) {
        this.text1.setText(text);
    }
}