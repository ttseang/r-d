import { SliderSquare } from "../classes/SliderSquare";
import { FlatButton } from "../ui/FlatButton";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneSlider extends BaseScene {
    private last: SliderSquare;
    private clickLock: boolean = false;
    private block: Phaser.GameObjects.Container;
    private squareArray: SliderSquare[] = [];
    private puzzleSize: number = 4;
    private image: Phaser.GameObjects.Image;
    private button1: FlatButton;

    constructor() {
        super("SceneSlider");
    }
    preload() {

        this.load.image("button", "./assets/ui/buttons/1/3.png");
        this.load.image("preview", "./assets/icecream.jpg");
        if (this.textures.checkKey("icecream") == true) {
            this.load.spritesheet("icecream", "./assets/icecream.jpg", { frameWidth: 500 / this.puzzleSize, frameHeight: 500 / this.puzzleSize });
        }
    }
    create() {
        super.create();
       
        this.makeGrid(11, 11);
        //this.grid.showNumbers();
        this.squareArray=[];
       

        this.block = this.add.container();
        this.makeBoard();
    }
    makeBoard()
    {
        console.log("make board");
        let c: number = 0;
        let pieceWidth: number = 0;
        let pieceHeight: number = 0;

        for (let i: number = 0; i < this.puzzleSize; i++) {
            for (let j: number = 0; j < this.puzzleSize; j++) {
                let p: SliderSquare = new SliderSquare(this, "icecream", c);
                p.xx = j;
                p.yy = i;
                //
                //
                //
                p.ox = p.xx;
                p.oy = p.yy;
                //
                //
                //
                p.callback = this.clickP.bind(this);

                let scale: number = 0.8 / this.puzzleSize;

                Align.scaleToGameW(p, scale, this);
                p.x = j * p.displayWidth;
                p.y = i * p.displayHeight;
                //
                //
                //
                this.block.add(p);
                this.squareArray.push(p);

                // this.grid.placeAt(3+j,3+i,p);
                this.last = p;
                c++;

                pieceWidth = p.displayWidth;
                pieceHeight = p.displayHeight;
            }
        }
        this.block.setSize(pieceWidth * this.puzzleSize, pieceHeight * this.puzzleSize);
        Align.center2(this.block, this);

        this.last.setTint(0x00000);

        this.mixUp();

        this.image = this.add.image(this.block.x, this.block.y, "preview").setOrigin(0, 0);
        this.image.displayWidth = this.block.displayWidth;
        this.image.displayHeight = this.block.displayHeight;
        this.image.visible = false;


        this.button1 = new FlatButton(this, "WHITE", "button", "show", this.showImage.bind(this));
        this.grid.placeAtIndex(104, this.button1);
        this.button1.setOnUp(this.hideImage.bind(this));

        let buttonSkip: FlatButton = new FlatButton(this, "WHITE", "button", "skip", this.exitGame.bind(this));
        this.grid.placeAtIndex(16, buttonSkip);
    }
    showImage() {
        this.image.visible = true;
    }
    hideImage() {
        this.image.visible = false;
    }
    mixUp() {
        for (let i: number = 0; i < 20; i++) {
            let index1: number = Phaser.Math.Between(0, this.squareArray.length - 1);
            let index2: number = Phaser.Math.Between(0, this.squareArray.length - 1);

            let square1: SliderSquare = this.squareArray[index1];
            let square2: SliderSquare = this.squareArray[index2];
            //
            //
            //
            let tx: number = square1.x;
            let ty: number = square1.y;
           
            //
            //
            //
            let txx: number = square1.xx;
            let tyy: number = square1.yy;
            //
            //
            //
            square1.xx = square2.xx;
            square1.yy = square2.yy;
            //
            //
            //
            square1.x = square2.x;
            square1.y = square2.y;
            //
            //
            //
            square2.x = tx;
            square2.y = ty;
            //
            //
            //
            square2.xx = txx;
            square2.yy = tyy;

            
            square1.visible=true;
            square2.visible=true;
        }
    }
    clickP(p: SliderSquare) {
        if (this.clickLock === true) {
            return;
        }
        if (p === this.last) {
            return;
        }
        if (p.xx === this.last.xx || p.yy === this.last.yy) {

            let diffX: number = Math.abs(p.xx - this.last.xx);
            let diffY: number = Math.abs(p.yy - this.last.yy);

            if (diffX > 1 || diffY > 1) {
                return;
            }
            this.clickLock = true;
            //
            //
            //
            //  p.alpha = 0.5;

            let lastX: number = this.last.xx;
            let lastY: number = this.last.yy;
            //
            //
            //
            this.last.xx = p.xx;
            this.last.yy = p.yy;
            //
            //
            //
            p.xx = lastX;
            p.yy = lastY;
            //
            //
            //
            let tx1: number = this.last.x;
            let ty1: number = this.last.y;
            //
            //
            //
            let tx2: number = p.x;
            let ty2: number = p.y;

            this.tweens.add({ targets: this.last, duration: 100, y: ty2, x: tx2 });
            this.tweens.add({ targets: p, duration: 100, y: ty1, x: tx1, onComplete: this.moveDone.bind(this) });
        }
    }
    checkWin() {
        for (let i: number = 0; i < this.squareArray.length; i++) {
            let square: SliderSquare = this.squareArray[i];
            if (square.xx !== square.ox || square.yy !== square.oy) {
                return false;
            }
        }
        return true;
    }
    moveDone() {
        if (this.checkWin() === true) {
            console.log("win");
            setTimeout(this.exitGame.bind(this), 4000);
        }
        this.clickLock = false;
    }
    exitGame() {
        this.scene.start("SceneSelect");
    }
}