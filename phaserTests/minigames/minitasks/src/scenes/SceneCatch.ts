import {
    BaseScene
} from "./BaseScene";

 import {
    ColorBurst
} from "../effects/colorBurst";

import { Physics } from "phaser";
import { FallingObj } from "../classes/FallingObj";
import Align from "../util/align";
import { Clock } from "../classes/Clock";
import { FlatButton } from "../ui/FlatButton";
//import { Clock } from "../classes/Clock";
//
//
//
export class SceneCatch extends BaseScene {
    private objGroup:Physics.Arcade.Group;
    private timer1:Phaser.Time.TimerEvent;
    private clock:Clock;

    constructor() {
        super('SceneCatch');
    }
    preload() {
        ColorBurst.preload(this,"./assets/effects/colorStars.png");
        this.load.image("button", "./assets/ui/buttons/1/3.png");
        this.load.audio("coinsound","./assets/games/catch/audio/coinSound.wav");
        let pngArray = ['dog','cat'];
        for (let i = 0; i < pngArray.length; i++) {
            let imagePath="./assets/games/catch/"+pngArray[i]+".png";
            console.log(imagePath);

            this.load.image(pngArray[i], imagePath);
        }
        this.load.image("night","./assets/games/catch/night.jpg");
    }
    create() {
        //set up the base scene
        super.create();
     //   this.setBackground('night');
        //set the grid for the scene
        this.makeGrid(11, 11);
        this.objGroup = this.physics.add.group();
        //show numbers for layout and debugging 
        //
       //  this.grid.showNumbers();
        //
        //
        //
        this.makeUi();
        this.dropBomb();
        this.timer1=this.time.addEvent({
            delay: 500,
            callback: this.dropBomb.bind(this),
            loop: true
        });
        
       this.clock.startClock();

       let buttonSkip:FlatButton=new FlatButton(this,"WHITE","button","skip",this.gameOver.bind(this));
       this.grid.placeAtIndex(104,buttonSkip);

       let dtext:Phaser.GameObjects.Text=this.add.text(0,0,"Click only the cats!").setOrigin(0.5,0.5);
       this.grid.placeAtIndex(5,dtext);
    }
    dropBomb() {
        let pos = Phaser.Math.Between(1, 9);
        let type = Phaser.Math.Between(0, 1);
        let keyArray = ['dog', 'cat'];
        let key = keyArray[type];        
       
        let obj:FallingObj=new FallingObj(this,key);
        obj.callback=this.clickObj.bind(this);

        Align.scaleToGameW(obj,0.15,this);
        this.grid.placeAtIndex(pos,obj);     
        obj.type = key;
        obj.setInteractive();
        this.objGroup.add(obj);
        obj.setVelocityY(200);
      
    }
    clickObj(obj:FallingObj) {
        /* if (obj.type2 != "falling") {
            return;
        } */
        
        let colorStars:ColorBurst=new ColorBurst(this,obj.x,obj.y);
       
        if (obj.type == "cat") {
            this.playSound("coinsound");
            //this.emitter.emit("UP_POINTS", 1);
        }
        if (obj.type == "dog") {
            //this.mm.playSound("boom");
            //this.emitter.emit("STOP_TIME");
            this.gameOver();
            return;
        }
        obj.destroy();
    }
    makeUi() {
        //super.makeSoundPanel();
        //super.makeGear();
        this.clock=new Clock(this,20);
        this.clock.callback=this.gameOver.bind(this);
        this.grid.placeAtIndex(16,this.clock);
    }
    gameOver() {
        this.timer1.remove();
        console.log("time is up");
       
        this.scene.start("SceneSelect");
    }
    update() {
       /*  this.objGroup.children.entries.forEach(function(child) {
            if (child) {
                if (child.y > this.gh) {
                    child.destroy();
                }
            }
        }.bind(this)); */
    }
}