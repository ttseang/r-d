import { Popper } from "../classes/Popper";
import { Sparks } from "../effects/sparks";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class ScenePoppers extends BaseScene {
    
    constructor() {
        super("ScenePoppers");
    }
    preload() {
      //  this.load.image("face", "./assets/face.png");
        this.load.image("sundae","./assets/sundae.png");
        this.load.audio("pop","./assets/pop.mp3");
    }
    create() {
        super.create();
        this.makeGrid(11,11);
        
        //this.bonusText.visible=false;
        console.log("create!");
        //let face:Phaser.GameObjects.Image=this.add.image(100, 200, "face");
        let popper:Popper=new Popper(this,"sundae","twirl",this.doPopper.bind(this));
        Align.scaleToGameW(popper,0.1,this);

        this.grid.showNumbers();
        this.grid.placeAtIndex(60,popper);
    }
    doPopper(p:Popper)
    {
        console.log(p.type2);

        if (p.type2==="flyup")
        {
            this.tweens.add({targets: p,duration: 500,y:-1000});
        }
        if (p.type2==="flydown")
        {
            this.tweens.add({targets: p,duration: 500,y:2000});
        }
        if (p.type2==="fade")
        {
            this.tweens.add({targets: p,duration: 500,alpha:0});
        }
        if (p.type2==="twirl")
        {
            this.tweens.add({targets: p,duration: 500,angle:360*5});
        }
        if (p.type2==="pop")
        {
            let s:Sparks=new Sparks(this,p.x,p.y);
            let pop:Phaser.Sound.BaseSound=this.sound.add("pop");
            pop.play();

            p.destroy();
        }
        if (p.type2==="randplace")
        {
            let xx:number=Phaser.Math.Between(this.gw*0.1,this.gw*0.9);
            let yy:number=Phaser.Math.Between(this.gh*0.1,this.gh*0.9);
          //  p.x=xx;
           // p.y=yy;
            this.tweens.add({targets: p,duration: 100,x:xx,y:yy});
            p.clickCount++;
            if (p.clickCount==4)
            {
                p.type2="pop";
            }
        }
       /*  if (p.type2==="bonusfact")
        {
            this.bonusText.x=p.x;
            this.bonusText.y=p.y;
            this.bonusText.visible=true;
        } */
    }
}