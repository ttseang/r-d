//import { PageModel } from "../mc/pageModel";
import { FlatButton } from "../ui/FlatButton";
import { BaseScene } from "./BaseScene";

export class SceneSelect extends BaseScene
{
   // private model:PageModel=PageModel.getInstance();

    constructor()
    {
        super("SceneSelect");
    }
    preload()
    {
        console.log("preload");
        this.load.image("button", "./assets/ui/buttons/1/3.png");
    }
    create()
    {
        super.create();
        console.log("create");
        this.makeGrid(11,11);
       // this.grid.showNumbers();

         let buttonArray:string[]=['Slider','Catch'];

        for(let i:number=0;i<buttonArray.length;i++)
        {
            let button:FlatButton=new FlatButton(this,"WHITE","button",buttonArray[i],this.selectGame.bind(this));
            button.index=i;
            this.grid.placeAt(5,2+i*2,button);
        } 
    }
    selectGame(button:FlatButton)
    {
     
       switch(button.index)
       {
           case 0:

           this.scene.start("SceneSlider");

           break;

           case 1:
           this.scene.start("SceneCatch");

           break;

       }
    }
}