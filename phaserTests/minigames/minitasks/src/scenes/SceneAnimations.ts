import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneAnimations extends BaseScene {
    constructor() {
        super("SceneAnimations");
    }
    preload() {
       // this.load.image("face", "./assets/face.png");
       this.load.spritesheet("snow","./assets/effects/snow.png",{frameWidth:759,frameHeight:375});
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        console.log("create!");

       // let frames=this.anims.generateFrameNames("snow");
         let anim=this.anims.create({
            key: 'fall',
            frames: [
                { key: 'snow',frame:1 },
                { key: 'snow',frame:2 },
                { key: 'snow',frame:3 },
                { key: 'snow',frame:4 },
                { key: 'snow',frame:5 },
                { key: 'snow',frame:6 },
                { key: 'snow',frame:7 },
                { key: 'snow',frame:8 },
            ],
            frameRate: 4,
            repeat: -1
        });
        let snow:Phaser.GameObjects.Sprite=this.add.sprite(0,0,"snow").setOrigin(0,0);
        Align.scaleToGameW(snow,1,this);
        snow.play("fall");
        /* let face:Phaser.GameObjects.Image=this.add.image(100, 200, "face");

        Align.scaleToGameW(face,0.25,this);

        this.grid.showNumbers();
        this.grid.placeAtIndex(60,face); */
    }
}