import { IBaseScene } from "../interfaces/IBaseScene";
import { TextStyles } from "../ui/textStyles";
import Align from "../util/align";

import { AlignGrid } from "../util/alignGrid";

/**
 * The base scene includes extra information
 * as well as the standard scene
 * passed to other classes as the IBaseScene interface
 */
export class BaseScene extends Phaser.Scene implements IBaseScene {
  public gw: number;
  public gh: number;
  //private graphics!: Phaser.GameObjects.Graphics;
  //align grid
  public grid!: AlignGrid;
  /**
   * coordinates from align grid
   */
  public ch: number = 0;
  public cw: number = 0;
  public cd: number = 0;
  protected textStyles:TextStyles;

  constructor(sceneName: string) {
    super(sceneName);



  }
  /**
   * 
   * @returns alginGrid
   */
  getGrid(): AlignGrid {
    return this.grid;
  }
  /**
   * overridden in scene class
   */
  create() {
    this.gw = this.game.config.width as number;
    this.gh = this.game.config.height as number;
    this.textStyles=TextStyles.getInstance(this.gw);
  }
  setBackground(key:string)
  {
     let bg:Phaser.GameObjects.Image=this.add.image(0,0,key);
     Align.scaleToGameW(bg,1,this);
     Align.center(bg,this);
  }
  /**
   * make the align grid
   * @param r rows
   * @param c columns
   */
  makeGrid(r: number = 11, c: number = 11) {
    this.grid = new AlignGrid(this, r, c);
    this.ch = this.grid.ch;
    this.cw = this.grid.cw;
    this.cd = this.grid.cd;
    // this.grid.showNumbers();
  }
  placePhysicImage(key: string, pos: number, scale: number): Phaser.Physics.Arcade.Sprite {
    let s2: Phaser.Physics.Arcade.Sprite = this.physics.add.sprite(0, 0, key);
    Align.scaleToGameW(s2, scale, this);
    this.grid.placeAtIndex(pos, s2);
    return s2;
  }
  placeImage(key: string, pos: number, scale: number) {

    let s: Phaser.GameObjects.Sprite = this.add.sprite(0, 0, key);
    console.log(scale);
    Align.scaleToGameW(s, scale, this);
    this.grid.placeAtIndex(pos, s);
    return s;
  }
  //
    //place text on the stage and style it
    //
    placeText(text:string, pos:number, style:string) {
      let textStyle = this.textStyles.getStyle(style);
      console.log(textStyle);
      
      let textObj:Phaser.GameObjects.Text=this.add.text(0,0,text,textStyle.style).setOrigin(0.5,0.5);
     /*  let textObj = new TextObj({
          scene: this,
          text: text          
      }); */
      this.grid.placeAtIndex(pos, textObj);
      return textObj;
  }
  /**
   * 
   * @returns the real scene
   */
  public getScene(): Phaser.Scene {
    return this;
  }
  /**
   * 
   * @returns the games width
   */
  public getW(): number {
    return this.gw;
  }
  /**
   * 
   * @returns the game height
   */
  public getH(): number {
    return this.gh;
  }

  public playSound(key:string)
  {
    let sound:Phaser.Sound.BaseSound=this.sound.add(key);
    sound.play();
  }
}
