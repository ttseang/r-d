//import Phaser = require("phaser");
//import Phaser from 'phaser';
import { SceneAnimations } from './scenes/SceneAnimations';
import { SceneCatch } from './scenes/SceneCatch';
import { ScenePoppers } from './scenes/ScenePoppers';
import { SceneSelect } from './scenes/SceneSelect';
import { SceneSlider } from "./scenes/SceneSlider";

let isMobile = navigator.userAgent.indexOf("Mobile");
if (isMobile == -1) {
    isMobile = navigator.userAgent.indexOf("Tablet");
}
let w = 480;
let h = 640;
//
//
if (isMobile != -1) {
    w = window.innerWidth;
    h = window.innerHeight;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    parent: 'phaser-game',
    physics: { default: 'arcade', arcade: { debug: false } },
    scene: [SceneSelect,ScenePoppers, SceneSlider,SceneCatch]
};

new Phaser.Game(config);