import IBaseScene from "../interfaces/IBaseScene";

export class FallingObj extends Phaser.Physics.Arcade.Sprite
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    public type:string="";
    public xx:number=0;
    public yy:number=0;
    public ox:number=0;
    public oy:number=0;
    
    public callback:Function;

    constructor(bscene:IBaseScene,key:string)
    {
        super(bscene.getScene(),0,0,key,0);
        this.setOrigin(0,0);
        this.scene=bscene.getScene();

        this.setInteractive();
        
        this.scene.add.existing(this);

        this.on('pointerdown',()=>{this.callback(this)});
    }
}