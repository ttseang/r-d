import {
    TextStyles
} from "../ui/textStyles";

import IBaseScene from "../interfaces/IBaseScene";
//
//
//
export class Clock extends Phaser.GameObjects.Container {
    public scene:Phaser.Scene;
    public secs:number=0;
    public callback:Function=()=>{};
    private resetSecs:number=0;
    private text1:Phaser.GameObjects.Text;
    private timer1:Phaser.Time.TimerEvent;
    private useOnlySecs:boolean=true;

    constructor(scene:IBaseScene,secs:number,text:string="Time:",useOnlySecs:boolean=true) {
        super(scene.getScene());
        this.scene = scene.getScene();
        
        this.useOnlySecs=useOnlySecs;

        this.secs=secs;
        
     //   this.emitter = EventDispatcher.getInstance();
        //
        //
        //
        this.resetSecs = this.secs;
        //
        //
        //
        this.text1 = this.scene.add.text(0,0,text,{fontSize:'30px'}).setOrigin(0.5,0.5);
        this.add(this.text1);
        this.setText();
        this.setSize(this.text1.displayWidth, this.text1.displayHeight);
        //
        //
        //
        this.scene.add.existing(this);
       /*  this.emitter.on("SET_TIME", this.setClock.bind(this));
        this.emitter.on("ADD_TIME", this.addTime.bind(this));
        this.emitter.on("STOP_TIME", this.stopClock.bind(this));
        this.emitter.on("START_TIME", this.startClock.bind(this));
        this.emitter.on("RESET_TIME", this.resetClock.bind(this)); */
    }
    startClock() {
        this.timer1 = this.scene.time.addEvent({
            delay: 1000,
            callback: this.tick,
            callbackScope: this,
            loop: true
        });
    }
    stopClock() {
        this.timer1.remove();
    }
    setClock(params) {
        this.secs = params;
        this.setText();
    }
    addTime(params) {
        this.secs += params;
    }
    resetClock() {
        this.secs = this.resetSecs;
    }
    tick() {
        this.secs--;
        if (this.secs == 0) {
            // this.stopClock();
            if (this.callback) {
                this.callback();
            }
          
        }
        this.setText();
    }
    setText2() {
        var mins = Math.floor(this.secs / 60);
        var secs = this.secs - (mins * 60);
        let secString:string = this.leadingZeros(secs);
       // let minString:string = this.leadingZeros(mins);
        this.text1.setText(secString + "s");
    }
    setText() {
        if (this.useOnlySecs == true) {
            this.setText2();
            return;
        }
        let mins:number = Math.floor(this.secs / 60);
        let secs:number = this.secs - (mins * 60);
        let secString:string = this.leadingZeros(secs);
        let minString:string = this.leadingZeros(mins);
        this.text1.setText(minString + ":" + secString);
    }
    leadingZeros(num:number) {
        if (num < 10) {
            return "0" + num.toString();
        }
        return num.toString();
    }
}