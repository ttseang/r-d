import IBaseScene from "../interfaces/IBaseScene";

export class ColorBurst {
    private scene: Phaser.Scene;
    private maxDist: number;


    constructor(bscene: IBaseScene, x: number = 0, y: number = 0, size: number = 5, count: number = 25, dist: number = 150, duration: number = 1000, maxDist: number = 300, color: number = 0xffffff)
    {
        this.scene = bscene.getScene();
        
        for (var i = 0; i < count; i++) {
            var star = this.scene.add.sprite(x, y, "effectColorStars");
            //
            //
            //
            var f = Phaser.Math.Between(0, 14);
            star.setFrame(f);
            star.setOrigin(0.5, 0.5);
            var r = Phaser.Math.Between(50, maxDist);
            var s = Phaser.Math.Between(1, 100) / 100;
            star.scaleX = s;
            star.scaleY = s;
            var angle = i * (360 / count);
            var tx = x + r * Math.cos(angle);
            var ty = y + r * Math.sin(angle);
           
            //  star.x=tx;
            // star.y=ty;
            this.scene.tweens.add({
                targets: star,
                duration: duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone.bind(this)
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    static preload(scene: Phaser.Scene, path: string) {
        scene.load.spritesheet('effectColorStars', path, {
            frameWidth: 26,
            frameHeight: 26
        });
    }
}