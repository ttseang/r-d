var WebFontConfig = {
    google: {
        families: ["Roboto", "Flamenco", "Indie Flower"]
    }
};
(function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
class MediaManager {
    constructor() {
        this.soundArray = [];
        eventDispatcher.add(this.gotEvent, this);
    }
    gotEvent(call, params) {
        switch (call) {
            case G.PLAY_SOUND:
                this.playSound(params);
                break;
            case G.MUSIC_STAT_CHANGED:
                this.updateMusic();
                break;
        }
    }
    addSound(key) {
        this.soundArray[key] = game.add.audio(key);
    }
    playSound(key) {
        if (model.soundOn == true) {
            this.soundArray[key].play();
        }
    }
    setBackgroundMusic(key) {
        this.musicPlaying = false;
        if (this.backgroundMusic) {
            this.backgroundMusic.stop();
        }
        this.backgroundMusic = game.add.audio(key);
        this.backgroundMusic.volume = .5;
        this.backgroundMusic.loop = true;
        this.updateMusic();
    }
    updateMusic() {
        if (model.musicOn == true) {
            if (this.musicPlaying == false) {
                this.musicPlaying = true;
                if (this.backgroundMusic) {
                    this.backgroundMusic.play();
                }
            }
        } else {
            this.musicPlaying = false;
            if (this.backgroundMusic) {
                this.backgroundMusic.stop();
            }
        }
    }
}
class Align {
    static center(obj) {
        obj.x = game.width / 2;
        obj.y = game.height / 2;
    }
    static centerH(obj) {
        obj.x = game.width / 2;
    }
    static centerHGroup(obj) {
        obj.x = game.width / 2 - obj.width / 2;
    }
    static centerV(obj) {
        obj.y = game.height / 2;
    }
    static centerVGroup(obj) {
        obj.y = game.height / 2 - obj.height / 2;
    }
    static centerGroup(obj) {
        obj.x = game.width / 2 - obj.width / 2;
        obj.y = game.height / 2 - obj.height / 2;
    }
    static centerGroup2(obj, obj2) {
        obj.x = obj2.width / 2 - obj.width / 2;
        obj.y = obj2.height / 2 - obj.height / 2;
    }
    static centerGroup3(obj) {
        obj.x = game.width / 2 - obj.width / 2 + obj.getChildAt(0).width / 2;
        obj.y = game.height / 2 - obj.height / 2 + obj.getChildAt(0).height / 2;
    }
    static alignToBottom(obj2, obj, offset = 0) {
        obj.y = obj2.y + obj2.height - obj.height / 2;
        obj.y += offset;
    }
    static fromRight(obj2, obj, percent, offset = 0) {
        obj.x = obj2.width - (game.width * percent);
        obj.x -= offset;
        //obj.x -= obj.width / 2;
    }
    static fromLeft(obj2, obj, percent, offset = 0) {
        obj.x = game.width * percent;
        obj.x += offset;
    }
    static fromCenter(obj2, obj, percent) {
        obj.x = obj2.width / 2 - (game.width * percent);
        obj.x -= obj.width / 2;
    }
    static getScaleToGameW(obj) {
        //console.log("per=" + obj.width / game.width);
    }
    static scaleToGameW(obj, percent) {
        obj.width = game.width * percent;
        obj.scale.y = obj.scale.x;
    }
    static getScaleToGameH(obj) {
        //console.log("per=" + obj.height / game.height);
    }
    static scaleToGameH(obj, percent) {
        obj.height = game.height * percent;
        obj.scale.x = obj.scale.y;
    }
    static fromObjY(obj, obj2, percent) {
        obj.y = obj2.y + (game.height * percent);
    }
    static fromObjX(obj, obj2, percent) {
        obj.x = obj2.x + (game.width * percent);
    }
    static getDif(obj1, obj2) {
        var difX = (obj1.x - obj2.x) / game.width;
        var difY = (obj1.y - obj2.y) / game.height;
        //console.log("difX=" + difX);
        //console.log("difY=" + difY);
    }
    static distributeH(hh, smalls, buffer = 0) {
        var len = smalls.length;
        var mod = hh / (len + 1);
        //
        //
        for (var i = 0; i < len; i++) {
            smalls[i].y = (i * mod) + buffer;
        }
    }
    static distributeW(ww, smalls, buffer = 0) {
        var len = smalls.length;
        var mod = ww / (len + 1);
        //
        //
        for (var i = 0; i < len; i++) {
            smalls[i].x = (i * mod) + buffer;
        }
    }
}
class AlignGrid extends Phaser.Group {
    constructor(cols = 3, rows = 3, par = null) {
        super(game);
        if (par == null) {
            par = game;
        }
        this.cw = par.width / cols;
        this.ch = par.height / rows;
        this.par = par;
        this.rows = rows;
        this.cols = cols;
    }
    placeAt(xx, yy, obj) {
        var x2 = this.cw * xx + this.cw / 2;
        var y2 = this.ch * yy + this.ch / 2;
        obj.x = x2;
        obj.y = y2;
    }
    placeAtIndex(index, obj) {
        var yy = Math.floor(index / this.cols);
        var xx = index - (yy * this.cols);
        this.placeAt(xx, yy, obj);
    }
    show() {
        this.graphics = game.add.graphics();
        this.graphics.lineStyle(4, 0xff0000, 1);
        //
        //
        for (var i = 0; i < this.par.width; i += this.cw) {
            //console.log(i);
            this.graphics.moveTo(i, 0);
            this.graphics.lineTo(i, this.par.height);
        }
        for (var i = 0; i < this.par.height; i += this.ch) {
            this.graphics.moveTo(0, i);
            this.graphics.lineTo(this.par.width, i);
        }
        this.add(this.graphics);
    }
    showNumbers() {
        this.show();
        var n = 0;
        for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.cols; j++) {
                var numText = game.add.text(0, 0, n);
                numText.anchor.set(0.5, 0.5);
                numText.fill = "#ff0000";
                this.add(numText);
                this.placeAt(j, i, numText);
                n++;
            }
        }
    }
}
class SoundButtons extends Phaser.Group {
    constructor() {
        super(game);
        //
        //
        this.soundIcon = new ImageToggle("soundOn", "soundOff", G.TOGGLE_SOUND);
        // this.soundIcon.scale.set(0.5, 0.5);
        Align.scaleToGameW(this.soundIcon, 0.109375);
        this.soundIcon.x = this.soundIcon.width / 2;
        this.soundIcon.y = this.soundIcon.height / 2;
        //
        //
        //
        this.musicIcon = new ImageToggle("musicOn", "musicOff", G.TOGGLE_MUSIC);
        Align.scaleToGameW(this.musicIcon, 0.109375);
        //this.musicIcon.scale.set(0.5, 0.5);
        this.musicIcon.x = game.width - this.musicIcon.width / 2;
        this.musicIcon.y = this.musicIcon.height / 2;
        this.add(this.musicIcon);
        this.add(this.soundIcon);
        this.soundIcon.setTo(model.soundOn);
        this.musicIcon.setTo(model.musicOn);
        //Align.getScaleToGameW(this.musicIcon);
        //Align.scaleToGameW(this.soundIcon, 0.109375);
    }
}
class ImageToggle extends Phaser.Group {
    constructor(onImage, offImage, event) {
        super(game);
        this.event = event;
        this.onSprite = game.add.image(0, 0, onImage);
        this.offSprite = game.add.image(0, 0, offImage);
        this.onSprite.anchor.set(0.5, 0.5);
        this.offSprite.anchor.set(0.5, 0.5);
        this.offSprite.visible = false;
        this.onSprite.inputEnabled = true;
        this.offSprite.inputEnabled = true;
        this.offSprite.events.onInputUp.add(this.turnOn, this);
        this.onSprite.events.onInputUp.add(this.turnOff, this);
        //
        //
        this.offSprite.events.onInputDown.add(this.onDown, this);
        this.onSprite.events.onInputDown.add(this.onDown, this);
        this.add(this.onSprite);
        this.add(this.offSprite);
    }
    onDown() {
        this.onSprite.y = -5;
        this.offSprite.y = -5;
    }
    onUp() {
        this.onSprite.y = 0;
        this.offSprite.y = 0;
    }
    turnOn() {
        this.setTo(true);
        this.onUp();
    }
    turnOff() {
        this.setTo(false);
        this.onUp();
    }
    setTo(val) {
        this._val = val;
        this.offSprite.visible = !val;
        this.onSprite.visible = val;
        if (this.event) {
            eventDispatcher.dispatch(this.event, val);
        }
    }
    getVal() {
        return _val;
    }
}
class CustomTextButton extends Phaser.Group {
    constructor(text = 'button', key = "defaultButton", event = null, params = null, buttonSize = 4, size = -1, textColor = -1) {
        super(game);
        this.key = key;
        this.params = params;
        if (textColor == -1) {
            textColor = model.defaultButtonTextColor;
        }
        if (size == -1) {
            size = model.buttonFontSize;
        }
        //
        //
        //
        this.text = text;
        this.event = event;
        //
        //
        //
        //
        this.buttonBack = game.add.sprite(0, 0, this.key);
        this.buttonBack.anchor.set(0.5, 0.5);
        this.add(this.buttonBack);
        //
        //
        this.textField = game.add.text(0, 0, text);
        this.textField.anchor.set(0.5, 0.5);
        this.textField.fontSize = size;
        this.textField.fill = textColor;
        if (model.buttonFont != "none") {
            this.textField.font = model.buttonFont;
        }
        this.add(this.textField);
        this.buttonBack.inputEnabled = true;
        this.buttonBack.events.onInputUp.add(this.onPressed, this);
        this.buttonBack.events.onInputDown.add(this.onDown, this);
        this.width = buttonSize / 10 * game.width;
        this.scale.y = this.scale.x;
        //
        //
        //  this.textField.x=this.textField.width/2;
        // this.textField.y=this.buttonBack.height/2-this.textField.height/2;
    }
    setPos(x, y) {
        this.x = x;
        this.y = y;
    }
    getBack() {
        return this.buttonBack;
    }
    onPressed() {
        this.buttonBack.y = 0;
        if (this.event) {
            eventDispatcher.dispatch(this.event, this.params);
        }
    }
    onDown() {
        this.buttonBack.y = -5;
    }
    setCallBack(callback) {
        this.callback = callback;
    }
    setTextSize(size) {
        this.textField.fontSize = size + "px";
    }
    setTextPos(xx, yy) {
        this.textField.x = xx;
        this.textField.y = yy;
    }
    setTextColor(textColor) {
        this.textField.fill = textColor;
    }
    getTextField() {
        return this.textField;
    }
    getText() {
        return this.textField.text;
    }
}
class Grid extends Phaser.Group {
    constructor(xspace = 1, yspace = 1) {
        super(game);
        this.xspace = xspace;
        this.yspace = yspace;
    }
    addItem(item) {
        this.add(item);
    }
    makeGrid(cols) {
        var len = this.children.length;
        var yy = 0;
        var xx = 0;
        for (var i = 0; i < len; i++) {
            var c = this.getChildAt(i);
            c.x = xx * c.width * this.xspace;
            c.y = yy * c.height * this.yspace;
            xx++;
            if (xx == cols) {
                xx = 0;
                yy++;
            }
        }
    }
}
class Logo extends Phaser.Group {
    constructor(key, w = -1) {
        super(game);
        var image = game.add.image(0, 0, key);
        image.anchor.set(0.5, 0.5);
        if (w != -1) {
            image.width = game.width * w;
            image.scale.y = image.scale.x;
        }
        this.add(image);
        this.x = game.width / 2;
        this.y = game.height / 2;
    }
}
class GButton extends Phaser.Group {
    constructor(text = 'button', event = null, params = null, w = 100, backColor = 0xE9B515, size = 22, textColor = '#000000') {
        super(game);
        this.event = event;
        this.params = params;
        //
        //
        //
        this.buttonBack = game.add.graphics();
        this.buttonBack.beginFill(backColor, 1);
        this.buttonBack.drawRoundedRect(0, 0, w, 50, 8);
        this.buttonBack.endFill();
        //
        //
        this.textField = game.add.text(0, 0, text);
        this.textField.anchor.set(0.5, 0.5);
        this.textField.fontSize = size + "px";
        this.textField.x = this.buttonBack.width / 2;
        this.textField.y = this.buttonBack.height / 2;
        this.textField.fill = textColor;
        if (model.buttonFont != "none") {
            this.textField.font = model.buttonFont;
        }
        this.add(this.buttonBack);
        this.add(this.textField);
        this.buttonBack.inputEnabled = true;
        this.buttonBack.events.onInputUp.add(this.onPressed, this);
        this.buttonBack.events.onInputDown.add(this.onDown, this);
    }
    getBack() {
        return this.buttonBack;
    }
    onPressed() {
        this.buttonBack.y = 0;
        if (this.event) {
            eventDispatcher.dispatch(this.event, this.params);
        }
    }
    onDown() {
        this.buttonBack.y = -5;
    }
    setCallBack(callback) {
        this.callback = callback;
    }
    setTextSize(size) {
        this.textField.fontSize = size + "px";
    }
    setTextPos(xx, yy) {
        this.textField.x = xx;
        this.textField.y = yy;
    }
    setTextColor(textColor) {
        this.textField.fill = textColor;
    }
    getTextField() {
        return this.textField;
    }
    getText() {
        return this.textField.text;
    }
}
class ImageButton extends Phaser.Group {
    constructor(key, event = null, params = null) {
        super(game);
        this.icon = game.add.image(0, 0, key);
        this.icon.anchor.set(0.5, 0.5);
        this.add(this.icon);
        this.event = event;
        this.params = params;
        if (this.event) {
            this.icon.inputEnabled = true;
            this.icon.events.onInputUp.add(this.clicked, this);
            this.icon.events.onInputDown.add(this.onDown, this);
        }
    }
    onDown() {
        this.icon.y = -5;
    }
    clicked() {
        this.icon.y = 0;
        eventDispatcher.dispatch(this.event, this.params);
    }
}
class BorderButton extends Phaser.Group {
    constructor(text = 'button', event = null, params = null, w = 100, borderColor = 0xE9B515, backColor = 0xE9B515, backAlpha = .1, size = 22, textColor = '#000000', round = 8, useShadow = true) {
        super(game);
        this.event = event;
        this.params = params;
        //
        //
        //
        this.buttonBack = game.add.graphics();
        this.buttonBack.lineStyle(2, borderColor);
        this.buttonBack.beginFill(backColor, backAlpha);
        this.buttonBack.drawRoundedRect(0, 0, w, 50, round);
        this.buttonBack.endFill();
        //
        //
        this.textField = game.add.text(0, 0, text);
        this.textField.anchor.set(0.5, 0.5);
        this.textField.fontSize = size + "px";
        this.textField.x = this.buttonBack.width / 2;
        this.textField.y = this.buttonBack.height / 2;
        this.textField.fill = textColor;
        if (useShadow == true) {
            this.textField.setShadow(5, 5, 'rgba(0,0,0,1)', 5);
        }
        if (model.buttonFont != "none") {
            this.textField.font = model.buttonFont;
        }
        this.add(this.buttonBack);
        this.add(this.textField);
        this.buttonBack.inputEnabled = true;
        this.buttonBack.events.onInputUp.add(this.onPressed, this);
        this.buttonBack.events.onInputDown.add(this.onDown, this);
    }
    adjust() {
        this.x -= this.width / 2;
        this.y -= this.height / 2;
    }
    getBack() {
        return this.buttonBack;
    }
    onPressed() {
        this.buttonBack.y = 0;
        if (this.event) {
            eventDispatcher.dispatch(this.event, this.params);
        }
    }
    onDown() {
        this.buttonBack.y = -5;
    }
    setCallBack(callback) {
        this.callback = callback;
    }
    setTextSize(size) {
        this.textField.fontSize = size + "px";
    }
    setTextPos(xx, yy) {
        this.textField.x = xx;
        this.textField.y = yy;
    }
    setTextColor(textColor) {
        this.textField.fill = textColor;
    }
    getTextField() {
        return this.textField;
    }
    getText() {
        return this.textField.text;
    }
}
class Background extends Phaser.Group {
    constructor(key, useScale = false) {
        super(game);
        this.image = game.add.image(0, 0, key);
        this.image.width = game.width;
        if (useScale == false) {
            this.image.height = game.height;
        } else {
            this.image.scale.y = this.image.scale.x;
        }
        this.add(this.image);
    }
}
class Thumb extends Phaser.Group {
    constructor(key) {
        super(game);
        this.icon = game.add.image(0, 0, key);
        this.icon.anchor.set(0.5, 0.5);
        this.add(this.icon);
    }
    getIcon() {
        return this.icon;
    }
}
class PopBack extends Phaser.Group {
    constructor(w = 400, h = 400) {
        super(game);
        //header
        this.header = game.add.graphics();
        this.header.beginFill(model.headerColor, 1);
        this.header.drawRoundedRect(0, 0, w, h / 3, 8);
        //hide the rounded corners on the bottom
        this.header.drawRect(0, 30, w, 25);
        this.header.endFill();
        Align.scaleToGameW(this.header, 0.5);
        //middle
        //
        this.middle = game.add.graphics();
        this.middle.beginFill(model.windowColor, 1);
        this.middle.drawRect(0, this.header.y + this.header.height - 10, w, h);
        this.middle.endFill();
        //footer
        this.footer = game.add.graphics();
        this.footer.beginFill(model.windowColor, 1);
        this.footer.drawRoundedRect(0, h + 25, w, 50, 8);
        this.footer.endFill();
        this.add(this.header);
        this.add(this.middle);
        this.add(this.footer);
    }
}
class PopUp extends Phaser.Group {
    constructor() {
        super(game);
        this.name = "";
        eventDispatcher.add(this.gotEvent, this);
    }
    drawBack(w, h) {
        this.back = new PopBack(w, h);
        //
        this.back.x = -this.back.width / 2;
        this.back.y = -this.back.height / 2;
        //
        this.add(this.back);
    }
    gotEvent(call, params) {
        switch (call) {
            case G.SHOW_POP_UP:
                if (params == this.name) {
                    this.visible = true;
                }
                break;
            case G.HIDE_POP_UPS:
                this.visible = false;
                break;
            default:
                this.doEvent(call, params);
                break;
        }
    }
    doEvent(call, params) {}
}
class Panel extends Phaser.Group {
    constructor(key, w = -1, h = -1) {
        super(game);
        var image = game.add.image(0, 0, key);
        //  image.anchor.set(0.5, 0.5);
        if (w != -1) {
            image.width = game.width * w;
            if (h != -1) {
                image.height = game.height * h;
            } else {
                image.scale.y = image.scale.x;
            }
        }
        this.add(image);
    }
}
class GameConstants {
    constructor() {
        this.UP_SCORE = "upScore";
        this.SCORE_UPDATED = "scoreUpdated";
        this.SET_TIME = "setTime";
        this.STOP_TIME = "stopTime";
        this.START_TIME = "startTime";
        this.TIMES_UP = "timesUp";
        this.ADD_TIME = "addTime";
        //
        //
        this.SET_BAR_TIME = "setBarTime";
        this.STOP_BAR_TIME = "stopBarTime";
        this.START_BAR_TIME = "startBarTime";
        this.ADD_BAR_TIME = "addBarTime";
        //
        //
        //
        this.TOGGLE_MUSIC = "toggleMusic";
        this.TOGGLE_SOUND = "toggleSound";
        this.MUSIC_STAT_CHANGED = "musicStatChanged";
        this.PLAY_SOUND = "playSound";
        //
        //
        this.START_GAME = "startGame";
        this.END_GAME = "endGame";
        //
        //
        this.SHOW_POP_UP = "showPopUp";
        this.HIDE_POP_UPS = "hidePopUps";
        this.SHOW_MESSAGE_BOX = "showMessageBox";
        this.SHOW_ITEM_BOX = "showItemBox";
        this.SHOW_YES_NO_BOX = "showYesNoBox";
        this.UNLOCK_LEVEL = "unlocklevel";
        this.GET_LEVEL = "getLevel";
        this.CHANGE_STATE = "changeState";
        this.SHOW_OVER_BAR = "showOverBar";
        this.SHOW_TOAST = "showToast";
    }
}
var game;
var model;
var controller;
var eventDispatcher;
var mediaManager;
var levelManager;
//
//
var useLandscape = false;
var isMobile;
var G;
var wrongTag;
window.onload = function() {
    isMobile = navigator.userAgent.indexOf("Mobile");
    isMobile = (isMobile != -1) ? true : false;
    if (isMobile == false) {
        if (useLandscape == true) {
            game = new Phaser.Game(640, 480, Phaser.AUTO, "ph_game");
        } else {
            game = new Phaser.Game(480, 640, Phaser.AUTO, "ph_game");
        }
    } else {
        game = new Phaser.Game('100', '100', Phaser.AUTO, "ph_game");
        if (useLandscape == true) {
            wrongTag = "wrongWayLandscape";
        } else {
            wrongTag = "wrongWayPortrait";
        }
    }
    G = new GameConstants();
    eventDispatcher = new Phaser.Signal();
    model = new Model();
    controller = new Controller();
    mediaManager = new MediaManager();
    levelManager = new LevelManager();
    // levelManager.resetLevels();
    //set to true to skip to stateMain and turn off music
    //used when starting to develop a game 
    model.devMode = false;
    //
    game.state.add("StateInit", StateInit);
    game.state.add("StateLoad", StateLoad);
    game.state.add("StateMain", StateMain);
    game.state.add("StateTitle", StateTitle);
    game.state.add("StateLevels", StateLevels);
    game.state.add("StateSetUp", StateSetUp);
    game.state.add("StateInstructions", StateInstructions);
    game.state.start("StateInit");
}
class Model {
    constructor() {
        this.score = 0;
        this.mainFont = "Flamenco";
        this.buttonFont = "Flamenco";
        this._musicOn = true;
        this.soundOn = true;
        this.lastUnlocked = 1;
        this.defButtonSet = 4;
        this.defButtonStyle = 3;
        this.musicState=true;
        this.soundState=true;
        //
        //
        this.defaultFont = "none";
        //
        //
        this.mainTextColor = "#ffffff";
        this.mainColor = "#ffffff";
        this.secondaryColor = "#ffff00";
        this.windowColor = "#ffffff";
        this.headerColor = "#E9B515";
        this.pointTextColor = "#ffffff";
        this.soundButtonIndex = 2;
        this.secondaryTextColor = "#ffff00";
        this.clockColor = "#ffffff";
        this.scoreColor = "#ff0000";
        this.toastTextColor = 0xffffff;
        this.toastBarColor = 0xff0000;
        //
        //
        //
        //
        this.titleFontSize = 20;
        this.buttonFontSize = 16;
        this.defaultFontSize = 12;
        this.scoreFontSize = 12;
        this.clockFontSize = 12;
        this.pointFontSize = 16;
        //
        //
        this.levelColor = "#ffffff";
        this.lockFontSize = 12;
        this.showUnlocked = false;
        //
        //
        //
        this.useScoreLabel = true;
        this.useTimeLabel = true;
        //
        this.sfx = [];
        //
        //levels
        this.levels = 100;
        this.pageSize = 25;
        this.levelPage = 0;
        this.pageTweenSpeed = 250;
        this.level = 0;
        this.levelPagesCount = Math.floor(this.levels / this.pageSize);
        if (this.levelPagesCount == this.levels / this.pageSize) {
            this.levelPagesCount--;
        }
        this.reset();
        this.coins = 0;
        this.correctWords = [];
        this.wordRows = [];
        this.toastColors = ['0xC440B0', '0xff0000', '0x16a085', '0xe67e22', '0x8e44ad'];
    }
    getHint() {
        var hints = [];
        for (var i = 0; i < 4; i++) {
            if (this.hasWord(this.correctWords[i]) == false) {
                hints.push(this.correctWords[i]);
            }
        }
        var index = game.rnd.integerInRange(0, hints.length - 1);
        return hints[index];
    }
    hasWord(word) {
        for (var i = 0; i < 4; i++) {
            if (this.wordRows[i] == word) {
                return true;
            }
        }
        return false;
    }
    getPositive() {
        var index = game.rnd.integerInRange(0, this.positives.length - 1);
        return this.positives[index];
    }
    getToastColor() {
        var index = game.rnd.integerInRange(0, this.toastColors.length - 1);
        return this.toastColors[index];
    }
    addCoins(coins) {
        this.coins += parseInt(coins);
        eventDispatcher.dispatch("ADD_COINS");
        eventDispatcher.dispatch(G.PLAY_SOUND, "coins");
        Saver.save("flw_coins", this.coins);
    }
    takeCoins(coins) {
        this.coins -= parseInt(coins);
        eventDispatcher.dispatch("ADD_COINS");
        eventDispatcher.dispatch(G.PLAY_SOUND, "coins");
        Saver.save("flw_coins", this.coins);
    }
    reset() {
        this.greens = [];
        this.greens.push(false);
        this.greens.push(false);
        this.greens.push(false);
        this.greens.push(false);
        this.newWords = [];
        this.showStars = [false, false, false, false];
    }
    upScore(points) {
        this.score += parseInt(points);
        //console.log("score=" + this.score);
        eventDispatcher.dispatch(G.SCORE_UPDATED);
    }
    regSound(name) {
        this.sfx.push(name);
    }
    set musicOn(val) {
        this._musicOn = val;
        eventDispatcher.dispatch(G.MUSIC_STAT_CHANGED);
    }
    get musicOn() {
        return this._musicOn;
    }
    setUpWords() {
        this.dict = [];
        for (var i = 0; i < this.words.length; i++) {
            this.dict[this.words[i]] = 1;
        }
    }
    checkWord(word) {
        if (this.dict.hasOwnProperty(word)) {
            return true;
        }
        return false;
    }
}
class Controller {
    constructor() {
        eventDispatcher.add(this.gotEvent, this);
    }
    gotEvent(call, params) {
        //console.log("call=" + call);
        switch (call) {
            case G.UP_SCORE:
                model.upScore(params);
                break;
                //
                //
            case G.TOGGLE_SOUND:
                model.soundOn = params;
                break;
                //
                //
            case G.TOGGLE_MUSIC:
                model.musicOn = params;
                break;
                //
                //
            case G.START_GAME:
                model.currentLevel = model.lastUnlocked;
                game.state.start("StateMain");
                break;
                //
                //
            case G.END_GAME:
                game.state.start("StateOver");
                break;
                //
                //
            case G.CHANGE_STATE:
                game.state.start(params);
                break;
                //
                //
            case G.GET_LEVEL:
                var level = parseInt(params);
                //console.log("level=" + level);
                if (level > model.lastUnlocked) {
                    eventDispatcher.dispatch(G.SHOW_MESSAGE_BOX, {
                        "title": "Locked!",
                        "message": "That Level Is Locked!"
                    });
                } else {
                    model.currentLevel = level;
                    game.state.start("StateMain");
                }
                break;
        }
    }
}
var StateInit = {
    preload: function() {
        //
        //This file sets up the preloader
        //
        //
        //
        game.load.image("loadingEmpty", "images/loading/progress_none.png");
        game.load.image("loadingFull", "images/loading/progress_all.png");
        if (isMobile == true) {
            if (useLandscape == true) {
                game.scale.forceOrientation(true, false);
            } else {
                game.scale.forceOrientation(false, true);
            }
            game.scale.enterIncorrectOrientation.add(this.wrongWay, this);
            game.scale.leaveIncorrectOrientation.add(this.rightWay, this);
        }
    },
    create: function() {
        game.state.start("StateLoad");
    },
    update: function() {},
    rightWay: function() {
        if (model.state != "main") {
            location.reload();
        }
        document.getElementById(wrongTag).style.display = "none";
    },
    wrongWay: function() {
        document.getElementById(wrongTag).style.display = "block";
    }
}
var StateLoad = {
    preload: function() {
        model.state = "load";
        var empty = game.add.image(0, 0, "loadingEmpty");
        var full = game.add.image(0, 0, "loadingFull");
        empty.anchor.set(0.5, 0.5);
        empty.x = game.width / 2;
        empty.y = game.height / 2;
        //
        //
        full.anchor.set(0, 0.5);
        full.x = game.world.centerX - empty.width / 2;
        full.y = empty.y;
        game.load.setPreloadSprite(full);
        //PRELOAD EVERYTHING HERE
        //
        //
        //
        //
        //Preload all text buttons
        // TextButton.preloadAll();
        ColorBurst.preload();
        //
        //
        //set a theme for the game
        //
        var theme = new GreenTheme();
        theme.loadTheme();
        //
        //
        //
        //
        this.loadMain("heart");
        this.loadMain("gameTitle");
        this.loadMain("pine");
        this.loadMain("dark");
        this.loadMain("coin");
        //
        //
        this.loadZen("greenBackground");
        this.loadZen("rocks1");
        this.loadZen("bamboo");
        //
        //
        //
        //
        this.loadJapan("japanBack");
        this.loadJapan("fore");
        this.loadJapan("tree");
        //
        //
        //
        game.load.image("fieldBack", "images/main/field/fieldBack.jpg");
        game.load.image("fieldFore", "images/main/field/fieldFore.png");
        this.loadField("base");
        this.loadField("blades");
        this.loadField("balloon");
        //
        //
        //
        //
        game.load.image("winter", "images/main/winter/winter.jpg");
        game.load.image("plains", "images/main/plains/plains.jpg");
        game.load.image("plainsFore", "images/main/plains/plainsFore.png");
        //
        //
        //
        game.load.atlas("crow", "images/main/crow.png", "images/main/crow.json");
        game.load.atlas("clouds", "images/main/clouds.png", "images/main/clouds.json");
        game.load.atlas("horseRider", "images/main/plains/horseRider.png", "images/main/plains/horseRider.json");
        //
        //
        //
        game.load.audio("background", "audio/background/background.mp3");
      //  game.load.text('dict', 'data/fourletterwords.txt');
      //  game.load.text('positives', 'data/positives.txt');
        //
        //
        this.loadSFX2("newlevel");
        this.loadSFX("right");
        this.loadSFX2("swish");
        this.loadSFX("coins");
        //
        //
        var coins = Saver.getData("flw_coins");
        coins = parseInt(coins);
        if (isNaN(coins)) {
            coins = 20;
        }
        model.coins = coins;
    },
    loadMain(name) {
        game.load.image(name, "images/main/" + name + ".png");
    },
    loadZen(name) {
        game.load.image(name, "images/main/zen/" + name + ".png");
    },
    loadJapan(name) {
        game.load.image(name, "images/main/japan/" + name + ".png");
    },
    loadField(name) {
        game.load.image(name, "images/main/field/" + name + ".png");
    },
    loadSFX(name) {
        game.load.audio(name, "audio/sfx/" + name + ".mp3");
        model.regSound(name);
    },
    loadSFX2(name) {
        game.load.audio(name, "audio/sfx/" + name + ".wav");
        model.regSound(name);
    },
    create: function() {
        //pass the key for background music to the media manager
        mediaManager.setBackgroundMusic("background");
        //pass sound keys to the media manager
        //a sound object will be created
        model.sfx.forEach(function(sound) {
            mediaManager.addSound(sound);
        }.bind(this));
        game.state.start("StateSetUp");
    },
    update: function() {}
}
var StateMain = {
    preload: function() {},
    create: function() {
        this.gameOver = false;
        //keep this line
        //to tell the game what state we are in!
        model.state = "main";
        /* var dict = game.cache.getText('dict');
         model.words = dict.split(' ');
         model.setUpWords();
         var positives = game.cache.getText('positives');
         model.positives = positives.split("\n");*/
        this.makeBack();
        model.mainTextColor = this.textColor;
        // var winter=new Winter();
        //  var plains=new Plains();
        // var zen=new Zen();
        //    var japan=new Japan();
        // 
        var soundButtons = new SoundButtons();
        var wordGame = new WordGame();
        this.wordGame = wordGame;
        this.levelText = game.add.text(game.width / 2, game.height * .1, "Level:" + model.currentLevel);
        this.levelText.anchor.set(0.5, 0.5);
        //
        //
        // this.btnNext = new TextButton("Next", 4, 13, "GO_NEXT", null, 44);
        this.btnNext = new BorderButton("Next", "GO_NEXT", null, game.width * .5, "0x1ED610", "0x1ED610", .8, 40, "#ffffff");
        this.btnNext.visible = false;
        //
        //
        //  this.btnHint = new TextButton("Click For Hint", 4, 10, "DO_POWER_UP", null, 40,"#24F30E");
        this.btnHint = new BorderButton("Click For Hint", "DO_POWER_UP", null, game.width * .5, "0x1ED610", "0x1ED610", .8, 40, "#ffffff");
        // this.btnHint.visible=false;
        Align.scaleToGameW(this.btnHint, .3);
        //
        //
        this.aGrid = new AlignGrid(11, 11);
        // this.aGrid.showNumbers();
        this.aGrid.placeAtIndex(104, this.btnNext);
        this.aGrid.placeAtIndex(103, this.btnHint);
        Align.centerHGroup(this.btnHint);
        Align.centerHGroup(this.btnNext);
        eventDispatcher.add(this.gotEvent, this);
        var coinBox = new CoinBox();
        var toastBar = new ToastBar();
        var messageBox = new MessageBox();
        var btnHome = new ImageButton("btnHome", G.CHANGE_STATE, "StateTitle");
        Align.scaleToGameW(btnHome, .1);
        btnHome.x = btnHome.width / 2;
        btnHome.y = game.height - btnHome.height / 2;
        if (model.currentLevel == 1) {
            this.showHelp();
        }
        var btnHelp = new ImageButton("btnHelp", "SHOW_HELP");
        Align.scaleToGameW(btnHelp, .1);
        btnHelp.y = btnHome.y;
        btnHelp.x = game.width - btnHelp.width / 2;
        //"Click 2 letters to swap them to make words. You need 4 words to complete the level. Only words across count. For each word you make you get a coin. Trade coins for hints."
    },
    showHelp() {
        eventDispatcher.dispatch(G.SHOW_MESSAGE_BOX, {
            title: "How To Play",
            message: "Click 2 letters to swap them to make rows of words. You need 4 words to complete the level. For each word you make you get a coin. Trade coins for hints.",
            color: '0xC440B0',
            fontSize: '14px',
            textColor: '#000000'
        });
    },
    makeBack() {
        this.textColor = "#000000";
        var cl = model.currentLevel;
        while (cl > 250) {
            cl -= 250;
        }
        if (cl < 51) {
            var zen = new Zen();
            return;
        }
        if (cl < 101) {
            var japan = new Japan();
            this.textColor = "#ffffff";
            return;
        }
        if (cl < 151) {
            var field = new Field();
            return;
        }
        if (cl < 201) {
            var winter = new Winter();
            return;
        }
        if (cl < 251) {
            var plains = new Plains();
            return;
        }
    },
    gotEvent(call, params) {
        if (call == "SHOW_HELP") {
            this.showHelp();
        }
        if (call == "GO_NEXT") {
            model.currentLevel++;
            console.log("CURRENT LEVEL=" + model.currentLevel);
            if (model.currentLevel / 2 == Math.floor(model.currentLevel / 2)) {
                //show ad
                // eventDispatcher.dispatch("SHOW_AD");
                console.log("show ad");
               // gdApi.showBanner();
            }
            model.clickLock = false;
            model.reset();
            game.state.start("StateMain");
        }
        if (call == G.HIDE_POP_UPS) {
            this.btnHint.visible = true;
        }
        if (call == "SHOW_NEXT_BUTTON") {
            this.gameOver = true;
            var newLevel = model.currentLevel + 1;
            eventDispatcher.dispatch(G.UNLOCK_LEVEL, newLevel);
            var stars = new ColorBurst(this.btnNext.x + this.btnNext.width / 2, this.btnNext.y, 200, 1, 50, 300);
            model.clickLock = true;
            this.btnHint.visible = false;
            this.btnNext.visible = true;
        }
        if (this.gameOver == false) {
            if (call == "DO_POWER_UP") {
                if (model.coins < 10) {
                    eventDispatcher.dispatch(G.SHOW_TOAST, {
                        message: "You need 10 coins for a hint!",
                        color: '0xC440B0',
                        textColor: '#ffffff'
                    });
                } else {
                    model.takeCoins(10);
                    this.btnHint.visible = false;
                    var hint = model.getHint();
                    eventDispatcher.dispatch(G.SHOW_MESSAGE_BOX, {
                        title: "Hint!",
                        message: "Try this word:" + hint,
                        color: '0xC440B0',
                        fontSize: '22px',
                        textColor: '#000000'
                    });
                }
            }
        }
    },
    update: function() {},
    shutdown: function() {
        this.wordGame.prepDestroy();
        this.wordGame.destroy(true);
    }
}
var StateTitle = {
    create: function() {
        model.state = "title";
        var field = new Field();
        var soundButtons = new SoundButtons();
        var agrid = new AlignGrid(11, 11);
        // var buttonStart = new TextButton("Start", 4, 14, G.START_GAME, null, 55, "#ff0000");
        var buttonStart = new BorderButton("Start", G.START_GAME, null, game.width * .5, "0x713792", "0xffffff", 0, 55, "#ff0000", 16, false);
        //  var buttonLevel = new TextButton("Levels", 4, 14, G.CHANGE_STATE, "StateLevels", 55, "#ff0000");
        var buttonLevel = new BorderButton("Levels", G.CHANGE_STATE, "StateLevels", game.width * .5, "0x713792", "0xffffff", 0, 55, "#ff0000", 16, false);
        // var buttonInstructions = new TextButton("Instructions", 4, 14, G.CHANGE_STATE, "StateInstructions", 45, "#ff0000");
        var buttonInstructions = new BorderButton("Instructions", G.CHANGE_STATE, "StateInstructions", game.width * .5, "0x713792", "0xffffff", 0, 45, "#ff0000", 16, false);
        var logo = new Logo("gameTitle", .8);
        // agrid.showNumbers();
        agrid.placeAtIndex(38, buttonStart);
        agrid.placeAtIndex(60, buttonLevel);
        agrid.placeAtIndex(82, buttonInstructions);
        agrid.placeAtIndex(16, logo);
        //
        //
        buttonStart.adjust();
        buttonLevel.adjust();
        buttonInstructions.adjust();
        /* Align.centerHGroup(buttonStart);
         Align.centerHGroup(buttonLevel);
         Align.centerHGroup(buttonInstructions);*/
    }
}
var StateSetUp = {
    preload: function() {},
    create: function() {
        var wordString = "aahs abet able ably abut aced aces ache acid acme acne acre acts adds adze afar afro agar aged ages agog ague ahas ahem ahoy aide aids ails aims airs airy ajar akin alas albs alef ales alga ally alms aloe alps also alto alum ambo amen amid amok amps anal ands anew anon ante ants anus aped aper apes apex apps aqua arch arcs area ares aria arid aril arks arms army arts arty ashs ashy asks asps atom atop aunt aura auto aver avid avow away awed awes awls awns awny awol awry axed axel axes axis axle axon ayes ayin baas babe baby back bade bags baht bail bait bake bald bale balk ball balm band bane bang bank bans barb bard bare barf bark barm barn bars base bash bask bass bath bats baud bawd bawl bays bead beak beam bean bear beat beau beds beef been beep beer bees beet begs bell belt bend bent berk berm best beta beth bets bevy bias bibs bide bids bike bile bilk bill bind bins bios bird birr bite bits bitt blab blah blat bleb bled blew blip blob blog blot blow blue blur boar boas boat bobs bode body bogs boil bold boll bolt bomb bond bone bonk bony book boom boon boor boos boot bops bore born boss both bots bout bowl bows boxy boys brad brag bran bras brat bray bred brew brie brim bris brow bubo buck buds buff bugs buhl buhr bulb bulk bull bump bums bunk buns bunt buoy burl burn burp burr burs bury bush busk bust busy buts butt buys buzz byes byte cabs cads cafe cage cake calf call calm calx came camp cams cane cans cant cape caps card care carp cars cart case cash cask cast cats cave caws ceca cede cedi cees cell celt cent chad chap char chat chef chew chic chin chip chis chiv chop chow chub chug chum cite city clad clan clap claw clay clef clip clod clog clop clot club clue coal coat coax cobs cock coda code cods coed cogs coho coif coil coin cola cold cole colt coma comb come cone conk cons cook cool coop coos coot cope cops copy cord core cork corm corn cost cosy cots coup cove cowl cows coys cozy crab crag cram crap crew crib crop crow crud crux cube cubs cuds cued cues cuff cull cult cups curb curd cure curl curs curt cusp cuss cute cuts cwms cyan cyst czar dabs dado dads daft dame damn damp dams dank dare dark darn dart dash data date daub dawn days daze dead deaf deal dean dear debt deck deed deem deep deer dees deft defy deil dele delf deli dell deme demo demy dene dens dent deny dere derm desk deva dews dewy deys dhow dial dibs dice died dies diet digs dill dime dims dine ding dins dips dire dirt disc dish disk ditz diva dive dock dodo doer does doff doge dogs dole doll dolt dome done dons doom door dope dork dorm dose dote doth dots doty dove down doze dozy drab drag dram draw dreg drew drip drop drug drum dual dubs duck duct dude duds duel dues duet duff duke dull duly dumb dump dune dung dunk duos dupe dusk dust duty dyed dyer dyes dyne each earl earn ears ease east easy eats eave ebbs echo ecru eddy edge edgy edit eeks eels eely eery effs eggs eggy egos eked eker ekes elks ells elms else emir emit emus ends enol envy eons epic eras ergo ergs eros etas etch euro even ever eves evil ewer ewes exam exes exit exon expo eyed eyes face fact fade fads fail fain fair fake fall fame fang fans fare farm fast fate fats faun faux fave fawn faze fear feat feds feed feel fees feet fell felt fend fens fern feta feud fibs figs file fill film find fine fink fins fire firm firs fish fist fits five fizz flab flag flan flap flat flaw flax flay flea fled flee flew flex flip flit floe flog flop flow flox flub flue flux foal foam fobs foci foes fogs fogy foil fold folk fond font food fool foot fops fora fore fork form fort foul four fowl foxy fray free fret friz frog from fuel full fume fumy fund funk furs fury fuse fuss fuzz gabs gaff gaga gage gags gain gait gala gale gall gals game gang gape gaps garb gash gasp gate gave gawk gaze gear geek gees geld gell gels gems gene gent germ gets gift gigs gild gill gilt gimp gins gird girl girn gist give glad glee glen glia glib glob glow glue glug glum glut gnar gnat gnaw gnus goad goal goat gobs gods goer goes goji gold golf gone gong good goof goon goop goos gore gory gosh goth gout gown grab gram gray grew grey grid grim grin grip grit grow grub guck guff gulf gull gulp gums gunk guns guru gush gust guts guys gyms gyps gyre gyro hack hags hail hair half hall halo halt hams hand hang haps hard hare hark harm harp hash hasp hate hath hats haul have hawk haws hays haze hazy head heal heap hear heat heck heed heel heir held hell helm help heme hems hens herb herd here hero hers heth hewn hews hick hide high hike hill hilt hind hint hips hire hiss hits hive hoar hoax hobo hoed hoer hoes hogs hold hole holy home hone honk hood hoof hook hoop hoot hope hops horn hose host hots hour howl hows hubs hued hues huff huge hugs huhs hula hulk hull hump hums hung hunk hunt hurl hurt hush husk huts hymn hype hypo ibex ibis iced icer ices icky icon idea ides idle idly idol iffy ilea ilka ilks ills imam imps inch inks inky inns into ions iota ired ires iris irks iron isle isms itch item jabs jack jade jags jail jali jamb jams jars java jaws jays jazz jean jeep jeer jeli jell jerk jest jets jibe jigs jilt jink jinx jive jobs jock jogs join joke jolt josh jots jowl joys judo jugs juke july jump june junk jury just jute juts kale kaph kays keek keel keen keep kegs kelp kept kern keys kick kids kill kiln kilt kina kind kine king kink kips kiss kite kits kiwi knar knee knew knit knob knot know knur kook kudu kuna kyak kyat labs lace lack lacy lade lads lady lags laid lain lair lake lamb lame lamp land lane lank laps lard lari lark lash lass last late laud lava lave lawn laws lays laze lazy lead leaf leak lean leap lear leas leek leer lees left legs leks lend lens lent less lest lets leus levs levy lewd liar lice lick lids lied lien lier lies lieu life lift like lily limb lime limn limo limp limy line link lint lion lips lira lire lisp list lite live load loaf loam loan lobe lobs loch loci lock loco lode loft logo logs loin loll lone long look loom loon loop loos loot lope lops lord lore lose loss lost loti lots loud lout love lows luau lube luck luff luge lugs lull lump lung lure lurk lush lust lute lynx lyre mace mach made mage magi maid mail maim main make male mall malt mama mane mans many maps mara mare mark marl mars mart mash mask mass mast mate math mats matt maul maws mayo mays maze mead meal mean meat meek meet meld melt meme memo mend mens menu meow mere mesa mesh mess mews mica mice midi miff mild mile milk mill mils mime mind mine mini mink mint minx mire miss mist mite mitt moan moat mobs mock mode mods moho mold mole molt moms monk mood moon moor moos moot mope mops more moss most moth move mown mows much muck muff mugs mule mull mums muon murk muse mush musk must mute mutt myna myth nabs nags nail name nape naps naut nave navy nays nazi neap near neat neck need neon nerd nest nets nevi news newt next nibs nice nick nigh nils nine nips nits nobs node nods noel none noon nope norm nose nosy note noun nova nude nuke null numb nuns nuts oafs oaks oars oath oats obey oboe odds odes odor offs ogle ogre ohms oils oily oink okay okra oleo omen omit omni once ones only onto onus onyx oohs ooid oops ooze oozy opal open opts oral orbs orca ores oryx ouch ours oust outs ouzo oval oven over ovum owed ower owes owls owly owns oxen oxes pace pack pact pads page paid pail pain pair pale pall palm pals pane pang pans pant papa paps pare park pars part pass past pate path pats pave pawn paws pays peak peal pear peas peat peck peek peel peep peer pegs pelf pelt pend pens pent peon peps perk perm pert peso pest pets pews phis phiz pick pied pier pies pigs pike pile pili pill pimp pine ping pink pins pint pipe pips pita pith pits pity pius plan play plea pled plod plop plot plow ploy plug plum plus pock pods poem poet pogo poke poky pole poll polo pomp pond pony pooh pool poop poor pope pops pore pork porn port pose posh post posy pots pouf pour pout poxy pram pray prep prey prim prod prom prop pros prow psis pubs puce puck puff pugs puke pull pulp puma pump punk puns punt puny pupa pups pure purr push puts putt pyre qoph quad quay quid quip quit quiz race rack racy raft rage rags raid rail rain rake rami ramp rams rand rang rank rant rape raps rapt rare rash rasp rate rats rave raws rays raze razz read reak real ream reap rear redo reds reed reef reek reel refs rein rely rend rent repo resh rest revs rhos rial ribs rice rich rick ride rids riel rife riff rift rigs rile rill rily rime rims rind ring rink riot ripe rips rise risk rite rive road roam roan roar robe robs rock rode rods roes roil role roll romp rood roof rook room root rope ropy rose rosy rote rots roue rout rove rows rubs ruby rude rued rues ruff rugs ruin rule rums rune rung runs runt ruse rush rust ruts sack sacs safe saga sage sags sagy said sail sake saki sale salt same sand sane sang sank saps sari sash sass sate save sawn saws says scab scam scan scar scat scot scud scum seal seam sear seas seat sect seed seek seem seen seep seer sees self sell send sent sera sere serf seta sets sewn sews sext sexy shah sham shed shew shim shin ship shiv shmo shoe shoo shop shot show shun shut shwa sick side sift sigh sign sikh silk sill silo silt sine sing sink sins sips sire sirs site sits sitz size skew skid skim skin skip skis skit slab slam slap slat slaw slay sled slew slid slim slip slit slob sloe slog slop slot slow slug slum slur smit smog smug smut snag snap snip snit snob snog snot snow snub snug soak soap soar sobs sock soda sods sofa soft soil sold sole solo some soms song sons soon soot sops sore sort sots soul soup sour sown sows soya soys spam span spar spas spat spay sped spin spit spot spry spud spun spur stab stag star stat stay stem step stew stir stop stow stub stud stun stye styx subs such suck suds sued suer sues suet suit sulk sumo sump sums sung sunk suns sure surf swab swag swam swan swap swat sway swig swim swum sync tabs tack taco tact tags tail taka take tala talc tale talk tall tame tamp tams tank tans tape taps tare tarn taro tarp tars tart task taus taut taxa taxi teak teal team tear teas tech teed teem teen tees tell tend tens tent term tern test teth text than that thaw thee them then they thin this thou thru thud thug thus tick tics tide tidy tied tier ties tiff tike tile till tilt time tine ting tins tint tiny tipi tips tire toad toed toes toff tofu toga toil told toll tomb tome tone tong tons took tool toot tops tore torn toro tort toss tote tots tour tout town tows toys tram trap tray tree trek trim trio trip trod trot troy true tsar tuba tube tubs tuck tufa tuff tuft tugs tums tuna tune turf turn tusk tutu twig twin twit twos tyke type typo tyro tzar ughs ugly ukes ulna umbo umps undo unit unix unto upon urea urge uric urns used user uses uvea vain vale vamp vane vang vans vara vary vase vast vats vatu veal vear veer vees veil vein vela vend vent verb very vest veto vets vial vibe vice vied vies view vile vine visa vise voes void vole volt vote vows vugs wack wade wads waft wage wags waif wail wait wake walk wall wand wane want ward ware warm warn warp wars wart wary wash wasp watt waul wave wavy wawl waxy ways weak wean wear webs weds weed week ween weep weir weld well welt wend went wept were west wets wham what when whet whew whey whim whip whir whiz whoa whom whop whup wick wide wife wifi wigs wild wile will wilt wily wimp wind wine wing wink wins wipe wire wiry wise wish wisp wist with wits wive woad woes woke woks wolf womb wons wont wood woof wool woos word wore work worm worn wort wove wows wrap wren writ wyes xray xyst yack yaff yagi yaks yald yams yang yank yaps yard yare yarn yaud yaup yawl yawn yawp yaws yeah yean year yeas yegg yeld yelk yell yelm yelp yens yerk yeti yett yeuk yews yill yins yipe yips yird yirr yodh yods yoga yogh yogi yoke yolk yond yoni yore your yowe yowl yows yoyo yuan yuck yuga yuks yule yurt yutz ywis zags zany zaps zarf zati zeal zebu zeds zees zein zens zerk zero zest zeta zigs zinc zine zing zips ziti zits zoea zoic zone zonk zoom zoon zoos zori zulu zyme hemp grog tock pong miso clam slut piss fuck cays cunt dick dike wats";
    
        //var dict = game.cache.getText('dict');
        model.words = wordString.split(' ');
        model.setUpWords();
        var positives = "Amazing,Awesome,Excellent,Fabulous,Fantastic,Great,Incredible,Impressive,Outstanding,Perfect,Remarkable,Smart,Spectacular,Splendid,Stellar,Stupendous,Super,Unbelievable,Wondrous";
        model.positives = positives.split(",");

        game.state.start("StateTitle");
    },
    update: function() {}
}
class ToastBar extends Phaser.Group {
    constructor() {
        super(game);
        this.graphics = game.add.graphics();
        //  this.drawBar();
        this.alpha = 0;
        this.text1 = game.add.text(0, 0, "Toast");
        this.text1.fill = "#ffffff";
        this.text1.fontSize = "22px";
        this.text1.x = game.width / 2;
        this.text1.anchor.set(0.5, 0);
        this.add(this.graphics);
        this.add(this.text1);
        eventDispatcher.add(this.gotEvent, this);
    }
    drawBar(color) {
        this.graphics.clear();
        this.graphics.beginFill(color, 1);
        this.graphics.drawRect(0, 0, game.width, game.height * .05);
        this.graphics.endFill();
    }
    fadeOut() {
        var tween = game.add.tween(this).to({
            alpha: 0
        }, 500, Phaser.Easing.Linear.None, true);
    }
    fadeInDone() {
        game.time.events.add(Phaser.Timer.SECOND / 2, this.fadeOut, this);
    }
    //listen for events
    gotEvent(call, params) {
        if (call == G.SHOW_TOAST) {
            this.text1.text = params.message;
            if (params.color) {
                this.drawBar(params.color);
            } else {
                this.drawBar(model.toastBarColor);
            }
            //
            //
            //
            if (params.textColor) {
                this.text1.fill = params.textColor;
            } else {
                this.text1.fill = model.toastTextColor;
            }
            //
            //
            //
            this.alpha = 0;
            this.visible = true;
            //tween
            var tween = game.add.tween(this).to({
                alpha: 1
            }, 500, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.fadeInDone, this);
        }
    }
}
class ColorBurst extends Phaser.Group {
    constructor(x, y, n = 50, speed = 1, min = 1, max = 100) {
        super(game);
        this.x = x;
        this.y = y;
        for (var i = 0; i < n; i++) {
            var star = game.add.sprite(0, 0, "effectColorStars");
            var f = game.rnd.integerInRange(0, 15);
            star.frame = f;
            star.anchor.set(0.5, 0.5);
            this.add(star);
            var r = game.rnd.integerInRange(50, 100);
            var s = game.rnd.integerInRange(min, max) / 100;
            star.scale.set(s, s);
            var angle = i * (360 / n);
            var tx = r * Math.cos(angle);
            var ty = r * Math.sin(angle);
            var time = 1000 * speed;
            var t = game.add.tween(star).to({
                y: ty,
                x: tx,
                alpha: 0
            }, time, Phaser.Easing.Linear.None, true);
            t.onComplete.add(this.tweenDone, this);
            //  star.x=tx;
            // star.y=ty;
        }
    }
    tweenDone(target) {
        target.destroy();
    }
    static preload() {
        game.load.spritesheet("effectColorStars", "images/effects/colorStars.png", 26, 26);
    }
}
class Sparks extends Phaser.Group {
    constructor(x, y, n = 25, color = 0xffffff) {
        super(game);
        this.x = x;
        this.y = y;
        //
        //
        //
        this.color = color;
        for (var i = 0; i < n; i++) {
            var s = this.getSpark();
            var angle = i * (360 / n);
            var r = game.rnd.integerInRange(50, 100);
            var tx = r * Math.cos(angle);
            var ty = r * Math.sin(angle);
            var t = game.add.tween(s).to({
                y: ty,
                x: tx,
                alpha: 0
            }, 1000, Phaser.Easing.Linear.None, true);
            t.onComplete.add(this.tweenDone, this);
        }
    }
    tweenDone(target) {
        target.destroy();
        if (this.children.length == 0) {
            this.destroy();
        }
    }
    getSpark() {
        var s = game.add.graphics();
        s.beginFill(this.color, 1);
        s.drawCircle(0, 0, 5);
        s.endFill();
        this.add(s);
        return s;
    }
}
var StateLevels = {
    create: function() {
        model.state = "levels";
        this.moving = false;
        var field = new Field();
        //
        //
        //
        var soundButtons = new SoundButtons();
        this.aGrid = new AlignGrid(5, 5);
        model.levelPage = 0;
        this.grid = new Grid(1.5, 1.5);
        var messageBox = new MessageBox();
        this.makeButtons();
        this.makePage();
        this.alignPage();
        this.gridX = this.grid.x;
        eventDispatcher.add(this.gotEvent, this);
    },
    makePage() {
        this.grid.removeAll();
        var start = model.levelPage * model.pageSize;
        var end = (model.levelPage + 1) * model.pageSize;
        if (end > model.levels) {
            end = model.levels;
        }
        for (var i = start; i < end; i++) {
            var level = i + 1;
            var lockButton = new LockButton(level);
            // Align.getScaleToGameW(lockButton);
            Align.scaleToGameW(lockButton, 0.11229166);
            if (parseInt(level) < parseInt(model.lastUnlocked) + 1) {
                lockButton.setLocked(false);
            } else {
                lockButton.setLocked(true);
            }
            this.grid.add(lockButton);
        }
        this.grid.makeGrid(5);
    },
    alignPage() {
        this.grid.x = game.width / 2 - this.grid.width / 2;
        this.grid.y = game.height / 2 - this.grid.height / 2;
    },
    gotEvent(call, params) {
        if (call == "nextLevelPage") {
            if (this.moving == true) {
                return;
            }
            if (model.levelPage != model.levelPagesCount) {
                model.levelPage++;
                if (model.levelPage == model.levelPagesCount) {
                    this.btnNext.alpha = .5;
                }
                this.btnPrev.alpha = 1;
                this.moving = true;
                this.btnNext.visible = false;
                this.btnPrev.visible = false;
                var tw = game.add.tween(this.grid).to({
                    x: -this.grid.width * 1.5
                }, model.pageTweenSpeed, Phaser.Easing.Linear.None, true);
                tw.onComplete.add(this.nextPage, this);
            }
        }
        if (call == "prevLevelPage") {
            if (this.moving == true) {
                return;
            }
            if (model.levelPage != 0) {
                model.levelPage--;
                if (model.levelPage == 0) {
                    this.btnPrev.alpha = .5;
                }
                this.btnNext.alpha = 1;
                this.moving = true;
                this.btnNext.visible = false;
                this.btnPrev.visible = false;
                var tw = game.add.tween(this.grid).to({
                    x: game.width
                }, model.pageTweenSpeed, Phaser.Easing.Linear.None, true);
                tw.onComplete.add(this.prevPage, this);
            }
        }
    },
    makeButtons() {
        this.btnNext = new ImageButton("btnNext", "nextLevelPage");
        this.btnPrev = new ImageButton("btnPrev", "prevLevelPage");
        this.btnHome = new ImageButton("btnHome", G.CHANGE_STATE, "StateTitle");
        this.aGrid.placeAtIndex(23, this.btnNext);
        this.aGrid.placeAtIndex(22, this.btnHome);
        this.aGrid.placeAtIndex(21, this.btnPrev);
        this.btnPrev.alpha = .5;
        Align.scaleToGameW(this.btnNext, 0.15625);
        Align.scaleToGameW(this.btnPrev, 0.15625);
        if (model.levels <= model.pageSize) {
            this.btnNext.visible = false;
            this.btnPrev.visible = false;
        }
    },
    nextPage() {
        this.grid.x = game.width + 100;
        this.makePage();
        var tw = game.add.tween(this.grid).to({
            x: this.gridX
        }, model.pageTweenSpeed, Phaser.Easing.Linear.None, true);
        tw.onComplete.add(this.pageStopped, this);
    },
    prevPage() {
        this.grid.x = -this.grid.width - 100;
        this.makePage();
        var tw = game.add.tween(this.grid).to({
            x: this.gridX
        }, model.pageTweenSpeed, Phaser.Easing.Linear.None, true);
        tw.onComplete.add(this.pageStopped, this);
    },
    pageStopped() {
        if (model.levels > model.pageSize) {
            this.btnNext.visible = true;
            this.btnPrev.visible = true;
        }
        this.moving = false;
    }
}
class LevelManager {
    constructor() {
        model.lastUnlocked = parseInt(Saver.getData("lastUnlockedLevel"));
      //  console.log("last unlocked="+model.lastUnlocked);
        if (model.lastUnlocked == undefined || isNaN(model.lastUnlocked)) {
            model.lastUnlocked = 1;
            Saver.save("lastUnlockedLevel", 1);
        }
        eventDispatcher.add(this.gotEvent, this);
    }
    unlockLevel(level) {
        //console.log("level = " + level);
        if (level > model.lastUnlocked) {
            model.lastUnlocked = level;
            //console.log("Unlock " + level);
            Saver.save("lastUnlockedLevel", level);
        }
    }
    gotEvent(call, params) {
        switch (call) {
            case G.UNLOCK_LEVEL:
                this.unlockLevel(params);
                break;
        }
    }
    resetLevels() {
        Saver.save("lastUnlockedLevel", 1);
    }
}
class LockButton extends Phaser.Group {
    constructor(level, s = 50) {
        super(game);
        this.level = level;
        this.buttonBack = game.add.sprite(0, 0, "lockBack");
        this.buttonBack.width = this.buttonBack.width * .7;
        this.buttonBack.height = this.buttonBack.height * .7;
        this.buttonBack.y = this.buttonBack.height / 2;
        this.buttonBack.x = this.buttonBack.width / 2;
        //
        //
        this.buttonBack.anchor.set(0.5, 0.5);
        this.add(this.buttonBack);
        //
        //
        this.locked = game.add.sprite(0, 0, "locked");
        this.locked.width = this.buttonBack.width * .7;
        this.locked.height = this.buttonBack.height * .7;
        this.locked.width = this.buttonBack.width / 2;
        this.locked.scale.y = this.locked.scale.x;
        this.locked.y = this.buttonBack.height / 2;
        this.locked.x = this.buttonBack.width / 2;
        //
        //
        this.locked.anchor.set(0.5, 0.5);
        this.add(this.locked);
        //
        //unlocked
        this.unlocked = game.add.sprite(0, 0, "unlocked");
        this.unlocked.width = this.buttonBack.width * .7;
        this.unlocked.height = this.buttonBack.height * .7;
        this.unlocked.y = this.buttonBack.height / 2;
        this.unlocked.x = this.buttonBack.width / 2;
        this.unlocked.anchor.set(0.5, 0.5);
        this.add(this.unlocked);
        //
        //
        //
        //
        this.buttonBack.inputEnabled = true;
        this.buttonBack.events.onInputUp.add(this.getLevel, this);
        this.buttonBack.events.onInputDown.add(this.onDown, this);
        this.text1 = game.add.text(0, 0, level);
        this.text1.anchor.set(0.5, 0.5);
        if (model.buttonFont != "none") {
            this.text1.font = model.buttonFont;
        }
        //
        //
        //
        //
        // this.text1.visible = false;
        this.text1.x = this.buttonBack.height / 2;
        this.text1.y = this.buttonBack.width / 2;
        this.text1.fill = model.levelColor;
        this.text1.fontSize = model.lockFontSize;
        this.add(this.text1);
    }
    onDown() {
        this.buttonBack.y -= 5;
    }
    getLevel() {
        this.buttonBack.y += 5;
        eventDispatcher.dispatch(G.GET_LEVEL, this.level);
    }
    setLocked(val) {
        if (val == false) {
            this.locked.visible = false;
            this.unlocked.visible = true;
            this.text1.visible = true;
            this.text1.fontSize = model.lockFontSize;
            this.text1.y = this.buttonBack.width / 2;
        } else {
            this.locked.visible = true;
            this.unlocked.visible = false;
            this.text1.fontSize = model.lockFontSize / 2;
            this.text1.y = this.text1.height / 2;
            this.text1.x = this.buttonBack.width - this.text1.width;
            //this.text1.visible = false;
        }
        if (model.showUnlocked == false) {
            this.unlocked.visible = false;
        }
    }
}
class Saver {
    constructor() {}
    static save(key, data) {
        localStorage.setItem(key, data);
    }
    static getData(key) {
        return localStorage.getItem(key);
    }
}
class BaseTheme extends Phaser.Group {
    constructor() {
        super(game);
        this.themeName = "default";
    }
    loadMusicButtons() {
        var path = "images/themes/" + this.themeName + "/soundButtons/";
        game.load.image("musicOn", path + "musicOn.png");
        game.load.image("musicOff", path + "musicOff.png");
        game.load.image("soundOff", path + "soundOff.png");
        game.load.image("soundOn", path + "soundOn.png");
    }
}
class GreenTheme extends BaseTheme {
    constructor() {
        super(game);
        this.themeName = "green";
    }
    loadTheme() {
        this.loadMusicButtons();
        game.load.image("defaultButton", "images/themes/green/buttons/default.png");
        game.load.image("blueButton", "images/themes/green/buttons/blueButton.png");
        game.load.image("textBack", "images/themes/green/buttons/textBack.png");
        game.load.image("panel", "images/themes/green/panels/full.png");
        game.load.image("smallPanel", "images/themes/green/panels/small.png");
        game.load.image("panelHeader", "images/themes/green/panels/header.png");
        //
        //
        //
        //
        //level screen
        //
        game.load.image("locked", "images/themes/green/buttons/locked.png");
        game.load.image("unlocked", "images/themes/green/buttons/unlocked.png");
        game.load.image("lockBack", "images/themes/green/buttons/lockBack.png");
        game.load.image("btnNext", "images/themes/green/buttons/btnNext.png");
        game.load.image("btnPrev", "images/themes/green/buttons/btnPrev.png");
        game.load.image("btnHome", "images/themes/green/buttons/btnHome.png");
        game.load.image("btnHelp", "images/themes/green/buttons/btnHelp.png");
        //  game.load.image("btnLevels", "images/themes/green/buttons/btnLevels.png");
        //game.load.image("btnRedo", "images/themes/green/buttons/btnRedo.png");
        //  game.load.image("btnNextLevel", "images/themes/green/buttons/btnNextLevel.png");
        //
        model.defaultButtonTextColor = "#ffffff";
        model.primaryTextColor = "#ffffff";
        model.secondTextColor = "#000000";
        model.timerBarColor = "#ff0000";
        model.pointTextColor = "#ff0000";
        model.clockColor = "#1BE618FF";
        model.scoreColor = "#1BE618FF";
        model.levelColor = "#ffffff";
        model.progBarColor = 0x1CF707;
        model.timerBarColor = 0xff0000;
        model.toastTextColor = 0xffffff;
        model.toastBarColor = 0xff0000;
        model.mainFont = "Flamenco";
        //
        //
        //
        model.titleFontSize = game.width / 24;
        model.buttonFontSize = game.width / 16;
        model.defaultFontSize = game.width / 30;
        model.scoreFontSize = game.width / 40;
        model.clockFontSize = game.width / 20;
        model.lockFontSize = game.width / 16;
        model.defaultFont = "Flamenco";
    }
}
class MessageBox extends PopUp {
    constructor() {
        super(game);
        this.visible = false;
        var panel = new Panel("panel", .8, .2);
        this.panel = panel;
        var header = new Panel("panelHeader", .8, .1);
        this.add(panel);
        this.add(header);
        panel.y = header.y + header.height;
        //
        //
        //
        this.x = game.width / 2;
        this.y = game.height / 2;
        this.titleText = game.add.text(0, 0, "Title");
        this.titleText.fill = model.secondTextColor;
        this.titleText.fontSize = model.titleFontSize;
        this.titleText.anchor.set(0.5, 0.5);
        // Align.scaleToGameW(this.titleText, .09166);
        // this.titleText.y = -this.back.height / 2 + this.titleText.height;
        //this.titleText.x = -this.back.width / 2 + 15;
        // Align.getScaleToGameW(this.text1);
        this.aGrid = new AlignGrid(5, 11, this);
        this.add(this.aGrid);
        // this.aGrid.showNumbers();
        Align.centerGroup(this);
        this.buttonClose = new CustomTextButton("OK!", "blueButton");
        this.buttonClose.event = G.HIDE_POP_UPS;
        this.add(this.buttonClose);
        this.aGrid.placeAtIndex(47, this.buttonClose);
        this.buttonClose.y += this.buttonClose.height / 2;
        this.add(this.titleText);
        this.aGrid.placeAtIndex(7, this.titleText);
        this.text1 = game.add.text(0, 0, "That Level Is Locked!");
        this.text1.wordWrap = true;
        this.text1.wordWrapWidth = this.width * .75;
        this.text1.fill = model.primaryTextColor;
        //Align.scaleToGameW(this.text1, 0.41875);
        this.text1.anchor.set(0.5, 0.5);
        this.add(this.text1);
        this.aGrid.placeAtIndex(32, this.text1);
        //
        //Align.scaleToGameW(this.buttonClose, .20833);
        // this.buttonClose.x = -this.buttonClose.width / 2;
        // this.buttonClose.y = this.buttonClose.height;
        // 
    }
    gotEvent(call, params) {
        super.gotEvent(call, params);
        switch (call) {
            case G.SHOW_MESSAGE_BOX:
                this.text1.text = params.message;
                this.titleText.text = params.title;
                if (params.fontSize) {
                    //console.log("fontSize2=" + params.fontSize);
                    this.text1.fontSize = params.fontSize;
                }
                this.text1.fill = params.color;
                //this.text1.y = this.panel.height / 2 - this.text1.height / 2;
                this.visible = true;
                break;
        }
    }
}
class WaterRipple extends Phaser.Group {
    constructor() {
        super(game);
        this.graphics = game.add.graphics();
        this.alpha = .8;
        //the elipse width
        this.ww = 1;
        //how much to speed up
        this.inc = 1;
        //add the graphics to the class
        this.add(this.graphics);
        //start the timer
        game.time.events.loop(Phaser.Timer.SECOND / 20, this.updateRipple, this);
    }
    drawRipple() {
        //height is 20% of the width
        this.hh = this.ww / 5;
        //set the thickness and color
        this.graphics.lineStyle(4, 0xE5F4C3);
        //draw the elipse        
        this.graphics.drawEllipse(0, 0, this.ww, this.hh);
    }
    updateRipple() {
        //grow the ripple
        this.ww += this.inc;
        //turn down the alpha
        this.alpha -= .005;
        //clear the graphics
        this.graphics.clear();
        this.drawRipple();
        //if the alpha is faded enough
        //destroy the object
        if (this.alpha < .01) {
            this.destroy(true);
        }
    }
}
class WaterRipples extends Phaser.Group {
    constructor() {
        super(game);
        game.time.events.loop(Phaser.Timer.SECOND * 2, this.addRipple, this);
    }
    addRipple() {
        var r = new WaterRipple();
        this.add(r);
    }
}
class Bubble extends Phaser.Group {
    constructor(xDir = 0, yDir = -1, inc = .005) {
        super(game);
        this.xDir = xDir;
        this.yDir = yDir;
        this.inc = inc;
        this.reset();
        //
        //
        //
        this.graphics = game.add.graphics();
        this.graphics.beginFill(0xffffff, 1);
        this.graphics.drawCircle(0, 0, 50);
        this.graphics.endFill();
        this.add(this.graphics);
    }
    reset() {
        this.speed = 1;
        this.scale.x = game.rnd.integerInRange(1, 100) / 100;
        this.scale.y = this.scale.x;
        this.alpha = game.rnd.integerInRange(1, 60) / 100;
    }
    update() {
        this.x += this.xDir * this.speed;
        this.y += this.yDir * this.speed;
        // this.speed += this.inc;
        //this.alpha-=this.inc;
        if (this.x < 0) {
            //this.destroy(true);
            this.reset();
            this.x = game.width;
        }
    }
}
class Bubbles extends Phaser.Group {
    constructor(w, h) {
        super(game);
        for (var i = 0; i < 20; i++) {
            var bubble = new Bubble(-1, 0, 0.05);
            bubble.x = game.rnd.integerInRange(0, w);
            bubble.y = game.rnd.integerInRange(0, h);
            this.add(bubble);
        }
    }
}
class Zen extends Phaser.Group {
    constructor() {
        super(game);
        var background = new Background("greenBackground");
        this.add(background);
        //
        //
        this.agrid = new AlignGrid(5, 5, this);
        //this.agrid.showNumbers();
        //
        //
        var rocks = new Thumb("rocks1");
        this.agrid.placeAtIndex(24, rocks);
        this.add(rocks);
        //
        //
        var ripple = new WaterRipples();
        this.agrid.placeAtIndex(21, ripple);
        this.add(ripple);
        var bubbles = new Bubbles(game.width, game.height * .2);
        this.add(bubbles);
        //
        //
        var bamboo = game.add.sprite(0, 0, "bamboo");
        bamboo.anchor.set(0, 0.5);
        this.add(bamboo);
        Align.scaleToGameH(bamboo, 1.25);
        Align.centerV(bamboo);
    }
}
class Japan extends Phaser.Group {
    constructor() {
        super(game);
        var back = new Background("japanBack");
        this.add(back);
        var fore = game.add.image(0, 0, "fore");
        fore.anchor.set(0.5, 0);
        if (fore.width < game.width) {
            fore.width = game.width;
            fore.scale.y = fore.scale.x;
        }
        fore.y = game.height - fore.height;
        Align.centerH(fore);
        this.add(fore);
        //
        //
        //
        this.agrid = new AlignGrid(5, 5, this);
        // this.agrid.showNumbers();
        this.add(this.agrid);
        //
        //
        var tree = new Thumb("tree");
        this.agrid.placeAtIndex(13, tree);
        this.add(tree);
        //Align.getScaleToGameW(tree);
        Align.scaleToGameW(tree, 1.1604);
        //
        //
        var snow = new Snow(game.width, game.height * .95);
    }
}
class Field extends Phaser.Group {
    constructor() {
        super(game);
        var bg = game.add.sprite(0, 0, "fieldBack");
        bg.anchor.set(0.5, 0.5);
        Align.scaleToGameH(bg, 1);
        Align.center(bg);
        this.add(bg);
        //
        //
        this.agrid = new AlignGrid(5, 5);
        // this.agrid.showNumbers();
        //
        //
        //
        //
        //
        //
        //
        var crows = new Crows();
        crows.y = game.height * .15;
        crows.x = game.width * .9;
        this.add(crows);
        //
        //
        //
        var clouds = new Clouds();
        clouds.y = game.height * .05;
        clouds.x = game.width * .6;
        this.add(clouds);
        var balloon = new Balloon();
        this.add(balloon);
        var fore = game.add.image(game.width / 2, 0, "fieldFore");
        fore.anchor.set(0.5, 1);
        Align.scaleToGameW(fore, 1);
        fore.y = game.height;
        this.add(fore);
        //
        //
        this.windmill = new WindMill();
        this.add(this.windmill);
        this.agrid.placeAt(0, 3.5, this.windmill);
    }
}
class WindMill extends Phaser.Group {
    constructor() {
        super(game);
        var base = game.add.sprite(0, 0, "base");
        var blades = game.add.sprite(0, 0, "blades");
        base.anchor.set(0.5, 0.5);
        blades.anchor.set(0.5, 0.5);
        this.add(base);
        this.add(blades);
        this.blades = blades;
        this.blades.y -= 20;
        Align.scaleToGameW(this, .235);
    }
    update() {
        this.blades.angle -= .5;
    }
}
class Snow extends Phaser.Group {
    constructor(w = 200, h = 200) {
        super(game);
        this.w = w;
        this.h = h;
        this.lastFlake = null;
        //
        //
        //
        this.makeFlakes();
    }
    makeFlakes() {
        for (var i = 0; i < 100; i++) {
            var flake = new Flake(this.w, this.h);
            //if the last flake exists 
            //place it as the prev flake
            if (this.lastFlake != null) {
                flake.prevFlake = this.lastFlake;
            }
            this.lastFlake = flake;
        }
    }
    update() {
        //move the last flake
        //this will start a chain reactin
        this.lastFlake.move();
    }
}
class Flake extends Phaser.Group {
    constructor(w, h) {
        super(game);
        //make the graphic
        var f = game.add.graphics();
        f.beginFill(0xffffff, 1);
        f.drawCircle(0, 0, 10);
        f.endFill();
        this.add(f);
        //
        //
        this.w = w;
        this.h = h;
        //
        //
        //init properties
        this.y = game.rnd.integerInRange(0, h);
        this.reset();
    }
    reset() {
        //re-init properites
        this.x = game.rnd.integerInRange(0, this.w);
        this.drift = game.rnd.integerInRange(-1, 1) * (.05 + Math.random() * .1);
        this.fallSpeed = 1 + Math.random() / 2;
        this.scale.x = .1 + Math.random();
        this.scale.y = this.scale.x;
        this.alpha = .1 + Math.random();
    }
    move() {
        this.x += this.drift;
        this.y += this.fallSpeed;
        if (this.y > this.h) {
            //take back to top
            this.y = -10;
            this.reset();
        }
        if (this.prevFlake) {
            //move the previous flake
            this.prevFlake.move();
        }
    }
}
class Crow extends Phaser.Group {
    constructor() {
        super(game);
        var image = game.add.sprite(0, 0, "crow");
        image.animations.add("fly", [0, 1, 3, 4, 5, 6], 8, true);
        image.animations.play("fly");
        image.anchor.set(0.5, 0.5);
        this.add(image);
    }
}
class Crows extends Phaser.Group {
    constructor() {
        super(game);
        var xArray = [0, 50, 100];
        var yArray = [0, 30, 60];
        for (var i = 0; i < 3; i++) {
            var crow = new Crow();
            crow.y = yArray[i];
            crow.x = xArray[i];
            this.add(crow);
        }
        this.width = game.width * .05;
        this.scale.y = this.scale.x;
    }
    update() {
        this.x--;
        if (this.x < 0) {
            this.x = game.width * 3;
        }
    }
}
class Clouds extends Phaser.Group {
    constructor() {
        super(game);
        var xArray = [50, 0, 100];
        var yArray = [15, -20, 0];
        for (var i = 0; i < 3; i++) {
            var image = game.add.image(xArray[i], yArray[i], "clouds");
            image.width = game.width * .2;
            image.scale.y = image.scale.x;
            image.frame = i;
            this.add(image);
        }
    }
    update() {
        this.x -= .1;
        if (this.x < 0) {
            this.x = game.width * 2;
        }
    }
}
class Winter extends Phaser.Group {
    constructor() {
        super(game);
        var bg = new Background("winter");
        var snow = new Snow(game.width, game.height * .95);
        var crows = new Crows();
        crows.y = game.height * .15;
        crows.x = game.width * .9;
        this.add(bg);
        this.add(snow);
        this.add(crows);
    }
}
class Plains extends Phaser.Group {
    constructor() {
        super(game);
        var bg = new Background("plains");
        this.add(bg);
        //
        //
        //
        var crows = new Crows();
        crows.y = game.height * .15;
        crows.x = game.width * .9;
        //
        //
        //
        var horse = new Horse();
        horse.y = game.height * .7;
        horse.x = game.width * 6;
        this.add(horse);
        //
        //
        var fore = game.add.image(game.width / 2, 0, "plainsFore");
        fore.anchor.set(0.5, 1);
        Align.scaleToGameW(fore, 1);
        fore.y = game.height;
        this.add(fore);
    }
}
class Horse extends Phaser.Group {
    constructor() {
        super(game);
        var image = game.add.sprite(0, 0, "horseRider");
        image.animations.add("run", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 12, true);
        image.animations.play("run");
        image.width = game.width * .1;
        image.scale.y = image.scale.x;
        this.add(image);
    }
    update() {
        this.x--;
        if (this.x < 0) {
            this.x = game.width * 4;
        }
    }
}
class Balloon extends Phaser.Group {
    constructor() {
        super(game);
        var image = game.add.sprite(0, 0, "balloon");
        image.anchor.set(0.5, 0.5);
        this.add(image);
        this.x = game.width + 100;
        this.y = game.height * .6;
        this.speed = .25;
        this.active = true;
    }
    update() {
        if (this.active == false) {
            return;
        }
        this.x -= this.speed;
        if (this.x > game.width / 2) {
            this.y -= this.speed / 2;
        } else {
            this.x -= this.speed;
        }
        if (this.x < -100) {
            this.visible = false;
            this.x = game.width + 100;
            this.y = game.height * .6;
            game.time.events.add(Phaser.Timer.SECOND * 5, this.resetBalloon, this);
        }
    }
    resetBalloon() {
        this.visible = true;
        this.active = true;
    }
}
class Tile extends Phaser.Group {
    constructor() {
        super(game);
        var back = game.add.sprite(0, 0, "pine");
        back.width = 50;
        back.height = 50;
        back.anchor.set(0.5, 0.5);
        this.back = back;
        this.text1 = game.add.text(0, 0, "A", {
            fill: "#000000"
        });
        this.text1.anchor.set(0.5, 0.5);
        this.add(back);
        this.add(this.text1);
        back.inputEnabled = true;
        back.events.onInputDown.add(this.selectMe, this);
        this.lastColor = "#000000";
    }
    setLetter(letter) {
        this.text1.text = letter;
    }
    getLetter() {
        return this.text1.text;
    }
    selectMe() {
        if (model.clickLock != true) {
            eventDispatcher.dispatch("SELECT_LETTER", this);
        }
    }
    setRed() {
        this.lastColor = this.text1.fill;
        this.text1.fill = "#ff0000";
    }
    resetColor() {
        this.text1.fill = this.lastColor;
    }
    setBlack() {
        this.text1.fill = "#000000";
    }
    setGreen() {
        this.text1.fill = "#186B14";
    }
    turnOffBack() {
        // this.back.visible = false;
    }
    turnOnBack() {
        this.back.visible = true;
    }
    showSparks() {
        var spark = new Sparks(0, 0);
        this.add(spark);
    }
}
class WordDeck {
    constructor() {
        this.levels = [];
        this.setUp();
    }
    pickRand() {
        var words = [];
        var len = model.words.length;
        for (var i = 0; i < 4; i++) {
            var index = game.rnd.integerInRange(0, len);
            words.push(model.words[index]);
        }
        //console.log(words);
        return this.getLetters(words);
    }
    getLevel(level) {
        while (level > 1000) {
            level -= 1000;
        }
       // console.log("LEVEL="+level);
        model.correctWords = this.levels[level];
        return this.getLetters(this.levels[level]);
    }
    getLetters(wordArray) {
        var letters = [];
        for (var i = 0; i < wordArray.length; i++) {
            var word = wordArray[i];
            for (var j = 0; j < word.length; j++) {
                letters.push(word.substr(j, 1));
            }
        }
        letters = this.mixUp(letters);
        //console.log(letters);
        return letters;
    }
    mixUp(letters) {
        for (var i = 0; i < 50; i++) {
            var index1 = game.rnd.integerInRange(0, letters.length - 1);
            var index2 = game.rnd.integerInRange(0, letters.length - 1);
            var temp = letters[index1];
            letters[index1] = letters[index2];
            letters[index2] = temp;
        }
        return letters;
    }
    setUp() {
        this.levels[0] = ["blow", "claw", "fail", "tied"];
        this.levels[1] = ["glow", "coup", "chef", "huts"];
        this.levels[2] = ['fled', 'lull', 'plow', 'hair'];
        this.levels[2] = ['tomb', 'cent', 'wist', 'cuss'];
        this.levels[3] = ['doll', 'nigh', 'peps', 'dime'];
        this.levels[4] = ['colt', 'mass', 'king', 'grip'];
        this.levels[5] = ['polo', 'nice', 'cane', 'wise'];
        this.levels[6] = ['dank', 'sues', 'town', 'tike'];
        this.levels[7] = ['ripe', 'teth', 'item', 'idle'];
        this.levels[8] = ['rids', 'shed', 'yill', 'knee'];
        this.levels[9] = ['chad', 'clip', 'fear', 'girl'];
        this.levels[10] = ['last', 'boss', 'lynx', 'tuck'];
        this.levels[11] = ['pure', 'doff', 'jeli', 'lack'];
        this.levels[12] = ['dram', 'nude', 'dues', 'vain'];
        this.levels[13] = ['bleb', 'spat', 'pile', 'mock'];
        this.levels[14] = ['ropy', 'yare', 'tots', 'emus'];
        this.levels[15] = ['buhl', 'puma', 'polo', 'grub'];
        this.levels[16] = ['rise', 'zoos', 'dens', 'rate'];
        this.levels[17] = ['loll', 'mull', 'draw', 'page'];
        this.levels[18] = ['whop', 'judo', 'wire', 'wren'];
        this.levels[19] = ['acne', 'draw', 'bout', 'blur'];
        this.levels[20] = ['crew', 'aims', 'tums', 'swag'];
        this.levels[21] = ['hobo', 'ides', 'ants', 'malt'];
        this.levels[22] = ['chap', 'loch', 'sear', 'alef'];
        this.levels[23] = ['hick', 'tool', 'shoo', 'prom'];
        this.levels[24] = ['oily', 'paws', 'flit', 'form'];
        this.levels[25] = ['sass', 'gulp', 'bail', 'feat'];
        this.levels[26] = ['foal', 'coed', 'fact', 'itch'];
        this.levels[27] = ['doth', 'owls', 'pius', 'bogs'];
        this.levels[28] = ['yowe', 'swam', 'inky', 'cane'];
        this.levels[29] = ['myna', 'holy', 'flub', 'tomb'];
        this.levels[30] = ['jags', 'hues', 'loci', 'coup'];
        this.levels[31] = ['math', 'keen', 'scot', 'gyms'];
        this.levels[32] = ['grit', 'paps', 'evil', 'flex'];
        this.levels[33] = ['ties', 'male', 'send', 'frog'];
        this.levels[34] = ['make', 'earn', 'king', 'emit'];
        this.levels[35] = ['fray', 'head', 'bean', 'lass'];
        this.levels[36] = ['raft', 'abut', 'sumo', 'soot'];
        this.levels[37] = ['papa', 'scab', 'jock', 'obey'];
        this.levels[38] = ['null', 'pupa', 'cake', 'rags'];
        this.levels[39] = ['moho', 'hood', 'oryx', 'jade'];
        this.levels[40] = ['yird', 'yuck', 'brag', 'pogo'];
        this.levels[41] = ['text', 'bats', 'glad', 'rock'];
        this.levels[42] = ['pars', 'rice', 'luge', 'kyat'];
        this.levels[43] = ['fain', 'loss', 'hams', 'dots'];
        this.levels[44] = ['emir', 'myna', 'brad', 'text'];
        this.levels[45] = ['mitt', 'leap', 'kiln', 'rang'];
        this.levels[46] = ['pawn', 'gift', 'park', 'nibs'];
        this.levels[47] = ['kilt', 'roar', 'goat', 'tipi'];
        this.levels[48] = ['limp', 'sage', 'sues', 'hole'];
        this.levels[49] = ['robs', 'yett', 'slog', 'loin'];
        this.levels[50] = ['omit', 'milk', 'kilt', 'atom'];
        this.levels[51] = ['bows', 'ails', 'meat', 'grid'];
        this.levels[52] = ['redo', 'loco', 'etas', 'feel'];
        this.levels[53] = ['guck', 'nine', 'goad', 'cool'];
        this.levels[54] = ['meow', 'guff', 'zati', 'teen'];
        this.levels[55] = ['dots', 'ilks', 'wigs', 'quad'];
        this.levels[56] = ['toot', 'thus', 'iffy', 'look'];
        this.levels[57] = ['kuna', 'fawn', 'bump', 'porn'];
        this.levels[58] = ['kids', 'seta', 'ewes', 'zeal'];
        this.levels[59] = ['stab', 'vary', 'nips', 'then'];
        this.levels[60] = ['ahas', 'spas', 'flat', 'beta'];
        this.levels[61] = ['apes', 'bare', 'bran', 'zori'];
        this.levels[62] = ['gets', 'sell', 'eddy', 'limo'];
        this.levels[63] = ['tome', 'like', 'jets', 'tone'];
        this.levels[64] = ['cars', 'grip', 'trod', 'yuan'];
        this.levels[65] = ['berm', 'west', 'gaff', 'deft'];
        this.levels[66] = ['cues', 'gosh', 'wifi', 'gift'];
        this.levels[67] = ['hose', 'cots', 'logs', 'rums'];
        this.levels[68] = ['disc', 'limn', 'deem', 'eked'];
        this.levels[69] = ['noun', 'vies', 'sash', 'drug'];
        this.levels[70] = ['yows', 'maws', 'hats', 'lost'];
        this.levels[71] = ['hone', 'weir', 'snap', 'join'];
        this.levels[72] = ['ewes', 'doth', 'dozy', 'then'];
        this.levels[73] = ['must', 'yews', 'hole', 'brat'];
        this.levels[74] = ['sits', 'fumy', 'lots', 'aqua'];
        this.levels[75] = ['thus', 'soot', 'fobs', 'yird'];
        this.levels[76] = ['figs', 'minx', 'yawl', 'sons'];
        this.levels[77] = ['agar', 'clad', 'etas', 'suns'];
        this.levels[78] = ['mash', 'racy', 'hate', 'vamp'];
        this.levels[79] = ['snap', 'sexy', 'ally', 'held'];
        this.levels[80] = ['fall', 'lank', 'just', 'bobs'];
        this.levels[81] = ['waxy', 'hubs', 'gong', 'hobo'];
        this.levels[82] = ['eked', 'plop', 'whop', 'burl'];
        this.levels[83] = ['dean', 'stab', 'yack', 'owly'];
        this.levels[84] = ['drab', 'yerk', 'peck', 'rote'];
        this.levels[85] = ['game', 'scot', 'plan', 'sofa'];
        this.levels[86] = ['what', 'dins', 'east', 'puce'];
        this.levels[87] = ['base', 'woes', 'cast', 'rasp'];
        this.levels[88] = ['coax', 'blog', 'meat', 'rest'];
        this.levels[89] = ['cuds', 'deal', 'crib', 'axes'];
        this.levels[90] = ['onyx', 'halt', 'ergs', 'nave'];
        this.levels[91] = ['huhs', 'dell', 'lull', 'pads'];
        this.levels[92] = ['away', 'smit', 'cool', 'alps'];
        this.levels[93] = ['loam', 'ruby', 'vial', 'city'];
        this.levels[94] = ['dubs', 'bulb', 'yuan', 'step'];
        this.levels[95] = ['dips', 'matt', 'burn', 'hews'];
        this.levels[96] = ['veal', 'birr', 'lone', 'rapt'];
        this.levels[97] = ['flax', 'guts', 'toot', 'birr'];
        this.levels[98] = ['swag', 'kink', 'kays', 'huhs'];
        this.levels[99] = ['lies', 'woke', 'weed', 'yurt'];
        this.levels[100] = ['sake', 'yams', 'muse', 'reel'];
        this.levels[101] = ['moat', 'birr', 'back', 'tipi'];
        this.levels[102] = ['hone', 'hits', 'page', 'glee'];
        this.levels[103] = ['duet', 'lend', 'yaup', 'kern'];
        this.levels[104] = ['menu', 'kips', 'goos', 'bade'];
        this.levels[105] = ['file', 'used', 'kilt', 'limy'];
        this.levels[106] = ['warn', 'vise', 'loos', 'outs'];
        this.levels[107] = ['kelp', 'bond', 'dude', 'scab'];
        this.levels[108] = ['fret', 'mash', 'teas', 'seal'];
        this.levels[109] = ['lend', 'stab', 'bead', 'wool'];
        this.levels[110] = ['jell', 'arty', 'buoy', 'dreg'];
        this.levels[111] = ['cars', 'sots', 'riel', 'tune'];
        this.levels[112] = ['awns', 'deli', 'bass', 'snap'];
        this.levels[113] = ['clog', 'stun', 'vale', 'feel'];
        this.levels[114] = ['vamp', 'self', 'just', 'dawn'];
        this.levels[115] = ['film', 'orbs', 'huhs', 'torn'];
        this.levels[116] = ['seed', 'lack', 'zeta', 'cues'];
        this.levels[117] = ['fins', 'vise', 'with', 'jink'];
        this.levels[118] = ['gong', 'bawd', 'like', 'lite'];
        this.levels[119] = ['vale', 'logo', 'gags', 'revs'];
        this.levels[120] = ['jaws', 'gall', 'polo', 'vale'];
        this.levels[121] = ['turf', 'buff', 'dual', 'jobs'];
        this.levels[122] = ['seen', 'yaff', 'emit', 'claw'];
        this.levels[123] = ['yuck', 'limp', 'keys', 'hell'];
        this.levels[124] = ['bees', 'sift', 'ants', 'yawl'];
        this.levels[125] = ['woos', 'hops', 'romp', 'fibs'];
        this.levels[126] = ['cast', 'land', 'sagy', 'chic'];
        this.levels[127] = ['mitt', 'need', 'some', 'ughs'];
        this.levels[128] = ['rage', 'star', 'call', 'hoed'];
        this.levels[129] = ['loon', 'skew', 'jest', 'yaff'];
        this.levels[130] = ['leks', 'mops', 'west', 'lear'];
        this.levels[131] = ['nevi', 'sect', 'hate', 'enol'];
        this.levels[132] = ['fade', 'kind', 'ween', 'clue'];
        this.levels[133] = ['lurk', 'eked', 'hack', 'logs'];
        this.levels[134] = ['sobs', 'easy', 'till', 'comb'];
        this.levels[135] = ['begs', 'alef', 'comb', 'kyat'];
        this.levels[136] = ['stow', 'rack', 'lane', 'elks'];
        this.levels[137] = ['zarf', 'styx', 'lost', 'toys'];
        this.levels[138] = ['bile', 'main', 'logs', 'dabs'];
        this.levels[139] = ['aril', 'hits', 'cost', 'lose'];
        this.levels[140] = ['glow', 'lost', 'roan', 'reed'];
        this.levels[141] = ['whiz', 'lend', 'toss', 'bops'];
        this.levels[142] = ['glee', 'hook', 'tike', 'moat'];
        this.levels[143] = ['lass', 'tech', 'hogs', 'hued'];
        this.levels[144] = ['sump', 'wars', 'rags', 'limb'];
        this.levels[145] = ['hair', 'dump', 'lack', 'copy'];
        this.levels[146] = ['mood', 'tike', 'welt', 'exon'];
        this.levels[147] = ['hays', 'hour', 'tall', 'celt'];
        this.levels[148] = ['feud', 'what', 'hays', 'pass'];
        this.levels[149] = ['eery', 'blah', 'face', 'beef'];
        this.levels[150] = ['scab', 'wads', 'ruts', 'gape'];
        this.levels[151] = ['sank', 'away', 'loci', 'road'];
        this.levels[152] = ['rice', 'mire', 'skin', 'bowl'];
        this.levels[153] = ['oozy', 'amen', 'flan', 'suet'];
        this.levels[154] = ['vats', 'need', 'eggy', 'roes'];
        this.levels[155] = ['nobs', 'slit', 'heth', 'chew'];
        this.levels[156] = ['bore', 'scum', 'vain', 'wait'];
        this.levels[157] = ['jars', 'bass', 'mast', 'inch'];
        this.levels[158] = ['arch', 'suit', 'liar', 'lags'];
        this.levels[159] = ['tutu', 'pair', 'levs', 'hurl'];
        this.levels[160] = ['lade', 'mild', 'sawn', 'blue'];
        this.levels[161] = ['roes', 'deep', 'yowl', 'thru'];
        this.levels[162] = ['cask', 'posy', 'joke', 'moan'];
        this.levels[163] = ['pray', 'mill', 'sort', 'lust'];
        this.levels[164] = ['snug', 'wawl', 'fray', 'omit'];
        this.levels[165] = ['lent', 'into', 'pros', 'chew'];
        this.levels[166] = ['delf', 'boot', 'brow', 'yeuk'];
        this.levels[167] = ['vets', 'mush', 'goer', 'lava'];
        this.levels[168] = ['bolt', 'vice', 'null', 'kind'];
        this.levels[169] = ['rime', 'saga', 'myna', 'face'];
        this.levels[170] = ['dues', 'cant', 'lobe', 'sera'];
        this.levels[171] = ['paws', 'quad', 'boot', 'fans'];
        this.levels[172] = ['hall', 'limb', 'nuke', 'plan'];
        this.levels[173] = ['ibis', 'clop', 'raft', 'dump'];
        this.levels[174] = ['dune', 'seal', 'oryx', 'toss'];
        this.levels[175] = ['spit', 'turn', 'berk', 'veto'];
        this.levels[176] = ['onto', 'pool', 'four', 'date'];
        this.levels[177] = ['news', 'pods', 'aims', 'axle'];
        this.levels[178] = ['dill', 'wrap', 'jive', 'gnar'];
        this.levels[179] = ['lacy', 'deaf', 'fang', 'hots'];
        this.levels[180] = ['spar', 'isle', 'saps', 'bell'];
        this.levels[181] = ['diva', 'ells', 'neat', 'rids'];
        this.levels[182] = ['club', 'skew', 'yeti', 'city'];
        this.levels[183] = ['node', 'nude', 'race', 'rows'];
        this.levels[184] = ['yews', 'skit', 'urea', 'fond'];
        this.levels[185] = ['gals', 'whoa', 'gory', 'dims'];
        this.levels[186] = ['bank', 'cute', 'edge', 'flog'];
        this.levels[187] = ['crow', 'bets', 'gaga', 'user'];
        this.levels[188] = ['cane', 'ruff', 'sins', 'tool'];
        this.levels[189] = ['tyke', 'rune', 'gnaw', 'mole'];
        this.levels[190] = ['weep', 'curl', 'dubs', 'slur'];
        this.levels[191] = ['dual', 'pops', 'coup', 'vole'];
        this.levels[192] = ['dyer', 'faze', 'hare', 'smit'];
        this.levels[193] = ['webs', 'owly', 'miss', 'saps'];
        this.levels[194] = ['slit', 'sigh', 'waxy', 'path'];
        this.levels[195] = ['sent', 'gnar', 'dung', 'skis'];
        this.levels[196] = ['gash', 'ides', 'push', 'axed'];
        this.levels[197] = ['keen', 'noun', 'yeld', 'sits'];
        this.levels[198] = ['only', 'wire', 'guck', 'soys'];
        this.levels[199] = ['sect', 'buck', 'mans', 'levy'];
        this.levels[200] = ['haws', 'clad', 'rigs', 'plot'];
        this.levels[201] = ['jali', 'pogo', 'pave', 'plug'];
        this.levels[202] = ['hill', 'runs', 'lira', 'gird'];
        this.levels[203] = ['dogs', 'yaud', 'aper', 'anus'];
        this.levels[204] = ['hoer', 'lass', 'kudu', 'okra'];
        this.levels[205] = ['toro', 'stop', 'lint', 'haze'];
        this.levels[206] = ['chat', 'bray', 'grow', 'demy'];
        this.levels[207] = ['coal', 'wawl', 'seta', 'jabs'];
        this.levels[208] = ['help', 'hobo', 'warm', 'tuna'];
        this.levels[209] = ['snog', 'beer', 'next', 'ween'];
        this.levels[210] = ['ties', 'bees', 'waul', 'idea'];
        this.levels[211] = ['mobs', 'yips', 'resh', 'poor'];
        this.levels[212] = ['glib', 'vale', 'buns', 'make'];
        this.levels[213] = ['curs', 'ires', 'trot', 'gong'];
        this.levels[214] = ['moat', 'pegs', 'dreg', 'xyst'];
        this.levels[215] = ['isle', 'cars', 'toed', 'sake'];
        this.levels[216] = ['even', 'ably', 'aims', 'loco'];
        this.levels[217] = ['apex', 'ting', 'hunt', 'ohms'];
        this.levels[218] = ['dire', 'slop', 'ease', 'tofu'];
        this.levels[219] = ['shed', 'haul', 'effs', 'pose'];
        this.levels[220] = ['coho', 'rily', 'pins', 'suet'];
        this.levels[221] = ['guns', 'hued', 'clod', 'raps'];
        this.levels[222] = ['tare', 'mays', 'yeah', 'bulk'];
        this.levels[223] = ['nail', 'rest', 'tern', 'jeep'];
        this.levels[224] = ['agog', 'zoic', 'yoke', 'lieu'];
        this.levels[225] = ['clue', 'anon', 'bris', 'peat'];
        this.levels[226] = ['join', 'toro', 'jazz', 'reef'];
        this.levels[227] = ['teed', 'case', 'prow', 'asks'];
        this.levels[228] = ['tell', 'typo', 'axle', 'loaf'];
        this.levels[229] = ['icer', 'seer', 'fizz', 'idea'];
        this.levels[230] = ['zing', 'spin', 'only', 'peel'];
        this.levels[231] = ['obey', 'plug', 'riff', 'lisp'];
        this.levels[232] = ['play', 'heat', 'swum', 'term'];
        this.levels[233] = ['chis', 'nays', 'rage', 'arts'];
        this.levels[234] = ['melt', 'gnaw', 'limn', 'vary'];
        this.levels[235] = ['spam', 'fads', 'wows', 'snub'];
        this.levels[236] = ['inks', 'kilt', 'gilt', 'mine'];
        this.levels[237] = ['goad', 'liar', 'font', 'lost'];
        this.levels[238] = ['asks', 'lace', 'bore', 'mama'];
        this.levels[239] = ['limb', 'yawn', 'junk', 'weed'];
        this.levels[240] = ['cola', 'butt', 'minx', 'nude'];
        this.levels[241] = ['zori', 'limb', 'chad', 'sulk'];
        this.levels[242] = ['lube', 'curd', 'jams', 'lien'];
        this.levels[243] = ['pile', 'skim', 'chad', 'waft'];
        this.levels[244] = ['till', 'hick', 'lids', 'yang'];
        this.levels[245] = ['chum', 'numb', 'deaf', 'yens'];
        this.levels[246] = ['wins', 'lieu', 'lays', 'snot'];
        this.levels[247] = ['ewes', 'pull', 'ayes', 'sear'];
        this.levels[248] = ['meme', 'dyes', 'chiv', 'reek'];
        this.levels[249] = ['scat', 'slay', 'lain', 'yeuk'];
        this.levels[250] = ['hall', 'hold', 'dust', 'just'];
        this.levels[251] = ['grim', 'posh', 'dabs', 'jack'];
        this.levels[252] = ['cyan', 'rapt', 'lung', 'silo'];
        this.levels[253] = ['suet', 'bean', 'wisp', 'swam'];
        this.levels[254] = ['sane', 'turf', 'roof', 'jogs'];
        this.levels[255] = ['vein', 'scab', 'maze', 'wham'];
        this.levels[256] = ['soak', 'ruts', 'ouzo', 'dark'];
        this.levels[257] = ['hams', 'keys', 'pram', 'lurk'];
        this.levels[258] = ['blue', 'yelp', 'wand', 'sins'];
        this.levels[259] = ['ween', 'wake', 'brad', 'food'];
        this.levels[260] = ['heed', 'heal', 'iris', 'whom'];
        this.levels[261] = ['heed', 'waif', 'oils', 'axis'];
        this.levels[262] = ['hung', 'pour', 'teed', 'kiwi'];
        this.levels[263] = ['jest', 'deep', 'tyke', 'lope'];
        this.levels[264] = ['want', 'oral', 'wind', 'axis'];
        this.levels[265] = ['thou', 'limn', 'sari', 'jail'];
        this.levels[266] = ['rank', 'whim', 'lien', 'leks'];
        this.levels[267] = ['hops', 'most', 'urge', 'moms'];
        this.levels[268] = ['keek', 'jell', 'dent', 'lite'];
        this.levels[269] = ['sour', 'code', 'bugs', 'pips'];
        this.levels[270] = ['whew', 'stow', 'tear', 'tell'];
        this.levels[271] = ['swig', 'ahem', 'came', 'kelp'];
        this.levels[272] = ['ways', 'test', 'crag', 'five'];
        this.levels[273] = ['dose', 'vear', 'read', 'rile'];
        this.levels[274] = ['cost', 'fine', 'wail', 'mown'];
        this.levels[275] = ['rule', 'nape', 'nape', 'dado'];
        this.levels[276] = ['hobo', 'ting', 'thud', 'ires'];
        this.levels[277] = ['brow', 'sent', 'loci', 'pain'];
        this.levels[278] = ['gums', 'gold', 'rods', 'pail'];
        this.levels[279] = ['bowl', 'doge', 'duke', 'shim'];
        this.levels[280] = ['stem', 'last', 'ewes', 'urea'];
        this.levels[281] = ['lady', 'reef', 'male', 'hate'];
        this.levels[282] = ['lung', 'zati', 'duly', 'agog'];
        this.levels[283] = ['wham', 'wisp', 'burp', 'dank'];
        this.levels[284] = ['dims', 'puce', 'hips', 'role'];
        this.levels[285] = ['five', 'safe', 'shoe', 'worm'];
        this.levels[286] = ['pink', 'eave', 'lard', 'sexy'];
        this.levels[287] = ['effs', 'smut', 'dole', 'kips'];
        this.levels[288] = ['nave', 'lame', 'dubs', 'tufa'];
        this.levels[289] = ['nice', 'toad', 'dozy', 'hulk'];
        this.levels[290] = ['sand', 'bleb', 'iris', 'tees'];
        this.levels[291] = ['alto', 'roan', 'melt', 'mums'];
        this.levels[292] = ['vamp', 'then', 'snog', 'hoop'];
        this.levels[293] = ['goat', 'boat', 'vang', 'raid'];
        this.levels[294] = ['span', 'maid', 'bogs', 'fens'];
        this.levels[295] = ['fate', 'sagy', 'best', 'lyre'];
        this.levels[296] = ['dews', 'talk', 'begs', 'eely'];
        this.levels[297] = ['nape', 'hype', 'sops', 'stab'];
        this.levels[298] = ['task', 'mist', 'leus', 'baht'];
        this.levels[299] = ['yoga', 'that', 'tail', 'gait'];
        this.levels[300] = ['lawn', 'icky', 'bees', 'keek'];
        this.levels[301] = ['fame', 'suck', 'vote', 'slew'];
        this.levels[302] = ['tear', 'crow', 'oats', 'crud'];
        this.levels[303] = ['crag', 'bark', 'sped', 'woke'];
        this.levels[304] = ['rill', 'nips', 'snap', 'yods'];
        this.levels[305] = ['joke', 'gell', 'emit', 'oink'];
        this.levels[306] = ['stud', 'hive', 'into', 'peps'];
        this.levels[307] = ['suer', 'hall', 'zits', 'kays'];
        this.levels[308] = ['vise', 'gong', 'drew', 'toll'];
        this.levels[309] = ['begs', 'wall', 'spat', 'emit'];
        this.levels[310] = ['seta', 'dell', 'wove', 'jump'];
        this.levels[311] = ['what', 'sewn', 'inks', 'sirs'];
        this.levels[312] = ['body', 'rein', 'hick', 'boor'];
        this.levels[313] = ['cure', 'pain', 'nits', 'self'];
        this.levels[314] = ['prim', 'ooid', 'duck', 'oars'];
        this.levels[315] = ['mats', 'effs', 'axon', 'wide'];
        this.levels[316] = ['dune', 'plug', 'ergs', 'pros'];
        this.levels[317] = ['seat', 'hump', 'neap', 'both'];
        this.levels[318] = ['mind', 'ones', 'dodo', 'blab'];
        this.levels[319] = ['nuns', 'tact', 'user', 'worn'];
        this.levels[320] = ['grab', 'defy', 'teth', 'nose'];
        this.levels[321] = ['soak', 'firm', 'slim', 'flax'];
        this.levels[322] = ['honk', 'dead', 'voes', 'wage'];
        this.levels[323] = ['burp', 'loti', 'yams', 'sure'];
        this.levels[324] = ['jeli', 'glia', 'cord', 'teal'];
        this.levels[325] = ['fort', 'etas', 'disk', 'gave'];
        this.levels[326] = ['dine', 'heel', 'gnaw', 'boys'];
        this.levels[327] = ['bulb', 'figs', 'etch', 'mate'];
        this.levels[328] = ['lent', 'bawl', 'deed', 'kept'];
        this.levels[329] = ['fish', 'gene', 'tiff', 'deme'];
        this.levels[330] = ['yutz', 'coil', 'left', 'peal'];
        this.levels[331] = ['nest', 'claw', 'shun', 'bide'];
        this.levels[332] = ['loci', 'hole', 'whey', 'hoof'];
        this.levels[333] = ['ruse', 'life', 'rove', 'vows'];
        this.levels[334] = ['scan', 'marl', 'glut', 'load'];
        this.levels[335] = ['lost', 'brag', 'link', 'last'];
        this.levels[336] = ['lieu', 'fund', 'icky', 'rage'];
        this.levels[337] = ['ploy', 'sari', 'stag', 'levs'];
        this.levels[338] = ['lash', 'lips', 'view', 'rigs'];
        this.levels[339] = ['verb', 'cole', 'josh', 'rick'];
        this.levels[340] = ['kaph', 'sung', 'omit', 'game'];
        this.levels[341] = ['arms', 'bogs', 'burs', 'twig'];
        this.levels[342] = ['coop', 'baht', 'disc', 'face'];
        this.levels[343] = ['slay', 'fool', 'onus', 'slot'];
        this.levels[344] = ['hath', 'wave', 'look', 'foal'];
        this.levels[345] = ['type', 'meat', 'pier', 'chub'];
        this.levels[346] = ['suds', 'digs', 'draw', 'cost'];
        this.levels[347] = ['byes', 'mica', 'yins', 'rigs'];
        this.levels[348] = ['exit', 'pelf', 'card', 'palm'];
        this.levels[349] = ['doge', 'bees', 'hers', 'dupe'];
        this.levels[350] = ['kick', 'wise', 'wons', 'hypo'];
        this.levels[351] = ['wart', 'toss', 'nope', 'leap'];
        this.levels[352] = ['camp', 'ware', 'lave', 'firm'];
        this.levels[353] = ['abet', 'repo', 'goes', 'cups'];
        this.levels[354] = ['awls', 'flux', 'idle', 'zarf'];
        this.levels[355] = ['dice', 'foot', 'hums', 'blob'];
        this.levels[356] = ['wive', 'step', 'mead', 'floe'];
        this.levels[357] = ['fans', 'jamb', 'mail', 'woks'];
        this.levels[358] = ['owes', 'duds', 'nope', 'warm'];
        this.levels[359] = ['wimp', 'sump', 'jute', 'mica'];
        this.levels[360] = ['serf', 'flux', 'gaff', 'pits'];
        this.levels[361] = ['zero', 'eave', 'vows', 'nosy'];
        this.levels[362] = ['clef', 'sexy', 'lead', 'derm'];
        this.levels[363] = ['cots', 'jowl', 'reel', 'taro'];
        this.levels[364] = ['mark', 'haze', 'stem', 'gone'];
        this.levels[365] = ['deme', 'cold', 'meek', 'bale'];
        this.levels[366] = ['plod', 'troy', 'bits', 'sued'];
        this.levels[367] = ['tens', 'clip', 'cups', 'yews'];
        this.levels[368] = ['eyes', 'aunt', 'aper', 'high'];
        this.levels[369] = ['toad', 'coot', 'haws', 'hats'];
        this.levels[370] = ['cabs', 'once', 'icky', 'slim'];
        this.levels[371] = ['zeta', 'wild', 'tone', 'xyst'];
        this.levels[372] = ['yang', 'form', 'yagi', 'side'];
        this.levels[373] = ['zing', 'ever', 'jail', 'marl'];
        this.levels[374] = ['mugs', 'vein', 'know', 'blur'];
        this.levels[375] = ['papa', 'tear', 'slob', 'soil'];
        this.levels[376] = ['pits', 'cope', 'path', 'live'];
        this.levels[377] = ['ouch', 'kiss', 'reds', 'mask'];
        this.levels[378] = ['yard', 'star', 'dots', 'sake'];
        this.levels[379] = ['rime', 'talc', 'voes', 'laud'];
        this.levels[380] = ['flea', 'owly', 'zero', 'myth'];
        this.levels[381] = ['quid', 'sale', 'days', 'hear'];
        this.levels[382] = ['grid', 'fume', 'ride', 'runs'];
        this.levels[383] = ['fall', 'volt', 'fair', 'yond'];
        this.levels[384] = ['vane', 'pomp', 'bold', 'yipe'];
        this.levels[385] = ['dove', 'buys', 'used', 'bash'];
        this.levels[386] = ['redo', 'hugs', 'vein', 'beef'];
        this.levels[387] = ['flub', 'bonk', 'tank', 'grab'];
        this.levels[388] = ['lick', 'flox', 'vary', 'saps'];
        this.levels[389] = ['dumb', 'ogle', 'swim', 'oils'];
        this.levels[390] = ['adze', 'blip', 'sync', 'seat'];
        this.levels[391] = ['snit', 'hark', 'pawn', 'mold'];
        this.levels[392] = ['area', 'mark', 'jogs', 'tech'];
        this.levels[393] = ['rite', 'cops', 'yegg', 'yard'];
        this.levels[394] = ['foes', 'haps', 'late', 'skew'];
        this.levels[395] = ['sell', 'rule', 'pent', 'oohs'];
        this.levels[396] = ['molt', 'grit', 'raid', 'flip'];
        this.levels[397] = ['post', 'sums', 'good', 'dear'];
        this.levels[398] = ['mode', 'dust', 'swim', 'clap'];
        this.levels[399] = ['date', 'days', 'tier', 'lira'];
        this.levels[400] = ['aunt', 'sing', 'poem', 'bite'];
        this.levels[401] = ['bind', 'nick', 'wasp', 'epic'];
        this.levels[402] = ['sirs', 'rial', 'veal', 'thus'];
        this.levels[403] = ['boat', 'sang', 'form', 'dubs'];
        this.levels[404] = ['lobs', 'slog', 'gobs', 'doll'];
        this.levels[405] = ['wyes', 'jays', 'rush', 'corm'];
        this.levels[406] = ['wove', 'shoe', 'revs', 'vibe'];
        this.levels[407] = ['lack', 'hive', 'xyst', 'spin'];
        this.levels[408] = ['wake', 'holy', 'scab', 'read'];
        this.levels[409] = ['gave', 'haws', 'sera', 'vile'];
        this.levels[410] = ['calm', 'inns', 'done', 'fuss'];
        this.levels[411] = ['folk', 'cape', 'test', 'duet'];
        this.levels[412] = ['gall', 'odes', 'tone', 'lute'];
        this.levels[413] = ['clay', 'hymn', 'zein', 'lout'];
        this.levels[414] = ['else', 'poxy', 'kink', 'balm'];
        this.levels[415] = ['outs', 'wood', 'wind', 'lock'];
        this.levels[416] = ['quay', 'hits', 'rise', 'race'];
        this.levels[417] = ['jowl', 'mend', 'kuna', 'will'];
        this.levels[418] = ['grey', 'lord', 'hunt', 'clan'];
        this.levels[419] = ['hind', 'tune', 'fate', 'ovum'];
        this.levels[420] = ['acne', 'sitz', 'feel', 'dean'];
        this.levels[421] = ['demy', 'shim', 'chef', 'dues'];
        this.levels[422] = ['imam', 'whet', 'bets', 'nerd'];
        this.levels[423] = ['peep', 'vela', 'spit', 'muse'];
        this.levels[424] = ['fury', 'sere', 'moos', 'awes'];
        this.levels[425] = ['jeep', 'bilk', 'arid', 'guys'];
        this.levels[426] = ['kind', 'yang', 'lane', 'grey'];
        this.levels[427] = ['goop', 'alms', 'mutt', 'halo'];
        this.levels[428] = ['nobs', 'glad', 'moor', 'ears'];
        this.levels[429] = ['caps', 'line', 'coed', 'gill'];
        this.levels[430] = ['kiwi', 'offs', 'nerd', 'lark'];
        this.levels[431] = ['mash', 'duet', 'mild', 'zoom'];
        this.levels[432] = ['such', 'loll', 'feat', 'slew'];
        this.levels[433] = ['flue', 'zebu', 'lift', 'chum'];
        this.levels[434] = ['jams', 'spot', 'hull', 'wavy'];
        this.levels[435] = ['cuts', 'meat', 'said', 'limy'];
        this.levels[436] = ['alto', 'fact', 'rind', 'pray'];
        this.levels[437] = ['wawl', 'oink', 'auto', 'soon'];
        this.levels[438] = ['harp', 'pang', 'wile', 'sagy'];
        this.levels[439] = ['fond', 'waul', 'coda', 'suer'];
        this.levels[440] = ['deny', 'park', 'toff', 'ugly'];
        this.levels[441] = ['aril', 'flat', 'stir', 'fool'];
        this.levels[442] = ['fell', 'ally', 'doer', 'tums'];
        this.levels[443] = ['twig', 'iris', 'slit', 'ywis'];
        this.levels[444] = ['goon', 'near', 'sang', 'land'];
        this.levels[445] = ['owed', 'beta', 'spar', 'pius'];
        this.levels[446] = ['cola', 'akin', 'deme', 'hers'];
        this.levels[447] = ['wifi', 'zoic', 'perk', 'acid'];
        this.levels[448] = ['kook', 'busk', 'glob', 'hewn'];
        this.levels[449] = ['pods', 'wist', 'ahem', 'tame'];
        this.levels[450] = ['zips', 'owly', 'uses', 'done'];
        this.levels[451] = ['leak', 'quip', 'ribs', 'took'];
        this.levels[452] = ['mere', 'eons', 'pity', 'pile'];
        this.levels[453] = ['corn', 'watt', 'kelp', 'told'];
        this.levels[454] = ['styx', 'soil', 'gawk', 'hubs'];
        this.levels[455] = ['late', 'kink', 'yurt', 'nevi'];
        this.levels[456] = ['oafs', 'yond', 'purr', 'ebbs'];
        this.levels[457] = ['clan', 'zees', 'bond', 'saws'];
        this.levels[458] = ['oozy', 'nevi', 'lady', 'wall'];
        this.levels[459] = ['weed', 'gala', 'lode', 'came'];
        this.levels[460] = ['ouzo', 'fuss', 'java', 'deaf'];
        this.levels[461] = ['odes', 'cook', 'woof', 'lame'];
        this.levels[462] = ['guns', 'wild', 'gate', 'snug'];
        this.levels[463] = ['toga', 'feed', 'dreg', 'lute'];
        this.levels[464] = ['okay', 'duff', 'umbo', 'lest'];
        this.levels[465] = ['lout', 'ilka', 'awol', 'anew'];
        this.levels[466] = ['bids', 'vile', 'uric', 'zoom'];
        this.levels[467] = ['fare', 'gift', 'crow', 'seem'];
        this.levels[468] = ['moho', 'says', 'birr', 'burp'];
        this.levels[469] = ['unit', 'shoe', 'grim', 'josh'];
        this.levels[470] = ['sikh', 'lean', 'deft', 'muck'];
        this.levels[471] = ['chiv', 'lazy', 'glob', 'tags'];
        this.levels[472] = ['left', 'them', 'yoni', 'bolt'];
        this.levels[473] = ['sect', 'diet', 'torn', 'rank'];
        this.levels[474] = ['jeep', 'peat', 'dire', 'lade'];
        this.levels[475] = ['lens', 'rips', 'robs', 'send'];
        this.levels[476] = ['hazy', 'mole', 'owns', 'ills'];
        this.levels[477] = ['best', 'sash', 'page', 'slob'];
        this.levels[478] = ['bang', 'held', 'whiz', 'rice'];
        this.levels[479] = ['flan', 'awry', 'game', 'mall'];
        this.levels[480] = ['dump', 'tend', 'wist', 'bins'];
        this.levels[481] = ['tofu', 'hear', 'nets', 'root'];
        this.levels[482] = ['poke', 'rock', 'host', 'flip'];
        this.levels[483] = ['muse', 'lead', 'chef', 'cede'];
        this.levels[484] = ['bead', 'saga', 'ahoy', 'boat'];
        this.levels[485] = ['care', 'boot', 'sell', 'loot'];
        this.levels[486] = ['dorm', 'pods', 'nags', 'down'];
        this.levels[487] = ['fume', 'zany', 'curd', 'numb'];
        this.levels[488] = ['toes', 'done', 'solo', 'kale'];
        this.levels[489] = ['tied', 'visa', 'cogs', 'cats'];
        this.levels[490] = ['sell', 'czar', 'kuna', 'loch'];
        this.levels[491] = ['many', 'over', 'jive', 'tugs'];
        this.levels[492] = ['peso', 'flop', 'foes', 'chis'];
        this.levels[493] = ['bang', 'swan', 'silk', 'oops'];
        this.levels[494] = ['etch', 'form', 'bean', 'bush'];
        this.levels[495] = ['dime', 'gals', 'vent', 'anal'];
        this.levels[496] = ['hear', 'work', 'lust', 'cwms'];
        this.levels[497] = ['rots', 'tail', 'orbs', 'vise'];
        this.levels[498] = ['rule', 'ajar', 'hoot', 'bilk'];
        this.levels[499] = ['gyre', 'byes', 'orbs', 'afro'];
        this.levels[500] = ['swag', 'moat', 'snug', 'oust'];
        this.levels[501] = ['mils', 'kaph', 'jolt', 'sown'];
        this.levels[502] = ['snub', 'anon', 'polo', 'soup'];
        this.levels[503] = ['meow', 'vang', 'crop', 'will'];
        this.levels[504] = ['elks', 'come', 'demy', 'bled'];
        this.levels[505] = ['used', 'hire', 'kyat', 'moon'];
        this.levels[506] = ['wink', 'raft', 'crud', 'deys'];
        this.levels[507] = ['hems', 'ping', 'baht', 'heal'];
        this.levels[508] = ['pita', 'guts', 'geek', 'veer'];
        this.levels[509] = ['ropy', 'item', 'rend', 'cock'];
        this.levels[510] = ['bins', 'wavy', 'hurl', 'wife'];
        this.levels[511] = ['rake', 'leas', 'coil', 'pigs'];
        this.levels[512] = ['able', 'coed', 'cowl', 'belt'];
        this.levels[513] = ['agog', 'mole', 'cops', 'oars'];
        this.levels[514] = ['curt', 'sink', 'guts', 'area'];
        this.levels[515] = ['laid', 'hums', 'lead', 'seas'];
        this.levels[516] = ['gobs', 'mess', 'rued', 'nods'];
        this.levels[517] = ['skin', 'skin', 'tart', 'toot'];
        this.levels[518] = ['ebbs', 'dill', 'anew', 'lamp'];
        this.levels[519] = ['rigs', 'jags', 'acts', 'rich'];
        this.levels[520] = ['pike', 'mall', 'abut', 'jogs'];
        this.levels[521] = ['zyme', 'bony', 'saki', 'zerk'];
        this.levels[522] = ['fern', 'duos', 'ired', 'earl'];
        this.levels[523] = ['jabs', 'mere', 'silk', 'look'];
        this.levels[524] = ['vela', 'bops', 'ably', 'pelt'];
        this.levels[525] = ['ride', 'lord', 'thru', 'yaks'];
        this.levels[526] = ['fine', 'tied', 'mask', 'gyps'];
        this.levels[527] = ['oryx', 'scat', 'scab', 'clef'];
        this.levels[528] = ['clog', 'fake', 'rubs', 'cube'];
        this.levels[529] = ['tore', 'acre', 'deal', 'slip'];
        this.levels[530] = ['ends', 'best', 'gunk', 'suit'];
        this.levels[531] = ['etas', 'ayin', 'wham', 'pelf'];
        this.levels[532] = ['guff', 'anus', 'cost', 'band'];
        this.levels[533] = ['loud', 'owes', 'saws', 'junk'];
        this.levels[534] = ['deft', 'tufa', 'vamp', 'pear'];
        this.levels[535] = ['hens', 'kegs', 'gulf', 'woof'];
        this.levels[536] = ['mass', 'hoar', 'garb', 'fish'];
        this.levels[537] = ['pact', 'form', 'mace', 'whew'];
        this.levels[538] = ['brat', 'flaw', 'coys', 'skip'];
        this.levels[539] = ['xyst', 'ween', 'yoke', 'cees'];
        this.levels[540] = ['pull', 'goad', 'deer', 'thus'];
        this.levels[541] = ['date', 'bode', 'loud', 'flan'];
        this.levels[542] = ['yipe', 'fizz', 'coda', 'fats'];
        this.levels[543] = ['alum', 'mage', 'tape', 'code'];
        this.levels[544] = ['slot', 'true', 'smit', 'sink'];
        this.levels[545] = ['duds', 'yank', 'roll', 'leas'];
        this.levels[546] = ['tabs', 'toys', 'thin', 'buds'];
        this.levels[547] = ['mold', 'dele', 'veer', 'acts'];
        this.levels[548] = ['toff', 'alas', 'both', 'aped'];
        this.levels[549] = ['saps', 'whom', 'coat', 'lots'];
        this.levels[550] = ['pits', 'bask', 'feel', 'flea'];
        this.levels[551] = ['miss', 'xyst', 'deil', 'robe'];
        this.levels[552] = ['vole', 'seen', 'cash', 'perk'];
        this.levels[553] = ['rook', 'adds', 'sold', 'hips'];
        this.levels[554] = ['jest', 'taus', 'ills', 'stew'];
        this.levels[555] = ['acts', 'tail', 'rhos', 'fade'];
        this.levels[556] = ['weir', 'bike', 'trek', 'nobs'];
        this.levels[557] = ['crop', 'cull', 'pouf', 'brew'];
        this.levels[558] = ['damp', 'doer', 'feat', 'raft'];
        this.levels[559] = ['whup', 'snob', 'jeer', 'pony'];
        this.levels[560] = ['wand', 'zein', 'grab', 'took'];
        this.levels[561] = ['blue', 'jell', 'seam', 'psis'];
        this.levels[562] = ['veal', 'cyan', 'thaw', 'umps'];
        this.levels[563] = ['gown', 'taxi', 'heck', 'tzar'];
        this.levels[564] = ['trio', 'luau', 'meet', 'lope'];
        this.levels[565] = ['leaf', 'slam', 'bait', 'bibs'];
        this.levels[566] = ['omen', 'sand', 'club', 'kink'];
        this.levels[567] = ['dons', 'your', 'mens', 'yarn'];
        this.levels[568] = ['span', 'palm', 'junk', 'lice'];
        this.levels[569] = ['gimp', 'dots', 'news', 'gilt'];
        this.levels[570] = ['ergs', 'ukes', 'corm', 'grit'];
        this.levels[571] = ['mops', 'dual', 'wads', 'undo'];
        this.levels[572] = ['rein', 'gape', 'bawl', 'spam'];
        this.levels[573] = ['poem', 'sews', 'game', 'jags'];
        this.levels[574] = ['tufa', 'pimp', 'unto', 'oily'];
        this.levels[575] = ['hare', 'mugs', 'main', 'posy'];
        this.levels[576] = ['mull', 'oxes', 'ibis', 'lave'];
        this.levels[577] = ['keep', 'hoed', 'feta', 'halt'];
        this.levels[578] = ['levy', 'yips', 'grey', 'dads'];
        this.levels[579] = ['ping', 'yowl', 'goes', 'feat'];
        this.levels[580] = ['hoop', 'loin', 'lava', 'less'];
        this.levels[581] = ['riel', 'buns', 'kudu', 'clip'];
        this.levels[582] = ['axis', 'vote', 'main', 'buoy'];
        this.levels[583] = ['hoax', 'love', 'herd', 'rues'];
        this.levels[584] = ['dees', 'yips', 'ells', 'tala'];
        this.levels[585] = ['josh', 'wavy', 'thug', 'sins'];
        this.levels[586] = ['noon', 'kern', 'idea', 'hoed'];
        this.levels[587] = ['ooze', 'camp', 'pews', 'amid'];
        this.levels[588] = ['worn', 'bard', 'seat', 'mats'];
        this.levels[589] = ['peso', 'taxi', 'lays', 'easy'];
        this.levels[590] = ['blob', 'nape', 'ajar', 'aahs'];
        this.levels[591] = ['five', 'tala', 'buns', 'cram'];
        this.levels[592] = ['rent', 'rued', 'yaks', 'nave'];
        this.levels[593] = ['fora', 'tens', 'frog', 'moot'];
        this.levels[594] = ['most', 'five', 'gown', 'yams'];
        this.levels[595] = ['jowl', 'pest', 'spay', 'cola'];
        this.levels[596] = ['hypo', 'yuga', 'veal', 'wisp'];
        this.levels[597] = ['juke', 'wife', 'hall', 'rite'];
        this.levels[598] = ['zest', 'okay', 'hoop', 'feet'];
        this.levels[599] = ['bans', 'mayo', 'roof', 'yodh'];
        this.levels[600] = ['bitt', 'tent', 'mugs', 'racy'];
        this.levels[601] = ['awed', 'chiv', 'pert', 'roam'];
        this.levels[602] = ['iced', 'loco', 'cool', 'ware'];
        this.levels[603] = ['clop', 'make', 'they', 'pooh'];
        this.levels[604] = ['ruby', 'fold', 'weld', 'sell'];
        this.levels[605] = ['bust', 'java', 'cant', 'much'];
        this.levels[606] = ['smog', 'zees', 'pins', 'wade'];
        this.levels[607] = ['rick', 'orca', 'dibs', 'cull'];
        this.levels[608] = ['gods', 'good', 'ante', 'crop'];
        this.levels[609] = ['pork', 'spas', 'tans', 'eked'];
        this.levels[610] = ['cell', 'blur', 'teem', 'ergs'];
        this.levels[611] = ['runt', 'slim', 'sits', 'meld'];
        this.levels[612] = ['waif', 'yill', 'taut', 'watt'];
        this.levels[613] = ['pugs', 'film', 'shwa', 'slim'];
        this.levels[614] = ['kine', 'sirs', 'side', 'wend'];
        this.levels[615] = ['drug', 'bail', 'mums', 'sand'];
        this.levels[616] = ['laws', 'dodo', 'heal', 'apps'];
        this.levels[617] = ['code', 'term', 'view', 'cook'];
        this.levels[618] = ['safe', 'bags', 'hype', 'work'];
        this.levels[619] = ['debt', 'into', 'naut', 'smut'];
        this.levels[620] = ['yuck', 'asks', 'pars', 'lens'];
        this.levels[621] = ['bots', 'lows', 'film', 'herd'];
        this.levels[622] = ['your', 'want', 'shut', 'give'];
        this.levels[623] = ['pawn', 'tout', 'soft', 'quip'];
        this.levels[624] = ['leks', 'kyak', 'oath', 'farm'];
        this.levels[625] = ['mace', 'fads', 'toes', 'kale'];
        this.levels[626] = ['disc', 'holy', 'team', 'awed'];
        this.levels[627] = ['mart', 'jeep', 'ears', 'inch'];
        this.levels[628] = ['ween', 'laws', 'fend', 'hunk'];
        this.levels[629] = ['hung', 'burp', 'puck', 'grin'];
        this.levels[630] = ['hula', 'king', 'pile', 'bots'];
        this.levels[631] = ['urea', 'best', 'vast', 'zeal'];
        this.levels[632] = ['hags', 'bile', 'racy', 'troy'];
        this.levels[633] = ['sext', 'bank', 'raze', 'boys'];
        this.levels[634] = ['muff', 'bred', 'cole', 'lode'];
        this.levels[635] = ['sagy', 'stew', 'smit', 'ires'];
        this.levels[636] = ['self', 'figs', 'spay', 'ribs'];
        this.levels[637] = ['none', 'bone', 'ween', 'reds'];
        this.levels[638] = ['rapt', 'grub', 'hoer', 'ebbs'];
        this.levels[639] = ['awns', 'plea', 'turn', 'ramp'];
        this.levels[640] = ['bust', 'berk', 'plug', 'tomb'];
        this.levels[641] = ['tote', 'drag', 'horn', 'chap'];
        this.levels[642] = ['slur', 'deer', 'gold', 'girn'];
        this.levels[643] = ['sumo', 'nest', 'dims', 'sold'];
        this.levels[644] = ['kiln', 'lags', 'dive', 'herb'];
        this.levels[645] = ['pets', 'kiln', 'beta', 'ired'];
        this.levels[646] = ['then', 'peek', 'adze', 'ship'];
        this.levels[647] = ['bear', 'okra', 'scan', 'seek'];
        this.levels[648] = ['seal', 'keek', 'lire', 'dues'];
        this.levels[649] = ['toes', 'sick', 'yaud', 'mine'];
        this.levels[650] = ['tune', 'pool', 'hums', 'mend'];
        this.levels[651] = ['slid', 'rind', 'loon', 'moat'];
        this.levels[652] = ['aids', 'airy', 'lira', 'jeep'];
        this.levels[653] = ['musk', 'cusp', 'boat', 'lush'];
        this.levels[654] = ['twos', 'file', 'luff', 'polo'];
        this.levels[655] = ['wrap', 'wade', 'lent', 'deck'];
        this.levels[656] = ['eras', 'days', 'dish', 'yaks'];
        this.levels[657] = ['runt', 'rage', 'jali', 'gins'];
        this.levels[658] = ['tint', 'loam', 'gods', 'buts'];
        this.levels[659] = ['pale', 'doth', 'punt', 'putt'];
        this.levels[660] = ['lags', 'wins', 'pled', 'they'];
        this.levels[661] = ['hats', 'fire', 'yens', 'yond'];
        this.levels[662] = ['sows', 'free', 'tugs', 'wane'];
        this.levels[663] = ['sail', 'pall', 'blur', 'abet'];
        this.levels[664] = ['pick', 'full', 'sown', 'bird'];
        this.levels[665] = ['lead', 'yean', 'seem', 'lime'];
        this.levels[666] = ['risk', 'rind', 'cods', 'berm'];
        this.levels[667] = ['gets', 'bubo', 'rood', 'czar'];
        this.levels[668] = ['slid', 'evil', 'pyre', 'poem'];
        this.levels[669] = ['axel', 'ship', 'save', 'tale'];
        this.levels[670] = ['odes', 'rigs', 'cent', 'nags'];
        this.levels[671] = ['hint', 'rash', 'nazi', 'demo'];
        this.levels[672] = ['ulna', 'soya', 'faux', 'nail'];
        this.levels[673] = ['bobs', 'pink', 'wish', 'ally'];
        this.levels[674] = ['july', 'fold', 'taps', 'hurt'];
        this.levels[675] = ['vial', 'hate', 'pimp', 'bard'];
        this.levels[676] = ['year', 'whim', 'norm', 'sway'];
        this.levels[677] = ['ooid', 'pair', 'hike', 'balm'];
        this.levels[678] = ['eons', 'lids', 'ever', 'nude'];
        this.levels[679] = ['vees', 'chip', 'wave', 'shoe'];
        this.levels[680] = ['amen', 'puns', 'pads', 'jest'];
        this.levels[681] = ['nobs', 'rail', 'aper', 'weak'];
        this.levels[682] = ['atom', 'expo', 'hues', 'bull'];
        this.levels[683] = ['hazy', 'zone', 'jowl', 'fury'];
        this.levels[684] = ['awny', 'zigs', 'hang', 'heme'];
        this.levels[685] = ['ante', 'teen', 'urge', 'pods'];
        this.levels[686] = ['soap', 'pest', 'euro', 'yerk'];
        this.levels[687] = ['four', 'wore', 'corn', 'ribs'];
        this.levels[688] = ['arch', 'wads', 'tail', 'prop'];
        this.levels[689] = ['wifi', 'boxy', 'yird', 'doll'];
        this.levels[690] = ['lush', 'ooze', 'tamp', 'deck'];
        this.levels[691] = ['rife', 'aged', 'buds', 'wets'];
        this.levels[692] = ['yows', 'jink', 'airs', 'fora'];
        this.levels[693] = ['glia', 'kine', 'flap', 'path'];
        this.levels[694] = ['digs', 'cord', 'caws', 'scum'];
        this.levels[695] = ['nibs', 'cope', 'faux', 'airs'];
        this.levels[696] = ['visa', 'mesh', 'ward', 'fill'];
        this.levels[697] = ['dawn', 'ugly', 'spur', 'tots'];
        this.levels[698] = ['toes', 'gait', 'clot', 'pled'];
        this.levels[699] = ['void', 'huge', 'poke', 'whim'];
        this.levels[700] = ['spot', 'earn', 'moan', 'ooze'];
        this.levels[701] = ['luff', 'jerk', 'mold', 'yank'];
        this.levels[702] = ['list', 'bath', 'trod', 'typo'];
        this.levels[703] = ['rots', 'ruts', 'eons', 'rime'];
        this.levels[704] = ['puke', 'meet', 'tzar', 'fall'];
        this.levels[705] = ['foul', 'barm', 'lugs', 'verb'];
        this.levels[706] = ['boil', 'hurl', 'glob', 'lens'];
        this.levels[707] = ['zips', 'cops', 'beta', 'gasp'];
        this.levels[708] = ['loom', 'jack', 'wisp', 'fend'];
        this.levels[709] = ['mute', 'slop', 'rats', 'rave'];
        this.levels[710] = ['pooh', 'gods', 'lope', 'icon'];
        this.levels[711] = ['scat', 'race', 'buoy', 'size'];
        this.levels[712] = ['form', 'vast', 'saga', 'peep'];
        this.levels[713] = ['foal', 'oozy', 'bobs', 'gape'];
        this.levels[714] = ['deck', 'toll', 'horn', 'rake'];
        this.levels[715] = ['woos', 'fobs', 'haze', 'bent'];
        this.levels[716] = ['bend', 'yett', 'dung', 'gets'];
        this.levels[717] = ['lite', 'lung', 'rife', 'snap'];
        this.levels[718] = ['ream', 'yuck', 'zonk', 'fort'];
        this.levels[719] = ['pita', 'zoon', 'tell', 'dogs'];
        this.levels[720] = ['spun', 'ploy', 'kiwi', 'hasp'];
        this.levels[721] = ['cord', 'maid', 'miff', 'brag'];
        this.levels[722] = ['crab', 'rift', 'glad', 'fain'];
        this.levels[723] = ['soil', 'spay', 'midi', 'desk'];
        this.levels[724] = ['lyre', 'vied', 'hold', 'seek'];
        this.levels[725] = ['fogy', 'pane', 'okra', 'ewes'];
        this.levels[726] = ['oven', 'aide', 'hens', 'near'];
        this.levels[727] = ['inns', 'rial', 'knob', 'aped'];
        this.levels[728] = ['deck', 'silo', 'awry', 'lent'];
        this.levels[729] = ['vans', 'stag', 'tomb', 'exit'];
        this.levels[730] = ['ails', 'waif', 'clan', 'dove'];
        this.levels[731] = ['lord', 'rays', 'nuns', 'rice'];
        this.levels[732] = ['tank', 'dote', 'iron', 'mode'];
        this.levels[733] = ['yegg', 'bore', 'hail', 'bubo'];
        this.levels[734] = ['hots', 'ired', 'pear', 'dole'];
        this.levels[735] = ['bawd', 'skin', 'were', 'doth'];
        this.levels[736] = ['kilt', 'fads', 'lump', 'xray'];
        this.levels[737] = ['maid', 'leks', 'beet', 'soft'];
        this.levels[738] = ['save', 'love', 'tabs', 'moms'];
        this.levels[739] = ['boil', 'wise', 'lots', 'none'];
        this.levels[740] = ['aped', 'vara', 'gent', 'rows'];
        this.levels[741] = ['maul', 'wipe', 'zinc', 'gimp'];
        this.levels[742] = ['bark', 'defy', 'seed', 'dune'];
        this.levels[743] = ['null', 'drag', 'foci', 'slur'];
        this.levels[744] = ['node', 'cent', 'tick', 'swum'];
        this.levels[745] = ['acid', 'maim', 'coal', 'ride'];
        this.levels[746] = ['goop', 'leaf', 'home', 'eves'];
        this.levels[747] = ['mode', 'oars', 'foul', 'tame'];
        this.levels[748] = ['bots', 'wool', 'brim', 'feet'];
        this.levels[749] = ['wons', 'dads', 'lurk', 'ware'];
        this.levels[750] = ['omni', 'goth', 'seat', 'peat'];
        this.levels[751] = ['yods', 'axle', 'matt', 'ruby'];
        this.levels[752] = ['nuns', 'heth', 'best', 'beam'];
        this.levels[753] = ['help', 'wend', 'bowl', 'yows'];
        this.levels[754] = ['suer', 'stay', 'swig', 'goes'];
        this.levels[755] = ['tums', 'onyx', 'yean', 'lain'];
        this.levels[756] = ['awls', 'hook', 'shim', 'derm'];
        this.levels[757] = ['expo', 'slur', 'vast', 'upon'];
        this.levels[758] = ['acme', 'tore', 'pats', 'hots'];
        this.levels[759] = ['eros', 'eeks', 'pies', 'avid'];
        this.levels[760] = ['late', 'zinc', 'cozy', 'qoph'];
        this.levels[761] = ['pals', 'ruin', 'wart', 'plop'];
        this.levels[762] = ['crow', 'mars', 'chis', 'soil'];
        this.levels[763] = ['boon', 'yogi', 'make', 'lane'];
        this.levels[764] = ['tiny', 'desk', 'flip', 'logo'];
        this.levels[765] = ['jute', 'shwa', 'crew', 'ecru'];
        this.levels[766] = ['fold', 'sits', 'heat', 'mews'];
        this.levels[767] = ['hunt', 'sued', 'part', 'rush'];
        this.levels[768] = ['drip', 'zeal', 'celt', 'prep'];
        this.levels[769] = ['sack', 'unix', 'peep', 'raft'];
        this.levels[770] = ['hose', 'pail', 'germ', 'peas'];
        this.levels[771] = ['dabs', 'scar', 'whey', 'tear'];
        this.levels[772] = ['most', 'hilt', 'redo', 'cups'];
        this.levels[773] = ['load', 'cabs', 'full', 'writ'];
        this.levels[774] = ['lags', 'baby', 'zein', 'bare'];
        this.levels[775] = ['jaws', 'sigh', 'lets', 'soms'];
        this.levels[776] = ['tied', 'hark', 'yelk', 'ands'];
        this.levels[777] = ['rout', 'jugs', 'dime', 'boll'];
        this.levels[778] = ['loan', 'bill', 'hoax', 'talc'];
        this.levels[779] = ['chub', 'tsar', 'dyed', 'omen'];
        this.levels[780] = ['caps', 'whet', 'yeld', 'dram'];
        this.levels[781] = ['jail', 'text', 'geld', 'bask'];
        this.levels[782] = ['loft', 'rest', 'loam', 'wipe'];
        this.levels[783] = ['gels', 'bogs', 'does', 'dyes'];
        this.levels[784] = ['hare', 'bull', 'ream', 'shut'];
        this.levels[785] = ['odds', 'gunk', 'pops', 'town'];
        this.levels[786] = ['laze', 'vugs', 'gell', 'ills'];
        this.levels[787] = ['yeas', 'hoof', 'iced', 'rots'];
        this.levels[788] = ['nazi', 'tort', 'caws', 'plow'];
        this.levels[789] = ['drum', 'wisp', 'dodo', 'meat'];
        this.levels[790] = ['iffy', 'rues', 'tons', 'dote'];
        this.levels[791] = ['boos', 'snap', 'sump', 'bale'];
        this.levels[792] = ['scat', 'duel', 'flaw', 'trap'];
        this.levels[793] = ['glen', 'play', 'yoke', 'arid'];
        this.levels[794] = ['geek', 'caws', 'chad', 'haze'];
        this.levels[795] = ['rami', 'fibs', 'knew', 'drug'];
        this.levels[796] = ['tyke', 'dado', 'cork', 'kina'];
        this.levels[797] = ['vear', 'jobs', 'yell', 'heth'];
        this.levels[798] = ['desk', 'case', 'buzz', 'tips'];
        this.levels[799] = ['sawn', 'ting', 'foxy', 'eddy'];
        this.levels[800] = ['zone', 'rows', 'sera', 'hare'];
        this.levels[801] = ['will', 'gage', 'dawn', 'naut'];
        this.levels[802] = ['axon', 'roof', 'sumo', 'resh'];
        this.levels[803] = ['faun', 'mess', 'held', 'brie'];
        this.levels[804] = ['blew', 'tips', 'beat', 'pats'];
        this.levels[805] = ['lack', 'flax', 'palm', 'oozy'];
        this.levels[806] = ['tips', 'veal', 'ebbs', 'tuba'];
        this.levels[807] = ['fair', 'funk', 'pout', 'spun'];
        this.levels[808] = ['chad', 'okra', 'born', 'drug'];
        this.levels[809] = ['june', 'gush', 'fail', 'lend'];
        this.levels[810] = ['plop', 'rain', 'hill', 'ibex'];
        this.levels[811] = ['rune', 'curb', 'cube', 'gyps'];
        this.levels[812] = ['cold', 'wail', 'tent', 'cull'];
        this.levels[813] = ['tutu', 'mach', 'caws', 'dens'];
        this.levels[814] = ['hump', 'bank', 'eats', 'felt'];
        this.levels[815] = ['pout', 'cees', 'urea', 'sect'];
        this.levels[816] = ['city', 'taut', 'gale', 'lamp'];
        this.levels[817] = ['spas', 'sirs', 'dons', 'shmo'];
        this.levels[818] = ['camp', 'aloe', 'sues', 'limn'];
        this.levels[819] = ['beak', 'dart', 'taxi', 'yoke'];
        this.levels[820] = ['boss', 'gulp', 'loon', 'boss'];
        this.levels[821] = ['deed', 'calf', 'mall', 'crud'];
        this.levels[822] = ['stub', 'nose', 'bush', 'awny'];
        this.levels[823] = ['well', 'glee', 'zulu', 'beak'];
        this.levels[824] = ['also', 'lads', 'icer', 'bide'];
        this.levels[825] = ['axis', 'must', 'awns', 'item'];
        this.levels[826] = ['lear', 'sail', 'cave', 'whop'];
        this.levels[827] = ['feet', 'neap', 'hoed', 'yows'];
        this.levels[828] = ['vugs', 'core', 'arcs', 'hued'];
        this.levels[829] = ['hazy', 'boys', 'slur', 'mace'];
        this.levels[830] = ['bras', 'lien', 'toed', 'days'];
        this.levels[831] = ['blat', 'vatu', 'yows', 'serf'];
        this.levels[832] = ['slew', 'knur', 'help', 'bawd'];
        this.levels[833] = ['cost', 'ruse', 'scat', 'know'];
        this.levels[834] = ['rent', 'swan', 'gang', 'skim'];
        this.levels[835] = ['hair', 'glen', 'spry', 'hasp'];
        this.levels[836] = ['oxes', 'goos', 'july', 'berk'];
        this.levels[837] = ['jamb', 'know', 'warn', 'dyne'];
        this.levels[838] = ['fair', 'shin', 'limo', 'riff'];
        this.levels[839] = ['bris', 'thin', 'tens', 'sumo'];
        this.levels[840] = ['iffy', 'swap', 'dorm', 'jaws'];
        this.levels[841] = ['cole', 'feed', 'gell', 'toed'];
        this.levels[842] = ['sort', 'flee', 'hero', 'yins'];
        this.levels[843] = ['gate', 'lash', 'brew', 'byte'];
        this.levels[844] = ['dads', 'vane', 'role', 'calm'];
        this.levels[845] = ['gull', 'darn', 'when', 'glum'];
        this.levels[846] = ['four', 'iffy', 'yowe', 'oohs'];
        this.levels[847] = ['zonk', 'curl', 'node', 'exon'];
        this.levels[848] = ['wads', 'neap', 'wend', 'cost'];
        this.levels[849] = ['albs', 'dear', 'skim', 'plow'];
        this.levels[850] = ['skip', 'cart', 'gala', 'muon'];
        this.levels[851] = ['coma', 'yens', 'fell', 'eggs'];
        this.levels[852] = ['peak', 'tall', 'tote', 'mist'];
        this.levels[853] = ['conk', 'flee', 'taco', 'spay'];
        this.levels[854] = ['dons', 'july', 'halt', 'tome'];
        this.levels[855] = ['skin', 'tots', 'dead', 'bids'];
        this.levels[856] = ['foot', 'rave', 'matt', 'fell'];
        this.levels[857] = ['shiv', 'lice', 'bras', 'spot'];
        this.levels[858] = ['peas', 'good', 'card', 'magi'];
        this.levels[859] = ['toed', 'tire', 'gaze', 'seed'];
        this.levels[860] = ['wick', 'city', 'vast', 'gram'];
        this.levels[861] = ['tams', 'silo', 'oink', 'poky'];
        this.levels[862] = ['zulu', 'task', 'owns', 'lugs'];
        this.levels[863] = ['jags', 'wile', 'puma', 'fang'];
        this.levels[864] = ['moth', 'data', 'zerk', 'shoo'];
        this.levels[865] = ['herb', 'cock', 'foxy', 'good'];
        this.levels[866] = ['guff', 'ways', 'stir', 'text'];
        this.levels[867] = ['king', 'cane', 'eave', 'peek'];
        this.levels[868] = ['chop', 'swab', 'spun', 'mama'];
        this.levels[869] = ['hogs', 'pats', 'dung', 'tout'];
        this.levels[870] = ['psis', 'pale', 'sage', 'mayo'];
        this.levels[871] = ['wide', 'hots', 'apes', 'talc'];
        this.levels[872] = ['rash', 'bank', 'goji', 'pays'];
        this.levels[873] = ['tone', 'deck', 'high', 'emir'];
        this.levels[874] = ['wise', 'unit', 'hell', 'mope'];
        this.levels[875] = ['vest', 'shin', 'ashy', 'troy'];
        this.levels[876] = ['dips', 'tsar', 'oats', 'bend'];
        this.levels[877] = ['suck', 'this', 'sear', 'laze'];
        this.levels[878] = ['dime', 'sign', 'sacs', 'ward'];
        this.levels[879] = ['goon', 'dorm', 'fade', 'sits'];
        this.levels[880] = ['reds', 'fora', 'moth', 'tint'];
        this.levels[881] = ['dupe', 'ovum', 'glue', 'memo'];
        this.levels[882] = ['jeli', 'dune', 'stat', 'phiz'];
        this.levels[883] = ['edit', 'sins', 'rime', 'love'];
        this.levels[884] = ['coin', 'gait', 'edit', 'zein'];
        this.levels[885] = ['cake', 'kyak', 'chis', 'keel'];
        this.levels[886] = ['papa', 'flue', 'oven', 'raze'];
        this.levels[887] = ['pair', 'ions', 'hear', 'boat'];
        this.levels[888] = ['nils', 'wake', 'plus', 'lear'];
        this.levels[889] = ['left', 'hula', 'core', 'deil'];
        this.levels[890] = ['slaw', 'wile', 'calx', 'bond'];
        this.levels[891] = ['knob', 'cons', 'epic', 'lugs'];
        this.levels[892] = ['yaup', 'call', 'skid', 'foes'];
        this.levels[893] = ['fool', 'judo', 'hunk', 'snog'];
        this.levels[894] = ['vela', 'delf', 'guys', 'tape'];
        this.levels[895] = ['shoo', 'yuga', 'gush', 'slaw'];
        this.levels[896] = ['glum', 'vote', 'pans', 'lain'];
        this.levels[897] = ['ogre', 'nail', 'tips', 'dude'];
        this.levels[898] = ['zoos', 'arts', 'fund', 'want'];
        this.levels[899] = ['mica', 'vugs', 'pure', 'twos'];
        this.levels[900] = ['prod', 'foxy', 'dabs', 'yarn'];
        this.levels[901] = ['gums', 'snug', 'milk', 'luge'];
        this.levels[902] = ['begs', 'laze', 'yill', 'guns'];
        this.levels[903] = ['slum', 'wets', 'doge', 'teed'];
        this.levels[904] = ['yack', 'flat', 'lags', 'swab'];
        this.levels[905] = ['note', 'bulb', 'just', 'pies'];
        this.levels[906] = ['brim', 'loin', 'head', 'cask'];
        this.levels[907] = ['logo', 'bale', 'yaud', 'pint'];
        this.levels[908] = ['dusk', 'tiny', 'lady', 'ales'];
        this.levels[909] = ['firs', 'mall', 'most', 'ovum'];
        this.levels[910] = ['buhl', 'calx', 'lets', 'egos'];
        this.levels[911] = ['rage', 'expo', 'wile', 'fizz'];
        this.levels[912] = ['lean', 'aqua', 'cwms', 'mull'];
        this.levels[913] = ['king', 'amps', 'ugly', 'jack'];
        this.levels[914] = ['birr', 'goer', 'fuel', 'riot'];
        this.levels[915] = ['glum', 'must', 'clan', 'sink'];
        this.levels[916] = ['quit', 'soft', 'pale', 'kern'];
        this.levels[917] = ['blow', 'peak', 'kick', 'bird'];
        this.levels[918] = ['alga', 'cock', 'thru', 'mach'];
        this.levels[919] = ['knot', 'woos', 'mops', 'bows'];
        this.levels[920] = ['tick', 'coif', 'sips', 'punt'];
        this.levels[921] = ['deck', 'garb', 'dote', 'none'];
        this.levels[922] = ['weak', 'monk', 'byes', 'prom'];
        this.levels[923] = ['awny', 'king', 'daze', 'feed'];
        this.levels[924] = ['zinc', 'bras', 'play', 'eels'];
        this.levels[925] = ['shut', 'brim', 'dive', 'wart'];
        this.levels[926] = ['spur', 'mods', 'brat', 'copy'];
        this.levels[927] = ['bras', 'ares', 'atop', 'flip'];
        this.levels[928] = ['bunt', 'pled', 'earn', 'core'];
        this.levels[929] = ['spin', 'yoke', 'jack', 'bush'];
        this.levels[930] = ['poop', 'clad', 'spas', 'zero'];
        this.levels[931] = ['pegs', 'riot', 'bulb', 'coho'];
        this.levels[932] = ['deft', 'slum', 'mold', 'isms'];
        this.levels[933] = ['garb', 'woof', 'thee', 'coax'];
        this.levels[934] = ['leaf', 'gild', 'drum', 'cyan'];
        this.levels[935] = ['pays', 'lewd', 'hubs', 'tics'];
        this.levels[936] = ['edge', 'made', 'nope', 'also'];
        this.levels[937] = ['folk', 'ands', 'loft', 'ambo'];
        this.levels[938] = ['euro', 'brow', 'hath', 'rich'];
        this.levels[939] = ['bode', 'gown', 'veal', 'hush'];
        this.levels[940] = ['face', 'tear', 'glen', 'yard'];
        this.levels[941] = ['scum', 'deaf', 'hash', 'scam'];
        this.levels[942] = ['acts', 'lens', 'drop', 'need'];
        this.levels[943] = ['alms', 'foxy', 'pens', 'sole'];
        this.levels[944] = ['else', 'shoe', 'sops', 'exes'];
        this.levels[945] = ['prep', 'keep', 'kiss', 'fumy'];
        this.levels[946] = ['sack', 'kegs', 'soya', 'slug'];
        this.levels[947] = ['jams', 'gyms', 'next', 'arch'];
        this.levels[948] = ['ding', 'felt', 'yore', 'unit'];
        this.levels[949] = ['bags', 'sock', 'sere', 'rode'];
        this.levels[950] = ['jamb', 'zori', 'bows', 'golf'];
        this.levels[951] = ['foal', 'loop', 'sane', 'bunt'];
        this.levels[952] = ['cedi', 'hill', 'ugly', 'tort'];
        this.levels[953] = ['vees', 'dust', 'fibs', 'yald'];
        this.levels[954] = ['bled', 'coop', 'bury', 'easy'];
        this.levels[955] = ['peso', 'save', 'calf', 'arks'];
        this.levels[956] = ['anal', 'blat', 'made', 'dele'];
        this.levels[957] = ['duel', 'site', 'alas', 'bawd'];
        this.levels[958] = ['rise', 'vees', 'teth', 'lier'];
        this.levels[959] = ['kays', 'bide', 'rosy', 'sour'];
        this.levels[960] = ['twos', 'herb', 'time', 'time'];
        this.levels[961] = ['alms', 'hunt', 'yews', 'zoic'];
        this.levels[962] = ['fund', 'eggs', 'fora', 'bent'];
        this.levels[963] = ['rood', 'loos', 'mill', 'ilks'];
        this.levels[964] = ['root', 'fair', 'wall', 'tons'];
        this.levels[965] = ['fist', 'carp', 'cope', 'slum'];
        this.levels[966] = ['tout', 'damn', 'leaf', 'zonk'];
        this.levels[967] = ['lane', 'ambo', 'none', 'tomb'];
        this.levels[968] = ['malt', 'seal', 'rice', 'dozy'];
        this.levels[969] = ['aged', 'wide', 'moth', 'idol'];
        this.levels[970] = ['idea', 'ripe', 'hang', 'fibs'];
        this.levels[971] = ['quid', 'pile', 'tarp', 'cozy'];
        this.levels[972] = ['flip', 'sure', 'dial', 'wage'];
        this.levels[973] = ['dodo', 'tidy', 'cups', 'tsar'];
        this.levels[974] = ['vile', 'tall', 'yaup', 'urea'];
        this.levels[975] = ['hams', 'grew', 'gait', 'full'];
        this.levels[976] = ['anon', 'lisp', 'itch', 'wool'];
        this.levels[977] = ['trip', 'tint', 'acme', 'deli'];
        this.levels[978] = ['hits', 'mode', 'food', 'tile'];
        this.levels[979] = ['lush', 'code', 'acid', 'flue'];
        this.levels[980] = ['sage', 'seta', 'gyre', 'gore'];
        this.levels[981] = ['ahoy', 'work', 'bawl', 'gulp'];
        this.levels[982] = ['swat', 'tilt', 'gout', 'cows'];
        this.levels[983] = ['chad', 'axel', 'wand', 'camp'];
        this.levels[984] = ['idea', 'pact', 'ages', 'bent'];
        this.levels[985] = ['pars', 'cole', 'gnus', 'guns'];
        this.levels[986] = ['dust', 'show', 'bunk', 'stud'];
        this.levels[987] = ['dram', 'army', 'bane', 'solo'];
        this.levels[988] = ['balk', 'bawd', 'hoes', 'clef'];
        this.levels[989] = ['ream', 'jute', 'sync', 'derm'];
        this.levels[990] = ['paid', 'exon', 'okra', 'spud'];
        this.levels[991] = ['slob', 'byte', 'hypo', 'gags'];
        this.levels[992] = ['prop', 'bugs', 'lewd', 'muck'];
        this.levels[993] = ['dyed', 'sump', 'mess', 'twit'];
        this.levels[994] = ['work', 'gull', 'soms', 'clot'];
        this.levels[995] = ['spun', 'foes', 'yeld', 'pups'];
        this.levels[996] = ['eked', 'scat', 'flub', 'gash'];
        this.levels[997] = ['egos', 'foam', 'last', 'jabs'];
        this.levels[998] = ['feud', 'asks', 'romp', 'swig'];
        this.levels[999] = ['foxy', 'name', 'shut', 'trip'];
        this.levels[1000] = ['soon', 'yens', 'swab', 'jack'];
    }
}
class CoinBox extends Phaser.Group {
    constructor() {
        super(game);
        this.text1 = game.add.text(0, 0, model.coins);
        this.text1.anchor.set(0.5, 0.5);
        this.text1.fill = model.mainTextColor;
        this.add(this.text1);
        //
        //
        //
        //
        var coin = game.add.sprite(0, 0, "coin");
        this.coin = coin;
        coin.anchor.set(0.5, 0.5);
        Align.scaleToGameW(coin, .05);
        coin.x = this.text1.x - this.text1.width / 2 - coin.width / 2;
        this.add(coin);
        this.x = game.width / 2;
        this.y = game.height - this.height / 2;
        eventDispatcher.add(this.getEvent, this);
    }
    getEvent(call, params) {
        if (call == "ADD_COINS") {
            this.text1.text = model.coins;
            this.coin.x = this.text1.x - this.text1.width / 2 - this.coin.width / 2;
        }
    }
}
var StateInstructions = {
    create: function() {
        var field = new Field();
        model.state = "instructions";
        var soundButtons = new SoundButtons();
        // var buttonStart = new TextButton("Start", 4, 14, G.START_GAME, null, 55, "#ff0000");
        var buttonStart = new BorderButton("Start", G.START_GAME, null, game.width * .5, "0x713792", "0xffffff", 0, 55, "#ff0000", 16, false);
        Align.center(buttonStart);
        buttonStart.adjust();
        //
        //
        //
        //
        var inText = game.add.text(game.width / 2, game.height / 2, "Click 2 letters to swap them to make rows of words. You need 4 words to complete the level. For each word you make you get a coin. Trade coins for hints.");
        inText.anchor.set(0.5, 0.5);
        inText.font = "Flamenco";
        inText.wordWrap = true;
        inText.wordWrapWidth = game.width * .85;
        inText.y = game.height * .25;
        inText.x = game.width / 2;
        //
        //
        var btnHome = new ImageButton("btnHome", G.CHANGE_STATE, "StateTitle");
        Align.scaleToGameW(btnHome, .1);
        btnHome.x = btnHome.width / 2;
        btnHome.y = game.height - btnHome.height / 2;
    }
}
class WordGame extends Phaser.Group {
    constructor() {
        super(game);
        this.gridGroup = game.add.group();
        var grid = new Grid();
        grid.visible = false;
        this.grid = grid;
        var wordDeck = new WordDeck();
        this.letters = wordDeck.getLevel(model.currentLevel);
        this.fillGrid(this.letters);
        //
        //
        //
        var back = game.add.graphics();
        back.beginFill(this.getBackColor(), 1);
        back.drawRoundedRect(0, 0, grid.width * 1.1, grid.height * 1.1, 16);
        back.endFill();
        this.add(back);
        this.add(grid);
        Align.centerGroup3(grid);
        Align.centerGroup(back);
        eventDispatcher.add(this.getEvent, this);
        this.checkWord();
        this.gy = grid.y;
        grid.y = -200;
        grid.visible = true;
        var tw1 = game.add.tween(grid).to({
            y: this.gy
        }, 500, Phaser.Easing.Linear.None, true);
        // eventDispatcher.dispatch(G.PLAY_SOUND, "swish");
    }
    getBackColor() {
        var backColor = "0xffffff";
        if (model.currentLevel < 51) {
            backColor = "0x1C6011";
        }
        if (model.currentLevel > 50 && model.currentLevel < 101) {
            backColor = "0xffffff";
        }
        if (model.currentLevel > 100 && model.currentLevel < 151) {
            backColor = "0xffffff";
        }
        if (model.currentLevel > 150 && model.currentLevel < 201) {
            backColor = "0xffffff";
        }
        if (model.currentLevel > 200 && model.currentLevel < 251) {
            backColor = "0xffffff";
        }
        return backColor;
    }
    fillGrid(letters) {
        this.grid.removeAll(true);
        var row = -1;
        for (var i = 0; i < 16; i++) {
            if (i / 4 == Math.floor(i / 4)) {
                row++;
            }
            var tile = new Tile();
            tile.index = i;
            tile.width = game.width * .15;
            tile.scale.y = tile.scale.x;
            tile.setLetter(letters[i]);
            if (model.greens[row] == true) {
                tile.setGreen();
            }
            if (model.showStars[row] == true) {
                tile.showSparks();
                //  eventDispatcher.dispatch(G.PLAY_SOUND,"right1");
            }
            this.grid.add(tile);
        }
        this.grid.makeGrid(4);
    }
    getEvent(call, params) {
        if (call == "SELECT_LETTER") {
            if (this.firstLetter == params) {
                this.firstLetter = null;
                params.resetColor();
                return;
            }
            if (this.firstLetter == null) {
                params.setRed();
                this.firstLetter = params;
                return;
            }
            var secondLetter = params;
            this.secondLetter = secondLetter;
            this.firstLetter.setBlack();
            this.firstLetter.turnOffBack();
            secondLetter.turnOffBack();
            //
            //
            var index1 = this.firstLetter.index;
            var index2 = this.secondLetter.index;
            //
            //
            var temp = this.letters[index1];
            this.letters[index1] = this.letters[index2];
            this.letters[index2] = temp;
            //
            //
            this.grid.bringToTop(this.secondLetter);
            this.grid.bringToTop(this.firstLetter);
            //
            //
            var x1 = this.firstLetter.x;
            var y1 = this.firstLetter.y;
            //
            //
            var x2 = secondLetter.x;
            var y2 = secondLetter.y;
            model.clickLock = true;
            var tw1 = game.add.tween(secondLetter).to({
                x: x1,
                y: y1
            }, 500, Phaser.Easing.Linear.None, true);
            //
            //
            var tw2 = game.add.tween(this.firstLetter).to({
                x: x2,
                y: y2
            }, 500, Phaser.Easing.Linear.None, true);
            tw1.onComplete.add(this.tweenDone, this);
        }
    }
    prepDestroy() {
        eventDispatcher.remove(this.getEvent, this);
    }
    tweenDone() {
        this.fillGrid(this.letters);
        this.firstLetter = null;
        model.clickLock = false;
        this.checkWord();
    }
    flyCoin() {
        if (this.coin) {
            this.coin.destroy(true);
        }
        this.coin = game.add.sprite(game.width / 2, game.height / 2, "coin");
        this.coin.anchor.set(0.5, 0.5);
        // Align.scaleToGameW(this.coin,.1);
        var x1 = game.width / 2;
        var y1 = game.height;
        var tw1 = game.add.tween(this.coin).to({
            x: x1,
            y: y1
        }, 500, Phaser.Easing.Linear.None, true);
        var tw2 = game.add.tween(this.coin.scale).to({
            x: 0,
            y: 0
        }, 500, Phaser.Easing.Linear.None, true);
        tw1.onComplete.add(this.addCoin, this);
    }
    addCoin() {
        model.addCoins(1);
    }
    checkWord() {
        var c = 0;
        var gridWords = [];
        var wordIndex = 0;
        var word = "";
        var row = 0;
        var rightCount = 0;
        this.grid.forEach(function(tile) {
            model.showStars[row] = false;
            // tile.setLetter(c);
            word += tile.getLetter();
            if (word.length == 4) {
                model.wordRows[row] = word;
                ////console.log(word);
                if (model.checkWord(word) == true) {
                    ////console.log("found one on " + row);
                    rightCount++;
                    model.greens[row] = true;
                    if (model.newWords[word] != 1) {
                        ////console.log("new word is " + word);
                        model.showStars[row] = true;
                        eventDispatcher.dispatch(G.PLAY_SOUND, "right");
                        this.flyCoin();
                        if (game.rnd.integerInRange(0, 100) < 60) {
                            eventDispatcher.dispatch(G.SHOW_TOAST, {
                                message: model.getPositive(),
                                color: model.getToastColor(),
                                textColor: '#ffffff'
                            });
                        }
                    }
                    model.newWords[word] = 1;
                } else {
                    model.greens[row] = false;
                }
                word = "";
                row++;
            }
            c++;
        }.bind(this));
        this.fillGrid(this.letters);
        if (rightCount == 4) {
            model.clickLock = true;
            eventDispatcher.dispatch(G.PLAY_SOUND, "newlevel");
            eventDispatcher.dispatch("SHOW_NEXT_BUTTON");
        }
    }
}