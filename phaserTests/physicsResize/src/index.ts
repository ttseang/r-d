//import Phaser = require("phaser");
import Phaser, { DOM } from 'phaser';
import { SceneMain } from "./scenes/SceneMain";
import {GM} from "./classes/GM";
import { SceneStart } from './scenes/SceneStart';
import { ResizeVo } from './dataObjs/ResizeVo';
import 'phaser/plugins/spine/dist/SpinePlugin';
//import * as SpinePlugin from "phaser/plugins/spine/dist/SpinePlugin";


let gm:GM=GM.getInstance();

let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad=navigator.userAgent.indexOf("iPad");


    if (isTablet!=-1 || isIpad!=-1)
    {
        gm.isTablet=true;
        gm.device=ResizeVo.DEVICE_TABLET;
        isMobile=1;
    }

let w = 600;
let h = 400;
//
//
if (isMobile != -1 && gm.isTablet==false) {
    gm.isMobile=true;
    gm.device=ResizeVo.DEVICE_MOBILE;
}

w = window.innerWidth;
h = window.innerHeight;

if (window['visualViewport'])
{
    w=window.visualViewport.width;
    h=window.visualViewport.height;
}

if (gm.isMobile===true || gm.isTablet===true)
{
    if (w<h)
    {
        gm.isPort=true;
        gm.orientation=ResizeVo.ORIENTATION_PORTRAIT;
    }
    else
    {
        gm.isPort=false;
        gm.orientation=ResizeVo.ORIENTATION_LANDSCAPE;
    }
}

const config:Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    width: w,
    height: h,    
    backgroundColor:'000000',
    parent: 'phaser-game',
    physics:{
        default:"arcade",
        arcade:
        {
            debug:false
        }
    },
    scene: [SceneStart,SceneMain]
};
/*
,
    plugins: {
        scene: [
            { key: 'SpinePlugin', plugin: window.SpinePlugin, mapping: 'spine' }
        ]
    }
*/

new Phaser.Game(config);

