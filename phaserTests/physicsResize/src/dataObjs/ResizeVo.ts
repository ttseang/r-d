export class ResizeVo
{
    public static DEVICE_DESKTOP:string="desktop";
    public static DEVICE_MOBILE:string="mobile";
    public static DEVICE_TABLET:string="tablet";
    //
    //
    //
    public static ORIENTATION_LANDSCAPE:string="landscape";
    public static ORIENTATION_PORTRAIT:string="port";
    //
    //
    //
    public device:string;
    public orientation:string;
    public posX:number;
    public posY:number;
    public scale:number;
    //
    //
    //

    constructor(device:string,orientation:string,posX:number,posY:number,scale:number)
    {
        this.device=device;
        this.orientation=orientation;
        this.posX=posX;
        this.posY=posY;
        this.scale=scale;
    }
}