//import Phaser = require("phaser");
//import Phaser from 'phaser';
import { SceneMain } from "./scenes/SceneMain";
import SceneFont from './scenes/SceneFont';
import SceneLoad from './scenes/SceneLoad';
import { SceneData } from "./scenes/SceneData";
let isMobile = navigator.userAgent.indexOf("Mobile");
if (isMobile == -1) {
    isMobile = navigator.userAgent.indexOf("Tablet");
}

//
//
//if (isMobile != -1) {
    let w = window.innerWidth;
   let  h = window.innerHeight;
//}
const config: any = {
    mode: Phaser.AUTO,
    width: w,
    height: h,
    parent: 'phaser-game',
    transparent:true,
    scene: [SceneFont,SceneData, SceneLoad, SceneMain]
}

new Phaser.Game(config);