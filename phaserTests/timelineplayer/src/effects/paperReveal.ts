import IBaseScene from "../interfaces/IBaseScene";
import { ImageMask } from "./imageMask";
import { Reveal } from "./reveal";
import { TextMask } from "./textMask";

export class PaperReveal
{
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private scale: number = 0;
   
    //   private corners: Phaser.GameObjects.Sprite[] = [];
    private timer1: Phaser.Time.TimerEvent | undefined;
   
    public callback: Function = () => { };
    private animationName: string;
    private t: Phaser.GameObjects.Text;
   // private tm:TextMask;
    private reveal:Reveal;
   

    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text, animationName: string)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.t=t;
        this.animationName=animationName;

        this.reveal = new Reveal(this.bscene, t, animationName);
       
                
    }
    private initPos()
    {
       
    }
    animate()
    {
        this.reveal.callback=this.callback;
        this.reveal.animate();
       //let tx:number=this.t.x+this.t.displayWidth/2;
      // this.scene.tweens.add({ targets: [this.tm.holder,this.bottom.holder], duration: 5000, x: tx,onComplete:this.destroy.bind(this) })
    }
    destroy()
    {
      //  this.tm.destroy();
        this.callback();
    }
}