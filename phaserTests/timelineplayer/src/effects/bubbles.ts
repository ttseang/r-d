
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { Bubble } from "./bubble";

export class Bubbles {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    private t: Phaser.GameObjects.Text;
    private key: string;
    private secs: number = 0;
    private firstBubble: Bubble | undefined;
    private timer1: any = "";
    private colors: number[];
    private effectName: string;
    private useAnimation:boolean=false;
    private bubbleGroup:Phaser.GameObjects.Group;

    
    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text, key: string, secs: number, effectName: string, colors: number[],maxScale:number=10,useAnimation:boolean=false) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.colors = colors;
        this.effectName = effectName;
        this.useAnimation=useAnimation;
        this.bubbleGroup=this.scene.add.group();

        this.t = t;
        this.key = key;
        let tw: number = t.x + t.displayWidth;
        let th: number = t.y + t.displayHeight;



        let prevBubble: Bubble | undefined;


        for (let i: number = 0; i < 20; i++) {
            let xx: number = Phaser.Math.Between(t.x, tw);
            let yy: number = Phaser.Math.Between(t.y, th);
            let bubble: Bubble = new Bubble(bscene, xx, yy, key);
            if (colors.length !== 0) {
                let colorIndex: number = Phaser.Math.Between(0, colors.length - 1);
                bubble.setTint(colors[colorIndex]);
            }
            if (useAnimation===true)
            {
                let animationName:string=this.key+"Play";
                console.log(animationName);
                bubble.showAnim(animationName);
            }
            let scale:number
            if (maxScale<0)
            {
                scale=Math.abs(maxScale)/100;
            }
            else
            {
                scale=Phaser.Math.Between(0,maxScale)/100;
            }
            Align.scaleToGameW(bubble,scale,bscene);


            if (this.firstBubble === undefined) {
                this.firstBubble = bubble;
            }

            if (prevBubble) {
                prevBubble.next = bubble;
            }

            prevBubble = bubble;

            this.bubbleGroup.add(bubble);
        }
        this.scene.children.bringToTop(this.t);
    }
    
    public animate() {
        this.scene.time.addEvent({ delay: 3000, callback: this.stopMe.bind(this), loop: false });
        this.timer1 = setInterval(this.tick.bind(this), 200);
        //   setTimeout(this.stop.bind(this), this.secs * 1000);
    }
    private stopMe() {
        console.log("STOP ME");
        clearInterval(this.timer1);
        this.bubbleGroup.children.entries.forEach((bubble)=>{
            bubble.removeAllListeners();
        })
        this.bubbleGroup.destroy(true,true);
        //this.firstBubble?.destroyMe();
    }
    private tick() {
        if (this.firstBubble) {
            switch (this.effectName) {
                case "bubble":
                    this.firstBubble?.move();
                    break;

                case "floataway":
                    this.firstBubble.moveUp();
                    break;

                case "zoomaway":
                    this.firstBubble.zoomUp();
                    break;

            }

        }

    }
}