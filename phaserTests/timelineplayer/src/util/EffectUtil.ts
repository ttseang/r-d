
import { PosVo } from "../dataObjs/PosVo";
import { AnimationOnText } from "../effects/animationOnText";
import {AnimationPlayer} from "../effects/animationPlayer";

import { BoxDraw } from "../effects/boxDraw";
import { Brackets } from "../effects/brackets";
import { Bubbles } from "../effects/bubbles";
import { Bubbles2 } from "../effects/bubbles2";
import { ImageReveal } from "../effects/imageReveal";

import { ObjectFly } from "../effects/objectFly";
//import { PaperReveal } from "../effects/paperReveal";
import { Rays } from "../effects/rays";
import { Reveal } from "../effects/reveal";
import Spotlight from "../effects/spotlight/spotlight";
import { Sunrise } from "../effects/sunrise";
import { TriText } from "../effects/TriText";
import IBaseScene from "../interfaces/IBaseScene";
//import Align from "./align";
import { FormUtil } from "./formUtil";

export class EffectUtil {
    private formUtil: FormUtil;
    private scene: Phaser.Scene;
    private callback: Function = () => { };
    private bscene: IBaseScene;

    constructor(bscene: IBaseScene, formUtil: FormUtil) {
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.formUtil = formUtil;
    }
    public setCallback(fun: Function) {
        this.callback = fun;
    }
    public lineUpToHtml(t: Phaser.GameObjects.Text, el: string) {
        //get the element
        let element: any = document.getElementById(el);

        //calculate the position of the element
        let pos: PosVo = this.getElPos(element);
        // let canvasPos:PosVo=this.getCanvasPos();      


        let canvas: any = document.getElementById('thecanvas');
        let canvasPos: PosVo = this.getElPos(canvas);

        ////console.log(canvasPos);
        //get the numbers from the position object
        let ex: number = pos.x - canvasPos.x;
        let ey: number = pos.y - canvasPos.y;

        ////console.log(pos);

        //get the computed styles
        let fs1: string = window.getComputedStyle(element, null).getPropertyValue('font-size');
        let ff: string = window.getComputedStyle(element, null).getPropertyValue('font-family');
        let tc:string=window.getComputedStyle(element, null).getPropertyValue('color');
        //convert font size to number
        let fs: number = parseInt(fs1);
       // console.log("fs="+fs);
        //console.log(ff);

        //make the phaser text match the html
        t.setFontFamily(ff);
        t.setFontSize(fs);
        t.setColor(tc);
        //t.setText("test");
        //console.log(ex,ey);
        //console.log(t);
        //position the phaser text on top the html
        t.x = ex;
        t.y = ey;
        t.visible = true;
    }
    private lineUpSpriteToEl(s: Phaser.GameObjects.Sprite, el: string) {
        let element: any = document.getElementById(el);

        //calculate the position of the element
        let pos: PosVo = this.getElPos(element);
        // let canvasPos:PosVo=this.getCanvasPos();      


        let canvas: any = document.getElementById('thecanvas');
        let canvasPos: PosVo = this.getElPos(canvas);

        //  ////console.log(canvasPos);
        //get the numbers from the position object
        let ex: number = pos.x - canvasPos.x;
        let ey: number = pos.y - canvasPos.y;
        ////console.log(ex, ey);

        s.x = ex;
        s.y = ey;
    }
    private getTextElPos(el: string) {
        let element: any = document.getElementById(el);

        //calculate the position of the element
        let pos: PosVo = this.getElPos(element);
        // let canvasPos:PosVo=this.getCanvasPos();      


        let canvas: any = document.getElementById('thecanvas');
        let canvasPos: PosVo = this.getElPos(canvas);

        ////console.log(canvasPos);
        //get the numbers from the position object
        let ex: number = pos.x - canvasPos.x;
        let ey: number = pos.y - canvasPos.y;
        return new PosVo(ex, ey);
    }
    public getElPos(el: any) {
        // yay readability
        for (var lx = 0, ly = 0;
            el != null;
            lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
        // return { x: lx, y: ly };
        return new PosVo(lx, ly);
    }

    public loadAnimations() {
        let frameNames: any[] = this.scene.anims.generateFrameNames('halfRays', { start: 1, end: 24, zeroPad: 4, prefix: 'Rays_Half', suffix: '.png' })

        this.scene.anims.create({
            key: 'halfRaysPlay',
            frames: frameNames,
            frameRate: 24,
            repeat: 0
        });

        let bracketFrames: any[] = this.scene.anims.generateFrameNames('brackets', { start: 1, end: 15, zeroPad: 4, prefix: 'CornerBracket_Large', suffix: '.png' })

        this.scene.anims.create({
            key: 'bracketsPlay',
            frames: bracketFrames,
            frameRate: 8,
            repeat: -1
        });
        //  let bubbleFrames:any[]= this.scene.anims.generateFrameNames('bubblePop', {start:1, end: 5, zeroPad: 4, prefix: 'Bubble7-2', suffix: '.png'})

        this.scene.anims.create({
            key: 'bubblePopPlay',
            frames: this.getFrames("bubblePop", 1, 5, 4, "Bubble7-2", ".png"),
            frameRate: 8,
            repeat: 0
        });
        this.scene.anims.create({
            key: 'starBurstPlay',
            frames: this.getFrames("starBurst", 3, 25, 4, "Starburst", ".png"),
            frameRate: 8,
            repeat: 0
        });
        this.scene.anims.create({
            key: 'fullRaysPlay',
            frames: this.getFrames("fullRays", 1, 24, 4, "Rays Full", ".png"),
            frameRate: 8,
            repeat: 0
        });
        this.scene.anims.create({
            key: 'raysPaperPlay',
            frames: this.getFrames("raysPaper", 1, 23, 4, "Rays_Paper", ".png"),
            frameRate: 8,
            repeat: 0
        });
        this.scene.anims.create({
            key: 'sparksPlay',
            frames: this.getFrames("sparks", 1,12, 4, "spark_prime", ".png"),
            frameRate: 8,
            repeat: 0
        });
        this.scene.anims.create({
            key: 'radialPlay',
            frames: this.getFrames("radial", 1,23, 4, "Radial Highlight B", ".png"),
            frameRate: 8,
            repeat: 0
        });
        //paperRays

    }
    private getFrames(key: string, s: number, e: number, z: number, pf: string, sf: string) {
        return this.scene.anims.generateFrameNames(key, { start: s, end: e, zeroPad: z, prefix: pf, suffix: sf })
    }
    public doEffect(effectName: string, t: Phaser.GameObjects.Text, el: string, secs: number = 0) {
        //console.log(effectName);
        ////console.log(el);
        t.setOrigin(0, 0);


        switch (effectName) {
            case "fly":
                this.flyToSpan(t, el, secs);
                break;

            case "flipInPlace":
                this.flipAtSpan(t, el, secs);
                break;

            case "grow":
                this.growAtSpan(t, el, secs);
                break;

            case "spin":
                this.spinAtSpan(t, el, secs);
                break;

            case "spinGrow":
                this.spinAndGrowAtSpan(t, el, secs);
                break;

            case "shake":
                this.shakeAtSpan(t, el, secs);
                break;

            case "brackets":
                this.addBrackets(t, el, secs);
                break;

            case "point":
                this.pointFinger(t, el, secs);
                break;
            case "punch":
                this.punch(t, el, secs);
                break;
            case "thumb":
                this.thumbsUp(t, el, secs);
                break;
            case "bob":
                this.bob(t, el, secs);
                break;
            case "shakeZoom":
                this.shakeZoomAtSpan(t, el, secs);
                break;
            case "spotlight":
                this.spotLight(t, el, secs);
                break;

            case "bubbles":
                this.bubbles(t, el, secs);
                break;

            case "hearts":
                this.hearts(t, el, secs);
                break;

            case "faces":
                this.faces(t, el, secs);
                break;

            case "balloons":
                this.balllons(t, el, secs);
                break;

            case "rockets":
                this.rockets(t, el, secs);
                break;

            case "dropText":
                this.dropText(t, el, "drop");
                break;
            case "sliceLeft":
                this.dropText(t, el, "sliceLeft");
                break;
            case "sliceRight":
                this.dropText(t, el, "sliceRight");
                break;
            case "rays":
                this.rays(t, el);
                break;

            case "sunrise":
                this.sunrise(t, el);
                break;

            case "redBox":
                this.redBox(t, el);
                break;

            case "mixSlice":
                this.dropText(t, el, "mix");
                break;

            case "revealLeft":
                this.revealWord(t, el, "revealLeft");
                break;
            case "revealRight":
                this.revealWord(t, el, "revealRight");
                break;
            case "revealUp":
                this.revealWord(t, el, "revealUp");
                break;
            case "revealDown":
                this.revealWord(t, el, "revealDown");
                break;

            case "halfRays":
                this.basicAnimation(t, el, "halfRays", 0,secs);
                break;

            case "bubblePop":
                this.bubblePop(t, el, secs);
                break;

            case "starBurst":
                this.starBurst(t, el, "starBurst",secs);
                break;

            case "fullRays":
                this.basicAnimation(t, el, "fullRays", 1,secs);
                break;

            case "raysPaper":
                this.basicAnimation(t, el, "raysPaper", 1,secs);
                break;

            case "paperReveal":
                this.paperReveal(t,el,"paperReveal");
                break;

                case "enlarge":
                    this.enlarge(t,el,secs);
                break;

            case "sparks":
                this.sparks(t,el,secs);
                break;
               
            case "radial":
                this.radial(t, el, secs);
                break;
        }
    }

    private effectDone(t: Phaser.GameObjects.Text, el: string) {
        ////console.log("effectDone");

        this.formUtil.unblankElement(el);
        t.visible = false;

        t.setAngle(0);
        t.setScale(1, 1);
        if (this.callback) {
            this.callback();
        }
    }
    private enlarge(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);

      //  let ox: number = t.x;
       // let oy: number = t.y;

        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;

        let ty=t.y-t.displayHeight;
        
        let time: number = secs * 1000;
        this.scene.tweens.add({ targets: t, duration: time, scaleX:2,scaleY:2,y:ty,yoyo:true, onComplete: () => { this.effectDone(t, el) } });
    }
    sparks(t: Phaser.GameObjects.Text, el: string, secs: number)
    {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let player:AnimationPlayer=new AnimationPlayer(this.bscene,"sparks",80);
        player.x=t.x;
        player.y=t.y+t.displayHeight/2;
        player.play();
        this.scene.tweens.add({ targets: player, duration: 1000, x:this.bscene.getW()+100, onComplete: () => { this.effectDone(t, el) } });
  
    }
    private flyToSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);

        let ox: number = t.x;
        let oy: number = t.y;

        t.x = 0;
        t.y = 0;
        

        console.log(t);
        this.scene.tweens.add({ targets: t, duration: secs, y: oy, x: ox, onComplete: () => { this.effectDone(t, el) } });

    }
    private flipAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        t.scaleX = -1;
        t.scaleY = -1;
        
        this.scene.tweens.add({ targets: t, duration: secs, scaleX: 1, scaleY: 1, onComplete: () => { this.effectDone(t, el) } });
    }
    private growAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 1);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight;

        let oh: number = t.displayHeight;
        t.displayHeight = 0;
        // t.scaleX=-1;
        // t.scaleY=-0.5;
        
        this.scene.tweens.add({ targets: t, duration: secs, displayHeight: oh, onComplete: () => { this.effectDone(t, el) } });
    }
    private spinAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        t.setAngle(-360);
        
        this.scene.tweens.add({ targets: t, duration: secs, angle: 360, onComplete: () => { this.effectDone(t, el) } });
    }
    private spinAndGrowAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        t.scaleX = 0;
        t.scaleY = 0;
        t.setAngle(-360);

        
        this.scene.tweens.add({ targets: t, duration: secs, angle: 360, scaleX: 1, scaleY: 1, onComplete: () => { this.effectDone(t, el) } });
    }
    private shakeAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        let angle: number = 20;
       
        t.setAngle(-angle);
        this.scene.tweens.add({ targets: t, duration: secs, angle: angle, yoyo: true, repeat: 2, onComplete: () => { this.effectDone(t, el) } });
    }
    private shakeZoomAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        let angle: number = 20;
        let time: number = secs * 1000;

        let osx: number = t.scaleX;
        let osy: number = t.scaleY;
        let tsx: number = osx * 1.5;
        let tsy: number = osy * 1.5;

        t.setAngle(-angle);
        this.scene.tweens.add({
            targets: t, duration: time, angle: angle, scaleX: tsx, scaleY: tsy, yoyo: true, repeat: 2, onComplete: () => {
                t.scaleX = osx;
                t.scaleY = osy;
                this.effectDone(t, el);
            }
        });
    }
    private bob(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);

        let ty: number = t.y + t.displayHeight / 5;
       

        this.scene.tweens.add({ targets: t, duration: secs, y: ty, yoyo: true, repeat: 10, onComplete: () => { this.effectDone(t, el) } });
    }
    private addBrackets(t: Phaser.GameObjects.Text, el: string, secs: number) {

        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.visible=true;
        //console.log(t);
        let brackets: Brackets = new Brackets(this.bscene, t);
        brackets.makeBrackets();
        brackets.animate();
        /*  setTimeout(() => {
            brackets.destroy();
            t.visible=false;
            this.formUtil.unblankElement(el);
        }, secs); */
    }
    private pointFinger(t: Phaser.GameObjects.Text, el: string, secs: number) {
        ////console.log("POINT");
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let objectFly: ObjectFly = new ObjectFly(this.bscene, t, "hand", secs, "point");
        objectFly.animate();
    }
    private punch(t: Phaser.GameObjects.Text, el: string, secs: number) {
        ////console.log("POINT");
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let objectFly: ObjectFly = new ObjectFly(this.bscene, t, "punch", secs, "punch");
        objectFly.animate();

        setTimeout(() => {            
            t.visible=false;
            this.formUtil.unblankElement(el);
        }, secs*2)
    }
    private thumbsUp(t: Phaser.GameObjects.Text, el: string, secs: number) {
        ////console.log("POINT");
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let objectFly: ObjectFly = new ObjectFly(this.bscene, t, "thumbsUp", secs, "thumb");
        objectFly.animate();
    }
    private bubbles(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let bubbles: Bubbles = new Bubbles(this.bscene, t, "circle", secs, "bubble", [0xe67e22, 0xe74c3c, 0x27ae60], 5);
        bubbles.animate();
    }
    private bubblePop(t: Phaser.GameObjects.Text, el: string, secs: number) {

        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let bubbles: Bubbles2 = new Bubbles2(this.bscene, t, "bubblePop", secs, "", [0xe67e22, 0xe74c3c, 0x27ae60], 5, true);
        bubbles.animate();
    }
    private radial(t: Phaser.GameObjects.Text, el: string, secs: number)
    {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let bubbles: Bubbles2 = new Bubbles2(this.bscene, t, "radial", secs, "", [0xe67e22, 0xe74c3c, 0x27ae60], 5, true);
        bubbles.animate();
    }
    private hearts(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let bubbles: Bubbles = new Bubbles(this.bscene, t, "heart", secs, "floataway", [0xe74c3c, 0xc0392b, 0xff0000]);
        bubbles.animate();
    }
    private balllons(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let bubbles: Bubbles = new Bubbles(this.bscene, t, "balloon", secs, "floataway", [0xe74c3c, 0x3498db, 0xf1c40f, 0x2ecc71], -2);
        bubbles.animate();
    }
    private rockets(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let bubbles: Bubbles = new Bubbles(this.bscene, t, "rocket", secs, "zoomaway", [], -2);
        bubbles.animate();
    }
    private faces(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let bubbles: Bubbles = new Bubbles(this.bscene, t, "face", secs, "bubble", [], 5);
        bubbles.animate();
    }
    private dropText(t: Phaser.GameObjects.Text, el: string, animationName: string) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.visible = false;
        let triText: TriText = new TriText(this.bscene, t, animationName);
        triText.callback = () => { this.effectDone(t, el) };
        triText.animate();
    }
    private rays(t: Phaser.GameObjects.Text, el: string,) {
        //console.log(el);
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setColor("#ff00f0");
        //t.visible = false;

        let rays: Rays = new Rays(this.bscene, t, "");
        rays.callback = () => { this.effectDone(t, el) };
        rays.animate();
    }
    private sunrise(t: Phaser.GameObjects.Text, el: string) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);

        t.visible = false;

        let rays: Sunrise = new Sunrise(this.bscene, t, "");
        rays.callback = () => { this.effectDone(t, el) };
    }
    private redBox(t: Phaser.GameObjects.Text, el: string) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let box: BoxDraw = new BoxDraw(this.bscene, t);
        box.start();

    }
    private revealWord(t: Phaser.GameObjects.Text, el: string, animationName: string) {

        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.visible = false;
        let reveal: Reveal = new Reveal(this.bscene, t, animationName);
        reveal.callback = () => { this.effectDone(t, el) };
        reveal.animate();
    }
    private paperReveal(t: Phaser.GameObjects.Text, el: string, animationName: string)
    {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.visible = false;
       // let reveal: PaperReveal = new PaperReveal(this.bscene, t, "revealLeft");
        
        let ir:ImageReveal=new ImageReveal(this.bscene,t,"paperBottom","revealLeft");
        ir.animate();

        let reveal: Reveal = new Reveal(this.bscene, t, 'revealLeft');
        reveal.callback = () => { this.effectDone(t, el) };
       reveal.animate();

       // let top:Phaser.GameObjects.Sprite=this.scene.add.sprite(t.x+t.displayWidth,t.y,"paperTop").setOrigin(0,0);
        //top.displayHeight=ir.tm.image.displayHeight;

       // this.scene.tweens.add({ targets: top, duration: 1000, x: t.x,})
    }
    private basicAnimation(t: Phaser.GameObjects.Text, el: string, animationName: string, placement: number,secs:number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
      //  t.visible = false;
        
        let animOnText: AnimationOnText = new AnimationOnText(this.bscene, t, animationName, placement);
        animOnText.player.play();
        this.scene.children.bringToTop(t);

        setTimeout(() => {            
            t.visible=false;
            this.formUtil.unblankElement(el);
        }, secs*2)
    }
    
    private starBurst(t: Phaser.GameObjects.Text, el: string, animationName: string,secs:number) {

        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.visible = false;

        let animOnText: AnimationOnText = new AnimationOnText(this.bscene, t, animationName, 1);
        animOnText.player.play();

        setTimeout(() => {            
            t.visible=false;
            this.formUtil.unblankElement(el);
        }, secs*2)
    }
    private spotLight(t: Phaser.GameObjects.Text, el: string, secs: number) {

        t.visible=false;
       
        let el2:HTMLElement | null=document.getElementById(el);
        if (el2)
        {
            let spot:Spotlight=new Spotlight();
            let r:ClientRect=el2.getBoundingClientRect()
            spot.run(r,secs);
        }
       
    }
}