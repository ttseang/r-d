import MainStorage from "../classes/MainStorage";
import { AttsVo } from "../dataObjs/AttsVo";
import { EffectParamVo } from "../dataObjs/EffectParamVo";

import { HighlightVo } from "../dataObjs/HighlightVo";
import { KeyframeVo } from "../dataObjs/KeyframeVo";

export class PageLoader {
    private ms: MainStorage = MainStorage.getInstance();
    private callback: Function;

    constructor(callback: Function) {
        this.callback = callback;
    }
    public loadPage(file: string) {
        let fileName = "./pages/"+this.ms.book+"/" + file;
        fetch(fileName)
            .then(response => response.json())
            .then(data => this.process({ data }));
    }
    process(results: any) {
        this.ms.keyFrames = [];

        // //console.log(results);
        let page: any = results.data.page;
        let backgroundImage:string=page.backgroundImage;
        let backgroundMusic:string=page.backgroundMusic;
        
        this.ms.backgroundImage=backgroundImage;
        this.ms.backgroundMusic=backgroundMusic;
        

        if (page.statics)
        {
            for (let k:number=0;k<page.statics.length;k++)
            {
                let staticItem:any=page.statics[k];
                //console.log(staticItem);
                let staticAtt:AttsVo=new AttsVo(staticItem.type,parseFloat(staticItem.x),parseFloat(staticItem.y),staticItem.textKey,parseFloat(staticItem.scale),parseFloat(staticItem.angle),parseFloat(staticItem.alpha),parseInt(staticItem.origin))
                if (staticItem.tint)
                {
                    staticAtt.tint=staticItem.tint;
                }
                if (staticItem.w)
                {
                    staticAtt.width=parseFloat(staticItem.w);
                }
                if (staticItem.h)
                {
                    staticAtt.height=parseFloat(staticItem.h);
                }
                this.ms.statics.push(staticAtt);
            }
        }

        let keyframes: any = page.keyframes;

        for (let i: number = 0; i < keyframes.length; i++) {
            let keyFrame: KeyframeVo = new KeyframeVo(keyframes[i].itemName, parseInt(keyframes[i].start), keyframes[i].eventType);
            if (keyframes[i].cssClass) {
                keyFrame.cssClass = keyframes[i].cssClass;
            }
            if (keyframes[i].cssSize) {
                keyFrame.cssSize = keyframes[i].cssSize;
            }
            if (keyframes[i].useTween) {
                keyFrame.useTween = keyframes[i].useTween;
            }
            
            if (keyframes[i].atts) {
                let att: any = keyframes[i].atts;
                let attsVo: AttsVo = new AttsVo(att.type, parseFloat(att.x), parseFloat(att.y), att.textKey, parseFloat(att.scale), parseFloat(att.angle), parseFloat(att.alpha),parseInt(att.origin))
                keyFrame.atts = attsVo;
                if (keyframes[i].atts.col) {
                    attsVo.col = parseFloat(keyframes[i].atts.col);
                }
                if (keyframes[i].atts.loadEffect)
                {
                    attsVo.loadEffect=parseInt(keyframes[i].atts.loadEffect);
                }
                if (keyframes[i].atts.background)
                {
                    attsVo.background=keyframes[i].atts.background;
                }
                if (keyframes[i].atts.tint)
                {
                    attsVo.tint=keyframes[i].atts.tint;
                }
                if (keyframes[i].atts.w)
                {
                    attsVo.width=parseFloat(keyframes[i].atts.w);
                }
                if (keyframes[i].atts.h)
                {
                    attsVo.height=parseFloat(keyframes[i].atts.h);
                }
                if (keyframes[i].atts.maskKey)
                {
                    attsVo.maskKey=keyframes[i].atts.maskKey;
                    console.log("MASKKEY "+attsVo.maskKey);
                }
                if (keyframes[i].atts.effectParams)
                {
                    let params:any=keyframes[i].atts.effectParams;
                    attsVo.effectParams=new EffectParamVo();
                    attsVo.effectParams.load(params);
                    console.log(attsVo);
                }
               
            }
            if (keyframes[i].highlights) {
                let hg: any = keyframes[i].highlights;
                for (let j: number = 0; j < hg.length; j++) {
                    let highlight: HighlightVo = new HighlightVo(hg[j].cssClass, hg[j].effectName, parseFloat(hg[j].duration));
                    keyFrame.highlights.push(highlight);
                }
            }
            

            //console.log(keyFrame);
            this.ms.addKeyFrame(keyFrame);
        }
        this.ms.keyFrames.sort((a: KeyframeVo, b: KeyframeVo) => (a.start < b.start) ? -1 : 1);
        // //console.log(this.ms.keyFrames);
        this.callback();
    }
}