export class EffectParamVo
{
    public startX:number=50;
    public startY:number=40;
    public endX:number=50;
    public endY:number=40;
    public zoomStart:number=1;
    public zoomEnd:number=1;
    public angleStart:number=0;
    public angleEnd:number=0;
    public duration:number=1000;
    
    constructor()
    {

    }
    load(obj:any)
    {        
        for (const property in obj) {
            this[property]=obj[property];
          }
         // console.log(this);
    }
}