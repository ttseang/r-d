import { AttsVo } from "./AttsVo";
import { KeyframeVo } from "./KeyframeVo";

export class PageVo {
    public title: string;
    public backgroundImage: string;
    public backgroundMusic: string;
    public keyframes:KeyframeVo[]=[];
    public statics:AttsVo[]=[];
    
    constructor(title: string, backgroundImage: string, backgroundMusic: string) {
        this.title = title;
        this.backgroundImage = backgroundImage;
        this.backgroundMusic = backgroundMusic;
        
    }
}