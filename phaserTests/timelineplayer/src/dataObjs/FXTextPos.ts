import { GameObjects } from "phaser";

export class FXTextPos
{
    public spanName:string;
    public effectText:GameObjects.Text;

    constructor(spanName:string,effectText:GameObjects.Text)
    {
        this.spanName=spanName;
        this.effectText=effectText;
    }
}