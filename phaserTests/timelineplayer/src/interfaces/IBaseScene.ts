
//import Phaser from "phaser";
import { AlignGrid } from "../util/alignGrid";

export interface IBaseScene
{
    getW():number;
    getH():number;
    getScene():Phaser.Scene;
    getGrid():AlignGrid;
    ch:number;
    cw:number;
    cd:number;
}
export default IBaseScene