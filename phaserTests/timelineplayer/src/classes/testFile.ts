import { AttsVo } from "../dataObjs/AttsVo";
import { HighlightVo } from "../dataObjs/HighlightVo";
//import { EffectsAttsVo } from "../dataObjs/effectAttsVo";
import { KeyframeVo } from "../dataObjs/KeyframeVo";
import { PageVo } from "../dataObjs/PageVo";
//import { TextClasses } from "../dataObjs/TextClasses";
import MainStorage from "./MainStorage";

export class TestFile
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor()
    {

        let page:PageVo=new PageVo("Page 1","bg1","");        

        page.backgroundMusic="FreshAndCool";
        
        let kf0:KeyframeVo=new KeyframeVo("audio1",-1,"playAudio");
        //kf0.atts=new AttsVo("a",0,0,"audio1");
        page.keyframes.push(kf0);


        let kf2:KeyframeVo=new KeyframeVo("c1",2000,"update");
        kf2.atts=new AttsVo("s",2.5,4,"circle",0.5,90,1);
        kf2.atts.tint=0x2980b9;        
        page.keyframes.push(kf2);

       let kf5:KeyframeVo=new KeyframeVo("textback",0,"update");
       kf5.atts=new AttsVo("s",8,3,"holder",0.35,90,1);
        kf5.atts.tint=0x000000;
     //  kf5.atts.tint=0x2980b9;    
     //  page.keyframes.push(kf5);

        let kf1:KeyframeVo=new KeyframeVo("text1",1000,"update");
        //kf1.centerText=true;
        kf1.atts=new AttsVo("h",7.5,1,"A delicious [history] of<br/> [Ice Cream.]<br/> Who Invented Ice Cream?",1,90,1);
        kf1.atts.col=-1;
        kf1.cssClass="bookText";
        kf1.highlights=[new HighlightVo("pink","halfRays",1000),new HighlightVo("blue","brackets",1000)];

        page.keyframes.push(kf1);

     //   this.ms.addKeyFrame(kf1);

        let kf7:KeyframeVo=new KeyframeVo("icecream1",2000,"update");
        kf7.atts=new AttsVo("im",2.5,4,"pimage1",0.6,90,1);
        kf7.atts.loadEffect=4;
        //kf7.useTween=true;
        page.keyframes.push(kf7);


       

        let kf8:KeyframeVo=new KeyframeVo("text1",5000,"update");
       //kf8.centerText=true;
       kf8.atts=new AttsVo("h",7.5,2,"It all begins with [snow]!",1,90,1);
      // kf8.atts.col=3;
       kf8.cssClass="bookText";
       kf8.highlights=[new HighlightVo("blue","noEffect",0),new HighlightVo("blue","noEffect",0)];

       page.keyframes.push(kf8);
       

/* 
        let kf2:KeyframeVo=new KeyframeVo("text3",3000,"update");
        kf2.centerText=true;
        kf2.atts=new AttsVo("h",2,2,"You just put ED on the end",1,90,1);
        kf2.highlights=[new HighlightVo(13,2,"blue","brackets",2000)];
        this.ms.addKeyFrame(kf2); */

       /*  let kf3:KeyframeVo=new KeyframeVo("text2",4000,"update");
        kf3.centerText=true;
        kf3.atts=new AttsVo("h",2,4,"The guys wash cars on the weekend",1,90,1);
        kf3.highlights=[new HighlightVo(9,4,"green","noEffect",1000)];
        this.ms.addKeyFrame(kf3); 

        let kf5:KeyframeVo=new KeyframeVo("text2",5000,"update");
        kf5.centerText=true;
        kf5.atts=new AttsVo("h",2,4,"The guys wash cars on the weekend",1,90,1);
        kf5.highlights=[new HighlightVo(9,4,"green","starBurst",1000)];
        this.ms.addKeyFrame(kf5);


        let kf6:KeyframeVo=new KeyframeVo("text2",6000,"update");
        kf6.centerText=true;
        kf6.atts=new AttsVo("h",2,4,"The guys washed cars on the weekend",1,90,1);
        kf6.highlights=[new HighlightVo(9,6,"green","noEffect",0)];
        this.ms.addKeyFrame(kf6);

        let kf7:KeyframeVo=new KeyframeVo("face",8000,"update");
        kf7.atts=new AttsVo("s",5,6,"face",1,90,0.1);
        kf7.useTween=true;
        this.ms.addKeyFrame(kf7);


        let kf8:KeyframeVo=new KeyframeVo("face",10000,"update");
        kf8.atts=new AttsVo("s",11,0,"face",0.1,90,1);
        
        this.ms.addKeyFrame(kf8); */

      //  kf1.textClasses=new TextClasses("standard","standard",true);
      //  kf1.effectAtts=new EffectsAttsVo("noEffect","",0);
       // 


        /* let kf2:KeyframeVo=new KeyframeVo("text3",2000,"update");
        kf2.atts=new AttsVo("h",2,2,"You just put ED on the end",1,90,1);
        kf2.textClasses=new TextClasses("standard","blue",true);
        kf2.effectAtts=new EffectsAttsVo("brackets","ED",3000);
        this.ms.addKeyFrame(kf2);
 */

       /*  let kf3:KeyframeVo=new KeyframeVo("text2",4000,"update");
        kf3.atts=new AttsVo("h",2,4,"The guys wash cars on the weekend",1,90,1);
        kf3.textClasses=new TextClasses("standard","blue",true);
        kf3.effectAtts=new EffectsAttsVo("","",0);
        this.ms.addKeyFrame(kf3); 


        let kf4:KeyframeVo=new KeyframeVo("text2",4000,"update");
        kf4.atts=new AttsVo("h",2,4,"The guys wash cars on the weekend",1,90,1);
        kf4.textClasses=new TextClasses("standard","red",true);
        kf4.effectAtts=new EffectsAttsVo("noEffect","wash",3000);
        this.ms.addKeyFrame(kf4);


        let kf5:KeyframeVo=new KeyframeVo("text2",6000,"update");
        kf5.atts=new AttsVo("h",2,4,"The guys wash cars on the weekend",1,90,1);
        kf5.textClasses=new TextClasses("standard","blue",true);
        kf5.effectAtts=new EffectsAttsVo("starBurst","wash",1000);
        this.ms.addKeyFrame(kf5);

         let kf6:KeyframeVo=new KeyframeVo("text2",8000,"update");
        kf6.atts=new AttsVo("h",2,4,"The guys washed cars on the weekend",1,90,1);
        kf6.textClasses=new TextClasses("standard","red",true);
        kf6.effectAtts=new EffectsAttsVo("noEffect","washed",3000);
        this.ms.addKeyFrame(kf6);
*/
     

       
        this.ms.keyFrames=page.keyframes;
        this.ms.backgroundMusic=page.backgroundMusic;
        this.ms.backgroundImage=page.backgroundImage;
        this.ms.keyFrames.sort((a:KeyframeVo,b:KeyframeVo)=>(a.start<b.start)?-1:1);
        console.log(this.ms.keyFrames);
        let js:any={page:page};
        let j:string=JSON.stringify(js);
        console.log(j);
    }
}