import { GameObjects } from "phaser";
import { AttsVo } from "../dataObjs/AttsVo";
import { EffectDataVo } from "../dataObjs/EffectDataVo";
import { FXTextPos } from "../dataObjs/FXTextPos";
import { GameObj } from "../dataObjs/GameObj";
import { HighlightVo } from "../dataObjs/HighlightVo";
import { KeyframeVo } from "../dataObjs/KeyframeVo";
import { PosVo } from "../dataObjs/PosVo";
//import { ImageMask } from "../effects/imageMask";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { EffectUtil } from "../util/EffectUtil";
import { FormUtil } from "../util/formUtil";
import { ImageEffectUtil } from "../util/ImageEffectsUtil";
import EffectsDef from "./EffectsDef";
import { MaskedImage } from "./MaskedImage";

export class LayoutManager {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    private objMap: Map<string, GameObj> = new Map();
    private effectTexts: Map<string, GameObjects.Text> = new Map();
    private grid: AlignGrid;
    private formUtil: FormUtil;
    private effectUtil: EffectUtil;
    private imageEffects: ImageEffectUtil;
    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.grid = bscene.getGrid();
        this.formUtil = new FormUtil(bscene);
        this.formUtil.alignGrid=this.grid;
        
        this.effectUtil = new EffectUtil(bscene, this.formUtil);
        this.imageEffects = new ImageEffectUtil(bscene);
        this.effectUtil.loadAnimations();

       // this.formUtil.alignGrid.showPos();

        this.makeTextHolder();
    }
    
    makeTextHolder() {
        if (document.getElementById('textholder') === null) {
            let h: HTMLElement = document.createElement('div');


            h.id = "textholder"
            h.style.position = "initial";

            let gameDiv: HTMLElement | null = document.getElementById('phaser-game');
            if (gameDiv) {
                gameDiv.appendChild(h);
            }
        }

    }
    clearAll() {
        let th: HTMLElement | null = document.getElementById('textholder');
        if (th !== null) {

            th.innerHTML = "";
        }
        this.objMap.clear();
        this.effectTexts.clear();
        /*   this.objMap.forEach((item:GameObj)=>{
              
          }) */
    }
    getObj(k: KeyframeVo) {
        if (this.objMap.has(k.itemName)) {
            let gameObj: GameObj | undefined = this.objMap.get(k.itemName);
            return gameObj;
        }
        else {
            //////console.log("create obj");
            //////console.log(k);

            if (k.atts) {
                if (k.atts.textKey) {
                    switch (k.atts.type) {
                        case "s":

                            let s: GameObjects.Sprite = this.scene.add.sprite(0, 0, k.atts?.textKey);
                            let gameObj = new GameObj(k.itemName, k.atts.type, s);
                            this.objMap.set(k.itemName, gameObj);
                            return gameObj;

                        case "t":

                            let t: GameObjects.Text = this.scene.add.text(0, 0, k.atts?.textKey);
                            let gameObjt = new GameObj(k.itemName, k.atts.type, t);
                            this.objMap.set(k.itemName, gameObjt);
                            return gameObjt;

                        case "im":

                            let im: MaskedImage = new MaskedImage(this.bscene, k.atts.maskKey, k.atts.textKey, k.atts.scale);
                            // im.setFullMask();
                            let gameObjIM = new GameObj(k.itemName, k.atts.type, im);
                            this.objMap.set(k.itemName, gameObjIM);
                            return gameObjIM;

                        case "h":
                            let h: HTMLElement = document.createElement('div');


                            h.id = k.itemName;
                            
                            if (k.cssSize)
                            {
                                h.classList.add(k.cssSize);
                            }

                            if (k.cssClass !== "") {
                                h.classList.add(k.cssClass);
                            }
                            else {
                                h.classList.add('titleText');
                            }


                            let gameDiv: HTMLElement | null = document.getElementById('textholder');
                            if (gameDiv) {
                                gameDiv.appendChild(h);
                            }



                            let gameObjh = new GameObj(k.itemName, k.atts.type, h);
                            this.objMap.set(k.itemName, gameObjh);

                            return gameObjh;
                    }
                }
            }
        }
    }
    setHtmlText(h: HTMLElement, k: KeyframeVo) {
        ////console.log("set html text");
        if (k.atts) {
            if (k.highlights) {
                let mainText: string = k.atts.textKey;
                let parts:string[]=mainText.split("]");
               // let segs: string[] = [];
                let htext:string="";
                let spanCount:number=0;

                //console.log(parts);
                for (let i:number=0;i<parts.length;i++)
                {
                    let part:string=parts[i];
                   

                    let parts2:string[]=part.split("[");
                    if (parts2.length>1)
                    {
                        //console.log(parts2);
                        htext+=parts2[0];
                        let spanName: string = k.itemName + "_high_" + i.toString();
                        let highlight:HighlightVo=k.highlights[spanCount];
                        highlight.scanned=parts2[1];
                        let spanText: string = "<span id='" + spanName + "' class='"+k.cssSize+" " + highlight.cssClass + "'>" + parts2[1] + "</span>";
                      //  let spanText: string = "<span id='" + spanName + "' class='"+ highlight.cssClass + "'>" + parts2[1] + "</span>";
                      
                        htext+=spanText;
                        spanCount++;
                    }
                    else
                    {
                        htext+=part;
                    }
                }
                

                h.innerHTML = htext;

            }

        }
    }
    placeObj(k: KeyframeVo) {
        let gameObj: GameObj | undefined = this.getObj(k);
        //////console.log(gameObj);

        if (gameObj) {

            //attributes are part of the keyframe
            //until epoch and 
            //then transfer the values to the object
            gameObj.atts=k.atts;

            if (gameObj.type === "h") {
                if (k.atts) {
                    let h: HTMLElement = gameObj.obj as HTMLElement;
                    ////console.log(h);
                    let index: number = this.grid.getIndexByXY(k.atts.x, k.atts.y);
                    this.formUtil.placeElementAt(index, h.id, false, false);

                 //   let effectText: GameObjects.Text | undefined = this.getEffectText(gameObj.name);
                    console.log(gameObj);
                   /*  for (let j:number=0;j<gameObj.fxText.length;j++)
                    {
                        let effectText:GameObjects.Text=gameObj.fxText[j].effectText;
                        //console.log(h);
                       
                    }  */
                    this.setHtmlText(h, k);
                    if (k.atts.background)
                    {
                        let tbackground:GameObjects.Sprite=this.scene.add.sprite(0,0,k.atts.background).setOrigin(0,0);
                        let size:any=this.formUtil.getSize(h.id);
                        let w1:number=size.w;
                        let h1:number=size.h;
                        tbackground.displayWidth=w1;
                        tbackground.displayHeight=h1;

                        this.grid.placeAt(k.atts.x,k.atts.y,tbackground);
                        k.atts.background="";
                        if (k.atts.tint)
                        {
                            //console.log("tint="+k.atts.tint);
                            
                            if (k.atts.tint!=-1)
                            {
                                tbackground.setTint(k.atts.tint); 
                            }
                          
                        }
                    }
                    /*   if (k.effectText)
                      {
                          k.effectText.setText(k.effectAtts.highlight);
                          ////console.log("set to "+k.effectAtts.highlight);
                      } */

                    /*  if (k.centerText === true) {
                         this.formUtil.centerElement(h.id);
                     } */
                    if (k.atts.col !== -1) {
                        this.formUtil.columnElement(h.id, k.atts.col);
                    }
                }

            }
            if (gameObj.type === "s") {
                let s: GameObjects.Sprite = gameObj.obj as GameObjects.Sprite;
                if (k.atts) {
                    if (k.useTween === false) {
                        this.placeItem(s, k.atts);
                    }
                    else {
                        this.tweenItem(s, k.atts);
                    }

                    if (k.atts.loadEffect !== 0) {
                        if (k.atts.effectParams==null)
                        {
                        this.imageEffects.doImageEffect(s, k.atts.loadEffect, this.imageEffectDone.bind(this));
                        }
                        else
                        {
                            this.imageEffects.doCustom(s,k.atts.effectParams,this.imageEffectDone.bind(this));
                        }
                        k.atts.loadEffect = 0;
                    }
                }

            }
            if (gameObj.type === "t") {
                let t: GameObjects.Text = gameObj.obj as GameObjects.Text;
                if (k.atts) {
                    if (k.useTween === false) {
                        this.placeItem(t, k.atts);
                    }
                    else {
                        this.tweenItem(t, k.atts);
                    }
                }
            }
            if (gameObj.type === "im") {
                let im: MaskedImage = gameObj.obj as MaskedImage;

                if (k.atts) {
                    if (k.useTween === false) {
                        this.placeItem(im, k.atts);
                    }
                    else {
                        this.tweenItem(im, k.atts);
                    }

                    if (k.atts.loadEffect !== 0) {
                        if (k.atts.effectParams==null)
                        {
                        this.imageEffects.doImageEffect(im.image, k.atts.loadEffect, this.imageEffectDone.bind(this));
                        }
                        else
                        {
                            this.imageEffects.doCustom(im.image,k.atts.effectParams,this.imageEffectDone.bind(this));
                        }
                        k.atts.loadEffect = 0;
                    }
                }

            }
        }
    }
    private imageEffectDone() {

    }
    private placeItem(iGameObj: IGameObj, atts: AttsVo) {

        //console.log(atts);

        if (atts.type === "s") {
            
            if (atts.scale!=-1)
            {
                Align.scaleToGameW(iGameObj, atts.scale, this.bscene);
            }

            if (atts.width!=-1)
            {
                iGameObj.displayWidth=this.grid.cw*atts.width;
            }
            if (atts.height!=-1)
            {
                iGameObj.displayHeight=this.grid.ch*atts.height;
            }
            
            let s:GameObjects.Sprite=iGameObj as GameObjects.Sprite;
            
            if (atts.origin===0)
            {
                s.setOrigin(0,0);
            }
            else
            {
                s.setOrigin(0.5,0.5);
            }

            iGameObj.alpha = atts.alpha;
            if (atts.tint!==-1)
            {
                s.setTint(atts.tint);
            }
        }
        if (atts.type === "t") {
            let t: GameObjects.Text = iGameObj as GameObjects.Text;
            t.setText(atts.textKey);

            

        }
        if (atts.type === "im") {
            let im: MaskedImage = iGameObj as MaskedImage;
            ////console.log(iGameObj);
            ////console.log(atts);
            im.image.alpha = atts.alpha;
            if (im.inPlace===false)
            {
                this.grid.placeAt(atts.x, atts.y, im.mask);
                im.inPlace=true;
            }
           
            this.grid.placeAt(atts.x, atts.y, im.image);
            return;
        }

        
        this.grid.placeAt(atts.x, atts.y, iGameObj);                   //
        //

        //iGameObj.x = atts.x;
        // iGameObj.y = atts.y;


    }
    onResize()
    {
        console.log(this);
        this.grid=this.bscene.getGrid();
        this.formUtil.alignGrid=this.bscene.getGrid();

        this.objMap.forEach((gameObj:GameObj)=>{
            //console.log(gameObj.name,gameObj.type);
            if (gameObj.type==="s" || gameObj.type==="t")
            {
                //console.log(gameObj.atts.x,gameObj.atts.y);
              //  console.log(gameObj.name+" at "+gameObj.atts.x+","+gameObj.atts.y);
                //this.grid.placeAt2(11,11,gameObj.obj);
                this.grid.placeAt(gameObj.atts.x,gameObj.atts.y,gameObj.obj);
            }
            if (gameObj.type==="h")
            {
                let h: HTMLElement = gameObj.obj as HTMLElement;
                ////console.log(h);
                let index: number = this.grid.getIndexByXY(gameObj.atts.x, gameObj.atts.y);
                this.formUtil.placeElementAt(index, h.id, false, false);

                for (let j:number=0;j<gameObj.fxText.length;j++)
                {
                    this.effectUtil.lineUpToHtml(gameObj.fxText[j].effectText,gameObj.fxText[j].spanName);
                }
               
            }

            if (gameObj.type==="im")
            {
                let im: MaskedImage = gameObj.obj as MaskedImage;
                im.resize(gameObj.atts.scale);
                this.grid.placeAt(gameObj.atts.x, gameObj.atts.y, im.mask);
                this.grid.placeAt(gameObj.atts.x, gameObj.atts.y, im.image);
                
            }
        });
    }
    private tweenItem(iGameObj: IGameObj, atts: AttsVo) {

        if (atts.type === "t") {
            let t: GameObjects.Text = iGameObj as GameObjects.Text;
            t.setText(atts.textKey);
        }
        if (atts.type === "im") {
            let im: MaskedImage = iGameObj as MaskedImage;
            let posVo: PosVo = this.grid.getRealXY(atts.x, atts.y);
            this.scene.tweens.add({ targets: im.image, duration: 1000, y: posVo.y, x: posVo.x, alpha: atts.alpha });
            return;
        }
        let posVo: PosVo = this.grid.getRealXY(atts.x, atts.y);
        ////console.log(posVo);
        this.scene.tweens.add({ targets: iGameObj, duration: 1000, y: posVo.y, x: posVo.x, alpha: atts.alpha });
    }
    private getEffectText(objName: string, create: boolean = true) {
        if (this.effectTexts.has(objName)) {
            return this.effectTexts.get(objName);
        }
        if (create === false) {
            return undefined;
        }
        let t: Phaser.GameObjects.Text = this.scene.add.text(0, 0, "effect text");
        t.visible = false;
        this.effectTexts.set(objName, t);
        return t;
    }
    public doEffect(k: KeyframeVo) {
        let gameObj: GameObj | undefined = this.getObj(k);
        ////console.log(gameObj);

        if (gameObj) {

            let fxData: EffectsDef = EffectsDef.getInstance();

            if (k.highlights) {
                for (let i: number = 0; i < k.highlights.length; i++) {
                    let hightlight: HighlightVo = k.highlights[i];
                    ////console.log(hightlight.scanned);
                    let effectVo: EffectDataVo = fxData.getEffectData(hightlight.effectName);
                    ////console.log(effectVo);
                    let spanName: string = k.itemName + "_high_" + i.toString();

                    if (effectVo.phaserEffect !== "none") {
                        let textName: string = k.itemName + "_high" + i.toString();
                        let t: GameObjects.Text | undefined = this.getEffectText(textName, true);
                        if (t) {
                            t.setText(hightlight.scanned);
                            
                            gameObj.fxText.push(new FXTextPos(spanName,t));

                            if (effectVo.phaserEffect !== "none") {
                                this.effectUtil.doEffect(effectVo.phaserEffect, t, spanName, hightlight.duration);
                            }

                        }
                    }
                    if (effectVo.mainClass !== "noEffect") {
                        let h: HTMLElement = gameObj.obj as HTMLElement;
                        ////console.log(h);
                        h.className = effectVo.mainClass;
                    }
                    if (effectVo.hClass !== "noEffect") {

                        let highSpan: HTMLElement | null = document.getElementById(spanName);
                        ////console.log(highSpan);

                        if (highSpan) {
                            highSpan.className = effectVo.hClass;
                        }
                    }
                }
            }
          

        }


    }
}