import {TextVo} from "../dataObj/textvo";

export class SubParser
{
   
    static parseSrt(text)
    {
        let texts = [];
        /**
         * Split each entry into its own element
         * 
         * Each element will be a string that looks like this
         * "2", "00:00:00,133 --> 00:00:06,699", "Hi this is the text""
         * 
         */
         let items = text.split('\n\r\n');
         //  console.log(items);
         //break down each element
         items.forEach(element => {            
             /*
                     turn the string into a 3 element array
                     e[0] is the index of the subtitle, 1,2,3 etc. We don't need it
                     e[1] is the time "00:00:00,133 --> 00:00:06,699"
                     e[2] is the text
                 **/
             let e = element.split("\n");
            
             //if the array is not 3 elements it has blank text
             //or is an empty line and we can ignore it
             if (e.length > 1) {
                     /**
                      * Split the time string into a start
                      * and ending time
                      */
                     let time = e[1].split(" --> ");
                 //if for some reason there is not a start and ending time
                 //ignore this entry
                 if (time.length > 1) {
 
                     /* there are some extra numbers at the end of the time
                     I'm not sure what they are for but we don't need them
                     we can remove them by using split by comma
                     and taking the first element 
                     
                     */
 
                     let startTime=time[0].split(",")[0];
                     let endTime=time[1].split(",")[0];
                     let text=e[2];
 
                     //make a text value object
                     let textVo = new TextVo(startTime,endTime, text);
                     //if the converted seconds inside the object is not a number
                     //do not add to the list
                     if (!isNaN(textVo.startTime)) {
                         texts.push(textVo);
                     }
                 }
             }
 
         });
 
         return texts;
    }
    static parseVTT(text)
    {
        let texts=[];
        //break the text by lines
        let items = text.split('\n');
       
        for(let i=0;i<items.length;i++)
        {
            let item=items[i];
            
            /**
             * if the item string contains --> we know it is a time stamp
             * and we know the next line is the text
             */
            if (item.indexOf("-->")>-1)
            {
                
                let time=item.split("-->");
                if (i<items.length-1)
                {
                    let startTime=time[0];
                    let endTime=time[1];
                    let text=items[i+1];

                    /* there are some extra numbers at the end of the time
                     I'm not sure what they are for but we don't need them
                     we can remove them by using split by dot
                     and taking the first element                      
                     */
                    startTime=startTime.split(".")[0];
                    endTime=endTime.split(".")[0];

                    //make a text value object
                    let textVo=new TextVo(startTime,endTime,text);

                    //if the converted seconds inside the object is not a number
                     //do not add to the list

                    if (!isNaN(textVo.startTime)) {
                        texts.push(textVo);
                    }
                }     
            }
        }
        return texts;
    }
}