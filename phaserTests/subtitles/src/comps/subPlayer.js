import { UIBlock } from "../util/UIBlock";
import { SubParser } from "../util/subparser";

export class SubPlayer extends UIBlock {
    constructor(scene, textKey, soundKey, useVTT = false) {
        super();
        this.scene = scene;
        //init variables

        //this is the index of the subtitle array
        this.textIndex = -1;

        //set the current start and end times to make sure 
        //they are out of bounds of the current time at the start
        this.startTime = 1000;
        this.endTime = 1000;

        //the time of the sound object
        this.currentTime = 0;

        //canPlay is set to false when we reach the end of the file
        this.canPlay = true;

        //add a text object to the stage
        this.dText = this.scene.add.text(0, 0, "TEXT", { fontFamily:"Impact",color: 'green', fontSize: '20px' });
        this.dText.setWordWrapWidth(400);
        this.add(this.dText);
        //convert the text into objects
        this.subs = this.parseText(textKey, useVTT);

        //create the phaser sound object
        this.sound = this.scene.sound.add(soundKey);
        this.sound.on(Phaser.Sound.Events.PAUSE, this.onPause.bind(this));
        this.sound.on(Phaser.Sound.Events.RESUME, this.onResume.bind(this));

        //set the first subtitle object to current
        this.nextText();
        //show the text of the currect subtitle object
        this.setText();

        //play the sound and pause right away so we 
        //can use pause and resume instead of play and stop
        //otherwise the sound will restart
        this.sound.play();
        this.sound.pause();
    }
    play() {
        this.sound.resume();
    }
    pause() {
        this.sound.pause();
    }
    onPause() {
        if (this.wordTimer) {
            this.wordTimer.remove();
        }
    }
    onResume() {
        if (this.wordTimer) {
            this.wordTimer.remove();
        }
        //make a timer to check the time
        this.wordTimer = this.scene.time.addEvent({ delay: 300, callback: this.checkTime.bind(this), loop: true });
    }
    setText() {
        //set the text to the current object's text
        this.dText.setText(this.currentSub.text);
    }
    clearText() {
        this.dText.setText("");
    }
    parseText(file, useVtt = false) {        
        let vtext = this.scene.cache.text.get(file);
        if (useVtt == true) {
            return SubParser.parseVTT(vtext);
        }
        return SubParser.parseSrt(vtext);
    }
    nextText() {
        this.textIndex++;
        if (this.textIndex == this.subs.length) {
            this.canPlay = false;
            return;
        }
        this.currentSub = this.subs[this.textIndex];
        this.startTime = this.currentSub.startTime;
        this.endTime = this.currentSub.endTime;
    }
    checkTime() {
        if (this.canPlay == true) {
            this.currentTime = this.sound.getCurrentTime();
            if (this.currentTime > this.startTime && this.currentTime < this.endTime) {
                this.setText();
            }
            if (this.currentTime > this.endTime) {
                this.clearText();
                this.nextText();
            }
        }
    }
}