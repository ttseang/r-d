//import Phaser from 'phaser';
import { SceneMain } from './SceneMain';



const config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: 800,
    height: 600,
    scene: [SceneMain]
};

const game = new Phaser.Game(config);
