import { SubPlayer } from "./comps/subPlayer";
import { TextVo } from "./dataObj/textvo";
import { SubParser } from "./util/subparser";
import ToggleButton from "./ui/toggleButton";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        //load toggle button assets
        
        this.load.image("toggleback", "./assets/images/ui/toggles/1.png");
        this.load.image("sfxOn", "./assets/images/ui/icons/sfxOn.png");
        this.load.image("sfxOff", "./assets/images/ui/icons/sfxOff.png");

        //load the subtitle files

        //this.load.text("voice", "./assets/1_1_games_intro.srt");
        this.load.text("voice2", "./assets/1_1_games_intro.vtt");

        this.load.audio("intro", "./assets/1_1_introduction.wav");
    }
    create() {

        //make a toggle button
        const toggle = new ToggleButton({
            scene: this,
            key: 'toggleback',
            onIcon: 'sfxOn',
            offIcon: 'sfxOff',
            isOn: false,
            callback: this.toggleSound.bind(this)
        });
        toggle.x = 400;
        toggle.y = 500;

        //make a subtitle player
        //parameters scene, subtitle file key, sound key, use VTT (true) or SRT(false)
        this.player = new SubPlayer(this, "voice2", "intro", true);
        this.player.x = 200;
        this.player.y = 200;
    }
    toggleSound(val) {       
        if (val == true) {
            this.player.play();
        }
        else {
            this.player.pause();
        }
    }
}