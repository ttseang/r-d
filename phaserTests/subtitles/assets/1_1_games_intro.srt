﻿1
00:00:00,000 --> 00:00:00,133

2
00:00:00,133 --> 00:00:06,699
Hi my name is William and I want thank you for taking this course Let me show you some of the games that we are going to be making

3
00:00:06,700 --> 00:00:13,000
the first is called road rush it's a game where we have to click on the roads to avoid obstacles

4
00:00:13,000 --> 00:00:17,900
Not only is it a good game to get started on the basics of Phaser we will also be making a 

5
00:00:17,900 --> 00:00:22,833
reusable template to help us make games faster in the future

6
00:00:22,833 --> 00:00:28,999
the next is pong frenzy and while it may look like a simple game is a lot harder than it looks

7
00:00:29,000 --> 00:00:35,700
and it's a great game to build to be able to learn the basics of physics in Phaser

8
00:00:35,700 --> 00:00:42,966
and the last one we will be making is a space battle game that introduces a lot of advanced game techniques

9
00:00:42,966 --> 00:00:46,699
so let's get started building some games

