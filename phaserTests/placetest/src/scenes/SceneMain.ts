import { GameObjects } from "phaser";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.image("heart","./assets/heart.png");
        this.load.image("num5","../assets/num5.png");
        this.load.image("5_1b","../assets/5_1b.png");
        this.load.image("5_2b","../assets/5_2b.png");
        this.load.image("5_3b","../assets/5_3b.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.showNumbers();
        
        let guide:GameObjects.Sprite=this.add.sprite(0,0,"num5").setOrigin(0,0);

        guide.displayWidth=this.cw*5;
        guide.displayHeight=this.ch*6;

        this.grid.placeAt(2,1,guide);


        let part3:GameObjects.Sprite=this.add.sprite(0,0,"5_3b");
        
        part3.displayWidth=this.cw*5;
        part3.displayHeight=this.ch*4;

        this.grid.placeAt(4.5,4.9,part3);

    }
    lightUp(pointer,gameObj)
    {
        gameObj.alpha=1;
    }

}