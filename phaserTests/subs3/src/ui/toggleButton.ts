import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import {UIBlock} from "../util/UIBlock";

export class ToggleButton extends UIBlock
{
    private scene:Phaser.Scene;
    public isOn:boolean;

    constructor(bscene:IBaseScene,config:any)
    {
        super();
       
        this.scene=bscene.getScene();
        this.isOn=true;       
        

        const bg=this.scene.add.image(0,0,config.key);
        const iconOn=this.scene.add.image(0,0,config.onIcon);
        const iconOff=this.scene.add.image(0,0,config.offIcon);

        iconOff.visible=!this.isOn;
        iconOn.visible=this.isOn;

        Align.scaleToGameW(iconOn,0.1,bscene);
        Align.scaleToGameW(iconOff,0.1,bscene);
        Align.scaleToGameW(bg,0.15,bscene);

        this.add(bg);
        this.add(iconOn);
        this.add(iconOff);
        this.setSize(bg.displayWidth,bg.displayHeight);

        if (config.callback)
        {
            bg.setInteractive();
            bg.on('pointerdown',()=>{
                this.isOn=!this.isOn;
                iconOn.visible=this.isOn;
                iconOff.visible=!this.isOn;
                config.callback(this.isOn);
            });
        }
    }
}
export default ToggleButton;