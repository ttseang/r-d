export class TextVo
{
    public startTime:number;
    public endTime:number;
    public text:string;

    constructor(startTime:string,endTime:string,text:string)
    {             
        this.startTime=this.convertToSecs(startTime);
        this.endTime=this.convertToSecs(endTime);
        this.text=text;
    }
    convertToSecs(time:string):number
    {
        //remove the quotes
        time.replace('"','');
        //split the string into an array
        let timeArray=time.split(":");

        let hours=parseInt(timeArray[0]);
        let mins=parseInt(timeArray[1]);
        let secs=parseInt(timeArray[2]);

        let totalSecs=(hours*3600)+(mins*60)+secs;

        return totalSecs;
    }
}
export default TextVo;