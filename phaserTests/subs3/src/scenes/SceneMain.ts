import { SubPlayer } from "../comps/subPlayer";
import ToggleButton from "../ui/toggleButton";
import Align from "../util/align";
import { FormUtil } from "../util/formUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    private player:SubPlayer;
    private formUtil:FormUtil;
    preload() {
        this.load.image("toggleback", "./assets/images/ui/toggles/1.png");
        this.load.image("sfxOn", "./assets/images/ui/icons/sfxOn.png");
        this.load.image("sfxOff", "./assets/images/ui/icons/sfxOff.png");

        //load the subtitle files

        this.load.text("voice", "./assets/icecream1.srt");
       // this.load.text("voice2", "./assets/1_1_games_intro.vtt");

        this.load.audio("intro", "./assets/1.mp3");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.formUtil=new FormUtil(this,11,11);
        
        this.formUtil.placeElementAt(13,'dtext',false,false);
        this.formUtil.showNumbers();

        
        const toggle = new ToggleButton(this,{
            key: 'toggleback',
            onIcon: 'sfxOn',
            offIcon: 'sfxOff',
            isOn: false,
            callback: this.toggleSound.bind(this)
        });
        toggle.x = 400;
        toggle.y = 500;

        //make a subtitle player
        //parameters scene, subtitle file key, sound key, use VTT (true) or SRT(false)
        this.player = new SubPlayer(this, "voice", "intro", true);
        this.player.x = 200;
        this.player.y = 200;
        this.player.callback=this.setHTMLText.bind(this);
     //   this.player.play();
    }
    setHTMLText(html:string)
    {
       // if (this.lastHt)                
        document.getElementById('dtext').innerHTML=html;
       // this.lastHtml=html;

    }
    toggleSound(val:boolean) {       
        if (val == true) {
            this.player.play();
        }
        else {
            this.player.pause();
        }
    }
}