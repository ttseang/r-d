﻿1
00:00:00,000 --> 00:00:00,666


2
00:00:00,666 --> 00:00:07,332
People in China, India, and the Middle East figure out how to 

3
00:00:07,333 --> 00:00:13,999
harvest ice and snow from the mountains in winter and store it in straw-filled caves for summer. 

4
00:00:14,000 --> 00:00:19,566
They use it to make cool drinks of snow, wine, and fruit juice.

