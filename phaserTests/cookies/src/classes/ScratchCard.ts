import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";

export class ScratchCard extends GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private back: GameObjects.Sprite;
    private scratch: GameObjects.Graphics;
    

    constructor(bscene: IBaseScene, key: string, frame: string,color:number) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;
        window['scratch'] = this;

        this.back = this.scene.add.sprite(0, 0, key, frame);
        
        this.back.setTint(color);
        this.add(this.back);


        this.scratch = this.scene.add.graphics();
        this.scratch.visible = false;
        this.scratch.fillStyle(0xff0000,0.1);
        Align.scaleToGameW(this.back,0.19,this.bscene);

        this.back.setMask(new Phaser.Display.Masks.GeometryMask(this.scene,this.scratch));
        this.scene.add.existing(this);
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.setInteractive();
        this.on('pointermove', this.makeScratch.bind(this))
    }
    makeScratch(e: Phaser.Input.Pointer) {
        if (e.isDown) {
            //this.scratch.moveTo(e.x,e.y);
            this.scratch.beginPath();
            this.scratch.fillCircle(e.x, e.y, 30);

            //console.log("scratch");
            //this.rt.draw(this.pen,e.x,e.y);
        }
    }
}