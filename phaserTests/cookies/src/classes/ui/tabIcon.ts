import IBaseScene from "../../interfaces/IBaseScene";

export class TabIcon extends Phaser.GameObjects.Container
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());
        
        this.bscene=bscene;
    }
}