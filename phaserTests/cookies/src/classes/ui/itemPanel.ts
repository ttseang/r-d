import { GameObjects } from "phaser";
import { IconVo } from "../../dataObjs/IconVo";
import { PosVo } from "../../dataObjs/PosVo";
import IBaseScene from "../../interfaces/IBaseScene";
import Align from "../../util/align";
import { AlignGrid } from "../../util/alignGrid";
import { Controller } from "../Controller";
import { CookieModel } from "../CookieModel";
import { LayoutManager } from "../layoutManager";
import { EditButton } from "./editButton";
import { TabButton } from "./tabButton";

export class ItemPanel extends Phaser.GameObjects.Container {
    private bscene: IBaseScene;
    public scene: Phaser.Scene;
    private lm: LayoutManager;
    private buttonArray: EditButton[] = [];
    private back: GameObjects.Image;
    private grid: AlignGrid;
    private gm: CookieModel = CookieModel.getInstance();
    private targetY: number = 0;
    private doorYDir: number = 0;
    private isOpen: boolean = false;
    private itemIcon: GameObjects.Sprite;
    private iconMap: Map<string, EditButton> = new Map();
    private controller: Controller = Controller.getInstance();

    constructor(bscene: IBaseScene, lm: LayoutManager) {
        super(bscene.getScene());
        this.lm = lm;
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.targetY = this.bscene.getH();
        //
        //
        //
        this.back = this.scene.add.image(0, 0, "holder").setOrigin(0, 0);
        this.back.displayWidth = this.bscene.getW();
        this.back.displayHeight = this.bscene.getH() * 0.25;
        this.back.setTint(0xfff000);
        this.add(this.back);
        this.y = this.bscene.getH();
        this.back.setInteractive();
        this.back.on('pointerdown', this.unsetObj.bind(this));

        this.grid = new AlignGrid(this.bscene, 11, 11, this.back.displayWidth, this.back.displayHeight);

        //
        //
        //
        this.scene.add.existing(this);
        // this.grid.showPos();
        //
        //
        //
        let buttons: IconVo[] = [];
        buttons.push(new IconVo("btnCCW", "ccw", "rotLeft"));
        buttons.push(new IconVo("btnCW", "cw", "rotRight"));
        buttons.push(new IconVo("btnGrow", "plus", "grow"));
        buttons.push(new IconVo("btnShrink", "minus", "shrink"));
        buttons.push(new IconVo("btnDel", "trash", "delete"));

        this.makeButtons(buttons);

        this.gm.objectCallback = this.objChanged.bind(this);

        this.controller.openEdit = this.open.bind(this);
        this.controller.closeEdit = this.close.bind(this);

    }
    private unsetObj() {
        this.gm.currentObject = null;
    }
    private setIcon() {
        if (this.itemIcon) {
            this.itemIcon.destroy();
        }
        if (this.gm.currentObject != null) {
            this.itemIcon = this.scene.add.sprite(0, 0, this.gm.currentObject.dragKey, this.gm.currentObject.dragFrame);
            let scale: number = this.lm.getDynamicScale(this.gm.editIconScale) / 2;
            Align.scaleToGameW(this.itemIcon, scale, this.bscene);
            this.add(this.itemIcon);
            this.grid.placeAt(10, 1, this.itemIcon);
        }
    }
    private objChanged() {
        if (this.gm.currentObject === null) {
            if (this.isOpen === true) {
                this.close();
            }
        }
        else {
            this.setIcon();
            this.checkItem();
            if (this.isOpen === false) {
                this.open();
            }
        }
    }
    public open() {
        

        this.targetY = this.bscene.getH() * 0.75;
        this.doorYDir = -1;
    }
    public close() {
        this.targetY = this.bscene.getH();
        this.doorYDir = 1;
    }
    public makeButtons(buttons: IconVo[]) {
        let pos: PosVo[] = this.lm.getEditButtonPos();
        while (this.buttonArray.length > 0) {
            this.buttonArray.pop().destroy();
        }
        for (let i: number = 0; i < buttons.length; i++) {
            let editButton: EditButton = new EditButton(this.bscene, "editicons", this.lm, buttons[i]);
            editButton.callback = this.buttonPressed.bind(this);
            this.add(editButton);

            this.iconMap.set(buttons[i].name, editButton);
            this.buttonArray.push(editButton);

            let pos2: PosVo = pos[i];
            this.grid.placeAt(pos2.x, pos2.y, editButton);
        }
        //this.bringToTop(this.back);
    }
    private buttonPressed(iconVo: IconVo) {
        
        switch (iconVo.action) {
            case "grow":
                this.gm.currentObject.upScale();
                break;
            case "shrink":
                this.gm.currentObject.downScale();
                break;

            case "rotLeft":
                this.gm.currentObject.turnCounter();
                break;

            case "rotRight":
                this.gm.currentObject.turnClockwise();
                break;

            case "delete":
                this.gm.currentObject.removeMe();
                break;
        }

        this.checkItem();
        
    }
    checkItem()
    {
        this.turnAllButtonsOn();
        if (this.gm.currentObject !== null) {
            if (this.gm.currentObject.scale == 0) {
                this.turnOffButton("btnShrink");
            }
            if (this.gm.currentObject.scale == 4) {
                this.turnOffButton("btnGrow");
            }
        }
    }
    turnAllButtonsOn() {
        for (let i: number = 0; i < this.buttonArray.length; i++) {
            this.buttonArray[i].alpha = 1;
        }
    }
    turnOffButton(btnName: string) {
        let btn: EditButton = this.iconMap.get(btnName);
        btn.alpha = 0.1;
    }
    turnOnButton(btnName: string) {
        let btn: EditButton = this.iconMap.get(btnName);
        btn.alpha = 1;
    }
    public updateMe() {
        if (this.doorYDir !== 0) {
            this.y += this.doorYDir * 20;
            

            if (this.doorYDir == -1) {
                if (this.y < this.targetY) {
                    this.doorYDir = 0;
                    
                    this.isOpen = true;
                }
            }
            else {
                if (this.y > this.targetY) {
                    this.doorYDir = 0;
                    this.isOpen = false;
                }
            }
        }
        else {
            this.y = this.targetY;
        }
    }
}