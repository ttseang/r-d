import IBaseScene from "../../interfaces/IBaseScene";

export class StarterUi extends Phaser.GameObjects.Container
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());
        
        this.bscene=bscene;
    }
}