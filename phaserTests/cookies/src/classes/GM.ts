import { GameObjects } from "phaser";
import { FileVo } from "../dataObjs/fileVo";
import { LayoutVo } from "../dataObjs/layoutVo";
import { MenuVo } from "../dataObjs/menuVo";
import { Dragger } from "./Dragger";

let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;
    //
    //
    //
    public assetFolder:string="./";
    public jsonAssets:string[]=[];
    public images:FileVo[]=[];
    public audios:FileVo[]=[];
    //
    //
    //
    public layouts:Map<string,LayoutVo>=new Map();
    public baseKey:string="";
    public baseItemPrefix:string="";
    public baseItemSufix:string="";
    public baseScales:string="0.2,0.2,0.2,0.5,0.2";
    public baseBackScales:string="0.2,0.2,0.2,0.5,0.2";
    //
    //
    //
    public afterLoadScene:string="SceneMain";
    //
    //
    //
    public menus:Map<string,MenuVo>=new Map<string,MenuVo>();
    public iconScales:string=".15,.25,.08,.19,.09";
    public editIconScale:string=".1,.17,.08,.15,.09";
    private _currentObject:Dragger | null;
    public cookieIndex:number=0;
    public scratchKey:string="icing";
    
    public objectCallback:Function=()=>{};

    constructor()
    {
        window['gm']=this;
    }
    public set currentObject(obj:Dragger)
    {
        this._currentObject=obj;
        this.objectCallback(obj);
    }
    public get currentObject()
    {
        return this._currentObject;
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}