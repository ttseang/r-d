import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { CookieModel } from "./CookieModel";


export class LayoutManager {
    public scene: Phaser.Scene;
    public guideImage: GameObjects.Sprite;
    public maskImage: GameObjects.Sprite;
    private gm: CookieModel = CookieModel.getInstance();
    private bscene: IBaseScene;
    public callback: Function = () => { };

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;

        window.addEventListener("orientationchange", function () {
            //console.log(window.innerWidth, window.innerHeight)
            this.flipped();
        }.bind(this));
    }
    private flipped() {
        //console.log('flipped');
        setTimeout(() => {
            let w: number = window.innerWidth;
            let h: number = window.innerHeight;
            if (w < h) {
                this.gm.isPort = true;
            }
            else {
                this.gm.isPort = false;
            }
            this.bscene.resetSize(w, h);
            this.scene.scale.resize(w, h);
            this.bscene.getGrid().hide();
            this.bscene.makeGrid(22, 22);
            this.callback();
        }, 1000);

    }
    public doLayout() {
        //this.placeAndScale("number",this.guideImage);
        //this.placeAndScale("number",this.maskImage);
    }
    private placeAndScale(item: string, obj: GameObjects.Sprite) {
        Align.scaleToGameW(obj, this.getScale(item), this.bscene);
        this.bscene.getGrid().placeAtIndex(this.getPos(item), obj);
    }
    public getLayoutKey()
    {
        if (this.gm.isMobile === false) {
            return "desktop";
        }
        //land phone
        if (this.gm.isMobile === true && this.gm.isPort === false) {
            return "phoneland";
        }
        //port phone
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            return "phoneport";
        }
        //land tablet
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            return "tabletport";
        }
        //land tablet
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            return "tabletland";
        }
        return "desktop";
    }
    public getPos(item: string) {
        let pos: number = 0;

        switch (item) {
            case "number":
                if (this.gm.isMobile === false) {

                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {

                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {

                }
                break;


        }
        return pos;
    }
    public getScale(item: string) {
        let scale: number = 0;

        switch (item) {

            case "base":

                scale = 0.2;
                if (this.gm.isMobile === false) {

                }
                //land phone
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                //port phone
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.5;
                }
                //land tablet
                if (this.gm.isTablet === true && this.gm.isPort === false) {

                }

                break;
                
            case "menuButton":
                scale = 0.05;
                if (this.gm.isMobile === false) {

                }
                //land phone
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale=0.1;
                }
                //port phone
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.15;
                }
                //land tablet
                if (this.gm.isTablet === true && this.gm.isPort === false) {

                }

                break;


            case "baseBack":

                scale = 0.4;
                if (this.gm.isMobile === false) {

                }
                //land phone
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                //port phone
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.9;
                }
                //land tablet
                if (this.gm.isTablet === true && this.gm.isPort === false) {

                }

                break;

            case "shelfW":
                scale = 0.4;
                if (this.gm.isMobile === false) {
                    scale = 0.6;
                }
                //land phone
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                //port phone
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 1;
                }
                //land tablet
                if (this.gm.isTablet === true && this.gm.isPort === false) {

                }

                break;
            case "icons":

                scale = 0.15;
                if (this.gm.isMobile === false) {

                }
                //land phone
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                //port phone
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.25;
                }
                //land tablet
                if (this.gm.isTablet === true && this.gm.isPort === false) {

                }

                break;

            case "arrow":
                scale = 0.05;
                if (this.gm.isMobile === false) {

                }
                //land phone
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                //port phone
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.15;
                }
                //land tablet
                if (this.gm.isTablet === true && this.gm.isPort === false) {

                }
                break;

        }
        return scale;
    }
    getDynamicScale(scales: string) {
        let scaleArray: string[] = scales.split(",");
        let scale: string = "0";

        if (this.gm.isMobile === false) {
            scale = scaleArray[0];
        }
        //port phone
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            scale = scaleArray[1];
        }
        //land phone
        if (this.gm.isMobile === true && this.gm.isPort === false) {
            scale = scaleArray[2];
        }
        //port tablet
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            scale = scaleArray[3];
        }
        //land tablet
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            scale = scaleArray[4];
        }
        return parseFloat(scale);
    }
    public getTabPos() {
        let pos: PosVo[] = [new PosVo(2, 1.45), new PosVo(5, 1.45), new PosVo(8, 1.45), new PosVo(2, 4.5),new PosVo(5, 4.5), new PosVo(8, 4.5), new PosVo(2, 7.5), new PosVo(5, 7.5), new PosVo(8, 7.5)];

        if (this.gm.isMobile === false) {

        }
        //land phone
        if (this.gm.isMobile === true && this.gm.isPort === false) {

        }
        //port phone
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            pos = [new PosVo(2, 2), new PosVo(5, 2), new PosVo(8, 2), new PosVo(2, 5), new PosVo(5, 5), new PosVo(8, 5), new PosVo(2, 8), new PosVo(5, 8), new PosVo(8, 8)];
        }
        //land tablet
        if (this.gm.isTablet === true && this.gm.isPort === false) {

        }
        return pos;
    }
    public getEditButtonPos() {
        let pos: PosVo[] = [new PosVo(1, 5), new PosVo(3, 5), new PosVo(5, 5), new PosVo(7, 5),new PosVo(9, 5)];

        if (this.gm.isMobile === false) {

        }
        //land phone
        if (this.gm.isMobile === true && this.gm.isPort === false) {

        }
        //port phone
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            pos =  [new PosVo(1, 5), new PosVo(3, 5), new PosVo(5, 5), new PosVo(7, 5),new PosVo(9, 5)];
        }
        //land tablet
        if (this.gm.isTablet === true && this.gm.isPort === false) {

        }
        return pos;
    }
    public getCutterButtonPos() {
        let pos: PosVo[] = [new PosVo(1, 4), new PosVo(3, 4), new PosVo(5, 4), new PosVo(7, 4),new PosVo(9, 4)];

        if (this.gm.isMobile === false) {

        }
        //land phone
        if (this.gm.isMobile === true && this.gm.isPort === false) {

        }
        //port phone
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            pos =  [new PosVo(1, 5), new PosVo(3, 5), new PosVo(5, 5), new PosVo(7, 5),new PosVo(9, 5)];
        }
        //land tablet
        if (this.gm.isTablet === true && this.gm.isPort === false) {

        }
        return pos;
    }
}