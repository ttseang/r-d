import { CookieModel } from "../classes/CookieModel";
import { GM } from "../classes/GM";
import { FileVo } from "../dataObjs/fileVo";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene {

    private gm:CookieModel=CookieModel.getInstance();

    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {
        super.create();
        for (let i: number = 0; i < this.gm.jsonAssets.length; i++) {
            let key: string = this.gm.jsonAssets[i];
            let path:string=this.gm.assetFolder + key + ".png";
            let jpath:string=this.gm.assetFolder+key+".json";

          

            this.load.atlas(key,path , jpath);
        }

        for (let i:number=0;i<this.gm.images.length;i++)
        {
            let image:FileVo=this.gm.images[i];
            this.load.image(image.key,this.gm.assetFolder+image.file);
        }
        for (let i:number=0;i<this.gm.audios.length;i++)
        {
            let audio:FileVo=this.gm.audios[i];
            let path:string=this.gm.assetFolder+audio.file;
            console.log(path);

            this.load.audio(audio.key,path);
        }

        /* this.load.image("holder", "./assets/holder.jpg");
        this.load.image("circle", "./assets/circle.png");
        this.load.image('background', "./assets/tabletop.jpg");
        this.load.image("shelf", "./assets/shelf.png");
        this.load.image("arrow", "./assets/arrow.png");
        this.load.image("dough", "./assets/dough.png");
        this.load.image("oven","./assets/oven.png");
        this.load.audio("ovenSound","./assets/microwave.wav");
        this.load.image("saveButton","./assets/disk.png"); */
    }
    create()
    {
       this.scene.start(this.gm.afterLoadScene);
    }
}