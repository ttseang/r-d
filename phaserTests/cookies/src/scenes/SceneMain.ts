import { GameObjects } from "phaser";
import { Controller } from "../classes/Controller";
import { CookieModel } from "../classes/CookieModel";
import { LayoutBuilder } from "../classes/layoutBuilder";
import { LayoutManager } from "../classes/layoutManager";
import { ScratchCard } from "../classes/ScratchCard";
import { Table } from "../classes/table";
import { ItemPanel } from "../classes/ui/itemPanel";
import { TabLine } from "../classes/ui/tabLine";
import { IconVo } from "../dataObjs/IconVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private gm: CookieModel = CookieModel.getInstance();
    private layoutManager: LayoutManager;
    private tabLine: TabLine;
    private itemPanel: ItemPanel;
    private btnSave: GameObjects.Image;
    private table: Table;
    private controller:Controller=Controller.getInstance();

    constructor() {
        super("SceneMain");
    }
    preload() {
       
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        let bg: GameObjects.Image = this.add.image(0, 0, "background");
        Align.scaleToGameW(bg, 1, this);

        bg.displayHeight = this.gh;
        bg.displayWidth = this.gw;
        Align.center(bg, this);
        window['scene'] = this;


       
        this.layoutManager = new LayoutManager(this);
        let cookie: string = this.gm.baseItemPrefix + this.gm.cookieIndex.toString() + this.gm.baseItemSufix;
        this.table = new Table(this, this.layoutManager, this.gm.baseKey, cookie);
        


        this.tabLine = new TabLine(this, this.layoutManager);
        this.tabLine.makeTabs(this.gm.menus.get("main").items);

        this.itemPanel = new ItemPanel(this, this.layoutManager);


        this.controller.saveImage=this.saveImage.bind(this);

        let layoutBuilder:LayoutBuilder=new LayoutBuilder(this,this.layoutManager);
        layoutBuilder.build("main");
    }
    exportCanvasAsPNG(fileName: string, dataUrl: string) {
        // var canvasElement = document.getElementById(id);
        var MIME_TYPE = "image/png";
        var imgURL = dataUrl;
        var dlLink = document.createElement('a');
        dlLink.download = fileName;
        dlLink.href = imgURL;
        dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');
        document.body.appendChild(dlLink);
        dlLink.click();
        document.body.removeChild(dlLink);
    }
    saveImage() {
        let xx:number=this.table.baseBack.x-this.table.baseBack.displayWidth/2;
        let yy:number=this.table.baseBack.y-this.table.baseBack.displayHeight/2;
        let ww:number=this.table.baseBack.displayWidth;
        let hh:number=this.table.baseBack.displayHeight;
        console.log(xx,yy,ww,hh);

        this.game.renderer.snapshotArea(xx,yy, ww, hh, (image) => {
            let image2: HTMLImageElement = image as HTMLImageElement;
            // image2.width = 1600;
            // image2.height = 1600;
            let fileName: string = "my_cookie";
            
            this.exportCanvasAsPNG(fileName, image2.src);
         });

      /*   this.game.renderer.snapshot((image) => {
            let image2: HTMLImageElement = image as HTMLImageElement;
            // image2.width = 1600;
            // image2.height = 1600;
            let fileName: string = "my_cookie";
            this.exportCanvasAsPNG(fileName, image2.src);
        }); */
    }
    update() {
        this.tabLine.updateMe();
        this.itemPanel.updateMe();
    }
}