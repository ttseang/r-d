import { IconVo } from "./IconVo";

export class MenuVo
{
    public name:string;
    public items:IconVo[]=[];

    constructor(name:string)
    {
        this.name=name;
    }
}