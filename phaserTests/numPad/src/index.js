import Phaser from 'phaser';
import {SceneMain} from "./scenes/SceneMain";

let isMobile=false;
const userAgent=navigator.userAgent;

if (userAgent.indexOf("Mobile")>-1)
{
    isMobile=true;
}
if (userAgent.indexOf("Tablet")>-1)
{
    isMobile=true;
}
console.log(isMobile);

let w=480;
let h=640;

if (isMobile==true)
{
    w=window.innerWidth;
    h=window.innerHeight;
}


const config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: w,
    height: h,
    scene: SceneMain
};

const game = new Phaser.Game(config);
