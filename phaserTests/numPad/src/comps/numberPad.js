import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";
import { BaseUI } from "./baseUI";
import { FlatButton } from "../ui/flatButton";

export class NumberPad extends BaseUI {
    constructor(config) {
        super(config);
        this.scene = config.scene;
        this.config=config;

        this.backScale=config.scale;

        this.setBack(config.back, config.scale);
        this.back.displayHeight = this.back.displayWidth * 1.5;
       
        this.back.setTint(config.tint);
        this.setAlignGrid(9, 7);
        
    }
    makeButtons() {
        const buttonPos = [52,8, 10, 12, 22, 24, 26,36,38,40];

        for (let i = 0; i < 10; i++) {
            let numButton = new FlatButton({ scene: this.scene,callback:this.config.callback, text: i, key: 'holder', scale: this.backScale*0.25, textStyle: { color: '#000000', fontSize: 20 } });
            numButton.bg.setTint(this.config.buttonTint);
            this.aGrid.placeAtIndex(buttonPos[i], numButton);
        }
    }
}