import Align from "../util/align";
import {UIBlock} from "../util/UIBlock";

export class FlatButton extends UIBlock
{
    constructor(config)
    {
        super();
        this.scene=config.scene;
        this.bg=this.scene.add.image(0,0,config.key);
        this.text=config.text;
        const textObj=this.scene.add.text(0,0,config.text,config.textStyle);
        textObj.setOrigin(0.5,0.5);

        this.add(this.bg);
        this.add(textObj);
        Align.scaleToGameW(this.bg,config.scale,this.scene);

        if (config.callback)
        {
           this.bg.setInteractive();
           this.bg.on('pointerdown',()=>{config.callback(this);});
        }
    }
}