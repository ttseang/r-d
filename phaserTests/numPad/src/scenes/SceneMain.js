/*jshint esversion: 6 */
import { NumberPad } from "../comps/numberPad";
import Align from "../util/align";


export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "../../assets/images/ui/backs/holder.jpg");

    }
    create() {

        //create the number pad
        let numPad = new NumberPad({ scene: this, back: "holder", scale: 0.35, tint: 0xbdc3c7, buttonTint: 0x7f8c8d, callback: this.enterNumber.bind(this) });
        //postion
        Align.center(numPad, this);

        //fix the postion of the alignGrid
        //must be done after placing in position
        //or making children
        numPad.fixGrid();

        //for aligning and debug
        //  numPad.showGridNumbers();

        //make the buttons or other children
        numPad.makeButtons();
    }
    enterNumber(button) {
        console.log(button.text);
    }
    update() {

    }
}
export default SceneMain;