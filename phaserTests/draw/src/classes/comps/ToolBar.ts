import { IconButtonVo } from "../../dataObjs/IconButtonVo";
import IBaseScene from "../../interfaces/IBaseScene";
import { IconButton } from "./IconButton";

export class ToolBar extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private buttons:IconButton[]=[];
    public callback:Function=()=>{};

    

    constructor(bscene:IBaseScene,buttonData:IconButtonVo[])
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.scene=bscene.getScene();

        let colors:number[]=[0xc0392b,0x3498db,0xe67e22,0xf1c40f,0x8e44ad,0x2980b9,0xbdc3c7];
        
        let bh:number=0;
        let bw:number=0;
        let xx:number=0;
        let yy:number=0;

        let tw:number=0;

        let size:number=buttonData.length;
        if (size>4)
        {
            size=4;
        }
        size=this.bscene.getW()/size;

        for (let i:number=0;i<buttonData.length;i++)
        {
            let button:IconButton=new IconButton(this.bscene,buttonData[i],size);
           
            
            bh=button.displayHeight;
            bw=button.displayWidth;

            button.x=xx+bw/2;
            button.y=yy+bh/2;

            xx+=bw;
            if (xx>tw)
            {
                tw=xx;
            }
            if (xx+bw>this.bscene.getW())
            {
                xx=0;
                yy+=bh;
            }


            button.setCallback(this.doAction.bind(this))
            this.add(button);
        }

        this.setSize(tw,yy);

        this.scene.add.existing(this);
    }
    doAction(buttonData:IconButtonVo)
    {
        console.log(buttonData.action);
        this.callback(buttonData.action);
    }
}