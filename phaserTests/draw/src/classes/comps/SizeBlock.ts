import { GameObjects } from "phaser";
import IBaseScene from "../../interfaces/IBaseScene";
import Align from "../../util/align";

export class SizeBlock extends GameObjects.Container
{
    
    private callback:Function;
    private back:GameObjects.Image;
    private bscene:IBaseScene;
    private mySize:number;

    constructor(bscene:IBaseScene,myColor:number,size,callback:Function)
    {
        super(bscene.getScene());

        this.bscene=bscene;
        this.mySize=size;
        this.callback=callback;

        this.back=this.scene.add.image(0,0,"holder");
        this.back.setTint(myColor);

        this.back.displayWidth = this.bscene.getW() / 5;
        this.back.displayHeight = this.bscene.getH() / 4;
        this.add(this.back);
       

        let circle:GameObjects.Image=this.scene.add.image(0,0,"circle");
        Align.scaleToGameW(circle,size/150,bscene);
        this.add(circle);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.setInteractive();
    

        this.on('pointerdown',this.pressMe.bind(this));

        bscene.getScene().add.existing(this);
    }
    pressMe()
    {
        console.log(this.mySize);
        this.callback(this.mySize);
    }
}