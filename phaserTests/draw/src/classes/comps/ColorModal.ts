import { GameObjects } from "phaser";
import IBaseScene from "../../interfaces/IBaseScene";
import { ColorBlock } from "./ColorBlock";

export class ColorModal extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
   
    private colors:number[]=[];

    constructor(bscene:IBaseScene,callback:Function)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.bscene=bscene;

        this.colors=[0x1abc9c,0x2ecc71,0x3498db,0x9b59b6,0x34495e,0x16a085,0x27ae60,0x2980b9,0x8e44ad,0x2c3e50,0xf1c40f,0xe67e22,0xe74c3c,0xecf0f1,0x95a5a6,0xf39c12,0xd35400,0xc0392b,0xbdc3c7,0x7f8c8d]

        let k:number=0;
        for (let i:number=0;i<4;i++)
        {
            for (let j:number=0;j<5;j++)
            {
                let colorBlock:ColorBlock=new ColorBlock(this.bscene,this.colors[k],callback)

                colorBlock.displayWidth=this.bscene.getW()/5;
                colorBlock.displayHeight=this.bscene.getH()/4;

                //colorBlock.setTint(this.colors[k]);
            

                colorBlock.x=j*colorBlock.displayWidth+colorBlock.displayWidth/2;
                colorBlock.y=i*colorBlock.displayHeight+colorBlock.displayHeight/2;

                k++;

                this.add(colorBlock);
            }
        }
        this.scene.add.existing(this);
    }
}