import { GameObjects } from "phaser";
import { IconButtonVo } from "../../dataObjs/IconButtonVo";
import IBaseScene from "../../interfaces/IBaseScene";
import { IGameObj } from "../../interfaces/IGameObj";
import Align from "../../util/align";
import UIBlock from "../../util/UIBlock";

export class IconButton extends GameObjects.Container implements IGameObj {
    public text1: GameObjects.Text;
    private bscene: IBaseScene;
    private back: GameObjects.Image;

    private callback: Function = () => { };

    public type:string="button";
    public scene: Phaser.Scene;
    private buttonData:IconButtonVo;
    private icon:GameObjects.Sprite;

    constructor(bscene: IBaseScene,buttonData:IconButtonVo,size:number) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.buttonData=buttonData;
        //
        //
        //
        this.back = this.scene.add.image(0, 0, "holder");
        this.text1 = this.scene.add.text(0, 0, buttonData.text, { "color":"white", "fontSize": "13px" }).setOrigin(0.5, 0.5);
       // Align.scaleToGameW(this.back,0.2,this.bscene);

       
         this.back.displayWidth=size;
        this.back.displayHeight=this.back.displayWidth/2;
        this.back.setTint(buttonData.color);

        this.text1.y=this.back.y+this.back.displayHeight/2-this.text1.displayHeight/2;
        this.icon=this.scene.add.sprite(0,0,"editIcons");
        this.icon.setFrame(buttonData.icon+".png");
        this.icon.setTint(0x00000);
        Align.scaleToGameW(this.icon,0.08,this.bscene);
        

        //
        //
        //
        this.back.setScrollFactor(0, 0);
        this.text1.setScrollFactor(0, 0);
        //
        //
        //
        this.add(this.back);
        this.add(this.text1);
        this.add(this.icon);


        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.scene.add.existing(this);
        //
        //
        //

    }
    scaleX: number;
    scaleY: number;
    /* setDepth(d:number)
    {
        this.back.setDepth(d+1);
        this.text1.setDepth(d+2);
        
    } */
    setCallback(cb: Function) {

        this.callback = cb;
        this.back.setInteractive();
        this.back.on('pointerdown', () => {
            this.callback(this.buttonData);
        })
    }
    update()
    {
        //console.log("update button");
        Align.scaleToGameW(this.back,0.3,this.bscene);
    }
}