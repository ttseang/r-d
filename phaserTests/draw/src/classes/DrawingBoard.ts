import { HistoryObj } from "../dataObjs/HistoryObj";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";

export class DrawingBoard extends Phaser.GameObjects.Graphics
{
    public mode:string="dots";

    public color:number=0xff0000;
    public thick:number=10;
    private bscene:IBaseScene;
    public scene:Phaser.Scene;

    public static MODE_DOTS:string="dots";
    public static MODE_LINE:string="line";
    
    public history:HistoryObj[]=[];
    public forwardHistory:HistoryObj[]=[];

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());
        this.scene.add.existing(this);
        this.lineStyle(2,0xff0000);
        this.fillStyle(0xff0000,1);
    }
    undo()
    {
        if (this.history.length==0)
        {
            return;
        }
        let h:HistoryObj=this.history.pop();
      //  console.log(h);
        this.forwardHistory.push(h);
        this.drawHistory();
    }
    redo()
    {
        if (this.forwardHistory.length==0)
        {
            return;
        }
        let h:HistoryObj=this.forwardHistory.pop();
        this.history.push(h);
        this.drawHistory();
    }
    drawHistory()
    {
        this.clear();
        for (let i:number=0;i<this.history.length;i++)
        {
            let h:HistoryObj=this.history[i];

            console.log(h);

           
           
            switch(h.type)
            {
                case DrawingBoard.MODE_DOTS:
                    this.fillStyle(h.color);

                    this.thick=h.thick;
                    this.drawCircles(h.pos);
                    break;
                case DrawingBoard.MODE_LINE:

                    this.lineStyle(h.thick,h.color);
                    console.log("draw history line");
                    if (h.pos.length>0)
                    {
                        this.drawLine(h.pos[0],h.pos[1]);
                    }                  
                    break;
            }
           
        }
    }
    drawLine(start:PosVo,end:PosVo)
    {
        this.moveTo(start.x,start.y);
        this.lineTo(end.x,end.y);
        this.closePath();
        this.strokePath();

    }
    drawCircles(p:PosVo[])
    {
        for (let i:number=0;i<p.length;i++)
        {
            this.makeCircle(p[i].x,p[i].y,this.thick);
        }
    }
    makeCircle(xx:number,yy:number,d:number)
    {        
        this.fillCircle(xx,yy,d);
        this.fillPath();
    }
    setColor(color:number)
    {
        this.color=color;
        //this.lineStyle(this.thick,color);
        this.fillStyle(color,1);
    }
    setThick(thick:number)
    {
        this.thick=thick;
        this.lineStyle(thick,this.color);
    }
}