import { Actions, GameObjects } from "phaser";
import { ColorModal } from "../classes/comps/ColorModal";
import { SizeModal } from "../classes/comps/SizeModal";
import { ToolBar } from "../classes/comps/ToolBar";
import { DrawingBoard } from "../classes/DrawingBoard";
import { HistoryObj } from "../dataObjs/HistoryObj";
import { IconButtonVo } from "../dataObjs/IconButtonVo";
import { PosVo } from "../dataObjs/PosVo";
import { ToolActions } from "../dataObjs/ToolActions";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    public board: DrawingBoard;
    public dragLine: GameObjects.Graphics;

    public dragIcon:GameObjects.Image;

    private lastColor:number=0xff0000;

    private currentHistory: HistoryObj;
    private startPos: PosVo;
    private endPos: PosVo;
    private toolbar: ToolBar;

    public modalOpen: boolean = false;
    public actionFlag: boolean = false;

    public colorModal: ColorModal;
    public sizeModal:SizeModal;

    private clickLock: boolean = false;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("circle", "./assets/circle.png");
      
        this.load.atlas("editIcons","./assets/editIcons.png","./assets/editIcons.json");

    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        let bg: GameObjects.Image = this.add.image(0, 0, "holder").setOrigin(0, 0);
        bg.displayHeight = this.gh;
        bg.displayWidth = this.gw;
        bg.setTint(0xffffff);

        window['scene'] = this;

        this.board = new DrawingBoard(this);
        this.board.mode = DrawingBoard.MODE_DOTS;

        this.dragLine = this.add.graphics();
        this.dragIcon=this.add.sprite(0,0,"editIcons");
        this.dragIcon.setOrigin(0.5,1);
        Align.scaleToGameW(this.dragIcon,0.05,this);

        this.dragIcon.setFrame("pen.png");

        this.makeBottomToolBar();
        // this.makeTopToolBar();

        this.colorModal = new ColorModal(this, this.setColor.bind(this));
        this.colorModal.visible = false;

        this.sizeModal=new SizeModal(this,this.setSize.bind(this));
        this.sizeModal.visible=false;

        this.input.once("pointerdown", this.doDraw.bind(this));
    }

    makeBottomToolBar() {
        let buttonData: IconButtonVo[] = [];
        buttonData.push(new IconButtonVo(0xc0392b, "pen", "pen", ToolActions.PEN));
        //buttonData.push(new IconButtonVo(0x3498db, "lines", "Line", ToolActions.LINES));
        buttonData.push(new IconButtonVo(0x3498db, "eraser", "erase", ToolActions.ERASE));

        buttonData.push(new IconButtonVo(0xe67e22, "paint", "color", ToolActions.CHANGE_COLOR));
        buttonData.push(new IconButtonVo(0xf9ca24, "sizes", "size", ToolActions.CHANGE_SIZE));
        buttonData.push(new IconButtonVo(0xf1c40f, "undo", "undo", ToolActions.UNDO));
        buttonData.push(new IconButtonVo(0xf39c12, "redo", "redo", ToolActions.REDO));

       buttonData.push(new IconButtonVo(0x8e44ad,"clear","clear",ToolActions.CLEAR));
        buttonData.push(new IconButtonVo(0x86de0, "save", "save", ToolActions.SAVE));

        this.toolbar = new ToolBar(this, buttonData);
        this.toolbar.callback = this.doAction.bind(this);
        this.grid.placeAt(0.5, 9, this.toolbar);

        Align.center2(this.toolbar, this);
        this.toolbar.y = this.gh - this.toolbar.displayHeight;

    }
    doAction(action: number) {
        console.log("do action");
        this.actionFlag = true;
        switch (action) {
            case ToolActions.PEN:

                this.board.setColor(this.lastColor);
                this.board.mode = "dots";
                this.dragIcon.setFrame("pen.png");
                break;

            case ToolActions.LINES:
                this.board.mode = "line";
                break;
            case ToolActions.ERASE:

                this.lastColor=this.board.color;
                this.board.setColor(0xffffff);
                this.dragIcon.setFrame("eraser.png");
                break;

            case ToolActions.UNDO:
                this.board.undo();
                break;

            case ToolActions.REDO:

                this.board.redo();
                break;

            case ToolActions.CLEAR:

                this.board.forwardHistory = [];
                this.board.history = [];
                this.board.clear();

                break;

            case ToolActions.CHANGE_COLOR:
                this.input.off("pointerup");
                this.input.off("pointermove");
                this.input.off("pointerdown");

                this.modalOpen=true;

                this.colorModal.visible = true;
                break;

            case ToolActions.CHANGE_SIZE:
                this.input.off("pointerup");
                this.input.off("pointermove");
                this.input.off("pointerdown");
                this.modalOpen=true;

                this.sizeModal.visible = true;
                break;

        }
    }
    setColor(color: number) {
        this.dragIcon.setFrame("pen.png");
        console.log(color);
        this.board.setColor(color);
        this.lastColor=color;
        this.colorModal.visible = false;
        this.onModalClose();
    }
    setSize(size:number)
    {
        console.log("SIZE="+size);
        this.dragIcon.setFrame("pen.png");
        this.board.setThick(size);
        this.sizeModal.visible=false;
        this.onModalClose();
    }
    onModalClose() {
        console.log("modal close");

        this.clickLock = true;
        setTimeout(() => {
            this.clickLock = false;
            this.input.once("pointerdown", this.doDraw.bind(this));
            this.actionFlag=false;

            console.log("ready");
        }, 250);
        this.modalOpen = false;

    }
    doDraw(p: Phaser.Input.Pointer) {
        if (this.actionFlag == true) {
            this.actionFlag = false;
            console.log("action flag");
            this.input.once("pointerdown", this.doDraw.bind(this));
            return;
        }


        if (p.y > this.toolbar.y) {
            this.input.once("pointerdown", this.doDraw.bind(this));
            console.log("toolbar");
            return;
        }
        if (this.modalOpen == true) {
            console.log("modal open");
            return;
        }
        console.log("do draw");
        this.currentHistory = new HistoryObj(this.board.mode, this.board.color, this.board.thick);

        if (this.board.mode === DrawingBoard.MODE_DOTS) {
            this.currentHistory.pos.push(new PosVo(p.x, p.y));
            this.board.makeCircle(p.x, p.y, this.board.thick);

            this.input.once("pointerup", this.onUp.bind(this));
            this.input.on("pointermove", this.doMove.bind(this));
        }
        if (this.board.mode === DrawingBoard.MODE_LINE) {
            this.currentHistory.pos.push(new PosVo(p.x, p.y));

            this.startPos = new PosVo(p.x, p.y);
            this.input.once("pointerup", this.onUp.bind(this));
            this.input.on("pointermove", this.doMove.bind(this));
        }
        this.dragIcon.x=p.x;
        this.dragIcon.y=p.y;
    }
    doMove(p: Phaser.Input.Pointer) {
        if (this.modalOpen == true) {
            this.input.off("pointermove");
            return;
        }
        if (p.y > this.gh * 0.8) {
            return;
        }
        if (this.board.mode === DrawingBoard.MODE_DOTS) {
            this.board.makeCircle(p.x, p.y, this.board.thick);
            this.currentHistory.pos.push(new PosVo(p.x, p.y));
        }
        if (this.board.mode == DrawingBoard.MODE_LINE) {
            this.endPos = new PosVo(p.x, p.y);
            this.dragLine.clear();
            this.dragLine.lineStyle(this.board.thick, this.board.color);
            this.dragLine.moveTo(this.startPos.x, this.startPos.y);
            this.dragLine.lineTo(this.endPos.x, this.endPos.y);
            this.dragLine.closePath();
            this.dragLine.strokePath();
        }

        this.dragIcon.x=p.x;
        this.dragIcon.y=p.y;

    }
    onUp(p: Phaser.Input.Pointer) {
        if (this.modalOpen == true) {
            this.input.off("pointerup");
            return;
        }
        if (this.clickLock == true) {

            return;
        }
        console.log("action flag " + this.actionFlag);

        console.log("on up");

        if (this.board.mode == DrawingBoard.MODE_LINE) {
            this.endPos = new PosVo(p.x, p.y);
            this.currentHistory.pos.push(this.endPos);

            this.board.lineStyle(this.board.thick, this.board.color);
            this.board.drawLine(this.startPos, this.endPos);
            this.dragLine.clear();
        }
        this.board.history.push(this.currentHistory);
        this.board.forwardHistory = [];

        this.input.once("pointerdown", this.doDraw.bind(this));
        this.input.off("pointermove");
    }
}