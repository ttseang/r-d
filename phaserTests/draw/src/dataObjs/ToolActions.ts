export class ToolActions
{
    public static PEN:number=0;
    public static LINES:number=1;
    public static ERASE:number=8;
    public static CHANGE_COLOR:number=2;
    public static CHANGE_SIZE:number=3;
    public static UNDO:number=4;
    public static REDO:number=5;
    public static CLEAR:number=6;
    public static SAVE:number=7;
}