import { PosVo } from "./PosVo";

export class HistoryObj
{
    public pos:PosVo[];
    public type:string;
    public color:number;
    public thick:number;

    constructor(type:string,color:number,thick:number)
    {
        this.type=type;
        this.color=color;
        this.thick=thick;
        this.pos=[];
    }
}