export class IconButtonVo
{
    public color:number;
    public icon:string;
    public text:string;
    public action:number;



    constructor(color:number,icon:string,text:string,action:number)
    {
        this.color=color;
        this.icon=icon;
        this.text=text;
        this.action=action;
    }
}