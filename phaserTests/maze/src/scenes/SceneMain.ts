import { Physics } from "phaser";
import { Joystick } from "../classes/Joystick";
import { Maze } from "../classes/Maze";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private maze: Maze;
    private walls: Physics.Arcade.Group;
    private char: Physics.Arcade.Sprite;
    private door:Physics.Arcade.Sprite;

    private keys: Phaser.Types.Input.Keyboard.CursorKeys;
    private speed: number = 100;
    private js: Joystick;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("face", "./assets/face.png");
        this.load.image("circle", "./assets/circle.png");
        this.load.image("door","./assets/door.png");
        this.load.image("button","./assets/ui/buttons/1/1.png");
    }
    create() {
        super.create();
        this.makeGrid(24, 30);

        this.js = new Joystick(this, this.moveChar.bind(this));
        //    Align.center(this.js,this);
        this.grid.placeAt(14, 21, this.js);
        //  this.grid.showPos();

        this.walls = this.physics.add.group();

        this.maze = new Maze(this, 20, 20);

        window['maze'] = this.maze;
        window['scene'] = this;

        this.maze.drawMaze(this.walls);

        this.char = this.physics.add.sprite(0, 0, "face");
      //  Align.scaleToGameW(this.char, 0.015, this);
        this.char.displayHeight=this.ch/2;
        this.char.scaleX=this.char.scaleY;
        this.grid.placeAt(6, 1.5, this.char);



        this.door=this.physics.add.sprite(0,0,"door");
        this.door.setImmovable();
        Align.scaleToGameW(this.door,0.025,this);
        this.grid.placeAt(22,17.5,this.door);

        this.keys = this.input.keyboard.createCursorKeys();

        this.physics.add.collider(this.char, this.walls);
        this.physics.add.collider(this.char,this.door,null,this.gotDoor.bind(this));
    }
    gotDoor()
    {
        this.door.visible=false;
        console.log("win");
        this.scene.start("ScenePlayAgain");
    }
    moveChar(xDir: number, yDir: number) {

        if (xDir == 0 && yDir == 0) {
            this.char.setVelocity(0, 0);

            return;
        }
        if (Math.abs(xDir) > Math.abs(yDir)) {
            xDir = (xDir > 1) ? 1 : -1;
            yDir = 0;
        }
        else {
            yDir = (yDir > 1) ? 1 : -1;
            xDir = 0;
        }

        console.log(xDir, yDir);
        this.char.setVelocity(xDir * this.speed, yDir * this.speed);
    }
    update() {

           
           this.char.setVelocity(0, 0);
           this.js.updateJoystick();

        if (this.keys.down.isDown == true) {
            this.char.setVelocity(0, this.speed)
            return;
        }

        if (this.keys.up.isDown == true) {
            this.char.setVelocity(0, -this.speed);
            return;
        }

        if (this.keys.left.isDown == true) {
            this.char.setVelocity(-this.speed, 0);
            return;
        }

        if (this.keys.right.isDown == true) {
            this.char.setVelocity(this.speed, 0);
        }
    }


}