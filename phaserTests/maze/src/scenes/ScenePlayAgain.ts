import { GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import { Align } from "../util/align";
import { BaseScene } from "./BaseScene";

export class ScenePlayAgain extends BaseScene {
    private btnPlayAgain: Button;

    constructor() {
        super("ScenePlayAgain");
    }
    create() {
        super.create();

        /* let bg: GameObjects.Image = this.add.image(0, 0, "bg");

        bg.displayWidth = this.gw;
        bg.scaleY = bg.scaleX;

        if (bg.displayHeight < this.gh) {
            bg.displayHeight = this.gh;
            bg.scaleX = bg.scaleY;
        }

        Align.center(bg, this); */


        this.btnPlayAgain = new Button(this, "button", "Play Again");
        Align.center(this.btnPlayAgain, this);
        this.btnPlayAgain.setCallback(this.playAgain.bind(this));


    }
    playAgain() {
        this.scene.start("SceneMain");
    }
}