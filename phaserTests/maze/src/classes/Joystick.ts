import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class Joystick extends GameObjects.Container {
    private knob: GameObjects.Image;
    private bscene: IBaseScene;

    private callback:Function=()=>{};
    private isDown:boolean=false;

    private dirX:number=0;
    private dirY:number=0;

    constructor(bscene: IBaseScene,callback:Function) {
        super(bscene.getScene());
        this.bscene = bscene;

        this.callback=callback;

        let base:GameObjects.Image=this.scene.add.image(0,0,"circle");

        base.setTint(0x00000);
        Align.scaleToGameW(base,0.05,bscene);
        this.add(base);

        this.knob = this.scene.add.image(0, 0, "circle");
        this.knob.setTint(0xff0000);
        this.knob.setInteractive();
        Align.scaleToGameW(this.knob, 0.03, bscene);
        this.add(this.knob);

        this.knob.on("pointerdown", this.knobDown.bind(this));

        this.scene.add.existing(this);
    }
    knobDown() {
        this.scene.input.on("pointermove", this.knobMove.bind(this));
        this.scene.input.once("pointerup", this.knobUp.bind(this));
        this.isDown=true;
    }
    knobMove(p: Phaser.Input.Pointer) {
        this.knob.x = p.x - this.x;
        this.knob.y = p.y - this.y;

        let modY: number = 0.02;
        let modX: number = 0.025;

        if (this.knob.x < -this.bscene.getW() * modX) {
            this.knob.x = -this.bscene.getW() * modX;
        }

        if (this.knob.x > this.bscene.getW() * modX) {
            this.knob.x = this.bscene.getW() * modX;
        }

        if (this.knob.y < -this.bscene.getW() * modY) {
            this.knob.y = -this.bscene.getW() * modY;
        }

        if (this.knob.y > this.bscene.getW() * modY) {
            this.knob.y = this.bscene.getW() * modY;
        }
        
        this.dirX=Math.floor(this.knob.x*100)/100;
        this.dirY=Math.floor(this.knob.y*100)/100;

      //  this.callback(dirX,dirY);        

        //console.log(dirX,dirY);
    }
    knobUp() {
        this.scene.input.off("pointermove");
        this.knob.x=0;
        this.knob.y=0;
        this.callback(0,0);
        this.isDown=false;
    }
    updateJoystick()
    {
        if (this.isDown==true)
        {
            this.callback(this.dirX,this.dirY);
        }
    }
}