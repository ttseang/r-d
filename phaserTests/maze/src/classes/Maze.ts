//import { MazeCell } from "../dataObjs/MazeCell";

import { GameObjects, Physics } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "../scenes/BaseScene";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";

export class Maze {
    private width: number;
    private height: number;

    private cells: number[][] = [];

    private posX: number = 1;
    private posY: number = 1;

    private moves: number[] = [];
    private bscene: IBaseScene;
    private scene: Phaser.Scene;

    constructor(bscene: IBaseScene, width: number, height: number) {
        this.width = width;
        this.height = height;

        this.bscene = bscene;
        this.scene = bscene.getScene();

        for (var i = 0; i < width+2; i++) {
            this.cells[i] = [];
            for (var j = 0; j < height+2; j++) {
                this.cells[i][j] = 1;
            }
        }
        this.buildMaze();
    }
    buildMaze() {
        let posX: number = 1;
        let posY: number = 1;

        this.cells[posX][this.posY] = 0;
        //
        //
        //make end?
        this.moves.push(posY + posY * this.width);
        while (this.moves.length > 0) {
            this.buildNext();
        }
        console.log("built");
    }
    buildNext() {

        var dirs = "";
        if (this.posX + 2 > 0 && this.posX + 2 < this.height - 1 && this.cells[this.posX + 2][this.posY] == 1) {
            dirs += "S";
        }
        if (this.posX - 2 > 0 && this.posX - 2 < this.height - 1 && this.cells[this.posX - 2][this.posY] == 1) {
            dirs += "N";
        }
        if (this.posY - 2 > 0 && this.posY - 2 < this.width - 1 && this.cells[this.posX][this.posY - 2] == 1) {
            dirs += "W";
        }
        if (this.posY + 2 > 0 && this.posY + 2 < this.width - 1 && this.cells[this.posX][this.posY + 2] == 1) {
            dirs += "E";
        }
        if (dirs) {
            var move = Math.floor(Math.random() * dirs.length);

            switch (dirs[move]) {
                case "N":
                    this.cells[this.posX - 2][this.posY] = 0;
                    this.cells[this.posX - 1][this.posY] = 0;
                    this.posX -= 2;
                    break;
                case "S":
                    this.cells[this.posX + 2][this.posY] = 0;
                    this.cells[this.posX + 1][this.posY] = 0;
                    this.posX += 2;
                    break;
                case "W":
                    this.cells[this.posX][this.posY - 2] = 0;
                    this.cells[this.posX][this.posY - 1] = 0;
                    this.posY -= 2;
                    break;
                case "E":
                    this.cells[this.posX][this.posY + 2] = 0;
                    this.cells[this.posX][this.posY + 1] = 0;
                    this.posY += 2;
                    break;
            }
            this.moves.push(this.posY + this.posX * this.width);
        }
        else {
            let back: number = this.moves.pop();
            this.posX = Math.floor(back / this.width);
            this.posY = back % this.width;
        }
    }
    drawMaze(g:Physics.Arcade.Group) {
        let grid: AlignGrid = this.bscene.getGrid();

        for (let i: number = 0; i < this.width-1; i++) {
            for (let j: number = 0; j < this.height-1; j++) {
                let cell: number = this.cells[i][j];

                if (cell == 1) {
                    let wall: Physics.Arcade.Sprite = this.scene.physics.add.sprite(0, 0, "holder");
                    wall.displayHeight=grid.ch;
                    wall.displayWidth=grid.cw;
                   
                    grid.placeAt(i+5,j+0.5,wall);
                    g.add(wall);
                    wall.setTint(0x000000);
                    wall.setImmovable();
                }
            }
        }
    }
}