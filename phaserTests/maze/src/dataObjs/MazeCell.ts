export class MazeCell
{
    public x:number=0;
    public y:number=0;
    public visited:boolean=false;

    constructor(x:number,y:number)
    {
        this.x=x;
        this.y=y;
    }
}