import { HighlightVo } from "../dataObjs/HighlightVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseTextEffect } from "./BaseTextEffect";

export class FlashingText extends BaseTextEffect {
    public scene: Phaser.Scene;   

    private dataProvider: HighlightVo;
    private letters1:Phaser.GameObjects.Text[]=[];
    private letters2:Phaser.GameObjects.Text[]=[];

    private isOn:boolean=false;
   

    constructor(bscene: IBaseScene, dataProvider: HighlightVo) {    

        super(bscene);
       

        this.dataProvider = dataProvider;
     //
        this.t2.color=dataProvider.color;
       /*  if (dataProvider.italics==true)
        {
            this.t2.fontFamily="BarthowheelIt";
        }     */ 

        this.letters2=this.makeLetters(this.dataProvider.text,false);
        this.letters1= this.makeHLetters(this.dataProvider.text,this.dataProvider.start,this.dataProvider.end);
        this.hideLetters(this.letters1);

    }
    hideLetters(letters:Phaser.GameObjects.Text[])
    {
        letters.forEach((letter:Phaser.GameObjects.Text)=>{
            letter.visible=false;
        })
    }
    showLetters(letters:Phaser.GameObjects.Text[])
    {
        letters.forEach((letter:Phaser.GameObjects.Text)=>{
            letter.visible=true;
        })
    }
    private flip()
    {
        this.isOn=!this.isOn;
        if (this.isOn==true)
        {
            this.showLetters(this.letters1);
            this.hideLetters(this.letters2);
        }
        else
        {
            this.showLetters(this.letters2);
            this.hideLetters(this.letters1);
        }
    }
    public start()
    {
        this.timer1=this.scene.time.addEvent({ delay: 500, callback: this.flip.bind(this), loop: true });
       
    }
    public removeMe()
    {
        this.timer1.remove();
    }
}