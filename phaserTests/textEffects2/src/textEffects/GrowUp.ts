import { FlipTextVo } from "../dataObjs/FlipTextVo";
import { HighlightVo } from "../dataObjs/HighlightVo";

import IBaseScene from "../interfaces/IBaseScene";
import { BaseTextEffect } from "./BaseTextEffect";

export class GrowUp extends BaseTextEffect {
    public scene: Phaser.Scene;   

    private dataProvider: HighlightVo;
    private letters1:Phaser.GameObjects.Text[]=[];
    private letterIndex:number=-1;

    constructor(bscene: IBaseScene, dataProvider: HighlightVo) {
        super(bscene);
       

        this.dataProvider = dataProvider;
      //  this.hstyle.fill=dataProvider.color;
        if (dataProvider.italics==true)
        {
         //   this.hstyle.fontFamily="BarthowheelIt";
        }     
       this.letters1=this.makeHLetters(this.dataProvider.text,this.dataProvider.start,this.dataProvider.end);
       this.letters1.forEach((letter)=>{
           letter.scaleY=0;
           letter.setOrigin(0,1);
          // letter.setAngle(-360);
       })
    }
    flip()
    {
        this.letterIndex++;
        if (this.letterIndex==this.letters1.length)
        {
            this.timer1.remove();
        }
        this.scene.tweens.add({targets: this.letters1[this.letterIndex],duration: 250,scaleY:1});
       /*  this.letters1.forEach((letter)=>{
            letter.scaleX+=0.05;
        })
        if (this.letters1[0].scaleX>0.9)
        {
            this.letters1.forEach((letter)=>{
                letter.scaleX=1;
            })
            this.timer1.remove();
        } */
    }
    public start()
    {
        this.timer1=this.scene.time.addEvent({ delay: 100, callback: this.flip.bind(this), loop: true });
    }
    public removeMe()
    {

    }
}