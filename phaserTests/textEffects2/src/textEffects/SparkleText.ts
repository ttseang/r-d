
import { SparkleVo } from "../dataObjs/SparkleVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseTextEffect } from "./BaseTextEffect";

export class SparkleText extends BaseTextEffect {
    public scene: Phaser.Scene;

    private dataProvider: SparkleVo;
    private letters: Phaser.GameObjects.Text[] = [];
    private letterIndex: number = -1;
    private sparkle: Phaser.GameObjects.Sprite;
    
    constructor(bscene: IBaseScene, dataProvider: SparkleVo) {
        super(bscene);

        this.dataProvider = dataProvider;
       /*  this.t2.color = dataProvider.color;
        if (dataProvider.italics == true) {
           this.t2.fontFamily = "BarthowheelIt";
        }
 */
        this.sparkle = this.scene.add.sprite(0, 0, "sparkle");
        this.sparkle.setFrame(2);
        this.sparkle.setTint(0xFFFF00);
       // this.sparkle.visible = false;
        this.add(this.sparkle);

        this.letters = this.makeLetters(this.dataProvider.text, false);

    }
    public start() {
        //this.scene.tweens.add({targets: this.sparkle,duration: time,y:targetY});
        this.timer1=this.scene.time.addEvent({ delay: 50, callback: this.nextLetter.bind(this), loop: true });
       /*  if (this.dataProvider.delay==0)
        {
            this.scene.time.addEvent({ delay: 1000, callback: this.nextLetter.bind(this), loop: true });
        }
        else
        {
          
            setTimeout(()=>{
               // this.removeAll(true);
                this.scene.time.addEvent({ delay: 1000, callback: this.nextLetter.bind(this), loop: true });
            },this.dataProvider.delay)
        } */
       
    }
    private nextLetter() {
        this.letterIndex++;
        this.sparkle.angle+=20;
        if (this.letterIndex==this.letters.length)
        {
            this.sparkle.visible=false;
            this.timer1.remove();
            return;
        }
        let currentLetter: Phaser.GameObjects.Text = this.letters[this.letterIndex];

        this.sparkle.x = currentLetter.x+currentLetter.displayWidth/2;
        this.sparkle.y = currentLetter.y+currentLetter.displayHeight/2;

        currentLetter.visible = true;
    }
    public removeMe() {

    }
}