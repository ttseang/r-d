
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private testText:Phaser.GameObjects.Text;

    constructor() {
        super("SceneMain");
    }
    preload() {
      
    }
    create() {
        super.create();
        this.makeGrid(11,11);
        //this.grid.showNumbers();
       this.testText=this.add.text(0,0,"Test",{ color: "#ffffff", fontFamily: 'BarthowheelReg', fontSize: '20px' });
        this.testText.visible=false;
        
        window['scene']=this;
    }
    setUp()
    {
        
    }
}