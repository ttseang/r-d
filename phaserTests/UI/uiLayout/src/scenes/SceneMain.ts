
import { ButtonController } from "../classes/comps/ButtonController";
import { CompLayout } from "../classes/comps/CompLayout";
import { CompManager } from "../classes/comps/CompManager";
import { IconTextButton } from "../classes/comps/ui/IconTextButton";
import { TextComp } from "../classes/comps/ui/TextComp";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private compLayout:CompLayout;
    private cm:CompManager=CompManager.getInstance();
    private buttonController:ButtonController=ButtonController.getInstance();

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("book", "./assets/book.png");
        this.load.image("book2", "./assets/book2.png");
        this.load.image("notebook", "./assets/notebook.png");
        this.load.image("pencil", "./assets/pencil.png");
        this.load.image("music", "./assets/music.png");
        this.load.image("keyboard", "./assets/keyboard.png");
        this.load.image("rabbit", "./assets/rabbit.png");
        this.load.image("torn", "./assets/torn.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);
        

        this.compLayout=new CompLayout(this);
        this.compLayout.loadPage(this.cm.startPage);
        this.buttonController.changePage=this.changePage.bind(this);    
        window.onresize=this.doResize.bind(this);
       // this.grid.showPos();

        /*  let testText:TextComp=new TextComp(this,"test","Test Me",300,"redText");
        testText.setPos(2,2);  */
       /*  let btnTest:IconTextButton=new IconTextButton(this,"btnTest","Work On A Lesson","book",this.cm.getButtonStyle("iconrect"));
        btnTest.setPos(5,5); */
    }
    changePage(pageName:string)
    {
        this.compLayout.loadPage(pageName);
    }
    doResize()
    {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();
        this.cm.doResize();
    }
}