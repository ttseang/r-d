export class GameScaleObject 
{
    public letterScale: number;
    public coinScale: number;
    public buttonScale: number;
    constructor(letterScale:number,coinScale:number,buttonScale:number)
    {
        this.letterScale=letterScale;
        this.coinScale=coinScale;
        this.buttonScale=buttonScale;
    }
}