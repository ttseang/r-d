import { GameObjects } from "phaser";
import { ButtonStyleVo } from "../../../dataObjs/ButtonStyleVo";
import IBaseScene from "../../../interfaces/IBaseScene";
import { IComp } from "../../../interfaces/IComp";
import { ButtonController } from "../ButtonController";
import { BaseComp } from "./BaseComp";
import { CompBack } from "./CompBack";

export class IconButton extends BaseComp implements IComp
{
    private back:CompBack
    private icon:GameObjects.Image;
    private buttonVo:ButtonStyleVo;
    private ww:number;
    private hh:number;

    public action:string;
    public actionParam:string;
    
    private buttonController:ButtonController=ButtonController.getInstance();
    
    constructor(bscene:IBaseScene,key:string,iconKey:string,action:string,actionParam:string,buttonVo:ButtonStyleVo)
    {
        super(bscene,key);
        
        this.action=action;
        this.actionParam=actionParam;
       
        this.icon=this.scene.add.image(0,0,iconKey);
        this.buttonVo=buttonVo;

        this.backStyleVo=this.cm.getBackStyle(buttonVo.backStyle);

        let hh: number = this.bscene.getH()*buttonVo.vsize;
        let ww:number=hh;
        
        this.hh=hh;
        this.ww=ww;

        this.back=new CompBack(bscene,ww,hh,this.backStyleVo);
        this.add(this.back);

        this.icon.displayWidth=ww*0.9;
        this.icon.displayHeight=hh*0.9;

        this.add(this.back);
        this.add(this.icon);
        this.on("pointerdown",()=>{
            this.setBorder(this.backStyleVo.borderPress);
            this.buttonController.doAction(this.action,this.actionParam);
        })
        this.on("pointerup",()=>{
            this.resetBorder();
        });
        this.on("pointerover",()=>{
            this.setBorder(this.backStyleVo.borderOver);
        });
        this.on("pointerout",()=>{
            this.resetBorder();
        });
        this.setSize(ww,hh);
        this.scene.add.existing(this);

        this.setInteractive();

        
    }
    doResize()
    {
        super.doResize();
        let hh: number = this.bscene.getH()*this.buttonVo.vsize;
        let ww:number=hh;
        this.back.doResize(ww,hh);
    }
    resetBorder() {
        this.back.lineStyle(2, this.backStyleVo.borderColor);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    }
    setBorder(color: number) {
        this.back.lineStyle(2, color);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    }
}