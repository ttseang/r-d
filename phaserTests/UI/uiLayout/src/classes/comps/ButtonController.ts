
export class ButtonController
{
    private static instance:ButtonController | null=null;

    public changePage:Function=()=>{};

    constructor()
    {

    }
   public static   getInstance():ButtonController
    {
        if (this.instance===null)
        {
            this.instance=new ButtonController();
        }
        return this.instance;
    }

    public doAction(action:string,actionParam:string)
    {
        console.log(action,actionParam);

        switch(action)
        {
            case "loadPage":
            this.changePage(actionParam);
            break;
        }
        
    }
}