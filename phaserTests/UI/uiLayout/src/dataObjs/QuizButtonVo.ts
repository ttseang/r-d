export class QuizButtonVo
{
    public text:string;
    public textColor:string;
    public backColor:number;
    public borderColor:number;

    constructor(text:string,textColor:string,backColor:number,borderColor:number)
    {
        this.text=text;
        this.textColor=textColor;
        this.backColor=backColor;
        this.borderColor=borderColor;
    }
}