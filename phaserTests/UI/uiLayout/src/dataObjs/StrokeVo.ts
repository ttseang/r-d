export class StrokeVo
{
    public thickness:number;
    public color:string;

    constructor(thickness:number,color:string)
    {
        this.thickness=thickness;
        this.color=color;
    }
}