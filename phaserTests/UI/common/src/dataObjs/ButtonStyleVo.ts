//import { BackStyleVo } from "./BackStyleVo";

export class ButtonStyleVo
{
    
    public textColor:string;
    public hsize:number;
    public vsize:number;
    
    public backStyle:string;

    constructor(textColor:string,hsize:number,vsize:number,backStyle:string)
    {
        this.textColor=textColor;
        this.hsize=hsize;
        this.vsize=vsize;
        this.backStyle=backStyle;
    }
   /*  clone()
    {
        return new ButtonStyleVo(this.textColor,this.hsize,this.vsize,this.backColor,this.borderColor,this.borderPress,this.borderOver,this.borderThick);
    } */
}