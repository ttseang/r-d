export class AnchorVo
{
   public anchorTo:string;
   public anchorX:number;
   public anchorY:number;
   public anchorInside:boolean;

    constructor(anchorTo:string,anchorX:number,anchorY:number,anchorInside:boolean)
    {
        this.anchorTo=anchorTo;
        this.anchorX=anchorX;
        this.anchorY=anchorY;
        this.anchorInside=anchorInside;
    }
}