import { BackStyleVo } from "../dataObjs/BackStyleVo";
import { IComp } from "../interfaces/IComp";

export class CompManager
{
    private static instance:CompManager=null;

    public comps:IComp[]=[];
    public backstyles:Map<string,BackStyleVo>=new Map<string,BackStyleVo>();
    
    constructor()
    {
        this.comps=[];
        window['cm']=this;
    }
    public static getInstance():CompManager
    {
        if (this.instance===null)
        {
            this.instance=new CompManager();
        }
        return this.instance;
    }
    public regBackStyle(key:string,backstyle:BackStyleVo)
    {
        this.backstyles.set(key,backstyle);
    }
    public getBackStyle(key:string)
    {
        if (this.backstyles.has(key))
        {
            return this.backstyles.get(key);
        }
        return new BackStyleVo(0xff0000,2,0xffff00,0x00000,0xf0f0f0);
    }
    doResize()
    {
        for (let i:number=0;i<this.comps.length;i++)
        {
            this.comps[i].doResize();
        }
    }
}