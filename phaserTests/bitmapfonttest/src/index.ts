//import Phaser = require("phaser");
import Phaser from 'phaser';
import "./style.css";
import { SceneMain } from "./scenes/SceneMain";
import { GM } from "./classes/GM";
import SceneFont from './scenes/SceneFont';

let gm: GM = GM.getInstance();

let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad = navigator.userAgent.indexOf("iPad");

if (isTablet != -1 || isIpad != -1) {
    gm.isTablet = true;
    isMobile = 1;
}

let w = 600;
let h = 400;
//
//
if (isMobile != -1) {
    gm.isMobile = true;

}
w = window.innerWidth;
h = window.innerHeight;
if (w < h) {
    gm.isPort = true;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    backgroundColor: 'cccccc',
    parent: 'phaser-game',
    scene: [SceneFont,SceneMain]
};

new Phaser.Game(config);