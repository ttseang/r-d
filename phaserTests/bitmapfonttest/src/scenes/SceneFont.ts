//import Phaser from "phaser";

import * as WebFont from "webfontloader";
import { FontLoaderConfig } from "../fonts/FontLoaderConfig";
import { FontUtil } from "../util/FontUtil";

export class SceneFont extends Phaser.Scene {
    private WebFontConfig = new FontLoaderConfig();

    constructor() {
        super("SceneFont");
    }

    preload() {
        // this.load.script('WebFont', './assets/wfl.js');

    }
    create() {

        var fontUtil = new FontUtil(this.fontsLoaded.bind(this));


        this.WebFontConfig.custom.families.push("font1");
        this.WebFontConfig.custom.urls.push("./assets/aff.ttf");

        this.WebFontConfig.inactive = fontUtil.onFontsInactive;
        this.WebFontConfig.fontactive = (familyName: string, fvd: string) => {
            console.log(familyName);
            this.fontReady();
        }
        this.WebFontConfig.active = this.fontReady.bind(this);
        // Start the application, which will add the script to load the fonts based
        // on our configuration.
        fontUtil.init();



    }
    fontsLoaded() {
        //font is loaded, we can now use webfont object
        WebFont.load(this.WebFontConfig);

    }
    fontReady() {
        //the font is active so now we can use it in Phaser 
        this.scene.start("SceneLoad");
    }
}
export default SceneFont;