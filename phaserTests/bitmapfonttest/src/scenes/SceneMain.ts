import { GameObjects } from "phaser";
import { BMF } from "../classes/BMF";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.bitmapFont("ds","./assets/ds.png","./assets/ds.xml");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        console.log("create!");
        let face:Phaser.GameObjects.Image=this.add.image(100, 200, "face");

       let text1:GameObjects.BitmapText=this.add.bitmapText(100,100,"ds","ONce Upon A Time...",).setOrigin(0.5,0.5);
       let bmf1:BMF=new BMF(this,"Happily ever after","ds").setOrigin(0.5,0.5);

        //Align.scaleToGameW(face,0.25,this);
        
        
        this.grid.showNumbers();
        this.grid.placeAtIndex(60,bmf1);
    }
}