
import { GameObjects } from "phaser";
import { CompLayout, CompManager, ButtonController, CompLoader, ResponseImage, IGameObj, IComp, TextComp, TextStyleVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { DragLetter } from "../classes/DragLetter";
import { LetterPost } from "../classes/LetterPost";
import { PaperLetter } from "../classes/PaperLetter";
import { BinVo } from "../dataObjs/BinVo";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;
    private dragLetters: DragLetter[] = [];
    private dragPapers: PaperLetter[] = [];
    private letterPosts: LetterPost[] = [];
    private dragMode: number = 0;
    private currentObj: GameObjects.Container;

    private targetPost: LetterPost;
    private dragType: string = "";
    private dragIndex: number = 0;
    private postIndex: number = 0;

    private binVo: BinVo = new BinVo("pip", "tip", "ets");
    private bin: ResponseImage;
    private binVos: BinVo[] = [];
    private wordIndex: number = -1;
    private wordText:GameObjects.Text;
    private instText:GameObjects.Text;

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("tpaper", "./assets/torn.png");
        this.load.image("bin", "./assets/bin.png");
        this.load.image("cork", "./assets/cork.png");
        this.load.image("arrows", "./assets/arrows.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.cm.regFontSize("main", 30, 1200);
        this.cm.regFontSize("instructions", 20, 1200);
        this.cm.regFontSize("word", 60, 1200);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        
        

        this.buttonController.callback = this.doButtonAction.bind(this);
    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        this.cm.getComp("right").visible = false;
        this.cm.getComp("wrong").visible = false;

        window.onresize = this.doResize.bind(this);
        this.loadGameData();
    }
    loadGameData() {

        fetch("./assets/words.json")
            .then(response => response.json())
            .then(data => this.gotGameData({ data }));
    }
    gotGameData(data: any) {
        console.log(data);
        let words: any = data.data.words;

        for (let i: number = 0; i < words.length; i++) {
            console.log(words[i]);
            let startWord: string = words[i].startword;
            let endWord: string = words[i].endword;
            let letters: string = words[i].letters;
            let binVo: BinVo = new BinVo(startWord, endWord, letters);
            this.binVos.push(binVo);
        }
        this.buildGame();
    }
    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
        // this.grid.showPos();
        this.cm.doResize();


        for (let i:number=0;i<3;i++)
        {
            this.dragPapers[i].doResize(this.gh / 8);
            this.dragLetters[i].doResize(this.gh/10);
            this.letterPosts[i].doResize2(this.gh/10);
        }


        this.lineUpLetters();

        this.wordText.setFontSize(this.cm.getFontSize("main",this.gw));
        this.grid.placeAt(5,1.5,this.wordText);

        this.instText.setFontSize(this.cm.getFontSize("instructions",this.gw));
        this.grid.placeAt(5,2.5,this.instText);


    }
    doButtonAction(action: string, params: string) {
        console.log(action);
        if (action === "check") {
            if (this.dragMode === 2) {
                this.checkWord();
            }
        }
        if (action === "reset") {
            this.resetLevel();
        }
    }
    resetLevel() {
        for (let i: number = 0; i < 3; i++) {
            let dragLetter: DragLetter = this.dragLetters[i];
            let paperLetter: PaperLetter = this.dragPapers[i];

            dragLetter.resetPos();
            paperLetter.resetPos();
        }
        this.cm.getComp("right").visible = false;
        this.cm.getComp("wrong").visible = false;
        this.dragMode = 0;
    }
    buildGame() {

        let styleVo:TextStyleVo=this.cm.getTextStyle("black");
        this.wordText=this.add.text(0,0,"Change X to Y",{fontFamily:styleVo.fontName,color:styleVo.textColor}).setOrigin(0.5,0.5);
        this.wordText.setFontSize(this.cm.getFontSize("main",this.gw));
        this.grid.placeAt(5,1.5,this.wordText);

        this.instText=this.add.text(0,0,"Drag the letter you do not want into the trash can.\nThen drag the right letter from above into the word to take it's place.")
        this.instText.setOrigin(0.5,0.5);
        this.instText.setColor(styleVo.textColor);
        this.instText.setFontSize(this.cm.getFontSize("instructions",this.gw));
        this.grid.placeAt(5,2.5,this.instText);
        

        let xx: number = 0;

        for (let i: number = 0; i < 3; i++) {
            let dragLetter: DragLetter = new DragLetter(this, this.gh / 10, "s", "black");
            dragLetter.index = i;
            this.dragLetters.push(dragLetter);
            dragLetter.setInteractive();

            let dragPaper: PaperLetter = new PaperLetter(this, this.gh / 8, "T", "black");
            dragPaper.index = i;
            dragPaper.setInteractive();
            this.dragPapers.push(dragPaper);

            let letterPost: LetterPost = new LetterPost(this, this.gh / 10, "lp");
            letterPost.index = i;
            this.letterPosts.push(letterPost);

        }

        this.bin = new ResponseImage(this, "bin", 0.1, "bin");
        this.bin.setPos(8, 7);

        let corkBoard: ResponseImage = new ResponseImage(this, "cork", 0.25, "cork");
        corkBoard.setPos(4.75, 6.75);

        this.children.bringToTop(this.wordText);
       
        //letterPost.setPos(5,5);

        this.lineUpLetters();
        this.doNextWord();

        this.input.on("gameobjectdown", this.dragBox.bind(this));
        this.input.on("pointermove", this.onMove.bind(this));
        this.input.on("pointerup", this.onUp.bind(this));


        
    }


    dragBox(p: Phaser.Input.Pointer, obj: any) {

        console.log(obj.dtype, this.dragMode);
        if (obj.dtype != "letter" && obj.dtype != "paper") {
            return;
        }
        if (this.dragMode === 2) {
            return;
        }
        if (this.dragMode === 0) {
            if (obj.dtype !== "paper") {
                return;
            }
        }
        if (this.dragMode === 1) {
            if (obj.dtype !== "letter") {
                return;
            }
        }
        this.dragType = obj.dtype;
        this.dragIndex = obj.index;

        this.children.bringToTop(obj);
        this.currentObj = obj;
    }
    onMove(p: Phaser.Input.Pointer) {
        if (this.currentObj) {
            this.currentObj.x = p.x;
            this.currentObj.y = p.y;
        }
    }
    onUp() {

        if (this.currentObj) {


            if (this.dragMode === 0) {
                let xDist: number = Math.abs(this.currentObj.x - this.bin.x);
                let yDist: number = Math.abs(this.currentObj.y - this.bin.y);

                if (xDist < this.gw * .025 && yDist < this.gh * .025) {
                    this.dragMode = 1;
                    this.children.bringToTop(this.bin);
                    this.postIndex = this.dragIndex;
                }
                else {
                    this.snapBack();
                }
            }
            else {
                let post: LetterPost = this.letterPosts[this.postIndex];
                let xDist: number = Math.abs(this.currentObj.x - post.x);
                let yDist: number = Math.abs(this.currentObj.y - post.y);

                if (xDist < this.gw * .025 && yDist < this.gh * .025) {
                    this.dragMode = 2;
                    this.currentObj.x = post.x;
                    this.currentObj.y = post.y;
                    /*  this.children.bringToTop(this.bin);
                     this.postIndex = this.dragIndex; */
                }
                else {
                    this.snapBack();
                }
            }
        }

        this.currentObj = null;
    }
    snapBack() {
        if (this.currentObj) {
            if (this.dragMode === 0) {
                let post: LetterPost = this.letterPosts[this.dragIndex];

                let snap: Phaser.Tweens.Tween = this.add.tween({ targets: [this.currentObj], duration: 200, x: post.x, y: post.y });
            }
            else {
                let dl: DragLetter = this.dragLetters[this.dragIndex];
                dl.snapBack();
            }
        }
    }
    fillLetters() {
        console.log(this.binVo);

        for (let i: number = 0; i < this.binVo.startWord.length; i++) {
            let paperLetter: PaperLetter = this.dragPapers[i];
            paperLetter.setText(this.binVo.startWord.substr(i, 1));
        }
        for (let i: number = 0; i < this.binVo.letters.length; i++) {
            let dragLetter: DragLetter = this.dragLetters[i];
            dragLetter.setText(this.binVo.letters.substr(i, 1));
        }

        this.wordText.setText("Change the word "+this.binVo.startWord+" to "+this.binVo.endWord);
    }
    lineUpLetters() {
        let xx: number = 0;
        let x2: number = 0;
        let x3: number = 0;

        for (let i: number = 0; i < 3; i++) {
            let dragLetter: DragLetter = this.dragLetters[i];

            this.grid.placeAt(4+(i*0.75), 4, dragLetter);
            //dragLetter.x += xx;
            dragLetter.initPos();

          //  xx += dragLetter.displayWidth * 1.25;


            let dragPaper: PaperLetter = this.dragPapers[i];
            this.grid.placeAt(4 + (i * 0.75), 6.5, dragPaper);
            dragPaper.initPos();
            /* dragPaper.x += x2;
            x2 += dragPaper.displayWidth * 1.2; */


            let letterPost: LetterPost = this.letterPosts[i];
            this.grid.placeAt(4 + (i * 0.75), 6.5, letterPost);
            /* letterPost.x+= x3;
            x3+=dragPaper.displayWidth; */

            this.children.bringToTop(letterPost);
            this.children.bringToTop(dragPaper);
        }
    }
    checkWord() {
        let word: string = "";

        for (let i: number = 0; i < 3; i++) {
            let letter: string = this.dragPapers[i].letter;

            if (i == this.postIndex) {
                letter = this.dragLetters[this.dragIndex].letter;
            }
            word += letter;
        }
        console.log(word);

        if (this.binVo.endWord === word) {
            console.log("correct");
            this.cm.getComp("right").visible = true;
            this.cm.getComp("wrong").visible = false;

            setTimeout(() => {
                this.resetLevel();
                this.doNextWord();
            }, 1000);
        }
        else {
            this.cm.getComp("right").visible = false;
            this.cm.getComp("wrong").visible = true;
            console.log("wrong");
        }
    }
    doNextWord() {
        this.wordIndex++;
        if (this.wordIndex < this.binVos.length) {
            this.binVo = this.binVos[this.wordIndex];
            this.fillLetters();
        }
        else
        {
            this.wordText.setText("Out of Words");
        }
    }

}