export class BinVo {
    public startWord: string;
    public endWord: string;
    public letters: string;

    constructor(startWord: string, endWord: string, letters: string) {
        this.startWord = startWord;
        this.endWord = endWord;
        this.letters = letters;
    }
}