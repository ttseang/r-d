import Phaser from "phaser";

export class BoxData {
    public box: Phaser.GameObjects.Sprite;
    public item: Phaser.GameObjects.Sprite;

    constructor(box: Phaser.GameObjects.Sprite, item: Phaser.GameObjects.Sprite) {
        this.box = box;
        this.item = item;
       
    }
}