import { SpinMask } from "../classes/spinMask";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        let imageIndex:number=Math.floor(Math.random()*8)+1;

        this.load.image("pic1", "./assets/pic"+imageIndex.toString()+".jpg");
        this.load.image("circle", "./assets/circle.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        console.log("create!");
        
        let xx:number=this.gw/2;
        let yy:number=this.gh/2;

        let scale:number=0.5;
        for (let i:number=0;i<5;i++)
        {
            let spinMask:SpinMask=new SpinMask(this,xx,yy,"pic1","circle",scale);
            scale-=0.1;
        }
        


    }
}