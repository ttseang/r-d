import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class SpinMask {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    private image: GameObjects.Image;
    private pad: GameObjects.Image;
    private maskObj: GameObjects.Image;

    private lastX: number = 0;
    private lastY: number = 0;
    private lastAng:number=0;

    constructor(bscene: IBaseScene, x: number, y: number, imageKey: string, maskKey: string,scale:number) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        //
        //
        //
        this.image = this.scene.add.image(x, y, imageKey);


        this.pad = this.scene.add.image(x, y, "circle");
        this.pad.setTint(0xffcc00);
        Align.scaleToGameW(this.pad,scale,this.bscene);
        this.pad.setInteractive();
        this.pad.once("pointerdown", this.onDown.bind(this));
        this.pad.alpha=0.1;
        //
        //
        //
        this.maskObj = this.scene.add.image(x, y, "circle");
        Align.scaleToGameW(this.maskObj,scale,this.bscene);
        this.image.setMask(new Phaser.Display.Masks.BitmapMask(this.scene,this.maskObj));
        this.maskObj.visible=false;

        this.image.setAngle(Math.floor(Math.random()*360));
    }
    private onDown(p: Phaser.Input.Pointer) {
        console.log("ondown");
        this.lastX = p.x;
        this.lastY = p.y;
        this.lastAng=this.image.angle;
        this.pad.on('pointermove', this.onMove2.bind(this));
        this.pad.once('pointerup', this.onUp.bind(this));
    }
    private onUp() {
        this.pad.off('pointermove');
        this.pad.once("pointerdown", this.onDown.bind(this));
    }
    private onMove2(p:Phaser.Input.Pointer) {
        let ang:number=this.getAngle(new PosVo(this.lastX,this.lastY),new PosVo(p.x,p.y));
        console.log(ang);
        this.image.angle=this.lastAng+ang;

        if (Math.abs(this.image.angle)<5)
        {
            this.image.angle=0;
            this.pad.off('pointermove');
            this.pad.removeInteractive();
        }
      // this.lastX=p.x;
      //  this.lastY=p.y;
       // this.lastAng=this.image.angle;
       //this.pad.once('pointermove', this.onMove2.bind(this));
    }
    private getAngle(obj1: PosVo, obj2: PosVo) {
        // angle in radians
        var angleRadians = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
        // angle in degrees
        var angleDeg = (Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x) * 180 / Math.PI);
        return angleDeg;
    }
    private onMove(p: Phaser.Input.Pointer) {
        let diffX: number = Math.abs(this.lastX - p.x);
        let diffY: number = Math.abs(this.lastY - p.y);

        //   console.log(diffX,diffY);

        if (diffY > diffX) {
            if (p.x < this.pad.displayWidth / 2) {
                if (this.lastY < p.y) {
                    console.log("down leftside");
                    this.image.angle--;
                }
                else {
                    console.log("up leftside");
                    this.image.angle++;
                }
            }
            else {
                if (this.lastY < p.y) {
                    console.log("down rightside");
                    this.image.angle++;
                }
                else {
                    console.log("up rightside");
                    this.image.angle--;
                }
            }
        }
        else {
            if (p.y < this.pad.displayHeight / 2) {
                if (this.lastX < p.x) {
                    console.log("right top");
                    this.image.angle++;
                }
                else {
                    console.log("left top");
                    this.image.angle--;
                }
            }
            else {
                if (this.lastX < p.x) {
                    console.log("right bottom");
                    this.image.angle--;
                }
                else {
                    console.log("left bottom");
                    this.image.angle++;
                }
            }
        }
    }
}