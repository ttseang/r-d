import { GameObjects } from "phaser"
import IBaseScene from "../../interfaces/IBaseScene";
import { IGameObj } from "../../interfaces/IGameObj";
import Align from "../../util/align";
import UIBlock from "../../util/UIBlock";
import { Button } from "./Button";

export class MessageBox extends UIBlock implements IGameObj
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private back:GameObjects.Image;
    private text1:GameObjects.Text;
    private button:Button;
    public closeCallback:Function=()=>{};

    constructor(bsene:IBaseScene)
    {
        super();
        this.scene=bsene.getScene();
        this.bscene=bsene;
        //
        //
        //
        this.back=this.scene.add.image(0,0,"mback");
        Align.scaleToGameW(this.back,0.5,this.bscene);
        this.add(this.back);
        this.back.setScrollFactor(0,0);
        //
        //
        //
        this.text1=this.scene.add.text(0,0,"Your Message here, whatever it is",{color:"#00000",fontSize:"26px",wordWrap: { width: this.back.displayWidth*0.9, useAdvancedWrap: true }});
        this.text1.setOrigin(0.5,0.5);
        this.text1.setScrollFactor(0,0);
    
         this.text1.y=-this.back.displayHeight/4;
       // this.text1.x=-this.back.displayWidth/2-this.text1.displayWidth/2;
        this.add(this.text1);
        //
        //
        //

        this.button=new Button(this.bscene,"button","OK");
        this.button.y=this.back.displayHeight/4;
        this.add(this.button);

       // this.setScrollFactor(0,0);
        Align.center(this,this.bscene);
       // this.scene.add.existing(this);
        //
        //
        //
        this.visible=false;
        this.button.setCallback(this.closeMe.bind(this));
        this.setSize(this.back.displayWidth,this.back.displayHeight);
     //   this.setInteractive();
    }
    scaleX: number;
    scaleY: number;
    showMessage(msg:string,buttonText:string="OK")
    {
        this.text1.setText(msg);
        this.button.text1.setText(buttonText);
        this.visible=true;
    }   
    closeMe()
    {
        console.log("close me");
        this.visible=false;
        this.closeCallback();
    }
}