import { LocVo } from "../dataObjs/LocVo";

let instance: GM = null;

export class GM {
    public isMobile: boolean = false;
    public isPort: boolean = false;
    public isTablet: boolean = false;

    public locations: Map<string, LocVo> = new Map<string, LocVo>();
    public buildings: Map<string, LocVo> = new Map<string, LocVo>();
    public currentBuilding:string="";
    public currentLoc:LocVo;
    public lastUnlocked:LocVo;

    constructor() {
        window['gm'] = this;
    }
    static getInstance() {
        if (instance === null) {
            instance = new GM();
        }
        return instance;
    }
    public unlockBuilding(building: string) {
        console.log("unlock "+building);
        if (this.buildings.has(building)) {
            let locVo: LocVo = this.buildings.get(building);
            let key: string = locVo.x.toString() + "-" + locVo.y.toString();

            locVo.locked = false;

            this.locations.set(key, locVo);
            this.buildings.set(building, locVo);
            this.lastUnlocked=locVo;
        }
    }
}