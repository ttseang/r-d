export class LocVo
{
    public x:number;
    public y:number;
    public name:string;
    public locked:boolean=true;
    public unlockNext:string;
    public level:number;

    constructor(level:number,x:number,y:number,name:string,unlockNext:string)
    {
        this.level=level;
        this.x=x;
        this.y=y;
        this.name=name;
        this.unlockNext=unlockNext;
    }
}