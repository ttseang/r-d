import { GM } from "../classes/GM";
import { LocVo } from "../dataObjs/LocVo";
import { BaseScene } from "./BaseScene";

export class SceneData extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneData");
    }
    preload()
    {
        this.load.image("lock", "./assets/lock.png");
        this.load.image("mback", "./assets/ui/mbback.jpg");
        this.load.image("holder", "./assets/ui/holder.jpg");
        this.load.image('button', "./assets/ui/buttons/1/1.png");
        this.load.image("village", "./assets/village.png");
        this.load.tilemapTiledJSON('map', './assets/village.json');
        this.load.image("bakery", "./assets/inside/bakery.png");
        this.load.image("coffee", "./assets/inside/coffee.png");
        this.load.image("pizza", "./assets/inside/pizza.png");
        this.load.image("food", "./assets/inside/food.png");
        this.load.image("bshop", "./assets/inside/bshop.png");
        this.load.atlas("fountain","./assets/animations/fountain1.png","./assets/animations/fountain1.json");
        this.load.image("hand","./assets/hand.png");
    }
    create()
    {
        
        this.getLocations();
    }
    
    getLocations()
    {
        fetch("./assets/locations.json")
        .then(response => response.json())
        .then(data => this.gotLocations({ data }));
    }
    gotLocations(data:any)
    {
        console.log(data);
        let locations:any=data.data.locations;
        console.log(locations);
        for (let i:number=0;i<locations.length;i++)
        {
            let loc:any=locations[i];
        
            let locVo:LocVo=new LocVo(parseInt(loc.level),parseInt(loc.x),parseInt(loc.y),loc.name,loc.unlock);
            let key:string=locVo.x.toString()+"-"+locVo.y.toString();
            this.gm.locations.set(key,locVo);            
            this.gm.buildings.set(locVo.name,locVo);
        }

        this.gm.unlockBuilding("bakery");
        this.scene.start("SceneMain");
    }
}