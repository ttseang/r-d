import { GameObjects, Tilemaps } from "phaser";
import { MessageBox } from "../classes/comps/MessageBox";
import { GM } from "../classes/GM";
import { LocVo } from "../dataObjs/LocVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private controls: any;
    private map: Phaser.Tilemaps.Tilemap;
    private layer1: Phaser.Tilemaps.TilemapLayer;
    private gm: GM = GM.getInstance();
    private clickLock: boolean = false;
    private mb: MessageBox;
   // private cursors: Phaser.Types.Input.Keyboard.CursorKeys;
    private scrollX:number=0;
    private scrollY:number=0;
    
    
    constructor() {
        super("SceneMain");
    }
    preload() {

       
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.makeAnim("fountain");
        //
        //
        //
        this.buildWorld();

        this.mb = new MessageBox(this);
        this.mb.closeCallback = this.messageClose.bind(this);
        //
        //
        //
      //  this.cursors = this.input.keyboard.createCursorKeys();

      this.input.keyboard.on("keydown",this.keyDown.bind(this));
      this.input.keyboard.on('keyup',this.keyUp.bind(this));

    }

    makeAnim(key:string)
    {
        //let frameNames:any[]= this.textures.get(key).getFrameNames();
       // //console.log(frameNames);
        let animConfig:any={
            key: key+'play',
            frames:this.anims.generateFrameNames(key),
            frameRate: 8,
            repeat: -1
        }
        //console.log(animConfig);
        this.anims.create(animConfig);
    }

    makeLocks() {
        this.gm.buildings.forEach((locVo: LocVo) => {
            //console.log(locVo);
            if (locVo.locked == true) {
                this.placeItem(locVo.x, locVo.y, "lock");
                let tile:Phaser.Tilemaps.Tile=this.layer1.getTileAt(locVo.x,locVo.y);
                if (tile)
                {
                    tile.tint=0xbbbbbb;
                }
            }
            if (locVo.name===this.gm.lastUnlocked.name)
            {
                let hand:GameObjects.Image=this.placeItem(locVo.x,locVo.y,"hand");
                Align.scaleToGameW(hand,0.05,this);
                hand.y-=hand.displayHeight;
                hand.x-=hand.displayWidth;
                hand.angle=300;
            }
        });
    }

    messageClose() {
        this.clickLock = false;
        //console.log("message close");
    }
    buildWorld() {
        this.map = this.add.tilemap('map');
        var tileset1 = this.map.addTilesetImage('village', 'village');
        this.layer1 = this.map.createLayer('Tile Layer 1', [tileset1]);


        /*  let layer2=this.map.createFromObjects("shops",[]);
         //console.log(layer2); */

        window['scene'] = this;
        //   this.cameras.main.setPosition(0,0);
        //this.cameras.main.setZoom(2);

        //  this.input.on("gameobjectdown", this.objectClicked.bind(this));
        this.input.on('pointerdown', this.worldClicked.bind(this));
        this.cameras.main.setScroll(-1900, 900);
        this.cameras.main.setZoom(1);

        this.makeLocks();

        this.placeAnim(4,7,"fountain");
    }
    worldClicked(p: Phaser.Input.Pointer) {
        if (this.clickLock === true) {
            return;
        }
        const { worldX, worldY } = p

        //const startVec = this.layer1.worldToTileXY(this.layer1.x, this.layer1.y)
        const targetVec = this.layer1.worldToTileXY(worldX, worldY);
        ////console.log(targetVec);

        let xx: number = Math.round(targetVec.x) - 1;
        let yy: number = Math.round(targetVec.y);

        let tile = this.layer1.getTileAt(xx, yy);
        //console.log(tile);

        //   tile.alpha=0.5;
        //console.log(xx, yy);
        let key: string = xx.toString() + "-" + yy.toString();

        if (this.gm.locations.has(key)) {
            let locVo: LocVo = this.gm.locations.get(key);

            let locName: string = locVo.name;
            //console.log(locName);

            /* let locVo2:LocVo=this.gm.buildings.get(locName);
            //console.log(locVo2); */
            if (locVo.locked === true) {
                this.clickLock = true;
                this.mb.showMessage("That building is locked!");
            }
            else {
                this.gm.currentBuilding = locName;
                this.gm.currentLoc = locVo;
                this.fadeScene();
                return;
            }
        }
        else {
            //console.log("nothing");

            let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(tile.x - 1, tile.y);
            //console.log(pos);
            // this.cameras.main.setScroll(this.gw/2+pos.x,this.gh/2+pos.y);
            // this.layer1.setPosition(pos.x,pos.y,0,0);
        }
    }
    public placeItem(xx: number, yy: number, key: string) {
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx + 1, yy);

        let item: GameObjects.Image = this.add.image(pos.x, pos.y, key);
        return item;
    }
    public placeAnim(xx:number,yy:number,key:string)
    {
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx , yy);
        let tile:Phaser.Tilemaps.Tile=this.layer1.getTileAt(xx,yy);
        
        let item: GameObjects.Sprite = this.add.sprite(pos.x-tile.width/2, pos.y+tile.height/3, key);//.setOrigin(0,0);
        Align.scaleToGameW(item,0.15,this);
        let animKey:string=key+"play";
        item.play(animKey);
        return item;
    }
    fadeScene() {
        this.tweens.add({ targets: this.layer1, duration: 1000, alpha: 0.1, onComplete: this.fadeDone.bind(this) });
    }
    fadeDone() {
        this.scene.start("SceneInside");
    }
    objectClicked(pointer, obj) {
        //console.log(obj);
    }
    keyDown(e)
    {
        //console.log(e);
        if (e.key=="ArrowLeft")
        {
            //console.log("left");
            this.scrollX=-1;
            this.scrollY=-1;
        }
        if (e.key=="ArrowRight")
        {
            //console.log("right");
            this.scrollX=1;
            this.scrollY=1;
        }
        if (e.key=="ArrowDown")
        {
            this.scrollX=-1;
            this.scrollY=1;
        }
        if (e.key=="ArrowUp")
        {
            this.scrollX=1;
            this.scrollY=-1;
        }
    }
    keyUp(e)
    {
        //console.log("key up");
        this.scrollX=0;
        this.scrollY=0;
    }
    update(time: number, delta: number) {

        let xx: number = this.cameras.main.scrollX+this.scrollX;
        let yy: number = this.cameras.main.scrollY+this.scrollY;
        this.cameras.main.setScroll(xx,yy);
       /*  let xx: number = this.cameras.main.x;
        let yy: number = this.cameras.main.y;

        //this.controls.update(delta)
        if (this.cursors.left.isDown === true) {
            this.cameras.main.setPosition(xx - 1, yy - 1);
            return;
        }
        if (this.cursors.right.isDown === true) {
            this.cameras.main.setPosition(xx + 1, yy + 1);
            return;
        }
        if (this.cursors.up.isDown === true) {
            this.cameras.main.setPosition(xx + 1, yy - 1);
            return;
        }
        if (this.cursors.down.isDown === true) {
            this.cameras.main.setPosition(xx - 1, yy + 1);
            return;
        } */
     //   this.cameras.main.setScroll(xx, yy);
    }
}