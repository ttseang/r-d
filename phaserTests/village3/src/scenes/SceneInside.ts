import { GameObjects } from "phaser";
import { MessageBox } from "../classes/comps/MessageBox";
import { GM } from "../classes/GM";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneInside extends BaseScene {
    private backImage: GameObjects.Image;
    private gm: GM = GM.getInstance();
    private clickLock: boolean = true;
    private mb:MessageBox;
    
    constructor() {
        super("SceneInside");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        // this.grid.showNumbers();


        this.backImage = this.add.image(0, 0, this.gm.currentBuilding);
        Align.scaleToGameW(this.backImage, 0.35, this);
        Align.center(this.backImage, this);

       

        this.backImage.on("pointerdown", () => {
            if (this.clickLock == false) {
                this.gm.unlockBuilding(this.gm.currentLoc.unlockNext);
                this.fadeScene();
            }
        })
        this.mb=new MessageBox(this);
        this.mb.closeCallback=this.messageClose.bind(this);

        setTimeout(() => {
            let level:string=this.gm.currentLoc.level.toString();
            this.mb.showMessage("Welcome To Chapter "+level+"!","I Have Completed\nChapter "+level);
        }, 1500);
       
    }
    fadeScene() {
        this.tweens.add({ targets: this.backImage, duration: 1000, alpha: 0.1, onComplete: this.fadeDone.bind(this) });
    }
    fadeDone() {
        this.scene.start("SceneMain");
    }
    messageClose()
    {
        this.clickLock=false;
        this.backImage.setInteractive();
        console.log("message close");
    }
}