import { CompGame } from "../classes/CompGame";
import { ListBox } from "../classes/ListBox";
import { SylBox } from "../classes/SylBox";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("qmark","./assets/questionmark.png");
    }
    create() {

        super.create();
        this.makeGrid(11,11);
        this.grid.showPos();

        let compGame:CompGame=new CompGame(this);
        compGame.start();
    }
}