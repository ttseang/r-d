import { Loader } from "phaser";
import { ButtonStyleVo } from "../dataObjs/ButtonStyleVo";
import { PosVo } from "../dataObjs/PosVo";
import { TextStyleVo } from "../dataObjs/TextStyleVo";
import { WordPartVo } from "../dataObjs/WordPartVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { CompManager } from "./comps/CompManager";
import { BlankComp } from "./comps/ui/BlankComp";
import { IconButton } from "./comps/ui/IconButton";
import { UIWindow } from "./comps/ui/UIWindow";
import { ListBox } from "./ListBox";
import { QBox } from "./QBox";
import { SylBox } from "./SylBox";

export class CompGame {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private words:WordPartVo[]=[];
    private wordList:string[]=[];
    private boxes:SylBox[]=[];

    private box:SylBox;
    private qbox:QBox;

    
    private listHolder:BlankComp;
    private qBoxHolder:BlankComp;

    private btnHelp:IconButton;
    private cm:CompManager=CompManager.getInstance();

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;
    }
    start() {
         
        this.scene.load.json("words", "./assets/words.json");
        this.scene.load.loadComplete = this.dataLoaded.bind(this);
        this.scene.load.start();
    }
    dataLoaded() {
        let words=this.scene.cache.json.get("words").words;
        console.log(words);

        for (let i:number=0;i<words.length;i++)
        {
            let word:any=words[i];
            let wordVo:WordPartVo=new WordPartVo(word.word,word.p1,word.p2);
            this.words.push(wordVo);

            this.wordList.push(word.word);
        }
        this.makeUI();
        this.create();
    }
    makeUI()
    {
    
       let textStyle:TextStyleVo=new TextStyleVo("Arial","#ff0000",40);
       
       let blueButton:ButtonStyleVo=new ButtonStyleVo("red",0.7,0.7,"blue");

       let uiWindow:UIWindow=new UIWindow(this.bscene,"backwindow",0.85,0.7,"default");
       uiWindow.setPos(5,5);

       this.btnHelp=new IconButton(this.bscene,"btnHelp","qmark","help","",this.cm.getButtonStyle("default"));
       this.btnHelp.setPos(1,9.5);
    }
    create() {       

        console.log(this.words);

        let lb: ListBox = new ListBox(this.bscene, this.wordList, 3);
        lb.x=-lb.displayWidth/2+(lb.displayWidth*0.05);
        lb.y=-lb.displayHeight/2;

        this.listHolder=new BlankComp(this.bscene,"listHolder",lb,"default");
        this.listHolder.setPos(3,5);
      
        let picks:number[]=this.pickThree();
        let correct:number=Math.floor(Math.random()*3);

        let ww:number=0;
        
        for (let j:number=0;j<picks.length;j++)
        {
            let index:number=picks[j];

            let sb: SylBox = new SylBox(this.bscene,this.words[index].part1);
            sb.setInteractive();
            this.boxes.push(sb);
            if (sb.displayWidth>ww)
            {
                ww=sb.displayWidth*1.1;
            }
        }

        let pos:PosVo=this.bscene.getGrid().getRealXY(7,5);
        let xx:number=pos.x;
        let yy:number=pos.y;

        for (let i:number=0;i<picks.length;i++)
        {       
            this.boxes[i].setBackSize(ww);
            this.boxes[i].x=xx;
            this.boxes[i].y=yy;
            this.boxes[i].initPos();
            xx+=ww*1.3;
            //this.bscene.getGrid().placeAt(6+(i*.5),5,this.boxes[i]);
        }
        /* let sb: SylBox = new SylBox(this.bscene, "Hub");
        sb.x = 200;
        sb.y = 100; */

        let qBox:QBox=new QBox(this.bscene,this.words[picks[correct]],ww);
        this.qbox=qBox;

        this.qBoxHolder=new BlankComp(this.bscene,"qbox",qBox,"defualt");
        this.qBoxHolder.setPos(8,7);

       // this.bscene.getGrid().placeAt(6,6,qBox);

       

        this.scene.input.on("gameobjectdown",this.startDrag.bind(this));
        this.scene.input.on("pointerup",this.onUp.bind(this));
        this.scene.input.on("pointermove",this.onMove.bind(this));
    }
    startDrag(p:Phaser.Input.Pointer,box:SylBox)
    {
        this.box=box;
        this.scene.children.bringToTop(this.box);
    }
    onMove(p:Phaser.Input.Pointer)
    {
        if (this.box)
        {
            this.box.x=p.x;
            this.box.y=p.y;
        }
    }
    onUp()
    {
        if (!this.qbox || !this.box)
        {
            return;
        }
        let distX:number=Math.abs(this.box.x-this.qBoxHolder.x);
        let distY:number=Math.abs(this.box.y-this.qBoxHolder.y);

        console.log(distX,distY);

        if (distX<this.qbox.displayWidth/2 && distY<this.qbox.displayHeight/2)
        {
            this.box.visible=false;
            this.qbox.setText(this.box.text);

            for (let i:number=0;i<this.boxes.length;i++)
            {
                if (this.boxes[i]!=this.box)
                {
                    this.boxes[i].resetPos();
                }
            }

        }
        else
        {
            this.box.snapBack();
        }

        this.box=null;
    }
    pickThree()
    {
        let indexes:number[]=[];
        let len:number=this.words.length;
        for (let i:number=0;i<len;i++)
        {
            indexes.push(i);
        }
        
        for (let j:number=0;j<100;j++)
        {
            let r1:number=Math.floor(Math.random()*len);
            let r2:number=Math.floor(Math.random()*len);

            let temp:number=indexes[r1];
            indexes[r1]=indexes[r2];
            indexes[r2]=temp;            
        }

        return indexes.slice(0,3);
    }
}