import { GameObjects } from "phaser";
import { WordPartVo } from "../dataObjs/WordPartVo";
import IBaseScene from "../interfaces/IBaseScene";

export class QBox extends GameObjects.Container
{
    private bscene:IBaseScene;
    public text1:GameObjects.Text;
    public text2:GameObjects.Text;
    private holder:GameObjects.Image;
    public text:string="";
    

    constructor(bscene:IBaseScene,word:WordPartVo,ww:number)
    {
        super(bscene.getScene());
        this.bscene=bscene;

        this.holder=this.scene.add.image(0,0,"holder");
        this.text1=this.scene.add.text(0,0,word.part2,{fontSize:"24px",color:"black"}).setOrigin(0,0.5);
        this.text2=this.scene.add.text(0,0,word.part1,{fontSize:"24px",color:"black"}).setOrigin(0,0.5);
        
        this.text2.x=-this.text2.displayWidth;
        this.text2.visible=false;

        this.holder.displayWidth=ww;
        this.holder.displayHeight=this.text1.displayHeight*1.5;
        this.holder.x=-this.holder.displayWidth/2;

        this.add(this.holder);
        this.add(this.text1);
        this.add(this.text2);
       
        let ww2:number=this.holder.displayWidth+this.text1.displayWidth;
        
        this.setSize(ww2*1.4,this.holder.displayHeight*1.2);
        this.scene.add.existing(this);
    }
    setText(text:string)
    {
        this.text=text;
        this.text2.setText(text);
        this.text2.x=-this.text2.displayWidth;
        this.text2.visible=true;
    }
    
}