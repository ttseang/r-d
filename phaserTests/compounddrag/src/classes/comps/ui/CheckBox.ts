import { GameObjects } from "phaser";
import IBaseScene from "../../../interfaces/IBaseScene";
import { IComp } from "../../../interfaces/IComp";

import { BaseComp } from "./BaseComp";
import { CompBack } from "./CompBack";

export class CheckBox extends BaseComp implements IComp
{
    private back:CompBack;
    private check:GameObjects.Image;
    private vscale:number;

    constructor(bscene:IBaseScene,key:string,vscale:number,backStyle:string)
    {
        super(bscene,key);
        
        let hh: number = this.bscene.getH()*vscale;
        let ww:number=hh;
        this.vscale=vscale;

        this.backStyleVo=this.cm.getBackStyle(backStyle);

        this.back=new CompBack(bscene,ww,hh,this.backStyleVo);
        this.check=this.scene.add.image(0,0,"check");
        this.add(this.back);

        this.check.displayWidth=ww*0.9;
        this.check.displayHeight=hh*0.9;

        this.add(this.back);
        this.add(this.check);

        this.setSize(ww,hh);
        this.scene.add.existing(this);

        this.setInteractive();

        this.on("pointerdown",()=>{
            this.check.visible=!this.check.visible;
        })
    }

    doResize()
    {
        super.doResize();
        let hh: number = this.bscene.getH()*this.vscale;
        let ww:number=hh;
        this.check.displayWidth=ww*0.9;
        this.check.displayHeight=hh*0.9;
        this.back.doResize(ww,hh);
    }
}