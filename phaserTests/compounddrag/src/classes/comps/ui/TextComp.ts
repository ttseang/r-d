import { GameObjects } from "phaser";
import { TextStyleVo } from "../../../dataObjs/TextStyleVo";
import IBaseScene from "../../../interfaces/IBaseScene";
import { IComp } from "../../../interfaces/IComp";
import { BaseComp } from "./BaseComp"

export class TextComp extends BaseComp implements IComp {
    private text1: GameObjects.Text;
    public ww: number = 0;
    public textStyleVo: TextStyleVo;

    constructor(bscene: IBaseScene, key: string, text: string, maxWidth: number, textStyle: string) {
        super(bscene, key);

        this.ww = maxWidth;

        let textStyleVo: TextStyleVo = this.cm.getTextStyle(textStyle);
        this.textStyleVo = textStyleVo;

       // //console.log(textStyleVo);

        this.text1 = this.scene.add.text(0, 0, text).setOrigin(0.5, 0.5);
        this.text1.setFontFamily(textStyleVo.fontName);
        this.text1.setColor(textStyleVo.textColor);

        if (textStyleVo.strokeVo) {
            //console.log(textStyleVo.strokeVo);
            this.text1.setStroke(textStyleVo.strokeVo.color, textStyleVo.strokeVo.thickness);
        }

        if (textStyleVo.shadowVo) {
            this.text1.setShadow(textStyleVo.shadowVo.x, textStyleVo.shadowVo.y, textStyleVo.shadowVo.color, textStyleVo.shadowVo.blur, textStyleVo.shadowVo.stroke, textStyleVo.shadowVo.fill);
        }

        this.add(this.text1);
        this.sizeText();

        this.scene.add.existing(this);
    }
    setText(text:string)
    {
        this.text1.setText(text);
    }
    setColor(c: string) {
        this.text1.setColor(c);
    }
    sizeText() {
        this.text1.setFontSize(this.textStyleVo.maxFontSize);

        let fs: number = parseInt(this.text1.style.fontSize.split("px")[0]);
        while (this.text1.displayWidth > this.ww * 0.95) {
            fs--;
            this.text1.setFontSize(fs);
        }
    }
    doResize() {
        this.sizeText();
        super.doResize();
    }
}