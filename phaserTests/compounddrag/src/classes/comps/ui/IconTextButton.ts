import { GameObjects } from "phaser";
import { ButtonStyleVo } from "../../../dataObjs/ButtonStyleVo";
import IBaseScene from "../../../interfaces/IBaseScene";
import { IComp } from "../../../interfaces/IComp";
import { ButtonController } from "../ButtonController";
import { BaseComp } from "./BaseComp";
import { CompBack } from "./CompBack";
import { TextComp } from "./TextComp";

export class IconTextButton extends BaseComp implements IComp {
    private back: CompBack
    private icon: GameObjects.Image;
    private buttonVo: ButtonStyleVo;
    private text1: TextComp;
    private ww: number;
    private hh: number;
    public text: string;

    public action:string;
    public actionParam:string;
    private buttonController:ButtonController=ButtonController.getInstance();

    constructor(bscene: IBaseScene, key: string, text: string, iconKey: string,action:string,actionParam:string, buttonVo: ButtonStyleVo) {
        super(bscene, key);


        this.icon = this.scene.add.image(0, 0, iconKey);
        this.buttonVo = buttonVo;
        this.text = text;

        this.action=action;
        this.actionParam=actionParam;

        this.backStyleVo = this.cm.getBackStyle(buttonVo.backStyle);
       // this.textStyleVo=this.cm.getTextStyle(buttonVo.textStyle);

        let hh: number = this.bscene.getH() * buttonVo.vsize;
        let ww: number = this.bscene.getW() * buttonVo.hsize;

        this.hh = hh;
        this.ww = ww;

        this.backStyleVo.round = false;
        this.back = new CompBack(bscene, ww, hh, this.backStyleVo);
        this.add(this.back);

        this.icon.displayHeight = hh * 0.5;
        this.icon.y = hh * .1;
        this.icon.scaleX = this.icon.scaleY;

        window['tb']=this;

        this.text1=new TextComp(this.bscene,this.key+"-text",text,ww,buttonVo.textStyle);
        this.text1.ingoreRepos=true;
        this.text1.setPos(0,0);
        
        this.text1.y = -hh / 2.75;

        this.add(this.back);
        this.add(this.icon);
        this.add(this.text1);
        

        this.on("pointerdown", () => {
            this.setBorder(this.backStyleVo.borderPress);
            this.buttonController.doAction(this.action,this.actionParam);
        })
        this.on("pointerup", () => {
            this.resetBorder();
        });
        this.on("pointerover", () => {
            this.setBorder(this.backStyleVo.borderOver);
        });
        this.on("pointerout", () => {
            this.resetBorder();
        });
        this.setSize(ww, hh);
        this.scene.add.existing(this);

        this.setInteractive();

       // this.sizeText();
    }
   /*  sizeText()
    {
        this.text1.setFontSize(40);
        
        let fs:number=parseInt(this.text1.style.fontSize.split("px")[0]);
        while(this.text1.displayWidth>this.ww*0.95)
        {
            fs--;
            this.text1.setFontSize(fs);
        }
    } */
    doResize() {
        super.doResize();
        let hh: number = this.bscene.getH() * this.buttonVo.vsize;
        let ww: number = this.bscene.getW() * this.buttonVo.hsize;

        this.ww=ww;
        this.hh=hh;
        
        this.back.doResize(ww, hh);

        this.icon.displayHeight = hh * 0.5;
        this.icon.y = hh * .1;
        this.icon.scaleX = this.icon.scaleY;
        this.text1.doResize();
       // this.sizeText();
        this.text1.y = -hh / 2.75;
        this.text1.x=0;

        
    }
    resetBorder() {
        this.back.lineStyle(2, this.backStyleVo.borderColor);
        if (this.backStyleVo.round === true) {
            this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        }
        else {
            this.back.strokeRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh);
        }
        this.back.stroke();
    }
    setBorder(color: number) {
        this.back.lineStyle(2, color);
        if (this.backStyleVo.round === true) {
            this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        }
        else {
            this.back.strokeRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh);
        }
        this.back.stroke();
    }
}