
import { GameObjects } from "phaser";
import { ButtonStyleVo } from "../../../dataObjs/ButtonStyleVo";
import { PosVo } from "../../../dataObjs/PosVo";
import IBaseScene from "../../../interfaces/IBaseScene";
import { IComp } from "../../../interfaces/IComp";
import { GM } from "../../GM";
import { ButtonController } from "../ButtonController";
import { BaseComp } from "./BaseComp";

export class TriangleButton extends BaseComp implements IComp {
    
    private buttonVo: ButtonStyleVo;
    private back: GameObjects.Graphics;
    private border: GameObjects.Graphics;

    public action:string;
    public actionParam:string;

    private ww: number;
    private hh: number;

    private gm:GM=GM.getInstance();
   

    public posVo:PosVo=new PosVo(0,0);
    private buttonController:ButtonController=ButtonController.getInstance();

    constructor(bscene: IBaseScene,key:string,action:string,actionParam:string, buttonVo: ButtonStyleVo,angle:number=0) {
        super(bscene,key);
       
        this.buttonVo=buttonVo;
        this.backStyleVo=this.cm.getBackStyle(buttonVo.backStyle);


      //  let ww: number = this.bscene.getW() * buttonVo.hsize;
        let hh: number = this.bscene.getH()*buttonVo.vsize;
        let ww:number=hh;
        

        this.ww = ww;
        this.hh = hh;


        //  hh=this.text1.displayHeight*3;

        this.back = this.scene.add.graphics();
        this.border = this.scene.add.graphics();

        this.border.lineStyle(2, this.backStyleVo.borderColor);
        this.border.moveTo(0,0);
        this.border.lineTo(ww/2,hh);
        this.border.lineTo(-ww/2,hh);
        this.border.lineTo(0,0);
        this.border.stroke();

        this.back.fillStyle(this.backStyleVo.backColor, 1);
        this.back.moveTo(0,0);
        this.back.lineTo(ww/2,hh);
        this.back.lineTo(-ww/2,hh);
        this.back.lineTo(0,0);
        //this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.back.fillPath();

        this.add(this.back);
        this.add(this.border);

       
        this.setAngle(angle);

        this.scene.add.existing(this);
        this.setSize(ww, hh);


        this.setInteractive();

        this.on("pointerdown",()=>{
            this.setBorder(this.backStyleVo.borderPress);
            this.buttonController.doAction(this.action,this.actionParam);
        })
        this.on("pointerup",()=>{
            this.resetBorder();
        });
        this.on("pointerover",()=>{
            this.setBorder(this.backStyleVo.borderOver);
        });
        this.on("pointerout",()=>{
            this.resetBorder();
        });

      

        window['btn']=this;
    }
   
    doResize()
    {
        let ww: number = this.bscene.getW() * this.buttonVo.hsize;
        let hh: number = this.bscene.getH()*this.buttonVo.vsize;

        this.ww = ww;
        this.hh = hh;

        this.border.clear();
        this.back.clear();

        this.border.lineStyle(2, this.backStyleVo.borderColor);
        this.border.moveTo(0,0);
        this.border.lineTo(ww/2,hh);
        this.border.lineTo(-ww/2,hh);
        this.border.lineTo(0,0);
        this.border.stroke();

        this.back.fillStyle(this.backStyleVo.backColor, 1);
        this.back.moveTo(0,0);
        this.back.lineTo(ww/2,hh);
        this.back.lineTo(-ww/2,hh);
        this.back.lineTo(0,0);
        //this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.back.fillPath();


        this.setSize(ww,hh);
        
        super.doResize();
    }
    
    resetBorder() {
        this.border.clear();
        this.border.lineStyle(2, this.backStyleVo.borderColor);
       // this.border.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.border.moveTo(0,0);
        this.border.lineTo(this.ww/2,this.hh);
        this.border.lineTo(-this.ww/2,this.hh);
        this.border.lineTo(0,0);
        this.border.stroke();
    }
    setBorder(color: number) {
        this.border.clear();
        this.border.lineStyle(2, color);
        this.border.moveTo(0,0);
        this.border.lineTo(this.ww/2,this.hh);
        this.border.lineTo(-this.ww/2,this.hh);
        this.border.lineTo(0,0);
        this.border.stroke();
    }
}