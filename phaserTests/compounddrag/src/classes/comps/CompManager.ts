import { BackStyleVo } from "../../dataObjs/BackStyleVo";
import { ButtonStyleVo } from "../../dataObjs/ButtonStyleVo";
import { CompVo } from "../../dataObjs/CompVo";
import { PageVo } from "../../dataObjs/PageVo";
import { ShadowVo } from "../../dataObjs/ShadowVo";
import { StrokeVo } from "../../dataObjs/StrokeVo";
import { TextStyleVo } from "../../dataObjs/TextStyleVo";
import { IComp } from "../../interfaces/IComp";


export class CompManager {
    private static instance: CompManager = null;

    public comps: IComp[] = [];
    public compDefs: CompVo[] = [];
    public pageDefs: PageVo[] = [];
    public currentPage: PageVo | null = null;

    private defTextStyle: TextStyleVo;

    public compMap: Map<string, IComp> = new Map<string, IComp>();

    public backstyles: Map<string, BackStyleVo> = new Map<string, BackStyleVo>();
    public buttonStyles: Map<string, ButtonStyleVo> = new Map<string, ButtonStyleVo>();
    public textStyles: Map<string, TextStyleVo> = new Map<string, TextStyleVo>();

    public startPage: string = "";

    constructor() {
        this.comps = [];
        window['cm'] = this;
        //Palanquin Dark
        //Andika
        this.defTextStyle = new TextStyleVo("font1", "#000000", 50);
        this.defTextStyle.strokeVo = new StrokeVo(4, "#ffffff");
        this.defTextStyle.shadowVo = new ShadowVo(5, 5, "#000000",4,true,false);

    }
    public static getInstance(): CompManager {
        if (this.instance === null) {
            this.instance = new CompManager();
        }
        return this.instance;
    }
    public loadPage(pageName: string) {
        for (let i: number = 0; i < this.pageDefs.length; i++) {
            if (this.pageDefs[i].pageName === pageName) {
                this.currentPage = this.pageDefs[i];
                this.compDefs = this.currentPage.comps;
                return;
            }
        }
        this.comps = [];
    }
    public regTextStyle(key: string, textStyle: TextStyleVo) {
        this.textStyles.set(key, textStyle);
    }
    public getTextStyle(key: string) {
        if (this.textStyles.has(key)) {
            return this.textStyles.get(key);
        }
        return this.defTextStyle;
    }
    public regBackStyle(key: string, backstyle: BackStyleVo) {
        this.backstyles.set(key, backstyle);
    }
    public regButtonStyle(key: string, buttonStyle: ButtonStyleVo) {
        this.buttonStyles.set(key, buttonStyle);
    }
    public getBackStyle(key: string) {
        if (this.backstyles.has(key)) {
            return this.backstyles.get(key);
        }
        return new BackStyleVo(0xF8F8F8, 8, 0x435D7B, 0x00000, 0xf0f0f0);
    }
    public getButtonStyle(key: string) {
        if (this.buttonStyles.has(key)) {
            return this.buttonStyles.get(key);
        }
        return new ButtonStyleVo("#ffffff", 0.06, 0.06, "default");
    }
    public getBasicTextStyle(key:string)
    {
        let textStyle:TextStyleVo=this.getTextStyle(key);
        return {"fontFamily":textStyle.fontName,"color":textStyle.textColor};
    }
    doResize() {
        for (let i: number = 0; i < this.comps.length; i++) {
            this.comps[i].doResize();
        }
    }
}