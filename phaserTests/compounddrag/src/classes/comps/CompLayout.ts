import { GameObjects } from "phaser";
import { AnchorVo } from "../../dataObjs/AnchorVo";
import { BackStyleVo } from "../../dataObjs/BackStyleVo";
import { ButtonStyleVo } from "../../dataObjs/ButtonStyleVo";
import { CompVo } from "../../dataObjs/CompVo";
import IBaseScene from "../../interfaces/IBaseScene";
import { IComp } from "../../interfaces/IComp";
import { IGameObj } from "../../interfaces/IGameObj";
import { CompManager } from "./CompManager";
import { BackgroundImage } from "./ui/BackgroundImage";
import { BorderButton } from "./ui/BorderButton";
import { CheckBox } from "./ui/CheckBox";
import { IconButton } from "./ui/IconButton";
import { IconTextButton } from "./ui/IconTextButton";
import { ResponseImage } from "./ui/ResponseImage";
import { TriangleButton } from "./ui/TriangleButton";
import UISlider from "./ui/UISlider";
import { UIWindow } from "./ui/UIWindow";

export class CompLayout {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    private cm: CompManager = CompManager.getInstance();
    
    private allComps:IGameObj[]=[];
    
    private background:BackgroundImage

    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
        this.scene = bscene.getScene();

    }
    clear()
    {
        if (this.background)
        {
            this.background.destroy();
        }
        let len:number=this.allComps.length;
        for (let i:number=0;i<len;i++)
        {
           this.allComps[i].destroy();
        }
        this.allComps=[];
    }
    loadPage(pageName:string)
    {
        this.cm.loadPage(pageName);
        this.clear();
        this.build();
    }
    build() {

        this.background=new BackgroundImage(this.bscene,"background",this.cm.currentPage.backgroundType,this.cm.currentPage.backgroundParams);

       /*  if (this.cm.currentPage.backgroundType==="color")
        {
            this.background=this.scene.add.image(0,0,"holder").setOrigin(0,0);
            this.background.displayWidth=this.bscene.getW();
            this.background.displayHeight=this.bscene.getH();

            this.background.setTint(parseInt(this.cm.currentPage.backgroundParams));
        } */


        for (let i: number = 0; i < this.cm.compDefs.length; i++) {
            let def: CompVo = this.cm.compDefs[i];

            let key: string = def.key;
            let icon: string = def.icon;

            let type: string = def.type;
            let backStyle: string = def.backstyle;
            let style: string = def.style;

            let ww: number = def.w;
            let hh: number = def.h;
            let xx: number = def.x;
            let yy: number = def.y;

            let text: string = def.text;

            let anchorVo: AnchorVo = def.anchorVo;

            let angle: number = def.angle;

            let action:string=def.action;
            let actionParam:string=def.actionParam;

            let buttonVo: ButtonStyleVo = this.cm.getButtonStyle(style);


            switch (type) {
                case "window":
                    let uiWindow: UIWindow = new UIWindow(this.bscene, key, ww, hh, backStyle);
                    uiWindow.setPos(xx, yy);
                    this.allComps.push(uiWindow);

                    break;

                case "btn":

                    let button: BorderButton = new BorderButton(this.bscene, key, text,action,actionParam, buttonVo);
                    button.setPos(xx, yy);

                    if (anchorVo) {

                        if (this.cm.compMap.has(anchorVo.anchorTo)) {
                            let comp: IComp = this.cm.compMap.get(anchorVo.anchorTo);
                            button.anchorTo(comp, anchorVo.anchorX, anchorVo.anchorY, anchorVo.anchorInside);
                        }
                    }
                    this.allComps.push(button);

                    break;

                case "img":
                    let image: ResponseImage = new ResponseImage(this.bscene, key, hh, text);
                    image.setPos(xx, yy);
                    image.flipX = def.flipX;
                    image.flipY = def.flipY;

                    this.allComps.push(image);

                    break;

                case "slider":
                    let sliderStyle: BackStyleVo = this.cm.getBackStyle(backStyle);
                    let slider: UISlider = new UISlider(this.bscene, key, hh, ww, sliderStyle.backColor, sliderStyle.borderColor);
                    slider.setPos(xx, yy);

                    this.allComps.push(slider);

                    break;

                case "checkbox":
                    let checkBox: CheckBox = new CheckBox(this.bscene, key, hh, backStyle);
                    checkBox.setPos(xx, yy);

                    this.allComps.push(checkBox);
                    break;

                case "tributton":

                    let triButton: TriangleButton = new TriangleButton(this.bscene, key,action,actionParam, buttonVo);
                    triButton.setPos(xx, yy);
                    triButton.setAngle(angle);

                    this.allComps.push(triButton);

                    break;

                case "iconbutton":

                    let btnIcon: IconButton = new IconButton(this.bscene, key, icon, action,actionParam,buttonVo);
                    btnIcon.setPos(xx, yy);

                    this.allComps.push(btnIcon);

                    break;

                case "icontextbutton":

                    let btnIconText: IconTextButton = new IconTextButton(this.bscene, key, text, icon,action,actionParam, buttonVo);
                    btnIconText.setPos(xx, yy);

                    this.allComps.push(btnIconText);

                    break;
            }

        }

    }

}