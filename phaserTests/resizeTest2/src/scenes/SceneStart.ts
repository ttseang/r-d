import { Button } from "../classes/comps/Button";
import { ResizeManager } from "../classes/resizeManager";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "./BaseScene";
import { SpineAnim2 } from "../classes/SpinAnim2";
import { GameObjects } from "phaser";
import { FallingObj } from "../classes/FallingObj";

export class SceneStart extends BaseScene implements IBaseScene {
    private button1: Button;
    //private face:GameObjects.Sprite;
    private coral: SpineAnim2;

    private resizeManager: ResizeManager;

    constructor() {
        super("SceneStart");
    }
    preload() {
        this.load.setPath("./assets/");

        // this.load.spine("me", 'me_01.json', ['me_01.atlas'], false);
        this.load.spine("coral", 'coral reef.json', ['coral reef.atlas'], false);


        this.load.image("button", "ui/buttons/1/1.png");
        this.load.image("starfish", "starfish.png");
        this.load.image("seahorse", "seahorse.png");
       // this.load.atlas("parachutes", "parachutes.png", "parachutes.json");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);
        this.grid.showPos();

        

        this.resizeManager = ResizeManager.getInstance(this);

        window['scene'] = this;

        this.makeChildren();
        this.definePos();
        this.placeItems();
    }
    makeChildren() {
       
        // this.face=this.add.sprite(0,0,"face");

        //this.add.spine(0,0,"coral","animation",true);

         this.coral = new SpineAnim2(this, "coral","coral");
         this.coral.cover=true;
         this.coral.setReal(1666.79, 1048.97);
        this.coral.setOffSet(785, -1040);
        this.coral.play("animation",true);

        this.button1 = new Button(this, "button", "Start");
      
    }
    definePos() {
        //
        //
        //
        let buttonLayout: LayoutVo = this.resizeManager.addItem("button1", this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 10, 10, 0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,10,19,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,3,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,19,19,0.4));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,2,2,.1));

        //
        //FACES
        //
        let keys: string[] = [];
        keys.push("button1");
        

        let deck: PosVo[] = this.makeDeck();

        for (let i: number = 0; i < 10; i++) {
            let face: GameObjects.Sprite = this.add.sprite(0, 0, "starfish");
            let faceLayout: LayoutVo = this.resizeManager.addItem("starfish" + i, face);
            keys.push("starfish" + i);
            let scale: number = Math.floor((Math.random() * 50)) / 500;
            // console.log(scale);
            faceLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", deck[i].x, deck[i].y, scale));
        }

        for (let i: number = 0; i < 5; i++) {
            let fallingObj: FallingObj = new FallingObj(this,"seahorse");
            let fallLayout: LayoutVo = this.resizeManager.addItem("falling" + i.toString(), fallingObj);
            fallLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", i * 4, 0, 0.05));
            this.grid.placeAt(i * 4, 0, fallingObj);
            fallingObj.pos = fallLayout.getDefinition(ResizeVo.DEVICE_DESKTOP, "");

            keys.push("falling" + i.toString());
        }

         //
        //
        //
        let coralLayout:LayoutVo=this.resizeManager.addItem("coral",this.coral);
        coralLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",-0.5,-0.5,1));
        keys.push("coral");
        //
        //
        //
        //buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,10,.3));

        this.resizeManager.setKeys(keys);

        // this.resizeManager.setKeys(['button1','face']);

    }
    placeItems() {
        this.resizeManager.placeChildren();
    }
    makeDeck() {
        let deck: PosVo[] = [];

        for (let i: number = 0; i < 22; i++) {
            for (let j: number = 0; j < 22; j++) {
                deck.push(new PosVo(i, j));
            }
        }
        return this.shuffleArray(deck);
    }
    shuffleArray(deck: PosVo[]) {
        for (let i: number = 0; i < 500; i++) {
            let t1: number = Math.floor(Math.random() * deck.length);
            let t2: number = Math.floor(Math.random() * deck.length);

            let temp: PosVo = deck[t1];
            deck[t1] = deck[t2];
            deck[t2] = temp;

        }
        return deck;
    }

}