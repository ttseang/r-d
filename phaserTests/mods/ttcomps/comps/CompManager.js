"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompManager = void 0;
var BackStyleVo_1 = require("../dataObjs/BackStyleVo");
var ButtonStyleVo_1 = require("../dataObjs/ButtonStyleVo");
var FontSizeVo_1 = require("../dataObjs/FontSizeVo");
var ShadowVo_1 = require("../dataObjs/ShadowVo");
var StrokeVo_1 = require("../dataObjs/StrokeVo");
var TextStyleVo_1 = require("../dataObjs/TextStyleVo");
var CompManager = /** @class */ (function () {
    function CompManager() {
        this.comps = [];
        this.compDefs = [];
        this.pageDefs = [];
        this.currentPage = null;
        this.defFontSize = new FontSizeVo_1.FontSizeVo(70, 1000);
        this.compMap = new Map();
        this.backstyles = new Map();
        this.buttonStyles = new Map();
        this.textStyles = new Map();
        this.fontSizes = new Map();
        this.defBackStyle = new BackStyleVo_1.BackStyleVo(0xF8F8F8, 8, 0x435D7B, 0x00000, 0xf0f0f0);
        this.defButtonStyle = new ButtonStyleVo_1.ButtonStyleVo("#ffffff", 0.06, 0.06, "default");
        this.startPage = "";
        this.comps = [];
        window['cm'] = this;
        //Palanquin Dark
        //Andika
        this.defTextStyle = new TextStyleVo_1.TextStyleVo("font1", "#000000", 50);
        this.defTextStyle.strokeVo = new StrokeVo_1.StrokeVo(4, "#ffffff");
        this.defTextStyle.shadowVo = new ShadowVo_1.ShadowVo(5, 5, "#000000", 4, true, false);
    }
    CompManager.getInstance = function () {
        if (this.instance === null) {
            this.instance = new CompManager();
        }
        return this.instance;
    };
    CompManager.prototype.updatePos = function (key, pos) {
        var compVo = this.getCompVo(key);
        if (compVo) {
            compVo.x = pos.y;
            compVo.y = pos.y;
        }
    };
    CompManager.prototype.loadPage = function (pageName) {
        for (var i = 0; i < this.pageDefs.length; i++) {
            if (this.pageDefs[i].pageName === pageName) {
                this.currentPage = this.pageDefs[i];
                this.compDefs = this.currentPage.comps;
                return;
            }
        }
        this.comps = [];
    };
    CompManager.prototype.getComp = function (key) {
        if (this.compMap.has(key)) {
            return this.compMap.get(key);
        }
        return null;
    };
    CompManager.prototype.regTextStyle = function (key, textStyle) {
        this.textStyles.set(key, textStyle);
    };
    CompManager.prototype.getTextStyle = function (key) {
        if (this.textStyles.has(key)) {
            return this.textStyles.get(key);
        }
        return this.defTextStyle;
    };
    CompManager.prototype.getCompVo = function (key) {
        for (var i = 0; i < this.compDefs.length; i++) {
            if (this.compDefs[i].key === key) {
                return this.compDefs[i];
            }
        }
        return null;
    };
    CompManager.prototype.getFontSize = function (sizeKey, canvasWidth) {
        var fontSizeVo = this.defFontSize;
        if (this.fontSizes.has(sizeKey)) {
            fontSizeVo = this.fontSizes.get(sizeKey);
        }
        var fontSize = fontSizeVo.defFontSize;
        var fontBase = fontSizeVo.canvasBase;
        var ratio = fontSize / fontBase; // calc ratio
        var size = canvasWidth * ratio; // get font size based on current width
        return (size | 0);
    };
    CompManager.prototype.regFontSize = function (key, defFontSize, canvasBase) {
        this.fontSizes.set(key, new FontSizeVo_1.FontSizeVo(defFontSize, canvasBase));
    };
    CompManager.prototype.regBackStyle = function (key, backstyle) {
        this.backstyles.set(key, backstyle);
    };
    CompManager.prototype.regButtonStyle = function (key, buttonStyle) {
        this.buttonStyles.set(key, buttonStyle);
    };
    CompManager.prototype.getBackStyle = function (key) {
        if (this.backstyles.has(key)) {
            return this.backstyles.get(key);
        }
        return this.defBackStyle;
    };
    CompManager.prototype.getButtonStyle = function (key) {
        if (this.buttonStyles.has(key)) {
            return this.buttonStyles.get(key);
        }
        return this.defButtonStyle;
    };
    CompManager.prototype.getBasicTextStyle = function (key) {
        var textStyle = this.getTextStyle(key);
        return { "fontFamily": textStyle.fontName, "color": textStyle.textColor };
    };
    CompManager.prototype.doResize = function () {
        for (var i = 0; i < this.comps.length; i++) {
            this.comps[i].doResize();
        }
    };
    CompManager.instance = null;
    return CompManager;
}());
exports.CompManager = CompManager;
exports.default = CompManager;
//# sourceMappingURL=CompManager.js.map