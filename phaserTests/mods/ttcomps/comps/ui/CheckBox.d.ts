import { IComp, IBaseScene } from "../..";
import { BaseComp } from "./BaseComp";
export declare class CheckBox extends BaseComp implements IComp {
    private back;
    private check;
    private vscale;
    constructor(bscene: IBaseScene, key: string, vscale: number, backStyle: string);
    doResize(): void;
}
