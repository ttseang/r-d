import IBaseScene from "../../interfaces/IBaseScene";
import { IComp } from "../../interfaces/IComp";
import { BaseComp } from "./BaseComp";
export declare class BackgroundImage extends BaseComp implements IComp {
    private back;
    constructor(bscene: IBaseScene, key: string, type: string, params: string);
    doResize(): void;
}
