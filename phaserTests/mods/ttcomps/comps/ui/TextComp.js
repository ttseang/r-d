"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextComp = void 0;
var BaseComp_1 = require("./BaseComp");
var TextComp = /** @class */ (function (_super) {
    __extends(TextComp, _super);
    function TextComp(bscene, key, text, maxWidth, textStyle) {
        var _this = _super.call(this, bscene, key) || this;
        _this.ww = 0;
        _this.maxWidth = maxWidth;
        _this.ww = bscene.getW() * maxWidth;
        console.log(_this.ww);
        var textStyleVo = _this.cm.getTextStyle(textStyle);
        _this.textStyleVo = textStyleVo;
        // //console.log(textStyleVo);
        _this.text1 = _this.scene.add.text(0, 0, text).setOrigin(0.5, 0.5);
        _this.text1.setFontFamily(textStyleVo.fontName);
        _this.text1.setColor(textStyleVo.textColor);
        if (textStyleVo.strokeVo) {
            //console.log(textStyleVo.strokeVo);
            _this.text1.setStroke(textStyleVo.strokeVo.color, textStyleVo.strokeVo.thickness);
        }
        if (textStyleVo.shadowVo) {
            _this.text1.setShadow(textStyleVo.shadowVo.x, textStyleVo.shadowVo.y, textStyleVo.shadowVo.color, textStyleVo.shadowVo.blur, textStyleVo.shadowVo.stroke, textStyleVo.shadowVo.fill);
        }
        _this.add(_this.text1);
        _this.sizeText();
        _this.scene.add.existing(_this);
        return _this;
    }
    TextComp.prototype.setText = function (text) {
        this.text1.setText(text);
    };
    TextComp.prototype.setColor = function (c) {
        this.text1.setColor(c);
    };
    TextComp.prototype.sizeText = function () {
        this.text1.setFontSize(this.textStyleVo.maxFontSize);
        var fs = parseInt(this.text1.style.fontSize.split("px")[0]);
        while (this.text1.displayWidth > this.ww * 0.95) {
            fs--;
            this.text1.setFontSize(fs);
        }
    };
    TextComp.prototype.doResize = function () {
        this.ww = this.bscene.getW() * this.maxWidth;
        this.sizeText();
        _super.prototype.doResize.call(this);
    };
    return TextComp;
}(BaseComp_1.BaseComp));
exports.TextComp = TextComp;
//# sourceMappingURL=TextComp.js.map