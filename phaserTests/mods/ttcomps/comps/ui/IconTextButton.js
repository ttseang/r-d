"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.IconTextButton = void 0;
var ButtonController_1 = require("../ButtonController");
var BaseComp_1 = require("./BaseComp");
var CompBack_1 = require("./CompBack");
var TextComp_1 = require("./TextComp");
var IconTextButton = /** @class */ (function (_super) {
    __extends(IconTextButton, _super);
    function IconTextButton(bscene, key, text, iconKey, action, actionParam, buttonVo) {
        var _this = _super.call(this, bscene, key) || this;
        _this.buttonController = ButtonController_1.ButtonController.getInstance();
        _this.icon = _this.scene.add.image(0, 0, iconKey);
        _this.buttonVo = buttonVo;
        _this.text = text;
        _this.action = action;
        _this.actionParam = actionParam;
        _this.backStyleVo = _this.cm.getBackStyle(buttonVo.backStyle);
        // this.textStyleVo=this.cm.getTextStyle(buttonVo.textStyle);
        var hh = _this.bscene.getH() * buttonVo.vsize;
        var ww = _this.bscene.getW() * buttonVo.hsize;
        _this.hh = hh;
        _this.ww = ww;
        _this.backStyleVo.round = false;
        _this.back = new CompBack_1.CompBack(bscene, ww, hh, _this.backStyleVo);
        _this.add(_this.back);
        _this.icon.displayHeight = hh * 0.5;
        _this.icon.y = hh * .1;
        _this.icon.scaleX = _this.icon.scaleY;
        window['tb'] = _this;
        _this.text1 = new TextComp_1.TextComp(_this.bscene, _this.key + "-text", text, ww, buttonVo.textStyle);
        _this.text1.ingoreRepos = true;
        _this.text1.setPos(0, 0);
        _this.text1.y = -hh / 2.75;
        _this.add(_this.back);
        _this.add(_this.icon);
        _this.add(_this.text1);
        _this.on("pointerdown", function () {
            _this.setBorder(_this.backStyleVo.borderPress);
            _this.buttonController.doAction(_this.action, _this.actionParam);
        });
        _this.on("pointerup", function () {
            _this.resetBorder();
        });
        _this.on("pointerover", function () {
            _this.setBorder(_this.backStyleVo.borderOver);
        });
        _this.on("pointerout", function () {
            _this.resetBorder();
        });
        _this.setSize(ww, hh);
        _this.scene.add.existing(_this);
        _this.setInteractive();
        return _this;
        // this.sizeText();
    }
    /*  sizeText()
     {
         this.text1.setFontSize(40);
         
         let fs:number=parseInt(this.text1.style.fontSize.split("px")[0]);
         while(this.text1.displayWidth>this.ww*0.95)
         {
             fs--;
             this.text1.setFontSize(fs);
         }
     } */
    IconTextButton.prototype.doResize = function () {
        _super.prototype.doResize.call(this);
        var hh = this.bscene.getH() * this.buttonVo.vsize;
        var ww = this.bscene.getW() * this.buttonVo.hsize;
        this.ww = ww;
        this.hh = hh;
        this.back.doResize(ww, hh);
        this.icon.displayHeight = hh * 0.5;
        this.icon.y = hh * .1;
        this.icon.scaleX = this.icon.scaleY;
        this.text1.doResize();
        // this.sizeText();
        this.text1.y = -hh / 2.75;
        this.text1.x = 0;
    };
    IconTextButton.prototype.resetBorder = function () {
        this.back.lineStyle(2, this.backStyleVo.borderColor);
        if (this.backStyleVo.round === true) {
            this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        }
        else {
            this.back.strokeRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh);
        }
        this.back.stroke();
    };
    IconTextButton.prototype.setBorder = function (color) {
        this.back.lineStyle(2, color);
        if (this.backStyleVo.round === true) {
            this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        }
        else {
            this.back.strokeRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh);
        }
        this.back.stroke();
    };
    return IconTextButton;
}(BaseComp_1.BaseComp));
exports.IconTextButton = IconTextButton;
//# sourceMappingURL=IconTextButton.js.map