import { IComp, PosVo, IBaseScene } from "../..";
import { ButtonStyleVo } from "../../dataObjs/ButtonStyleVo";
import { BaseComp } from "./BaseComp";
export declare class TriangleButton extends BaseComp implements IComp {
    private buttonVo;
    private back;
    private border;
    action: string;
    actionParam: string;
    private ww;
    private hh;
    posVo: PosVo;
    private buttonController;
    constructor(bscene: IBaseScene, key: string, action: string, actionParam: string, buttonVo: ButtonStyleVo, angle?: number);
    doResize(): void;
    resetBorder(): void;
    setBorder(color: number): void;
}
