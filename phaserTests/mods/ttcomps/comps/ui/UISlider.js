"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.UISlider = void 0;
var BaseComp_1 = require("./BaseComp");
var UISlider = /** @class */ (function (_super) {
    __extends(UISlider, _super);
    function UISlider(bscene, key, hscale, vscale, color, knobColor) {
        var _this = _super.call(this, bscene, key) || this;
        //
        //
        //
        _this.vscale = vscale;
        _this.hscale = hscale;
        _this.back = _this.scene.add.image(0, 0, "holder");
        _this.back.setTint(color);
        _this.add(_this.back);
        _this.knob = _this.scene.add.image(0, 0, "holder");
        _this.knob.setTint(knobColor);
        _this.knob.setInteractive();
        _this.knob.on("pointerdown", _this.knobDown.bind(_this));
        _this.add(_this.knob);
        _this.doResize();
        _this.scene.add.existing(_this);
        return _this;
    }
    UISlider.prototype.knobDown = function (p) {
        this.knob.x = p.x - this.x;
        this.bscene.getScene().input.on("pointermove", this.dragKnob.bind(this));
        this.bscene.getScene().input.once("pointerup", this.knobUp.bind(this));
    };
    UISlider.prototype.dragKnob = function (p) {
        this.knob.x = p.x - this.x;
        if (this.knob.x < -this.back.displayWidth / 2) {
            this.knob.x = -this.back.displayWidth / 2;
        }
        if (this.knob.x > this.back.displayWidth / 2) {
            this.knob.x = this.back.displayWidth / 2;
        }
    };
    UISlider.prototype.knobUp = function () {
        this.bscene.getScene().input.off("pointermove");
    };
    UISlider.prototype.doResize = function () {
        this.back.displayWidth = this.bscene.getW() * this.hscale;
        this.back.displayHeight = this.bscene.getH() * this.vscale;
        this.knob.displayWidth = this.back.displayWidth / 20;
        this.knob.displayHeight = this.back.displayHeight;
        _super.prototype.doResize.call(this);
    };
    return UISlider;
}(BaseComp_1.BaseComp));
exports.UISlider = UISlider;
exports.default = UISlider;
//# sourceMappingURL=UISlider.js.map