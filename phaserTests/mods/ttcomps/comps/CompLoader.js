"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompLoader = void 0;
var __1 = require("..");
var ButtonStyleVo_1 = require("../dataObjs/ButtonStyleVo");
var CompLoader = /** @class */ (function () {
    function CompLoader(callback) {
        this.cm = __1.CompManager.getInstance();
        this.callback = callback;
    }
    CompLoader.prototype.loadComps = function (file) {
        var _this = this;
        fetch(file)
            .then(function (response) { return response.json(); })
            .then(function (data) { return _this.process({ data: data }); });
    };
    CompLoader.prototype.process = function (data) {
        var info = data.data.info;
        var startPage = info.startPage;
        //console.log(startPage);
        this.cm.startPage = startPage;
        //textstyles
        var textStyles = data.data.textStyles;
        if (textStyles) {
            for (var i = 0; i < textStyles.length; i++) {
                var style = textStyles[i];
                var key = style.key;
                var ff = style.fontFamily;
                var color = style.color;
                var maxSize = parseFloat(style.maxSize);
                var stroke = style.stroke;
                var strokeVo = null;
                if (stroke) {
                    var strokeThick = parseFloat(stroke.thick);
                    var strokeColor = stroke.color;
                    strokeVo = new __1.StrokeVo(strokeThick, strokeColor);
                }
                var shadow = style.shadow;
                var shadowVo = null;
                if (shadow) {
                    var shadowX = parseFloat(shadow.x);
                    var shadowY = parseFloat(shadow.y);
                    var shadowColor = shadow.color;
                    var shadowBlur = parseFloat(shadow.blur);
                    var shadowStroke = shadow.stroke;
                    var shadowFill = shadow.fill;
                    shadowVo = new __1.ShadowVo(shadowX, shadowY, shadowColor, shadowBlur, shadowStroke, shadowFill);
                }
                var textStyleVo = new __1.TextStyleVo(ff, color, maxSize);
                textStyleVo.shadowVo = shadowVo;
                textStyleVo.strokeVo = strokeVo;
                this.cm.regTextStyle(key, textStyleVo);
            }
        }
        //backstyles
        var backStyles = data.data.backStyles;
        if (backStyles) {
            for (var i = 0; i < backStyles.length; i++) {
                var style = backStyles[i];
                var key = style.key;
                var backColor = parseInt(style.backColor);
                var borderColor = parseInt(style.borderColor);
                var borderOver = parseInt(style.borderOver);
                var borderPress = parseInt(style.borderPress);
                var borderThick = parseInt(style.borderThick);
                var backStyle = new __1.BackStyleVo(backColor, borderThick, borderColor, borderPress, borderOver);
                this.cm.regBackStyle(key, backStyle);
            }
        }
        //button styles
        var buttonStyles = data.data.buttonStyles;
        for (var i = 0; i < buttonStyles.length; i++) {
            var buttonStyle = buttonStyles[i];
            var key = buttonStyle.key;
            var buttonTextStyle = buttonStyle.textStyle;
            var vsize = parseFloat(buttonStyle.vsize);
            var hsize = parseFloat(buttonStyle.hsize);
            var backStyle = buttonStyle.backStyle;
            var buttonStyleVo = new ButtonStyleVo_1.ButtonStyleVo(buttonTextStyle, hsize, vsize, backStyle);
            this.cm.regButtonStyle(key, buttonStyleVo);
        }
        //pages
        var pages = data.data.pages;
        for (var k = 0; k < pages.length; k++) {
            var page = data.data.pages[k];
            var pageVo = new __1.PageVo(page.name, []);
            pageVo.backgroundType = page.backgroundType;
            pageVo.backgroundParams = page.backgroundParams;
            //comps
            var comps = page.components;
            for (var i = 0; i < comps.length; i++) {
                var comp = comps[i];
                var id = comp.id;
                var type = comp.type;
                var text = comp.text || "";
                var x = parseFloat(comp.x);
                var y = parseFloat(comp.y);
                var h = parseFloat(comp.h) || -1;
                var w = parseFloat(comp.w) || -1;
                var style = comp.style || "";
                var backStyle = comp.backStyle || "";
                var icon = comp.icon || "";
                var action = comp.action || "";
                var actionParam = comp.actionParam || "";
                var flipX = comp.flipX || false;
                var flipY = comp.flipY || false;
                var angle = comp.angle || 0;
                //
                //anchor
                var anchorVo = null;
                var anchorTo = comp.anchorTo;
                if (anchorTo) {
                    var anchorY = parseInt(comp.anchorY);
                    var anchorX = parseInt(comp.anchorX);
                    var anchorInside = comp.anchorInside;
                    anchorVo = new __1.AnchorVo(anchorTo, anchorX, anchorY, anchorInside);
                }
                var compVo = new __1.CompVo(id, type, text, icon, x, y, w, h, style, backStyle);
                compVo.anchorVo = anchorVo;
                compVo.flipX = flipX;
                compVo.flipY = flipY;
                compVo.angle = angle;
                compVo.action = action;
                compVo.actionParam = actionParam;
                // //console.log(compVo);
                pageVo.comps.push(compVo);
                // this.cm.compDefs.push(compVo);
            }
            this.cm.pageDefs.push(pageVo);
        }
        this.callback();
    };
    return CompLoader;
}());
exports.CompLoader = CompLoader;
//# sourceMappingURL=CompLoader.js.map