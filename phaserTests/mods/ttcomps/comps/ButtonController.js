"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ButtonController = void 0;
var ButtonController = /** @class */ (function () {
    function ButtonController() {
        this.callback = function () { };
    }
    ButtonController.getInstance = function () {
        if (this.instance === null) {
            this.instance = new ButtonController();
        }
        return this.instance;
    };
    ButtonController.prototype.doAction = function (action, actionParam) {
        this.callback(action, actionParam);
    };
    ButtonController.instance = null;
    return ButtonController;
}());
exports.ButtonController = ButtonController;
//# sourceMappingURL=ButtonController.js.map