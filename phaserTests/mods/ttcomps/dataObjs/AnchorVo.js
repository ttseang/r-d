"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnchorVo = void 0;
var AnchorVo = /** @class */ (function () {
    function AnchorVo(anchorTo, anchorX, anchorY, anchorInside) {
        this.anchorTo = anchorTo;
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.anchorInside = anchorInside;
    }
    return AnchorVo;
}());
exports.AnchorVo = AnchorVo;
//# sourceMappingURL=AnchorVo.js.map