import { ShadowVo } from "./ShadowVo";
import { StrokeVo } from "./StrokeVo";
export declare class TextStyleVo {
    strokeVo: StrokeVo;
    shadowVo: ShadowVo;
    fontName: string;
    textColor: string;
    maxFontSize: number;
    constructor(fontName: string, textColor: string, maxFontSize: number);
}
