"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackStyleVo = void 0;
var BackStyleVo = /** @class */ (function () {
    function BackStyleVo(backColor, borderThick, borderColor, borderPress, borderOver) {
        this.round = true;
        this.backColor = backColor;
        this.borderThick = borderThick;
        this.borderColor = borderColor;
        this.borderPress = borderPress;
        this.borderOver = borderOver;
    }
    return BackStyleVo;
}());
exports.BackStyleVo = BackStyleVo;
//# sourceMappingURL=BackStyleVo.js.map