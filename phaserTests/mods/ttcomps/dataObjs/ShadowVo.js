"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShadowVo = void 0;
var ShadowVo = /** @class */ (function () {
    function ShadowVo(x, y, color, blur, stroke, fill) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.blur = blur;
        this.stroke = stroke;
        this.fill = fill;
    }
    return ShadowVo;
}());
exports.ShadowVo = ShadowVo;
//# sourceMappingURL=ShadowVo.js.map