export declare class FontSizeVo {
    defFontSize: number;
    canvasBase: number;
    constructor(defFontSize: number, canvasBase: number);
}
