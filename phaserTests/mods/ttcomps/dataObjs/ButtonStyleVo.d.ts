export declare class ButtonStyleVo {
    textStyle: string;
    hsize: number;
    vsize: number;
    backStyle: string;
    constructor(textStyle: string, hsize: number, vsize: number, backStyle: string);
}
