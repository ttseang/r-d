"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StarBurst = exports.Sparks = exports.Ripple = exports.Flare = exports.ColorBurst = void 0;
var colorBurst_1 = require("./classes/effects/colorBurst");
Object.defineProperty(exports, "ColorBurst", { enumerable: true, get: function () { return colorBurst_1.ColorBurst; } });
var flare_1 = require("./classes/effects/flare");
Object.defineProperty(exports, "Flare", { enumerable: true, get: function () { return flare_1.Flare; } });
var ripple_1 = require("./classes/effects/ripple");
Object.defineProperty(exports, "Ripple", { enumerable: true, get: function () { return ripple_1.Ripple; } });
var sparks_1 = require("./classes/effects/sparks");
Object.defineProperty(exports, "Sparks", { enumerable: true, get: function () { return sparks_1.Sparks; } });
var starBurst_1 = require("./classes/effects/starBurst");
Object.defineProperty(exports, "StarBurst", { enumerable: true, get: function () { return starBurst_1.StarBurst; } });
/*
import Phaser from 'phaser';
import { SceneMain } from "./scenes/SceneMain";
import {GM} from "./classes/GM";

let gm:GM=GM.getInstance();

let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad=navigator.userAgent.indexOf("iPad");

    if (isTablet!=-1 || isIpad!=-1)
    {
        gm.isTablet=true;
        isMobile=1;
    }

let w = 600;
let h = 400;
//
//
if (isMobile != -1) {
    gm.isMobile=true;
    
}
w = window.innerWidth;
    h = window.innerHeight;
if (w<h)
{
    gm.isPort=true;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    backgroundColor:'cccccc',
    parent: 'phaser-game',
    scene: [SceneMain]
};

new Phaser.Game(config); */ 
//# sourceMappingURL=index.js.map