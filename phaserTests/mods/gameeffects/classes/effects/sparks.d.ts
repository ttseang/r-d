export declare class Sparks {
    scene: Phaser.Scene;
    color: number;
    size: number;
    count: number;
    x: number;
    y: number;
    constructor(scene: Phaser.Scene, x?: number, y?: number, size?: number, color?: number, count?: number);
    start(): void;
    tweenDone(tween: any, targets: any, custom: any): void;
    getSpark(): Phaser.GameObjects.Graphics;
}
