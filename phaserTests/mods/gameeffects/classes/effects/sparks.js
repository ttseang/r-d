"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sparks = void 0;
var Sparks = /** @class */ (function () {
    function Sparks(scene, x, y, size, color, count) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (size === void 0) { size = 1; }
        if (color === void 0) { color = 0xffffff; }
        if (count === void 0) { count = 25; }
        // super(config.scene);
        this.scene = scene;
        this.color = color;
        this.size = size;
        this.count = count;
        this.x = x;
        this.y = y;
    }
    Sparks.prototype.start = function () {
        for (var i = 0; i < this.count; i++) {
            var s = this.getSpark();
            s.x = this.x;
            s.y = this.y;
            //   this.add(s);
            var angle = i * (360 / this.count);
            var r = Phaser.Math.Between(50, 100);
            var tx = this.x + r * Math.cos(angle);
            var ty = this.y + r * Math.sin(angle);
            this.scene.tweens.add({
                targets: s,
                duration: 1000,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone.bind(this)
            });
        }
    };
    Sparks.prototype.tweenDone = function (tween, targets, custom) {
        targets[0].destroy();
    };
    Sparks.prototype.getSpark = function () {
        var s = this.scene.add.graphics();
        s.fillStyle(this.color, 1);
        s.fillCircle(0, 0, this.size);
        return s;
    };
    return Sparks;
}());
exports.Sparks = Sparks;
//# sourceMappingURL=sparks.js.map