"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ripple = void 0;
var Ripple = /** @class */ (function () {
    function Ripple(scene, x, y, count, size, dist, duration, color) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (count === void 0) { count = 25; }
        if (size === void 0) { size = 5; }
        if (dist === void 0) { dist = 150; }
        if (duration === void 0) { duration = 1000; }
        if (color === void 0) { color = 0xffffff; }
        this.scene = scene;
        this.size = size;
        //
        //
        //
        this.color = color;
        this.x = x;
        this.y = y;
        this.count = count;
        this.size = size;
        this.dist = dist;
        this.duration = duration;
    }
    Ripple.prototype.start = function () {
        for (var i = 0; i < this.count; i++) {
            var s = this.getSpark();
            s.x = this.x;
            s.y = this.y;
            //
            //
            //
            var angle = i * (360 / this.count);
            //  let r = game.rnd.integerInRange(50, 100);
            var tx = this.x + this.dist * Math.cos(angle);
            var ty = this.y + this.dist * Math.sin(angle);
            this.scene.tweens.add({
                targets: s,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                        scope: this
                    }]
            });
        }
    };
    Ripple.prototype.tweenDone = function (tween, targets, custom) {
        targets[0].destroy();
    };
    Ripple.prototype.getSpark = function () {
        var s = this.scene.add.graphics();
        s.fillStyle(this.color, 1);
        s.fillCircle(0, 0, this.size);
        return s;
    };
    return Ripple;
}());
exports.Ripple = Ripple;
//# sourceMappingURL=ripple.js.map