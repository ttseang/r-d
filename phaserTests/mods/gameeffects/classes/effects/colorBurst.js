"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ColorBurst = void 0;
var ColorBurst = /** @class */ (function () {
    function ColorBurst(scene, x, y, size, count, dist, duration, maxDist, color) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (size === void 0) { size = 5; }
        if (count === void 0) { count = 25; }
        if (dist === void 0) { dist = 150; }
        if (duration === void 0) { duration = 1000; }
        if (maxDist === void 0) { maxDist = 300; }
        if (color === void 0) { color = 0xffffff; }
        this.scene = scene;
        this.x = x;
        this.y = y;
        this.size = size;
        this.count = count;
        this.dist = dist;
        this.duration = duration;
        this.maxDist = maxDist;
        this.color = color;
    }
    ColorBurst.prototype.start = function () {
        for (var i = 0; i < this.count; i++) {
            var star = this.scene.add.sprite(this.x, this.y, "effectColorStars");
            //
            //
            //
            var f = Phaser.Math.Between(0, 14);
            star.setFrame(f);
            star.setOrigin(0.5, 0.5);
            var r = Phaser.Math.Between(50, this.maxDist);
            var s = Phaser.Math.Between(1, 100) / 100;
            star.scaleX = s;
            star.scaleY = s;
            var angle = i * (360 / this.count);
            var tx = this.x + r * Math.cos(angle);
            var ty = this.y + r * Math.sin(angle);
            //  star.x=tx;
            // star.y=ty;
            this.scene.tweens.add({
                targets: star,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone.bind(this)
            });
        }
    };
    ColorBurst.prototype.tweenDone = function (tween, targets, custom) {
        targets[0].destroy();
    };
    ColorBurst.preload = function (scene, path) {
        scene.load.spritesheet('effectColorStars', path, {
            frameWidth: 26,
            frameHeight: 26
        });
    };
    return ColorBurst;
}());
exports.ColorBurst = ColorBurst;
//# sourceMappingURL=colorBurst.js.map