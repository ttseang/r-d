export declare class Ripple {
    scene: Phaser.Scene;
    color: number;
    size: number;
    x: number;
    y: number;
    count: number;
    dist: number;
    duration: number;
    constructor(scene: Phaser.Scene, x?: number, y?: number, count?: number, size?: number, dist?: number, duration?: number, color?: number);
    start(): void;
    tweenDone(tween: any, targets: any, custom: any): void;
    getSpark(): Phaser.GameObjects.Graphics;
}
