"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Flare = void 0;
var Flare = /** @class */ (function (_super) {
    __extends(Flare, _super);
    function Flare(scene, gameWidth, x, y, tint) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (tint === void 0) { tint = 0xffffff; }
        var _this = _super.call(this, scene, x, y, "flare") || this;
        _this.scale = .2;
        _this.direction = -1;
        _this.duration = 1000;
        _this.tint = tint;
        _this.x = x;
        _this.y = y;
        _this.gameWidth = gameWidth;
        return _this;
    }
    Flare.prototype.start = function () {
        this.setTint(this.tint);
        var finalAngle = 270 * this.direction;
        this.displayWidth = this.scale * this.gameWidth;
        this.scaleY = this.scaleX;
        this.scene.add.existing(this);
        this.scene.tweens.add({
            targets: this,
            duration: this.duration,
            alpha: 0,
            angle: finalAngle,
            onComplete: this.tweenDone.bind(this)
        });
    };
    Flare.prototype.tweenDone = function () {
        this.destroy();
    };
    Flare.preload = function (scene, path) {
        scene.load.image('flare', path);
    };
    return Flare;
}(Phaser.GameObjects.Sprite));
exports.Flare = Flare;
//# sourceMappingURL=flare.js.map