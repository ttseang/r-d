import { IGameObj } from "../interfaces/IGameObj";
export declare class UIBlock {
    private _x;
    private _y;
    private _oldX;
    private _oldY;
    private _visible;
    private _displayWidth;
    private _displayHeight;
    children: any[];
    childIndex: number;
    isPosBlock: boolean;
    private _depth;
    private _alpha;
    constructor();
    set depth(val: number);
    get depth(): number;
    setChildDepth(uiChild: UIChild): void;
    set x(val: number);
    set y(val: number);
    get x(): number;
    get y(): number;
    add(obj: IGameObj): void;
    removeChild(uiChild: UIChild): void;
    buildList(): void;
    willRender(): void;
    get displayWidth(): number;
    get displayHeight(): number;
    setSize(w: number, h: number): void;
    setXY(x: number, y: number): void;
    set visible(val: boolean);
    get visible(): boolean;
    set alpha(val: number);
    get alpha(): number;
    updateChildAlpha(uiChild: UIChild, alpha: any): void;
    updateChildVisible(uiChild: UIChild, vis: any): void;
    updateChildPos(uiChild: UIChild): void;
    updatePositions(): void;
    getRelPos(uiChild: UIChild): {
        x: number;
        y: number;
    };
    getChildren(myArray: any, child: any): void;
    getAllChildren(): any[];
    getChildAt(index: number): any;
    setAngle(angle: number): void;
    destroy(): void;
}
export default UIBlock;
export declare class UIChild {
    child: IGameObj;
    nextChild: UIChild | undefined;
    childIndex: number;
    isPosBlock: boolean;
    depth: number;
    constructor(child: IGameObj);
}
