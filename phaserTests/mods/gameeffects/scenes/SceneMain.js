"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.SceneMain = void 0;
var colorBurst_1 = require("../classes/effects/colorBurst");
var flare_1 = require("../classes/effects/flare");
var sparks_1 = require("../classes/effects/sparks");
var starBurst_1 = require("../classes/effects/starBurst");
var BaseScene_1 = require("./BaseScene");
var SceneMain = /** @class */ (function (_super) {
    __extends(SceneMain, _super);
    function SceneMain() {
        return _super.call(this, "SceneMain") || this;
    }
    SceneMain.prototype.preload = function () {
        starBurst_1.StarBurst.preload(this, "./assets/effects/stars.png");
        colorBurst_1.ColorBurst.preload(this, "./assets/effects/colorStars.png");
        flare_1.Flare.preload(this, "./assets/effects/flare.png");
    };
    SceneMain.prototype.create = function () {
        _super.prototype.create.call(this);
        this.makeGrid(11, 11);
        /* let sb:StarBurst=new StarBurst(this,100,100);
        sb.start(); */
        /*  let cb:ColorBurst=new ColorBurst(this,100,100);
         cb.start(); */
        /* let fl:Flare=new Flare(this,100,100);
        fl.start(); */
        /*   let ripple:Ripple=new Ripple(this,100,100);
          ripple.start(); */
        var sparks = new sparks_1.Sparks(this, 100, 100);
        sparks.color = 0xff0000;
        sparks.start();
    };
    return SceneMain;
}(BaseScene_1.BaseScene));
exports.SceneMain = SceneMain;
//# sourceMappingURL=SceneMain.js.map