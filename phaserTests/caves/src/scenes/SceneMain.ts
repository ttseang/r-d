import { Button } from "../classes/comps/Button";
import { GM } from "../classes/GM";
import { Zoomer } from "../classes/Zoomer";
import { ZoomPosVo } from "../dataObjs/ZoomPosVo";
import { ZoomVo } from "../dataObjs/ZoomVo";
import Align from "../util/align";
import { FormUtil } from "../util/formUtil";

import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private formUtil: FormUtil;
    private zoomer: Zoomer;
    private btnNext: Button;

    private pageIndex: number = -1;
    private pages: ZoomVo[] = [];
    private initalZoomPos: ZoomPosVo;

    private gm: GM = GM.getInstance();

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("cave", "./assets/"+this.gm.mainImage+".jpg");
        this.load.image("circle", "./assets/circle.png");
        this.load.image("button", "./assets/ui/buttons/1/1.png");
        this.load.image("arrow", "./assets/arrow.png");
        this.load.image("ring", "./assets/ring.png");

        for (let i: number = 1; i < this.gm.pages.length+1; i++) {
            this.load.audio("audio" + i.toString(), "./assets/audio/"+this.gm.folder+"/" + i.toString() + ".wav");

        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.pages = this.gm.pages;

        //this.grid.showNumbers();

        this.formUtil = new FormUtil(this, 11, 11);
        this.formUtil.placeElementAt(17.5, 'dtext', false, false);
        this.setHTMLText("HELLO <span class='high'>There</span>!");

        this.zoomer = new Zoomer(this, "cave", 0.4);
        this.grid.placeAt(3, 5, this.zoomer);
        this.zoomer.init();

        window['zoomer'] = this.zoomer;

        this.input.keyboard.on("keydown", this.onKeyPress.bind(this));

        this.initalZoomPos = new ZoomPosVo(this.zoomer.image.x, this.zoomer.image.y, this.zoomer.image.scaleX);


        this.btnNext = new Button(this, "button", "NEXT");
        Align.scaleToGameW(this.btnNext, 0.2, this);
        this.grid.placeAt(7.5, 8, this.btnNext);

        //this.positions=[zoom0,zoom1,zoom2,zoom3];

        window['scene'] = this;
        this.showNextPage();

        this.btnNext.setCallback(this.showNextPage.bind(this));
        this.zoomer.zoomDone = this.zoomDone.bind(this);
    }
    zoomDone() {
        let zoomVo: ZoomVo = this.pages[this.pageIndex];
        if (zoomVo.callout != null) {
            this.zoomer.setCallout(zoomVo.callout);
        }
    }
    showNextPage() {
        this.btnNext.visible = false;
        this.pageIndex++;

        if (this.zoomer.calloutImage) {
            this.zoomer.calloutImage.destroy();
        }
        let zoomVo: ZoomVo = this.pages[this.pageIndex];
        this.setHTMLText(zoomVo.text);

        let indexString: string = (this.pageIndex + 1).toString();

        let audioKey: string = "audio" + indexString;
        let pageAudio: Phaser.Sound.BaseSound = this.sound.add(audioKey);

        pageAudio.on("complete", () => {
            if (this.pageIndex != this.pages.length - 1) {
                this.btnNext.visible = true;
            }

        });
        pageAudio.play();

        switch (zoomVo.type) {
            case "cut":
                this.zoomer.cutTo(zoomVo.zoomPosVo);
                break;

            case "zoom":
                this.zoomer.zoomTo(zoomVo.zoomPosVo);
                break;

            case "zoom2":

                this.zoomer.zoomTo(this.initalZoomPos);
                setTimeout(() => {
                    this.zoomer.zoomTo(zoomVo.zoomPosVo);
                }, 2000);

                break;
        }


    }

    setHTMLText(html: string) {
        console.log(html);
        document.getElementById('dtext').innerHTML = html;
    }
    //-1244,-792,.3

    onKeyPress(e: KeyboardEvent) {
        console.log(e.key);

        switch (e.key) {
            case "ArrowDown":
                this.zoomer.image.y++;
                break;

            case "ArrowUp":
                this.zoomer.image.y--;
                break;

            case "ArrowRight":
                this.zoomer.image.x++;

                break;

            case "ArrowLeft":
                this.zoomer.image.x--;
                break;
        }
    }
}