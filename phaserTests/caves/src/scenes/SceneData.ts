import { GM } from "../classes/GM";
import { CalloutVo } from "../dataObjs/CalloutVo";
import { ZoomPosVo } from "../dataObjs/ZoomPosVo";
import { ZoomVo } from "../dataObjs/ZoomVo";
import { BaseScene } from "./BaseScene";

export class SceneData extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneData");
    }
    create()
    {

        this.gm.folder="caves";
        this.gm.mainImage="cave";

        let jsonData:string="./assets/caveData.json";

        
       /*  this.gm.folder="rome";
        this.gm.mainImage="rome";

        let jsonData:string="./assets/romeData.json";  */

        fetch(jsonData)
        .then(response => response.json())
        .then(data => this.process({ data }));
    }
    process(data:any)
    {
        let pages:any=data.data.pages;
        let zpages:ZoomVo[]=[];

        for (let i:number=0;i<pages.length;i++)
        {
            let page:any=pages[i];
           // console.log(page);
            let pos:ZoomPosVo=new ZoomPosVo(parseFloat(page.x),parseFloat(page.y),parseFloat(page.zoom));
           // console.log(pos);

            let zoomVo:ZoomVo=new ZoomVo(page.type,page.text,pos);

            if (page['callout'])
            {
               // console.log("found callout");
                let callout:CalloutVo=new CalloutVo(parseFloat(page.callout.x),parseFloat(page.callout.y),page.callout.key,parseFloat(page.callout.scale),parseFloat(page.callout.angle),parseInt(page.callout.tint));

                zoomVo.callout=callout;
            }

           // console.log(zoomVo);
            zpages.push(zoomVo);



        }
        this.gm.pages=zpages;

        this.scene.start("SceneMain");
    }
}