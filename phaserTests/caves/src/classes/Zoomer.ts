import { GameObjects } from "phaser";
import { CalloutVo } from "../dataObjs/CalloutVo";
import { ZoomPosVo } from "../dataObjs/ZoomPosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class Zoomer extends GameObjects.Container
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    public image:GameObjects.Image;
    public calloutImage:GameObjects.Image;

    public circle:GameObjects.Image;
    public zoomDone:Function=()=>{};

    private lastX:number=0;
    private lastY:number=0;

    constructor(bscene:IBaseScene,imageKey:string,cScale:number)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.bscene=bscene;

        this.image=this.scene.add.image(0,0,imageKey).setOrigin(0,0);
        this.add(this.image);

        Align.scaleToGameW(this.image,1,this.bscene);


        this.circle=this.scene.add.image(0,0,"circle");
        Align.scaleToGameW(this.circle,cScale,this.bscene);

        //this.add(this.circle);

        this.image.x=-268;
        this.image.y=-275;

        this.image.mask=new Phaser.Display.Masks.BitmapMask(this.scene,this.circle);
       // this.image.visible=false;
       this.circle.visible=false;

        this.scene.add.existing(this);
    }
    setImageScale(ss:number)
    {
        this.image.setScale(ss,ss);
    }
    public setCallout(callout:CalloutVo)
    {        
        if (this.calloutImage)
        {
            this.calloutImage.destroy();
        }
        this.calloutImage=this.scene.add.image(0,0,callout.key);
        this.calloutImage.x=this.circle.x+this.circle.displayWidth*callout.x;
        this.calloutImage.y=this.circle.y+this.circle.displayHeight*callout.y;
        this.calloutImage.angle=callout.angle;
        this.calloutImage.setTint(callout.tint);
        console.log(callout.tint);
        Align.scaleToGameW(this.calloutImage,callout.scale,this.bscene);
       

    }
    public zoomTo(z:ZoomPosVo)
    {

        if (this.lastY==z.y && this.lastX==z.x)
        {
            this.zoomDone();
        }
        this.lastX=z.x;
        this.lastY=z.y;

        this.scene.tweens.add({targets:this.image,x:z.x,y:z.y,scaleX:z.zoom,scaleY:z.zoom,onComplete:()=>{
            this.zoomDone();
        }});
    }
    public cutTo(z:ZoomPosVo)
    {
        this.setImageScale(z.zoom);
        this.image.x=z.x;
        this.image.y=z.y;
    }
    init()
    {
        this.circle.x=this.x;
        this.circle.y=this.y;
        
    }
}