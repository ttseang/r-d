import { ZoomVo } from "../dataObjs/ZoomVo";

let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

    public pages:ZoomVo[]=[];
    public folder:string="caves";
    public mainImage:string="cave";
    
    constructor()
    {
        window['gm']=this;
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}