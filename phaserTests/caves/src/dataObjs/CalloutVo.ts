export class CalloutVo
{
    public x:number;
    public y:number;
    public key:string;
    public scale:number;
    public angle:number;
    public tint:number;
    constructor(x:number,y:number,key:string,scale:number,angle:number,tint:number)
    {
        this.x=x;
        this.y=y;
        this.key=key;
        this.scale=scale;
        this.angle=angle;
        this.tint=tint;
    }
}