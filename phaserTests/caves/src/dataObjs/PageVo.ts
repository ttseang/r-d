import { EffectParamVo } from "./EffectParamVo";
import { PopperVo } from "./PopperVo";
import TextVo from "./Textvo";

export class PageVo
{
    public image:string;
    public audio:string;
    public bgMusic:string;
    public title:string;
    public text:string;
    public effect:number;
    public effectParams:EffectParamVo=new EffectParamVo();
    public poppers:PopperVo[]=[];
    public subs:TextVo[]=[];
    
    constructor(image:string,audio:string,bgMusic:string,title:string,text:string,effect:number=1)
    {
        this.image=image;
        this.audio=audio;
        this.bgMusic=bgMusic;
        this.title=title;
        this.text=text;
        this.effect=effect;
    }
}