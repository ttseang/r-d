import { CalloutVo } from "./CalloutVo";
import { ZoomPosVo } from "./ZoomPosVo";

export class ZoomVo
{
    public type:string;
    public text:string;
    public zoomPosVo:ZoomPosVo;

    public callout:CalloutVo | null=null;
    
    constructor(type:string,text:string,zoomPosVo:ZoomPosVo)
    {
        this.type=type;
        this.text=text;
        this.zoomPosVo=zoomPosVo;
    }
}