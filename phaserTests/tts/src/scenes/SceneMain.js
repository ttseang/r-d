/*jshint esversion: 6 */
import Align from "../util/align";
import {AlignGrid} from "../util/alignGrid";
import { UIBlock } from "../util/UIBlock";
import {FormUtil} from "../util/formUtil";
import {FlatButton} from "../ui/flatButton";

export class SceneMain extends Phaser.Scene
{
    constructor()
    {
        super("SceneMain");
    }
    preload()
    {
       this.load.text("sample","../assets/sample.txt");
       this.load.image("buttonback","../assets/images/ui/buttons/1/1.png");
    }
    create()
    {
         //set the inital sentence index
         this.sentIndex=-1;

        //use the form utility to place html element over the phaser canvas
        //the uses the AlignGrid class so that we can place by percentages
        this.formUtil=new FormUtil({scene:this,rows:11,cols:11});
       // this.formUtil.showNumbers();

       //place the dropdown
        this.formUtil.placeElementAt(16,'voices',true,false);   
        
        //add a callback for when the drop down changes
        this.formUtil.addChangeCallback('voices',this.onVoiceChanged.bind(this),this);

        //add a listener for when the browser is ready to give us the voice list
        speechSynthesis.addEventListener("voiceschanged",this.getVoices.bind(this));
        
        //make a text object to hold the sentences
        this.dText=this.add.text(400,300,"TEXT",{color:"white",fontSize:'16px'});
        this.dText.setOrigin(0.5,0.5);
        this.dText.setWordWrapWidth(400);

       
        this.formUtil.alignGrid.placeAtIndex(60,this.dText);
        //
        //
        let instructions=this.add.text(0,0,"The voice will not change until a new sentence is spoken").setOrigin(0.5,0.5);
        this.formUtil.alignGrid.placeAtIndex(5,instructions);

        let button=new FlatButton({scene:this,key:"buttonback",text:"play",callback:this.startTalk.bind(this)});
        this.formUtil.alignGrid.placeAtIndex(82,button);
    }
    startTalk()
    {
        this.getNext();
    }
    getNext()
    {
        this.sentIndex++;
        if (this.sentIndex>this.sents.length)
        {
            this.dText.setText("Finished");
            return;
        }
        let sent=this.sents[this.sentIndex];
        this.dText.setText(sent);
        
        let talk=new SpeechSynthesisUtterance(sent);
        
        talk.voice=this.selectedVoice;
        talk.rate=1;
        talk.volume=20;
        talk.onend=this.getNext.bind(this);
        console.log(talk);
        speechSynthesis.speak(talk);
        console.log(speechSynthesis);
    }
   
    makeSents()
    {
        let sample=this.cache.text.get("sample");
        console.log(sample);
        this.sents=sample.match( /[^\.!\?]+[\.!\?]+/g );
        console.log(this.sents);
      //  this.getNext();
    }
    getVoices()
    {
         //get voices
       const voices=speechSynthesis.getVoices();
       for (let i=0;i<voices.length;i++)
       {           
        this.formUtil.addOption('voices',voices[i].name,voices[i]);
       }
       this.selectedVoice=voices[0];
       this.makeSents();
    }
    onVoiceChanged()
    {
        this.selectedVoice=this.formUtil.getSelectedItem('voices');        
    }
    update()
    {

    }
}
export default SceneMain;