import Align from "../util/align";
import {UIBlock} from "../util/UIBlock";

export class FlatButton extends UIBlock
{
    constructor(config)
    {
        super();
        this.scene=config.scene;
        const bg=this.scene.add.image(0,0,config.key);

        const textObj=this.scene.add.text(0,0,config.text,config.textStyle);
        textObj.setOrigin(0.5,0.5);

        this.add(bg);
        this.add(textObj);
        Align.scaleToGameW(bg,0.33,this.scene);

        if (config.callback)
        {
            bg.setInteractive();
            bg.on('pointerdown',()=>{config.callback()});
        }
    }
}