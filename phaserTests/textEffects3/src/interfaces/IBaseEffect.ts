export interface IBaseEffects
{
    start():void;
    stop():void;
    getCon():Phaser.GameObjects.Container;
}