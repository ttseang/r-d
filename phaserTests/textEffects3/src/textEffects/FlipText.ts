import { FlipTextVo } from "../dataObjs/FlipTextVo";

import IBaseScene from "../interfaces/IBaseScene";
import { BaseTextEffect } from "./BaseTextEffect";

export class FlipText extends BaseTextEffect {
    public scene: Phaser.Scene;   

    private dataProvider: FlipTextVo;
    private letters1:Phaser.GameObjects.Text[]=[];

    constructor(bscene: IBaseScene, dataProvider: FlipTextVo) {
        super(bscene);
       

        this.dataProvider = dataProvider;
      //  this.hstyle.fill=dataProvider.color;
        if (dataProvider.italics==true)
        {
         //   this.hstyle.fontFamily="BarthowheelIt";
        }     
       
    }
    flip()
    {
        this.letters1.forEach((letter)=>{
            letter.scaleX+=0.05;
        })
        if (this.letters1[0].scaleX>0.9)
        {
            this.letters1.forEach((letter)=>{
                letter.scaleX=1;
            })
            this.callback();
            this.timer1.remove();
        }
    }
    public build()
    {
        this.letters1=this.makeLetters(this.dataProvider.text,true);
       this.letters1.forEach((letter)=>{
           letter.scaleX=0;
       })
    }
    public start()
    {
        this.timer1=this.scene.time.addEvent({ delay: 100, callback: this.flip.bind(this), loop: true });
    }
    public removeMe()
    {

    }
}