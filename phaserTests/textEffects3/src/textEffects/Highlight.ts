import { HighlightVo } from "../dataObjs/HighlightVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseTextEffect } from "./BaseTextEffect";

export class Hightlight extends BaseTextEffect {
    public scene: Phaser.Scene;   

    private dataProvider: HighlightVo;
  
    constructor(bscene: IBaseScene, dataProvider: HighlightVo) {
        super(bscene);
       

        this.dataProvider = dataProvider;
      //  this.hstyle.fill=dataProvider.color;
        if (dataProvider.italics==true)
        {
         //   this.hstyle.fontFamily="BarthowheelIt";
        }     

        
    }
    public start()
    {
        if (this.dataProvider.delay==0)
        {
          this.makeHLetters(this.dataProvider.text,this.dataProvider.start,this.dataProvider.end);
        }
        else
        {
           this.makeLetters(this.dataProvider.text);
            setTimeout(()=>{
                this.removeAll(true);
                this.makeHLetters(this.dataProvider.text,this.dataProvider.start,this.dataProvider.end);
            },this.dataProvider.delay)
        }
    }
    public removeMe()
    {

    }
}