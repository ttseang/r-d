export class FlipTextVo
{
    
    public color:string;
    public text:string;
    public italics:boolean;
    public delay:number;

    constructor(text:string,color:string,italics:boolean=false,delay:number=0)
    {
        this.text=text;
       
        this.color=color;
        this.italics=italics;
        this.delay=delay;
    }
}