import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import UIBlock from "../util/UIBlock";

export class FlatButton extends UIBlock {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private textStyle: string;
    public onDown: Function;
    public onUp:Function=()=>{};
    private back:Phaser.GameObjects.Sprite;

    public text1: Phaser.GameObjects.Text
    public extraData:string="";
    public index:number=0;
    public tabGroup:number=0;
    
    constructor(bscene: IBaseScene, textStyle: string, key: string, text: string,callback:Function) {
      //  super(bscene.getScene());
        super();
        this.scene=bscene.getScene();
        this.back = this.scene.add.sprite(0, 0, key);
       // this.back.displayWidth=bscene.getW()*0.4;
     //   Align.scaleToGameW(this.back,0.3,bscene);
        this.add(this.back);
        
        this.text1 = this.scene.add.text(0, 0, text).setOrigin(0.5, 0.5);
        this.add(this.text1);

       // this.scene.add.existing(this);
        this.back.setInteractive();
        this.onDown=callback;

        
        this.setSize(this.back.displayWidth,this.back.displayHeight);
     //   this.setInteractive();
        this.back.on('pointerdown', this.clickMe.bind(this));

      
        /* if (this.text1.displayWidth>this.back.displayWidth)
        {
            console.log("resize "+this.text1.text);
        } */
        
    }
    /* setOnUp(callback:Function)
    {
        this.onUp=callback;
        this.back.on('pointerup', this.release.bind(this));
    } */
    clickMe() {
        console.log("click");
        window['back']=this.back;

        if (this.onDown)
        {
            this.onDown(this);
        }      
    }
    release()
    {
        if (this.onUp)
        {
            this.onUp();
        }
    }
    setText(text: string) {
        this.text1.setText(text);
    }
}