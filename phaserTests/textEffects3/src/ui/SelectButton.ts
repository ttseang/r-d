import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class SelectButton extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private textStyle: string;
    public onDown: Function;
    public onUp:Function=()=>{};
    private back:Phaser.GameObjects.Image;
    private back2:Phaser.GameObjects.Image;

    public text1: Phaser.GameObjects.Text
    public extraData:string="";
    public index:number=0;
    private _selected:boolean;

    constructor(bscene: IBaseScene, textStyle: string, normalBack: string,selectBack:string, text: string,callback:Function) {
        super(bscene.getScene());
        
        this.back = this.scene.add.image(0, 0, normalBack);
        this.back.displayWidth=bscene.getW()*0.3;
        this.add(this.back);

        this.back2 = this.scene.add.image(0, 0, selectBack);
        this.back2.displayWidth=bscene.getW()*0.3;
        this.add(this.back2);
        this.back2.visible=false;



        this.text1 = this.scene.add.text(0, 0, text).setOrigin(0.5, 0.5);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.scene.add.existing(this);
        this.setInteractive();
        
        this.onDown=callback;


        this.on('pointerdown', this.clickMe.bind(this));
        
    }
    get selected()
    {
        return this._selected;
    }
    set selected(val:boolean)
    {
      
        this.back.visible=!val;
        this.back2.visible=val;
        this._selected=val;
    }
    setOnUp(callback:Function)
    {
        this.onUp=callback;
        this.back.on('pointerup', this.release.bind(this));
    }
    clickMe() {
        if (this.onDown)
        {
            this.onDown(this);
        }      
    }
    release()
    {
        if (this.onUp)
        {
            this.onUp();
        }
    }
    setText(text: string) {
        this.text1.setText(text);
        
        while (this.text1.displayWidth>this.back.displayWidth)
        {
            console.log("resize "+this.text1.text);
            let fs:number=parseInt(this.text1.style.fontSize);
            fs--;
            let fs2:string=fs.toString()+"px";            
            console.log(fs);
            this.text1.setFontSize(fs);
            window['btn']=this;
        }
    }
}