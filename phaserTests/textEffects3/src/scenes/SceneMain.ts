import { FlipTextVo } from "../dataObjs/FlipTextVo";
import { IBaseEffects } from "../interfaces/IBaseEffect";
import { FlipText } from "../textEffects/FlipText";
import { FlatButton } from "../ui/FlatButton";
import Align from "../util/align";
import { FormUtil } from "../util/formUtil";
import UIBlock from "../util/UIBlock";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private t1: any = { color: "#ffffff", fontWeight: 'bold', fontFamily: 'BarthowheelReg', fontSize: '28px' };
    private t2: any = { color: "#5E7f57", fontWeight: 'bold', fontFamily: 'BarthowheelReg', fontSize: '28px' };
    private formUtil: FormUtil;
    private testText: Phaser.GameObjects.Text
    private buttonGroup:FlatButton[]=[];

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("button", "./assets/ui/buttons/1/1.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
       // this.grid.showPos();

        this.formUtil = new FormUtil(this, 11, 11);
        // this.formUtil.showNumbers();

        this.testText = this.add.text(0, 0, "TEXT HERE");
       // this.testText.visible = false;
        // this.grid.placeAtIndex(60,this.testText);

        setTimeout(this.setUp.bind(this), 1000);
        window['scene'] = this;
       // this.input.on("gameobjectdown",this.gameObjDown.bind(this));
    }
    gameObjDown(a,b)
    {
        console.log(a,b);
    }
    setUp() {
        //let testText2:Phaser.GameObjects.Text = this.add.text(200, 200, "TEST TEXT",this.t1);

        let buttonList: string[] = ['Fly', 'Flip', 'Grow', 'Spin', 'Spin&Grow', 'Shake','Curtains'];
        let xx: number =1;
        let yy: number = 6;
        for (let i: number = 0; i < buttonList.length; i++) {
            let flatButton: FlatButton = new FlatButton(this, "WHITE", "button", buttonList[i], this.buttonClicked.bind(this))
            flatButton.index = i;

            this.grid.placeAt(xx, yy, flatButton);
            xx+=4;
            if (xx >9) {
                yy += 2;
                xx = 1;
            }
            
            this.buttonGroup.push(flatButton);
           
        }

        this.formUtil.placeElementAt(23, 'htext', false, false);
        this.showButtons(false);
        this.flyToSpan(this.testText, 'ptext', 3000);
        //this.shakeAtSpan(this.testText, 'ptext', 1000);
        // this.curtainsAtSpan('ptext');
    }
    showButtons(val:boolean)
    {
        this.buttonGroup.forEach((button)=>{
            button.visible=val;
        })
    }
    buttonClicked(b: FlatButton) {
        this.showButtons(false);
        console.log(b.index);
        switch (b.index) {
            case 0:
                this.flyToSpan(this.testText, 'ptext', 3000);
                break;

            case 1:
                this.flipAtSpan(this.testText, 'ptext', 3000);
                break;

            case 2:
                this.growAtSpan(this.testText, 'ptext', 3000);
                break;
            case 3:
                this.spinAtSpan(this.testText, 'ptext', 3000);
                break;
            case 4:
                this.spinAndGrowAtSpan(this.testText, 'ptext', 3000);
                break;
            case 5:
                this.shakeAtSpan(this.testText, 'ptext', 1000);
                break;
            case 6:
                this.curtainsAtSpan('ptext');
                break;
        }
    }
    lineUpToHtml(t: Phaser.GameObjects.Text, el: string) {
        //get the element
        let element: HTMLElement = document.getElementById(el);

        //calculate the position of the element
        let pos: any = this.getElPos(element);

        //get the numbers from the position object
        let ex: number = pos.x;
        let ey: number = pos.y;

        console.log(pos);

        //get the computed styles
        let fs1: string = window.getComputedStyle(element, null).getPropertyValue('font-size');
        let ff: string = window.getComputedStyle(element, null).getPropertyValue('font-family');

        //convert font size to number
        let fs: number = parseInt(fs1);


        //make the phaser text match the html
        t.setFontFamily(ff);
        t.setFontSize(fs);
        t.setColor('#ff0000');

        //position the phaser text on top the html
        t.x = ex;
        t.y = ey;
        t.visible = true;
    }
    flyToSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        let ox: number = t.x;
        let oy: number = t.y;

        t.x = 0;
        t.y = 0;

        this.tweens.add({ targets: t, duration: secs, y: oy, x: ox, onComplete: () => { this.effectDone(t, el) } });

    }
    flipAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        t.scaleX = -1;
        t.scaleY = -1;
        this.tweens.add({ targets: t, duration: secs, scaleX: 1, scaleY: 1, onComplete: () => { this.effectDone(t, el) } });
    }

    growAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 1);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight;

        let oh: number = t.displayHeight;
        t.displayHeight = 0;
        // t.scaleX=-1;
        // t.scaleY=-0.5;
        this.tweens.add({ targets: t, duration: secs, displayHeight: oh, onComplete: () => { this.effectDone(t, el) } });
    }
    spinAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        t.setAngle(-360);
        this.tweens.add({ targets: t, duration: secs, angle: 360, onComplete: () => { this.effectDone(t, el) } });
    }
    spinAndGrowAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        t.scaleX = 0;
        t.scaleY = 0;
        t.setAngle(-360);
        this.tweens.add({ targets: t, duration: secs, angle: 360, scaleX: 1, scaleY: 1, onComplete: () => { this.effectDone(t, el) } });
    }
    shakeAtSpan(t: Phaser.GameObjects.Text, el: string, secs: number) {
        this.formUtil.blankElement(el);
        this.lineUpToHtml(t, el);
        t.setOrigin(0.5, 0.5);
        t.x += t.displayWidth / 2;
        t.y += t.displayHeight / 2;
        let angle: number = 20;

        t.setAngle(-angle);
        this.tweens.add({ targets: t, duration: secs, angle: angle, yoyo: true, repeat: 2, onComplete: () => { this.effectDone(t, el) } });
    }
    effectDone(t: Phaser.GameObjects.Text, el: string) {
        this.formUtil.unblankElement(el);
        t.visible = false;
        this.showButtons(true);
        t.setAngle(0);
        t.setScale(1,1);
    }
    curtainsAtSpan(el: string) {
        let element: HTMLElement = document.getElementById(el);
        //  console.log(element);
        this.formUtil.blankElement(el);
        //calculate the position of the element
        let pos: any = this.getElPos(element);
        //get the computed styles
        let fs1: string = window.getComputedStyle(element, null).getPropertyValue('font-size');
        let ff: string = window.getComputedStyle(element, null).getPropertyValue('font-family');
        //let ls:string = window.getComputedStyle(element, null).getPropertyValue('letter-spacing');
        //console.log(ls);

        //convert font size to number
        let fs: number = parseInt(fs1);

        let flipVo: FlipTextVo = new FlipTextVo("Text Here", "#0fffff", false, 0);
        let flipText: FlipText = new FlipText(this, flipVo);
        flipText.setFontSize(fs);
        flipText.setFont(ff);
        flipText.build();
        //  flipText.color1="#ffffff";

        flipText.x = pos.x;
        flipText.y = pos.y;

        // this.grid.placeAt(0,4,flipText);
        flipText.setCallback(() => { this.letterEffectDone(flipText, el) });
        flipText.start();
    }
    letterEffectDone(effect: IBaseEffects, el: string) {
        //  console.log("letter effect done");
        effect.getCon().removeAll(true);
        effect.getCon().destroy();
        this.formUtil.unblankElement(el);
        this.showButtons(true);
    }
    getElPos(el: any) {
        // yay readability
        for (var lx = 0, ly = 0;
            el != null;
            lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
        return { x: lx, y: ly };
    }
}