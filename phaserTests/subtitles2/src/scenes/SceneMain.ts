import { SubPlayer } from "../comps/subPlayer";
import ToggleButton from "../ui/toggleButton";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private player: SubPlayer;

    constructor() {
        super("SceneMain");
    }
    preload() {
        //load toggle button assets

        this.load.image("toggleback", "./assets/images/ui/toggles/1.png");
        this.load.image("sfxOn", "./assets/images/ui/icons/sfxOn.png");
        this.load.image("sfxOff", "./assets/images/ui/icons/sfxOff.png");

        //load the subtitle files

        //this.load.text("voice", "./assets/1_1_games_intro.srt");
        this.load.text("voice2", "./assets/1_1_games_intro.vtt");

        this.load.audio("intro", "./assets/1_1_introduction.wav");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        const toggle = new ToggleButton({
            scene: this,
            key: 'toggleback',
            onIcon: 'sfxOn',
            offIcon: 'sfxOff',
            isOn: false,
            callback: this.toggleSound.bind(this)
        });
        toggle.x = 400;
        toggle.y = 500;

        //make a subtitle player
        //parameters scene, subtitle file key, sound key, use VTT (true) or SRT(false)
        this.player = new SubPlayer(this, "voice2", "intro", true);
        this.player.x = 200;
        this.player.y = 200;
    }
    toggleSound(val:boolean) {
        if (val == true) {
            this.player.play();
        }
        else {
            this.player.pause();
        }
    }
}
