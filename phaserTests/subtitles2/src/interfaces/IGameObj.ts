/**
 * allows us to pass a sprite or image
 * as an IGameObj
 */
export interface IGameObj
{
    alpha:number;
    visible:boolean;
    displayWidth:number;
    displayHeight:number;
    scaleX:number;
    scaleY:number;
    x:number;
    y:number;
}