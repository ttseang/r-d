//import Phaser = require("phaser");
import Phaser from 'phaser';
import { SceneMain } from "./scenes/SceneMain";
import { GM } from "./classes/GM";

let gm: GM = GM.getInstance();

let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad = navigator.userAgent.indexOf("iPad");

if (isTablet != -1 || isIpad != -1) {
    gm.isTablet = true;
    isMobile = 1;
}

let w = 640;
let h = 640;
//
//
if (isMobile != -1) {
    gm.isMobile = true;

}
if (gm.isMobile == true || gm.isTablet) {
    w = window.innerWidth;
    h = window.innerHeight;
}
if (w < h) {
    gm.isPort = true;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    backgroundColor: 'ffffff',
    parent: 'draw-app',
    scene: [SceneMain]
};

new Phaser.Game(config);