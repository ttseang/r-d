
import IBaseScene from "../../../interfaces/IBaseScene";
import { SizeBlock } from "./SizeBlock";

export class SizeModal extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;

    private sizes: number[] = [];
    private colors: number[] = [0x95afc0, 0x535c68];

    constructor(bscene: IBaseScene, callback: Function) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;


        let k: number = 0;
        let size:number=5;
        let m:number=0;

        for (let i: number = 0; i < 4; i++) {
            for (let j: number = 0; j < 3; j++) {
                m++;
                let sizeBlock: SizeBlock = new SizeBlock(this.bscene, this.colors[k],size,m, callback)

                size+=8;

               /*  sizeBlock.displayWidth = this.bscene.getW() / 5;
                sizeBlock.displayHeight = this.bscene.getH() / 4; */

                //sizeBlock.setTint(this.colors[k]);


                sizeBlock.x = j * sizeBlock.displayWidth + sizeBlock.displayWidth / 2;
                sizeBlock.y = i * sizeBlock.displayHeight + sizeBlock.displayHeight / 2;

                k++;
                if (k > this.colors.length-1) {
                    k = 0;
                }

                this.add(sizeBlock);
            }
        }
        this.scene.add.existing(this);
    }
}
