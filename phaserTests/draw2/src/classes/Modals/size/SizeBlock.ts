import { GameObjects } from "phaser";
import IBaseScene from "../../../interfaces/IBaseScene";
import Align from "../../../util/align";


export class SizeBlock extends GameObjects.Container
{
    
    private callback:Function;
    private back:GameObjects.Image;
    private bscene:IBaseScene;
    private mySize:number;
    private index:number;

    constructor(bscene:IBaseScene,myColor:number,size:number,index:number,callback:Function)
    {
        super(bscene.getScene());

        this.bscene=bscene;
        this.mySize=size;
        this.callback=callback;
        this.index=index;

        this.back=this.scene.add.image(0,0,"holder");
        this.back.setTint(myColor);

        this.back.displayWidth = this.bscene.getW() / 3;
        this.back.displayHeight = this.bscene.getH() / 4;
        this.add(this.back);
       

        let circle:GameObjects.Image=this.scene.add.image(0,0,"circle");
        Align.scaleToGameW(circle,size/400,bscene);
        this.add(circle);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.setInteractive();
    

        this.on('pointerdown',this.pressMe.bind(this));

        bscene.getScene().add.existing(this);
    }
    pressMe()
    {
        console.log(this.mySize);
        this.callback(this.mySize,this.index);
    }
}