import { GameObjects } from "phaser";
import IBaseScene from "../../../interfaces/IBaseScene";

export class ColorBlock extends GameObjects.Image
{
    public myColor:number;
    private callback:Function;

    constructor(bscene:IBaseScene,myColor:number,callback:Function)
    {
        super(bscene.getScene(),0,0,"holder");

        this.myColor=myColor;
        this.callback=callback;

        this.setTint(myColor);
        this.setInteractive();

        this.on('pointerdown',this.pressMe.bind(this));

        bscene.getScene().add.existing(this);
    }
    pressMe()
    {
        console.log(this.myColor);
        this.callback(this.myColor);
    }
}