import { GameObjects } from "phaser";
import IBaseScene from "../../../interfaces/IBaseScene";
import Align from "../../../util/align";


export class ShapeBlock extends GameObjects.Container
{
    
    private callback:Function;
    private back:GameObjects.Image;
    private bscene:IBaseScene;
    private myShape:string;

    constructor(bscene:IBaseScene,myColor:number,myShape:string,callback:Function)
    {
        super(bscene.getScene());

        this.bscene=bscene;
        this.myShape=myShape;
        this.callback=callback;

        this.back=this.scene.add.image(0,0,"holder");
        this.back.setTint(myColor);

        this.back.displayWidth = this.bscene.getW() / 5;
        this.back.displayHeight = this.bscene.getH() / 4;
        this.add(this.back);
       

        let shape:GameObjects.Image=this.scene.add.image(0,0,myShape);
        Align.scaleToGameW(shape,0.18,bscene);
        this.add(shape);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.setInteractive();
    

        this.on('pointerdown',this.pressMe.bind(this));

        bscene.getScene().add.existing(this);
    }
    pressMe()
    {
        console.log(this.myShape);
        this.callback(this.myShape);
    }
}