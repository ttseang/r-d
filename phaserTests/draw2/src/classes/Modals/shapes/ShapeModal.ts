
import IBaseScene from "../../../interfaces/IBaseScene";
import { GM } from "../../GM";
import { ShapeBlock } from "./ShapeBlock";


export class ShapeModal extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;

    private sizes: number[] = [];
    private colors: number[] = [0x95afc0, 0x535c68];
    private gm:GM=GM.getInstance();

    constructor(bscene: IBaseScene, callback: Function) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;


        let k: number = 0;
        let xx:number=0;
        let yy:number=0;

        for (let i:number=0;i<this.gm.shapes.length;i++)
        {
            let shapeBlock: ShapeBlock = new ShapeBlock(this.bscene, 0x000000,this.gm.shapes[i], callback);

            shapeBlock.x=xx+shapeBlock.displayWidth/2;
            shapeBlock.y=yy+shapeBlock.displayHeight/2;

            this.add(shapeBlock);

            xx+=shapeBlock.displayWidth;
            if (xx>this.bscene.getW()-1)
            {
                xx=0;
                yy+=shapeBlock.displayHeight;
            }
        }
       
        this.scene.add.existing(this);
    }
}
