let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

    public shapes:string[]=["circle","line1","line2","star","heart","square","stop","triangle","flower","face","good6","good7","good8","good9"];

    constructor()
    {
        window['gm']=this;
        for (let i:number=1;i<10;i++)
        {
            this.shapes.push(i.toString());
        }
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}