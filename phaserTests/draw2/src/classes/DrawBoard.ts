import { GameObjects } from "phaser";
import { HistoryObj } from "../dataObjs/HistoryObj";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import Controller from "./Controller";

export class DrawBoard extends GameObjects.RenderTexture
{
    public past:HistoryObj[]=[];
    public future:HistoryObj[]=[];
    private bscene:IBaseScene;
    private controller:Controller=Controller.getInstance();

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene(),0,0,bscene.getW(),bscene.getH());
        this.bscene=bscene;
        this.scene.add.existing(this);

        this.controller.undo=this.undo.bind(this);
        this.controller.redo=this.redo.bind(this);
    }
    
    undo()
    {
        console.log("undo");

        if (this.past.length>0)
        {
            let h:HistoryObj=this.past.pop();
            this.future.push(h);
            this.drawHistory();
        }
    }
    redo()
    {
        if (this.future.length>0)
        {
            let h:HistoryObj=this.future.pop();
            this.past.push(h);
            this.drawHistory();
        }
    }
    drawHistory()
    {
        this.clear();
        for (let i:number=0;i<this.past.length;i++)
        {
            let h:HistoryObj=this.past[i];

            
            for (let j:number=0;j<h.pos.length;j++)
            {
                let posVo:PosVo=h.pos[j];
                this.drawImage(posVo.x,posVo.y,h.image,h.color,h.size);
            }
        }
    }
    drawImage(xx:number, yy:number, key:string,color:number,size:number)
    {
        let obj:GameObjects.Image=this.scene.make.image({key:key},false);
        obj.setTint(color);
        Align.scaleToGameW(obj,size,this.bscene);

        this.draw(obj,xx,yy,1);
    }
}