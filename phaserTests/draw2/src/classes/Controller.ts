export class Controller {
    private static instance: Controller;

    public undo: Function = () => { };
    public redo: Function = () => { };
    public changeColor: Function = () => { };
    public changeSize: Function = () => { };
    public changeImage: Function = () => { };
    public setStamp: Function = () => { };
    public setErase: Function = () => { };
    public setPen: Function = () => { };
    public clearCanvas: Function = () => { }
    public saveImage: Function = () => { };

    constructor() {

    }
    public static getInstance(): Controller {
        if (this.instance == null) {
            this.instance = new Controller();
        }
        return this.instance;
    }
    setImage(img: string) {
        let selectedImage: HTMLImageElement = document.getElementById("selectedImage") as HTMLImageElement;
        if (selectedImage) {
            selectedImage.src = "./assets/shapes/" + img + ".png";
        }
    }
    setColor(color: number) {
        let btnColor: HTMLButtonElement = document.getElementById("btnColor") as HTMLButtonElement;
        let hex: string = "#" + color.toString(16);

        btnColor.style.backgroundColor = hex;

        if (hex == "#ecf0f1") {
            btnColor.style.color = "#000000";
        }
        else {
            btnColor.style.color = "#ffffff";
        }
    }
    setSize(size: number) {
        let sizeDiv: HTMLElement = document.getElementById("mysize");
        sizeDiv.innerText = size.toString();
    }
    setMode(mode: string) {
        //#6c757d

        let btnPen: HTMLButtonElement = document.getElementById("btnPen") as HTMLButtonElement;
        let btnStamp: HTMLButtonElement = document.getElementById("btnStamp") as HTMLButtonElement;
        let btnErase: HTMLButtonElement = document.getElementById("btnErase") as HTMLButtonElement;

        btnPen.style.backgroundColor = "#6c757d";
        btnStamp.style.backgroundColor = "#6c757d";
        btnErase.style.backgroundColor = "#6c757d";

        switch (mode) {
            
            case "draw":
                btnPen.style.backgroundColor = "#2ecc71";
                break;

            case "stamp":
                btnStamp.style.backgroundColor = "#2ecc71";
                break;

            case "erase":
                btnErase.style.backgroundColor = "#2ecc71";
                break;
        }
    }
    setUp() {
        let btnUndo: HTMLButtonElement = document.getElementById("btnUndo") as HTMLButtonElement;
        let btnRedo: HTMLButtonElement = document.getElementById("btnRedo") as HTMLButtonElement;
        let btnColor: HTMLButtonElement = document.getElementById("btnColor") as HTMLButtonElement;
        let btnSize: HTMLButtonElement = document.getElementById("btnSize") as HTMLButtonElement;
        let btnImage: HTMLButtonElement = document.getElementById("btnImage") as HTMLButtonElement;

        let btnPen: HTMLButtonElement = document.getElementById("btnPen") as HTMLButtonElement;
        let btnStamp: HTMLButtonElement = document.getElementById("btnStamp") as HTMLButtonElement;
        let btnErase: HTMLButtonElement = document.getElementById("btnErase") as HTMLButtonElement;

        let btnClear: HTMLButtonElement = document.getElementById("btnClear") as HTMLButtonElement;
        let btnSave: HTMLButtonElement = document.getElementById("btnSave") as HTMLButtonElement;

        if (btnUndo) {
            btnUndo.onclick = () => {
                this.undo();
            }
        }
        if (btnRedo) {
            btnRedo.onclick = () => {
                this.redo();
            }
        }
        if (btnColor) {
            btnColor.onclick = () => {
                this.changeColor();
            }
        }
        if (btnSize) {
            btnSize.onclick = () => {
                this.changeSize();
            }
        }
        if (btnImage) {
            btnImage.onclick = () => {
                this.changeImage();
            }
        }
        if (btnPen) {
            btnPen.onclick = () => {
                this.setPen();
            }
        }
        if (btnStamp) {
            btnStamp.onclick = () => {
                this.setStamp();
            }
        }
        if (btnErase) {
            btnErase.onclick = () => {
                this.setErase();
            }
        }
        if (btnClear) {
            btnClear.onclick = () => {
                this.clearCanvas();
            }
        }
        if (btnSave) {
            btnSave.onclick = () => {
                this.saveImage();
            }
        }
    }
}
export default Controller;