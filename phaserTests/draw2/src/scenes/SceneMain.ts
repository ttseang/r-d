import { GameObjects } from "phaser";
import Controller from "../classes/Controller";
import { DrawBoard } from "../classes/DrawBoard";
import { GM } from "../classes/GM";
import { ColorModal } from "../classes/Modals/color/ColorModal";
import { ShapeModal } from "../classes/Modals/shapes/ShapeModal";

import { SizeModal } from "../classes/Modals/size/SizeModal";
import { HistoryObj } from "../dataObjs/HistoryObj";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";

import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private drawingBoard: DrawBoard;
    private obj: GameObjects.Image;

    private mode: string = "draw";

    private currentHistory: HistoryObj;

    private colorModal: ColorModal;
    private sizeModal: SizeModal;
    private shapeModal: ShapeModal;


    private controller: Controller = Controller.getInstance();

    private modalOpen: boolean = false;
    private changeFlag: boolean = false;


    private frameObj: GameObjects.Graphics;


    private key: string = "circle";
    private color: number = 0xff0000;
    private size: number = 0.052;
    private gm: GM = GM.getInstance();

    private dragIcon:GameObjects.Sprite;


    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");


        for (let i: number = 0; i < this.gm.shapes.length; i++) {
            this.load.image(this.gm.shapes[i], "./assets/shapes/" + this.gm.shapes[i] + ".png");
        }


        this.load.atlas("dragIcons", "./assets/dragIcons.png", "./assets/dragIcons.json");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.controller.setUp();

        this.drawingBoard = new DrawBoard(this);

        this.input.on("pointerdown", this.onDown.bind(this));
        this.input.on("pointermove", this.doDraw.bind(this));

        window['board'] = this.drawingBoard;
        window['scene'] = this;

        this.colorModal = new ColorModal(this, this.colorChanged.bind(this));
        this.colorModal.visible = false;

        this.sizeModal = new SizeModal(this, this.sizeChanged.bind(this));
        this.sizeModal.visible = false;

        this.shapeModal = new ShapeModal(this, this.shapeChanged.bind(this));
        this.shapeModal.visible = false;

        this.controller.changeColor = this.changeColor.bind(this);
        this.controller.changeSize = this.changeSize.bind(this);
        this.controller.changeImage = this.changeShape.bind(this);

        this.controller.setErase = this.setErase.bind(this);
        this.controller.setStamp = this.setStamp.bind(this);
        this.controller.setPen = this.setPen.bind(this);

        this.controller.clearCanvas = this.clearCanvas.bind(this);
        this.controller.saveImage = this.saveImage.bind(this);

        this.frameObj = this.add.graphics();
        this.frameObj.lineStyle(2, 0x00000);
        this.frameObj.strokeRect(0, 0, this.gw, this.gh);
        this.frameObj.stroke();


        this.controller.setColor(this.color);
        this.controller.setImage(this.key);
        this.controller.setSize(5);
        this.controller.setMode(this.mode);


        this.dragIcon=this.add.sprite(0,0,"dragIcons");
        Align.scaleToGameW(this.dragIcon,0.05,this);
        this.dragIcon.setOrigin(0,1);
        this.dragIcon.setFrame("pen.png");
    }

    exportCanvasAsPNG(fileName: string, dataUrl: string) {
        // var canvasElement = document.getElementById(id);
        var MIME_TYPE = "image/png";
        var imgURL = dataUrl;
        var dlLink = document.createElement('a');
        dlLink.download = fileName;
        dlLink.href = imgURL;
        dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');
        document.body.appendChild(dlLink);
        dlLink.click();
        document.body.removeChild(dlLink);
    }
    saveImage() {
        this.game.renderer.snapshot((image) => {
            let image2: HTMLImageElement = image as HTMLImageElement;
            // image2.width = 1600;
            // image2.height = 1600;
            let fileName: string = "image";
            this.exportCanvasAsPNG(fileName, image2.src);
        });
    }
    clearCanvas() {
        this.drawingBoard.clear();
        this.drawingBoard.past = [];
        this.drawingBoard.future = [];

    }
    setStamp() {
        this.mode = "stamp";
        this.dragIcon.setFrame("stamp.png");
        this.dragIcon.setOrigin(0.5,1);
        this.controller.setMode(this.mode);
    }
    setPen() {
        this.mode = "draw";
        this.dragIcon.setFrame("pen.png");
        this.dragIcon.setOrigin(0,1);
        this.controller.setMode(this.mode);
    }
    setErase() {
        this.mode = "erase";
        this.dragIcon.setOrigin(0.5,1);
        this.dragIcon.setFrame("erase.png");
        this.controller.setMode(this.mode);
    }
    allModalsOff() {
        this.colorModal.visible = false;
        this.shapeModal.visible = false;
        this.sizeModal.visible = false;
        this.modalOpen = false;
        this.dragIcon.visible=true;
    }
    changeSize() {
        if (this.sizeModal.visible==true)
        {
            this.allModalsOff();
            return;
        }
        this.allModalsOff();
        this.sizeModal.visible = true;
        this.modalOpen = true;
        this.dragIcon.visible=false;
    }
    changeShape() {
        if (this.shapeModal.visible==true)
        {
            this.allModalsOff();
            return;
        }
        this.allModalsOff();
        this.shapeModal.visible = true;
        this.modalOpen = true;
        this.dragIcon.visible=false;

    }
    sizeChanged(size: number,index:number) {
        console.log(size);
        this.size = Math.floor((size / 700)*1000)/1000;
        this.allModalsOff();
        this.changeFlag = true;
        this.controller.setSize(index);
    }
    shapeChanged(shape: string) {
        this.key = shape;
        this.allModalsOff();
        this.changeFlag = true;

        //
        //
        //
        this.controller.setImage(shape);

        
    }
    changeColor() {
        if (this.colorModal.visible==true)
        {
            this.allModalsOff();
            return;
        }
        this.allModalsOff();
        this.modalOpen = true;
        this.dragIcon.visible=false;
        this.colorModal.visible = true;
    }
    colorChanged(color: number) {
        this.color = color;
        this.allModalsOff();
        this.changeFlag = true;
        this.controller.setColor(color);
    }
    onDown(p: Phaser.Input.Pointer) {
        if (this.modalOpen == true) {
            console.log("return open");
            return;
        }
        if (this.changeFlag == true) {
            this.changeFlag = false;
            return;
        }
        this.drawingBoard.future = [];

        if (this.mode == "stamp") {
            this.drawingBoard.drawImage(p.x, p.y, this.key, this.color, this.size);

            this.currentHistory = new HistoryObj("stamp", this.key, this.color, this.size);
            this.currentHistory.pos.push(new PosVo(p.x, p.y));
            this.drawingBoard.past.push(this.currentHistory);
            //  this.input.once("pointerdown", this.onDown.bind(this));           
            return;
        }

        if (this.mode == "draw") {
            this.currentHistory = new HistoryObj("draw", this.key, this.color, this.size);
        }
        if (this.mode == "erase") {
            this.currentHistory = new HistoryObj("erase", "circle", 0xffffff, this.size);
        }

        this.input.once("pointerup", this.onUp.bind(this));
    }
    onUp() {
        if (this.modalOpen == true) {
            return;
        }
        this.drawingBoard.past.push(this.currentHistory);
        // this.input.once("pointerdown", this.onDown.bind(this));
    }
    doDraw(p: Phaser.Input.Pointer) {

        this.dragIcon.x=p.x;
        this.dragIcon.y=p.y;

        if (this.modalOpen == true) {
            return;
        }
        if (p.isDown) {
            if (this.mode == "draw") {
                this.drawingBoard.drawImage(p.x, p.y, this.key, this.color, this.size);
                if (this.currentHistory) {
                    this.currentHistory.pos.push(new PosVo(p.x, p.y));
                }
            }
            if (this.mode == "erase") {
                this.drawingBoard.drawImage(p.x, p.y, "circle", 0xffffff, this.size);
                if (this.currentHistory) {
                    this.currentHistory.pos.push(new PosVo(p.x, p.y));
                }
            }
        }
    }
}