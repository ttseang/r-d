import { PosVo } from "./PosVo";

export class HistoryObj
{
    public pos:PosVo[];
    public type:string;
    public image:string;
    public color:number;
    public size:number;

    constructor(type:string,image:string,color:number,size:number)
    {
        this.type=type;
        this.image=image;
        this.color=color;
        this.size=size;
        this.pos=[];
    }
}