export class MainStorage {
  private static instance: MainStorage;
  public selectedGame:number;
  constructor() {
    this.selectedGame=0;
  }

  public static getInstance(): MainStorage {
    if (this.instance === undefined || this.instance === null) {
      this.instance = new MainStorage();
    }
    return this.instance;
  }  
}
export default MainStorage;
