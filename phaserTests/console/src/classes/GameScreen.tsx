import React, { Component, Ref } from "react";
import { Button } from "react-bootstrap";
import MainStorage from "./MainStorage";
interface MyProps {}
interface MyState {}
class GameScreen extends Component<MyProps, MyState> {
  private modules: string[] = ["dragon", "ninja"];
  private ms: MainStorage = MainStorage.getInstance();
  private gameFrameRef:React.RefObject<HTMLIFrameElement> = React.createRef();

  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  setSize()
  {   
     let h:number=this.gameFrameRef.current?.contentDocument?.body.offsetHeight || 0;    
    // let w:number=this.gameFrameRef.current?.contentDocument?.body.offsetWidth || 0;

     this.gameFrameRef.current?.setAttribute('height',h.toString());
    // this.gameFrameRef.current?.setAttribute('width',w.toString());
    
  }
  render() {
    let path: string =
      "modules/" + this.modules[this.ms.selectedGame] + "/index.html";
      console.log(path);
  //  let d:HTMLElement=document.getElementById("main");
   
    return (
      <div>          
        <iframe ref={this.gameFrameRef} id="gameframe" width="100%"  title="game" onLoad={this.setSize.bind(this)} src={path} scrolling="no"></iframe>
      </div>
    );
  }
}
export default GameScreen;
