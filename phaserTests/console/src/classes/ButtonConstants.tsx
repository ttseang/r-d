import { ButtonVo } from "../dataobjs/ButtonVo";

export class ButtonConstants {
  static NAV_BUTTONS: ButtonVo[] = [
    new ButtonVo("Spine", "primary", 0),
    new ButtonVo("Ninja", "primary", 1)    
  ];  
}