import React, { Component } from "react";
import { Button, Card } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import SelectScreen from "./SelectScreen";
import MainStorage from "../classes/MainStorage";
import GameScreen from "../classes/GameScreen";

interface MyProps {}
interface MyState {
  mode: number;
}
class BaseScreen extends Component<MyProps, MyState> {
  private ms:MainStorage=MainStorage.getInstance();

  constructor(props: MyProps) {
    super(props);
    this.state = { mode: 0 };
  }
  getScreen() {
    switch (this.state.mode) {
      case 0:
        return <SelectScreen callback={this.loadGame.bind(this)}></SelectScreen>;
      case 1:
        return <GameScreen></GameScreen>
    }
  }
  goHome()
  {
    this.setState({mode:0});
  }
  loadGame(game:number)
  {
     this.ms.selectedGame=game;
     this.setState({mode:1});
  }
  render() {
    return (
      <div id="base">
        <Card>
          <Card.Header id="top"><Button id="btnHome" variant="success" onClick={this.goHome.bind(this)}>Home</Button>Software Name</Card.Header>
          <Card.Body id="main">{this.getScreen()}</Card.Body>
        </Card>
      </div>
    );
  }
}
export default BaseScreen;
