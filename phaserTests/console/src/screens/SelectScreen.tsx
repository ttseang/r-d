import React, { Component } from "react";
import { ButtonConstants } from "../classes/ButtonConstants";
import ButtonLine from "../comps/ui/ButtonLine";
interface MyProps {
  callback: Function;
}
interface MyState {}
class SelectScreen extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);
    this.state = {};
  }
  onClick(index: number, action: number) {
    this.props.callback(action);
  }
  render() {
    return (
      <div>
        <ButtonLine
          buttonArray={ButtonConstants.NAV_BUTTONS}
          actionCallback={this.onClick.bind(this)}
        ></ButtonLine>
      </div>
    );
  }
}
export default SelectScreen;
