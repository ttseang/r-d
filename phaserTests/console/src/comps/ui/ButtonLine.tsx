import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { ButtonVo } from "../../dataobjs/ButtonVo";

interface MyProps {buttonArray:ButtonVo[],actionCallback:Function}
interface MyState {
}
class ButtonLine extends Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getButtons() {
        let blist: JSX.Element[] = [];
        for (let i: number = 0; i < this.props.buttonArray.length; i++) {
          let text: string = this.props.buttonArray[i].text;
          let variant: string = this.props.buttonArray[i].variant;
          let action: number = this.props.buttonArray[i].action;
          //
          //
          let key: string = "bbutton" + i.toString();
          blist.push(
            <Button
              className="buttonPad2"
              key={key}
              variant={variant}
              onClick={() => {
                this.props.actionCallback(i,action);
              }}
            >
              {text}
            </Button>
          );
        }
        return <div>{blist}</div>;
      }
    render() {
        return (
           <div>{this.getButtons()}</div> 
        );
    }
}
export default ButtonLine;