import { AnswerVo } from "../dataObjs/AnswerVo";
import { MQuizVo } from "../dataObjs/MQuizVo";
import { QuestVo } from "../dataObjs/QuestVo";
import IBaseScene from "../interfaces/IBaseScene";

export class QuizLoader {
    private scene: Phaser.Scene;
    constructor(bscene: IBaseScene,) {
        this.scene = bscene.getScene();
    }
    public loadQuiz(cacheKey: string) {

        let qData: any = this.scene.cache.json.get(cacheKey);

       // console.log(qData);

        let questions: any = qData.quiz;

        let mQuiz: MQuizVo = new MQuizVo("test");

        for (let i: number = 0; i < qData.length; i++) {
            let question: any = qData[i];
         //   console.log(question);


            let qtext: string = question.q;
            let preface: string = question.p;
            let shuffle: boolean = (parseInt(question.shuffle) == 1) ? true : false;
            let tabbed: boolean = (parseInt(question.tab) == 1) ? true : false;
            let multiselect: boolean = (parseInt(question.m) == 1) ? true : false;
            let image: string = question.image;

            

             let questVo: QuestVo = new QuestVo(qtext, preface, image, multiselect, shuffle, tabbed);
           
            let choices: any = question.c;
            for (let j: number = 0; j < choices.length; j++) {
                let choice: any = choices[j];
                //  console.log(choice);

                let correct: boolean = false;

                if (choice.c) {
                    if (choice.c == 1) {
                        correct = true;
                    }
                }

                let answerVo: AnswerVo = new AnswerVo(choice.t, correct);
                questVo.addChoice(answerVo);
            } 
//console.log(questVo);
            mQuiz.addQuestion(questVo);
        }
        //  console.log(mQuiz);

        return mQuiz;
    }
}