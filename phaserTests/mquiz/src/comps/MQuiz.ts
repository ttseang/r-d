import { AnswerVo } from "../dataObjs/AnswerVo";
import { MQuizVo } from "../dataObjs/MQuizVo";
import { PosVo } from "../dataObjs/PosVo";
import { QuestVo } from "../dataObjs/QuestVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "../scenes/BaseScene";
import { FlatButton } from "../ui/FlatButton";
import { SelectButton } from "../ui/SelectButton";
import { TabButton } from "../ui/TabButton";
import { TextStyles } from "../ui/TextStyles";
import { AlignGrid } from "../util/alignGrid";

export class MQuiz {
    public scene: Phaser.Scene;
    public quizData: MQuizVo;
    private grid: AlignGrid;

    private preText: Phaser.GameObjects.Text;
    private qtext: Phaser.GameObjects.Text;
    private bscene: IBaseScene;
    private currentQuestion: QuestVo;
    private questIndex: number = -1;
    private buttonArray: SelectButton[] = [];
    private tabArray: TabButton[] = [];

    private tabBack: Phaser.GameObjects.Image;
    private btnOk: FlatButton;
    private selectedAnswers: number[] = [];
    private tabPages: Phaser.GameObjects.Text[] = [];


    private tabColors: number[] = [0x95a5a6, 0x8e44ad, 0x8c7ae6, 0x9b59b6, 0xe74c3c, 0x34495e];
    constructor(bscene: IBaseScene, quizData: MQuizVo) {
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.quizData = quizData;
        this.grid = bscene.getGrid();
        console.log(quizData);
        //  this.grid.showPos();

        this.preText = this.placeText(5, 1, "preface", "PREFACE");
        this.qtext = this.placeText(5, 2, "question", "QUIZ_TEXT2");
        // this.makeTabs();
        this.nextQuestion();

        this.btnOk = new FlatButton(this.bscene, "", "toggle", "OK", this.enterAnswer.bind(this));
        this.grid.placeAt(9, 9, this.btnOk);
    }

    placeText(xx: number, yy: number, text: string, textStyle: string) {

        let ts: TextStyles = TextStyles.getInstance(this.bscene.getW());
        let style: any = ts.getStyle(textStyle);

        let textObj: Phaser.GameObjects.Text = this.scene.add.text(0, 0, text, style.style).setOrigin(0.5, 0.5);

        this.grid.placeAt(xx, yy, textObj);

        return textObj;
    }
    enterAnswer() {
        console.log(this.selectedAnswers);

        if (this.selectedAnswers.length == 0) {
            this.showMessage("Please select an answer");
            return;
        }

        let correct: number[] = this.getCorrectAnswers();
        console.log(correct);


        let right: boolean = true;
        if (this.selectedAnswers.length != correct.length) {
            console.log("Wrong");
            right = false;
        }
        else {

            let answers: number[] = this.selectedAnswers.slice();
            while (answers.length > 0) {
                let answer: number = answers.pop();
                if (!correct.includes(answer)) {
                    right = false;
                }
            }
        }
        console.log("RIGHT=" + right);
        if (right == false) {
            this.showCorrectAnswers();
            this.playSound("wrong");
        }
        else {
            this.playSound("right");
            setTimeout(this.nextQuestion.bind(this), 1000);
        }

    }
    showMessage(msg: string) {
        this.btnOk.visible = false;
        this.preText.visible = false;
        this.qtext.setText(msg);
        // this.showButtons(false);
        /*  if (this.tabBack)
         {
             this.tabBack.visible=false;
         } */
        setTimeout(this.resetMessage.bind(this), 1000);
    }
    showCorrectAnswers() {
        let correct: number[] = this.getCorrectAnswers();
        if (this.currentQuestion.tabs == true) {
            this.showTabGroup(correct[0]);
            // return;
        }
        else {
            this.resetAllButtons();


            for (let i: number = 0; i < correct.length; i++) {
                let button: SelectButton = this.buttonArray[correct[i]];

                button.selected = true;
            }
        }
        setTimeout(this.nextQuestion.bind(this), 1000);
    }
    resetMessage() {
        /*  if (this.currentQuestion.tabs == true) {
             this.tabBack.visible=true;
         } */
        this.btnOk.visible = true;
        this.preText.visible = true;
        this.qtext.setText(this.currentQuestion.question);
        //  this.showButtons(true);
    }
    getCorrectAnswers() {
        let correct: number[] = [];
        for (let i: number = 0; i < this.currentQuestion.answers.length; i++) {
            let answer: AnswerVo = this.currentQuestion.answers[i];
            if (answer.correct == true) {
                correct.push(i);
            }
        }
        return correct;
    }
    nextQuestion() {
        this.selectedAnswers = [];
        this.questIndex++;
        if (this.questIndex == this.quizData.questions.length) {
            console.log("quiz done");
            return;
        }
        this.currentQuestion = this.quizData.questions[this.questIndex];
        console.log(this.currentQuestion);
        this.preText.setText(this.currentQuestion.preface);
        this.qtext.setText(this.currentQuestion.question);
        
        if (this.currentQuestion.tabs == true) {
            this.makeTabs();
            this.showTabGroup(0);
        }
        else {
            this.makeButtons();
        }
    }
    destroyElements() {
        while (this.buttonArray.length > 0) {
            this.buttonArray.pop().destroy();
        }
        while (this.tabPages.length > 0) {
            this.tabPages.pop().destroy();
        }
        while (this.tabArray.length > 0) {
            this.tabArray.pop().destroy();
        }
        if (this.tabBack)
        {
            this.tabBack.visible=false;
        }
      
    }
    makeButtons() {
        this.destroyElements();

        let posArray: PosVo[] = [{ x: 3, y: 5.5 }, { x: 3, y: 6.5 }, { x: 3, y: 7.5 }, { x: 7, y: 5.5 }, { x: 7, y: 6.5 }, { x: 7, y: 7.5 }];
     
        if (this.currentQuestion.answers.length==2)
        {
            posArray=[{ x: 5, y: 5.5 }, { x: 5, y: 7.5 }];
        }

        if (this.currentQuestion.answers.length==4)
        {
            posArray=[{ x: 5, y: 4.5 }, { x: 5, y: 5.5 },{ x: 5, y: 6.5 }, { x:5, y: 7.5 }];
        }
     
        let choices: AnswerVo[] = this.currentQuestion.answers;
        console.log(choices);
        let posIndex: number = -1;
        let tabIndex: number = 0;
        for (let i: number = 0; i < this.currentQuestion.answers.length; i++) {
            posIndex++;
            tabIndex++;
            /*  if (posIndex > posArray.length - 1) {
                 posIndex = 0;
                 tabIndex++;
             } */
            let btn = new SelectButton(this.bscene, "", "button1", "button2", "pressMe", this.select.bind(this));
            btn.index = i;
            //  btn.tabIndex = tabIndex;
            this.buttonArray.push(btn);
            btn.setText(choices[i].text);
            this.grid.placeAt(posArray[posIndex].x, posArray[posIndex].y, btn);
        }
    }
    makeTabs() {
        this.destroyElements();
        this.tabBack = this.scene.add.image(0, 0, 'holder').setOrigin(0, 0);
        let choices: AnswerVo[] = this.currentQuestion.answers;
        let tabNames: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
        for (let i: number = 0; i < choices.length; i++) {
            let tabButton: TabButton = new TabButton(this.bscene, "", "holder", tabNames[i], this.tabPressed.bind(this), this.tabColors[i]);
            tabButton.tabIndex = i;
            this.tabArray.push(tabButton);

            let page: Phaser.GameObjects.Text = this.placeText(1, 5, choices[i].text, "TABBED").setOrigin(0, 0);
            this.tabPages.push(page);
            this.grid.placeAt(1 + i * 2, 4, tabButton);
        }

        this.grid.placeAt(0, 4.3, this.tabBack);
        // this.tabBack.x=0;
        this.tabBack.displayWidth = this.bscene.getW() * 0.9;
        // this.tabBack.alpha = 0.5;
        this.tabBack.displayHeight = this.bscene.getH() - this.tabBack.y;
        this.tabBack.setTint(this.tabColors[0]);
    }
    resetAllButtons() {
        for (let i: number = 0; i < this.buttonArray.length; i++) {
            let button: SelectButton = this.buttonArray[i];
            button.selected = false;
        }
    }

    showButtons(val: boolean) {
        for (let i: number = 0; i < this.buttonArray.length; i++) {
            let button: SelectButton = this.buttonArray[i];
            button.visible = val;
        }
    }
    showTabs(val: boolean) {
        for (let i: number = 0; i < this.tabArray.length; i++) {
            let tab: TabButton = this.tabArray[i];
            tab.visible = val;
        }
    }
    tabPressed(tabButton: TabButton) {
        this.showTabGroup(tabButton.tabIndex);

    }
    showTabGroup(index: number) {
        this.selectedAnswers = [index];
        this.tabBack.setTint(this.tabColors[index]);
        console.log(index);
        for (let i: number = 0; i < this.tabPages.length; i++) {
            this.tabPages[i].visible = false;
        }
        this.tabPages[index].visible = true;

        this.scene.children.sendToBack(this.tabBack);
    }
    select(b: SelectButton) {
        if (this.currentQuestion.multi === false) {
            this.resetAllButtons();
            b.selected = true;
            this.selectedAnswers = [b.index];
        }
        else {
            b.selected = !b.selected;

            if (this.selectedAnswers.includes(b.index)) {
                let pos: number = this.selectedAnswers.indexOf(b.index, 0);
                this.selectedAnswers.splice(pos, 1);
            }

            if (b.selected == true) {
                this.selectedAnswers.push(b.index);
            }
        }
        this.playSound("select");
    }
    playSound(skey: string) {
        let sound: Phaser.Sound.BaseSound = this.scene.sound.add(skey);
        sound.play();
    }
}