import { AnswerVo } from "./AnswerVo";

export class QuestVo {
    public question: string;
    public answers: AnswerVo[] = [];
    public preface: string;
    public image: string;
    public multi: boolean;
    public shuffle: boolean;
    public tabs: boolean

    constructor(question: string, preface: string, image: string, multi: boolean, shuffle: boolean, tabs: boolean) {
        this.question = question;
        this.preface = preface;
        this.image = image;
        this.multi = multi;
        this.shuffle = shuffle;
        this.tabs = tabs;
    }

    addChoice(choice: AnswerVo) {
        this.answers.push(choice);
    }
}