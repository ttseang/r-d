import { QuestVo } from "./QuestVo";

export class MQuizVo
{
    public quizName:string;
    public questions:QuestVo[]=[];

    constructor(quizName:string)
    {
        this.quizName=quizName;
    }
    public addQuestion(q:QuestVo)
    {
        this.questions.push(q);
    }
}