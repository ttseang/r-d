export class AnswerVo
{
    public correct:boolean;
    public text:string;
    constructor(text:string,correct:boolean)
    {
        this.text=text;
        this.correct=correct;
    }
}