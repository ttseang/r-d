import { MQuiz } from "../comps/MQuiz";
import { AnswerVo } from "../dataObjs/AnswerVo";
import { MQuizVo } from "../dataObjs/MQuizVo";
import { QuestVo } from "../dataObjs/QuestVo";
import { FlatButton } from "../ui/FlatButton";
import { SelectButton } from "../ui/SelectButton";
import { TabButton } from "../ui/TabButton";
import Align from "../util/align";
import { QuizLoader } from "../util/quizLoader";
import { BaseScene } from "./BaseScene";

export class SceneMQuiz extends BaseScene {
    constructor() {
        super("SceneMQuiz");
    }
    preload() {
        this.load.image("button1", "./assets/ui/buttons/1/2.png");
        this.load.image("button2", "./assets/ui/buttons/1/6.png");
        this.load.json("quizData","./assets/data/mcquestions.json");
        this.load.image("holder","./assets/holder.jpg");
        this.load.image("toggle","./assets/ui/toggles/1.png");
        
        this.load.audio("select","./assets/audio/quiz_select.wav");
        this.load.audio("right","./assets/audio/quiz_right.mp3");
        this.load.audio("wrong","./assets/audio/quiz_wrong.wav");
    }   
    create() {
        super.create();
        this.makeGrid(11,11);

        let quizLoader:QuizLoader=new QuizLoader(this);
        let quizData:MQuizVo=quizLoader.loadQuiz("quizData");
        let quiz:MQuiz=new MQuiz(this,quizData);
         window['quiz']=quiz;
     //    this.grid.showPos();
      //  this.grid.showPos();
      //  let tabButton:TabButton=new TabButton(this,"","holder","A",this.onPress.bind(this),0x3498db);
       // let btnOk:FlatButton=new FlatButton(this,"","toggle","OK",this.onPress.bind(this));
      //  let btnTest=new SelectButton(this,"","button1","button2","pressMe",this.selected.bind(this));
     //  this.grid.placeAt(5,5,tabButton);
    }
    onPress()
    {

    }
  /*   selected(b:SelectButton)
    {
        console.log(b);
        b.selected=!b.selected;

    } */
}