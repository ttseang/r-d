

export class SceneFonts extends Phaser.Scene {
    constructor() {
        super("SceneFonts");
    }
    preload() {        
        this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js');

    }
    create() {
        this.fontsLoaded = 0;
        this.fonts = ['Palanquin Dark'];
        WebFont.load({ google: { families: this.fonts }, loading: this.fontsLoading.bind(this), fontactive: this.fontActive.bind(this) });
    }
    fontsLoading() {
        console.log("font started");
    }
    fontActive() {
        console.log("font active");
        this.fontsLoaded++;
        if (this.fontsLoaded == this.fonts.length) {
            this.scene.start("SceneMain");
        }
    }
}