import { DragWord } from "../comps/dragWord";
import { AlignGrid } from "../util/alignGrid";

export class SceneDrag extends Phaser.Scene {
    constructor() {
        super("SceneDrag");
        this.words = ["One", "Blue", "Fish", "Green"];

    }
    preload() {
        this.load.image("holder", "./assets/images/holder.jpg");
    }
    create() {
        this.gw = this.sys.game.config.width;
        this.gh = this.sys.game.config.height;

        window.scene = this;

        this.grid = new AlignGrid({ scene: this, rows: 11, cols: 22 });
        this.makeBlocks();
    }
    makeBlocks() {
        for (let i = 0; i < this.words.length; i++) {
            let block = new DragWord({ scene: this, word: this.words[i], callback: this.selectBlock.bind(this) });
            this.grid.placeAtIndex(50 + i * 5, block);
        }
    }
    selectBlock(block) {
        this.selectedBlock = block;
        block.alpha=0.5;
        this.input.on('pointermove',this.doDrag.bind(this));
        this.input.once('pointerup',this.stopDrag.bind(this));
    }
    doDrag()
    {
       
        this.selectedBlock.x=this.input.activePointer.x;
        this.selectedBlock.y=this.input.activePointer.y;
    }
    stopDrag()
    {
        this.selectedBlock.alpha=1;
        this.input.off('pointermove');
    }
}