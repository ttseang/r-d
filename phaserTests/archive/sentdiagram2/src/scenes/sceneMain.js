/*jshint esversion: 6 */
import { WordBox } from "../comps/wordBox";
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
//import { UIBlock } from "../util/UIBlock";
import { DragWord } from "../comps/dragWord";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/images/holder.jpg");
        this.load.json("allsents", "./assets/data/sample.json");
    }
    create() {

        this.clickLock = false;

        this.gw = this.sys.game.config.width;
        this.gh = this.sys.game.config.height;
        this.topBoxes = [];
        this.allBoxes = [];
        this.words = [];
        this.dropTargets = [];

        //make a grid        
        this.grid = new AlignGrid({ scene: this, rows: 11, cols: 22 });
        //  this.grid.showNumbers();

        window.scene = this;



        const allsents = this.cache.json.get('allsents');
        //let r=Math.floor(Math.random()*allsents.length);
        this.sentdata = allsents[3];


        this.drawTree();
        //  this.allBoxes.forEach((box)=>{box.hideWord();});
        // this.makeBlocks();
    }
    drawTree() {

        let len = this.sentdata.diagram.length;

        let totalW = 0;
        for (let i = 0; i < len; i++) {
            let data = this.sentdata.diagram[i];
            console.log(data);

            let box = this.makeBox(data.w);
            this.topBoxes.push(box);
            let modCount = 0;
            if (data.mod) {
                modCount = data.mod.length;
            }
            //size the box based on mods or word size
            //whichever makes the longer box       
            box.sizeBox(modCount);
            box.drawLines(data.l);
            totalW += box.displayWidth;


        }

        this.lineUpBoxes(totalW);
        this.drawMods();
    }
    lineUpBoxes(totalW) {

        //  let left=100;
        let left = this.gw / 2 - totalW / 2;
        let top = this.gh * 0.25;

        let xx = left;
        let len = this.topBoxes.length;
        // len=2;
        for (let i = 0; i < len; i++) {
            let box = this.topBoxes[i];

            box.x = xx;
            box.y = top;
            xx += box.displayWidth;

        }
    }
    drawMods() {
        let len = this.sentdata.diagram.length;

        for (let i = 0; i < len; i++) {
            let data = this.sentdata.diagram[i];
            console.log(data);

            let box = this.topBoxes[i];


            if (data.mod) {
                if (data.mod.length > 0) {
                    this.drawMod(box, data.mod);
                }
            }
        }
    }
    drawMod(parentBox, mods) {
        console.log(mods);

        let len = mods.length;
        let start = parentBox.x + parentBox.displayWidth / 2;
        start -= Align.getXPer(0.04, this) * len;

        for (let i = 0; i < len; i++) {
            let mod = mods[i];
            console.log(mod);

            let modBox = this.makeBox(mod.w, 0.15, 0.5);
            modBox.parentBox=parentBox;
            modBox.sizeBox(1);

            if (mod.l == "d") {
                modBox.setAngle(45);
            }
            modBox.drawLines(mod.l);          
            modBox.isChild=true;

            modBox.y = parentBox.getBottom();
            modBox.x = start + i * Align.getXPer(0.08, this) + modBox.displayHeight;


            if (modBox.parentBox)
            {
                if (modBox.parentBox.drawIndex=="d")
                {
                    modBox.x = modBox.parentBox.x+Align.getXPer(0.015,this);
                    modBox.y = parentBox.getBottom()+Align.getYPer(0.01,this);
                }
                if (modBox.parentBox.drawIndex=="h" && modBox.parentBox.isChild==true)
                {
                  //  modBox.x=modBox.parentBox.x+i * Align.getXPer(0.08, this) + modBox.displayHeight;
                    modBox.y=parentBox.getBottom()-Align.getYPer(0.03,this);
                }
            }
            if (mod.mod) {
                if (mod.mod.length > 0) {
                    this.drawMod(modBox, mod.mod);
                }
            }
        }
    }
    makeBox(word, scale = 0.4, textScale = 1) {
        let wordBox = new WordBox({ scene: this, word: word, scale: scale, textScale: textScale });
        this.allBoxes.push(wordBox);
        if (word != undefined) {
            this.words.push(word);
            this.dropTargets.push(wordBox);
        }
        return wordBox;
    }
    makeBlocks() {
        for (let i = 0; i < this.words.length; i++) {
            let block = new DragWord({ scene: this, word: this.words[i], callback: this.selectBlock.bind(this) });
            this.grid.placeAtIndex(155 + i * 4, block);
            block.ox = block.x;
            block.oy = block.y;

        }
    }
    selectBlock(block) {
        if (this.clickLock == true) {
            return;
        }
        console.log(block.word);

        this.selectedBlock = block;
        block.alpha = 0.5;
        this.input.on('pointermove', this.doDrag.bind(this));
        this.input.once('pointerup', this.stopDrag.bind(this));
    }
    doDrag() {

        this.selectedBlock.x = this.input.activePointer.x;
        this.selectedBlock.y = this.input.activePointer.y;
        this.checkTargets();
    }
    stopDrag() {
        this.selectedBlock.alpha = 1;
        this.input.off('pointermove');
        let dt = this.getDroppedTarget();
        console.log("dt=" + dt);

        if (dt) {

            let dropWord = dt.word.toLowerCase();
            let selectWord = this.selectedBlock.word.toLowerCase();
            console.log("Drop word=" + dropWord);
            console.log("selectWord=" + selectWord);

            if (dropWord != selectWord) {
                this.flyBack();
            }
            else {
                this.selectedBlock.destroy();
                this.selectedBlock = null;
                dt.showWord();
            }
        }
        else {
            this.flyBack();
        }

    }
    flyBack() {
        this.clickLock = true;
        this.tweens.add({ targets: this.selectedBlock, duration: 250, y: this.selectedBlock.oy, x: this.selectedBlock.ox, onComplete: this.flyBackDone.bind(this) });
    }
    flyBackDone() {
        this.clickLock = false;
    }
    getDroppedTarget() {
        for (let i = 0; i < this.dropTargets.length; i++) {
            let dt = this.dropTargets[i];
            let dropX = dt.x + dt.displayWidth / 2;
            let dropY = dt.y + dt.displayHeight / 2;

            let distX = Math.abs(this.selectedBlock.x - dropX);
            let distY = Math.abs(this.selectedBlock.y - dropY);
            if (distX < this.gw * 0.05 && distY < this.gh * 0.05) {
                return dt;
            }

        }
        return null;
    }
    checkTargets() {
        for (let i = 0; i < this.dropTargets.length; i++) {
            let dt = this.dropTargets[i];
            let dropX = dt.x + dt.displayWidth / 2;
            let dropY = dt.y + dt.displayHeight / 2;

            let distX = Math.abs(this.selectedBlock.x - dropX);
            let distY = Math.abs(this.selectedBlock.y - dropY);
            if (distX < this.gw * 0.05 && distY < this.gh * 0.05) {
                dt.turnSelected();
            }
            else {
                dt.turnNormal();
            }
        }
    }
    update() {

    }
}
export default SceneMain;