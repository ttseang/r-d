import Align from "../util/Align";


export class WordBox extends Phaser.GameObjects.Container {
    constructor(config) {
        super(config.scene);
        this.scene = config.scene;
        this.word = config.word;
        this.isChild=false;
        
        this.back = this.scene.add.image(0, 0, "holder");
        this.back.setOrigin(0, 0);
        this.add(this.back);
        // this.back.alpha = 0.2;
        this.back.visible = false;
        //
        //
        //Align.scaleToGameW(this.back, config.scale, this.scene);
        // this.back.displayHeight = Align.getYPer(0.07, this.scene);
        //
        //
        if (config.word) {
            this.textObj = this.scene.add.text(0, 0, config.word, { color: 'white', fontFamily: 'Palanquin Dark', fontSize: (this.scene.gw / 20) * config.textScale });
            this.textObj.setOrigin(0.5, 0);
            this.add(this.textObj);


        }


        window.wordBox = this;
        this.scene.add.existing(this);
    }
    sizeBox(mods) {
        if (!this.word) {
            console.log('make sep');
            this.back.displayWidth = this.scene.gw * 0.025;

        }
        else {
            let widthByWords = this.textObj.displayWidth * 1.1;
            let widthByMods = Align.getXPer(0.1, this.scene) * mods;

            let w = Math.max(widthByMods, widthByWords);

            this.back.displayWidth = w;

            this.textObj.x = this.back.displayWidth / 2;
        }

        this.back.displayHeight = Align.getYPer(0.07, this.scene);
        this.setSize(this.back.displayWidth, this.back.displayHeight);
    }
    turnNormal() {
        if (this.textObj) {
            this.textObj.setColor("#ffffff");
        }
    }
    turnSelected() {
        if (this.textObj) {
            this.textObj.setColor("#ff0000");
        }
    }
    hideWord() {
        if (this.textObj) {
            this.textObj.setText("?");
        }
    }
    showWord() {
        if (this.textObj) {
            this.textObj.setText(this.word);
            this.turnNormal();
        }
    }
    getTop() {
        return this.y;// - this.displayHeight / 2;
    }
    getBottom() {
        return this.y + this.displayHeight;
    }
    getLeft() {
        return this.x;//- this.displayWidth;
    }
    getRight() {
        return this.x + this.displayWidth;
    }
    getCenter() {
        return this.x - this.displayWidth / 2;
    }
    drawLines(drawIndex) {

        this.drawIndex = drawIndex;

        switch (drawIndex) {
            case "d":
                this.back.displayWidth = this.textObj.displayWidth * 2;
                this.back.displayHeight = Align.getYPer(0.05, this.scene);
                this.setSize(this.back.displayWidth, this.back.displayHeight);

                this.lineObj = this.scene.add.image(-Align.getYPer(0.05, this.scene), this.getBottom(), "holder").setOrigin(0, 0);
                this.lineObj.setTint(0xff000);
                this.lineObj.displayWidth = Align.getXPer(0.1, this.scene);
                this.lineObj.displayHeight = Align.getYPer(0.01, this.scene);
                this.add(this.lineObj);
                this.textObj.x = this.back.displayWidth / 2;
                this.textObj.y = this.back.displayHeight - this.textObj.displayHeight;

                // this.textObj.setOrigin(-0.4,0);
                break;
            case "h":
                if (!this.parentBox) {
                    this.lineObj = this.scene.add.image(0, this.displayHeight, "holder").setOrigin(0, 0);
                    this.lineObj.setTint(0xff000);
                    this.lineObj.displayWidth = this.back.displayWidth;
                    this.lineObj.displayHeight = this.displayHeight * 0.1;
                    this.add(this.lineObj);
                }
                else {
                                      
                        this.lineObj = this.scene.add.image(0, this.displayHeight / 2, "holder").setOrigin(0, 0);
                        this.lineObj.setTint(0xff000);
                        this.lineObj.displayWidth = this.back.displayWidth;
                        this.lineObj.displayHeight = this.displayHeight * 0.1;
                        this.add(this.lineObj);   
                    
                }
                // this.textObj.setOrigin(0.5,0.5);
                break;

            case "tvsep":

                let xMod = 0.005;
                let yMod = 0.08;

                let lineWidth = this.scene.gw * xMod;
                let lineHeight = this.scene.gh * yMod;

                let sideLine = this.scene.add.image(0, this.getTop(), "holder").setOrigin(0, 0);
                sideLine.setTint(0xff000);
                sideLine.displayWidth = lineWidth;
                sideLine.displayHeight = lineHeight;
                this.back.visible = false;
                this.setSize(lineWidth, lineHeight);
                this.add(sideLine);
                break;
        }

    }
}