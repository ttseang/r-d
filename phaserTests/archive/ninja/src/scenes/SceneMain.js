/*jshint esversion: 6 */
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { AnimationUtil } from "../util/animationUtil";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");
        this.load.image("box", "images/boxAlt.png");
        this.load.image("grass", "images/grass.png");
        this.load.atlas("ninja", "images/ninja.png", "images/ninja.json");

    }
    create() {
        this.aGrid = new AlignGrid({ scene: this, rows: 11, cols: 11 });

        //make collision groups
        this.boxGroup = this.physics.add.group();
        this.grassGroup = this.physics.add.group();

        let animUtil = new AnimationUtil();
        animUtil.makeAnims(this, "ninja");

        this.currentAnim="idle";

        this.ninja=this.physics.add.sprite(0,0,"ninja");
       // this.ninja.visible = false;
        this.ninja.body.collideWorldBounds=true;
        Align.scaleToGameW(this.ninja, 0.15, this);
        this.aGrid.placeAtIndex(60, this.ninja);

        this.ninja.play("idle");
        this.ninja.setGravityY(100);

        //make the ground and blocks
        this.makeBlocks();
        this.makeGrass();

        this.physics.add.collider(this.ninja, this.boxGroup);
        this.physics.add.collider(this.boxGroup);
        this.physics.add.collider(this.grassGroup, this.boxGroup);
        this.physics.add.collider(this.grassGroup, this.ninja);

        this.time.addEvent({ delay: 1000, callback: this.fixBody.bind(this), loop: false });
        this.input.on('pointerdown', this.onClick.bind(this));
    }
    fixBody() {
        
        this.ninja.body.setSize(this.ninja.body.displayWidth, this.ninja.body.displayHeight);
        this.ninja.visible = true;
    }
    setAnim(anim)
    {
        if (this.currentAnim!=anim)
        {
            this.ninja.play(anim);
            this.currentAnim=anim;
        }
    }
    //make the ground
    makeGrass() {
        for (let i = 110; i < 121; i++) {
            let grass = this.physics.add.sprite(0, 0, "grass");
            this.grassGroup.add(grass);
            Align.scaleToGameW(grass, 0.1, this);
            this.aGrid.placeAtIndex(i, grass);
            grass.setImmovable(true);
        }
    }
    //make the blocks
    makeBlocks() {
        for (let i = 20; i < 99; i += 11) {
            let block = this.physics.add.sprite(0, 0, "box");
            this.boxGroup.add(block);
            Align.scaleToGameW(block, 0.05, this);
            this.aGrid.placeAtIndex(i, block);
            block.setGravityY(20);
        }
    }
    updateAnimation() {
        if (Math.abs(this.ninja.body.velocity.x) == 0) {
            this.setAnim("idle");
        }
        else {
            this.setAnim("run");
        }
        if (Math.abs(this.ninja.body.velocity.x) > 0) {
            if (this.ninja.body.velocity.x < 0) {
                this.ninja.flipX = true;
            }
            else {
                this.ninja.flipX = false;
            }
        }
    }
    checkTargetDist() {
               
        //get the distance between the target and ninja
        let distX = Math.abs(this.ninja.x - this.targetX);
        

        //if close to the target, then stop the ninja
        if (distX < 10) {
            this.ninja.body.setVelocity(0, 0);
        }
    }
    onClick(pointer) {
        this.targetX = pointer.x;
        if (this.targetX > this.ninja.x) {
            this.ninja.setVelocityX(200);
        }
        else {
            this.ninja.setVelocityX(-200);
        }
    }
    update() {
        this.updateAnimation();
        this.checkTargetDist();
    }
}
export default SceneMain;