
import Align from "../util/Align";
import {UIBlock} from "../util/UIBlock";

export class DragWord extends UIBlock {
    constructor(config) {
        super();
        this.scene = config.scene;
        this.word=config.word;
        this.callback=config.callback;
        
        this.back = this.scene.add.sprite(0, 0, "holder");
        this.back.setTint(0x3498db);
        this.back.setOrigin(0.5, 0.5);
        this.back.setInteractive();
        this.back.on('pointerdown',this.selectMe.bind(this));

        this.add(this.back);

        this.textObj = this.scene.add.text(0, 0, config.word, { color: 'black', fontFamily: 'Palanquin Dark', fontSize: (this.scene.gw / 20) });
        this.textObj.setOrigin(0.5, 0.5);
        this.add(this.textObj);

        this.back.displayWidth=this.textObj.displayWidth*1.1;
        this.back.displayHeight = Align.getYPer(0.07, this.scene);

        window.dragBox = this;
        
      // this.scene.add.existing(this);
    }
    selectMe()
    {
        console.log("here");
        if (this.callback)
        {
            this.callback(this);
        }
       
    }
}