/*jshint esversion: 6 */
import { WordBox } from "../comps/wordBox";
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
//import { UIBlock } from "../util/UIBlock";
import { DragWord } from "../comps/dragWord";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("triangle", "./assets/images/triangle.png");
        this.load.image("holder", "./assets/images/holder.jpg");
        this.load.json("allsents", "./assets/data/sample.json");
    }
    create() {

        this.clickLock = false;
        this.selectedBlock=null;
        this.allBoxes = [];
        this.dropTargets=[];
        this.words=[];

        this.gw = this.sys.game.config.width;
        this.gh = this.sys.game.config.height;
        this.graphics=this.add.graphics();
        this.graphics.lineStyle(5,0x27ae60,1);

        //make a grid        
        this.grid = new AlignGrid({ scene: this, rows: 11, cols: 22 });
     //   this.grid.showPos();

        this.cw = this.grid.cw;
        this.ch = this.grid.ch;
        this.cd=this.grid.cd;

        window.scene = this;



        const allsents = this.cache.json.get('allsents');
        let r=Math.floor(Math.random()*allsents.length);
        this.sentdata = allsents[r];
        //
        //
        //
        this.startX = this.sentdata.startX;
        this.startY = this.sentdata.startY;

        this.drawTree();
        this.dropTargets.forEach((box)=>{box.hideWord();});
        this.makeBlocks();
        
    }
    drawTree() {
        let len = this.sentdata.diagram.length;

        for (let i = 0; i < len; i++) {
            let data = this.sentdata.diagram[i];
           // //console.log(data);
            let box = this.makeBox(data.w, parseFloat(data.px), parseFloat(data.py), parseFloat(data.len), data.l);

            if (data.mod) {
                this.makeMods(data.mod);
            }

        }
    }
    makeMods(mod) {
     //   //console.log(mod);
        for (let j = 0; j < mod.length; j++) {
            let modData = mod[j];
            ////console.log(modData);
            let modBox = this.makeBox(modData.w, parseFloat(modData.px), parseFloat(modData.py), parseFloat(modData.len), modData.l);
            if (modData.mod) {
                this.makeMods(modData.mod);
            }
        }

    }
    makeBox(word, xx, yy, len = 1, line = "h") {
       // //console.log(word, xx, yy, line);
        if (isNaN(len)) {
            len = 1;
        }

        let wordBox = new WordBox({ scene: this, word: word, len: len, line: line });
        this.grid.placeAt2(this.startX + xx, +this.startY + yy, wordBox, false);
        wordBox.drawGraphics(line);
        this.allBoxes.push(wordBox);
        if (word!=undefined)
        {
            this.words.push(word);
            this.dropTargets.push(wordBox);
        }
        return wordBox;
    }
    makeBlocks() {
        for (let i = 0; i < this.words.length; i++) {
            let block = new DragWord({ scene: this, word: this.words[i], callback: this.selectBlock.bind(this) });
            this.grid.placeAtIndex(155 + i * 4, block);
            block.ox = block.x;
            block.oy = block.y;

        }
    }
    selectBlock(block) {
        if (this.clickLock == true) {
            return;
        }
        //console.log(block.word);

        this.selectedBlock = block;
        block.alpha = 0.5;
        this.input.on('pointermove', this.doDrag.bind(this));
        this.input.once('pointerup', this.stopDrag.bind(this));
    }
    doDrag() {

        this.selectedBlock.x = this.input.activePointer.x;
        this.selectedBlock.y = this.input.activePointer.y;
        this.checkTargets();
    }
    stopDrag() {
        this.selectedBlock.alpha = 1;
        this.input.off('pointermove');
        let dt = this.getDroppedTarget();
        //console.log("dt=" + dt);

        if (dt) {

            let dropWord = dt.word.toLowerCase();
            let selectWord = this.selectedBlock.word.toLowerCase();
            //console.log("Drop word=" + dropWord);
            //console.log("selectWord=" + selectWord);

            if (dropWord != selectWord) {
                this.flyBack();
            }
            else {
                this.selectedBlock.destroy();
                this.selectedBlock = null;
                dt.showWord();
            }
        }
        else {
            this.flyBack();
        }

    }
    flyBack() {
        this.clickLock = true;
        this.tweens.add({ targets: this.selectedBlock, duration: 250, y: this.selectedBlock.oy, x: this.selectedBlock.ox, onComplete: this.flyBackDone.bind(this) });
    }
    flyBackDone() {
        this.clickLock = false;
    }
    getDroppedTarget() {
        for (let i = 0; i < this.dropTargets.length; i++) {
            let dt = this.dropTargets[i];
            let dropX = dt.x + dt.displayWidth / 2;
            let dropY = dt.y + dt.displayHeight / 2;
            
            if (dt.angle!=0)
            {
                dropX-=this.cw*1.1;
              //  dropY-=this.ch;
            }

            let distX = Math.abs(this.selectedBlock.x - dropX);
            let distY = Math.abs(this.selectedBlock.y - dropY);
            if (distX < this.gw * 0.05 && distY < this.gh * 0.05) {
                return dt;
            }

        }
        return null;
    }
    checkTargets() {
        for (let i = 0; i < this.dropTargets.length; i++) {
            let dt = this.dropTargets[i];
            let dropX = dt.x + dt.displayWidth / 2;
            let dropY = dt.y + dt.displayHeight / 2;

            if (dt.angle!=0)
            {
                dropX-=this.cw*1.1;
               // dropY-=this.ch;
            }

            let distX = Math.abs(this.selectedBlock.x - dropX);
            let distY = Math.abs(this.selectedBlock.y - dropY);
            if (distX < this.gw * 0.05 && distY < this.gh * 0.05) {
                dt.turnSelected();
            }
            else {
                dt.turnNormal();
            }
        }
    }
    update() {

    }
}
export default SceneMain;