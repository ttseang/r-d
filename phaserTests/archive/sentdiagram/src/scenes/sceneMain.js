/*jshint esversion: 6 */
import { WordBox } from "../comps/wordBox";
import Align from "../util/Align";
import { AlignGrid } from "../util/alignGrid";
//import { UIBlock } from "../util/UIBlock";

export class SceneMain extends Phaser.Scene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/images/holder.jpg");
    }
    create() {

        let dataObj = '{"sentence":"the blue bird flew quickly to the red house","subject":"bird","directObject":"house","verb":"flew","subjectMods":["the","blue"],"directObjectMods":["the","red"],"verbMods":["quickly"],"predicate":"house ","fullSubject":"the bird flew quickly to the red "}';

        this.sentence = JSON.parse(dataObj);

        
        this.gw = this.sys.game.config.width;
        this.gh = this.sys.game.config.height;

        //make a grid        
        this.grid = new AlignGrid({ scene: this, rows: 11, cols: 22 });
       // this.grid.showNumbers();


        this.sentenceText=this.add.text(0,0,this.sentence.sentence,{color:'white',fontSize:22}).setOrigin(0.5,0.5);
        this.grid.placeAtIndex(32,this.sentenceText);


        this.verbBox = this.makeBox(this.sentence.verb, 76, 3);
        this.subjectBox = this.makeBox(this.sentence.subject, 72, 3);
        this.doBox = this.makeBox(this.sentence.directObject, 80, 1);
        //
        //
        // modBox1.setAngle(45);

        this.subjectBox.x = this.verbBox.x - this.subjectBox.displayWidth;
        this.doBox.x = this.verbBox.x + this.doBox.displayWidth;

        this.makeSubjectMods();
        this.makePredMods();
        this.makeVerbMods();

        window.scene = this;
    }
    makeSubjectMods() {
        //make subject boxes
        let xx = 5.2;

        for (let i = 0; i < this.sentence.subjectMods.length; i++) {
            let mod = this.sentence.subjectMods[i];
            console.log(mod);
            let modBox = this.makeBox(mod, 0, 1);
            this.grid.placeAt(xx, 4.4, modBox);
            modBox.setAngle(55);
            xx += 2;
        }
    }
    makePredMods()
    {
        let xx = 15;
        for (let i = 0; i < this.sentence.directObjectMods.length; i++) {
            let mod = this.sentence.directObjectMods[i];
            console.log(mod);
            let modBox = this.makeBox(mod, 0, 1);
            this.grid.placeAt(xx, 4.4, modBox);
            modBox.setAngle(55);
            xx += 2;
        }
    }
    //make verb mods
    makeVerbMods()
    {
        let xx = 10;
        for (let i = 0; i < this.sentence.verbMods.length; i++) {
            let mod = this.sentence.verbMods[i];
            console.log(mod);
            let modBox = this.makeBox(mod, 0, 1);
            this.grid.placeAt(xx, 4.4, modBox);
            modBox.setAngle(55);
            xx += 2;
        }
    }
    makeBox(word, pos, lines, scale = 0.2, textScale = 0.7) {
        let wordBox = new WordBox({ scene: this, word: word, scale: scale, textScale: textScale });
        wordBox.drawLines(lines);
        this.grid.placeAtIndex(pos, wordBox);

        return wordBox;
    }
    update() {

    }
}
export default SceneMain;