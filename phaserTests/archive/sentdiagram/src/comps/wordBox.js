import Align from "../util/Align";
import { UIBlock } from "../util/UIBlock";

export class WordBox extends Phaser.GameObjects.Container {
    constructor(config) {
        super(config.scene);
        this.scene = config.scene;
        this.back = this.scene.add.image(0, 0, "holder");
        this.add(this.back);
      //  this.back.alpha=0.1;
        this.back.visible=false;
        //
        //
        Align.scaleToGameW(this.back, config.scale, this.scene);
        this.back.displayHeight = this.back.displayWidth / 4;
        //
        //
        this.textObj = this.scene.add.text(0, 0, config.word, { color: 'white', fontFamily: 'Palanquin Dark', fontSize: (this.scene.gw / 20) * config.textScale });
        this.textObj.setOrigin(0.5, 0.5);
        this.add(this.textObj);
        
        //
        //
        this.setSize(this.back.displayWidth, this.back.displayHeight);

        window.wordBox = this;
        this.scene.add.existing(this);
    }
    getTop() {
        return this.y - this.displayHeight / 2;
    }
    getBottom() {
        return this.y + this.displayHeight / 2;
    }
    getLeft() {
        return this.x - this.displayWidth / 2;
    }
    getRight() {
        return this.x + this.displayWidth / 2;
    }
    drawLines(drawIndex) {

        this.lineObj = this.scene.add.image(this.getLeft(), this.getBottom(), "holder").setOrigin(0, 0);
        this.lineObj.setTint(0xff000);
        this.lineObj.displayWidth = this.displayWidth;
        this.lineObj.displayHeight = this.displayHeight * 0.1;
        this.add(this.lineObj);

        switch(drawIndex)
        {
            case 2:
                let sideLine = this.scene.add.image(this.getRight(), this.getTop(), "holder").setOrigin(0, 0);
                sideLine.setTint(0xff000);
                sideLine.displayWidth = this.displayWidth*0.05;
                sideLine.displayHeight = this.displayHeight;
                this.add(sideLine);
            break;

            case 3:
                let sideLine2 = this.scene.add.image(this.getRight(), this.getTop(), "holder").setOrigin(0, 0);
                sideLine2.setTint(0xff000);
                sideLine2.displayWidth = this.displayWidth*0.05;
                sideLine2.displayHeight = this.displayHeight*1.5;
                this.add(sideLine2);
            break;
        }
    }
}