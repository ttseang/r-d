export class ColorVo
{
    public back:number;
    public textColor:string;

    constructor(back:number,textColor:string)
    {
        this.back=back;
        this.textColor=textColor;
    }
}