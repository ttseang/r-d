import { GameObjects } from "phaser"
import { ColorVo } from "../dataObjs/ColorVo";
import IBaseScene from "../interfaces/IBaseScene";

export class NumberBox extends GameObjects.Container
{
    private bscene:IBaseScene;
    private back:GameObjects.Image;
    
    private numText:GameObjects.Text;
    public index:number=0;
    public value:number=0;
    public selected:boolean=false;
    private border:GameObjects.Image;

    constructor(bscene:IBaseScene,value:number,color:ColorVo)
    {
        super(bscene.getScene());
        this.back=this.scene.add.image(0,0,"holder");
        this.back.displayWidth=bscene.getW()/21;
        this.back.displayHeight=bscene.getH()/16;
        this.back.setTint(color.back);
        this.add(this.back);

        this.value=value;

        this.numText=this.scene.add.text(0,0,value.toString(),{color:color.textColor,fontSize:"26px"});
        this.numText.setOrigin(0.5,0.5);
        this.add(this.numText);

        this.border=this.scene.add.image(0,0,"border");
        this.border.displayHeight=this.back.displayHeight;
        this.border.displayWidth=this.back.displayWidth;
        this.add(this.border);
        this.border.setTint(0xff0000);
        this.border.visible=false;

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);
        this.setInteractive();
    }
    setSelected(val:boolean)
    {
        this.selected=val;
         if (this.selected==true)
        {
            this.border.visible=true;
        }
        else
        {
            this.border.visible=false;
        } 
    }
}