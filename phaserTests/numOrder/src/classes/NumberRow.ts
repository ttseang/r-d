import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { GM } from "./GM";
import { NumberBox } from "./NumberBox";

export class NumberRow extends GameObjects.Container
{
    private bscene:IBaseScene;
    private gm:GM=GM.getInstance();
    public isDragging:boolean=false;
    public index:number=0;
    public handle:GameObjects.Image;

    constructor(bscene:IBaseScene,start:number,end:number,index:number)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        let c:number=-1;
        let hh:number=0;
        let ww:number=0;
        this.index=index;

        

        for (let i:number=start;i<end;i++)
        {
            c++;
            let nb:NumberBox=new NumberBox(bscene,i,this.gm.boxColors[c+2]);

            nb.index=this.index;
            nb.x=c*nb.displayWidth+nb.displayWidth/2;
            nb.y=nb.displayHeight/2;
            this.add(nb);


            hh=nb.displayHeight;
            ww+=nb.displayWidth;
        }

       /*  this.handle=this.scene.add.image(0,0,"holder").setOrigin(0,0);

        this.handle.displayWidth=ww;
        this.handle.displayHeight=hh;
        this.add(this.handle); */
        



        this.setSize(ww,hh);
       // this.setInteractive();

        this.scene.add.existing(this);
    }
}