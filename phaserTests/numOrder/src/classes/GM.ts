import { ColorVo } from "../dataObjs/ColorVo";

let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

    public colors:number[]=[];
    public boxColors:ColorVo[]=[];
    
    constructor()
    {
        window['gm']=this;
        this.colors=[0x1abc9c,0x2ecc71,0x3498db,0x9b59b6,0x34495e,0x16a085,0x27ae60,0x2980b9,0x8e44ad,0x2c3e50,0xf1c40f,0xe67e22,0xe74c3c,0xecf0f1,0x95a5a6,0xf39c12,0xd35400,0xc0392b,0xbdc3c7,0x7f8c8d];
    
        this.boxColors.push(new ColorVo(0x1abc9c,"#ffffff"));
        this.boxColors.push(new ColorVo(0x2ecc71,"#ffffff"));
        this.boxColors.push(new ColorVo(0x3498db,"#ffffff"));
        this.boxColors.push(new ColorVo(0x9b59b6,"#ffffff"));
        this.boxColors.push(new ColorVo(0x34495e,"#ffffff"));
        this.boxColors.push(new ColorVo(0x16a085,"#ffffff"));
        this.boxColors.push(new ColorVo(0x27ae60,"#ffffff"));
        this.boxColors.push(new ColorVo(0x2980b9,"#ffffff"));
        this.boxColors.push(new ColorVo(0x8e44ad,"#ffffff"));
        this.boxColors.push(new ColorVo(0x2c3e50,"#ffffff"));
        this.boxColors.push(new ColorVo(0xf1c40f,"#ffffff"));
        this.boxColors.push(new ColorVo(0xe67e22,"#ffffff"));
        this.boxColors.push(new ColorVo(0xe74c3c,"#ffffff"));
        this.boxColors.push(new ColorVo(0xecf0f1,"#000000"));
        this.boxColors.push(new ColorVo(0x95a5a6,"#000000"));
        this.boxColors.push(new ColorVo(0xf39c12,"#ffffff"));
        this.boxColors.push(new ColorVo(0xd35400,"#ffffff"));
        this.boxColors.push(new ColorVo(0xc0392b,"#ffffff"));
        this.boxColors.push(new ColorVo(0xbdc3c7,"#000000"));
        this.boxColors.push(new ColorVo(0x7f8c8d,"#ffffff"));
 
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}