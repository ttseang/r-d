import { GameObjects } from "phaser";
import { ColorBurst } from "../classes/effects/colorBurst";
import { NumberRow } from "../classes/NumberRow";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private rows: NumberRow[] = [];
    private currentRow: NumberRow;

    private myRow: NumberRow = null;
    private graphics: GameObjects.Graphics;
    private rowCount: number = 4;
    private fairy: GameObjects.Sprite;
    private backgroundMusic: Phaser.Sound.BaseSound;
    private bg: GameObjects.Image;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("border", "./assets/border.png");

        this.load.atlas("fairy","./assets/fairy1.png","./assets/fairy1.json");

        this.load.audio("newlevel", "./assets/audio/sfx/newlevel.wav");
        this.load.audio("fail", "./assets/audio/sfx/fail.mp3");
        this.load.audio("fairytale", "./assets/audio/background/fairytale.wav");

        this.load.image("bg", "./assets/bg.jpg");
        this.load.image("button", "./assets/ui/buttons/1/3.png");

        ColorBurst.preload(this);
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.bg = this.add.image(0, 0, "bg");
        Align.scaleToGameW(this.bg, 1, this);
        if (this.bg.displayHeight < this.gh) {
            this.bg.displayHeight = this.gh;
            this.bg.scaleX = this.bg.scaleY;
        }
        Align.center(this.bg, this);


        this.graphics = this.add.graphics();

        this.makeFairy();
        this.makeRows();

        this.input.on("gameobjectdown", this.startDragRow.bind(this));
        this.input.on("pointermove", this.dragRow.bind(this));
        this.input.on("pointerup", this.onUp.bind(this));

        if (this.backgroundMusic)
        {
            this.backgroundMusic.stop();
        }
        this.backgroundMusic = this.sound.add("fairytale");
        this.backgroundMusic.play({ volume: 0.2, loop: true });

        window['scene'] = this;
       
        this.children.bringToTop(this.fairy);
    }
    showStars(xx:number,yy:number) {
        let cb1 = new ColorBurst(this, xx,yy, 50, 150, 1000, 0xffffff);
    }
    makeFairy() {
        this.fairy = this.add.sprite(0, 0, "fairy");
        Align.scaleToGameW(this.fairy,0.1,this);

        let frames = this.anims.generateFrameNames('fairy', { start: 0, end: 7, zeroPad: 3, prefix: 'Idle Blinking_', suffix: '.png' });

        this.anims.create({
            key: 'idle',
            frames: frames,
            frameRate: 8,
            repeat: -1
        });      


        let frames2 = this.anims.generateFrameNames("fairy",{ start: 0, end:7,zeroPad:3,prefix:"Casting Spells_",suffix: '.png'})
        this.anims.create({
            key: 'casting',
            frames: frames2,
            frameRate:24,
            repeat:0
        });
        window['fairy']=this.fairy;

        this.fairy.play("idle");

        this.grid.placeAt(3, 3, this.fairy);

    }
    makeRows() {

        while (this.rows.length > 0) {
            this.rows.pop().destroy();
        }

        for (let i: number = 0; i < this.rowCount; i++) {
            let start: number = (i * 10) + 1;
            let end: number = start + 10;
            let nr: NumberRow = new NumberRow(this, start, end, i);

           

            this.rows.push(nr);
            // nr.x=100;
            Align.center2(nr, this);
            nr.y = i * 10;
        }
        this.lineUpRows();
        this.pickRandom();
    }
    playSound(sound: string) {
        let audio: Phaser.Sound.BaseSound = this.sound.add(sound);
        audio.play();
    }
    test() {
        this.graphics.clear();
        this.graphics.lineStyle(2, 0xffff00);

        let bounds: Phaser.Geom.Rectangle = this.myRow.getBounds();
        this.graphics.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);

    }
    pickRandom() {
        let r: number = Math.floor(Math.random() * this.rows.length - 1) + 1;
        let row: NumberRow = this.rows[r];

        this.myRow = row;
        // row.setInteractive();

        window['row'] = row;

        let c: number = 0;

        for (let i: number = 0; i < this.rows.length; i++) {
            if (i != r) {
                this.rows[i].y = (this.gh * 0.2) + c * this.rows[i].displayHeight;
                c++;
            }
        }

        row.y = this.gh * 0.8;
        this.fairy.y=row.y;
        this.fairy.x=row.x-this.fairy.displayWidth/2;

    }
    startDragRow(p: Phaser.Input.Pointer, obj: any) {

        let index: number = obj.index;
        if (index != this.myRow.index) {
            return;
        }
        this.currentRow = this.rows[index];
        this.currentRow.y = p.y;
        this.fairy.y=this.currentRow.y;
        this.fairy.x=this.currentRow.x-this.fairy.displayWidth/2;
        this.currentRow.isDragging = true;
        this.children.bringToTop(this.currentRow);
    }
    dragRow(p: Phaser.Input.Pointer) {
        if (this.currentRow) {
            this.currentRow.y = p.y;
            this.fairy.y=this.currentRow.y;
            this.lineUpRows();
        }
    }
 
    onUp() {
        if (this.currentRow) {
            this.currentRow.isDragging = false;
            this.currentRow = null;
            this.lineUpRows();

            let correct: boolean = this.checkCorrect();
            console.log(correct);

            if (correct == true) {
                this.playSound("newlevel");
                this.showStars(this.gw/2,this.gh/2);

                setTimeout(() => {
                    if (this.rowCount < 10) {
                        this.rowCount++;
                        this.makeRows();
                    }     
                }, 2000);
               
            }
        }
    }
    checkCorrect() {
        let rows2: NumberRow[] = this.rows.slice();


        rows2.sort((a: NumberRow, b: NumberRow) => {
            return (a.y - b.y);
        });
        // console.log(rows2);


        for (let i: number = 0; i < this.rows.length; i++) {
            if (rows2[i].index != i) {
                return false;
            }

        }
        return true;
    }
    lineUpRows() {
        let startY: number = this.gh * 0.2;
        let rows2: NumberRow[] = this.rows.slice();


        rows2.sort((a: NumberRow, b: NumberRow) => {
            return (a.y - b.y);
        });
        // console.log(rows2);


        for (let i: number = 0; i < this.rows.length; i++) {
            if (rows2[i].isDragging == false) {
                rows2[i].y = startY + i * rows2[i].displayHeight;
            }

        }


    }
}