import { GameObjects } from "phaser";
import { CustomKeyboard } from "../classes/CustomKeyboard";
import { LetterBox } from "../classes/LetterBox";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private kb:CustomKeyboard | null=null;
    private kbn:CustomKeyboard | null=null;

    private text1:GameObjects.Text;
    private text2:GameObjects.Text;

    private numText:string="";
    private myText:string="";

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("letterbox", "./assets/letterbox.png");
        this.load.image("holder","./assets/holder.jpg");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.text1=this.add.text(this.gw/2,this.gh*0.1,"Text here",{color:"black",fontFamily:"Arial",fontSize:"30px"}).setOrigin(0.5,0.5);
        this.text2=this.add.text(this.gw/2,this.gh*0.1,"Text here",{color:"black",fontFamily:"Arial",fontSize:"30px"}).setOrigin(0.5,0.5);

        /* let letterBox:LetterBox=new LetterBox(this,"A");
        Align.scaleToGameW(letterBox,0.1,this);
        Align.center(letterBox,this); */

        this.kb=new CustomKeyboard(this,4);
        this.kb.callback=this.addToText.bind(this);
        this.kb.makeLetters();
       
        Align.center2(this.kb,this);
        
        this.kb.x=this.gw*.1;
        this.text1.x=this.kb.x+this.kb.displayWidth/2;        


        this.kbn=new CustomKeyboard(this,3,0.2,0.4);
        this.kbn.callback=this.addToNum.bind(this);
        this.kbn.makeNumbers();
      
        Align.center2(this.kbn,this);
        
        this.kbn.x=this.gw*.6;
        //this.kbn.grid.showPos();
        this.text2.x=this.kbn.x+this.kbn.displayWidth/2;

        this.input.keyboard.on('keydown', this.onKeyUp.bind(this));
    }
    onKeyUp(e)
    {
        let numbers:string[]="1234567890".split("");

        if (!numbers.includes(e.key))
        {
            this.addToText(e.key.toUpperCase());
        }
        else
        {
            this.addToNum(e.key);
        }
       
    }
    private addToNum(digit:string)
    {
        switch(digit)
        {
            case "↢":
            case "BACKSPACE":
                if (this.numText.length>0)
                {
                    this.numText=this.numText.substr(0,this.numText.length-1);
                }
                break;

            case "☒":
            case "ESCAPE":
                this.numText="";
            break;
            default:
                if (!isNaN(parseInt(digit)))
                {
                    this.numText+=digit;
                }
              
        }
        this.text2.setText(this.numText);
    }
    private addToText(letter:string)
    {
        console.log("letter="+letter);
        switch(letter)
        {
            case "↢":
            case "BACKSPACE":
                if (this.myText.length>0)
                {
                    this.myText=this.myText.substr(0,this.myText.length-1);
                }
                break;

            case "☒":
            case "ESCAPE":
                this.myText="";
            break;
            default:
                this.myText+=letter;
        }
        this.text1.setText(this.myText);
    }
}