import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { LetterBox } from "./LetterBox";

export class CustomKeyboard extends Phaser.GameObjects.Container
{
    private back:GameObjects.Image;
    public scene:Phaser.Scene;
    public grid:AlignGrid;
    private bscene:IBaseScene;
    public callback:Function=()=>{};
    private cols:number=4;
    private useNumber:boolean=false;

    constructor(bscene:IBaseScene,cols:number,ww:number=0.3,hh:number=0.7)
    {
        super(bscene.getScene());

        this.bscene=bscene;

        this.back=this.scene.add.image(0,0,"holder").setOrigin(0,0);

        this.back.displayHeight=bscene.getH()*hh;
        this.back.displayWidth=bscene.getW()*ww;

        this.cols=cols;

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.add(this.back);

        this.grid=new AlignGrid(bscene,11,11,this.displayWidth,this.displayHeight);      
        

        this.scene.add.existing(this);

       
       // this.grid.showPos();
        
    }
   
   
    onPressed(p:Phaser.Input.Pointer,letterBox:LetterBox)
    {
        if (letterBox instanceof LetterBox)
        {

            console.log(letterBox.letter);

            this.callback(letterBox.letter);


        }
    }
    makeLetters()
    {
        let keys:string[]="ABCDEFGHIJKLMNOPQRSTUVWX↢YZ☒".split("");

        let xx:number=2;
        let yy:number=0.5;
        let count:number=0;

        for (let i:number=0;i<28;i++)
        {
            let letterBox:LetterBox=new LetterBox(this.bscene,keys[i],this.callback);
            Align.scaleToGameW(letterBox,0.05,this.bscene);

            this.add(letterBox);

            this.grid.placeAt(xx,yy,letterBox);
            xx+=2;
            count++;
            if (count==this.cols)
            {
                count=0;
                xx=2;
                yy+=1.5;
            }
        }
    }
    makeNumbers()
    {
        let keys:string[]="123456789↢0☒".split("");

        let xx:number=2;
        let yy:number=1;

        let count:number=0;

        for (let i:number=0;i<keys.length;i++)
        {
            let letterBox:LetterBox=new LetterBox(this.bscene,keys[i],this.callback);
            Align.scaleToGameW(letterBox,0.05,this.bscene);

            this.add(letterBox);

            this.grid.placeAt(xx,yy,letterBox);
            xx+=3;
            count++;

            if (count==this.cols)
            {
                count=0;
                xx=2;
                yy+=2.5;
            }
        }
    }
}