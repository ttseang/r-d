import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";

export class LetterBox extends Phaser.GameObjects.Container
{
    public letter:string;
    private back:GameObjects.Image;
    private text1:GameObjects.Text;
    public scene:Phaser.Scene;
    private callback:Function=()=>{};

    constructor(bscene:IBaseScene,letter:string,callback:Function)
    {
        super(bscene.getScene());

        this.scene=bscene.getScene();
        this.letter=letter;
        this.callback=callback;

        this.back=this.scene.add.image(0,0,"letterbox");
        this.add(this.back);

        this.text1=this.scene.add.text(0,0,this.letter,{fontSize:"100px",color:"black"}).setOrigin(0.5,0.5);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
        this.scene.add.existing(this);
        
        this.on('pointerdown',()=>{
            console.log(this.callback);
            this.callback(this.letter);
        })
    }
   
}