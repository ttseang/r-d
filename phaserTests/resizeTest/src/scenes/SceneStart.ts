import { GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import { Parachute } from "../classes/Parachute";
import { ResizeManager } from "../classes/resizeManager";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { BaseScene } from "./BaseScene";

export class SceneStart extends BaseScene implements IBaseScene
{
    private button1:Button;
    //private face:GameObjects.Sprite;

    private resizeManager:ResizeManager;

    constructor()
    {
        super("SceneStart");
    }
    preload()
    {
        this.load.image("button","./assets/ui/buttons/1/1.png");
        this.load.image("face","./assets/face.png");
        this.load.atlas("parachutes","./assets/parachutes.png","./assets/parachutes.json");
    }
    create()
    {
        super.create();
        this.makeGrid(22,22);
       this.grid.showPos();

        this.resizeManager=ResizeManager.getInstance(this);

        window['scene']=this;
        
        this.makeChildren();
        this.definePos();
        this.placeItems();
    }
    makeChildren()
    {
        this.button1=new Button(this,"button","Start");
       // this.face=this.add.sprite(0,0,"face");
    }
    definePos()
    {
        //
        //
        //
        let buttonLayout:LayoutVo=this.resizeManager.addItem("button1",this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",10,10,0.2));
        //
        //FACES
        //
        let keys:string[]=[];
        keys.push("button1");

        let deck:PosVo[]=this.makeDeck();

        for (let i:number=0;i<10;i++)
        {
            let face:GameObjects.Sprite=this.add.sprite(0,0,"face");
            let faceLayout:LayoutVo=this.resizeManager.addItem("face"+i,face);
            keys.push("face"+i);
            let scale:number=Math.floor((Math.random()*50))/500;
           // console.log(scale);
            faceLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",deck[i].x,deck[i].y,scale));
        }

         for (let i:number=0;i<5;i++)
        {
            let parachute:Parachute=new Parachute(this);
            let paraLayout:LayoutVo=this.resizeManager.addItem("para"+i.toString(),parachute);
            paraLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",i*4,0,0.05));
            this.grid.placeAt(i*4,0,parachute);
            parachute.pos=paraLayout.getDefinition(ResizeVo.DEVICE_DESKTOP,"");

            keys.push("para"+i.toString());
        }
        
        
        //buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,10,.3));
        
        this.resizeManager.setKeys(keys);

       // this.resizeManager.setKeys(['button1','face']);

    }
    placeItems()
    {
        this.resizeManager.placeChildren();
    }
    makeDeck()
    {
        let deck:PosVo[]=[];

        for (let i:number=0;i<22;i++)
        {
            for(let j:number=0;j<22;j++)
            {
                deck.push(new PosVo(i,j));
            }
        }
        return this.shuffleArray(deck);
    }  
    shuffleArray(deck:PosVo[])
    {
        for (let i:number=0;i<500;i++)
        {
            let t1:number=Math.floor(Math.random()*deck.length);
            let t2:number=Math.floor(Math.random()*deck.length);

            let temp:PosVo=deck[t1];
            deck[t1]=deck[t2];
            deck[t2]=temp;

        }
        return deck;
    }
   
}