import { IGameObj } from "../interfaces/IGameObj";
import { ResizeVo } from "./ResizeVo";

export class LayoutVo
{
    private resizeDef:Map<string,ResizeVo>=new Map<string,ResizeVo>();
    private default:ResizeVo;
    public obj:IGameObj;

    constructor(obj:IGameObj)
    {
        this.obj=obj;
    }
    public setDefintion(resizeVo:ResizeVo)
    {
        let key:string=resizeVo.device+"_"+resizeVo.orientation;
        this.resizeDef.set(key,resizeVo);

        if (resizeVo===null)
        {
            this.default=resizeVo;
        }
    }
    public getDefinition(device:string,orientation:string)
    {
        let key:string=device+"_"+orientation;
        if (this.resizeDef.has(key))
        {
            return this.resizeDef.get(key);
        }
        return this.default;
    }
    public setDefault(resizeVo:ResizeVo)
    {
        this.default=resizeVo;
    }
}