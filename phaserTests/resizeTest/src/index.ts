//import Phaser = require("phaser");
import Phaser, { DOM } from 'phaser';
import { SceneMain } from "./scenes/SceneMain";
import {GM} from "./classes/GM";
import { SceneStart } from './scenes/SceneStart';
import { ResizeVo } from './dataObjs/ResizeVo';

let gm:GM=GM.getInstance();

let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad=navigator.userAgent.indexOf("iPad");


    if (isTablet!=-1 || isIpad!=-1)
    {
        gm.isTablet=true;
        gm.device=ResizeVo.DEVICE_TABLET;
        isMobile=1;
    }

let w = 600;
let h = 400;
//
//
if (isMobile != -1) {
    gm.isMobile=true;
    gm.device=ResizeVo.DEVICE_MOBILE;
}

w = window.innerWidth;
h = window.innerHeight;

if (window['visualViewport'])
{
    w=window.visualViewport.width;
    h=window.visualViewport.height;
}

if (w<h)
{
    gm.isPort=true;
    gm.orientation=ResizeVo.ORIENTATION_PORTRAIT;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,    
    backgroundColor:'cccccc',
    parent: 'phaser-game',
    scene: [SceneStart,SceneMain]
};

new Phaser.Game(config);