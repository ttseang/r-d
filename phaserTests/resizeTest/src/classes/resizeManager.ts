import { LayoutVo } from "../dataObjs/LayoutVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import Align from "../util/align";
import { GM } from "./GM";

let instance: ResizeManager = null;

export class ResizeManager {
    private objMap: Map<string, LayoutVo> = new Map<string, LayoutVo>();
    private keys: string[];
    private bscene: IBaseScene;

    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
    }
    public static getInstance(bscene: IBaseScene) {
        if (instance === null) {
            instance = new ResizeManager(bscene);

            window.addEventListener("orientationchange", () => { instance.flipped() });
            window.addEventListener("resize", () => { instance.doResize() });
        }
        return instance;
    }
    private flipped() {

    }
    private doResize() {
       
        let rows:number=this.bscene.getGrid().rows;
        let cols:number=this.bscene.getGrid().cols;

        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        if (window['visualViewport']) {
            w = window.visualViewport.width;
            h = window.visualViewport.height;
        }
        //console.log(w,h);

        //change the size of the scene
        this.bscene.resetSize(w, h);

        //change the scale to the scene
        this.bscene.getScene().scale.resize(w, h);

        //remove the debug grid - should be off for production
        this.bscene.getGrid().hide();
        
        //remake the grid based on the new size
        this.bscene.makeGrid(cols, rows);

        //turn back on the debug grid -very slow
        //this.bscene.getGrid().showPos();

        //place the children on the new resized grid
        //but at the same grid positions
        this.placeChildren();
    }
    
    public placeChildren()
    {
        let gm:GM=GM.getInstance();
        //
        //
        //
        for (let i:number=0;i<this.keys.length;i++)
        {
            let key:string=this.keys[i];
            let layoutVo:LayoutVo=this.objMap.get(key);
            if (layoutVo)
            {
                let resizeVo:ResizeVo=layoutVo.getDefinition(gm.device,gm.orientation);
                let obj:IGameObj=layoutVo.obj;
               // console.log(obj);
                if (obj.type==="button")
                {
                    obj.update();
                }
                else
                {
                    Align.scaleToGameW(obj,resizeVo.scale,this.bscene);
                }

                if (obj.type==="parachute")
                {
                    obj.update();
                }
                else
                {
                    this.bscene.getGrid().placeAt(resizeVo.posX,resizeVo.posY,obj);
                }
               
            }
        }       
    }
    public addItem(key: string, obj: IGameObj) {
        if (!this.objMap.has(key)) {
            this.objMap.set(key, new LayoutVo(obj));
        }
        return this.objMap.get(key);
    }

    public setKeys(keys: string[]) {
        this.keys = keys;
    }
}