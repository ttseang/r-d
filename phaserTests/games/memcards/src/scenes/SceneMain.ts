import { GameObjects } from "phaser";
import { CountCard } from "../classes/CountCard";
import { GM } from "../classes/GM";
import { LayoutManager } from "../classes/layoutManager";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { AnimationUtil } from "../util/animUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    public mainCard: CountCard;
    public smallCards: CountCard[] = [];
    private door1: GameObjects.Image;
    private door2: GameObjects.Image;
    private lift: GameObjects.Image;

    private door1X: number = 0;
    private door2X: number = 0;
    private cardVals: number[] = [];
    private right: number = 0;
    private gm: GM = GM.getInstance();
    private layoutManager: LayoutManager;
    private text1:GameObjects.Text;
    private fox:GameObjects.Sprite;
    private bubble:GameObjects.Sprite;
    private itemKey:string="apple";
    private itemName:string="apples";

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("blank", "./assets/blank.png");
        this.load.image("lift", "./assets/lift.png");
        this.load.image("door", "./assets/door.png");
        this.load.image("holder", "./assets/holder.jpg");

        this.load.image("raspberry", "./assets/raspberry.png");
        this.load.image("apple", "./assets/apple.png");
        this.load.image("banana","./assets/banana.png");
        this.load.image("back", "./assets/back.jpg");

        this.load.atlas("fox","./assets/fox.png","./assets/fox.json");
        this.load.image("bubble","./assets/bubble2.png");

        this.load.audio("rightSound","./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");
        this.load.audio("doorSound","./assets/audio/door.wav");
        this.load.audio("doorClose","./assets/audio/door_close.wav");
    }
    create() {
        super.create();

          

        this.makeGrid(11, 11);
      //  this.grid.showPos();

        this.layoutManager = new LayoutManager(this);

        this.makeItems();
     //   this.makeDeck();

        
        this.layoutManager.callback = this.flipped.bind(this);

        this.fox=this.add.sprite(100, 200, "fox");

        Align.scaleToGameW(this.fox,0.25,this);
        
     //   this.grid.showNumbers();
        this.grid.placeAt(1,3,this.fox);

        let animUtil:AnimationUtil=new AnimationUtil();
        animUtil.makeAnims(this,"fox");
        //this.moveIn();
        this.fox.play("idle");
        this.bubble=this.add.sprite(0,0,"bubble");
        Align.scaleToGameW(this.bubble,0.15,this);
        this.grid.placeAt(3,2,this.bubble);
        
        this.placeItems();
        this.start();
    }
    start()
    {
        this.getItem();
        this.hideSmallCards();
        this.makeDeck();

        this.text1.setText("Count the\n"+this.itemName+"!");
        this.children.bringToTop(this.text1);
        setTimeout(() => {
            this.openDoors();
        }, 3000);
    }
    flipped() {
        this.makeItems();
        this.placeItems();
    }
    makeItems() {
        if (this.lift) {
            this.lift.destroy();
        }
        if (this.mainCard) {
            this.mainCard.destroyMe();
        }
        if (this.door1) {
            this.door1.destroy();
        }
        if (this.door2) {
            this.door2.destroy();
        }
        while (this.smallCards.length > 0) {
            this.smallCards.pop().destroyMe();
        }

        this.lift = this.add.image(0, 0, "lift");

        this.text1=this.add.text(0,0,"YOUR MESSSAGE HERE",{fontSize:"16px",color:"#000000"}).setOrigin(0.5,0.5);
        
        this.mainCard = new CountCard(this, "banana", this.layoutManager.mainCardScale, this.layoutManager.largeIconScale);
        this.mainCard.makeCard(9);


        this.door1 = this.add.image(0, 0, "door").setOrigin(0, 0.5);
        this.door2 = this.add.image(0, 0, "door").setOrigin(0, 0.5);

        window['door1'] = this.door1;
        window['door2'] = this.door2;

        window['scene'] = this;

        // card.y=-card.displayHeight/2;
        this.makeSmallCards();
    }
    getItem()
    {
        let r:number=Math.floor(Math.random()*3);
        switch(r)
        {
            case 0:
            this.itemName="bananas";
            this.itemKey="banana";
            break;
            case 1:
                this.itemName="raspberries";
                this.itemKey="raspberry";
                break;
            case 2:
                this.itemName="apples";
                this.itemKey="apple";
        }
    }
    placeItems() {
        console.log("placeItems");
        this.layoutManager.setPosScale();

        this.grid.placeAt(3,2,this.text1);

        let mainPos: PosVo = this.layoutManager.mainPos;
        this.mainCard.resizeCard(this.layoutManager.mainCardScale, this.layoutManager.largeIconScale);

        this.grid.placeAt(mainPos.x, mainPos.y, this.mainCard);
        this.mainCard.x -= this.mainCard.displayWidth / 2;

        Align.scaleToGameW(this.lift, this.layoutManager.liftScale, this);
        this.lift.x = this.mainCard.x + this.mainCard.displayWidth / 2;
        this.lift.y = this.mainCard.y + this.mainCard.displayHeight / 2;


        this.door1.scaleX = this.lift.scaleX;
        this.door1.scaleY = this.lift.scaleY;

        this.door1.x = this.lift.x - this.lift.displayWidth / 2;
        this.door1.y = this.lift.y;
        //
        //
        //

        this.door2.scaleX = this.lift.scaleX;
        this.door2.scaleY = this.lift.scaleY;

        this.door2.x = this.lift.x + this.lift.displayWidth / 2 - this.door1.displayWidth;
        this.door2.y = this.lift.y;

        this.door1X = this.door1.x;
        this.door2X = this.door2.x;



        for (let i: number = 0; i < this.smallCards.length; i++) {
            this.smallCards[i].resizeCard(this.layoutManager.smallCardScale, this.layoutManager.smallIconScale);
        }


        this.children.bringToTop(this.door1);
        this.children.bringToTop(this.door2);

    }
    hideSmallCards() {
        for (let i: number = 0; i < this.smallCards.length; i++) {
            this.smallCards[i].visible = false;
        }
    }
    showSmallCards() {
        for (let i: number = 0; i < this.smallCards.length; i++) {
            this.smallCards[i].visible = true;
        }
    }
    makeSmallCards() {
        let smallPos: PosVo[] = this.layoutManager.smallPos;

        for (let i: number = 0; i < 3; i++) {
            let card: CountCard = new CountCard(this, "apple", this.layoutManager.smallCardScale, this.layoutManager.smallIconScale);
            this.smallCards.push(card);
           // card.makeCard(i + 1);
           card.showNumber(i+1);
            card.index = i;
             card.visible=false;
            this.grid.placeAt(smallPos[i].x, smallPos[i].y, card);
            card.x -= card.displayWidth / 2;
            card.setCallback(this.chooseCard.bind(this))
        }
    }
   
    chooseCard(index: number) {
        console.log(index);
        this.hideSmallCards();
        if (index == this.right) {
            console.log("correct");
            let rightSound:Phaser.Sound.BaseSound=this.sound.add("rightSound");
            rightSound.play();
            this.showRight();
            setTimeout(() => {
                this.start();
            }, 1000);
          
        }
        else {
            console.log("Wrong");
            this.text1.setText("Try Again!");
            let wrongSound:Phaser.Sound.BaseSound=this.sound.add("wrongSound");
            wrongSound.play();
            
            this.showWrong();
            setTimeout(() => {
                this.openDoors();
            }, 1000);
            
        }
    }
    makeDeck() {
        let deck: number[] = [];

        for (let i: number = 1; i < 10; i++) {
            deck.push(i);
        }
        this.shuffle(deck);

        this.cardVals = deck.slice(0, 3);

        this.right = Math.floor(Math.random() * 3);

        for (let i: number = 0; i < 3; i++) {
            this.smallCards[i].showNumber(this.cardVals[i]);
           // this.smallCards[i].makeCard(this.cardVals[i]);
        }
        this.mainCard.itemKey=this.itemKey;
        this.mainCard.makeCard(this.cardVals[this.right]);
        this.children.bringToTop(this.door1);
        this.children.bringToTop(this.door2);
    }
    shuffle(arr: number[]) {
        for (let i: number = 0; i < 10; i++) {
            let r1: number = Math.floor(Math.random() * arr.length);
            let r2: number = Math.floor(Math.random() * arr.length);

            let temp: number = arr[r1];
            arr[r1] = arr[r2];
            arr[r2] = temp;

        }
    }
    openDoors() {
        let doorSound:Phaser.Sound.BaseSound=this.sound.add("doorSound");
        doorSound.play();
        this.bubble.visible=false;
        this.text1.visible=false;
        let tx1: number = this.door1X - this.door1.displayWidth;
        this.tweens.add({ targets: this.door1, duration: 100, x: tx1 });

        let tx2: number = this.door2X + this.door2.displayWidth;
        this.tweens.add({ targets: this.door2, duration: 100, x: tx2 });

        setTimeout(() => {
            this.closeDoors();
        }, 3000);
    }
    closeDoors() {
        let doorCloseSound:Phaser.Sound.BaseSound=this.sound.add("doorClose");
        doorCloseSound.play();

        this.tweens.add({ targets: this.door1, duration: 100, x: this.door1X });
        this.tweens.add({ targets: this.door2, duration: 100, x: this.door2X, onComplete: this.doorsClosed.bind(this) });
    }
    doorsClosed() {
        this.text1.setText("How many "+this.itemName+"\ndid you count?");
        this.bubble.visible=true;
        this.text1.visible=true;
        this.showSmallCards();
    }
    showWrong()
    {
        this.fox.play("jump");
        setTimeout(() => {
            this.fox.play("idle");
        }, 1000);
    }
    showRight()
    {
        this.fox.play("joy");
        setTimeout(() => {
            this.fox.play("idle");
        }, 1000);
    }
}