import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { GM } from "./GM";

export class LayoutManager {
    public scene: Phaser.Scene;
    

    public mainCardScale:number=0.2;
    public smallCardScale:number=0.10;
    public largeIconScale:number=0.05;
    public smallIconScale:number=0.025;
    public liftScale:number=0.28;

    public smallPos:PosVo[]=[new PosVo(3,7),new PosVo(5,7),new PosVo(7,7)];
    public mainPos:PosVo=new PosVo(5,0.5);

    private gm: GM = GM.getInstance();
    private bscene: IBaseScene;
    public callback: Function = () => { };

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.setPosScale();
        
        window.addEventListener("orientationchange", function () {
            console.log(window.innerWidth, window.innerHeight)
            this.flipped();
        }.bind(this));
    }
    private flipped() {
        console.log('flipped');
        setTimeout(() => {
            let w: number = window.innerWidth;
            let h: number = window.innerHeight;
            console.log(w,h);
            
            if (w < h) {
                this.gm.isPort = true;
            }
            else {
                this.gm.isPort = false;
            }
           
            this.bscene.resetSize(w, h);
            this.scene.scale.resize(w, h);
            this.bscene.getGrid().hide();
            this.bscene.makeGrid(11, 11);
            this.bscene.getGrid().showPos();
            this.callback();
        }, 2000);

    }
    public doLayout() {
        //this.placeAndScale("number",this.guideImage);
        //this.placeAndScale("number",this.maskImage);
    }
    private placeAndScale(item: string, obj: GameObjects.Sprite) {
        Align.scaleToGameW(obj, this.getScale(item), this.bscene);
        let pos:PosVo=this.getPos(item);
        this.bscene.getGrid().placeAt(pos.x,pos.y, obj);
    }
    public setPosScale()
    {
        this.mainCardScale=0.10;
        this.smallCardScale=0.10;
        this.largeIconScale=0.025;
        this.smallIconScale=0.025;
        this.liftScale=0.15;
    
        this.smallPos=[new PosVo(3,6),new PosVo(5,6),new PosVo(7,6)];
        this.mainPos=new PosVo(5,1);

        if (this.gm.isMobile === true && this.gm.isPort === false) {
            console.log("mobile landscape");
            // pos = 58;
            this.mainCardScale=0.10;
            this.smallCardScale=0.10;
            this.largeIconScale=0.04;
            this.smallIconScale=0.035;

            this.liftScale=0.15;
            this.smallPos=[new PosVo(2,5.5),new PosVo(5,5.5),new PosVo(8,5.5)];
            this.mainPos= new PosVo(5,0.5);
        }
        if (this.gm.isMobile === true && this.gm.isPort === true) {
           
            console.log("mobile port");

            this.mainCardScale=0.40;
            this.smallCardScale=0.25;
            this.largeIconScale=0.08;
            this.smallIconScale=0.045;

            this.liftScale=0.55;
            this.smallPos=[new PosVo(2,5.5),new PosVo(5,5.5),new PosVo(8,5.5)];
            this.mainPos= new PosVo(5,0.5);
           
        }
        if (this.gm.isTablet && this.gm.isPort === false) {
            // pos=146;
        }
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            // pos=57;
        }
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            //pos=52;
        }
    }
    public getPos(item: string) {
        let pos: PosVo = new PosVo(0,0)

        switch (item) {
            case "mainCard":
                pos = new PosVo(5,0.5)
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    // pos = 58;
                    pos.y=1;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    pos=new PosVo(5,0.5);
                }
                if (this.gm.isTablet && this.gm.isPort === false) {
                    // pos=146;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    // pos=57;
                }
                if (this.gm.isTablet === true && this.gm.isPort === true) {
                    //pos=52;
                }
                break;

            case "smallCard":

                pos = new PosVo(3,7);
                if (this.gm.isMobile === true && this.gm.isPort===false) {
                    pos=new PosVo(3,7.5);
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    pos.y=6;
                }
                break;
        }
        return pos;
    }
    public getRange(item:string)
    {
        let posArray:PosVo[]=[];

        if (item==="smallCards")
        {
            posArray=[new PosVo(3,7),new PosVo(5,7),new PosVo(7,7)];

            if (this.gm.isMobile === true && this.gm.isPort === false) {
                posArray=[new PosVo(3,7.5),new PosVo(5,7.5),new PosVo(7,7.5)];
            }
            if (this.gm.isMobile === true && this.gm.isPort === true) {
                posArray=[new PosVo(2,7),new PosVo(5,7),new PosVo(8,7)];
            }
        }


        return posArray;

    }
    public getScale(item: string) {
        let scale: number = 0;

        switch (item) {

            case "lift":
             scale=0.28;
             if (this.gm.isTablet === true && this.gm.isPort === false) {
                scale = 0.20;
            }
            if (this.gm.isTablet === true && this.gm.isPort === false) {
                scale = 0.18;
            }
            if (this.gm.isMobile === true && this.gm.isPort === false) {
                scale = 0.25;
            }
            if (this.gm.isMobile === true && this.gm.isPort === true) {
                scale = 0.5;
            }
            break;

            case "mainCard":

                scale = 0.2;
                if (this.gm.isMobile === false) {
                   // scale = 0.20;
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale = 0.18;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.40;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    scale = 0.25;
                }

                break;

                case "smallCard":

                scale = 0.1;
                if (this.gm.isMobile === false) {
                   // scale = 0.20;
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale = 0.18;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.40;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    scale = 0.25;
                }

                break;

            case "icon":

                scale = 0.05;
                if (this.gm.isMobile === false) {

                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    //scale = 0.45;
                }
                if (this.gm.isTablet === true && this.gm.isPort === true) {
                    //scale = 0.35;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    //scale = 0.4;
                }
        }

        return scale;
    }
}