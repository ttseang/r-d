import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { BaseScene } from "../scenes/BaseScene";
import { Align } from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import UIBlock from "../util/UIBlock";

export class CountCard extends UIBlock implements IGameObj {
    private scene: Phaser.Scene;
    private bscene: IBaseScene;
    private cardGrid: AlignGrid;
    private posArray: PosVo[][] = [];
    public itemKey: string;
    public value: number = 0;
    public scaleX: number = 1;
    public scaleY: number = 1;

    public cardScale: number = 0.2;
    public iconScale: number = 0.05;
    private icons: GameObjects.Image[] = [];
    private back:GameObjects.Image;
    public index:number=0;
    public text2:GameObjects.Text;
    
    constructor(bscene: IBaseScene, itemKey: string,cardScale:number,iconScale:number) {
        super();
        this.bscene = bscene;
        this.scene = bscene.getScene();

        this.cardScale=cardScale;
        this.iconScale=iconScale;

        let back: GameObjects.Image = this.scene.add.image(0, 0, "blank").setOrigin(0, 0);
        Align.scaleToGameW(back, this.cardScale, this.bscene);
        this.add(back);
        this.setSize(back.displayWidth, back.displayHeight);
        this.back=back;

        this.cardGrid = new AlignGrid(this.bscene, 7, 7, back.displayWidth, back.displayHeight);

        this.posArray.push([new PosVo(3, 3)]);
        this.posArray.push([new PosVo(2, 2), new PosVo(4, 4)]);
        this.posArray.push([new PosVo(3, 1), new PosVo(3, 3), new PosVo(3, 5)]);
        this.posArray.push([new PosVo(1, 1), new PosVo(5, 1), new PosVo(1, 4), new PosVo(5, 4)]);
        this.posArray.push([new PosVo(1, 1), new PosVo(5, 1), new PosVo(1, 4), new PosVo(5, 4), new PosVo(3, 2.5)]);
        this.posArray.push([new PosVo(1, 1), new PosVo(5, 1), new PosVo(1, 3), new PosVo(5, 3), new PosVo(1, 5), new PosVo(5, 5)]);
        this.posArray.push([new PosVo(1, 1), new PosVo(5, 1), new PosVo(1, 3), new PosVo(5, 3), new PosVo(1, 5), new PosVo(5, 5), new PosVo(3, 2)]);
        this.posArray.push([new PosVo(1, 1), new PosVo(5, 1), new PosVo(1, 3), new PosVo(5, 3), new PosVo(1, 5), new PosVo(5, 5), new PosVo(3, 2), new PosVo(3, 4)]);
        this.posArray.push([new PosVo(1, 1), new PosVo(5, 1), new PosVo(1, 3), new PosVo(5, 3), new PosVo(1, 5), new PosVo(5, 5), new PosVo(3, 1), new PosVo(3, 3), new PosVo(3, 5)]);
        // this.posArray=[[24],[16,32],[10,24,38],[8,12,29,33],[8,12,29,33,17],[8,12,29,33,22,26],[8,12,29,33,22,26,24],[8,15,22,29,12,19,26,35]];
        this.itemKey = itemKey;

        this.text2=this.scene.add.text(0,0,"0",{fontSize:"100px",color:"#000000"}).setOrigin(0.5,0.5);
        this.text2.visible=false;
        this.add(this.text2);
    }
    public setCallback(callback:Function)
    {
        this.back.setInteractive();
        this.back.on('pointerdown',()=>{callback(this.index)});

    }
    public resizeCard(backScale:number,iconScale:number)
    {
        Align.scaleToGameW(this.back,backScale,this.bscene);
        for (let i:number=0;i<this.icons.length;i++)
        {
            Align.scaleToGameW(this.icons[i],iconScale,this.bscene);
        }
    }
    public destroyMe()
    {
        while (this.icons.length > 0) {
            this.icons.pop().destroy();
        }
        this.back.destroy();
        
    }
    public showNumber(value:number)
    {
        this.text2.setText(value.toString());
        //this.text2.visible=true;
        this.text2.x=this.x+this.back.displayWidth/2;
        this.text2.y=this.y+this.back.displayHeight/2;
    }
    public makeCard(value: number) {
        this.value = value;
        let spots: PosVo[] = this.posArray[value - 1];
        console.log(spots);
        let remake: boolean = false;
        if (this.icons.length > 0) {
            remake = true;
        }
        while (this.icons.length > 0) {
            this.icons.pop().destroy();
        }
        for (let i: number = 0; i < spots.length; i++) {
            let icon: GameObjects.Image = this.scene.add.image(0, 0, this.itemKey);
            Align.scaleToGameW(icon, this.iconScale, this.bscene);
            this.cardGrid.placeAt(spots[i].x, spots[i].y, icon);
            this.icons.push(icon);

            if (remake == true) {
                icon.x += this.x;
                icon.y += this.y;
            }
            this.add(icon);
            if (this.visible == false) {
                icon.visible = false;
            }
        }
    }
}