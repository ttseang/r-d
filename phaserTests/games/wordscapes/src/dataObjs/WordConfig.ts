export class WordConfig
{
    public word:string;
    public dir:string;
    public xPos:number;
    public yPos:number;

    constructor(word:string,dir:string="h",xPos:number=0,yPos:number=0)
    {
        this.word=word;
        this.dir=dir;
        this.xPos=xPos;
        this.yPos=yPos;
    }
}