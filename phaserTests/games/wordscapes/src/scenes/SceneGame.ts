import { BoxCollection } from "../dataObjs/BoxCollection";
import { WordConfig } from "../dataObjs/WordConfig";
import { LetterBox } from "../ui/LetterBox";
import { LetterSpot } from "../ui/LetterSpot";
import { WordPad } from "../ui/WordPad";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneGame extends BaseScene {

    private word: string = "";
    private collections:BoxCollection[]=[];

    private spotLine: Phaser.GameObjects.Graphics;

    private wordPad: WordPad;
    private wordCount:number=0;

    constructor() {
        super("SceneGame");
    }
    preload() {
        this.load.image('background', "./assets/backs/3.jpg");
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("face", "./assets/face.png");
        this.load.image("circle", "./assets/circle.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        let bg = this.add.image(0, 0, 'background');
        Align.scaleToGameW(bg, 1.1, this);
        Align.center(bg, this);
        //
        //
        //
        this.spotLine = this.add.graphics();


        // this.grid.showNumbers();
        //
        //
        //
      //  let levelString: string = '{"word":"ear","wordConfigs":[{"word":"are","dir":"v","xPos":4,"yPos":2},{"word":"ear","dir":"h","xPos":4,"yPos":4},{"word":"era","dir":"v","xPos":6,"yPos":3}]}';
      //let levelString:string='{"word":"lake","wordConfigs":[{"word":"ale","dir":"v","xPos":5,"yPos":2},{"word":"lea","dir":"h","xPos":5,"yPos":3},{"word":"lake","dir":"h","xPos":2,"yPos":4}]}';
      // let levelString:string='{"word":"ahead","wordConfigs":[{"word":"aha","dir":"v","xPos":4,"yPos":3},{"word":"ahead","dir":"h","xPos":4,"yPos":3},{"word":"had","dir":"v","xPos":8,"yPos":1},{"word":"head","dir":"h","xPos":2,"yPos":5}]}';
       let levelString:string='{"word":"aspect","wordConfigs":[{"word":"ace","dir":"h","xPos":3,"yPos":3},{"word":"seat","dir":"v","xPos":5,"yPos":2},{"word":"sept","dir":"h","xPos":5,"yPos":2},{"word":"tape","dir":"h","xPos":5,"yPos":5},{"word":"pet","dir":"v","xPos":8,"yPos":0},{"word":"cat","dir":"v","xPos":3,"yPos":2},{"word":"pea","dir":"v","xPos":7,"yPos":5}]}';
      //let levelString:string='{"word":"banking","wordConfigs":[{"word":"bag","dir":"v","xPos":7,"yPos":2},{"word":"ban","dir":"h","xPos":7,"yPos":2},{"word":"banking","dir":"h","xPos":1,"yPos":4},{"word":"king","dir":"v","xPos":4,"yPos":4},{"word":"nan","dir":"v","xPos":9,"yPos":2},{"word":"nab","dir":"h","xPos":4,"yPos":6}]}';
      
      let levelData: any = JSON.parse(levelString);


        for (let i: number = 0; i < levelData.wordConfigs.length; i++) {
            let data: any = levelData.wordConfigs[i];


            let wordConfig: WordConfig = new WordConfig(data.word, data.dir, data.xPos, data.yPos);
            this.addWord(wordConfig);
        }
        this.word = levelData.word;


        this.wordPad = new WordPad(this, this.word);

        // 
        this.wordPad.showLetters();
        this.grid.placeAtIndex(80, this.wordPad);
        this.wordPad.fixSpots();
        this.wordPad.callback=this.checkWord.bind(this);
    }

    checkWord(word:string)
    {  
      //  console.log(word);
        for(let i:number=0;i<this.collections.length;i++)
        {
            let collection:BoxCollection=this.collections[i];
            if (collection.word===word)
            {
                if (collection.used==false)
                {
                    collection.used=true;
                    this.turnOverLetters(collection);
                    this.wordCount++;
                }               
            }
        }
        if (this.wordCount===this.collections.length)
        {
            console.log("Next Level");
        }
    }
    turnOverLetters(collection:BoxCollection)
    {
        for(let i:number=0;i<collection.boxes.length;i++)
        {
            let box:LetterBox=collection.boxes[i];
            this.children.bringToTop(box);
            box.showLetter(true);
        }
    }
    addWord(wordConfig: WordConfig) {

        let word: string = wordConfig.word;
        let dir: string = wordConfig.dir;
        let xx: number = wordConfig.xPos;
        let yy: number = wordConfig.yPos;
        //
        //
        let collection:BoxCollection=new BoxCollection(word);
        this.collections.push(collection);
        //
        //
        for (let i: number = 0; i < word.length; i++) {
            let letter = word[i];
            let box: LetterBox = new LetterBox(this, letter, 0x27ae60);
            collection.boxes.push(box);

            box.showLetter(false);
            this.grid.placeAt(xx, yy, box);

            if (dir === "h") {
                xx++;
            }
            else {
                yy++;
            }
        }
    }

}