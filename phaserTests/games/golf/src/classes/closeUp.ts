import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class CloseUp extends Phaser.GameObjects.Container
{
    private ball:GameObjects.Image;
    public back:GameObjects.Image;
    private hole:GameObjects.Image;

    

    private mholder:GameObjects.Image;
    private dropFlag:boolean=false;

    public scene:Phaser.Scene;
    private bscene:IBaseScene;

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());

        this.scene=bscene.getScene();
        this.bscene=bscene;

        

        this.back=this.scene.add.image(0,0,"closegrass");
        this.hole=this.scene.add.image(0,0,"closehole");
        this.ball=this.scene.add.image(0,0,"closeball");

        
        this.add(this.back);
        this.add(this.hole);
        this.add(this.ball);

        

        //this.ball.y=this.back.displayHeight*0.3;
        
        Align.scaleToGameW(this.ball,0.2,this.bscene);

        Align.scaleToGameW(this.back,.5,this.bscene);
      
        //console.log(this.back.displayWidth,this.back.displayHeight);

        this.hole.scaleX=this.back.scaleX;
        this.hole.scaleY=this.back.scaleY;

        this.hole.y=this.back.displayHeight*0.1;
        
       

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);
    }
    public dropBall()
    {
        if (this.dropFlag==true)
        {
            return;
        }
        this.dropFlag=true;
        this.scene.tweens.add({targets:[this.ball],duration:500,x:0,y:0,onComplete:this.sinkBall.bind(this)});
    }
    private sinkBall()
    {
        this.mholder=this.scene.add.image(0,0,"holder").setOrigin(0.5,0.5);
       // this.mholder.y=-this.displayHeight*0.2;
        this.mholder.displayHeight=this.back.displayHeight/2;
        this.mholder.displayWidth=this.back.displayWidth;

        this.setMask(null);

     //   this.add(this.mholder);
        this.mholder.x=this.x;
        this.mholder.y=this.y-21;
        this.mholder.scaleX=this.scaleX;
        this.mholder.scaleY=this.scaleY;
        //
        this.ball.setMask(new Phaser.Display.Masks.BitmapMask(this.scene,this.mholder));
        this.mholder.visible=false;      
        this.scene.tweens.add({targets:[this.ball],duration:250,x:0,y:this.displayHeight,onComplete:()=>{}});
    }
    public setBallPos(xx:number,yy:number)
    {
        yy+=this.ball.displayHeight*0.1;

        let y2:number=yy+this.back.displayHeight/2;

        //console.log(y2);

        let s:number=(y2/250);
       // console.log(s);

        this.ball.x=xx;
        this.ball.y=yy;
        this.ball.scaleY=s;
        this.ball.scaleX=this.ball.scaleY;
    }
}