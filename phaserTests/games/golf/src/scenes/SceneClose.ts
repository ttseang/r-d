import { CloseUp } from "../classes/closeUp";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneClose extends BaseScene
{
    private closeUp:CloseUp;

    constructor()
    {
        super("SceneClose");
    }
    preload()
    {
        this.load.image("closeball","./assets/close/closeball.png");
        this.load.image("closegrass","./assets/close/closegrass.png");
        this.load.image("closehole","./assets/close/closehole2.png");

    }
    create()
    {
        super.create();

        this.closeUp=new CloseUp(this);
        this.closeUp.x=400;
        this.closeUp.y=200;
        this.closeUp.setBallPos(0,0);
        
        window['scene']=this;
        
    }
}