import { GameObjects, Physics } from "phaser";
import { CloseUp } from "../classes/closeUp";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private startPoint: PosVo | undefined;
    private currentPoint: PosVo | undefined;
    private triangle: GameObjects.Image;
    private lines: GameObjects.Graphics;

    private ball: Physics.Arcade.Sprite;
    private shootDist: number = 0;
    private green: Physics.Arcade.Sprite;

    private layer: Phaser.Tilemaps.TilemapLayer;
  
    private collideLayer: Phaser.Tilemaps.TilemapLayer;
    private clickLock: boolean = false;
    private ballMoving: boolean = false;

    private holePos:PosVo=new PosVo(0,0);

    private lastDist:number=0;
    private closeUp:CloseUp;
    private imageFrame:GameObjects.Image;
    private closeFlag:boolean=false;

    constructor() {
        super("SceneMain");
    }
    preload() {

        this.load.image("bg", "./assets/bg.jpg");
        this.load.image("ball", "./assets/ball.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("holder", "./assets/holder.jpg");

        this.load.audio("putter","./assets/audio/putter.wav");
        this.load.audio("inthehole","./assets/audio/inthehole.mp3");
        this.load.audio("clapping","./assets/audio/golf_clap2.wav");
        this.load.audio("bounce","./assets/audio/bounce.wav");
        this.load.audio("aww","./assets/audio/aww.mp3");
        this.load.audio("tada","./assets/audio/tada.wav");

        this.load.image("frame","./assets/close/frame.png");

        this.load.image("closeball","./assets/close/closeball.png");
        this.load.image("closegrass","./assets/close/closegrass.png");
        this.load.image("closehole","./assets/close/closehole2.png");

        this.load.image("golftiles", "./assets/golftiles.png");
        this.load.tilemapTiledJSON("golf1", "./assets/golf2.json");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        let bg: GameObjects.Image = this.add.image(0, 0, "bg");
        bg.displayWidth = this.gw;
        bg.displayHeight = this.gh;
        Align.center(bg, this);

        this.ball = this.physics.add.sprite(100, 200, "ball");

        Align.scaleToGameW(this.ball, 0.01, this);

        // this.grid.showPos();
        this.grid.placeAt(7, 3, this.ball);

        this.triangle = this.add.image(0, 0, "triangle");
        Align.scaleToGameW(this.triangle, 0.02, this);
        this.triangle.setTint(0xf0932b);
        this.triangle.visible = false;

        this.lines = this.add.graphics();

        this.makeMap();

        /*  this.green=this.physics.add.sprite(0,0,"green").setOrigin(0,0);
         this.green.displayWidth=this.gw*0.8;
         this.green.displayHeight=this.gh*0.5;
         
         this.grid.placeAt(0,2,this.green); */

        this.children.bringToTop(this.ball);


        // this.children.sendToBack(this.green);


        this.ball.setDrag(0.7, 0.7);
        this.ball.setDamping(true);
        window['scene'] = this;
        /*   this.ball.setFriction(0.5,0.5);
          this.green.setImmovable(true);
          this.green.setFriction(1,1);
          this.physics.add.collider(this.green,this.ball); */

        this.ball.setBounce(0.9, 0.9);

        this.children.sendToBack(this.layer);
        this.children.sendToBack(bg);

        this.physics.add.collider(this.ball, this.collideLayer,this.hitwall.bind(this));
        this.physics.add.overlap(this.layer, this.ball);

        this.imageFrame=this.add.image(0,0,"frame");

        this.closeUp=new CloseUp(this);
        this.closeUp.x=200;
        this.closeUp.y=100;
        this.closeUp.setBallPos(0,0);
        Align.scaleToGameW(this.closeUp,0.2,this);

        this.grid.placeAt(6,6,this.closeUp);
        this.grid.placeAt(6,6,this.imageFrame);
        this.imageFrame.displayWidth=this.closeUp.displayWidth*1.1;
        this.imageFrame.displayHeight=this.closeUp.displayHeight*1.1;

        let hmask:GameObjects.Image=this.add.image(this.closeUp.x,this.closeUp.y,"holder");
        hmask.displayWidth=this.closeUp.displayWidth;
        hmask.displayHeight=this.closeUp.displayHeight;
        this.closeUp.setMask(new Phaser.Display.Masks.BitmapMask(this,hmask));
        hmask.visible=false;


       let hole= this.layer.filterTiles((tile:Phaser.Tilemaps.Tile)=>{
            return (tile.index===3);
        })
        this.holePos=new PosVo(hole[0].getCenterX(),hole[0].getCenterY());

        this.input.on("pointerdown", this.makeStartPoint.bind(this));

        this.setClosePos();
    }
    hitwall()
    {
        this.playSound("bounce");
    }
    playSound(sound:string)
    {
       let audio:Phaser.Sound.BaseSound=this.sound.add(sound); 
       audio.play();
    }
    inTheHole(sprite: Physics.Arcade.Sprite, tile: Phaser.Tilemaps.Tile) {
        if (this.ballMoving==false)
        {
            return;
        }       

        // tile.alpha=.2;
        let distX: number = Math.abs(tile.getCenterX() - this.ball.x);
        let distY: number = Math.abs(tile.getCenterY() - this.ball.y);

        this.closeFlag=true;
        console.log("CLOSE!");
        //console.log(distX,distY);

        if (distX < this.ball.displayWidth / 2 && distY < this.ball.displayHeight / 2) {
            
          
            this.ballMoving=false;
           
            this.tweens.add({ targets: this.ball, duration: 200, x: tile.getCenterX(), y: tile.getCenterY(),scaleX:0,scaleY:0, onComplete: this.levelComplete.bind(this) })
            this.closeUp.dropBall();
            setTimeout(() => {
                this.playSound("inthehole");
            }, 750);
            setTimeout(() => {
                this.playSound("tada");
                this.playSound("clapping");
            }, 1000);
        }
        
    }
    levelComplete() {
        this.ball.setVelocity(0, 0);
        this.ball.visible = false;
        this.ballMoving = false;
    }
    makeMap() {
        let map = this.make.tilemap({ key: "golf1", tileWidth: 64, tileHeight: 64 });
        let tileset = map.addTilesetImage("ground", "golftiles");
        this.layer = map.createLayer("Tile Layer 1", tileset, 0, 0);
        this.collideLayer = map.createLayer("bounds", tileset, 0, 0);
        this.collideLayer.setCollisionBetween(3, 6);

        this.layer.setTileIndexCallback([3], this.inTheHole.bind(this), this);


        Align.scaleToGameW(this.layer, 0.5, this);
        Align.scaleToGameW(this.collideLayer, 0.5, this);
        Align.center2(this.layer, this);
        Align.center2(this.collideLayer, this);
        if (this.layer.displayHeight < this.gh) {
            Align.scaleToGameH(this.layer, 0.9, this);
            Align.scaleToGameH(this.collideLayer, 0.9, this);
            Align.center2(this.layer, this);
            Align.center2(this.collideLayer, this);
            /*  this.layer.height=this.gh;
             this.layer.scaleX=this.layer.scaleY;
             this.collideLayer.height=this.gh;
             this.collideLayer.scaleX=this.collideLayer.scaleY; */
        }

    }
    makeStartPoint(p: Phaser.Input.Pointer) {
        if (this.clickLock == true) {
            return;
        }
        this.startPoint = new PosVo(this.ball.x, this.ball.y);
        /*  this.triangle.setPosition(p.x, p.y);
         this.triangle.visible = true;
         this.startPoint = new PosVo(p.x, p.y); */
        this.input.on("pointermove", this.onMove.bind(this));
        this.input.once("pointerup", this.onUp.bind(this));
    }
    onMove(p: Phaser.Input.Pointer) {

        //
        //
        //

        let xLimitL: number = this.ball.x - this.gw * 0.2;
        let xLimitR: number = this.ball.x + this.gw * 0.2;

        let yLimitT: number = this.ball.y - this.gh * 0.2;
        let yLimitB: number = this.ball.y + this.gh * 0.2;

        let xx: number = p.x;
        let yy: number = p.y;

        if (xx < xLimitL) {
            xx = xLimitL;
        }
        if (xx > xLimitR) {
            xx = xLimitR;
        }
        if (yy < yLimitT) {
            yy = yLimitT;
        }
        if (yy > yLimitB) {
            yy = yLimitB;
        }
        //
        //
        //
        this.triangle.setPosition(this.ball.x, this.ball.y);
        this.triangle.visible = true;

        //
        //
        //
        this.currentPoint = new PosVo(xx, yy);


        let angle: number = this.getAngle(this.startPoint, this.currentPoint);
        this.triangle.setAngle(angle);
        //  console.log(angle);



        this.lines.clear();
        this.lines.lineStyle(4, 0xf0932b);
        this.lines.moveTo(this.startPoint.x, this.startPoint.y);
        this.lines.lineTo(this.currentPoint.x, this.currentPoint.y);
        this.lines.stroke();
        // this.triangle.setAngle(angle);
    }
    onUp(p: Phaser.Input.Pointer) {

        //console.log(this.currentPoint);
        if (this.currentPoint === undefined) {
            return;
        }
        this.clickLock = true;
      

        this.shootDist = this.getDist(this.startPoint, this.currentPoint) * 2;

        //  console.log(this.shootAngle,this.shootDist);

        this.input.off("pointermove");


        this.lines.clear();
        this.triangle.visible = false;

        this.ballMoving = true;
        this.playSound("putter");
        this.physics.moveTo(this.ball, this.currentPoint.x, this.currentPoint.y, this.shootDist);
    }
    getDist(obj1: PosVo, obj2: PosVo) {
        let xDist: number = Math.abs(obj1.x - obj2.x);
        let yDist: number = Math.abs(obj1.y - obj2.y);
        let dist: number = Math.sqrt((xDist * xDist) + (yDist * yDist));
        return dist;
    }
    getAngle(obj1: PosVo, obj2: PosVo) {
        //I use the offset because the ship is pointing down
        //at the 6 o'clock position
        //set to 0 if your sprite is facing 3 o'clock
        //set to 180 if your sprite is facing 9 o'clock
        //set to 270 if your sprite is facing 12 o'clock
        //
        let offSet: number = 0;
        // angle in radians
        var angleRadians = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
        // angle in degrees
        let angleDeg = (Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x) * 180 / Math.PI);
        //add the offset
        angleDeg += offSet;
        return angleDeg;
    }
    updateBall(xx:number,yy:number)
    {
        this.ball.x=xx;
        this.ball.y=yy;
        this.setClosePos();
    }
    setClosePos()
    {
        let distX2:number=this.ball.x-this.holePos.x;
        let distY2:number=this.ball.y-this.holePos.y;
        this.closeUp.setBallPos(distX2*10,distY2*5);
    }
    update() {
        //    console.log(this.ball.body.velocity);
        if (this.ballMoving === true) {
           
            this.setClosePos();

            if (this.closeFlag===true)
            {
                let distX2:number=Math.abs(this.ball.x-this.holePos.x);
                let distY2:number=Math.abs(this.ball.y-this.holePos.y);
                console.log(distX2,distY2);

                if (distX2>100 || distY2>100)
                {
                    this.closeFlag=false;
                    this.playSound("aww");
                }
            }
            
            if (Math.abs(this.ball.body.velocity.x) < 20 && Math.abs(this.ball.body.velocity.y) < 20) {
                this.ballMoving = false;
                this.ball.setVelocity(0, 0);
                this.triangle.setPosition(this.ball.x, this.ball.y);
                this.triangle.visible = true;
                this.triangle.angle = 0;
                this.clickLock = false;

            }
        }
    }
}