import { GM } from "../classes/GM";
import { LayoutDefs } from "../classes/layoutDefs";
import { CardImageVo } from "../dataObjs/CardImageVo";
import { BaseScene } from "./BaseScene";

export class SceneData extends BaseScene
{
    private file:string="coins";
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneData");
    }
    preload()
    {

    }
    create() {

        fetch("./assets/json/"+this.gm.file+".json")
        .then(response => response.json())
        .then(data => this.process({ data }));
    }
    process(data:any)
    {
        //console.log(data);
        let imageVos:CardImageVo[] = [];

        let images:any=data.data.images;
        for(let i=0;i<images.length;i++){
            let cardImageVo:CardImageVo=new CardImageVo(images[i].key,images[i].scale*this.gm.iconMultipli);
            //console.log(cardImageVo);
            imageVos.push(cardImageVo);
        }
        this.gm.cardImageVos=imageVos;
        this.scene.start("SceneMain");
    }
}