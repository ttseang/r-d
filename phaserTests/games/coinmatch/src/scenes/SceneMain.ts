import { FlipCard } from "../classes/flipCard";
import { GM } from "../classes/GM";
import { CardImageVo } from "../dataObjs/CardImageVo";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private coinScales: number[] = [];
    private imageDefs: CardImageVo[] = [];
    private deck: number[] = [];
    private cards: FlipCard[] = [];
    private clickLock: boolean = true;
    private selectedCard: FlipCard | null = null;
    private matches: number = 0;
    private gm: GM = GM.getInstance();

    private cardScale: number = 0.1;
    private imageMulti: number = 1;

    constructor() {
        super("SceneMain");
    }
    preload() {



        this.imageDefs = this.gm.cardImageVos;

        this.load.setPath("./assets/");

        this.load.image("button", "ui/buttons/1/1.png");


        this.imageDefs.forEach((def: CardImageVo) => {
            this.load.image(def.key, "cardImages/" + this.gm.file + "/" + def.key + ".png");
        })


        this.load.image("box", "box.png");
        this.load.image("bg", "bg.jpg");
        this.load.image("holder", "holder.jpg");
        this.load.image("back", "back.png");
        this.load.image("selectedback", "selectedback.png");

        this.load.audio("coin", "quiz_right.wav");
        this.load.audio("wrong", "quiz_wrong.wav");
        this.load.audio("select", "click.wav");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
      //  this.grid.showPos();

        this.cards = [];
        // this.imageDefs=this.gm.cardImageVos;

        this.matches = 0;
        this.makeDeck();
        this.makeCards();

        setTimeout(() => {
            this.flipBack();
            this.clickLock = false;;
        }, 2000);

        this.input.on("gameobjectdown", this.selectCard.bind(this));
    }
    selectCard(p: Phaser.Input.Pointer, card: FlipCard) {
        if (this.clickLock === true) {
            return;

        }
        if (card.matched === true) {
            return;
        }
        if (this.selectedCard === card) {
            /* this.selectedCard.flipToBack();
            this.selectedCard=null; */
            return;
        }
        if (this.selectedCard == null) {
            this.selectedCard = card;
            this.playSound("select");
            this.selectedCard.flipToFront();
            return;
        }
        this.playSound("select");
        this.clickLock = true;
        card.flipToFront();
        if (card.cardData.key == this.selectedCard.cardData.key) {

            card.setMatched();
            this.selectedCard.setMatched();
            this.selectedCard = null;
            this.clickLock = false;
            this.matches++;
            this.playSound("coin");
            if (this.matches == 3) {
                this.clickLock = true;
                setTimeout(() => {
                    this.flyAwayCards();
                }, 500);
                setTimeout(() => {
                    this.scene.restart();
                }, 2000);
            }
            return;
        }
        else {
            this.playSound("wrong");

            setTimeout(() => {
                this.flipFront();
                setTimeout(() => {
                    this.flipBack();
                    this.selectedCard = null;
                    this.clickLock = false;
                }, 2000);
            }, 1000);
        }
    }
    playSound(sound: string) {
        let audio: Phaser.Sound.BaseSound = this.sound.add(sound);
        audio.play();
    }
    makeDeck() {
        let deck: number[] = [];
        for (let i: number = 0; i < this.imageDefs.length; i++) {
            deck.push(i);
        }

        for (let j: number = 0; j < 30; j++) {
            let p1: number = Math.floor(Math.random() * deck.length);
            let p2: number = Math.floor(Math.random() * deck.length);

            let t: number = deck[p1];
            deck[p1] = deck[p2];
            deck[p2] = t;
        }
        this.deck = deck;
    }
    makeCards() {
        //  let pos: PosVo[] = [new PosVo(3, 3), new PosVo(5, 3), new PosVo(7, 3), new PosVo(3, 6), new PosVo(5, 6), new PosVo(7, 6)];
        let pos: PosVo[] = this.gm.pos.slice();

        for (let i: number = 0; i < 3; i++) {

            let cardImageVo: CardImageVo = this.imageDefs[this.deck[i]];

            for (let j: number = 0; j < 2; j++) {
                let posVo: PosVo = pos.shift();

                let card: FlipCard = new FlipCard(this, cardImageVo, this.gm.cardScale);
                this.grid.placeAt(posVo.x, posVo.y, card);
                this.cards.push(card);
            }
        }
        this.shuffleCards();
    }
    shuffleCards() {
        for (let i: number = 0; i < 20; i++) {
            let r1: number = Math.floor(Math.random() * this.cards.length);
            let r2: number = Math.floor(Math.random() * this.cards.length);

            let card1: FlipCard = this.cards[r1];
            let card2: FlipCard = this.cards[r2];

            let pos: PosVo = new PosVo(card1.x, card1.y);

            card1.x = card2.x;
            card1.y = card2.y;

            card2.x = pos.x;
            card2.y = pos.y;
        }
    }
    flyAwayCards() {
        this.cards.forEach((card: FlipCard) => {
            card.flyUp();
        })
    }
    flipFront(): void {
        this.cards.forEach((card: FlipCard) => {
            card.flipToFront();
        })
    }
    flipBack(): void {
        this.cards.forEach((card: FlipCard) => {
            card.flipToBack();
        })
    }
}