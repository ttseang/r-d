import { GM } from "../classes/GM";
import { LayoutDefs } from "../classes/layoutDefs";
import { SelectCard } from "../classes/selectCard";
import { BaseScene } from "./BaseScene";

export class SceneSelect extends BaseScene {
    private cats: string[] = ["coins", "fruits", "farm"];
    private gm: GM = GM.getInstance();
    constructor() {
        super("SceneSelect");
    }
    preload() {
        for (let i: number = 0; i < this.cats.length; i++) {
            let cat: string = this.cats[i];
            this.load.image("select" + cat, "./assets/selectImages/" + cat + ".png");

        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        //this.grid.showPos();


        let layoutDefs:LayoutDefs=new LayoutDefs();
        layoutDefs.setDefs();

        for (let i: number = 0; i < this.cats.length; i++) {
            let selectCard: SelectCard = new SelectCard(this, "select" + this.cats[i], i);
            this.grid.placeAt(this.gm.selectStart + i * 3, 4, selectCard);
            selectCard.setInteractive();
           
        }
        this.input.on("gameobjectdown", this.selectCard.bind(this));
    }
    selectCard(p: Phaser.Input.Pointer, card: SelectCard) {
       // console.log(card);
        console.log(this.cats[card.index]);
        this.gm.file=this.cats[card.index];
        this.scene.start("SceneData");
    }
}