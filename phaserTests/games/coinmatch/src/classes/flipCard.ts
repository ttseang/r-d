import { GameObjects } from "phaser";
import { CardImageVo } from "../dataObjs/CardImageVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class FlipCard extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private back:GameObjects.Image;
    private selectedBack:GameObjects.Image;

    private frontImage:GameObjects.Image;


    private flipTween:Phaser.Tweens.Tween;
    private flip2Tween:Phaser.Tweens.Tween;

    private flipZoom:number=1.5;
    private flipSpeed:number=200;
    public showFront:boolean=true;
    public cardData:CardImageVo;
    public matched:boolean=false;

    constructor(bscene:IBaseScene,cardImageVo:CardImageVo,cardScale:number=0.1)
    {
        super(bscene.getScene());

        this.scene=bscene.getScene();

        this.cardData=cardImageVo;
        
        this.back=this.scene.add.image(0,0,"back");
        Align.scaleToGameW(this.back,cardScale,bscene);
        this.add(this.back);

        this.selectedBack=this.scene.add.image(0,0,"selectedback");
        this.selectedBack.displayHeight=this.back.displayHeight;
        this.selectedBack.displayWidth=this.back.displayWidth;

        this.add(this.selectedBack);
        this.selectedBack.visible=false;

        this.frontImage=this.scene.add.image(0,0,cardImageVo.key);
        Align.scaleToGameW(this.frontImage,cardImageVo.scale,bscene);
        this.add(this.frontImage);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();

        this.scene.add.existing(this);
        //this.on("pointerdown",this.flip.bind(this));
    }
    setMatched(){
        this.matched=true;
        this.back.visible=false;
        this.selectedBack.visible=true;
    }
    flip()
    {
        this.flipTween=this.scene.tweens.add({targets: this,duration: this.flipSpeed/2,scaleX:0,scaleY:this.flipZoom,onComplete:this.tweenDone.bind(this)});
        
    }
    tweenDone()
    {
        this.showFront=!this.showFront;
        this.frontImage.visible=this.showFront;
        this.flip2Tween=this.scene.tweens.add({targets: this,duration: this.flipSpeed/2,scaleX:1,scaleY:1});
    }
    flyUp()
    {
        this.scene.tweens.add({targets: this,duration: 500,y:-100});
    }
    flipToFront()
    {
        if (this.showFront===false && this.matched==false)
        {
            this.flip();
        }
    }
    flipToBack()
    {
        if (this.showFront===true && this.matched==false)
        {
            this.flip();
        }
    }

}