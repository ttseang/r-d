import { PosVo } from "../dataObjs/PosVo";
import { GM } from "./GM";

export class LayoutDefs
{
    private gm: GM = GM.getInstance();

    constructor()
    {

    }
    public setDefs(): void{
        if (this.gm.isMobile==true)
        {
            if (this.gm.isTablet==true)
            {
                if (this.gm.isPort==true)
                {
                    this.gm.cardScale=0.3;
                    this.gm.iconMultipli=2.5;
                    this.gm.selectStart=2;
                    this.gm.pos=[new PosVo(1.5, 3), new PosVo(5, 3), new PosVo(8.5, 3), new PosVo(1.5, 6), new PosVo(5, 6), new PosVo(8.5, 6)];
                
                }
                else
                {
                    this.gm.cardScale=0.2;
                    this.gm.iconMultipli=2;
                    this.gm.selectStart=2;
                    this.gm.pos=[new PosVo(1.5, 2), new PosVo(5, 2), new PosVo(8.5, 2), new PosVo(1.5, 6), new PosVo(5, 6), new PosVo(8.5, 6)];
                
                }
            }
            else
            {
                if (this.gm.isPort==true)
                {
                    this.gm.cardScale=0.3;
                    this.gm.iconMultipli=2.5;
                    this.gm.selectStart=2;
                    this.gm.pos=[new PosVo(1.5, 3), new PosVo(5, 3), new PosVo(8.5, 3), new PosVo(1.5, 6), new PosVo(5, 6), new PosVo(8.5, 6)];
                }
                else
                {
                    this.gm.cardScale=0.2;
                    this.gm.iconMultipli=2;
                    this.gm.selectStart=2;
                    this.gm.pos=[new PosVo(1.5, 2), new PosVo(5, 2), new PosVo(8.5, 2), new PosVo(1.5, 6), new PosVo(5, 6), new PosVo(8.5, 6)];
                
                }
            }
        }
    }
}