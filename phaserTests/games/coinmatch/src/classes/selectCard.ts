import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";

export class SelectCard extends GameObjects.Image
{
    public scene:Phaser.Scene;
    public index:number;

    constructor(bscene:IBaseScene,key:string,index:number)
    {
        super(bscene.getScene(),0,0,key);
        this.scene=bscene.getScene();
        this.index=index;
        Align.scaleToGameW(this,0.2,bscene);
        this.scene.add.existing(this);
    }
}