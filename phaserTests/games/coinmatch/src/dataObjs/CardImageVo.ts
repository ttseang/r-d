export class CardImageVo
{
    public key:string;
    public scale:number;

    constructor(key:string,scale:number)
    {
        this.key=key;
        this.scale=scale;
    }
}