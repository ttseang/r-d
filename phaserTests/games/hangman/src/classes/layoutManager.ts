import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { GM } from "./GM";

export class LayoutManager {
    public scene: Phaser.Scene;
    public guideImage: GameObjects.Sprite;
    public maskImage: GameObjects.Sprite;
    private gm: GM = GM.getInstance();
    private bscene: IBaseScene;
    public callback: Function = () => { };

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;

        window.addEventListener("orientationchange", function () {
           // console.log(window.visualViewport.width, window.visualViewport.height)
            this.flipped();
        }.bind(this));
    }
    private flipped() {
        console.log('flipped');
        setTimeout(() => {
            let w: number = 0;;
            let h: number = 0;

            if (window['visualViewport']) {
                w = window.visualViewport.width;
                h = window.visualViewport.height;
            }
            else {
                w = window.innerWidth;
                h = window.innerHeight;
            }

            if (w < h) {
                this.gm.isPort = true;
            }
            else
            {
                this.gm.isPort=false;
            }
            this.bscene.resetSize(w,h);
            this.scene.scale.resize(w, h);
            this.bscene.getGrid().hide();
            this.bscene.makeGrid(11, 7);
            this.callback();
        }, 1000);

    }
    public doLayout() {
        //this.placeAndScale("number",this.guideImage);
        //this.placeAndScale("number",this.maskImage);
    }
    
    public getPos(item: string) {
        let pos: PosVo=new PosVo(0,0);

        switch (item) {
            case "button":
                if (this.gm.isPort===true)
                {
                    pos=new PosVo(3,8);
                }
                else
                {
                    pos=new PosVo(3,8);
                }
                break;
            case "apple":
                //pos = 100;
                pos=new PosVo(3,3);

                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    //pos = 58;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    //pos = 52;
                    ////pos = -1;
                }
                if (this.gm.isTablet && this.gm.isPort===false)
                {
                    //pos=146;
                }
                if (this.gm.isTablet===true && this.gm.isPort===false)
                {
                    //pos=57;
                }
                if (this.gm.isTablet===true && this.gm.isPort===true)
                {
                    //pos=52;
                }
                break;

            
        }
        return pos;
    }
    public getScale(item: string) {
        let scale: number = 0;

        switch (item) {
            case "spider":

                scale = 0.25;
                if (this.gm.isMobile === false) {
                    scale = 0.10;
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale = 0.10;
                }
                if (this.gm.isMobile===true && this.gm.isPort===true)
                {
                    scale=0.2;
                }
                if (this.gm.isTablet===true && this.gm.isPort===false)
                {
                    scale=0.1;
                }
                
                break;

                case "apple":

                scale = 0.5;
                if (this.gm.isMobile === false) {
                    scale = 0.10;
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale = 0.1;
                }
                if (this.gm.isMobile===true && this.gm.isPort===true)
                {
                    scale=0.2;
                }
                if (this.gm.isTablet===true && this.gm.isPort===false)
                {
                    scale=0.1;
                }1
                break;

                case "dog":

                scale=0.25;
                if (this.gm.isMobile === false) {
                    
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    
                }
                if (this.gm.isMobile===true && this.gm.isPort===true)
                {
                    scale=0.45;
                }
                if (this.gm.isTablet===true && this.gm.isPort===true)
                {
                    scale=0.35;
                }
                if (this.gm.isTablet===true && this.gm.isPort===false)
                {
                    scale=0.4;
                }
        }

        return scale;
    }
}