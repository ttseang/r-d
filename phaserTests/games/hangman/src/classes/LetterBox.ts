import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { AlignGrid } from "../util/alignGrid";

export class LetterBox extends GameObjects.Sprite {
    private bscene: IBaseScene;
    public scene: Phaser.Scene;
    private text1: GameObjects.Text;
    public row: number = 0;
    public col: number = 0;
    public letter:string="";
    private callback:Function=()=>{};
    public correct:boolean=false;

    constructor(bscene: IBaseScene, letter: string, color: number) {
        super(bscene.getScene(), 0, 0, "rect");
        this.scene = bscene.getScene();
        this.bscene = bscene;

        this.letter=letter;
        this.text1 = this.scene.add.text(0, 0, letter, { fontSize: "26px", color: "#ffffff" }).setOrigin(0.5, 0.5);

        this.displayHeight = this.bscene.ch;
        this.displayWidth = this.bscene.cw;

        this.setTint(color);
        this.scene.add.existing(this);
    }
    setCallback(callback:Function)
    {
        this.setInteractive();
        this.callback=callback;
        this.off('pointerdown');

        this.on('pointerdown',()=>{this.callback(this)});
    }
    hideLetter()
    {
        this.text1.setText("-");
    }
    showLetter()
    {
        this.text1.setText(this.letter);
    }
    setPos(col: number, row: number) {
        let grid: AlignGrid = this.bscene.getGrid();
        grid.placeAt(col, row, this);
        this.row = row;
        this.col = col;
        this.updateTextPos();
    }
    updatePos() {
        let grid: AlignGrid = this.bscene.getGrid();
        grid.placeAt(this.col, this.row, this);
        this.displayHeight = this.bscene.ch;
        this.displayWidth = this.bscene.cw;

        this.updateTextPos();
    }
    updateTextPos() {
        this.text1.x = this.x;
        this.text1.y = this.y;
        this.scene.children.bringToTop(this.text1);
    }
    destroyMe()
    {
        this.callback=()=>{};
        this.text1.destroy();
        this.destroy();
    }
}