import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import Align from "../util/align";
import {UIBlock} from "../util/UIBlock";

export class FlatButton extends UIBlock implements IGameObj
{
    private scene:Phaser.Scene;
    public index:number=0;
    public scaleX: number=1;
    public scaleY: number=1;

    constructor(bscene:IBaseScene,key:string,text:string,textStyle:any,callback:Function=()=>{})
    {
        super();
        this.scene=bscene.getScene();
        const bg=this.scene.add.image(0,0,key);

        const textObj=this.scene.add.text(0,0,text,textStyle);
        textObj.setOrigin(0.5,0.5);

        this.add(bg);
        this.add(textObj);
        Align.scaleToGameW(bg,0.5,bscene);

        if (callback)
        {
            bg.setInteractive();
            bg.on('pointerdown',()=>{callback(this)});
        }
        console.log(this);
    }
    
}