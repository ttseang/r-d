import { GameObjects } from "phaser";
import { LayoutManager } from "../classes/layoutManager";
import { PosVo } from "../dataObjs/PosVo";
import { FlatButton } from "../ui/flatButton";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneOver extends BaseScene
{
    private btnPlay:FlatButton
    private layoutManager:LayoutManager;
    private back:Phaser.GameObjects.Image;
    private spider:GameObjects.Image;

    constructor()
    {
        super("SceneOver");
    }
    preload()
    {
        /* this.load.image("spider", "./assets/spider.png");
        this.load.image("apple", "./assets/apple.png");
        this.load.image("button","./assets/button.png");
        this.load.image("back", "./assets/back.jpg"); */
    }
    create()
    {
        super.create();
        console.log("sceneOver");
        this.makeGrid(11,7);
        //this.grid.showNumbers();

        this.back=this.add.image(0,0,"back");


        this.btnPlay=new FlatButton(this,"button","Play Again",{fontSize:"26px",color:"#000000"},this.playAgain.bind(this));
        // this.grid.placeAt(5,8,this.btnPlay);
        this.layoutManager=new LayoutManager(this);
        this.layoutManager.callback=this.flipped.bind(this);

        this.spider=this.add.image(0,0,"spider");    

        this.flipped();
    }
    flipped()
    {
        Align.scaleToGameH(this.back, 1, this);
        if (this.back.displayWidth<this.gw)
        {
            Align.scaleToGameW(this.back,1,this);
        }
        Align.center(this.back, this);

        this.children.sendToBack(this.back);
        //this.grid.showPos();

        Align.scaleToGameW(this.spider,this.layoutManager.getScale("spider"),this);
        this.spider.flipY=true;
        this.grid.placeAt(3,3,this.spider);
        

//let buttonPos:PosVo=this.layoutManager.getPos("button");
        this.grid.placeAt(3,8,this.btnPlay);
    }
    playAgain()
    {
        this.scene.start("SceneMain");
    }
}