import { GameObjects } from "phaser";
import { LayoutManager } from "../classes/layoutManager";
import { LetterBox } from "../classes/LetterBox";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { AnimationUtil } from "../util/animUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private letterBoxes: LetterBox[] = [];
    private layoutManager: LayoutManager;
    private wordBoxes: LetterBox[] = [];
    private word: string = "";
    private tries: number = 10;

    private spiderMoving: boolean = false;
    private spider: GameObjects.Sprite;
    private line: GameObjects.Graphics;

    private appleCount: GameObjects.Text;
    private apple: GameObjects.Image;

    private back: GameObjects.Image;

    private clickLock: boolean = false;
    private words: string[] = ["spider", "woods", "trees", "veggies", "zombie", "kitchen", "people", "floor", "yesterday", "superman", "potions"];

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("back", "./assets/back.jpg");
        this.load.image("rect", "./assets/rect2.png");
        this.load.image("holder", "./assets/rect.png");
        this.load.image("spider", "./assets/spider.png");
        this.load.image("apple", "./assets/apple.png");
        this.load.audio("chomp", "./assets/audio/chomp.mp3");
        this.load.audio("rightSound", "./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound", "./assets/audio/quiz_wrong.wav");
        this.load.audio("levelUp","./assets/audio/levelUp3.mp3");
        this.load.audio("gameOver","./assets/audio/gameOver.mp3");

        this.load.image("button","./assets/button.png");

    }
    create() {
        super.create();
        this.makeGrid(11, 7);
        this.tries=10;
        this.layoutManager = new LayoutManager(this);
        this.layoutManager.callback = this.flipped.bind(this);
        //
        //
        this.makeDecor();
      //  this.grid.showNumbers();

        this.makeLetters();
        this.nextWord();
        window['scene']=this;
    }
    makeDecor() {

        this.back = this.add.image(0, 0, "back");
        Align.scaleToGameH(this.back, 1, this);
        if (this.back.displayWidth<this.gw)
        {
            Align.scaleToGameW(this.back,1,this);
        }
        Align.center(this.back, this);

        this.spider = this.placeImage2("spider", 3, 0, this.layoutManager.getScale("spider"));
        this.spider.flipY = true;
        this.line = this.add.graphics();

        
        this.apple = this.placeImage2("apple", 3, 3, this.layoutManager.getScale("apple"));
        this.appleCount = this.add.text(0, 0, this.tries.toString(), { fontSize: "26px", color: "#000000" }).setOrigin(0.5, 0.5);
        this.appleCount.x = this.apple.x;
        this.appleCount.y = this.apple.y;
        this.flipped();

    }
    flipped() {
        //console.log("FLIPPED");
        //console.log(this.gw, this.gh);
        //this.grid.showNumbers();
        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            let letterBox: LetterBox = this.letterBoxes[i];
            letterBox.updatePos();
        }
        this.positonWordBoxes();
        this.grid.placeAt(3,0,this.spider);
        Align.scaleToGameW(this.spider,this.layoutManager.getScale("spider"),this);

        let applePos:PosVo=this.layoutManager.getPos("apple");
        Align.scaleToGameW(this.apple,this.layoutManager.getScale("apple"),this);
        this.grid.placeAt(applePos.x,applePos.y,this.apple);

        this.appleCount.x=this.apple.x;
        this.appleCount.y=this.apple.y;

        Align.scaleToGameH(this.back, 1, this);
        if (this.back.displayWidth<this.gw)
        {
            Align.scaleToGameW(this.back,1,this);
        }
        Align.center(this.back, this);
    }
    nextWord() {
        if (this.words.length > 0) {
            this.makeWord(this.words.shift());
        }
    }
    positonWordBoxes() {
        let letterWidth: number = this.cw / 2;
        let xx: number = this.gw / 2 - ((letterWidth * this.word.length) / 2) + (letterWidth / 2);

        for (let i: number = 0; i < this.wordBoxes.length; i++) {
            let letterBox: LetterBox = this.wordBoxes[i];
            letterBox.displayWidth = this.cw / 2;
            letterBox.y = this.gh * .20;
            letterBox.x = xx;
            xx += letterBox.displayWidth;
            letterBox.updateTextPos();
        }
    }
    makeWord(word: string) {
        word = word.toUpperCase();
        this.word = word;
        let letterWidth: number = this.cw / 2;
        let xx: number = this.gw / 2 - ((letterWidth * word.length) / 2) + (letterWidth / 2);
        for (let i: number = 0; i < word.length; i++) {
            let letterBox: LetterBox = new LetterBox(this, word.substr(i, 1), 0xfd9644);
            letterBox.displayWidth = this.cw / 2;
            /* letterBox.y=this.gh*.3;
            letterBox.x=xx; */
            letterBox.hideLetter();

            this.wordBoxes.push(letterBox);
            // xx+=letterBox.displayWidth;
            // letterBox.updateTextPos();
        }
        this.positonWordBoxes();
    }

    makeLetters() {


        let colors: number[] = [0xfc5c65, 0x26de81, 0xfd9644, 0xa55eea];
        let colorIndex: number = 0;
        let letters: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let letterArray: string[] = letters.split("");
        let letterIndex: number = 0;

        for (let i: number = 0; i < 5; i++) {
            for (let j: number = 0; j < 5; j++) {
                let letterBox = this.makeLetterBox(letterArray[letterIndex], j + 1, i + 4.5, 0xfc5c65);                // let letterBox = new LetterBox(this, letterArray[letterIndex], colors[colorIndex]);


                colorIndex++;
                if (colorIndex > colors.length - 1) {
                    colorIndex = 0;
                }
                letterIndex++;
            }
        }
        this.makeLetterBox("Z", 3, 9.5, 0xfc5c65);
    }
    makeLetterBox(letter: string, col: number, row: number, color: number) {
        let letterBox = new LetterBox(this, letter, color);
        this.letterBoxes.push(letterBox);
        letterBox.setCallback(this.clickLetter.bind(this))
        letterBox.setPos(col, row);
        return letterBox;
    }
    clickLetter(lb: LetterBox) {
        if (this.clickLock === true) {
            return;
        }
        //console.log(lb.letter);
        lb.setCallback(() => { });
        lb.setTint(0x778ca3);

        let miss: boolean = true;

        for (let i: number = 0; i < this.wordBoxes.length; i++) {
            let lb2: LetterBox = this.wordBoxes[i];
            if (lb.letter.toLowerCase() == lb2.letter.toLowerCase()) {
                lb2.showLetter();
                lb2.correct = true;
                miss = false;
            }
        }
        if (miss === false) {
            let rightSound = this.sound.add("rightSound");
            rightSound.play();

            let win: boolean = this.checkWin();
            //console.log("WIN=" + win);
            if (win === true) {
                this.clickLock = true;
                
                let levelUp:Phaser.Sound.BaseSound=this.sound.add("levelUp");
                levelUp.play();

                setTimeout(() => {
                    this.resetBoard();
                }, 2000);
            }
        }
        else {
            let wrongSound = this.sound.add("wrongSound");
            wrongSound.play();

            this.dropSpider();
        }
    }
    resetBoard() {
        this.tries = 10;
        this.appleCount.setText(this.tries.toString());

        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            let box: LetterBox = this.letterBoxes[i];
            box.setTint(0xfc5c65);
            box.correct = false;
            box.setCallback(this.clickLetter.bind(this));

        }
        while (this.wordBoxes.length > 0) {
            this.wordBoxes.pop().destroyMe();
        }
        this.nextWord();
        this.clickLock = false;
    }
    downApple() {
        this.tries--;
        this.appleCount.setText(this.tries.toString());
        if (this.tries === 0) {
            this.clickLock=false;
            //console.log("GAME OVER");
            let overSound:Phaser.Sound.BaseSound=this.sound.add("gameOver");
            overSound.play();
            for (let i:number=0;i<this.wordBoxes.length;i++)
            {
                let box:LetterBox=this.wordBoxes[i];
                box.showLetter();
            }

            setTimeout(() => {
                
               this.doOver();
            }, 2000);
        }

    }
    doOver()
    {
        while(this.letterBoxes.length>0)
        {
            this.letterBoxes.pop().destroyMe();
        }
        while(this.wordBoxes.length>0)
        {
            this.wordBoxes.pop().destroyMe();
        }
        this.layoutManager.callback=()=>{};

        this.scene.start("SceneOver");
    }
    checkWin() {
        for (let i: number = 0; i < this.wordBoxes.length; i++) {
            let lb2: LetterBox = this.wordBoxes[i];
            if (lb2.correct == false) {
                return false;
            }
        }
        return true;
    }
    dropSpider() {
        this.clickLock = true;
        this.spider.flipY = false;
        this.spiderMoving = true;
        this.tweens.add({ targets: this.spider, duration: 500, y: this.apple.y, onComplete: this.stealApple.bind(this) });
    }
    stealApple() {
        this.downApple();
        let chomp: Phaser.Sound.BaseSound = this.sound.add("chomp");
        chomp.play();
        this.spider.flipY=true;
        this.tweens.add({ targets: this.spider, duration: 500, y: this.gh * 0.1, onComplete: this.moveDone.bind(this) });
    }
    moveDone() {
        this.spider.flipY = true;
        this.spiderMoving = false;
        this.clickLock = false;
    }
    update() {
        if (this.spiderMoving == true) {
            this.line.clear();
            this.line.lineStyle(2, 0x00000, 1);
            this.line.moveTo(this.gw / 2, 0);
            this.line.lineTo(this.spider.x, this.spider.y);
            this.line.strokePath();
            this.children.bringToTop(this.spider);
        }
    }
}