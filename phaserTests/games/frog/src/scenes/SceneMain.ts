import { WordHolder } from "../classes/WordHolder";
import { InstructPanel } from "../ui/InstructPanel";
import Align from "../util/align";
import { UIBlock } from "../util/UIBlock";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    //private pads:Phaser.GameObjects.Sprite[]=[];
    private pads: UIBlock = new UIBlock();
    private home: Phaser.GameObjects.Sprite;
    private leftPad: Phaser.GameObjects.Sprite;
    private rightPad: Phaser.GameObjects.Sprite;
    private padDistX: number = 0;
    private padDistY: number = 0;
    private frog: Phaser.GameObjects.Sprite;

    private audioList: string[] = [];
    private wordIndex: number = -1;
    private instructions:InstructPanel;

    private levelData: any;

    private wh1: WordHolder;
    private wh2: WordHolder;

    private clickLock:boolean=false;

    constructor() {
        super("SceneMain");
    }
    preload() {

    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.levelData = this.cache.json.get("levels");
        console.log(this.levelData);


        this.add.image(0, 0, "background").setOrigin(0, 0);

        this.frog = this.add.sprite(0, 0, "frog");
        this.frog.setFrame("frog1.png");
        Align.scaleToGameW(this.frog, 0.25, this);

        // this.grid.showNumbers();
        // this.grid.placeAtIndex(93,frog);
        this.grid.placeAt(5, 8.5, this.frog);
        //
        //
        //

        this.makePads();
        this.frog.setDepth(80000);

        /*  this.input.on('pointerdown',()=>{
             if (this.input.activePointer.x>this.gw/2)
             {
                 this.jumpRight();
             }
             else
             {
                 this.jumpLeft();
             }
            
         }); */

        this.instructions = new InstructPanel(this);
        this.instructions.x = this.gw / 2;
        this.instructions.y = this.instructions.displayHeight / 2;
        this.instructions.callback=this.sayVowel.bind(this);
        this.instructions.setDepth(1200);
        // this.sayInstruct(1);
        this.showChoices();
        console.log(this.cache);
    }
    makePads() {
        this.pads = new UIBlock();
        this.leftPad = this.addPad(46);
        this.rightPad = this.addPad(52);
        this.home = this.addPad(93);


        this.padDistX = Math.abs(this.home.x - this.rightPad.x);
        this.padDistY = this.home.y - this.rightPad.y;

        let pad3: Phaser.GameObjects.Sprite = this.addPad(0);
        pad3.x = this.rightPad.x - this.padDistX;
        pad3.y = this.rightPad.y - this.padDistY;

        let pad4: Phaser.GameObjects.Sprite = this.addPad(0);
        pad4.x = this.rightPad.x + this.padDistX;
        pad4.y = this.rightPad.y - this.padDistY;

        let pad5: Phaser.GameObjects.Sprite = this.addPad(0);
        pad5.x = this.leftPad.x - this.padDistX;
        pad5.y = this.leftPad.y - this.padDistY;

    }
    showChoices() {
        this.wordIndex++;
        if (this.wordIndex===this.levelData.levels.length)
        {
            this.showEnd();
            return;
        }
        let current: any = this.levelData.levels[this.wordIndex];
        console.log(current.words);

        this.wh1 = new WordHolder(current.words[0].id, current.words[0].word, this);
        this.wh1.index=0;
        this.wh1.setDepth(3000);
        this.grid.placeAtIndex(46, this.wh1);
        //
        //
        //
        //
        this.wh2 = new WordHolder(current.words[1].id, current.words[1].word, this);
        this.wh2.index=1;
        this.wh2.setDepth(3001);
        this.grid.placeAtIndex(52, this.wh2);

        this.wh1.callback=this.makeChoice.bind(this);
        this.wh2.callback=this.makeChoice.bind(this);

        this.clickLock=true;
        this.sayInstruct(current.vowel);
    }
    makeChoice(index: number) {
        if (this.clickLock===true)
        {
            return;
        }
        let current: any = this.levelData.levels[this.wordIndex];
        if (index === current.correct) {
            console.log("right");
            if (index===1)
            {
                this.jumpRight();
            }
            else
            {
                this.jumpLeft();
            }
        }
        else {
            console.log("wrong");
            this.wrongMove();
        }
    }
    addPad(pos: number) {
        let pad = this.add.sprite(0, 0, "pad");
        Align.scaleToGameW(pad, 0.3, this);
        this.grid.placeAtIndex(pos, pad, true);
        pad.setDepth(500 + this.pads.childIndex);
        this.pads.add(pad);
        return pad;
    }
    jumpRight() {
        if (this.wh1) {
            this.wh1.destroy();
        }
        if (this.wh2) {
            this.wh2.destroy();
        }
        this.frog.setFrame("frog2.png");
        this.tweens.add({ targets: this.pads, duration: 250, y: this.padDistY, x: -this.padDistX, onComplete: this.jumpDone.bind(this) });
    }
    jumpLeft() {
        if (this.wh1) {
            this.wh1.destroy();
        }
        if (this.wh2) {
            this.wh2.destroy();
        }
        this.frog.setFrame("frog2.png");
        this.tweens.add({ targets: this.pads, duration: 250, y: this.padDistY, x: this.padDistX, onComplete: this.jumpDone.bind(this) });
    }
    jumpDone() {
        this.frog.setFrame("frog1.png");
        this.pads.destroy();
        this.makePads();
        this.showChoices();
    }
    wrongMove() {
        let wrongSound=this.sound.add("error");
        wrongSound.play();
        this.frog.setFrame("frog2.png");
        this.time.addEvent({ delay: 500, callback: this.resetFrog.bind(this), loop: false });
    }
    resetFrog() {
        this.frog.setFrame("frog1.png");
    }
    sayInstruct(index: number) {
        this.clickLock=true;
        this.audioList.push("findTheWord");
        let key: string = "vowel" + index.toString();
        this.audioList.push(key);
        this.audioList.push("inTheMiddle");
        this.playNextAudio();
    }
    sayVowel()
    {
        let current: any = this.levelData.levels[this.wordIndex];
        let vowel:number=current.vowel;

        let sound:Phaser.Sound.BaseSound=this.sound.add("vowel"+vowel.toString());
        sound.play();
    }
    showEnd()
    {
        let end=this.add.image(0,0,"end").setOrigin(0.5,0);
        Align.scaleToGameW(end,1,this);
        end.x=this.gw/2;
        end.setDepth(10000);
        end.y=0;
        this.instructions.visible=false;
     //   this.pads.visible=false;
        this.frog.setFrame("frog2.png");
        //
        //
        let winSound:Phaser.Sound.BaseSound=this.sound.add("win");
        winSound.play();
        
        this.tweens.add({targets: this.frog,duration: 500,y:-500});
    }
    playNextAudio() {
        let key: string = this.audioList.shift();
        let sound: Phaser.Sound.BaseSound = this.sound.add(key);
        if (this.audioList.length > 0) {
            sound.once("complete", this.playNextAudio.bind(this));
        }
        else
        {
            this.clickLock=false;
        }
        sound.play();
    }
}