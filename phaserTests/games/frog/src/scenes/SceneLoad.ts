import { BaseScene } from "./BaseScene"

export class SceneLoad extends BaseScene{
    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {
        this.load.atlas("frog","assets/images/frog.png","assets/images/frog.json");
        this.load.json("levels","assets/levelData.json");
        this.load.image("pad","assets/images/pad.png");
        this.load.image("background","assets/images/background.jpg");

        this.load.image("holder","assets/images/holder.jpg");
        this.load.image("circle","assets/images/circle.png");
        this.load.image("end","assets/images/end.png");

        this.load.image("sb","assets/images/soundButton.png");

        this.loadListAudio(1);
        this.loadListAudio(2);

        this.loadListPicture(1);
        this.loadListPicture(2);

        for (let j:number=94;j<139;j++)
        {
            this.loadListPicture(j);
            this.loadListAudio(j);
        }

        for (let i:number=1;i<5;i++)
        {
            this.loadVowel(i);
        }
      

        this.load.audio("findTheWord","assets/audio/find_the_word.mp3");
        this.load.audio("inTheMiddle","assets/audio/in_the_middle.mp3");
        this.load.audio("error","assets/audio/error.wav");
        this.load.audio("win","assets/audio/win.wav");
    }
    loadVowel(index:number)
    {
        let file="assets/audio/vowels/"+index.toString()+".wav";
        this.load.audio("vowel"+index.toString(),file);
    }
    loadListAudio(index:number)
    {
        let file="assets/audio/listAudio/"+index.toString()+".mp3";
       
        this.load.audio("listaudio"+index.toString(),file);
    }
    loadListPicture(index:number)
    {
        let file="assets/images/listImages/"+index.toString()+".jpg";
        this.load.image("listimage"+index.toString(),file);
    }
    create()
    {
        this.scene.start("SceneMain");
    }
}