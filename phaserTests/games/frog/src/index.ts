//import Phaser = require("phaser");
//import Phaser from 'phaser';
import { SceneLoad } from './scenes/SceneLoad';
import { SceneMain } from "./scenes/SceneMain";
let isMobile = navigator.userAgent.indexOf("Mobile");
if (isMobile == -1) {
    isMobile = navigator.userAgent.indexOf("Tablet");
}
let w = 480;
let h = 640;
//
//
if (isMobile != -1) {
    w = window.innerWidth;
    h = window.innerHeight;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    parent: 'phaser-game',
    scene: [SceneLoad,SceneMain]
};

new Phaser.Game(config);