import IBaseScene from "../interfaces/IBaseScene";

export class InstructPanel extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    public textObj:Phaser.GameObjects.Text;
    public callback:Function=()=>{};

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        let back:Phaser.GameObjects.Image=this.scene.add.image(0,0,"holder");//.setOrigin(0,0);
        back.setTint(0x2ecc71);
        back.displayWidth=bscene.getW();
        back.displayHeight=bscene.getH()*0.2;
        this.add(back);

        //
        //
        //
        this.textObj=this.scene.add.text(0,0,"Instructions here").setOrigin(0.5,0.5);
        this.add(this.textObj);
        this.scene.add.existing(this);

        this.setSize(back.displayWidth,back.displayHeight);

        let btnSound=this.scene.add.image(0,0,"sb");
        btnSound.x=back.x-back.displayWidth/2+btnSound.displayWidth;
        btnSound.y=back.y-back.displayHeight/2+btnSound.displayHeight;

        this.add(btnSound);
        btnSound.setInteractive();
        btnSound.on('pointerdown',()=>{this.callback()})
    }
}