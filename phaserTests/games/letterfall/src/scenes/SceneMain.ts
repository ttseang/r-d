import { GameObjects, Physics } from "phaser";
import { FallingLetter } from "../classes/FallingLetter";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    private floor:Physics.Arcade.Sprite;
    private letters:Physics.Arcade.Group;

    preload() {
       this.load.atlas("letters","./assets/letters.png","./assets/letters.json");
       this.load.image("holder","./assets/holder.jpg");
    }
    create() {
        super.create();
        this.makeGrid(11,11);
        this.grid.showNumbers();

        this.letters=this.physics.add.group({bounceX: 0,bounceY: 0, collideWorldBounds: true});
      
        this.floor=this.physics.add.sprite(0,this.gh*0.8,"holder").setOrigin(0,0);
        this.floor.displayWidth=this.gw;
        this.floor.displayHeight=this.gh*0.1;
        this.floor.setImmovable();

        this.physics.add.collider(this.floor,this.letters,this.hitFloor.bind(this));
        this.physics.add.collider(this.letters,this.letters,this.hitLetter.bind(this));
        
        
        this.dropLetter();

        setInterval(this.dropLetter.bind(this),1000);

        this.input.on("gameobjectdown",this.clickLetter.bind(this));

    }
    clickLetter(p:Phaser.Input.Pointer,letter:FallingLetter)
    {
        letter.destroy();
    }
    hitFloor(thefloor:Physics.Arcade.Sprite,letter:FallingLetter)
    {
        letter.onFloor=true;
    }
    hitLetter(letter1:FallingLetter,letter2:FallingLetter)
    {
        if (letter1.onFloor==true)
        {
            letter2.onFloor=true;
            letter1.setVelocity(0,0);
            letter1.setGravity(0,10);

            letter2.setGravity(0,10);
        }
    }
    dropLetter()
    {
      /*   let letter:Physics.Arcade.Sprite=this.physics.add.sprite(0,0,"letters");
        Align.scaleToGameW(letter,0.1,this);

        
        this.grid.placeAt(xx,0,letter);
        
       

        let frame:string="letter_"+char+".png";
        letter.setFrame(frame); */
        //
        //
        //
        let alphabet:string="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let index:number=Phaser.Math.Between(0,25);
        let char:string=alphabet.substr(index,1);
        //
        //
        //
        let letter:FallingLetter=new FallingLetter(this,char);
        let xx=Math.floor(Math.random()*9)+1;
        this.grid.placeAt(xx,0,letter);
        this.letters.add(letter);
        //letter.setAngularVelocity(100);
        letter.setGravityY(100);
        letter.setPushable(false);
        letter.setInteractive();

        //letter.setVelocityX(Phaser.Math.Between(-10,10));
      //  letter.setBounce(0.1,0.1);

    }
}