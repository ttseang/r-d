import { Physics } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class FallingLetter extends Physics.Arcade.Sprite
{
    public scene:Phaser.Scene;
    public onFloor:boolean=false;

    constructor(bscene:IBaseScene,letter:string)
    {
        super(bscene.getScene(),0,0,"letters");
        this.scene=bscene.getScene();

        let frame:string="letter_"+letter+".png";

        this.setFrame(frame);

        Align.scaleToGameW(this,0.09,bscene);

        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
    }
}