//import Phaser = require("phaser");
import Phaser from 'phaser';
import { SceneMain } from "./scenes/SceneMain";
import { GM } from "./classes/GM";

let gm: GM = GM.getInstance();

let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad = navigator.userAgent.indexOf("iPad");

if (isTablet != -1 || isIpad != -1) {
    gm.isTablet = true;
    isMobile = 1;
}

let w = 480;
let h = 600;
//
//
if (isMobile != -1) {
    gm.isMobile = true;

}
/* w = window.innerWidth;
    h = window.innerHeight; */
if (w < h) {
    gm.isPort = true;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    backgroundColor: 'cccccc',
    parent: 'phaser-game',
    scene: [SceneMain],
    physics: {
        default: "arcade",
        arcade: {
            debug: true
        }
    }
};

new Phaser.Game(config);