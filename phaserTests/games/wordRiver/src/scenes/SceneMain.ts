import { GameObjects, Physics } from "phaser";
import { GM } from "../classes/GM";
import { LetterBox } from "../classes/LetterBox";
import { LetterBubble } from "../classes/LetterBubble";
import Align from "../util/align";
import { RandWordUtil } from "../util/randWordUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
  
    private bg: GameObjects.Image;
    private ground: GameObjects.TileSprite;
    
    private randWordUtil: RandWordUtil;
    private gm: GM = GM.getInstance();
    private bubbles:Phaser.GameObjects.Group;

    private currentWord:string="";
    private letterBoxes:LetterBox[]=[];
    private wrongCount:number=0;

    private strikes:GameObjects.Image[]=[];
    private correctText:GameObjects.Text;
    private correctCount:number=0;
    private lettersLeft:number=0;
    private leftText: GameObjects.Text;
    private top:GameObjects.Image;
    private river:GameObjects.TileSprite;

    constructor() {
        super("SceneMain");
    }
    preload() {
        
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.letterBoxes=[];
        this.strikes=[];
        
        this.wrongCount=0;
        this.correctCount=0;
        this.currentWord="";
        this.grid.showPos();
        this.lettersLeft=301;

       /*  this.bg = this.add.image(0, 0, "sky");
        this.bg.displayWidth = this.gw;
        this.bg.scaleY = this.bg.scaleX;
        Align.center(this.bg, this); */
               
        console.log(this.gw);

        this.river=this.add.tileSprite(0,0,0,0,"river");
        Align.scaleToGameW(this.river,1.2,this);
        Align.center(this.river,this);        
        
        this.bubbles=this.add.group();

        window['scene'] = this;

        this.randWordUtil = new RandWordUtil(this);
        this.gm.letters = this.randWordUtil.pickRandomLetter();

        this.makeBubbles();


        this.top=this.add.image(0,0,"holder").setOrigin(0,0);
        this.top.displayHeight=this.gh*0.25;
        this.top.displayWidth=this.gw;
        this.top.setTint(0x2ecc71);

        this.correctText=this.add.text(0,0,"Correct:0",{fontSize:"20px",color:"white",fontFamily:"Arial"}).setOrigin(0.5,0.5);
        this.grid.placeAt(2,0,this.correctText);

        this.leftText=this.add.text(0,0,"Remaining:0",{fontSize:"20px",color:"white",fontFamily:"Arial"}).setOrigin(0.5,0.5);
        this.grid.placeAt(8,0,this.leftText);

        
        this.makeStrikes();
        this.makeLetterBoxes();

       this.input.on("gameobjectdown",this.collectLetter.bind(this));

       this.downLetters();

    }
    downLetters()
    {
        this.lettersLeft--;
        
        this.leftText.setText("Remaining:"+this.lettersLeft.toString());

        if (this.lettersLeft==0)
        {
            this.gameOver();
        }
    }
    getNextLetter() {
        if (this.gm.letters.length == 0) {
            this.gm.letters = this.randWordUtil.pickRandomLetter();
        }
        let letter: string = this.gm.letters.shift();
        return letter;
    }
    makeBubbles() {
        for (let i: number = 0; i < 8; i++) {
            let letterBubble: LetterBubble = new LetterBubble(this,this.getNextLetter.bind(this),this.downLetters.bind(this));
            letterBubble.setLetter(this.getNextLetter());
            letterBubble.y=-200-(i*100);
            
            letterBubble.setPos();
            this.bubbles.add(letterBubble);
            
            
        }
    }
    collectLetter(p:Phaser.Input.Pointer,bubble:LetterBubble)
    {
        if (bubble.visible==false)
        {
            return;
        }
        let letter:string=bubble.letter;
        this.currentWord+=letter;
        this.updateTiles();
        if (this.currentWord.length==4)
        {
            this.checkWord();
        }
        this.playSound("pop");
        bubble.visible=false;
    }
    
   
   
    makeStrikes()
    {
        for (let i:number=0;i<this.strikes.length;i++)
        {
            this.strikes.pop().destroy();
        }
        for (let i:number=0;i<this.wrongCount;i++)
        {
            let strike:GameObjects.Image=this.add.image(0,0,"strike");
            Align.scaleToGameW(strike,0.1,this);
            this.grid.placeAt(3+(i*2),9,strike);
            this.strikes.push(strike);
        }
        if (this.wrongCount==3)
        {
            this.gameOver();
        }
    }
    updateTiles()
    {
        let letters:string[]=this.currentWord.split("");
        while(letters.length<4)
        {
            letters.unshift("?");
        }
        for (let i:number=0;i<letters.length;i++)
        {
            this.letterBoxes[i].setLetter(letters[i]);
        }

    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    makeLetterBoxes()
    {
        for(let i:number=0;i<4;i++)
        {
            let letterBox:LetterBox=new LetterBox(this);
            this.letterBoxes.push(letterBox);
            Align.scaleToGameW(letterBox,0.2,this);
            this.grid.placeAt(2+i*2.1,1.5,letterBox);
        }
    }
    checkWord()
    {
        if (this.randWordUtil.checkWord(this.currentWord))
        {
            console.log("correct");
            this.currentWord="";
            this.correctCount++;
            this.correctText.setText("Correct:"+this.correctCount);
            this.playSound("rightSound");
        }
        else
        {
            console.log("wrong");
            this.playSound("wrongSound");
            this.wrongCount++;
            this.makeStrikes();
            this.currentWord="";
            this.updateTiles();
        }
        
    }
    gameOver()
    {
        this.bubbles.children.iterate((bubble:LetterBubble)=>{
            if (bubble)
            {
                this.bubbles.remove(bubble,true,true);
            }
        })

        this.scene.start("SceneOver");
    }
    update() {
       
        this.bubbles.children.iterate((bubble:LetterBubble)=>{
            bubble.onUpdate();
        })
        this.river.tilePositionY-=10;
    }
}