import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class LetterBubble extends GameObjects.Container {
    private bscene: IBaseScene;
    public letter: string = "A";
    public scene: Phaser.Scene;
    private text1: GameObjects.Text;
    private back: GameObjects.Image;
   // private callback: Function;
    private nextLetter:Function;
    private downCallback:Function;

    constructor(bscene: IBaseScene,nextLetter:Function,downCallback:Function) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.active = false;
        this.nextLetter=nextLetter;
        this.downCallback=downCallback;

       // this.callback = callback;

        this.back = this.scene.add.image(0, 0, "circle");
        this.add(this.back);
        Align.scaleToGameW(this.back, 0.15, this.bscene);

        this.text1 = this.scene.add.text(0, 0, "?", { fontSize: "30px", fontFamily: "Arial", color: "black" }).setOrigin(0.5, 0.5);
      /*   this.text1.x = this.back.displayWidth / 2;
        this.text1.y = this.back.displayHeight / 2; */
        this.add(this.text1);


        //this.scene.physics.world.enable(this);

       /*  setTimeout(() => {
            let body: any = this.body;
            body.setSize(this.back.displayWidth, this.back.displayHeight);
        }, 1000); */


        this.scene.add.existing(this);
        //this.scene.physics.add.existing(this);
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
       /*  let obj: any = this;
        obj.body.setVelocityX(-300); */
    }
    setPos()
    {
        let xx:number=Math.floor(Math.random()*this.bscene.getW()*0.6)+this.bscene.getH()*0.2;
        this.x=xx;
    }
    setLetter(letter:string)
    {
        this.letter=letter;
        this.text1.setText(letter);
    }
   
    onUpdate() {
        this.y+=this.bscene.getH()/400;

        if (this.y>this.bscene.getH())
        {
            this.y=-100;
            this.visible=true;
            this.setPos();
            this.letter=this.nextLetter();
            this.downCallback();
            this.text1.setText(this.letter);
        }
    }
}