import { GM } from "../classes/GM";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {

        this.load.image("bg","./assets/bg2.jpg");
        this.load.atlas("crab","./assets/crab.png","./assets/crab.json");

        this.load.image("button","./assets/ui/buttons/1/3.png");

        this.load.image("bubble","./assets/bubble.png");
        this.load.image("sand","./assets/sand.png");
        this.load.image("pine","./assets/pine.png");
        this.load.image("strike","./assets/strike.png");

        this.load.audio("rightSound","./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");
        this.load.audio("pop","./assets/audio/pop.wav");
        this.load.audio("backgroundMusic","./assets/audio/happy_walk.mp3");

        this.load.text("words", "./assets/data/fourletterwords.txt");
    }
    create()
    {
        this.gm.bgMusic=this.sound.add("backgroundMusic",{loop:true,volume:0.5});    
        this.gm.bgMusic.play();
        
        this.scene.start("SceneMain");
    }
}