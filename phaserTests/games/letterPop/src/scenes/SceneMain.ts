import { GameObjects, Physics } from "phaser";
import { LetterBox } from "../classes/LetterBox";
import { LetterBubble } from "../classes/LetterBubble";
import Align from "../util/align";
import { AnimationUtil } from "../util/animUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    
    private background:GameObjects.Image;
    private crab:Physics.Arcade.Sprite;
    private grassGroup:Physics.Arcade.Group;
    private speed:number=200;
    private bubbleGroup:Phaser.GameObjects.Group;
    private blurred:boolean=false;
    private pickwords: string[] = [];
    private randLetters:string[]=[];
    private currentWord:string="";
    private letterBoxes:LetterBox[]=[];
    private wrongCount:number=0;

    private strikes:GameObjects.Image[]=[];
    private timer1:any=0;

    constructor() {
        super("SceneMain");
    }
    preload() {
        
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.pickwords = this.cache.text.get("words").split(" ");
        this.pickRandomWords();

        this.grassGroup=this.physics.add.group();
        this.bubbleGroup=this.add.group();

        /* for (let i:number=0;i<5;i++)
        {
            this.bubbleGroup.add(new LetterBubble(this,this.addBubble.bind(this)));
        } */

        this.background=this.add.image(0,0,"bg");
        Align.scaleToGameW(this.background,1,this);

        if (this.background.displayHeight<this.gh)
        {
            this.background.displayHeight=this.gh;
            this.background.scaleX=this.background.scaleY;
        }

        Align.center(this.background,this);
       //  this.grid.showPos();

        //
        //
        //

        this.crab=this.physics.add.sprite(this.gw/2, 100, "crab");
        this.crab.setGravityY(200);

        let animUtil:AnimationUtil=new AnimationUtil();
        animUtil.makeCrab(this,"crab");

        //this.moveIn();
        this.crab.play("idle");

        Align.scaleToGameW(this.crab,0.1,this);

        setTimeout(() => {
            this.crab.body.setSize(this.crab.width*0.5,this.crab.height*0.9);    
        }, 1000);
        this.makeGrass();

        this.physics.add.collider(this.crab,this.grassGroup);
        this.physics.add.collider(this.crab,this.bubbleGroup,null,this.gotBubble.bind(this))

        this.input.on("pointerdown",this.clickScreen.bind(this));
      //  this.input.on("pointerup",this.onUp.bind(this));
        this.input.keyboard.on("keydown",this.onKeyDown.bind(this));
        //this.addBubble();      
        this.timer1=setInterval(this.addBubble.bind(this),1000);
       

        window.onblur=()=>{
        this.blurred=true;
        clearInterval(this.timer1);
       };
        window.onfocus=()=>{
            this.blurred=false;
            clearInterval(this.timer1);
            this.timer1=setInterval(this.addBubble.bind(this),1000);
        };

        this.makeLetterBoxes();
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    makeLetterBoxes()
    {
        for(let i:number=0;i<4;i++)
        {
            let letterBox:LetterBox=new LetterBox(this);
            this.letterBoxes.push(letterBox);
            Align.scaleToGameW(letterBox,0.09,this);
            this.grid.placeAt(3.5+i,1,letterBox);
        }
    }
    pickRandomWords()
    {
        let randLetters:string[]=[];
        for (let i:number=0;i<10;i++)
        {
            let r:number=Math.floor(Math.random()*this.pickwords.length);
            let word:string=this.pickwords[r];
            console.log(word);

            let letters:string[]=word.split("");
            //console.log(letters);

           randLetters=randLetters.concat(letters);

           for (let j:number=0;j<2;j++)
           {
             let letters2:string[]="abcdefghijklmnopqrstuvwxyz".split("");
             let letterIndex:number=Math.floor(Math.random()*26); 
             randLetters.push(letters2[letterIndex]);
           }

        }
        //console.log(randLetters);
        this.randLetters=this.shuffleArray(randLetters);
    }
    
    shuffleArray(s:string[])
    {
        for (let i:number=0;i<50;i++)
        {
            let r:number=Math.floor(Math.random()*s.length);
            let r2:number=Math.floor(Math.random()*s.length);

            let temp:string=s[r];
            s[r]=s[r2];
            s[r2]=temp;
        }
        return s;
    }
    gotBubble(crab:Physics.Arcade.Sprite,bubble:LetterBubble)
    {
        let letter:string=bubble.letter;
        this.currentWord+=letter;
        this.updateTiles();
        if (this.currentWord.length==4)
        {
            this.checkWord();
        }
        this.playSound("pop");
        bubble.destroy();

    }
    checkWord()
    {
        if (this.pickwords.includes(this.currentWord))
        {
            console.log("correct");
            this.currentWord="";
            this.playSound("rightSound");
        }
        else
        {
            console.log("wrong");
            this.playSound("wrongSound");
            this.wrongCount++;
            this.makeStrikes();
            this.currentWord="";
            this.updateTiles();
        }
        
    }
    makeStrikes()
    {
        for (let i:number=0;i<this.strikes.length;i++)
        {
            this.strikes.pop().destroy();
        }
        for (let i:number=0;i<this.wrongCount;i++)
        {
            let strike:GameObjects.Image=this.add.image(0,0,"strike");
            Align.scaleToGameW(strike,0.05,this);
            this.grid.placeAt(i,1,strike);
            this.strikes.push(strike);
        }
        if (this.wrongCount==3)
        {
            this.gameOver();
        }
    }
    updateTiles()
    {
        let letters:string[]=this.currentWord.split("");
        while(letters.length<4)
        {
            letters.unshift("?");
        }
        for (let i:number=0;i<letters.length;i++)
        {
            this.letterBoxes[i].setLetter(letters[i]);
        }

    }
    onKeyDown(e)
    {
      //  console.log(e.key);
        if (e.key=="ArrowRight")
        {
            this.crab.setVelocityX(this.speed); 
            this.crab.play("walk");
            return;       
        }
        if (e.key=="ArrowLeft")
        {
            this.crab.setVelocityX(-this.speed);
            this.crab.play("walk");
            return;
        }
        /* this.crab.play("idle");
        this.crab.setVelocityX(0); */

    }
    addBubble()
    {
        if (this.blurred==true)
        {
            return;
        }
        /* let letters:string[]="ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
        let letterIndex:number=Math.floor(Math.random()*26); */
        if (this.randLetters.length==0)
        {
            this.pickRandomWords();
        }
        let letter:string=this.randLetters.shift();

        let lb:LetterBubble=new LetterBubble(this);
        this.bubbleGroup.add(lb);
        lb.y=-100;
        lb.x=Math.floor(Math.random()*this.gw*0.9)+this.gw*0.1;
        lb.active=true;
        lb.start(letter);
        
      //  this.bubbleGroup.push(lb);
    }
    clickScreen(p:Phaser.Input.Pointer)
    {
        if (p.x>this.crab.x)
        {
            this.crab.setVelocityX(this.speed);
        }
        else
        {
            this.crab.setVelocityX(-this.speed);
        }
        this.crab.play("walk");
    }
    onUp()
    {
        this.crab.setVelocity(0,0);
        this.crab.play("idle");
    }
    makeGrass()
    {
        let xx=-this.gw*0.1;
        let yy:number=this.gh-(this.gh*0.05);

        while(xx<this.gw*1.1)
        {
            let tile:Physics.Arcade.Sprite=this.physics.add.sprite(xx,yy,"sand");
            
            Align.scaleToGameW(tile,0.1,this);
            xx+=tile.displayWidth;
            this.grassGroup.add(tile);

            tile.setImmovable();
        }
    }
    gameOver()
    {
        clearInterval(this.timer1);

        this.bubbleGroup.children.entries.forEach((bubble:LetterBubble)=>{
            if (bubble)
            {
                bubble.destroy();
            }
        })
        setTimeout(() => {
            this.scene.start("SceneOver");
        }, 2000);
    }
    update()
    {
        if (this.crab.x>this.gw-this.crab.displayWidth/2)
        {
           // this.crab.x=this.gw-this.crab.displayWidth;
            this.crab.setVelocityX(-Math.abs(this.crab.body.velocity.x));
        }
        if (this.crab.x<this.crab.displayWidth/2)
        {
          //  this.crab.x=this.crab.displayWidth*1.01;
            this.crab.setVelocityX(Math.abs(this.crab.body.velocity.x));
        }
        this.bubbleGroup.children.iterate((bubble:LetterBubble)=>{
            if (bubble)
            {
                bubble.onUpdate();
            }
                      
        })
    }
}