import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class LetterBubble extends GameObjects.Container {
    private bscene: IBaseScene;
    public letter: string = "A";
    public scene: Phaser.Scene;
    private text1: GameObjects.Text;
    private back: GameObjects.Image;
   // private callback: Function;
    private sprouted:boolean=false;
   

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.active = false;

       // this.callback = callback;

        this.back = this.scene.add.image(0, 0, "bubble").setOrigin(0, 0);
        this.add(this.back);
        Align.scaleToGameW(this.back, 0.05, this.bscene);

        this.text1 = this.scene.add.text(0, 0, "?", { fontSize: "30px", fontFamily: "Arial", color: "black" }).setOrigin(0.5, 0.5);
        this.text1.x = this.back.displayWidth / 2;
        this.text1.y = this.back.displayHeight / 2;
        this.add(this.text1);


        this.scene.physics.world.enable(this);

        setTimeout(() => {
            let body: any = this.body;
            body.setSize(this.back.displayWidth, this.back.displayHeight);
        }, 1000);



       

    }
    start(letter: string) {
        this.letter = letter;
        this.sprouted=false;
        this.text1.setText(letter);
        let obj: any = this;
        obj.body.setGravityY(10);
        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
    }
    onUpdate() {
        if (this.active) {
           /*  if (this.y > this.bscene.getH()*0.2 && this.sprouted==false) {
                console.log("make new");
                this.sprouted=true;
                this.callback();
            } */
            if (this.y > this.bscene.getH()) {
                this.active = false;
                this.visible = false;
                let obj: any = this;
                obj.body.setGravityY(0);
                obj.body.setVelocity(0,0);
                this.x=0;
                this.y=0;
                this.destroy();
            }
        }
    }
}