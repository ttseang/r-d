export class AnimationUtil {
    AnimationUtil() {}

    
    makeAnims(scene, key) {
        console.log("anim key=" + key);
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Jump_',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Run_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'joy',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Joy_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'flight',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Flyght_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
       
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Idle_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        
    } 
    makeCrab(scene:Phaser.Scene,key:string)
    {
        scene.anims.create({
            key: 'walk',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 17,
                zeroPad: 3,
                prefix: 'Walking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Idle Blinking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 3,
                prefix: 'Attacking_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
    }
}