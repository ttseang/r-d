import { SylBox } from "../ui/SylBox";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private bumperGroup: Phaser.Physics.Arcade.Group;
    private soupGroup: Phaser.Physics.Arcade.Group;
    private qText: Phaser.GameObjects.Text;
    private wordData: any;
    private questonIndex: number = -1;
    private spoon: Phaser.GameObjects.Image;
    private bowl:Phaser.GameObjects.Sprite;
    private correct: string = "";
    private currentBox:SylBox;
    private word:string="";
    private clickLock:boolean=false;
    private parts:string[];

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/images/holder.jpg");
        // this.load.image("circle", "./assets/images/circle.png");
        this.load.image("bowl", "./assets/images/bowl2.png");
        this.load.image("carrot", "./assets/images/carrot.png");
        this.load.image("cloth", "./assets/images/tablecloth.png");
        this.load.image("spoon", "./assets/images/spoon.png");
        this.load.image("background", "./assets/images/background.png");
        this.load.json("worddata", "./assets/data/sdata.json");

        this.load.audio("rightSound","./assets/audio/quiz_right.wav");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");
        this.load.audio("whoosh","./assets/audio/whoosh.mp3");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.makeBackground();
        //this.grid.showNumbers();
        this.wordData = this.cache.json.get("worddata");
        //
        //
        //
        this.bumperGroup = this.physics.add.group();
        this.soupGroup = this.physics.add.group();

        this.makeBumpers();
        
        this.setQuestion();


        this.physics.add.collider(this.soupGroup, this.bumperGroup);
        this.physics.add.collider(this.soupGroup, this.soupGroup);

        
        //this.input.on('pointerdown',this.flipSpoon.bind(this));
    }
    setQuestion() {
        this.destroySoup();
       
        this.questonIndex++;
        console.log(this.questonIndex);

        if (this.questonIndex > this.wordData.wordData.length - 1) {
            console.log("out of questions");
            return;
        }

        let current: any = this.wordData.wordData[this.questonIndex];
        //
        //
        //
        this.qText.setText(current.q);
        let parts: string[] = current.parts;
        parts.push(current.a);
        this.correct=current.a;
        this.word=current.word;
        //
        //
        //
        parts = this.mixUpArray(parts);
        this.grid.placeAt(4.5, 7.5, this.qText);        
        this.makeBoxes(parts);

        this.clickLock=false;
    }
    makeBoxes(parts:string[])
    {
        let posArray: number[] = [25, 27, 36, 39, 48, 51,26];
        let len:number=parts.length;
       
        for (let i: number = 0; i <len; i++) {
            this.makeLetterBox(parts[i], posArray[i]);
        }
    }
    mixUpArray(array: string[]) {
        for (let i: number = 0; i < 50; i++) {
            let p1: number = Math.floor(Math.random() * array.length);
            let p2: number = Math.floor(Math.random() * array.length);

            let temp: string = array[p1];
            array[p1] = array[p2];
            array[p2] = temp;
        }
        return array;
    }
    chooseAnswer(box: SylBox) {
        if (this.clickLock===true)
        {
            return;
        }
        this.clickLock=true;
        console.log(box.letters);
       // box.visible = false;
        if (box.letters === this.correct) {
            box.body.velocity.x=0;
            box.body.velocity.y=0;
            this.currentBox=box;
            this.playSound("rightSound");
            this.tweens.add({ targets: box, duration: 300, y:this.qText.y,x:this.qText.x,onComplete:this.flyDone.bind(this)});
        }
        else
        {
            box.visible=false;
            this.playSound("wrongSound");
            this.clickLock=false;
        }
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    flyDone()
    {
        this.currentBox.visible=false;
        this.qText.setText(this.word);
        this.time.addEvent({delay:1000,callback:this.flipSpoon.bind(this),loop:false})
       
    }
    destroySoup() {
        while (this.soupGroup.children.entries.length > 0) {
            this.soupGroup.remove(this.soupGroup.children.entries[0], true, true);
        }

        // this.soupGroup=this.physics.add.group();
        //   this.soupGroup.children.entries.forEach((box:SylBox)=>{box.destroyMe()});
    }
    flipSpoon() {
        // this.spoon.setAngle(-45);
        this.tweens.add({ targets: this.spoon, duration: 400, angle: -45, onComplete: this.flipSpoonBack.bind(this) });
        this.tweens.add({ targets: this.qText, duration: 300, y: -200 });
        this.playSound("whoosh");
    }
    flipSpoonBack() {
        this.tweens.add({ targets: this.spoon, duration: 200, angle: 0, onComplete: this.setQuestion.bind(this) });
    }
    makeBackground() {
        let bg: Phaser.GameObjects.Image = this.add.image(0, 0, "background").setOrigin(0, 0);
        bg.displayWidth = this.gw;
        bg.displayHeight = this.gh;
        //
        //
        //
        let cloth: Phaser.GameObjects.Image = this.add.image(0, 0, "cloth").setOrigin(0, 0);
        Align.scaleToGameW(cloth, 1.2, this);

        this.bowl = this.add.sprite(0, 0, "bowl");
        //bowl.alpha=0.2;
        Align.scaleToGameW(this.bowl, 0.9, this);
        this.grid.placeAtIndex(38, this.bowl);


        this.spoon = this.add.image(0, 0, "spoon");
        Align.scaleToGameW(this.spoon, 0.6, this);
        this.grid.placeAtIndex(80, this.spoon);

        this.qText = this.add.text(0, 0, "Fi", { color: '#000000', fontSize: "20px", fontFamily: "Dela Gothic One" });
        this.grid.placeAt(4.5, 7.5, this.qText);
    }
    makeBumpers() {
        let posArray: any[] = [{ pos: 1, dir: 0, len: 6 }, { pos: 57, dir: 1, len: 6 }, { pos: 8, dir: 0, len: 6 }, { pos: 2, dir: 1, len: 6 }];
        for (let i: number = 0; i < posArray.length; i++) {
            let bumper: Phaser.Physics.Arcade.Sprite = this.physics.add.sprite(0, 0, "holder").setOrigin(0, 0);
            this.grid.placeAtIndex(posArray[i].pos, bumper);
            if (posArray[i].dir === 0) {
                bumper.displayWidth = this.cw;
                bumper.displayHeight = this.ch * posArray[i].len;
            }
            else {
                bumper.displayWidth = this.cw * posArray[i].len;
                bumper.displayHeight = this.ch;
            }

            this.bumperGroup.add(bumper);
            bumper.setImmovable();
            bumper.body.debugShowBody = true;
            bumper.alpha = 0.01;
        }
    }
    makeLetterBox(letters: string, pos: number) {
        console.log(letters);

        let letterBox: SylBox = new SylBox(this, letters);
        let letterBox2: any = letterBox;
        letterBox.callback = this.chooseAnswer.bind(this);

        this.physics.world.enableBody(letterBox);
        this.soupGroup.add(letterBox);

        letterBox.body.velocity.x = Phaser.Math.Between(-15, 15);
        letterBox.body.velocity.y = Phaser.Math.Between(-15, 15);
        //letterBox2.body.debugShowBody = true;

        letterBox2.body.setBounce(1, 1);
        this.grid.placeAtIndex(pos, letterBox);
    }
    
}