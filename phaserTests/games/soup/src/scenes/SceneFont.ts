
//import * as WebFont from "WebFont";

import WebFontFile from "../classes/WebFontFile";

export class SceneFonts extends Phaser.Scene {
    private fontsLoaded: number = 0;
    private fonts: string[] = [];
    public WebFont:any;
    constructor() {
        super("SceneFonts");
    }
    
    preload() {
        
       // this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js');
       this.load.addFile(new WebFontFile(this.load,['Palanquin Dark','Dela Gothic One']));
    }
    create() {
        console.log("CREATE");
        
        setTimeout(()=>{this.scene.start("SceneMain");},2000);
      //  WebFont.load({ google: { families: this.fonts }, loading: this.fontsLoading.bind(this), fontactive: this.fontActive.bind(this) });
    }
    
}