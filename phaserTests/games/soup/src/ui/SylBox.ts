import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class SylBox extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private bScene:IBaseScene;
    private textObj:Phaser.GameObjects.Text;
    public letters:string;
    public callback:Function=()=>{};

    constructor(bscene:IBaseScene,letters:string)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.bScene=bscene;
        this.letters=letters;

        let back:Phaser.GameObjects.Sprite=this.scene.add.sprite(0,0,"carrot");
        Align.scaleToGameW(back,0.1,this.bScene);
        this.textObj=this.scene.add.text(0,0,letters,{fontFamily:"Arial",fontSize:"20px",color:"#000000"});
        this.textObj.setOrigin(0.5,0.5);

        back.displayWidth=this.textObj.displayWidth*1.5;
        back.displayHeight=back.displayWidth;
        //
        //
        //
        this.add(back);
        this.add(this.textObj);
        this.setSize(back.displayWidth,back.displayHeight);

        this.setInteractive();

        this.on('pointerdown',()=>{this.callback(this)});

        this.scene.add.existing(this);
    }
    destroyMe()
    {
        console.log(this.letters);
        this.destroy();
    }
}