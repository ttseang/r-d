import { GameObjects } from "phaser";
import { Box } from "../classes/Box";
import { Coin } from "../classes/Coin";
import { Button } from "../classes/comps/Button";
import { ResizeManager } from "../classes/resizeManager";
import { LayoutVo } from "../dataObjs/LayoutVo";
//import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";
//import { SpineAnim2 } from "../classes/SpinAnim2";
//import { GameObjects } from "phaser";
//import { FallingObj } from "../classes/FallingObj";

export class SceneMain extends BaseScene implements IBaseScene {
    private button1: Button;
    private resizeManager: ResizeManager;

    private selectedCoin: Coin | null = null;
    private coins: Coin[] = [];
    private clickLock: boolean = false;
    private numberOfCoins: number = 100;
    private bar: GameObjects.Image;
    private text1:GameObjects.Text;
    private text2:GameObjects.Text;
    private targetAmount:number=0;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");

        this.load.image("button", "ui/buttons/1/1.png");
        this.load.image("penny", "penny.png");
        this.load.image("nickle", "nickle.png");
        this.load.image("dime", "dime.png");
        this.load.image("quarter", "quarter.png");
        this.load.image("box", "box.png");
        this.load.image("bg", "bg.jpg");
        this.load.image("holder", "holder.jpg")

        this.load.audio("coin", "quiz_right.wav");
        this.load.audio("wrong", "quiz_wrong.wav");
        this.load.audio("select", "click.wav");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);
        //    this.grid.showPos();

        let bg: GameObjects.Image = this.add.image(0, 0, "bg");
        bg.displayWidth = this.gw;
        bg.scaleY = bg.scaleX;

        if (this.gw < this.gh) {
            bg.displayHeight = this.gh;
            bg.scaleX = bg.scaleY;
        }
        Align.center(bg, this);

        this.resizeManager = ResizeManager.getInstance(this);

        //   window['scene'] = this;

        this.makeChildren();
        // this.definePos();
        this.placeItems();
        this.setTargetAmount();

        this.input.once("gameobjectdown", this.selectCoin.bind(this));
    }
    setTargetAmount()
    {
        this.targetAmount=Math.floor((Math.random()*200))/100;
        this.text2.setText(this.formatCurrency(this.targetAmount));
        this.text1.setText(this.formatCurrency(0));
    }
    selectCoin(pointer: Phaser.Input.Pointer, coin: Coin) {
        if (this.clickLock === true) {
            return;
        }
        this.playSound("select");
        //coin.alpha=0.5;
        this.selectedCoin = coin;
        this.children.bringToTop(this.selectedCoin);
        this.input.on("pointermove", this.dragCoin.bind(this));
        this.input.once("pointerup", this.dropCoin.bind(this));
    }
    playSound(key: string) {
        let sound: Phaser.Sound.BaseSound = this.sound.add(key);
        sound.play();
    }
    dropCoin() {
        this.selectedCoin.updatePos();
        this.playSound("select");
        //
        //
        //
        this.selectedCoin = null;
        this.input.off("pointermove", this.dragCoin.bind(this));
        this.input.once("gameobjectdown", this.selectCoin.bind(this));
        let count:number=this.countCoins();
        this.text1.setText(this.formatCurrency(count));
        if (count===this.targetAmount)
        {
            this.playSound("coin");
            setTimeout(() => {
                this.setTargetAmount();
                this.replaceCoins();
            }, 1000);
        }
    }
    dragCoin(pointer: Phaser.Input.Pointer) {
        if (this.selectedCoin) {
            this.selectedCoin.x = pointer.x;
            this.selectedCoin.y = pointer.y;
        }
    }
    countCoins() {

        let val:number=0;
        let coinVals:number[]=[.01,.05,.10,.25];

        let startX:number=this.bar.x-this.bar.displayWidth/2;
        let startY:number=this.bar.y-this.bar.displayHeight/2;

        let endX:number=this.bar.x+this.bar.displayWidth/2;
        let endY:number=this.bar.y+this.bar.displayHeight/2;

        for (let i:number=0;i<this.coins.length;i++)
        {
            let coin:Coin=this.coins[i];
            if (coin.x>startX && coin.x<endX)
            {
                if (coin.y>startY && coin.y<endY)
                {
                    val+=coinVals[coin.index];
                 //   coin.alpha=0.5;
                }
            }
        }
        val=Math.round(val*100)/100;
        console.log(val);

        return val;
    }
    formatCurrency(count:number)
    {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
          
            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
          });
          return formatter.format(count);
    }
    replaceCoins()
    {
        let startX:number=this.bar.x-this.bar.displayWidth/2;
        let startY:number=this.bar.y-this.bar.displayHeight/2;

        let endX:number=this.bar.x+this.bar.displayWidth/2;
        let endY:number=this.bar.y+this.bar.displayHeight/2;

        for (let i:number=0;i<this.coins.length;i++)
        {
            let coin:Coin=this.coins[i];
            if (coin.x>startX && coin.x<endX)
            {
                if (coin.y>startY && coin.y<endY)
                {
                    coin.replace();
                }
            }
        }
    }
    makeChildren() {

        //this.button1 = new Button(this, "button", "Start");
        let coinVals: string[] = ["1¢", "5¢", "10¢", "25¢"];
        let keys: string[] = [];
        /* for (let i:number=0;i<4;i++)
        {
            let box:Box=new Box(this,coinVals[i],i);
            this.boxes.push(box);

            let boxKey:string="box"+i.toString();
            let boxLayout: LayoutVo = this.resizeManager.addItem(boxKey, box);
            boxLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",5+(i*4),5,.15));
            keys.push(boxKey);
        } */

        this.bar = this.add.image(0, 0, "holder");
        this.bar.type = "bar";
        this.bar.displayWidth = this.gw * 0.8;
        this.bar.displayHeight = this.gh * 0.25;

        let barLayout: LayoutVo = this.resizeManager.addItem("bar", this.bar);
        barLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 11, 6, -1));
        keys.push("bar");
        //
        //
        //

        this.text1=this.add.text(0,0,"current");
        this.text2=this.add.text(0,0,"target");

        let text1Layout: LayoutVo = this.resizeManager.addItem("text1", this.text1);
        text1Layout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 5, 2, -1));
        keys.push("text1");

        let text2Layout: LayoutVo = this.resizeManager.addItem("text2", this.text2);
        text2Layout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 15, 2, -1));
        keys.push("text2");

        let coinKeys: string[] = ["penny", "nickle", "dime", "quarter"];


        let coinScales: number[] = [0.05, 0.056, 0.047, 0.063];

        for (let i: number = 0; i < this.numberOfCoins; i++) {
            let index: number = Phaser.Math.Between(0, 3);
            let xx: number = Phaser.Math.Between(5, 17);
            let yy: number = Phaser.Math.Between(13, 17);
            let coin: Coin = new Coin(this, coinKeys[index], index, xx, yy);
            this.coins.push(coin);
            Align.scaleToGameW(coin, coinScales[index], this);

            let coinKey: string = "coin" + i.toString();
            let coinLayout: LayoutVo = this.resizeManager.addItem(coinKey, coin);
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", xx, yy, coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE, ResizeVo.ORIENTATION_PORTRAIT, xx, yy, coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE, ResizeVo.ORIENTATION_LANDSCAPE, xx, yy, coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET, ResizeVo.ORIENTATION_PORTRAIT, xx, yy, coinScales[index] * 2));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET, ResizeVo.ORIENTATION_LANDSCAPE, xx, yy, coinScales[index] * 2));


            coin.pos = new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", xx, yy, coinScales[index]);
            coin.setInteractive();

            coin.layout = coinLayout;

            coin.updatePos();

            keys.push(coinKey);
        }
        console.log(keys);

        this.resizeManager.setKeys(keys);
    }
    definePos() {
        let keys: string[] = [];
        //
        //
        //
        /* let buttonLayout: LayoutVo = this.resizeManager.addItem("button1", this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 10, 10, 0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,10,19,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,3,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,19,19,0.4));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,2,2,.1));

        keys.push("button1");

      */
        this.resizeManager.setKeys(keys);
        // this.resizeManager.setKeys(['button1','face']);

    }
    placeItems() {
        this.resizeManager.placeChildren();
    }
}