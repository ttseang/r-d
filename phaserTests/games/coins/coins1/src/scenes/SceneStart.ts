import { Button } from "../classes/comps/Button";
import { ResizeManager } from "../classes/resizeManager";
import { LayoutVo } from "../dataObjs/LayoutVo";
//import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "./BaseScene";
//import { SpineAnim2 } from "../classes/SpinAnim2";
//import { GameObjects } from "phaser";
//import { FallingObj } from "../classes/FallingObj";

export class SceneStart extends BaseScene implements IBaseScene {
    private button1: Button;
    private resizeManager: ResizeManager;

    constructor() {
        super("SceneStart");
    }
    preload() {
        this.load.setPath("./assets/");

        this.load.image("button", "ui/buttons/1/1.png");
        
    }
    create() {
        super.create();
        this.makeGrid(22, 22);
     //   this.grid.showPos();

        

        this.resizeManager = ResizeManager.getInstance(this);

        window['scene'] = this;

        this.makeChildren();
        this.definePos();
        this.placeItems();
    }
    makeChildren() {
       
       this.button1 = new Button(this, "button", "Start");
    }
    definePos() {
        let keys:string[]=[];
        //
        //
        //
        let buttonLayout: LayoutVo = this.resizeManager.addItem("button1", this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 10, 10, 0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,10,19,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,3,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,19,19,0.4));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,2,2,.1));

        keys.push("button1");

        this.resizeManager.setKeys(keys);

        // this.resizeManager.setKeys(['button1','face']);

    }
    placeItems() {
        this.resizeManager.placeChildren();
    }
}