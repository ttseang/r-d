import { GameObjects } from "phaser";
import { Box } from "../classes/Box";
import { Coin } from "../classes/Coin";
import { Button } from "../classes/comps/Button";
import { ResizeManager } from "../classes/resizeManager";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { PosVo } from "../dataObjs/PosVo";
//import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";
//import { SpineAnim2 } from "../classes/SpinAnim2";
//import { GameObjects } from "phaser";
//import { FallingObj } from "../classes/FallingObj";

export class SceneMain extends BaseScene implements IBaseScene {
    private button1: Button;
    private resizeManager: ResizeManager;
    
    private selectedCoin:Coin | null=null;
    private boxes:Box[]=[];
    private coins:Coin[]=[];
    private clickLock:boolean=false;
    private numberOfCoins:number=20;
   
    
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.setPath("./assets/");

        this.load.image("button", "ui/buttons/1/1.png");
        this.load.image("penny","penny.png");
        this.load.image("nickle","nickle.png");
        this.load.image("dime","dime.png");
        this.load.image("quarter","quarter.png");
        this.load.image("box","box.png");
        this.load.image("bg","bg.jpg");
        
        this.load.audio("coin","quiz_right.wav");
        this.load.audio("wrong","quiz_wrong.wav");
        this.load.audio("select","click.wav");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);
    //    this.grid.showPos();

        let bg:GameObjects.Image=this.add.image(0,0,"bg");
        bg.displayWidth=this.gw;
        bg.scaleY=bg.scaleX;
        
        if (this.gw<this.gh)
        {
            bg.displayHeight=this.gh;
            bg.scaleX=bg.scaleY;
        }
        Align.center(bg,this);

        this.resizeManager = ResizeManager.getInstance(this);

     //   window['scene'] = this;

        this.makeChildren();
       // this.definePos();
        this.placeItems();

        this.input.on("gameobjectdown",this.selectBox.bind(this))
    }
    selectBox(pointer:Phaser.Input.Pointer,box:Box)
    {
       // box.alpha=0.5;
        let first:Coin=this.coins[0];
        if (first.index===box.index)
        {
           let coin:Coin= this.coins.shift();
           this.tweens.add({targets: coin,duration: 250,y:box.y,x:box.x,onComplete:(tween:any,target:Coin)=>{
                coin.destroy();
           }});
            this.lineUpCoins();
            
            this.playSound("coin");

            if (this.coins.length===0)
            {
                this.scene.restart();
            }
        }
        else
        {
            this.playSound("wrong");
        }
        
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    
  
    makeChildren() {
       
        this.resizeManager.clear();
       //this.button1 = new Button(this, "button", "Start");
       let coinVals:string[]=["1¢","5¢","10¢","25¢"];
       let keys:string[]=[];
       let boxPos:PosVo[]=[new PosVo(8,4),new PosVo(15,4),new PosVo(8,9),new PosVo(15,9)];

        for (let i:number=0;i<4;i++)
        {
            let box:Box=new Box(this,coinVals[i],i);
            this.boxes.push(box);
            box.setInteractive();

            let boxKey:string="box"+i.toString();
            let boxLayout: LayoutVo = this.resizeManager.addItem(boxKey, box);
            boxLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",boxPos[i].x,boxPos[i].y,.125));
            keys.push(boxKey);
        }

        let coinKeys:string[]=["penny","nickle","dime","quarter"];
       

        let coinScales:number[]=[0.05,0.056,0.047,0.063];

        for (let i:number=0;i<this.numberOfCoins;i++)
        {
            let index:number=Phaser.Math.Between(0,3);
            let xx:number=5+i;
            let yy:number=15;
            let coin:Coin=new Coin(this,coinKeys[index],index,xx,yy);
            this.coins.push(coin);
            Align.scaleToGameW(coin,coinScales[index],this);

            let coinKey:string="coin"+i.toString();
            let coinLayout:LayoutVo=this.resizeManager.addItem(coinKey,coin);
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",xx,yy,coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,xx,yy,coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,xx,yy,coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,xx,yy,coinScales[index]*2));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,xx,yy,coinScales[index]*2));
            

            coin.pos=new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",xx,yy,coinScales[index]);
            

            coin.layout=coinLayout;
            
            coin.updatePos();

            keys.push(coinKey);
        }
        console.log(keys);
        this.lineUpCoins();

        this.resizeManager.setKeys(keys);
    }
    lineUpCoins()
    {   
        let xx:number=this.gw/4;
        console.log(this.coins);

        for(let i:number=0;i<this.coins.length;i++)
        {
            let coin:Coin=this.coins[i];
            
            coin.x=xx;
            xx+=coin.displayWidth;
            coin.updatePos();
        }
    }
    definePos() {
        let keys:string[]=[];
        //
        //
        //
        /* let buttonLayout: LayoutVo = this.resizeManager.addItem("button1", this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 10, 10, 0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,10,19,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,3,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,19,19,0.4));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,2,2,.1));

        keys.push("button1");

      */
        this.resizeManager.setKeys(keys); 
        // this.resizeManager.setKeys(['button1','face']);

    }
    placeItems() {
        this.resizeManager.placeChildren();
    }
}