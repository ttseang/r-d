import { GameObjects } from "phaser";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { GM } from "./GM";

export class Coin extends GameObjects.Sprite
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;

    public xx:number=0;
    public yy:number=0;
    public layout:LayoutVo;
    public pos:ResizeVo;
    private grid:AlignGrid;
    private gm:GM=GM.getInstance();
    public index:number=0;
    public startPos:PosVo;

    constructor(bscene:IBaseScene,key:string,index:number,xx:number,yy:number)
    {
        super(bscene.getScene(),0,0,key);
        this.bscene=bscene;
        this.type="coin";
        this.index=index;
        
        this.scene=bscene.getScene();

        this.scene.add.existing(this);
        this.xx=xx;
        this.yy=yy;
        bscene.getGrid().placeAt(xx,yy,this);
        this.grid=bscene.getGrid();
        this.startPos=new PosVo(this.x,this.y);

        this.angle=Math.floor(Math.random()*360);
    }
    replace()
    {
        this.scene.tweens.add({targets: this,duration:250,y:this.startPos.y,x:this.startPos.x});
    }
    shrink()
    {
        this.scene.tweens.add({targets: this,duration: 250,scaleX:0,scaleY:0,onComplete:this.shrinkDone.bind(this)});
    }
    shrinkDone()
    {
        this.destroy();
    }
    updatePos()
    {       
       let posVo:PosVo=this.grid.findNearestGridXY(this.x,this.y);
       // console.log(posVo);
        
        this.pos.posX=posVo.x;
        this.pos.posY=posVo.y;

    }
    update()
    {
        let rpos:ResizeVo=this.layout.getDefinition(this.gm.device,this.gm.orientation);
        Align.scaleToGameW(this,rpos.scale,this.bscene);

 //       this.updatePos();
        //this.pos=this.layout.getDefinition(this.gm.device,this.gm.orientation);
        this.grid=this.bscene.getGrid();
        this.grid.placeAt(this.pos.posX,this.pos.posY,this);

        this.startPos=new PosVo(this.x,this.y);
    }
}