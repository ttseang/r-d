import { GameObjects } from "phaser"
import IBaseScene from "../interfaces/IBaseScene";

export class Box extends GameObjects.Container
{
    public scene:Phaser.Scene;
    public index:number;
    private back:GameObjects.Image;

    constructor(bscene:IBaseScene,text:string,index:number)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.index=index;
        //
        //
        //       
        this.back=this.scene.add.image(0,0,"box");
        this.add(this.back);
        //
        //
        //
        let text1:GameObjects.Text=this.scene.add.text(0,0,text,{fontSize:"26px"}).setOrigin(0.5,0.5);
        this.add(text1);
        //
        //
        //
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);
    }
}