import { GameObjects } from "phaser";
import { Box } from "../classes/Box";
import { Coin } from "../classes/Coin";
import { CoinStack } from "../classes/CoinStack";
import { Button } from "../classes/comps/Button";
import { ResizeManager } from "../classes/resizeManager";
import { LayoutVo } from "../dataObjs/LayoutVo";
//import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";


export class SceneMain extends BaseScene implements IBaseScene {
    private button1: Button;
    private resizeManager: ResizeManager;
    
    private selectedCoin:Coin | null=null;
    private boxes:Box[]=[];
    private clickLock:boolean=false;
    private numberOfCoins:number=30;
    private tubes:Phaser.GameObjects.Image;
    public stacks:CoinStack[]=[];

    constructor() {
        super("SceneMain");
        (window as any).scene=this;
    }
    preload() {
        this.load.setPath("./assets/");

        this.load.image("button", "ui/buttons/1/1.png");
        this.load.image("penny","penny.png");
        this.load.image("nickle","nickle.png");
        this.load.image("dime","dime.png");
        this.load.image("quarter","quarter.png");
        this.load.image("box","box.png");
        this.load.image("bg","bg.jpg");
        this.load.image("tubes","tubes.jpg");
        this.load.image("flatsilver","flat_silver.png");
        this.load.image("flatpenny","flatpenny.png");

        this.load.image("face","face.png");
        
        this.load.audio("coin","quiz_right.wav");
        this.load.audio("wrong","quiz_wrong.wav");
        this.load.audio("select","click.wav");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);
    //    this.grid.showPos();

        let bg:GameObjects.Image=this.add.image(0,0,"bg");
        bg.displayWidth=this.gw;
        bg.scaleY=bg.scaleX;
        
        if (this.gw<this.gh)
        {
            bg.displayHeight=this.gh;
            bg.scaleX=bg.scaleY;
        }
        Align.center(bg,this);

        this.resizeManager = ResizeManager.getInstance(this);

     //   window['scene'] = this;
        this.tubes=this.add.image(0,0,"tubes").setOrigin(0,0);
        Align.scaleToGameW(this.tubes,0.35,this);
        this.grid.placeAt(2,2,this.tubes);
        //this.tubes.alpha=.5;

        this.makeChildren();
       // this.definePos();
        this.placeItems();

        this.input.once("gameobjectdown",this.selectCoin.bind(this))
    }
    findNearestBox()
    {
        for (let i:number=0;i<this.boxes.length;i++)
        {
            let box:Box=this.boxes[i];
            let boxEndX=box.x+box.displayWidth/2;
            let boxEndY=box.y+box.displayHeight/2;
            let boxStartX=box.x-box.displayWidth/2;
            let boxStartY=box.y-box.displayHeight/2;
            //
            //
            //
        


            if (this.selectedCoin.x>boxStartX && this.selectedCoin.x<boxEndX)
            {
                if (this.selectedCoin.y>boxStartY && this.selectedCoin.y<boxEndY)
                {
                    return box;
                }
            }
        }
        return null;
    }
    selectCoin(pointer:Phaser.Input.Pointer,coin:Coin)
    {
        if (this.clickLock===true)
        {
            return;
        }
        this.playSound("select");
        //coin.alpha=0.5;
        this.selectedCoin=coin;
        this.children.bringToTop(this.selectedCoin);
        this.input.on("pointermove",this.dragCoin.bind(this));
        this.input.once("pointerup",this.dropCoin.bind(this));
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    dropCoin()
    {
        this.selectedCoin.updatePos();
        
        let dropBox=this.findNearestBox();
        console.log(dropBox);
        
        if (dropBox)
        {
            //dropBox.alpha=.5;
            /* console.log(dropBox.index);
            console.log(this.selectedCoin.index); */


            if (dropBox.index!==this.selectedCoin.index)
            {
                this.playSound("wrong");
                this.selectedCoin.replace();
            }
            else
            {
                this.playSound("coin");
                this.selectedCoin.drop();
            }
        }

        this.selectedCoin=null;
        this.input.off("pointermove",this.dragCoin.bind(this));
        this.input.once("gameobjectdown",this.selectCoin.bind(this));

    }
    dragCoin(pointer:Phaser.Input.Pointer)
    {
        if (this.selectedCoin)
        {
            this.selectedCoin.x=pointer.x;
            this.selectedCoin.y=pointer.y;
        }
    }
    makeChildren() {
       
       //this.button1 = new Button(this, "button", "Start");
       let coinVals:string[]=["1¢","5¢","10¢","25¢"];
       coinVals.reverse();
       let keys:string[]=[];
        for (let i:number=0;i<4;i++)
        {
            let box:Box=new Box(this,coinVals[i],3-i);
            this.boxes.push(box);
           // box.alpha=.01;

            let boxKey:string="box"+i.toString();
            let boxLayout: LayoutVo = this.resizeManager.addItem(boxKey, box);
            boxLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",3+(i*1.85),4,.05));
            keys.push(boxKey);

            let coinStack:CoinStack=new CoinStack(this,"flatsilver");

           /*  let stackKey:string="stack"+i.toString();
            let stackLayout: LayoutVo = this.resizeManager.addItem(stackKey, coinStack);
            stackLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",3+(i*1.85),6,.2));
            keys.push(stackKey); */


            //coinStack.x=box.x;
           // coinStack.y=this.gw*0.4;
            this.stacks.push(coinStack);

        }

        let coinKeys:string[]=["penny","nickle","dime","quarter"];
        

        let coinScales:number[]=[0.05,0.056,0.047,0.063];

        for (let i:number=0;i<this.numberOfCoins;i++)
        {
            let index:number=Phaser.Math.Between(0,3);
            let xx:number=Phaser.Math.Between(11,17);
            let yy:number=Phaser.Math.Between(5,13);
            let coin:Coin=new Coin(this,coinKeys[index],index,xx,yy);
            Align.scaleToGameW(coin,coinScales[index],this);

            let coinKey:string="coin"+i.toString();
            let coinLayout:LayoutVo=this.resizeManager.addItem(coinKey,coin);
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",xx,yy,coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,xx,yy,coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,xx,yy,coinScales[index]));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,xx,yy,coinScales[index]*2));
            coinLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,xx,yy,coinScales[index]*2));
            

            coin.pos=new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",xx,yy,coinScales[index]);
            coin.setInteractive();

            coin.layout=coinLayout;
            
            coin.updatePos();

            keys.push(coinKey);
        }
        console.log(keys);

        this.resizeManager.setKeys(keys);
    }
    definePos() {
        let keys:string[]=[];
        //
        //
        //
        /* let buttonLayout: LayoutVo = this.resizeManager.addItem("button1", this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 10, 10, 0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,10,19,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,3,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,19,19,0.4));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,2,2,.1));

        keys.push("button1");

      */
        this.resizeManager.setKeys(keys); 
        // this.resizeManager.setKeys(['button1','face']);

    }
    placeItems() {
        this.resizeManager.placeChildren();

        for (let i:number=0;i<this.stacks.length;i++)
        {
            this.stacks[i].x=this.boxes[i].x;
            this.stacks[i].y=this.boxes[i].y+this.gw/6;
        }
    }
}