import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class CoinStack extends Phaser.GameObjects.Container
{
    private stackSize:number=0;
    private coins:Phaser.GameObjects.Image[]=[];
    private key:string;
    private bscene:IBaseScene;

    constructor(bscene:IBaseScene,key:string)
    {
        super(bscene.getScene());
        this.key=key;
        this.bscene=bscene;

       /*  let face:GameObjects.Image=this.scene.add.image(0,0,"face");
        this.add(face); */
        
        this.scene.add.existing(this);
    }

   
    addCoin()
    {
        let coin:Phaser.GameObjects.Image=this.scene.add.image(0,0,this.key);
        Align.scaleToGameW(coin,0.055,this.bscene);
        this.coins.push(coin);
        this.add(coin);
        this.lineUpCoins();
    }
    lineUpCoins()
    {
        let yy:number=0;
        for (let i:number=0;i<this.coins.length;i++)
        {
            let coin:Phaser.GameObjects.Image=this.coins[i];
            coin.y=yy;
            yy-=coin.displayHeight;
        }
    }
}