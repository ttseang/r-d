/**
 * allows us to pass a sprite or image
 * as an IGameObj
 */
export interface IGameObj
{
    displayWidth:number;
    displayHeight:number;
    scaleX:number;
    scaleY:number;
    x:number;
    y:number;
    alpha:number;
    visible:boolean;
}