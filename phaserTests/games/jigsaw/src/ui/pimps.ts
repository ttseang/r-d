import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class Pimps extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;

    public pmaskWest:Phaser.GameObjects.Sprite;
    public pmaskEast:Phaser.GameObjects.Sprite;
    public pmaskNorth:Phaser.GameObjects.Sprite;
    public pmaskSouth:Phaser.GameObjects.Sprite;

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.scene=bscene.getScene();

        this.pmaskWest=this.scene.add.sprite(0,0,'pimps').setOrigin(0,0);
        this.add(this.pmaskWest);
        Align.scaleToGameW(this.pmaskWest,0.1,this.bscene);
        this.pmaskWest.x-=this.pmaskWest.displayWidth/2;
        
        //
        //
        //
        this.pmaskEast=this.scene.add.sprite(0,0,'pimps').setOrigin(0,0);
        this.pmaskEast.flipX=true;
        this.add(this.pmaskEast);
        Align.scaleToGameW(this.pmaskEast,0.1,this.bscene);
        this.pmaskEast.x+=this.pmaskEast.displayWidth/2;
       // this.pmaskEast.setFrame("pimp2");
        //
        //
        //
        this.pmaskNorth=this.scene.add.sprite(0,0,'pimps');//.setOrigin(0,0);
        this.pmaskNorth.setAngle(90);
        this.add(this.pmaskNorth);
        Align.scaleToGameW(this.pmaskNorth,0.1,this.bscene);
        this.pmaskNorth.x+=this.pmaskNorth.displayWidth/2;
      //  this.pmaskNorth.y+=this.pmaskNorth.displayHeight/2;
        //
        //
        //
        this.pmaskSouth=this.scene.add.sprite(0,0,'pimps');//.setOrigin(0,0);
        this.pmaskSouth.setAngle(-90);
        this.add(this.pmaskSouth);
        Align.scaleToGameW(this.pmaskSouth,0.1,this.bscene);
        this.pmaskSouth.x+=this.pmaskSouth.displayWidth/2;
        this.pmaskSouth.y+=this.pmaskNorth.displayHeight;

      //  this.scene.add.existing(this);
    }
}
export default Pimps;