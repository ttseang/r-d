import IBaseScene from "../interfaces/IBaseScene";
import { UIBlock } from "../util/UIBlock";
import Pimps from "./pimps";

export class Jig {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private square: Phaser.GameObjects.Image;
    private dragArea: Phaser.GameObjects.Image;
    private children: ChildImage[] = [];
    private _x: number = 0;
    private _y: number = 0;
    public onDown: Function = () => { };
    public onUp: Function = () => { };
    public index:number=0;

    public image:Phaser.GameObjects.Image;

    public north:number=0;
    public south:number=0;
    public east:number=0;
    public west:number=0;
    public xx:number=0;
    public yy:number=0;

    constructor(bscene: IBaseScene,index:number) {
        //super();
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.index=index;

        /* this.square = this.scene.add.image(0, 0, "holder").setOrigin(0, 0);
        this.square.displayWidth = 200;
        this.square.displayHeight = 200;
        this.square.alpha = 0.1;
        this.add(this.square); */

        //  this.add(this.image);     



    }
    add(image: Phaser.GameObjects.Image) {
        let childImage: ChildImage = new ChildImage(this, image);

        this.children.push(childImage);
    }
    setRandPimps()
    {
        this.north=Phaser.Math.Between(1,3);
        this.south=Phaser.Math.Between(1,3);
        this.west=Phaser.Math.Between(1,3);
        this.east=Phaser.Math.Between(1,3);
    }
    setPimps(n: number, e: number, s: number, w: number)
    {
        this.north=n;
        this.south=s;
        this.east=e;
        this.west=w;
    }
    addPimps() {
        let northKey: string = "pn" + this.north.toString();
        let eastKey: string = "pe" + this.east.toString();
        let southKey: string = "ps" + this.south.toString();
        let westKey: string = "pw" + this.west.toString();

        let p1 = this.scene.textures.get(westKey).getSourceImage() as HTMLImageElement;
        let p2 = this.scene.textures.get(eastKey).getSourceImage() as HTMLImageElement;
        let p3 = this.scene.textures.get(southKey).getSourceImage() as HTMLImageElement;
        let p4 = this.scene.textures.get(northKey).getSourceImage() as HTMLImageElement;



        let canvasFrame: Phaser.Textures.CanvasTexture = this.scene.textures.createCanvas("pimp"+this.index.toString(), 250, 250);
        canvasFrame.draw(25, 25, p1);
        canvasFrame.draw(25, 25, p2);
        canvasFrame.draw(25, 25, p3);
        canvasFrame.draw(25, 25, p4);

        let p = this.scene.add.image(100, 100, 'pimp'+this.index.toString());


        let image = this.scene.add.sprite(-this.xx*100, -this.yy*100, "puzzleimage").setOrigin(0, 0);
        this.image=image;
        let mask = new Phaser.Display.Masks.BitmapMask(this.scene, p);

        image.setMask(mask);


        this.dragArea = this.scene.add.image(100, 100, "holder");
        this.dragArea.displayWidth = 100;
        this.dragArea.displayHeight = 100;
        this.dragArea.alpha = 0.01;
        this.dragArea.setInteractive();

        
        this.dragArea.on('pointerdown', this.selectMe.bind(this));

        this.add(this.dragArea);
        this.add(p);
        this.add(image);
    }
    setPos(xx:number,yy:number)
    {
        this.xx=xx;
        this.yy=yy;
       /*  this.image.x=-xx*2;
        this.image.y=-yy*2; */
    }
    selectMe() {
        if (this.onDown) {
            this.onDown(this);
        }
        //this.dragArea.once('pointerup', this.unselect.bind(this));
    }
    unselect() {
        if (this.onUp) {
            this.onUp();
        }
        this.dragArea.once('pointerdown', this.selectMe.bind(this));
    }
    set x(val: number) {
        this._x = val;
       // console.log(this._x);
        this.updateChildPos();
    }
    get x() {
        return this._x;
    }
    set y(val: number) {
        this._y = val;
        this.updateChildPos();
    }
    get y() {
        return this._y;
    }
    updateChildPos() {
        this.children.forEach((child: ChildImage) => {
            child.image.x = this.x + child.diffX;
            child.image.y = this.y + child.diffY;
            //console.log(child.image);
        });
    }
}
export default Jig;

export class ChildImage {
    public diffX: number;
    public diffY: number;

    // public x:number;
    // public y:number;
    public image: Phaser.GameObjects.Image;

    constructor(parent: Jig, image: Phaser.GameObjects.Image) {
        this.image = image;
        this.diffX = image.x - parent.x;
        this.diffY = image.y - parent.y;
      //  console.log(this.diffX, this.diffY);
    }
}