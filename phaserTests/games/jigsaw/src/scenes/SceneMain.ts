import Jig from "../ui/jig";
import Pimps from "../ui/pimps";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private image: Phaser.GameObjects.Image;
    private current: Jig;
    private diffX: number = 0;
    private diffY: number = 0;

    constructor() {
        super("SceneMain");
    }
    preload() {
        // this.load.image("face", "./assets/face.png");

        this.load.atlas("pimps", "./assets/pimp.png", "./assets/pimp.json");
        this.load.image('puzzleimage', "./assets/image2.png");
        this.load.image("square", "./assets/square.png");
        this.load.image("holder", "./assets/holder.jpg");

        let dirs = ['n', 's', 'e', 'w'];
        for (let i: number = 0; i < dirs.length; i++) {
            for (let j: number = 1; j < 4; j++) {
                let key: string = "p" + dirs[i] + j.toString();
                let path: string = "./assets/pimps/" + dirs[i] + "/" + j.toString() + ".png";
                this.load.image(key, path);
            }
        }


    }
    create() {
        super.create();
        this.makeGrid(11, 11);
       
        /*  jig.addPimps(2, 2, 2, 2);
         jig.onDown=this.onSelect.bind(this);
         jig.onUp=this.onUp.bind(this); */
        this.makePieces();
        /*  this.input.on('pointerdown', () => {
             jig.x = jig.x + 5;
             // this.image.setMask(mask);
         }); */
        this.input.on('pointerup', this.onUp.bind(this));
    }
    makePieces() {
        let index:number=0;

        for (let i: number = 0; i < 4; i++) {
            for (let j: number = 0; j < 4; j++) {
                index++;

                let jig: Jig = new Jig(this,index);
                jig.setPos(i,j);
                jig.setRandPimps();
              //  jig.setPimps(1, 2, 3,3);
                jig.addPimps();
                jig.x=i*100;
                jig.y=j*100;
                
                jig.onDown = this.onSelect.bind(this);
                
            }
        }
    }
    onSelect(jig: Jig) {
        this.current = jig;
        this.diffX = jig.x - this.input.activePointer.x;
        this.diffY = jig.y - this.input.activePointer.y;

        this.input.on('pointermove', this.updatePos.bind(this))
    }
    updatePos() {
        if (this.current) {
            this.current.x = this.input.activePointer.x + this.diffX;
            this.current.y = this.input.activePointer.y + this.diffY;
        }
    }
    onUp() {
        this.current = null;
        this.input.off('pointermove');
      //  console.log("on up");
    }
}