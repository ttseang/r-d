import { WordHolder } from "../classes/WordHolder";
import { WordVo } from "../dataObjs/WordVo";
import IBaseScene from "../interfaces/IBaseScene";
import { AlignGrid } from "../util/alignGrid";

export class InstructPanel extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    public textObj:Phaser.GameObjects.Text;
    public callback:Function=()=>{};
    public soundCallback:Function=()=>{};
    private grid:AlignGrid;
    private words:WordHolder[]=[];
    private bscene:IBaseScene;

    constructor(bscene:IBaseScene,grid:AlignGrid)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.bscene=bscene;
        this.grid=grid;
        let back:Phaser.GameObjects.Image=this.scene.add.image(0,0,"holder").setOrigin(0,0);
        back.setTint(0x2ecc71);
        back.displayWidth=bscene.getW();
        back.displayHeight=bscene.getH()*0.45;
        back.alpha=0.8;
        this.add(back);

        //
        //
        //
        this.textObj=this.scene.add.text(0,0,"Instructions here").setOrigin(0.5,0.5);
        this.add(this.textObj);
        this.scene.add.existing(this);

        this.setSize(back.displayWidth,back.displayHeight);
        this.grid.placeAtIndex(49,this.textObj)


        let btnSound=this.scene.add.image(0,0,"sb");
      //  btnSound.x=back.x-back.displayWidth/2+btnSound.displayWidth;
      //  btnSound.y=back.y-back.displayHeight/2+btnSound.displayHeight;
        this.grid.placeAtIndex(0,btnSound)
        this.add(btnSound);
        btnSound.setInteractive();
        btnSound.on('pointerdown',()=>{this.soundCallback()});
    }
    destroyBoxes()
    {
        for(let i:number=0;i<this.words.length;i++)
        {
            let holder:WordHolder=this.words[i];
            holder.destroy();
        }
    }
    makeBoxes(words:WordVo[],sound:string)
    {
        let pos:number[]=[14,18];
        for(let i:number=0;i<words.length;i++)
        {
            
            let holder:WordHolder=new WordHolder(words[i],this.bscene);
            holder.index=i;
            holder.callback=this.callback;
            this.grid.placeAtIndex(pos[i],holder);
            this.add(holder);
        }
        this.textObj.setText("Pick the word that ends with a "+sound+" sound");
    }
}   