export class WordVo {
    public id: number;
    public word: string;
    public correct:number;
    constructor(id: number, word: string,correct:number=0) {
        this.id = id;
        this.word = word;
        this.correct=correct;
    }
}