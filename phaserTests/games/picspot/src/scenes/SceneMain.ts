import { WordHolder } from "../classes/WordHolder";
import { WordVo } from "../dataObjs/WordVo";
//import { InstructPanel } from "../ui/InstructPanel";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private monkey: Phaser.Physics.Arcade.Sprite;
    private floorGroup: Phaser.Physics.Arcade.Group;

    /* private sides: Phaser.GameObjects.TileSprite;
    private sky: Phaser.GameObjects.Sprite;
    private decor: Phaser.GameObjects.TileSprite; */
    private back: Phaser.GameObjects.TileSprite;
    //private ground:Phaser.GameObjects.Sprite;
    private instructText: Phaser.GameObjects.Text;

    private runFlag: boolean = false;
    private speed: number = 10;
    // private instructions: InstructPanel;
    private wordIndex: number = -1;
    private wordData: any;
    private correct: number = 0;
    private wordHolders: WordHolder[] = [];

    constructor() {
        super("SceneMain");
    }
    preload() {
        //this.load.image("face", "./assets/images/face.png");
        this.load.json("wordData", "./assets/data/wordData.json");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.showNumbers();
        this.setBackground();
        this.floorGroup = this.physics.add.group();
        this.makeFloor();

        this.monkey = this.physics.add.sprite(0, 0, "monkey");
        Align.scaleToGameW(this.monkey, 0.25, this);
        this.grid.placeAtIndex(2, this.monkey);
        this.monkey.setVelocityY(200);

        this.makeAnimations();



        this.wordData = this.cache.json.get("wordData");
        this.instructText = this.add.text(0, 0, "INSTRUCTION", { color: '#ffffff', backgroundColor: '#000000', fontSize: '20px', align: 'center' }).setOrigin(0.5, 0.5);
        this.instructText.visible=false;

        this.grid.placeAtIndex(27, this.instructText);
        //   this.input.on("pointerdown", this.run.bind(this));



        this.physics.add.collider(this.monkey, this.floorGroup);

        setTimeout(() => { this.monkey.body.setSize(this.monkey.displayWidth, this.monkey.displayHeight), 1000 });
        setTimeout(() => { this.resetMonkey() }, 2000);
    }
    setQuestion() {
        this.wordIndex++;
        this.correct = 0;

        if (this.wordIndex === this.wordData.levels.length) {
            this.instructText.setText("Place winning animation here");
            let finalSound:Phaser.Sound.BaseSound=this.sound.add("chipquest");
            finalSound.play();
            return;
        }

        let current: any = this.wordData.levels[this.wordIndex];
        this.instructText.setText("Pick all the words start\nwith the sound of " + current.sound);


        for (let i: number = 0; i < current.words.length; i++) {
            let word: WordVo = new WordVo(current.words[i].id, current.words[i].word, parseInt(current.words[i].correct));
            let wordHolder: WordHolder = new WordHolder(word, this);
            wordHolder.callback = this.answer.bind(this);
            wordHolder.index = i;
            this.grid.placeAtIndex(46 + i * 2, wordHolder);
            this.wordHolders.push(wordHolder);
            if (current.words[i].correct === 1) {
                this.correct++;
            }
        }

    }
    answer(wordHolder: WordHolder) {
        let wordVo: WordVo = wordHolder.wordVo;
        wordHolder.visible = false;

        if (wordVo.correct === 1) {
            this.correct--;
            if (this.correct === 0) {
                this.run();
                let winSound: Phaser.Sound.BaseSound = this.sound.add("win");
                winSound.play();
            }
            else{
                let beepSound:Phaser.Sound.BaseSound=this.sound.add("beep");
                beepSound.play();
            }
        }
        else {
            let sound: Phaser.Sound.BaseSound = this.sound.add("error");
            sound.play();

            this.monkey.play("jump");
            setTimeout(() => {
                this.monkey.play("idle");
            }, 250);

        }
    }
    destroyBoxes() {
        for (let i: number = 0; i < this.wordHolders.length; i++) {
            this.wordHolders[i].destroy();
        }
    }
    makeFloor() {
        for (var i = 99; i < 110; i++) {
            let floorImage: Phaser.Physics.Arcade.Sprite = this.placePhysicImage('grass', i, 0.1);

            this.floorGroup.add(floorImage);
            floorImage.setImmovable();
        }

    }
    setBackground() {
        this.back = this.add.tileSprite(0, 0, this.gw, this.gh, "background").setOrigin(0, 0);
        this.back.setSize(this.gw, this.gh * 5);
        this.back.displayWidth = this.gw;
    }
    run() {
        this.destroyBoxes();

        this.runFlag = true;

        this.monkey.play("run");
        this.time.addEvent({ delay: 2000, callback: this.resetMonkey.bind(this), callbackScope: this, loop: false });
        this.instructText.visible = false;
        // let tw: Phaser.Tweens.Tween = this.tweens.add({ targets: this.instructions, duration: 500, y: -500 });
    }
    resetMonkey() {
        // this.grid.placeAtIndex(77, this.monkey);
        // this.monkey.setAngle(-45);
        this.instructText.visible = true;
        this.monkey.flipX = false;
        this.monkey.play("idle");
        this.runFlag = false;
        this.setQuestion();

    }
    makeAnimations() {

        let frameNames = this.anims.generateFrameNames('monkey', { start: 0, end: 17, zeroPad: 3, prefix: 'Idle_', suffix: '.png' })


        this.anims.create({
            key: 'idle',
            frames: frameNames,
            frameRate: 8,
            repeat: -1
        });

        let frameNames2 = this.anims.generateFrameNames('monkey', { start: 0, end: 4, zeroPad: 3, prefix: 'Jumping_', suffix: '.png' })


        this.anims.create({
            key: 'jump',
            frames: frameNames2,
            frameRate: 8,
            repeat: -1
        });


        let frameNames3 = this.anims.generateFrameNames('monkey', { start: 0, end: 13, zeroPad: 3, prefix: 'Running_', suffix: '.png' })


        this.anims.create({
            key: 'run',
            frames: frameNames3,
            frameRate: 32,
            repeat: -1
        });
    }
    update() {
        if (this.runFlag == true) {
            this.back.tilePositionX += this.speed;
        }

        /*  if (this.climbFlag === true) {
             this.clouds.tilePositionY -= this.speed / 2;
             //  this.sky.tilePositionY-=this.speed;
             this.decor.tilePositionY -= this.speed;
             this.sides.tilePositionY -= this.speed;
         } */
    }
}