import { JigP } from "../classes/JigP";

export class PuzzleUtil
{
    constructor()
    {
        
    }
    public isSib(p1:JigP,p2:JigP)
    {
        if (Math.abs(p1.xx-p2.xx)==1 || p1.yy==p2.yy)
        {
            return true;
        }
        if (Math.abs(p1.yy-p2.yy)==1 || p1.xx==p2.xx)
        {
            return true;
        }
        return false;
    }
    public isInRange(p1:JigP,p2:JigP)
    {
        //let xrange:number=p1.displayWidth*0.1;
        //let yrange:number=p1.displayHeight*0.1;

        let xrange:number=Math.abs(p1.ox-p2.ox)*0.1;
        let yrange:number=Math.abs(p1.oy-p2.oy)*0.1;

        let xDist:number=Math.abs(p1.x-p2.x);
        let yDist:number=Math.abs(p1.y-p2.y);

        if (xDist<xrange && yDist<yrange)
        {
            return true;
        }
        return false;
    }
    public snap(p1:JigP,p2:JigP)
    {
        let oDistX:number=p1.ox-p2.ox;
        let oDistY:number=p1.oy-p2.oy;

        p2.x=p1.x+oDistX;
        p2.y=p1.y+oDistY;
    }
}