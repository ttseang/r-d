import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";

export class SizeThumb extends Phaser.GameObjects.Image
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    public index:number;
    public frameName:string;

    constructor(bscene:IBaseScene,index:number,frameName:string)
    {
        super(bscene.getScene(),0,0,"sizes",frameName);
        this.bscene=bscene;
        this.index=index;
        this.frameName=frameName;

        Align.scaleToGameW(this,0.1,this.bscene);
        
        this.scene=bscene.getScene();
        this.scene.add.existing(this);

    }
}