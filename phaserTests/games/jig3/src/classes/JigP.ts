import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import Align from "../util/align";
import UIBlock from "../util/UIBlock";
import { JigModel } from "./Jigmodel";

export class JigP extends UIBlock implements IGameObj {
  public scene: Phaser.Scene;
  public scaleX: number = 1;
  public scaleY: number = 1;
  private maskImage: Phaser.GameObjects.Sprite;
  private image: Phaser.GameObjects.TileSprite;
  private square: Phaser.GameObjects.Image;
  //
  //
  //
  public northVal: number = 0;
  public eastVal: number = 0;
  public southVal: number = 0;
  public westVal: number = 0;
  //
  //
  //
  public northSib: JigP;
  public southSib: JigP;
  public eastSib: JigP;
  public westSib: JigP;
  public next: JigP;
  public prev: JigP;
  //
  //
  //
  public northConnected: boolean = false;
  public eastConnected: boolean = false;
  public southConnected: boolean = false;
  public westConnected: boolean = false;
  //
  //
  //
  public xx: number = 0;
  public yy: number = 0;

  public leftEdge: number = 0;
  public bottomEdge: number = 0;

  public ox: number = 0;
  public oy: number = 0;

  public callback: Function = () => { };
  public checked: boolean = false;
  public index: number = 0;

  public beenHere: boolean = false;
  private jm:JigModel=JigModel.getInstance();
  
  constructor(bscene: IBaseScene, key: string, xx: number, yy: number, scale: number) {
    super();
    this.scene = bscene.getScene();
    this.image = this.scene.add.tileSprite(0, 0, 500, 500, key);//.setOrigin(0,0);
    //  this.image.setTint(0xff0000);
    this.add(this.image);

    this.xx = xx;
    this.yy = yy;
    //
    //
    //

    Align.scaleToGameW(this.image, scale, bscene);

    this.maskImage = this.scene.make.sprite({ x: 0, y: 0, key: 'jig', add: false });//.setOrigin(0,0);
    this.maskImage.setScale(this.image.scaleX, this.image.scaleY);


    this.add(this.maskImage);
    this.image.setMask(new Phaser.Display.Masks.BitmapMask(this.scene, this.maskImage));

   


    this.square = this.scene.add.image(0, 0, "holder");//.setOrigin(0,0);
    this.square.displayWidth = 100;
    this.square.displayHeight = 100;
    this.square.alpha = 0.1;
    this.square.setScale(this.image.scaleX, this.image.scaleY);
    this.add(this.square);

    
    this.image.tilePositionX = (xx - 2) * 100;
    this.image.tilePositionY = (yy - 2) * 100;

    this.square.setInteractive();
    this.square.on('pointerdown', this.selectMe.bind(this));
    // this.square.on('pointerup', this.checkSibs.bind(this));
    this.setSize(this.square.displayWidth, this.square.displayHeight);

  //  //console.log(this);

  }
  bringToTop() {
    this.scene.children.bringToTop(this.image);
    this.scene.children.bringToTop(this.maskImage);
    this.scene.children.bringToTop(this.square);
  }
  sendToBack() {
    this.scene.children.sendToBack(this.image);
    this.scene.children.sendToBack(this.maskImage);
    this.scene.children.sendToBack(this.square);
  }
  deactivate() {
    this.square.removeInteractive();
  }
  selectMe() {
    /* if (this.westSib)
    {
      this.westSib.alpha=0.5;
    } */
    //console.log(this);
    this.jm.connected=[];
    //this.findConnections();
    this.callback(this);
    
  }
  public findConnections() {

    if (this.beenHere == false) {
      this.beenHere = true;
      this.jm.connected.push(this.index);

     ////console.log(this.index);
    //  this.square.setTint(0xff0000);
      if (this.westSib) {
        if (this.westConnected == true) {
          if (this.westSib.beenHere == false) {
            this.westSib.findConnections();
          }
        }

      }
      if (this.northSib) {
        if (this.northConnected == true) {
          if (this.northSib.beenHere == false) {
            this.northSib.findConnections();
          }
        }
      }
      if (this.southSib) {
        if (this.southConnected) {
          if (this.southSib.beenHere == false) {
            this.southSib.findConnections();
          }
        }
      }
      if (this.eastSib) {
        if (this.eastConnected == true) {
          if (this.eastSib.beenHere == false) {
            this.eastSib.findConnections();
          }
        }
      }
    }

  }
  public fixP() {
    if (this.yy === 0) {

      this.setNorth(2);
    }
    if (this.yy === this.bottomEdge) {

      this.setSouth(2);
    }

    if (this.xx === this.leftEdge) {

      this.setEast(2);
    }
    if (this.xx === 0) {

      this.setWest(2);
    }
    if (this.eastSib) {
      if (this.eastSib.westVal == 0) {
        this.setEast(1);
      }
      else {
        this.setEast(0);
      }
    }
    if (this.southSib) {
      if (this.southSib.northVal == 0) {
        this.setSouth(1);
      }
      else {
        this.setSouth(0);
      }
    }
    if (this.next) {
      this.next.fixP();
    }
  }
  public setNorth(n: number) {
    this.setPimps(n, this.eastVal, this.southVal, this.westVal);
  }
  public setWest(w: number) {
    this.setPimps(this.northVal, this.eastVal, this.southVal, w);
  }
  public setEast(e: number) {
    this.setPimps(this.northVal, e, this.southVal, this.westVal);
  }
  public setSouth(s: number) {
    this.setPimps(this.northVal, this.eastVal, s, this.westVal);
  }
  public setPimps(n: number, e: number, s: number, w: number) {
    this.northVal = n;
    this.southVal = s;
    this.eastVal = e;
    this.westVal = w;
    //
    //
    //
    let key: string = n.toString() + e.toString() + s.toString() + w.toString() + ".png";
    this.maskImage.setFrame(key);
  }
  public setRand() {
    let n: number = Phaser.Math.Between(0, 1);
    let e: number = Phaser.Math.Between(0, 1);
    let s: number = Phaser.Math.Between(0, 1);
    let w: number = Phaser.Math.Between(0, 1);

    this.setPimps(n, e, s, w);
  }
  public checkSibs() {
    // this.eastSib.alpha=0.5;
    if (this.eastSib) {
      if (this.isInRange(this.eastSib)) {
        if (this.eastConnected==false) {
          this.jm.madeConnection=true;
        }
        this.eastConnected = true;
        this.eastSib.westConnected = true;
        this.snap(this.eastSib);
      }
    }
    if (this.westSib) {
      if (this.isInRange(this.westSib)) {
        if (this.westConnected==false)
        {
          this.jm.madeConnection=true;
        }
        this.westConnected = true;
        this.westSib.eastConnected = true;
        this.snap(this.westSib);
      }
    }
    if (this.northSib) {
      if (this.isInRange(this.northSib)) {
        if (this.northConnected==false)
        {
          this.jm.madeConnection=true;
        }
        this.northConnected = true;
        this.northSib.southConnected = true;
        this.snap(this.northSib);
      }
    }
    if (this.southSib) {
      if (this.isInRange(this.southSib)) {
        if (this.southConnected==false)
        {
          this.jm.madeConnection=true;
        }
        //  this.southSib.image.setTint(0xff0000);
        this.southConnected = true;
        this.southSib.northConnected = true;
        this.snap(this.southSib);
      }
    }
  }
  public checkConn() {
    this.checkSibs();

    if (this.next) {
      this.next.checkConn();
    }
  }
  public updateSiblings(){

    if (this.checked == false) {
      this.checked = true;
      if (this.northConnected == true) {
        //  //console.log("n");
          // this.northSib.image.setTint(0xff000);
          if (this.northSib)
          {
            this.snapSibling(this.northSib);
          }   
        
        //
      }

      if (this.southConnected == true) {
        //  //console.log("s");
          if (this.southSib)
          {
            this.snapSibling(this.southSib);
          }    
        

        //  
      }
      if (this.eastConnected == true) {
       // //console.log("e");
          if (this.eastSib)
          {
            this.snapSibling(this.eastSib);
          }
      }
      if (this.westConnected == true) {
         // //console.log("w");
          if (this.westSib)
          {
            this.snapSibling(this.westSib);        
          }
          
        //
      }
     
      /*  if (this.next)
       {
         this.next.updateSiblings(current);
       } */

      if (this.southSib) {
        if (this.southSib.checked == false && this.southConnected == true) {
          this.southSib.updateSiblings();
        }
      }

      if (this.northSib) {
        if (this.northSib.checked == false && this.northConnected == true) {
          this.northSib.updateSiblings();
        }
      }

      if (this.eastSib) {
        if (this.eastSib.checked == false && this.eastConnected == true) {
          this.eastSib.updateSiblings();
        }
      }
      if (this.westSib) {
        if (this.westSib.checked == false && this.westConnected == true) {
          this.westSib.updateSiblings();
        }
      }

    }
    /* if (forward == true) {

      if (this.next) {
        this.next.updateSiblings(current, true);
      }
    }
    else {

      if (this.prev) {

        this.prev.updateSiblings(current, false);
      }
    } */
  }
  public clearChecked() {
    this.beenHere = false;
    this.checked=false;
    if (this.next) {
      this.next.clearChecked();
    }
  }
  public orginalSnap() {
    let xrange: number = this.displayWidth * 0.1;
    let yrange: number = this.displayHeight * 0.1;

    //console.log(xrange, yrange);

    if (yrange < 10) {
      yrange = 10;
    }
    if (xrange < 10) {
      xrange = 10;
    }
    let xDist: number = Math.abs(this.x - this.ox);
    let yDist: number = Math.abs(this.y - this.oy);

    if (xDist < xrange && yDist < yrange) {
      this.x = this.ox;
      this.y = this.oy;
    }
  }
  public isInRange(p1: JigP) {


    let originalDistX: number = (p1.ox - this.ox);
    let originalDistY: number = (p1.oy - this.oy);

    let tx: number = p1.x - originalDistX;
    let ty: number = p1.y - originalDistY;


    let distX: number = Math.abs(tx - this.x);
    let distY: number = Math.abs(ty - this.y);

   // //console.log(distX, distY);

    if (distX < 15 && distY < 15) {
      return true;
    }
    return false;

  }
  public snap(p1: JigP) {
    // this.image.setTint(0xff000);

    let oDistX: number = this.ox - p1.ox;
    let oDistY: number = this.oy - p1.oy;

    this.x = p1.x + oDistX;
    this.y = p1.y + oDistY;
    
  }
  public snapSibling(sib: JigP) {


    let oDistX: number = sib.ox - this.ox;
    let oDistY: number = sib.oy - this.oy;

    sib.x = this.x + oDistX;
    sib.y = this.y + oDistY;
  }
  public inPlace(): boolean {
    let oDistX: number = this.x - this.ox;
    let oDistY: number = this.y - this.oy; 

      if (Math.abs(oDistX)<20 && Math.abs(oDistY)<20) {
        return true;
      }
      return false;
  }
  public turnOff() {
    this.visible=false;
    if (this.next)
    {
      this.next.turnOff();
    }
  }
  
}