import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";

export class JigThumb extends Phaser.GameObjects.Image
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    public index:number;
    public frameName:string;

    constructor(bscene:IBaseScene,index:number,frameName:string)
    {
        super(bscene.getScene(),0,0,"thumbs",frameName);
        this.bscene=bscene;
        this.index=index;
        this.frameName=frameName;

        Align.scaleToGameW(this,0.08,this.bscene);
        
        this.scene=bscene.getScene();
        this.scene.add.existing(this);

    }
}