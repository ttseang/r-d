import { JigP } from "./JigP";

let instance:JigModel=null;

export class JigModel
{
    public connected:number[]=[];
    public pSize:number =100;
    public offset:number=100;

    public rows:number=3;
    public cols:number=3;
    public pscale:number=0.4;
    public image:string="tt1";
    public size:string="size0";
    
    public madeConnection:boolean = false;
    
    public static getInstance():JigModel
    {
        if (instance==null)
        {
            instance=new JigModel();
            window['jm']=instance;
        }
        return instance;
    }
}