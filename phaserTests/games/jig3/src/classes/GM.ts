
import { PosVo } from "../dataObjs/PosVo";

let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

   
    public file:string="farm";
    public size:string="size0";
    
    public iconMultipli:number=1;
    public cardScale:number=0.1;
    public selectStart:number=2.5;

    public pos:PosVo[]=[new PosVo(3, 3), new PosVo(5, 3), new PosVo(7, 3), new PosVo(3, 6), new PosVo(5, 6), new PosVo(7, 6)];

    constructor()
    {
        window['gm']=this;
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}