import { JigModel } from "../classes/Jigmodel";
import { SizeThumb } from "../classes/SizeThumb";
import { BaseScene } from "./BaseScene";

export class SceneSize extends BaseScene {
    private jm:JigModel=JigModel.getInstance();
    
    constructor()
    {
        super("SceneSize");
    }
    preload()
    {
        this.load.atlas("sizes","./assets/sizes.png","./assets/sizes.json");
    }
    create() {

        super.create();
        this.makeGrid(11, 11);

        let xx:number =3;

        for (let i: number = 0; i < 3; i++)
        {
            let sizeThumb:SizeThumb=new SizeThumb(this,i,"size"+i.toString()+".png");
            this.grid.placeAt(xx,5,sizeThumb);
            xx+=2;
            sizeThumb.setInteractive();
        }

        this.input.on("gameobjectdown",this.selectSize.bind(this));
    }
    selectSize(p: Phaser.Input.Pointer,sizeThumb:SizeThumb) {
        this.jm.size="size"+sizeThumb.index.toString();

        switch (sizeThumb.index) {
            case 0:
                this.jm.rows = 3;
                this.jm.cols = 3;
                break;

            case 1:
                this.jm.rows = 4;
                this.jm.cols = 4;
                break;

            case 2:
                this.jm.rows = 5;
                this.jm.cols = 5;
                break;
        }
        this.scene.start("SceneMain");
    }
}