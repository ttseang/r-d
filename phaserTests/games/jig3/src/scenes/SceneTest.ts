import { BaseScene } from "./BaseScene";

export class SceneTest extends BaseScene {
    
  
    private rows: number = 5;
    private cols: number = 5;
   
    private ph: number = -1;
    private pw: number = -1;

    private back:Phaser.GameObjects.Image;

    constructor() {
        super("SceneTest");
        
    }
    preload() {
        this.load.image("main", "./assets/image5.jpg");
        this.load.atlas("jig", "./assets/jig.png", "./assets/jig.json");
        this.load.image("holder", "./assets/holder.jpg");

    }
    create() {
        super.create();
        this.makeGrid(11, 11);

    }
}