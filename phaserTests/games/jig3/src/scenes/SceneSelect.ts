import { JigModel } from "../classes/Jigmodel";
import { JigThumb } from "../classes/JigThumb";
import { BaseScene } from "./BaseScene";

export class SceneSelect extends BaseScene {
    private jm: JigModel = JigModel.getInstance();

    constructor() {
        super("SceneSelect");
    }
    preload() {
        
        this.load.atlas("thumbs", "./assets/jigthumbs.png", "./assets/jigthumbs.json");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        let xx: number = 3;
        let yy: number = 2;

        for (let i: number = 0; i < 16; i++) {
            let fkey: string = "tt" + (i + 1).toString() + ".jpg";
            let thumb: JigThumb = new JigThumb(this, i, fkey);
            thumb.setInteractive();
            this.grid.placeAt(xx, yy, thumb);
            xx += 1.5;
            if (xx > 8) {
                xx = 3;
                yy += 2;
            }
        }
        this.input.on("gameobjectdown", this.selectCard.bind(this))
        window['s2']=this;
    }
    selectCard(p: Phaser.Input.Pointer, thumb: JigThumb) {
        this.jm.image = thumb.frameName;
        this.scene.start("SceneSize");
    }
}