import Phaser from "phaser";
import { GM } from "../classes/GM";
import { JigModel } from "../classes/Jigmodel";
import { JigP } from "../classes/JigP";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private current: JigP;
    private pGrid: JigP[][];
    private rows: number;
    private cols: number;
    private pscale:number=0.5;
    //private offset:number=100;

    private lastP: JigP;
    private firstP: JigP;
    private ph: number = -1;
    private pw: number = -1;

    private inPlaceCount:number=0;

    private back: Phaser.GameObjects.Image;
    private jm:JigModel=JigModel.getInstance();
    private gm: GM = GM.getInstance();

    constructor() {
        super("SceneMain");
        this.pGrid=[[]];
       
      window['scene']=this;
    }
    preload() {
        this.textures.removeKey("main")
        this.load.image("main", "./assets/puzzleImages/"+this.jm.size+"/"+this.jm.image);
        this.load.atlas("jig", "./assets/jig.png", "./assets/jig.json");
        this.load.image("holder", "./assets/holder.jpg");
        
        this.load.audio("win","./assets/audio/glassbell.wav");
        this.load.audio("snap","./assets/audio/snap.wav");
        this.load.audio("dropinplace","./assets/audio/dropinplace.mp3");
        this.load.audio("downclick","./assets/audio/downclick.wav");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.jm.connected=[];
        this.inPlaceCount=0;

        for (let i: number = 0; i < 10; i++) {
            this.pGrid[i] = [];
        }

        //this.grid.showNumbers();

        this.rows=this.jm.rows;
        this.cols=this.jm.cols;

        this.back = this.add.image(0, 0, "holder").setOrigin(0, 0);

        let index: number = 0;
        for (let i: number = 0; i < this.cols; i++) {
            for (let j: number = 0; j < this.rows; j++) {
                let p: JigP = new JigP(this, "main", i, j, this.pscale);
                if (!this.firstP) {
                    this.firstP = p;
                }
                p.x = 96 * i + 50;
                p.y = 97 * j + 50;
                p.index = index;
                index++;

                p.leftEdge = this.cols - 1;
                p.bottomEdge = this.rows - 1;
                // p.setPimps(2,2,2,2);
                p.callback = this.onClick.bind(this);
                this.pGrid[i].push(p);
                p.setRand();
                if (this.lastP) {
                    this.lastP.next = p;
                    p.prev = this.lastP;
                }
                this.lastP = p;

                if (this.ph === -1) {
                    this.ph = p.displayHeight;
                }
                console.log(p.displayWidth);
                if (p.displayWidth>this.pw) {
                    this.pw = p.displayWidth;
                    //console.log(this.pw);
                }

            }
        }

        this.mapGrid();

        this.firstP.fixP();
       // console.log(this.pGrid);
        this.input.on('pointerup', this.onUp.bind(this));

        //  console.log(this.ph, this.pw);



        this.alignPuzzle();
        //this.firstP.checkConn();
        this.mixUp();
    }
    alignPuzzle() {
        let gridWidth: number = this.pw * this.cols;
        let gridHeight: number = this.ph * this.rows;
        let startX: number = this.pw / 2 + this.gw / 2 - (gridWidth / 2);
        //  let startX:number=this.gw/2-(gridWidth/2);
        let startY: number = this.gh / 2 - (gridHeight / 2);

        let bw:number =Math.floor(this.pw*this.cols)-this.cols;
        let bh:number = Math.floor(this.ph*this.rows)-this.rows;

        console.log(bw,bh);

        this.back.displayWidth = bw;
        this.back.displayHeight =bh;
        this.back.x = startX - this.ph / 2;
        this.back.y = startY - this.pw / 2;

        for (let i: number = 0; i < this.cols; i++) {
            for (let j: number = 0; j < this.rows; j++) {
                let p: JigP = this.pGrid[i][j];
                p.x = startX + Math.floor(this.pw - 1) * i;
                p.y = startY + Math.floor(this.ph - 1) * j;
                p.ox = p.x;
                p.oy = p.y;
                p.depth = ((j * this.rows) + (i * this.cols)) + 100;
            }
        }

    }
    mixUp() {
        for (let i: number = 0; i < this.cols; i++) {
            for (let j: number = 0; j < this.rows; j++) {
                let p: JigP = this.pGrid[i][j];
                p.x = Phaser.Math.Between(this.gw * 0.2, this.gw * 0.8);
                p.y = Phaser.Math.Between(this.gh * 0.2, this.gh * 0.8);
            }
        }
    }
    destroyPuzzle() {
        for (let i: number = 0; i < this.cols; i++) {
            for (let j: number = 0; j < this.rows; j++) {
                let p: JigP = this.pGrid[i][j];
                this.pGrid[i][j] =null;
                p.destroy();
            }
        }
    }
    mapGrid() {
        for (let i: number = 0; i < this.cols; i++) {
            for (let j: number = 0; j < this.rows; j++) {
                let p: JigP = this.pGrid[i][j];
                if (j > 0) {
                    p.northSib = this.pGrid[i][j - 1];
                }
                if (j < this.rows - 1) {
                    p.southSib = this.pGrid[i][j + 1];
                }
                if (i > 0) {
                    p.westSib = this.pGrid[i - 1][j];
                }
                if (i < this.cols - 1) {
                    p.eastSib = this.pGrid[i + 1][j];
                }
            }

        }
    }
    onUp() {
        if (this.current) {
            this.current.orginalSnap();
            this.firstP.clearChecked();
            this.current.updateSiblings();
            // this.firstP.checkConn();
            this.current.checkSibs();
            let inPlace:boolean=false;
            
            if (this.current.inPlace()==true)
            {
                this.current.x=this.current.ox;
                this.current.y=this.current.oy;
                this.current.sendToBack();
                this.children.sendToBack(this.back);
                this.current.deactivate();
                console.log("inplace"); 
                this.inPlaceCount++;
                //#sound in place
                this.playSound("dropinplace");
                inPlace=true;
            }


            this.current = null;
            
            this.firstP.findConnections();  
           

            if (this.jm.madeConnection==true && inPlace==false)
            {
                this.playSound("snap");
                this.jm.madeConnection=false;
            }

            //console.log("con="+this.jm.connected.length);
            if (this.inPlaceCount==(this.rows*this.cols))
            {
               // console.log("WIN");
                this.doWinEffect();
            }
            
        }
        this.input.off("pointermove");
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    doWinEffect()
    {
        let winImage:Phaser.GameObjects.Image=this.add.image(this.back.x,this.back.y,"main").setOrigin(0,0);
        winImage.displayWidth=this.back.displayWidth;
        winImage.displayHeight=this.back.displayHeight;
       
        this.playSound("win");
        setTimeout(() => {
            this.destroyPuzzle();
            this.firstP=null;
            this.lastP=null;
            this.scene.start("SceneSelect");
        }, 4000);
    }
    onClick(jigP: JigP) {
        jigP.bringToTop();
        this.firstP.clearChecked();
       // jigP.findConnections();
        //#sound first click
        this.playSound("downclick");
        this.current = jigP;
        this.current.x = this.input.activePointer.x;
        this.current.y = this.input.activePointer.y;
        this.current.updateSiblings();
        this.input.on("pointermove", this.updateCurrent.bind(this));
    }
    updateCurrent() {

        this.firstP.clearChecked();
        this.current.x = this.input.activePointer.x;
        this.current.y = this.input.activePointer.y;

         this.current.updateSiblings();
        //this.current.updateSiblings(this.current, false);
    }
}