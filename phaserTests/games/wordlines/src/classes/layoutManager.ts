import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { GM } from "./GM";

export class LayoutManager {
    public scene: Phaser.Scene;
    public guideImage: GameObjects.Sprite;
    public maskImage: GameObjects.Sprite;
    private gm: GM = GM.getInstance();
    private bscene: IBaseScene;
    public callback: Function = () => { };

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;

        window.addEventListener("orientationchange", function () {
            console.log(window.innerWidth, window.innerHeight)
            this.flipped();
        }.bind(this));
    }
    private flipped() {
        console.log('flipped');
        setTimeout(() => {

            let w: number = window.innerWidth;
            let h: number = window.innerHeight;

            if (window['visualViewport']) {
                w = window.visualViewport.width;
                h = window.visualViewport.height;
            }
            if (w < h) {
                this.gm.isPort = true;
            }
            else {
                this.gm.isPort = false;
            }
            this.bscene.resetSize(w, h);
            this.scene.scale.resize(w, h);
            this.bscene.getGrid().hide();
            this.bscene.makeGrid(19, 11);
            this.callback();
        }, 1000);

    }
    public doLayout() {
        //this.placeAndScale("number",this.guideImage);
        //this.placeAndScale("number",this.maskImage);
    }
    private placeAndScale(item: string, obj: GameObjects.Sprite) {
        Align.scaleToGameW(obj, this.getScale(item), this.bscene);
        this.bscene.getGrid().placeAtIndex(this.getPos(item), obj);
    }
    public getPos(item: string) {
        let pos: number = 0;

        switch (item) {
            case "number":
                pos = 100;
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    pos = 58;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    pos = 52;
                    //pos = -1;
                }
                if (this.gm.isTablet && this.gm.isPort === false) {
                    pos = 146;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    pos = 57;
                }
                if (this.gm.isTablet === true && this.gm.isPort === true) {
                    pos = 52;
                }
                break;

            case "dog":

                pos = 225;
                if (this.gm.isMobile === true && this.gm.isPort) {
                    pos = 363;
                }

                break;
        }
        return pos;
    }
    public getScale(item: string) {
        let scale: number = 0;

        switch (item) {
            case "number":

                scale = 0.5;
                if (this.gm.isMobile === false) {
                    scale = 0.20;
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale = 0.15;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.35;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    scale = 0.35;
                }

                break;

            case "arrow":

                scale = 0.5;
                if (this.gm.isMobile === false) {
                    scale = 0.10;
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale = 0.05;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.1;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    scale = 0.1;
                }

                break;

            case "dog":

                scale = 0.25;
                if (this.gm.isMobile === false) {

                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {

                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    scale = 0.45;
                }
                if (this.gm.isTablet === true && this.gm.isPort === true) {
                    scale = 0.35;
                }
                if (this.gm.isTablet === true && this.gm.isPort === false) {
                    scale = 0.4;
                }
        }

        return scale;
    }
}