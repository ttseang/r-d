import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";

export class WordBox extends Phaser.GameObjects.Container {
    private bscene: IBaseScene;
    public scene: Phaser.Scene;
    private text1: Phaser.GameObjects.Text;
    private back: Phaser.GameObjects.Image;
    public word: string = "";
    public col: number = 0;
    public index: number = 0;

    public row: number = 0;

    constructor(bscene: IBaseScene, word: string) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.word=word;
        //
        //
        //
        this.back = this.scene.add.image(0, 0, "holder");
        this.back.displayHeight = this.bscene.ch;
        this.back.displayWidth = this.bscene.cw * 2;
        this.add(this.back);
        //
        //
        //
        let fs: number = this.bscene.getW() / 30;

        this.text1 = this.scene.add.text(0, 0, word, { fontSize: fs.toString() + "px", color: "#ffffff" }).setOrigin(0.5, 0.5);
        this.add(this.text1);

        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.setInteractive();

        this.scene.add.existing(this);
        this.fixText();
    }
    setBackColor(c:number)
    {
        this.back.setTint(c);
    }
    fixText() {
        if (this.text1.displayWidth > this.back.displayWidth) {
            this.back.displayWidth=this.text1.displayWidth*1.05;
        }
    }
    resizeToGrid() {
        let fs: number = this.bscene.getW() / 30;
        this.text1.setFontSize(fs);

        this.back.displayHeight = this.bscene.ch;
        this.back.displayWidth = this.bscene.cw * 2;
        this.bscene.getGrid().placeAt(this.col, this.row, this);
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.fixText();
    }
}