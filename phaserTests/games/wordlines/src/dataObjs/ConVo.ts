import { WordBox } from "../classes/WordBox";

export class ConVo
{
    public box1:WordBox;
    public box2:WordBox;

    constructor(box1:WordBox,box2:WordBox)
    {
        this.box1=box1;
        this.box2=box2;
    }
}