import IBaseScene from "../interfaces/IBaseScene";

export class AnimationUtil {
    AnimationUtil() {}
    makeButterfly(scene:Phaser.Scene,key:string)
    {
        //butterfly blue animation 
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 11,
                zeroPad: 0,
                prefix: 'butterfly blue animation ',
                suffix: ' 1200.png'
            }),
            frameRate: 12,
            repeat: -1
        });

        scene.anims.create({
            key: 'fly',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 11,
                zeroPad: 0,
                prefix: 'butterfly blue animation ',
                suffix: ' 1200.png'
            }),
            frameRate:24,
            repeat: -1
        });
    }
    makeAnims3(scene, key) {
        console.log("anim key=" + key);
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 10,
                zeroPad: 0,
                prefix: 'Attack (',
                suffix: ').png'
            }),
            frameRate: 16,
            repeat: 0
        });
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 10,
                zeroPad: 0,
                prefix: 'Jump (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: 0
        });
        
        scene.anims.create({
            key: 'jumpAttack',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 10,
                zeroPad: 0,
                prefix: 'JumpAttack (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
       
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 10,
                zeroPad: 0,
                prefix: 'Idle (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 10,
                zeroPad: 0,
                prefix: 'Dead (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 1,
                end: 10,
                zeroPad: 0,
                prefix: 'Run (',
                suffix: ').png'
            }),
            frameRate: 16,
            repeat: -1
        });
    }

    makeAnims2(scene, key) {
        console.log("anim key=" + key);
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Attack__',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'slide',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Slide__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpAttack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Attack__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpThrow',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Throw__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Idle__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Dead__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Run__',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: -1
        });
    }
    makeAnims(scene, key) {
        console.log("anim key=" + key);
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Jump_',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Run_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'joy',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Joy_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'flight',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Flyght_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
       
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Char_Fox_Idle_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        
    } 
}