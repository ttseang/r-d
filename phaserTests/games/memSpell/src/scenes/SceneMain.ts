import { GameObjects } from "phaser";
import { LetterBox } from "../classes/LetterBox";
import { LetterTile } from "../classes/LetterTile";
import Align from "../util/align";
import { RandWordUtil } from "../util/randWordUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private letterBoxes:LetterBox[]=[];
    private letterTiles:LetterTile[]=[];

    private randWordUtil:RandWordUtil;
    private letters:string[]=[];
    private currentWord:string="";
    private clickLock:boolean=false;

    private rightText:GameObjects.Text;
    private rightCount:number=0;

    private usedWords:string[]=[];

    private timeText:GameObjects.Text;
    private timer1:any=0;
    private secs:number=0;

    constructor() {
        super("SceneMain");
    }
    preload() {
        
      
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        let back:GameObjects.Image=this.add.image(0,0,"holder");
        back.displayWidth=this.gw*0.5;
        back.displayHeight=this.gh*0.7;
        back.setTint(0xc23616);
        Align.center(back,this);

        let circle:GameObjects.Image=this.add.image(0,0,"circle");
        circle.setTint(0x8c7ae6);
        Align.scaleToGameW(circle,0.1,this);
        this.grid.placeAt(8,1,circle);

       // this.grid.showPos();
        
        this.randWordUtil=new RandWordUtil(this);
        this.letters=this.randWordUtil.pickRandomLetters();

        this.makeLetterBoxes();
        this.makeLetterGrid();

        this.rightCount=0;

        this.rightText=this.add.text(0,0,"Words Right:0",{color:"white",fontSize:"24px",fontFamily:"Arial"}).setOrigin(0.5,0.5);
        this.grid.placeAt(5,2,this.rightText);

        this.timeText=this.add.text(0,0,"Time:"+this.secs.toString(),{fontFamily:"Arial",fontSize:"20px",color:"white"}).setOrigin(0.5,0.5);
        this.grid.placeAt(8,1,this.timeText);

        this.input.on("gameobjectdown",this.turnOverLetter.bind(this));

        this.secs=180;
        this.timer1=setInterval(this.tick.bind(this),1000);
    }
    tick()
    {
        console.log("tick");
        this.secs--;
        this.timeText.setText("Time:"+this.secs.toString());
        if (this.secs==0)
        {
            clearInterval(this.timer1);
            this.scene.start("SceneOver");
        }
    }
    turnOverLetter(p:Phaser.Input.Pointer,letterTile:LetterTile)
    {
        if (letterTile.cover.alpha<1)
        {
            return;
        }
        if (this.clickLock==true)
        {
            return;
        }
        this.playSound("pop");
        //
        //
        //
        letterTile.flashCover();
        this.currentWord+=letterTile.letter;
        this.updateTiles();

        if (this.currentWord.length==4)
        {
            this.clickLock=true;
            console.log(this.currentWord);
            if (this.randWordUtil.checkWord(this.currentWord)==true)
            {
                if (this.usedAlready(this.currentWord)==true)
                {
                    console.log("already");
                    this.showMessase("You have already used that word");
                    this.playSound("wrongSound");
                    return;
                }
                this.usedWords.push(this.currentWord);
                console.log("correct");
                this.playSound("rightSound");
                this.rightCount++;
                this.rightText.setText("Right Words:"+this.rightCount.toString());
                this.resetAll();
            }
            else
            {
                this.playSound("wrongSound");
                this.resetAll();
            }
          
        }
    }
    showMessase(msg:string)
    {
        this.rightText.setText(msg);
        setTimeout(() => {
            this.rightText.setText("Right Words:"+this.rightCount.toString());
        }, 2000);
    }
    usedAlready(word:string)
    {
        for (let i:number=0;i<this.usedWords.length;i++)
        {
            if (this.usedWords[i]==word)
            {
                return true;
            }
        }
        return false;
    }
    resetAll()
    {
        setTimeout(() => {
            this.currentWord="";
            this.updateTiles();
            this.hideAll();
            this.clickLock=false;
        }, 1000);
    }
    makeLetterGrid()
    {
        for (let i:number=0;i<4;i++)
        {
            for (let j:number=0;j<5;j++)
            {
                let letterTile:LetterTile=new LetterTile(this);
                letterTile.setLetter(this.letters.shift());
                this.letterTiles.push(letterTile);
                this.grid.placeAt(3+j,3+i*1.5,letterTile);
            }
        }
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    updateTiles()
    {
        let letters:string[]=this.currentWord.split("");
        while(letters.length<4)
        {
            letters.unshift("?");
        }
        for (let i:number=0;i<letters.length;i++)
        {
            this.letterBoxes[i].setLetter(letters[i]);
        }

    }
    hideAll()
    {
        for (let i:number=0;i<this.letterTiles.length;i++)
        {
            this.letterTiles[i].hideCover();
        }
    }
    makeLetterBoxes()
    {
        for(let i:number=0;i<4;i++)
        {
            let letterBox:LetterBox=new LetterBox(this);
            this.letterBoxes.push(letterBox);
            Align.scaleToGameW(letterBox,0.05,this);
            this.grid.placeAt(3.5+i,1,letterBox);
        }
    }
}