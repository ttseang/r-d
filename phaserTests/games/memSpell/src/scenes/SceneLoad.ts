import { GM } from "../classes/GM";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {
        this.load.image("button","./assets/ui/buttons/1/1.png");
        this.load.text("words", "./assets/data/fourletterwords.txt");
        this.load.image("pine","./assets/pine.png");
        this.load.image("holder","./assets/holder.jpg");
        this.load.image("circle","./assets/circle.png");
        
        this.load.audio("rightSound","./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");
        this.load.audio("pop","./assets/audio/pop.wav");

        this.load.audio("backgroundMusic","./assets/audio/bm.mp3")
        
    }
    create()
    {
        this.gm.bgMusic=this.sound.add("backgroundMusic",{loop:true,volume:0.5});    
        this.gm.bgMusic.play();
        
        this.scene.start("SceneMain");
    }
}