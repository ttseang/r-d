import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class LetterTile extends GameObjects.Container
{
    private back:GameObjects.Image;
    private text1:GameObjects.Text;

    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    public letter:string="";
    public cover:GameObjects.Image;
    
    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());

        this.bscene=bscene;
        this.scene=bscene.getScene();

        this.back=this.scene.add.image(0,0,"holder");
        this.back.setTint(0xfbc531);
        this.add(this.back);
        Align.scaleToGameW(this.back,0.05,this.bscene);



        this.text1=this.scene.add.text(0,0,"A",{color:"black",fontSize:"30px",fontFamily:"Arial"}).setOrigin(0.5,0.5);
        this.add(this.text1);

     //   this.text1.alpha=0;

        this.cover=this.scene.add.image(0,0,"holder");
        this.cover.displayHeight=this.back.displayHeight;
        this.cover.displayWidth=this.back.displayWidth;
        this.add(this.cover);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);
        this.setInteractive();

        this.cover.setTint(0x0097e6);

        this.cover.alpha=0;

        setTimeout(() => {
            this.hideCover();
        }, 1000);
    }

    setLetter(letter:string)
    {
        this.text1.setText(letter);
        this.letter=letter;
    }
    flashCover()
    {
        let tl:Phaser.Tweens.Timeline=this.scene.tweens.createTimeline();

        tl.add({targets:this.cover,duration:500,alpha:0});
       // tl.add({targets:this.cover,duration:1000,delay:1000,alpha:1});
        tl.play();
    }
    hideCover()
    {
        let tl:Phaser.Tweens.Timeline=this.scene.tweens.createTimeline();

        tl.add({targets:this.cover,duration:1000,alpha:1});
       // tl.add({targets:this.cover,duration:1000,delay:1000,alpha:1});
        tl.play();
    }
}