import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "../scenes/BaseScene";

export class LetterBox extends Phaser.GameObjects.Container
{
    private back:GameObjects.Image;
    private text1:GameObjects.Text;
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    public letter:string="";

    constructor(bscene:BaseScene)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.scene=bscene.getScene();
        

        this.back=this.scene.add.image(0,0,"pine");
        this.add(this.back);

        this.text1=this.scene.add.text(0,0,"?",{fontSize:"30px",fontFamily:"Arial",color:"black"}).setOrigin(0.5,0.5);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);
    }
    setLetter(letter:string)
    {
        this.letter=letter;
        this.text1.setText(letter);
    }
}