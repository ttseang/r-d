export class RandWordUtil
{
    private pickwords: string[] = [];

    constructor(scene:Phaser.Scene)
    {
        this.pickwords = scene.cache.text.get("words").split(" ");
        
    }
    checkWord(word:string)
    {
        return this.pickwords.includes(word);
    }
    pickRandomLetters()
    {
        let randLetters:string[]=[];
        for (let i:number=0;i<10;i++)
        {
            let r:number=Math.floor(Math.random()*this.pickwords.length);
            let word:string=this.pickwords[r];
            console.log(word);

            let letters:string[]=word.split("");
            //console.log(letters);

           randLetters=randLetters.concat(letters);

           /* for (let j:number=0;j<2;j++)
           {
             let letters2:string[]="abcdefghijklmnopqrstuvwxyz".split("");
             let letterIndex:number=Math.floor(Math.random()*26); 
             randLetters.push(letters2[letterIndex]);
           } */

        }
        //return randLetters;
        return this.shuffleArray(randLetters);
    }
    shuffleArray(s:string[])
    {
        for (let i:number=0;i<50;i++)
        {
            let r:number=Math.floor(Math.random()*s.length);
            let r2:number=Math.floor(Math.random()*s.length);

            let temp:string=s[r];
            s[r]=s[r2];
            s[r2]=temp;
        }
        return s;
    }
}