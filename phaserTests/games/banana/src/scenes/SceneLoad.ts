import { BaseScene } from "./BaseScene"

export class SceneLoad extends BaseScene{
    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {
        this.load.atlas("monkey", "./assets/images/monkey_anim.png", "./assets/images/monkey_anim.json");
        this.load.image("vine", "./assets/images/vine.png");
        this.load.image("sides", "./assets/images/sides.png");
        this.load.image("sky", "./assets/images/sky.png");
        this.load.image("decor", "./assets/images/decor.png");
        this.load.image("clouds", "./assets/images/clouds.png");
        this.load.image("holder","./assets/images/holder.jpg");
     //   this.load.image("ground", "./assets/images/ground.png");

        this.load.image("sb","assets/images/soundButton2.png");

        this.loadListAudio(1);
        this.loadListAudio(2);

        this.loadListPicture(1);
        this.loadListPicture(2);

        for (let j:number=94;j<139;j++)
        {
            this.loadListPicture(j);
            this.loadListAudio(j);
        }
      

      
        this.load.audio("error","assets/audio/error.wav");
        this.load.audio("win","assets/audio/win.wav");
    }
   
    loadListAudio(index:number)
    {
        let file="assets/audio/listAudio/"+index.toString()+".mp3";
       
        this.load.audio("listaudio"+index.toString(),file);
    }
    loadListPicture(index:number)
    {
        let file="assets/images/listImages/"+index.toString()+".jpg";
        this.load.image("listimage"+index.toString(),file);
    }
    create()
    {
        this.scene.start("SceneMain");
    }
}