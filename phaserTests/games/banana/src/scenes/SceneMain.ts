import { WordVo } from "../dataObjs/WordVo";
import { InstructPanel } from "../ui/InstructPanel";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private monkey: Phaser.GameObjects.Sprite;
    private sides: Phaser.GameObjects.TileSprite;
    private sky: Phaser.GameObjects.Sprite;
    private decor: Phaser.GameObjects.TileSprite;
    private clouds: Phaser.GameObjects.TileSprite;
    //private ground:Phaser.GameObjects.Sprite;

    private climbFlag: boolean = false;
    private speed: number = 16;
    private instructions: InstructPanel;
    private wordIndex: number = -1;
    private wordData: any;
    private correct: number = 0;


    constructor() {
        super("SceneMain");
    }
    preload() {
        //this.load.image("face", "./assets/images/face.png");
        this.load.json("wordData", "./assets/data/wordData.json");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.showNumbers();
        this.setBackground();

        this.monkey = this.add.sprite(0, 0, "monkey");
        this.makeAnimations();


        this.instructions = new InstructPanel(this, this.grid);
        this.instructions.callback = this.answer.bind(this);
        //this.instructions.x=this.gw/2;
        //  this.instructions.y = this.instructions.displayHeight / 2;
        this.wordData = this.cache.json.get("wordData");

        //  this.input.on("pointerdown", this.climb.bind(this));

        this.resetMonkey();
    }
    setQuestion() {
        this.wordIndex++;

        if (this.wordIndex === this.wordData.levels.length) {
            return;
        }


        let current: any = this.wordData.levels[this.wordIndex];
        this.correct = parseInt(current.correct);

        console.log(current);
        let words: WordVo[] = [];
        for (let i: number = 0; i < current.words.length; i++) {
            let word: WordVo = new WordVo(current.words[i].id, current.words[i].word);
            words.push(word);
        }
        console.log(words);
        this.instructions.makeBoxes(words, current.sound);

        if (this.instructions) {
            //this.instructions.visible=true;
            let tw: Phaser.Tweens.Tween = this.tweens.add({ targets: this.instructions, duration: 500, y: 0 });
        }
    }
    answer(index: number) {
       
        if (index === this.correct) {
            this.climb();
        }
        else {
           
            let sound: Phaser.Sound.BaseSound = this.sound.add("error");
            sound.play();

            this.monkey.play("jump");
            setTimeout(() => {
                this.monkey.play("idle");
                this.monkey.setAngle(-45);
            }, 250);

            let tw: Phaser.Tweens.Tween = this.tweens.add({ targets: this.monkey, duration: 200, angle: 360 });
        }
    }
    setBackground() {
        this.sky = this.add.sprite(0, 0, "sky").setOrigin(0, 0);
        this.sky.displayWidth = this.gw;
        this.sky.displayHeight = this.gh;

        this.clouds = this.add.tileSprite(0, 0, this.gw, this.gh, "clouds").setOrigin(0, 0);
        this.sides = this.add.tileSprite(0, 0, this.gw, this.gh, "sides").setOrigin(0, 0);
        
        this.decor = this.add.tileSprite(0, 0, this.gw, this.gh, "decor").setOrigin(0, 0);

        /*   this.ground=this.add.sprite(0,0,"ground");
          Align.scaleToGameW(this.ground,1,this);
          this.ground.y=this.gh-this.ground.displayHeight/2;
          this.ground.x=this.gw/2; */
    }
    climb() {
        this.monkey.flipX = true;
        this.monkey.setAngle(45);
        this.grid.placeAtIndex(79, this.monkey);
        this.climbFlag = true;
        
        this.monkey.play("run");
        this.time.addEvent({ delay: 2000, callback: this.resetMonkey.bind(this), callbackScope: this, loop: false });

        let tw: Phaser.Tweens.Tween = this.tweens.add({ targets: this.instructions, duration: 500, y: -500 });
    }
    resetMonkey() {
        this.grid.placeAtIndex(77, this.monkey);
        this.monkey.setAngle(-45);
        this.monkey.flipX = false;
        this.monkey.play("idle");
        this.climbFlag = false;
        this.setQuestion();

    }
    makeAnimations() {

        let frameNames = this.anims.generateFrameNames('monkey', { start: 0, end: 17, zeroPad: 3, prefix: 'Idle_', suffix: '.png' })
        console.log(frameNames);

        this.anims.create({
            key: 'idle',
            frames: frameNames,
            frameRate: 8,
            repeat: -1
        });

        let frameNames2 = this.anims.generateFrameNames('monkey', { start: 0, end: 4, zeroPad: 3, prefix: 'Jumping_', suffix: '.png' })


        this.anims.create({
            key: 'jump',
            frames: frameNames2,
            frameRate: 8,
            repeat: -1
        });


        let frameNames3 = this.anims.generateFrameNames('monkey', { start: 0, end: 13, zeroPad: 3, prefix: 'Running_', suffix: '.png' })


        this.anims.create({
            key: 'run',
            frames: frameNames3,
            frameRate: 32,
            repeat: -1
        });
    }
    update() {
        if (this.climbFlag === true) {
            this.clouds.tilePositionY -= this.speed / 2;
            //  this.sky.tilePositionY-=this.speed;
            this.decor.tilePositionY -= this.speed;
            this.sides.tilePositionY -= this.speed;
        }
    }
}