export class WordVo {
    public id: number;
    public word: string;
    constructor(id: number, word: string) {
        this.id = id;
        this.word = word;
    }
}