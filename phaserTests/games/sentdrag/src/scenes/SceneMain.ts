import { GameObjects } from "phaser";
import { WordBubble } from "../classes/WordBubble";
import { SentVo } from "../dataObjs/SentVo";
import { ColorBurst } from "../effects/colorBurst";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private sents:SentVo[]=[];
    private sentIndex:number=-1;
    private text1:GameObjects.Text;
    private text2:GameObjects.Text;
    private bubbles:WordBubble[]=[];

    private blank:WordBubble;
    private current:WordBubble;
    private correct:string="";

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.audio("right","./assets/audio/sfx/newlevel.wav");
        this.load.image("bubble", "./assets/bubble.png");
        this.load.image("paper","./assets/paper.png");
        ColorBurst.preload(this);
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        let bg:GameObjects.Image=this.add.image(0,0,"paper");
       // Align.scaleToGameW(bg,0.8,this);

        bg.displayWidth=this.gw*0.8;
        bg.displayHeight=this.gh*0.7;
        Align.center(bg,this);

        this.text1=this.add.text(0,0,"FIRST",{fontFamily:"Arial",fontSize:"26px",color:"black"});
        this.text2=this.add.text(0,0,"Second",{fontFamily:"Arial",fontSize:"26px",color:"black"});

       fetch("./assets/sents.json")
       .then(response => response.json())
       .then(data => this.process({ data }));

      

      
    }
    process(data:any)
    {
        console.log(data);
        let sents:any=data.data.sents;
        for (let i:number=0;i<sents.length;i++)
        {
            let sent:any=sents[i];
           
            let sentVo:SentVo=new SentVo(sent.template,sent.words,sent.correct);

            console.log(sentVo);

            this.sents.push(sentVo);
        }
        this.makeSent();
    }
    showStars() {
        let cb1 = new ColorBurst(this, this.blank.x + this.blank.displayWidth / 2, this.blank.y + this.blank.displayHeight / 2, 50, 150, 1000, 0xffffff);
    }
    selectBubble(p:Phaser.Input.Pointer,bubble:WordBubble)
    {
        this.current=bubble;
        this.children.bringToTop(bubble);
        this.input.once("pointerup",this.onUp.bind(this));
        this.input.on("pointermove",this.onMove.bind(this));
    }
    onMove(p:Phaser.Input.Pointer)
    {
        this.current.x=p.x;
        this.current.y=p.y;
    }
    onUp()
    {
        this.input.off("pointermove");

        let diffx:number=Math.abs(this.current.x-this.blank.x);
        let diffy:number=Math.abs(this.current.y-this.blank.y);

        if (diffx<this.current.displayWidth/2 && diffy<this.current.displayHeight/2)
        {
            this.current.x=this.blank.x;
            this.current.y=this.blank.y;
            
            console.log(this.correct);

            if (this.correct==this.current.word)
            {
                console.log("correct");
                this.showStars();
                this.playSound("right");
                setTimeout(() => {
                    this.makeSent();
                }, 2000);
                return;
            }
            else
            {
                console.log("wrong");
                this.current.snapBack();
            }
        }
        this.input.once("gameobjectdown",this.selectBubble.bind(this));
       // this.current=null;
    }
    playSound(sound: string) {
        let audio: Phaser.Sound.BaseSound = this.sound.add(sound);
        audio.play();
    }
    makeSent()
    {

        for (let i:number=0;i<this.bubbles.length;i++)
        {
            this.bubbles[i].destroy();
        }
        this.bubbles=[];

        if (this.blank)
        {
            this.blank.destroy();
        }
        this.sentIndex++;
        if (this.sentIndex>this.sents.length)
        {
            return;
        }
        let sent:SentVo=this.sents[this.sentIndex];
        let words:string[]=sent.words;
        this.correct=sent.correct;

        words.push(sent.correct);

        let bw:number=0;

        for (let i:number=0;i<words.length;i++)
        {
            let bubble:WordBubble=new WordBubble(this,words[i]);
            this.bubbles.push(bubble);
            this.grid.placeAt(i,7,bubble);
            bw+=bubble.displayWidth;
        }       

        for (let j:number=0;j<20;j++)
        {
            let p1:number=Math.floor(Math.random()*this.bubbles.length);
            let p2:number=Math.floor(Math.random()*this.bubbles.length);

            let temp:WordBubble=this.bubbles[p1];
            this.bubbles[p1]=this.bubbles[p2];
            this.bubbles[p2]=temp;
        }
        
        let parts:string[]=sent.template.split("_");

        let sent1:string=parts[0];
        let sent2:string=parts[1];

        

        this.text1.setText(sent1);
        this.text2.setText(sent2);

       

        this.grid.placeAt(3,3,this.text1);

        let blank:WordBubble=new WordBubble(this,"          ");
        blank.removeInteractive();
        this.blank=blank;

        let fullWidth:number=this.text1.displayWidth+blank.displayWidth+this.text2.displayWidth;

        let startX:number=this.gw/2-fullWidth/2;

        this.text1.x=startX;

        blank.y=this.text1.y+this.text1.displayHeight/2;
        blank.x=this.text1.x+this.text1.displayWidth+blank.displayWidth/2;
        
        this.text2.y=this.text1.y;
        this.text2.x=blank.x+blank.displayWidth/2+this.gw*0.01;
        
        
       // let bubbleStartX:number=this.gw/2-(this.bubbles.length*this.gw*0.05)

        
        for(let i:number=0;i<this.bubbles.length;i++)
        {
            this.grid.placeAt(3+i*1.25,5,this.bubbles[i]);
            
            this.bubbles[i].initPos();
          
        }
        this.input.once("gameobjectdown",this.selectBubble.bind(this));
    }
}