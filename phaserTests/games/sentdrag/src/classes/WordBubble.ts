import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class WordBubble extends GameObjects.Container
{
    private text1:GameObjects.Text;
    public scene:Phaser.Scene;
    private back:GameObjects.Image;
    private ox:number=0;
    private oy:number=0;
    public word:string;

    constructor(bscene:IBaseScene,word:string)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();

        this.word=word;
        this.back=this.scene.add.image(0,0,"bubble");
      //  Align.scaleToGameW(this.back,0.1,bscene);
        this.add(this.back);

        this.text1=this.scene.add.text(0,0,word,{fontFamily:"Arial",fontSize:"26px",color:"black"});
        this.text1.setOrigin(0.5,0.5);
        this.add(this.text1);

        this.back.displayHeight=bscene.getH()*.15;
        this.back.displayWidth=this.text1.displayWidth*1.5;

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);

        this.setInteractive();
    }
    initPos()
    {
      this.ox=this.x;
      this.oy=this.y;
    }
    snapBack()
    {
      this.scene.add.tween({targets:this,duration:500,y:this.oy,x:this.ox})
    }
}