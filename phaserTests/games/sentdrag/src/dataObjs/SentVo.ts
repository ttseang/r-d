export class SentVo
{
    public template:string;
    public words:string[];
    public correct:string;

    constructor(template:string,words:string[],correct:string)
    {
        this.template=template;
        this.words=words;
        this.correct=correct;
    }
}