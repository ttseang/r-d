import { GameObjects } from "phaser";
import { config } from "webpack";
import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";

export class ColorBurst {
    public x: number;
    public y: number;
    public scene:Phaser.Scene;
    public size:number=.3;
    public dist:number;
    public duration:number;
    public maxDist:number;
    public n:number;
    public color:number;

    constructor(bscene:IBaseScene,x:number,y:number,count:number = 25,dist: number=150,duration:number=1000,color:number = 0xffffff) {
        this.scene = bscene.getScene();
        
        //
        //
        //
        this.color = color;
        this.duration=duration;;
        this.maxDist=dist*6;
        this.dist=dist;
        this.x=x;
        this.y=y;       
        this.n = count;
        
        for (let i = 0; i < this.n; i++) {
            let star:GameObjects.Sprite = this.scene.add.sprite(this.x, this.y, "effectColorStars");
          //  Align.scaleToGameW(star,this.size,bscene);
        //    console.log(star);
            //
            //
            //
            let f:number = Phaser.Math.Between(0, 14);
            star.setFrame(f);
            star.setOrigin(0.5, 0.5);
            let r:number = Phaser.Math.Between(50, this.maxDist);
            let s:number = Phaser.Math.Between(1, 100) / 100;
            star.scaleX = s;
            star.scaleY = s;
            let angle:number = i * (360 / this.n);
            //console.log(angle);
            let tx:number =  this.x + r * Math.cos(angle);
            let ty:number = this.y + r * Math.sin(angle);
          //  console.log(tx,ty);
            //  star.x=tx;
            // star.y=ty;
            this.scene.tweens.add({
                targets: star,
                duration: this.duration,
                alpha: 0,
                angle:360,
                y: ty,
                x: tx,
                scaleX:2,
                scaleY:2,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                    scope: this
                }]
            });
        }
    }
    tweenDone(tween:Phaser.Tweens.Tween, targets, custom) {
        targets[0].destroy();
    }
    static preload(scene) {
        scene.load.spritesheet('effectColorStars', './assets/effects/colorStars.png', {frameWidth: 26,frameHeight: 26});
    }
}