import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";

export class Puddle extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    public index:number=0;
    public num:number=0;
    private bscene:IBaseScene;
    private text1:GameObjects.Text;
    private back:GameObjects.Sprite;
    private textOffset:number=0;
    public isLast:boolean=false;
    constructor(bscene:IBaseScene,index:number,num:number,textOffset:number)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.scene=bscene.getScene();

        this.index=index;
        this.num=num;
        this.textOffset=textOffset;

        let key:string="puddle"+index.toString()+".png";

        this.back=this.scene.add.sprite(0,0,"puddles");
        this.back.setFrame(key);
        Align.scaleToGameW(this.back,0.15,bscene);

        this.add(this.back);

        this.text1=this.scene.add.text(0,0,this.num.toString(),{fontSize:"50px",color:"black"}).setOrigin(0.5,0.5);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
        this.scene.add.existing(this);
    }
    hasFrog()
    {
        if (this.textOffset==0)
        {
            
        }
       switch(this.textOffset)
       {
           case 0:
            this.text1.y=-this.text1.displayHeight;
           break;
           case 1:
            this.text1.y=this.text1.displayHeight;
            break;
            case 2:
            this.text1.x=-this.text1.displayWidth;
            break;

       }

      
       this.text1.setFontStyle("normal");
        this.scene.tweens.add({targets:this.text1,duration:500,scaleX:1.2,scaleY:1.2});

         Align.scaleToGameW(this.back,0.2,this.bscene);      
    }
    showAsNext()
    {
        this.text1.setFontStyle("bold");
    }
    hasNoFrog()
    {
      /*   this.text1.y=0;
        this.text1.x=0; */

        this.text1.setFontStyle("normal");
        this.scene.tweens.add({targets:this.text1,duration:500,scaleX:1,scaleY:1,x:0,y:0});

        Align.scaleToGameW(this.back,0.15,this.bscene);
    }
    hideText()
    {
        this.text1.visible=false;
    }
}