import { GameObjects } from "phaser";
import { Puddle } from "../classes/puddle";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private frog: GameObjects.Sprite;
    private currentIndex: number = 0;
    private clickLock: boolean = false;
    private lastPuddle: Puddle | null = null;
    private puddles:Puddle[]=[];
    private startNum:number=0;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.atlas("frog", "assets/images/frog.png", "assets/images/frog.json");
        this.load.atlas("puddles", "assets/images/puddle.png", "assets/images/puddle.json");
      //  this.load.image("guide", "./assets/images/guide.png");
        this.load.image("final","./assets/images/final.png");

        this.load.audio("jump","./assets/audio/jump.mp3");
        this.load.audio("win","./assets/audio/win.wav");
        this.load.audio("error","./assets/audio/quiz_wrong.wav");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);

        /*  let guide:GameObjects.Image=this.add.image(0,0,"guide");
         guide.displayWidth=this.gw;
         guide.displayHeight=this.gh;
 
         Align.center(guide,this); */

        //this.grid.showPos();

        this.frog = this.add.sprite(0, 0, "frog");
        this.frog.setFrame("frog1.png");
        this.frog.setAngle(180);

        this.grid.placeAt(2, 1, this.frog);

        this.makePuddles();
        this.input.on("gameobjectdown", this.clickPuddle.bind(this));

        this.showNextPuddle();
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    showNextPuddle()
    {
        let puddleIndex:number=this.currentIndex-this.startNum;
        console.log("puddleIndex="+puddleIndex);
        
        if (puddleIndex<this.puddles.length)
        {
            let nextPuddle:Puddle=this.puddles[puddleIndex];
           
            nextPuddle.showAsNext();
        }
    }
    makePuddles() {
        let pos: PosVo[] = [];
        pos.push(new PosVo(2, 6));
        pos.push(new PosVo(2, 11));
        pos.push(new PosVo(4, 15));
        pos.push(new PosVo(6, 19));
        pos.push(new PosVo(9, 15.5));
        pos.push(new PosVo(10, 10.5));
        pos.push(new PosVo(11, 5.5));
        pos.push(new PosVo(14, 3));
        pos.push(new PosVo(17.5, 6.5));
        pos.push(new PosVo(18.5, 12));
        pos.push(new PosVo(18.5, 18));

        let offsets: number[] = [0, 0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0];

        this.startNum = 31;

        this.currentIndex = this.startNum;

        for (let i: number = 0; i < pos.length; i++) {
            let puddle: Puddle = new Puddle(this, i, this.startNum + i, offsets[i]);
            //  puddle.setFrame("puddle"+i.toString());
            this.grid.placeAt(pos[i].x, pos[i].y, puddle);

            this.puddles.push(puddle);

            if (i == pos.length - 1) {
                puddle.hideText();
                puddle.isLast=true;
                Align.scaleToGameW(puddle, .25, this);
            }
        }

        this.children.bringToTop(this.frog);
    }
    clickPuddle(p: Phaser.Input.Pointer, puddle: Puddle) {
        if (this.clickLock == true) {
            return;
        }
        if (puddle instanceof Phaser.GameObjects.Image) {
            return;
        }
        if (puddle.num != this.currentIndex) {
            this.playSound("error");
            return;
        }
        if (this.lastPuddle) {
            this.lastPuddle.hasNoFrog();
        }
        this.lastPuddle = puddle;

        // puddle.alpha=.5;
        this.currentIndex++;

        this.playSound("jump");
        let angle: number = this.getAngle(new PosVo(this.frog.x, this.frog.y), new PosVo(puddle.x, puddle.y));

        this.frog.setAngle(angle);
        this.clickLock = true;

        this.frog.setFrame("frog2.png");

        this.tweens.add({ targets: [this.frog], duration: 500, y: puddle.y, x: puddle.x, onComplete: this.jumpDone.bind(this) });

    }
    jumpDone() {
        if (this.lastPuddle.isLast==false)
        {
            this.lastPuddle.hasFrog();
        }      
        else
        {
            this.playSound("win");
            //do win
            setTimeout(() => {
                this.scene.start("SceneOver");
            }, 2000);
        }
        this.clickLock = false;
        this.frog.setFrame("frog1.png");
        this.showNextPuddle();
    }
    getAngle(obj1: PosVo, obj2: PosVo) {
        //I use the offset because the ship is pointing down
        //at the 6 o'clock position
        //set to 0 if your sprite is facing 3 o'clock
        //set to 180 if your sprite is facing 9 o'clock
        //set to 270 if your sprite is facing 12 o'clock
        //
        let offSet: number = 90;
        // angle in radians
        let angleRadians = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
        // angle in degrees
        let angleDeg = (Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x) * 180 / Math.PI);
        //add the offset
        angleDeg += offSet;
        return angleDeg;
    }
}