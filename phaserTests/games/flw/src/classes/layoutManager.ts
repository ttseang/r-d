import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import {  GameFontSizes } from "./GameFontSizes";
import { GamePosObj } from "./GamePosObj";
import { GameScaleObject } from "./GameScaleObj";
import { GM } from "./GM";

export class LayoutManager {
    public scene: Phaser.Scene;
    
    private gm: GM = GM.getInstance();
    private bscene: IBaseScene;
    

    public callback: Function = () => { };

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;

        window.addEventListener("orientationchange", function () {
            console.log(window.innerWidth, window.innerHeight)
            this.flipped();
        }.bind(this));
    }
    private flipped() {
        console.log('flipped');
        setTimeout(() => {
            let w: number = window.innerWidth;
            let h: number = window.innerHeight;
            if (w < h) {
                this.gm.isPort = true;
            }
            else {
                this.gm.isPort = false;
            }
            this.bscene.resetSize(w, h);
            this.scene.scale.resize(w, h);
            this.bscene.getGrid().hide();
            this.bscene.makeGrid(11, 11);
            this.callback();
        }, 1000);

    }
   
    public getPos() {

        let messagePosition: PosVo = new PosVo(5, 1);
        let coinPosition = new PosVo(2, 1);
        let coinTextPosition=new PosVo(5, 1);
        let buttonPosition = new PosVo(5,9);

        if (this.gm.isMobile === true && this.gm.isPort === false) {

        }
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            coinPosition=new PosVo(1,1);
        }
        if (this.gm.isTablet && this.gm.isPort === false) {

        }
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            coinPosition=new PosVo(3,1);
        }
        if (this.gm.isTablet === true && this.gm.isPort === true) {

        }

        let gamePosObj: GamePosObj = new GamePosObj(messagePosition, coinPosition,coinTextPosition,buttonPosition);

        return gamePosObj;
    }
    public getScale() {

         //desktop

        let letterScale: number = 0.075;
        let coinScale: number = 0.1;
        let buttonScale: number =0.25;

       
       
        
        //mobile landscape
        if (this.gm.isMobile === true && this.gm.isPort === false) {
            letterScale = 0.08;
            buttonScale=0.3;
        }

        //mobile portrait
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            letterScale = 0.2;
            coinScale=0.2;
            buttonScale=0.7;
            
        }
        //tablet landscape
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            letterScale = 0.1;
            
        }
        //tablet portrait
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            letterScale = 0.2;
            coinScale = 0.2;
            buttonScale=0.5;
        }


        return new GameScaleObject(letterScale, coinScale,buttonScale);

    }
    public getFontSizes() {

        let coinTextSize:number =52;
        let messageTextSize:number =52;
        let buttonTextSize:number =22;

       
        if (this.gm.isMobile === true && this.gm.isPort === false) {
            buttonTextSize=18;
        }
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            buttonTextSize=24;
            coinTextSize=36;
            messageTextSize=40;
        }
        if (this.gm.isTablet && this.gm.isPort === false) {
            
        }
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            buttonTextSize=30;
        }
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            buttonTextSize=25;
        }
       

        let gameFontSizes:GameFontSizes = new GameFontSizes(coinTextSize, buttonTextSize,messageTextSize);
        return gameFontSizes
    }
}