import {
    UIBlock
} from "../ui/uiblock";;
import {Align} from "../util/align";

export class Effect extends UIBlock {
    constructor(config) {
        super(config.scene);
        this.scene = config.scene;
        //
        //
        //
        var effectNumber = config.effect;
        var imageKey = "effect" + effectNumber;
       // console.log("imageKey="+imageKey);

        this.image = this.scene.add.sprite(0, 0, imageKey);
        this.add(this.image);
        if (config.scale) {
            Align.scaleToGameW(this.image, config.scale,config.scene);
        }
        //
        //
        //
        if (effectNumber < 11) {
            //
            //
            //
            this.maxScale = 1;
            if (config.maxScale) {
                this.maxScale = config.maxScale;
            }
            this.time = 500;
            if (config.time) {
                this.time = config.time;
            }
            //
            //
            //
            this.scene.tweens.add({
                targets: this.image,
                duration: this.time,
                scaleX: this.maxScale,
                scaleY: this.maxScale,
                alpha: 0,
                angle: 180,
                onComplete: this.destroy.bind(this)
            });
        } else {
            var animKey = 'effect_anim_' + effectNumber;
            //
            //
            //
            //var frameNames = this.textures.get('ef1').getFrameNames();
            if (!this.scene.anims.anims.has(animKey)) {
                var frameNames = this.scene.anims.generateFrameNumbers(imageKey);
                this.scene.anims.create({
                    key: animKey,
                    frames: frameNames,
                    frameRate: 64,
                    repeat: 0
                });
            }
            this.image.play(animKey);
            this.image.on('animationcomplete', this.destroy, this);
        }
        if (config.x) {
            this.x = config.x;
        }
        if (config.y) {
            this.y = config.y;
        }
    }
    destroy() {
        super.destroy();
    }
    static preload(scene, effectNumber) {
        var key = "effect" + effectNumber;
        var isSpriteSheet = false;
        var w = 0;
        var h = 0;
        //
        //
        //
        var path = "fx1/" + effectNumber;
        //
        //
        //
        if (effectNumber > 10 && effectNumber < 81) {
            var effectNumber2 = effectNumber - 10;
            path = "fx2/" + effectNumber2;
            isSpriteSheet = true;
            w = 256;
            h = 256;
        }
        if (effectNumber >80) {
            var effectNumber2 = effectNumber - 80;
            path = "fx3/" + effectNumber2;
            isSpriteSheet = true;
            w = 100;
            h = 100;
        }
        path += ".png";
        console.log(path);

        if (isSpriteSheet == true) {
            console.log(key);
            scene.load.spritesheet(key, "https://happyplacegames.com/assets/images/games/common/effects/"+path, {
                frameWidth: w,
                frameHeight: h
            });
        } else {
            scene.load.image(key, "https://happyplacegames.com/assets/images/games/common/effects/"+path);
        }
    }
}