import { GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import { ColorBurst } from "../classes/effects/colorBurst";
import { GameFontSizes } from "../classes/GameFontSizes";
import { GamePosObj } from "../classes/GamePosObj";
import { LayoutManager } from "../classes/layoutManager";
import { LetterBox } from "../classes/LetterBox";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private tw: number = -1;
    private th: number = -1;
    private tiles: LetterBox[] = [];
    public tileGrid: string[][] = [[]];
    private selectedTile: LetterBox | null = null;
    private swapTile: LetterBox | null = null;
    private clickLock: boolean = false;
    private usedWords: string[] = [];

    public pickwords: string[] = [];
    private coinCount: number = 10;
    //
    //
    //
    private words: string[] = [];
    private letters: string[] = [];

    //text objects
    private coinText: Phaser.GameObjects.Text;
    private msgText: Phaser.GameObjects.Text;
    private hintIndex: number = -1;

    private coin: GameObjects.Image;
    private bg: GameObjects.Image;


    //scale and placing
    private letterScale: number = 0.075;
    private coinScale: number = 0.1;
    private buttonScale: number = 0.5;

    private messagePosition: PosVo;
    private coinPosition: PosVo;
    private coinTextPosition: PosVo;
    private buttonPosition: PosVo;

    private buttonTextSize: number = 26;
    private coinTextSize: number = 26;
    private messageTextSize: number = 26;


    private layoutManager: LayoutManager;

    private messasges: string[] = [];

    private hintButton: Button;

    private level: number=0;
    private levelWords: string[] = [];

    private backgroundMusic:Phaser.Sound.BaseSound;
    private crow:GameObjects.Sprite;

    constructor() {
        super("SceneMain");
    }
    preload() {
        //this.load.image("face", "./assets/face.png");
        this.load.image("pine", "./assets/pine.png");
        this.load.image("coin", "./assets/coin.png");
        this.load.image("button", "./assets/ui/buttons/1/3.png");
        this.load.image("bg", "./assets/bg.jpg");

        this.load.text("words", "./assets/data/fourletterwords.txt");
        //
        //
        //
        this.load.audio("coins", "./assets/audio/sfx/coins.mp3");
        this.load.audio("newlevel", "./assets/audio/sfx/newlevel.wav");
        this.load.audio("rightSound", "./assets/audio/sfx/right.mp3");
        this.load.audio("swish", "./assets/audio/sfx/swish.wav");
        this.load.audio("bgm","./assets/audio/background/forest.mp3");

        this.load.atlas("crow", "./assets/anims/crow.png","./assets/anims/crow.json");

        ColorBurst.preload(this);
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.showPos();

        


        this.layoutManager = new LayoutManager(this);

        for (let i = 0; i < 4; i++) {
            this.tileGrid[i] = [];
        }

        this.setScalePos();

        this.bg = this.add.image(0, 0, "bg");
        Align.scaleToGameW(this.bg, 1, this);
        if (this.bg.displayHeight < this.gh) {
            this.bg.displayHeight = this.gh;
            this.bg.scaleX = this.bg.scaleY;
        }
        Align.center(this.bg, this);

        this.addCrow();

        let fs: number = this.gw / 20;

        this.coinText = this.add.text(0, 0, this.coinCount.toString(), { "fontSize": fs.toString() + "px", "color": "black", "fontStyle": "bold" }).setOrigin(0.5, 0.5);
        this.msgText = this.add.text(0, 0, "", { "fontSize": fs.toString() + "px", "color": "black", "backgroundColor": "yellow" }).setOrigin(0.5, 0.5);

        this.coin = this.add.image(0, 0, "coin");

        this.hintButton = new Button(this, "button", "Get a Hint for 5 coins");
        this.hintButton.setCallback(this.showHint.bind(this));

        fetch("./assets/levels.json")
        .then(response => response.json())
        .then(data => this.process({ data }));

          
    }
    process(data:any)
    {
        let levels:any=data.data.levels;
        for (let i=0; i<levels.length;i++)
        {
            let words:string[]=[];
            console.log(levels[i].words);
            for (let j=0; j<levels[i].words.length;j++)
            {
                console.log(levels[i].words[j]);
                words.push(levels[i].words[j]);
            }
            this.levelWords.push(words.join(","));
        }

        this.backgroundMusic=this.sound.add("bgm");
        this.backgroundMusic.play({volume:0.5,loop:true});

        this.setUp();
    }
    addCrow()
    {
        var frameNames= this.anims.generateFrameNames('crow', {start: 1, end: 6, zeroPad: 0, prefix: 'Asset ', suffix: '.png'})

        //console.log(frameNames);
        this.anims.create({
            key: 'crowfly',
            frames:frameNames,
            frameRate: 8,
            repeat: -1
        });
      
        this.crow=this.add.sprite(0, 0,"crow");
        this.crow.x=this.gw-100;
        this.crow.y=this.gh*0.1;
        this.crow.play("crowfly");  
    }
    setUp() {
        this.pickwords = this.cache.text.get("words").split(" ");

        this.pickRandomWords();

        this.makeTileGrid();

        this.shuffle();

        this.scaleItems();

        this.placeItems();

        this.updateTextSize();

        this.checkRows();

        this.input.on("gameobjectdown", this.clickTile.bind(this));

        this.layoutManager.callback = this.flipped.bind(this);


        window['scene'] = this;
    }
    flipped() {
        this.setScalePos();
        this.scaleItems();
        this.placeItems();

    
    }
    setScalePos(): void {
        let scaleObj: any = this.layoutManager.getScale();

        this.letterScale = scaleObj.letterScale;
        this.coinScale = scaleObj.coinScale;
        this.buttonScale = scaleObj.buttonScale;


        let posObj: GamePosObj = this.layoutManager.getPos();
        this.messagePosition = posObj.messagePosition;
        this.coinPosition = posObj.coinPosition;
        this.coinTextPosition = posObj.coinTextPosition;
        this.buttonPosition = posObj.buttonPosition;

        let gameFontSizes: GameFontSizes = this.layoutManager.getFontSizes();
        this.coinTextSize = gameFontSizes.coinText;
        this.buttonTextSize = gameFontSizes.buttonText;
        this.messageTextSize = gameFontSizes.messageText;
    }
    updateTextSize() {
        this.hintButton.setTextSize(this.buttonTextSize);
        this.coinText.setFontSize(this.coinTextSize);
        this.msgText.setFontSize(this.messageTextSize);
    }
    scaleItems() {
        Align.scaleToGameW(this.coin, this.coinScale, this);
        this.hintButton.setBackScale(this.buttonScale);
        for (let i = 0; i < this.tiles.length; i++) {
            this.tiles[i].setFlippedScale(this.letterScale);
        }

        Align.scaleToGameW(this.bg, 1, this);
        if (this.bg.displayHeight < this.gh) {
            this.bg.displayHeight = this.gh;
            this.bg.scaleX = this.bg.scaleY;
        }
        Align.center(this.bg, this);
    }
    placeItems() {

        console.log(this.buttonPosition);

        this.grid.placeAt(this.messagePosition.x, this.messagePosition.y, this.msgText);
        this.grid.placeAt(this.coinPosition.x, this.coinPosition.y, this.coinText);
        this.grid.placeAt(this.coinPosition.x, this.coinPosition.y, this.coin);
        this.grid.placeAt(this.buttonPosition.x, this.buttonPosition.y, this.hintButton);

        this.children.bringToTop(this.coinText);
        this.positionTiles();

        this.crow.x=this.gw;
        this.crow.y=this.gh*0.2;
    }
    setMessage(msg: string) {
        this.coin.visible = false;;
        this.coinText.visible = false;

        this.children.bringToTop(this.msgText);
        this.msgText.setText(msg);
        setTimeout(() => {
            this.msgText.setText("");
            this.coin.visible = true;;
            this.coinText.visible = true;

        }, 2000);
    }
    showHint() {
        if (this.coinCount < 5) {
            this.setMessage("You do not have enough coins");
            return;
        }
        if (this.hintIndex == 3) {
            this.setMessage("There are no hints available");
            return;
        }
        this.spendCoin();
        this.hintIndex++;
        let word = this.words[this.hintIndex];
        this.setMessage("Hint: " + word);
    }
    spendCoin() {
        this.coinCount-=5;
        this.coinText.setText(this.coinCount.toString());
    }
    getCoin() {
        this.coinCount++;
        this.coinText.setText(this.coinCount.toString());
    }
    showFoundWords() {
        if (this.messasges.length === 0) {
            return;
        }
        this.setMessage(this.messasges.shift());
        this.playSound("coins");
        this.getCoin();
        setTimeout(() => {
            this.showFoundWords();
        }, 1000);
    }
    pickRandomWords() {
        
        let letters: string = "";
        if (this.level<this.levelWords.length)
        {
           console.log(this.levelWords[this.level]);
           let words:string=this.levelWords[this.level];
           console.log(words);
           this.words=words.split(",");

            this.words=this.levelWords[this.level].split(",");
                   

           letters = this.words.join("");
        }
        else
        {
        for (let i = 0; i < 4; i++) {
            let r: number = Math.floor(Math.random() * this.pickwords.length);
            let word: string = this.pickwords[r];
            letters += word;
            this.words.push(word);
        }
    }
        this.letters = letters.split("");
    }
    clickTile(p: Phaser.Input.Pointer, obj: LetterBox) {

        if (obj instanceof (GameObjects.Image)) {
            return;
        }
        this.clickLock = true;
        if (obj == this.selectedTile) {
            this.selectedTile.clearSelected();
            this.selectedTile = null;
            return;
        }
        if (this.selectedTile == null) {
            this.selectedTile = obj;
            obj.showSelected();
            this.clickLock = false;
            return;
        }
        else {
            //do swap
            this.selectedTile.clearSelected();

            this.swapTile = obj;
            this.swapTiles();

           
        }
    }
    playSound(sound: string) {
        let audio: Phaser.Sound.BaseSound = this.sound.add(sound);
        audio.play();
    }
    swapTiles() {

        this.playSound("swish");

        this.children.bringToTop(this.swapTile);
        this.children.bringToTop(this.selectedTile);

        let posVo: PosVo = new PosVo(this.selectedTile.x, this.selectedTile.y);
        let posVo2: PosVo = new PosVo(this.swapTile.x, this.swapTile.y);

        if (this.swapTile) {
            this.tweens.add({ targets: this.swapTile, duration: 250, y: posVo.y, x: posVo.x });

        }
        if (this.selectedTile) {
            this.tweens.add({ targets: this.selectedTile, duration: 250, y: posVo2.y, x: posVo2.x });
        }


        let xx1: number = this.selectedTile.xx;
        let xx2: number = this.swapTile.xx;

        let yy1: number = this.selectedTile.yy;
        let yy2: number = this.swapTile.yy;

        let letter1: string = this.selectedTile.letter;
        let letter2: string = this.swapTile.letter;



        this.tileGrid[yy1][xx1] = letter2;
        this.tileGrid[yy2][xx2] = letter1;

        this.selectedTile.xx = xx2;
        this.selectedTile.yy = yy2;

        this.swapTile.xx = xx1;
        this.swapTile.yy = yy1;



        setTimeout(() => {
            this.checkRows();
            this.selectedTile = null;
            this.clickLock = false;
        }, 300);
    }
    shuffle() {
        for (let i = 0; i < 10; i++) {
            let r1: number = Math.floor(Math.random() * this.tiles.length);
            let r2: number = Math.floor(Math.random() * this.tiles.length);

            let tile1: LetterBox = this.tiles[r1];
            let tile2: LetterBox = this.tiles[r2];

            let xx1: number = tile1.xx;
            let xx2: number = tile2.xx;

            let yy1: number = tile1.yy;
            let yy2: number = tile2.yy;

            let letter1: string = tile1.letter;
            let letter2: string = tile2.letter;



            this.tileGrid[yy1][xx1] = letter2;
            this.tileGrid[yy2][xx2] = letter1;

            tile1.xx = xx2;
            tile2.xx = xx1;

            tile1.yy = yy2;
            tile2.yy = yy1;

            let x1: number = tile1.x;
            let y1: number = tile1.y;

            let x2: number = tile2.x;
            let y2: number = tile2.y;

            tile1.x = x2;
            tile1.y = y2;

            tile2.x = x1;
            tile2.y = y1;



        }
    }
    checkRows() {


        let wordCount: number = 0;

        for (let i: number = 0; i < 4; i++) {
            let word: string = this.tileGrid[i].join("");


            if (this.pickwords.includes(word)) {
                if (!this.usedWords.includes(word)) {
                    this.messasges.push("found word " + word);

                    if (this.swapTile) {
                        let cb1 = new ColorBurst(this, this.swapTile.x, this.swapTile.y, 50, 150, 1000, 0xffffff);
                    }
                    if (this.selectedTile) {
                        let cb2 = new ColorBurst(this, this.selectedTile.x, this.selectedTile.y, 50, 150, 1000, 0xffffff);
                    }

                    //this.setMessage("found "+word);
                    //this.playSound("coins");
                    this.usedWords.push(word);
                }
                wordCount++;
                if (wordCount == 4) {
                    this.playSound("newlevel");
                    this.clickLock=true;
                    let cb3 = new ColorBurst(this, this.gw/2, this.gh/2, 100, 150, 1000, 0xffffff);
                    setTimeout(() => {
                        this.doNewLevel();
                    }, 3000);
                   
                }
            }
        }
        this.showFoundWords();

    }
    doNewLevel() {

        for (let i:number = 0; i <this.tiles.length;i++) {
            this.tiles[i].destroy();
        }
        this.tiles=[];

        this.level++;

        this.pickRandomWords();

        this.makeTileGrid();

        this.shuffle();

        this.scaleItems();

        this.placeItems();

        this.updateTextSize();

        this.checkRows();
    }
    makeTileGrid(): void {
        let xx: number = 0;
        let yy: number = 0;
        let tile: LetterBox;

        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {

                let letter: string = this.letters.shift();

                tile = new LetterBox(this, letter, this.letterScale);
                this.tiles.push(tile);
                this.tileGrid[i][j] = letter;
                tile.x = xx;
                tile.y = yy;
                tile.xx = j;
                tile.yy = i;
                //
                //
                //
                xx += tile.displayWidth;

                if (this.tw === -1) {
                    this.tw = tile.displayWidth;
                }
                if (this.th === -1) {
                    this.th = tile.displayHeight;
                }
            }
            yy += tile.displayHeight;
            xx = 0;
        }


    }

    positionTiles() {

        this.tw = this.tiles[0].displayWidth;
        this.th = this.tiles[0].displayHeight;


        let startX: number = this.gw / 2 - (this.tw * 4) / 2 + this.tw / 2;
        let startY: number = this.gh / 2 - (this.th * 4) / 2 + this.th / 2;

        for (let i: number = 0; i < this.tiles.length; i++) {
            let tile: LetterBox = this.tiles[i];
            tile.x = startX + tile.xx * this.tw;
            tile.y = startY + tile.yy * this.th;
        }
    }
    update()
    {
        this.crow.x--;
        if (this.crow.x<-2000) {
            this.crow.x=this.gw+100;
        }
    }
}