import { GameObjects } from "phaser";
import { ColorBurst } from "../classes/effects/colorBurst";
import { LetterBlank } from "../classes/LetterBlank";
import { LetterBox } from "../classes/LetterBox";
import { Align } from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private wordIndex: number = -1;
    private words: string[] = [];
    private letterCount: number = 4;

    private word: string = "bird";
    private correct: string = "";

    private alphabet: string = "abcdefghijklmnopqrstuvwxyz";

    private blankIndex: number = 0;
    private blanks: LetterBlank[] = [];

    private boxes: LetterBox[] = [];
    private clock: GameObjects.Image;

    private clickLock: boolean = false;
    private pickwords: string[] = [];

    private timeText: GameObjects.Text;
    private secs: number = 5;
    private timer1: number = 0;
    private bg: GameObjects.Image;

    private backgroundMusic: Phaser.Sound.BaseSound;

    private fairy: GameObjects.Sprite;

    constructor() {
        super("SceneMain");

    }
    preload() {
        // this.load.image("pine","./assets/pine.png");
        this.load.image("blank", "./assets/blank.png");
        //   this.load.image("holder", "./assets/holder.jpg");
        this.load.image("blue", "./assets/blue.jpg");
        this.load.image("clock", "./assets/clock.png");
        this.load.image("bg", "./assets/bg.jpg");
        this.load.image("button", "./assets/ui/buttons/1/3.png");

        if (!this.cache.text.has("words"))
        {
            this.load.text("words", "./assets/data/fourletterwords.txt");
        }
      


        this.load.atlas("fairy", "./assets/fairy1.png", "./assets/fairy1.json");

        this.load.audio("newlevel", "./assets/audio/sfx/newlevel.wav");
        this.load.audio("fail", "./assets/audio/sfx/fail.mp3");
        this.load.audio("fairytale", "./assets/audio/background/fairytale.wav");

        ColorBurst.preload(this);
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.showNumbers();

        this.boxes = [];
        this.pickwords = [];
        this.blanks = [];
        this.secs = 5;
        this.words = ["bird", "word", "test", "find", "what", "cake", "cats"];
        this.wordIndex = -1;

        this.bg = this.add.image(0, 0, "bg");
        Align.scaleToGameW(this.bg, 1, this);
        if (this.bg.displayHeight < this.gh) {
            this.bg.displayHeight = this.gh;
            this.bg.scaleX = this.bg.scaleY;
        }
        Align.center(this.bg, this);

        this.pickwords = this.cache.text.get("words").split(" ");

        for (let i = 0; i < this.letterCount; i++) {

            let blank: LetterBlank = new LetterBlank(this, "_", 0.09);
            this.blanks.push(blank);
            this.grid.placeAt(3.5 + i, 4, blank);

            let letterBox = new LetterBox(this, "_", 0.09);
            this.boxes.push(letterBox);
            this.grid.placeAt(3.5 + i, 7, letterBox);
        }

        this.clock = this.placeImage2("clock", 5, 1.5, 0.1);

        this.timeText = this.add.text(0, 0, "5", { "fontSize": "80px", "fontFamily": "Arial", "color": "#ff0000", "stroke": "#000000", "strokeThickness": 4 })
        this.timeText.setOrigin(0.5, 0.5);
        this.grid.placeAt(5, 1.5, this.timeText);



        this.nextLevel();

        this.input.on("gameobjectdown", this.chooseLetter.bind(this));

        if (this.backgroundMusic)
        {
            this.backgroundMusic.stop();
        }
        this.backgroundMusic = this.sound.add("fairytale");
        this.backgroundMusic.play({ volume: 0.2, loop: true });

        //Idle Blinking_
        this.makeFairy();
    }
    makeFairy() {
        this.fairy = this.add.sprite(0, 0, "fairy");

        let frames = this.anims.generateFrameNames('fairy', { start: 0, end: 7, zeroPad: 3, prefix: 'Idle Blinking_', suffix: '.png' });

        this.anims.create({
            key: 'idle',
            frames: frames,
            frameRate: 8,
            repeat: -1
        });

       


        let frames2 = this.anims.generateFrameNames("fairy",{ start: 0, end:7,zeroPad:3,prefix:"Casting Spells_",suffix: '.png'})
        this.anims.create({
            key: 'casting',
            frames: frames2,
            frameRate:24,
            repeat:0
        });
        window['fairy']=this.fairy;

        this.fairy.play("idle");

        this.grid.placeAt(3, 3, this.fairy);
    }
    downTime() {
        this.secs--;
        this.timeText.setText(this.secs.toString());
        if (this.secs == 0) {
            this.showWrong();
        }
    }
    chooseLetter(p: Phaser.Input.Pointer, obj: LetterBox) {

        if (this.clickLock === true) {
            return;
        }
        this.clickLock = true;
        if (obj instanceof (GameObjects.Image)) {
            return;
        }
        let chosenLetter: string = obj.letter;
        if (chosenLetter === this.correct) {
            this.showCorrect();
        }
        else {

            //check if word is in the dictionary

            let checkArray: string[] = this.word.split("");

            checkArray[this.blankIndex] = chosenLetter;

            let checkWord: string = checkArray.join("");
            console.log(checkWord);
            if (this.pickwords.includes(checkWord)) {

                
                this.blanks[this.blankIndex].setLetter(chosenLetter);
                for (let i = 0; i < 4; i++) {
                    this.boxes[i].visible = false;
                }
                
                this.fairy.play("casting");

                setTimeout(() => {
                    this.playSound("newlevel");
                    this.showStars();
                    this.fairy.play("idle");
                }, 300);
                clearInterval(this.timer1);
                setTimeout(() => {
                    this.nextLevel();
                }, 2000);
            }
            else {
                this.showWrong();
            }


        }
    }
    playSound(sound: string) {
        let audio: Phaser.Sound.BaseSound = this.sound.add(sound);
        audio.play();
    }
    showStars() {
        let cb1 = new ColorBurst(this, this.blanks[1].x + this.blanks[1].displayWidth / 2, this.blanks[1].y + this.blanks[1].displayHeight / 2, 50, 150, 1000, 0xffffff);
    }
    showCorrect() {
      
        this.fairy.play("casting");

        setTimeout(() => {
            this.playSound("newlevel");
            this.showStars();
            this.fairy.play("idle");
        }, 300);

        for (let i = 0; i < this.blanks.length; i++) {
            let blank: LetterBlank = this.blanks[i];
            blank.setLetter(this.word.substr(i, 1));
            this.boxes[i].visible = false;
        }
        clearInterval(this.timer1);
        
        setTimeout(() => {
            
            this.nextLevel();
        }, 2000);
    }
    showWrong() {
        this.clickLock=true;;
        this.playSound("fail");
        clearInterval(this.timer1);

        for (let i = 0; i < this.blanks.length; i++) {
            // let blank:LetterBlank=this.blanks[i];
            //  blank.setLetter(this.word.substr(i,1));  
            if (this.boxes[i].letter != this.correct) {
                this.boxes[i].visible = false;
            }

        }

        setTimeout(() => {
            this.scene.start("SceneOver");
        }, 2000);
    }
    nextLevel() {
        this.wordIndex++;
        this.setWord();
        this.setTileValues();
        this.clickLock = false;
        this.secs = 5;
        this.timeText.setText("5");
        this.timer1 = window.setInterval(this.downTime.bind(this), 1000);
    }
    setWord() {

        if (this.wordIndex < this.words.length) {
            this.word = this.words[this.wordIndex];
        }
        else {
            let r: number = Math.floor(Math.random() * this.pickwords.length);
            this.word = this.pickwords[r];

        }
        console.log(this.word);

        let space: number = Math.floor(Math.random() * 4);
        this.blankIndex = space;

        for (let i = 0; i < this.blanks.length; i++) {
            let blank: LetterBlank = this.blanks[i];
            if (i == space) {
                blank.setLetter("_");
                this.correct = this.word.substr(i, 1);
            }
            else {
                blank.setLetter(this.word.substr(i, 1));
            }
        }

    }
    setTileValues() {
        let alphaLetters: string[] = this.alphabet.split("");

        for (let i: number = 0; i < 20; i++) {
            let p1: number = Math.floor(Math.random() * alphaLetters.length);
            let p2: number = Math.floor(Math.random() * alphaLetters.length);

            let letter1: string = alphaLetters[p1];
            let letter2: string = alphaLetters[p2];

            alphaLetters[p1] = letter2;
            alphaLetters[p2] = letter1;

        }

        let four: string[] = alphaLetters.slice(0, 4);


        if (!four.includes(this.correct)) {


            let space = Math.floor(Math.random() * 4);
            four[space] = this.correct;
        }


        for (let i: number = 0; i < 4; i++) {
            this.boxes[i].setLetter(four[i]);
            this.boxes[i].visible = true;
        }

    }
}