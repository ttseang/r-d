import { GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneOver extends BaseScene
{
    private bg:GameObjects.Image;
    private playAgainButton:Button;
    constructor()
    {
        super("SceneOver");
    }

    create()
    {
        super.create();
        this.makeGrid(11, 11);

        this.bg = this.add.image(0, 0, "bg");
        Align.scaleToGameW(this.bg, 1, this);
        if (this.bg.displayHeight < this.gh) {
            this.bg.displayHeight = this.gh;
            this.bg.scaleX = this.bg.scaleY;
        }
        Align.center(this.bg, this);

        this.playAgainButton=new Button(this,"button","Play Again");
        this.playAgainButton.setCallback(()=>{
            this.scene.start("SceneMain");
        })

        this.grid.placeAtIndex(60,this.playAgainButton);
    }
}