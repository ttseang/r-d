import {Align} from "../util/align";

export class Flare extends Phaser.GameObjects.Sprite {
    constructor(config) {
        if (!config.x) {
            config.x = 0;
        }
        if (!config.y) {
            config.y = 0;
        }
        super(config.scene, config.x, config.y, "flare");
        var scale=.2;
        if (config.scale)
        {
            scale=config.scale;
        }
        Align.scaleToGameW(this,scale,this.scene);
        this.scene = config.scene;
        this.scene.add.existing(this);
        this.scene.tweens.add({
            targets: this,
            duration: 1000,
            alpha: 0,
            angle: -270,
            onComplete: this.tweenDone,
            onCompleteParams:[{scope:this}]
        });
    }
    tweenDone(tween, targets, custom) {
        custom.scope.destroy();
    }
}