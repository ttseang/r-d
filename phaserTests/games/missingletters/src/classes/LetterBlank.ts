import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class LetterBlank extends Phaser.GameObjects.Container
{
    public letter:string;
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private back:GameObjects.Image;
    private text1:GameObjects.Text;
    private graphics:GameObjects.Graphics;
    public xx:number=0;
    public yy:number=0;
    
    constructor(bscene:IBaseScene,letter:string,scale:number)
    {
        super(bscene.getScene());
        this.letter=letter;
        this.bscene=bscene;

        this.back=this.scene.add.image(0,0,"blank");
        this.add(this.back);
      //  this.back.setTint(0xEEEE05);
        Align.scaleToGameW(this.back,scale,this.bscene);

        let fs:number =this.back.displayWidth*0.8;
        this.text1=this.scene.add.text(0,0,letter,{"fontFamily":"Arial","fontSize":fs.toString()+"px","color":"#000000"}).setOrigin(0.5, 0.5);
        this.add(this.text1);

        this.graphics=this.scene.add.graphics();
        this.add(this.graphics);
        
        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.scene.add.existing(this);
    }
    setLetter(letter:string)
    {
        this.letter=letter;
        this.text1.setText(letter);
    }
    setFlippedScale(scale:number)
    {
        console.log("setFlippedScale="+scale);
        Align.scaleToGameW(this.back,scale,this.bscene);

        let fs:number =this.back.displayWidth;
        this.text1.setFontSize(fs);
        this.setSize(this.back.displayWidth,this.back.displayHeight);
    }
   
}