let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

    public letters:string[]=[];
    public bgMusic:Phaser.Sound.BaseSound;
    
    public rows:number[]=[4,6];
    constructor()
    {
        window['gm']=this;
        
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}