import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { GM } from "./GM";

export class LetterBubble extends GameObjects.Container {
    private bscene: IBaseScene;
    public letter: string = "A";
    public scene: Phaser.Scene;
    private text1: GameObjects.Text;
    private back: GameObjects.Image;
    // private callback: Function;
    private nextLetter: Function;
    private gm: GM = GM.getInstance();

    constructor(bscene: IBaseScene, nextLetter: Function) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.active = false;
        this.nextLetter = nextLetter;

        // this.callback = callback;

        this.back = this.scene.add.image(0, 0, "bubble").setOrigin(0, 0);
        this.add(this.back);
        Align.scaleToGameW(this.back, 0.05, this.bscene);

        this.text1 = this.scene.add.text(0, 0, "?", { fontSize: "30px", fontFamily: "Arial", color: "black" }).setOrigin(0.5, 0.5);
        this.text1.x = this.back.displayWidth / 2;
        this.text1.y = this.back.displayHeight / 2;
        this.add(this.text1);


        this.scene.physics.world.enable(this);

        setTimeout(() => {
            let body: any = this.body;
            body.setSize(this.back.displayWidth, this.back.displayHeight);
        }, 1000);


        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);

        /*  let obj: any = this;
         obj.body.setVelocityX(-300); */
    }
    setPos() {
        let r: number = (Math.floor(Math.random() * 2));
        let row:number=this.gm.rows[r];
        //letterBubble.y=this.gh/2;
        this.bscene.getGrid().placeAt(12, row-0.5, this);
    }
    setLetter(letter: string) {
        this.letter = letter;
        this.text1.setText(letter);
    }

    onUpdate() {
        if (this.x < 0) {


            this.setPos();
            this.x = this.bscene.getW() + 200;

            this.visible = true;
            //  this.setPos();
            this.letter = this.nextLetter();
            this.text1.setText(this.letter);
        }
    }
}