import { GM } from "../classes/GM";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {
        let folder: string = "3";

        this.load.image("bg", "./assets/background/" + folder + "/background.png");

        this.load.image("sky", "./assets/background/" + folder + "/1.png");
        this.load.image("mountains", "./assets/background/" + folder + "/2.png");
        this.load.image("back2", "./assets/background/" + folder + "/3.png");
        this.load.image("foreground", "./assets/background/" + folder + "/4.png");
        this.load.image("ground", "./assets/background/" + folder + "/5.png");
        this.load.atlas("bee", "./assets/bee.png", "./assets/bee.json");

        this.load.image("bubble","./assets/bubble.png");
        
        this.load.image("pine","./assets/pine.png");
        this.load.image("strike","./assets/strike.png");
        this.load.image("button","./assets/ui/buttons/1/3.png");
        

        this.load.audio("rightSound","./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");
        this.load.audio("pop","./assets/audio/pop.wav");
        this.load.audio("whoosh","./assets/audio/whoosh.mp3");
        this.load.audio("backgroundMusic","./assets/audio/beeMusic.mp3");


        this.load.text("words", "./assets/data/fourletterwords.txt");
        
    }
    create()
    {
      this.gm.bgMusic=this.sound.add("backgroundMusic",{loop:true,volume:0.5});    
      this.gm.bgMusic.play();
        
        this.scene.start("SceneMain");
    }
}