import { GameObjects, Physics } from "phaser";
import { GM } from "../classes/GM";
import { LetterBox } from "../classes/LetterBox";
import { LetterBubble } from "../classes/LetterBubble";
import Align from "../util/align";
import { RandWordUtil } from "../util/randWordUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private layers: GameObjects.TileSprite[] = [];
    private bg: GameObjects.Image;
    private ground: GameObjects.TileSprite;
    private bee: Physics.Arcade.Sprite;
    private randWordUtil: RandWordUtil;
    private gm: GM = GM.getInstance();
    private bubbles:Phaser.Physics.Arcade.Group;

    private currentWord:string="";
    private letterBoxes:LetterBox[]=[];
    private wrongCount:number=0;

    private strikes:GameObjects.Image[]=[];
    private correctText:GameObjects.Text;
    private correctCount:number=0;
    private rowIndex:number=0;
    private rowMax:number=1;

    constructor() {
        super("SceneMain");
    }
    preload() {
        
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

       

        this.letterBoxes=[];
        this.strikes=[];
        this.layers=[];
        this.wrongCount=0;
        this.correctCount=0;
        this.currentWord="";

        this.bg = this.add.image(0, 0, "sky");
        this.bg.displayWidth = this.gw;
        this.bg.scaleY = this.bg.scaleX;
        Align.center(this.bg, this);

        this.makeLayers();
        this.makeAnimation("bee");

        this.bee = this.physics.add.sprite(this.gw * .3, this.gh / 2, "bee");
        Align.scaleToGameW(this.bee, 0.05, this);
        this.bee.play("flying");
       // this.bee.setGravityY(300);

      
        

        this.bubbles=this.physics.add.group();

        window['scene'] = this;

        this.randWordUtil = new RandWordUtil(this);
        this.gm.letters = this.randWordUtil.pickRandomLetter();

        this.correctText=this.add.text(0,0,"Correct:0",{fontSize:"30px",color:"black",fontFamily:"Arial"});
        this.grid.placeAt(8,1,this.correctText);

        this.input.on('pointerdown', this.flap.bind(this));

        this.makeBubbles();
        this.makeStrikes();
        this.makeLetterBoxes();

        this.physics.add.overlap(this.bee,this.bubbles,()=>{ return false},this.collectLetter.bind(this));

        setTimeout(() => {
            this.bee.body.setSize(this.bee.width*0.9,this.bee.height*0.9);
        }, 1000);

        this.grid.placeAt(2,this.gm.rows[0],this.bee);
      //  this.grid.showPos();
    }
    getNextLetter() {
        if (this.gm.letters.length == 0) {
            this.gm.letters = this.randWordUtil.pickRandomLetter();
        }
        let letter: string = this.gm.letters.shift();
        return letter;
    }
    makeBubbles() {
        for (let i: number = 0; i < 5; i++) {
            let letterBubble: LetterBubble = new LetterBubble(this,this.getNextLetter.bind(this));
            letterBubble.setLetter(this.getNextLetter());
            letterBubble.setPos();            
            letterBubble.x=this.gw+i*300;
          //  
            this.bubbles.add(letterBubble);
            
            let obj:any=letterBubble;
            obj.body.setVelocityX(-300);
        }
    }
    collectLetter(m:Phaser.Physics.Arcade.Sprite,bubble:LetterBubble)
    {
        if (bubble.visible==false)
        {
            return;
        }
        let letter:string=bubble.letter;
        this.currentWord+=letter;
        this.updateTiles();
        if (this.currentWord.length==4)
        {
            this.checkWord();
        }
        this.playSound("pop");
        bubble.visible=false;
    }
    flap() {
       // this.bee.setVelocityY(-200);
        this.rowIndex++;
        if (this.rowIndex>this.rowMax)
        {
            this.rowIndex=0;
        }
        this.playSound("whoosh");
        this.grid.placeAt(2,this.gm.rows[this.rowIndex],this.bee);
    }
    makeAnimation(key: string) {
        let frames = this.anims.generateFrameNames(key, {
            start: 1,
            end: 8,
            zeroPad: 0,
            prefix: 'a',
            suffix: '.png'
        });
        //console.log(frames);
        this.anims.create({
            key: 'flying',
            frames: frames,
            frameRate: 8,
            repeat: -1
        });

    }
    makeLayers() {
        let keys: string[] = ["mountains", "back2", "foreground"];

        for (let i: number = 0; i < keys.length; i++) {
            let ts: GameObjects.TileSprite = this.add.tileSprite(0, 0, this.gw, this.gh, keys[i]);
            Align.center(ts, this);

            this.layers.push(ts);
        }
        this.ground = this.add.tileSprite(0, 0, this.gw, this.gh * 0.1, "ground");

        Align.center(this.ground, this);

    }
    makeStrikes()
    {
        for (let i:number=0;i<this.strikes.length;i++)
        {
            this.strikes.pop().destroy();
        }
        for (let i:number=0;i<this.wrongCount;i++)
        {
            let strike:GameObjects.Image=this.add.image(0,0,"strike");
            Align.scaleToGameW(strike,0.05,this);
            this.grid.placeAt(i,1,strike);
            this.strikes.push(strike);
        }
        if (this.wrongCount==3)
        {
            this.gameOver();
        }
    }
    updateTiles()
    {
        let letters:string[]=this.currentWord.split("");
        while(letters.length<4)
        {
            letters.unshift("?");
        }
        for (let i:number=0;i<letters.length;i++)
        {
            this.letterBoxes[i].setLetter(letters[i]);
        }

    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    makeLetterBoxes()
    {
        for(let i:number=0;i<4;i++)
        {
            let letterBox:LetterBox=new LetterBox(this);
            this.letterBoxes.push(letterBox);
            Align.scaleToGameW(letterBox,0.09,this);
            this.grid.placeAt(3.5+i,1,letterBox);
        }
    }
    checkWord()
    {
        if (this.randWordUtil.checkWord(this.currentWord))
        {
            console.log("correct");
            this.currentWord="";
            this.correctCount++;
            this.correctText.setText("Correct:"+this.correctCount);
            this.playSound("rightSound");
        }
        else
        {
            console.log("wrong");
            this.playSound("wrongSound");
            this.wrongCount++;
            this.makeStrikes();
            this.currentWord="";
            this.updateTiles();
        }
        
    }
    gameOver()
    {
        this.bubbles.children.iterate((bubble:LetterBubble)=>{
            if (bubble)
            {
                this.bubbles.remove(bubble,true,true);
            }
        })

        this.scene.start("SceneOver");
    }
    update() {
        for (let i: number = 0; i < this.layers.length; i++) {
            this.layers[i].tilePositionX += (i + 1) * 4;
        }
        this.bubbles.children.iterate((bubble:LetterBubble)=>{
            bubble.onUpdate();
        })
        if (this.bee.y>this.gh*1.2)
        {
            this.gameOver();
        }
    }
}