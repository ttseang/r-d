import { GameObjects } from "phaser";
import { Button } from "../classes/Button";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneOver extends BaseScene {
    private background: GameObjects.Image;

    constructor() {
        super("SceneOver");
    }
    create() {
        super.create();
        this.background = this.add.image(0, 0, "bg");

        Align.scaleToGameW(this.background, 1, this);

        if (this.background.displayHeight < this.gh) {
            this.background.displayHeight = this.gh;
            this.background.scaleX = this.background.scaleY;
        }

        Align.center(this.background, this);

        let btnPlay: Button = new Button(this, "button", "Play Again");

        btnPlay.setCallback(this.playAgain.bind(this));
        Align.center(btnPlay, this);
    }
    playAgain() {
        this.scene.start("SceneMain");
    }
}