import IBaseScene from "../interfaces/IBaseScene";

export class JigP extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    public p1: Phaser.GameObjects.Image;
    public p2: Phaser.GameObjects.Image;
    public p0: Phaser.GameObjects.Image;

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());
        this.scene = bscene.getScene();


        this.p1 = this.scene.add.image(-17, 0, "p1");
        this.p2 = this.scene.add.image(0, 0, "p2");
       // this.p2.setTint(0xff0000);

        this.p0 = this.scene.add.image(0, 0, "p0");

        this.add(this.p1);
        this.add(this.p2);
        this.add(this.p0);

        this.scene.add.existing(this);
    }
    setPimps(p: number) {
        //
        //
        //
        this.p0.visible = false;
        this.p1.visible = false;
        this.p2.visible = false;
        //
        //
        //

        switch (p) {
            case 0:

                this.p0.visible = true;

                break;

            case 1:

                this.p1.visible = true;

                break;

            case 2:

                this.p2.visible = true;

                break;



        }
    }
}