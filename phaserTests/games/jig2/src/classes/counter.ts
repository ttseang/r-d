export class Counter
{
    private limit:number=0;
    public current:number=0;
    public parent:Counter;
    public callback:Function=()=>{};

    constructor(limit:number)
    {
        this.limit=limit;
    }
    public advance()
    {
        this.current++;
        if (this.current>this.limit)
        {
            this.current=0;
            if (this.parent)
            {
                this.parent.advance();
            }
            else{
                if (this.callback)
                {
                    this.callback();
                }
            }
        }
    }
}