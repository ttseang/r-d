//import Phaser = require("phaser");
//import Phaser from 'phaser';
import { SceneMain } from "./scenes/SceneMain";

let w = 170;
let h = 180;
//
//

const config = {
    type: Phaser.CANVAS,
    width: w,
    height: h, 
    transparent: true,
    backgroundColor: 'rgba(255,110,110,0)',
    parent: 'phaser-game',
    scene: [SceneMain]
};

new Phaser.Game(config);