import { Time } from "phaser";
import { Counter } from "../classes/counter";
import { JigP } from "../classes/jigp";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private all: Phaser.GameObjects.Container;
    private counters:Counter[]=[];
    private first:Counter;
    private last:Counter;
    private top:JigP;
    private bottom:JigP;
    private left:JigP;
    private right:JigP;
    private timer1:Time.TimerEvent;
    private fileName:string="0000";

    constructor() {
        super("SceneMain");
    }
    preload() {
        for (let i: number = 0; i < 3; i++) {
            this.load.image("p" + i.toString(), "./assets/pimp" + i.toString() + ".png");
        }
        //this.load.image("face", "./assets/face.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        console.log("create!");



        /* let face:Phaser.GameObjects.Image=this.add.image(100, 200, "face");

        Align.scaleToGameW(face,0.25,this);

        this.grid.showNumbers();
        this.grid.placeAtIndex(60,face); */
        this.left = new JigP(this);
        this.left.x = 1;
        this.left.y = 0;

        this.right = new JigP(this);
        this.right.x = 49;
        this.right.y = 0;
        this.right.setAngle(-180);


        this.top = new JigP(this);
        this.top.x = 25;
        this.top.y = -25;
        this.top.setAngle(90);

        this.bottom = new JigP(this);
        this.bottom.x = 25;
        this.bottom.y = 25;
        this.bottom.setAngle(-90);

        this.all = this.add.container();

        this.all.add(this.left);
        this.all.add(this.top);
        this.all.add(this.bottom);
        this.all.add(this.right);

        this.top.setPimps(0);
        this.left.setPimps(0);
        this.right.setPimps(0);
        this.bottom.setPimps(0);

        this.all.x = 60;
        this.all.y = 90;
        // Align.center(this.all,this);

        window['all'] = this.all;

        let prevCounter:Counter;
        
        for (let i:number=0;i<4;i++)
        {
            let counter:Counter=new Counter(2);
            if (prevCounter)
            {
                prevCounter.parent=counter;
            }
            this.counters.push(counter);
            prevCounter=counter;
        }
        
        this.first=this.counters[0];
        this.first.current=-1;
        this.last=this.counters[3];
        this.last.callback=this.done.bind(this);
        //console.log(this.counters);
        console.log(this.first);

       // this.input.on("pointerdown", this.getImage.bind(this));
       this.input.on("pointerdown", this.advance.bind(this));

   //   this.timer1=this.time.addEvent({delay:2000,loop:true,callback:this.advance.bind(this)});
    }
    advance()
    {
        this.first.advance();
        this.getValues();
    }
    getValues()
    {
        let topIndex:number=this.counters[0].current;
        let rightIndex:number=this.counters[1].current;
        let bottomIndex:number=this.counters[2].current;
        let leftIndex:number=this.counters[3].current;

        this.top.setPimps(topIndex);
        this.right.setPimps(rightIndex);
        this.bottom.setPimps(bottomIndex);
        this.left.setPimps(leftIndex);
        this.getImage();
        this.fileName=topIndex.toString()+rightIndex.toString()+bottomIndex.toString()+leftIndex.toString();
        console.log(this.fileName);
      
        
       // console.log(leftIndex,bottomIndex,rightIndex,topIndex);
    }
    done()
    {
        this.timer1.remove();
        console.log("done");

    }

    getImage() {
        var canvasData = this.game.canvas.toDataURL("image/png");
        var ajax = new XMLHttpRequest();

        var xmlHttpReq = false;

        if (window.XMLHttpRequest) {
            ajax = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        ajax.open("POST", "http://localhost/saveImage/index.php?fn="+this.fileName, false);
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.onreadystatechange = function () {
            console.log(ajax.responseText);
           // console.log(this.fileName);
        }
        ajax.send("imgData=" + canvasData);
    }
    //this.game.canvas.toDataURL()
    // 
    getSnap()
    {
        this.game.renderer.snapshotArea(this.all.x-60,this.all.y-90,200,200,this.gotImage.bind(this));
    }
    gotImage(image) {
        console.log(image);
        document.body.appendChild(image);
    }
}