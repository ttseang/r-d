import { GameObjects } from "phaser";
import { GM } from "../classes/GM";
import { LetterBox } from "../classes/letterBox";
import { TargetBox } from "../classes/targetBox";
import { WordVo } from "../dataObjs/WordVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private dragBox: LetterBox | null = null;
    private targetBoxes: TargetBox[] = [];
    private chars: string[] = ["A", "B", "C"];
    private level: number = -1;
    private levels: WordVo[] = [];
    private letterBoxes: LetterBox[] = [];
    private target1: TargetBox;
    private target2:TargetBox;

    private clickLock:boolean=false;

    public word: string;
    private mainImage: GameObjects.Image;
    private mainText:GameObjects.Text;

    private gm: GM = GM.getInstance();

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("box", "./assets/box.png");
        this.load.image("paper","./assets/paper.png");
        
        this.levels = this.gm.levels;

        for (let i: number = 0; i < this.levels.length; i++) {
            this.load.image(this.levels[i].word, "./assets/pics/" + this.levels[i].word + ".png");
        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        //this.grid.showPos();

        let bg: GameObjects.Image = this.add.image(0, 0, "paper");
        Align.scaleToGameW(bg, 1, this);
        Align.center(bg, this);


        this.target1 = this.addTargetBox(0x00ff00);
        this.grid.placeAt(3, 3, this.target1);

        this.target2 = this.addTargetBox(0xff0000);
        this.grid.placeAt(8, 3, this.target2);

        let instructions:GameObjects.Text=this.add.text(0,0,"Drag the first letter of the picture to the green box\nand the last letter to the red box",{"color":"#000000","fontSize":"26px"});
        this.grid.placeAt(2,1,instructions);

        this.mainText=this.add.text(0,0,"WORD",{"color":"#000000","fontSize":"26px"}).setOrigin(0.5,0.5);
        this.grid.placeAt(5.5,7,this.mainText);

        this.nextLevel();
    }

    nextLevel() {
        this.level++;
        if (this.level > this.levels.length - 1) {
           // console.log("Game Over");
            this.scene.start("SceneOver");
            return;
        }
        this.chars = this.levels[this.level].chars;
        this.word = this.levels[this.level].word;
        if (this.mainImage) {
            this.mainImage.destroy();

        }
        this.mainText.setText(this.word);
        this.mainText.visible=false;

        this.mainImage = this.add.image(0, 0, this.word);
        
        Align.scaleToGameW(this.mainImage, 0.15, this);
        this.grid.placeAt(5.5, 4, this.mainImage);
        this.makeBoxes();
        this.clickLock=false;
    }
    hideBoxes() {
        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            this.letterBoxes[i].visible = false;
        }
    }
    makeBoxes() {
        this.hideBoxes();

        for (let i: number = 0; i < this.chars.length; i++) {
            let box: LetterBox;
            if (this.letterBoxes[i]) {
                box = this.letterBoxes[i];
                box.setLetter(this.chars[i]);
            }
            else {
                box = this.addLetter(this.chars[i]);
            }

            this.grid.placeAt(i + 4, 9, box);
            box.setPlace();
            box.setInteractive();
            box.visible = true;
        }


        this.target1.correct = this.word.substr(0, 1).toLowerCase();
        this.target2.correct=this.word.substr(-1,1);

        this.input.once("gameobjectdown", this.onDown.bind(this));
    }

    addLetter(letter: string) {
        let box: LetterBox = new LetterBox(this, letter);
        this.letterBoxes.push(box);
        return box;
    }
    addTargetBox(color: number) {
        let targetBox: TargetBox = new TargetBox(this, color);
        this.targetBoxes.push(targetBox);
        return targetBox;
    }
    onDown(p: Phaser.Input.Pointer, letterBox: LetterBox) {
        if (this.clickLock==true)
        {
            return;
        }
        this.dragBox = letterBox;
        this.children.bringToTop(letterBox);
        this.input.on("pointermove", this.onMove.bind(this));
        this.input.once("pointerup", this.onUp.bind(this));
    }
    onMove(p: Phaser.Input.Pointer) {
        this.dragBox.x = p.x;
        this.dragBox.y = p.y;
    }
    onUp() {
        this.input.off("pointermove");
        this.checkTargets();
        this.dragBox = null;
        this.input.once("gameobjectdown", this.onDown.bind(this));
    }
    doWinEffect() {
        this.mainText.visible=true;

        setTimeout(() => {
            this.nextLevel();
        }, 2000);
    }
    checkTargets() {
        console.log(this.targetBoxes);
        console.log(this.dragBox);
        if (this.dragBox === null) {
            return;
        }

        for (let i: number = 0; i < this.targetBoxes.length; i++) {
            let targetBox: TargetBox = this.targetBoxes[i];

            let distY: number = Math.abs(this.dragBox.y - targetBox.y);
            let distX: number = Math.abs(this.dragBox.x - targetBox.x);

            if (distX < this.dragBox.displayWidth * 0.4 && distY < this.dragBox.displayHeight * 0.4) {
                this.dragBox.x = targetBox.x;
                this.dragBox.y = targetBox.y;
                if (this.dragBox.letter.toLowerCase() !== targetBox.correct) {
                    this.dragBox.snapBack();
                }
                else {
                    targetBox.setLetter=this.dragBox.letter;
                    this.dragBox.removeInteractive();
                    if (this.target1.setLetter===this.target1.correct && this.target2.setLetter===this.target2.correct)
                    {
                        this.clickLock=true;
                        this.doWinEffect();
                    }                   
                }
            }
        }
    }
}