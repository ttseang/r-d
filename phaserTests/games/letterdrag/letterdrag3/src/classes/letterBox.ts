import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class LetterBox extends GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;

    private back:GameObjects.Image;
    private textBox:GameObjects.Text;
    public orginal:PosVo=new PosVo(0,0);
    public letter:string="";
    private graphics:GameObjects.Graphics;

    constructor(bscene:IBaseScene,letter:string)
    {
        super(bscene.getScene())
        this.bscene=bscene;
        this.scene=bscene.getScene();
        
        this.letter=letter;
        this.back=this.scene.add.image(0,0,"box");
        Align.scaleToGameW(this.back,0.08,this.bscene);

        this.textBox=this.scene.add.text(0,0,letter,{fontSize:"100px",color:"#000000"}).setOrigin(0.5,0.5);
      /*   this.textBox=this.scene.add.bitmapText(0,0,"aff",letter).setOrigin(0.5,0.5);
        this.textBox.fontSize=120;
        this.textBox.setMaxWidth(this.back.displayWidth*0.7); */

        this.add(this.back);
        this.add(this.textBox);

        this.graphics=this.scene.add.graphics();
        this.graphics.lineStyle(2,0x00000);
        this.graphics.strokeRect(this.back.x-this.back.displayWidth/2,this.back.y-this.back.displayHeight/2,this.back.displayWidth,this.back.displayHeight);
       
        this.add(this.graphics);
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
        this.scene.add.existing(this);        
    }
    setLetter(char:string)
    {
        this.letter=char;
        this.textBox.setText(char);
    }
    public setPlace()
    {
        this.orginal.x=this.x;
        this.orginal.y=this.y;
    }
    public snapBack()
    {
        this.scene.tweens.add({targets: this,duration: 500,y:this.orginal.y,x:this.orginal.x});
    }
}