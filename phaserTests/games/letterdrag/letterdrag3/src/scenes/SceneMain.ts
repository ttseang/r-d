import { GameObjects } from "phaser";
import { GM } from "../classes/GM";
import { LetterBox } from "../classes/letterBox";
import { TargetBox3 } from "../classes/targetBox3";
import { WordVo } from "../dataObjs/WordVo";
import { WordVo2 } from "../dataObjs/WordVo2";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private dragBox: LetterBox | null = null;
    private targetBoxes: TargetBox3[] = [];
    private chars: string[] = ["A", "B", "C"];
    private level: number = -1;
    private levels: WordVo2[] = [];
    private letterBoxes: LetterBox[] = [];
    private show:string="";

   // private target1: TargetBox;
  //  private target2:TargetBox;

    private clickLock:boolean=false;

    public word: string;
    private mainImage: GameObjects.Image;
  //  private mainText:GameObjects.Text;

    private gm: GM = GM.getInstance();

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("box", "./assets/box.png");
        this.load.image("paper","./assets/paper.png");

        this.levels = this.gm.levels2;

        for (let i: number = 0; i < this.levels.length; i++) {
            this.load.image(this.levels[i].word, "./assets/pics/" + this.levels[i].word + ".png");
        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);


        let bg:GameObjects.Image=this.add.image(0,0,"paper");
        Align.scaleToGameW(bg,1.1,this);
        Align.center(bg,this);

        //this.grid.showPos();     

        let instructions:GameObjects.Text=this.add.text(0,0,"Drag the missing vowel to the yellow box to spell the word in the picture",{"color":"#000000","fontSize":"26px"});
        this.grid.placeAt(2,1,instructions);

        //this.mainText=this.add.text(0,0,"WORD",{"color":"#000000","fontSize":"26px"}).setOrigin(0.5,0.5);
       // this.grid.placeAt(5.5,7,this.mainText);

        this.nextLevel();
    }

    nextLevel() {
        this.level++;
        if (this.level > this.levels.length - 1) {
           // console.log("Game Over");
            this.scene.start("SceneOver");
            return;
        }
        this.chars = this.levels[this.level].chars;
        this.word = this.levels[this.level].word;
        this.show=this.levels[this.level].show;

        if (this.mainImage) {
            this.mainImage.destroy();

        }
       /*  this.mainText.setText(this.word);
        this.mainText.visible=false; */

        this.mainImage = this.add.image(0, 0, this.word);
        
        Align.scaleToGameW(this.mainImage, 0.2, this);
        this.grid.placeAt(3, 5, this.mainImage);
        this.makeBoxes();
        this.clickLock=false;
    }
    
    hideBoxes() {
        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            this.letterBoxes[i].visible = false;
        }
    }
    makeBoxes() {
        this.hideBoxes();

        for (let i: number = 0; i < this.chars.length; i++) {
            let box: LetterBox;

            let char:string=this.chars[i]
            console.log(char);

            if (this.letterBoxes[i]) {
                box = this.letterBoxes[i];
                box.setLetter(char);
            }
            else {
                box = this.addLetter(this.chars[i]);
            }            
            let start:number=10-this.chars.length;
            this.grid.placeAt(i + start, 3, box);
            box.setScale(1,1);
            box.setPlace();
            box.setInteractive();
            box.visible = true;
        }
        
        for (let j:number=0;j<this.word.length;j++)
        {
            let tb:TargetBox3=this.targetBoxes[j];

            if (!tb)
            {
                tb=this.addTargetBox(0x00ff00);
                
            }
            
            if (j===this.word.length-1)
            {
                tb.setColor(0xff0000);
            }
            if (this.show.substr(j,1)=="-")
            {
                tb.setColor(0xffff00);
            }
            let start:number=10-this.word.length;
            tb.setCorrect(this.word.substr(j,1));
            tb.setText(this.show.substr(j,1));
            this.grid.placeAt(j+start,6,tb);
        }

        this.mixUp();

       /*  this.target1.correct = this.word.substr(0, 1).toLowerCase();
        this.target2.correct=this.word.substr(-1,1); */

        this.input.once("gameobjectdown", this.onDown.bind(this));
    }
    mixUp()
    {
        for (let i:number=0;i<50;i++)
        {
            let b1:number=Math.floor(Math.random()*this.letterBoxes.length);
            let b2:number=Math.floor(Math.random()*this.letterBoxes.length);

            let box1:LetterBox=this.letterBoxes[b1];
            let box2:LetterBox=this.letterBoxes[b2];

            let tx:number=box2.x;
            let ty:number=box2.y;

            box2.x=box1.x;
            box2.y=box1.y;

            box1.x=tx;
            box1.y=ty;

            box1.setPlace();
            box2.setPlace();
        }
    }
    addLetter(letter: string) {
        let box: LetterBox = new LetterBox(this, letter);
        this.letterBoxes.push(box);
        return box;
    }
    addTargetBox(color: number) {
        let targetBox: TargetBox3 = new TargetBox3(this, color);
        this.targetBoxes.push(targetBox);
        return targetBox;
    }
    onDown(p: Phaser.Input.Pointer, letterBox: LetterBox) {
        if (this.clickLock==true)
        {
            return;
        }
        this.dragBox = letterBox;
        this.children.bringToTop(letterBox);
        this.input.on("pointermove", this.onMove.bind(this));
        this.input.once("pointerup", this.onUp.bind(this));
    }
    onMove(p: Phaser.Input.Pointer) {
        this.dragBox.x = p.x;
        this.dragBox.y = p.y;
    }
    onUp() {
        this.input.off("pointermove");
        this.checkTargets();
        this.dragBox = null;
        this.input.once("gameobjectdown", this.onDown.bind(this));
    }
    doWinEffect() {
       // this.mainText.visible=true;

        setTimeout(() => {
            this.nextLevel();
        }, 2000);
    }
    checkAll()
    {
        for (let i:number=0;i<this.targetBoxes.length;i++)
        {
            let tb:TargetBox3=this.targetBoxes[i];
            console.log(tb.setLetter);
            console.log(tb.correct);

            if (tb.setLetter!==tb.correct)
            {
              return;
            }  
        }
        this.clickLock=true;
        this.doWinEffect();
    }
    checkTargets() {
        console.log(this.targetBoxes);
        console.log(this.dragBox);
        if (this.dragBox === null) {
            return;
        }

        for (let i: number = 0; i < this.targetBoxes.length; i++) {
            let targetBox: TargetBox3 = this.targetBoxes[i];

            let distY: number = Math.abs(this.dragBox.y - targetBox.y);
            let distX: number = Math.abs(this.dragBox.x - targetBox.x);

            if (distX < this.dragBox.displayWidth * 0.4 && distY < this.dragBox.displayHeight * 0.4) {
                this.dragBox.x = targetBox.x;
                this.dragBox.y = targetBox.y;
                if (this.dragBox.letter.toLowerCase() !== targetBox.correct) {
                    this.dragBox.snapBack();
                }
                else {
                    targetBox.setLetter=this.dragBox.letter;
                    this.dragBox.removeInteractive();
                    Align.scaleToGameW(this.dragBox,0.08,this);
                    this.checkAll();                 
                }
            }
        }
    }
}