import { GM } from "../classes/GM";
import { WordVo } from "../dataObjs/WordVo";
import { WordVo2 } from "../dataObjs/WordVo2";

export class SceneData extends Phaser.Scene
{
    constructor()
    {
        super("SceneData");
    }
    private gm:GM=GM.getInstance();
    create()
    {

        fetch("./assets/data.json")
        .then(response => response.json())
        .then(data => this.process(data));   
            
    }
    process(data:any)
    {
        console.log(data);
        data=data.data;
        let levels:WordVo2[]=[];

        for (let i:number=0;i<data.length;i++)
        {
            console.log(data[i]);
            let wordVo:WordVo2=new WordVo2(data[i].word,data[i].letters,data[i].show);
            levels.push(wordVo);
        }
        this.gm.levels2=levels;

        this.scene.start("SceneMain");
    }
}