import { GameObjects } from "phaser";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneOver extends BaseScene
{

    constructor()
    {
        super("SceneOver");
        this.makeGrid(11,11);
    }
    create()
    {
        super.create();
        let overText:GameObjects.Text=this.add.text(0,0,"Game Over",{"color":"#000000","fontSize":"30px"});
        Align.center(overText,this);
    }
}