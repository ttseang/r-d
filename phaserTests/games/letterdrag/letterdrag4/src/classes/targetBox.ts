import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { Align } from "../util/align";

export class TargetBox extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;

    public correct:string="";
    public setLetter:string="";
    private text1:GameObjects.Text;
    
    private back:GameObjects.Image;
    constructor(bscene:IBaseScene,color:number)
    {
        super(bscene.getScene())
        this.bscene=bscene;
        this.scene=bscene.getScene();
        
        this.back=this.scene.add.image(0,0,"box");
        Align.scaleToGameW(this.back,0.08,this.bscene);

        this.text1=this.scene.add.text(0,0,"x",{"fontSize":"50px","color":"#ff0000"}).setOrigin(0.5,0.5);
        this.text1.visible=false;

        let graphics:GameObjects.Graphics=this.scene.add.graphics();
        graphics.lineStyle(4,color);
        graphics.strokeRect(this.back.x-this.back.displayWidth/2,this.back.y-this.back.displayHeight/2,this.back.displayWidth,this.back.displayHeight);
        
        this.add(this.back);
        this.add(graphics);
        this.add(this.text1);

        this.scene.add.existing(this);
    }
    setCorrect(correct:string)
    {
        this.correct=correct;
        this.text1.setText(correct);
    }
   
}