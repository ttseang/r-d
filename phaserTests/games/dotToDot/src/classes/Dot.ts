import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";

export class Dot extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private num: number;
    private text1: GameObjects.Text;
    public index: number = 0;

    constructor(bscene: IBaseScene, index: number, num: number, textPos: number) {
        super(bscene.getScene());
        this.index = index;

        this.num = num;

        let back: GameObjects.Image = this.scene.add.image(0, 0, "dot");
        this.add(back);


        let positions: PosVo[] = [];
        //1 o'clock
        //0
        positions.push(new PosVo(25, 25));

        //3 o'clock
        //1
        positions.push(new PosVo(35, 0));

        //5 o'clock
        //2
        positions.push(new PosVo(35, 25));

        //6 o'clock
        //3
        positions.push(new PosVo(0, 25));

        //8 o'clock
        //4
        positions.push(new PosVo(-25, 25));

        //9 o'clock
        //5
        positions.push(new PosVo(-25, 0));

        //11 o'clock
        //6
        positions.push(new PosVo(-25, -25));

        //12 o'clock
        //7
        positions.push(new PosVo(0, -25));

        //8
        positions.push(new PosVo(-50, 0));
        //9
        positions.push(new PosVo(50, 0));

        //10
        positions.push(new PosVo(0, -35));

        //11
        positions.push(new PosVo(-35, -15));

         //12
         positions.push(new PosVo(35, -10));

        let posVo: PosVo = positions[textPos];

        this.text1 = this.scene.add.text(posVo.x, posVo.y, num.toString(), { color: "black", fontSize: "30px" });
        this.text1.setOrigin(0.5, 0.5);
     //   this.text1.setScale(0.9,0.9);

        this.add(this.text1);

        //  this.alpha=.5;
        this.setSize(back.displayWidth, back.displayHeight);
        this.setInteractive();

    }
    public setBig()
    {
        this.text1.setFontStyle("bold");
     //   this.text1.setFontSize(40);
        this.scene.tweens.add({targets:this.text1,duration:500,scaleX:1.5,scaleY:1.5,ease:'Back',easeParams:[10]})
    }
    public setSmall()
    {
        this.text1.setFontStyle("normal");
        //this.text1.setFontSize(30);
        this.scene.tweens.add({targets:this.text1,duration:500,scaleX:1,scaleY:1})
    }
}