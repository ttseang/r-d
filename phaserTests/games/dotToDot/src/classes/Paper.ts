import { GameObjects } from "phaser";
import { DotVo } from "../dataObjs/DotVo";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import { AlignGrid } from "../util/alignGrid";
import { Dot } from "./Dot";
import { GM } from "./GM";

export class Paper extends Phaser.GameObjects.Container
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    private back:GameObjects.Sprite;
    private gm:GM=GM.getInstance();

    constructor(bscene:IBaseScene,key:string)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.scene=bscene.getScene();

      

        this.back=this.scene.add.sprite(0,0,key).setOrigin(0,0);
        this.back.setFrame("10.png");
        this.add(this.back);
        
        this.setSize(this.back.displayWidth,this.back.displayHeight);

       /*  this.back.setInteractive();

        this.back.on('pointerdown',this.showPos.bind(this)); */

        this.scene.add.existing(this);
    }
    showPos(p:Phaser.Input.Pointer)
    {
        let px:number=Math.floor(p.x-this.x);
        let py:number=Math.floor(p.y-this.y);
        let pString:string="dots.push(new PosVo("+px.toString()+","+py.toString()+"));";
        console.log(pString);
        //console.log(p.x-this.x,p.y-this.y);
    }
    setIndex(index:number)
    {
        let frame:number=11-index;
        let frameName:string=frame.toString()+".png";
        
        this.back.setFrame(frameName);
    }
    placeDots(dots:DotVo[])
    {
        for(let i:number=0;i<dots.length;i++)
        {
          //  let dot:GameObjects.Image=this.scene.add.image(0,0,"dot");
            let dot:Dot=new Dot(this.bscene,i,i+21,dots[i].textPos);
            dot.x=dots[i].x;
            dot.y=dots[i].y;
            dot.displayWidth=25;
            dot.displayHeight=25;
            this.gm.dots.push(dot);
            this.add(dot);
        }
    }
}