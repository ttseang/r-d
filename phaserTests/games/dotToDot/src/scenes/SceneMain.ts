import { GameObjects } from "phaser";
import { Dot } from "../classes/Dot";
import { GM } from "../classes/GM";
import { Paper } from "../classes/Paper";
import { DotVo } from "../dataObjs/DotVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private paper:Paper
    private dotIndex:number=-1;
    private gm:GM=GM.getInstance();
    private lastDot:Dot | null =null;

    constructor() {
        super("SceneMain");
    }
    preload() {
      
        this.load.atlas("fish","./assets/fish.png","./assets/fish.json");
        this.load.image("frame","./assets/frame.jpg");
        this.load.image("dot","./assets/dot.png");

        this.load.audio("win","./assets/audio/win.wav");

        for(let i:number=0;i<9;i++)
        {
            let soundPath:string=i.toString()+".mp3";
            this.load.audio("note"+i.toString(),"./assets/audio/"+soundPath);
        }
    }
    create() {
        super.create();
        this.makeGrid(22,22);
       // this.grid.showPos();

        this.paper=new Paper(this,"fish");

      //  Align.scaleToGameW(this.paper,0.25,this);
        Align.center2(this.paper,this);
        
        let frame:GameObjects.Image=this.add.image(0,0,"frame");
       
        frame.displayHeight=this.paper.displayHeight*1.5;
        frame.displayWidth=this.paper.displayWidth*1.5;
        Align.center(frame,this);

        this.children.sendToBack(frame);

        let dotPositions:DotVo[]=[];
        //21
        dotPositions.push(new DotVo(35,365,6));
        dotPositions.push(new DotVo(52,244,8));
        dotPositions.push(new DotVo(68,186,10));
        dotPositions.push(new DotVo(120,139,6));
        //25
        dotPositions.push(new DotVo(155,83,6));
        //26
        dotPositions.push(new DotVo(225,29,11));
        dotPositions.push(new DotVo(362,91,12));
        dotPositions.push(new DotVo(443,143,1));
        dotPositions.push(new DotVo(463,208,1));
        dotPositions.push(new DotVo(432,307,1));
        this.paper.placeDots(dotPositions);

        this.input.on("gameobjectdown",this.dotClicked.bind(this));

        this.upCount();
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    upCount()
    {
        if (this.lastDot)
        {
            this.lastDot.setSmall();
        }
        this.dotIndex++;

       

        let currentDot:Dot=this.gm.dots[this.dotIndex];
        if (currentDot)
        {
            currentDot.setBig();
            this.lastDot=currentDot;
        }
        else
        {
            setTimeout(() => {
                this.paper.setIndex(0);
                for (let i:number=0;i<this.gm.dots.length;i++)
                {
                    this.gm.dots[i].visible=false;
                }
                this.playSound("win");
            }, 2000);
           
        }

         
        if (this.dotIndex>0)
        {
            this.paper.setIndex(this.dotIndex);
        }
       
       
    }
    dotClicked(p:Phaser.Input.Pointer,dot:Dot)
    {
        if (dot instanceof GameObjects.Image)
        {
            return;
        }
        if (dot.index!==this.dotIndex)
        {
            return;
        }
        let noteIndex:number=this.dotIndex;
        if (noteIndex>8)
        {
            noteIndex-=8;
        }
        this.playSound("note"+noteIndex.toString());
        //dot.alpha=.2;

        this.upCount();
       
    }
}