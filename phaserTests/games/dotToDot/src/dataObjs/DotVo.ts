export class DotVo
{
    public x:number;
    public y:number;
    public textPos:number;

    constructor(x:number,y:number,textPos:number)
    {
        this.x=x;
        this.y=y;
        this.textPos=textPos;
    }
}