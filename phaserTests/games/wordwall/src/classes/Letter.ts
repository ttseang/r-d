import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class Letter extends Phaser.GameObjects.Sprite
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    public char:string;

    constructor(bscene:IBaseScene,char:string)
    {
        super(bscene.getScene(),0,0,"letters");
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.char=char;
        this.scene.add.existing(this);

        let frame:string="letters_"+char+".png";
        this.setFrame(frame);
        Align.scaleToGameW(this,0.08,this.bscene);
    }

}