import { Letter } from "../classes/Letter";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.atlas("letters","./assets/letters.png","./assets/letters.json");

    }
    create() {
        super.create();
        this.makeGrid(11,11);

        console.log("create!");
        
        let chars:string="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for (let j:number=0;j<20;j++)
        {
        for (let i:number=0;i<9;i++)
        {

            let letter:Letter=new Letter(this,"C");
            this.grid.placeAt(i,j,letter);
        }
    }
    }
}