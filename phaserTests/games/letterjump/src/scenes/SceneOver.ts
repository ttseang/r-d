import { Button } from "../classes/comps/Button";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneOver extends BaseScene
{
    constructor()
    {
        super("SceneOver");
    }
    create()
    {
        super.create();
        let btnPlay:Button=new Button(this,"button","Play Again");
        Align.center(btnPlay,this);
        btnPlay.setCallback(()=>{window.location.reload()})
    }
}