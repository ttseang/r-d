import { GameObjects, Sound } from "phaser";
import { NoiseBlock } from "../classes/comps/NoiseBlock";
import { ColorBurst } from "../classes/effects/colorBurst";
import { LetterBox } from "../classes/LetterBox";
import { Align } from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private tiles: LetterBox[] = [];
    public tileGrid: string[][] = [[]];
    private clickLock: boolean = false;
    private moving: boolean = false;
    // private usedWords: string[] = [];
    private letters: string[] = "asdfgrijoxcvwmiu".split("");
    private letterScale: number = 0.05;
    private tw: number = -1;
    private th: number = -1;
    private rowIndex: number = 3;

    public pickwords: string[] = [];
    private selected: string = "";
    private correct: string = "";

    private sq: GameObjects.Image;
    private wordText: GameObjects.Text;
    private player: GameObjects.Sprite;
    //
    //
    //
    private playerY: number = 0;
    private playerX: number = 0;
    //
    //
    //
    private topY: number = 0;

    private room: GameObjects.Container;
    private room2: GameObjects.Container;

    private bgMusic: Sound.BaseSound;

    private particles: GameObjects.Particles.ParticleEmitterManager;

    private lava: GameObjects.TileSprite;

    private noiseBlock: NoiseBlock;
    private noiseBlock2:NoiseBlock;


    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("white", "./assets/white.jpg");
        this.load.image("lava", "./assets/lava3.png");
        //this.load.image("lavatile", "./assets/lava2.jpg");
        this.load.image("coin", "./assets/coin.png");
        this.load.image("button", "./assets/ui/buttons/1/3.png");
        this.load.atlas("runner", "./assets/runner.png", "./assets/runner.json")

        this.load.image("floor", "./assets/floor.png");
        this.load.image("mite", "./assets/mite.png");
        this.load.image("skull", "./assets/skull.png");
        //  this.load.image("bg", "./assets/bg.jpg");

        this.load.text("words", "./assets/data/fourletterwords.txt");

        //  this.load.audio("coins", "./assets/audio/sfx/coins.mp3");
        this.load.audio("newlevel", "./assets/audio/sfx/newlevel.wav");
        this.load.audio("scream", "./assets/audio/sfx/scream.mp3");
        this.load.audio("swish", "./assets/audio/sfx/swish.wav");
        this.load.audio("bgm", "./assets/audio/background/littleTown.mp3")

        this.load.atlas("flares", "./assets/effects/flares.png", "./assets/effects/flares.json");
        this.load.spritesheet("exp", "./assets/explosion.jpg", { frameWidth: 18, frameHeight: 16 });

        ColorBurst.preload(this);


    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        //this.grid.showPos();


        this.room = this.add.container();
        this.room2 = this.add.container();
        this.rowIndex = 3;

        //this.lava=this.add.tileSprite(0,0,this.gw,this.gh,"lavatile").setOrigin(0,0);

        this.tiles = [];
        for (let i = 0; i < 4; i++) {
            this.tileGrid[i] = [];
        }

        /*  this.sq = this.add.image(0, 0, "lava");
         Align.scaleToGameW(this.sq, 0.4, this);
         Align.center(this.sq, this); */

        this.wordText = this.add.text(0, 0, "WORD", { "fontSize": "36px", "color": "#ffffff", "fontFamily": "Arial" }).setOrigin(0.5, 0.5);
        this.grid.placeAt(5, 0, this.wordText);

        this.pickwords = this.cache.text.get("words").split(" ");

        this.getRandomLetters();
        this.makeTileGrid();
        this.offTiles();
        this.positionTiles();
        this.setTileValues();
        this.fallInPlace();
        this.input.on("gameobjectdown", this.clickTile.bind(this));

        this.makeAnims();

        //   this.lava=this.add.tileSprite(0,0,this.gw,this.gh,"lava").setOrigin(0,0);

        //   this.fillItem(0,2,11,9,"lava",this.room);
        this.fillItem(0, 9, 11, 11, "floor", this.room);
        this.fillItem(0, 0, 11, 1, "floor", this.room);
        this.placeItem("mite", 2, 9, this.room);
        //  this.placeItem("mite",9,0,this.room);
        this.placeItem("skull", 9, 9, this.room);

        // this.fillItem(0,2,11,9,"lava",this.room2);
        this.fillItem(0, 9, 11, 11, "floor", this.room2);
        this.fillItem(0, 0, 11, 1, "floor", this.room2);
        this.placeItem("mite", 2, 9, this.room2);
        // this.placeItem("mite",9,0,this.room2);
        this.placeItem("skull", 9, 9, this.room2);
        this.room2.y = -this.gh;



        //this.fillBack(this.room);
        //  this.fillBack(this.room2);

        this.player = this.add.sprite(0, 0, "runner").setOrigin(0.5, 1);

        this.grid.placeAt(4, 1, this.player);
        this.topY = this.player.y;
        this.grid.placeAt(3, 9, this.player);
        //
        //
        //
        this.playerY = this.player.y;
        this.playerX = this.player.x;
        //
        //
        //
        Align.scaleToGameW(this.player, 0.07, this);
        this.player.play("idle");


        this.bgMusic = this.sound.add("bgm", { loop: true, volume: 0.6 });
        this.bgMusic.play();

        this.noiseBlock = new NoiseBlock(this);
        this.noiseBlock.fill();

        this.noiseBlock2=new NoiseBlock(this);
        this.noiseBlock2.copyFrom(this.noiseBlock);

        this.grid.placeAt(-1,1,this.noiseBlock);
        this.grid.placeAt(-1,1,this.noiseBlock2);

        this.children.sendToBack(this.noiseBlock);
        this.children.sendToBack(this.noiseBlock2);

        this.room.add(this.noiseBlock);
        this.room2.add(this.noiseBlock2);

          this.particles=this.add.particles("flares");
         let emitter=this.particles.createEmitter({
             frame: [ 'red'],
         x: this.gw/2,
         y: this.gh/2,
         lifespan: 2000,
         angle: { min: 0, max: 720 },
         speed: { min: 200, max: 400 },
         scale: { start: 0.8, end: 0 },
         tint:0xff0000,
         gravityY:10,
         gravityX:10,
         bounce: 0.9,
         alpha:0.4,
         bounds: { x: 0, y: 0, w: this.gw, h: this.gh },
         collideTop: false,
         collideBottom: false,
         blendMode: 'ADD'
         });

        //this.tweens.add({targets: this.lava,duration: 500,alpha:0.8,yoyo:true,repeat:-1);

      
    }
    fillBack(location: GameObjects.Container) {
        let xx: number = 0;
        let yy: number = 0;

        for (let i: number = 0; i < 100; i++) {
            let lavaTile: GameObjects.Image = this.add.image(xx, yy, "lava");
            Align.scaleToGameW(lavaTile, 0.1, this);
            xx += lavaTile.displayWidth;
            if (xx > this.gw) {
                xx = 0;
                yy += lavaTile.displayHeight;
            }

            location.add(lavaTile);
            location.sendToBack(lavaTile);
        }

    }
    fillItem(x1: number, y1: number, x2: number, y2: number, key: string, place: GameObjects.Container) {
        for (let i: number = x1; i < x2; i++) {
            for (let j: number = y1; j < y2; j++) {
                this.placeItem(key, i, j, place);
            }
        }
    }
    placeItem(key: string, xx: number, yy: number, place: GameObjects.Container) {
        let obj: GameObjects.Sprite = this.placeImage2(key, xx, yy, 0.1);
        place.add(obj);
        place.bringToTop(obj);
    }
    makeAnims() {
        let idleFrames = this.anims.generateFrameNames('runner', { start: 0, end: 9, zeroPad: 3, prefix: 'Idle__', suffix: '.png' });
        let jumpFrame = this.anims.generateFrameNames('runner', { start: 0, end: 9, zeroPad: 3, prefix: 'Jump__', suffix: '.png' })
        let runFrame = this.anims.generateFrameNames('runner', { start: 0, end: 9, zeroPad: 3, prefix: 'Run__', suffix: '.png' })


        this.anims.create({
            key: 'run',
            frames: runFrame,
            frameRate: 32,
            repeat: 0
        });

        this.anims.create({
            key: 'jump',
            frames: jumpFrame,
            frameRate: 32,
            repeat: 0
        });

        this.anims.create({
            key: 'idle',
            frames: idleFrames,
            frameRate: 8,
            repeat: -1
        });
    }
    offTiles(): void {
        for (let i = 0; i < this.tiles.length; i++) {
            this.tiles[i].visible = false;
        }
    }
    clickTile(p: Phaser.Input.Pointer, obj: LetterBox) {
        if (this.clickLock == true) {
            return;
        }
        if (obj instanceof (GameObjects.Image)) {
            return;
        }
        this.clickLock = true;
        console.log(obj.yy);

        if (this.rowIndex != obj.yy) {
            for (let i = 0; i < this.tiles.length; i++) {
                if (this.tiles[i].yy == this.rowIndex) {
                    this.tiles[i].highlight();
                }
                this.clickLock = false;
            }
            return;
        }

        //  this.fallTiles(obj);
        this.jumpToTile(obj);
        this.selected += obj.letter;
        this.wordText.setText(this.selected);
        this.rowIndex--;
        if (this.rowIndex < 0) {
            setTimeout(() => {
                this.checkWord();
            }, 1000);

        }
        else {
            this.clickLock = false;
        }
    }
    jumpToTile(obj: LetterBox) {
        this.player.play("jump");
        this.playSound("swish");
        if (this.player.x < obj.x) {
            this.player.flipX = false;
        }
        else {
            this.player.flipX = true;
        }
        this.tweens.add({ targets: this.player, duration: 250, y: obj.y + obj.displayHeight / 2, x: obj.x, onComplete: () => { this.fallTiles(obj) } });
    }
    fallTiles(obj: LetterBox) {
        this.player.play("idle");
        for (let i: number = 0; i < 4; i++) {
            let index: number = (obj.yy) * 4 + i;
            console.log(index);
            let tile: LetterBox = this.tiles[index];
            if (tile != obj) {
                //tile.visible=false;
                tile.fall();
            }

        }
    }
    moveTilesDown() {
        for (let i = 0; i < this.tiles.length; i++) {
            if (this.tiles[i].visible == true) {
                this.tiles[i].moveOffDown();
            }
        }

        this.tweens.add({ targets: this.room, duration: 1000, y: this.gh });
        this.tweens.add({ targets: this.room2, duration: 1000, y: 0 });
      //  let scrollTarg: number = this.lava.tilePositionY - 1000;

      //  this.tweens.add({ targets: this.lava, tilePositionY: scrollTarg, duration: 1000 })


        this.player.flipX = true;
        this.tweens.add({ targets: this.player, duration: 1000, y: this.playerY, x: this.playerX });
        this.player.play("run");

        this.moving = true;

        setTimeout(() => {
            this.resetLevel();
            this.room.y = 0;
            this.room2.y = -this.gh;
            this.player.flipX = false;
            this.player.play("idle");
            this.moving = false;
        }, 2000);
    }
    resetLevel() {
        this.getRandomLetters();
        this.setTileValues();
        this.fallInPlace();
        this.rowIndex = 3;
        this.selected = "";
        this.clickLock = false;

    }
    checkWord() {
        if (this.pickwords.includes(this.selected)) {
            console.log("correct");
            this.playSound("newlevel");
            this.player.play('jump');
            this.tweens.add({ targets: this.player, duration: 200, y: this.topY });

            setTimeout(() => {
                this.player.play("idle");
            }, 400);
            setTimeout(() => {
                this.moveTilesDown();
                this.wordText.setText("");
            }, 1500);

        }
        else {
            console.log("wrong");
            this.wordText.setText("Correct Word is " + this.correct);
            this.playSound("scream");
            for (let i: number = 0; i < this.tiles.length; i++) {
                this.tiles[i].shrink();
            }
            this.player.play("jump");
            this.tweens.add({ targets: this.player, duration: 1000, scaleX: 0, scaleY: 0 });

            setTimeout(() => {
                this.scene.start("SceneOver");
            }, 2000);
        }
    }
    fallInPlace() {
        for (let i = 0; i < this.tiles.length; i++) {
            this.tiles[i].riseInPlace();
        }
    }
    getRandomLetters() {
        let alphabet: string = "abcdefghijklmnopqrstuvwxyz";
        let letters: string[] = [];
        for (let i: number = 0; i < 16; i++) {
            let r: number = Math.floor(Math.random() * alphabet.length);
            let letter = alphabet.substr(r, 1);
            letters.push(letter);
        }
        this.letters = letters;
        this.getRandomWord();
    }
    getRandomWord() {
        let r: number = Math.floor(Math.random() * this.pickwords.length);
        let word: string = this.pickwords[r];
        this.correct = word;
        console.log(word);
        word = word.split("").reverse().join("");

        for (let i = 0; i < 4; i++) {
            let col: number = Math.floor(Math.random() * 4);
            let index: number = (i * 4) + col;
            this.letters[index] = word.substr(i, 1);
        }
        console.log(this.letters);
    }
    playSound(sound: string) {
        let audio: Phaser.Sound.BaseSound = this.sound.add(sound);
        audio.play();
    }
    setTileValues() {
        console.log(this.letters);
        let index: number = 0;
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {

                let letter: string = this.letters.shift();
                console.log(letter);
                this.tileGrid[i][j] = letter;
                this.tiles[index].setLetter(letter);
                index++;
            }
        }
    }
    makeTileGrid(): void {
        let xx: number = 0;
        let yy: number = 0;
        let tile: LetterBox;

        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {



                tile = new LetterBox(this, "A", this.letterScale);
                this.tiles.push(tile);
                //  this.tileGrid[i][j] = letter;
                tile.x = xx;
                tile.y = yy;
                tile.xx = j;
                tile.yy = i;
                //
                //
                //
                xx += tile.displayWidth;

                if (this.tw === -1) {
                    this.tw = tile.displayWidth;
                }
                if (this.th === -1) {
                    this.th = tile.displayHeight;
                }
            }
            yy += tile.displayHeight;
            xx = 0;
        }

    }

    positionTiles() {

        this.tw = this.tiles[0].displayWidth;
        this.th = this.tiles[0].displayHeight;

        let ww: number = 0;
        let hh: number = 0;

        let startX: number = this.gw / 2 - (this.tw * 4) / 2 - this.tw / 2;
        let startY: number = this.gh / 2.5 - (this.th * 4) / 2 + this.th / 2;

        let xx: number = 3.5;
        let yy: number = 2.5;

        for (let i: number = 0; i < this.tiles.length; i++) {
            let tile: LetterBox = this.tiles[i];
            this.grid.placeAt(xx, yy, tile);
            tile.setPlace();
            xx++;
            if (xx > 7) {
                yy += 1.5;
                xx = 3.5;
            }
            /*  tile.x = startX + tile.xx * this.tw*1.25;
             tile.y = startY + tile.yy * this.th*1.25;   
             
             ww=tile.x-startX+this.tw;
             hh=tile.y-startY+this.th; */
        }

        /*  this.sq.x=startX-this.tw;
         this.sq.y=startY-this.th;
         this.sq.displayWidth=ww+this.tw;
         this.sq.displayHeight=hh+this.th; */

    }
    update() {
        /*  if (this.moving==true)
         {
             this.lava.tilePositionY-=2;
         } */

    }
}