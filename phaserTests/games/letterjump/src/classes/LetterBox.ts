import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class LetterBox extends Phaser.GameObjects.Container
{
    public letter:string;
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private back:GameObjects.Image;
    private text1:GameObjects.Text;
    private graphics:GameObjects.Graphics;
    public xx:number=0;
    public yy:number=0;
    public ox:number=0;
    public oy:number=0;
    public oscaleX:number=0;
    public oscaleY:number=0;

    constructor(bscene:IBaseScene,letter:string,scale:number)
    {
        super(bscene.getScene());
        this.letter=letter;
        this.bscene=bscene;

        this.back=this.scene.add.image(0,0,"white");
        this.add(this.back);

       // this.back.setTint(0x4A86E8);

        Align.scaleToGameW(this.back,scale,this.bscene);

        let fs:number =this.back.displayWidth*0.9;
        this.text1=this.scene.add.text(0,0,letter,{"fontFamily":"Arial","fontSize":fs.toString()+"px","color":"#000000"}).setOrigin(0.5, 0.5);
        this.add(this.text1);

        this.graphics=this.scene.add.graphics();
        this.add(this.graphics);
        
        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.setInteractive();
        this.scene.add.existing(this);
        
    }
    setPlace()
    {
        this.ox=this.x;
        this.oy=this.y;
        this.oscaleY = this.scaleY;
        this.oscaleX = this.scaleX;
    }
    setOriginialPlace()
    {
        this.x=this.ox;
        this.y=this.oy;
        this.scaleX=this.oscaleX;
        this.scaleY = this.oscaleY;

    }
    setLetter(letter:string) {
        this.letter=letter;
        this.text1.setText(letter);
    }
    setFlippedScale(scale:number)
    {
        console.log("setFlippedScale="+scale);
        Align.scaleToGameW(this.back,scale,this.bscene);

        let fs:number =this.back.displayWidth;
        this.text1.setFontSize(fs);
        this.setSize(this.back.displayWidth,this.back.displayHeight);
    }
    showSelected() {
        this.graphics.clear();
        this.graphics.lineStyle(8, 0xff0000);
        this.graphics.strokeRect(-this.back.displayWidth/2,-this.back.displayHeight/2,this.back.displayWidth,this.back.displayHeight);

    }
    highlight() {
        this.angle=-10;
        this.scene.tweens.add({targets: this,duration: 500,scaleX:this.oscaleX*1.1,scaleY:this.oscaleY*1.1});
        setTimeout(() => {
            this.scaleX=this.oscaleX;
            this.scaleY = this.oscaleY;
            this.angle=0;
        }, 800);
    }
    clearSelected() {
        this.graphics.clear();
    }
    fallInPlace() {
        this.x=this.ox;
        this.y=this.oy-1000;
        this.scaleX=this.oscaleX;
        this.scaleY = this.oscaleY;
        this.visible=true;
        this.scene.tweens.add({targets: this,duration: 1000,y:this.oy});
    }
    riseInPlace()
    {
        this.x=this.ox;
        this.y=this.oy;
        this.scaleX=0;
        this.scaleY =0;
        this.visible=true;
        this.scene.tweens.add({targets: this,duration: 1000,scaleX:this.oscaleX,scaleY:this.oscaleY});
    }
    shrink()
    {
        this.scene.tweens.add({targets: this,duration: 1000,scaleX:0,scaleY:0});
    }
    fall() {
        this.scene.tweens.add({targets: this,duration: 1000,scaleX:0,scaleY:0});
    }
    moveOffDown() {
        this.scene.tweens.add({targets: this,duration: 1000,y:this.oy+1000});
    }
}