import { GameObjects } from "phaser";
import Noise from "phaser3-rex-plugins/plugins/utils/math/noise/Perlin";
import IBaseScene from "../../interfaces/IBaseScene";

export class NoiseBlock extends GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    public blocks:GameObjects.Sprite[]=[];

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.bscene=bscene;
        this.scene.add.existing(this);
    }
    fill()
    {
        let noiseObj:any = this.scene.plugins.get('rexPerlin');
        let noise:Noise=noiseObj.add(Math.random()*100);
        
        let cols:number=(this.bscene.getW()/18)+2;
        let rows:number=this.bscene.getH()*0.65/16;

        for (let i:number=0;i<cols;i++)
        {
            for (let j:number=0;j<rows;j++)
            {
                let p=noise.perlin2(i/10,j/10);
                p=Math.floor(p*10);

              // console.log(p);
                
                let frame:number=9+p;
               // console.log(frame);
                let img:GameObjects.Sprite=this.scene.add.sprite(i*18,j*16,"exp");
                this.blocks.push(img);
              //  this.tiles.push(img);
                img.setFrame(frame);
                this.add(img);
            }
        }
    }
    copyFrom(n:NoiseBlock)
    {
       // console.log(n);

        let len:number=n.blocks.length;
        for (let i:number=0;i<len;i++)
        {
            let block:GameObjects.Sprite=n.blocks[i];

            let img:GameObjects.Sprite=this.scene.add.sprite(block.x,block.y,"exp");
            img.setFrame(block.frame.name);
            this.blocks.push(img);
            this.add(img);
        }
    }

}