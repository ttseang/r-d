import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";

export class Face extends GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private back:GameObjects.Image;
    private faceGrid:AlignGrid;
    //
    //
    //
    private rightEye:GameObjects.Image;
    private leftEye:GameObjects.Image;
    //
    //
    //
    private rightPos:PosVo=new PosVo(0,0);
    private leftPos:PosVo=new PosVo(0,0);

    private closedRight:GameObjects.Image;
    private closedLeft:GameObjects.Image;
    //
    //
    //
    private mouth:GameObjects.Sprite;
    //
    //
    //
    private hand1:GameObjects.Image;
    private hand2:GameObjects.Image;

    constructor(bscene:IBaseScene)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.bscene=bscene;

        this.back=this.scene.add.image(0,0,"face").setOrigin(0,0);
        Align.scaleToGameW(this.back,0.3,this.bscene);
        this.add(this.back);
        //
        //
        //
        this.rightEye=this.scene.add.image(0,0,"eye");
        Align.scaleToGameW(this.rightEye,0.025,this.bscene);
        this.add(this.rightEye);
        //
        //
        //
        this.leftEye=this.scene.add.image(0,0,"eye");
        Align.scaleToGameW(this.leftEye,0.025,this.bscene);
        this.add(this.leftEye);
        //
        //
        //
        this.faceGrid=new AlignGrid(this.bscene,44,44,this.back.displayWidth,this.back.displayHeight);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

       // this.faceGrid.showNumbers();

        this.faceGrid.placeAt(15,20,this.rightEye);
        this.faceGrid.placeAt(28,20,this.leftEye);

        this.scene.add.existing(this);

        this.rightPos=new PosVo(this.rightEye.x,this.rightEye.y);
        this.leftPos=new PosVo(this.leftEye.x,this.leftEye.y);
        //
        //
        //
        this.closedLeft=this.scene.add.image(this.leftPos.x,this.leftPos.y,"closed");
        this.closedRight=this.scene.add.image(this.rightPos.x,this.rightPos.y,"closed");
        
        Align.scaleToGameW(this.closedRight,0.055,this.bscene);
        Align.scaleToGameW(this.closedLeft,0.055,this.bscene);

        this.closedRight.visible=false;
        this.closedLeft.visible=false;

        this.add(this.closedLeft);
        this.add(this.closedRight);
        //
        //
        //
        this.mouth=this.scene.add.sprite(0,0,"mouth");
        Align.scaleToGameW(this.mouth,0.05,this.bscene);
        this.faceGrid.placeAt(22,30,this.mouth);
        this.add(this.mouth);

        this.hand1=this.scene.add.image(0,0,"hand1");
        this.hand2=this.scene.add.image(0,0,"hand2");

        Align.scaleToGameW(this.hand1,0.125,this.bscene);
        Align.scaleToGameW(this.hand2,0.125,this.bscene);

        this.faceGrid.placeAt(7,39,this.hand1);
        this.faceGrid.placeAt(40,40,this.hand2);
        
        this.add(this.hand1);
        this.add(this.hand2);

        this.hand1.visible=false;
        this.hand2.visible=false;
        
        setInterval(this.blink.bind(this),3000);
    }
    blink()
    {
        

        this.closedRight.visible=true;
        this.closedLeft.visible=true;
        setTimeout(() => {
            this.closedRight.visible=false;
        this.closedLeft.visible=false;
        }, 100);
    }
    public lookAt(xx:number,yy:number)
    {
        let distMod:number=50;

        let distXRight:number=(this.rightPos.x-xx)/distMod;
        let distYRight:number=(this.rightPos.y-yy)/distMod;
        //
        //
        //
        let distXLeft:number=(this.leftPos.x-xx)/distMod;
        let distYLeft:number=(this.leftPos.y-yy)/distMod;
        //
        //
        //
        let maxX2:number=this.getSgn(distXRight)*(this.faceGrid.cw);
        if (Math.abs(distXRight)>Math.abs(maxX2))
        {
            distXRight=maxX2;
        }
        let maxY2:number=this.getSgn(distYLeft)*(this.faceGrid.ch);
        if (Math.abs(distYLeft)>Math.abs(maxY2))
        {
            distYRight=maxY2;
        }
        //
        //
        //
        let rightX:number=this.rightPos.x-distXRight;
        let rightY:number=this.rightPos.y-distYRight;

        this.rightEye.x=rightX;
        this.rightEye.y=rightY;
        //
        //
        //

        let maxX:number=this.getSgn(distXLeft)*(this.faceGrid.cw);
        if (Math.abs(distXLeft)>Math.abs(maxX))
        {
            distXLeft=maxX;
        }
        let maxY:number=this.getSgn(distYLeft)*(this.faceGrid.ch);
        if (Math.abs(distYLeft)>Math.abs(maxY))
        {
            distYLeft=maxY;
        }
        let leftX:number=this.leftPos.x-distXLeft;
        let leftY:number=this.leftPos.y-distYLeft;
        

        this.leftEye.x=leftX;
        this.leftEye.y=leftY;

        


    }
    public setMouth(index:number)
    {
        let f:string="m"+index.toString()+".png";
        this.mouth.setFrame(f);
    }
    public randMouth()
    {
        let r:number=Math.floor(Math.random()*6)+1;
        let f:string="m"+r.toString()+".png";
        this.mouth.setFrame(f);
    }
    private getSgn(num:number)
    {
        if (num===0)
        {
            return 0;
        }
        if (num<-1)
        {
            return -1;
        }
        return 1;
    }
}