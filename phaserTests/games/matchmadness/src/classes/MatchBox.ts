import { GameObjects } from "phaser"
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class MatchBox extends GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private back:GameObjects.Image;
    private icon:GameObjects.Sprite;
    private cover:GameObjects.Image;
    private callback:Function;
    public index:number=0;

    constructor(bscene:IBaseScene,index:number,callback:Function)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.scene=bscene.getScene();

        this.callback=callback;
        this.index=index;
        //
        //
        //
        this.back=this.scene.add.image(0,0,"square");
        this.icon=this.scene.add.sprite(0,0,"icons");
        this.cover=this.scene.add.sprite(0,0,"square");

        let scale:number=0.08;

        Align.scaleToGameW(this.back,scale,this.bscene);
        Align.scaleToGameW(this.icon,scale,this.bscene);
        Align.scaleToGameW(this.cover,scale,this.bscene);

        

      //  this.cover.alpha=0.5;

        this.add(this.back);
        this.add(this.icon);
        this.add(this.cover);

        let f:string=(index+1).toString()+".png";
        this.icon.setFrame(f);

        this.scene.add.existing(this);
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
        
        this.on('pointerdown',()=>{this.callback(this)});

    }
    show()
    {
        this.scene.tweens.add({targets: this.cover,duration: 500,alpha:0});
    }
    hide()
    {
        this.scene.tweens.add({targets: this.cover,duration: 500,alpha:1});
    }
}