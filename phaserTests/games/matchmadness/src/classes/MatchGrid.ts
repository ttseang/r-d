import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import { MatchBox } from "./MatchBox";

export class MatchGrid {
    private scene: Phaser.Scene;
    private bscene: IBaseScene;
    private pos: PosVo[] = [];
    private clickLock: boolean = false;
    private firstBox: MatchBox | null = null;
    private secondBox: MatchBox | null = null;
    private boxes:MatchBox[]=[];

    public correctCallback:Function=()=>{};
    public wrongCallback:Function=()=>{};
    public resetCallback:Function=()=>{};
    public selectCallback:Function=()=>{};

    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
        this.scene = bscene.getScene();


        this.pos.push(new PosVo(2, 2));
        this.pos.push(new PosVo(2, 4));
        this.pos.push(new PosVo(2, 6));
        this.pos.push(new PosVo(2, 8));
        this.pos.push(new PosVo(5, 1));
        this.pos.push(new PosVo(5, 9));
        this.pos.push(new PosVo(8, 2));
        this.pos.push(new PosVo(8, 4));
        this.pos.push(new PosVo(8, 6));
        this.pos.push(new PosVo(8, 8));

    }
    makeBoxes() {
        let index: number = 0;

        for (let i: number = 0; i < this.pos.length; i++) {
            let matchBox: MatchBox = new MatchBox(this.bscene, index, this.selectBox.bind(this));
            this.boxes.push(matchBox);

            if (i / 2 !== Math.floor(i / 2)) {
                index++;
            }
            this.bscene.getGrid().placeAt(this.pos[i].x, this.pos[i].y, matchBox);
        }
        this.mixUp();
    }

    mixUp()
    {
        for (let i:number=0;i<10;i++)
        {
            let r:number=Math.floor(Math.random()*this.boxes.length);
            let r2:number=Math.floor(Math.random()*this.boxes.length);
            console.log(r,r2);

            let box1:MatchBox=this.boxes[r];
            let box2:MatchBox=this.boxes[r2];

            let pos1:PosVo=new PosVo(box1.x,box1.y);
            let pos2:PosVo=new PosVo(box2.x,box2.y);

            box1.x=pos2.x;
            box1.y=pos2.y;
            box2.x=pos1.x;
            box2.y=pos1.y;
        }
    }
    selectBox(box: MatchBox) {
        if (this.clickLock==true)
        {
            return;
        }
        if (this.firstBox===box)
        {
            return;
        }
        this.clickLock=true;

        this.selectCallback(new PosVo(box.x,box.y));

        if (this.firstBox == null) {
            box.show();
            this.firstBox = box;
            this.clickLock=false;
            return;
        }
        this.secondBox = box;
        box.show();

        setTimeout(() => {
            this.checkMatch();
        }, 1000);
    }
    checkMatch() {

        if (this.firstBox.index != this.secondBox.index) {
            this.wrongCallback();
         this.swapBoxes();   
        }
        else {
            this.correctCallback();
            this.firstBox.removeInteractive();
            this.secondBox.removeInteractive();
            this.firstBox = null;
            this.secondBox = null;
            setTimeout(() => {
                this.resetCallback();
            }, 1000);
            
            this.clickLock=false;
        }
        
    }
    swapBoxes()
    {
        let pos1:PosVo=new PosVo(this.firstBox.x,this.firstBox.y);
        let pos2:PosVo=new PosVo(this.secondBox.x,this.secondBox.y);

        this.scene.tweens.add({targets: this.firstBox,duration: 500,y:pos2.y,x:pos2.x});
        this.scene.tweens.add({targets: this.secondBox,duration: 500,y:pos1.y,x:pos1.x});
        this.firstBox.hide();
        this.secondBox.hide();
        setTimeout(() => {
            this.swapDone();
        }, 500);
    }
    swapDone()
    {       
        this.firstBox = null;
        this.secondBox = null;
        this.clickLock=false;
        this.resetCallback();
    }
}