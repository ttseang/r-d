import { GameObjects } from "phaser";
import { Face } from "../classes/Face";
import { MatchGrid } from "../classes/MatchGrid";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private face:Face;
    private matchGrid:MatchGrid;
    private back:GameObjects.Image;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.image("eye","./assets/eye.png");
        this.load.image("closed","./assets/closed.png");
        this.load.atlas("mouth","./assets/mouth.png","./assets/mouth.json");
        this.load.atlas("icons","./assets/icons.png","./assets/icons.json");
        this.load.image("square","./assets/square.png");
        this.load.image("hand1","./assets/hand1.png");
        this.load.image("hand2","./assets/hand2.png");
        this.load.image("back","./assets/back2.jpg");
        //
        //
        //
        this.load.audio("rightSound","./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");
        this.load.audio("selectSound","./assets/audio/quiz_select.wav");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.back=this.add.image(0,0,"back");

        Align.scaleToGameW(this.back,1,this);

        if (this.back.displayHeight<this.gh)
        {
            this.back.displayHeight=this.gh;
            this.back.scaleX=this.back.scaleY;
        }

        Align.center(this.back,this);
        

       // this.grid.showPos();

        this.matchGrid=new MatchGrid(this);
        this.matchGrid.makeBoxes();

        this.face=new Face(this);

        this.grid.placeAtIndex(60,this.face,true);
        this.face.x-=this.face.displayWidth/2;
        this.face.y-=this.face.displayHeight/2;

        this.input.on("pointermove",this.doLook.bind(this));
       // this.input.on('pointerdown',()=>{this.face.randMouth()})
       this.matchGrid.correctCallback=this.doSmile.bind(this);
       this.matchGrid.wrongCallback=this.doMagic.bind(this);
       this.matchGrid.resetCallback=this.defaultSmile.bind(this);
       this.matchGrid.selectCallback=this.clicked.bind(this);

    }
    clicked(pos:PosVo)
    {
        this.face.lookAt(pos.x,pos.y);
        let clickSound:Phaser.Sound.BaseSound=this.sound.add("selectSound");
        clickSound.play();
    }
    defaultSmile()
    {
        this.face.setMouth(1);
    }
    doSmile()
    {
        let correctSound:Phaser.Sound.BaseSound=this.sound.add("rightSound");
        correctSound.play();
        this.face.setMouth(3);
    }
    doMagic()
    {
        let wrongSound:Phaser.Sound.BaseSound=this.sound.add("wrongSound");
        wrongSound.play();
        this.face.setMouth(4);        
    }
    doLook()
    {
        this.face.lookAt(this.input.x,this.input.y);
    }
}