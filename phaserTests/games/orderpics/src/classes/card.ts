import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { LayoutManager } from "./layoutManager";

export class Card extends GameObjects.Sprite
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    private lm:LayoutManager;
    public origin:PosVo=new PosVo(0,0);

    constructor(bscene:IBaseScene,lm:LayoutManager,key:string,frame:string)
    {
        super(bscene.getScene(),0,0,key,frame);
        this.bscene=bscene;
        this.lm=lm;
        this.scene=bscene.getScene();

        Align.scaleToGameW(this,this.lm.getScale("card"),this.bscene);
        this.scene.add.existing(this);
    }
    init()
    {
        this.origin=new PosVo(this.x,this.y);
    }
    inRightPlace()
    {
        if (this.x==this.origin.x && this.y==this.origin.y)
        {
            return true;
        }
        return false;
    }
}