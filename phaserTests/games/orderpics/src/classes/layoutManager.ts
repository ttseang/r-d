import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { GM } from "./GM";

export class LayoutManager {
    public scene: Phaser.Scene;
    public guideImage: GameObjects.Sprite;
    public maskImage: GameObjects.Sprite;
    private gm: GM = GM.getInstance();
    private bscene: IBaseScene;
    public callback: Function = () => { };

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;

        window.addEventListener("orientationchange", function () {
            console.log(window.innerWidth, window.innerHeight)
            this.flipped();
        }.bind(this));
    }
    private flipped() {
        console.log('flipped');
        setTimeout(() => {
            let w: number = window.innerWidth;
            let h: number = window.innerHeight;
            if (w < h) {
                this.gm.isPort = true;
            }
            else
            {
                this.gm.isPort=false;
            }
            this.bscene.resetSize(w,h);
            this.scene.scale.resize(w, h);
            this.bscene.getGrid().hide();
            this.bscene.makeGrid(22, 22);
            this.callback();
        }, 1000);

    }
    public doLayout() {
        //this.placeAndScale("number",this.guideImage);
        //this.placeAndScale("number",this.maskImage);
    }
    private placeAndScale(item: string, obj: GameObjects.Sprite) {
        Align.scaleToGameW(obj, this.getScale(item), this.bscene);
        this.bscene.getGrid().placeAtIndex(this.getPos(item), obj);
    }
    public getPos(item: string) {
        let pos: number = 0;

        switch (item) {
            case "number":
                pos = 100;
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    pos = 58;
                }
                if (this.gm.isMobile === true && this.gm.isPort === true) {
                    pos = 52;
                    //pos = -1;
                }
                if (this.gm.isTablet && this.gm.isPort===false)
                {
                    pos=146;
                }
                if (this.gm.isTablet===true && this.gm.isPort===false)
                {
                    pos=57;
                }
                if (this.gm.isTablet===true && this.gm.isPort===true)
                {
                    pos=52;
                }
                break;

            
        }
        return pos;
    }
    public getScale(item: string) {
        let scale: number = 0;

        switch (item) {
            case "card":

                scale = 0.5;
                if (this.gm.isMobile === false) {
                    scale = 0.20;
                }
                if (this.gm.isMobile === true && this.gm.isPort === false) {
                    scale = 0.2;
                }
                if (this.gm.isMobile===true && this.gm.isPort===true)
                {
                    scale=0.35;
                }
                if (this.gm.isTablet===true && this.gm.isPort===false)
                {
                    scale=0.2;
                }
                
                break;

               
        }

        return scale;
    }
    getPosArray(item:string)
    {
        let posArray:PosVo[]=[];
        switch(item)
        {
        case "cards":

            posArray=[new PosVo(5,7),new PosVo(10,7),new PosVo(15,7),new PosVo(5,17),new PosVo(10,17),new PosVo(15,17)];
            if (this.gm.isMobile === false) {
                posArray=[new PosVo(5,7),new PosVo(10,7),new PosVo(15,7),new PosVo(5,17),new PosVo(10,17),new PosVo(15,17)];
            }
            if (this.gm.isMobile === true && this.gm.isPort === false) {
               
            }
            if (this.gm.isMobile===true && this.gm.isPort===true)
            {
                posArray=[new PosVo(5,4),new PosVo(16,4),new PosVo(5,11),new PosVo(16,11),new PosVo(5,17),new PosVo(16,17)];
            }
            if (this.gm.isTablet===true && this.gm.isPort===false)
            {
                posArray=[new PosVo(5,7),new PosVo(10,7),new PosVo(15,7),new PosVo(5,15),new PosVo(10,15),new PosVo(15,15)];
            }
            
            break;
        }
        return posArray;
    }
}