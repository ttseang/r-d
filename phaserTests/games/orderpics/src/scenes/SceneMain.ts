import { GameObjects } from "phaser";
import { Card } from "../classes/card";
import { LayoutManager } from "../classes/layoutManager";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private lm: LayoutManager;
    private cardArray: Card[] = [];
    private selectedCard: Card | null = null;
    private selectBox: GameObjects.Image;
    private clickLock: Boolean = false;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.atlas("story1", "./assets/story1.png", "./assets/story1.json");
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("background","./assets/background.jpg");
        this.load.audio("swapdone","./assets/swapdone.mp3");
        this.load.audio("tap","./assets/tap.mp3");
        this.load.audio("win","./assets/win.mp3");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);

        let bg: GameObjects.Image = this.add.image(0, 0, "background");
        Align.scaleToGameW(bg, 1, this);

        bg.displayHeight = this.gh;
        bg.displayWidth = this.gw;
        Align.center(bg, this);

        this.lm = new LayoutManager(this);

      //  this.grid.showPos();
        let posArray: PosVo[] = this.lm.getPosArray("cards");

        for (let i: number = 1; i < 7; i++) {
            let f: string = i.toString() + ".jpg";
            // console.log(f);
            let card: Card = new Card(this, this.lm, "story1", f);
            this.cardArray.push(card);
            card.setInteractive();
            //card.alpha=0.5;
            this.grid.placeAt(posArray[i - 1].x, posArray[i - 1].y, card);
            card.init();
        }

        this.selectBox = this.add.image(0, 0, "holder");
        this.selectBox.setTint(0xffcc00);
        this.selectBox.visible = false;

        this.mixUp();

        this.input.on("gameobjectdown", this.selectCard.bind(this));
    }
    mixUp() {
        for (let i: number = 0; i < 50; i++) {
            let r1: number = Math.floor(Math.random() * this.cardArray.length);
            let r2: number = Math.floor(Math.random() * this.cardArray.length);

            let card1: Card = this.cardArray[r1];
            let card2: Card = this.cardArray[r2];

            let pos1: PosVo = new PosVo(card1.x, card1.y);
            let pos2: PosVo = new PosVo(card2.x, card2.y);

            card2.x = pos1.x;
            card2.y = pos1.y;

            card1.x = pos2.x;
            card1.y = pos2.y;

        }
    }
    selectCard(p: Phaser.Input.Pointer, obj: Card) {
        if (this.clickLock == true) {
            return;
        }
        if (this.selectedCard == obj) {
            this.selectBox.visible = false;
            this.selectedCard = null;
            return;
        }
        this.playSound("tap");

        this.selectBox.x = obj.x;
        this.selectBox.y = obj.y;
        this.selectBox.displayWidth = obj.displayWidth * 1.1;
        this.selectBox.displayHeight = obj.displayHeight * 1.1;

        this.selectBox.visible = true;

        this.children.bringToTop(obj);

        if (this.selectedCard === null) {
            this.selectedCard = obj;
            this.clickLock=false;
        }
        else {
            this.selectBox.visible=false;
            this.swapCards(this.selectedCard, obj);
        }
    }
    swapCards(card1: Card, card2: Card) {
        this.selectedCard=null;
        let pos1: PosVo = new PosVo(card1.x, card1.y);
            let pos2: PosVo = new PosVo(card2.x, card2.y);
        this.tweens.add({targets: card1,duration: 500,y:pos2.y,x:pos2.x});
        this.tweens.add({targets: card2,duration: 500,y:pos1.y,x:pos1.x});
        setTimeout(() => {
            this.swapDone();
        }, 600);
    }
    swapDone()
    {
       this.playSound("swapdone");

        if (this.checkWin()==false)
        {
            this.clickLock=false;
        }
        else
        {
            console.log("WIN!");
            this.clickLock=true;
            this.playSound("win");
        }
    }
    checkWin()
    {
        for (let i:number=0;i<this.cardArray.length;i++)
        {
            let card:Card=this.cardArray[i];
            if (card.inRightPlace()==false)
            {
                return false;
            }
        }
        return true;
    }
    playSound(soundKey:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(soundKey);
        sound.play();
    }
}







