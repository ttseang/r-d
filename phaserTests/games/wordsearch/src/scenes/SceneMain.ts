import { GameObjects } from "phaser";
import { LetterBox } from "../classes/LetterBox";
import { ListBox } from "../classes/ListBox";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private words:string[]=[];
    private wordGrid:string[][]=[[]];
    private boxArray:LetterBox[][]=[[]];
    private selectArray:LetterBox[]=[];
    //
    //
    private rows:number=15;
    private cols:number=14;
    private gridStartX:number=1;
    private gridStartY:number=2;
    private back:Phaser.GameObjects.Image;

    private pointerStartX:number=0;
    private pointerStartY:number=0;

    private startX:number=0;
    private startY:number=0;
    private endX:number=0;
    private endY:number=0;
    private dragGraphics:Phaser.GameObjects.Graphics;
    private wordList:string[]=[];
    private listBox:ListBox;
    private layoutRows:number=23;
    private layoutCols:number=20;
    public face:GameObjects.Sprite;
    private board:string="";
    private wordCellH:number=50;
    private wordCellW:number=50;
    private firstBox:LetterBox | null;
    private lastBox:LetterBox | null;

    constructor() {
        super("SceneMain");
    }
    preload() {
       this.load.image("holder","./assets/holder.jpg");
       this.load.image("back","./assets/back3.jpg");
       this.load.image("face","./assets/face.png");
       this.load.image("arrow","./assets/arrow.png");
    }
    create() {
        super.create();
        console.log(this.gw,this.gh);

        this.makeGrid(this.layoutRows,this.layoutCols);

        window['scene']=this;
       //
        this.back=this.add.image(0,0,"back");
        
        this.input.once("gameobjectdown",this.selectStart.bind(this));

        fetch("assets/words/level1.json").then(response => response.json()).then(data=>{
            let words:string=data.level.words;
            this.words=words.split(",");
            this.board=data.level.board;
            this.setUp();
        });
       
        //this.face=this.add.sprite(0,0,"face");
      //  Align.scaleToGameW(this.face,0.1,this);
        
       this.scale.on('orientationchange',this.flipped.bind(this));
    }
    setUp()
    {
        console.log("set up");
        this.getPlaySize();
        let boardArray:string[]=this.board.split("|");
        for (let i:number=0;i<boardArray.length;i++)
        {
            this.wordGrid[i]=boardArray[i].split("");
        }  
        
            this.displayWordGrid();
        this.dragGraphics=this.add.graphics();
       

        this.listBox=new ListBox(this,this.words);
        this.placeThings(this.scale.orientation.toString());
       
      
    }
    getPlaySize()
    {
        let playSizeH:number;
        let playSizeW:number;

        if (this.scale.orientation.toString()===Phaser.Scale.LANDSCAPE)
        {
            playSizeH=this.gh*0.9;
            playSizeW=this.gw*0.6;
            this.gridStartY=this.gh*0.1;
            this.gridStartX=this.gw*0.1;
        }
        else
        {
            playSizeH=this.gh*0.8;
            playSizeW=this.gw*0.8;
            this.gridStartY=this.gh*0.05;
            this.gridStartX=this.gw*0.09;
        }
        
        this.wordCellH=playSizeH/this.rows;
        this.wordCellW=playSizeW/this.cols;

      //  this.gridStartX-=this.wordCellW;

    }
    flipped(orientation:string)
    {
        console.log(orientation);
        console.log("FLIPPED");
       
        let temp:number=this.gw;
        this.gw=this.gh;
        this.gh=temp;

      //  this.game.scale.displaySize.aspectRatio = 1777/999;
        this.scale.resize(this.gw,this.gh);
        this.grid.resetDim(this.gw,this.gh);
        this.grid.hide();
        this.makeGrid(this.layoutRows,this.layoutCols);

        

       // this.grid.show();
      //  this.grid.showNumbers();
      this.getPlaySize();
      this.placeLetters();
        this.placeThings(orientation);
    }
    placeThings(orientation:string)
    {     
       
        if (orientation===Phaser.Scale.LANDSCAPE)
        {
            console.log("landscape");

            Align.scaleToGameW(this.back,1.5,this);
            this.grid.placeAt(15,-1,this.listBox);
        }
        else
        {
            console.log("portrait");
            Align.scaleToGameH(this.back,1,this);
            this.grid.placeAt(-1,18,this.listBox);
        }
        Align.center(this.back,this);
        
        this.listBox.lineUp();
    }
    selectStart(pointer:Phaser.Input.Pointer,obj:LetterBox)
    {
        console.log(obj.name.substr(0,9));

        if (obj.name.substr(0,9)!=="letterbox")
        {
            return;
        }
        
        this.startX=obj.xx;
        this.startY=obj.yy;
        this.pointerStartX=obj.x;
        this.pointerStartY=obj.y;

        this.input.once("gameobjectup",this.selectEnd.bind(this));
        this.input.on('pointermove',this.selectMove.bind(this));
    }
    selectMove()
    {
        this.dragGraphics.clear();
        this.dragGraphics.lineStyle(8,0x2ecc71,0.9);
        this.dragGraphics.moveTo(this.pointerStartX,this.pointerStartY);
        this.dragGraphics.lineTo(this.input.activePointer.x,this.input.activePointer.y);
        this.dragGraphics.strokePath();
    }
    selectEnd(pointer:Phaser.Input.Pointer,obj:LetterBox)
    {
        this.input.off('pointermove');

        this.dragGraphics.clear();
        this.endX=obj.xx;
        this.endY=obj.yy;
               
        let word:string="";
        this.selectArray=[];

        let diffX:number=Math.abs(this.startX-this.endX);
        let diffY:number=Math.abs(this.startY-this.endY);
       

        if (diffX===0 && diffY!==0)
        {
            word=this.getColSlice(this.startX,this.startY,this.endY);
        }
        if (diffY===0 && diffX!==0)
        {
            
            word=this.getRowSlice(this.startY,this.startX,this.endX);
        }
        if (diffX===diffY)
        {   
            
            word=this.getDiagSlice(this.startY,this.startX,this.endY,this.endX);
        }
        console.log(word);
        for (let i:number=0;i<this.words.length;i++)
        {
            let word2:string=this.words[i];
            word2=word2.replace(" ","");

            if(word2.toLowerCase()===word.toLowerCase())
            {
                this.markWord();
            this.listBox.strike(word);
            }
        }
       /* if (this.words.indexOf(word.toLowerCase())>-1)
        {
            this.markWord();
            this.listBox.strike(word);
        }*/

        this.input.once("gameobjectdown",this.selectStart.bind(this));
    }
    markWord()
    {
        for(let i:number=0;i<this.selectArray.length;i++)
        {
            this.selectArray[i].setColor('#ffffff');
        }
    }
    getRowSlice(row:number,start:number,end:number)
    {
        if (end<start)
        {
            let temp:number=end;
            end=start;
            start=temp;
        }
        let word:string="";
        for (let i:number=start;i<end+1;i++)
        {
            word+=this.wordGrid[row][i];
            this.selectArray.push(this.boxArray[i][row]);
        }
      
        return word;
    }
    getColSlice(col:number,start:number,end:number)
    {
        if (end<start)
        {
            let temp:number=end;
            end=start;
            start=temp;
        }
        let word:string="";
        for (let i:number=start;i<end+1;i++)
        {
            word+=this.wordGrid[i][col];
            this.selectArray.push(this.boxArray[col][i]);
        }
       return word;
    }
    getDiagSlice(startRow:number,startCol:number,endRow:number,endCol:number)
    {
        
        if (endRow<startRow)
        {
            let temp:number=endRow;
            endRow=startRow;
            startRow=temp;
        }

        if (endCol<startCol)
        {
            let temp2:number=endCol;
            endCol=startCol;
            startCol=temp2;
        }

       
        let len:number=Math.abs(startRow-endRow)+1;       
        let word:string="";
        for (let i:number=0;i<len;i++)
        {
            let xx:number=startRow+i;
            let yy:number=startCol+i;
            console.log(xx,yy);
            word+=this.wordGrid[yy][xx];
            this.selectArray.push(this.boxArray[yy][xx]);
        }
        return word;
        
    }
    placeAllWords()
    {
        for(let i:number=0;i<this.words.length;i++)
        {
            this.placeWordAtRandomPlace(this.words[i]);
        }
    }
    initWordGrid()
    {
        for (let i:number=0;i<this.cols;i++)
        {          
            for (let j:number=0;j<this.rows;j++)
            {
                this.wordGrid[i][j]=".";
            }
        }
    }
    displayWordGrid()
    {
        console.log(this.wordGrid);
        let lastBox:LetterBox | null=null;

        for (let i:number=0;i<this.cols;i++)
        {
            this.boxArray[i]=[];
            for (let j:number=0;j<this.rows;j++)
            {
                //let textObj=this.add.text(0,0,this.wordGrid[i][j],{fontSize:'22px'}).setOrigin(0.5,0.5);
               // this.grid.placeAt(this.gridStartX+i,this.gridStartY+j,textObj);
                let letterBox:LetterBox=new LetterBox(this,this.wordGrid[j][i]);
                letterBox.name="letterbox"+i.toString()+"_"+j.toString();

                if (this.firstBox==null)
                {
                    this.firstBox=letterBox;
                }
                this.boxArray[i][j]=letterBox;
                letterBox.xx=i;
                letterBox.yy=j;
                
                if (lastBox)
                {
                    lastBox.nextBox=letterBox;
                }

                lastBox=letterBox;
                this.lastBox=lastBox;
                //this.grid.placeAt(this.gridStartX+i,this.gridStartY+j,letterBox);
            }
        }
        
        this.placeLetters();
    }
    placeLetters()
    {
        /* if (this.scale.orientation.toString()===Phaser.Scale.LANDSCAPE)
        {
            console.log("landscape");
            this.gridStartY=this.gh*0.1;
            this.gridStartX=this.gw*0.1;
        }
        else
        {
            this.gridStartY=this.gh*0.05;
            this.gridStartX=this.gw*0.1;
            console.log("portrait");
        } */
        let xx:number=this.gridStartX;
        let yy:number=this.gridStartY;
        let letterBox:LetterBox;

        for (let i:number=0;i<this.cols;i++)
        {
            for (let j:number=0;j<this.rows;j++)
            {
                letterBox=this.boxArray[i][j];
                letterBox.resize(this.wordCellW,this.wordCellH);
                letterBox.x=xx;
                letterBox.y=yy;
                xx+=letterBox.displayWidth;
              //  this.grid.placeAt(this.gridStartX+i,this.gridStartY+j,letterBox);
            }
            xx=this.gridStartX;
            yy+=letterBox.displayHeight;
        }

        let fs:number=this.firstBox.resizeFont();
        console.log("FS="+fs);
        this.firstBox.setFontSize(fs);
    }
    placeWordAtRandomPlace(word:string)
    {
        word=word.toUpperCase();
        let len=word.length;
        let xx:number=Phaser.Math.Between(0,this.cols-len);
        let yy:number=Phaser.Math.Between(0,this.rows-len);
        let dir:number=Phaser.Math.Between(0,2);
        
        if (this.canPlace(xx,yy,dir,word)===false)
        {
            this.placeWordAtRandomPlace(word);
            return;
        }
      

        //1 horizontal
        //2 vert
        //3 diag
        
        switch(dir)
        {
            case 0:

            for (let i:number=0;i<len;i++)
            {
                this.wordGrid[xx+i][yy]=word.substr(i,1);
            }
            break;

            case 1:

                for (let i:number=0;i<len;i++)
                {
                    this.wordGrid[xx][yy+i]=word.substr(i,1);
                }
            break;

            case 2:
                for (let i:number=0;i<len;i++)
                {
                    this.wordGrid[xx+i][yy+i]=word.substr(i,1);
                }
            break;
        }

    }
    canPlace(xx:number,yy:number,dir:number,word:string):boolean
    {
        let len=word.length;
        switch(dir)
        {
            case 1:

            for (let i:number=0;i<len;i++)
            {
                let letter:string=this.wordGrid[xx+i][yy];
                let letter2:string=word[i];

                if (letter!==letter2 && letter!=".")
                {
                    return false;
                }
            }
            break;

            case 2:

                for (let i:number=0;i<len;i++)
                {
                    let letter:string=this.wordGrid[xx][yy+i];
                    let letter2:string=word[i];
    
                    if (letter!==letter2 && letter!=".")
                    {
                        return false;
                    }
                    
                }
            break;

            case 3:
                for (let i:number=0;i<len;i++)
                {
                    let letter:string=this.wordGrid[xx+i][yy+i];
                    let letter2:string=word[i];    
                    if (letter!==letter2 && letter!=".")
                    {
                        return false;
                    }
                }
            break;
        }

        return true;
    }
    fillInBlanks()
    {
        for (let i:number=0;i<this.cols;i++)
        {          
            for (let j:number=0;j<this.rows;j++)
            {
               if (this.wordGrid[i][j]===".")
               {
                   let index:number=Phaser.Math.Between(1,26);
                   let letter:string=String.fromCharCode(64+index);
                   this.wordGrid[i][j]=letter;
               }
            }
        }
    }
}