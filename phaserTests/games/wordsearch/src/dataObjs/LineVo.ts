export class LineVo
{
    public line:Phaser.GameObjects.Sprite;
    public textf:Phaser.GameObjects.Text;

    constructor(line:Phaser.GameObjects.Sprite,textf:Phaser.GameObjects.Text)
    {
        this.line=line;
        this.textf=textf;
    }
}