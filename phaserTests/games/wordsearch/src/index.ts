//import Phaser = require("phaser");
import Phaser from 'phaser';
import { SceneMain } from "./scenes/SceneMain";
import {SceneFonts} from "./scenes/SceneFont";

let isMobile = navigator.userAgent.indexOf("Mobile");
if (isMobile == -1) {
    isMobile = navigator.userAgent.indexOf("Tablet");
}
let w = 1024;
let h = 768;
console.log(isMobile);
//
//
//if (isMobile != -1) {
   w = window.innerWidth;
    h = window.innerHeight;
//}

if(window.innerHeight > window.innerWidth){
    console.log("port");
}
else
{
    console.log("land");
}

const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    mode:Phaser.Scale.NONE,
    parent: 'phaser-game',
    scene: [SceneFonts,SceneMain]
};
console.log(config);
setTimeout(()=>{
    new Phaser.Game(config);
},2000);
