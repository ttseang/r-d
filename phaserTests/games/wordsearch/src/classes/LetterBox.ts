import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { BaseScene } from "../scenes/BaseScene";
import Align from "../util/align";
import { UIBlock } from "../util/UIBlock";

export class LetterBox extends Phaser.GameObjects.Container
{
    public back:Phaser.GameObjects.Image;
    public textObj:Phaser.GameObjects.Text;
    private bscene:IBaseScene;

    public xx:number=0;
    public yy:number=0;
    public nextBox:LetterBox;

    constructor(bscene:IBaseScene,letter:string)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.back=bscene.getScene().add.image(0,0,"holder");
        this.add(this.back);

        this.back.displayWidth=bscene.cw;
        this.back.displayHeight=bscene.ch;
        this.back.setTint(0x3498db);
        this.back.alpha=0.9;
       
       // Align.scaleToGameW(this.back,0.04,bscene);

        let fs:number=200;
        
        /* if (this.scene.scale.orientation===Phaser.Scale.Orientation.PORTRAIT)
        {
            fs=bscene.getW()/20;
        }
        if (this.scene.sys.game.device.os.desktop===true)
        {
            fs=bscene.getW()/50;
        } */

       // console.log(fs);
        this.textObj=bscene.getScene().add.text(0,0,letter,{color:"#000000",fontFamily:"Roboto",fontSize:fs.toString()+"px"}).setOrigin(0.5,0.5);
       
        this.add(this.textObj);
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
        //this.on('pointerdown',()=>{console.log("HI")});
        bscene.getScene().add.existing(this);
    }
    resize(w:number,h:number)
    {
        this.back.displayWidth=w;
        this.back.displayHeight=h;
        this.setSize(this.back.displayWidth,this.back.displayHeight);
    }
    resizeFont()
    {
        let fs:number=42;
        this.textObj.setFontSize(fs);

        let tooBig:boolean=true;
        while(tooBig==true)
        {
            if (this.textObj.displayWidth<this.back.displayWidth && this.textObj.displayHeight<this.back.displayHeight)
            {
                tooBig=false;
            }
            else
            {
                fs--;
                this.textObj.setFontSize(fs);
            }
        }
        return fs;
    }
    setFontSize(s:number)
    {
        this.textObj.setFontSize(s);
        if (this.nextBox)
        {
            this.nextBox.setFontSize(s);
        }
    }
    setColor(color:string)
    {
        this.textObj.setColor(color);
    }
}