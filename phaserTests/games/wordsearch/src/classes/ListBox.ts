import { GameObjects } from "phaser";
import { LineVo } from "../dataObjs/LineVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { BaseScene } from "../scenes/BaseScene";
import Align from "../util/align";
import { UIBlock } from "../util/UIBlock";

export class ListBox extends Phaser.GameObjects.Container
{
    public back:Phaser.GameObjects.Image;
    private scrollMask:Phaser.GameObjects.Image;

    public textObjs:Phaser.GameObjects.Text[]=[];
    public lines:Phaser.GameObjects.Sprite[]=[];
    public lineObjs:LineVo[]=[];

    private bscene:IBaseScene;

    public xx:number=0;
    public yy:number=0;

    private startDragY:number=0;
    
    private scroller:GameObjects.Container;

    constructor(bscene:IBaseScene,words:string[])
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.back=bscene.getScene().add.image(0,0,"holder");
        this.back.setOrigin(0,0);
        this.add(this.back);

        this.scrollMask=bscene.getScene().add.image(0,0,"holder");
        this.scrollMask.setOrigin(0,0);
        //this.add(this.scrollMask);


        this.scroller=this.scene.add.container(0,0);
        this.scroller.x=this.x;
        this.scroller.y=this.y;
        //this.add(this.scroller);
       
        this.back.setTint(0x3498db);
      //  this.back.displayWidth=this.bscene.getW()*0.9;
        //this.back.displayHeight=this.bscene.getH()*0.15;

        //Align.scaleToGameW(this.back,0.9,bscene);

        let fs:number=bscene.getH()/25;
        if (this.scene.scale.orientation===Phaser.Scale.Orientation.PORTRAIT)
        {
            fs=bscene.getW()/40;
        }
       // //console.log(fs);
        
       for (let i:number=0;i<words.length;i++)
       {
           let wordText=this.scene.add.text(0,0,words[i],{fontSize:fs.toString()+"px",align:'center'});
           this.scroller.add(wordText);
           this.textObjs.push(wordText);          
       }       

       

        
        bscene.getScene().add.existing(this);
        this.lineUp();

       
      //  this.back.setInteractive();
      //  this.back.on('pointerdown',this.onDown.bind(this));
       // this.back.on('pointerup',this.onUp.bind(this));
        // this.back.on('pointermove',this.onMove.bind(this));
    }
    onDown(pointer)
    {
        this.startDragY=pointer.y;
    }
    onMove(pointer:Phaser.Input.Pointer)
    {
       
        let dif:number=pointer.y-this.startDragY;
       
        this.scroller.y+=dif;
        this.scroller.x=this.x;
        this.scene.children.bringToTop(this.scroller);
        this.startDragY=pointer.y;
        if (this.scroller.y<-this.scroller.displayHeight)
        {
           // //console.log("OK");
            //this.scroller.y=-this.scroller.displayHeight;
        }
        //console.log(this.scroller.y);
    }
    onUp()
    {

    }
    scrollDown()
    {
        this.scroller.y++;
    }
    scrollUp()
    {
        this.scroller.y--;
    }
    strike(word:string)
    {
        word=word.toLowerCase();        

        for (let i:number=0;i<this.textObjs.length;i++)
        {
            let wordText:Phaser.GameObjects.Text=this.textObjs[i];
            let word2:string=wordText.text.toLowerCase();
            word2=word2.replace(" ","");
            if (word2===word)
            {
                let line:GameObjects.Sprite=this.scene.add.sprite(0,0,"holder").setOrigin(0,0);
                line.setTint(0xffffff);
                line.alpha=0.8;
                line.displayWidth=wordText.displayWidth;
                line.displayHeight=this.bscene.ch/10;
                this.lines.push(line);
                line.x=wordText.x;
                line.y=wordText.y+wordText.displayHeight/2;
                this.scroller.add(line);

                this.lineObjs.push(new LineVo(line,wordText));
            }
        }
    }
    lineUp()
    {
        this.scroller.x=this.x;
        this.scroller.y=this.y;
        this.scene.children.bringToTop(this.scroller)
        let fs:number=this.bscene.getH()/30;
        
        if (this.scene.scale.orientation===Phaser.Scale.Orientation.LANDSCAPE)
        {
            //console.log("HERE");

        this.back.displayWidth=this.bscene.getW()*0.2;
        this.back.displayHeight=this.bscene.getH();
            
        
           
        }
        else
        {
          

            fs=this.bscene.getW()/30;
            this.back.displayWidth=this.bscene.getW()*1.1;
            this.back.displayHeight=this.bscene.getH()*0.2;
        }

        this.scrollMask.displayWidth=this.back.displayWidth;
        this.scrollMask.displayHeight=this.back.displayHeight;

        let startX:number=this.bscene.cw;
        let startY:number=this.bscene.ch;
        let y1:number=1;
        let x1:number=startX;
        let xspacing:number=this.bscene.getGrid().cols/(this.textObjs.length/2)-1;
        let longestWord:number=0;

        //console.log(xspacing);
        for (let i:number=0;i<this.textObjs.length;i++)
        {
            let wordText:Phaser.GameObjects.Text=this.textObjs[i];
            wordText.setFontSize(fs);
            if (this.scene.scale.orientation===Phaser.Scale.Orientation.LANDSCAPE)
            {
                wordText.scaleX=1;
                wordText.scaleY=1;
                if (wordText.displayWidth>this.back.displayWidth)
                {
                     wordText.displayWidth=this.back.displayWidth;
                     wordText.scaleY=wordText.scaleX;
                }

            wordText.x=this.back.displayWidth/2-wordText.displayWidth/2;
            wordText.y=startY+y1*this.bscene.ch;

            y1++;       
            if (y1/10==Math.floor(y1/10))
            {
                //y1=0;
            }     
            }
            else
            {
                wordText.scaleX=1;
                wordText.scaleY=1;

                wordText.y=startY+(y1-1)*this.bscene.ch;
              
                wordText.x=x1;    

                x1+=wordText.displayWidth+20;

                if (i/3==Math.floor(i/3))
                {
                    y1++;
                    x1=startX;
                }
               /*  if (x1>2)
                {
                    y1++;
                    x1=startX;
                }
                else
                {
                    x1+=wordText.displayWidth+10;
                }     
                if (i/10==Math.floor(i/10))
                {
                    x1=startX;
                    y1=1;
                }   */  
                   
            }

        }
        
        for (let j:number=0;j<this.lineObjs.length;j++)
        {
            let lineObj:LineVo=this.lineObjs[j];
            lineObj.line.x=lineObj.textf.x;
            lineObj.line.y=lineObj.textf.y+lineObj.textf.displayHeight/2;
        }

      //  this.scroller.mask=new Phaser.Display.Masks.BitmapMask(this.scene,this.scrollMask);
        this.scrollMask.visible=false;
        this.scrollMask.x=this.x;
        this.scrollMask.y=this.y;
    }
    resize()
    {
        this.lineUp();
        
        //this.back.displayWidth=this.bscene.cw;
        //this.back.displayHeight=this.bscene.ch;
        //this.setSize(this.back.displayWidth,this.back.displayHeight);
    }
    setColor(color:string)
    {
        //this.textObj.setColor(color);
    }
}