import { LetterBox } from "../classes/LetterBox";

export class BoardObj
{
    public board:string[];
    public words:string[];
    public foundWords:string[]=[];
    //
    //
    //
    public rows:number=0;
    public cols:number=0;
    public wordCellH:number=0;
    public wordCellW:number=0;
    public gridStartX:number=0;
    public gridStartY:number=0;
    
    public letterBoxes:LetterBox[][]=[];
    public letters:string[][]=[];


    constructor(board:string[],words:string[],rows:number,cols:number)
    {
        this.board=board;
        this.words=words;
        this.rows=rows;
        this.cols=cols;       

        for (let i:number=0;i<this.rows;i++)
        {
            this.letters[i]=board[i].split("");
            this.letterBoxes[i]=[];
        }
    }
    checkWord(word:string)
    {
        if (this.words.includes(word))
        {
            let index:number=this.words.indexOf(word);
            if (index>-1)
            {
                this.foundWords.push(word);
                this.words.splice(index,1);
            }
            return true;
        }
        return false;
    }
}