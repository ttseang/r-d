export class FlipVo
{
    public lW:number=0;
    public lH:number=0;
    public pW:number=0;
    public pH:number=0;

    constructor(w:number,h:number,init:string)
    {

        if (init=="landscape-primary")
        {
            this.lW=w;
            this.lH=h;
            //
            //
            this.pW=h;
            this.pH=w;
        }
        else
        {
            this.pH=h;
            this.pW=w;
            //
            //
            this.lH=w;
            this.lW=h;
        }

    }
}