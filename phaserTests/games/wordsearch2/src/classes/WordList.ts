import { GameObjects } from "phaser";
import { StrikeVo } from "../dataObjs/strikeVo";
import { IBaseScene } from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import UIBlock from "../util/UIBlock"

export class WordList extends Phaser.GameObjects.Container {
    private words: string[] = [];
    public back: GameObjects.Sprite;
    private bscene: IBaseScene;
    public scene: Phaser.Scene;
    public scaleX: number = 1;
    public scaleY: number = 1;
    public wordBoxes: Phaser.GameObjects.Text[] = [];
    private boxGraphics: Phaser.GameObjects.Graphics;
    private strikes: StrikeVo[] = [];

    constructor(bscene: IBaseScene, words: string[]) {
        super(bscene.getScene());
        this.words = words;
        this.bscene = bscene;
        this.scene = bscene.getScene();

        this.back = this.scene.add.sprite(0, 0, "holder").setOrigin(0, 0);
        this.boxGraphics = this.scene.add.graphics();
        this.add(this.boxGraphics);
        /*  this.back.displayHeight=this.bscene.getH()*0.4;
         this.back.displayWidth=this.bscene.getW()*0.3;
         */
        this.back.visible = false;
        this.back.setTint(0xcccccc);
        this.add(this.back);

        this.makeListBoxes();

        this.scene.add.existing(this);
    }
    private makeListBoxes() {
        let fs: number = this.bscene.getW() / 40;

        for (let i: number = 0; i < this.words.length; i++) {
            let wordBox = this.scene.add.text(0, 0, this.words[i], { fontSize: fs.toString() + "px", color: "#000000" });
            wordBox.setOrigin(0.5, 0.5);
            this.add(wordBox);
            this.wordBoxes.push(wordBox);

        }
        this.resizeMe();
    }
    public strike(word: string) {
        word = word.toLowerCase();
        for (let i: number = 0; i < this.wordBoxes.length; i++) {
            let boxText: string = this.wordBoxes[i].text.toLowerCase();
            if (word === boxText) {
                let strikeLine: GameObjects.Sprite = this.scene.add.sprite(0, 0, "holder").setOrigin(0.5, 0.5);
                strikeLine.setTint(0x00000);
                strikeLine.alpha = 0.5;
                this.add(strikeLine);

                strikeLine.displayWidth = this.wordBoxes[i].displayWidth;
                strikeLine.displayHeight = this.wordBoxes[i].displayHeight / 3;
                this.strikes.push(new StrikeVo(strikeLine, this.wordBoxes[i]))
            }
        }
        this.restrike();
    }
    private restrike() {
        for (let i: number = 0; i < this.strikes.length; i++) {
            let strikeVo: StrikeVo = this.strikes[i];
            strikeVo.rePos();
        }
    }
    public resizeMe() {
        let yy: number = 0;
        let xx: number = 0;
        let startX:number=0;

        if (this.scene.scale.orientation.toString() === Phaser.Scale.LANDSCAPE) {
            //  this.back.displayHeight=this.bscene.getH()*0.4;
            this.back.displayWidth = this.bscene.getW() * 0.2;
            this.back.displayHeight = 20;
            yy = this.bscene.getH() * 0.05;
            xx = this.back.displayWidth/2;
            startX=xx;
        }
        else {
            //this.back.displayHeight=this.bscene.getH()*0.4;
            this.back.displayWidth = this.bscene.getW() * 0.8;
            this.back.displayHeight = this.bscene.getH()*0.1;
            yy =this.back.displayHeight*0.2;
            xx = this.bscene.getW()*0.1;
            startX=xx;
        }


        let fs: number = this.bscene.getW() / 20;


        /*  for (let j:number=0;j<this.words.length;j++)
         {
             let wordBox:Phaser.GameObjects.Text=this.wordBoxes[j];
             console.log(wordBox.displayWidth);
             if (this.back.displayWidth<wordBox.displayWidth)
             {
                 this.back.displayWidth=wordBox.displayWidth*1.1;
             }
         } */
        for (let i: number = 0; i < this.wordBoxes.length; i++) {
            let wordBox: Phaser.GameObjects.Text = this.wordBoxes[i];
            wordBox.y = yy;
            wordBox.x = xx;
            //wordBox.setBackgroundColor("#00ff00");
            wordBox.setFontSize(fs);

            if (this.scene.scale.orientation.toString() === Phaser.Scale.LANDSCAPE) {
            yy += wordBox.displayHeight * 1.05;
            if (yy > this.back.displayHeight) {
                this.back.displayHeight = yy * 1.01;
            }
        }
        else
        {
            if (xx+wordBox.displayWidth*1.1>this.back.displayWidth)
            {
                xx=startX;
                yy += wordBox.displayHeight * 1.5;
                if (yy> this.back.displayHeight) {
                    this.back.displayHeight = yy;
                }
            }
            else
            {
                xx+=wordBox.displayWidth*1.5;
            }           
            
        }
       

        }
        this.boxGraphics.clear();
        this.boxGraphics.lineStyle(4, 0x43718B, 1);
        this.boxGraphics.strokeRoundedRect(this.back.x, this.back.y, this.back.displayWidth, this.back.displayHeight, 16);
        this.boxGraphics.strokePath();

        this.restrike();
    }
}