import { Button } from "../classes/comps/Button";
import { Face } from "../classes/Face";
import { SBubble } from "../classes/SBubble";
import { BubblePosVo } from "../dataObjs/BubblePosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private face:Face;
    private bubble:SBubble;

    private btnNext:Button;
    private btnPrev:Button;
    private btnRand:Button;

    constructor() {
        super("SceneMain");
    }
    preload() {
       
    }
    create() {
        super.create();
        this.makeGrid(11,11);
       // this.grid.showPos();

        //let bubbleFrame:number=Math.floor(Math.random()*11)+1;
        let  bubbleFrame:number=1;

        let testText:string="Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal. Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.";
        let fontSize:number=20;

        this.bubble=new SBubble(this,testText,fontSize,bubbleFrame);
        
        window['bubble']=this.bubble;

        this.face = new Face(this);

        Align.scaleToGameW(this.face,0.2,this);

        //this.grid.placeAt(4,1,bubble);

      //  this.grid.placeAt(5,5,this.face);
        Align.center2(this.face,this);

        this.input.keyboard.on("keydown", this.keypressed.bind(this));

        this.children.bringToTop(this.bubble);

        this.btnRand=new Button(this,"button","Random Text");
        this.btnRand.setCallback(this.setPrepText.bind(this));

        this.btnPrev=new Button(this,"button2","Prev Section");
        this.btnPrev.setCallback(this.prevChunk.bind(this));

        this.btnNext=new Button(this,"button3","Next Section");
        this.btnNext.setCallback(this.nextChunk.bind(this));

        Align.scaleToGameW(this.btnRand,0.2,this);
        Align.scaleToGameW(this.btnPrev,0.2,this);
        Align.scaleToGameW(this.btnNext,0.2,this);

        this.grid.placeAt(0.5,7,this.btnRand);
        this.grid.placeAt(0.5,8,this.btnPrev);
        this.grid.placeAt(0.5,9,this.btnNext);

        this.bubble.doneCallback=this.chunkDone.bind(this);
        this.setPrepText();
    }
    private setPrepText()
    {
        let testText:string="Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal. Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.";
       
        let bubblePos:BubblePosVo[]=[];
        bubblePos.push(new BubblePosVo(5,3,"EXCELLENT",1,20));
        bubblePos.push(new BubblePosVo(6,2,"YOU HAVE A GREAT DEAL OF SKILL AND KNOWLEDGE",1,20));
        bubblePos.push(new BubblePosVo(3,6,"YOU ARE SO SMART!",2,18)); 
        bubblePos.push(new BubblePosVo(5,7,"Yes",4,20));
        bubblePos.push(new BubblePosVo(5,7,"Cool!",4,20));
        bubblePos.push(new BubblePosVo(5,7,"Good Job",4,20));
        bubblePos.push(new BubblePosVo(2.5,6,"THAT WAS AMAZING!",5,20));
        bubblePos.push(new BubblePosVo(4,6,"Cool",6,20));
        bubblePos.push(new BubblePosVo(2.5,6,"YOU HAVE A GREAT DEAL OF SKILL AND KNOWLEDGE",8,20));
        bubblePos.push(new BubblePosVo(5,3,"Cool",9,24));
        bubblePos.push(new BubblePosVo(4,3,testText,11,18));
        //
        let r:number=Math.floor(Math.random()*bubblePos.length);
        let bubblePosVo:BubblePosVo=bubblePos[r];

        this.bubble.setFrame2(bubblePosVo.bubbleFrame);
        this.bubble.setFontSize(bubblePosVo.fontSize);
        this.bubble.setBubbleText(bubblePosVo.text,70);

        this.grid.placeAt(bubblePosVo.x,bubblePosVo.y,this.bubble);
        
        this.bubble.autoSize();

        if (this.bubble.chunks.length==1)
        {
            this.btnPrev.visible=false;
            this.btnNext.visible=false;
        }
        else
        {
            this.btnPrev.visible=true;
            this.btnNext.visible=true;
        }

    }
    private setRandomText()
    {
        let testText:string="Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal. Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.";
        let sample1:string="You did great!";
        let sample2:string="Wow! Way to go!";

        let randText:string[]="YES!,COOL!,GOOD JOB!,EXCELLENT!,YOU ARE SO SMART!,THAT WAS AMAZING!,YOU HAVE A GREAT DEAL OF SKILL AND KNOWLEDGE".split(",");
        //randText.push(testText);
        //let randText:string[]=[testText,sample1,sample2];
        let r:number=Math.floor(Math.random()*randText.length);

        
        
        let text:string=randText[r];

        this.bubble.setBubbleText(text);
        
      //  let fs:number=Math.floor(Math.random()*16)+10;
        let bubbleFrame:number=Math.floor(Math.random()*11)+1;
       // bubbleFrame=1;
        console.log(bubbleFrame);

        this.bubble.setBubbleFrame(bubbleFrame);
        //this.bubble.setFontSize(fs);
       
        this.bubble.autoSize();

        if (this.bubble.chunks.length==1)
        {
            this.btnPrev.visible=false;
            this.btnNext.visible=false;
        }
        else
        {
            this.btnPrev.visible=true;
            this.btnNext.visible=true;
        }
    }
    private chunkDone()
    {
        this.btnPrev.visible=false;
            this.btnNext.visible=false;
    }
    private nextChunk()
    {
        this.bubble.nextChunk();
    }
   private prevChunk()
   {
       this.bubble.prevChunk();
   }
   private keypressed(e: KeyboardEvent) {

        e.preventDefault();
        switch (e.key) {

            
            case "q":
                this.face.lookAtPos(1);
                break;

            case "w":
                this.face.lookAtPos(2);
                break;
            case "e":
                this.face.lookAtPos(3);
                break;

            case "r":
                this.face.lookAtPos(4);
                break;
            case "t":
                this.face.lookAtPos(5);
                break;

            case "a":
                this.face.lookAtPos(6);
                break;

            case "s":
                this.face.lookAtPos(7);
                break;
            case "d":
                this.face.lookAtPos(8);
                break;

            case "f":
                this.face.lookAtPos(9);
                break;
            case "g":
                this.face.lookAtPos(10);
                break;


            case "z":
                this.face.lookAtPos(11);
                break;

            case "x":
                this.face.lookAtPos(12);
                break;
            case "c":
                this.face.lookAtPos(13);
                break;

            case "v":
                this.face.lookAtPos(14);
                break;
            case "b":
                this.face.lookAtPos(15);
                break;

            case " ":
                this.face.blink2();
                break;

            case "u":
                this.face.setMouth(1);
                break;
            case "i":
                this.face.setMouth(2);
                break;
            case "o":
                this.face.setMouth(3);
                break;
            case "p":
                this.face.setMouth(4);
                break;
            case "[":
                this.face.setMouth(5);
                break;
            case "n":
                this.face.showHands();
                break;
            case "j":
                this.face.toggleLBrow();
                break;
            case "k":
                this.face.toggleRBrow();
                break;
            case "l":
                this.face.angleLBrow();
                break;
            case ";":
                this.face.angleRBrow();
                break;

            case "1":
                if (e.ctrlKey) {
                    this.face.setHand2(1);
                }
                else {
                    this.face.setHand1(1);
                }
                break;
            case "2":
                if (e.ctrlKey) {
                    this.face.setHand2(2);
                }
                else {
                    this.face.setHand1(2);
                }
                break;
            case "3":
                if (e.ctrlKey) {
                    this.face.setHand2(3);
                }
                else {
                    this.face.setHand1(3);
                }
                break;
            case "4":
                if (e.ctrlKey) {
                    this.face.setHand2(4);
                }
                else {
                    this.face.setHand1(4);
                }
                break;
            case "5":
                if (e.ctrlKey) {
                    this.face.setHand2(5);
                }
                else {
                    this.face.setHand1(5);
                }
                break;
            case "6":
                if (e.ctrlKey) {
                    this.face.setHand2(6);
                }
                else {
                    this.face.setHand1(6);
                }
                break;
            case "7":
                if (e.ctrlKey) {
                    this.face.setHand2(7);
                }
                else {
                    this.face.setHand1(7);
                }
                break;
            case "8":
                if (e.ctrlKey) {
                    this.face.setHand2(8);
                }
                else {
                    this.face.setHand1(8);
                }
                break;
            case "9":
                if (e.ctrlKey) {
                    this.face.setHand2(9);
                }
                else {
                    this.face.setHand1(9);
                }
                break;
            case "0":
                if (e.ctrlKey) {
                    this.face.setHand2(10);
                }
                else {
                    this.face.setHand1(10);
                }


                break;
        }
    }
}