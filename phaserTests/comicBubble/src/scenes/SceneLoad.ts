import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene
{
    
    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {
       this.load.image("holder","./assets/holder.jpg");
       this.load.image("button","./assets/ui/buttons/1/1.png");
       this.load.image("button2","./assets/ui/buttons/1/2.png");
       this.load.image("button3","./assets/ui/buttons/1/3.png");

       this.load.atlas("bubbles","./assets/bubbles2.png","./assets/bubbles2.json");


      

       this.load.image("face", "./assets/puppet/face3.png");
       this.load.image("eye", "./assets/puppet/eye.png");
       this.load.image("closed", "./assets/puppet/closed.png");
       this.load.image("eyeshade", "./assets/puppet/eyeshade.png");
       //
       //
       //
       this.load.image("lbrow", "./assets/puppet/lbrow.png");
       this.load.image("rbrow", "./assets/puppet/rbrow.png");

       this.load.atlas("mouth", "./assets/puppet/mouth.png", "./assets/puppet/mouth.json");

       this.load.image("hand1", "./assets/puppet/hand1.png");
       this.load.image("hand2", "./assets/puppet/hand2.png");

       this.load.atlas("hands", "./assets/puppet/hands.png", "./assets/puppet/hands.json");
    }
    create()
    {
        this.scene.start("SceneMain");
    }
}