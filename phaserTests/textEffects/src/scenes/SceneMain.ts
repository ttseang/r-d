import { FlipTextVo } from "../dataObjs/FlipTextVo";
import { HighlightVo } from "../dataObjs/HighlightVo";
import { SparkleVo } from "../dataObjs/SparkleVo";
import { FlashingText } from "../textEffects/FlashingText";
import { FlipText } from "../textEffects/FlipText";
import { FlipText2 } from "../textEffects/FlipText2";
import { GrowUp } from "../textEffects/GrowUp";
import { Hightlight } from "../textEffects/Highlight";
import { SparkleText } from "../textEffects/SparkleText";
import { SpinText } from "../textEffects/SpinText";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private testText:Phaser.GameObjects.Text;

    constructor() {
        super("SceneMain");
    }
    preload() {
       this.load.spritesheet("sparkle","./assets/stars.png",{frameWidth:25,frameHeight:25});
    }
    create() {
        super.create();
        this.makeGrid(11,11);
        //this.grid.showNumbers();
       this.testText=this.add.text(0,0,"Test",{ color: "#ffffff", fontFamily: 'BarthowheelReg', fontSize: '20px' });
        this.testText.visible=false;
        setTimeout(this.setUp.bind(this),1000);
        window['scene']=this;
    }
    setUp()
    {
         let hv1:HighlightVo=new HighlightVo("A simple highlight",2,7,"#2ecc71",true,2000);
        let h:Hightlight=new Hightlight(this,hv1);
       
        this.grid.placeAt(2,0,h);
        h.start();

         let sparkle:SparkleText=new SparkleText(this,new SparkleVo("She had some amazing news to share but nobody to share it with.","#00ff00",false,0));
        this.grid.placeAt(0,2,sparkle);
        sparkle.start();

        let hv:HighlightVo=new HighlightVo("Pay attention to me!",16,18,"#ff0000",true,0);
        let fl:FlashingText=new FlashingText(this,hv);
        this.grid.placeAt(1,1,fl);
        fl.start();

        let flipVo:FlipTextVo=new FlipTextVo("Doctor, help I'm a pair of curtains","#0fffff",false,0);
        let flipText:FlipText=new FlipText(this,flipVo);
        this.grid.placeAt(0,4,flipText);
        flipText.start();

        let flipVo2:FlipTextVo=new FlipTextVo("Happiness can be found in the depths\nof chocolate pudding.","#0fffff",false,0);
        let flipText2:FlipText2=new FlipText2(this,flipVo2);
        this.grid.placeAt(0,5,flipText2);
        flipText2.start();


        let flipVo3:FlipTextVo=new FlipTextVo("I liked their first two albums but changed my mind after that charity gig.","#0fffff",false,0);
        let spinText:SpinText=new SpinText(this,flipVo3);
        this.grid.placeAt(0,7,spinText);
        spinText.start();
       
        let flipVo4:HighlightVo=new HighlightVo("There can never be too many cherries on an ice cream sundae.",28,36,"#0fffff",false,0);
        let growText:GrowUp=new GrowUp(this,flipVo4);
        this.grid.placeAt(0,9,growText);
        growText.start();
    }
}