export class HighlightVo
{
    public start:number;
    public end:number;
    public color:string;
    public text:string;
    public italics:boolean;
    public delay:number;

    constructor(text:string,start:number,end:number,color:string,italics:boolean=false,delay:number=0)
    {
        this.text=text;
        this.start=start;
        this.end=end;
        this.color=color;
        this.italics=italics;
        this.delay=delay;
    }
}