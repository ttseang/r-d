import IBaseScene from "../interfaces/IBaseScene";

export class BaseTextEffect extends Phaser.GameObjects.Container {

    protected t1: any = { color: "#000000",fontWeight:'bold', fontFamily: 'BarthowheelReg', fontSize: '28px' };
    protected t2: any = { color: "#5E7f57",fontWeight:'bold', fontFamily: 'BarthowheelReg', fontSize: '28px' };
    public bscene: IBaseScene;
    public scene: Phaser.Scene;
    protected timer1:Phaser.Time.TimerEvent;

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene=bscene;

        this.scene.add.existing(this);
    }
    protected makeWords(text:string,vis:boolean)
    {
        let letterArray:Phaser.GameObjects.Text[]=[];
        let wordArray:string[]=text.split(" ");
        let spacing: number = 1.05;
        let xx: number = 0;
        for (let i: number = 0; i < text.length; i++) {
            let letter: string = wordArray[i];
            
            let myStyle:any=this.t1;
            let letterBox: Phaser.GameObjects.Text = this.scene.add.text(0, 0, letter,myStyle);
            letterArray.push(letterBox);
            letterBox.visible=vis;
            letterBox.x = xx;
            xx += letterBox.displayWidth * spacing;
            this.add(letterBox);
        }
        return letterArray;
    }
    protected makeHLetters(text:string,hstart:number,hend:number) {
        let letterArray:Phaser.GameObjects.Text[]=[];
        let spacing: number = 1.05;
        let xx: number = 0;
        let yy: number=0;
        for (let i: number = 0; i < text.length; i++) {
            let letter: string = text[i];

            let myStyle:any=this.t1;

            if (i>hstart-1 && i<hend+1)
            {
                myStyle=this.t2;
            }

            let letterBox: Phaser.GameObjects.Text = this.scene.add.text(0, 0, letter,myStyle);
            letterArray.push(letterBox);

            letterBox.x = xx;
            letterBox.y=yy;
            xx += letterBox.displayWidth * spacing;
            if (xx>this.bscene.getW()*0.9)
            {
                xx=0;
                yy+=letterBox.displayHeight*spacing;
            }
            this.add(letterBox);
        }
        return letterArray;
    }
    protected makeLetters(text:string,vis:boolean=true) {
        let letterArray:Phaser.GameObjects.Text[]=[];
        let spacing: number = 1.05;
        let xx: number = 0;
        let yy:number=0;
        for (let i: number = 0; i < text.length; i++) {
            let letter: string = text[i];
           
            let myStyle:any=this.t1;
            let letterBox: Phaser.GameObjects.Text = this.scene.add.text(0, 0, letter,myStyle);
            letterArray.push(letterBox);
            letterBox.visible=vis;
            letterBox.x = xx;
            letterBox.y=yy;

            xx += letterBox.displayWidth * spacing;
            if (xx>this.bscene.getW()*0.9)
            {
                xx=0;
                yy+=letterBox.displayHeight*spacing;
            }
            this.add(letterBox);
        }
        return letterArray;
    }
}