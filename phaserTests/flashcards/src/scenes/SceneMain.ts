import { GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import { FlashCard } from "../classes/FlashCard";
import { GM } from "../classes/GM";
import { GamePosObj } from "../classes/layout/GamePosObj";
import { GameScaleObject } from "../classes/layout/GameScaleObj";
import { LayoutManager } from "../classes/layout/layoutManager";
import { FlashCardVo } from "../dataObjs/FlashCardVo";
import { PosVo } from "../dataObjs/PosVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private layoutManager: LayoutManager;
    private gameScaleObj: GameScaleObject;
    private card: FlashCard;
    private btnNext: GameObjects.Image;
    private btnPrev: GameObjects.Image;
    private cardIndex: number = 0;
    private gm: GM = GM.getInstance();
    private cardX: number = 0;

    private clickLock: boolean = false;

    private btnQuiz: Button;

    private robot: GameObjects.Sprite;

    constructor() {
        super("SceneMain");
    }
    preload() {

    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        let bg: GameObjects.Image = this.add.image(0, 0, "bg1");
        bg.displayWidth = this.gw;
        bg.scaleY = bg.scaleX;

        if (bg.displayHeight < this.gh) {
            bg.displayHeight = this.gh;
            bg.scaleX = bg.scaleY;
        }
        Align.center(bg, this);

        this.grid.showPos();

        this.layoutManager = LayoutManager.getInstance(this);
        this.gameScaleObj = this.layoutManager.getScale();
        this.layoutManager.callback = this.onScreenFlipped.bind(this);

        this.card = new FlashCard(this, this.gameScaleObj.cardWidth, this.gameScaleObj.cardHeight);

        this.cardIndex = 0;

        Align.center(this.card, this);

        window['card'] = this.card;

        this.btnNext = this.add.image(0, 0, "btnArrow");
        this.btnPrev = this.add.image(0, 0, "btnArrow");
        this.btnPrev.flipX = true;

        this.btnNext.setInteractive();
        this.btnPrev.setInteractive();

        this.btnNext.on("pointerdown", this.nextCard.bind(this));
        this.btnPrev.on("pointerdown", this.prevCard.bind(this));

        this.btnQuiz = new Button(this, "button", "QUIZ");
        this.btnQuiz.setCallback(this.startQuiz.bind(this));

        this.robot = this.add.sprite(0, 0, "robot");

        this.placeThings();
        this.setCardData();

        this.card.flipCallback = this.onCardFlip.bind(this);
        this.robot.play("idle");
    }
    startQuiz() {
        this.scene.start("SceneQuiz");
    }
    setCardData() {
        let flashCardVo: FlashCardVo = this.gm.cardSetVo.cards[this.cardIndex];
        this.card.setText(flashCardVo.front, flashCardVo.back);
    }
    placeThings() {
        let buttonScale: number = this.layoutManager.getScale().buttonScale;
        Align.scaleToGameW(this.btnNext, buttonScale, this);
        Align.scaleToGameW(this.btnPrev, buttonScale, this);

        let scaleObj: GameScaleObject = this.layoutManager.getScale();
        Align.scaleToGameW(this.robot, scaleObj.robotScale, this);



        let pos: GamePosObj = this.layoutManager.getPos();

        this.grid.placeAt(pos.btnNext.x, pos.btnNext.y, this.btnNext);
        this.grid.placeAt(pos.btnPrev.x, pos.btnPrev.y, this.btnPrev);

        this.grid.placeAt(pos.cardPosition.x, pos.cardPosition.y, this.card);
        this.grid.placeAt(pos.btnQuiz.x, pos.btnQuiz.y, this.btnQuiz);

        let rPos:PosVo=this.grid.getRealXY(pos.robotPos.x,pos.robotPos.y);
        this.robot.x=rPos.x;
        this.robot.y=rPos.y;

        //this.grid.placeAt(pos.robotPos.x, pos.robotPos.y, this.robot);

    }
    onCardFlip() {
        this.playSound("whoosh");
        let pos: GamePosObj = this.layoutManager.getPos();
        let targetPos: PosVo = pos.robotPos;

        if (this.card.frontShown == true) {
            targetPos = pos.robotPos2;
        }
        if (this.card.frontShown === true) {
            this.robot.flipX = false;
        }
        else {
            this.robot.flipX = true;
        }
        console.log(targetPos);

        let tx: number = this.grid.getRealXY(targetPos.x, targetPos.y).x;
        this.robot.play("run");

        this.tweens.add({ targets: this.robot, duration: this.card.flipSpeed*2, x: tx, onComplete: this.runDone.bind(this) });

    }
    runDone() {
        if (this.card.frontShown === true) {
            this.robot.flipX = false;
        }
        else {
            this.robot.flipX = true;
        }
        this.robot.play("idle");
    }
    onScreenFlipped() {
        
        this.gameScaleObj = this.layoutManager.getScale();
        this.card.setCardSize(this.gameScaleObj.cardWidth, this.gameScaleObj.cardHeight);
        this.placeThings();
    }
    nextCard() {
        if (this.clickLock == true) {
            return;
        }
       
        if (this.cardIndex < this.gm.cardSetVo.cards.length - 1) {
            this.cardIndex++;
            this.robot.play("shoot");
            this.moveCardLeft();
        }
    }
    prevCard() {
        if (this.clickLock == true) {
            return;
        }
       
        if (this.cardIndex > 0) {
            this.cardIndex--;
            this.robot.play("shoot");
            this.moveCardRight();
        }
    }
    moveCardLeft() {
        this.clickLock = true;
        this.cardX = this.card.x;
        this.tweens.add({ targets: this.card, duration: 250, x: 0, onComplete: this.moveLeftDone.bind(this) })

    }
    moveLeftDone() {
        this.setCardData();
        this.card.x = this.gw;
        this.tweens.add({
            targets: this.card, duration: 250, x: this.cardX, onComplete: () => {
                this.clickLock = false;
                this.robot.play("idle");
            }
        })
    }
    moveCardRight() {
        this.clickLock = true;
        this.cardX = this.card.x;
        this.tweens.add({ targets: this.card, duration: 250, x: this.gw, onComplete: this.moveRightDone.bind(this) })

    }
    moveRightDone() {
        this.setCardData();
        this.card.x = 0;
        this.tweens.add({
            targets: this.card, duration: 250, x: this.cardX, onComplete: () => {
                this.clickLock = false;
                this.robot.play("idle");
            }
        })
    }
}