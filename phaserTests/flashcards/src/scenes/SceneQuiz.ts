import { Game, GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import { GM } from "../classes/GM";
import { GamePosObj } from "../classes/layout/GamePosObj";
import { GameScaleObject } from "../classes/layout/GameScaleObj";
import { LayoutManager } from "../classes/layout/layoutManager";
import { FlashCardVo } from "../dataObjs/FlashCardVo";
import { QuestVo } from "../dataObjs/QuestVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneQuiz extends BaseScene
{
    private questText:GameObjects.Text;
    private buttonArray:Button[]=[];
    private questIndex:number=-1;
    private clickLock:boolean=false;
    private currentQuestion:QuestVo;

    private gm:GM=GM.getInstance();

    private questions:QuestVo[]=[];
    private answers:string[]=[];

    private layoutManager:LayoutManager;
    private correct:number=0;
    private wrong:number=0;

    private robot:GameObjects.Sprite;
    
    constructor()
    {
        super("SceneQuiz");
    }
    preload()
    {
        this.load.image("button","./assets/ui/buttons/1/1.png");     
    }
    create()
    {
        super.create();
        this.makeGrid(11,11);
        //this.grid.showPos();

        this.questIndex=-1;
        this.buttonArray=[];
        
        let bg:GameObjects.Image=this.add.image(0,0,"bg");
        bg.displayWidth=this.gw;
        bg.scaleY=bg.scaleX;

        if (bg.displayHeight<this.gh)
        {
            bg.displayHeight=this.gh;
            bg.scaleX=bg.scaleY;
        }
        Align.center(bg,this);

        this.layoutManager=LayoutManager.getInstance(this);
        this.layoutManager.callback=this.flipped.bind(this);

        this.questText = this.add.text(0,0,"Question",{fontSize:"36px",color:"white",fontFamily:"Arial"}).setOrigin(0.5,0.5);
        //
        //make buttons
        //
        for (let i = 0; i < 4; i++) {
            let btnAnswer = new Button(this,"button","answer");
            btnAnswer.index=i;
            this.grid.placeAt(5,i+2, btnAnswer);
            btnAnswer.setCallback(this.pickAnswer.bind(this));
            this.buttonArray.push(btnAnswer);
        }

        this.robot=this.add.sprite(0,0,"robot");
        this.robot.play("idle");

        this.placeThings();
        this.makeQuestions();

        this.getNext();
    }
    placeThings()
    {
        let buttonScale:number=this.layoutManager.getScale().buttonScale;
        let pos:GamePosObj=this.layoutManager.getPos();
        let scaleObj:GameScaleObject=this.layoutManager.getScale();

        for (let i:number=0;i<this.buttonArray.length;i++)
        {
           // Align.scaleToGameW(this.buttonArray[i],buttonScale,this);
            this.grid.placeAt(pos.quizStart.x,pos.quizStart.y+i*1.5,this.buttonArray[i]);
        }
       
        this.grid.placeAt(pos.quizText.x,pos.quizText.y,this.questText);       

        Align.scaleToGameW(this.robot,scaleObj.robotScale,this);
        this.grid.placeAt(pos.quizRobotPos.x,pos.quizRobotPos.y,this.robot);
    }
    flipped()
    {

    }
    makeQuestions()
    {
        let cards:FlashCardVo[]=this.gm.cardSetVo.cards;
        let question:string=this.gm.cardSetVo.question;
        let answer:string=this.gm.cardSetVo.answer;

        for (let i:number=0;i<cards.length;i++)
        {
            let card:FlashCardVo=cards[i];
            
            let a2:string=answer.replace("[front]",card.front);
            a2=a2.replace("[back]",card.back);

            let q2:string=question.replace("[front]",card.front);
            q2=q2.replace("[back]",card.back);

            let questVo:QuestVo=new QuestVo(q2,a2);
            //console.log(questVo);
            this.questions.push(questVo);
        }

        this.questions=this.mixUpArray(this.questions);
    }
    pickWrongAnswers()
    {
        let indexes:number[]=[];
        for (let i:number=0;i<this.questions.length;i++)
        {
            if (i!=this.questIndex)
            {
                indexes.push(i);
            }            
        }

        indexes=this.mixUpArray(indexes);

        let wrongs:string[]=[];

        for (let i:number=0;i<3;i++)
        {
            let answer:string=this.questions[indexes[i]].a;
            wrongs.push(answer);
        }

        return wrongs;

    }
    flyInButtons()
    {
        for (let i:number=0;i<this.buttonArray.length;i++)
        {
            let button:Button=this.buttonArray[i];
            let bx:number=button.x;
            button.x=this.gw+500;
            button.visible=true;
            button.alpha=1;
            this.tweens.add({targets:button,x:bx,duration:200,delay:i*100});
        }
    }

    getNext()
    {
        this.questIndex++;
        if (this.questIndex>10)
        {
            //console.log("out of questions");
            this.gm.correct=this.correct;
            this.gm.wrong=this.wrong;
            this.scene.start("SceneStats");
            return;
        }

        if (this.questIndex>this.questions.length-1)
        {
            this.gm.correct=this.correct;
            this.gm.wrong=this.wrong;
            //console.log("out of questions");
            this.scene.start("SceneStats");
            return;
        }
        //console.log("get next");

        this.clickLock=false;
        this.currentQuestion=this.questions[this.questIndex];
        this.answers=this.pickWrongAnswers();
        
        this.answers.push(this.currentQuestion.a);

        this.answers=this.mixUpArray(this.answers);

        this.questText.setText(this.currentQuestion.q);

        for (let i:number=0;i<4;i++)
        {
            this.buttonArray[i].visible=false;
            this.buttonArray[i].text1.setText(this.answers[i]);
            this.buttonArray[i].text1.setColor("#ffffff");
        }
        this.flyInButtons();
    }
    mixUpArray(array) {
        let len = array.length;
        for (let i = 0; i < 10; i++) {
            let p1 = Phaser.Math.Between(0, len - 1);
            let p2 = Phaser.Math.Between(0, len - 1);
            let temp = array[p1];
            array[p1] = array[p2];
            array[p2] = temp;
        }
        return array;
    }
    pickAnswer(index:number)
    {
        if (this.clickLock==true)
        {
            return;
        }
        this.clickLock=true;

        this.playSound("pop");

        for (let i:number=0;i<this.buttonArray.length;i++)
            {
                let button:Button=this.buttonArray[i];

                if (this.buttonArray[i].text1.text!=this.currentQuestion.a)
                {
                    this.tweens.add({targets:button,alpha:0,duration:200});

                  //  this.buttonArray[i].text1.setColor("#ffff00");
                                      
                }
            }

        //console.log(this.answers[index]);
        if (this.answers[index]==this.currentQuestion.a)
        {
            //console.log("correct");
           this.correct++;
           this.robot.play("jump");
           this.playSound("rightSound");
        }
        else
        {
            //console.log("wrong");
            this.robot.play("die");
            this.playSound("wrongSound");
            this.wrong++;
        }
        setTimeout(() => {
            this.getNext();
           this.robot.play("idle");
        }, 2000);
    }
}