import { GM } from "../classes/GM";
import { CardSetVo } from "../dataObjs/CardSetVo";
import { FlashCardVo } from "../dataObjs/FlashCardVo";
import { BaseScene } from "./BaseScene";

export class SceneData extends BaseScene {
    private gm: GM = GM.getInstance();

    constructor() {
        super("SceneData");

    }
    create() {
        super.create();

        fetch("./assets/statecaps.json")
            .then(response => response.json())
            .then(data => this.process(data));
    }
    process(data: any) {
        let cards: any = data.cards;
        let question: any = data.question;
        let answer: any = data.answer;

        let cardsVo: FlashCardVo[] = [];

        for (let i: number = 0; i < cards.length; i++) {
            cardsVo.push(new FlashCardVo(cards[i].front, cards[i].back));
        }

        let cardSet: CardSetVo = new CardSetVo(cardsVo, question, answer);

        this.gm.cardSetVo = cardSet;

        this.scene.start("SceneMain");
    }
}