import { GameObjects } from "phaser";
import { Stats } from "webpack";
import { Button } from "../classes/comps/Button";
import { GM } from "../classes/GM";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

 export class SceneStats extends BaseScene
 {
     private text1:GameObjects.Text;
     private btnPlay:Button;
     private gm:GM=GM.getInstance();

     constructor()
     {
         super("SceneStats");
     }
     create()
     {
        super.create();
        this.makeGrid(11,11);

        let bg:GameObjects.Image=this.add.image(0,0,"bg");
        bg.displayWidth=this.gw;
        bg.scaleY=bg.scaleX;

        if (bg.displayHeight<this.gh)
        {
            bg.displayHeight=this.gh;
            bg.scaleX=bg.scaleY;
        }
        Align.center(bg,this);

        let per:number=this.gm.correct/(this.gm.correct+this.gm.wrong)*100;

        per=Math.floor(per*100)/100;

        let statString:string="You answered "+this.gm.correct.toString()+" correctly";
        statString+="\nand "+this.gm.wrong.toString()+" incorrectly";
        statString+="\nYou answered "+per.toString()+"% correct";

        this.text1=this.add.text(0,0,statString,{fontFamily:"Arial",fontSize:"26px","backgroundColor":"white",color:"black"}).setOrigin(0.5,0.5);
        
        this.grid.placeAt(5,4,this.text1);

        this.btnPlay=new Button(this,"button","Back to Cards");
        this.btnPlay.setCallback(this.playAgain.bind(this));
        this.grid.placeAt(5,7,this.btnPlay);
        
     }
     playAgain()
     {
         this.scene.start("SceneMain");
     }
 }