import { GameObjects } from "phaser";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene {
    constructor() {
        super("SceneLoad");
    }
    preload() {
        this.load.image("btnArrow", "./assets/btnArrow.png");
        this.load.image("button", "./assets/ui/buttons/1/1.png");
        this.load.image("bg", "./assets/quizBG.jpg");
        this.load.image("bg1", "./assets/bg1.jpg");
        this.load.atlas("robot", "./assets/robot.png", "./assets/robot.json");

        this.load.audio("rightSound","./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");
        this.load.audio("pop","./assets/audio/pop.wav");
        this.load.audio("whoosh","./assets/audio/whoosh.mp3");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        

        let idleFrames = this.anims.generateFrameNames('robot', { start: 1, end: 10, zeroPad: 0, prefix: 'Idle (', suffix: ').png' });
        this.anims.create({
            key: 'idle',
            frames: idleFrames,
            frameRate: 8,
            repeat: -1
        });

        let walkFrames=this.anims.generateFrameNames('robot', { start: 1, end: 8, zeroPad: 0, prefix: 'Run (', suffix: ').png' });

        this.anims.create({
            key: 'run',
            frames: walkFrames,
            frameRate: 8,
            repeat: -1
        });

        let jumpFrames=this.anims.generateFrameNames('robot', { start: 1, end: 5, zeroPad: 0, prefix: 'JumpShoot (', suffix: ').png' });

        this.anims.create({
            key: 'jump',
            frames: jumpFrames,
            frameRate: 8,
            repeat: -1
        });

        let shootFrames=this.anims.generateFrameNames('robot', { start: 1, end: 4, zeroPad: 0, prefix: 'Shoot (', suffix: ').png' });

        this.anims.create({
            key: 'shoot',
            frames: shootFrames,
            frameRate: 8,
            repeat: 0
        });

        let deadFrames=this.anims.generateFrameNames('robot', { start: 1, end: 10, zeroPad: 0, prefix: 'Dead (', suffix: ').png' });

        this.anims.create({
            key: 'die',
            frames: deadFrames,
            frameRate: 8,
            repeat: 0
        });


        this.scene.start("SceneData");
        
    }
}