import { GameObjects } from "phaser";
import { PosVo } from "../../dataObjs/PosVo";
import IBaseScene from "../../interfaces/IBaseScene";
import { GM } from "../GM";

import {  GameFontSizes } from "./GameFontSizes";
import { GamePosObj } from "./GamePosObj";
import { GameScaleObject } from "./GameScaleObj";


export class LayoutManager {
    public scene: Phaser.Scene;
    
    private gm: GM = GM.getInstance();
    private bscene: IBaseScene;
    
    private static instance:LayoutManager;


    public callback: Function = () => { };

    constructor(bscene: IBaseScene) {
        this.scene = bscene.getScene();
        this.bscene = bscene;

        window.addEventListener("orientationchange", function () {
            console.log(window.innerWidth, window.innerHeight)
            this.flipped();
        }.bind(this));
    }
    public static getInstance(bscene:IBaseScene)
    {
        if (this.instance==null)
        {
            this.instance=new LayoutManager(bscene);
        }
        return this.instance;
    }
    private flipped() {
        console.log('flipped');
        setTimeout(() => {
            let w: number = window.innerWidth;
            let h: number = window.innerHeight;
            if (w < h) {
                this.gm.isPort = true;
            }
            else {
                this.gm.isPort = false;
            }
            this.bscene.resetSize(w, h);
            this.scene.scale.resize(w, h);
            this.bscene.getGrid().hide();
            this.bscene.makeGrid(11, 11);
            this.callback();
        }, 1000);

    }
   
    public getPos() {

        let messagePosition: PosVo = new PosVo(5, 1);
        let cardPos = new PosVo(5, 3);
        let btnPrev=new PosVo(3, 7);
        let btnNext = new PosVo(7,7);
        let btnQuiz=new PosVo(5,7);
        let quizText=new PosVo(5,1);
        let robotPos=new PosVo(2.5,3);
        let robotPos2=new PosVo(8.5,3);
        let quizRobotPos:PosVo=new PosVo(3,6);

        let quizStart=new PosVo(5,2);

        if (this.gm.isMobile === true && this.gm.isPort === false) {
            cardPos=new PosVo(5,4);
            btnPrev=new PosVo(2,9);
            btnNext=new PosVo(8,9);
            btnQuiz=new PosVo(5,9);
        }
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            cardPos=new PosVo(5,3);
            btnPrev=new PosVo(2,6);
            btnNext=new PosVo(8,6);
            btnQuiz=new PosVo(5,6);
        }
        if (this.gm.isTablet && this.gm.isPort === false) {

        }
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            cardPos=new PosVo(5,3.5);
            btnPrev=new PosVo(2,8);
            btnNext=new PosVo(8,8);
            btnQuiz=new PosVo(5,8);
        }
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            cardPos=new PosVo(5,3);
            btnPrev=new PosVo(2,7);
            btnNext=new PosVo(8,7);
            btnQuiz=new PosVo(5,7);
        }

        let gamePosObj: GamePosObj = new GamePosObj(messagePosition, cardPos,btnPrev,btnNext,btnQuiz,quizStart,quizText);
        gamePosObj.robotPos=robotPos;
        gamePosObj.robotPos2=robotPos2;
        gamePosObj.quizRobotPos=quizRobotPos;

        return gamePosObj;
    }
    public getScale() {

         //desktop

        let cardWidth: number = 0.5;
        let cardHeight: number = 0.4;
        let buttonScale: number =0.075;      
        let robotScale:number=0.1;
        
        //mobile landscape
        if (this.gm.isMobile === true && this.gm.isPort === false) {
            cardWidth = 0.8;
            cardHeight=0.7;
            buttonScale=0.09;
        }

        //mobile portrait
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            cardWidth = 0.8;
            cardHeight=0.3;
            buttonScale=0.15;
            
        }
        //tablet landscape
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            cardWidth = 0.8;
            cardHeight = 0.6;
            
        }
        //tablet portrait
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            cardWidth = 0.8;
            cardHeight = 0.45;
            buttonScale=0.2;
        }


        return new GameScaleObject(cardWidth, cardHeight,buttonScale,robotScale);

    }
    public getFontSizes() {

        let coinTextSize:number =52;
        let messageTextSize:number =52;
        let buttonTextSize:number =25;

       
        if (this.gm.isMobile === true && this.gm.isPort === false) {
            buttonTextSize=18;
        }
        if (this.gm.isMobile === true && this.gm.isPort === true) {
            buttonTextSize=24;
            coinTextSize=36;
            messageTextSize=40;
        }
        if (this.gm.isTablet && this.gm.isPort === false) {
            
        }
        if (this.gm.isTablet === true && this.gm.isPort === true) {
            buttonTextSize=30;
        }
        if (this.gm.isTablet === true && this.gm.isPort === false) {
            buttonTextSize=25;
        }
       

        let gameFontSizes:GameFontSizes = new GameFontSizes(coinTextSize, buttonTextSize,messageTextSize);
        return gameFontSizes
    }
}