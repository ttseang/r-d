import { PosVo } from "../../dataObjs/PosVo";

export class GamePosObj
{
    public messagePosition:PosVo
    public cardPosition:PosVo;
    public btnNext:PosVo;
    public btnPrev:PosVo;
    public btnQuiz:PosVo;
    public quizStart:PosVo;
    public quizText:PosVo;

    public robotPos:PosVo;
    public robotPos2:PosVo;
    public quizRobotPos:PosVo;

    constructor(messagePosition:PosVo,cardPosition: PosVo,btnPrev: PosVo,btnNext: PosVo,btnQuiz:PosVo,quizStart:PosVo,quizText:PosVo)
    {
        this.messagePosition=messagePosition;
        this.cardPosition=cardPosition;
        this.btnPrev=btnPrev;
        this.btnNext=btnNext;
        this.btnQuiz=btnQuiz;
        this.quizStart=quizStart;
        this.quizText=quizText;
    }
}