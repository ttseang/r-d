export class GameScaleObject 
{
    public cardWidth: number;
    public cardHeight: number;
    public buttonScale: number;
    public robotScale:number;

    constructor(cardWidth:number,cardHeight:number,buttonScale:number,robotScale:number)
    {
        this.cardWidth=cardWidth;
        this.cardHeight=cardHeight;
        this.buttonScale=buttonScale;
        this.robotScale=robotScale;
    }
}