import { GameObjects, Tweens } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";

export class FlashCard extends GameObjects.Container
{
    private back:GameObjects.Graphics;
    public flipSpeed:number=350;
    private text1:GameObjects.Text;
    public frontShown:boolean=true;

    private bscene:IBaseScene;

    private frontText:string="FRONT TEXT";
    private backText:string="BACK TEXT";

    public flipCallback:Function=()=>{};

    constructor(bscene:IBaseScene,wscale:number,hscale:number)
    {
        super(bscene.getScene());
        this.bscene=bscene;

        let w:number=bscene.getW()*wscale;
        let h:number=bscene.getH()*hscale;

        this.back=this.scene.add.graphics();
        this.back.fillStyle(0xffffff,1);
        this.back.fillRoundedRect(-w/2,-h/2,w,h,8);
        
        this.add(this.back);

        this.text1=this.scene.add.text(0,0,"Front",{color:"black",fontFamily:"Arial",fontSize:"32px",align:"center"}).setOrigin(0.5,0.5);
        this.add(this.text1);

        this.setSize(w,h);
        this.scene.add.existing(this);
        
        this.setInteractive();
        this.on("pointerdown",this.flip.bind(this));
        
    }
    flip()
    {
        this.flipCallback();
        let tl:Phaser.Tweens.Timeline=this.scene.tweens.createTimeline();

        tl.add({ targets: this, scaleX: 0, duration: this.flipSpeed });

        setTimeout(() => {
            this.frontShown=!this.frontShown;
            if (this.frontShown==true)
            {
                this.text1.setText(this.frontText);
            }
            else
            {
                this.text1.setText(this.backText);
            }
        }, this.flipSpeed);

        tl.add({ targets: this, scaleX: 1, duration: this.flipSpeed });
        tl.on("complete",()=>{
            //console.log("done2");
            
        })
        tl.play();
    }
    setCardSize(wscale:number,hscale:number)
    {
        let w:number=this.bscene.getW()*wscale;
        let h:number=this.bscene.getH()*hscale;

        this.back.clear();
        this.back.fillStyle(0xffffff,1);
        this.back.fillRoundedRect(-w/2,-h/2,w,h,8);
    }
    setText(f:string,b:string)
    {
        this.backText=b;
        this.frontText=f;
        if (this.frontShown==true)
        {
            this.text1.setText(f);
        }
        else
        {
            this.text1.setText(b);
        }
       
    }
}