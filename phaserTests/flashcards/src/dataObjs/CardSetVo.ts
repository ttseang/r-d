import { FlashCardVo } from "./FlashCardVo";

export class CardSetVo
{
    public cards:FlashCardVo[]=[];
    public question:string;
    public answer:string;

    constructor(cards:FlashCardVo[],question:string,answer:string)
    {
        this.cards=cards;
        this.question=question;
        this.answer=answer;
    }
}