export class FlashCardVo
{
    public front:string;
    public back:string;

    constructor(front:string,back:string)
    {
        this.front=front;
        this.back=back;
    }
}