import { PageObj } from "./PageObj";

export class PageVo
{
    public backgroundKey:string="";
    public pageObjs:PageObj[]=[];
    
    constructor(backgroundKey:string)
    {
        this.backgroundKey=backgroundKey;
    }
} 