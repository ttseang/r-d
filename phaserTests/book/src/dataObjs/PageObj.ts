export class PageObj
{
    public static TYPE_TEXT:string="typeText";
    public static TYPE_IMAGE:string="typeImage";

    public type:string;
    public data:string;
    public xx:number;
    public yy:number;

    constructor(type:string,data:string,xx:number,yy:number)
    {
        this.type=type;
        this.data=data;
        this.xx=xx;
        this.yy=yy;
    }
}