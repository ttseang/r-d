import { GameObjects } from "phaser";
import { PageObj } from "../dataObjs/PageObj";
import { PageVo } from "../dataObjs/PageVo";
import IBaseScene from "../interfaces/IBaseScene";

export class Page extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private back:GameObjects.Image;
    private pageVo:PageVo;
    private right:boolean=false;

    constructor(bscene:IBaseScene,pageVo:PageVo,right:boolean)
    {
        super(bscene.getScene());

        this.scene=bscene.getScene();
        this.bscene=bscene;
        this.back=this.scene.add.image(0,0,pageVo.backgroundKey);
        this.right=right;
        //
        //
        //
        this.pageVo=pageVo;

        if (right==true)
        {
            this.back.setOrigin(0,0);
        }
        else
        {
            this.back.setOrigin(1,0);
        }
       
        this.back.displayWidth=this.bscene.getW()/2;
        this.back.scaleY=this.back.scaleX;

        if (this.back.displayHeight>this.bscene.getH()*.6)
        {
            this.back.displayHeight=this.bscene.getH()*0.6;
            this.back.scaleX=this.back.scaleY;
        }

        this.add(this.back);
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);
    }
    public build()
    {
      
        let objs:PageObj[]=this.pageVo.pageObjs;
        for (let i:number=0;i<objs.length;i++)
        {
            console.log("add "+objs[i].type);
            let obj:PageObj=objs[i];

            switch(obj.type)
            {
                case PageObj.TYPE_TEXT:
                let text:GameObjects.Text=this.scene.add.text(0,0,obj.data,{color:"black",fontSize:"20px",fontFamily:"Arial"});
                text.setOrigin(0.5,0.5);

                console.log(this.back.displayWidth,this.back.displayHeight);

                if (this.right==false)
                {
                    text.x=-this.back.displayWidth+(this.back.displayWidth*obj.xx);
                    text.y=this.back.displayHeight*obj.yy;
                    
                }
                else
                {
                    text.x=this.back.displayWidth*obj.xx;
                    text.y=this.back.displayHeight*obj.yy;
                }

                window['text']=text;
                window['p2']=this;
                /* text.x=this.back.displayWidth*obj.xx;
                text.y=this.back.displayHeight*obj.yy; */

                this.add(text);
                break;
            }
        }
    }   
}