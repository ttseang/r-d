import { PageVo } from "../dataObjs/PageVo";
import IBaseScene from "../interfaces/IBaseScene";
import { Page } from "./page";

export class Book extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;

    public rpages: Page[] = [];
    public lpages: Page[] = [];
    public pageIndex: number = 0;

    private currentLeft: Page;
    private currentRight: Page;

    private forwardFlag: boolean = false;
    private backFlag: boolean = false;

    private right:PageVo[]=[];
    private left:PageVo[]=[];
    public lockFlag:boolean=false;
    public flipSpeed:number=200;

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;

        this.scene.add.existing(this);
    }
    turnForward() {
        if (this.lockFlag==true)
        {
            return;
        }
        console.log("forward");
        this.lockFlag=true;
        if (this.backFlag == true) {
            this.backFlag = false;
            this.pageIndex++;
        }
              

        if (this.pageIndex == this.rpages.length) {
            console.log("at the end");
            
         //   this.pageIndex = this.rpages.length - 1;
            this.lockFlag=false;
            return;
        }
        this.currentLeft = this.lpages[this.pageIndex];
        this.currentRight = this.rpages[this.pageIndex];

        this.bringToTop(this.currentLeft);
        // this.scene.children.bringToTop(this.currentRight);

        this.currentRight.localTransform.setTransform(0,200,0,0,0,0);

        //
        //
        //
        let tl: Phaser.Tweens.Timeline = this.scene.tweens.createTimeline();

        tl.add({ targets: this.currentRight, scaleX: 0, duration: this.flipSpeed });
        tl.add({ targets: this.currentLeft, scaleX: 1, duration: this.flipSpeed });
        tl.on("complete",()=>{
            console.log("done2");
            this.lockFlag=false;
        })
        tl.play();

       

        this.pageIndex++;

        this.forwardFlag = true;
    }
    turnBack() {
        if (this.lockFlag==true)
        {
            return;
        }
        this.lockFlag=true;

        console.log("back");

        if (this.forwardFlag == true) {
            this.forwardFlag = false;
            this.pageIndex--;
        }

        if (this.pageIndex < 0) {
            this.lockFlag=false;
            return;
        }
        this.currentLeft = this.lpages[this.pageIndex];
        this.currentRight = this.rpages[this.pageIndex];

        this.bringToTop(this.currentRight);

        let tl: Phaser.Tweens.Timeline = this.scene.tweens.createTimeline();

        tl.add({ targets: [this.currentLeft], scaleX: 0, duration: this.flipSpeed });
        tl.add({ targets: this.currentRight, scaleX: 1, duration: this.flipSpeed });

        tl.on("complete",()=>{
            console.log("done");
            this.lockFlag=false;
        })

        tl.play();

        this.pageIndex--;

        this.backFlag = true;
    }
    makePages(right: PageVo[], left: PageVo[]) {
        let pw: number = 0;
        let ph: number = 0;

        this.right=right;
        this.left=left;

        for (let i: number = right.length - 1; i > -1; i--) {
            let page: Page = new Page(this.bscene, right[i], true);
            page.x = 0;
            this.rpages.unshift(page);

            pw = page.displayWidth;
            ph = page.displayHeight;
            page.build();
            this.add(page);
        }

        for (let i: number = left.length - 1; i > -1; i--) {
            let page2: Page = new Page(this.bscene, left[i], false);
            page2.x = 0;

            page2.scaleX = 0;
            page2.build();
            this.add(page2);
            this.scene.children.sendToBack(page2);
            this.lpages.unshift(page2);
        }

          this.setSize(pw * 2, ph);
    }
    
}