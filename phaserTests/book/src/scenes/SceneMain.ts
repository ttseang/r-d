import { GameObjects } from "phaser";
import { Book } from "../classes/book";
import { Page } from "../classes/page";
import { PageObj } from "../dataObjs/PageObj";
import { PageVo } from "../dataObjs/PageVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    
    public book:Book

    private bookDataLeft:PageVo[]=[];
    private bookDataRight:PageVo[]=[];

    constructor() {
        super("SceneMain");
    }
    preload() {
        for (let i:number=1;i<7;i++)
        {
            this.load.image("page"+i.toString(), "./assets/page"+i.toString()+".jpg");
        }
        this.load.image("paper","./assets/paper.jpg");
        this.load.image("grey","./assets/grey.jpg");
        this.load.image("circles","./assets/circles.jpg");
        this.load.image("triangles","./assets/triangles.jpg");
        this.load.image("arrow","./assets/arrow.png");

    }
    create() {
        super.create();
        this.makeGrid(11,11);
        //this.grid.showNumbers();

        let testPage:PageVo=new PageVo("paper");
        testPage.pageObjs.push(new PageObj(PageObj.TYPE_TEXT,"This page intentionally left blank",0.5,0.5));
        testPage.pageObjs.push(new PageObj(PageObj.TYPE_IMAGE,"pic1",.5,0.25));

        let testPage2:PageVo=new PageVo("grey");
        testPage2.pageObjs.push(new PageObj(PageObj.TYPE_TEXT,"This one was an accident",0.5,0.5));
        testPage2.pageObjs.push(new PageObj(PageObj.TYPE_IMAGE,"pic1",.5,0.25));


        this.bookDataLeft=[new PageVo("page1"),testPage2,new PageVo("page3"),new PageVo("page5")];
        this.bookDataRight=[new PageVo("page2"),testPage,new PageVo("page4"),new PageVo("page6")];
        

        this.book=new Book(this);
        this.book.makePages(this.bookDataRight,this.bookDataLeft);       
        
        Align.center(this.book,this);

        this.book.y=100;

        let leftArrow:GameObjects.Image=this.add.image(0,0,"arrow");
        leftArrow.flipX=true;
        leftArrow.setInteractive();
        leftArrow.on("pointerdown",()=>{this.book.turnBack()});
        this.grid.placeAt(3,9,leftArrow);

        let rightArrow:GameObjects.Image=this.add.image(0,0,"arrow");
        rightArrow.setInteractive();
        rightArrow.on("pointerdown",()=>{this.book.turnForward();})
       
        this.grid.placeAt(7,9,rightArrow);
        
        window['scene']=this;
    }
}