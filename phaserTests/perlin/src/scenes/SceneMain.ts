import { GameObjects } from "phaser";
import Perlin from "phaser3-rex-plugins/plugins/utils/math/noise/Perlin";
import Noise from "phaser3-rex-plugins/plugins/utils/math/noise/Perlin";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private tiles:GameObjects.Sprite[]=[];

    constructor() {
        super("SceneMain");
    }
    preload() {
       this.load.spritesheet("exp","./assets/explosion.jpg",{frameWidth:18,frameHeight:16});
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        let noiseObj:any = this.plugins.get('rexPerlin');
        let noise:Noise=noiseObj.add(Math.random()*100);
        

        for (let i:number=0;i<100;i++)
        {
            for (let j:number=0;j<20;j++)
            {
                let p=noise.perlin2(i/10,j/10);
                p=Math.floor(p*10);

               console.log(p);

                let frame:number=9+p;
               // console.log(frame);
                let img:GameObjects.Sprite=this.add.sprite(i*18,j*16,"exp");
                this.tiles.push(img);
                img.setFrame(frame);
            }
        }
        this.input.on("pointerdown",this.advanceFrames.bind(this));
    }
    advanceFrames()
    {
        for (let i:number=0;i<this.tiles.length;i++)
        {
            let tile:GameObjects.Sprite=this.tiles[i];
            let frame:number=parseInt(tile.frame.name);
          //  console.log(tile.frame.name);
             frame++;
            if (frame>14)
            {
                frame=5;
            }
            tile.setFrame(frame);
        }
    }
}