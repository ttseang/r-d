import { Quiz } from "../classes/ui/Quiz";
import { QuizBox } from "../classes/ui/QuizBox";
import { QuizButton } from "../classes/ui/QuizButton";
import { AnswerVo } from "../dataObjs/AnswerVo";
import { QuizButtonVo } from "../dataObjs/QuizButtonVo";
import { QuestionVo } from "../dataObjs/QuestionVo";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";
import { GM } from "../classes/GM";
import { Background } from "../classes/Background";

export class SceneMain extends BaseScene {

    private quiz: Quiz;
    private gm:GM=GM.getInstance();
    private background:Background;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("arrow", "./assets/arrow.png");
        this.load.image("nextArrow", "./assets/btnNext.png");
        this.load.image("bg","./assets/bg.jpg");

        for (let i:number=0;i<this.gm.images.length;i++)
        {
            let key:string="imageKey"+i.toString();
            this.load.image(key,"assets/quizImages/"+this.gm.images[i]);
        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.background=new Background(this,"bg");
        
        this.quiz=new Quiz(this,this.gm.questions);
          


    }
}