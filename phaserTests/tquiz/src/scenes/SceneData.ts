import { GM } from "../classes/GM";
import { AnswerVo } from "../dataObjs/AnswerVo";
import { QuestionVo } from "../dataObjs/QuestionVo";
import { BaseScene } from "./BaseScene";

export class SceneData extends BaseScene {
    private gm:GM=GM.getInstance();

    constructor() {
        super("SceneData");
    }
    create() {
        fetch("./assets/quizData.json")
            .then(response => response.json())
            .then(data => this.process(data));
    }
    process(data: any) {
        // console.log(data);

        let questions: QuestionVo[] = [];
        let images:string[]=[];

        for (let i: number = 0; i < data.questions.length; i++) {
            let question = data.questions[i];

            let answers = question.answers;
            // console.log(answers);

            for (let j: number = 0; j < answers.length; j++) {
                let answer = answers[j];
                let answerVo: AnswerVo = new AnswerVo(j, answer.text, answer.correct);
            }
            images.push(question.image);
            let key:string="imageKey"+i.toString();
            let questionVo: QuestionVo = new QuestionVo(question.question, answers, key, question.wrongMsg, question.rightMsg, question.explainText);
            //console.log(questionVo);
            questions.push(questionVo);
        }
        this.gm.questions=questions;
        this.gm.images=images;
        this.scene.start("SceneMain");
    }
}