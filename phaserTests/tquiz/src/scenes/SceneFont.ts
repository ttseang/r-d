//import Phaser from "phaser";

export class SceneFont extends Phaser.Scene {
    constructor() {
        super("SceneFont");
    }
    private fontsLoaded:number=0;
    private fonts:string[]=[];
    private localfonts:string[]=['BarthowheelReg'];
    private urls:string[]=['./assets/fstyle.css'];
    private WebFont:any;

    preload() {        
        this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js');

    }
    create() {
        
        this.fontsLoaded = 0;
        this.fonts = ['Ubuntu'];
        this.game.canvas.id="thecanvas";
        
        //window['scene']=this;
      //  //console.log(WebFont);\
        this.WebFont=(window as any).WebFont;
      
       this.WebFont.load({
         custom:{familes:this.localfonts,urls:this.urls},
         google: { families: this.fonts },
         loading: this.fontsLoading.bind(this), fontactive: this.fontActive.bind(this) });
    }
    fontsLoading() {
        //console.log("font started");
    }
    fontActive() {
        //console.log("font active");
        this.fontsLoaded++;
        //console.log(this.fontsLoaded);
       // let total:number=this.fonts.length+this.localfonts.length;

        if (this.fontsLoaded === this.fonts.length) {
            this.scene.start("SceneData");
            //this.scene.start("SceneMovieData");
        }
    }
}
export default SceneFont;