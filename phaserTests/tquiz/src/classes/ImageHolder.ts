import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "../scenes/BaseScene";
import Align from "../util/align";

export class ImageHolder
{
    private bscene:IBaseScene;
    private scene:Phaser.Scene;
    private image:GameObjects.Image;
    private maskImage:GameObjects.Image;
    private back:GameObjects.Image;

    constructor(bscene:IBaseScene)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();       

    }

    makeImage(key:string,maxWidth:number)
    {
        if (this.back)
        {
            this.back.destroy();
        }
        this.back=this.scene.add.image(0,0,"holder");
        this.back.displayWidth=maxWidth*1.1;
        this.back.displayHeight=maxWidth*1.1;
        this.back.setTint(0x98D8E7);

        if (this.image)
        {
            this.image.destroy();
        }
        this.image=this.scene.add.image(0,0,key);
        Align.scaleToGameW(this.image,0.4,this.bscene);

        if (this.maskImage)
        {
            this.maskImage.destroy();
        }
        this.maskImage=this.scene.add.image(0,0,"holder");
        this.maskImage.displayWidth=maxWidth;
        this.maskImage.displayHeight=maxWidth;

        this.maskImage.visible=false;
        this.image.setMask(new Phaser.Display.Masks.BitmapMask(this.scene,this.maskImage));

       

    }
    setVisible(value:boolean)
    {
        this.image.visible=value;
        this.back.visible=value;
    }
    setPos(xx:number,yy:number)
    {
        this.image.x=xx;
        this.image.y=yy;

        this.maskImage.x=xx;
        this.maskImage.y=yy;

        this.back.x=xx;
        this.back.y=yy;
    }
}