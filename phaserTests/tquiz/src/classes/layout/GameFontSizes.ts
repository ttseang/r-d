export class GameFontSizes
{
    public coinText:number;
    public buttonText:number;
    public messageText:number;

    constructor(coinText:number, buttonText:number, messageText:number)
    {
        this.coinText = coinText;
        this.buttonText = buttonText;
        this.messageText=messageText;
    }
}