import { QuestionVo } from "../dataObjs/QuestionVo";

let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

    public testText:string="";

    public questions:QuestionVo[]=[];
    public images:string[]=[];

    public mainFont:string="Ubuntu";

    constructor()
    {
        window['gm']=this;
        this.testText="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum a aliquet enim, eu sollicitudin nunc. Aenean tincidunt porttitor scelerisque. Duis maximus, elit vitae rutrum hendrerit, ipsum elit commodo orci, id elementum quam velit sit amet lacus. Nulla facilisi. Ut rutrum mi dolor, sed cursus mauris mollis sed. Pellentesque non fermentum nibh. Proin et ligula ut ex volutpat aliquet. Pellentesque quis tempus dolor, sed varius nisi. Fusce finibus finibus erat sed molestie. Sed ornare, felis id blandit porttitor, diam dolor consectetur risus, ac mattis felis libero a ex. Sed eget tellus eget mauris porttitor sagittis. Donec ornare ante non tellus luctus, sit amet volutpat dolor vestibulum. Nullam volutpat massa sagittis vulputate imperdiet. In et nisi eleifend, tristique metus vitae, pellentesque est. Nullam et porttitor turpis.";
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}