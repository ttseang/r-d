import { GameObjects } from "phaser"
import { AnswerVo } from "../../dataObjs/AnswerVo";
import { QuizButtonVo } from "../../dataObjs/QuizButtonVo";
import { QuestionVo } from "../../dataObjs/QuestionVo";
import IBaseScene from "../../interfaces/IBaseScene";
import Align from "../../util/align";
import { QuizButton } from "./QuizButton";
import { GM } from "../GM";

export class QuizBox extends GameObjects.Container {
    private questionVo: QuestionVo;
    private back: GameObjects.Image;
    private bscene: IBaseScene;
    private buttons:QuizButton[]=[];
    public selectedIndex:number=-1;

    private gm:GM=GM.getInstance();

    private btnArrow:GameObjects.Image;
    private qtext:GameObjects.Text;

    public selectCallback:Function=()=>{};

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());

        this.bscene = bscene;

        this.back = this.scene.add.image(0, 0, "holder").setOrigin(0, 0);
        this.back.displayHeight = this.bscene.getH() * 0.9;
        this.back.displayWidth =this.bscene.getW()*0.4;

        this.add(this.back);

        this.scene.add.existing(this);

        this.btnArrow=this.scene.add.image(0,0,"arrow");
        Align.scaleToGameW(this.btnArrow,0.1,this.bscene);
        this.add(this.btnArrow);
        this.btnArrow.setInteractive();
        this.btnArrow.on("pointerdown",this.confirmAnswer.bind(this));

        this.qtext=this.scene.add.text(0,0,"",{color:"#000000",fontFamily:this.gm.mainFont,fontSize:"30px"}).setOrigin(0.5,0.5);
        this.qtext.y=this.back.displayHeight*0.1;
        this.qtext.x=this.back.displayWidth/2;

        this.add(this.qtext);

      //  this.makeButtons();
    }
    setQuestions(questionVo:QuestionVo)
    {
        this.btnArrow.alpha=0.5;

        this.questionVo=questionVo;
        this.qtext.setText(questionVo.q);
        for (let i:number=0;i<this.buttons.length;i++)
        {
            this.buttons[i].destroy();
        }
        this.makeButtons();
    }
    makeButtons() {
        let yy: number = this.back.displayHeight*0.15;
       // console.log(this.questionVo);
        this.buttons=[];
        for (let i: number = 0; i < this.questionVo.answers.length; i++) {
            let answer: AnswerVo = this.questionVo.answers[i];
            let qv: QuizButtonVo = new QuizButtonVo(answer.text, "#000000", 0x98D8E7, 0xffffff);
            let qb: QuizButton = new QuizButton(this.bscene, qv);
            this.buttons.push(qb);

            qb.setInteractive();
            qb.on("pointerdown", () => { this.clickAnswer(i) });

            qb.y = yy + qb.displayHeight;

            yy += qb.displayHeight * 1.2;

            qb.x = this.back.displayWidth / 2;
            this.add(qb);

        }
        this.btnArrow.y=yy+this.btnArrow.displayHeight;
        this.btnArrow.x=this.back.displayWidth/2;

    }
    confirmAnswer()
    {
        if (this.btnArrow.alpha==1)
        {
            this.selectCallback();
        }       
    }
    resetButtons()
    {
        for (let i:number=0;i<this.buttons.length;i++)
        {
            let button:QuizButton=this.buttons[i];
            button.setBorder(0xffffff);
        }
    }
    clickAnswer(index: number) {
        this.resetButtons();
        console.log(index);
        this.btnArrow.alpha=1;
        this.selectedIndex=index;
        let button:QuizButton=this.buttons[index];

        button.setBorder(0xff0000);

    }
}