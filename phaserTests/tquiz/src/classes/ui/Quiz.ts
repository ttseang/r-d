import { GameObjects } from "phaser";
import { AnswerVo } from "../../dataObjs/AnswerVo";
import { QuestionVo } from "../../dataObjs/QuestionVo";
import IBaseScene from "../../interfaces/IBaseScene";
import Align from "../../util/align";
import { GM } from "../GM";
import { ImageHolder } from "../ImageHolder";
import { QuizBox } from "./QuizBox";

export class Quiz
{
    private bscene:IBaseScene;
    private scene:Phaser.Scene;

    public questions:QuestionVo[]=[];
    private currentQuestion:QuestionVo;

    public qIndex:number=-1;
    private qb:QuizBox;

    private storyText:GameObjects.Text;
    private responseText:GameObjects.Text;
    private btnNext:GameObjects.Image;

    private imageHolder:ImageHolder;

    private gm:GM=GM.getInstance();

    constructor(bscene:IBaseScene,questions:QuestionVo[])
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();

        this.questions=questions;

        this.qb=new QuizBox(bscene);
        this.qb.selectCallback=this.answerQuestion.bind(this);
        
        this.bscene.getGrid().placeAt(0.5,0,this.qb);
        
        this.imageHolder=new ImageHolder(bscene);

        this.responseText=this.scene.add.text(0,0,"You got it all figured out!",{fontStyle:"bold",fontSize:"44px",align:"center",color:"white"}).setOrigin(0.5,0.5);
        this.bscene.getGrid().placeAt(5,1,this.responseText);

        this.storyText=this.scene.add.text(0,0,this.gm.testText,{fontFamily:this.gm.mainFont,fontSize:"40px",color:"white",wordWrap:{useAdvancedWrap:true,width:this.bscene.getW()*0.7}});
        this.storyText.setOrigin(0.5,0.5);
        Align.center(this.storyText,this.bscene);

        this.btnNext=this.scene.add.image(0,0,"nextArrow");
        Align.scaleToGameW(this.btnNext,0.05,bscene);
        this.bscene.getGrid().placeAt(9,9,this.btnNext);
        this.btnNext.setInteractive();
        this.btnNext.setTint(0xffffff);
        this.btnNext.on("pointerdown",this.setNext.bind(this));
        this.setNext();

        
    }
    setForQuestions()
    {
        this.storyText.visible=false;
        this.responseText.visible=false;
        this.btnNext.visible=false;
        this.imageHolder.setVisible(true);
        this.qb.visible=true;
    }
    setForResponse()
    {
        this.storyText.visible=true;
        this.responseText.visible=true;
        if (this.qIndex<this.questions.length-1)
        {
            this.btnNext.visible=true;
        }
        
        this.imageHolder.setVisible(false);

        this.qb.visible=false;
    }
    answerQuestion()
    {
        console.log("answer="+this.qb.selectedIndex);
        let answer:AnswerVo=this.currentQuestion.answers[this.qb.selectedIndex];
        console.log(answer);

        if (answer.correct==true)
        {
            this.responseText.setText(this.currentQuestion.rightMsg);
        }
        else
        {
            this.responseText.setText(this.currentQuestion.wrongMsg);
        }
        this.storyText.setText(this.currentQuestion.explainText);

        this.setForResponse();
    }
    setNext()
    {
        this.qIndex++;
        this.currentQuestion=this.questions[this.qIndex];
        this.qb.setQuestions(this.currentQuestion);
        this.imageHolder.makeImage(this.currentQuestion.image,500);
        this.imageHolder.setPos(this.bscene.getW()*.7,this.bscene.getH()/2);
        this.setForQuestions();
    }
}