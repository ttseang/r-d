import { AnswerVo } from "./AnswerVo";

export class QuestionVo
{
    public answers:AnswerVo[];
    public q:string;
    public image:string;
    public wrongMsg:string;
    public rightMsg:string;
    public explainText:string;

    constructor(q:string,answers:AnswerVo[],image:string,wrongMsg:string,rightMsg:string,explainText)
    {
        this.q=q;
        this.answers=answers;
        this.image=image;

        this.wrongMsg=wrongMsg;
        this.rightMsg=rightMsg;
        this.explainText=explainText;
    }
}