import { ResizeManager } from "../classes/resizeManager";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import { Face } from "../Face";
import { BaseScene } from "./BaseScene";

export class ScenePuppet extends BaseScene {
    private face: Face;
    private bg: Phaser.GameObjects.Sprite;
    private bgIndex: number = 1;
    private resizeManager: ResizeManager;
    
    constructor() {
        super("ScenePuppet");
    }
    preload() {

        this.load.path = "./assets/puppet/";

        this.load.image("face", "face3.png");
        this.load.image("eye", "eye.png");
        this.load.image("closed", "closed.png");
        this.load.image("eyeshade", "eyeshade.png");
        //
        //
        //
        this.load.image("lbrow", "lbrow.png");
        this.load.image("rbrow", "rbrow.png");

        this.load.atlas("mouth", "mouth.png", "mouth.json");

        this.load.image("hand1", "hand1.png");
        this.load.image("hand2", "hand2.png");

        this.load.atlas("hands", "hands.png", "hands.json");
       //this.load.atlas('bg', "bg.png", "bg.json");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.game.canvas.id="puppetcanvas";

        this.resizeManager = ResizeManager.getInstance(this);

        window['scene'] = this;

     /*    this.bg = this.add.sprite(0, 0, "bg");

        this.bg.displayHeight = this.gh;
        this.bg.scaleX = this.bg.scaleY;
        Align.center(this.bg, this); */

        this.face = new Face(this);

      //  Align.scaleToGameW(this.face, 0.75, this);

        //  this.grid.showNumbers();
       // Align.center2(this.face, this);

       // this.face.y = this.gh - this.face.displayHeight + 30;

        let keys:string[]=[];
        let buttonLayout: LayoutVo = this.resizeManager.addItem("face", this.face);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 4, 0, 1));

        keys.push("face");

        this.resizeManager.setKeys(keys);
        this.resizeManager.placeChildren();

        this.input.keyboard.on("keydown", this.keypressed.bind(this));

       // this.grid.showPos();
    }
    nextBG() {
        this.bgIndex++;
        if (this.bgIndex == 5) {
            this.bgIndex = 1;
        }
        let f: string = "bg" + this.bgIndex.toString() + ".jpg";
        this.bg.setFrame(f);
    }
    prevBg() {
        this.bgIndex--;
        if (this.bgIndex == 0) {
            this.bgIndex = 4;
        }
        let f: string = "bg" + this.bgIndex.toString() + ".jpg";
        this.bg.setFrame(f);
    }
    keypressed(e: KeyboardEvent) {

        e.preventDefault();
        switch (e.key) {

            case ".":
                this.nextBG();
                break;
            case ",":
                this.prevBg();
                break;

            case "q":
                this.face.lookAtPos(1);
                break;

            case "w":
                this.face.lookAtPos(2);
                break;
            case "e":
                this.face.lookAtPos(3);
                break;

            case "r":
                this.face.lookAtPos(4);
                break;
            case "t":
                this.face.lookAtPos(5);
                break;

            case "a":
                this.face.lookAtPos(6);
                break;

            case "s":
                this.face.lookAtPos(7);
                break;
            case "d":
                this.face.lookAtPos(8);
                break;

            case "f":
                this.face.lookAtPos(9);
                break;
            case "g":
                this.face.lookAtPos(10);
                break;


            case "z":
                this.face.lookAtPos(11);
                break;

            case "x":
                this.face.lookAtPos(12);
                break;
            case "c":
                this.face.lookAtPos(13);
                break;

            case "v":
                this.face.lookAtPos(14);
                break;
            case "b":
                this.face.lookAtPos(15);
                break;

            case " ":
                this.face.blink2();
                break;

            case "u":
                this.face.setMouth(1);
                break;
            case "i":
                this.face.setMouth(2);
                break;
            case "o":
                this.face.setMouth(3);
                break;
            case "p":
                this.face.setMouth(4);
                break;
            case "[":
                this.face.setMouth(5);
                break;
            case "n":
                this.face.showHands();
                break;
            case "j":
                this.face.toggleLBrow();
                break;
            case "k":
                this.face.toggleRBrow();
                break;
            case "l":
                this.face.angleLBrow();
                break;
            case ";":
                this.face.angleRBrow();
                break;

            case "1":
                if (e.ctrlKey) {
                    this.face.setHand2(1);
                }
                else {
                    this.face.setHand1(1);
                }
                break;
            case "2":
                if (e.ctrlKey) {
                    this.face.setHand2(2);
                }
                else {
                    this.face.setHand1(2);
                }
                break;
            case "3":
                if (e.ctrlKey) {
                    this.face.setHand2(3);
                }
                else {
                    this.face.setHand1(3);
                }
                break;
            case "4":
                if (e.ctrlKey) {
                    this.face.setHand2(4);
                }
                else {
                    this.face.setHand1(4);
                }
                break;
            case "5":
                if (e.ctrlKey) {
                    this.face.setHand2(5);
                }
                else {
                    this.face.setHand1(5);
                }
                break;
            case "6":
                if (e.ctrlKey) {
                    this.face.setHand2(6);
                }
                else {
                    this.face.setHand1(6);
                }
                break;
            case "7":
                if (e.ctrlKey) {
                    this.face.setHand2(7);
                }
                else {
                    this.face.setHand1(7);
                }
                break;
            case "8":
                if (e.ctrlKey) {
                    this.face.setHand2(8);
                }
                else {
                    this.face.setHand1(8);
                }
                break;
            case "9":
                if (e.ctrlKey) {
                    this.face.setHand2(9);
                }
                else {
                    this.face.setHand1(9);
                }
                break;
            case "0":
                if (e.ctrlKey) {
                    this.face.setHand2(10);
                }
                else {
                    this.face.setHand1(10);
                }


                break;
        }
    }

    update() {

    }
}