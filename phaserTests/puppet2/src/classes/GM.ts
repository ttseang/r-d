let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

    public device:string="desktop";
    public orientation:string="";
    
    public canvasScaleW:number=0.15;
    public canvasScaleH:number=0.25;

    constructor()
    {
        window['gm']=this;
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}