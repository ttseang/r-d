import { LayoutVo } from "../dataObjs/LayoutVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import Align from "../util/align";
import { GM } from "./GM";

let instance: ResizeManager = null;

export class ResizeManager {
    private objMap: Map<string, LayoutVo> = new Map<string, LayoutVo>();
    private keys: string[];
    private bscene: IBaseScene;
    private gm:GM=GM.getInstance();

    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
    }
    public static getInstance(bscene: IBaseScene | null) {
        if (instance === null) {
            instance = new ResizeManager(bscene);

            window.addEventListener("orientationchange", () => { instance.flipped() });
            window.addEventListener("resize", () => { instance.doResize() });
        }
        return instance;
    }
    private flipped() {
        
        //the height and width have been flipped
        //so this may look like an error but it is not
        let w:number=this.bscene.getH();
        let h:number=this.bscene.getW();

        if (w<h)
    {
        this.gm.isPort=true;
        this.gm.orientation=ResizeVo.ORIENTATION_PORTRAIT;
    }
    else
    {
        this.gm.isPort=false;
        this.gm.orientation=ResizeVo.ORIENTATION_LANDSCAPE;
    }
        this.doResize();
    }
    private doResize() {
       
        let rows:number=this.bscene.getGrid().rows;
        let cols:number=this.bscene.getGrid().cols;

        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        if (window['visualViewport']) {
            w = window.visualViewport.width;
            h = window.visualViewport.height;
        }

        w=w*this.gm.canvasScaleW;
        h=h*this.gm.canvasScaleH;
        
        //console.log(w,h);

        //change the size of the scene
        this.bscene.resetSize(w, h);

        //change the scale to the scene
        this.bscene.getScene().scale.resize(w, h);

        //remove the debug grid - should be off for production
        this.bscene.getGrid().hide();
        
        //remake the grid based on the new size
        this.bscene.makeGrid(cols, rows);

        //turn back on the debug grid -very slow
      //  this.bscene.getGrid().showPos();

        //place the children on the new resized grid
        //but at the same grid positions
        this.placeChildren();
    }
    
    public placeChildren()
    {
        
        //
        //
        //
        for (let i:number=0;i<this.keys.length;i++)
        {
            let key:string=this.keys[i];
            let layoutVo:LayoutVo=this.objMap.get(key);
            if (layoutVo)
            {
                console.log(layoutVo);
                let resizeVo:ResizeVo=layoutVo.getDefinition(this.gm.device,this.gm.orientation);
                console.log(resizeVo);
                let obj:IGameObj=layoutVo.obj;
               // console.log(obj);
                if (obj.type==="button")
                {
                    obj.update();
                }
                else
                {
                    Align.scaleToGameW(obj,resizeVo.scale,this.bscene);
                }

                if (obj.type==="spine")
                {
                    obj.update();
                }

                if (obj.type==="fallingObj")
                {
                    obj.update();
                }
                else
                {
                    console.log("Place new");
                    this.bscene.getGrid().placeAt(resizeVo.posX,resizeVo.posY,obj);

                    if (obj.type==="special")
                    {
                        obj.update();
                    }
                }
               
            }
        }       
    }
    public addItem(key: string, obj: IGameObj) {
        if (!this.objMap.has(key)) {
            this.objMap.set(key, new LayoutVo(obj));
        }
        return this.objMap.get(key);
    }
    public getLayout(key:string)
    {
        ;
        let layoutVo:LayoutVo=this.objMap.get(key);
        return layoutVo.getDefinition(this.gm.device,this.gm.orientation);
    }
    public setKeys(keys: string[]) {
        this.keys = keys;
    }
}