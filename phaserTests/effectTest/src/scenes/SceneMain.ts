import { ColorBurst, Flare, Ripple, Sparks, StarBurst } from "ttgameeffects";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private index:number=0;
    private sf:number=0;

    constructor() {
        super("SceneMain");
    }
    preload() {
        Flare.preload(this,"./assets/effects/flare.png");
        StarBurst.preload(this,"./assets/effects/stars.png");
        ColorBurst.preload(this,"./assets/effects/colorStars.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);       

        

      
        this.input.on("pointerdown",this.makeSparks.bind(this));
    }
    makeSparks(p:Phaser.Input.Pointer)
    {

        switch(this.index)
        {
            case 0:
                let sparks:Sparks=new Sparks(this,p.x,p.y);
                sparks.start();
            break;


            case 1:
                let flare:Flare=new Flare(this,this.gw,p.x,p.y,0x0000ff);
                flare.start();
            break;

            case 2:
                let stars:StarBurst=new StarBurst(this,p.x,p.y);
                stars.f=this.sf;
                stars.start();  
                this.sf++;
                if (this.sf>5)
                {
                    this.sf=0;
                }      
            break;

            case 3:
                let ripples:Ripple=new Ripple(this,p.x,p.y);
                ripples.start();
                break;

            case 4:
                let colorStars:ColorBurst=new ColorBurst(this,p.x,p.y);
                colorStars.start();
                break;
        }
       this.index++;
       if (this.index==5)
       {
           this.index=0;
       }
    }
}