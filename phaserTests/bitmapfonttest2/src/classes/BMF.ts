import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";

export class BMF extends GameObjects.BitmapText implements IGameObj
{
    private _displayHeight:number=0;

    constructor(bscene:IBaseScene,text:string,font:string)
    {
        super(bscene.getScene(),0,0,font,text);
        bscene.getScene().add.existing(this);
    }
   set displayWidth(val:number)
   {
      this.setMaxWidth(val);
   }
   get displayWidth():number
   {
     return this.maxWidth;
   }
   get displayHeight(): number
   {
       return this._displayHeight;
   }
   set displayHeight(val:number)
   {
     this._displayHeight=val;
   }
}