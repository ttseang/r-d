//import { Typekit } from "webfontloader";
import { CustomFontProvider } from "./CustomFontProvider";
import { FontDeck } from "./FontDeck";
import { FontsDotCom } from "./FontsDotCom";
import { GoogleFontsApi } from "./GoogleFontsApi";
import { Typekit } from "./TypeKit";

export class FontLoaderConfig {
    public constructor() {}
    
    // Font Sources
    // NOTE: You must delete any provider that you do not use.
    public custom: CustomFontProvider = new CustomFontProvider();
    /* public fontdeck: FontDeck = new FontDeck;
    public monotype: FontsDotCom = undefined;
    public google: GoogleFontsApi = new GoogleFontsApi();
    public typekit: Typekit = new Typekit(); */
  
    // Event Handlers
    public loading: () => any = undefined;
    public active: () => any = () => undefined;
    public inactive: () => any = undefined;
    public fontloading: (familyName: string, fvd: string) => any = undefined;
    public fontactive: (familyName: string, fvd: string) => any = undefined;
    public fontinactive: (familyName: string, fvd: string) => any = undefined;
    
    // It is possible to disable setting classes on the HTML element
    // by setting the classes configuration parameter to false
    // (defaults to true).
    public classes: boolean = true;
  
    // You can disable font events (callbacks) by setting the events
    // parameter to false (defaults to true).
    public events: boolean = true;
  
    // You can change the default timeout by using the timeout option.
    public timeout: number = 3000;
  }