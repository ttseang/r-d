const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: {
    app: './src/index.ts',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Phaser Game',
    }),
    new CopyPlugin({
      patterns: [{ from: "./assets", to: "assets" }],
    })
  ],
  output: {
    filename: 'bundle.min.js',
    path: path.resolve(__dirname, '../dist'),
    clean: true,
  },
  module: {
    
    rules: [
      {
        test: /\.(gif|png|jpe?g|svg|xml|atlas|ttf)$/i,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'assets/',
          publicPath: 'assets/',
          postTransformPublicPath: (p) => `__webpack_public_path__ + ${p}`,
        }
      },
        {
            test: /\.tsx?$/,
            loader: 'ts-loader',
            exclude: /node_modules/,
        },
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        }
    ]
},
resolve: {
    extensions: [".tsx", ".ts", ".js"]
}
};