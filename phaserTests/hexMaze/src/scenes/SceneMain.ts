import { GameObjects } from "phaser";
import { Background } from "../classes/Background";
import { GM } from "../classes/GM";
import { HexCell } from "../classes/HexCell";
import { HexGrid } from "../classes/HexGrid";
import { LevelVo } from "../dataObjs/LevelVo";
import Align from "../util/align";
import { AnimUtil } from "../util/animUtil";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private hexGrid: HexGrid;
    private currentCell: HexCell;
    private destCell: HexCell;
    private checkType: number = 0;
    private checkVal: number = 6;

    public currentLevel: LevelVo;
    private gm: GM = GM.getInstance();
    private penquin: GameObjects.Sprite;
    private clickLock: boolean = false;
    private background: Background;
    private igloo: GameObjects.Image;
    private messageText: GameObjects.Text;

    constructor() {
        super("SceneMain");

    }
    preload() {
        this.load.image("hexagon", "./assets/hex.png");
        this.load.image("hexborder", "./assets/hexborder.png");
        this.load.image("bg", "./assets/bg.jpg");

        this.load.image("igloo", "./assets/igloo.png");
        this.load.atlas("penquin", "./assets/penquin.png", "./assets/penquin.json");
        this.load.image("button", "./assets/buttons/1/4.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.grid.showPos();

        let testHex: GameObjects.Image = this.add.image(0, 0, "hexagon");
        Align.scaleToGameW(testHex, 0.07, this);


        let cw: number = testHex.displayWidth;
        let ch: number = testHex.displayHeight;

        this.currentLevel = this.gm.levels[this.gm.levelIndex];
        console.log(this.currentLevel);

        this.checkType = this.currentLevel.type;
        this.checkVal = this.currentLevel.checkVal;


        testHex.destroy();

        this.background = new Background(this, "bg");

        // console.log(cw,ch);

        this.hexGrid = new HexGrid(this, this.currentLevel.gridW, this.currentLevel.gridH, cw, ch);

        // let data:number[]=[5,4,6,6,6,4,4,6,4,4,5,6,5,4,5,6,6,5,4,4,-1];

        this.hexGrid.buildGrid(this.currentLevel.data);
        this.currentCell = this.hexGrid.hexArray[this.currentLevel.start.x][this.currentLevel.start.y];
        this.currentCell.back.setTint(0x48dbfb);

        this.destCell = this.hexGrid.hexArray[this.currentLevel.end.x][this.currentLevel.end.y];
        this.destCell.back.setTint(0x48dbfb);

        this.igloo = this.add.image(0, 0, "igloo");
        // this.igloo.visible=false;
        Align.scaleToGameW(this.igloo, 0.05, this);



        window['scene'] = this;

        this.grid.placeAt(4, 3, this.hexGrid);

        this.penquin = this.add.sprite(0, 0, "penquin");
        Align.scaleToGameW(this.penquin, 0.03, this);

        this.penquin.x = this.currentCell.x + this.hexGrid.x;
        this.penquin.y = this.currentCell.y + this.hexGrid.y;
        this.igloo.x = this.destCell.x + this.hexGrid.x;
        this.igloo.y = this.destCell.y + this.hexGrid.y;


        AnimUtil.makeAnims(this, "penquin");
        this.penquin.play("idle");

        this.messageText = this.add.text(0, 0, this.currentLevel.msg, { backgroundColor: "black", color: "white", fontSize: "30px", fontFamily: "Arial" }).setOrigin(0.5, 0.5);
        this.grid.placeAt(5, 0, this.messageText);

        this.input.on("gameobjectdown", this.cellClick.bind(this));
    }
    cellClick(p: Phaser.Input.Pointer, cell: HexCell) {
        if (this.clickLock == true) {
            return;
        }
        if (this.hexGrid.isAdj(this.currentCell, cell)) {

            switch (this.checkType) {
                case 0:
                    /*  console.log(cell.value);
                     console.log(this.checkVal); */

                    if (cell.value != this.checkVal) {
                        this.doOver();
                        return;
                    }
                    break;

                case 1:
                    //should be even
                    console.log(this.checkVal);

                    if (this.checkVal == 0) {
                        //odd number
                        if (cell.value / 2 != Math.floor(cell.value / 2)) {

                            this.doOver();
                            return;
                        }
                    }
                    else {
                        //should be odd
                        //even number
                        if (cell.value / 2 == Math.floor(cell.value / 2)) {

                            this.doOver();
                            return;
                        }
                    }
                    break;

                case 2:
                    if (!this.isPrime(cell.value)) {
                        this.doOver();
                        return;
                    }
                    break;
            }

            this.clickLock = true;

            cell.back.setTint(0x48dbfb);
            this.currentCell = cell;

            let targetX: number = this.currentCell.x + this.hexGrid.x;
            let targetY: number = this.currentCell.y + this.hexGrid.y;

            this.penquin.play("walk");
            this.tweens.add({ targets: this.penquin, duration: 500, x: targetX, y: targetY, onComplete: this.walkDone.bind(this) })

        }

    }
    isPrime(num) {
        
        for (var i = 2; i < num; i++)
            if (num % i === 0) return false;
        return num > 1;
    }
    doOver() {
        this.tweens.add({ duration: 2000, targets: [this.hexGrid, this.igloo], y: this.gh + 1000 })
        this.tweens.add({ delay: 1000, duration: 2000, targets: [this.penquin], y: this.gh + 1000 });

        setTimeout(() => {
            this.scene.start("SceneOver");
        }, 2500);
    }
    walkDone() {
        this.clickLock = false;
        this.penquin.play("idle");
        if (this.currentCell == this.destCell) {
            this.gm.levelIndex++;
            if (this.gm.levelIndex < this.gm.levels.length) {
                setTimeout(() => {
                    this.scene.restart();
                }, 2000);

            }

        }
    }
}