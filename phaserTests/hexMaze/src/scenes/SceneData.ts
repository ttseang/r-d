import { GM } from "../classes/GM";
import { LevelVo } from "../dataObjs/LevelVo";
import { PosVo } from "../dataObjs/PosVo";
import { BaseScene } from "./BaseScene";

export class SceneData extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneData");
    }
    create()
    {
        fetch("./assets/levels.json")
        .then(response => response.json())
        .then(data => this.process({ data }));
    }
    process(data:any)
    {
        console.log(data.data.levels);

        let levels:any=data.data.levels;
        
        let levelVos:LevelVo[]=[];

        for (let i:number=0;i<levels.length;i++)
        {
            let level=levels[i];
           // console.log(level);
            let startPos:PosVo=new PosVo(parseInt(level.start.x),parseInt(level.start.y));
            let endPos:PosVo=new PosVo(parseInt(level.end.x),parseInt(level.end.y));

            let levelVo:LevelVo=new LevelVo(parseInt(level.type),parseInt(level.checkVal),level.gridW,level.gridH,startPos,endPos,level.data,level.msg);
            //console.log(levelVo);
            levelVos.push(levelVo);
        }
        this.gm.levels=levelVos;
        this.scene.start("SceneMain");       
    }
}