import { PosVo } from "./PosVo";

export class LevelVo
{
    public data:number[]=[];
    public type:number=0;
    public checkVal:number=0;
    public gridW:number=0;
    public gridH:number=0;
    public start:PosVo;
    public end:PosVo;
    public msg:string;

    constructor(type:number,checkVal:number,gridW:number,gridH:number,start:PosVo,end:PosVo,data:number[],msg:string)
    {
        this.type=type;
        this.checkVal=checkVal;
        this.gridW=gridW;
        this.gridH=gridH;
        this.start=start;
        this.end=end;
        this.data=data;
        this.msg=msg;
    }
}