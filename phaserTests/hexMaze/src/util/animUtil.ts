export class AnimUtil
{
    constructor()
    {

    }
    public static  makeAnims(scene, key) {
      //  console.log("anim key=" + key);
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 13,
                zeroPad: 2,
                prefix: 'Idle_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });

        scene.anims.create({
            key: 'walk',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 11,
                zeroPad: 2,
                prefix: 'Walk_',
                suffix: '.png'
            }),
            frameRate: 32,
            repeat: -1
        });
    }
}