import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class HexCell extends GameObjects.Container
{
    private bscene:IBaseScene;
    public back:GameObjects.Image;
    public border:GameObjects.Image;

    private text1:GameObjects.Text;
    public xx:number;
    public yy:number;
    public index:number=0;
    public value:number;
    constructor(bscene:IBaseScene,xx:number,yy:number,value:number)
    {
        super(bscene.getScene());
        this.bscene=bscene;

        this.value=value;
        this.xx=xx;
        this.yy=yy;

        this.back=this.scene.add.image(0,0,"hexagon");
        this.border=this.scene.add.image(0,0,"hexborder");

        this.text1=this.scene.add.text(0,0,xx.toString()+"-"+yy.toString(),{color:"black"}).setOrigin(0.5,0.5);
        this.text1.setText(value.toString());
     //   Align.scaleToGameW(this.back,0.1,this.bscene);

        
        this.add(this.back);
        this.add(this.border);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();

        this.scene.add.existing(this);
    }
}