import { PosVo } from "../../dataObjs/PosVo";

export class GamePosObj
{
    public messagePosition:PosVo
    public coinPosition:PosVo;
    public coinTextPosition:PosVo;
    public buttonPosition:PosVo;

    constructor(messagePosition:PosVo,coinPosition: PosVo,coinTextPosition: PosVo,buttonPosition: PosVo)
    {
        this.messagePosition=messagePosition;
        this.coinPosition=coinPosition;
        this.coinTextPosition=coinTextPosition;
        this.buttonPosition=buttonPosition;
    }
}