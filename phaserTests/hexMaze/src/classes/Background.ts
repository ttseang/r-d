import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";


export class Background extends GameObjects.Image
{
    private bscene:IBaseScene;

    constructor(bscene:IBaseScene,key:string)
    {
        super(bscene.getScene(),0,0,key);
        this.bscene=bscene;
        this.resizeToScreen();
        
        this.scene.add.existing(this);
    }
    public resizeToScreen()
    {
        this.displayWidth=this.bscene.getW();
        this.scaleY=this.scaleX;

        if (this.displayHeight>this.bscene.getH())
        {
            this.displayHeight=this.bscene.getH();
            this.scaleX=this.scaleY;
        }

        if(this.displayWidth<this.bscene.getW())
        {
            this.displayHeight=this.bscene.getH();
            this.displayWidth=this.bscene.getW();
        }
        Align.center(this,this.bscene);

    
    }
}