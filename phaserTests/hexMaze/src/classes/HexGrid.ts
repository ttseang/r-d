import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import { HexCell } from "./HexCell";

export class HexGrid extends GameObjects.Container {
    public gridWidth: number = 70;
    public gridHeight: number = 80;
    private cellWidth: number = 70;
    private cellHeight: number = 80;
    public hexArray: HexCell[][] = [];
    private bscene: IBaseScene;
    public allCells:HexCell[]=[];

    constructor(bscene: IBaseScene, gridWidth: number, gridHeight: number, cellWidth: number, cellHeight: number) {
        super(bscene.getScene());

        this.gridHeight = gridHeight;
        this.gridWidth = gridWidth;

        this.cellHeight = cellHeight;
        this.cellWidth = cellWidth;

        this.bscene = bscene;

        
        this.scene.add.existing(this);
    }
    buildGrid(data:number[]) {
        let index:number=0;

        for (let i = 0; i < this.gridHeight / 2; i++) {
            this.hexArray[i] = [];
            for (let j = 0; j < this.gridWidth; j++) {



                let hexagon: HexCell = new HexCell(this.bscene,j,i,data[index]);

                //
                let hexagonX: number = this.cellWidth *.75 * j;
                let hexagonY: number = this.cellHeight * i;

                if (j / 2 != Math.floor(j / 2)) {
                    hexagonY -= this.cellHeight / 2;
                    // hexagon.alpha=.05;
                }

                hexagon.displayWidth = this.cellWidth;
                hexagon.displayHeight = this.cellHeight;

                hexagon.x = hexagonX;
                hexagon.y = hexagonY;
            //    hexagon.back.setTint(0x48dbfb);

                this.add(hexagon);
                this.hexArray[i][j] = hexagon;
                hexagon.index=index;
                index++;
                this.allCells.push(hexagon);
            }
            // }
        }
        this.setSize(this.gridWidth * this.cellWidth, this.gridHeight * this.cellHeight);
    }
    isAdj(cell1:HexCell,cell2:HexCell)
    {
        let cells:HexCell[]=this.getAdj(cell1);
        if (cells.includes(cell2))
        {
            return true;
        }
        return false;
    }
    getAdj(targCell:HexCell)
    {
        let cells:HexCell[]=[];

        for (let i:number=0;i<this.allCells.length;i++)
        {
            let cell:HexCell=this.allCells[i];

            if (cell.index!=targCell.index)
            {
                let distX:number=Math.abs(targCell.x-cell.x);
                let distY:number=Math.abs(targCell.y-cell.y);
                
                let cellName:string=cell.xx.toString()+"|"+cell.yy.toString();

                if (distX<this.cellWidth && distY<this.cellHeight*1.5)
                {
                   
                    cells.push(cell);
                }    
                else
                {
                    //console.log(distX,distY,cellName);
                }            
            }
        }
        return cells;
    }
}