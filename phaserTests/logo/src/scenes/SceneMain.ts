import { GameObjects } from "phaser";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private logo:GameObjects.Image;
    private sx:number=1;
    private sy:number=1;
    private style: number = 3;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("logo", "./assets/logo.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        console.log("create!");
        this.logo=this.add.image(0, 0, "logo");

        Align.scaleToGameW(this.logo,0.9,this);

        this.sx=this.logo.scaleX;
        this.sy=this.logo.scaleY;

      //  this.grid.showNumbers();
        this.grid.placeAtIndex(60,this.logo);
        switch(this.style)
        {
            case 1:
            this.doIn();
            break;

            case 2:
                this.tiltOut();
            break;

            case 3:
                this.swayOut();
            break;
        }
     
    }
    doOut() {
        this.tweens.add({targets: this.logo,duration: 1000,scaleX:this.sx*1.01,scaleY:this.sy*1.01,onComplete:this.doIn.bind(this)});
    }
    doIn() {
        this.tweens.add({targets: this.logo,duration: 1000,scaleX:this.sx,scaleY:this.sy,onComplete:this.doOut.bind(this)});
    }
    tiltOut() {
        this.tweens.add({targets: this.logo,duration: 2000,scaleY:this.sy*1.05,onComplete:this.tiltIn.bind(this)});
    }
    tiltIn() {
        this.tweens.add({targets: this.logo,duration: 2000,scaleY:this.sy,onComplete:this.tiltOut.bind(this)});
    }
    swayOut() {
        this.tweens.add({targets: this.logo,duration: 2000,scaleX:this.sx*1.05,onComplete:this.swayIn.bind(this)});
    }
    swayIn() {
        this.tweens.add({targets: this.logo,duration: 2000,scaleX:this.sx,onComplete:this.swayOut.bind(this)});
    }
}