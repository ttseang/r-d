import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private dlg:any;
    private startX:number=70;
    private startY:number=70;
    private stopX:number=100;
    private stopY:number=100;
    private isDown:Boolean=false;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image('kenny', './assets/kennyBlue.png')
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.startX = 50
    this.startY = 70
    this.stopX = Phaser.Math.Between(150,300);
    this.stopY = Phaser.Math.Between(150,300);
    const width = this.stopX - this.startX
    const height = this.stopY - this.startY

    this.dlg = this.add.nineslice(
      this.startX,
      this.startY,
      width,
      height,
      'kenny',
      [35, 15, 15]
    )
        this.input.once('pointerdown',this.onDown.bind(this));
    }
    onDown()
    {
        this.input.once('pointerup',this.onUp.bind(this));
        this.input.on('pointermove',this.doResize.bind(this));
    }
    onUp()
    {
        this.input.once('pointerdown',this.onDown.bind(this));
        this.input.off('pointermove');
    }
    doResize()
    {
       // newWidth:number = Phaser.Math.Between(150,300);
        //let newHeight:number = Phaser.Math.Between(150,300);

        let newWidth:number =this.input.activePointer.x;
        let newHeight:number = this.input.activePointer.y;

        this.dlg.resize(newWidth, newHeight)
    }
}