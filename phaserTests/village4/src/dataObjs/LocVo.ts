import { LocButtonVo } from "./LocButtonVo";

export class LocVo
{
    public x:number;
    public y:number;
    public name:string;
    public locked:boolean=true;
    public unlockNext:string;
    public level:number;
    public button:LocButtonVo;
    public message:string;

    constructor(level:number,x:number,y:number,name:string,unlockNext:string,message:string="")
    {
        this.level=level;
        this.x=x;
        this.y=y;
        this.name=name;
        this.unlockNext=unlockNext;
        this.button=new LocButtonVo("","","");
        this.message=message;
    }
}