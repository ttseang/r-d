//import Phaser = require("phaser");
import Phaser from 'phaser';
import { SceneMap } from "./village/scenes/SceneMap";
import {GM} from "./classes/GM";
import { SceneInside } from './village/scenes/SceneInside';
import { SceneData } from './scenes/SceneData';
import { SceneCookieData } from './minigames/cookies/scenes/SceneCookieData';
import { SceneCookieLoad } from './minigames/cookies/scenes/SceneCookieLoad';
import { SceneCookie } from './minigames/cookies/scenes/SceneCookie';
import { SceneCutter } from './minigames/cookies/scenes/SceneCutter';
import SceneFont from './scenes/SceneFont';
import SceneLoadMovie from './theater/scenes/SceneLoadMovie';
import { SceneMovieData } from './theater/scenes/SceneMovieData';
import SceneMovie from './theater/scenes/SceneMovie';

let gm:GM=GM.getInstance();

let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad=navigator.userAgent.indexOf("iPad");

    if (isTablet!=-1 || isIpad!=-1)
    {
        gm.isTablet=true;
        isMobile=1;
    }

let w = 600;
let h = 400;
//
//
if (isMobile != -1) {
    gm.isMobile=true;
    
}
w = window.innerWidth;
    h = window.innerHeight;

    if (window['visualViewport'])
    {
        w=window.visualViewport.width;
        h=window.visualViewport.height;
    }
if (w<h)
{
    gm.isPort=true;
}
const config = {
    type: Phaser.AUTO,
    width: w,
    height: h,    
    backgroundColor:'000000',
    parent: 'phaser-game',
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: [SceneFont,SceneData,SceneMap,SceneInside,SceneCookieData,SceneCookieLoad,SceneCutter,SceneCookie,SceneLoadMovie,SceneMovieData,SceneMovie]
};

new Phaser.Game(config);