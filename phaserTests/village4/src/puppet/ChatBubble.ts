import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";

export class ChatBubble extends GameObjects.Container
{
    public scene:Phaser.Scene;

    private bubbleBack:GameObjects.Graphics;
    private text1:GameObjects.Text;
    

    constructor(bscene:IBaseScene,text:string)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();

        this.text1=this.scene.add.text(0,0,text,{color:"#000000",wordWrap:{useAdvancedWrap:true,width:bscene.getW()*0.2}}).setOrigin(0.5,0.5);
        this.bubbleBack=this.scene.add.graphics();
        this.add(this.bubbleBack);
        this.add(this.text1);
        
        let ww:number=this.text1.displayWidth*1.5;
        let xx:number=this.text1.x-ww/2;
        
        let hh:number=this.text1.displayHeight*4;
        let yy:number=this.text1.y-hh/2;


        this.bubbleBack.fillStyle(0xffffff,1);
        this.bubbleBack.lineStyle(2,0x000000,1);
        this.bubbleBack.fillRoundedRect(xx,yy,ww,hh,8);
        this.bubbleBack.strokeRoundedRect(xx,yy,ww,hh,8);
        
        this.setSize(ww,hh);
        /* this.bubbleBack.moveTo(ww/2-bscene.getW()*0.05,this.y+hh/2);
        this.bubbleBack.lineTo(ww/2,hh+bscene.getH()*0.05);
        this.bubbleBack.lineTo(ww/2+bscene.getW()*0.05,this.y+hh/2);
        this.bubbleBack.strokePath(); */

        window['bb']=this;
        //
        //
        //
       
        this.scene.add.existing(this);
    }
}