import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";

export class Face extends GameObjects.Container {

    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private back: GameObjects.Image;
    private faceGrid: AlignGrid;
    //
    //
    //
    private rightEye: GameObjects.Image;
    private leftEye: GameObjects.Image;
    //
    //
    //
    private rightPos: PosVo = new PosVo(0, 0);
    private leftPos: PosVo = new PosVo(0, 0);

    private lbrowPos: PosVo = new PosVo(0, 0);
    private rbrowPos: PosVo = new PosVo(0, 0);

    private closedRight: GameObjects.Image;
    private closedLeft: GameObjects.Image;
    //
    //
    //
    private lbrow: GameObjects.Image;
    private rbrow: GameObjects.Image;
    //
    //
    //
    private mouth: GameObjects.Sprite;
    //
    //
    //
    private hand1: GameObjects.Sprite;
    private hand2: GameObjects.Sprite;

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;

        window['face'] = this;

        this.back = this.scene.add.image(0, 0, "face").setOrigin(0, 0);
        Align.scaleToGameW(this.back, 0.3, this.bscene);
        this.add(this.back);

        let shade:GameObjects.Image=this.scene.add.image(0,0,"eyeshade");//.setOrigin(0,0);
        let shade2:GameObjects.Image=this.scene.add.image(0,0,"eyeshade");
        //
        //
        //
        this.rightEye = this.scene.add.image(0, 0, "eye");
        Align.scaleToGameW(this.rightEye, 0.025, this.bscene);
        this.add(this.rightEye);
        //
        //
        //
        this.leftEye = this.scene.add.image(0, 0, "eye");
        Align.scaleToGameW(this.leftEye, 0.025, this.bscene);
        this.add(this.leftEye);
        //
        //
        //
        
        

        this.faceGrid = new AlignGrid(this.bscene, 44, 44, this.back.displayWidth, this.back.displayHeight);
        
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        shade.displayWidth=this.faceGrid.cw*7;
        shade.displayHeight=this.faceGrid.ch*7;

        shade2.displayWidth=this.faceGrid.cw*7;
        shade2.displayHeight=this.faceGrid.ch*7;
        
        // this.faceGrid.showNumbers();

        this.faceGrid.placeAt(15, 20, this.rightEye);
        this.faceGrid.placeAt(28, 20, this.leftEye);
        this.faceGrid.placeAt(15,19,shade);
        this.add(shade);

        this.faceGrid.placeAt(28,19,shade2);
        this.add(shade2);

        this.scene.add.existing(this);

        this.rightPos = new PosVo(this.rightEye.x, this.rightEye.y);
        this.leftPos = new PosVo(this.leftEye.x, this.leftEye.y);
        //
        //
        //
        this.closedLeft = this.scene.add.image(this.leftPos.x, this.leftPos.y, "closed");
        this.closedRight = this.scene.add.image(this.rightPos.x, this.rightPos.y, "closed");

        Align.scaleToGameW(this.closedRight, 0.055, this.bscene);
        Align.scaleToGameW(this.closedLeft, 0.055, this.bscene);

        this.closedRight.visible = false;
        this.closedLeft.visible = false;

        this.add(this.closedLeft);
        this.add(this.closedRight);
        //
        //
        //
        this.mouth = this.scene.add.sprite(0, 0, "mouth");
        Align.scaleToGameW(this.mouth, 0.05, this.bscene);
        this.faceGrid.placeAt(22, 30, this.mouth);
        this.add(this.mouth);

        this.hand1 = this.scene.add.sprite(0, 0, "hands");
        this.hand2 = this.scene.add.sprite(0, 0, "hands");
        this.hand1.flipX=true;

        Align.scaleToGameW(this.hand1, 0.08, this.bscene);
        Align.scaleToGameW(this.hand2, 0.08, this.bscene);

        this.faceGrid.placeAt(0, 36, this.hand1);
        this.faceGrid.placeAt(46, 36, this.hand2);
       // this.faceGrid.show();

        this.add(this.hand1);
        this.add(this.hand2);

        this.hand1.visible = false;
        this.hand2.visible = false;


        this.lbrow = this.scene.add.image(0, 0, "lbrow");
        Align.scaleToGameW(this.lbrow, 0.05, this.bscene);
        this.faceGrid.placeAt(15, 16, this.lbrow);

        this.rbrow = this.scene.add.image(0, 0, "rbrow");
        Align.scaleToGameW(this.rbrow, 0.05, this.bscene);
        this.faceGrid.placeAt(28, 16, this.rbrow);

        this.add(this.lbrow);
        this.add(this.rbrow);

        this.lbrowPos = new PosVo(this.lbrow.x, this.lbrow.y);
        this.rbrowPos = new PosVo(this.rbrow.x, this.rbrow.y);

        this.sendToBack(this.rightEye);
        this.sendToBack(this.leftEye);
        this.sendToBack(shade);
        this.sendToBack(shade2);
        
        //setInterval(this.blink.bind(this), 3000);
    }
    blink() {


        this.closedRight.visible = true;
        this.closedLeft.visible = true;
        setTimeout(() => {
            this.closedRight.visible = false;
            this.closedLeft.visible = false;
        }, 100);
    }
    blink2() {
        this.closedRight.visible = !this.closedRight.visible;
        this.closedLeft.visible = !this.closedLeft.visible;
    }
    showHands() {
        this.hand1.visible = !this.hand1.visible;
        this.hand2.visible = !this.hand2.visible;

    }
    toggleRBrow() {
        if (this.rbrow.y == this.rbrowPos.y) {
            this.rbrow.y -= this.faceGrid.ch;
        }
        else {
            this.rbrow.y = this.rbrowPos.y;
        }
    }
    toggleLBrow() {
        if (this.lbrow.y == this.lbrowPos.y) {
            this.lbrow.y -= this.faceGrid.ch;
        }
        else {
            this.lbrow.y = this.lbrowPos.y;
        }
    }
    angleRBrow() {
        if (this.rbrow.angle === 0) {
            this.rbrow.setAngle(-20);
        }
        else {
            this.rbrow.setAngle(0);
        }
    }
    angleLBrow() {
        if (this.lbrow.angle === 0) {
            this.lbrow.setAngle(20);
        }
        else {
            this.lbrow.setAngle(0);
        }
    }
    public lookAtPos(pos: number, dist: number = 2) {
        let distX: number = 0;
        let distY: number = 0;

        switch (pos) {
            case 1:
                distX = -3;
                distY = -2;
                break;

            case 2:
                distX = -2;
                distY = -2;
                break;

            case 3:
                distX = 0;
                distY = -2;
                break;

            case 4:
                distX = 2;
                distY = -2;
                break;

            case 5:
                distX = 3;
                distY = -2;
                break;

            case 6:
                distX = -3;
                distY = 0;
                break;
            case 7:
                distX = -2;
                distY = 0;
                break;

            case 8:
                distX = 0;
                distY = 0;
                break;
            case 9:
                distX = 2;
                distY = 0;
                break;
            case 10:
                distX = 3;
                distY = 0;
                break;

            case 11:
                distX = -3;
                distY = 2;
                break;
            case 12:
                distX = -2;
                distY = 2;
                break;

            case 13:
                distX = 0;
                distY = 2;
                break;
            case 14:
                distX = 2;
                distY = 2;
                break;
            case 15:
                distX = 3;
                distY = 2;
                break;

        }

        this.rightEye.x = this.rightPos.x + distX * dist;
        this.rightEye.y = this.rightPos.y + distY * dist;
        //
        //
        //
        this.leftEye.x = this.leftPos.x + distX * dist;
        this.leftEye.y = this.leftPos.y + distY * dist;

    }
    public lookAt(xx: number, yy: number) {
        let modX: number = this.bscene.getW() / 5;
        let modY: number = this.bscene.getH() / 5;

        let centerFaceX: number = this.x + this.displayWidth / 2;
        let centerFaceY: number = this.y + this.displayHeight / 2;

        let dirX = this.getSgn(xx - centerFaceX);
        let dirY = this.getSgn(yy - centerFaceY);

        let distX = Math.abs(centerFaceX - xx) / modX;
        distX *= dirX;
        distX *= this.faceGrid.cw;

        let distY = Math.abs(centerFaceY - yy) / modY;
        distY *= dirY;
        distY *= this.faceGrid.ch / 2;

      //  console.log(distX, distY);

      //  console.log(dirX, dirY);

        this.rightEye.x = this.rightPos.x + distX;
        this.leftEye.x = this.leftPos.x + distX;

        this.rightEye.y = this.rightPos.y + distY;

    }

    public setMouth(index: number) {
        let f: string = "m" + index.toString() + ".png";
        this.mouth.setFrame(f);
    }
    public setHand1(index:number)
    {
        let f: string = "hand (" + index.toString() + ").png";
        this.hand1.setFrame(f);
    }
    public setHand2(index:number)
    {
        let f: string = "hand (" + index.toString() + ").png";
        this.hand2.setFrame(f);
    }
    public randMouth() {
        let r: number = Math.floor(Math.random() * 6) + 1;
        let f: string = "m" + r.toString() + ".png";
        this.mouth.setFrame(f);
    }
    private getSgn(num: number) {
        if (num === 0) {
            return 0;
        }
        if (num < -1) {
            return -1;
        }
        return 1;
    }
}