import { AttsVo } from "./AttsVo";
import { FXTextPos } from "./FXTextPos";


export class GameObj
{
    public name:string;
    public type:string;
    public obj:any;

    public atts:AttsVo|undefined=undefined;
    public fxText:FXTextPos[]=[];
    
    constructor(name:string,type:string,obj:any)
    {
        this.name=name;
        this.type=type;
        this.obj=obj;
    }
}