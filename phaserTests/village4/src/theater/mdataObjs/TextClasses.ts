export class TextClasses
{
    public main:string;
    public htext:string;
    public centerText:boolean=false;
    constructor(main:string,htext:string,centerText:boolean=false)
    {
        this.main=main;
        this.htext=htext;
        this.centerText=centerText;
    }
}