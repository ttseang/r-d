
import IBaseScene from "../../interfaces/IBaseScene";
import Align from "../../util/align";

export class Bubbles2 {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    private t: Phaser.GameObjects.Text;
    private key: string;
    private secs: number = 0;

    private timer1: any = "";
    private colors: number[];
    private effectName: string;
    private useAnimation:boolean=false;
    private bubbleGroup:Phaser.GameObjects.Group;
    private speed:number=6;
    private moveSpeed:number=30;
    
    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text, key: string, secs: number, effectName: string, colors: number[],maxScale:number=10,useAnimation:boolean=false) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.colors = colors;
        this.effectName = effectName;
        this.useAnimation=useAnimation;
        this.bubbleGroup=this.scene.add.group();

        this.t = t;
        this.key = key;

        let tw: number = t.x + t.displayWidth;
        let th: number = t.y + t.displayHeight;


        for (let i: number = 0; i < 20; i++) {
            let xx: number = Phaser.Math.Between(t.x, tw);
            let yy: number = Phaser.Math.Between(t.y, th);
       //     let bubble: Bubble = new Bubble(bscene, xx, yy, key);
            let bubble:Phaser.GameObjects.Sprite=this.scene.add.sprite(xx,yy,key);
            
            if (colors.length !== 0) {
                let colorIndex: number = Phaser.Math.Between(0, colors.length - 1);
                bubble.setTint(colors[colorIndex]);
            }
            if (useAnimation===true)
            {
                let animationName:string=this.key+"Play";
                console.log(animationName);
                bubble.play(animationName);
            }
            let scale:number
            if (maxScale<0)
            {
                scale=Math.abs(maxScale)/100;
            }
            else
            {
                scale=Phaser.Math.Between(0,maxScale)/100;
            }
            Align.scaleToGameW(bubble,scale,bscene);

            this.bubbleGroup.add(bubble);
        }
        this.scene.children.bringToTop(this.t);
    }
    
    public animate() {
        this.scene.time.addEvent({ delay: 1000, callback: this.stopMe.bind(this), loop: false });
        this.timer1 = setInterval(this.tick.bind(this), 50);
        //   setTimeout(this.stop.bind(this), this.secs * 1000);
    }
    private stopMe() {
        console.log("STOP ME");
        clearInterval(this.timer1);
        this.bubbleGroup.children.entries.forEach((bubble)=>{
            bubble.removeAllListeners();
        })
        this.bubbleGroup.destroy(true,true);
        //this.firstBubble?.destroyMe();
    }
    private tick() {
       
            switch (this.effectName) {
                case "bubble":
                    //this.firstBubble?.move();
                    break;

                case "floataway":
                    this.bubbleGroup.children.entries.forEach((bubble)=>{
                        this.moveUp(bubble);
                    })
                   
                    break;

                case "zoomaway":
                    //this.firstBubble.zoomUp();
                    break;

            } 
        
    }
    zoomUp(s:any) {
        if (s.active === false) {
            if (s.y > 0) {
                if (Phaser.Math.Between(0, 100) < 40) {
                    s.active = true;
                }
            }
        }
        else {
            s.y -= 20;
            if (s.y < -100) {
                s.visible = false;
                s.active = false;
            }
        }        
    }
    moveUp(s:any) {
        s.alpha -= this.speed * 2;
        s.scaleX -= this.speed;
        s.scaleY -= this.speed;
        s.y -= this.moveSpeed * 4;
        s.x += Phaser.Math.Between(-1,1)* this.moveSpeed;
        if (s.scaleX < 0) {
            s.visible = false;
        }       
    }
    /* move(s:Phaser.GameObjects.Sprite) {
        s.alpha -= this.speed * 2;
        //this.y-=this.speed;
        s.scaleX -= this.speed;
        s.scaleY -= this.speed;
        s.y += this.ydir * this.moveSpeed;
        s.x += this.xdir * this.moveSpeed;

        if (this.scaleX < 0) {
            this.visible = false;
        }
        if (this.next) {
            this.next.move();
        }
        
    } */
}