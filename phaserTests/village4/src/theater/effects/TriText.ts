import IBaseScene from "../../interfaces/IBaseScene";

import { TextMask } from "./textMask";

export class TriText {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private scale: number = 0;
    private dir: number = 1;
    private count: number = 0;
    private blocks: TextMask[] = [];
    //   private corners: Phaser.GameObjects.Sprite[] = [];
    private timer1: Phaser.Time.TimerEvent | undefined;
    private dropIndex: number = 3;
    public callback: Function = () => { };
    private animationName: string;
    private t: Phaser.GameObjects.Text;
    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text, animationName: string) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.animationName = animationName;
       
        this.t = t;

        for (let i: number = 0; i < 3; i++) {
            let tm: TextMask = new TextMask(bscene, t,"holder");
            tm.setMask(i);
            tm.index=i;
            tm.ox = t.x;
            tm.oy = t.y;

            this.setInit(tm);

            this.blocks.push(tm);
        }

    }
    private setInit(tm: TextMask) {
        switch (this.animationName) {
            case "drop":
                tm.x = this.t.x;
                tm.y = -500;
                break;

            case "sliceRight":
                tm.x = -this.t.displayWidth;
                tm.y = this.t.y;
                break;

            case "sliceLeft":
                tm.x = this.bscene.getW()+this.t.displayWidth;
              //  tm.x=500;
                tm.y = this.t.y;
                break;    
           
            case "mix":

                if (tm.index/2===Math.floor(tm.index/2))
                {
                    tm.x = this.bscene.getW()+this.t.displayWidth;
              //  tm.x=500;
                   tm.y = this.t.y;
                }
                else
                {
                    tm.x = -this.t.displayWidth;
                    tm.y = this.t.y;
                }

        }
    }
    animate() {
        if (this.animationName==="mix")
        {
            for (let i:number=0;i<3;i++)
            {            
            this.scene.tweens.add({ targets: this.blocks[i], duration: 1000, x: this.blocks[i].ox,onComplete:this.done.bind(this)});
            }
            return;
        }
       this.timer1 = this.scene.time.addEvent({ delay: 400, callback: this.doNext.bind(this), loop: true });
    }
    private doNext() {
        this.dropIndex--;
        if (this.dropIndex === -1) {
            this.done();
            return;
        }
      //  this.blocks[this.dropIndex].alpha = 0.5;

        switch (this.animationName) {
            case "drop":
                this.dropText();
                break;

            case "sliceRight":
                this.slideText();
                break;
            case "sliceLeft":
                this.slideText();
                break;
        }
    }
    slideText() {        
        this.scene.tweens.add({ targets: this.blocks[this.dropIndex], duration: 300, x: this.blocks[this.dropIndex].ox });
    }
    dropText() {
        this.scene.tweens.add({ targets: this.blocks[this.dropIndex], duration: 300, y: this.blocks[this.dropIndex].oy });
    }
    done() {
        for (let i:number=0;i<3;i++)
        {
            this.blocks[i].destroy();
        }
        this.timer1?.remove();
        this.callback();
    }
}