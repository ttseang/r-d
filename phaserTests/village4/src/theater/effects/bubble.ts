import { GameObjects } from "phaser";
import IBaseScene from "../../interfaces/IBaseScene";
//import Align from "../util/align";

export class Bubble extends GameObjects.Sprite {
    public next: Bubble | undefined;
    private speed: number = 0;
    private moveSpeed: number = 0;
    private xdir: number = -25;
    private ydir: number = -25;
    public active: boolean = false;
    

    constructor(bscene: IBaseScene, x: number, y: number, key: string) {
        super(bscene.getScene(), x, y, key);
        bscene.getScene().add.existing(this);

        
        //let scale: number = Math.floor(Math.random() * 100) / 1000;

       // Align.scaleToGameW(this, scale, bscene);
        this.speed = Math.floor(Math.random() * 4) / 200;
        this.moveSpeed = Math.random() * 2;
        // this.alpha=0.5;
        this.xdir = Phaser.Math.Between(-1, 1);
        this.ydir = Phaser.Math.Between(-1, 1);

      //  console.log(this.speed);
        // this.speed=Phaser.Math.Between(1,6);
       // this.scene.events.on("preupdate",this.preUpdate.bind(this));
    }
    showAnim(key:string)
    {
        super.play(key);
    }
    destroyMe() {
        console.log("destroy bubble");
        this.scene.events.off("preupdate");
        if (this.next) {
            this.next.destroyMe();
        }
        this.destroy();
    }
    zoomUp() {
        if (this.active === false) {
            if (this.y > 0) {
                if (Phaser.Math.Between(0, 100) < 40) {
                    this.active = true;
                }
            }
        }
        else {
            this.y -= 20;
            if (this.y < -100) {
                this.visible = false;
                this.active = false;
            }
        }

        if (this.next) {
            this.next.zoomUp();
        }
    }
    moveUp() {
        this.alpha -= this.speed * 2;
        this.scaleX -= this.speed;
        this.scaleY -= this.speed;
        this.y -= this.moveSpeed * 4;
        if (this.scaleX < 0) {
            this.visible = false;
        }
        if (this.next) {
            this.next.moveUp();
        }
    }
    move() {
        this.alpha -= this.speed * 2;
        //this.y-=this.speed;
        this.scaleX -= this.speed;
        this.scaleY -= this.speed;
        this.y += this.ydir * this.moveSpeed;
        this.x += this.xdir * this.moveSpeed;

        if (this.scaleX < 0) {
            this.visible = false;
        }
        if (this.next) {
            this.next.move();
        }
        
    }
    preUpdate(time:number,delta:number)
    {
        //console.log(this);
        
        if (this.anims)
        {
            super.preUpdate(time,delta);
        }
        else
        {
            console.log('here');
            this.removeAllListeners();
           // this.removeFromDisplayList();
            this.removeFromUpdateList();
            
            this.destroy();
        }
        
        //console.log("preUpdate");
    }
}