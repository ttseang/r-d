import IBaseScene from "../../interfaces/IBaseScene";
import { ImageMask } from "./imageMask";


export class ImageReveal
{
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private scale: number = 0;
   
    //   private corners: Phaser.GameObjects.Sprite[] = [];
    private timer1: Phaser.Time.TimerEvent | undefined;
   
    public callback: Function = () => { };
    private animationName: string;
    private t: Phaser.GameObjects.Text;
    public tm:ImageMask;
    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text,key:string, animationName: string)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.t=t;
        this.animationName=animationName;
        this.tm=new ImageMask(bscene,"holder",key,0.2);
        this.tm.setFullMask();      
        this.tm.holder.displayWidth=this.t.displayWidth*2;
        

        this.initPos();
        this.tm.holder.setOrigin(0,0);
        this.tm.image.setOrigin(0,0);
        this.tm.image.displayHeight=t.displayHeight*1.5;
        this.tm.image.displayWidth=t.displayWidth*1.1;
        this.tm.x=t.x;
        this.tm.y=t.y;

        
    }
    private initPos()
    {
        switch(this.animationName)
        {
            case "revealLeft":
                this.tm.holder.x=this.t.displayWidth;
                break;

            case "revealRight":
                this.tm.holder.x=-this.t.displayWidth;
                break;

            case "revealUp":
                this.tm.holder.y=this.t.displayHeight;
                break;
            case "revealDown":
                this.tm.holder.y=-this.t.displayHeight;
                break;
        }
    }
    animate()
    {
       this.scene.tweens.add({ targets: this.tm.holder, duration: 1000, x: this.t.x,y:this.t.y,onComplete:this.destroy.bind(this) })
    }
    destroy()
    {
        //this.tm.destroy();
        this.callback();
    }
}