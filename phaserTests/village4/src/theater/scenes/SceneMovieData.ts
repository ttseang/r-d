import MainStorage from "../classes/MainStorage";

import { BaseScene } from "../../scenes/BaseScene";
import { FileVo } from "../mdataObjs/FileVo";

export class SceneMovieData extends BaseScene
{
    private ms:MainStorage=MainStorage.getInstance();

    constructor()
    {
        super("SceneMovieData");
    }
    preload()
    {

    }
    create()
    {
        fetch("assets/movies/startinfo.json")
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any)
    {
        console.log(data);
        let book:string=data.startinfo.book;
        let startPage:string=data.startinfo.startPage;
        let pageCount:number=parseInt(data.startinfo.pageCount);
        let loadPageImages:number=parseInt(data.startinfo.loadPageImages);
        let loadPageAudio:number=parseInt(data.startinfo.loadPageAudio);

       /*  let bgKey:string=data.background;
        this.ms.backgroundImage=bgKey; */

        this.ms.book=book;
        this.ms.currentPage=startPage;
        this.ms.numberOfPages=pageCount;
        if (loadPageImages===1)
        {
            this.ms.loadPageImages=true;
        }
        if (loadPageAudio===1)
        {
            this.ms.loadPageAudio=true;
        }

        let images:any[]=data.images;
        for (let i:number=0;i<images.length;i++)
        {
            let image:FileVo=new FileVo(images[i].key,images[i].path);
            this.ms.customImages.push(image);
        }

        let audio:any[]=data.audio;
        for (let i:number=0;i<audio.length;i++)
        {
            let audioFile:FileVo=new FileVo(audio[i].key,audio[i].path);
            console.log(audioFile);
            this.ms.customAudio.push(audioFile);
        }

        this.scene.start("SceneLoadMovie");
    }
}