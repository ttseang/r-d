import { GameObjects } from "phaser";
import { LayoutManager } from "../classes/layoutManager";
import { MaskObj } from "../classes/MaskObj";
import { CutterPanel } from "../classes/ui/cutterPanel";
import { IconVo } from "../dataObjs/IconVo";
import { MenuVo } from "../dataObjs/menuVo";
import Align from "../../../util/align";
import { BaseScene } from "../../../scenes/BaseScene";

export class SceneCutter extends BaseScene {
    private cutterPanel: CutterPanel;
    private layoutManager: LayoutManager;
    private flyCutter: GameObjects.Sprite;
    private frame: number = -1;
    private dough: GameObjects.Image;
    private maskObj: MaskObj;
    private oven: GameObjects.Image;
    private btnExit: GameObjects.Image;

    constructor() {
        super("SceneCutter");
    }
    preload() {

    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.layoutManager = new LayoutManager(this);
        // this.grid.show();
        this.dough = this.add.sprite(0, 0, "dough");
        Align.scaleToGameW(this.dough, 0.4, this);
        // Align.center(this.dough, this);
        this.grid.placeAt(5, 3, this.dough);
        //
        //
        //
        this.cutterPanel = new CutterPanel(this, this.layoutManager);
        this.cutterPanel.callback = this.makeCutter.bind(this);

        this.btnExit = this.placeImage("btnExit", 12, 0.1);
        this.btnExit.setTint(0xffffff);
        this.btnExit.setInteractive();


        this.btnExit.on("pointerdown", () => {
            {
                this.scene.start("SceneMap");
            }
        })
    }
    makeCutter(frame: number) {
        this.frame = frame;
        let frame2: string = frame.toString() + ".png";
        console.log(frame2);



        this.flyCutter = this.add.sprite(0, 0, "cutters", frame2);
        this.grid.placeAt(5, 8, this.flyCutter);
        Align.scaleToGameW(this.flyCutter, 0.15, this);
        //
        //
        //
        this.tweens.add({ targets: this.flyCutter, duration: 1000, x: this.gw / 2, y: this.gh * .4, onComplete: this.makeShape.bind(this) });
    }
    makeShape() {
        let frame2: string = this.frame.toString() + ".png";
        //let shape: GameObjects.Sprite = this.add.sprite(0, 0, "icing", frame2);
        this.maskObj = new MaskObj(this, "dough", "icing");

        this.maskObj.maskImage.setFrame(frame2);
        Align.scaleToGameW(this.maskObj.backImage, 0.4, this);
        Align.scaleToGameW(this.maskObj.maskImage, 0.15, this);
        this.maskObj.setPos(this.flyCutter.x, this.flyCutter.y);
        this.maskObj.doMask();
        this.dough.visible = false;

        setTimeout(() => {
            this.showOven();
        }, 2000);
        /* shape.x = this.flyCutter.x;
        shape.y = this.flyCutter.y;
        shape.displayWidth = this.flyCutter.displayWidth;
        shape.displayHeight = this.flyCutter.displayHeight; */

    }
    showOven() {
        this.flyCutter.visible = false;
        this.oven = this.add.sprite(0, this.gh / 2, "oven");
        this.children.sendToBack(this.oven);
        this.tweens.add({ targets: this.oven, duration: 1000, x: this.gw / 2, y: this.gh / 2, onComplete: this.ovenInPlace.bind(this) });

    }
    ovenInPlace() {
        this.tweens.add({ targets: [this.maskObj.backImage, this.maskObj.maskImage], duration: 1000, scaleX: 0.01, scaleY: 0.01, onComplete: this.cookieInOver.bind(this) });

    }
    cookieInOver() {
        this.maskObj.backImage.visible = false;
        this.maskObj.maskImage.visible = false;

        let mw: Phaser.Sound.BaseSound = this.sound.add("ovenSound");
        mw.play();
        setTimeout(() => {
            this.bakingDone();
        }, 8000);
    }
    bakingDone() {
        this.tweens.add({ targets: this.oven, duration: 1000, x: this.gw + 500, onComplete: this.ovenGone.bind(this) });
    }
    ovenGone() {
        this.scene.start("SceneCookie");
    }
}