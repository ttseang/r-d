import { CookieModel } from "../classes/CookieModel";
import { GM } from "../../../classes/GM";
import { FileVo } from "../dataObjs/fileVo";
import { IconVo } from "../dataObjs/IconVo";
import { ItemVo } from "../dataObjs/itemVo";
import { LayoutVo } from "../dataObjs/layoutVo";
import { MenuVo } from "../dataObjs/menuVo";
import { SposVo } from "../dataObjs/sposVo";

export class SceneCookieData extends Phaser.Scene
{
    private cm:CookieModel=CookieModel.getInstance();

    constructor()
    {
        super("SceneCookieData");
    }
    preload()
    {

    }
    create()
    {
        fetch("./assets/cookiegame/"+this.cm.objectFile)
        .then(response => response.json())
        .then(data => this.process({ data }));
    }
    process(data:any)
    {
       //console.log(data);
       this.cm.jsonAssets=[];
        this.cm.images=[];
        
        let gameData:any=data.data.game;

        this.cm.assetFolder=gameData.assetFolder;
        this.cm.baseKey=gameData.baseKey;
        this.cm.baseItemPrefix=gameData.baseItemPrefix;
        this.cm.baseItemSufix=gameData.baseItemSufix;
        this.cm.cookieIndex=parseInt(gameData.defaultIndex);
        this.cm.scratchKey=gameData.scratchkey;
        //
        //
        //
        this.cm.iconScales=gameData.iconScales;
        this.cm.baseScales=gameData.baseScales;
        this.cm.baseBackScales=gameData.baseBackScales;

        this.cm.afterLoadScene=gameData.afterload;

        for (let i:number=0;i<gameData.jsonAssets.length;i++)
        {
            let data2:string=gameData.jsonAssets[i];
           
            this.cm.jsonAssets.push(data2);
        }

        let images:any[]=gameData.images;
        let imageLen:number=images.length;
        
      //  console.log(images);

        for (let i:number=0;i<imageLen;i++)
        {
            let image:any=gameData.images[i];
            let fileVo:FileVo=new FileVo(image.key,image.file);
           // console.log(fileVo);
            this.cm.images.push(fileVo);
        }

        let audios:any[]=gameData.audios;
        let audioLen:number=audios.length;
        
        

        for (let i:number=0;i<audioLen;i++)
        {
            let audio:any=gameData.audios[i];
            let fileVo:FileVo=new FileVo(audio.key,audio.file);
           // console.log(fileVo);
            this.cm.audios.push(fileVo);
        }
        //
        //LAYOUTS
        //
        let layouts:any=gameData.layouts;
      
        for(let i:number=0;i<layouts.length;i++)
        {
            let layout:any=layouts[i];
            let id:string=layout.id;
            let layoutVo:LayoutVo=new LayoutVo(id);
            let items:any[]=layout.items;
            for (let j:number=0;j<items.length;j++)
            {
                console.log(items[j]);
                let itemVo:ItemVo=new ItemVo(items[j].key,items[j].action);
                let pos:any[]=items[j].pos;
                for (let k:number=0;k<pos.length;k++)
                {
                    itemVo.add(new SposVo(pos[k].key,parseFloat(pos[k].x),parseFloat(pos[k].y),parseFloat(pos[k].scale)));
                    
                }
                layoutVo.add(itemVo);
               // layoutVo.add();
            }
           // layoutVo.items=items;
            this.cm.layouts.set(layoutVo.id,layoutVo);
        }
        //
        //
        //
        let menus:any=gameData.menus;
        //console.log(menus);
        for (let i:number=0;i<menus.length;i++)
        {
            let menu:any=menus[i];
            ////console.log(menu);
            let menuVo:MenuVo=new MenuVo(menu.name);

            let items:any[]=menu.items;

            for (let j:number=0;j<items.length;j++)
            {
                let item:any=items[j];
                ////console.log(item);
                let scales:string="";
                if (item.scales)
                {
                    scales=item.scales;
                }
                menuVo.items.push(new IconVo(item.name,item.icon,item.action,scales));
                this.cm.menus.set(menuVo.name,menuVo);
            }
        }
        this.scene.start("SceneCookieLoad");
    }
}