export class FileVo
{
    public file:string;
    public key:string;

    constructor(key:string,file:string)
    {
        this.key=key;
        this.file=file;
    }
}