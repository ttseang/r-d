import { ItemVo } from "./itemVo";
export class LayoutVo {
    public id: string;
    public items: ItemVo[] = [];

    constructor(id: string) {
        this.id = id;
    }
    public add(item: ItemVo) {
        this.items.push(item);
    }
}