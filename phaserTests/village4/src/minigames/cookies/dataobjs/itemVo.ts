import { SposVo } from "./sposVo";

export class ItemVo
{
    public key:string;
    public action:string;

    public scalePos:Map<string,SposVo>=new Map();

    constructor(key:string,action:string)
    {
        this.key=key;
        this.action=action;
    }
    public add(scalePosObj:SposVo)
    {
        this.scalePos.set(scalePosObj.dkey,scalePosObj);
        //this.scalePos.push(scalePosObj);
    }
}