import { GameObjects } from "phaser";
import IBaseScene from "../../../interfaces/IBaseScene";
import Align from "../../../util/align";
import { CookieModel } from "./CookieModel";
import { LayoutManager } from "./layoutManager";

export class Dragger {
    private bscene: IBaseScene;
    public scene: Phaser.Scene;

    private dragObj: GameObjects.Sprite;
    private maskImage: GameObjects.Image;
    private canDrag: boolean = true;
    private lm: LayoutManager;
    private gm: CookieModel = CookieModel.getInstance();
    public dragKey: string;
    public dragFrame: string;
    private scales: string;
    public angle: number = 0;
    public scale: number = 2;

    constructor(bscene: IBaseScene, lm: LayoutManager, xx: number, yy: number, dragKey: string, dragFrame: string, maskKey: string, scales: string) {
        //super(bscene.getScene());

        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.lm = lm;
        this.dragKey = dragKey;
        this.dragFrame = dragFrame;
        this.scales = scales;

        let scale: number = this.lm.getDynamicScale(scales);

        this.dragObj = this.scene.add.sprite(xx, yy, dragKey).setOrigin(0.5, 0.5);

        this.dragObj.setInteractive();
        this.dragObj.once('pointerdown', this.startDrag.bind(this));
        // this.add(this.dragObj);

        Align.scaleToGameW(this.dragObj, scale, this.bscene);



        if (maskKey !== "") {
            this.maskImage = this.scene.add.image(xx, yy, maskKey);
            Align.scaleToGameW(this.maskImage, lm.getScale("baseBack"), this.bscene);
            this.dragObj.setFrame(dragFrame);
            // this.maskImage.alpha=.5;

            //  this.add(this.maskImage);
            this.dragObj.setMask(new Phaser.Display.Masks.BitmapMask(this.scene, this.maskImage));

            this.maskImage.visible = false;
        }

        // this.scene.add.existing(this);
    }
    public turnClockwise() {
        this.angle++;
        if (this.angle > 23) {
            this.angle = 0;
        }
        this.updateAngle();
    }
    public turnCounter() {
        this.angle--;
        if (this.angle < 0) {
            this.angle = 23;
        }
        this.updateAngle();
    }
    private updateAngle() {
        this.dragObj.setAngle(this.angle * 15);
    }
    public upScale() {
        this.scale++;
        if (this.scale > 4) {
            this.scale = 4;
        }
        this.updateScale();
    }

    public downScale() {
        this.scale--;
        if (this.scale < 0) {
            this.scale = 0;
        }
        this.updateScale();
    }
    private updateScale() {
        let scale: number = this.lm.getDynamicScale(this.scales);
        let mods: number[] = [(scale / 2), scale / 1.5, scale, scale * 1.5, scale * 2];
        Align.scaleToGameW(this.dragObj, mods[this.scale], this.bscene);
    }
    public removeMe()
    {
        this.dragObj.off('pointerdown');
        this.gm.currentObject=null;
        this.dragObj.destroy();
        
    }
    startDrag() {
        this.gm.currentObject = this;
        this.canDrag = true;
        this.scene.input.on('pointermove', this.isDragging.bind(this));
        this.scene.input.on('pointerup', this.stopDrag.bind(this));

    }
    stopDrag() {
        this.scene.input.off('pointermove');
        this.scene.input.off('pointerup');
        this.dragObj.once('pointerdown', this.startDrag.bind(this));
    }
    isDragging(p: Phaser.Input.Pointer) {
        this.dragObj.x = p.x;//-this.x;
        this.dragObj.y = p.y;//-this.y;
    }
}