import { GameObjects } from "phaser";
import { IconVo } from "../../dataObjs/IconVo";
import IBaseScene from "../../../../interfaces/IBaseScene";
import Align from "../../../../util/align";
import { CookieModel } from "../CookieModel";
import { LayoutManager } from "../layoutManager";

export class EditButton extends Phaser.GameObjects.Container
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    private back:GameObjects.Sprite;
  //  private text1:GameObjects.Text;
    private lm:LayoutManager;
    private gm:CookieModel=CookieModel.getInstance();

    public callback:Function;
    
    constructor(bscene:IBaseScene,iconKey:string,lm:LayoutManager,dp:IconVo)
    {
        super(bscene.getScene());
        
        this.bscene=bscene;
        this.lm=lm;

        this.back=this.scene.add.sprite(0,0,iconKey);
        let frame:string=dp.frame+".png";
        this.back.setFrame(frame);

        Align.scaleToGameW(this.back,this.lm.getDynamicScale(this.gm.editIconScale),this.bscene);
        this.add(this.back);

      /*   this.text1=this.scene.add.text(0,0,dp.name,{color:"#ffffff",backgroundColor:"#000000",fontSize:"26px"}).setOrigin(0.5,0.5);
        this.add(this.text1);
        this.text1.y=this.back.y+this.back.displayHeight/2; */


        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.scene.add.existing(this);
        this.setInteractive();
        this.on('pointerdown',()=>{this.callback(dp)})
    }
}