import { GameObjects } from "phaser";
import { IconVo } from "../../dataObjs/IconVo";
import { PosVo } from "../../../../dataObjs/PosVo";
import IBaseScene from "../../../../interfaces/IBaseScene";
import { AlignGrid } from "../../../../util/alignGrid";
import { CookieModel } from "../CookieModel";
import { LayoutManager } from "../layoutManager";
import { CutterButton } from "./cutterButton";
import { EditButton } from "./editButton";

export class CutterPanel extends Phaser.GameObjects.Container {
    private bscene: IBaseScene;
    public scene: Phaser.Scene;
    private back: GameObjects.Image;
    private grid: AlignGrid;
    private lm: LayoutManager;
    private buttonArray: CutterButton[] = [];
    private gm:CookieModel=CookieModel.getInstance();
    private clickLock:boolean=false;
    private selectedAction:number=-1;
    public callback:Function=()=>{};

    constructor(bscene: IBaseScene, lm: LayoutManager) {
        super(bscene.getScene());

        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.lm = lm;

        this.back = this.scene.add.image(0, 0, "holder").setOrigin(0, 0);
        this.back.displayWidth = this.bscene.getW();
        this.back.displayHeight = this.bscene.getH() * 0.35;
        this.back.setTint(0xfff000);
        this.add(this.back);
        this.y = this.bscene.getH() - this.back.displayHeight;
        this.back.setInteractive();

        this.setSize(this.back.displayWidth, this.back.displayHeight);

        this.grid = new AlignGrid(this.bscene, 11, 11, this.back.displayWidth, this.back.displayHeight);
       // this.grid.showPos();

        this.scene.add.existing(this);

        let buttons: IconVo[] = [];
        buttons.push(new IconVo("", "0", "0"));
        buttons.push(new IconVo("", "1", "1"));
        buttons.push(new IconVo("", "2", "2"));
        buttons.push(new IconVo("", "3", "3"));
        //  buttons.push(new IconVo("btnDel", "trash", "delete"));

        this.makeButtons(buttons);

    }
    public makeButtons(buttons: IconVo[]) {
        let pos: PosVo[] = this.lm.getCutterButtonPos();

        while (this.buttonArray.length > 0) {
            this.buttonArray.pop().destroy();
        }
        for (let i: number = 0; i < buttons.length; i++) {
            let cutterButton: CutterButton = new CutterButton(this.bscene, "cutters", this.lm, buttons[i]);
            cutterButton.callback = this.buttonPressed.bind(this);
            this.add(cutterButton);

            // this.iconMap.set(buttons[i].name, editButton);
            // this.buttonArray.push(editButton);

            let pos2: PosVo = pos[i];
            this.grid.placeAt(pos2.x, pos2.y, cutterButton);
        }
        //this.bringToTop(this.back);
    }
    private buttonPressed(iconVo: IconVo) {
        console.log(iconVo);
        if (this.clickLock===true)
        {
            return;
        }
        this.clickLock=true;
        this.selectedAction=parseInt(iconVo.action);
        this.gm.cookieIndex=this.selectedAction;
        this.callback(this.selectedAction);
        this.visible=false;
    }
   
}