import { GameObjects } from "phaser";
import { ItemVo } from "../dataObjs/itemVo";
import { LayoutVo } from "../dataObjs/layoutVo";
import { SposVo } from "../dataObjs/sposVo";
import IBaseScene from "../../../interfaces/IBaseScene";
import Align from "../../../util/align";
import { AlignGrid } from "../../../util/alignGrid";
import { Controller } from "./Controller";
import { CookieModel } from "./CookieModel";

import { LayoutManager } from "./layoutManager";

export class LayoutBuilder
{
    private scene:Phaser.Scene;
    private bscene:IBaseScene;
    private cm:CookieModel=CookieModel.getInstance();
    private lm:LayoutManager;
    private grid:AlignGrid;
    private controller:Controller=Controller.getInstance();

    constructor(bscene:IBaseScene,lm:LayoutManager)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.lm=lm;
        this.grid=bscene.getGrid();
    }
    build(name:string)
    {
        let layout:LayoutVo=this.cm.layouts.get(name);
        let items:ItemVo[]=layout.items;
        for (let i:number=0;i<items.length;i++)
        {
            let item:ItemVo=items[i];
            let layoutKey:string=this.lm.getLayoutKey();

            let scalePos:SposVo=item.scalePos.get(this.lm.getLayoutKey());
            let image:GameObjects.Image=this.scene.add.image(0,0,item.key);
            Align.scaleToGameW(image,scalePos.scale,this.bscene);
            this.grid.placeAt(scalePos.x,scalePos.y,image);
            //
            //
            //
            if (item.action!=="")
            {
                image.setInteractive();
                image.on('pointerdown',()=>{
                    this.controller.doAction(item.action);
                })
            }
        }
    }
}