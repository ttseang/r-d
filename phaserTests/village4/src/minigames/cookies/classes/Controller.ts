let instance: Controller = null;
export class Controller {
    public addScratch: Function = () => { };
    public openSide: Function = () => { };
    public closeSide: Function = () => { };
    public addDrag: Function = () => { };
    public selectDragObj: Function = () => { };
    public openEdit: Function = () => { };
    public closeEdit: Function = () => { };
    public saveImage: Function = () => { };

    constructor() {

    }
    doAction(action: string) {
        console.log(action);
        
        switch (action) {
            case "open":
                this.openSide();
                break;

            case "saveImage":
                this.saveImage();
                break;
        }
    }
    public static getInstance() {
        if (instance === null) {
            instance = new Controller();
        }
        return instance;
    }
}