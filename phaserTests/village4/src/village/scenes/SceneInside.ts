import { GameObjects } from "phaser";
import { MessageBox } from "../../classes/comps/MessageBox";
import { GM } from "../../classes/GM";
import Align from "../../util/align";
import { BaseScene } from "../../scenes/BaseScene";
import { LocButtonVo } from "../../dataObjs/LocButtonVo";
import { CookieModel } from "../../minigames/cookies/classes/CookieModel";
import { Face } from "../../puppet/Face";
import { ChatBubble } from "../../puppet/ChatBubble";

export class SceneInside extends BaseScene {
    private backImage: GameObjects.Image;
    private gm: GM = GM.getInstance();
    private clickLock: boolean = true;
    private mb: MessageBox;
    private btnExit: GameObjects.Image;
    private btnGame: GameObjects.Image;

    private bubble:ChatBubble;
    private face: Face;

    private eyeDir:number=1;
    private eyeX:number=300;
    private eyeY:number=400;

    constructor() {
        super("SceneInside");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        
        let bg:GameObjects.Image = this.add.image(0, 0, "greycity");
        Align.scaleToGameW(bg, 1, this);
        if (bg.displayHeight<this.gh)
        {
            bg.displayHeight=this.gh;
            bg.scaleX=bg.scaleY;
        }
        Align.center(bg, this);

      //  this.grid.showNumbers();

        console.log(this.gm.currentLoc);

        this.backImage = this.add.image(0, 0, this.gm.currentBuilding);
        Align.scaleToGameW(this.backImage, 0.35, this);
        Align.center(this.backImage, this);

        this.btnExit = this.placeImage("btnExit", 20, 0.1);
        this.btnExit.setTint(0x000000);


        this.btnExit.on("pointerdown", () => {
            if (this.clickLock == false) {
                this.gm.unlockBuilding(this.gm.currentLoc.unlockNext);
                this.fadeScene();
            }
        })

        let gameButton: LocButtonVo = this.gm.currentLoc.button;

        if (gameButton.key !== "") {
            this.btnGame = this.placeImage(gameButton.key, 64, 0.15);
            this.btnGame.on("pointerdown", () => {
                console.log(gameButton);
                this.gm.unlockBuilding(this.gm.currentLoc.unlockNext);
                if (gameButton.type === "cookie") {
                    let cm: CookieModel = CookieModel.getInstance();
                    cm.objectFile = gameButton.file;

                    if (this.mb) {
                        this.mb.closeCallback = () => { };
                    }

                    this.scene.start("SceneCookieData");
                    return;
                }
                if (gameButton.type === "movie") {
                    if (this.mb) {
                        this.mb.closeCallback = () => { };
                    }
                    this.scene.start("SceneMovieData");
                    return;
                }

            });
        }



        this.mb = new MessageBox(this);
        this.mb.closeCallback = this.messageClose.bind(this);

        if (this.gm.currentLoc.level !== -1) {
            setTimeout(() => {
                let level: string = this.gm.currentLoc.level.toString();
                this.mb.showMessage("Welcome To Chapter " + level + "!", "I Have Completed\nChapter " + level);
            }, 1500);
        }
        else {
            this.clickLock = false;
            this.btnExit.setInteractive();
            if (this.btnGame) {
                console.log(this.btnGame);
                if (this.btnGame.scene !== undefined) {
                    this.btnGame.setInteractive();
                }
            }
        }
        if (this.gm.currentLoc.message!=="")
        {
        this.face=new Face(this);
        this.face.y=this.gh-this.face.displayHeight;
        window['face']=this.face;

        this.bubble=new ChatBubble(this,this.gm.currentLoc.message);
        this.bubble.x=this.face.x+this.face.displayWidth/2;
        this.bubble.y=this.face.y-this.bubble.displayHeight/2;
      //  this.grid.placeAt(1,1,this.bubble);
        }
    }
    fadeScene() {
        this.tweens.add({ targets: this.backImage, duration: 1000, alpha: 0.1, onComplete: this.fadeDone.bind(this) });
    }
    fadeDone() {
        this.scene.start("SceneMap");
    }
    messageClose() {
        this.clickLock = false;
        this.btnExit.setInteractive();
        if (this.btnGame) {
            console.log(this.btnGame);
            if (this.btnGame.scene !== undefined) {
                this.btnGame.setInteractive();
            }

        }
        console.log("message close");
    }
    update()
    {
        //this.face.lookAt(this.eyeX,this.eyeY);
        this.eyeX+=this.eyeDir;
        
        /* this.eyeX+=this.eyeDir;
        this.eyeY+=this.eyeDir; */
        if (this.eyeX/100===Math.floor(this.eyeX/100))
        {
            this.face.blink();
        }       
        if (this.eyeX>500 || this.eyeX<300)
        {
            this.eyeDir=-this.eyeDir;
        }
    }
}