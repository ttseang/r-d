import { GameObjects } from "phaser";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { BaseScene } from "../scenes/BaseScene";
import Align from "../util/align";
import { AlignGrid } from "../util/alignGrid";
import { GM } from "./GM";

export class SPhysicObj extends Phaser.Physics.Arcade.Sprite implements IGameObj
{
    private grid:AlignGrid;
    public layout:LayoutVo;
    public pos:ResizeVo;
    public scene:Phaser.Scene;

    private bscene:IBaseScene;
   
    private gm:GM=GM.getInstance();

    constructor(bscene:IBaseScene,key:string)
    {
        super(bscene.getScene(),0,0,key);

        this.grid=bscene.getGrid();
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.type="sphysicsObj";
        
        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
        /*  setInterval(() => {
            this.updatePos();
        }, 50); */
    }
    updatePos()
    {       
       /* let posVo:PosVo=this.grid.findNearestGridXY(this.x,this.y);
       // console.log(posVo);
        
        this.pos.posX=posVo.x;
        this.pos.posY=posVo.y; */

    }
    update()
    {      
       

 //       this.updatePos();
      /*   this.pos=this.layout.getDefinition(this.gm.device,this.gm.orientation);
        this.grid=this.bscene.getGrid();
        this.grid.placeAt(this.pos.posX,this.pos.posY,this);
        Align.scaleToGameW(this,this.pos.scale,this.bscene); */
    }
}