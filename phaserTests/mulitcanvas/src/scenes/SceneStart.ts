import { Button } from "../classes/comps/Button";
import { PhysicObj } from "../classes/PhysObj";
import { ResizeManager } from "../classes/resizeManager";
import { SPhysicObj } from "../classes/SPhysObj";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "./BaseScene";
//import { SpineAnim2 } from "../classes/SpinAnim2";

//import { FallingObj } from "../classes/FallingObj";

export class SceneStart extends BaseScene implements IBaseScene {
    private button1: Button;
    private resizeManager: ResizeManager;
    private ball:PhysicObj;
    private firstObj:PhysicObj;
    private blocks:Phaser.Physics.Arcade.Group;

    constructor() {
        super("SceneStart");
    }
    preload() {
        this.load.setPath("./assets/main");

        this.load.image("button", "ui/buttons/1/1.png");
        this.load.image("circle","circle.png");
        this.load.image("holder","holder.png");
    }
    create() {
        super.create();
        this.makeGrid(22, 22);
     //   this.grid.showPos();
        this.game.canvas.id="thecanvas";
        
        this.blocks=this.physics.add.group();

        this.resizeManager = ResizeManager.getInstance(this);

        window['scene'] = this;

        this.makeChildren();
        //this.definePos();
        this.placeItems();

        this.physics.add.collider(this.ball,this.blocks);
    }
    makeChildren() {
       
        let keys:string[]=[];
      // this.button1 = new Button(this, "button", "Start");
        this.ball=new PhysicObj(this,"circle");
        this.ball.setBounce(1,1);
        let ballLayout:LayoutVo=this.resizeManager.addItem("ball",this.ball);
        ballLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",2,2,.01));
        this.ball.pos=ballLayout.defaultResize;

        this.ball.layout=ballLayout;

        this.firstObj=this.ball;

        this.ball.setTint(0xf1c40f);
        this.ball.setVelocity(300,300);
        this.ball.setCollideWorldBounds(true);

       for (let i:number=0;i<5;i++)
       {
           for (let j:number=0;j<4;j++)
           {
              let block:SPhysicObj=new SPhysicObj(this,"holder");
              
              let blockKey:string="block"+i.toString()+"_"+j.toString();
            
              let blockLayout:LayoutVo=this.resizeManager.addItem(blockKey,block);
              blockLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",4+i*3,2+j*4,.05));
              block.setTint(0x3498db);
             
              block.layout=blockLayout;
              this.blocks.add(block);
              block.setImmovable(true);
              keys.push(blockKey);
           }
       }
       keys.push("ball");
       this.resizeManager.setKeys(keys);
    }

    definePos() {
        
        //
        //
        //
       /*  let buttonLayout: LayoutVo = this.resizeManager.addItem("button1", this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 10, 10, 0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,10,19,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,3,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,19,19,0.4));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,2,2,.1)); */

      //  keys.push("button1");

       

        // this.resizeManager.setKeys(['button1','face']);

    }
    placeItems() {
        this.resizeManager.placeChildren();
    }
    update()
    {
        this.firstObj.updatePos();
    }
}