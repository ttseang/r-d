class Board
{
    
    constructor()
    {
        this.words="tidy,liberty,balcony,defy,comply,identify,bystander,stylus,dehydrate,myth,mystery,system,symptom,sympathy,crystal,Olympics,calculate,subway,shadow,logo".split(',');
        this.questions=[];

        this.questIndex=-1;
        this.questions.push({quest:" If you go to summer camp you have to <u>go along</u> with the rules",answer:"comply"});
        this.questions.push({quest:"you can stay more organized if your room is <u>neat</u>",answer:"tidy"});
        this.questions.push({quest:"America was built on the values of life, <u>freedom</u>, and the pursuit of happiness",answer:"liberty"});
        this.questions.push({quest:"<u>A person walking by</u> when the accident happened was injured.",answer:"bystander"});
        this.questions.push({quest:" Athletes drink lots of water so they don't <u>lose too much</u> of it during physical activity.",answer:"dehydrate"});
        this.questions.push({quest:"Drivers who <u>disobey</u> traffic laws deserve to get a ticket.",answer:"defy"});
        this.questions.push({quest:"Some people believe the <u>story</u> about the existence of a creature called Big Foot",answer:"mystery"});
        this.questions.push({quest:"Can you <u>name</u> twenty kinds of dogs if I show you pictures?",answer:"identify"});
        this.questions.push({quest:"Do you know which company has a swoosh as a <u>trademark</u>?",answer:"logo"}); 
       
        

    }
    render()
    {
        document.getElementById("wordArea").innerHTML=this.renderWords();

        var elements = document.getElementsByClassName("btnword");
      
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', this.cardClicked.bind(this), false);
        }
        this.nextQuest();
    }
    resetButtons()
    {
        var elements = document.getElementsByClassName("btnwrong");
        let len=elements.length;

        while(elements.length>0)
        {
            console.log(elements.length);
            elements[0].className="btn btn-dark btnword";
        }
    }
    nextQuest()
    {
        this.resetButtons();
        this.questIndex++;
        if (this.questIndex>this.questions.length-1)
        {
            console.log("out of questions");
            return;
        }
        document.getElementById("questArea").innerHTML=this.questions[this.questIndex].quest;

    }
    renderWords()
    {
        //let grid="";
       let grid="<div class='row gx-1 gy-5'>";
        for (let i=0;i<this.words.length;i++)
        {
            grid+="<div class='col-sm-3'>";
            grid+=this.getCard(this.words[i],i);
            grid+="</div>";
        }
        grid+="</div>";
        return grid;
    }
    cardClicked(e)
    {
        if (e.target.used===true)
        {
            return;
        }
        //console.log(e);
        let index=e.target.id;
        index=index.replace("word","");

        let answer=this.questions[this.questIndex].answer;
    
      //  console.log(this);
        let word=this.words[index];
        console.log(word);

        if (word==answer)
        {
            console.log("Correct");
            e.target.used=true;

            e.target.className="btn correctbox";
            this.nextQuest();

        }
        else
        {
            console.log("Wrong");
            e.target.className="btn btnwrong";
            window.target=e.target;
            setTimeout(() => {
                this.nextQuest();
            }, 2000);
        }
    }
    getCard(word,index)
    {
        let c="<span class='btn btn-dark btnword' id='word"+index+"'>"+word+"</span>";
        return c;
    }
}