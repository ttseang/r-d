import { AnchorVo } from "./AnchorVo";
export declare class CompVo {
    key: string;
    type: string;
    x: number;
    y: number;
    w: number;
    h: number;
    style: string;
    backstyle: string;
    text: string;
    flipX: boolean;
    flipY: boolean;
    angle: number;
    anchorVo: AnchorVo;
    icon: string;
    action: string;
    actionParam: string;
    constructor(key: string, type: string, text: string, icon: string, x: number, y: number, w: number, h: number, style: string, backstyle: string);
}
