export declare class BackStyleVo {
    backColor: number;
    borderColor: number;
    borderPress: number;
    borderOver: number;
    borderThick: number;
    round: boolean;
    constructor(backColor: number, borderThick: number, borderColor: number, borderPress: number, borderOver: number);
}
