"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StrokeVo = void 0;
var StrokeVo = /** @class */ (function () {
    function StrokeVo(thickness, color) {
        this.thickness = thickness;
        this.color = color;
    }
    return StrokeVo;
}());
exports.StrokeVo = StrokeVo;
//# sourceMappingURL=StrokeVo.js.map