"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageVo = void 0;
var PageVo = /** @class */ (function () {
    function PageVo(pageName, comps) {
        this.pageName = "";
        this.backgroundType = "color";
        this.backgroundParams = "0xff0000";
        this.pageName = pageName;
        this.comps = comps;
    }
    return PageVo;
}());
exports.PageVo = PageVo;
//# sourceMappingURL=PageVo.js.map