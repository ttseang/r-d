import { ShadowVo } from "./ShadowVo";
import { StrokeVo } from "./StrokeVo";

export class TextStyleVo
{
    public strokeVo:StrokeVo;
    public shadowVo:ShadowVo;

    public fontName:string;
    public textColor:string;
    public maxFontSize:number;

    constructor(fontName:string,textColor:string,maxFontSize:number)
    {
        this.fontName=fontName;
        this.textColor=textColor;
        this.maxFontSize=maxFontSize;
    }
}