"use strict";
//import { BackStyleVo } from "./BackStyleVo";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ButtonStyleVo = void 0;
var ButtonStyleVo = /** @class */ (function () {
    function ButtonStyleVo(textStyle, hsize, vsize, backStyle) {
        this.textStyle = textStyle;
        this.hsize = hsize;
        this.vsize = vsize;
        this.backStyle = backStyle;
    }
    return ButtonStyleVo;
}());
exports.ButtonStyleVo = ButtonStyleVo;
//# sourceMappingURL=ButtonStyleVo.js.map