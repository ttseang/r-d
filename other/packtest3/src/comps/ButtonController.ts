
export class ButtonController
{
    private static instance:ButtonController | null=null;

    public callback:Function=()=>{};

    constructor()
    {

    }
   public static   getInstance():ButtonController
    {
        if (this.instance===null)
        {
            this.instance=new ButtonController();
        }
        return this.instance;
    }

    public doAction(action:string,actionParam:string)
    {        
        this.callback(action,actionParam);
    }
}