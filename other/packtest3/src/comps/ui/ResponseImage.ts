
import { IComp, PosVo, IBaseScene, Align } from "../..";

import { CompManager } from "../CompManager";

export class ResponseImage extends Phaser.GameObjects.Image implements IComp
{
    public posVo:PosVo=new PosVo(0,0);
    private bscene:IBaseScene;
    private hscale:number;
    private cm:CompManager=CompManager.getInstance();

    constructor(bscene:IBaseScene,key:string,hscale:number,imageKey:string)
    {
        super(bscene.getScene(),0,0,imageKey);
        this.bscene=bscene;
        this.hscale=hscale;
        Align.scaleToGameW(this,hscale,bscene);

        this.scene.add.existing(this);

        this.cm.comps.push(this);
        this.cm.compMap.set(key,this);
    }

    doResize()
    {
        Align.scaleToGameW(this,this.hscale,this.bscene);
        this.bscene.getGrid().placeAt(this.posVo.x,this.posVo.y,this);
    }
    setPos(xx:number,yy:number)
    {
        this.posVo=new PosVo(xx,yy);
        this.bscene.getGrid().placeAt(xx,yy,this);
    }
}