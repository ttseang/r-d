import { IComp, TextStyleVo, IBaseScene } from "../..";
import { BaseComp } from "./BaseComp";
export declare class TextComp extends BaseComp implements IComp {
    private text1;
    ww: number;
    textStyleVo: TextStyleVo;
    constructor(bscene: IBaseScene, key: string, text: string, maxWidth: number, textStyle: string);
    setText(text: string): void;
    setColor(c: string): void;
    sizeText(): void;
    doResize(): void;
}
