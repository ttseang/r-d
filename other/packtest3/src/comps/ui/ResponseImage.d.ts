import { GameObjects } from "phaser";
import { IComp, PosVo, IBaseScene } from "../..";
export declare class ResponseImage extends GameObjects.Image implements IComp {
    posVo: PosVo;
    private bscene;
    private hscale;
    private cm;
    constructor(bscene: IBaseScene, key: string, hscale: number, imageKey: string);
    doResize(): void;
    setPos(xx: number, yy: number): void;
}
