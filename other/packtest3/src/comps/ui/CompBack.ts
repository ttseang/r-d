
import { BackStyleVo, IBaseScene } from "../..";


export class CompBack extends Phaser.GameObjects.Graphics {
    private backStyle: BackStyleVo;
    private ww: number = 0;
    private hh: number = 0;
    constructor(bscene: IBaseScene, ww: number, hh: number, backStyle: BackStyleVo) {
        super(bscene.getScene());

        this.backStyle = backStyle;
        this.ww = ww;
        this.hh = hh;


        this.doResize(ww,hh);

        this.scene.add.existing(this);
    }

    doResize(ww: number, hh: number) {
        this.ww = ww;
        this.hh = hh;

        this.clear();

        this.lineStyle(this.backStyle.borderThick, this.backStyle.borderColor);
        if (this.backStyle.round === true) {
            this.strokeRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        }
        else {
            this.strokeRect(-ww / 2, -hh / 2, ww, hh);
        }
        this.stroke();

        this.fillStyle(this.backStyle.backColor, 1);
        if (this.backStyle.round === true) {
            this.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        }
        else {
            this.fillRect(-ww / 2, -hh / 2, ww, hh);
        }
        this.fillPath();

    }
}