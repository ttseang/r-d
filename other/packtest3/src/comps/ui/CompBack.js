"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompBack = void 0;
var phaser_1 = require("phaser");
var CompBack = /** @class */ (function (_super) {
    __extends(CompBack, _super);
    function CompBack(bscene, ww, hh, backStyle) {
        var _this = _super.call(this, bscene.getScene()) || this;
        _this.ww = 0;
        _this.hh = 0;
        _this.backStyle = backStyle;
        _this.ww = ww;
        _this.hh = hh;
        _this.doResize(ww, hh);
        _this.scene.add.existing(_this);
        return _this;
    }
    CompBack.prototype.doResize = function (ww, hh) {
        this.ww = ww;
        this.hh = hh;
        this.clear();
        this.lineStyle(this.backStyle.borderThick, this.backStyle.borderColor);
        if (this.backStyle.round === true) {
            this.strokeRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        }
        else {
            this.strokeRect(-ww / 2, -hh / 2, ww, hh);
        }
        this.stroke();
        this.fillStyle(this.backStyle.backColor, 1);
        if (this.backStyle.round === true) {
            this.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        }
        else {
            this.fillRect(-ww / 2, -hh / 2, ww, hh);
        }
        this.fillPath();
    };
    return CompBack;
}(phaser_1.GameObjects.Graphics));
exports.CompBack = CompBack;
//# sourceMappingURL=CompBack.js.map