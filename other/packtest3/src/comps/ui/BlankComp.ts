
import { IBaseScene, BackStyleVo } from "../..";
import { BaseComp } from "./BaseComp";
import { CompBack } from "./CompBack";

export class BlankComp extends BaseComp
{
    private back: CompBack;
    
    constructor(bscene:IBaseScene,key:string,obj:any,style:string)
    {
        super(bscene,key);

        let backStyleVo:BackStyleVo=this.cm.getBackStyle(style);

        this.back=new CompBack(bscene,obj.displayWidth,obj.displayHeight,backStyleVo);
        
        

        this.add(this.back);
        this.add(obj);

        this.scene.add.existing(this);
    }
}