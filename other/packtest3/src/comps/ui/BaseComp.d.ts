import { GameObjects } from "phaser";
import { IComp, PosVo, IBaseScene, BackStyleVo, TextStyleVo } from "../..";
import CompManager from "../CompManager";
export declare class BaseComp extends GameObjects.Container implements IComp {
    posVo: PosVo;
    bscene: IBaseScene;
    protected cm: CompManager;
    private anchorObj;
    private anchorX;
    private anchorY;
    private anchorInside;
    protected backStyleVo: BackStyleVo;
    protected textStyleVo: TextStyleVo;
    ingoreRepos: boolean;
    key: string;
    constructor(bscene: IBaseScene, key: string);
    doResize(): void;
    setPos(xx: number, yy: number): void;
    anchorTo(obj: IComp, anchorX: number, anchorY: number, inside: boolean): void;
    reAnchor(): void;
}
