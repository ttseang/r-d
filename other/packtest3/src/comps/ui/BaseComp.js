"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseComp = void 0;
var phaser_1 = require("phaser");
var __1 = require("../..");
var CompManager_1 = __importDefault(require("../CompManager"));
var BaseComp = /** @class */ (function (_super) {
    __extends(BaseComp, _super);
    function BaseComp(bscene, key) {
        var _this = _super.call(this, bscene.getScene()) || this;
        _this.posVo = new __1.PosVo(0, 0);
        _this.cm = CompManager_1.default.getInstance();
        _this.anchorObj = null;
        _this.anchorX = 0;
        _this.anchorY = 0;
        _this.anchorInside = false;
        _this.ingoreRepos = false;
        _this.bscene = bscene;
        _this.cm.comps.push(_this);
        _this.cm.compMap.set(key, _this);
        return _this;
    }
    BaseComp.prototype.doResize = function () {
        if (this.ingoreRepos === false) {
            this.bscene.getGrid().placeAt(this.posVo.x, this.posVo.y, this);
        }
        this.reAnchor();
    };
    BaseComp.prototype.setPos = function (xx, yy) {
        this.posVo = new __1.PosVo(xx, yy);
        if (this.ingoreRepos === false) {
            this.bscene.getGrid().placeAt(xx, yy, this);
        }
    };
    BaseComp.prototype.anchorTo = function (obj, anchorX, anchorY, inside) {
        this.anchorObj = obj;
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.anchorInside = inside;
        this.reAnchor();
    };
    BaseComp.prototype.reAnchor = function () {
        if (this.anchorObj === null) {
            return;
        }
        if (this.anchorX != -1) {
            if (this.anchorInside == false) {
                switch (this.anchorX) {
                    case 0:
                        this.x = this.anchorObj.x - this.anchorObj.displayWidth / 2 + this.displayWidth / 2;
                        break;
                    case 0.5:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2;
                        break;
                    case 1:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2 - this.displayWidth / 2;
                        break;
                }
            }
            else {
                switch (this.anchorX) {
                    case 0:
                        this.x = this.anchorObj.x - this.anchorObj.displayWidth / 2 + this.displayWidth * 2;
                        break;
                    case 0.5:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2;
                        break;
                    case 1:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2 - this.displayWidth / 1.5;
                        break;
                }
            }
        }
        if (this.anchorY != -1) {
            if (this.anchorInside == false) {
                switch (this.anchorY) {
                    case 0:
                        this.y = this.anchorObj.y - this.anchorObj.displayHeight / 2 + this.displayHeight / 2;
                        break;
                    case 0.5:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2;
                        break;
                    case 1:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2 - this.displayHeight / 2;
                        break;
                }
            }
            else {
                switch (this.anchorY) {
                    case 0:
                        this.y = this.anchorObj.y - this.anchorObj.displayHeight / 2 + this.displayHeight;
                        break;
                    case 0.5:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2;
                        break;
                    case 1:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2 - this.displayHeight / 1.5;
                        break;
                }
            }
        }
    };
    return BaseComp;
}(phaser_1.GameObjects.Container));
exports.BaseComp = BaseComp;
//# sourceMappingURL=BaseComp.js.map