"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.IconButton = void 0;
var ButtonController_1 = require("../ButtonController");
var BaseComp_1 = require("./BaseComp");
var CompBack_1 = require("./CompBack");
var IconButton = /** @class */ (function (_super) {
    __extends(IconButton, _super);
    function IconButton(bscene, key, iconKey, action, actionParam, buttonVo) {
        var _this = _super.call(this, bscene, key) || this;
        _this.buttonController = ButtonController_1.ButtonController.getInstance();
        _this.action = action;
        _this.actionParam = actionParam;
        _this.icon = _this.scene.add.image(0, 0, iconKey);
        _this.buttonVo = buttonVo;
        _this.backStyleVo = _this.cm.getBackStyle(buttonVo.backStyle);
        var hh = _this.bscene.getH() * buttonVo.vsize;
        var ww = hh;
        _this.hh = hh;
        _this.ww = ww;
        _this.back = new CompBack_1.CompBack(bscene, ww, hh, _this.backStyleVo);
        _this.add(_this.back);
        _this.icon.displayWidth = ww * 0.9;
        _this.icon.displayHeight = hh * 0.9;
        _this.add(_this.back);
        _this.add(_this.icon);
        _this.on("pointerdown", function () {
            _this.setBorder(_this.backStyleVo.borderPress);
            _this.buttonController.doAction(_this.action, _this.actionParam);
        });
        _this.on("pointerup", function () {
            _this.resetBorder();
        });
        _this.on("pointerover", function () {
            _this.setBorder(_this.backStyleVo.borderOver);
        });
        _this.on("pointerout", function () {
            _this.resetBorder();
        });
        _this.setSize(ww, hh);
        _this.scene.add.existing(_this);
        _this.setInteractive();
        return _this;
    }
    IconButton.prototype.doResize = function () {
        _super.prototype.doResize.call(this);
        var hh = this.bscene.getH() * this.buttonVo.vsize;
        var ww = hh;
        this.back.doResize(ww, hh);
    };
    IconButton.prototype.resetBorder = function () {
        this.back.lineStyle(2, this.backStyleVo.borderColor);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    };
    IconButton.prototype.setBorder = function (color) {
        this.back.lineStyle(2, color);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    };
    return IconButton;
}(BaseComp_1.BaseComp));
exports.IconButton = IconButton;
//# sourceMappingURL=IconButton.js.map