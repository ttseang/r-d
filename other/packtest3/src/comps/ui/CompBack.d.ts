import { GameObjects } from "phaser";
import { BackStyleVo, IBaseScene } from "../..";
export declare class CompBack extends GameObjects.Graphics {
    private backStyle;
    private ww;
    private hh;
    constructor(bscene: IBaseScene, ww: number, hh: number, backStyle: BackStyleVo);
    doResize(ww: number, hh: number): void;
}
