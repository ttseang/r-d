"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.UIWindow = void 0;
var __1 = require("../..");
var BaseComp_1 = require("./BaseComp");
var UIWindow = /** @class */ (function (_super) {
    __extends(UIWindow, _super);
    function UIWindow(bscene, key, ww, hh, backStyle) {
        var _this = _super.call(this, bscene, key) || this;
        _this.hh = 0;
        _this.ww = 0;
        _this.posVo = new __1.PosVo(0, 0);
        _this.bscene = bscene;
        var backStyleVo = _this.cm.getBackStyle(backStyle);
        _this.borderColor = backStyleVo.borderColor;
        _this.fillColor = backStyleVo.backColor;
        _this.graphics = _this.scene.add.graphics();
        _this.add(_this.graphics);
        /*
                this.box1=this.scene.add.image(0,0,"holder");
                this.box2=this.scene.add.image(0,0,"holder"); */
        _this.hh = hh;
        _this.ww = ww;
        /*  this.box1.setTint(borderColor);
         this.box2.setTint(fillColor);
 
         this.add(this.box1);
         this.add(this.box2); */
        _this.doResize();
        _this.scene.add.existing(_this);
        return _this;
    }
    UIWindow.prototype.doResize = function () {
        this.graphics.clear();
        this.graphics.fillStyle(this.fillColor, 1);
        this.graphics.lineStyle(8, this.borderColor, 1);
        console.log(this.borderColor);
        var ww2 = this.bscene.getW() * this.ww;
        var hh2 = this.bscene.getH() * this.hh;
        this.graphics.fillRect(-ww2 / 2, -hh2 / 2, ww2, hh2);
        this.graphics.strokeRect(-ww2 / 2, -hh2 / 2, ww2, hh2);
        this.graphics.stroke();
        /* this.box1.displayWidth=this.bscene.getW()*this.ww;
        this.box1.displayHeight=this.bscene.getH()*this.hh;

        this.box2.displayWidth=this.box1.displayWidth*0.98;
        this.box2.displayHeight=this.box1.displayHeight*0.95;

        this.setSize(this.box1.displayWidth,this.box1.displayHeight); */
        this.setSize(ww2, hh2);
        _super.prototype.doResize.call(this);
    };
    return UIWindow;
}(BaseComp_1.BaseComp));
exports.UIWindow = UIWindow;
//# sourceMappingURL=UIWindow.js.map