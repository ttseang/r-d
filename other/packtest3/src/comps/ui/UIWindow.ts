
import { GameObjects } from "phaser";
import { IComp, PosVo, IBaseScene, BackStyleVo } from "../..";

import { CompManager } from "../CompManager";
import { BaseComp } from "./BaseComp";

export class UIWindow extends BaseComp implements IComp
{
    private box1:GameObjects.Image;
    private box2:GameObjects.Image;

    private hh:number=0;
    private ww:number=0;
    
    private graphics:GameObjects.Graphics;

    public posVo:PosVo=new PosVo(0,0);
    private fillColor:number;
    private borderColor:number;

    constructor(bscene:IBaseScene,key:string,ww:number,hh:number,backStyle:string)
    {
        super(bscene,key);
        this.bscene=bscene;

        let backStyleVo:BackStyleVo=this.cm.getBackStyle(backStyle);

        this.borderColor=backStyleVo.borderColor;
        this.fillColor=backStyleVo.backColor;
       
        this.graphics=this.scene.add.graphics();

        this.add(this.graphics);
        
/* 
        this.box1=this.scene.add.image(0,0,"holder");
        this.box2=this.scene.add.image(0,0,"holder"); */
        
        this.hh=hh;
        this.ww=ww;        

       /*  this.box1.setTint(borderColor);
        this.box2.setTint(fillColor);

        this.add(this.box1);
        this.add(this.box2); */

        this.doResize();

        this.scene.add.existing(this);

    }
    
    doResize():void
    {
        this.graphics.clear();
        this.graphics.fillStyle(this.fillColor,1);
        this.graphics.lineStyle(8,this.borderColor,1);

        console.log(this.borderColor);

        let ww2:number=this.bscene.getW()*this.ww;
        let hh2:number=this.bscene.getH()*this.hh;

        this.graphics.fillRect(-ww2/2,-hh2/2,ww2,hh2);
        this.graphics.strokeRect(-ww2/2,-hh2/2,ww2,hh2);

        this.graphics.stroke();

        /* this.box1.displayWidth=this.bscene.getW()*this.ww;
        this.box1.displayHeight=this.bscene.getH()*this.hh;

        this.box2.displayWidth=this.box1.displayWidth*0.98;
        this.box2.displayHeight=this.box1.displayHeight*0.95;

        this.setSize(this.box1.displayWidth,this.box1.displayHeight); */

        this.setSize(ww2,hh2);

        super.doResize();
    }
}