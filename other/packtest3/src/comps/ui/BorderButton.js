"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BorderButton = void 0;
var __1 = require("../..");
var ButtonController_1 = require("../ButtonController");
var BaseComp_1 = require("./BaseComp");
var CompBack_1 = require("./CompBack");
var TextComp_1 = require("./TextComp");
var BorderButton = /** @class */ (function (_super) {
    __extends(BorderButton, _super);
    function BorderButton(bscene, key, text, action, actionParam, buttonVo) {
        var _this = _super.call(this, bscene, key) || this;
        _this.buttonController = ButtonController_1.ButtonController.getInstance();
        _this.posVo = new __1.PosVo(0, 0);
        _this.buttonVo = buttonVo;
        // let ww: number = this.bscene.getW() * buttonVo.hsize;
        var hh = _this.bscene.getH() * buttonVo.vsize;
        var ww = _this.bscene.getW() * buttonVo.hsize;
        _this.action = action;
        _this.actionParam = actionParam;
        _this.ww = ww;
        _this.hh = hh;
        _this.backStyleVo = _this.cm.getBackStyle(buttonVo.backStyle);
        _this.textStyleVo = _this.cm.getTextStyle(buttonVo.textStyle);
        _this.text1 = new TextComp_1.TextComp(_this.bscene, key, text, ww, buttonVo.textStyle);
        _this.text1.ingoreRepos = true;
        //  hh=this.text1.displayHeight*3;
        _this.back = new CompBack_1.CompBack(bscene, _this.ww, _this.hh, _this.backStyleVo);
        _this.add(_this.back);
        _this.add(_this.text1);
        _this.scene.add.existing(_this);
        _this.setSize(ww, hh);
        _this.setInteractive();
        _this.on("pointerdown", function () {
            _this.setBorder(_this.backStyleVo.borderPress);
            _this.buttonController.doAction(_this.action, _this.actionParam);
        });
        _this.on("pointerup", function () {
            _this.resetBorder();
        });
        _this.on("pointerover", function () {
            _this.setBorder(_this.backStyleVo.borderOver);
        });
        _this.on("pointerout", function () {
            _this.resetBorder();
        });
        // this.sizeText();
        window['btn'] = _this;
        return _this;
    }
    BorderButton.prototype.setText = function (text) {
        this.text1.setText(text);
    };
    BorderButton.prototype.doResize = function () {
        //  let ww: number = this.bscene.getW() * this.buttonVo.hsize;
        var hh = this.bscene.getH() * this.buttonVo.vsize;
        var ww = hh * 2;
        this.ww = ww;
        this.hh = hh;
        /* this.back.clear();


        this.back.fillStyle(this.backStyleVo.backColor, 1);
        this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.back.fillPath(); */
        this.back.doResize(this.ww, this.hh);
        this.setSize(ww, hh);
        _super.prototype.doResize.call(this);
        this.text1.sizeText();
        this.text1.x = 0;
        this.text1.y = 0;
    };
    BorderButton.prototype.resetBorder = function () {
        this.back.lineStyle(this.backStyleVo.borderThick, this.backStyleVo.borderColor);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    };
    BorderButton.prototype.setBorder = function (color) {
        this.back.lineStyle(this.backStyleVo.borderThick, color);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    };
    return BorderButton;
}(BaseComp_1.BaseComp));
exports.BorderButton = BorderButton;
//# sourceMappingURL=BorderButton.js.map