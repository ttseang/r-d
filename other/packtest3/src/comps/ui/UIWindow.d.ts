import { IComp, PosVo, IBaseScene } from "../..";
import { BaseComp } from "./BaseComp";
export declare class UIWindow extends BaseComp implements IComp {
    private box1;
    private box2;
    private hh;
    private ww;
    private graphics;
    posVo: PosVo;
    private fillColor;
    private borderColor;
    constructor(bscene: IBaseScene, key: string, ww: number, hh: number, backStyle: string);
    doResize(): void;
}
