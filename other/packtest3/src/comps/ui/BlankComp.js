"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlankComp = void 0;
var BaseComp_1 = require("./BaseComp");
var CompBack_1 = require("./CompBack");
var BlankComp = /** @class */ (function (_super) {
    __extends(BlankComp, _super);
    function BlankComp(bscene, key, obj, style) {
        var _this = _super.call(this, bscene, key) || this;
        var backStyleVo = _this.cm.getBackStyle(style);
        _this.back = new CompBack_1.CompBack(bscene, obj.displayWidth, obj.displayHeight, backStyleVo);
        _this.add(_this.back);
        _this.add(obj);
        _this.scene.add.existing(_this);
        return _this;
    }
    return BlankComp;
}(BaseComp_1.BaseComp));
exports.BlankComp = BlankComp;
//# sourceMappingURL=BlankComp.js.map