"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompLayout = void 0;
var CompManager_1 = require("./CompManager");
var BackgroundImage_1 = require("./ui/BackgroundImage");
var BorderButton_1 = require("./ui/BorderButton");
var CheckBox_1 = require("./ui/CheckBox");
var IconButton_1 = require("./ui/IconButton");
var IconTextButton_1 = require("./ui/IconTextButton");
var ResponseImage_1 = require("./ui/ResponseImage");
var TriangleButton_1 = require("./ui/TriangleButton");
var UISlider_1 = __importDefault(require("./ui/UISlider"));
var UIWindow_1 = require("./ui/UIWindow");
var CompLayout = /** @class */ (function () {
    function CompLayout(bscene) {
        this.cm = CompManager_1.CompManager.getInstance();
        this.allComps = [];
        this.bscene = bscene;
        this.scene = bscene.getScene();
    }
    CompLayout.prototype.clear = function () {
        if (this.background) {
            this.background.destroy();
        }
        var len = this.allComps.length;
        for (var i = 0; i < len; i++) {
            this.allComps[i].destroy();
        }
        this.allComps = [];
    };
    CompLayout.prototype.loadPage = function (pageName) {
        this.cm.loadPage(pageName);
        this.clear();
        this.build();
    };
    CompLayout.prototype.build = function () {
        this.background = new BackgroundImage_1.BackgroundImage(this.bscene, "background", this.cm.currentPage.backgroundType, this.cm.currentPage.backgroundParams);
        /*  if (this.cm.currentPage.backgroundType==="color")
         {
             this.background=this.scene.add.image(0,0,"holder").setOrigin(0,0);
             this.background.displayWidth=this.bscene.getW();
             this.background.displayHeight=this.bscene.getH();
 
             this.background.setTint(parseInt(this.cm.currentPage.backgroundParams));
         } */
        for (var i = 0; i < this.cm.compDefs.length; i++) {
            var def = this.cm.compDefs[i];
            var key = def.key;
            var icon = def.icon;
            var type = def.type;
            var backStyle = def.backstyle;
            var style = def.style;
            var ww = def.w;
            var hh = def.h;
            var xx = def.x;
            var yy = def.y;
            var text = def.text;
            var anchorVo = def.anchorVo;
            var angle = def.angle;
            var action = def.action;
            var actionParam = def.actionParam;
            var buttonVo = this.cm.getButtonStyle(style);
            switch (type) {
                case "window":
                    var uiWindow = new UIWindow_1.UIWindow(this.bscene, key, ww, hh, backStyle);
                    uiWindow.setPos(xx, yy);
                    this.allComps.push(uiWindow);
                    break;
                case "btn":
                    var button = new BorderButton_1.BorderButton(this.bscene, key, text, action, actionParam, buttonVo);
                    button.setPos(xx, yy);
                    if (anchorVo) {
                        if (this.cm.compMap.has(anchorVo.anchorTo)) {
                            var comp = this.cm.compMap.get(anchorVo.anchorTo);
                            button.anchorTo(comp, anchorVo.anchorX, anchorVo.anchorY, anchorVo.anchorInside);
                        }
                    }
                    this.allComps.push(button);
                    break;
                case "img":
                    var image = new ResponseImage_1.ResponseImage(this.bscene, key, hh, text);
                    image.setPos(xx, yy);
                    image.flipX = def.flipX;
                    image.flipY = def.flipY;
                    this.allComps.push(image);
                    break;
                case "slider":
                    var sliderStyle = this.cm.getBackStyle(backStyle);
                    var slider = new UISlider_1.default(this.bscene, key, hh, ww, sliderStyle.backColor, sliderStyle.borderColor);
                    slider.setPos(xx, yy);
                    this.allComps.push(slider);
                    break;
                case "checkbox":
                    var checkBox = new CheckBox_1.CheckBox(this.bscene, key, hh, backStyle);
                    checkBox.setPos(xx, yy);
                    this.allComps.push(checkBox);
                    break;
                case "tributton":
                    var triButton = new TriangleButton_1.TriangleButton(this.bscene, key, action, actionParam, buttonVo);
                    triButton.setPos(xx, yy);
                    triButton.setAngle(angle);
                    this.allComps.push(triButton);
                    break;
                case "iconbutton":
                    var btnIcon = new IconButton_1.IconButton(this.bscene, key, icon, action, actionParam, buttonVo);
                    btnIcon.setPos(xx, yy);
                    this.allComps.push(btnIcon);
                    break;
                case "icontextbutton":
                    var btnIconText = new IconTextButton_1.IconTextButton(this.bscene, key, text, icon, action, actionParam, buttonVo);
                    btnIconText.setPos(xx, yy);
                    this.allComps.push(btnIconText);
                    break;
            }
        }
    };
    return CompLayout;
}());
exports.CompLayout = CompLayout;
//# sourceMappingURL=CompLayout.js.map