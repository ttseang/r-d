import { BackStyleVo } from "../dataObjs/BackStyleVo";
import { ButtonStyleVo } from "../dataObjs/ButtonStyleVo";
import { CompVo } from "../dataObjs/CompVo";
import { PageVo } from "../dataObjs/PageVo";
import { TextStyleVo } from "../dataObjs/TextStyleVo";
import { IComp } from "../interfaces/IComp";
export declare class CompManager {
    private static instance;
    comps: IComp[];
    compDefs: CompVo[];
    pageDefs: PageVo[];
    currentPage: PageVo | null;
    private defTextStyle;
    compMap: Map<string, IComp>;
    backstyles: Map<string, BackStyleVo>;
    buttonStyles: Map<string, ButtonStyleVo>;
    textStyles: Map<string, TextStyleVo>;
    startPage: string;
    constructor();
    static getInstance(): CompManager;
    loadPage(pageName: string): void;
    regTextStyle(key: string, textStyle: TextStyleVo): void;
    getTextStyle(key: string): TextStyleVo;
    regBackStyle(key: string, backstyle: BackStyleVo): void;
    regButtonStyle(key: string, buttonStyle: ButtonStyleVo): void;
    getBackStyle(key: string): BackStyleVo;
    getButtonStyle(key: string): ButtonStyleVo;
    getBasicTextStyle(key: string): {
        fontFamily: string;
        color: string;
    };
    doResize(): void;
}
export default CompManager;
