import IBaseScene from "../interfaces/IBaseScene";
export declare class CompLayout {
    private bscene;
    private scene;
    private cm;
    private allComps;
    private background;
    constructor(bscene: IBaseScene);
    clear(): void;
    loadPage(pageName: string): void;
    build(): void;
}
