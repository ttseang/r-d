import { AssetManager } from "./AssetManager";
import { SkeletonRenderer } from "./SkeletonRenderer";

export class Main
{
    
    private assetManager!:AssetManager;
    private skeletonRenderer!:SkeletonRenderer;
    private canvas:HTMLCanvasElement | null;

    private skelName:string = "spineboy-ess";
	private animName:string = "walk";

    constructor()
    {
        this.canvas = document.getElementById("canvas") as HTMLCanvasElement;

        if (this.canvas)
        {

        
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
		let context:CanvasRenderingContext2D | null = this.canvas.getContext("2d");
        
        if (context)
        {
        this.skeletonRenderer=new SkeletonRenderer(context);

        this.assetManager=new AssetManager("./assets/");
        this.assetManager.loadText(this.skelName + ".json");
		this.assetManager.loadText(this.skelName.replace("-pro", "").replace("-ess", "") + ".atlas");
		this.assetManager.loadTexture(this.skelName.replace("-pro", "").replace("-ess", "") + ".png");

       //requestAnimationFrame(load);
        }

        }
        
    }
    private load() {
		if (this.assetManager.isLoadingComplete()) {
			var data = this.loadSkeleton(skelName, animName, "default");
			let skeleton = data.skeleton;
			let state = data.state;
			let bounds = data.bounds;
			//requestAnimationFrame(render);
		} else {
			//requestAnimationFrame(load);
		}
	}
    private loadSkeleton(name, initialAnimation, skin) {
		if (skin === undefined) skin = "default";

		// Load the texture atlas using name.atlas and name.png from the AssetManager.
		// The function passed to TextureAtlas is used to resolve relative paths.
		atlas = new spine.TextureAtlas(assetManager.require(name.replace("-pro", "").replace("-ess", "") + ".atlas"));
		atlas.setTextures(assetManager);

		// Create a AtlasAttachmentLoader, which is specific to the WebGL backend.
		atlasLoader = new spine.AtlasAttachmentLoader(atlas);

		// Create a SkeletonJson instance for parsing the .json file.
		var skeletonJson = new spine.SkeletonJson(atlasLoader);

		// Set the scale to apply during parsing, parse the file, and create a new skeleton.
		var skeletonData = skeletonJson.readSkeletonData(assetManager.require(name + ".json"));
		var skeleton = new spine.Skeleton(skeletonData);
		skeleton.scaleY = -1;
		var bounds = calculateBounds(skeleton);
		skeleton.setSkinByName(skin);

		// Create an AnimationState, and set the initial animation in looping mode.
		var animationState = new spine.AnimationState(new spine.AnimationStateData(skeleton.data));
		animationState.setAnimation(0, initialAnimation, true);
		animationState.addListener({
			event: function (trackIndex, event) {
				// console.log("Event on track " + trackIndex + ": " + JSON.stringify(event));
			},
			complete: function (trackIndex, loopCount) {
				// console.log("Animation on track " + trackIndex + " completed, loop count: " + loopCount);
			},
			start: function (trackIndex) {
				// console.log("Animation on track " + trackIndex + " started");
			},
			end: function (trackIndex) {
				// console.log("Animation on track " + trackIndex + " ended");
			}
		})

		// Pack everything up and return to caller.
		return { skeleton: skeleton, state: animationState, bounds: bounds };
	}

	private calculateBounds(skeleton) {
		var data = skeleton.data;
		skeleton.setToSetupPose();
		skeleton.updateWorldTransform();
		var offset = new spine.Vector2();
		var size = new spine.Vector2();
		skeleton.getBounds(offset, size, []);
		return { offset: offset, size: size };
	}

	private render() {
		let now = Date.now() / 1000;
		let delta = now - lastFrameTime;
		lastFrameTime = now;

		resize();

		this.context.save();
		this.context.setTransform(1, 0, 0, 1, 0, 0);
		this.context.fillStyle = "#cccccc";
		this.context.fillRect(0, 0, canvas.width, canvas.height);
		this.context.restore();

		state.update(delta);
		state.apply(skeleton);
		skeleton.updateWorldTransform();
		skeletonRenderer.draw(skeleton);

		this.context.strokeStyle = "green";
		this.context.beginPath();
		this.context.moveTo(-1000, 0);
		this.context.lineTo(1000, 0);
		this.context.moveTo(0, -1000);
		this.context.lineTo(0, 1000);
		this.context.stroke();

		//requestAnimationFrame(render);
	}

}