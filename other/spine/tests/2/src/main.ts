import { AnimationState, AnimationStateData, SkeletonData, SpinePlayer, SpinePlayerConfig } from "@esotericsoftware/spine-player";

export class Main {
    private player: SpinePlayer;
    constructor() {
        (window as any).scene = this;
        this.player = new SpinePlayer('p1', this.getConfig());
    }
    onSuccess() {
        this.player.play();
        this.player.animationState.setAnimation(0,"walk",true);
        this.player.animationState.setAnimation(1,"repeating/blink",true);        
    }
    getConfig() {
        //let config:SpinePlayerConfig={};
        let config: any = {};
        config.atlasUrl = "./assets/Penny Loafer.atlas";
        config.jsonUrl = "./assets/Penny Loafer.json";
        config.showControls = true;
        config.premultipliedAlpha = true;
        //config.backgroundColor="#000000";
        config.alpha = true;
        //config.showLoading=false;
        //  config.animation="walk";
        config.success = this.onSuccess.bind(this);
        return config;
    }
}