import { BaseScreen, SvgObj } from "svggame"
import { Box } from "../classes/Box";

export class ScreenMain extends BaseScreen
{
    private box1!:Box;
    private isDown:boolean=false;
    private currentBox:Box | null=null;

    constructor()
    {
        super("ScreenMain");
        console.log("Screen Main");
        (window as any).scene=this;
    }

    create()
    {
        this.box1=new Box(this);
        this.box1.setListeners(this.dragStart.bind(this))
        this.center(this.box1);

        
        document.onpointermove=this.onPointerMove.bind(this);
        document.onpointerup=this.dragStop.bind(this);
    }

    dragStart(box:Box)
    {
        this.currentBox=box;
    }
    dragStop()
    {
        this.currentBox=null;
    }
    onPointerMove(e:PointerEvent)
    {
        console.log("move");
        e.preventDefault();
        if (this.currentBox)
        {
            this.currentBox.x=e.clientX-this.currentBox.displayWidth/2;
            this.currentBox.y=e.clientY-this.currentBox.displayHeight/2;
        }
    }
}