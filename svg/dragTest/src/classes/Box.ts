import { IScreen, SvgObj } from "svggame";

export class Box extends SvgObj
{
    constructor(iScreen:IScreen)
    {
        super(iScreen,"box");
    }

    setListeners(onDown:Function)
    {
        if (this.el)
        {
            this.el.onpointerdown=()=>{
                onDown(this);
            }
        }
    }
}