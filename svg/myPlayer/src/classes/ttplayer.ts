/******************************************************************************
 * Spine Runtimes License Agreement
 * Last updated January 1, 2020. Replaces all prior versions.
 *
 * Copyright (c) 2013-2020, Esoteric Software LLC
 *
 * Integration of the Spine Runtimes into software or otherwise creating
 * derivative works of the Spine Runtimes is permitted under the terms and
 * conditions of Section 2 of the Spine Editor License Agreement:
 * http://esotericsoftware.com/spine-editor-license
 *
 * Otherwise, it is permitted to integrate the Spine Runtimes into software
 * or otherwise create derivative works of the Spine Runtimes (collectively,
 * "Products"), provided that each user of the Products must obtain their own
 * Spine Editor license and redistribution of the Products in any form must
 * include this license and copyright notice.
 *
 * THE SPINE RUNTIMES ARE PROVIDED BY ESOTERIC SOFTWARE LLC "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL ESOTERIC SOFTWARE LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES,
 * BUSINESS INTERRUPTION, OR LOSS OF USE, DATA, OR PROFITS) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THE SPINE RUNTIMES, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

import { Animation, AnimationState, AnimationStateData, AtlasAttachmentLoader, Bone, Color, Downloader, Event, MathUtils, MixBlend, MixDirection, Skeleton, SkeletonBinary, SkeletonData, SkeletonJson, StringMap, TextureAtlas, TextureFilter, TimeKeeper, TrackEntry, Vector2 } from "@esotericsoftware/spine-core"
import { AssetManager, GLTexture, LoadingScreen, ManagedWebGLRenderingContext, ResizeMode, SceneRenderer } from "@esotericsoftware/spine-webgl"

export interface SpinePlayerConfig {
    /* The URL of the skeleton JSON file (.json). */
    jsonUrl: string

    /* Optional: The name of a field in the JSON that holds the skeleton data. Default: none */
    jsonField: string

    /* The URL of the skeleton binary file (.skel). */
    binaryUrl: string

    /* The URL of the skeleton atlas file (.atlas). Atlas page images are automatically resolved. */
    atlasUrl: string

    /* Raw data URIs, mapping a path to base64 encoded raw data. When player's asset manager resolves the jsonUrl, binaryUrl,
       atlasUrl, or the image paths referenced in the atlas, it will first look for that path in the raw data URIs. This
       allows embedding assets directly in HTML/JS. Default: none */
    rawDataURIs: StringMap<string>

    /* Optional: The name of the animation to be played. Default: empty animation */
    animation: string

    /* Optional: List of animation names from which the user can choose. Default: all animations */
    animations: string[]

    /* Optional: The default mix time used to switch between two animations. Default: 0.25 */
    defaultMix: number

    /* Optional: The name of the skin to be set. Default: the default skin */
    skin: string

    /* Optional: List of skin names from which the user can choose. Default: all skins */
    skins: string[]

    /* Optional: Whether the skeleton's atlas images use premultiplied alpha. Default: true */
    premultipliedAlpha: boolean

    /* Optional: Whether to show the player controls. When false, no external CSS file is needed. Default: true */
    showControls: boolean

    /* Optional: Whether to show the loading animation. Default: true */
    showLoading: boolean

    /* Optional: Which debugging visualizations are shown. Default: none */
    debug: {
        bones: boolean
        regions: boolean
        meshes: boolean
        bounds: boolean
        paths: boolean
        clipping: boolean
        points: boolean
        hulls: boolean
    }

    /* Optional: The position and size of the viewport in the skeleton's world coordinates. Default: the bounding box that fits
      the current animation, 10% padding, 0.25 transition time */
    viewport: {
        /* Optional: The position and size of the viewport in the skeleton's world coordinates. Default: the bounding box that
           fits the current animation */
        x: number
        y: number
        width: number
        height: number

        /* Optional: Padding around the viewport size, given as a number or percentage (eg "25%"). Default: 10% */
        padLeft: string | number
        padRight: string | number
        padTop: string | number
        padBottom: string | number

        /* Optional: Whether to draw lines showing the viewport bounds. Default: false */
        debugRender: boolean,

        /* Optional: When the current viewport changes, the time to animate to the new viewport. Default: 0.25 */
        transitionTime: number

        /* Optional: Viewports for specific animations. Default: none */
        animations: StringMap<Viewport>
    }

    /* Optional: Whether the canvas is transparent, allowing the web page behind the canvas to show through when
       backgroundColor alpha is < ff. Default: false */
    alpha: boolean

    /* Optional: The canvas background color, given in the format #rrggbb or #rrggbbaa. Default: #000000ff (black) or when
       alpha is true #00000000 (transparent) */
    backgroundColor: string

    /* Optional: The background color used in fullscreen mode, given in the format #rrggbb or #rrggbbaa. Default: backgroundColor */
    fullScreenBackgroundColor: string

    /* Optional: An image to draw behind the skeleton. Default: none */
    backgroundImage: {
        url: string

        /* Optional: The position and size of the background image in the skeleton's world coordinates. Default: fills the viewport */
        x: number
        y: number
        width: number
        height: number
    }

    /* Optional: Whether mipmapping and anisotropic filtering are used for highest quality scaling when available, otherwise the
       filter settings from the texture atlas are used. Default: true */
    mipmaps: true

    /* Optional: List of bone names that the user can drag to position. Default: none */
    controlBones: string[]

    /* Optional: Callback when the skeleton and its assets have been successfully loaded. If an animation is set on track 0,
       the player won't set its own animation. Default: none */
    success: (player: SpinePlayer) => void

    /* Optional: Callback when the skeleton could not be loaded or rendered. Default: none */
    error: (player: SpinePlayer, msg: string) => void

    /* Optional: Callback at the start of each frame, before the skeleton is posed or drawn. Default: none */
    frame: (player: SpinePlayer, delta: number) => void

    /* Optional: Callback after the skeleton is posed each frame, before it is drawn. Default: none */
    update: (player: SpinePlayer, delta: number) => void

    /* Optional: Callback after the skeleton is drawn each frame. Default: none */
    draw: (player: SpinePlayer, delta: number) => void

    /* Optional: Callback each frame before the skeleton is loaded. Default: none */
    loading: (player: SpinePlayer, delta: number) => void

    /* Optional: The downloader used by the player's asset manager. Passing the same downloader to multiple players using the
       same assets ensures the assets are only downloaded once. Default: new instance */
    downloader: Downloader
}

export interface Viewport {
    /* Optional: The position and size of the viewport in the skeleton's world coordinates. Default: the bounding box that fits
       the current animation */
    x: number,
    y: number,
    width: number,
    height: number,

    /* Optional: Padding around the viewport size, given as a number or percentage (eg "25%"). Default: 10% */
    padLeft: string | number
    padRight: string | number
    padTop: string | number
    padBottom: string | number
}

export class SpinePlayer {
    public parent: HTMLElement | null;
    public dom: HTMLElement;
    public canvas!: HTMLCanvasElement;
    public context!: ManagedWebGLRenderingContext;
    public sceneRenderer!: SceneRenderer;
    public loadingScreen!: LoadingScreen;
    public assetManager!: AssetManager;
    public bg = new Color();
    public bgFullscreen = new Color();

    // private playerControls!: HTMLElement;
    //private timelineSlider!: Slider;
    private skinButton!: HTMLElement;
    private animationButton!: HTMLElement;

    private playTime = 0;
    private selectedBones!: Bone[];
    //public popup: Popup |null=null;

    /* True if the player is unable to load or render the skeleton. */
    /* True if the player is unable to load or render the skeleton. */

    public error!: boolean;
    /* The player's skeleton. Null until loading is complete (access after config.success). */
    /* The player's skeleton. Null until loading is complete (access after config.success). */
    public skeleton!: Skeleton;
    /* The animation state controlling the skeleton. Null until loading is complete (access after config.success). */
    /* The animation state controlling the skeleton. Null until loading is complete (access after config.success). */
    public animationState!: AnimationState;

    public paused = true;
    public speed = 1;
    public time = new TimeKeeper();
    private stopRequestAnimationFrame = false;

    private viewport: Viewport = {} as Viewport;
    private currentViewport!: Viewport;
    private previousViewport!: Viewport;
    private viewportTransitionStart = 0;

    constructor(parent: HTMLElement | string, private config: SpinePlayerConfig) {
        this.parent = typeof parent === "string" ? document.getElementById(parent) : parent;
        if (!this.parent) throw new Error("SpinePlayer parent not found: " + parent);



        this.parent.appendChild(this.dom = createElement(
                 /*html*/`<div class="spine-player" style="position:relative;height:100%"><canvas class="spine-player-canvas" style="display:block;width:100%;height:100%"></canvas></div>`));

        try {
            this.validateConfig(config);
        } catch (e: any) {
            this.showError(e.message, e);
        }

        this.initialize();

        // Register a global resize handler to redraw, avoiding flicker.
        window.addEventListener("resize", () => this.drawFrame(false));

        // Start the rendering loop.
        requestAnimationFrame(() => this.drawFrame());
    }

    private validateConfig(config: SpinePlayerConfig) {
        if (!config) throw new Error("A configuration object must be passed to to new SpinePlayer().");
        if ((config as any).skelUrl) config.binaryUrl = (config as any).skelUrl;
        if (!config.jsonUrl && !config.binaryUrl) throw new Error("A URL must be specified for the skeleton JSON or binary file.");
        if (!config.atlasUrl) throw new Error("A URL must be specified for the atlas file.");
        if (!config.backgroundColor) config.backgroundColor = config.alpha ? "00000000" : "000000";
        if (!config.fullScreenBackgroundColor) config.fullScreenBackgroundColor = config.backgroundColor;
        // if (config.backgroundImage && !config.backgroundImage.url) config.backgroundImage = null;
        if (config.premultipliedAlpha === void 0) config.premultipliedAlpha = true;
        if (config.mipmaps === void 0) config.mipmaps = true;
        if (!config.debug) config.debug = {} as any;
        if (config.animations && config.animation && config.animations.indexOf(config.animation) < 0)
            throw new Error("Animation '" + config.animation + "' is not in the config animation list: " + toString(config.animations));
        if (config.skins && config.skin && config.skins.indexOf(config.skin) < 0)
            throw new Error("Default skin '" + config.skin + "' is not in the config skins list: " + toString(config.skins));
        if (!config.viewport) config.viewport = {} as any;
        if (!config.viewport.animations) config.viewport.animations = {};
        if (config.viewport.debugRender === void 0) config.viewport.debugRender = false;
        if (config.viewport.transitionTime === void 0) config.viewport.transitionTime = 0.25;
        if (!config.controlBones) config.controlBones = [];
        if (config.showLoading === void 0) config.showLoading = true;
        if (config.defaultMix === void 0) config.defaultMix = 0.25;
    }

    private initialize(): HTMLElement {
        let config = this.config;
        let dom = this.dom;

        if (!config.alpha) { // Prevents a flash before the first frame is drawn.
            let hex = config.backgroundColor;
            this.dom.style.backgroundColor = (hex.charAt(0) == '#' ? hex : "#" + hex).substr(0, 7);
        }

        try {
            // Setup the OpenGL context.
            this.canvas = findWithClass(dom, "spine-player-canvas") as HTMLCanvasElement;
            this.context = new ManagedWebGLRenderingContext(this.canvas, { alpha: config.alpha });

            // Setup the scene renderer and loading screen.
            this.sceneRenderer = new SceneRenderer(this.canvas, this.context, true);
            if (config.showLoading) this.loadingScreen = new LoadingScreen(this.sceneRenderer);
        } catch (e: any) {
            this.showError("Sorry, your browser does not support \nPlease use the latest version of Firefox, Chrome, Edge, or Safari.", e);
        }

        // Load the assets.
        this.assetManager = new AssetManager(this.context, "", config.downloader);
        if (config.rawDataURIs) {
            for (let path in config.rawDataURIs)
                this.assetManager.setRawDataURI(path, config.rawDataURIs[path]);
        }
        if (config.jsonUrl)
            this.assetManager.loadJson(config.jsonUrl);
       /*  else
            this.assetManager.loadBinary(config.binaryUrl); */
        this.assetManager.loadTextureAtlas(config.atlasUrl);
      
        return dom;
    }

    private loadSkeleton() {
        
        console.log("loadSkeleton");
        
        if (this.error) return;

        if (this.assetManager.hasErrors())
            this.showError("Error: Assets could not be loaded.\n" + toString(this.assetManager.getErrors()));

        let config = this.config;

        // Configure filtering, don't use mipmaps in WebGL1 if the atlas page is non-POT
        let atlas = this.assetManager.require(config.atlasUrl) as TextureAtlas;
        let gl = this.context.gl, anisotropic = gl.getExtension("EXT_texture_filter_anisotropic");
        let isWebGL1 = gl.getParameter(gl.VERSION).indexOf("WebGL 1.0") != -1;
        console.log("webGL="+isWebGL1);
        for (let page of atlas.pages) {
            let minFilter = page.minFilter;
            var useMipMaps: boolean = config.mipmaps;
            var isPOT = MathUtils.isPowerOfTwo(page.width) && MathUtils.isPowerOfTwo(page.height);
            if (isWebGL1 && !isPOT) useMipMaps = false;

            console.log(useMipMaps);
            
            if (useMipMaps) {
                if (anisotropic) {
                    gl.texParameterf(gl.TEXTURE_2D, anisotropic.TEXTURE_MAX_ANISOTROPY_EXT, 8);
                    minFilter = TextureFilter.MipMapLinearLinear;
                } else
                    minFilter = TextureFilter.Linear; // Don't use mipmaps without anisotropic.
                page.texture.setFilters(minFilter, TextureFilter.Nearest);
            }
            if (minFilter != TextureFilter.Nearest && minFilter != TextureFilter.Linear) (page.texture as GLTexture).update(true);
        }

        // Load skeleton data.
        let skeletonData: SkeletonData | null = null;

        if (config.jsonUrl) {
            try {
                let jsonData = this.assetManager.remove(config.jsonUrl);
                if (!jsonData) throw new Error("Empty JSON data.");
                if (config.jsonField) {
                    jsonData = jsonData[config.jsonField];
                    if (!jsonData) throw new Error("JSON field does not exist: " + config.jsonField);
                }
                let json = new SkeletonJson(new AtlasAttachmentLoader(atlas));
                skeletonData = json.readSkeletonData(jsonData);
            } catch (e: any) {
                this.showError(`Error: Could not load skeleton JSON.\n${e.message}`, e);
            }
        } else {
            let binaryData = this.assetManager.remove(config.binaryUrl);
            let binary = new SkeletonBinary(new AtlasAttachmentLoader(atlas));
            try {
                skeletonData = binary.readSkeletonData(binaryData);
            } catch (e: any) {
                this.showError(`Error: Could not load skeleton binary.\n${e.message}`, e);
            }
        }
        if (skeletonData) {
            this.skeleton = new Skeleton(skeletonData);
            let stateData = new AnimationStateData(skeletonData);
            stateData.defaultMix = config.defaultMix;
            this.animationState = new AnimationState(stateData);

            // Check if all control bones are in the skeleton
            config.controlBones.forEach(bone => {
                if (!skeletonData!.findBone(bone)) this.showError(`Error: Control bone does not exist in skeleton: ${bone}`);
            })

            // Setup skin.
            if (!config.skin && skeletonData.skins.length) config.skin = skeletonData.skins[0].name;
            if (config.skins && config.skin.length) {
                config.skins.forEach(skin => {
                    if (!this.skeleton.data.findSkin(skin))
                        this.showError(`Error: Skin in config list does not exist in skeleton: ${skin}`);
                });
            }
            if (config.skin) {
                if (!this.skeleton.data.findSkin(config.skin))
                    this.showError(`Error: Skin does not exist in skeleton: ${config.skin}`);
                this.skeleton.setSkinByName(config.skin);
                this.skeleton.setSlotsToSetupPose();
            }

            // Check if all animations given a viewport exist.
            Object.getOwnPropertyNames(config.viewport.animations).forEach((animation: string) => {
                if (!skeletonData!.findAnimation(animation))
                    this.showError(`Error: Animation for which a viewport was specified does not exist in skeleton: ${animation}`);
            });

            // Setup the animations after the viewport, so default bounds don't get messed up.
            if (config.animations && config.animations.length) {
                config.animations.forEach(animation => {
                    if (!this.skeleton.data.findAnimation(animation))
                        this.showError(`Error: Animation in config list does not exist in skeleton: ${animation}`);
                });
                if (!config.animation) config.animation = config.animations[0];
            }

            if (config.animation && !skeletonData.findAnimation(config.animation))
                this.showError(`Error: Animation does not exist in skeleton: ${config.animation}`);





            if (config.success) config.success(this);

            let entry = this.animationState.getCurrent(0);
            if (!entry) {
                if (config.animation) {
                    entry = this.setAnimation(config.animation);
                    this.play();
                } else {
                    entry = this.animationState.setEmptyAnimation(0);
                    entry.trackEnd = 100000000;
                    this.setViewport(entry.animation);
                    this.pause();
                }
            } else if (!this.currentViewport) {
                this.setViewport(entry.animation);
                this.play();
            }
        }
    }



    play() {
        this.paused = false;

    }

    pause() {
        this.paused = true;

    }

    /* Sets a new animation and viewport on track 0. */
    setAnimation(animation: string | Animation, loop: boolean = true): TrackEntry {
        animation = this.setViewport(animation);
        return this.animationState.setAnimationWith(0, animation, loop);
    }

    /* Adds a new animation and viewport on track 0. */
    addAnimation(animation: string | Animation, loop: boolean = true, delay: number = 0): TrackEntry {
        animation = this.setViewport(animation);
        return this.animationState.addAnimationWith(0, animation, loop, delay);
    }

    /* Sets the viewport for the specified animation. */
    setViewport(animation: string | Animation): Animation {
        if (typeof animation == "string") animation = this.skeleton.data.findAnimation(animation);

        this.previousViewport = this.currentViewport;

        // Determine the base viewport.
        let globalViewport = this.config.viewport;
        let viewport = this.currentViewport = {
            padLeft: globalViewport.padLeft !== void 0 ? globalViewport.padLeft : "10%",
            padRight: globalViewport.padRight !== void 0 ? globalViewport.padRight : "10%",
            padTop: globalViewport.padTop !== void 0 ? globalViewport.padTop : "10%",
            padBottom: globalViewport.padBottom !== void 0 ? globalViewport.padBottom : "10%"
        } as Viewport;
        if (globalViewport.x !== void 0 && globalViewport.y !== void 0 && globalViewport.width && globalViewport.height) {
            viewport.x = globalViewport.x;
            viewport.y = globalViewport.y;
            viewport.width = globalViewport.width;
            viewport.height = globalViewport.height;
        } else
            this.calculateAnimationViewport(animation, viewport);

        // Override with the animation specific viewport for the final result.
        let userAnimViewport = this.config.viewport.animations[animation.name];
        if (userAnimViewport) {
            if (userAnimViewport.x !== void 0 && userAnimViewport.y !== void 0 && userAnimViewport.width && userAnimViewport.height) {
                viewport.x = userAnimViewport.x;
                viewport.y = userAnimViewport.y;
                viewport.width = userAnimViewport.width;
                viewport.height = userAnimViewport.height;
            }
            if (userAnimViewport.padLeft !== void 0) viewport.padLeft = userAnimViewport.padLeft;
            if (userAnimViewport.padRight !== void 0) viewport.padRight = userAnimViewport.padRight;
            if (userAnimViewport.padTop !== void 0) viewport.padTop = userAnimViewport.padTop;
            if (userAnimViewport.padBottom !== void 0) viewport.padBottom = userAnimViewport.padBottom;
        }

        // Translate percentage padding to world units.
        viewport.padLeft = this.percentageToWorldUnit(viewport.width, viewport.padLeft);
        viewport.padRight = this.percentageToWorldUnit(viewport.width, viewport.padRight);
        viewport.padBottom = this.percentageToWorldUnit(viewport.height, viewport.padBottom);
        viewport.padTop = this.percentageToWorldUnit(viewport.height, viewport.padTop);

        this.viewportTransitionStart = performance.now();
        return animation;
    }

    private percentageToWorldUnit(size: number, percentageOrAbsolute: string | number): number {
        if (typeof percentageOrAbsolute === "string")
            return size * parseFloat(percentageOrAbsolute.substr(0, percentageOrAbsolute.length - 1)) / 100;
        return percentageOrAbsolute;
    }

    private calculateAnimationViewport(animation: Animation, viewport: Viewport) {
        this.skeleton.setToSetupPose();

        let steps = 100, stepTime = animation.duration ? animation.duration / steps : 0, time = 0;
        let minX = 100000000, maxX = -100000000, minY = 100000000, maxY = -100000000;
        let offset = new Vector2(), size = new Vector2();

        for (let i = 0; i < steps; i++, time += stepTime) {

            let events: Event[] = [];

            animation.apply(this.skeleton, time, time, false, events, 1, MixBlend.setup, MixDirection.mixIn);
            this.skeleton.updateWorldTransform();
            this.skeleton.getBounds(offset, size);

            if (!isNaN(offset.x) && !isNaN(offset.y) && !isNaN(size.x) && !isNaN(size.y)) {
                minX = Math.min(offset.x, minX);
                maxX = Math.max(offset.x + size.x, maxX);
                minY = Math.min(offset.y, minY);
                maxY = Math.max(offset.y + size.y, maxY);
            } else
                this.showError("Animation bounds are invalid: " + animation.name);
        }

        viewport.x = minX;
        viewport.y = minY;
        viewport.width = maxX - minX;
        viewport.height = maxY - minY;
    }

    private drawFrame(requestNextFrame = true) {
        try {
            if (this.error) return;
            if (requestNextFrame && !this.stopRequestAnimationFrame) requestAnimationFrame(() => this.drawFrame());

            let doc = document as any;
            let isFullscreen = doc.fullscreenElement || doc.webkitFullscreenElement || doc.mozFullScreenElement || doc.msFullscreenElement;
            let bg = isFullscreen ? this.bgFullscreen : this.bg;

            this.time.update();
            let delta = this.time.delta;

            // Load the skeleton if the assets are ready.
            let loading = this.assetManager.isLoadingComplete();
            if (!this.skeleton && loading) this.loadSkeleton();
            let skeleton = this.skeleton;
            let config = this.config;
            if (skeleton) {
                // Resize the canvas.
                let renderer = this.sceneRenderer;
                renderer.resize(ResizeMode.Expand);

                let playDelta = this.paused ? 0 : delta * this.speed;
                if (config.frame) config.frame(this, playDelta);

                // Update animation time and pose the skeleton.
                if (!this.paused) {
                    this.animationState.update(playDelta);
                    this.animationState.apply(skeleton);
                    skeleton.updateWorldTransform();

                    if (config.showControls) {
                        this.playTime += playDelta;
                        let entry = this.animationState.getCurrent(0);
                        if (entry) {
                            let duration = entry.animation.duration;
                            while (this.playTime >= duration && duration != 0)
                                this.playTime -= duration;
                            this.playTime = Math.max(0, Math.min(this.playTime, duration));
                            //  this.timelineSlider.setValue(this.playTime / duration);
                        }
                    }
                }

                // Determine the viewport.
                let viewport = this.viewport;
                viewport.x = this.currentViewport.x - (this.currentViewport.padLeft as number),
                    viewport.y = this.currentViewport.y - (this.currentViewport.padBottom as number),
                    viewport.width = this.currentViewport.width + (this.currentViewport.padLeft as number) + (this.currentViewport.padRight as number),
                    viewport.height = this.currentViewport.height + (this.currentViewport.padBottom as number) + (this.currentViewport.padTop as number)

                if (this.previousViewport) {
                    let transitionAlpha = (performance.now() - this.viewportTransitionStart) / 1000 / config.viewport.transitionTime;
                    if (transitionAlpha < 1) {
                        let x = this.previousViewport.x - (this.previousViewport.padLeft as number);
                        let y = this.previousViewport.y - (this.previousViewport.padBottom as number);
                        let width = this.previousViewport.width + (this.previousViewport.padLeft as number) + (this.previousViewport.padRight as number);
                        let height = this.previousViewport.height + (this.previousViewport.padBottom as number) + (this.previousViewport.padTop as number);
                        viewport.x = x + (viewport.x - x) * transitionAlpha;
                        viewport.y = y + (viewport.y - y) * transitionAlpha;
                        viewport.width = width + (viewport.width - width) * transitionAlpha;
                        viewport.height = height + (viewport.height - height) * transitionAlpha;
                    }
                }

                renderer.camera.zoom = this.canvas.height / this.canvas.width > viewport.height / viewport.width
                    ? viewport.width / this.canvas.width : viewport.height / this.canvas.height;
                renderer.camera.position.x = viewport.x + viewport.width / 2;
                renderer.camera.position.y = viewport.y + viewport.height / 2;

                // Clear the screen.
                let gl = this.context.gl;
                gl.clearColor(bg.r, bg.g, bg.b, bg.a);
                gl.clear(gl.COLOR_BUFFER_BIT);

                if (config.update) config.update(this, playDelta);

                renderer.begin();

                // Draw the background image.
                let bgImage = config.backgroundImage;
                if (bgImage) {
                    let texture = this.assetManager.require(bgImage.url);
                    if (bgImage.x !== void 0 && bgImage.y !== void 0 && bgImage.width && bgImage.height)
                        renderer.drawTexture(texture, bgImage.x, bgImage.y, bgImage.width, bgImage.height);
                    else
                        renderer.drawTexture(texture, viewport.x, viewport.y, viewport.width, viewport.height);
                }

                // Draw the skeleton and debug output.
                renderer.drawSkeleton(skeleton, config.premultipliedAlpha);
                if ((renderer.skeletonDebugRenderer.drawBones = config.debug.bones)
                    || (renderer.skeletonDebugRenderer.drawBoundingBoxes = config.debug.bounds)
                    || (renderer.skeletonDebugRenderer.drawClipping = config.debug.clipping)
                    || (renderer.skeletonDebugRenderer.drawMeshHull = config.debug.hulls)
                    || (renderer.skeletonDebugRenderer.drawPaths = config.debug.paths)
                    || (renderer.skeletonDebugRenderer.drawRegionAttachments = config.debug.regions)
                    || (renderer.skeletonDebugRenderer.drawMeshTriangles = config.debug.meshes)
                ) {
                    renderer.drawSkeletonDebug(skeleton, config.premultipliedAlpha);
                }

                // Draw the control bones.
                let controlBones = config.controlBones;
                if (controlBones.length) {
                    let selectedBones = this.selectedBones;
                    gl.lineWidth(2);
                    for (let i = 0; i < controlBones.length; i++) {
                        let bone = skeleton.findBone(controlBones[i]);
                        if (!bone) continue;
                        let colorInner = selectedBones[i] ? BONE_INNER_OVER : BONE_INNER;
                        let colorOuter = selectedBones[i] ? BONE_OUTER_OVER : BONE_OUTER;
                        renderer.circle(true, skeleton.x + bone.worldX, skeleton.y + bone.worldY, 20, colorInner);
                        renderer.circle(false, skeleton.x + bone.worldX, skeleton.y + bone.worldY, 20, colorOuter);
                    }
                }

                // Draw the viewport bounds.
                if (config.viewport.debugRender) {
                    gl.lineWidth(1);
                    renderer.rect(false, this.currentViewport.x, this.currentViewport.y, this.currentViewport.width, this.currentViewport.height, Color.GREEN);
                    renderer.rect(false, viewport.x, viewport.y, viewport.width, viewport.height, Color.RED);
                }

                renderer.end();

                if (config.draw) config.draw(this, playDelta);
            }

            // Draw the loading screen.
            if (config.showLoading) {
                this.loadingScreen.backgroundColor.setFromColor(bg);
                this.loadingScreen.draw(loading);
            }
            if (loading && config.loading) config.loading(this, delta);
        } catch (e: any) {
            this.showError(`Error: Unable to render skeleton.\n${e.message}`, e);
        }
    }

    stopRendering() {
        this.stopRequestAnimationFrame = true;
    }








    private showError(message: string, error: Error | null = null) {
        if (this.error) {
            if (error) throw error; // Don't lose error if showError throws, is caught, and showError is called again.
        } else {
            this.error = true;
            this.dom.appendChild(createElement(
                     /*html*/`<div class="spine-player-error" style="background:#000;color:#fff;position:absolute;top:0;width:100%;height:100%;display:flex;justify-content:center;align-items:center;overflow:auto;z-index:999">`
                + message.replace("\n", "<br><br>") + `</div>`));
            if (this.config.error) this.config.error(this, message);
            throw (error ? error : new Error(message));
        }
    }
}






function findWithClass(element: HTMLElement, className: string): HTMLElement {
    return element.getElementsByClassName(className)[0] as HTMLElement;
}

function createElement(html: string): HTMLElement {
    let div = document.createElement("div");
    div.innerHTML = html;
    return div.children[0] as HTMLElement;
}


function toString(object: any) {
    return JSON.stringify(object)
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&#34;")
        .replace(/'/g, "&#39;");
}

const BONE_INNER_OVER = new Color(0.478, 0, 0, 0.25);
const BONE_OUTER_OVER = new Color(1, 1, 1, 1);
const BONE_INNER = new Color(0.478, 0, 0, 0.5);
const BONE_OUTER = new Color(1, 0, 0, 0.8);