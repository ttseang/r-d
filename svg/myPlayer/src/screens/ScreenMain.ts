import { AssetManager } from "@esotericsoftware/spine-webgl";
import { BaseScreen, IScreen, SvgObj } from "svggame";

import { SpineChar } from "../classes/SpineChar";
import { AnimationVo } from "../dataObjs/AnimationVo";

export class ScreenMain extends BaseScreen implements IScreen
{
    private box!:SvgObj;
    private assetManager!:AssetManager;
    private config: any = {};
    private spineChar:SpineChar

    private idleAnimations:AnimationVo[];
    private walkAnimations:AnimationVo[];
    private fidgetAnimations:AnimationVo[];


    constructor()
    {
        super("ScreenMain");

        this.idleAnimations=[new AnimationVo("idle",true),new AnimationVo("Repeating animations/blink",true)];
        this.walkAnimations=[new AnimationVo("walk",true),new AnimationVo("Repeating animations/blink",true)];
        this.fidgetAnimations=[new AnimationVo("fidget01",false),new AnimationVo("Repeating animations/blink",true)];


        this.spineChar=new SpineChar("char","./assets/char/Amka.json","./assets/char/Amka.atlas",this.idleAnimations);
        this.spineChar.flip=true;
        this.spineChar.scaleX=.4;
        this.spineChar.scaleY=.4;
        this.spineChar.x=2000;
        this.spineChar.y=-1400;
        
        
    }
    create()
    {
        (window as any).scene=this;

        this.box=new SvgObj(this,'box');
        this.box.gameWRatio=0.05;
        
        
        
        let scale:number=this.gw/4000

        this.spineChar.setScale(scale);

        setTimeout(() => {
            let posX:number=-this.gw/.4;
            let posY:number=this.gh/10;
            console.log(posX,posY);

            this.spineChar.moveTo(posX,posY,6000,this.walkAnimations,this.idleAnimations,true);
        }, 2000);

        this.grid?.showGrid();
        this.placeOnGrid(2,2,this.box,true);

       /*  setTimeout(() => {
            let posX:number=-this.gw/.4;
            let posY:number=this.gh/10;
        console.log(posX,posY);

            this.spineChar.moveTo(posX,posY,6000,this.walkAnimations,this.idleAnimations,true);
        }, 2000); */
        
    }
   
    doResize()
    {
        super.doResize();
        this.grid?.showGrid();
        this.box.updateScale();
        this.placeOnGrid(2,2,this.box,true);
    }
}