import { BaseScreen, SvgObj, TweenObj, IGameObj, SVGAnimation } from "svggame";



export class ScreenMain extends BaseScreen {
    private button3:SvgObj | null=null;

    constructor() {
        super("ScreenMain");
    }
    create() {
        let s1: SvgObj = new SvgObj(this, "screen1");
        let grid: SvgObj = this.addImage("agrid");
        let button1: SvgObj = this.addImage("change1");
        let button2: SvgObj = this.addImage("btnSpin");
        this.button3 = this.addImage("btnFly");

        this.placeOnGrid(4, 5, button1);
        this.placeOnGrid(4,8,button2);
        this.placeOnGrid(4,6.5,this.button3);

        button1.onClick(this.showGameOver.bind(this));
        button2.onClick(this.startSpin.bind(this));
        this.button3.onClick(this.startTween.bind(this));
      //  button1.skewX=-80;
    //    button1.skewX=-10;
       // button1.skewY=-10;

       

    }
    startTween()
    {
        let tweenObj:TweenObj=new TweenObj();
        tweenObj.skewX=50;
     //   tweenObj.scaleY=.2;
      /*   tweenObj.displayWidth=200;
        tweenObj.displayHeight=200; */
        //tweenObj.x=300;
       // tweenObj.y=400;
       // tweenObj.duration=2000;
        /* tweenObj.alpha=.25;
        tweenObj.angle=359;
        tweenObj.onComplete=this.tweenDone.bind(this); */

      // let tween1:SimpleTween=new SimpleTween(button1,tweenObj);
        if (this.button3)
        {
        this.tweenGridPos(0,9,this.button3 as IGameObj,1000,this.tweenDone.bind(this));
        }
    }
    tweenDone()
    {
        console.log("tween done");
    }
    startSpin()
    {
        console.log("click");
        let spin:SVGAnimation=new SVGAnimation("spin1");
        spin.onComplete(()=>{
            console.log("SPIN Done");
        })
        spin.start();
    }
    showGameOver() {
        this.changeScreen("ScreenOver");
    }
}