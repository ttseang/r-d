/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/classes/Ball.ts":
/*!*****************************!*\
  !*** ./src/classes/Ball.ts ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Ball\": () => (/* binding */ Ball)\n/* harmony export */ });\n/* harmony import */ var _SvgObj__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SvgObj */ \"./src/classes/SvgObj.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\r\n    var extendStatics = function (d, b) {\r\n        extendStatics = Object.setPrototypeOf ||\r\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\r\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\r\n        return extendStatics(d, b);\r\n    };\r\n    return function (d, b) {\r\n        if (typeof b !== \"function\" && b !== null)\r\n            throw new TypeError(\"Class extends value \" + String(b) + \" is not a constructor or null\");\r\n        extendStatics(d, b);\r\n        function __() { this.constructor = d; }\r\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\r\n    };\r\n})();\r\n\r\nvar Ball = /** @class */ (function (_super) {\r\n    __extends(Ball, _super);\r\n    function Ball(scene, key) {\r\n        var _this = _super.call(this, key, false) || this;\r\n        _this.xDir = 1;\r\n        _this.speed = 2;\r\n        _this.scene = scene;\r\n        return _this;\r\n        /*  if (this.el)\r\n         {\r\n             this.el.setAttribute(\"transform\",\"translate(-12.5,-12.5)\");\r\n         } */\r\n    }\r\n    Ball.prototype.move = function () {\r\n        this.incX(this.xDir * this.speed);\r\n        if (this.x > this.scene.gw * .8 && this.xDir == 1) {\r\n            this.xDir = -1;\r\n        }\r\n        if (this.x < this.scene.gw * .15 && this.xDir == -1) {\r\n            this.xDir = 1;\r\n        }\r\n    };\r\n    Ball.prototype.reverse = function () {\r\n        this.xDir = -this.xDir;\r\n    };\r\n    return Ball;\r\n}(_SvgObj__WEBPACK_IMPORTED_MODULE_0__.SvgObj));\r\n\r\n\n\n//# sourceURL=webpack://svg1/./src/classes/Ball.ts?");

/***/ }),

/***/ "./src/classes/Box.ts":
/*!****************************!*\
  !*** ./src/classes/Box.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Box\": () => (/* binding */ Box)\n/* harmony export */ });\n/* harmony import */ var _util_collideUtil__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util/collideUtil */ \"./src/util/collideUtil.ts\");\n/* harmony import */ var _SvgObj__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SvgObj */ \"./src/classes/SvgObj.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\r\n    var extendStatics = function (d, b) {\r\n        extendStatics = Object.setPrototypeOf ||\r\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\r\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\r\n        return extendStatics(d, b);\r\n    };\r\n    return function (d, b) {\r\n        if (typeof b !== \"function\" && b !== null)\r\n            throw new TypeError(\"Class extends value \" + String(b) + \" is not a constructor or null\");\r\n        extendStatics(d, b);\r\n        function __() { this.constructor = d; }\r\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\r\n    };\r\n})();\r\n\r\n\r\nvar Box = /** @class */ (function (_super) {\r\n    __extends(Box, _super);\r\n    function Box(scene, key) {\r\n        var _this = _super.call(this, key) || this;\r\n        _this.nextBox = null;\r\n        _this.scene = scene;\r\n        _this.key = key;\r\n        if (_this.el) {\r\n            //  this.el.setAttribute(\"transform\", \"translate(-12.5,-12.5)\");\r\n            /*  this.el.onclick=()=>{\r\n                this.visible=false;\r\n            }  */\r\n        }\r\n        return _this;\r\n    }\r\n    Box.prototype.move = function () {\r\n        this.incY(1);\r\n        this.incRot(2);\r\n        if (this.y > this.scene.gh) {\r\n            this.y = -100;\r\n            this.x = Math.floor(Math.random() * (this.scene.gw * .8)) + this.scene.gw * .1;\r\n        }\r\n        if (this.nextBox) {\r\n            this.nextBox.move();\r\n        }\r\n    };\r\n    Box.prototype.checkCollide = function (ball) {\r\n        if (this.visible == true) {\r\n            if (_util_collideUtil__WEBPACK_IMPORTED_MODULE_0__.CollideUtil.checkOverlap(this, ball)) {\r\n                if (this.key == \"box2\") {\r\n                    this.scene.doAction(\"gameOver\", \"\");\r\n                }\r\n                else {\r\n                    this.visible = false;\r\n                    this.scene.doAction(\"points\", \"\");\r\n                }\r\n            }\r\n        }\r\n        if (this.nextBox) {\r\n            this.nextBox.checkCollide(ball);\r\n        }\r\n    };\r\n    return Box;\r\n}(_SvgObj__WEBPACK_IMPORTED_MODULE_1__.SvgObj));\r\n\r\n\n\n//# sourceURL=webpack://svg1/./src/classes/Box.ts?");

/***/ }),

/***/ "./src/classes/SvgObj.ts":
/*!*******************************!*\
  !*** ./src/classes/SvgObj.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"SvgObj\": () => (/* binding */ SvgObj)\n/* harmony export */ });\nvar SvgObj = /** @class */ (function () {\r\n    function SvgObj(id, useC) {\r\n        if (useC === void 0) { useC = false; }\r\n        var _a;\r\n        this.el = null;\r\n        this.instanceName = \"\";\r\n        this.useC = false;\r\n        this._x = 0;\r\n        this._y = 0;\r\n        this._angle = 0;\r\n        this._visible = true;\r\n        this._width = 0;\r\n        this._height = 0;\r\n        this._displayWidth = 0;\r\n        this._displayHeight = 0;\r\n        this._scaleX = 1;\r\n        this._scaleY = 1;\r\n        this._alpha = 1;\r\n        this.id = id;\r\n        this.useC = useC;\r\n        var def = document.getElementById(id);\r\n        if (def) {\r\n            this.el = def.cloneNode(true);\r\n            this.el.id = SvgObj.getInstanceName(id);\r\n            if (this.el) {\r\n                if (this.useC == false) {\r\n                    this._x = this.getAttNum(\"x\");\r\n                    this._y = this.getAttNum(\"y\");\r\n                    this._height = this.getAttNum(\"width\");\r\n                    this._width = this.getAttNum(\"height\");\r\n                    this._displayHeight = this._height;\r\n                    this._displayWidth = this._width;\r\n                }\r\n                else {\r\n                    this._x = this.getAttNum(\"cx\");\r\n                    this._y = this.getAttNum(\"cy\");\r\n                    //  console.log(this.getAttNum(\"r\"));\r\n                    this._width = this.getAttNum(\"r\");\r\n                    this._height = this._width;\r\n                    this._displayHeight = this._height;\r\n                    this._displayWidth = this._width;\r\n                }\r\n                (_a = document.getElementById(\"playArea\")) === null || _a === void 0 ? void 0 : _a.appendChild(this.el);\r\n            }\r\n        }\r\n    }\r\n    SvgObj.getInstanceName = function (key) {\r\n        SvgObj.count++;\r\n        return key + \"-\" + SvgObj.count.toString();\r\n    };\r\n    SvgObj.prototype.incRot = function (rot) {\r\n        this.angle = this.angle + rot;\r\n    };\r\n    Object.defineProperty(SvgObj.prototype, \"angle\", {\r\n        get: function () {\r\n            return this._angle;\r\n        },\r\n        set: function (value) {\r\n            while (value > 360) {\r\n                value -= 360;\r\n            }\r\n            this._angle = value;\r\n            if (this.el) {\r\n                var w2 = this.displayWidth / 2;\r\n                var h2 = this.displayHeight / 2;\r\n                w2 += this.x;\r\n                h2 += this.y;\r\n                this.updateTransform();\r\n            }\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"width\", {\r\n        get: function () {\r\n            return this._width;\r\n        },\r\n        set: function (value) {\r\n            this._width = value;\r\n            /* if (this.el) {\r\n                if (this.useC == false) {\r\n                    this.el.setAttribute(\"width\", value.toString());\r\n                }\r\n                else {\r\n                    this.el.setAttribute(\"r\", value.toString());\r\n                }\r\n            } */\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"height\", {\r\n        get: function () {\r\n            return this._height;\r\n        },\r\n        set: function (value) {\r\n            this._height = value;\r\n            /* if (this.el) {\r\n                if (this.useC == false) {\r\n                    this.el.setAttribute(\"height\", value.toString());\r\n                }\r\n                else {\r\n                    this.el.setAttribute(\"r\", value.toString());\r\n                }\r\n            } */\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"displayWidth\", {\r\n        get: function () {\r\n            return this._displayWidth;\r\n        },\r\n        set: function (value) {\r\n            this._displayWidth = value;\r\n            /*  if (this.el) {\r\n                 if (this.useC == false) {\r\n                     this.el.setAttribute(\"width\", value.toString());\r\n                 }\r\n                 else {\r\n                     this.el.setAttribute(\"r\", value.toString());\r\n                 }\r\n             } */\r\n            this._scaleX = value / this.width;\r\n            this.updateTransform();\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"displayHeight\", {\r\n        get: function () {\r\n            return this._displayHeight;\r\n        },\r\n        set: function (value) {\r\n            this._displayHeight = value;\r\n            /*    if (this.el) {\r\n                   if (this.useC == false) {\r\n                       this.el.setAttribute(\"height\", value.toString());\r\n                   }\r\n                   else {\r\n                       this.el.setAttribute(\"r\", value.toString());\r\n                   }\r\n               } */\r\n            this._scaleY = value / this._height;\r\n            this.updateTransform();\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"scaleX\", {\r\n        get: function () {\r\n            return this._scaleX;\r\n        },\r\n        set: function (value) {\r\n            this._scaleX = value;\r\n            this._displayWidth = this.width * value;\r\n            this.updateTransform();\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"scaleY\", {\r\n        get: function () {\r\n            return this._scaleY;\r\n        },\r\n        set: function (value) {\r\n            this._scaleY = value;\r\n            this._displayHeight = this.height * value;\r\n            this.updateTransform();\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    SvgObj.prototype.setScale = function (scale) {\r\n        this._scaleX = scale;\r\n        this._scaleY = scale;\r\n        this.updateTransform();\r\n    };\r\n    Object.defineProperty(SvgObj.prototype, \"alpha\", {\r\n        get: function () {\r\n            return this._alpha;\r\n        },\r\n        set: function (value) {\r\n            this._alpha = value;\r\n            if (this.el) {\r\n                this.el.setAttribute(\"opacity\", value.toString());\r\n            }\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"visible\", {\r\n        get: function () {\r\n            return this._visible;\r\n        },\r\n        set: function (value) {\r\n            this._visible = value;\r\n            if (this.el) {\r\n                this.el.style.display = (value == true) ? \"block\" : \"none\";\r\n            }\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    SvgObj.prototype.destroy = function () {\r\n        if (this.el) {\r\n            this.el.remove();\r\n        }\r\n    };\r\n    SvgObj.prototype.getAttNum = function (attName) {\r\n        if (this.el) {\r\n            var val = this.el.getAttribute(attName);\r\n            if (val == null) {\r\n                return 0;\r\n            }\r\n            if (isNaN(parseInt(val))) {\r\n                return 0;\r\n            }\r\n            return parseInt(val);\r\n        }\r\n        return 0;\r\n    };\r\n    Object.defineProperty(SvgObj.prototype, \"y\", {\r\n        get: function () {\r\n            return this._y;\r\n        },\r\n        set: function (value) {\r\n            this._y = value;\r\n            this.updateTransform();\r\n            if (this.el) {\r\n                /*  if (this.useC == false) {\r\n                     this.el.setAttribute(\"y\", this._y.toString());\r\n                 }\r\n                 else {\r\n                     this.el.setAttribute(\"cy\", this._y.toString());\r\n                 } */\r\n            }\r\n            else {\r\n                console.log(\"missing element\");\r\n            }\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    Object.defineProperty(SvgObj.prototype, \"x\", {\r\n        get: function () {\r\n            return this._x;\r\n        },\r\n        set: function (value) {\r\n            this._x = value;\r\n            this.updateTransform();\r\n            if (this.el) {\r\n                /*  if (this.useC == false) {\r\n                    // console.log(this.el);\r\n     \r\n                     this.el.setAttribute(\"x\", this._x.toString());\r\n                 }\r\n                 else {\r\n                     this.el.setAttribute(\"cx\", this._x.toString());\r\n                 } */\r\n            }\r\n            else {\r\n                console.log(\"missing element\");\r\n            }\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    SvgObj.prototype.updateTransform = function () {\r\n        var w2 = this.displayWidth / 2;\r\n        var h2 = this.displayHeight / 2;\r\n        w2 += this.x;\r\n        h2 += this.y;\r\n        var rotString = \"rotate(\" + this.angle.toString() + \",\" + w2.toString() + \",\" + h2.toString() + \")\";\r\n        var posString = \"translate(\" + this.x.toString() + \",\" + this.y.toString() + \")\";\r\n        var scaleString = \"scale(\" + this._scaleX.toString() + \",\" + this._scaleY.toString() + \")\";\r\n        var transString = rotString + \" \" + posString + \" \" + scaleString;\r\n        if (this.el) {\r\n            this.el.setAttribute(\"transform\", transString);\r\n        }\r\n    };\r\n    SvgObj.prototype.incX = function (val) {\r\n        this.x = this._x + val;\r\n    };\r\n    SvgObj.prototype.incY = function (val) {\r\n        this.y = this._y + val;\r\n    };\r\n    SvgObj.prototype.getBoundingClientRect = function () {\r\n        if (this.el) {\r\n            return this.el.getBoundingClientRect();\r\n        }\r\n        return new DOMRect(0, 0, 100, 100);\r\n    };\r\n    SvgObj.prototype.clone = function (instance) {\r\n        var _a;\r\n        if (this.el) {\r\n            var nb = this.el.cloneNode(true);\r\n            nb.id = instance;\r\n            //   nb.setAttribute(\"x\",x.toString());\r\n            // nb.setAttribute(\"y\",y.toString());\r\n            (_a = document.getElementById(\"playArea\")) === null || _a === void 0 ? void 0 : _a.appendChild(nb);\r\n            return new SvgObj(instance);\r\n        }\r\n        return null;\r\n    };\r\n    SvgObj.count = 0;\r\n    return SvgObj;\r\n}());\r\n\r\n0;\r\n\n\n//# sourceURL=webpack://svg1/./src/classes/SvgObj.ts?");

/***/ }),

/***/ "./src/classes/TextObj.ts":
/*!********************************!*\
  !*** ./src/classes/TextObj.ts ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"TextObj\": () => (/* binding */ TextObj)\n/* harmony export */ });\n/* harmony import */ var _SvgObj__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SvgObj */ \"./src/classes/SvgObj.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\r\n    var extendStatics = function (d, b) {\r\n        extendStatics = Object.setPrototypeOf ||\r\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\r\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\r\n        return extendStatics(d, b);\r\n    };\r\n    return function (d, b) {\r\n        if (typeof b !== \"function\" && b !== null)\r\n            throw new TypeError(\"Class extends value \" + String(b) + \" is not a constructor or null\");\r\n        extendStatics(d, b);\r\n        function __() { this.constructor = d; }\r\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\r\n    };\r\n})();\r\n\r\nvar TextObj = /** @class */ (function (_super) {\r\n    __extends(TextObj, _super);\r\n    function TextObj(key) {\r\n        return _super.call(this, key, false) || this;\r\n    }\r\n    TextObj.createNew = function () {\r\n        //implement here\r\n    };\r\n    TextObj.prototype.setText = function (text) {\r\n        if (this.el) {\r\n            console.log(text);\r\n            this.el.textContent = text;\r\n        }\r\n    };\r\n    return TextObj;\r\n}(_SvgObj__WEBPACK_IMPORTED_MODULE_0__.SvgObj));\r\n\r\n\n\n//# sourceURL=webpack://svg1/./src/classes/TextObj.ts?");

/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main */ \"./src/main.ts\");\n\r\nwindow.onload = function () {\r\n    var main = new _main__WEBPACK_IMPORTED_MODULE_0__.Main();\r\n};\r\n\n\n//# sourceURL=webpack://svg1/./src/index.ts?");

/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Main\": () => (/* binding */ Main)\n/* harmony export */ });\n/* harmony import */ var _classes_Ball__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/Ball */ \"./src/classes/Ball.ts\");\n/* harmony import */ var _classes_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./classes/Box */ \"./src/classes/Box.ts\");\n/* harmony import */ var _classes_TextObj__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./classes/TextObj */ \"./src/classes/TextObj.ts\");\n\r\n\r\n\r\nvar Main = /** @class */ (function () {\r\n    //private ship:SvgObj;\r\n    //   private box:SvgObj;\r\n    // public boxArray:Box[]=[];\r\n    function Main() {\r\n        // public ballDef: Ball = new Ball(\"ball\");\r\n        //public gboxDef: Box = new Box(\"box1\");\r\n        //public wboxDef: Box = new Box(\"box2\");\r\n        this.boxCount = 0;\r\n        this.gw = 480;\r\n        this.gh = 640;\r\n        this.firstBox = null;\r\n        this.prevBox = null;\r\n        this.gameOver = false;\r\n        this.score = 0;\r\n        this.svg = document.getElementById(\"myCanvas\");\r\n        // this.box1=new SvgObj(\"box1\");\r\n        this.ball = new _classes_Ball__WEBPACK_IMPORTED_MODULE_0__.Ball(this, \"ship\");\r\n        // this.ship=new SvgObj(\"ship\");\r\n        this.scoreText = new _classes_TextObj__WEBPACK_IMPORTED_MODULE_2__.TextObj(\"scoreText\");\r\n        // this.box=new SvgObj(\"box1\");\r\n        window.scene = this;\r\n        if (this.svg) {\r\n            this.buildGame();\r\n        }\r\n        this.updateTimer = setInterval(this.update.bind(this), 20);\r\n    }\r\n    Main.prototype.buildGame = function () {\r\n        var _this = this;\r\n        if (this.ball) {\r\n            this.ball.setScale(0.25);\r\n            this.ball.x = this.gw / 2;\r\n            this.ball.y = this.gh / 2;\r\n            this.ball.y -= this.ball.displayHeight / 4;\r\n        }\r\n        // this.setClick(\"box1\",this.clickMe.bind(this));\r\n        var colors = [1, 1, 1, 0, 1, 1, 0, 0, 1];\r\n        var colorIndex = -1;\r\n        for (var i = 0; i < 10; i++) {\r\n            colorIndex++;\r\n            if (colorIndex > colors.length) {\r\n                colorIndex = 0;\r\n            }\r\n            var color = colors[colorIndex];\r\n            var y = -i * this.gh / 10 - 100;\r\n            var x = Math.floor(Math.random() * (this.gw * .8)) + this.gw * .1;\r\n            var box = this.addNewBox(x, y, color);\r\n            if (box) {\r\n                //    this.boxArray.push(box);\r\n                if (this.firstBox == null) {\r\n                    this.firstBox = box;\r\n                }\r\n                if (this.prevBox) {\r\n                    this.prevBox.nextBox = box;\r\n                }\r\n                this.prevBox = box;\r\n            }\r\n        }\r\n        if (this.svg) {\r\n            this.svg.onclick = function () { var _a; (_a = _this.ball) === null || _a === void 0 ? void 0 : _a.reverse(); };\r\n        }\r\n    };\r\n    Main.prototype.addNewBox = function (x, y, color) {\r\n        this.boxCount++;\r\n        var box;\r\n        if (color == 0) {\r\n            box = new _classes_Box__WEBPACK_IMPORTED_MODULE_1__.Box(this, \"box1\");\r\n        }\r\n        else {\r\n            box = new _classes_Box__WEBPACK_IMPORTED_MODULE_1__.Box(this, \"box2\");\r\n        }\r\n        if (box) {\r\n            box.x = x;\r\n            box.y = y;\r\n        }\r\n        return box;\r\n    };\r\n    Main.prototype.doAction = function (action, param) {\r\n        if (action == \"gameOver\") {\r\n            this.gameOver = true;\r\n        }\r\n        if (action == \"points\") {\r\n            this.score++;\r\n            this.scoreText.setText(\"Score \" + this.score.toString());\r\n        }\r\n    };\r\n    Main.prototype.update = function () {\r\n        if (this.gameOver == false) {\r\n            if (this.firstBox) {\r\n                this.firstBox.move();\r\n                if (this.ball) {\r\n                    this.firstBox.checkCollide(this.ball);\r\n                }\r\n            }\r\n            if (this.ball) {\r\n                this.ball.move();\r\n            }\r\n        }\r\n    };\r\n    return Main;\r\n}());\r\n\r\n\n\n//# sourceURL=webpack://svg1/./src/main.ts?");

/***/ }),

/***/ "./src/util/collideUtil.ts":
/*!*********************************!*\
  !*** ./src/util/collideUtil.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"CollideUtil\": () => (/* binding */ CollideUtil)\n/* harmony export */ });\nvar CollideUtil = /** @class */ (function () {\r\n    function CollideUtil() {\r\n    }\r\n    CollideUtil.checkOverlap = function (obj1, obj2) {\r\n        var r1 = obj1.getBoundingClientRect(); //BOUNDING BOX OF THE FIRST OBJECT\r\n        var r2 = obj2.getBoundingClientRect(); //BOUNDING BOX OF THE SECOND OBJECT\r\n        //CHECK IF THE TWO BOUNDING BOXES OVERLAP\r\n        return !(r2.left > r1.right ||\r\n            r2.right < r1.left ||\r\n            r2.top > r1.bottom ||\r\n            r2.bottom < r1.top);\r\n    };\r\n    return CollideUtil;\r\n}());\r\n\r\n\n\n//# sourceURL=webpack://svg1/./src/util/collideUtil.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.ts");
/******/ 	
/******/ })()
;