/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../gen2/common/mods/svgGame/classes/core/BaseScreen.js":
/*!*****************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/BaseScreen.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.BaseScreen = void 0;
var AlignGridSvg_1 = __webpack_require__(/*! ../../util/AlignGridSvg */ "../../gen2/common/mods/svgGame/util/AlignGridSvg.js");
var GameManager_1 = __webpack_require__(/*! ./GameManager */ "../../gen2/common/mods/svgGame/classes/core/GameManager.js");
var SoundObj_1 = __webpack_require__(/*! ./gameobjects/SoundObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/SoundObj.js");
var SvgObj_1 = __webpack_require__(/*! ./gameobjects/SvgObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/SvgObj.js");
var TextObj_1 = __webpack_require__(/*! ./gameobjects/TextObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/TextObj.js");
var ScreenManager_1 = __webpack_require__(/*! ./ScreenManager */ "../../gen2/common/mods/svgGame/classes/core/ScreenManager.js");
var SimpleTween_1 = __webpack_require__(/*! ./tweens/SimpleTween */ "../../gen2/common/mods/svgGame/classes/core/tweens/SimpleTween.js");
var TweenObj_1 = __webpack_require__(/*! ./tweens/TweenObj */ "../../gen2/common/mods/svgGame/classes/core/tweens/TweenObj.js");
var BaseScreen = /** @class */ (function () {
    function BaseScreen(key) {
        this.gm = GameManager_1.GameManager.getInstance();
        this.screenManager = ScreenManager_1.ScreenManager.getInstance();
        this.children = [];
        this.grid = null;
        this.key = key;
    }
    BaseScreen.prototype.start = function () {
        this.grid = new AlignGridSvg_1.AlignGrid(11, 11, this.gm.gw, this.gm.gh);
        this.create();
    };
    BaseScreen.prototype.create = function () {
    };
    BaseScreen.prototype.destroy = function () {
        var _a;
        while (this.children.length > 0) {
            (_a = this.children.pop()) === null || _a === void 0 ? void 0 : _a.destroy();
        }
    };
    BaseScreen.prototype.addExisting = function (obj) {
        this.children.push(obj);
    };
    BaseScreen.prototype.addImage = function (key) {
        var svgObj = new SvgObj_1.SvgObj(this, key);
        this.addExisting(svgObj);
        return svgObj;
    };
    BaseScreen.prototype.addImageAt = function (key, x, y) {
        var svgObj = this.addImage(key);
        svgObj.x = x;
        svgObj.y = y;
        return svgObj;
    };
    BaseScreen.prototype.addText = function (text, textKey) {
        if (textKey === void 0) { textKey = "defaultText"; }
        var textObj = new TextObj_1.TextObj(this, textKey);
        textObj.setText(text);
        this.addExisting(textObj);
        return textObj;
    };
    BaseScreen.prototype.addTextAt = function (text, x, y, textKey) {
        if (textKey === void 0) { textKey = "defaultText"; }
        var textObj = this.addText(textKey, text);
        textObj.x = x;
        textObj.y = y;
        return textObj;
    };
    BaseScreen.prototype.centerW = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        obj.x = this.gw / 2;
        if (adjust == true) {
            obj.x -= obj.displayWidth / 2;
        }
    };
    BaseScreen.prototype.centerH = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        obj.y = this.gh / 2;
        if (adjust) {
            obj.y -= obj.displayHeight / 2;
        }
    };
    BaseScreen.prototype.placeOnGrid = function (col, row, obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        var xx = this.grid.cw * col;
        var yy = this.grid.ch * row;
        obj.x = xx;
        obj.y = yy;
        if (adjust) {
            obj.y += this.grid.ch / 2 - obj.displayHeight / 2;
            obj.x += this.grid.cw / 2 - obj.displayWidth / 2;
        }
    };
    BaseScreen.prototype.tweenGridPos = function (col, row, obj, duration, callback) {
        if (callback === void 0) { callback = function () { }; }
        var tweenObj = new TweenObj_1.TweenObj();
        var xx = this.grid.cw * col;
        var yy = this.grid.ch * row;
        tweenObj.x = xx;
        tweenObj.y = yy;
        tweenObj.onComplete = callback;
        tweenObj.duration = duration;
        var tween = new SimpleTween_1.SimpleTween(obj, tweenObj);
    };
    BaseScreen.prototype.center = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        this.centerW(obj, adjust);
        this.centerH(obj, adjust);
    };
    Object.defineProperty(BaseScreen.prototype, "gh", {
        /**
         * game height;
         */
        get: function () {
            return this.gm.gh;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BaseScreen.prototype, "gw", {
        /**
         * game width
         */
        get: function () {
            return this.gm.gw;
        },
        enumerable: false,
        configurable: true
    });
    BaseScreen.prototype.changeScreen = function (nScreen) {
        //  console.log("change to "+nScreen);
        this.screenManager.changeScreen(nScreen);
    };
    BaseScreen.prototype.doResize = function () {
        //can override in screen
        /*   this.grid.hide();
          this.grid.resetSize(); */
        this.grid = new AlignGridSvg_1.AlignGrid(11, 11, this.gm.gw, this.gm.gh);
        for (var i = 0; i < this.children.length; i++) {
            this.children[i].doResize();
        }
    };
    BaseScreen.prototype.playSound = function (path) {
        var s = new SoundObj_1.SoundObj(path);
        s.play();
    };
    return BaseScreen;
}());
exports.BaseScreen = BaseScreen;
//# sourceMappingURL=BaseScreen.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/GameManager.js":
/*!******************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/GameManager.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.GameManager = void 0;
var fontSizeVo_1 = __webpack_require__(/*! ./dataobjs/fontSizeVo */ "../../gen2/common/mods/svgGame/classes/core/dataobjs/fontSizeVo.js");
var gameOptions_1 = __webpack_require__(/*! ./dataobjs/gameOptions */ "../../gen2/common/mods/svgGame/classes/core/dataobjs/gameOptions.js");
var ScreenManager_1 = __webpack_require__(/*! ./ScreenManager */ "../../gen2/common/mods/svgGame/classes/core/ScreenManager.js");
var GameManager = /** @class */ (function () {
    function GameManager() {
        this.gameOptions = new gameOptions_1.GameOptions();
        this.gameID = "";
        this.gameEl = null;
        this.gw = 0;
        this.gh = 0;
        this.objMap = new Map();
        this.defFontSize = new fontSizeVo_1.FontSizeVo(70, 1000);
        this.fontSizes = new Map();
        window.gm = this;
        window.onresize = this.doResize.bind(this);
    }
    GameManager.getInstance = function () {
        if (this.instance == null) {
            this.instance = new GameManager();
        }
        return this.instance;
    };
    GameManager.prototype.regObj = function (id, obj) {
        //console.log("register " + id);
        this.objMap.set(id, obj);
    };
    GameManager.prototype.unRegObj = function (id) {
        this.objMap.delete(id);
    };
    GameManager.prototype.getObj = function (id) {
        if (this.objMap.has(id)) {
            return this.objMap.get(id);
        }
        return null;
    };
    GameManager.prototype.getFontSize = function (sizeKey, canvasWidth) {
        var fontSizeVo = this.defFontSize;
        if (this.fontSizes.has(sizeKey)) {
            fontSizeVo = this.fontSizes.get(sizeKey);
        }
        var fontSize = fontSizeVo.defFontSize;
        var fontBase = fontSizeVo.canvasBase;
        var ratio = fontSize / fontBase; // calc ratio
        var size = canvasWidth * ratio; // get font size based on current width
        return (size | 0);
    };
    GameManager.prototype.regFontSize = function (key, defFontSize, canvasBase) {
        this.fontSizes.set(key, new fontSizeVo_1.FontSizeVo(defFontSize, canvasBase));
    };
    GameManager.prototype.doResize = function () {
        if (this.gameOptions.useFull == true) {
            if (this.gameEl) {
                var b = document.body.getBoundingClientRect();
                // let b: any = (this.gameEl as any).getBBox();
                this.gw = b.width;
                this.gh = b.height;
            }
        }
        ScreenManager_1.ScreenManager.getInstance().doResize();
    };
    return GameManager;
}());
exports.GameManager = GameManager;
//# sourceMappingURL=GameManager.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/ScreenManager.js":
/*!********************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/ScreenManager.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ScreenManager = void 0;
var ScreenManager = /** @class */ (function () {
    function ScreenManager() {
        this.screens = [];
        this.currentScreen = null;
    }
    ScreenManager.getInstance = function () {
        if (this.instance === null) {
            this.instance = new ScreenManager();
        }
        return this.instance;
    };
    ScreenManager.prototype.doResize = function () {
        if (this.currentScreen) {
            this.currentScreen.doResize();
        }
    };
    ScreenManager.prototype.startScreen = function (screen) {
        if (this.currentScreen != null) {
            this.currentScreen.destroy();
        }
        this.currentScreen = screen;
        this.currentScreen.start();
    };
    ScreenManager.prototype.findScreen = function (screenName) {
        for (var i = 0; i < this.screens.length; i++) {
            if (this.screens[i].key == screenName) {
                return this.screens[i];
            }
        }
        return null;
    };
    ScreenManager.prototype.changeScreen = function (screenName) {
        var screen = this.findScreen(screenName);
        if (!screen) {
            throw new Error("Screen Not Found");
        }
        this.startScreen(screen);
    };
    ScreenManager.prototype.init = function () {
        if (this.screens.length > 0) {
            this.startScreen(this.screens[0]);
            this.doResize();
        }
    };
    ScreenManager.instance = null;
    return ScreenManager;
}());
exports.ScreenManager = ScreenManager;
//# sourceMappingURL=ScreenManager.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/SvgGame.js":
/*!**************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/SvgGame.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SVGGame = void 0;
var SvgUtil_1 = __webpack_require__(/*! ../../util/SvgUtil */ "../../gen2/common/mods/svgGame/util/SvgUtil.js");
var gameOptions_1 = __webpack_require__(/*! ./dataobjs/gameOptions */ "../../gen2/common/mods/svgGame/classes/core/dataobjs/gameOptions.js");
var GameManager_1 = __webpack_require__(/*! ./GameManager */ "../../gen2/common/mods/svgGame/classes/core/GameManager.js");
var ScreenManager_1 = __webpack_require__(/*! ./ScreenManager */ "../../gen2/common/mods/svgGame/classes/core/ScreenManager.js");
var SVGGame = /** @class */ (function () {
    function SVGGame(gameID, gameOptions) {
        this.gameEl = null;
        this.screenManager = ScreenManager_1.ScreenManager.getInstance();
        this.gameManager = GameManager_1.GameManager.getInstance();
        var width = gameOptions.width;
        var height = gameOptions.height;
        this.gameEl = document.getElementById(gameID);
        if (this.gameEl == null) {
            if (width == 0 || height == 0) {
                throw new Error("Height and Width must be set if creating dynamic svg canvas");
                // return;
            }
            this.gameEl = document.createElement("svg");
            this.gameEl.id = gameID;
            this.gameEl.setAttribute("width", width.toString());
            this.gameEl.setAttribute("height", height.toString());
            document.body.appendChild(this.gameEl);
            this.gw = width;
            this.gh = height;
        }
        else {
            this.gw = SvgUtil_1.SVGUtil.getAttNum(this.gameEl, "width");
            this.gh = SvgUtil_1.SVGUtil.getAttNum(this.gameEl, "height");
        }
        this.gameManager.gameEl = this.gameEl;
        this.gameManager.gameID = gameID;
        if (gameOptions.screens.length > 0) {
            this.screenManager.screens = gameOptions.screens;
        }
        else {
            gameOptions = new gameOptions_1.GameOptions();
        }
        if (gameOptions.addAreaID == "") {
            gameOptions.addAreaID = gameID;
        }
        if (gameOptions.useFull == true) {
            if (this.gameEl) {
                var b = this.gameEl.getBBox();
                console.log(b);
                this.gw = b.width;
                this.gh = b.height;
                console.log(this.gh);
                console.log(this.gw);
            }
        }
        gameOptions.width = this.gw;
        gameOptions.height = this.gh;
        this.gameManager.gameOptions = gameOptions;
        this.gameManager.gw = this.gw;
        this.gameManager.gh = this.gh;
        this.screenManager.init();
    }
    return SVGGame;
}());
exports.SVGGame = SVGGame;
//# sourceMappingURL=SvgGame.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/animations/SvgAnimation.js":
/*!******************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/animations/SvgAnimation.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SVGAnimation = void 0;
var SVGAnimation = /** @class */ (function () {
    function SVGAnimation(id) {
        this.id = id;
        this.el = document.getElementById(id);
        console.log(this.el);
        window.anim = this;
    }
    SVGAnimation.prototype.onComplete = function (callback) {
        if (this.el) {
            this.el.addEventListener("endEvent", function () {
                callback();
            });
        }
    };
    SVGAnimation.prototype.start = function () {
        if (this.el) {
            this.el.beginElement();
        }
    };
    SVGAnimation.prototype.stop = function () {
        if (this.el) {
            this.el.endElement();
        }
    };
    return SVGAnimation;
}());
exports.SVGAnimation = SVGAnimation;
//# sourceMappingURL=SvgAnimation.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/dataobjs/fontSizeVo.js":
/*!**************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/dataobjs/fontSizeVo.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.FontSizeVo = void 0;
var FontSizeVo = /** @class */ (function () {
    function FontSizeVo(defFontSize, canvasBase) {
        this.defFontSize = defFontSize;
        this.canvasBase = canvasBase;
    }
    return FontSizeVo;
}());
exports.FontSizeVo = FontSizeVo;
//# sourceMappingURL=fontSizeVo.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/dataobjs/gameOptions.js":
/*!***************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/dataobjs/gameOptions.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.GameOptions = void 0;
var GameOptions = /** @class */ (function () {
    function GameOptions() {
        this.screens = [];
        this.width = 0;
        this.height = 0;
        this.addAreaID = "main";
        this.useFull = false;
    }
    return GameOptions;
}());
exports.GameOptions = GameOptions;
//# sourceMappingURL=gameOptions.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/dataobjs/posVo.js":
/*!*********************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/dataobjs/posVo.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PosVo = void 0;
var PosVo = /** @class */ (function () {
    function PosVo(x, y) {
        this.x = x;
        this.y = y;
    }
    return PosVo;
}());
exports.PosVo = PosVo;
//# sourceMappingURL=posVo.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/gameobjects/FTextObj.js":
/*!***************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/gameobjects/FTextObj.js ***!
  \***************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.FTextObj = void 0;
var SvgObj_1 = __webpack_require__(/*! ./SvgObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/SvgObj.js");
var FTextObj = /** @class */ (function (_super) {
    __extends(FTextObj, _super);
    function FTextObj(screen, key) {
        var _this = _super.call(this, screen, key) || this;
        _this.textEl = null;
        return _this;
    }
    FTextObj.createNew = function () {
        //implement here
    };
    FTextObj.prototype.setText = function (text) {
        if (this.el) {
            console.log(text);
            if (this.el) {
                this.textEl = this.el.getElementsByTagName("p")[0];
                console.log(this.textEl);
                this.textEl.innerHTML = text;
            }
            //this.el.textContent=text;
            this.updateSizes();
        }
    };
    FTextObj.prototype.setFontSize = function (size) {
        if (this.el) {
            this.el.setAttribute("font-size", size.toString() + "px");
            this.updateSizes();
        }
    };
    return FTextObj;
}(SvgObj_1.SvgObj));
exports.FTextObj = FTextObj;
//# sourceMappingURL=FTextObj.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/gameobjects/SoundObj.js":
/*!***************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/gameobjects/SoundObj.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SoundObj = void 0;
var SoundObj = /** @class */ (function () {
    function SoundObj(src) {
        this.sound = document.createElement("audio");
        this.sound.src = src;
        this.sound.setAttribute("preload", "auto");
        this.sound.setAttribute("controls", "none");
        this.sound.style.display = "none";
        document.body.appendChild(this.sound);
    }
    SoundObj.prototype.play = function () {
        this.sound.play();
    };
    SoundObj.prototype.stop = function () {
        this.sound.pause();
    };
    return SoundObj;
}());
exports.SoundObj = SoundObj;
//# sourceMappingURL=SoundObj.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/gameobjects/SvgObj.js":
/*!*************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/gameobjects/SvgObj.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SvgObj = void 0;
var GameManager_1 = __webpack_require__(/*! ../GameManager */ "../../gen2/common/mods/svgGame/classes/core/GameManager.js");
var SvgObj = /** @class */ (function () {
    function SvgObj(screen, id) {
        var _a;
        this.el = null;
        this.elID = "";
        this._gameWRatio = -1;
        this._gameHRatio = -1;
        this._x = 0;
        this._y = 0;
        this._angle = 0;
        this._visible = true;
        this._width = 0;
        this._height = 0;
        this._displayWidth = 0;
        this._displayHeight = 0;
        this._scaleX = 1;
        this._scaleY = 1;
        this._skewX = 0;
        this._skewY = 0;
        this.gm = GameManager_1.GameManager.getInstance();
        this._alpha = 1;
        this.id = id;
        this.screen = screen;
        var def = document.getElementById(id);
        if (def) {
            this.el = def.cloneNode(true);
            this.elID = SvgObj.getInstanceName(id);
            this.el.id = this.elID;
            if (this.el) {
                this._x = this.getAttNum("x");
                this._y = this.getAttNum("y");
                var addArea = "playArea";
                if (this.gm.gameOptions) {
                    addArea = this.gm.gameOptions.addAreaID;
                }
                (_a = document.getElementById(addArea)) === null || _a === void 0 ? void 0 : _a.appendChild(this.el);
                var bounds = this.el.getBoundingClientRect();
                this._height = bounds.height;
                this._width = bounds.width;
                this._displayHeight = this._height;
                this._displayWidth = this._width;
                this.gm.regObj(this.elID, this);
            }
        }
        screen.addExisting(this);
    }
    SvgObj.prototype.updateSizes = function () {
        var bounds = this.el.getBoundingClientRect();
        this._height = bounds.height;
        this._width = bounds.width;
        this._displayHeight = this._height;
        this._displayWidth = this._width;
    };
    SvgObj.prototype.onClick = function (callback) {
        if (this.el) {
            this.el.onclick = function () { callback(); };
        }
    };
    SvgObj.getInstanceName = function (key) {
        SvgObj.count++;
        return key + "-" + SvgObj.count.toString();
    };
    SvgObj.prototype.incRot = function (rot) {
        this.angle = this.angle + rot;
    };
    Object.defineProperty(SvgObj.prototype, "angle", {
        get: function () {
            return this._angle;
        },
        set: function (value) {
            while (value > 360) {
                value -= 360;
            }
            this._angle = value;
            if (this.el) {
                var w2 = this.displayWidth / 2;
                var h2 = this.displayHeight / 2;
                w2 += this.x;
                h2 += this.y;
                this.updateTransform();
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "width", {
        get: function () {
            return this._width;
        },
        set: function (value) {
            this._width = value;
            /* if (this.el) {
                if (this.useC == false) {
                    this.el.setAttribute("width", value.toString());
                }
                else {
                    this.el.setAttribute("r", value.toString());
                }
            } */
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "height", {
        get: function () {
            return this._height;
        },
        set: function (value) {
            this._height = value;
            /* if (this.el) {
                if (this.useC == false) {
                    this.el.setAttribute("height", value.toString());
                }
                else {
                    this.el.setAttribute("r", value.toString());
                }
            } */
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "displayWidth", {
        get: function () {
            return this._displayWidth;
        },
        set: function (value) {
            this._displayWidth = value;
            /*  if (this.el) {
                 if (this.useC == false) {
                     this.el.setAttribute("width", value.toString());
                 }
                 else {
                     this.el.setAttribute("r", value.toString());
                 }
             } */
            this._scaleX = value / this.width;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "displayHeight", {
        get: function () {
            return this._displayHeight;
        },
        set: function (value) {
            this._displayHeight = value;
            /*    if (this.el) {
                   if (this.useC == false) {
                       this.el.setAttribute("height", value.toString());
                   }
                   else {
                       this.el.setAttribute("r", value.toString());
                   }
               } */
            this._scaleY = value / this._height;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "scaleX", {
        get: function () {
            return this._scaleX;
        },
        set: function (value) {
            this._scaleX = value;
            this._displayWidth = this.width * value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "scaleY", {
        get: function () {
            return this._scaleY;
        },
        set: function (value) {
            this._scaleY = value;
            this._displayHeight = this.height * value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    SvgObj.prototype.setScale = function (scale) {
        this._scaleX = scale;
        this._scaleY = scale;
        this.updateTransform();
    };
    Object.defineProperty(SvgObj.prototype, "skewX", {
        get: function () {
            return this._skewX;
        },
        set: function (value) {
            this._skewX = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "skewY", {
        get: function () {
            return this._skewY;
        },
        set: function (value) {
            this._skewY = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "alpha", {
        get: function () {
            return this._alpha;
        },
        set: function (value) {
            this._alpha = value;
            if (this.el) {
                this.el.setAttribute("opacity", value.toString());
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (value) {
            this._visible = value;
            if (this.el) {
                this.el.style.display = (value == true) ? "block" : "none";
            }
        },
        enumerable: false,
        configurable: true
    });
    SvgObj.prototype.destroy = function () {
        if (this.el) {
            this.gm.unRegObj(this.id);
            this.el.remove();
        }
    };
    SvgObj.prototype.getAttNum = function (attName) {
        if (this.el) {
            var val = this.el.getAttribute(attName);
            if (val == null) {
                return 0;
            }
            if (isNaN(parseInt(val))) {
                return 0;
            }
            return parseInt(val);
        }
        return 0;
    };
    Object.defineProperty(SvgObj.prototype, "y", {
        get: function () {
            return this._y;
        },
        set: function (value) {
            this._y = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "x", {
        get: function () {
            return this._x;
        },
        set: function (value) {
            this._x = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    SvgObj.prototype.updateTransform = function () {
        var w2 = this.displayWidth / 2;
        var h2 = this.displayHeight / 2;
        w2 += this.x;
        h2 += this.y;
        w2 = this.roundVal(w2);
        h2 = this.roundVal(h2);
        var rotString = "rotate(" + this.roundVal(this.angle).toString() + "," + this.roundVal(w2).toString() + "," + this.roundVal(h2).toString() + ")";
        var posString = "translate(" + this.roundVal(this.x).toString() + "," + this.roundVal(this.y).toString() + ")";
        var scaleString = "scale(" + this.roundVal(this._scaleX).toString() + "," + this.roundVal(this._scaleY).toString() + ")";
        var skewString = "skewX(" + this.roundVal(this._skewX).toString() + ") skewY(" + this.roundVal(this._skewY).toString() + ")";
        var transString = rotString + " " + posString + " " + scaleString + " " + skewString;
        // console.log(transString);
        if (this.el) {
            this.el.setAttribute("transform", transString);
        }
    };
    SvgObj.prototype.roundVal = function (num) {
        return Math.floor(num * 1000) / 1000;
    };
    SvgObj.prototype.incX = function (val) {
        this.x = this._x + val;
    };
    SvgObj.prototype.incY = function (val) {
        this.y = this._y + val;
    };
    SvgObj.prototype.getBoundingClientRect = function () {
        if (this.el) {
            return this.el.getBoundingClientRect();
        }
        return new DOMRect(0, 0, 100, 100);
    };
    Object.defineProperty(SvgObj.prototype, "gameWRatio", {
        get: function () {
            return this._gameWRatio;
        },
        set: function (value) {
            this._gameWRatio = value;
            this.displayWidth = this.screen.gw * value;
            this.scaleY = this.scaleX;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "gameHRatio", {
        get: function () {
            return this._gameHRatio;
        },
        set: function (value) {
            this._gameHRatio = value;
            this.displayHeight = this.screen.gh * value;
            this.scaleX = this.scaleY;
        },
        enumerable: false,
        configurable: true
    });
    SvgObj.prototype.adjust = function () {
        this.y -= this.displayHeight / 2;
        this.x -= this.displayWidth / 2;
    };
    SvgObj.prototype.adjustForward = function () {
        this.y += this.displayHeight / 2;
        this.x += this.displayWidth / 2;
    };
    SvgObj.prototype.updateScale = function () {
        if (this._gameWRatio != -1) {
            this.gameWRatio = this._gameWRatio;
        }
        else {
            if (this._gameHRatio != -1) {
                this.gameHRatio = this._gameHRatio;
            }
        }
    };
    SvgObj.prototype.doResize = function () {
        //override in class
    };
    SvgObj.count = 0;
    return SvgObj;
}());
exports.SvgObj = SvgObj;
//# sourceMappingURL=SvgObj.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/gameobjects/TextObj.js":
/*!**************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/gameobjects/TextObj.js ***!
  \**************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.TextObj = void 0;
var SvgObj_1 = __webpack_require__(/*! ./SvgObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/SvgObj.js");
var TextObj = /** @class */ (function (_super) {
    __extends(TextObj, _super);
    function TextObj(screen, key) {
        return _super.call(this, screen, key) || this;
    }
    TextObj.createNew = function () {
        //implement here
    };
    TextObj.prototype.setText = function (text) {
        if (this.el) {
            console.log(text);
            this.el.textContent = text;
            this.updateSizes();
        }
    };
    TextObj.prototype.setFontSize = function (size) {
        if (this.el) {
            this.el.setAttribute("font-size", size.toString() + "px");
            this.updateSizes();
        }
    };
    return TextObj;
}(SvgObj_1.SvgObj));
exports.TextObj = TextObj;
//# sourceMappingURL=TextObj.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/tweens/SimpleTween.js":
/*!*************************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/tweens/SimpleTween.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SimpleTween = void 0;
var SimpleTween = /** @class */ (function () {
    function SimpleTween(gameObj, tweenObj, autoStart) {
        if (autoStart === void 0) { autoStart = true; }
        /**
         * Increments
         *  */
        this.xInc = 0;
        this.yInc = 0;
        this.alphaInc = 0;
        this.angleInc = 0;
        this.widthInc = 0;
        this.heightInc = 0;
        this.scaleXInc = 0;
        this.scaleYInc = 0;
        this.skewXInc = 0;
        this.skewYInc = 0;
        /**
         * Timer
         */
        this.myTimer = null;
        this.myTime = 0;
        this.gameObj = gameObj;
        this.tweenObj = tweenObj;
        this.autoStart = autoStart;
        //
        //
        this.xTarget = gameObj.x;
        this.yTarget = gameObj.y;
        this.alphaTarget = gameObj.alpha;
        this.angleTarget = gameObj.angle;
        this.htarget = gameObj.displayHeight;
        this.wtarget = gameObj.displayWidth;
        this.scaleXTarget = gameObj.scaleX;
        this.scaleYTarget = gameObj.scaleY;
        this.skewXTarget = gameObj.skewX;
        this.skewYTarget = gameObj.skewY;
        if (tweenObj.x != null) {
            this.xTarget = tweenObj.x;
        }
        if (tweenObj.y != null) {
            this.yTarget = tweenObj.y;
        }
        if (tweenObj.alpha != null) {
            this.alphaTarget = tweenObj.alpha;
        }
        if (tweenObj.angle != null) {
            this.angleTarget = tweenObj.angle;
        }
        if (tweenObj.displayWidth != null) {
            this.wtarget = tweenObj.displayWidth;
        }
        if (tweenObj.displayHeight != null) {
            this.htarget = tweenObj.displayHeight;
        }
        if (tweenObj.scaleX != null) {
            this.scaleXTarget = tweenObj.scaleX;
        }
        if (tweenObj.scaleY != null) {
            this.scaleYTarget = tweenObj.scaleY;
        }
        if (tweenObj.skewX != null) {
            this.skewXTarget = tweenObj.skewX;
        }
        if (tweenObj.skewY != null) {
            this.skewYTarget = tweenObj.skewY;
        }
        var duration2 = tweenObj.duration / 5;
        this.myTime = tweenObj.duration;
        //
        //
        this.xInc = (this.xTarget - gameObj.x) / duration2;
        this.yInc = (this.yTarget - gameObj.y) / duration2;
        //
        //
        this.alphaInc = (this.alphaTarget - gameObj.alpha) / duration2;
        this.angleInc = (this.angleTarget - gameObj.angle) / duration2;
        //
        //
        this.heightInc = (this.htarget - gameObj.displayHeight) / duration2;
        this.widthInc = (this.wtarget - gameObj.displayWidth) / duration2;
        //
        //
        this.scaleXInc = (this.scaleXTarget - gameObj.scaleX) / duration2;
        this.scaleYInc = (this.scaleYTarget - gameObj.scaleY) / duration2;
        //
        //
        this.skewXInc = (this.skewXTarget - gameObj.skewX) / duration2;
        this.skewYInc = (this.skewYTarget - gameObj.skewY) / duration2;
        if (autoStart == true) {
            this.start();
        }
    }
    SimpleTween.prototype.start = function () {
        this.myTimer = setInterval(this.doStep.bind(this), 5);
    };
    SimpleTween.prototype.doStep = function () {
        this.myTime -= 5;
        if (this.myTime < 0) {
            this.gameObj.x = this.xTarget;
            this.gameObj.y = this.yTarget;
            this.gameObj.alpha = this.alphaTarget;
            this.gameObj.angle = this.angleTarget;
            clearInterval(this.myTimer);
            this.tweenObj.onComplete(this.gameObj);
            return;
        }
        this.gameObj.x += this.xInc;
        this.gameObj.y += this.yInc;
        this.gameObj.alpha += this.alphaInc;
        this.gameObj.angle += this.angleInc;
        this.gameObj.displayHeight += this.heightInc;
        this.gameObj.displayWidth += this.widthInc;
        this.gameObj.scaleX += this.scaleXInc;
        this.gameObj.scaleY += this.scaleYInc;
        this.gameObj.skewX += this.skewXInc;
        this.gameObj.skewY += this.skewYInc;
    };
    return SimpleTween;
}());
exports.SimpleTween = SimpleTween;
//# sourceMappingURL=SimpleTween.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/classes/core/tweens/TweenObj.js":
/*!**********************************************************************!*\
  !*** ../../gen2/common/mods/svgGame/classes/core/tweens/TweenObj.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.TweenObj = void 0;
var TweenObj = /** @class */ (function () {
    function TweenObj() {
        this.x = null;
        this.y = null;
        this.alpha = null;
        this.angle = null;
        this.displayWidth = null;
        this.displayHeight = null;
        this.scaleX = null;
        this.scaleY = null;
        this.skewX = null;
        this.skewY = null;
        this.duration = 1000;
        this.onComplete = function () { };
    }
    return TweenObj;
}());
exports.TweenObj = TweenObj;
//# sourceMappingURL=TweenObj.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/index.js":
/*!***********************************************!*\
  !*** ../../gen2/common/mods/svgGame/index.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.FTextObj = exports.Math2 = exports.FontSizeVo = exports.PosVo = exports.CollideUtil = exports.SVGUtil = exports.AlignGrid = exports.SVGGame = exports.ScreenManager = exports.GameManager = exports.BaseScreen = exports.TweenObj = exports.SimpleTween = exports.TextObj = exports.SvgObj = exports.SVGAnimation = exports.GameOptions = void 0;
var gameOptions_1 = __webpack_require__(/*! ./classes/core/dataobjs/gameOptions */ "../../gen2/common/mods/svgGame/classes/core/dataobjs/gameOptions.js");
Object.defineProperty(exports, "GameOptions", ({ enumerable: true, get: function () { return gameOptions_1.GameOptions; } }));
var SvgAnimation_1 = __webpack_require__(/*! ./classes/core/animations/SvgAnimation */ "../../gen2/common/mods/svgGame/classes/core/animations/SvgAnimation.js");
Object.defineProperty(exports, "SVGAnimation", ({ enumerable: true, get: function () { return SvgAnimation_1.SVGAnimation; } }));
var SvgObj_1 = __webpack_require__(/*! ./classes/core/gameobjects/SvgObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/SvgObj.js");
Object.defineProperty(exports, "SvgObj", ({ enumerable: true, get: function () { return SvgObj_1.SvgObj; } }));
var TextObj_1 = __webpack_require__(/*! ./classes/core/gameobjects/TextObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/TextObj.js");
Object.defineProperty(exports, "TextObj", ({ enumerable: true, get: function () { return TextObj_1.TextObj; } }));
var SimpleTween_1 = __webpack_require__(/*! ./classes/core/tweens/SimpleTween */ "../../gen2/common/mods/svgGame/classes/core/tweens/SimpleTween.js");
Object.defineProperty(exports, "SimpleTween", ({ enumerable: true, get: function () { return SimpleTween_1.SimpleTween; } }));
var TweenObj_1 = __webpack_require__(/*! ./classes/core/tweens/TweenObj */ "../../gen2/common/mods/svgGame/classes/core/tweens/TweenObj.js");
Object.defineProperty(exports, "TweenObj", ({ enumerable: true, get: function () { return TweenObj_1.TweenObj; } }));
var BaseScreen_1 = __webpack_require__(/*! ./classes/core/BaseScreen */ "../../gen2/common/mods/svgGame/classes/core/BaseScreen.js");
Object.defineProperty(exports, "BaseScreen", ({ enumerable: true, get: function () { return BaseScreen_1.BaseScreen; } }));
var GameManager_1 = __webpack_require__(/*! ./classes/core/GameManager */ "../../gen2/common/mods/svgGame/classes/core/GameManager.js");
Object.defineProperty(exports, "GameManager", ({ enumerable: true, get: function () { return GameManager_1.GameManager; } }));
var ScreenManager_1 = __webpack_require__(/*! ./classes/core/ScreenManager */ "../../gen2/common/mods/svgGame/classes/core/ScreenManager.js");
Object.defineProperty(exports, "ScreenManager", ({ enumerable: true, get: function () { return ScreenManager_1.ScreenManager; } }));
var SvgGame_1 = __webpack_require__(/*! ./classes/core/SvgGame */ "../../gen2/common/mods/svgGame/classes/core/SvgGame.js");
Object.defineProperty(exports, "SVGGame", ({ enumerable: true, get: function () { return SvgGame_1.SVGGame; } }));
var AlignGridSvg_1 = __webpack_require__(/*! ./util/AlignGridSvg */ "../../gen2/common/mods/svgGame/util/AlignGridSvg.js");
Object.defineProperty(exports, "AlignGrid", ({ enumerable: true, get: function () { return AlignGridSvg_1.AlignGrid; } }));
var SvgUtil_1 = __webpack_require__(/*! ./util/SvgUtil */ "../../gen2/common/mods/svgGame/util/SvgUtil.js");
Object.defineProperty(exports, "SVGUtil", ({ enumerable: true, get: function () { return SvgUtil_1.SVGUtil; } }));
var collideUtil_1 = __webpack_require__(/*! ./util/collideUtil */ "../../gen2/common/mods/svgGame/util/collideUtil.js");
Object.defineProperty(exports, "CollideUtil", ({ enumerable: true, get: function () { return collideUtil_1.CollideUtil; } }));
var posVo_1 = __webpack_require__(/*! ./classes/core/dataobjs/posVo */ "../../gen2/common/mods/svgGame/classes/core/dataobjs/posVo.js");
Object.defineProperty(exports, "PosVo", ({ enumerable: true, get: function () { return posVo_1.PosVo; } }));
var fontSizeVo_1 = __webpack_require__(/*! ./classes/core/dataobjs/fontSizeVo */ "../../gen2/common/mods/svgGame/classes/core/dataobjs/fontSizeVo.js");
Object.defineProperty(exports, "FontSizeVo", ({ enumerable: true, get: function () { return fontSizeVo_1.FontSizeVo; } }));
var Math2_1 = __webpack_require__(/*! ./util/Math2 */ "../../gen2/common/mods/svgGame/util/Math2.js");
Object.defineProperty(exports, "Math2", ({ enumerable: true, get: function () { return Math2_1.Math2; } }));
var FTextObj_1 = __webpack_require__(/*! ./classes/core/gameobjects/FTextObj */ "../../gen2/common/mods/svgGame/classes/core/gameobjects/FTextObj.js");
Object.defineProperty(exports, "FTextObj", ({ enumerable: true, get: function () { return FTextObj_1.FTextObj; } }));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/util/AlignGridSvg.js":
/*!***********************************************************!*\
  !*** ../../gen2/common/mods/svgGame/util/AlignGridSvg.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AlignGrid = void 0;
var __1 = __webpack_require__(/*! .. */ "../../gen2/common/mods/svgGame/index.js");
var AlignGrid = /** @class */ (function () {
    function AlignGrid(rows, cols, gw, gh) {
        this.ch = 0;
        this.cw = 0;
        this.gw = 0;
        this.gh = 0;
        this.gridID = "gridLines";
        this.gw = gw;
        this.gh = gh;
        this.cw = Math.floor((gw / cols) * 1000) / 1000;
        this.ch = Math.floor((gh / rows) * 1000) / 1000;
    }
    AlignGrid.prototype.placeAt = function (col, row, obj) {
        var xx = this.cw * col;
        var yy = this.ch * row;
        obj.x = xx;
        obj.y = yy;
    };
    AlignGrid.prototype.showGrid = function () {
        //<path stroke-width="4" stroke="red" d="M0 0 L0 100 M100 0 L100 100 "></path>
        var p = "";
        for (var i = 0; i < this.gw; i += this.cw) {
            p += "M" + i.toString() + " 0 ";
            p += "L" + i.toString() + " " + this.gh.toString();
        }
        for (var i = 0; i < this.gh; i += this.ch) {
            p += "M0 " + i.toString() + " L" + this.gw + " " + i.toString();
        }
        // console.log(p);
        var p2 = '<path stroke-width="4" stroke="red" d="' + p + '"></path>';
        document.getElementById(this.gridID).innerHTML = p2;
    };
    AlignGrid.prototype.findNearestGridXY = function (xx, yy) {
        var row = Math.floor(yy / this.ch);
        var col = Math.floor(xx / this.cw);
        return new __1.PosVo(col, row);
    };
    AlignGrid.prototype.findNearestGridXYDec = function (xx, yy) {
        var row = yy / this.ch;
        var col = xx / this.cw;
        return new __1.PosVo(col, row);
    };
    return AlignGrid;
}());
exports.AlignGrid = AlignGrid;
//# sourceMappingURL=AlignGridSvg.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/util/Math2.js":
/*!****************************************************!*\
  !*** ../../gen2/common/mods/svgGame/util/Math2.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Math2 = void 0;
var Math2 = /** @class */ (function () {
    function Math2() {
    }
    Math2.between = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    return Math2;
}());
exports.Math2 = Math2;
//# sourceMappingURL=Math2.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/util/SvgUtil.js":
/*!******************************************************!*\
  !*** ../../gen2/common/mods/svgGame/util/SvgUtil.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SVGUtil = void 0;
var SVGUtil = /** @class */ (function () {
    function SVGUtil() {
    }
    SVGUtil.getAttNum = function (el, attName) {
        if (el) {
            var val = el.getAttribute(attName);
            if (val == null) {
                return 0;
            }
            if (isNaN(parseInt(val))) {
                return 0;
            }
            return parseInt(val);
        }
        return 0;
    };
    SVGUtil.trimNum = function (num) {
        return Math.round(Math.floor(num * 1000) / 1000);
    };
    SVGUtil.findTop = function (el, tagName) {
        var obj = el;
        while (obj.tagName != tagName) {
            obj = obj.parentElement;
            if (obj == null || obj == undefined) {
                break;
            }
        }
        return obj;
    };
    return SVGUtil;
}());
exports.SVGUtil = SVGUtil;
//# sourceMappingURL=SvgUtil.js.map

/***/ }),

/***/ "../../gen2/common/mods/svgGame/util/collideUtil.js":
/*!**********************************************************!*\
  !*** ../../gen2/common/mods/svgGame/util/collideUtil.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CollideUtil = void 0;
var CollideUtil = /** @class */ (function () {
    function CollideUtil() {
    }
    CollideUtil.checkOverlap = function (obj1, obj2) {
        var r1 = obj1.getBoundingClientRect(); //BOUNDING BOX OF THE FIRST OBJECT
        var r2 = obj2.getBoundingClientRect(); //BOUNDING BOX OF THE SECOND OBJECT
        //CHECK IF THE TWO BOUNDING BOXES OVERLAP
        return !(r2.left > r1.right ||
            r2.right < r1.left ||
            r2.top > r1.bottom ||
            r2.bottom < r1.top);
    };
    return CollideUtil;
}());
exports.CollideUtil = CollideUtil;
//# sourceMappingURL=collideUtil.js.map

/***/ }),

/***/ "./src/screens/ScreenMain.ts":
/*!***********************************!*\
  !*** ./src/screens/ScreenMain.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ScreenMain": () => (/* binding */ ScreenMain)
/* harmony export */ });
/* harmony import */ var svggame__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! svggame */ "../../gen2/common/mods/svgGame/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var ScreenMain = /** @class */ (function (_super) {
    __extends(ScreenMain, _super);
    function ScreenMain() {
        return _super.call(this, "ScreenMain") || this;
    }
    ScreenMain.prototype.create = function () {
        var _a;
        window.scene = this;
        (_a = this.grid) === null || _a === void 0 ? void 0 : _a.showGrid();
        this.gm.regFontSize("main", 20, 1180);
        this.ship = new svggame__WEBPACK_IMPORTED_MODULE_0__.SvgObj(this, "ship");
        this.ship.gameWRatio = 0.3;
        this.text1 = new svggame__WEBPACK_IMPORTED_MODULE_0__.TextObj(this, "text1");
        this.text1.setText("The History Of Pirates");
        this.text1.setFontSize(50);
        /*  if (this.text1.el) {
             this.text1.displayWidth = this.text1.el?.getBoundingClientRect().width;
             this.text1.displayHeight = this.text1.el?.getBoundingClientRect().height;
         } */
        this.mainText = new svggame__WEBPACK_IMPORTED_MODULE_0__.FTextObj(this, "text3");
        this.mainText.setText("Pirates have existed since ancient times –they threatened the trading routes of ancient Greece, and seized cargoes of grain and olive oil from Roman ships. The most far-reaching pirates in early medieval Europe were the Vikings.");
        //this.mainText.gameWRatio=0.5;
        //this.grid?.placeAt(5,5,this.ship);
        //  this.ship.adjust();
    };
    ScreenMain.prototype.doResize = function () {
        var _a, _b, _c;
        _super.prototype.doResize.call(this);
        (_a = this.grid) === null || _a === void 0 ? void 0 : _a.showGrid();
        this.ship.updateScale();
        (_b = this.grid) === null || _b === void 0 ? void 0 : _b.placeAt(5, 2, this.ship);
        this.centerW(this.ship, true);
        //  this.placeOnGrid(4,2,this.text1);
        this.center(this.text1, true);
        this.text1.y = this.gh * 0.1;
        (_c = this.grid) === null || _c === void 0 ? void 0 : _c.placeAt(5.5, 9, this.mainText);
        this.mainText.setFontSize(this.gm.getFontSize("main", this.gw));
        //this.mainText.updateScale();
        this.mainText.adjust();
    };
    return ScreenMain;
}(svggame__WEBPACK_IMPORTED_MODULE_0__.BaseScreen));



/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var svggame__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! svggame */ "../../gen2/common/mods/svgGame/index.js");
/* harmony import */ var _screens_ScreenMain__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./screens/ScreenMain */ "./src/screens/ScreenMain.ts");


window.onload = function () {
    var opts = new svggame__WEBPACK_IMPORTED_MODULE_0__.GameOptions();
    opts.useFull = true;
    opts.addAreaID = "addArea";
    opts.screens = [new _screens_ScreenMain__WEBPACK_IMPORTED_MODULE_1__.ScreenMain()];
    var game = new svggame__WEBPACK_IMPORTED_MODULE_0__.SVGGame("myCanvas", opts);
};

})();

/******/ })()
;
//# sourceMappingURL=bundle.js.map