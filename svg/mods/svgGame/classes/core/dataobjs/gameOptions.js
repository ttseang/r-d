"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GameOptions = void 0;
var GameOptions = /** @class */ (function () {
    function GameOptions() {
        this.screens = [];
        this.width = 0;
        this.height = 0;
        this.addAreaID = "main";
    }
    return GameOptions;
}());
exports.GameOptions = GameOptions;
//# sourceMappingURL=gameOptions.js.map