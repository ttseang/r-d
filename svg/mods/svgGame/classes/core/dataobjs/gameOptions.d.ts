import { IScreen } from "../interfaces/IScreen";
export declare class GameOptions {
    screens: IScreen[];
    width: number;
    height: number;
    addAreaID: string;
}
