import { GameOptions } from "./dataobjs/gameOptions";
export declare class GameManager {
    private static instance;
    gameOptions: GameOptions;
    constructor();
    static getInstance(): GameManager;
}
