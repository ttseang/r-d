"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseScreen = void 0;
var AlignGridSvg_1 = require("../../util/AlignGridSvg");
var GameManager_1 = require("./GameManager");
var SvgObj_1 = require("./gameobjects/SvgObj");
var TextObj_1 = require("./gameobjects/TextObj");
var ScreenManager_1 = require("./ScreenManager");
var SimpleTween_1 = require("./tweens/SimpleTween");
var TweenObj_1 = require("./tweens/TweenObj");
var BaseScreen = /** @class */ (function () {
    function BaseScreen(key) {
        this.gm = GameManager_1.GameManager.getInstance();
        this.screenManager = ScreenManager_1.ScreenManager.getInstance();
        this.children = [];
        this.key = key;
        console.log(this.gm.gameOptions);
        this.grid = new AlignGridSvg_1.AlignGrid(10, 10, this.gm.gameOptions.width, this.gm.gameOptions.height);
    }
    BaseScreen.prototype.start = function () {
        this.grid = new AlignGridSvg_1.AlignGrid(10, 10, this.gm.gameOptions.width, this.gm.gameOptions.height);
        this.create();
    };
    BaseScreen.prototype.create = function () {
    };
    BaseScreen.prototype.destroy = function () {
        var _a;
        while (this.children.length > 0) {
            (_a = this.children.pop()) === null || _a === void 0 ? void 0 : _a.destroy();
        }
    };
    BaseScreen.prototype.addExisting = function (obj) {
        this.children.push(obj);
    };
    BaseScreen.prototype.addImage = function (key) {
        var svgObj = new SvgObj_1.SvgObj(this, key);
        this.addExisting(svgObj);
        return svgObj;
    };
    BaseScreen.prototype.addImageAt = function (key, x, y) {
        var svgObj = this.addImage(key);
        svgObj.x = x;
        svgObj.y = y;
        return svgObj;
    };
    BaseScreen.prototype.addText = function (text, textKey) {
        if (textKey === void 0) { textKey = "defaultText"; }
        var textObj = new TextObj_1.TextObj(this, textKey);
        textObj.setText(text);
        this.addExisting(textObj);
        return textObj;
    };
    BaseScreen.prototype.addTextAt = function (text, x, y, textKey) {
        if (textKey === void 0) { textKey = "defaultText"; }
        var textObj = this.addText(textKey, text);
        textObj.x = x;
        textObj.y = y;
        return textObj;
    };
    BaseScreen.prototype.centerW = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        obj.x = this.gw / 2;
        if (adjust == true) {
            obj.x -= obj.displayWidth / 2;
        }
    };
    BaseScreen.prototype.centerH = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        obj.y = this.gh / 2;
        if (adjust) {
            obj.y -= obj.displayHeight / 2;
        }
    };
    BaseScreen.prototype.placeOnGrid = function (col, row, obj) {
        var xx = this.grid.ch * col;
        var yy = this.grid.cw * row;
        obj.x = xx;
        obj.y = yy;
    };
    BaseScreen.prototype.tweenGridPos = function (col, row, obj, duration, callback) {
        if (callback === void 0) { callback = function () { }; }
        var tweenObj = new TweenObj_1.TweenObj();
        var xx = this.grid.ch * col;
        var yy = this.grid.cw * row;
        tweenObj.x = xx;
        tweenObj.y = yy;
        tweenObj.onComplete = callback;
        var tween = new SimpleTween_1.SimpleTween(obj, tweenObj);
    };
    BaseScreen.prototype.center = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        this.centerW(obj, adjust);
        this.centerH(obj, adjust);
    };
    Object.defineProperty(BaseScreen.prototype, "gh", {
        get: function () {
            return this.gm.gameOptions.height;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BaseScreen.prototype, "gw", {
        get: function () {
            return this.gm.gameOptions.width;
        },
        enumerable: false,
        configurable: true
    });
    BaseScreen.prototype.changeScreen = function (nScreen) {
        console.log("change to " + nScreen);
        this.screenManager.changeScreen(nScreen);
    };
    return BaseScreen;
}());
exports.BaseScreen = BaseScreen;
//# sourceMappingURL=BaseScreen.js.map