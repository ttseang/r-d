"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GameManager = void 0;
var gameOptions_1 = require("./dataobjs/gameOptions");
var GameManager = /** @class */ (function () {
    function GameManager() {
        this.gameOptions = new gameOptions_1.GameOptions();
        window.gm = this;
    }
    GameManager.getInstance = function () {
        if (this.instance == null) {
            this.instance = new GameManager();
        }
        return this.instance;
    };
    return GameManager;
}());
exports.GameManager = GameManager;
//# sourceMappingURL=GameManager.js.map