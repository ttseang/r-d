import { IScreen } from "./interfaces/IScreen";
export declare class ScreenManager {
    private static instance;
    screens: IScreen[];
    currentScreen: IScreen | null;
    constructor();
    static getInstance(): ScreenManager;
    startScreen(screen: IScreen): void;
    private findScreen;
    changeScreen(screenName: string): void;
    init(): void;
}
