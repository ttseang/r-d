export declare class TweenObj {
    x: number | null;
    y: number | null;
    alpha: number | null;
    angle: number | null;
    displayWidth: number | null;
    displayHeight: number | null;
    scaleX: number | null;
    scaleY: number | null;
    skewX: number | null;
    skewY: number | null;
    duration: number;
    onComplete: Function;
    constructor();
}
