import { IScreen } from "../interfaces/IScreen";
import { SvgObj } from "./SvgObj";
export declare class TextObj extends SvgObj {
    constructor(screen: IScreen, key: string);
    static createNew(): void;
    setText(text: string): void;
}
