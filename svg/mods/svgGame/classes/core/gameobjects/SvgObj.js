"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SvgObj = void 0;
var GameManager_1 = require("../GameManager");
var SvgObj = /** @class */ (function () {
    function SvgObj(screen, id) {
        var _a;
        this.el = null;
        this.instanceName = "";
        this._x = 0;
        this._y = 0;
        this._angle = 0;
        this._visible = true;
        this._width = 0;
        this._height = 0;
        this._displayWidth = 0;
        this._displayHeight = 0;
        this._scaleX = 1;
        this._scaleY = 1;
        this._skewX = 0;
        this._skewY = 0;
        this.gm = GameManager_1.GameManager.getInstance();
        this._alpha = 1;
        this.id = id;
        this.screen = screen;
        var def = document.getElementById(id);
        if (def) {
            this.el = def.cloneNode(true);
            this.el.id = SvgObj.getInstanceName(id);
            if (this.el) {
                this._x = this.getAttNum("x");
                this._y = this.getAttNum("y");
                var addArea = "playArea";
                if (this.gm.gameOptions) {
                    addArea = this.gm.gameOptions.addAreaID;
                }
                (_a = document.getElementById(addArea)) === null || _a === void 0 ? void 0 : _a.appendChild(this.el);
                var bounds = this.el.getBoundingClientRect();
                this._height = bounds.height;
                this._width = bounds.width;
                this._displayHeight = this._height;
                this._displayWidth = this._width;
            }
        }
        screen.addExisting(this);
    }
    SvgObj.prototype.onClick = function (callback) {
        if (this.el) {
            this.el.onclick = function () { callback(); };
        }
    };
    SvgObj.getInstanceName = function (key) {
        SvgObj.count++;
        return key + "-" + SvgObj.count.toString();
    };
    SvgObj.prototype.incRot = function (rot) {
        this.angle = this.angle + rot;
    };
    Object.defineProperty(SvgObj.prototype, "angle", {
        get: function () {
            return this._angle;
        },
        set: function (value) {
            while (value > 360) {
                value -= 360;
            }
            this._angle = value;
            if (this.el) {
                var w2 = this.displayWidth / 2;
                var h2 = this.displayHeight / 2;
                w2 += this.x;
                h2 += this.y;
                this.updateTransform();
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "width", {
        get: function () {
            return this._width;
        },
        set: function (value) {
            this._width = value;
            /* if (this.el) {
                if (this.useC == false) {
                    this.el.setAttribute("width", value.toString());
                }
                else {
                    this.el.setAttribute("r", value.toString());
                }
            } */
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "height", {
        get: function () {
            return this._height;
        },
        set: function (value) {
            this._height = value;
            /* if (this.el) {
                if (this.useC == false) {
                    this.el.setAttribute("height", value.toString());
                }
                else {
                    this.el.setAttribute("r", value.toString());
                }
            } */
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "displayWidth", {
        get: function () {
            return this._displayWidth;
        },
        set: function (value) {
            this._displayWidth = value;
            /*  if (this.el) {
                 if (this.useC == false) {
                     this.el.setAttribute("width", value.toString());
                 }
                 else {
                     this.el.setAttribute("r", value.toString());
                 }
             } */
            this._scaleX = value / this.width;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "displayHeight", {
        get: function () {
            return this._displayHeight;
        },
        set: function (value) {
            this._displayHeight = value;
            /*    if (this.el) {
                   if (this.useC == false) {
                       this.el.setAttribute("height", value.toString());
                   }
                   else {
                       this.el.setAttribute("r", value.toString());
                   }
               } */
            this._scaleY = value / this._height;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "scaleX", {
        get: function () {
            return this._scaleX;
        },
        set: function (value) {
            this._scaleX = value;
            this._displayWidth = this.width * value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "scaleY", {
        get: function () {
            return this._scaleY;
        },
        set: function (value) {
            this._scaleY = value;
            this._displayHeight = this.height * value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    SvgObj.prototype.setScale = function (scale) {
        this._scaleX = scale;
        this._scaleY = scale;
        this.updateTransform();
    };
    Object.defineProperty(SvgObj.prototype, "skewX", {
        get: function () {
            return this._skewX;
        },
        set: function (value) {
            this._skewX = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "skewY", {
        get: function () {
            return this._skewY;
        },
        set: function (value) {
            this._skewY = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "alpha", {
        get: function () {
            return this._alpha;
        },
        set: function (value) {
            this._alpha = value;
            if (this.el) {
                this.el.setAttribute("opacity", value.toString());
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (value) {
            this._visible = value;
            if (this.el) {
                this.el.style.display = (value == true) ? "block" : "none";
            }
        },
        enumerable: false,
        configurable: true
    });
    SvgObj.prototype.destroy = function () {
        if (this.el) {
            this.el.remove();
        }
    };
    SvgObj.prototype.getAttNum = function (attName) {
        if (this.el) {
            var val = this.el.getAttribute(attName);
            if (val == null) {
                return 0;
            }
            if (isNaN(parseInt(val))) {
                return 0;
            }
            return parseInt(val);
        }
        return 0;
    };
    Object.defineProperty(SvgObj.prototype, "y", {
        get: function () {
            return this._y;
        },
        set: function (value) {
            this._y = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SvgObj.prototype, "x", {
        get: function () {
            return this._x;
        },
        set: function (value) {
            this._x = value;
            this.updateTransform();
        },
        enumerable: false,
        configurable: true
    });
    SvgObj.prototype.updateTransform = function () {
        var w2 = this.displayWidth / 2;
        var h2 = this.displayHeight / 2;
        w2 += this.x;
        h2 += this.y;
        var rotString = "rotate(" + this.angle.toString() + "," + w2.toString() + "," + h2.toString() + ")";
        var posString = "translate(" + this.x.toString() + "," + this.y.toString() + ")";
        var scaleString = "scale(" + this._scaleX.toString() + "," + this._scaleY.toString() + ")";
        var skewString = "skewX(" + this._skewX.toString() + ") skewY(" + this._skewY.toString() + ")";
        var transString = rotString + " " + posString + " " + scaleString + " " + skewString;
        if (this.el) {
            this.el.setAttribute("transform", transString);
        }
    };
    SvgObj.prototype.incX = function (val) {
        this.x = this._x + val;
    };
    SvgObj.prototype.incY = function (val) {
        this.y = this._y + val;
    };
    SvgObj.prototype.getBoundingClientRect = function () {
        if (this.el) {
            return this.el.getBoundingClientRect();
        }
        return new DOMRect(0, 0, 100, 100);
    };
    SvgObj.count = 0;
    return SvgObj;
}());
exports.SvgObj = SvgObj;
//# sourceMappingURL=SvgObj.js.map