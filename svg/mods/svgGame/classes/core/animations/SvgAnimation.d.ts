export declare class SVGAnimation {
    id: string;
    private el;
    constructor(id: string);
    onComplete(callback: Function): void;
    start(): void;
    stop(): void;
}
