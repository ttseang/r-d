"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SVGGame = void 0;
var SvgUtil_1 = require("../../util/SvgUtil");
var gameOptions_1 = require("./dataobjs/gameOptions");
var GameManager_1 = require("./GameManager");
var ScreenManager_1 = require("./ScreenManager");
var SVGGame = /** @class */ (function () {
    function SVGGame(gameID, gameOptions) {
        this.gameEl = null;
        this.screenManager = ScreenManager_1.ScreenManager.getInstance();
        this.gameManager = GameManager_1.GameManager.getInstance();
        var width = gameOptions.width;
        var height = gameOptions.height;
        this.gameEl = document.getElementById(gameID);
        if (this.gameEl == null) {
            if (width == 0 || height == 0) {
                throw new Error("Height and Width must be set if creating dynamic svg canvas");
                return;
            }
            this.gameEl = document.createElement("svg");
            this.gameEl.id = gameID;
            this.gameEl.setAttribute("width", width.toString());
            this.gameEl.setAttribute("height", height.toString());
            document.body.appendChild(this.gameEl);
            this.gw = width;
            this.gh = height;
        }
        else {
            this.gw = SvgUtil_1.SVGUtil.getAttNum(this.gameEl, "width");
            this.gh = SvgUtil_1.SVGUtil.getAttNum(this.gameEl, "height");
        }
        if (gameOptions.screens.length > 0) {
            this.screenManager.screens = gameOptions.screens;
        }
        else {
            gameOptions = new gameOptions_1.GameOptions();
        }
        if (gameOptions.addAreaID == "") {
            gameOptions.addAreaID = gameID;
        }
        gameOptions.width = this.gw;
        gameOptions.height = this.gh;
        this.gameManager.gameOptions = gameOptions;
        this.screenManager.init();
    }
    return SVGGame;
}());
exports.SVGGame = SVGGame;
//# sourceMappingURL=SvgGame.js.map