export interface IGameObj {
    x: number;
    y: number;
    displayWidth: number;
    displayHeight: number;
    visible: boolean;
    alpha: number;
    angle: number;
    scaleX: number;
    scaleY: number;
    skewX: number;
    skewY: number;
    destroy(): void;
}
