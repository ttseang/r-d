import { IGameObj } from "./IGameObj";
export interface IScreen {
    start(): void;
    destroy(): void;
    key: string;
    addExisting(obj: IGameObj): void;
}
