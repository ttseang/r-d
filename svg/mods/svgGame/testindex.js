"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var gameOptions_1 = require("./classes/core/dataobjs/gameOptions");
var SvgGame_1 = require("./classes/core/SvgGame");
var ScreenMain_1 = require("./screens/ScreenMain");
//require("./template.html");
window.onload = function () {
    var options = new gameOptions_1.GameOptions();
    options.screens = [new ScreenMain_1.ScreenMain()];
    options.addAreaID = "addHere";
    var game = new SvgGame_1.SVGGame("myCanvas", options);
};
//# sourceMappingURL=testindex.js.map