export declare class AlignGrid {
    ch: number;
    cw: number;
    constructor(rows: number, cols: number, gw: number, gh: number);
}
