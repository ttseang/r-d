"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlignGrid = void 0;
var AlignGrid = /** @class */ (function () {
    function AlignGrid(rows, cols, gw, gh) {
        this.ch = 0;
        this.cw = 0;
        this.ch = gw / cols;
        this.cw = gh / rows;
    }
    return AlignGrid;
}());
exports.AlignGrid = AlignGrid;
//# sourceMappingURL=AlignGridSvg.js.map