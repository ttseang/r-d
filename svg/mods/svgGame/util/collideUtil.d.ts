import { SvgObj } from "../classes/core/gameobjects/SvgObj";
export declare class CollideUtil {
    static checkOverlap(obj1: SvgObj, obj2: SvgObj): boolean;
}
