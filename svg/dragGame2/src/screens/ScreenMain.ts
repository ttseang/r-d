import { BaseScreen, IScreen, SvgObj } from "svggame";

export class ScreenMain extends BaseScreen implements IScreen
{
    private box!:SvgObj;

    constructor()
    {
        super("ScreenMain");
    }
    create()
    {
        (window as any).scene=this;

        this.box=new SvgObj(this,'box');
        this.box.gameWRatio=0.1;
        
        this.grid?.showGrid();
        this.placeOnGrid(2,2,this.box,true);
        
    }
}