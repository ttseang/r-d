
import { BaseScreen, GameManager, IScreen, SvgObj, TextObj } from "svggame";
import { DragEng } from "dragengsvg";
import { IconButton } from "../classes/IconButton";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;
    private dragEng: DragEng;

    private uiWindow!: SvgObj;
    private btnCheck!: SvgObj;
    private btnPlay!: SvgObj;
    private btnReset!: SvgObj;
    private btnHelp!: SvgObj;

    private buttons: SvgObj[] = [];
    private instructionText: TextObj | null = null;
   
    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions",26,1000);

        this.dragEng = new DragEng(this);

        
    }
    create() {
        super.create();
        (window as any).scene = this;

        this.uiWindow = new SvgObj(this, "uiWindow");
        this.uiWindow.gameWRatio = .9;

        
        //
        //
        //
        this.btnPlay = new IconButton(this, "btnPlay", "play", "", this.onbuttonPressed.bind(this));
        this.btnReset = new IconButton(this, "btnReset", "reset", "", this.onbuttonPressed.bind(this));
        this.btnCheck = new IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
        this.btnHelp = new IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));

        this.buttons = [this.btnHelp, this.btnPlay, this.btnReset, this.btnCheck];

        /**
         * SET CALLBACK ON THE DRAG ENGINE
         */

        this.dragEng.upCallback = () => { };
        this.dragEng.dropCallback = () => { };
        this.dragEng.collideCallback = () => { };


        this.instructionText = new TextObj(this, "instText");
        
    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
    }
    doResize() {
        super.doResize();


        this.grid?.showGrid();

        //console.log(this.gw,this.gh);
        // this.uiWindow.updateScale();
        if (this.instructionText) {
            this.instructionText.setFontSize(this.gm.getFontSize("instructions",this.gm.gw));
            this.grid?.placeAt(5, 2, this.instructionText);
            this.centerW(this.instructionText, true);
        }

        this.uiWindow.displayWidth = this.gw * 0.9;
        this.uiWindow.displayHeight = this.gh * 0.70;

        this.center(this.uiWindow, true);

        for (let i: number = 0; i < this.buttons.length; i++) {
            this.buttons[i].gameWRatio = 0.05;
            this.grid?.placeAt(2 + i * 2, 9.7, this.buttons[i]);
        }

        this.dragEng.doResize();
    }
}