export class ArrowSpotVo
{
    public x:number;
    public y:number;
    public rot:number;

    constructor(x:number,y:number,rot:number)
    {
        this.x=x;
        this.y=y;
        this.rot=rot;
    }
}