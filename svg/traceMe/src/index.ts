import { GameOptions, SVGGame } from "svggame"
import { SceneTest } from "./screens/SceneTest";


window.onload=()=>{
    
   

    let opts:GameOptions=new GameOptions();
    opts.useFull=true;
    opts.addAreaID="addArea";
    opts.screens=[new SceneTest()]

    let game:SVGGame=new SVGGame("myCanvas",opts);
}
