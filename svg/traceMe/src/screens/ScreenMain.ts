import { AlignGrid, BaseScreen, IScreen, PosVo, SvgObj } from "svggame";
import { Spot } from "../classes/Spot";

export class ScreenMain extends BaseScreen implements IScreen {
    private letter!: SvgObj;
    private statLetter!: SvgObj;

    //private myMask!:SvgObj;
    //private ball!:SvgObj;

    private spots: PosVo[] = [];
    private maskSpots: Spot[] = [];

    private isDown: boolean = false;

    private letterGrid!: AlignGrid;

    public spotCount:number=0;

    constructor() {
        super("ScreenMain");
    }
    create() {
        (window as any).scene = this;

        //  this.myMask=new SvgObj(this,'mask1');

        this.statLetter = new SvgObj(this, 's2');
        this.statLetter.gameWRatio = 0.2;
        //this.statLetter.alpha=.2;
        //  this.statLetter.visible=false;

        this.letter = new SvgObj(this, 's');
        this.letter.gameWRatio = 0.2;

        this.letterGrid = new AlignGrid(10, 10, this.letter.displayWidth, this.letter.displayHeight);
        this.letterGrid.showGrid();


      //  this.addSpot(0,0);
        this.addSpot(1,0);
        this.addSpot(2,0);
        this.addSpot(3,0);
        this.addSpot(4,0);
        this.addSpot(5,0);
        /* this.addSpot(7.6, .2);
        this.addSpot(6, 0);
        this.addSpot(5, 0);
        this.addSpot(4, 0);
        this.addSpot(3, .1);
        this.addSpot(1, .3);
        
        this.addSpot(0.3, 1);
         this.addSpot(0.3, 2.4);
        this.addSpot(0.8, 3);
        this.addSpot(1.9, 3.5); */
      //  this.addSpot(2.2, 2.5);
        // let maskEl:HTMLElement | null=document.getElementById("test1");

        let spotSize:number=0.05;

        for (let i: number = 0; i < this.spots.length; i++) {
            let spot: Spot = new Spot(this);
            spot.index = i;
            spot.displayHeight=this.letterGrid.ch*1.2;
            spot.displayWidth=this.letterGrid.cw*1.01;
           // spot.gameWRatio = spotSize;
            this.letterGrid.placeAt(this.spots[i].x, this.spots[i].y, spot);
            //spot.adjust();
            //spot.visible=false;
            spot.alpha = .3;
           spot.setCallback(this.spotOver.bind(this));

             let mspot: Spot = new Spot(this);
             mspot.displayHeight=this.letterGrid.ch;
             mspot.displayWidth=this.letterGrid.cw;
         //   mspot.gameWRatio = spotSize;
            mspot.x = spot.x;
            mspot.y = spot.y;
            mspot.index = i;

            mspot.addToMask("test1");

            this.maskSpots.push(mspot);
           mspot.visible = false; 
        }

        //  this.grid?.showGrid();
        // this.placeOnGrid(2,2,this.letter,true);
        if (this.letter) {
            if (this.letter.el) {
                this.letter.el.setAttribute("clip-path", "url(#test1)");
            }
        }

        window.onpointerup = () => {
            this.isDown = false;
        }
        window.onpointerdown = () => {
            this.isDown = true;
        }
    }

    spotOver(spot: Spot) {
        // spot.alpha=1;

        if (this.isDown == false) {
            return;
        }
        console.log(spot.index);

        if (spot.index > 0) {
            if (this.maskSpots[spot.index - 1].visible == false) {
                return;
            }
        }

        let mspot: Spot = this.maskSpots[spot.index];
        if (mspot.visible==false)
        {
            this.spotCount++;
            mspot.visible = true;
        }
       
    }

    addSpot(x: number, y: number) {
        this.spots.push(new PosVo(x, y));
    }
    doResize() {
        super.doResize();
        // this.grid?.showGrid();
        this.letter.updateScale();
        //this.center(this.letter,true);
    }
}