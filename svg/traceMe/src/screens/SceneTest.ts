import { AlignGrid, BaseScreen, IScreen, PosVo, SvgObj } from "svggame";
import { Spot } from "../classes/Spot";
import { ArrowSpotVo } from "../dataObjs/ArrowSpotVo";
import { SpotVo } from "../dataObjs/SpotVo";

export class SceneTest extends BaseScreen implements IScreen {

    private statLetter!: SvgObj;
    private letter!: SvgObj;

    private letterGrid!: AlignGrid;
    private arrow!:SvgObj;

    private gridSpots: SpotVo[] = [];
    private arrowSpots:ArrowSpotVo[]=[];

    private arrowPlaces:Spot[]=[];
    private spotTriggers:Spot[]=[];

    private maskSpots: Spot[] = [];
    private isDown:boolean=false;
    public spotCount:number=0;
    private arrowIndex:number=0;

    constructor() {
        super("SceneTest");
    }
    create() {
        (window as any).scene = this;

        //  this.myMask=new SvgObj(this,'mask1');

        this.statLetter = new SvgObj(this, 's2');
        this.statLetter.gameWRatio = 0.2;

        this.letter = new SvgObj(this, 's');
        this.letter.gameWRatio = 0.2;


        this.arrowSpots.push(new ArrowSpotVo(8,.5,-150));
        this.arrowSpots.push(new ArrowSpotVo(5,0,180));
        this.arrowSpots.push(new ArrowSpotVo(1.25,1,90));
        this.arrowSpots.push(new ArrowSpotVo(1.25,2,90));
        this.arrowSpots.push(new ArrowSpotVo(1.25,3,45));
        this.arrowSpots.push(new ArrowSpotVo(2.5,3.5,45));
        this.arrowSpots.push(new ArrowSpotVo(4,4.25,45));

        this.arrowSpots.push(new ArrowSpotVo(5.5,4.5,45));
        this.arrowSpots.push(new ArrowSpotVo(7,5,45));
        this.arrowSpots.push(new ArrowSpotVo(8,7,90));
        this.arrowSpots.push(new ArrowSpotVo(7,8.5,150));
        this.arrowSpots.push(new ArrowSpotVo(5,9,180));
        this.arrowSpots.push(new ArrowSpotVo(2,9,180));

        this.addSpot(7, 0, 3, 1.5);
        this.addSpot(4, 0, 3, 1.5);
        this.addSpot(0, 0, 4, 1.5);
        this.addSpot(0, 1.5, 3, 1.5);
        this.addSpot(0, 3, 3, 1.5);
        this.addSpot(2, 3, 2, 2);
        this.addSpot(3, 3.5, 2, 2);
        this.addSpot(5, 4, 2, 2);
        this.addSpot(6, 4.5, 4, 2);
        this.addSpot(6, 6, 4, 2);
        this.addSpot(5, 8, 4, 2);
        
        this.addSpot(3, 8, 2, 2);
        this.addSpot(0, 7, 3, 3);

        this.letterGrid = new AlignGrid(10, 10, this.statLetter.displayWidth, this.statLetter.displayHeight);
        this.letterGrid.showGrid();

        for (let i:number=0;i<this.arrowSpots.length;i++)
        {
            let aSpot:Spot=new Spot(this);
            aSpot.visible=false;
            aSpot.displayWidth=this.letterGrid.cw;
            aSpot.displayHeight=this.letterGrid.ch;
            this.arrowPlaces.push(aSpot);
            this.letterGrid.placeAt(this.arrowSpots[i].x,this.arrowSpots[i].y,aSpot);
        }


        for (let i: number = 0; i < this.gridSpots.length; i++) {
            let spot: Spot = new Spot(this);
            let spotVo: SpotVo = this.gridSpots[i];

           // spot.alpha = .1;
            spot.displayWidth = this.letterGrid.cw * spotVo.w;
            spot.displayHeight = this.letterGrid.ch * spotVo.h;
            spot.index=i;
            this.spotTriggers.push(spot);
            this.letterGrid.placeAt(spotVo.x, spotVo.y, spot);



            spot.alpha = .1;
           spot.setCallback(this.spotOver.bind(this));


            let mspot: Spot = new Spot(this);
            mspot.displayHeight = spot.displayHeight;
            mspot.displayWidth = spot.displayWidth;
            //   mspot.gameWRatio = spotSize;
            mspot.x = spot.x;
            mspot.y = spot.y;
            mspot.index = i;

            mspot.addToMask("test1");

            this.maskSpots.push(mspot);
            mspot.visible = false;
        }

        if (this.letter) {
            if (this.letter.el) {
                this.letter.el.setAttribute("clip-path", "url(#test1)");
            }
        }

        window.onpointerup = () => {
            this.isDown = false;
        }
        window.onpointerdown = () => {
            this.isDown = true;
        }

        this.arrow=new SvgObj(this,"arrow");
        this.arrow.gameWRatio=0.03;

        this.setArrow(0);
    }

    private addSpot(x: number, y: number, w: number = 1, h: number = 1) {
        this.gridSpots.push(new SpotVo(x, y, w, h));
    }
    spotOver(spot: Spot) {
        // spot.alpha=1;

        if (this.isDown == false) {
            return;
        }
        console.log(spot.index);
        
        if (spot.index > 0) {
            if (this.maskSpots[spot.index - 1].visible == false) {
                return;
            }
        }

        let mspot: Spot = this.maskSpots[spot.index];
        if (mspot.visible==false)
        {
            this.spotCount++;
           this.setArrow(spot.index);

           this.arrowIndex=spot.index;
            mspot.visible = true;
        }
       
    }
    setArrow(index:number)
    {
        let arrowVo:ArrowSpotVo=this.arrowSpots[index];
        this.letterGrid.placeAt(arrowVo.x,arrowVo.y,this.arrow);
        this.arrow.angle=arrowVo.rot;
    }
    doResize() {
        super.doResize();
        // this.grid?.showGrid();
        this.statLetter.updateScale();
        this.letter.updateScale();
       // this.letterGrid = new AlignGrid(10, 10, this.statLetter.displayWidth, this.statLetter.displayHeight);
        this.letterGrid.showGrid();

       /*  for (let i: number = 0; i < this.gridSpots.length; i++) {
            let spot: Spot = this.spotTriggers[i];
            let spotVo: SpotVo = this.gridSpots[i];

           // spot.alpha = 1;
            spot.displayWidth = this.letterGrid.cw * spotVo.w;
            spot.displayHeight = this.letterGrid.ch * spotVo.h;
          
          
            this.letterGrid.placeAt(spotVo.x, spotVo.y, spot);

            let mspot: Spot =this.maskSpots[i];
          //  mspot.displayHeight = spot.displayHeight;
         //   mspot.displayWidth = spot.displayWidth;
            //   mspot.gameWRatio = spotSize;
            mspot.x = spot.x;
            mspot.y = spot.y;
           // mspot.visible=true;

             let aspot:Spot=this.arrowPlaces[i];
            aspot.displayHeight=this.letterGrid.ch;
            aspot.displayWidth=this.letterGrid.cw;
            aspot.visible=true; 

           // this.letterGrid.placeAt(this.gridSpots[i].x,this.gridSpots[i].y,aspot);
        } */

        this.setArrow(this.arrowIndex);
      //  this.place
        //this.center(this.letter,true);
    }
}