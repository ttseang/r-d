import { IScreen, SvgObj } from "svggame";

export class Spot extends SvgObj
{
    public index:number=0;
    private overCallback:Function=()=>{};

    constructor(scene:IScreen) {
        super(scene,"spot");
    }
    setCallback(callback:Function)
    {
        this.overCallback=callback;

        if (this.el)
        {
            this.el.onpointerover=()=>{
                this.overCallback(this);
            }
        }
    }
    addToMask(id:string)
    {
        let maskEl:HTMLElement | null=document.getElementById(id);
        if (maskEl)
        {
            if (this.el)
            {
               maskEl.append(this.el);
            }
           
        }   
    }
}