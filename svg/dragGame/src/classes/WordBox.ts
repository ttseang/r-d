import { IScreen, SimpleTween, SvgObj } from "svggame";


export class WordBox extends SvgObj
{
    private textEl:SVGTextElement |null=null;

    public ox:number=0;
    public oy:number=0;
    private myScreen:IScreen;
    public word:string="";

    constructor(screen:IScreen,callback:Function)
    {
        super(screen,"wordBox");

        (window as any).box=this;

        
        this.myScreen=screen;

        if (this.el)
        {
            this.el.addEventListener("pointerdown",(e:PointerEvent)=>{
                e.preventDefault();
                callback(this);
            });
        }
    }
   
    initPlace(xx:number,yy:number)
    {
        this.ox=xx;
        this.oy=yy;
        if (this.myScreen.grid)
        {
            this.myScreen.grid?.placeAt(xx,yy,this);
        }
         
    }
    setWord(word:string)
    {
        this.word=word;
        if (this.el)
        {
            this.textEl=this.el.getElementsByTagName("text")[0];
            
            this.textEl.innerHTML=word;
        }
       
    }
    doResize()
    {
        console.log("resize word box");

        
        this.displayHeight=this.myScreen.gh*.10;
        this.scaleX=this.scaleY;
        if (this.myScreen.grid)
        {
            this.myScreen.grid?.placeAt(this.ox,this.oy,this);
        }
    }
}