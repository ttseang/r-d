

import { GameOptions, SVGGame } from "svggame";
import { ScreenMain } from "./screens/ScreenMain";
import { ScreenOver } from "./screens/ScreenOver";
//require("./template.html");


window.onload=()=>{

    let options:GameOptions=new GameOptions();
    options.height=600;
    options.width=800;
    options.useFull=true;
    options.addAreaID="addHere";
    options.screens=[new ScreenMain(),new ScreenOver()];
    
    
    let game:SVGGame=new SVGGame("myCanvas",options);
}
