export class WordLevelVo
{
    public q:string;
    public answers:string[];
    public correct:string;
    constructor(q:string,answers:string[],correct:string)
    {
        this.q=q;
        this.answers=answers;
        this.correct=correct;
    }
}