import { BaseScreen, SvgObj, TextObj } from "svggame";

export class ScreenOver extends BaseScreen {
    constructor() {
        super("ScreenOver");
    }
    create() {
        let grid: SvgObj = this.addImage("agrid");
        let button1: SvgObj = this.addImageAt("testButton", 100, 100);
        let text1: TextObj = this.addText("This is the Game Over Screen");
        let text2:TextObj=this.addText("Press the button to see the main game screen");

        this.placeOnGrid(3, 2, text1);
        this.placeOnGrid(2,3,text2);
        this.placeOnGrid(4,8,button1);
        
        button1.onClick(this.showMain.bind(this));
    }
    showMain()
    {
       this.changeScreen("ScreenMain");       
    }
}