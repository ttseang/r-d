import { BaseScreen, SvgObj, TweenObj, IGameObj, SVGAnimation, CollideUtil, GameManager, TextObj } from "svggame";
import { WordBox } from "../classes/WordBox";
import { WordLevelVo } from "../dataObjs/WordLevelVo";



export class ScreenMain extends BaseScreen {

    private boxes: WordBox[] = [];
    private words: string[] = ["fish", "bird", "cat"];
    private isDown: boolean = false;
    private currentBox: WordBox | null = null;
    private levelIndex: number = -1;
    private levelData: WordLevelVo[] = [];
    private currentQuestion: WordLevelVo = new WordLevelVo("", [], "");
    private mainText: TextObj | null = null;

    private targBox: SvgObj | null = null;
    private clickLock: boolean = false;

    constructor() {
        super("ScreenMain");
    }
    create() {

        //console.log(GameManager.getInstance().gameOptions);

        let graphicGrid: SvgObj = this.addImage("agrid");

        let uiWindow: SvgObj = this.addImage("uiWindow");

        this.getGameData();
        (window as any).scene=this;
    }
    getGameData() {
        fetch("./assets/gamedata.json")
            .then(response => response.json())
            .then(data => this.gotGameData(data));
    }
    gotGameData(data: any) {
        console.log(data);
        let questions: any[] = data.quiz.questions;
        for (let i: number = 0; i < questions.length; i++) {
            let q: any = questions[i];
            console.log(q);
            let wordLevelVo: WordLevelVo = new WordLevelVo(q.q, q.a.split(","), q.correct);
            this.levelData.push(wordLevelVo);
        }
        this.buildGame();
    }
    buildGame() {
        this.targBox = this.addImage("targBox");
        this.placeOnGrid(5, 3, this.targBox);

        this.mainText = new TextObj(this, "qtext");

       
         this.nextLevel();

        document.addEventListener("pointerup", this.onUp.bind(this));
        // document.onmousedown=this.onDown.bind(this);
    }
    doResize()
    {
        super.doResize();
        if (this.targBox)
        {
           console.log("targ box");
           this.targBox.displayWidth=this.gw*0.2;

           console.log(this.targBox.scaleX);

           this.targBox.scaleY=this.targBox.scaleX;
           this.placeOnGrid(5, 3, this.targBox);
        }
    }
    nextLevel() {
        this.levelIndex++;
        if (this.levelIndex < this.levelData.length) {
            this.clickLock = false;
            this.currentQuestion = this.levelData[this.levelIndex];

            if (this.mainText) {
                if (this.mainText.el)
                {
                this.mainText.setText(this.currentQuestion.q);
                
              /*   this.placeOnGrid(5, 1, this.mainText);
                this.mainText.x=this.gw/2-this.mainText.el.clientWidth/2; */
                }
            }
            this.makeBoxes();
        }
    }
    destroyBoxes() {
        while (this.boxes.length > 0) {
            this.boxes.pop()?.destroy();
        }
    }
    makeBoxes() {
        this.destroyBoxes();
        //
        //
        //

        if (this.currentQuestion) {

            let answers: string[] = this.currentQuestion.answers;

            for (let i: number = 0; i < answers.length; i++) {
                //console.log("box");
                let box: WordBox = new WordBox(this, this.onDown.bind(this));
                box.setWord(answers[i]);
                box.initPlace(2.5, 3 + i * 1.5);
                this.boxes.push(box);
                

            }
        }
    }
    onDown(box: WordBox) {
        console.log(box);
        if (this.clickLock == true) {
            return;
        }
        this.currentBox = box;
        this.isDown = true;
        document.addEventListener("pointermove", this.onMove.bind(this));

    }
    onMove(event: PointerEvent) {
       // console.log("move");
        //console.log(event);
        event.preventDefault();
        if (this.clickLock == true) {
            console.log("locked");
            return;
        }
        if (this.isDown == true) {
            if (this.currentBox != null) {
                let xx: number = event.clientX - this.currentBox.displayWidth / 2;
                let yy: number = event.clientY - this.currentBox.displayHeight / 2;

                this.currentBox.x = xx;
                this.currentBox.y = yy;
            }
        }
        else
        {
            console.log("not down");
        }
    }
    onUp(e:PointerEvent) {
        e.preventDefault();
        console.log("on up");
        if (this.clickLock == true) {
            return;
        }
       
        if (this.currentBox && this.targBox) {
            if (CollideUtil.checkOverlap(this.currentBox, this.targBox) == true) {
                this.currentBox.x = this.targBox.x + this.targBox.displayWidth / 2 - this.currentBox.displayWidth / 2;
                this.currentBox.y = this.targBox.y + this.currentBox.displayHeight / 2;
                this.clickLock = true;
                
                if (this.currentBox.word == this.currentQuestion.correct) {
                    setTimeout(() => {
                        this.nextLevel();
                    }, 1000);

                }
                else {
                    console.log("wrong");

                    setTimeout(() => {
                        if (this.currentBox) {
                            this.snapBack(this.currentBox);
                            document.removeEventListener("pointermove", this.onMove.bind(this));
                            this.currentBox = null;
                            this.isDown = false;
                        }
                    }, 1000);
                    return;
                }
            }
            else {
                this.snapBack(this.currentBox);
            }
        }

        document.removeEventListener("pointermove", this.onMove.bind(this));
        this.currentBox = null;
        this.isDown = false;

    }
    checkCorrect() {

    }
    snapBack(box: WordBox) {
        this.tweenGridPos(box.ox, box.oy, box, 200, this.tweenDone.bind(this));
    }
    tweenDone() {
        console.log("done");
        this.clickLock = false;
    }
}