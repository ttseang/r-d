export class SVGUtil
{
    public static getAttNum(el:HTMLElement,attName: string) {
        if (el) {
            let val: string | null = el.getAttribute(attName);
            if (val == null) {
                return 0;
            }
            if (isNaN(parseInt(val))) {
                return 0;
            }
            return parseInt(val);
        }
        return 0;
    }
}