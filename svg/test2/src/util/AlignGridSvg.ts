export class AlignGrid 
{
    public ch:number=0;
    public cw:number=0;

    constructor(rows:number,cols:number,gw:number,gh:number)
    {
        this.ch=gw/cols;
        this.cw=gh/rows;
    }
}