import { GameOptions } from "./classes/core/dataobjs/gameOptions";
import { SVGGame } from "./classes/core/SvgGame"

import { ScreenMain } from "./screens/ScreenMain";
import { ScreenOver } from "./screens/ScreenOver";
//require("./template.html");


window.onload=()=>{

    let options:GameOptions=new GameOptions();
    options.screens=[new ScreenMain(),new ScreenOver()];
    options.addAreaID="addHere";
    
    let game:SVGGame=new SVGGame("myCanvas",options);
}
