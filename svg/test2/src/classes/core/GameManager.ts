import { GameOptions } from "./dataobjs/gameOptions";

export class GameManager
{
    private static instance:GameManager | null;
    public gameOptions:GameOptions =new GameOptions();

    constructor()
    {
        (window as any).gm=this;
    }
    public static getInstance():GameManager
    {
        if (this.instance==null)
        {
            this.instance=new GameManager();
        }
        return this.instance;
    }
}