export class SVGAnimation
{
    public id:string;
    private el:HTMLElement |null;
    
    constructor(id:string)
    {
        this.id=id;

        this.el=document.getElementById(id);
        console.log(this.el);
        (window as any).anim=this;
    }
    onComplete(callback:Function)
    {
        if (this.el)
        {
        this.el.addEventListener("endEvent",()=>{
            callback();
        });
    }
    }
    start()
    {
        if (this.el)
        {
            (this.el as any).beginElement();
        }
    }
    stop()
    {
        if (this.el)
        {
            (this.el as any).endElement();
        }
    }
}