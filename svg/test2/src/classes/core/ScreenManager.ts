import { IScreen } from "./interfaces/IScreen";
import { ScreenMain } from "../../screens/ScreenMain";

export class ScreenManager {
    private static instance: ScreenManager | null = null;
    public screens: IScreen[] = [];
    public currentScreen: IScreen | null = null;

    constructor() {

    }
    public static getInstance() {
        if (this.instance === null) {
            this.instance = new ScreenManager();
        }
        return this.instance;
    }
    public startScreen(screen: IScreen) {
        if (this.currentScreen != null) {
            this.currentScreen.destroy();
        }
        this.currentScreen = screen;
        this.currentScreen.start();

    }
    private findScreen(screenName: string): IScreen | null {
        for (let i: number = 0; i < this.screens.length; i++) {
            if (this.screens[i].key == screenName) {
                return this.screens[i];
            }
        }
        return null;
    }
    public changeScreen(screenName: string) {
        let screen: IScreen | null = this.findScreen(screenName);
        if (!screen) {
            throw new Error("Screen Not Found");
        }
        this.startScreen(screen);
    }
    public init() {
        if (this.screens.length > 0) {
            this.startScreen(this.screens[0]);
        }
    }
}