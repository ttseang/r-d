import { SVGUtil } from "../../util/SvgUtil";
import { GameOptions } from "./dataobjs/gameOptions";
import { GameManager } from "./GameManager";
import { ScreenManager } from "./ScreenManager";

export class SVGGame {
    private gameEl: HTMLElement | null = null;
    public gw: number;
    public gh: number;
    private screenManager: ScreenManager = ScreenManager.getInstance();
    private gameManager: GameManager = GameManager.getInstance();

    constructor(gameID: string, gameOptions: GameOptions) {
        let width: number = gameOptions.width;
        let height: number = gameOptions.height;

        this.gameEl = document.getElementById(gameID);
        if (this.gameEl == null) {
            if (width == 0 || height == 0) {
                throw new Error("Height and Width must be set if creating dynamic svg canvas");
                return;
            }
            this.gameEl = document.createElement("svg");
            this.gameEl.id = gameID;
            this.gameEl.setAttribute("width", width.toString());
            this.gameEl.setAttribute("height", height.toString());
            document.body.appendChild(this.gameEl);

            this.gw = width;
            this.gh = height;
        }
        else {
            this.gw = SVGUtil.getAttNum(this.gameEl, "width");
            this.gh = SVGUtil.getAttNum(this.gameEl, "height");
        }
        if (gameOptions.screens.length > 0) {
            this.screenManager.screens = gameOptions.screens;

        }
        else {
            gameOptions = new GameOptions();
        }
        if (gameOptions.addAreaID == "") {
            gameOptions.addAreaID = gameID;
        }
        gameOptions.width = this.gw;
        gameOptions.height = this.gh;
        

        this.gameManager.gameOptions = gameOptions;
        this.screenManager.init();
    }
}