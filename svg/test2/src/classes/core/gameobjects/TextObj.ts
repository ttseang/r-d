import { IScreen } from "../interfaces/IScreen";
import { SvgObj } from "./SvgObj";

export class TextObj extends SvgObj
{
    constructor(screen:IScreen,key:string)
    {
        super(screen,key);
    }
    static createNew()
    {
        //implement here
    }
    setText(text:string)
    {
        if (this.el)
        {
            console.log(text);
            this.el.textContent=text;
        }
    }
}