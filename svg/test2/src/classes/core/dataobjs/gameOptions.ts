import { BaseScreen } from "../BaseScreen";
import { IScreen } from "../interfaces/IScreen";


export class GameOptions
{
    public screens:IScreen[]=[];
    public width:number=0;
    public height:number=0;
    public addAreaID:string="main";
}