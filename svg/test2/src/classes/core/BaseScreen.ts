import { AlignGrid } from "../../util/AlignGridSvg";
import { GameManager } from "./GameManager";
import { SvgObj } from "./gameobjects/SvgObj";
import { TextObj } from "./gameobjects/TextObj";
import { IGameObj } from "./interfaces/IGameObj";
import { IScreen } from "./interfaces/IScreen";
import { ScreenManager } from "./ScreenManager";
import { SimpleTween } from "./tweens/SimpleTween";
import { TweenObj } from "./tweens/TweenObj";

export class BaseScreen implements IScreen
{
    public key:string;
    private gm:GameManager=GameManager.getInstance();
    private screenManager:ScreenManager=ScreenManager.getInstance();

    private children:IGameObj[]=[];

    private grid:AlignGrid;
    
    constructor(key:string)
    {
        this.key=key;
        console.log(this.gm.gameOptions);

        this.grid=new AlignGrid(10,10,this.gm.gameOptions.width,this.gm.gameOptions.height);
    }
    public start()
    {
        this.grid=new AlignGrid(10,10,this.gm.gameOptions.width,this.gm.gameOptions.height);
        this.create();
    }
    public create()
    {
       
    }
    public destroy()
    {

        while(this.children.length>0)
        {
            this.children.pop()?.destroy();
        }
    }
    public addExisting(obj:IGameObj)
    {
        this.children.push(obj);
    }
    public addImage(key:string)
    {
        let svgObj:SvgObj=new SvgObj(this,key);
        this.addExisting(svgObj);
        return svgObj;
    }
    public addImageAt(key:string,x:number,y:number)
    {
        let svgObj:SvgObj=this.addImage(key);
        svgObj.x=x;
        svgObj.y=y;
        return svgObj;
    }
    public addText(text:string,textKey:string="defaultText")
    {
        let textObj:TextObj=new TextObj(this,textKey);
        textObj.setText(text);
        this.addExisting(textObj);
        return textObj;
    }
    public addTextAt(text:string,x:number,y:number,textKey:string="defaultText")
    {
        let textObj:TextObj=this.addText(textKey,text);
        textObj.x=x;
        textObj.y=y;
        return textObj;       
    }
    public centerW(obj:IGameObj,adjust:boolean=false)
    {
        obj.x=this.gw/2;
        if (adjust==true)
        {
            obj.x-=obj.displayWidth/2;
        }
    }
    public centerH(obj:IGameObj,adjust:boolean=false)
    {
        obj.y=this.gh/2;
        if (adjust)
        {
            obj.y-=obj.displayHeight/2;
        }
    }
    public placeOnGrid(col:number,row:number,obj:IGameObj)
    {
        let xx:number=this.grid.ch*col;
        let yy:number=this.grid.cw*row;

        obj.x=xx;
        obj.y=yy;
    }
    public tweenGridPos(col:number,row:number,obj:IGameObj,duration:number,callback:Function=()=>{})
    {
        let tweenObj:TweenObj=new TweenObj();
        let xx:number=this.grid.ch*col;
        let yy:number=this.grid.cw*row;

        tweenObj.x=xx;
        tweenObj.y=yy;
        tweenObj.onComplete=callback;
        
        let tween:SimpleTween=new SimpleTween(obj,tweenObj);

    }
    public center(obj:IGameObj,adjust:boolean=false)
    {
        this.centerW(obj,adjust);
        this.centerH(obj,adjust);
    }
    public get gh():number
    {
        return this.gm.gameOptions.height;
    }
    public get gw():number
    {
        return this.gm.gameOptions.width
    }
    public changeScreen(nScreen:string)
    {
        console.log("change to "+nScreen);
        this.screenManager.changeScreen(nScreen);
    }
}