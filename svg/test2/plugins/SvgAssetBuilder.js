class SvgAssetBuilder {

    constructor(options) {
        this.options = options;
        this.lastTime=0;
    }

    apply(compiler) {
        compiler.hooks.beforeCompile.tap('SvgAssetBuilder', () => {

                            
           

            const fse = require('fs-extra');

            const templateFile = this.options.template;
            //'./src/template.html';

            const output = this.options.output;

            let exitFlag=false;

            fse.stat(templateFile,(err,stats)=>{
              let now=new Date().getTime();
              let then=stats.ctime.getTime();
              this.elapsed=now-then;

              if (this.lastTime==then)
              {
                exitFlag=true;
              }
              
              console.log(exitFlag);
              console.log(now);
              console.log(then);
              console.log(this.lastTime);
              console.log(this.elapsed);
             
              this.lastTime=then;
            })

            
            if (this.elapsed>100 && exitFlag==false)
            {
            let data = fse.readFileSync(templateFile, { encoding: "utf-8" });
            let reps = data.split("<!--file:");
            console.log(reps.length);
            if (reps.length===0)
            {
              return;
            }
            for (let i = 1; i < reps.length; i++) {
                let rep2 = reps[i].split("-->")[0];
                let file = fse.readFileSync("./src/defs/" + rep2 + ".html", { encoding: "utf-8" });
                let findString = "<!--file:" + rep2 + "-->";
                data = data.replace(findString, file);
            }
            fse.writeFile(output, data);

           // console.log(this.options.message || 'Hello World!');
          }
        });
    }
}

module.exports = SvgAssetBuilder;