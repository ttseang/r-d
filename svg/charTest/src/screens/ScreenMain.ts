import { BaseScreen, IScreen, SvgObj } from "svggame";
import { SpineChar } from "../classes/SpineChar";
import { TTPlayer } from "../classes/TTPlayer";
import { AnimationVo } from "../dataObjs/AnimationVo";

export class ScreenMain extends BaseScreen implements IScreen
{
    private box!:SvgObj;
   // private ttPlayer:TTPlayer;
    private spineChar:SpineChar;

    private idleAnimations:AnimationVo[];
    private walkAnimations:AnimationVo[];
    private fidgetAnimations:AnimationVo[];
    
    constructor()
    {
        super("ScreenMain");
    }
    create()
    {
        (window as any).scene=this;

        this.idleAnimations=[new AnimationVo("idle",true),new AnimationVo("Repeating animations/blink",true)];
        this.walkAnimations=[new AnimationVo("walk",true),new AnimationVo("Repeating animations/blink",true)];
        this.fidgetAnimations=[new AnimationVo("fidget01",false),new AnimationVo("Repeating animations/blink",true)];


       /*  this.box=new SvgObj(this,'box');
        this.box.gameWRatio=0.05; */
        this.spineChar=new SpineChar("canvas","./assets/","Amka","Amka",this.idleAnimations);

      //  this.ttPlayer=new TTPlayer("Amka","Amka");
        
        this.grid?.showGrid();
      //  this.placeOnGrid(2,2,this.box,true);
        
    }
    doResize()
    {
        super.doResize();
       // this.grid?.showGrid();
      //  this.box.updateScale();
       // this.placeOnGrid(2,2,this.box,true);
    }
}