import { BaseScreen, FTextObj, IScreen, SvgObj, TextObj } from "svggame";
import { MainText } from "../classes/MainText";

export class ScreenMain extends BaseScreen implements IScreen {

    private ship!: SvgObj;
    private text1!: TextObj;
    private mainText!: FTextObj;

    constructor() {
        super("ScreenMain");
    }
    create() {
        (window as any).scene = this;

        this.grid?.showGrid();

        this.gm.regFontSize("main",20,1180);

        this.ship = new SvgObj(this, "ship");

        this.ship.gameWRatio = 0.3;


        this.text1 = new TextObj(this, "text1");
        this.text1.setText("The History Of Pirates");
        this.text1.setFontSize(50);

       /*  if (this.text1.el) {
            this.text1.displayWidth = this.text1.el?.getBoundingClientRect().width;
            this.text1.displayHeight = this.text1.el?.getBoundingClientRect().height;
        } */
        
        this.mainText=new FTextObj(this,"text3");
        this.mainText.setText("Pirates have existed since ancient times –they threatened the trading routes of ancient Greece, and seized cargoes of grain and olive oil from Roman ships. The most far-reaching pirates in early medieval Europe were the Vikings.");
        //this.mainText.gameWRatio=0.5;
       
        

        //this.grid?.placeAt(5,5,this.ship);

        //  this.ship.adjust();

    }
    doResize() {
        super.doResize();
        this.grid?.showGrid();

        this.ship.updateScale();
        this.grid?.placeAt(5, 2, this.ship);
        this.centerW(this.ship, true);

        //  this.placeOnGrid(4,2,this.text1);
        this.center(this.text1, true);
        this.text1.y = this.gh * 0.1;

        
        this.grid?.placeAt(5.5,9,this.mainText);    
        this.mainText.setFontSize(this.gm.getFontSize("main",this.gw));    
        //this.mainText.updateScale();
        this.mainText.adjust();
    }
}