import { IScreen, SvgObj } from "svggame";

export class MainText extends SvgObj
{
    private scene:IScreen;
    
    constructor(scene:IScreen)
    {
        super(scene,"mainText");
        this.scene=scene;
    }
}