const fse = require('fs-extra');

const main = './testin/index.html';

const destDir = './testout/';
                            
let data=fse.readFileSync(main,{encoding:"utf-8"});
let reps=data.split("<!--file:");

for (let i=1;i<reps.length;i++)
{
    let rep2=reps[i].split("-->")[0];
    let file=fse.readFileSync("./testin/"+rep2+".html",{encoding:"utf-8"});
    let findString="<!--file:"+rep2+"-->";
    data=data.replace(findString,file);
}
fse.writeFile(destDir+"index.html",data);

