import { Main } from "../main";
import { SvgObj } from "./SvgObj";

export class Ball extends SvgObj
{
    private xDir:number=1;
    private scene:Main;
    private speed:number=2;

    constructor(scene:Main,key:string)
    {
        super(key,false);
        this.scene=scene;
       /*  if (this.el)
        {
            this.el.setAttribute("transform","translate(-12.5,-12.5)");
        } */
      
    }
    move()
    {
        this.incX(this.xDir*this.speed);
        if (this.x>this.scene.gw*.8 && this.xDir==1)
        {
            this.xDir=-1;
        }
        if (this.x<this.scene.gw*.15 && this.xDir==-1)
        {
            this.xDir=1;
        }
    }
    reverse()
    {
        this.xDir=-this.xDir;
    }
   
}