import { SvgObj } from "./SvgObj";

export class TextObj extends SvgObj
{
    constructor(key:string)
    {
        super(key,false);
    }
    static createNew()
    {
        //implement here
    }
    setText(text:string)
    {
        if (this.el)
        {
            console.log(text);
            this.el.textContent=text;
        }
    }
}