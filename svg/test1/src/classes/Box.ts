import { Main } from "../main";
import { CollideUtil } from "../util/collideUtil";
import { Ball } from "./Ball";
import { SvgObj } from "./SvgObj";

export class Box extends SvgObj {
    public nextBox: Box | null = null;
    private scene: Main;
    private key: string;
    
    constructor(scene: Main, key: string) {
        super(key);
        this.scene = scene;
        this.key = key;
        if (this.el) {
          //  this.el.setAttribute("transform", "translate(-12.5,-12.5)");
            /*  this.el.onclick=()=>{
                this.visible=false;
            }  */
        }
    }
    move() {

        this.incY(1);
        this.incRot(2);

        if (this.y > this.scene.gh) {
            this.y = -100;
            this.x = Math.floor(Math.random() * (this.scene.gw * .8)) + this.scene.gw * .1;
        }
        if (this.nextBox) {
            this.nextBox.move();
        }
    }
    public checkCollide(ball: Ball) {
        if (this.visible == true) {
            if (CollideUtil.checkOverlap(this,ball))
            {
                if (this.key == "box2") {
                    this.scene.doAction("gameOver", "");
                }
                else {
                    this.visible = false;
                    this.scene.doAction("points", "");
                }
            }
        }
        if (this.nextBox) {
            this.nextBox.checkCollide(ball);
        }
    }
}