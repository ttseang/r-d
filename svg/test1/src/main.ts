import { Ball } from "./classes/Ball";
import { Box } from "./classes/Box";
import { SvgObj } from "./classes/SvgObj";
import { TextObj } from "./classes/TextObj";
import { HtmlUtils } from "./util/htmlUtil";

export class Main {

    private svg: HTMLElement | null=null;
    // public ballDef: Ball = new Ball("ball");
    //public gboxDef: Box = new Box("box1");
    //public wboxDef: Box = new Box("box2");
    private boxCount: number = 0;
    private ball: Ball | null=null;
    public gw: number = 480;
    public gh: number = 640;

    private firstBox: Box | null = null;
    private prevBox: Box | null = null;
    private gameOver: boolean = false;

    private updateTimer: any;
    private score:number=0;
    private scoreText:TextObj | null=null;
    //private ship:SvgObj;
    //private box:SvgObj |null=null;
    
    // public boxArray:Box[]=[];

    constructor() {           
        (window as any).scene = this;

        this.loadLib();
    }
    loadLib()
    {
       HtmlUtils.loadLib(()=>{this.libLoaded()});
    }
    libLoaded()
    {
        this.svg = document.getElementById("myCanvas");
        // this.box1=new SvgObj("box1");
        this.ball = new Ball(this, "ship");
       // this.ship=new SvgObj("ship");
        this.scoreText=new TextObj("scoreText");

        
        if (this.svg) {
            this.buildGame();
            // this.preload();
         }
         this.updateTimer = setInterval(this.update.bind(this), 20);

        // this.box=new SvgObj("rock");
    }
  
    buildGame() {
        if (this.ball) {
            this.ball.setScale(0.25);
            this.ball.x = this.gw / 2;
            this.ball.y = this.gh / 2;           
            this.ball.y-=this.ball.displayHeight/4;
        }

        

        // this.setClick("box1",this.clickMe.bind(this));

        let colors: number[] = [1, 1, 1, 0, 1, 1, 0, 0, 1];
        let colorIndex: number = -1;

        for (let i: number = 0; i < 10; i++) {
            colorIndex++;
            if (colorIndex > colors.length) {
                colorIndex = 0;
            }
            let color: number = colors[colorIndex];

            let y: number = -i * this.gh / 10 - 100;
            let x: number = Math.floor(Math.random() * (this.gw * .8)) + this.gw * .1;
           

            let box: Box | null = this.addNewBox(x, y, color);

            if (box) {
                //    this.boxArray.push(box);
                if (this.firstBox == null) {
                    this.firstBox = box;
                }
                if (this.prevBox) {
                    this.prevBox.nextBox = box;
                }
                this.prevBox = box;
            }
        }
        if (this.svg) {
            this.svg.onclick = () => { this.ball?.reverse() }
        }
    }
    addNewBox(x: number, y: number, color: number) {

        this.boxCount++;
        let box: Box | null;
        if (color == 0) {
            box = new Box(this, "box1");
        }
        else {
            box = new Box(this, "rock");
            box.displayWidth=this.gw*.05;
            box.scaleY=box.scaleX;
        }

        if (box) {
            box.x = x;
            box.y = y;
        }
        return box;
    }
    doAction(action: string, param: string) {
        if (action == "gameOver") {
            this.gameOver=true;
        }
        if (action=="points")
        {
            this.score++;
            if (this.scoreText)
            {
                this.scoreText.setText("Score "+this.score.toString());
            }           
        }
    }
    update() {
        if (this.gameOver == false) {
            if (this.firstBox) {
                this.firstBox.move();
                if (this.ball) {
                    this.firstBox.checkCollide(this.ball);
                }

            }
            if (this.ball) {
                this.ball.move();
            }
        }
    }
}