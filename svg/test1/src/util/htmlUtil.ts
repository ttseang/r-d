export class HtmlUtils {
    constructor() {

    }
    public static loadLib(callback:Function=()=>{}) {

        let z:HTMLCollectionOf<Element>;
        let elmnt:Element;

        let  file:any;
        let xhttp:XMLHttpRequest;
        
        /* Loop through a collection of all HTML elements: */
        z = document.getElementsByTagName("*");
        for (let i:number = 0; i < z.length; i++) {
            elmnt = z[i];
            /*search for elements with a certain atrribute:*/
            file = elmnt.getAttribute("w3-include-html");
            if (file) {
                /* Make an HTTP request using the attribute value as the file name: */
                xhttp = new XMLHttpRequest();
               
                xhttp.onreadystatechange = (function () {
                    if (xhttp.readyState == 4) {
                        if (xhttp.status == 200) { elmnt.innerHTML = xhttp.responseText; }
                        if (xhttp.status == 404) { elmnt.innerHTML = "Page not found."; }
                        /* Remove the attribute, and call this function once more: */
                        elmnt.removeAttribute("w3-include-html");
                        callback();
                      //  HtmlUtils.loadLib();
                    }
                }).bind(this);

                xhttp.open("GET", file, true);
                xhttp.send();
                /* Exit the function: */
                return;
            }
        }

    }
}