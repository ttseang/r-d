import { AssetManager } from "./AssetManager";
import { AnimationState, AnimationStateData, AtlasAttachmentLoader, MathUtils, Skeleton, SkeletonData, SkeletonJson, TextureAtlas, TextureFilter, Vector2 } from "./core";
import { GLTexture } from "./GLTexture";
import { SkeletonRenderer } from "./SkeletonRenderer";
import { ManagedWebGLRenderingContext } from "./WebGL";

export class TTPlayer {
	private jsonName: string;
	private atlasName: string;
	private canvas: HTMLCanvasElement;
	//private context: CanvasRenderingContext2D | null;
	private context:WebGL2RenderingContext|null;

	private skeletonRenderer!: SkeletonRenderer;
	private assetManager!: AssetManager;

	public skeleton!: Skeleton;

	private state: any;

	private bounds: any;

	private atlas!: TextureAtlas;
	private atlasLoader!: AtlasAttachmentLoader;

	private lastFrameTime: number = Date.now() / 1000;

	private defaultAnimation: string = "idle";

	public animationState!: AnimationState;
	private callback: Function;
	
	constructor(folder: string, jsonName: string, atlasName: string, callback: Function) {
		this.jsonName = jsonName;
		this.atlasName = atlasName;
		this.callback = callback;

		this.canvas = document.getElementById("canvas") as HTMLCanvasElement;
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
		//this.context = this.canvas.getContext("2d");
		this.context=this.canvas.getContext("webgl2");

		if (this.context) {

			this.skeletonRenderer = new SkeletonRenderer(this.context);
		//	this.skeletonRenderer.debugRendering = false;
			// enable the triangle renderer, supports meshes, but may produce artifacts in some browsers
			//this.skeletonRenderer.triangleRendering = false;

			this.assetManager = new AssetManager(this.context,folder);
			this.assetManager.loadText(jsonName + ".json");

			this.assetManager.loadText(atlasName + ".atlas");
			this.assetManager.loadTexture(atlasName + ".png");

			requestAnimationFrame(this.load.bind(this));
		}
	}
	load() {
		console.log("load");

		if (this.assetManager.isLoadingComplete()) {
			var data = this.loadSkeleton(this.jsonName, this.defaultAnimation, "default");
			this.skeleton = data.skeleton;
			this.state = data.state;
			this.bounds = data.bounds;

			// Create an AnimationState, and set the initial animation in looping mode.

		this.animationState = new AnimationState(new AnimationStateData(data.skeleton.data));
		this.animationState.addListener({ event: (trackIndex, event) => { console.log("Event on track " + trackIndex + ": " + JSON.stringify(event)); } });


			requestAnimationFrame(this.render.bind(this));

			this.callback();
		} else {
			requestAnimationFrame(this.load.bind(this));
		}

	}
	loadSkeleton(name: string, initialAnimation: string, skin: string): any {
		console.log("skeleton");
		console.log(name);
		if (!this.context) {
			return
		}

		if (skin === undefined) skin = "default";
		// Load the texture atlas using name.atlas and name.png from the AssetManager.
		// The function passed to TextureAtlas is used to resolve relative paths.

		let atlasName = name.replace("-pro", "").replace("-ess", "") + ".atlas";
		this.atlas = new TextureAtlas(this.assetManager.require(atlasName));

		this.atlas.setTextures(this.assetManager);


		// Configure filtering, don't use mipmaps in WebGL1 if the atlas page is non-POT
		//let atlas = this.assetManager.require(this.atlasUrl) as TextureAtlas;
		//let gl = this.context.gl;

		//	let anisotropic = gl.getExtension("EXT_texture_filter_anisotropic");
		//let isWebGL1 = gl.getParameter(gl.VERSION).indexOf("WebGL 1.0") != -1;
		let isWebGL1: boolean = true;
		for (let page of this.atlas.pages) {
			console.log(page);

			let minFilter = page.minFilter;
		//	var useMipMaps: boolean = false;
		
			if (minFilter != TextureFilter.Nearest && minFilter != TextureFilter.Linear) (page.texture as GLTexture).update(true);
			minFilter = TextureFilter.Linear; // Don't use mipmaps without anisotropic.
			if (page.texture) {
				page.texture.setFilters(minFilter, TextureFilter.Nearest);
			}

		}

		// Create a AtlasAttachmentLoader, which is specific to the WebGL backend.
		this.atlasLoader = new AtlasAttachmentLoader(this.atlas);

		// Create a SkeletonJson instance for parsing the .json file.
		var skeletonJson = new SkeletonJson(this.atlasLoader);

		// Set the scale to apply during parsing, parse the file, and create a new skeleton.
		var skeletonData = skeletonJson.readSkeletonData(this.assetManager.require(name + ".json"));
		let skeleton = new Skeleton(skeletonData);
		this.skeleton=skeleton;
		skeleton.scaleY = -1;
		var bounds = this.calculateBounds(skeleton);
		skeleton.setSkinByName(skin);

		
		return { skeleton: skeleton, state: this.animationState, bounds: bounds };
	}
	calculateBounds(skeleton: Skeleton) {
		var data = skeleton.data;
		skeleton.setToSetupPose();
		skeleton.updateWorldTransform();
		var offset = new Vector2();
		var size = new Vector2();
		skeleton.getBounds(offset, size, []);
		return { offset: offset, size: size };
	}

	render(delta:number) {
		var now = Date.now() / 1000;
		var delta = now - this.lastFrameTime;
		this.lastFrameTime = now;

		this.resize();
		if (this.context) {
			/* this.context.save();
			this.context.setTransform(1, 0, 0, 1, 0, 0);
			this.context.fillStyle = "#cccccc";
			this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
			this.context.restore(); */

			this.animationState.update(delta);
			this.animationState.apply(this.skeleton);
			this.skeleton.updateWorldTransform();
		//	this.skeletonRenderer.draw(this.skeleton);
/* 
			this.context.strokeStyle = "green";
			this.context.beginPath();
			this.context.moveTo(-1000, 0);
			this.context.lineTo(1000, 0);
			this.context.moveTo(0, -1000);
			this.context.lineTo(0, 1000);
			this.context.stroke(); */

			requestAnimationFrame(this.render.bind(this));
		}
	}
	resize() {
		var w = this.canvas.clientWidth;
		var h = this.canvas.clientHeight;
		if (this.canvas.width != w || this.canvas.height != h) {
			this.canvas.width = w;
			this.canvas.height = h;
		}

		// magic
		var centerX = this.bounds.offset.x + this.bounds.size.x / 2;
		var centerY = this.bounds.offset.y + this.bounds.size.y / 2;
		var scaleX = this.bounds.size.x / this.canvas.width;
		var scaleY = this.bounds.size.y / this.canvas.height;
		var scale = Math.max(scaleX, scaleY) * 1.2;
		if (scale < 1) scale = 1;
		var width = this.canvas.width * scale;
		var height = this.canvas.height * scale;

		/* if (this.context) {
			this.context.setTransform(1, 0, 0, 1, 0, 0);
			this.context.scale(1 / scale, 1 / scale);
			this.context.translate(-centerX, -centerY);
			this.context.translate(width / 2, height / 2);
		} */
	}
}