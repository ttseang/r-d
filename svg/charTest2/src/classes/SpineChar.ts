
import { AnimationVo } from "../dataObjs/AnimationVo";

import { SpineTween } from "./SpineTween";
import { SpineTweenObj } from "./SpineTweenObj";
import { TTPlayer2 } from "./TTPlayer2";

export class SpineChar {
    

    private player: TTPlayer2;
    private animations: AnimationVo[];

    private canvasID: string;
    private _x: number = 0;
    private _y: number = 0;
    private _scaleX: number = 1;
    private _scaleY: number = 1;

    public realH:number=3000;
    public realW:number=6000;

    private _flip: boolean = false;
    

    public el: HTMLElement | null = null;

    constructor(canvasID: string,folder:string, jsonKey: string, atlasKey: string, animations: AnimationVo[]) {
        //let config:SpinePlayerConfig={};

        this.canvasID = canvasID;
        this.animations = animations;

       
        this.el = document.getElementById(canvasID);
        if (this.el) {
            this.el.style.visibility = "hidden";
        }
        

        this.player = new TTPlayer2(canvasID,folder,jsonKey,atlasKey,this.onSuccess.bind(this));

    }
    public setPerPos(xper:number,yper:number)
    {
        /* let w2:number=(this.player as any).viewport.width;
        let h2:number=(this.player as any).viewport.height;

        console.log(w2,h2); */
        this.player.onResize();
        let yy:number=screen.height*(100-yper)/100;
        let xx:number=screen.width*xper/100;
        console.log(xx,yy);

        this.x=xx;
        this.y=yy;
    }
    scaleToGameW(per:number)
    {
        let desiredWidth:number=screen.width*per;
        console.log("ds="+desiredWidth);
        let scale1:number=desiredWidth/this.realW;
        console.log(scale1);

        this.scaleX=scale1;
        this.scaleY=scale1;

    }
    adjustY()
    {
        let displayHeight:number=this.realH*this.scaleY;
        this.y-=displayHeight;
    }

    onSuccess() {
      
        for (let i: number = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }

        setTimeout(() => {

            /* 
                 this.player.skeleton.x = this._x;
                this.player.skeleton.y = this._y;
                this.player.skeleton.scaleX = this._scaleX;
                this.player.skeleton.scaleY = this._scaleY; */

                //trigger setters
                 this.flip=this._flip;
                this.x=this._x;
                this.y=this._y;
                this.scaleY=this._scaleY;
                this.scaleX=this._scaleX;

                if (this.el) {
                    this.el.style.visibility = "visible";
                }
            
        }, 1000);
    }
    setScale(scale:number)
    {
        this.scaleX=scale;
        this.scaleY=scale;
    }
    restartAnimation()
    {
        this.setAnimations(this.animations);
    }
    setAnimations(animations: AnimationVo[]) {
        this.animations = animations;

        if (!this.player)
        {
            setTimeout(this.restartAnimation.bind(this),1000);
            return;
        }
        if (!this.player.animationState)
        {
            setTimeout(this.restartAnimation.bind(this),1000);
            return;
        }

        for (let i: number = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }
    }
    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
        if (this.player) {
            if (this.player.skeleton) {
                this.player.skeleton.x = value;
            }
        }
    }
    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
        if (this.player) {
            if (this.player.skeleton) {

                //let sy:number=value-this.realH/2;

                this.player.skeleton.y = value;
            }
        }
    }

    public get scaleX(): number {
        return this._scaleX;
    }
    public set scaleX(value: number) {
        this._scaleX = value;
        if (this.player) {
            if (this.player.skeleton) {

                let v:number=value;
                if (this._flip==true)
                {
                    v=-v;
                }

                this.player.skeleton.scaleX = v;
            }
        }
    }

    public get scaleY(): number {
        return this._scaleY;
    }
    public set scaleY(value: number) {
        this._scaleY = value;
        if (this.player) {
            if (this.player.skeleton) {
                this.player.skeleton.scaleY = value;
            }
        }
    }
    public get flip(): boolean {
        return this._flip;
    }
    public set flip(value: boolean) {
        this._flip = value;
        this.scaleX=this._scaleX;
    }
    moveTo(xx: number, yy: number,duration:number=1000,animations:AnimationVo[],endAnimations:AnimationVo[],flipOnEnd:boolean=false) {
        let tweenObj:SpineTweenObj=new SpineTweenObj();
        tweenObj.x=xx;
        tweenObj.y=yy;
        tweenObj.duration=duration;
        this.setAnimations(animations);

        tweenObj.onComplete=()=>{
            this.setAnimations(endAnimations);
            if (flipOnEnd==true)
            {
                this.flip=!this.flip;
            }
        }

        let tween:SpineTween=new SpineTween(this,tweenObj,true);


        
    }
}