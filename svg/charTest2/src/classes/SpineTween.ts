
import { SpineChar } from "./SpineChar";
import { SpineTweenObj } from "./SpineTweenObj";


export class SpineTween {
    private gameObj: SpineChar;
    private tweenObj: SpineTweenObj;
    private autoStart: boolean;

    /**
     * Targets
     */
    private xTarget: number;
    private yTarget: number;
    private scaleXTarget:number;
    private scaleYTarget:number;
   

    /**
     * Increments
     *  */ 
    private xInc: number = 0;
    private yInc: number = 0;
    private scaleXInc:number=0;
    private scaleYInc:number=0;
    /**
     * Timer
     */
    private myTimer: any = null;
    private myTime: number = 0;

    constructor(gameObj: SpineChar, tweenObj: SpineTweenObj, autoStart: boolean = true) {
        this.gameObj = gameObj;
        this.tweenObj = tweenObj;
        this.autoStart = autoStart;
        //
        //
        this.xTarget = gameObj.x;
        this.yTarget = gameObj.y;
        
        this.scaleXTarget=gameObj.scaleX;
        this.scaleYTarget=gameObj.scaleY;
        

        if (tweenObj.x != null) {
            this.xTarget = tweenObj.x;
        }
        if (tweenObj.y != null) {
            this.yTarget = tweenObj.y;
        }
        
        if (tweenObj.scaleX!=null)
        {
            this.scaleXTarget=tweenObj.scaleX;
        }
        if (tweenObj.scaleY!=null)
        {
            this.scaleYTarget=tweenObj.scaleY;
        }
      
        let duration2: number = tweenObj.duration / 5;
        this.myTime = tweenObj.duration;
        //
        //
        this.xInc = (this.xTarget - gameObj.x) / duration2;
        this.yInc = (this.yTarget - gameObj.y) / duration2;
        //
        //
        this.scaleXInc=(this.scaleXTarget-gameObj.scaleX)/duration2;
        this.scaleYInc=(this.scaleYTarget-gameObj.scaleY)/duration2;
        //
        //

        if (autoStart == true) {
            this.start();
        }
    }
    public start() {
        this.myTimer = setInterval(this.doStep.bind(this), 5);
    }
    private doStep() {
        this.myTime -= 5;

        if (this.myTime < 0) {
            this.gameObj.x = this.xTarget;
            this.gameObj.y = this.yTarget;
            

            clearInterval(this.myTimer);
            this.tweenObj.onComplete(this.gameObj);
            return;
        }
        this.gameObj.x += this.xInc;
        this.gameObj.y += this.yInc;
        this.gameObj.scaleX+=this.scaleXInc;
        this.gameObj.scaleY+=this.scaleYInc;
       
        
    }
}