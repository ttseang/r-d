import { AssetManager } from "./AssetManager";
import { AnimationState, AnimationStateData, AtlasAttachmentLoader, Skeleton, TextureAtlas, TimeKeeper } from "./core";
import { SceneRenderer } from "./SceneRenderer";
import { SkelLoader } from "./util/SkelLoader";
import { ManagedWebGLRenderingContext } from "./WebGL";

export class TTPlayer2 {
    private jsonName: string;
    private atlasName: string;
    private canvas!: HTMLCanvasElement;
    private renderer!:SceneRenderer;
    //
    //
    //
    //private context: CanvasRenderingContext2D | null;
    private context!: WebGL2RenderingContext | null;
    public callback: Function = () => { };


    //
    //
    //
    private atlas!: TextureAtlas;
    private assetManager!: AssetManager;
    public skeleton!: Skeleton;
    public atlasLoader!: AtlasAttachmentLoader;
    private state: any;
	private bounds: any;
    public animationState!: AnimationState;
    private folder:string;
    private canvasID:string;
    //
    //
    //
    private timeKeeper:TimeKeeper=new TimeKeeper();

    constructor(canvasID:string,folder: string, jsonName: string, atlasName: string, callback: Function) {
        this.folder=folder;
        this.canvasID=canvasID;
        this.jsonName = jsonName;
        this.atlasName = atlasName;
        this.callback = callback;
        this.setUpCanvas();

    }
    setUpCanvas() {
        
        this.canvas = document.getElementById(this.canvasID) as HTMLCanvasElement;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        let options:WebGLContextAttributes={};
        options.alpha=true;
        this.context=this.canvas.getContext("webgl2",options);
        
        //new ManagedWebGLRenderingContext(this.context);
        

        if (this.context)
        {


            this.assetManager = new AssetManager(this.context,this.folder);
			this.assetManager.loadText(this.jsonName + ".json");
			this.assetManager.loadText(this.atlasName + ".atlas");

           
			this.assetManager.loadTexture(this.atlasName + ".png");
            this.assetManager.loadTexture(this.atlasName + "_2.png");

            this.renderer=new SceneRenderer(this.canvas,this.context);
            
			requestAnimationFrame(this.loadAssets.bind(this));

        }
    }
    loadAssets() {
        // Load the skeleton file.

        
        if (this.assetManager.isLoadingComplete()) {
        console.log(this.atlasName);

        

        //texture atlas
        let atlasName = this.atlasName.replace("-pro", "").replace("-ess", "") + ".atlas";
        this.atlas = new TextureAtlas(this.assetManager.require(atlasName));
        this.atlas.setTextures(this.assetManager);
            
        
        // Create a AtlasAttachmentLoader, which is specific to the WebGL backend.
        this.atlasLoader = new AtlasAttachmentLoader(this.atlas);
        if (this.context)
        {
        let skelLoader:SkelLoader=new SkelLoader(this.assetManager,this.context,this.atlas,this.atlasLoader);
        let data:any=skelLoader.loadSkeleton(this.jsonName);


        this.skeleton = data.skeleton;
		//this.state = data.state;
		this.bounds = data.bounds;

        


        let animationStateData = new AnimationStateData(data.skeleton.data);
		this.animationState = new AnimationState(animationStateData);
		//this.animationState.setAnimation(0, "idle", true);

       
        this.doLoop();
        this.callback();
        }
       
    }
    else
    {
        
        requestAnimationFrame(this.loadAssets.bind(this));
    }
      //  this.callback();
    }
    doLoop()
    {
        this.timeKeeper.update();
        this.update(this.timeKeeper.delta);
        this.render();
        requestAnimationFrame(this.doLoop.bind(this));
    }
   
    update(delta:number) {
        // Update the animation state using the delta time.
				this.animationState.update(delta);
				// Apply the animation state to the skeleton.
				this.animationState.apply(this.skeleton);
				// Let the skeleton update the transforms of its bones.
				this.skeleton.updateWorldTransform();
    }
    onResize()
    {
        this.renderer.canvas.width=screen.width;
        this.renderer.canvas.height=screen.height;
        this.renderer.camera.position.x=screen.width/2;
        this.renderer.camera.position.y=screen.height/2;
        this.renderer.camera.setViewport(screen.width,screen.height);
    }
    render() {
       
				// Resize the viewport to the full canvas.
				//this.renderer.resize(ResizeMode.Expand);
                if (this.context)
                {
				// Clear the canvas with a light gray color.
				this.context.clearColor(0, 0, 0, 0.5);
                this.context.clear(this.context.COLOR_BUFFER_BIT);

                }
     
				// Begin rendering.
				this.renderer.begin();
                
				// Draw the skeleton
				this.renderer.drawSkeleton(this.skeleton, true);

                //this.renderer.drawSkeletonDebug(this.skeleton,true);

                
				// Complete rendering.
				this.renderer.end();

              
    }
   
}