export class SuccessType
{
    public path:string;
    public binary:Uint8Array;

    constructor(path:string,binary:Uint8Array)
    {
        this.path=path;
        this.binary=binary;
    }
}