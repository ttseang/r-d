export class ErrorType
{
    public path:string;
    public message:string;

    constructor(path:string,message:string)
    {
        this.path=path;
        this.message=message;
    }
}