import { AssetManager } from "../AssetManager";
import { AnimationState, AtlasAttachmentLoader, MathUtils, Skeleton, SkeletonJson, TextureAtlas, TextureFilter, Vector2 } from "../core";
import { GLTexture } from "../GLTexture";
import { ManagedWebGLRenderingContext } from "../WebGL";

export class SkelLoader {
    private assetManager: AssetManager;
    private atlas: TextureAtlas;
    private atlasLoader: AtlasAttachmentLoader;
    private context: WebGL2RenderingContext;

    constructor(assetManager: AssetManager, context: WebGL2RenderingContext, atlas: TextureAtlas, atlasLoader: AtlasAttachmentLoader) {
        this.context = context;
        this.assetManager = assetManager;
        this.atlas = atlas;
        this.atlasLoader = atlasLoader;
    }
    loadSkeleton(name: string, skin: string = undefined): any {
        if (skin === undefined) skin = "default";
        let atlasName = name.replace("-pro", "").replace("-ess", "") + ".atlas";


        
        // Configure filtering, don't use mipmaps in WebGL1 if the atlas page is non-POT


        let context = new ManagedWebGLRenderingContext(this.context);
        let gl = context.gl;
        let anisotropic = gl.getExtension("EXT_texture_filter_anisotropic");

        let isWebGL1 = gl.getParameter(gl.VERSION).indexOf("WebGL 1.0") != -1;
        //
        //
        //
        for (let page of this.atlas.pages) {
          //  console.log(page);

           let name:string=page.name;
           console.log(name);
         
           
            
            let minFilter = page.minFilter;
            var useMipMaps: boolean = true;
            var isPOT = MathUtils.isPowerOfTwo(page.width) && MathUtils.isPowerOfTwo(page.height);
            if (isWebGL1 && !isPOT) useMipMaps = false;

            if (useMipMaps) {
                if (anisotropic) {
                    gl.texParameterf(gl.TEXTURE_2D, anisotropic.TEXTURE_MAX_ANISOTROPY_EXT, 8);
                    minFilter = TextureFilter.MipMapLinearLinear;
                } else
                    minFilter = TextureFilter.Linear; // Don't use mipmaps without anisotropic.
                if (page.texture) {
                    page.texture.setFilters(minFilter, TextureFilter.Nearest);
                }
            }
            if (page.texture) {
                if (minFilter != TextureFilter.Nearest && minFilter != TextureFilter.Linear) (page.texture as GLTexture).update(true);
            }
        }


        let skeletonJson = new SkeletonJson(this.atlasLoader);
        //  console.log(skeletonJson);
        //  console.log(this.assetManager);

        let jdata: any = this.assetManager.require(name + ".json");

       //  console.log(jdata);

        // Set the scale to apply during parsing, parse the file, and create a new skeleton.
        let skeletonData = skeletonJson.readSkeletonData(jdata);
        let skeleton = new Skeleton(skeletonData);

        skeleton.scaleY = -1;
        let bounds = this.calculateBounds(skeleton);
        skeleton.setSkinByName(skin);
        return { skeleton: skeleton, bounds: bounds };
    }
    calculateBounds(skeleton: Skeleton) {
        var data = skeleton.data;
        skeleton.setToSetupPose();
        skeleton.updateWorldTransform();
        var offset = new Vector2();
        var size = new Vector2();
        skeleton.getBounds(offset, size, []);
        return { offset: offset, size: size };
    }
}