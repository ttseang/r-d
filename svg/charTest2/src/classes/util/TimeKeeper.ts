
export class TimeKeeper {
    public maxDelta: number;
    public framesPerSecond: number;
    public delta: number;
    public totalTime: number;
    public lastTime: number;
    public frameCount: number;
    public frameTime: number;

    constructor() {
        this.maxDelta = 0.064;
        this.framesPerSecond = 0;
        this.delta = 0;
        this.totalTime = 0;
        this.lastTime = Date.now() / 1e3;
        this.frameCount = 0;
        this.frameTime = 0;
    }
    update() {
        let now = Date.now() / 1e3;
        this.delta = now - this.lastTime;
        this.frameTime += this.delta;
        this.totalTime += this.delta;
        if (this.delta > this.maxDelta)
            this.delta = this.maxDelta;
        this.lastTime = now;
        this.frameCount++;
        if (this.frameTime > 1) {
            this.framesPerSecond = this.frameCount / this.frameTime;
            this.frameTime = 0;
            this.frameCount = 0;
        }
    }
};