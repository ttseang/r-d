//allow unused variables
#![allow(unused)]
//allow unused imports
#![allow(unused_imports)]

use rand::seq::SliceRandom;
use rand::Rng;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, ErrorKind, Write};
//ordering
use std::cmp::Ordering;

fn main() {
    //word array
    let st3=String::from("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z");
    let mut v1:Vec<char>=st3.chars().collect();
    //remove all spaces
    v1.retain(|&x| x != ' ');
    v1.dedup();

    for ch in v1{
        println!("{}",ch);
    }
}
