import { IPage } from "./interfaces/IPage";
import { MainPage } from "./pages/MainPage";
import { Pages } from "./constants/PageConstants";
import { GradeBook } from "./pages/GradeBook";
import { SettingsPage } from "./pages/SettingsPage";
import { DetailPage } from "./pages/DetailPage";
import { WallpaperPage } from "./pages/WallpaperPage";
import { StickerBookPage } from "./pages/StickerBookPage";
import { BookIndexPage } from "./pages/BookIndexPage";

export class MathUI {

    private currentPage: IPage | null = null;

    private pageIDs: number[] = [];
    private pageClasses: any[] = [MainPage, WallpaperPage, GradeBook, SettingsPage, DetailPage, StickerBookPage,BookIndexPage];

    constructor() {

        this.pageIDs = [Pages.Page.Home, Pages.Page.Wallpaper, Pages.Page.Grades, Pages.Page.Settings, Pages.Page.Details, Pages.Page.Stickers,Pages.Page.BookIndex];
        this.changePage(0);
    }
    doAction(action: number, actionData: number): void {
        switch (action) {
            case Pages.Actions.ChangePage:
                this.changePage(actionData);
                break;

        }
    }
    changePage(pageID: number): void {

        console.log("changePage: " + pageID);

        //make a new instance based on index of the pageClasses array
        let pageEnum: number = this.pageIDs[pageID];
        let page: IPage = new this.pageClasses[pageID](pageEnum, this.doAction.bind(this));

        if (page) {
            if (this.currentPage) {
                this.currentPage.setInactive();
                this.currentPage = null;
            }

            this.currentPage = page;
            this.currentPage.setActive();
        }

    }
}