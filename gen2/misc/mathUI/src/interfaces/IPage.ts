//make an interface for the page classes
//each page class will implement this interface
//methods - setactive, setinactive, update, render
//constructor will pass in a callback function to the main class
export interface IPage {
    setActive(): void;
    setInactive(): void;
    render(): void;    
    getPageID():number;
}