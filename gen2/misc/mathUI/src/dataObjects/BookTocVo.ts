//make a dataobject with the properties title page lesson
export class BookTocVo
{
    public title:string;
    public page:number;
    public lesson:number;
    constructor(title:string, page:number, lesson:number)
    {
        this.title = title;
        this.page = page;
        this.lesson = lesson;
    }
}
