import { ProblemVo } from "./ProblemVo";

//make a data object for the grade with the following properties lti,  name, score, lastwork, complete, prescore, rawscore, probsdone, probstotal, bonuspoints, probscorrect,
export class GradeVo
{
    public id:number;
    public lti: string;
    public name: string;
    public score: string;
    public lastwork: string;
    public complete: boolean;
    public prescore: string;
    public rawscore: string;
    public probsdone: number;
    public probstotal: number;
    public bonuspoints: number;
    public probscorrect: number;
    public problems:ProblemVo[]=[];

    constructor(id:number,lti:string,name:string,score:string,lastwork:string,complete:boolean,prescore:string,rawscore:string,probsdone:number,probstotal:number,bonuspoints:number,probscorrect:number)
    {
        this.lti=lti;
        this.name=name;
        this.score=score;
        this.lastwork=lastwork;
        this.complete=complete;
        this.prescore=prescore;
        this.rawscore=rawscore;
        this.probsdone=probsdone;
        this.probstotal=probstotal;
        this.bonuspoints=bonuspoints;
        this.probscorrect=probscorrect;
        this.id=id;        
    }
    addProblems(data:any)
    {
        for (let i: number = 0; i < data.length; i++) {
            let problemData: any = data[i];
           
            let problemVo: ProblemVo = new ProblemVo(i,problemData.name, problemData.lti, problemData.hint, problemData.mark, problemData.second, problemData.solution);
            this.problems.push(problemVo);
        }        
    }
    getHtml(): string {

        let numberCorrectHtml: string = `<td>${this.probscorrect}</td>`;
        if (this.bonuspoints > 0) {
            numberCorrectHtml = `<td class="bonus b${this.bonuspoints.toString().trim()}">${this.probscorrect}</td>`;
        }

        let isCompleteHtml: string = `<td class="mr"><span></span></td>`;
        if (this.complete) {
            isCompleteHtml = `<td class="mr"><span class="C"></span></td>`;
        }

        return `<tr>${isCompleteHtml}
                <td class="mr ml">${this.name}</td>
                <td class="ml"><button>D</button></td>
                <td><data>${this.probstotal}</data></td>
                <td><data>${this.probsdone}</data></td>
                    ${numberCorrectHtml}
                <td><data>${this.prescore}</data></td>
                <td><data>12/30/2022</data></td>
                <td><button data-id='${this.id}' class='btnDetail'>View</button></td>
                </tr>`;
    }
}