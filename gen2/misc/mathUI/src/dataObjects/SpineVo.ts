//json atlas animation
export class SpineVo
{
    public charName:string;
    public animation:string;
    public jsonUrl:string;
    public atlasUrl:string;
    public charClass:string;

    constructor(charName:string,animation:string="",charClass:string,jsonUrl:string="",atlasUrl:string="")
    {
        this.charName = charName;
        this.animation = animation;
        this.jsonUrl = jsonUrl;
        this.atlasUrl = atlasUrl;
        this.charClass = charClass;
        
        if (jsonUrl == "")
        {
            this.jsonUrl = "./assets/buddies/" + charName + ".json";
        }
        if (atlasUrl == "")
        {
            this.atlasUrl = "./assets/buddies/" + charName + ".atlas";
        }
    }
}