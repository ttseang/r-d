//data object with the following properties title page
export class SubIndexVo
{
    public title:string;
    public page:number;
    constructor(title:string, page:number)
    {
        this.title = title;
        this.page = page;
    }
}