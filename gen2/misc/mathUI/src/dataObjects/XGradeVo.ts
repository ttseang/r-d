/* //make a data object for the grade with the following properties lesson, total problems, numcompleted,numcorrect,percentscore,lastwork,gradeoneach



export class GradeVo {
    public id:number;
    public lesson: string;
    public totalproblems: number;
    public numcompleted: number;
    public numcorrect: number;
    public percentscore: number;
    public lastwork: string;
    public gradeoneach: boolean;
    public bonus: number;
    public isComplete: boolean;
    
   

    constructor(id:number,lesson: string = "", totalproblems: number = 0, numcompleted: number = 0, numcorrect: number = 0, percentscore: number = 0, lastwork: string = "", gradeoneach: boolean = false, isComplete: boolean = false, bonus: number = 0) {
        this.id=id;
        this.lesson = lesson;
        this.totalproblems = totalproblems;
        this.numcompleted = numcompleted;
        this.numcorrect = numcorrect;
        this.percentscore = percentscore;
        this.lastwork = lastwork;
        this.gradeoneach = gradeoneach;
        this.bonus = bonus;
        this.isComplete = isComplete;
    }
    fillWithRandValues() {
        this.lesson = "lesson" + Math.floor(Math.random() * 100);
        this.totalproblems = Math.floor(Math.random() * 100);
        this.numcompleted = Math.floor(Math.random() * 100);
        this.numcorrect = Math.floor(Math.random() * 100);
        this.percentscore = Math.floor(Math.random() * 100);
        this.lastwork = "12/30/2022";
        this.gradeoneach = Math.random() > 0.5;
        this.bonus = Math.floor(Math.random() * 4);
        this.isComplete = Math.random() > 0.5;
        return this;
    }
    getHtml(): string {

        let numberCorrectHtml: string = `<td>${this.numcorrect}</td>`;
        if (this.bonus > 0) {
            numberCorrectHtml = `<td class="bonus b${this.bonus.toString().trim()}">${this.numcorrect}</td>`;
        }

        let isCompleteHtml: string = `<td class="mr"><span></span></td>`;
        if (this.isComplete) {
            isCompleteHtml = `<td class="mr"><span class="C"></span></td>`;
        }

        return `<tr>${isCompleteHtml}
                <td class="mr ml">${this.lesson}</td>
                <td class="ml"><button>D</button></td>
                <td><data>${this.totalproblems}</data></td>
                <td><data>${this.numcompleted}</data></td>
                    ${numberCorrectHtml}
                <td><data>${this.percentscore}%</data></td>
                <td><data>12/30/2022</data></td>
                <td><button data-id='${this.id}' class='btnDetail'>View</button></td>
                </tr>`


    }
} */