import { SubIndexVo } from "./SubIndexVo";

//data object with the following properties title page subIndexes
export class BookIndexVo
{
    public title:string;
    public page:number;
    public subIndexes:SubIndexVo[];
    constructor(title:string, page:number, subIndexes:SubIndexVo[])
    {
        this.title = title;
        this.page = page;
        this.subIndexes = subIndexes;
    }
}