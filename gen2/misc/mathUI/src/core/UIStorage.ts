import { BookIndexVo } from "dataObjects/BookIndexVo";
import { IndexAnimationVo } from "dataObjects/IndexAnimationVo";
import { GradeVo } from "../dataObjects/GradeVo";

//make a singleton class for the UIStorage
export class UIStorage {
    private static instance: UIStorage | null = null;
    private constructor() { }

    public selectedGrade: GradeVo | null = null;
    public pageIndexMap: Map<string, BookIndexVo[]> = new Map<string, BookIndexVo[]>();

    public indexPagesLoaded: boolean = false;
    public indexAnimationData:IndexAnimationVo[]=[];

    public static getInstance(): UIStorage {
        if (!UIStorage.instance) {
            UIStorage.instance = new UIStorage();
        }
        return UIStorage.instance;
    }
    public indexPage(bookIndexVo: BookIndexVo) {
        //let the key equal the lower case first letter of the title
        let key: string = bookIndexVo.title.charAt(0).toLowerCase();
        //if the key is not in the map
        if (!this.pageIndexMap.has(key)) {
            //create a new array
            let bookIndexVoArray: BookIndexVo[] = [];
            //add the bookIndexVo to the array
            bookIndexVoArray.push(bookIndexVo);
            //add the array to the map
            this.pageIndexMap.set(key, bookIndexVoArray);
        }
        else {
            //get the array from the map
            let bookIndexVoArray: BookIndexVo[] = this.pageIndexMap.get(key);
            //add the bookIndexVo to the array
            bookIndexVoArray.push(bookIndexVo);
            //add the array to the map
            this.pageIndexMap.set(key, bookIndexVoArray);
        }
    }
    public getIndexPagesByLetter(letter: string): BookIndexVo[] {
        //if the key is not in the map
        if (!this.pageIndexMap.has(letter)) {
            //create a new array
            let bookIndexVoArray: BookIndexVo[] = [];
            //add the array to the map
            this.pageIndexMap.set(letter, bookIndexVoArray);
        }
        //get the array from the map
        let bookIndexVoArray: BookIndexVo[] = this.pageIndexMap.get(letter);
        return bookIndexVoArray;
    }
    public checkIfLetterHasPages(letter: string): boolean {
        return this.pageIndexMap.has(letter);
    }
    public getIndexPageCountByLetter(letter: string): number {
        //if the key is not in the map
        if (!this.pageIndexMap.has(letter)) {
            return 0;
        }
        //get the array from the map
        let bookIndexVoArray: BookIndexVo[] = this.pageIndexMap.get(letter);
        return bookIndexVoArray.length;
    }
    public splitToPages(bookIndexVos: BookIndexVo[]): BookIndexVo[][] {
        let pages: BookIndexVo[][] = [];
        let pageIndex: number = 0;
        pages[0] = [];
        //count the index and subIndex pages

        let itemCount: number = 0;

        for (let i = 0; i < bookIndexVos.length; i++) {
            let bookIndexVo: BookIndexVo = bookIndexVos[i];
            itemCount++;
            itemCount += bookIndexVo.subIndexes.length;
            if (itemCount > 32) {
                pageIndex++;
                itemCount = 0;
                pages[pageIndex] = [];
            }
            pages[pageIndex].push(bookIndexVo);
        }
        return pages;
    }
    public getAnimationByLetterAndPage(letter: string, page: number): IndexAnimationVo | undefined {
        //search the array for the letter and page
        for (let i = 0; i < this.indexAnimationData.length; i++) {
            let indexAnimationVo: IndexAnimationVo = this.indexAnimationData[i];
            if (indexAnimationVo.letter == letter && indexAnimationVo.page == page) {
                return indexAnimationVo;
            }
        }
        return undefined;
    }
}