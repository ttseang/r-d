export class Slider {

    private mainCSS: string;
    private index: number;
    private callback: Function;
    private id:string="";

    constructor(mainCSS: string, index: number, callback: Function) {
        this.mainCSS = mainCSS;
        this.index = index;
        this.callback = callback;
    }
    getValue()
    {
        let el:HTMLInputElement | null=document.getElementById(this.id) as HTMLInputElement;
        if (el)
        {
            return el.value;
        }
        return undefined;
    }
    setValue(value:boolean)
    {
        let el:HTMLInputElement | null=document.getElementById(this.id) as HTMLInputElement;
        if (el)
        {
            el.value=value.toString();
        }
    }
    getHtml() {
        this.id=this.mainCSS+"__slider"+this.index.toString();
        return `<label class="${this.mainCSS} switch">
        <input type="checkbox" id=${this.id}>
        <span class="slider round"></span>
      </label>`;
    }
}