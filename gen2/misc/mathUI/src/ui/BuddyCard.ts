import { SpinePlayer } from "@esotericsoftware/spine-player";
import { HtmlObj } from "../core/HTMLObj";
import { BuddyVo } from "../dataObjects/BuddyVo";

export class BuddyCard extends HtmlObj {
    public buddyVo: BuddyVo;
    private clickCallback: Function;
    public nextCard: BuddyCard | null = null;
    public videoLoaded:boolean = false;
    private player: SpinePlayer | null = null;
    private parentElement: HTMLElement;
    constructor(BuddyVo: BuddyVo,parentElement:HTMLElement, callback: Function) {
        super(document.createElement("data"));
        this.buddyVo = BuddyVo;
        this.parentElement = parentElement;
        this.clickCallback = callback;
        //console.log(this.buddyVo);
    }
    render() {
        if (this.el) {

            if (this.buddyVo.isNew == true) {
                this.el.classList.add("new");
            }
            if (this.buddyVo.locked === false) {
                this.el.classList.remove("locked");
                //this.el.style.setProperty("--thumb", "url(" + this.buddyVo.image + ")");
            }
            else {
                this.el.classList.add("locked");
            }

          //  this.el.innerHTML = `<figcaption>${this.buddyVo.title}</figcaption>`;
            this.el.innerHTML=`<figure><figcaption>${this.buddyVo.title}</figcaption></figure>`;
        //    this.el.innerHTML += `<h4 class='new'></h4>`;
            this.el.addEventListener("click", () => { this.clickCallback() });

            // this.loadSpine();

        }

    }
    loadVideo() {
        //console.log("loadVideo");
        if (this.buddyVo.locked) {
            if (this.nextCard) {
                this.nextCard.loadVideo();
            }
            return;
        }
        this.videoLoaded = true;

        const video: HTMLVideoElement = document.createElement("video");
        
        //let videoUrl: string = "./assets/buddies/video/" + this.buddyVo.vthumb + '.mp4';
        //  video.src = this.buddyVo.videoVo.videoUrl;
        video.src = this.buddyVo.vthumb;
        video.autoplay = true;
        video.loop = true;
        video.muted = true;
        video.playsInline = true;

       
       
        video.addEventListener("canplay", () => {
            video.style.opacity = "1";
            if (this.nextCard && this.nextCard.videoLoaded === false) {
                //video.play();
                this.nextCard.loadVideo();
            }
        });
        if (this.el) {
            this.el.prepend(video);
        }
     
    }
    loadSpine() {
        if (this.buddyVo.locked) {
            if (this.nextCard) {
                this.nextCard.loadSpine();
            }
            return;
        }
        const config: any = {
            width: 200,
            height: 200,
            jsonUrl: this.buddyVo.spineVo.jsonUrl,
            atlasUrl: this.buddyVo.spineVo.atlasUrl,
            loop: true,
            autoplay: true,
            scale: 1,
            showControls: false,
            success: this.onSpineLoaded.bind(this),
            animation: this.buddyVo.spineVo.animation,
            alpha: true, // Enable player translucency
            backgroundColor: "#00000000" // Background is fully transparent
        };
        const player: SpinePlayer = new SpinePlayer(this.el, config);

        const canvas: HTMLCanvasElement = player.canvas;
        if (this.buddyVo.spineVo.charClass !== "" && this.buddyVo.spineVo.charClass !== undefined) {
            canvas.classList.add(this.buddyVo.spineVo.charClass);
        }
    }
    onSpineLoaded(player: SpinePlayer) {
        console.log("Spine loaded!");
        console.log(this.nextCard);
        if (this.nextCard) {
            this.nextCard.loadSpine();
        }
    }
    checkIfBelowScreen() {
        if (this.el) {
            const rect = this.el.getBoundingClientRect();
            if (rect.bottom < 0) {
                return true;
            }
        }
        return false;
    }
    checkOutSideFrame() {
        if (this.el) {
            const parentBox: DOMRect = this.parentElement.getBoundingClientRect();
            const rect = this.el.getBoundingClientRect();
           /*  if (rect.top > parentBox.bottom) {
                return true;
            }
            if (rect.bottom < parentBox.top) {
                return true;
            } */
            if (rect.bottom > parentBox.bottom) {
                return true;
            }
            if (rect.top < parentBox.top) {
                return true;
            }
        }
        return false;
    }
    checkVidOnScroll() {
        if (!this.checkOutSideFrame())
        {
            if (this.el) {
                const video: HTMLVideoElement = this.el.querySelector("video");
                if (video) {
                    if (video.paused)
                    {
                        video.play();
                    }
                }
            }
        }
        else
        {
            if (this.el) {
                const video: HTMLVideoElement = this.el.querySelector("video");
                if (video) {
                    if (!video.paused)
                    {
                        video.pause();
                    }
                }
            }
        }
        if (this.nextCard) {
            this.nextCard.checkVidOnScroll();
        }
    }

    destroy() {
        if (this.el) {
            this.el.removeEventListener("click", () => { this.clickCallback() });
        }
    }
    select() {
        if (this.el) {
            this.el.classList.add("on");
        }
    }
    deselect() {
        if (this.el) {
            this.el.classList.remove("on");
        }
    }
}