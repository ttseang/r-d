import { HtmlObj } from "../core/HTMLObj";
import { StickerVo } from "../dataObjects/StickerVo";


export class StickerCard extends HtmlObj {
    public stickerVo: StickerVo;
    private clickCallback: Function;

    constructor(stickerVo: StickerVo, callback: Function) {
        super(document.createElement("figure"));
        this.stickerVo = stickerVo;
        this.clickCallback = callback;
    }
    render() {
        if (this.el) {
            if (this.stickerVo.isNew == true) {
                this.el.classList.add("new");
            }
            else
            {
                this.el.classList.remove("new");
            }           
          
            this.el.innerHTML = `<figcaption>${this.stickerVo.title}</figcaption><s></s>`;

            this.el.addEventListener("click", () => { this.toggle() });

            const thumb:HTMLElement=this.el.querySelector("s");


            if (this.stickerVo.active) {
                this.select();                
            }
            else {
                this.deselect();
            }
            if (this.stickerVo.locked === false) {
                this.el.classList.remove("locked");
                thumb.style.setProperty("--stkr", "url(" + this.stickerVo.image + ")");
            }
            else {
                
                this.el.classList.remove("off");
               // this.el.classList.remove("on");
                this.el.classList.add("locked");
            }
           
        }
    }
    toggle() {

        this.stickerVo.active = !this.stickerVo.active;
        console.log("active: " + this.stickerVo.active);
        if (this.stickerVo.active) {
            this.select();
        }
        else {
            this.deselect();
        }
        this.clickCallback(this.stickerVo);
    }
    destroy() {
        if (this.el) {
            this.el.removeEventListener("click", () => { this.toggle() });
        }
    }
    select() {
        if (this.el) {
          //  this.el.classList.add("on");
            this.el.classList.remove("off");
        }
    }
    deselect() {
        if (this.el) {
          //  this.el.classList.remove("on");
            this.el.classList.add("off");
        }
    }
}