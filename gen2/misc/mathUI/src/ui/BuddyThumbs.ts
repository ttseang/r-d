import { BuddyVo } from "dataObjects/BuddyVo";
import { HtmlObj } from "../core/HTMLObj";
import { BuddyCard } from "./BuddyCard";
import { BuddyPreview } from "./BuddyPreview";

export class BuddyThumbs extends HtmlObj {
    private buddyVos: BuddyVo[];
    private callback: Function;
    private buddyCards: BuddyCard[] = [];
    private selectedIndex: number = 0;
    constructor(buddyVos: BuddyVo[], callback: Function) {
        super(document.querySelector(".buddythumbnails"));
        this.buddyVos = buddyVos;
        this.callback = callback;
        this.makeCards();
        (window as any).buddyThumbs = this;
    }
    private makeCards() {


        for (let i: number = 0; i < this.buddyVos.length; i++) {
            let buddyCard: BuddyCard = new BuddyCard(this.buddyVos[i],this.el, () => { this.cardClicked(i) });

            buddyCard.render();
            if (this.el) {
                if (buddyCard.el) {
                    this.el.appendChild(buddyCard.el);
                }
            }
            if (this.buddyCards.length > 0) {
                this.buddyCards[i - 1].nextCard = buddyCard;
            }
            this.buddyCards.push(buddyCard);

        }
        // console.log(this.buddyCards);
        //  this.buddyCards[0].loadSpine();
        this.buddyCards[0].loadVideo();
       // this.buddyCards[this.selectedIndex].select();
        
        setTimeout(() => {
            this.checkVidOnScroll();
        }, 2000);
    }
    public deselect() {
        this.buddyCards[this.selectedIndex].deselect();
    }
    public checkVidOnScroll()
    {
        this.buddyCards[0].checkVidOnScroll();
    }
    private cardClicked(index: number) {
        this.buddyCards[this.selectedIndex].deselect();
        this.buddyCards[index].select();
        this.selectedIndex = index;
        this.callback(this.buddyCards[index].buddyVo);
        
    }
    
}