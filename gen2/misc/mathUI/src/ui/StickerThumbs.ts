import { HtmlObj } from "../core/HTMLObj";
import { StickerVo } from "../dataObjects/StickerVo";
import { StickerCard } from "./StickerCard";


export class StickerThumbs extends HtmlObj {
    private stickerVos: StickerVo[];
    private callback: Function;
    private stickerCards: StickerCard[] = [];
    private selectedIndex: number = 0;

    constructor(stickerVos: StickerVo[], callback: Function) {
        super(document.querySelector(".stickers"));
        this.stickerVos = stickerVos;
        this.callback = callback;
        this.makeCards();
    }
    public showAll() {
        this.stickerCards.forEach((card) => {
            card.select();
            card.stickerVo.active = true;
        });
    }
    public hideAll() {
        this.stickerCards.forEach((card) => {
            card.deselect();
            card.stickerVo.active = false;
        });
    }
    private makeCards() {
        for (let i: number = 0; i < this.stickerVos.length; i++) {
            let stickerCard: StickerCard = new StickerCard(this.stickerVos[i], this.cardClicked.bind(this));
            stickerCard.render();
            if (this.el) {
                if (stickerCard.el) {
                    this.el.appendChild(stickerCard.el);
                }
            }
            this.stickerCards.push(stickerCard);
        }
        this.stickerCards[this.selectedIndex].select();
    }
    private cardClicked(stickerVo:StickerVo) {
        this.callback(stickerVo);
    }
}