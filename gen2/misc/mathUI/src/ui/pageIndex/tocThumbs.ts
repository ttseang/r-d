import { HtmlObj } from "../../core/HTMLObj";
import { BookTocVo } from "../../dataObjects/BookTocVo";
import { TocCard } from "./tocCard";


export class TocThumbs extends HtmlObj {
    private thumbData: BookTocVo[];
    private page: number = 0;

    private nextArrow: HTMLButtonElement | null = null;
    private prevArrow: HTMLButtonElement | null = null;

    private nextDisabled: boolean = false;
    private prevDisabled: boolean = false;

    constructor(thumbData: BookTocVo[]) {
        super(document.createElement("menu"));
        // this.el.classList.add("toc");
        this.thumbData = thumbData;
        this.makeThumbs();

        //select the 1st button of main>nav
        const prevArrow: HTMLButtonElement | null = document.querySelector(".navButtons>button");
        //select the 2nd button of main>nav
        const nextarrow: HTMLButtonElement | null = document.querySelector(".navButtons>button:nth-child(2)");
        this.setArrows(nextarrow, prevArrow);


    }
    setArrows(nextarrow: HTMLButtonElement, prevArrow: HTMLButtonElement): void {
        //console.log("nextarrow", nextarrow);
      //  console.log("prevArrow", prevArrow);


        this.nextArrow = nextarrow;
        this.prevArrow = prevArrow;
        this.nextArrow.addEventListener("click", () => { this.nextPage() });
        this.prevArrow.addEventListener("click", () => { this.prevPage() });

        this.checkForLimits();
    }
    hideArrowButtons(): void {
        if (!this.nextArrow || !this.prevArrow) return;
        this.nextArrow.classList.add("disabled");
        this.prevArrow.classList.add("disabled");
    }
    showArrowButtons(): void {
        if (!this.nextArrow || !this.prevArrow) return;
        this.nextArrow.classList.remove("disabled");
        this.prevArrow.classList.remove("disabled");
    }
    nextPage(): void {
        console.log("nextPage");
        if (this.nextDisabled) return;

        this.page++;
        this.makeThumbs();
        this.checkForLimits();
    }
    prevPage(): void {
        if (this.prevDisabled) return;
        this.page--;
        this.makeThumbs();
        this.checkForLimits();
    }
    checkForLimits(): void {

        if (this.page == 0) {
            this.prevArrow.classList.add("disabled");
            this.prevDisabled = true;
        }
        else {
            this.prevArrow.classList.remove("disabled");
            this.prevDisabled = false;
        }
        if ((this.page + 1) * 12 > this.thumbData.length) {
            this.nextArrow.classList.add("disabled");
            this.nextDisabled = true;
        }
        else {
            this.nextArrow.classList.remove("disabled");
            this.nextDisabled = false;
        }
    }
    render() {
        this.el.innerHTML = `<nav>
        <button>&#x2B9C;Previous</button>
        <button>Next&#x2B9E;</button></nav>`;
    }
    makeThumbs(): void {

        //remove all children
        while (this.el.firstChild) {
            this.el.removeChild(this.el.firstChild);
        }

        let start: number = 12 * this.page;
        let end: number = start + 12;
        /*  if (end > this.thumbData.length) {
             end = this.thumbData.length;
         } */
        for (let i: number = start; i < end; i++) {
            let thumbData: BookTocVo | null = null;
            if (i < this.thumbData.length) {
                thumbData = this.thumbData[i];
            }


            let tocCard: TocCard = new TocCard(thumbData);
            tocCard.render();
            this.el.appendChild(tocCard.el);
        }
    }

}