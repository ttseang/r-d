import { HtmlObj } from "../../core/HTMLObj";
import { BookTocVo } from "../../dataObjects/BookTocVo";

export class TocCard extends HtmlObj
{
    private thumbData:BookTocVo | null;
    constructor(thumbData:BookTocVo | null)
    {
        super(document.createElement("li"));
       // this.el.classList.add("indexCard");
        this.thumbData = thumbData;
    }
    render()
    {
        if (this.thumbData == null)
        {
            this.el.innerHTML = `<p></p>`;
            return;
        }
        let title:string = this.thumbData.title;
        //if title doesn't include the word quiz, add lesson number to title
        if (title.indexOf("Quiz") == -1)
        {
            title = "Lesson " + this.thumbData.lesson + ": " + title;
        }
        this.el.innerHTML = `<p><small>LESSON</small><b>${this.thumbData.lesson}</b></p><h3>${title}</h3><p><small>PAGE</small><b>${this.thumbData.page}</b></p>`;
    }
}