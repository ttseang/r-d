import { TTScrollbar } from "@teachingtextbooks/ttscrollbar";
import { Pages } from "../constants/PageConstants";
import { StickerVo } from "../dataObjects/StickerVo";

import { IPage } from "../interfaces/IPage";
import { StickerThumbs } from "../ui/StickerThumbs";



import { BasePage } from "./BasePage"

export class StickerBookPage extends BasePage implements IPage {

    private backButton: HTMLButtonElement | null = null;
    private stickerThumbs: StickerThumbs | null = null;
    private stickerVos: StickerVo[] = [];


    private ttScrollBar: TTScrollbar | null = null;

    constructor(pageID: number, callback: Function) {
        super(pageID, callback);
    }
    protected setReferences(): void {
        this.backButton = document.querySelector(".tempbutton");
    }
    protected makeChildElements() {
        this.loadStickerData();
    }
    protected setUpEventListeners(): void {
        if (this.backButton) {
            this.backButton.addEventListener("click", () => { this.callback(Pages.Actions.ChangePage, Pages.Page.Home) });
        }

        const stickerSwitch: HTMLInputElement | null = document.querySelector("#stickerSwitch");
        if (stickerSwitch) {
            stickerSwitch.addEventListener("change", () => {
                if (this.stickerThumbs) {
                    if (stickerSwitch.checked) {
                        this.stickerThumbs.showAll();
                    } else {
                        this.stickerThumbs.hideAll();
                    }
                }
            });
        }
    }

    /**
     * load the json data for the stickers
     */
    loadStickerData() {
        fetch("./assets/stickers.json")
            .then(response => response.json())
            .then(data => this.stickerDataLoaded(data));
    }
    /**
     * when the stickers json data is loaded convert it to stickerVo objects
     * @param data 
     */
    stickerDataLoaded(data: any) {
        for (let i: number = 1; i < 100; i++) {
            //let paperData: any = data[i];
            // let image: string = "https://ttv4.s3.amazonaws.com/sticker/WYB02.gif";


            let index: string = i.toString();
            while (index.length < 4) {
                index = "0" + index;
            }
            const stickerName: string = "YG" + index;
         //   const stickerName:string="YN0277";
            // https://ttv4.s3.amazonaws.com/sticker/TB0001.gif
            //YG0027,YG0037,YG0066,YG0077,YG0090,YN0105,YN0277,YN0610,YN0612,YN0754,

            let image: string = "https://ttv4.s3.amazonaws.com/sticker/" + stickerName + ".gif"

            let stickerVo: StickerVo = new StickerVo("test", "Title", image);
            if (i > 90) {
                stickerVo.locked = true;
            }
            if (i<10){
                stickerVo.isNew=true;
            }
            this.stickerVos.push(stickerVo);
        }

        //now that we have the data, we can make the thumbnails


        //the stickerThumbs is the line of thumbnails containing the stickers
        this.stickerThumbs = new StickerThumbs(this.stickerVos, this.chooseSticker.bind(this));

        (window as any).stickerThumbs = this.stickerThumbs;
        const scrollbarHolder: HTMLElement | null = document.querySelector(".scrollbarHolder");
        if (scrollbarHolder) {
            if (this.stickerThumbs.el) {

                this.ttScrollBar = new TTScrollbar(this.stickerThumbs.el, scrollbarHolder, this.updateScroll.bind(this));
                this.ttScrollBar.addCssToHead();
                this.ttScrollBar.setImage("track", "./assets/scroll/scrollback.png");
                this.ttScrollBar.setImage("knob", "./assets/scroll/thumbScroll.png");
                this.ttScrollBar.setImage("btnUp", "./assets/scroll/arrowUp.png");
                this.ttScrollBar.setImage("btnDown", "./assets/scroll/arrowDown.png");
            }
        }
    }
    chooseSticker(stickerVo: StickerVo) {
        console.log("sticker chosen", stickerVo);
        const stickerSwitch: HTMLInputElement | null = document.querySelector("#stickerSwitch");
        if (stickerSwitch) {
            if (!stickerSwitch.checked && stickerVo.active) {
                stickerSwitch.checked = true;
            }
        }
        //for later implementation
    }
    setInactive(): void {
        const body: HTMLElement | null = document.querySelector("body");
        if (body) {
            body.classList.remove("stickerbook");
        }
    }
    updateScroll(percent: number) {
        if (this.stickerThumbs) {
            if (this.stickerThumbs.el) {
                const scrollHeight: number = this.stickerThumbs.el.scrollHeight - this.stickerThumbs.el.clientHeight;
                const px: number = scrollHeight * percent / 100;
                this.stickerThumbs.el.scrollTo(0, px);
            }
        }
    }

    //later we need to update the database or send a signal to the main app when a sticker is selected
    updateDatabase() {
        console.log("update database");
        //for later implementation

    }
    render(): void {
        super.render();
        const body: HTMLElement | null = document.querySelector("body");
        if (body) {
            body.classList.add("stickerbook");
            body.innerHTML=`<header>
			<button type="button" class="tempbutton" onclick="location.assign('index.html');">Back</button>
			<nav class="on">Sticker Book</nav>
		</header>
		<main>
			<article>
				Here are your stickers. Every time you answer a problem, you&#x2019;ll see one of these, and you&#x2019;ll get more as you work your way through the course.<br />If you don&#x2019;t like a sticker, just tap it to turn it off, and if you don&#x2019;t want stickers, tap the &#x201C;All Stickers&#x201D; button. You can always turn stickers back&#xA0;on&#xA0;again if you change your mind.
			</article>
			<div class="stickers">
            </div>
            </main> <div class="scrollbarHolder"></div>`;

            /* body.innerHTML = ` <header>
			<button type="button" class="tempbutton">Back</button>
            <h2>Sticker Book</h2>
            <div class='allSwitch'>
            <div>
            <input type="checkbox" id="stickerSwitch"checked />
            <label for="stickerSwitch"></label>
            <span class="status"></span>
            </div>
            <h3>All Stickers</h3>
            </div>            
		</header>
    <main>
        <article>
        Here are your stickers. Every time you answer a problem, you'll see one of these, and you’ll yet more as you work your way through the course If you don’t like a sticker, just tap it to turn it off, and if you don’t want stickers, tap the -All Stickers” button, you can always turn stickers back on again if you change your mind.
        </article>
       
          <div class="stickerthumbnails">
          </div>
        
        <div class="scrollbarHolder"></div>
    </main>`; */
        }
    }
}
