import { Pages } from "../constants/PageConstants";
import { IPage } from "../interfaces/IPage";

export class BasePage implements IPage
{
    protected callback:Function;
    protected pageID:number;
    protected active:boolean=false;

    constructor(pageID:number,callback:Function) {
        this.callback = callback;
        this.pageID = pageID;
    }
    setActive(): void {

        //IMPORTANT 
        //don't add or change code here, use the called methods instead
        //this order is important for the page to work correctly

        this.active = true;

        //set up any data that needs to be in place before the page is shown
        this.setData();

        //render the page
        this.render();

        //set up javascript references to elements after they are rendered
        this.setReferences();

        //make custom child elements
        this.makeChildElements();

        //set up event listeners
        this.setUpEventListeners();
    }
    protected setData(): void {
        
    }
    protected setReferences(): void {
        
    }
    protected makeChildElements(): void {
        
    }
    protected setUpEventListeners(): void {
        
    }
    protected  goHome()
    {
        this.callback(Pages.Actions.ChangePage,Pages.Page.Home);
    }
    setInactive(): void {
        this.active=false;
    }
    
    render(): void {
        
       
    }
   
    getPageID(): number {
        return this.pageID;
    }
}