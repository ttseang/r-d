import { Pages } from "../constants/PageConstants";
import { SoundVo } from "../dataObjects/SoundVo";
import { IPage } from "../interfaces/IPage";
import { Soundbar } from "../ui/soundbar";
import { BasePage } from "./BasePage";

export class SettingsPage extends BasePage implements IPage {

    private backButton: HTMLButtonElement | null = null;
    private sounds: SoundVo[] = [];

    constructor(pageID: number, callback: Function) {
        super(pageID, callback);
        this.callback = callback;
        this.pageID = pageID;

        (window as any).settings = this;
        this.sounds.push(new SoundVo(1, "Squeaky Toy", "https://ttv5.s3.amazonaws.com/snd/fx/FX00001a.mp3", true));
        this.sounds.push(new SoundVo(2, "Rising Tones", "https://ttv5.s3.amazonaws.com/snd/fx/FX00001c.mp3", false));
        this.sounds.push(new SoundVo(3, "Short Swipe", "https://ttv5.s3.amazonaws.com/snd/fx/FX00001e.mp3", true));
        this.sounds.push(new SoundVo(4, "Gulp", "https://ttv5.s3.amazonaws.com/snd/fx/FX000020.mp3", false));
        this.sounds.push(new SoundVo(5, "Rising Wind Chime", "https://ttv5.s3.amazonaws.com/snd/fx/FX000022.mp3", true));
        this.sounds.push(new SoundVo(6, "Bike Bell", "https://ttv5.s3.amazonaws.com/snd/fx/FX000024.mp3", false));
        this.sounds.push(new SoundVo(7, "Cartoon Laugh", "https://ttv5.s3.amazonaws.com/snd/fx/FX000026.mp3", true));
        this.sounds.push(new SoundVo(8, "Status Boost", "https://ttv5.s3.amazonaws.com/snd/fx/FX000028.mp3", true));
        this.sounds.push(new SoundVo(8, "Cheeky Warm Tones", "https://ttv5.s3.amazonaws.com/snd/fx/FX00002a.mp3", true));
        this.sounds.push(new SoundVo(8, "Service Bell", "https://ttv5.s3.amazonaws.com/snd/fx/FX00002c.mp3", true));
    }
    protected setReferences(): void {
        this.backButton = document.querySelector(".tempbutton");
    }
    protected makeChildElements() {
        //throw new Error("Method not implemented.");
        this.makeSoundBars();
    }
    protected setUpEventListeners(): void {
        if (this.backButton) {
            this.backButton.addEventListener("click", () => { this.callback(Pages.Actions.ChangePage, Pages.Page.Home) });
        }

        let soundButtons: NodeListOf<HTMLButtonElement> = document.querySelectorAll(".soundButton");
        soundButtons.forEach((button: HTMLButtonElement) => {         

            button.addEventListener("click", () => {
                let index:number=parseInt(button.dataset.index || "0");                
                let soundVo: SoundVo = this.sounds[index];
                this.playSound(soundVo);
            });
        });
    }
    setInactive(): void {
        super.setInactive();
        //throw new Error("Method not implemented.");
    }
    private makeSoundBars() {
        const soundBarContainer: HTMLDivElement = document.querySelector(".soundBarContainer") as HTMLDivElement;
        for (let i = 0; i < this.sounds.length; i++) {
            const soundBar: Soundbar = new Soundbar(i, this.sounds[i]);
            soundBarContainer.appendChild(soundBar.el);
        }
    }
    private playSound(soundVo: SoundVo) {
        const audio: HTMLAudioElement = document.createElement("audio") as HTMLAudioElement;
        audio.src = soundVo.file;
        audio.play();
    }
    render(): void {

        const body: HTMLElement = document.querySelector("body") as HTMLElement;
        body.classList.add("corky");
        body.classList.add("uisettings");
        //
        //
        //
        body.innerHTML = `<header class='headerbackbutton'>
        <button type="button" class="tempbutton">Back</button>
        <h1 title="Hint Settings"></h1>
        <b>&#xA0;</b></header><main><div class='card1'><div class='soundBarContainer'></div></div></main>`;
    }
}