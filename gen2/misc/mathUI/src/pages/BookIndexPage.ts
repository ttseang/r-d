import { BookTocVo } from "../dataObjects/BookTocVo";

import { TabVo } from "../dataObjects/TabVo";
import { TabLine } from "../ui/TabLine";
import { BasePage } from "./BasePage";
import { TocThumbs } from "../ui/pageIndex/tocThumbs";
import { BookIndexVo } from "../dataObjects/BookIndexVo";
import { SubIndexVo } from "../dataObjects/SubIndexVo";
import { UIStorage } from "../core/UIStorage";
import { IndexThumbs } from "../ui/pageIndex/indexThumbs";
import { IndexAnimationVo } from "../dataObjects/IndexAnimationVo";


export class BookIndexPage extends BasePage {
    public callback: Function;
    public pageID: number;
    private tabLine: TabLine;
    private tabsVo: TabVo[] = [];
    private bookTocVos: BookTocVo[] = [];
   // private bookIndexVos: BookIndexVo[] = [];
    private uiStorage: UIStorage = UIStorage.getInstance();

    private tocThumbs: TocThumbs | null = null;
    private indexThumbs: IndexThumbs | null = null;

    constructor(pageID: number, callback: Function) {
        super(pageID, callback);
        this.callback = callback;
        this.pageID = pageID;
        (window as any).detailPage = this;
    }
    protected setData(): void {
        this.loadTabData();
    }
    /**
      *load the json data for the tabs
      *
      * @memberof WallpaperMain
      */
    loadTabData() {
        fetch("./assets/indexTabs.json")
            .then(response => response.json())
            .then(data => this.tabsloaded({ data }));
    }
    /**
     *make the json data into a TabVo array
     *when the tabs are loaded, we need to load the wallpapers
     * @param {*} data
     * @memberof WallpaperMain
     */

    tabsloaded(data: any) {
        //map data to tabVo objects
        for (let i: number = 0; i < data.data.tabs.length; i++) {
            let tabVo: TabVo = new TabVo(data.data.tabs[i].title, data.data.tabs[i].description);
            this.tabsVo.push(tabVo);
        }

        this.loadTocData();
        this.makeTabs();
        this.tabClicked(0);
    }
    private loadTocData(): void {

        fetch("./assets/pageToc.json")
            .then(response => response.json())
            .then(data => this.tocLoaded({ data }));
    }
    private tocLoaded(data: any): void {
       
        let contents: any = data.data.contents;
        //map data to bookTocVo objects
        for (let i: number = 0; i < contents.length; i++) {
            let bookTocVo: BookTocVo = new BookTocVo(contents[i].title, contents[i].page, contents[i].lesson);
            this.bookTocVos.push(bookTocVo);
        }
        //  console.log("bookTocVos", this.bookTocVos);
        this.loadIndexData();
    }
    private loadIndexData(): void {
        if (this.uiStorage.indexPagesLoaded) {
            this.makeThumbs();
            return;
        }
        fetch("./assets/indexData.json")
            .then(response => response.json())
            .then(data => this.indexLoaded({ data }));
    }
    private indexLoaded(data: any): void {

        let contents: any = data.data;

        //map data to bookIndexVo objects

        for (let i: number = 0; i < contents.length; i++) {
            let bookIndexVo: BookIndexVo = new BookIndexVo(contents[i].title, contents[i].page, []);

            //get subpages
            let subPages: any = contents[i].subIndexes;

            for (let j: number = 0; j < subPages.length; j++) {
                let subIndexVo: SubIndexVo = new SubIndexVo(subPages[j].title, subPages[j].page);
                bookIndexVo.subIndexes.push(subIndexVo);
            }
           // this.bookIndexVos.push(bookIndexVo);
            this.uiStorage.indexPage(bookIndexVo);
        }
        this.uiStorage.indexPagesLoaded = true;
        //   console.log("bookIndexVos", this.bookIndexVos);
       this.loadAnimaitonData();
    }
    private loadAnimaitonData(): void {
        fetch("./assets/indexAnimations.json")
        .then(response => response.json())
        .then(data => this.animationLoaded({ data }));
    }
    private animationLoaded(data: any): void {
        //console.log("animationLoaded", data);
        let animationData:IndexAnimationVo[] = [];
        let contents: any = data.data;
        for (let i: number = 0; i < contents.length; i++) {
            let animationVo: IndexAnimationVo = new IndexAnimationVo(contents[i].letter,contents[i].page, contents[i].animation, contents[i].x, contents[i].y,contents[i].w,contents[i].h);
            animationData.push(animationVo);
        }
        this.uiStorage.indexAnimationData = animationData;
        this.makeThumbs();
    }
    protected makeChildElements(): void {


    }
    private makeTabs(): void {

        this.tabLine = new TabLine("header", (index: number) => { this.tabClicked(index) }, this.tabsVo);
        this.tabLine.el.classList.add("toc");
        this.tabLine.showTab(0);
    }
    private makeThumbs(): void {
        

        this.tocThumbs = new TocThumbs(this.bookTocVos);
        this.indexThumbs = new IndexThumbs();
        this.indexThumbs.hideArrowButtons();

        const main: HTMLElement | null = document.querySelector("main");
        if (main) {
            main.appendChild(this.tocThumbs.el);
            
        }
        

    }
    private tabClicked(index: number): void {
        console.log("tabClicked", index);
        const body: HTMLElement | null = document.querySelector("body");
        const main: HTMLElement | null = document.querySelector("main");
        const letterTabs: HTMLElement | null = document.querySelector("main>nav>ul");
        console.log("letterTabs", letterTabs);
        if (index == 0) {
            if (body) {
               
                body.classList.add("toc");
                body.classList.remove("indexes");
            }
            if (main) {
                if (this.tocThumbs)
                {
                    main.appendChild(this.tocThumbs.el);
                    this.tocThumbs.showArrowButtons();
                }
                
                if (this.indexThumbs)
                {
                    main.removeChild(this.indexThumbs.el);
                    this.indexThumbs.hideArrowButtons();
                }
                letterTabs.classList.add("hid");
            }
            //this.thumbs.visible = true;
            
           // this.indexThumbs.visible = false;
        }
        else {
            if (body) {               
                body.classList.add("indexes");
                body.classList.remove("toc");
            }
            if (main) {
                main.appendChild(this.indexThumbs.el);
                this.tocThumbs.hideArrowButtons();
                this.indexThumbs.checkForLimits();
                letterTabs.classList.remove("hid");
            }
            if (this.tocThumbs)
            {
                if (main)
                {
                    main.removeChild(this.tocThumbs.el);
                }
              
                this.tocThumbs.hideArrowButtons();
            }
            
        }
    }
    protected setElementReferences(): void {

    }
    protected addEventListeners(): void {

    }
    protected removeEventListeners(): void {

    }
    protected updateScroll(): void {

    }
    protected update(): void {

    }
    protected destroy(): void {

    }
    public render(): void {
        const body: HTMLElement | null = document.querySelector("body");
        if (body) {       
            body.classList.add("ebook"); 
            body.innerHTML = `<header><button type="button" class="tempbutton">Back</button></header>
            <main>
            <nav>            
            <ul class="letterTabs"></ul>
            </nav>
            <div class="navButtons">
                <button>&#x2B9C;Previous</button>
                <button>Next&#x2B9E;</button>
                <button>&#x2B9C;Previous Index</button>
                <button>Next Index&#x2B9E;</button>
            </div>
            </main>
       `;
        }
    }
}