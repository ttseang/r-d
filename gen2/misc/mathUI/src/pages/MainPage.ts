
import { Pages } from "../constants/PageConstants";
import { IPage } from "../interfaces/IPage";
import { BasePage } from "./BasePage";

export class MainPage extends BasePage implements IPage {
    

    private mainElement: HTMLElement | null = null;
    constructor(pageID: number, callback: Function) {
        super(pageID, callback);
        this.callback = callback;
        this.pageID = pageID;
        (window as any).mainPage = this;
    }

    protected setReferences(): void {
        this.mainElement = document.querySelector("main") as HTMLElement;
    }
    protected makeChildElements() {
        this.makeThumbs();
    }
    protected setUpEventListeners(): void {
        //throw new Error("Method not implemented.");
    }
    setInactive(): void {
        super.setInactive();
        const body: HTMLElement = document.querySelector("body") as HTMLElement;
        body.classList.remove("corky");
        body.classList.remove("dashboard");
        body.innerHTML = "";
        //remove all event listeners
        //throw new Error("Method not implemented.");
    }
    update(): void {
        throw new Error("Method not implemented.");
    }
    private makeThumbs() {
        //temporary data for testing
        let thumbs: any[] = [{ "name": "View Ebook", "css": "", "id": "0" },
                             { "name": "Work on a Lesson", "css": "", "id": "0"},
                             { "name": "View Gradebook", "css": "", "id": Pages.Page.Grades },
                             { "name": "Wallpaper", "css": "wallpaper", "id": Pages.Page.Wallpaper },
                             { "name": "Sticker Book", "css": "", "id": Pages.Page.Stickers},
                             { "name": "Settings", "css": "", "id": Pages.Page.Settings }];
        for (let i = 0; i < thumbs.length; i++) {
            let thumb: HTMLElement = document.createElement("article");

            thumb.classList.add("framebtn");

            if (thumbs[i].css != "") {
                thumb.classList.add(thumbs[i].css);
            }
            thumb.id = thumbs[i].id;
            thumb.setAttribute("title", thumbs[i].name);
            thumb.innerHTML = `<hr />
            <h2 title="${thumbs[i].name}"></h2>
            <figure></figure>`;
            thumb.addEventListener("click", () => { this.callback(Pages.Actions.ChangePage, thumbs[i].id) });
            if (this.mainElement) {
                this.mainElement.appendChild(thumb);
            }
        }

    }
    render(): void {
        const body: HTMLElement = document.querySelector("body") as HTMLElement;
       
        body.classList.add("corky");
        body.classList.add("dashboard");
        body.innerHTML = `<header><b>&#xA0;</b>
        <h1 title="Math App Layout"></h1>
        <b>&#xA0;</b>
        </header><main></main>`;

    }
    getPageID(): number {
        return this.pageID;
    }

}