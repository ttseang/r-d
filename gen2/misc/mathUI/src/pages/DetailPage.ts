import { TTScrollbar } from "@teachingtextbooks/ttscrollbar";
import { Pages } from "../constants/PageConstants";
import { UIStorage } from "../core/UIStorage";
import { GradeDetailVo } from "../dataObjects/GradeDetailVo";
import { GradeVo } from "../dataObjects/GradeVo";
import { ProblemVo } from "../dataObjects/ProblemVo";
import { IPage } from "../interfaces/IPage";
import { BasePage } from "./BasePage";

export class DetailPage extends BasePage implements IPage {
    private mainElement: HTMLElement | null = null;
    private backButton: HTMLButtonElement | null = null;
    private ttScrollBar: TTScrollbar | null = null;
    private tableScroller: HTMLDivElement | null = null;
    private table: HTMLTableElement | null = null;
    private gradeDetails: ProblemVo[] | null = null;
    private grade: GradeVo | null = null;
    private ms: UIStorage = UIStorage.getInstance();
    private lessonName: string = "Lesson";

    constructor(pageID: number, callback: Function) {
        super(pageID, callback);
        this.callback = callback;
        this.pageID = pageID;
        (window as any).detailPage = this;
    }
    protected setData(): void {
        this.grade = this.ms.selectedGrade;
        if (this.grade) {
            this.gradeDetails = this.grade.problems;
            this.lessonName = this.grade.name;
        }
    }
    protected makeChildElements(): void {

        // const tableGrid: HTMLDivElement = document.querySelector(".tableGrid") as HTMLDivElement;
        const scrollbarHolder: HTMLDivElement = document.querySelector(".scrollbarHolder") as HTMLDivElement;

        this.tableScroller = document.querySelector(".scroller") as HTMLDivElement;
        const table: HTMLTableElement = document.createElement("table");
        const tbody: HTMLTableSectionElement = document.createElement("tbody");

        this.tableScroller.appendChild(table);
        table.appendChild(tbody);

        /* for (let i = 0; i < 100; i++) {
            let detailVo:GradeDetailVo=new GradeDetailVo(i);
            detailVo.fillWithRandValues();
            tbody.innerHTML += detailVo.getHtml();
        } */
        if (this.gradeDetails && this.grade) {
            for (let i: number = 0; i < this.gradeDetails.length; i++) {
                const problemVo: ProblemVo = this.gradeDetails[i];
                tbody.innerHTML += problemVo.getHtml();
            }
        }
        if (scrollbarHolder && this.tableScroller && table) {
            this.ttScrollBar = new TTScrollbar(this.tableScroller, null, this.updateScroll.bind(this));
            this.ttScrollBar.addCssToHead();
            this.ttScrollBar.setImage("track", "./assets/scroll/scrollback.png");
            this.ttScrollBar.setImage("knob", "./assets/scroll/thumbScroll.png");
            this.ttScrollBar.setImage("btnUp", "./assets/scroll/arrowUp.png");
            this.ttScrollBar.setImage("btnDown", "./assets/scroll/arrowDown.png");

            this.ttScrollBar.element.style.maxHeight = "100%";
            this.ttScrollBar.debug = false;

            this.ttScrollBar.appendTo(scrollbarHolder);
        }
    }
    updateScroll(percent: number) {
        if (this.tableScroller) {
            const scrollHeight: number = this.tableScroller.scrollHeight - this.tableScroller.clientHeight;
            const px: number = scrollHeight * percent / 100;
            this.tableScroller.scrollTo(0, px);
        }
    }
    protected setReferences(): void {
        this.mainElement = document.querySelector("main") as HTMLElement;
        this.backButton = document.querySelector(".tempbutton");
    }
    protected setUpEventListeners(): void {
        if (this.backButton) {
            this.backButton.addEventListener("click", () => { this.callback(Pages.Actions.ChangePage, Pages.Page.Grades) });
        }
    }
    setInactive(): void {
        super.setInactive();
        const body: HTMLElement = document.querySelector("body") as HTMLElement;
        body.classList.remove("gradebook");
        body.classList.remove("details");
        body.classList.remove("corky");
        body.innerHTML = "";
        //remove all event listeners

        //throw new Error("Method not implemented.");
    }
    render(): void {
        const body: HTMLElement = document.querySelector("body") as HTMLElement;
        body.classList.add("gradebook");
        body.classList.add("details");
        body.classList.add("corky");

        body.innerHTML = `<header>
        <button type="button" class="tempbutton">Back</button>
        <h1 title="Math Gradebook Details"></h1>
        <b>&#xA0;</b>
    </header>
    <main>
        
        <table>
            <thead>
                <tr>
                    <th class="mr"></th>
                    <th class="mr ml">${this.lessonName} Details</th>
                    <th class="ml"></th>
                    <th>Viewed Hint?</th>
                    <th>Used 2nd Chance?</th>
                    <th>Viewed Solution?</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="6">
                        <div class="scroller">
                           
                        </div>
                    </td>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="6">Summary</th>
                </tr>
                <tr>
                    <th colspan="4">Last Work Done On:</th>
                    <td colspan="2">12/22/2022</td>
                </tr>
                <tr>
                    <th colspan="4">Raw Score:</th>
                    <td colspan="2">22/22</td>
                </tr>
                <tr>
                    <th colspan="4">Percentage Score <small>(without bonus)</small>:</th>
                    <td colspan="2">100%</td>
                </tr>
                <tr>
                    <th colspan="4">Bonus Points:</th>
                    <td colspan="2">+4</td>
                </tr>
                <tr>
                    <th colspan="4">Final Percentage Score:</th>
                    <td colspan="2">108%</td>
                </tr>
            </tfoot>
        </table>
        <div class="scrollbarHolder"></div>
    </main>
    <menu class="gblegend">
        <li class="correct">Correct</li>
        <li class="incorrect">Incorrect</li>
        <li class="incomplete">Incomplete</li>
        <li class="edit">Edit Grade</li>
    </menu>`;

    }


}