import { TTScrollbar } from "@teachingtextbooks/ttscrollbar";

import { BuddyVo } from "../dataObjects/BuddyVo";
import { BuddyThumbs } from "../ui/BuddyThumbs";

import { Pages } from "../constants/PageConstants";
import { TabVo } from "../dataObjects/TabVo";
import { WallpaperVo } from "../dataObjects/WallpaperVo";
import { IPage } from "../interfaces/IPage";
import { TabLine } from "../ui/TabLine";
import { WallpaperThumbs } from "../ui/WallpaperThumbs";
import { BasePage } from "./BasePage";
import { BuddyPreview } from "../ui/BuddyPreview";

export class WallpaperPage extends BasePage implements IPage {

    private backButton: HTMLButtonElement | null = null;
    private wallpaperThumbs: WallpaperThumbs | null = null;
    private wallpaperVos: WallpaperVo[] = [];
    
    private selectedWallpaperVo: WallpaperVo | null = null;    
    private selectedBuddyVo: BuddyVo | null = null;

    private tabsVo: TabVo[] = [];
    private tabLine: TabLine | null = null;
   // private scrollGroup: HTMLElement | null = null;
   // private buddyScrollGroup: HTMLElement | null = null;
    private ttScrollBar: TTScrollbar | null = null;
    private ttScrollBar2: TTScrollbar | null = null;

    private buddyThumbs: BuddyThumbs | null = null;
    private tabIndex: number = 1;

    constructor(pageID: number, callback: Function) {
        super(pageID, callback);
    }
    protected setReferences(): void {
        this.backButton = document.querySelector(".tempbutton");
        // this.scrollGroup = document.querySelector(".wpscrollgroup") as HTMLElement;

    }
    protected makeChildElements() {
        this.loadTabData();
    }
    protected setUpEventListeners(): void {
        if (this.backButton) {
            this.backButton.addEventListener("click", () => { this.callback(Pages.Actions.ChangePage, Pages.Page.Home) });
        }
    }
    /**
      *load the json data for the tabs
      *
      * @memberof WallpaperMain
      */
    loadTabData() {
        fetch("./assets/wallpapertabs.json")
            .then(response => response.json())
            .then(data => this.tabsloaded({ data }));
    }
    /**
     *make the json data into a TabVo array
     *when the tabs are loaded, we need to load the wallpapers
     * @param {*} data
     * @memberof WallpaperMain
     */

    tabsloaded(data: any) {
        //map data to tabVo objects
        for (let i: number = 0; i < data.data.tabs.length; i++) {
            let tabVo: TabVo = new TabVo(data.data.tabs[i].title, data.data.tabs[i].description);
            this.tabsVo.push(tabVo);
        }
        this.loadWallpaperData();
    }
    /**
     * load the json data for the wallpapers
     */
    loadWallpaperData() {
        fetch("./assets/m3wallpapers.json")
            .then(response => response.json())
            .then(data => this.wallpaperDataLoaded(data));
    }
    loadBuddyData() {
        fetch("./assets/buddy.json")
            .then(response => response.json())
            .then(data => this.buddyDataLoaded(data));
    }
    buddyDataLoaded(data: any) {
        console.log("buddy data loaded");
        console.log(data);
    }
    makeTempBuddyData() {
        let buddyData: BuddyVo[] = [];
        let names:string[]=["Bunny", "Banan", "Bebe", "Bubbly", "Cuddlebug", "Darlings", "Elliott", "Fuzzybutt", "Goldilocks","Banana", "Blue Monkey", "Brown Monkey", "Coral Monkey", "Dusky Monkey", "Electric monkey", "Grey Monkey", "Green Monkey"," Honey Monkey", "IndigoMonkey"];
        let wnames:string[]=["Woodchuckie", "Woodchuckie Doodle", "Woodchuckie Joe", "Woodchuckie Louie","Stumpy","Scruffy","Bashful","Grumpy","Sneezy","Maggie", "Ellie", "Joey", "Brooke", "Sierra"]
        for (let i: number = 0; i < 50; i++) {
            
            let monkeyName:string="Bonzo"+i;
            if (i<names.length)
            {
                monkeyName=names[i];
            }
            let woodchuckName:string="Woody "+i;
            if (i<wnames.length)
            {
                woodchuckName=wnames[i];
            }
            buddyData.push(new BuddyVo("buddy 3", woodchuckName,"https://ttv5.s3.amazonaws.com/vid/bt/BT00002.mp4", "BookishWoodchuck", "thumbnail", "woodchuck", false, false));
            buddyData.push(new BuddyVo("buddy 2",monkeyName,"https://ttv5.s3.amazonaws.com/vid/bt/BT00001.mp4", "Amka", "walk", "amka", false, false));
          
        }
        for (let i: number = 0; i < 10; i++) {
            buddyData[i].isNew=true;
        }
        for (let i: number = 45; i < 50; i++) {
            buddyData[i].locked=true;
        }
        this.makeBuddies(buddyData);
    }
    makeBuddies(data: BuddyVo[]) {
        this.buddyThumbs = new BuddyThumbs(data,this.selectBuddy.bind(this));
        this.buddyThumbs.visible = false;

        if (this.buddyThumbs.el) {


            this.ttScrollBar2 = new TTScrollbar(this.buddyThumbs.el, null, this.updateScrollBuddies.bind(this));
            this.ttScrollBar2.setImage("track", "./assets/scroll/scrollback.png");
            this.ttScrollBar2.setImage("knob", "./assets/scroll/thumbScroll.png");
            this.ttScrollBar2.setImage("btnUp", "./assets/scroll/arrowUp.png");
            this.ttScrollBar2.setImage("btnDown", "./assets/scroll/arrowDown.png");

            const scrollbarHolder: HTMLElement | null = document.querySelector(".scrollbarHolder");
            if (scrollbarHolder) {
                this.ttScrollBar2.appendTo(scrollbarHolder);
            }
            this.ttScrollBar2.element.style.display = "none";
        }
    }
    private showBuddyPreview(buddyVo: BuddyVo) {
        let buddyPreview: BuddyPreview = new BuddyPreview(buddyVo);
        buddyPreview.render();
        document.body.appendChild(buddyPreview.element);
    }
    /**
     * when the wallpapers json data is loaded convert it to WallpaperVo objects
     * @param data 
     */
    wallpaperDataLoaded(data: any) {

        this.makeTempBuddyData();

        for (let i: number = 0; i < data.length; i++) {
            let paperData: any = data[i];
            let image: string = "https://ttv4.s3.amazonaws.com/wallpaper/" + paperData[0] + ".T.jpg"

            let wallpaperVo: WallpaperVo = new WallpaperVo(paperData[0], paperData[1], image);

            if (i < 3) {
                wallpaperVo.isNew = true;
            }
            /* if (i > 40) {
                  wallpaperVo.locked = true;
              } */
            this.wallpaperVos.push(wallpaperVo);
        }

        //now that we have the data, we can make the tabs and the thumbnails

        //the tabline is the line of tabs at the top
        this.tabLine = new TabLine("header", (index: number) => { this.tabClicked(index) }, this.tabsVo);

        //the wallpaperThumbs is the line of thumbnails containing the wallpapers
        this.wallpaperThumbs = new WallpaperThumbs(this.wallpaperVos, this.selectWallpaper.bind(this));
        (window as any).wallpaperThumbs = this.wallpaperThumbs;

        const scrollbarHolder: HTMLElement | null = document.querySelector(".scrollbarHolder");

        //console.log("scrollbarHolder", scrollbarHolder);
        if (this.wallpaperThumbs.el) {

            this.ttScrollBar = new TTScrollbar(this.wallpaperThumbs.el, null, this.updateScroll.bind(this));
            this.ttScrollBar.addCssToHead();
            //this.ttScrollBar.setScrollbarWidthModifier(2);
            this.ttScrollBar.setImage("track", "./assets/scroll/scrollback.png");
            this.ttScrollBar.setImage("knob", "./assets/scroll/thumbScroll.png");
            this.ttScrollBar.setImage("btnUp", "./assets/scroll/arrowUp.png");
            this.ttScrollBar.setImage("btnDown", "./assets/scroll/arrowDown.png");
            this.ttScrollBar.appendTo(scrollbarHolder);
        }
        this.wallpaperThumbs.visible = true;

        this.tabClicked(0);
    }
    setInactive(): void {
        const body: HTMLElement | null = document.querySelector("body");
        if (body) {
            body.classList.remove("wallpaperbook");
        }
    }
    updateScroll(percent: number) {


        if (this.wallpaperThumbs) {
            if (this.wallpaperThumbs.el) {
                const scrollHeight: number = this.wallpaperThumbs.el.scrollHeight - this.wallpaperThumbs.el.clientHeight;
                const px: number = scrollHeight * percent / 100;
                this.wallpaperThumbs.el.scrollTo(0, px);
            }
        }
    }

    updateScrollBuddies(percent: number) {

        if (this.buddyThumbs) {
            if (this.buddyThumbs.el) {
                const scrollHeight: number = this.buddyThumbs.el.scrollHeight - this.buddyThumbs.el.clientHeight;
                const px: number = scrollHeight * percent / 100;
                this.buddyThumbs.el.scrollTo(0, px);
                
            }
            this.buddyThumbs.checkVidOnScroll();
            setTimeout(() => {
                this.buddyThumbs.checkVidOnScroll();
            }, 2000);
        }
    }

    tabClicked(index: number) {
        console.log("tab clicked: " + index);
        this.tabIndex = index;
        this.updateArticleText(index);
        switch (index) {
            case 0:
                if (this.wallpaperThumbs) {
                    this.wallpaperThumbs.el.scrollTo(0, 0);
                    this.wallpaperThumbs.visible = false;
                }
                if (this.buddyThumbs) {
                    this.buddyThumbs.el.scrollTo(0, 0);
                    this.buddyThumbs.visible = true;
                }

                this.ttScrollBar.element.style.display = "none";
                this.ttScrollBar2.element.style.display = "block";
                break;
            case 1:
                if (this.wallpaperThumbs) {

                    this.wallpaperThumbs.visible = true;
                }
                if (this.buddyThumbs) {
                    this.buddyThumbs.visible = false;
                }
                this.ttScrollBar.element.style.display = "block";
                this.ttScrollBar2.element.style.display = "none";

        }
    }
    //update the article text when a tab is clicked

    updateArticleText(index: number) {
        let article: HTMLElement | null = document.querySelector("main article");
        if (article) {
            article.innerHTML = this.tabsVo[index].description;
        }
    }
    selectWallpaper(wallpaperVo: WallpaperVo) {
        console.log("select wallpaper: " + wallpaperVo);
        this.selectedWallpaperVo = wallpaperVo;
        this.buddyThumbs.deselect();
        this.updateDatabase();
    }
    selectBuddy(buddyVo: BuddyVo) {
        console.log("select buddy: " + buddyVo);
        this.selectedBuddyVo = buddyVo;
        this.wallpaperThumbs.deselect();
        this.updateDatabase();
    }
    //later we need to update the database or send a signal to the main app when a wallpaper or buddy is selected
    updateDatabase() {
        console.log("update database");
        //for later implementation

    }
    render(): void {
        super.render();
        const body: HTMLElement | null = document.querySelector("body");
        if (body) {
            body.classList.add("wallpaperbook");

            body.innerHTML = `<header>
			<button type="button" class="tempbutton">Back</button>			
		</header>
    <main>
        <article>
            Here are your wallpapers. Choose the one you like best. 
            You will get more of these as you work your way through the course. 
            Tap the wallpaper you&#x2019;d like to use. 
            Also, if you want to change your wallpaper in the middle of a lesson, 
            just tap &#x201C;Controls&#x201D; on the lesson screen 
            and then hit &#x201C;Wallpapers.&#x201D;
        </article>
       
      <div class="wpthumbnails">
      </div>     
      
      <div class="buddythumbnails">
      </div>
      <div class="scrollbarHolder"></div>
    </div>
    </main>`;

        }
    }
}
