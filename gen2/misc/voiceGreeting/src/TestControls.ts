import { Greeter } from "./Greeter";

export class TestControls
{
    private monthDropdown: HTMLSelectElement | null;
    private dayDropdown: HTMLSelectElement | null;
    private genderDropdown: HTMLSelectElement | null;
    private locationDropdown: HTMLSelectElement | null;
    private speakButton: HTMLButtonElement | null;
    private motivationCheckbox: HTMLInputElement | null;

    constructor()
    {
        this.monthDropdown = document.querySelector(".month");
        this.dayDropdown = document.querySelector(".day");
        this.genderDropdown=document.querySelector(".gender");
        this.locationDropdown=document.querySelector(".location");
        this.speakButton=document.querySelector(".btnSpeak");
        this.motivationCheckbox=document.querySelector(".motivational");

        if (this.monthDropdown != null)
        {
            this.monthDropdown.addEventListener("change", this.monthDropdownChanged.bind(this));
        }
        
        if (this.speakButton != null)
        {
            this.speakButton.addEventListener("click", this.speakButtonClicked.bind(this));
            this.speakButton.disabled = true;
            
        }

        (window as any).testControls = this;
    }
    speakButtonClicked()
    {
      
        if (this.motivationCheckbox==null || this.monthDropdown == null || this.dayDropdown == null || this.genderDropdown == null || this.locationDropdown == null) return;

        let month:number=parseInt(this.monthDropdown.value)-1;
        let day:number=parseInt(this.dayDropdown.value);
        let location:string=this.locationDropdown.value;
        let gender:string=this.genderDropdown.value;

        console.log("month=" + month);
        console.log("day=" + day);
        console.log("location=" + location);
        console.log("gender"+gender);

        if (this.speakButton != null)
        {
            this.speakButton.disabled = true;
            this.speakButton.classList.add("disabled");
        }
        let year = new Date().getFullYear();
        let date:Date=new Date(year, month, day);
        console.log("date=" + date);
        let greeter:Greeter=new Greeter(date,this.fileDone.bind(this));
        greeter.location=location;
        greeter.gender=gender;
        greeter.addGreeting();
        greeter.addTheDate();
        greeter.addYesterday();
        

        if (this.motivationCheckbox.checked)
        {
            //add motivational
            greeter.addMotivational();
        }
        greeter.addClosing();
        greeter.playNext();
    }
    fileDone()
    {
        if (this.speakButton)
        {
            this.speakButton.disabled = false;
            this.speakButton.classList.remove("disabled");
        }
    }
    monthDropdownChanged()
    {
        console.log("monthDropdownChanged");
        //get the number of days in the month
        if (this.monthDropdown == null) return;
        let month:number = parseInt(this.monthDropdown.value);
        console.log("month=" + month);
        //get current year
        let year = new Date().getFullYear();
        //get the number of days in the month

        let daysInMonth = new Date(year, month, 0).getDate();
        //clear the day dropdown
        if (this.dayDropdown == null) return;
        this.dayDropdown.innerHTML = "";
        //add the days to the day dropdown
        for (let i = 1; i <= daysInMonth; i++)
        {
            let option = document.createElement("option");
            option.text = i.toString();
            option.value = i.toString();
            this.dayDropdown.add(option);
        }

        if (this.speakButton != null)
        {
            this.speakButton.disabled = false;
            this.speakButton.classList.remove("disabled");
        }
    }
    
}