

export class Greeter {
    private date: Date;
    public location: string = "us";
    public gender: string = "female";

    private audios: HTMLAudioElement[] = [];
    private callback:Function;

    constructor(date: Date,callback:Function) {
        this.date = date;
        this.callback=callback;
    }
    public addGreeting() {
        //put together the paths to the mp3 files
        //greeting array
        let greetingArray = ["hello","hellothere","hi","hithere"];
        //get a random greeting
        let greeting = greetingArray[Math.floor(Math.random() * greetingArray.length)];
        this.stackMp3(["greetings/" + greeting]);

    }
    public addTheDate() {
        //put together the paths to the mp3 files
        let month = this.date.getMonth() + 1;
        //month array
        let monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        //day array
        let dayArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        //date array in words
        let dateArray = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth", "thirtieth", "thirty-first"];
        //get the day of the week
        let day: string = dayArray[this.date.getDay()];

        let dayName: string = day.toLowerCase();
        //limit the day name to 10 characters
        
        //get the date
        let date = dateArray[this.date.getDate() - 1];
        let dateName: string = "the"+date.toLowerCase();
        if (dateName.length > 10) {
            dateName = dateName.substring(0, 10);
        }


        //get the month
        let monthName = monthArray[month - 1];

        let paths: string[] = [];
        //add the path to the mp3 file for the day of the week

        //get an opening prefix
        let prefixArray = ["Itis","Thedateis","Todayis"];
        let prefix = prefixArray[Math.floor(Math.random() * prefixArray.length)];
        paths.push("openPrefixes/" + prefix);



        paths.push("days/" + day.toLowerCase());
        paths.push("months/" + monthName.toLowerCase());
        paths.push("oridinalDates/" +dateName);
        this.stackMp3(paths);
    }
    public addYesterday() {
        //put together the paths to the mp3 files
        //get the day of the week 0-6 of yesterday
        let yesterday = new Date(this.date.getTime() - 24 * 60 * 60 * 1000);
        let day = yesterday.getDay();
        let paths: string[] = [];
        //add the path to the mp3 file for the day of the week
        paths.push("yesterdayAndTomorrow/" + day.toString());
        this.stackMp3(paths);

    }
    public addMotivational() {
        //put together the paths to the mp3 files
        let movArray=["Achampioni","Itdoesntma","Keepitupyo","Startwhere","Successist","Theonlyway","Youaredoin","Youcandoan","Youcandoit"];
        let mov = movArray[Math.floor(Math.random() * movArray.length)];
        this.stackMp3(["motivational/" + mov]);


    }
    public addClosing() {
        //put together the paths to the mp3 files
        let closingArray = ["Herewego","Letsgetsta","Letsstarto","Okayletsgo"];
        let closing = closingArray[Math.floor(Math.random() * closingArray.length)];
        this.stackMp3(["closings/" + closing]);
    }
    public stackMp3(paths: string[]) {
        //play the mp3s one right after the other ends
        let fullPath: string = "../assets/" + this.location + "/" + this.gender + "/";
        for (let i = 0; i < paths.length; i++) {
            paths[i] = fullPath + paths[i] + ".mp3";
            console.log(paths[i]);
           this.audios.push(new Audio(paths[i]));
        }
        
    }
    public playNext() {
        
        if (this.audios.length > 0) {
            let audio:HTMLAudioElement | undefined = this.audios.shift();
            if (audio === undefined) {
                return;
            }
            audio.play();
            audio.onended = () => {
                this.playNext();
            }
        }
        else {
            this.callback();
        }
        

    }
}