import { PartVo } from "./dataObjects/PartVo";

export class CoefficientsExtractor {
    //a class to extract the coefficients from the equation
    //a*x^2 + b*x + c = 0
    //app is the root of the app

    private app: HTMLElement;
    private input: HTMLInputElement;
    private lastInput: string = '';
    private values: string[] = [];
    private lastValues: string[] = [];
    private operators: string[] = [];
    private displayOperators: string[] = [];
    private changeListeners: Function[] = [];
    private coefficientChanged: boolean = false;
    constructor() {
        //get the root of the app by class
        this.app = document.querySelector('.app') as HTMLElement;

        this.input = document.querySelector('.formulaInput') as HTMLInputElement;

        //restrict the input to only numbers and operators and the letter x
        //  this.input.setAttribute('pattern', '[0-9\+\-\*\/\^x]*');

        this.input.addEventListener('input', this.onInput.bind(this));

        this.input.value = "9x2-6x-9";
        this.onInput();
        (window as any).test = this;
    }
    onInput() {
        //validate the input
        /* if (this.coefficientChanged===true)
        {
            this.coefficientChanged=false;
            return;
        } */
        //get the value of the input
        let value = this.input.value;

        //if the value is empty, return
        if (value === '') {
            this.lastInput = value;
            //remove all from the display
            let displayElement: HTMLElement = this.app.querySelector('.display') as HTMLElement;
            displayElement.innerHTML = '';
            return;
        }

        //if the value contains anything other than numbers, operators and the letter x, return
        if (!value.match(/^[0-9\+\-\*\/\^x]*$/)) {
            this.input.value = this.lastInput;
            return;
        }
    
        //split the value by all operators
        let values: string[] = value.split(/[\+\-\*\/\^]/);
        // 



        //get all the operators from the value and store them in an array
        let operators: string[] = value.match(/[\+\-\*\/\^]/g) || [];
        //

        this.lastInput = value;


        
        

        this.values = values;
        this.operators = operators;

        //make the display of the equation
        let display: string = this.makeDisplay(values, operators);
        //
        //get the html element that will display the equation
        let displayElement: HTMLElement = this.app.querySelector('.display') as HTMLElement;
        //set the display of the equation
        displayElement.innerHTML = display;        

        //add event listeners to the inputs
        this.addEventListeners();
    }

    removeEventListeners() {
        //remove event listeners from the inputs
        let inputs: NodeListOf<HTMLInputElement> = this.app.querySelectorAll('.coefficient');
        for (let i = 0; i < inputs.length; i++) {
            let input: HTMLInputElement = inputs[i];
            input.removeEventListener('input', ()=>{this.onCoefficientChange()});
        }
    }
    addEventListeners() {
        //remove event listeners from the inputs
        this.removeEventListeners();

        //add event listeners to the inputs
        let inputs: NodeListOf<HTMLInputElement> = this.app.querySelectorAll('.coefficient');
        for (let i = 0; i < inputs.length; i++) {
            let input: HTMLInputElement = inputs[i];
            input.addEventListener('input', ()=>{this.onCoefficientChange()});
        }
    }
    onCoefficientChange() {

        let values2: string[] = this.values.slice();
        
        
        //get the values from the inputs
        let inputs: NodeListOf<HTMLInputElement> = this.app.querySelectorAll('.coefficient');
        let inputValues: string[] = [];
        for (let i = 0; i < inputs.length; i++) {
            let input: HTMLInputElement = inputs[i];
            let value: string = input.value;
            //if the number doesn't start with a negative sign, add a + sign
            if (value[0] !== '-') {
                value = '+' + value;
            }
            inputValues.push(value);
        }
        for (let i = 0; i < this.values.length; i++) {
            let value: string = this.values[i];
            if (value.includes('x')) {
                
                let parts: string[] = value.split('x');
                parts[0] = inputValues[i];
                values2[i] = parts.join('x');
            }
            else
            {
                values2[i] = inputValues[i];
            }
        }
        
        

        //build the equation from the values
        let equation: string = '';
        for (let i = 0; i < values2.length; i++) {
            equation += values2[i];
            if (i < this.operators.length) {
                equation += this.operators[i];
            }
        }
        
        console.log(equation);

        //replace a +1x with a +x
        equation = equation.replace(/\+1x/g, '+x');
        //replace a -1x with a -x
        equation = equation.replace(/\-1x/g, '-x');

        //replace a -- with a -
        equation = equation.replace(/\-\-/g, '-');
        //replace a +- with a +
        equation = equation.replace(/\+\-/g, '+');
        //replace a -+ with a -
        equation = equation.replace(/\-\+/g, '+');
        //replace a ++ with a +
        equation = equation.replace(/\+\+/g, '+');

        

        if (equation[0] === '+') {
            equation = equation.slice(1);
        }

        
        this.input.value = equation;
    }
    makeDisplay(values: string[], operators: string[]) {
        //make the display of the equation
        let display: string = '';

        //make a copy of the values and operators
        let displayValues: string[] = values.slice();
        let displayOperators: string[] = operators.slice();


        for (let i = 0; i < displayValues.length; i++) {
            let value = displayValues[i];
            
            //if the operator before the value is a minus, add a minus sign to the value and change the operator to a plus
            if (i > 0 && operators[i - 1] === '-') {
                value = '-' + value;
                displayOperators[i - 1] = '+';
            }

            //if the value contains the letter x, split it by the letter x
            let change:boolean=false;
            let changeText:string='';
            if (value.includes('x')) {
                
                //split the value by the letter x
                let splitValue: string[] = value.split('x');

                //if there is a number before the letter x, replace it with an input
                if (splitValue[0] !== 'x') {
                    //replace the number with a numberic input
                    if (splitValue[0] === '')
                    {
                        splitValue[0]='1';
                    }
                    this.lastValues[i]=splitValue[0];
                
                    let id:string=`coefficient${i}`;
                    splitValue[0] = "<input type='number' id='"+id+"' value='" + splitValue[0] + "' class='coefficient'>";
                    //splitValue[0] = '@'+splitValue[0];
                    change=true;
                    
                }

                //if there is a number after the letter x, replace it with a subscript number
                if (splitValue[1] !== '') {                    
                    splitValue[1] = "<sup>" + splitValue[1] + "</sup>";
                    change=true;
                }

                changeText=splitValue.join('x');
            }
            //if the value is a number, replace it with a numeric input
            else if (!isNaN(Number(value))) {
                this.lastValues[i]=value;
                let id:string=`coefficient${i}`;
               

                value = "<input type='number' id='"+id+"' value='" + value + "' class='coefficient'>";
                change=true;
                changeText=value;
                
            }
            if (change) {
                //join the value back together
                value = changeText;
                
            }
            
            displayValues[i] = value;
        }
        //join the values and operators together
        
        for (let i = 0; i < displayValues.length; i++) {
            display += displayValues[i];

            if (i < displayOperators.length) {
                display += displayOperators[i];
            }
        }
        
        this.displayOperators=displayOperators;
        
        return display;
    }
    
}