//data object for the control panel
//type is the type of control
//id is the id of the control
//value is the value of the control
export class ControlVo {

   
    public type: string;
    public id: string;
    public value: string;
    public min:number;
    public max:number;

    public constructor(type: string, id: string, value: string,min:number=0,max:number=100) {
        this.type = type;
        this.id = id;
        this.value = value;
        this.min = min;
        this.max = max;

    }
}
