import { PropertyPanel } from "../ui/propertyPanel";
import { ShapeToolbar } from "../ui/ShapeToolbar";
import { SvgObject } from "../ui/SvgObject";
import { SvgEditor } from "./SvgEditor";

export class EditScreen {
    public element: HTMLElement;
    public svgEditor: SvgEditor;
    public shapeToolbar: ShapeToolbar;
    public propertiesPanel: PropertyPanel;

    constructor() {
        this.element = document.createElement("div");
        this.element.classList.add("editScreen");

        //get a reference to the svg tag
        //const svg: SVGSVGElement | null = document.querySelector("svg");
        const defs: SVGDefsElement | null = document.querySelector("defs");
        
        this.svgEditor = new SvgEditor(defs, this.shapeSelected.bind(this));
        this.shapeToolbar = new ShapeToolbar(this.addShapetoEditor.bind(this));
        this.propertiesPanel = new PropertyPanel();

        this.render();

        const body = document.querySelector("body");
        if (body) {
            body.appendChild(this.element);

        }
        //append the toolbar to the first div of the element
        this.element.children[0].appendChild(this.shapeToolbar.element);
        this.element.children[0].appendChild(this.propertiesPanel.element);
        //append the svg editor to the second div of the element
        this.element.children[1].appendChild(this.svgEditor.element);

        //this.element.appendChild(this.shapeToolbar.element);
        // this.element.appendChild(this.svgEditor.element);
    }
    shapeSelected(object: SvgObject) {
        this.propertiesPanel.setShape(object);
    }
    addShapetoEditor(shape: string) {
        this.svgEditor.addShapeByTagName(shape);
    }
    render() {
        this.element.innerHTML = `<div></div><div></div>`;
    }
}