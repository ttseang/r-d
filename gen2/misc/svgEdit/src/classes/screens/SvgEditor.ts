import { SvgConstants } from "../constants/SvgConstants";
import { Resizer } from "../ui/resizer";
import { SvgObject } from "../ui/SvgObject";

export class SvgEditor {
    public element: HTMLElement;
    private currentShape: SVGElement | null = null;
    private dragShape: SVGElement | null = null;
    private offset: any = { x: 0, y: 0 };

    private selectCallback: Function;
    private svg: SVGSVGElement | null = null;
    private defs: SVGDefsElement | null = null;

    private mouseup: EventListener = this.onDragStop.bind(this) as EventListener;
    private mousemove: EventListener = this.onDragging.bind(this) as EventListener;
    private mousedown: EventListener = this.selectShape.bind(this) as EventListener;

    private objectMap: Map<string, SvgObject> = new Map<string, SvgObject>();

    private selection: SVGSVGElement | undefined = undefined;

    constructor(defs: SVGDefsElement | null, selectCallback: Function) {

        if (!defs) {
            throw new Error("No defs tag found");

        }

        this.defs = defs;

        this.selectCallback = selectCallback;
        this.element = document.createElement("div");
        this.element.classList.add("svgeditor");

        this.selection=<SVGSVGElement>defs.querySelector("#selection")?.cloneNode(true);

        this.render();

        this.svg = this.element.querySelector("svg");

        (window as any).svgEditor = this;

        //add a event listener to the svg tag to drag the selected child element
        if (this.svg) {
            console.log("svg found");
            this.svg.addEventListener("mousedown", this.mousedown);
            if (this.selection)
            {
                this.svg.appendChild(this.selection);
            }            
        }
    }
    selectShape(e: MouseEvent) {
        const target = e.target as SVGElement;
        console.log("target: " + target.tagName);
        if (!target) return;
        console.log(target.tagName);

        if (SvgConstants.shapes.includes(target.tagName)) {

            if (this.currentShape) {
                this.currentShape.classList.remove("selected");
                
               
                /* if (this.currentShape === target) {
                    this.currentShape = null;
                    this.dragShape = null;

                    if (this.svg) {
                        this.svg.removeEventListener("mousemove", this.mousemove);
                        this.svg.removeEventListener("mouseup", this.mouseup);
                    }
                    return;
                } */
            }
            this.currentShape = target;
            this.dragShape = target;

            let uid: string = target.getAttribute("data-uid") || "";
            if (uid === "") {
                //check parent
                let parent = target.parentElement;
                console.log("parent: " + parent);
                uid = parent?.getAttribute("data-uid") || "";

            }


            let svgObject = this.objectMap.get(uid);
            if (svgObject) {
                if (this.selection)
                {
                    this.selection.setAttribute("x",svgObject.x.toString());
                    this.selection.setAttribute("y",svgObject.y.toString());
                    this.selection.setAttribute("width",svgObject.getWidth().toString());
                    this.selection.setAttribute("height",svgObject.getHeight().toString());
                    
                }
            }
            this.selectCallback(svgObject);

            this.offset = this.getMousePosition(e);


            this.offset.x -= parseFloat(target.getAttributeNS(null, "x") || "0");
            this.offset.y -= parseFloat(target.getAttributeNS(null, "y") || "0");


            target.classList.add("selected");



            //add a event listener to the svg tag to drag the selected child element

            if (this.svg) {
                this.svg.addEventListener("mousemove", this.mousemove);
                this.svg.addEventListener("mouseup", this.mouseup);
            }
        }
        else {
            console.log("not a shape");
        }
    }
    onDragging(e: MouseEvent) {

        //get the relative position of the mouse and subtract the offset of the svg tag and half the width and height of the shape

        if (this.svg) {
            if (this.dragShape) {
                //  console.log("dragging");


                let uid: string = this.dragShape.getAttribute("data-uid") || "";
                if (uid === "")
                {
                    //check parent
                    let parent = this.dragShape.parentElement;
                    console.log("parent: " + parent);

                    if (parent) {
                        uid = parent.getAttribute("data-uid") || "";
                    }
                }
                let svgObject: SvgObject | undefined = this.objectMap.get(uid);

                if (svgObject) {
                    e.preventDefault();
                    const coord = this.getMousePosition(e);
                    let coordX: number = coord.x;// - this.offset.x;
                    let coordY: number = coord.y;// - this.offset.y;
                    svgObject.updatePosition(coordX, coordY);
                }

            }
        }
    }
    onDragStop(e: MouseEvent) {
        if (this.dragShape) {
            //this.dragShape.classList.remove("selected");
            (window as any).testShape = this.dragShape;
            this.dragShape = null;
            if (this.svg) {
                this.svg.removeEventListener("mousemove", this.mousemove);
                this.svg.removeEventListener("mouseup", this.mouseup);
            }
            
        }
    }

    addShapeByTagName(tagName: string) {

        //create an svgObject
        if (this.svg && this.defs) {
            {
                const svgObject = new SvgObject(tagName, this.svg, this.defs);
                this.objectMap.set(svgObject.id, svgObject);
            }
        }
    }

    getMousePosition(evt: MouseEvent) {

        if (!this.svg) {
            return { x: 0, y: 0 };
        }
        let CTM: DOMMatrix | null = this.svg.getScreenCTM();
        if (!CTM) {
            return { x: 0, y: 0 };
        }
        return {
            x: (evt.clientX - CTM.e) / CTM.a,
            y: (evt.clientY - CTM.f) / CTM.d
        };
    }
    render() {
        //make an svg editor
        this.element.innerHTML = `
        <svg width="100%" height="100%"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>`;
    }
}