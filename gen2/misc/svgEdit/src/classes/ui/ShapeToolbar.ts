import { ShapeButton } from "./buttons/shapeButton";

export class ShapeToolbar
{
    public element: HTMLElement;
    private callback:Function;

    constructor(callback:Function)
    {
        this.callback = callback;
        this.element = document.createElement("menu");
        this.element.classList.add("shapetoolbar");
        this.render();
        this.makeButtons();
    }
    makeButtons()
    {
        const buttons = [];
        const shapes = ["rectangle", "circle", "triangle","star","heart","square"];
        for (let i = 0; i < shapes.length; i++)
        {
            const button = new ShapeButton(shapes[i], this.callback);
            buttons.push(button);
            this.element.appendChild(button.element);
        }
    }
    render()
    {
        //make a toolbar with shape icons
        this.element.innerHTML = ``;
       
    }
}