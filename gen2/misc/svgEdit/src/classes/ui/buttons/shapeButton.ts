export class ShapeButton
{
    public element: HTMLElement;
    private callback:Function;
    private shape:string;

    constructor(shape:string, callback:Function)
    {
        this.callback = callback;
        this.shape = shape;
        this.element = document.createElement("li");
        this.element.classList.add("shapebutton");
        this.render();

    }
    render()
    {
        //make a toolbar with shape icons
        this.element.innerHTML = `
            <img src="assets/toolbar/${this.shape}.svg" alt="${this.shape}" />`;
        this.element.addEventListener("click", () => this.callback(this.shape));
    }
}