//draw a property panel with sliders for scale,skew, and rotation
//and a color picker for fill color
//and a color picker for stroke color
//and a slider for stroke width
//and a slider for opacity

import { ControlVo } from "../dataObjects/controlVo";
import { SvgObject } from "./SvgObject";

export class PropertyPanel
{
    public element: HTMLMenuElement;
    private shape:SVGElement | null = null;    
    private object:SvgObject | null = null;

    constructor()
    {
        this.element = document.createElement("menu");
        this.element.classList.add("propertyPanel");
        this.render();
        let controls:ControlVo[] = [];
        controls.push(new ControlVo("Slider","scale","50",50,500));
        controls.push(new ControlVo("Slider","skewX","50"));
        controls.push(new ControlVo("Slider","skewY","50"));
        controls.push(new ControlVo("Slider","rotation","50"));
        controls.push(new ControlVo("ColorPicker","fillColor","#000000"));
        controls.push(new ControlVo("ColorPicker","strokeColor","#000000"));
        controls.push(new ControlVo("Slider","strokeWidth","50"));
        controls.push(new ControlVo("Slider","opacity","50"));
        controls.push(new ControlVo("Button","delete","Delete"));
        this.makeControls(controls);

    }
    setShape(object:SvgObject)
    {
        this.object = object;
        console.log(object);
        this.shape = object.element;
        console.log("shape selected");
        console.log(this.shape);
        if (!this.shape)
        {
            return;
        }

        //set controls to match object properties
        const scaleSlider:HTMLInputElement | null = document.querySelector("#scale");
        if (scaleSlider)
        {
            let per:number = this.object.scale * 100;
            scaleSlider.value = per.toString();
        }
        const skewXSlider:HTMLInputElement | null = document.querySelector("#skewX");
        if (skewXSlider)
        {
            skewXSlider.value = this.object.skewX.toString();
        }
        const skewYSlider:HTMLInputElement | null = document.querySelector("#skewY");
        if (skewYSlider)
        {
            skewYSlider.value = this.object.skewY.toString();
        }
        const rotationSlider:HTMLInputElement | null = document.querySelector("#rotation");
        if (rotationSlider)
        {
            rotationSlider.value = this.object.rotation.toString();
        }
        const fillColorPicker:HTMLInputElement | null = document.querySelector("#fillColor");
        if (fillColorPicker)
        {
            fillColorPicker.value = this.object.fillColor;
        }
        const strokeColorPicker:HTMLInputElement | null = document.querySelector("#strokeColor");
        if (strokeColorPicker)
        {
            strokeColorPicker.value = this.object.strokeColor;
        }
        const strokeWidthSlider:HTMLInputElement | null = document.querySelector("#strokeWidth");
        if (strokeWidthSlider)
        {
            strokeWidthSlider.value = this.object.strokeWidth.toString();
        }
        const opacitySlider:HTMLInputElement | null = document.querySelector("#opacity");
        if (opacitySlider)
        {
            let oper:number = this.object.opacity * 100;
            opacitySlider.value = oper.toString();
        }


    }
    makeControls(controls:ControlVo[])
    {

        for (let i = 0; i < controls.length; i++)
        {
            let listItem:HTMLLIElement | null = null;
            switch(controls[i].type)
            {
                case "Slider":
                    listItem = this.createSlider(controls[i].id,controls[i].id,controls[i].min,controls[i].max,controls[i].value);
                    break;
                case "ColorPicker":
                    listItem = this.createColorPicker(controls[i].id,controls[i].id);
                    break;
                case "Button":
                    listItem = this.createButton(controls[i].id,controls[i].id);
                    break;
            }
            if (listItem)
            {
                this.element.appendChild(listItem);
            }            
        }
    }
    createColorPicker(id:string,text:string)
    {
        const colorPicker = document.createElement("input");
        colorPicker.id = id;
        colorPicker.type = "color";
        colorPicker.addEventListener("change",this.onColorChange.bind(this));

        const label = document.createElement("label");
        label.htmlFor = id;
        label.innerText = text+":"

        const listItem:HTMLLIElement = document.createElement("li");
        listItem.appendChild(label);
        listItem.appendChild(colorPicker);

        return listItem;

    }
    createButton(id:string,text:string)
    {
        const button = document.createElement("button");
        button.id = id;
        button.innerText = text;
        button.addEventListener("click",this.onButtonClick.bind(this));

        const listItem:HTMLLIElement = document.createElement("li");
        listItem.appendChild(button);

        return listItem;
    }

    createSlider(id:string,text:string,min:number,max:number,value:string)
    {
        const slider = document.createElement("input");
        slider.id = id;
        slider.type = "range";
        slider.min = min.toString();
        slider.max = max.toString();
        slider.value = value;
        slider.addEventListener("input",this.onSliderChange.bind(this));

        const label = document.createElement("label");
        label.htmlFor = id;
        label.innerText = text;

        const listItem:HTMLLIElement = document.createElement("li");
        listItem.appendChild(label);
        listItem.appendChild(slider);


        return listItem;
    }
    onSliderChange(event:Event)
    {
        const target = event.target as HTMLInputElement;
        if (this.object)
        {
            switch(target.id)
            {
                case "scale":
                    this.object.updateScale(parseFloat(target.value)/100);
                    break;
                case "skewX":
                    this.object.updateSkewX(parseFloat(target.value));
                    break;
                case "skewY":
                    this.object.updateSkewY(parseFloat(target.value));
                    break;
                case "rotation":
                    this.object.updateRotation(parseFloat(target.value));
                    break;
                case "strokeWidth":
                    this.object.updateStrokeWidth(parseFloat(target.value));
                    break;
                case "opacity":
                    this.object.updateOpacity(parseFloat(target.value)/100);
                    break;

            }
            
        }
    }
    onColorChange(event:Event)
    {
        const target = event.target as HTMLInputElement;
        if (this.shape)
        {
            switch(target.id)
            {
                case "fillColor":
                    this.shape.setAttribute("fill",target.value);
                    break;
                case "strokeColor":
                    this.shape.setAttribute("stroke",target.value);
                    break;
            }
        }
    }
    onButtonClick(event:Event)
    {
        const target = event.target as HTMLButtonElement;
        if (this.shape)
        {
            switch(target.id)
            {
                case "delete":
                    this.shape.remove();
                    this.shape = null;
                    break;
            }
        }
    }
    render()
    {
        this.element.innerHTML = ``;

    }
}