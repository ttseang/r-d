export class SvgObject {
    private static uid: number = 0;
    public element: SVGSVGElement | null = null;
    public offSetX: number = 0;
    public offSetY: number =0;
    public id: string = "";

    public x: number = 30;
    public y: number =30;

    public scale: number = 1;
    public rotation: number = 0;
    public skewX: number = 0;
    public skewY: number = 0;
    public opacity: number = 1;
    public fillColor: string = "#000000";
    public strokeColor: string = "#000000";
    public strokeWidth: number = 1;

    private pathElement: SVGPathElement | null = null;

    constructor(tagName: string, svgElement: SVGSVGElement, def: SVGDefsElement) {
        SvgObject.uid++;

        let elementToClone = <SVGSVGElement>def.querySelector("#" + tagName);
        console.log("elementToClone: " + elementToClone);

        if (elementToClone != null) {
            this.element = <SVGSVGElement>elementToClone.cloneNode(true);
            this.element.setAttribute("data-uid", tagName + SvgObject.uid);
            this.id = tagName + SvgObject.uid;
            svgElement.appendChild(this.element);

            this.pathElement = <SVGPathElement>this.element.querySelector("path");

            this.makeTransform();
        }

    }
    getWidth(): number {
        if (this.element) {
            return this.element.getBBox().width;
        }
        return 0;
    }
    getHeight(): number {
        if (this.element) {
            return this.element.getBBox().height;
        }
        return 0;
    }
    
    updatePosition(x: number, y: number) {
        this.x = x;
        this.y = y;
        this.makeTransform();
    }
    updateScale(scale: number) {
        this.scale = scale;
        this.makeTransform();
    }
    updateRotation(rotation: number) {
        this.rotation = rotation;
        this.makeTransform();
    }
    updateSkewX(skewX: number) {
        this.skewX = skewX;
        this.makeTransform();
    }
    updateSkewY(skewY: number) {
        this.skewY = skewY;
        this.makeTransform();
    }
    updateOpacity(opacity: number) {
        this.opacity = opacity;
        if (this.element) {
            this.element.setAttribute("opacity", opacity.toString());
        }
    }
    updateStrokeWidth(strokeWidth: number) {
        this.strokeWidth = strokeWidth;
        if (this.pathElement) {
            this.pathElement.setAttribute("stroke-width", this.strokeWidth.toString());
        }
    }
    updateFillColor(fillColor: string) {
        this.fillColor = fillColor;
        if (this.pathElement) {
            this.pathElement.setAttribute("fill", this.fillColor);
        }
    }
    updateStrokeColor(strokeColor: string) {
        this.strokeColor = strokeColor;
        if (this.pathElement) {
            this.pathElement.setAttribute("stroke", this.strokeColor);
        }
    }
    makeTransform() {
        if (this.pathElement && this.element) {
            /*  let xOffSet:number=this.element.getBoundingClientRect().width/2 ?? 0;
             let yOffSet:number=this.element.getBoundingClientRect().height/2 ?? 0;
     
             let xx:number=this.x+xOffSet;
             let yy:number=this.y+yOffSet; */

            //set position
            this.element.setAttribute("x", this.x.toString());
            this.element.setAttribute("y", this.y.toString());

            let transform = "translate(" + this.offSetX.toString() + "," + this.offSetY.toString() + ")";
            transform += " scale(" + this.scale + ")";
            transform += " rotate(" + this.rotation + ")";
            transform += " skewX(" + this.skewX + ")";
            transform += " skewY(" + this.skewY + ")";

            this.pathElement.setAttribute("transform", transform);
        }

    }
    addClass(className: string) {
        if (this.element) {
            this.element.classList.add(className);
        }

    }
    removeClass(className: string) {
        if (this.element) {
            this.element.classList.remove(className);
        }

    }

}