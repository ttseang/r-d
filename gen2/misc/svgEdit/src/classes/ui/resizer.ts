export class Resizer
{
    public element:SVGElement | null = null;
    
    private handles:SVGElement[]=[];

    constructor(resizeObj:SVGElement)
    {
        //get the def tag
        const defs = document.querySelector("defs");
        if (!defs)
        {
            console.error("Resizer: no defs tag");
            return;
           }
        //get the resizer symbol
        const resizer = defs.querySelector("#resizer");
        if (!resizer)
        {
            console.error("Resizer: no resizer symbol");
            return;
        }
        //clone the resizer symbol
        this.element = resizer.cloneNode(true) as SVGElement;

       // this.element = document.createElementNS("http://www.w3.org/2000/svg", "use");
       // this.element.setAttribute("href", "#resizer");

        console.log(this.element);
        
        for (let i:number=0;i<8;i++)
        {
            const handle = this.element.querySelector("#p"+i) as SVGElement;   
            console.log(handle); 
            if (handle)
            {
                this.handles.push(handle);
            }   
        }
        if (this.handles.length<7)
        {
            console.error("Resizer: not enough handles");
            return;
        }
        if (resizeObj)
        {
            this.element.setAttribute("x", resizeObj.getAttribute("x") || "0");
            this.element.setAttribute("y", resizeObj.getAttribute("y") || "0");

            //place the handles around the object clockwise starting at the top left
            const bbox = resizeObj.getBoundingClientRect();
            const x = bbox.x;
            const y = bbox.y;
            const width = bbox.width;
            const height = bbox.height;
            const halfWidth = width/2;
            const halfHeight = height/2;
            const quarterWidth = width/4;
            const quarterHeight = height/4;
            const threeQuarterWidth = quarterWidth*3;
            const threeQuarterHeight = quarterHeight*3;
            this.handles[0].setAttribute("x", (x - 5).toString());
            this.handles[0].setAttribute("y", (y - 5).toString());
            this.handles[1].setAttribute("x", (x + halfWidth - 5).toString());
            this.handles[1].setAttribute("y", (y - 5).toString());
            this.handles[2].setAttribute("x", (x + width - 5).toString());
            this.handles[2].setAttribute("y", (y - 5).toString());
            this.handles[3].setAttribute("x", (x + width - 5).toString());
            this.handles[3].setAttribute("y", (y + halfHeight - 5).toString());
            this.handles[4].setAttribute("x", (x + width - 5).toString());
            this.handles[4].setAttribute("y", (y + height - 5).toString());
            this.handles[5].setAttribute("x", (x + halfWidth - 5).toString());
            this.handles[5].setAttribute("y", (y + height - 5).toString());
            this.handles[6].setAttribute("x", (x - 5).toString());
            this.handles[6].setAttribute("y", (y + height - 5).toString());
            this.handles[7].setAttribute("x", (x - 5).toString());
            this.handles[7].setAttribute("y", (y + halfHeight - 5).toString());

        }
    }
    
}