export class SvgConstants
{
    public static shapes: string[] = ["use","g","rect", "circle", "ellipse", "line", "polyline", "polygon", "path"];
}