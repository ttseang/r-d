import { CardContainer } from "./blocks/CardContainer";
import { TabLine } from "./blocks/TabLine";
import { HtmlObj } from "./core/HTMLObj";

export class WallpaperApp {
    private cardContainer: CardContainer;
    private tabLine:TabLine | null=null;
    private dummyLine:TabLine | null=null;
    private instructionText:HtmlObj;

    constructor() {
        (window as any).wpa=this;
        const noteBook: HTMLElement = document.querySelector(".notebook") as HTMLElement;
        this.cardContainer = new CardContainer();
        this.instructionText=new HtmlObj(document.querySelector(".instruction-text") as HTMLElement || document.createElement("p"));

        if (noteBook) {
            this.tabLine = new TabLine(".tab-line",this.setTabIndex.bind(this), ["Buddies", "Papers"]);
            this.dummyLine=new TabLine(".dummy-line",()=>{}, ["Buddies", "Papers"]);
            this.dummyLine.showTab(1);
        }
    }

    private setTabIndex(index: number) {
        if (this.dummyLine) {
            this.dummyLine.showTab(index);
        }
        switch (index) {
            case 0:
                this.cardContainer.visible=false;
                this.instructionText.setText("choose a buddy to start");
                break;
            case 1:
                this.cardContainer.visible=true;
                this.instructionText.setText("Here are your wallpapers. Choose the one you like best. You will get more of these as you work your way through the course. Tap the wallpaper you’d like to use. Also, if you want to change your wallpaper in the middle of a lesson, just tap “Controls” on the lesson screen and then hit “Wallpapers”.");
                break;
        }
    }

}