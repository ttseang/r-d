export class WallpaperVo
{
    public name:string;
    public title:string;
    public image:string;
    public locked:boolean;

    constructor(name:string,title:string,image:string,locked:boolean=false)
    {
        this.name=name;
        this.title=title;
        this.image=image;
        this.locked=locked;
    }
}