import { HtmlObj } from "../core/HTMLObj";
import { WallpaperVo } from "../dataObjects/WallpaperVo";

export class WCard extends HtmlObj {
    private wallpaper: WallpaperVo;
    private selected: boolean = false;
    private callback: Function;

    constructor(wallpaper: WallpaperVo, index: number, callback: Function) {
        super(document.createElement("article"));
        this.wallpaper = wallpaper;
        this.callback = callback;
        if (this.el) {
            this.el.onclick = () => { this.callback(index) };
        }
        this.render();
    }
    setSelected(selected: boolean) {
        let change: boolean = (selected === this.selected) ? false : true;
        this.selected = selected;
        if (change) {
            this.render();
        }

    }
    render() {
        let image: string = (this.wallpaper.locked === false) ? this.wallpaper.image : "../assets/locked.png";
        let title: string = (this.wallpaper.locked === false) ? this.wallpaper.title : "LOCKED";
      //  let selectedStyle: string = (this.selected === true) ? "fcard__text fcard--selected" : "fcard__text";

        if (this.el) {
            this.el.classList.add("wcard");
            this.el.innerHTML = `<h2>${title}</h2><figure class="wcard__figure" style="background-image:url(${image});"></figure>`;
        }
    }
}