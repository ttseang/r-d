import { HtmlObj } from "../core/HTMLObj";
import { WallpaperVo } from "../dataObjects/WallpaperVo";
import { FCard } from "./FCard";

export class CardContainer extends HtmlObj{
   // private el: HTMLElement;
    private wallpapers: WallpaperVo[] = [];
    public selectedCard: number = 4;
    //private cards: FCard[] = [];
    private cards: FCard[] = [];
    
    constructor() {
        super(document.querySelector("main") as HTMLElement || document.createElement("main"),"grid");
        this.getData();
    }
    getData() {
        fetch("./assets/m3wallpapers.json")
            .then(response => response.json())
            .then(data => this.processData(data));
    }
    private processData(data: any) {
        //console.log(data);
        for (let i: number = 0; i < data.length; i++) {
            let paperData: any = data[i];
            let image: string = "https://ttv4.s3.amazonaws.com/wallpaper/" + paperData[0] + ".T.jpg"

            let wallpaperVo: WallpaperVo = new WallpaperVo(paperData[0], paperData[1], image);
           /*  if (i > 50) {
                wallpaperVo.locked = true;
            } */
            this.wallpapers.push(wallpaperVo);
        }
        this.makeCards();
    }
    private selectCard(index: number) {
        for (let i: number = 0; i < this.cards.length; i++) {
            if (i === index) {
                this.cards[i].setSelected(true);
            }
            else {
                this.cards[i].setSelected(false);
            }
        }
    }
    private makeCards() {

        for (let i: number = 0; i < this.wallpapers.length; i++) {
            let card: FCard = new FCard(this.wallpapers[i], i, this.selectCard.bind(this));

            this.cards.push(card);

            if (this.selectedCard === i) {
                card.setSelected(true);
            }

            if (card.el) {
                if (this.el)
                {
                    this.el.appendChild(card.el);
                }
                
            }
        }
    }
}