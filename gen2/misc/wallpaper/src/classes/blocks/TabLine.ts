import { PaperButton } from "../buttons/PaperButton";
import { HtmlObj } from "../core/HTMLObj";

export class TabLine extends HtmlObj {
    private callback: Function;
    private buttons: string[];
    private buttonElements: HTMLElement[] = [];

    constructor(className: string, callback: Function, buttons: string[]) {
        super(document.querySelector(className));
        this.callback = callback;
        this.buttons = buttons;
        console.log(this.el);
        if (this.el) {
            //this.el.classList.add("tab-line");
            this.makeButtons();
        }

    }
    private makeButtons() {
        for (let i: number = 0; i < this.buttons.length; i++) {
            let paperButton: PaperButton = new PaperButton(this.buttons[i], i, this.callback);
            if (this.el) {
                if (paperButton.el) {
                    this.el.appendChild(paperButton.el);
                }
            }
        }
    }
   
    public showTab(index: number) {
        
        if (this.el) {
            //loop through each child of the tabline element
            for (let i: number = 0; i < this.el.children.length; i++) {
                //if the child is the one we want to keep visible
                if (i !== index) {
                    //hide it
                    this.el.children[i].classList.add("hid");
                }
                else{
                    //show it
                    this.el.children[i].classList.remove("hid");
                }
            }
        }
    }
}