import { HtmlObj } from "../core/HTMLObj";

export class PaperButton extends HtmlObj
{
    private text:string;
    private index:number;
    private callback:Function;

    constructor(text:string,index:number,callback:Function)
    {
        super(document.createElement("div"));
        this.text=text;
        this.callback=callback;
        this.index=index;
        if (this.el)
        {
            this.el.classList.add("paper-button");
        }
        this.render();
    }
    render()
    {
        if (this.el)
        {
            this.el.innerHTML=`<div class="paper-button__text">${this.text}</div>`;
            this.el.onclick=()=>{this.callback(this.index)};
        }
    }
}