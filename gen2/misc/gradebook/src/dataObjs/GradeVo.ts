//make a data object for the grade with the following properties lesson, total problems, numcompleted,numcorrect,percentscore,lastwork,gradeoneach
export class GradeVo
{
    lesson:string;
    totalproblems:number;
    numcompleted:number;
    numcorrect:number;
    percentscore:number;
    lastwork:string;
    gradeoneach:boolean;
    constructor(lesson:string="",totalproblems:number=0,numcompleted:number=0,numcorrect:number=0,percentscore:number=0,lastwork:string="",gradeoneach:boolean=false)
    {
        this.lesson=lesson;
        this.totalproblems=totalproblems;
        this.numcompleted=numcompleted;
        this.numcorrect=numcorrect;
        this.percentscore=percentscore;
        this.lastwork=lastwork;
        this.gradeoneach=gradeoneach;
    }
    fillWithRandValues()
    {
        this.lesson="lesson"+Math.floor(Math.random()*100);
        this.totalproblems=Math.floor(Math.random()*100);
        this.numcompleted=Math.floor(Math.random()*100);
        this.numcorrect=Math.floor(Math.random()*100);
        this.percentscore=Math.floor(Math.random()*100);
        this.lastwork="lastwork"+Math.floor(Math.random()*100);
        this.gradeoneach=Math.random()>0.5;      

    }
    getHtml(index:number):string
    {
       const valueArray:any[]=[this.lesson,this.totalproblems,this.numcompleted,this.numcorrect,this.percentscore,this.lastwork,this.gradeoneach];
      return '<h2>'+valueArray[index]+'</h2>';
      

    }
}