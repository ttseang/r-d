import { TTScrollbar } from "@teachingtextbooks/ttscrollbar";
import { GradeVo } from "../dataObjs/GradeVo";

export class Gradebook {
    private grades: GradeVo[] = [];
    private ttScrollBar: TTScrollbar;
    private scrollContent: HTMLElement;

    constructor() {
        this.makeFakeGrades();

        const scrollGroup: HTMLElement = document.getElementsByClassName("gradescrollgroup")[0] as HTMLElement;
        this.scrollContent = document.getElementsByClassName("grade-scroll")[0] as HTMLElement;

        /*  console.log("scrollGroup", scrollGroup);
          console.log("scrollContent",this.scrollContent); */

        this.ttScrollBar = new TTScrollbar(scrollGroup, this.scrollContent, this.updateScroll.bind(this));
        this.ttScrollBar.addCssToHead();
        this.ttScrollBar.setImage("track", "./assets/scroll/scrollback.png");
        this.ttScrollBar.setImage("knob", "./assets/scroll/thumbScroll.png");
        this.ttScrollBar.setImage("btnUp", "./assets/scroll/arrowUp.png");
        this.ttScrollBar.setImage("btnDown", "./assets/scroll/arrowDown.png");
        this.ttScrollBar.setScrollbarWidthModifier(2);

        scrollGroup.appendChild(this.ttScrollBar.element);
    }
    updateScroll(percent: number) {

        if (this.scrollContent) {
            const scrollHeight: number = this.scrollContent.scrollHeight;
            const px: number = scrollHeight * percent / 100;
            this.scrollContent.scrollTo(0, px);
        }

    }
    makeFakeGrades() {
        for (let i = 0; i < 100; i++) {
            let grade = new GradeVo();
            grade.fillWithRandValues();
            this.grades.push(grade);
        }
        this.render();
    }

    render() {
        const gradeScroll: HTMLElement = document.querySelector(".grade-scroll") as HTMLElement;
        gradeScroll.innerHTML = "";
        for (let i = 0; i < this.grades.length; i++) {
            let grade = this.grades[i];

            for (let j = 0; j < 7; j++) {
                let gradeDiv: HTMLElement = document.createElement("article");
                gradeDiv.innerHTML = grade.getHtml(j);
                if (i / 2 == Math.floor(i / 2)) {
                    gradeDiv.classList.add("odd");
                }
                else {
                    gradeDiv.classList.add("even");
                }

                gradeScroll.appendChild(gradeDiv);
            }
        }
    }
}