export enum ServerStatus
{
    Up,
    Down,
    Unknown
}