// All of the Node.js APIs are available in the preload process.
import { contextBridge, ipcMain, ipcRenderer } from "electron";
import Main = require("electron/main");
import { ServerController } from "./classes/mc/ServerController";
import { BaseScreen } from "./classes/view/BaseScreen";



// It has the same sandbox as a Chrome extension.
window.addEventListener("DOMContentLoaded", () => {

  const serverController:ServerController=ServerController.getInstance();
  serverController.showNotification=(title:string,msg:string)=>{
    //console.log(msg);
    ipcRenderer.send("show-notification",title, msg);
  };

  const replaceText = (selector: string, text: string) => {
    const element = document.getElementById(selector);
    if (element) {
      element.innerText = text;
    }
  };

  for (const type of ["chrome", "node", "electron"]) {
    replaceText(`${type}-version`, process.versions[type as keyof NodeJS.ProcessVersions]);
  }

  
  new BaseScreen();
});

