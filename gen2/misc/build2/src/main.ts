import { app, BrowserWindow, ipcMain, Menu, MenuItem, Notification,Tray } from "electron";
import * as path from "path";
import { ServerModel } from "./classes/mc/ServerModel";

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
      nodeIntegration: true,
      contextIsolation: true,
    },
    icon: path.join(__dirname, "./assets/server.png"),
    width: 800,
  });
  app.name = "Server Checker";
  app.setAppUserModelId(app.name);
  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, "../index.html"));

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
}


function showNotification(title:string,msg: string) {
  const serverModel: ServerModel = ServerModel.getInstance();
  if (serverModel.showNotification==false) {
    return;
  }
  new Notification({ title: title, body: msg }).show();

}
function buildTray()
{
  const serverModel: ServerModel = ServerModel.getInstance();
  //build tray
  const tray = new Tray(path.join(__dirname, "./assets/server.png"));
   const contextMenu =new Menu();
  const menuItem1:MenuItem=new MenuItem({label: 'Show Notifications', type: 'checkbox', checked: serverModel.showNotification,click:()=>{serverModel.showNotification=true;} });
//  const menuItem2:MenuItem=new MenuItem({label: 'Hide Notifications', type: 'radio', checked: !serverModel.showNotification,click:()=>{serverModel.showNotification=false;} });
  contextMenu.append(menuItem1);
 // contextMenu.append(menuItem2);
  
  contextMenu.on('menu-will-show', (event: any) => {
    menuItem1.checked=(serverModel.showNotification==true);
   // menuItem2.checked=(serverModel.showNotification==false);
    console.log(menuItem1);
   // console.log(menuItem2);
    console.log(serverModel.showNotification);
    //tray.setContextMenu(contextMenu);
  });
    
 
  
  tray.setToolTip('Server Checker')
  tray.setContextMenu(contextMenu);
}
function buildMenu() {
  const serverModel: ServerModel = ServerModel.getInstance();
  //build menu
  const menuTemplate: any[] = [];
 
 
  
  menuTemplate.push({
    label: "View",
    submenu: [
      {
        label: "Show Notifications",
        accelerator: "Ctrl+Shift+N",
        type: "checkbox",
        checked: serverModel.showNotification==true,
        click: () => {
          serverModel.showNotification=!serverModel.showNotification;
          buildMenu();
        }
      }]
  });
  const menu: Menu = Menu.buildFromTemplate(menuTemplate);
  menu.on('menu-will-show', (event: any) => {
    menu.items[0].submenu.items[0].checked=serverModel.showNotification;
    console.log(serverModel.showNotification);
    console.log(menu.items[0].submenu.items[0].checked);
  });
  Menu.setApplicationMenu(menu);
}


ipcMain.on('show-notification', (e: Event, title:string,msg: string) => {
  showNotification(title,msg);
});


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();
  buildMenu();
  buildTray();
  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });

});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
