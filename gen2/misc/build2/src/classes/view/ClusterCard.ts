import { ClusterVo } from "../dataObjects/ClusterVo";
import { ServerVo } from "../dataObjects/ServerVo";
import { StatusVo } from "../dataObjects/StatusVo";
import { ServerController } from "../mc/ServerController";
import { ServerModel } from "../mc/ServerModel";
import { ServerStatus } from "../ServerConstants";

export class ClusterCard
{
    private serverList:ServerVo[]=[];
    private sm:ServerModel=ServerModel.getInstance();
    private clusterVo:ClusterVo;
    private parent:HTMLElement;
    private serverController:ServerController=ServerController.getInstance();
    
    constructor(parent:HTMLElement,clusterVo:ClusterVo)
    {
        this.clusterVo=clusterVo;
        this.parent=parent;

        this.serverList=this.sm.getCluster(clusterVo.id);
    }
    makeCard()
    {
        let card:HTMLElement=document.createElement("li");
        card.classList.add("clusterCard");

        card.onclick=()=>{
            this.serverController.showList(this.clusterVo.id);
        };
        
        let heading:HTMLElement=document.createElement("h2");
        heading.innerHTML=this.clusterVo.label;
        card.appendChild(heading);

        let para:HTMLElement=document.createElement("p");
        para.innerHTML=this.serverList.length+" endpoints";
        card.appendChild(para);

        let list:HTMLElement=document.createElement("ul");
        for(let i:number=0;i<this.serverList.length;i++)
        {
            let li:HTMLElement=document.createElement("li");

            let server:ServerVo=this.serverList[i];
            let statusVo:StatusVo=server.statusVo;
            let markClass:string="";
            if (server.loading==true)
            {
                markClass="loading";
            }
            else{
                switch(statusVo.isUp)
                {
                    case ServerStatus.Up:
                        markClass="good";
                        break;
                    case ServerStatus.Down:
                        markClass="bad";
                        break;
                    case ServerStatus.Unknown:
                        markClass="unknown";
                        break;
                }
            }
            

            let mark:HTMLElement=document.createElement("mark");
            mark.classList.add(markClass);
            li.appendChild(mark);
            list.appendChild(li);
        }
        card.appendChild(list);
        this.parent.appendChild(card);
    }
}