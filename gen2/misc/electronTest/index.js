const { app, BrowserWindow, Notification } = require('electron');
const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600
  })

  win.loadFile('app/index.html')
}

const NOTIFICATION_TITLE = 'Basic Notification'
const NOTIFICATION_BODY = 'Notification from the Main process'

function showNotification() {
  if (!Notification.isSupported()) {
    console.log('Notifications are not supported on your system')
    return
  }
  console.log('Showing notification');
  new Notification({ title: NOTIFICATION_TITLE, body: NOTIFICATION_BODY,closeButtonText:"Close" }).show()
}
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
});
app.whenReady().then(createWindow);
//.then(showNotification);