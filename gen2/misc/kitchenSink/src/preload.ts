// All of the Node.js APIs are available in the preload process.
import { contextBridge, ipcMain, ipcRenderer } from "electron";
import Main = require("electron/main");


const win: any = Window;

// It has the same sandbox as a Chrome extension.
window.addEventListener("DOMContentLoaded", () => {


  const replaceText = (selector: string, text: string) => {
    const element = document.getElementById(selector);
    if (element) {
      element.innerText = text;
    }
  };

  for (const type of ["chrome", "node", "electron"]) {
    replaceText(`${type}-version`, process.versions[type as keyof NodeJS.ProcessVersions]);
  }

  // Expose protected methods that allow the renderer process to use
  // the ipcRenderer without exposing the entire object
  contextBridge.exposeInMainWorld(
    "api", {
    send: (channel: string, data: any) => {
      // whitelist channels
      let validChannels = ["toMain"];
      if (validChannels.includes(channel)) {
        ipcRenderer.send(channel, data);
      }
    },
    receive: (channel: string, func: (arg0: any) => void) => {
      let validChannels = ["fromMain"];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender` 
        console.log("in receive");

        //  ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    }
  }
  );
  function sendMessage() {
    console.log((window as any).api);
    console.log(window);
    //console.log(this);
    //  console.log(win.api);
    //can not get access to the window object

    ipcRenderer.send("toMain","notify:hello from renderer");
    // win.api.send("toMain", "hello from renderer");
    // (window as any).api.send("toMain", "hello from renderer");
  }
  const button = document.getElementById("btnNotify");
  button.addEventListener("click", () => {
    sendMessage();
  });

  const btnRead: HTMLElement = document.getElementById("btnRead");
  btnRead.addEventListener("click", () => {
    ipcRenderer.sendSync("toMain", "readClipboard");
  });

  const btnWrite: HTMLElement = document.getElementById("btnWrite");
  btnWrite.addEventListener("click", () => {
    ipcRenderer.sendSync("toMain", "writeClipboard:hello from renderer");
  });

});


