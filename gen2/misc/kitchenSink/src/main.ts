import { app, BrowserWindow, clipboard, ipcMain, Menu, MenuItem, Notification, Tray } from "electron";
import * as path from "path";


function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
      nodeIntegration: true,
      contextIsolation: true,
    },
    icon: path.join(__dirname, "./assets/server.png"),
    width: 800
  });

  //set app name
  app.name = "Server Checker";

  //set the app user model id this is the id that is used to identify the app in the notification settings
  app.setAppUserModelId(app.name);

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, "../index.html"));

  // Open the DevTools.
  //turn off for production
  mainWindow.webContents.openDevTools();
}



function buildTray() {

  //build tray, include the icon path
  const tray = new Tray(path.join(__dirname, "./assets/server.png"));
  //make a context menu for the tray
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show App', click: function () {
        console.log("show app");
      }
    },
    {
      label: 'Quit', click: function () {
        app.quit();
      }
    }
  ]);
  //set the tray tool tip
  tray.setToolTip('Server Checker');

  tray.setContextMenu(contextMenu);
}
function buildMenu() {
  //create the menu object
  const menu = new Menu();
  //add a file menu
  menu.append(new MenuItem({
    label: "File",
    submenu: [
      {
        label: "Quit",
        click: () => {
          app.quit();
        },
      },
    ],
  }));

  //add the menu to the app
  Menu.setApplicationMenu(menu);


//custom functions here to be called from the renderer or preload process
function showNotification(title: string, msg: string) {

  new Notification({ title: title, body: msg }).show();

}

function getClipboardText()
{
  return clipboard.readText();
}
function setClipboardText(text:string)
{
  clipboard.writeText(text);
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();
  buildMenu();
  buildTray();

  //this is listening for the message from the renderer or preload process
  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
  ipcMain.on("toMain", (event, args) => {
    console.log(args);
    let textArray = args.split(":");
    let verb: string = textArray[0];
    let noun: string = textArray[1];
    let object: string = textArray[2];
    switch (verb) {
      case "showNotification":
        showNotification(noun,object);
        break;
      case "readClipboard":
        console.log("read clipboard");
        showNotification("Clipboard",getClipboardText());
        break;

      case "writeClipboard":
        console.log("write clipboard");
        setClipboardText(object);
        break;
      default:
        console.log("unknown verb");
        break;
    }
    
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
});
}
