import { ServerVo } from "../dataObjects/ServerVo";
import { ServerStatus } from "../ServerConstants";

export class StatusBar
{
    private serverVo:ServerVo;
    private parent:HTMLElement;
    private element:HTMLElement;
    public constructor(serverVo:ServerVo,parent:HTMLElement)
    {
        this.serverVo = serverVo;
        this.parent = parent;
        this.element = document.createElement("li");
        this.element.classList.add("statusBar");
        this.parent.appendChild(this.element);
        
        //make a figure element for the icon and a span element for the label
        let figure:HTMLElement = document.createElement("figure");
        figure.classList.add("statusicon");
        let label:HTMLElement = document.createElement("span");
        let domainLabel:HTMLElement = document.createElement("span");
        let statusLabel:HTMLElement = document.createElement("span");
        let responseTimeLabel:HTMLElement = document.createElement("span");
        
        responseTimeLabel.classList.add("repsonseTime");

        this.element.appendChild(figure);
        this.element.appendChild(label);
        this.element.appendChild(domainLabel);
        this.element.appendChild(statusLabel);
        this.element.appendChild(responseTimeLabel);

        responseTimeLabel.innerHTML = "response time:"+this.serverVo.statusVo.time+"ms";

        label.innerHTML = this.serverVo.label;
        domainLabel.innerHTML = this.serverVo.domainName;
        let statusText:string = "";
        
        label.classList.add("labelText");
        domainLabel.classList.add("domainText");

        switch(this.serverVo.statusVo.isUp)
        {
            case ServerStatus.Up:
                statusText = "Good";
                figure.classList.add("green");
                statusLabel.classList.add("goodText");
                break;
            case ServerStatus.Down:
                statusText = "Bad";
                figure.classList.add("red");
                statusLabel.classList.add("badText");
                break;
            case ServerStatus.Unknown:
                statusText = "Unknown";
                figure.classList.add("grey");
                statusLabel.classList.add("unknownText");
               
                break;
        }
        if (this.serverVo.loading==true)
        {
            figure.classList.add("animated");
        }
        statusLabel.innerHTML = statusText;

        //this.element.innerHTML = this.serverVo.label+" "+this.serverVo.statusVo.isUp+" "+this.serverVo.statusVo.time;
    }
    
}