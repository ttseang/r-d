/* eslint-disable @typescript-eslint/no-inferrable-types */
import { ServerStatus } from "../ServerConstants";

export class StatusVo
{
    public isUp:ServerStatus;
    public time:number;
    constructor(isUp:ServerStatus=ServerStatus.Unknown, time:number=-1)
    {
        this.isUp = isUp;
        this.time = time;
    }
}