import { ClusterVo } from "../dataObjects/ClusterVo";
import { ServerController } from "../mc/ServerController";
import { ServerModel } from "../mc/ServerModel";
import { ClusterCard } from "./ClusterCard";

export class ClusterScreen
{
    private sm:ServerModel=ServerModel.getInstance();
    private serverController:ServerController=ServerController.getInstance();
    
    constructor()
    {
        //do nothing
       
    }
    public start()
    {
        this.makeScreen();
        this.serverController.updateDraw=this.makeScreen.bind(this);
        this.serverController.updateTime=this.updateTimer.bind(this);
    }
    private updateTimer(sec:number)
    {
        const secsTextHolder:HTMLElement=document.querySelector(".secondText") as HTMLElement;
        if (secsTextHolder)
        {
            secsTextHolder.innerHTML = sec.toString() + " seconds until next check";
        }
    }
    clearScreen()
    {
        const main: HTMLElement | null = document.querySelector("main") || document.createElement("main");
        main.innerHTML="";
    }
    makeScreen()
    {
        this.clearScreen();
        const main: HTMLElement | null = document.querySelector("main") || document.createElement("main");


      

        const section:HTMLElement=document.createElement("section");
        section.className="clusterList";
        main.appendChild(section);

        const h2:HTMLElement=document.createElement("h2");
        h2.innerHTML="TT Server Monitor";
        section.appendChild(h2);

        const cardList:HTMLElement=document.createElement("ul");
        cardList.className="clusterCards";
        section.appendChild(cardList);

        const clusterList:ClusterVo[]=this.sm.clusterList;
        for (let i:number=0;i<clusterList.length;i++)
        {
            let card:ClusterCard=new ClusterCard(cardList,clusterList[i]);
            card.makeCard();
            
        }
        
        const p:HTMLElement=document.createElement("p");
        p.className="secondText";
        p.innerHTML="30 seconds until next check";
        section.appendChild(p);
    }
    
}