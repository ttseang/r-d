import { ClusterVo } from "../dataObjects/ClusterVo";
import { ServerVo } from "../dataObjects/ServerVo";

//singleton
export class ServerModel {

    private static instance: ServerModel;
    public serverList: ServerVo[] = [];
    public clusterList: ClusterVo[] = [];
    constructor()
    {
        //do nothing
        (window as any).sm=this;
    }
    public static getInstance(): ServerModel
    {
        if (ServerModel.instance == null)
        {
            ServerModel.instance = new ServerModel();
        }
        return ServerModel.instance;
    }
    public getCluster(cluster:number):ServerVo[]
    {
        let list:ServerVo[]=[];
        for(let i:number=0;i<this.serverList.length;i++)
        {
            if(this.serverList[i].cluster==cluster)
            {
                list.push(this.serverList[i]);
            }
        }
        return list;
    }
}
