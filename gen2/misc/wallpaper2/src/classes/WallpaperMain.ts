import { WallpaperVo } from "./dataObjects/WallpaperVo";
import { WallpaperThumbs } from "./containers/WallpaperThumbs";
import { TabLine } from "./containers/TabLine";
import { TabVo } from "./dataObjects/TabVo";
import { TTScrollbar } from "@teachingtextbooks/ttscrollbar";

export class WallpaperMain {
    private wallpaperVos: WallpaperVo[] = [];
    private tabsVo: TabVo[] = [];
    private wallpaperThumbs: WallpaperThumbs | null = null;
    private tabLine: TabLine | null = null;
    private ttScrollBar: TTScrollbar | null = null;
    private scrollGroup: HTMLElement;

    constructor() {
        this.scrollGroup = document.getElementsByClassName("wpscrollgroup")[0] as HTMLElement;

        this.loadTabData();
    }

    /**
     *load the json data for the tabs
     *
     * @memberof WallpaperMain
     */
    loadTabData() {
        fetch("./assets/wallpapertabs.json")
            .then(response => response.json())
            .then(data => this.tabsloaded({ data }));
    }
    /**
     *make the json data into a TabVo array
     *when the tabs are loaded, we need to load the wallpapers
     * @param {*} data
     * @memberof WallpaperMain
     */

    tabsloaded(data: any) {
        //map data to tabVo objects
        for (let i: number = 0; i < data.data.tabs.length; i++) {
            let tabVo: TabVo = new TabVo(data.data.tabs[i].title, data.data.tabs[i].description);
            this.tabsVo.push(tabVo);
        }
        this.loadWallpaperData();
    }
    /**
     * load the json data for the wallpapers
     */
    loadWallpaperData() {
        fetch("./assets/m3wallpapers.json")
            .then(response => response.json())
            .then(data => this.wallpaperDataLoaded(data));
    }
    /**
     * when the wallpapers json data is loaded convert it to WallpaperVo objects
     * @param data 
     */
    wallpaperDataLoaded(data: any) {
        for (let i: number = 0; i < data.length; i++) {
            let paperData: any = data[i];
            let image: string = "https://ttv4.s3.amazonaws.com/wallpaper/" + paperData[0] + ".T.jpg"

            let wallpaperVo: WallpaperVo = new WallpaperVo(paperData[0], paperData[1], image);

            if (i < 3) {
                wallpaperVo.isNew = true;
            }
            /* if (i > 40) {
                  wallpaperVo.locked = true;
              } */
            this.wallpaperVos.push(wallpaperVo);
        }

        //now that we have the data, we can make the tabs and the thumbnails

        //the tabline is the line of tabs at the top
        this.tabLine = new TabLine("header", (index: number) => { this.tabClicked(index) }, this.tabsVo);

        //the wallpaperThumbs is the line of thumbnails containing the wallpapers
        this.wallpaperThumbs = new WallpaperThumbs(this.wallpaperVos, () => { });
        (window as any).wallpaperThumbs = this.wallpaperThumbs;
        if (this.wallpaperThumbs.el) {
            console.log(this.scrollGroup);

            this.ttScrollBar = new TTScrollbar(this.wallpaperThumbs.el, this.scrollGroup, this.updateScroll.bind(this));
            this.ttScrollBar.addCssToHead();
            this.ttScrollBar.setImage("track", "./assets/scroll/scrollback.png");
            this.ttScrollBar.setImage("knob", "./assets/scroll/thumbScroll.png");
            this.ttScrollBar.setImage("btnUp", "./assets/scroll/arrowUp.png");
            this.ttScrollBar.setImage("btnDown", "./assets/scroll/arrowDown.png");
        }

    }
    updateScroll(percent:number)
    {
        if (this.wallpaperThumbs)
        {
            if (this.wallpaperThumbs.el)
            {
                const scrollHeight:number = this.wallpaperThumbs.el.scrollHeight;
                const px:number = scrollHeight * percent/100;
                this.wallpaperThumbs.el.scrollTo(0,px);
            }
        }
    }
    //when a tab is clicked we need to switch between buddies and wallpapers

    tabClicked(index: number) {
        console.log("tab clicked: " + index);
        this.updateArticleText(index);
        switch (index) {
            case 0:
                if (this.wallpaperThumbs) {
                    this.wallpaperThumbs.visible = false;
                }
                break;
            case 1:
                if (this.wallpaperThumbs) {
                    this.wallpaperThumbs.visible = true;
                }
        }
    }
    //update the article text when a tab is clicked

    updateArticleText(index: number) {
        let article: HTMLElement | null = document.querySelector("main article");
        if (article) {
            article.innerHTML = this.tabsVo[index].description;
        }
    }
    //later we need to update the database or send a signal to the main app when a wallpaper is selected
    updateDatabase() {
        console.log("update database");
        //for later implementation

    }
}
