import { HtmlObj } from "../core/HTMLObj";
import { WallpaperVo } from "../dataObjects/WallpaperVo";

export class WallpaperCard extends HtmlObj {
    private wallpaperVo: WallpaperVo;
    private clickCallback: Function;

    constructor(wallpaperVo: WallpaperVo, callback: Function) {
        super(document.createElement("figure"));
        this.wallpaperVo = wallpaperVo;
        this.clickCallback = callback;
    }
    render() {
        if (this.el) {
            
            if (this.wallpaperVo.isNew == true) {
                this.el.classList.add("new");
            }
            if (this.wallpaperVo.locked === false) {
                this.el.classList.remove("locked");
                this.el.style.setProperty("--thumb", "url(" + this.wallpaperVo.image + ")");
            }
            else
            {
                this.el.classList.add("locked");
            }

           
            this.el.innerHTML = `<figcaption>${this.wallpaperVo.title}</figcaption>`;
            this.el.addEventListener("click", () => { this.clickCallback() });
        }
    }
    destroy() {
        if (this.el) {
            this.el.removeEventListener("click", () => { this.clickCallback() });
        }
    }
    select() {
        if (this.el) {
            this.el.classList.add("on");
        }
    }
    deselect() {
        if (this.el) {
            this.el.classList.remove("on");
        }
    }
}