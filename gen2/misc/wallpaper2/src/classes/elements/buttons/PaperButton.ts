import { HtmlObj } from "../../core/HTMLObj";

export class PaperButton extends HtmlObj
{
    private callback: Function;
    private index: number;
    private text: string;
    constructor(text: string, index: number, callback: Function)
    {
        super(document.createElement("nav"));
        this.text = text;
        this.index = index;
        this.callback = callback;
        this.makeButton();
    }
    private makeButton()
    {
        if (this.el) {
            if (this.text.includes("<br/>") || this.text.includes("<br>")) {
                this.el.classList.add("twoline");
            }

            this.el.innerHTML = this.text;
            this.el.onclick = () => { this.callback(this.index); };
           // this.el.addEventListener("click", this.click.bind(this));
        }
    }
}