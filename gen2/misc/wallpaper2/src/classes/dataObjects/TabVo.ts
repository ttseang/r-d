//make a dataobject for the tab that has title and description
export class TabVo {
    public title: string;
    public description: string;

    constructor(title: string, description: string) {
        this.title = title;
        this.description = description;
    }
}
