
import { HtmlObj } from "../core/HTMLObj";
import { TabVo } from "../dataObjects/TabVo";
import { PaperButton } from "../elements/buttons/PaperButton";

export class TabLine extends HtmlObj {
    private callback: Function;
    private buttons: TabVo[];
    private buttonElements: HTMLElement[] = [];

    constructor(className: string, callback: Function, buttons: TabVo[]) {
        super(document.querySelector(className));
        
       
        this.callback = callback;
        this.buttons = buttons;
        console.log(this.el);
        if (this.el) {
            //this.el.classList.add("tab-line");
            this.makeButtons();
        }

    }
    private makeButtons() {
        for (let i: number = 0; i < this.buttons.length; i++) {
            let paperButton: PaperButton = new PaperButton(this.buttons[i].title, i, this.onButtonClicked.bind(this));
            if (this.el) {
                if (paperButton.el) {
                    this.el.appendChild(paperButton.el);
                }
            }
        }
        this.showTab(1);
    }
    private onButtonClicked(index: number) {
        this.showTab(index);
        this.callback(index);
    }
    
    public showTab(index: number) {
        //we have to subtract 1 from the index because the first element is the back button
        
        if (this.el) {
            //loop through each child of the tabline element
            for (let i: number = 0; i < this.el.children.length; i++) {
                //if the child is the one we want to keep visible
                if (i-1 == index) {
                    //show it
                    this.el.children[i].classList.add("on");
                }
                else{
                    //hide it
                    this.el.children[i].classList.remove("on");
                }
            }
        }
    }
}