

export class HtmlObj {
    public el: HTMLElement | null;

    private _x: number = 0;
    private _y: number = 0;
    private _visible: boolean = false;

    public displayStyle: string;

    constructor(element: HTMLElement | null, displayStyle: string = "block") {
        this.el = element;
        this.displayStyle = displayStyle;
    }
    //x and y only work if the element is positioned in absolute or relative
    //x and y are percentages of the parent element
    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
        if (this.el) {
            this.el.style.left = value.toString() + "%";
        }
    }
    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
        if (this.el) {
            this.el.style.top = value.toString() + "%";
        }
    }
    public get visible(): boolean {
        return this._visible;
    }
    public set visible(value: boolean) {
        this._visible = value;

        if (this.el) {
            if (this.visible == true) {
                this.el.style.display = this.displayStyle;
            }
            else {
                this.el.style.display = "none";
            }
        }

    }

    //set the innner text
    public setText(text: string) {
        if (this.el) {
            this.el.innerText = text;
        }
    }

}