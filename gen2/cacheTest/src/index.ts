import { CacheManager } from "@teachingtextbooks/ttcachemanager";


window.onload=()=>{

    let cm:CacheManager=CacheManager.getInstance();

    cm.getFromCache("mykey","nothing found")

    cm.loadFromLocal("myapp");

    console.log(cm.getFromCache("hp","not found"));

}