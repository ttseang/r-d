export class ApiConnect {
    endPoint = "https://tthq.me/api/";

    async fetchWithTimeout(url: string, options: any = {}) {    
        const abortController = new AbortController();

        const timer = setTimeout(() => {
            abortController.abort();
        }, 8 * 1000); // 8 seconds

        const response = await fetch(url, {
            ...options,
            signal: abortController.signal  
        });

        clearTimeout(timer);
        
        return response;
    }
  
    public async getFileContent(lti:string, callback:Function) {
        let url: string = `${this.endPoint}pr/getequation/${lti}`;

        await this.fetchWithTimeout(url, { method: "post" }).then(response => {
            if (response.ok) {
                response.json().then(json => callback(json));
            }
        });
    }
}

export default ApiConnect;