import { CellVo } from "../dataObjs/CellVo";

export class CellUtil {

    public static makeCols(index: number, cells: CellVo[], columnCount: number) {
        let cols: string[] = [];

        cells.forEach(cell => cols.push(cell.toHtml()));

        while (cols.length < columnCount) {
            cols.unshift("<span>&nbsp;</span>");
        }

        return cols;
    }

    public static dataToCell(data: any) {
        return new CellVo(data._text, data.classArray);
    }

    public static dataToCells(data: any[]) {
        let cells: CellVo[] = [];

        data.forEach(d => cells.push(CellUtil.dataToCell(d)));

        return cells;
    }

    public static gridToCells(data: any[][]) {
        let cells: CellVo[][] = [];

        data.forEach((d1, i) => {
            cells[i] = [];
            d1.forEach((d2, j) => {
                cells[i].push(CellUtil.dataToCell(d2));
            });
        });

        return cells;
    }
}