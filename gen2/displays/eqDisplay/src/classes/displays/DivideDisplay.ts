import { DivideDisplayVo } from "../dataObjs/DivideDisplayVo";
import { CellUtil } from "../util/CellUtil";

export class DivideDisplay {
    private data: DivideDisplayVo;

    constructor(data: DivideDisplayVo) {
        this.data = data;
    }

    private getProductRows() {
        let rows: string[] = [];

        this.data.products.forEach((product, index) => {
            // TODO - need to figure out the hidden?
            const bottom: boolean = !(index % 2);

            rows.push("<div></div>");

            //  rows.push(`<div>${(bottom ? `<span class="bottomBorder">` : "") + CellUtil.makeCols(index, product, 0).join("\n") + (bottom ? "</span>" : "")}</div>`);
            //I moved the bottom border to the div so it would maintain the same parent child relationship as the admin
            //I changed the logic just a bit to make it a bit more readable
            if (bottom) {
                rows.push(`<div class="bottomBorder">${CellUtil.makeCols(index, product, 0).join("\n")}</div>`);
            }
            else {
                rows.push(`<div>${CellUtil.makeCols(index, product, 0).join("\n")}</div>`);
            }
        });

        return rows.join("\n");
    }

    toHtml() {
        const { quotient, divisor, dividend } = this.data;

        return `
            <div class="equation division">
                <div></div>

                <div class="quotient">&nbsp;&nbsp;&nbsp;${CellUtil.makeCols(0, quotient, 0).join("\n")}</div>
                <div class="divisor">${CellUtil.makeCols(0, divisor, 0).join("\n")}&nbsp;</div>
                <div class="dividend">
                    <div class="inner">
                        <i class="circle">&nbsp;</i>
                        ${CellUtil.makeCols(0, dividend, 0).splice(1).join("\n")}
                    </div>
                </div>
                ${this.getProductRows()}
            </div>
        `;
    }
}