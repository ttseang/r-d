import { Unicodes } from "../Constants";
import { FractCellVo } from "../dataObjs/FractCellVo";
import { FractDisplayVo } from "../dataObjs/FractDisplayVo";

export class FractDisplay {
    private data: FractDisplayVo;

    constructor(data: FractDisplayVo) {
        this.data = data;
    }

    getLen2() {
        let len: number = 0;

        const fracts:FractCellVo[] = this.data.fracts;
        fracts.forEach(fract => len += fract.getLen());
        len += fracts.length - 1;

        return len;
    }

    getStyle() {
        let len: number = this.getLen2();

        return `grid-template-columns: repeat(${len}, 1fr)`;
    }

    getFracts() {
        let fArray: string[] = [];
        const { fracts, ops, opCells } = this.data;


        fracts.forEach((fract:FractCellVo, index:number) => {
            if (fract.w > 0) {
                fArray.push(`<div>${fract.getWhole()}</div>`);
            }

            fArray.push(`<div>${fract.getTop()}</div>`);

            if (ops[index]) {
                // if (Unicodes[ops[index]]) {
                //     opCells[index].text = Unicodes[ops[index]];
                // }

                fArray.push(`<div>${opCells[index].toHtml()}</div>`);
            }
        });


        fracts.forEach((fract:FractCellVo) => {
            if (fract.w > 0) {
                fArray.push("<div></div>");
            }
            fArray.push(`<div>${fract.getBottom()}</div>`);
            fArray.push("<div></div>");
        });

        return fArray.join("");
    }

    getHtml() {
        return `
            <div class="equation fraction" style="${this.getStyle()}">
                ${this.getFracts()}
            </div>
        `;
    }
}