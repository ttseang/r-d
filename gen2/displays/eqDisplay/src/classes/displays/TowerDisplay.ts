import { Unicodes } from "../Constants";
import { CellVo } from "../dataObjs/CellVo";
import { TowerDisplayVo } from "../dataObjs/TowerDisplayVo";
import { CellUtil } from "../util/CellUtil";

export class TowerDisplay {
    private data: TowerDisplayVo;
    private columns: number;

    constructor(data: TowerDisplayVo, columns: number) {
        this.data = data;
        this.columns = columns + 1; // add one for the operator
    }

    getTop() {
        let topRows: string[] = [];

        this.data.top.forEach((top, index) => {

            const cols:string[] = CellUtil.makeCols(index, top,this.columns);

            topRows.unshift(`<div class="row small" style="grid-template-columns: repeat(${this.columns}, 1fr);">${cols.join(" ")}</div>`);
        });

        return topRows.join("\n");
    }

    getRows() {
        let rowArray: string[] = [];
        const { rows, operator } = this.data; 

        rows.forEach((row:CellVo[], index:number) => {
            const cols = CellUtil.makeCols(index, row, this.columns);

            // THIS IS THE HACK FOR SPACING!
            if (index === rows.length - 1) {
                cols[0] = `<span>&nbsp;${Unicodes[operator]}</span>`;
            }

            rowArray.push(`<div class="row" style="grid-template-columns: repeat(${this.columns}, 1fr)">${cols.join(" ")}</div>`);
        });

        return rowArray.join("\n");
    }

    getAnswer() {
        const cols:string[] = CellUtil.makeCols(20, this.data.answer, this.columns);
        return `<div class="row answer" style="grid-template-columns: repeat(${this.columns}, 1fr)">${cols.join(" ")}</div>`;
    }

    getHtml() {
        return `
            <div class="equation addition">
                ${this.getTop()}
                ${this.getRows()}
                <hr>
                ${this.getAnswer()}
            </div>
        `;
    }
}