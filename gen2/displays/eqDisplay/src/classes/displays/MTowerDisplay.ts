import { Unicodes } from "../Constants";
import { CellVo } from "../dataObjs/CellVo";
import { MTowerDisplayVo } from "../dataObjs/MTowerDisplayVo";
import { CellUtil } from "../util/CellUtil";

export class MTowerDisplay {
    private data: MTowerDisplayVo;
    private columns: number;

    constructor(data: MTowerDisplayVo, columns: number) {
        this.data = data;
        this.columns = columns + 1;
    }

    getTop() {
        let topRows: string[] = [];
        const top = this.data.top;

        if (top) {
            
            top.forEach((t: CellVo[], index: number) => {
                const cols = CellUtil.makeCols(index, t, this.columns);

                topRows.push(`<div class="row small" style="grid-template-columns: repeat(${this.columns}, 1fr);">${cols.join(" ")}</div>`);
            });
        }

        return topRows.join("\n");
    }

    getRows() {
        let rowArray: string[] = [];

        const rows: CellVo[][] = this.data.rows;

        if (rows) {
            rows.forEach((row: CellVo[], index: number) => {
                const cols = CellUtil.makeCols(index, row, this.columns);

                // THIS IS THE HACK FOR SPACING!
                if (index === rows.length - 1) {
                    cols[0] = `<span>&nbsp;${Unicodes["x"]}</span>`;
                }

                rowArray.push(`<div class="row" style="grid-template-columns: repeat(${this.columns}, 1fr)">${cols.join(" ")}</div>`);
            });
        }

        return rowArray.join("\n");
    }

    getProducts() {
        let productRows: string[] = [];

        const products: CellVo[][] = this.data.products;

        if (products) {
            products.forEach((product: CellVo[], index: number) => {
                const cols = CellUtil.makeCols(index, product, this.columns);
                productRows.push(`<div class="row" style="grid-template-columns: repeat(${this.columns}, 1fr)">${cols.join(" ")}</div>`);

            });
        }
        return productRows.join("\n");
    }

    getCarryRow() {

        const carryRow: CellVo[] = this.data.carryRow;

        if (carryRow.length) {
            const cols = CellUtil.makeCols(30, carryRow, this.columns);
            return `<div class="row" style="grid-template-columns: repeat(${this.columns}, 1fr)">${cols.join(" ")}</div>`;
        }
        
        return "";
    }

    getAnswer() {
        const { answer, products } = this.data;

        if (answer) {
            if (answer.length === 0) {
                return "";
            }
            if (products) {

                const cols = CellUtil.makeCols(20, answer, this.columns);

                return `
                    ${products.length ? "<hr />" : ""}
                    <div class="row answer" style="grid-template-columns: repeat(${this.columns}, 1fr)">
                        ${cols.join(" ")}
                    </div>
                `;
            }
        }

        return "";
    }


    getHtml() {
        return `
            <div class="equation multiply">
                ${this.getTop()}
                ${this.getRows()}
                <hr />
                ${this.getCarryRow()}
                ${this.getProducts()}
                ${this.getAnswer()}
            </div>
        `;
    }
}