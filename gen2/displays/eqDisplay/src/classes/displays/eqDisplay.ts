import ApiConnect from "../util/ApiConnect";
import { CacheManager } from "@teachingtextbooks/ttcachemanager";
import { ProblemTypes } from "../Constants";
import { DivideDisplayVo } from "../dataObjs/DivideDisplayVo";
import { FractDisplayVo } from "../dataObjs/FractDisplayVo";
import { MTowerDisplayVo } from "../dataObjs/MTowerDisplayVo";
import { TowerDisplayVo } from "../dataObjs/TowerDisplayVo";
import { CSSUtil } from "../util/CSSUtil";
import { DivideDisplay } from "./DivideDisplay";
import { FractDisplay } from "./FractDisplay";
import { MTowerDisplay } from "./MTowerDisplay";
import { TowerDisplay } from "./TowerDisplay";

export class EqDisplay {
    private expression: string = "";
    private data: any = {};
    private problemType: ProblemTypes = ProblemTypes.UNSUPORTED;
    private container: HTMLElement;
    private steps: string[] = [];
    public isReady: boolean = false;
    public onReady: Function = () => { };
    public step: number = 0;
    private file: string = "";
    private cm: CacheManager = CacheManager.getInstance();

    constructor(container: HTMLElement | string) {
        CSSUtil.makeCSS();
        this.container = typeof container === "string" ? document.querySelector(container)! : container;
        (window as any).eqDisplay = this;
    }


    async loadFile(file: string, step: number = 0) {
        this.isReady = false;
        this.step = step;
        this.file = file;

        // const cachedData: string = this.cm.getFromCache(file, "");

        // if (cachedData) {
        //     this.data = JSON.parse(cachedData);
        //     this.formatData(this.data);
        //     return;
        // }

        let apiConnect: ApiConnect = new ApiConnect();
        await apiConnect.getFileContent(file, this.process.bind(this));
    }

    process(data: any) {

        this.cm.saveToCache(this.file, JSON.stringify(data), 60000);

        this.data = data;
        this.formatData(this.data);
    }

    formatData(data: any) {
        this.expression = data.eqstring;
        let stat: boolean = this.decideProblem();

        if (stat === false) {
            if (this.container) {
                this.container.style.width = "fit-content";
                this.container.innerHTML = "Invalid LTI Code";
            }
        }
    }

    decideProblem() {
        // console.log("decide problem");
        // console.log(this.expression);
        if (this.expression === undefined || this.expression === "") {
            return false;
        }

        let opArray: string[] = ["+", "-", "*", "/"];

        let problemTypes: ProblemTypes[] = [ProblemTypes.ADDITION, ProblemTypes.SUBTRACTION, ProblemTypes.MULTIPLICATION, ProblemTypes.DIVISION]

        this.problemType = ProblemTypes.UNSUPORTED;

        for (let i: number = 0; i < opArray.length; i++) {
            if (this.expression.includes(opArray[i])) {
                this.problemType = problemTypes[i];
            }
        }
        if (this.expression.includes("_")) {
            this.problemType = ProblemTypes.FRACTIONS;
        }

        // mark we are ready
        this.isReady = true;

        this.makeSteps();
        this.showStep(this.step);

        this.onReady();

        return true;
    }

    showStep(step: number) {
        // it's not ready, so overwrite the step property so when it is ready it will show that step
        if (!this.isReady) {
            this.step = step;
            return;
        }

        this.step = step;

        if (this.container) {
            this.container.style.width = "fit-content";
            let content: string = this.getStep(step);

            if (content === undefined) {
                content = "Step Not Found";
            }
            this.container.innerHTML = content;
        }
        else {
            console.warn("DIV NOT FOUND")

        }
    }

    getStep(step: number) {
        return this.steps[step];
    }

    makeSteps() {
        this.steps = [];

        // find the max columns

        let maxColumns: number = 0;

        if (this.problemType === ProblemTypes.ADDITION || this.problemType === ProblemTypes.SUBTRACTION || this.problemType === ProblemTypes.MULTIPLICATION) {
            const checkDecimals = (columns: any) => {
                columns.reverse();
                for (let i = 0; i < columns.length; i++) {
                    let text = columns[i]._text;
                    if (text === "." || text === ",") {
                        let symClass:string = (text === "." ? "decimal" : "comma");
                        columns[i + 1].classArray.push(symClass);
                        columns.splice(i, 1); // remove it
                        i--; // back track
                    }
                }
                columns.reverse();
            };

            this.data.data.forEach((data: any) => {
                data.top.forEach((top: any) => {
                    checkDecimals(top);
                    maxColumns = Math.max(maxColumns, top.length);
                });
                data.rows.forEach((row: any) => {
                    checkDecimals(row);
                    maxColumns = Math.max(maxColumns, row.length);
                });
                // multiplication
                (data.products || []).forEach((product: any) => {
                    checkDecimals(product);
                    maxColumns = Math.max(maxColumns, product.length);
                });
                checkDecimals(data.answer);
                maxColumns = Math.max(maxColumns, data.answer.length);
            });

            // the max number of columns
            this.data.maxColumns = maxColumns;
        }

        switch (this.problemType) {
            case ProblemTypes.ADDITION: case ProblemTypes.SUBTRACTION: {
                this.data.data.forEach((data: any) => {
                    let towerData: TowerDisplayVo = new TowerDisplayVo([], [], [], "");
                    towerData.fromObj(data);

                    let td: TowerDisplay = new TowerDisplay(towerData, maxColumns);
                    this.steps.push(`<div class="tteq">${td.getHtml()}</div>`);
                });
                break;
            }
            case ProblemTypes.MULTIPLICATION: {
                this.data.data.forEach((data: any) => {
                    let mTowerData: MTowerDisplayVo = new MTowerDisplayVo([], [], [], [], [], "");
                    mTowerData.fromObj(data);

                    let td: MTowerDisplay = new MTowerDisplay(mTowerData, maxColumns);
                    this.steps.push(`<div class="tteq">${td.getHtml()}</div>`);
                });
                break;
            }
            case ProblemTypes.DIVISION: {
                this.data.data.forEach((data: any) => {
                    let divData: DivideDisplayVo = new DivideDisplayVo([], [], [], [], []);
                    divData.fromObj(data);

                    let dd: DivideDisplay = new DivideDisplay(divData);
                    this.steps.push(`<div class="tteq">${dd.toHtml()}</div>`);
                });
                break;
            }
            case ProblemTypes.FRACTIONS: {
                this.data.data.forEach((data: any) => {
                    let fractDisplayVo: FractDisplayVo = new FractDisplayVo([], []);
                    fractDisplayVo.fromObj(data);

                    let fractDisplay: FractDisplay = new FractDisplay(fractDisplayVo);
                    this.steps.push(`<div class="tteq">${fractDisplay.getHtml()}</div>`);
                });
                break;
            }
        }

    }
}