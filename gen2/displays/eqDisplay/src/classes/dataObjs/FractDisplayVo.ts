import { CellUtil } from "../util/CellUtil";
import { CellVo } from "./CellVo";
import { FractCellVo } from "./FractCellVo";

export class FractDisplayVo
{
    public fracts:FractCellVo[];
    public ops:string[];
    public opCells:CellVo[]=[];

    constructor(fracts:FractCellVo[],ops:string[])
    {
        this.fracts=fracts;
        this.ops=ops.slice();

        for (let i:number=0;i<this.ops.length;i++)
        {
            this.opCells.push(new CellVo(this.ops[i],['fop']));
        }
    }
    fromObj(data:any)
    {
        let cells:FractCellVo[]=[];
        for (let i:number=0;i<data.fracts.length;i++)
        {
            let fractData:any=data.fracts[i];
            
            let top:CellVo[]=CellUtil.dataToCells(fractData.top);
            let bottom:CellVo[]=CellUtil.dataToCells(fractData.bottom);
            let whole:CellVo[]=CellUtil.dataToCells(fractData.whole);

            let fCellVo:FractCellVo=new FractCellVo(top,bottom,whole);
            cells.push(fCellVo);
        }
        this.fracts=cells;
        this.ops=data.ops;
        // for (let i:number=0;i<this.ops.length;i++)
        for (let i:number=0;i<data.opCells.length;i++)
        {
            // this.opCells.push(new CellVo(this.ops[i],['fop']));
            this.opCells.push(new CellVo(data.opCells[i]._text, data.opCells[i].classArray));
        }
    }
}
