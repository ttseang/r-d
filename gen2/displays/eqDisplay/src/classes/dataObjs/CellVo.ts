import { Unicodes } from "../Constants";

export class CellVo {
    public text: string = "";
    public classArray: string[];

    constructor(text: string, classArray: string[]) {
        this.text = Unicodes[text] || text;
        this.classArray = classArray;

        //check original text
        switch (text) {
            case "@":
                this.addClass("hid");
            break;
            case "|":
                this.addClass("hid");
                this.addClass("opmargin");
            break;
            case ".":
            case "x":
            case "-":
                this.addClass("opmargin");
            break;
        }

        //check the changed text
        if (this.text==="-" || this.text==="."  || this.text==="*" || this.text==="=" || this.text===">" || this.text==="<") {
            this.addClass("opmargin");
        }
       
    }
    private addClass(cssClass: string) {
        if (this.classArray.indexOf(cssClass) < 0) {
            this.classArray.push(cssClass);
        }
    }
    toHtml() {
        let cssClasses: string[] = this.classArray.slice();

        //if future don't ingore the bb class or any other class from the admin unless we agree to ignore it
        //I'm still passing it in so we can use it for the css and didn't know it was being ignored

         const index: number = cssClasses.findIndex(css => css === "bb");

        if (index >= 0) {
            cssClasses.splice(index, 1);
        }
        //this is returning <span class> instead of <span> when there are no classes hence the if statement
        if (cssClasses.length === 0) {
            return `<span>${this.text}</span>`;
        }
        else
        {
            console.log("cssClasses", cssClasses);
        }
        return `<span ${cssClasses.length ? `class="${cssClasses.join(" ")}"` : ""}>${this.text}</span>`;
    }
}