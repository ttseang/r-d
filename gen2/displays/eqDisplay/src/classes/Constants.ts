export enum ProblemTypes {
    UNSUPORTED,
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION,
    FRACTIONS
};

export const Unicodes: {[type: string]: string} = {
    "+": "\u002B",
    "-": "\u2212",
    "x": "\u00D7",
    "*": "\u2022",
    "<": "\u003C",
    ">": "\u003E",
    "=": "\u003D",
    "@":"0",
    "|": "\u2212"
    // "@": " "
};