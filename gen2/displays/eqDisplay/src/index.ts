
import { EqDisplay } from "./classes/displays/eqDisplay";
import "./css/style.css";
import "./css/mathStyles.css";

if (!module.exports) {
    window.addEventListener("load", async () => {

        let currentStep = 0;
        const samples = document.querySelector("#samples")!;
        // const ltis = ["GA.A", "GA.A2", "GA.S2", "GA.S3", "GA.S4", "GA.F2", "GA.D", "GA.M", "GA.M2", "GA.M3"];
        const ltis = ["TT.RD.DIVTEST2"];
        const eqDisplays: Array<EqDisplay> = [];

        await Promise.all(ltis.map(async (lti, index) => {
            const sample = document.createElement("div") as HTMLElement;
            sample.classList.add("sample");
            sample.innerHTML = `
                <strong>${lti}</strong>
                <div></div>
            `;
            samples.appendChild(sample);

            let eq:EqDisplay = new EqDisplay(sample.querySelector("div")!);
            await eq.loadFile(lti);

            eqDisplays.push(eq);
        }));

        function showStep(step: number) {
            eqDisplays.forEach(eq => eq.showStep(step));

            // show the step number
            controls.querySelector("span")!.innerText = step.toString();
        }

        const controls = document.querySelector(".controls")!;
        controls.addEventListener("click", (event: Event) => {
            const element = event.target as HTMLElement;
            if (element.tagName.toLowerCase() === "button") {
                if (element.classList.contains("controls__prev")) {
                    currentStep--;
                }
                else {
                    currentStep++;
                }

                if (currentStep < 0) {
                    currentStep = 0;
                }

                showStep(currentStep);
            }
        });

        showStep(currentStep);
    });
}

//export { EqDisplay } from "./classes/displays/eqDisplay";