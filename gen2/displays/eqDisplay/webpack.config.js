const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const config = {
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
    library: {
      name: "@teachingtextbooks/eqdisplay",
      type: "umd"
    },
  },
  module: {
    rules: [{
      test: /\.ts(x)?$/,
      loader: 'ts-loader',
      exclude: /node_modules/
    }, {
      test: /\.css$/i,
      use: ["style-loader", "css-loader"],
    }]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: 'src/index.html' }, { from: 'data/*' }],
    })
  ]
};

module.exports = config;