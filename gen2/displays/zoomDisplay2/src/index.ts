import { ButtonControls } from "./classes/core/ButtonControls";
import { ZoomyDisplay } from "./classes/zoomyDisplay";

window.onload = () => {


    const zoomyDisplay = new ZoomyDisplay(() => {
        const buttonControls: ButtonControls = new ButtonControls();
    });
    //check for an html tag with the id of bookData in the parent of the iframe
    const bookDataElement = window.parent.document.getElementById("bookData");
    console.log(bookDataElement);
   
    if (bookDataElement) {
        console.log("bookDataElement exists");
        let data: string = bookDataElement.innerHTML;
        console.log(data);
        //if it exists, load the data from the tag
        zoomyDisplay.loadPreview(data);
    } else {
        console.log("bookDataElement does not exist");
        //if it doesn't exist, load the demo data

        zoomyDisplay.loadData("./assets/smallPreTest.json");
    }

    //todo:fix the volume button on mobile
    //todo: fix the drag slider on mobile


};