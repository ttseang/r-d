import { ZoomyController } from "../core/ZoomyController";
import { ZoomyModel } from "../core/ZoomyModel";

export class AudioPlayer
{
    private static instance: AudioPlayer;
    private audio: HTMLAudioElement | null = null;
    private backgroundAudio: HTMLAudioElement | null = null;
    private zoomyModel: ZoomyModel = ZoomyModel.getInstance();
    private zoomyController: ZoomyController = ZoomyController.getInstance();
    private backgroundAudioPath:string = "";
    
    constructor()
    {
        (window as any).audioPlayer = this;
        this.zoomyController.audioSpeedChanged = () => this.audioUpdated();
        this.zoomyController.audioVolumeChanged = () => this.audioUpdated();
        this.zoomyController.backgroundAudioVolumeChanged = () => this.audioUpdated();
        this.zoomyController.togglePlayPause = () => this.togglePlayPause();
    }
    public static getInstance(): AudioPlayer {
        if (!AudioPlayer.instance) {
            AudioPlayer.instance = new AudioPlayer();
        }
        return AudioPlayer.instance;
    }
    private togglePlayPause()
    {
        if (this.audio)
        {
            if (this.audio.paused)
            {
                this.audio.play();
                this.zoomyModel.isPlaying = true;
            }
            else
            {
                this.audio.pause();
                this.zoomyModel.isPlaying = false;
            }
        }
        if (this.backgroundAudio)
        {
            if (this.backgroundAudio.paused)
            {
                this.backgroundAudio.play();
                this.zoomyModel.isPlaying = true;
            }
            else
            {
                this.backgroundAudio.pause();
                this.zoomyModel.isPlaying = false;
            }
        }
        this.zoomyController.onAudioChanged();
    }
    public playAudio(path:string)
    {
        path = this.zoomyModel.getAudioPath(path);
        //destroy old audio
        if (this.audio)
        {
            this.audio.removeEventListener('ended', () => this.zoomyController.audioComplete());
            this.audio.pause();
            this.audio = null;
        }

        this.audio = new Audio(path);
        this.audioUpdated();
        this.audio.play();
        this.zoomyModel.isPlaying = true;
        this.audio.addEventListener('ended', () => this.zoomyController.audioComplete());
        this.zoomyController.onAudioChanged();
    }
    public stopAudio()
    {
        console.log("stopAudio");
        if (this.audio)
        {
            this.audio.pause();
            this.audio = null;
        }
        this.zoomyModel.isPlaying = false;
        this.zoomyController.onAudioChanged();
    }
    public playBackgroundAudio(path:string)
    {
        if (this.backgroundAudioPath == path) return;
        this.backgroundAudioPath = path;
        
        path = this.zoomyModel.getBackgroundAudioPath(path);

        //destroy old audio
        if (this.backgroundAudio)
        {
            this.backgroundAudio.pause();
            this.backgroundAudio = null;
        }
        this.backgroundAudio = new Audio(path);
        this.backgroundAudio.loop = true;
        this.audioUpdated();
        this.backgroundAudio.play();
    }
    private audioUpdated() {
        if (this.audio) {
            this.audio.volume = this.zoomyModel.audioVolume;
            this.audio.playbackRate = this.zoomyModel.audioSpeed;
        }
        if (this.backgroundAudio) {
            this.backgroundAudio.volume = this.zoomyModel.backgroundAudioVolume;
            if (this.zoomyModel.backgroundAudioMuted)
            {
                this.backgroundAudio.volume = 0;
            }
        }
        this.zoomyController.onAudioChanged();
    }
}