
import { ZoomyController } from "../core/ZoomyController";
import { ZoomyModel } from "../core/ZoomyModel";
import { CardElement } from "../elements/CardElement";
import { LineElement } from "../elements/LineElement";
import { ShapeElement } from "../elements/ShapeElement";
import { ElementExtrasVo } from "./ElementExtrasVo";
import {TextCurveElement} from "../elements/TextCurveElement"

export class PageElementVo {

    public static TYPE_IMAGE: string = "IMAGE";
    public static TYPE_TEXT: string = "TEXT";
    public static TYPE_BACK_IMAGE = "BACK_IMAGE";
    public static TYPE_CARD: string = "CARD";
    public static TYPE_LINE: string = "LINE";
    public static TYPE_SHAPE: string = "SHAPE";

    public static SUB_TYPE_NONE: string = "none";
    public static SUB_TYPE_GUTTER: string = "gutter";
    public static SUB_TYPE_POPUP_LINK: string = "popuplink";

    public static SUB_TYPE_GREEN_CARD: string = "greenCard";
    public static SUB_TYPE_WHITE_CARD: string = "whiteCard";

    public static SUB_TYPE_SPECIAL: string = "special";

    public delFlag: boolean = false;
    //unique id to instance
    public id: string;
    //id link to element
    //used to know if we need to add or update 
    //an element

    public eid: string;
    public type: string;
    public subType: string;
    public classes: string[];
    public content: string[];
    public x: number;
    public y: number;
    public w: number;
    public h: number;
    public textStyle: number = -1;

    public extras: ElementExtrasVo;

    public static elementID: number = 0;
    private zoomyModel: ZoomyModel = ZoomyModel.getInstance();
    private zoomyController: ZoomyController = ZoomyController.getInstance();

    private origins: string[];


    constructor(id: string = "", type: string = "", subType: string = "", classes: string[] = [], content: string[] = [], x: number = 0, y: number = 0, w: number = 0, h: number = 0) {
        this.id = id;
        this.type = type;
        this.subType = subType;
        this.classes = classes;
        this.content = content;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.eid = "instance0";

        this.origins = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
        this.extras = new ElementExtrasVo();



    }
    fromObj(obj: any) {
        //console.log("PE");
        //console.log(obj);

        PageElementVo.elementID++;
        this.id = "Element" + PageElementVo.elementID.toString();

        this.type = obj.type;
        this.subType = obj.subType;
        this.classes = obj.classes;
        this.content = obj.content;
        this.textStyle = parseInt(obj.textStyle);


        this.eid = obj.eid;
        this.x = parseFloat(obj.x);
        this.y = parseFloat(obj.y);
        this.w = parseFloat(obj.w);
        this.h = parseFloat(obj.h) || 0;

        this.extras.type = this.type;
        this.extras.fromObj(obj.extras);

        /*  //console.log(obj.subtype);
         //console.log(this); */

    }
    getTextCSS() {
        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;
        console.log('FONT SIZE: '+this.extras.fontSize);
        cssStyle.fontSize=this.extras.fontSize.toString()+'px';
        cssStyle.fontFamily = this.extras.fontName;
        cssStyle.color = this.extras.fontColor;

        return cssStyle;
    }
    private getClasses() {
        let classArray: string[] = this.classes.slice();

        if (this.type === PageElementVo.TYPE_LINE) {
            return [];
        }

        if (this.type === PageElementVo.TYPE_CARD) {

            classArray.push("infoCard");
            switch (this.subType) {
                case PageElementVo.SUB_TYPE_GREEN_CARD:
                    classArray.push("green");
                    break;

                case PageElementVo.SUB_TYPE_WHITE_CARD:
                    classArray.push("white");
                    break;
            }
        }
        return classArray;
    }
    public getCss() {
        
        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;
        cssStyle.position = "absolute";
        cssStyle.left = this.x.toString() + "%";
        cssStyle.top = this.y.toString() + "%";

        if (this.w !== 0 && this.type !== PageElementVo.TYPE_CARD && this.type !== PageElementVo.TYPE_LINE && this.type !== PageElementVo.TYPE_TEXT) {
            cssStyle.width = this.w.toString() + "%";
        }

        //todo: replace with a class
         if (this.type === PageElementVo.TYPE_TEXT) {
            if(this.extras.textCurve.label !== 'None'){
                
                
            }
            var fontVw = this.extras.fontSize/16;
            cssStyle.fontSize = ""+fontVw+"vw";
        } 
        if (this.extras) {
            let transformString: string = "translate(" + this.origins[this.extras.orientation] + ")";


            if (this.extras.alpha) {
                cssStyle.opacity = this.extras.alpha.toString() + "%";
            }
            if (this.extras.borderRadius!==0) {
                cssStyle.borderRadius = this.extras.borderRadius.toString() + "px";
            }
            if (this.extras.fade!==100) {
                //set  -webkit-mask-image linear-gradient
                //-webkit-mask-image:linear-gradient(rgb(0, 0, 0) 0%, rgba(0, 0, 0, 0) 76%)
                cssStyle.webkitMaskImage = "linear-gradient(rgb(0, 0, 0) 0%, rgba(0, 0, 0, 0) " + this.extras.fade.toString() + "%)";
                
                // linear-gradient(rgb(0, 0, 0) 0%, rgba(0, 0, 0, 0) 76%)

            }

            if (this.extras.rotation > 0) {
                let angle: number = ((this.extras.rotation) / 100) * 360;
                angle = Math.floor(angle);

                transformString += " rotate(" + angle.toString() + "deg)";
            }
            if (this.extras.skewY !== 0) {
                let skewAngleY: number = ((this.extras.skewY) / 100) * 360;
                skewAngleY = Math.floor(skewAngleY);

                transformString += " skewY(" + skewAngleY.toString() + "deg)";
            }
            if (this.extras.skewX !== 0) {
                let skewAngleX: number = ((this.extras.skewX) / 100) * 360;
                skewAngleX = Math.floor(skewAngleX);
                transformString += " skewX(" + skewAngleX.toString() + "deg)";
            }
            if (this.extras.flipV === true) {
                transformString += " scaleY(-1)";
            }
            if (this.extras.flipH === true) {
                transformString += " scaleX(-1)";
            }

            if (this.extras.borderThick > 0 && this.type !== PageElementVo.TYPE_LINE) {
                cssStyle.border = "solid";
                cssStyle.borderWidth = this.extras.borderThick.toString();
                cssStyle.borderColor = this.extras.borderColor;
            }

            cssStyle.transform = transformString;

            
        }

        cssStyle.transitionProperty = "top, left, height, width";
        cssStyle.transitionDuration = "2s";
        return cssStyle;
    }
    public getHtml(): HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement | HTMLImageElement {
        const zm: ZoomyModel = ZoomyModel.getInstance();

        switch (this.type) {

            case PageElementVo.TYPE_IMAGE:

                const img: HTMLImageElement = document.createElement("img");
                // img.id = this.id;
                img.src = zm.getFullPath(this.content[0]);
                img.setAttribute("style", this.getCss().cssText);
                img.setAttribute("class", this.getClasses().join(" "));

                return img;

            case PageElementVo.TYPE_CARD:

                let card: CardElement = new CardElement(this, this.getClasses());

            //    console.log("subType: " + this.subType);
                
                switch (this.subType) {
                    case PageElementVo.SUB_TYPE_GREEN_CARD:
                        return card.getCard1(this.id);
                    case PageElementVo.SUB_TYPE_WHITE_CARD:
                        return card.getCard2(this.id);
                }

            case PageElementVo.TYPE_TEXT:

                
                if(this.extras.textCurve.label !== 'None'){
                   // console.log("!!!!!!!!!!!!!!"+text.toString());
                    let specText: TextCurveElement = new TextCurveElement(this, this.getClasses());
                    return specText.getTextCurve();
                } else {

                    const text: HTMLElement = document.createElement("article");
                    text.id = this.id;
                    text.innerText = this.content[0];
                   
                    text.setAttribute("class", this.getClasses().join(" "));
                    text.setAttribute("style", this.getTextCSS().cssText);



                    return text;
                }

                

            case PageElementVo.TYPE_LINE:
                let le: LineElement = new LineElement(this, this.getClasses());
                return le.getLine();

            //shape
            case PageElementVo.TYPE_SHAPE:

                let shape: ShapeElement = new ShapeElement(this, this.getClasses());
                return shape.getShape();
        }

        let notFound: HTMLElement = document.createElement("div");
        notFound.innerHTML = "not found";
        return notFound;
    }

}