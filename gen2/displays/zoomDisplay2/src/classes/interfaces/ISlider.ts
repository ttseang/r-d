export interface ISlider
{
    setPercent: (percent:number) => void;
    setCallback: (callback:Function) => void;
}