import { ZoomyController } from "../core/ZoomyController";
import { ZoomyModel } from "../core/ZoomyModel";

export class TrackSlider2
{
    private element:HTMLElement;
    private downListener: EventListener = () => { };
    private moveListener: EventListener = () => { };
    private upListener: EventListener = () => { };
    private leaveListener: EventListener = () => { };
    private knob:HTMLElement;
    private track:HTMLElement;
    private value:number = 0;
    private stepNumber:number = 0;
    
    private zoomyController:ZoomyController = ZoomyController.getInstance();
    private zoomyModel:ZoomyModel = ZoomyModel.getInstance();

    constructor(element:HTMLElement)
    {
        this.element = element;
        this.element = element;
        this.knob = element.querySelector(".knob") as HTMLElement;
        this.track = element.querySelector(".track") as HTMLElement;

        this.zoomyController.setSliderPosition = this.setStep.bind(this);
        
        this.setListeners();
    }
    //use touch events
    setListeners()
    {
        this.downListener = this.startDrag.bind(this) as EventListener;
        this.moveListener = this.moveKnob.bind(this) as EventListener;
        this.upListener = this.stopDrag.bind(this) as EventListener;
        this.leaveListener = this.stopDrag.bind(this) as EventListener;
        this.knob.addEventListener("touchstart", this.downListener);
    }
    startDrag(e:TouchEvent)
    {
        this.knob.addEventListener("touchmove", this.moveListener);
        window.addEventListener("touchend", this.upListener);
        window.addEventListener("touchcancel", this.leaveListener);
    }
    moveKnob(e:TouchEvent)
    {
        let knobX = e.touches[0].clientX - this.track.getBoundingClientRect().left;
        if (knobX < 0) {
            knobX = 0;
        }
        if (knobX > this.track.getBoundingClientRect().width) {
            knobX = this.track.getBoundingClientRect().width;
        }
        this.knob.style.left = knobX + "px";
        this.value = knobX / this.track.getBoundingClientRect().width;

        //convert knobX to percentage
        this.value = knobX / this.track.getBoundingClientRect().width;

        const numberOfSteps = this.zoomyModel.stepCount;
        const stepSize: number = 100 / numberOfSteps;
        const percentage = this.value * 100;

        //get the step number based on the percentage and the step size
        this.stepNumber = Math.floor(percentage / stepSize);

        this.showPreviewThumb(this.stepNumber);

    }
    stopDrag(e:TouchEvent)
    {
        window.removeEventListener("touchmove", this.moveListener);
        window.removeEventListener("touchend", this.upListener);
        this.zoomyController.gotoPage(this.stepNumber);
    }
    setStep(stepNumber:number)
    {
        this.stepNumber = stepNumber;
    }
    showPreviewThumb(index: number) {
        // console.log("showPreviewThumb: "+index);
        const thumbPreview: HTMLElement | null = document.querySelector('.thumbPreviews');
        if (thumbPreview) {
            const thumbs: NodeListOf<HTMLElement> = thumbPreview.querySelectorAll('li');
            thumbs.forEach((thumb: HTMLElement) => {
                thumb.classList.add('invis');
            });
            thumbs[index].classList.remove('invis');
        }
    }
    hidePreviewThumb(index: number) {
        const thumbPreview: HTMLElement | null = document.querySelector('.thumbPreviews');
        if (thumbPreview) {
            const thumbs: NodeListOf<HTMLElement> = thumbPreview.querySelectorAll('li');
            thumbs[index].classList.add('invis');
        }
    }
}