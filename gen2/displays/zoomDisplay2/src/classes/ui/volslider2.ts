import { ISlider } from "../interfaces/ISlider";

export class Volslider2 implements ISlider
{
    private element:HTMLElement;
    private knob:HTMLElement;
    private track:HTMLElement;
    private value:number=0;
    private callback:Function=()=>{};
    

    private upListener:EventListener = this.stopDrag.bind(this) as EventListener;
    private moveListener:EventListener = this.moveKnob.bind(this) as EventListener;
    private downListener:EventListener = this.startDrag.bind(this) as EventListener;

    constructor(element:HTMLElement)
    {
        this.element = element;
        this.knob = element.querySelector(".knob") as HTMLElement;
        this.track = element.querySelector(".track") as HTMLElement;
        this.knob.addEventListener("touchstart", this.downListener);
    }
    

    //make a volume slider for mobile using touch events
    private startDrag(e:TouchEvent)
    {
        console.log("start drag");
        window.addEventListener("touchmove", this.moveListener)
        window.addEventListener("touchend", this.upListener);
    }
    //move knob verticle when dragging keeping it within the track
    private moveKnob(e:TouchEvent)
    {
        console.log("move knob");
        let knobY = e.touches[0].clientY - this.track.getBoundingClientRect().top;
        if (knobY < 0) {
            knobY = 0;
        }
        if (knobY > this.track.getBoundingClientRect().height) {
            knobY = this.track.getBoundingClientRect().height;
        }
        this.knob.style.top = knobY + "px";

        this.value =Math.abs(1-( knobY / this.track.getBoundingClientRect().height));
        //convert the value to 3 decimal places
        this.value = Math.round(this.value * 1000) / 1000;
        
        console.log(this.value);
       // this.callback(this.value);
    }
    private stopDrag(e:TouchEvent)
    {
        console.log("stop drag");
        window.removeEventListener("touchmove", this.moveListener)
        window.removeEventListener("touchend", this.upListener);
    }
    public setPercent(percent:number)
    {
        this.value = percent;
    }
    public setCallback(callback:Function)
    {
        this.callback = callback;
    }
}