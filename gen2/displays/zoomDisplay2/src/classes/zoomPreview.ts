import { ZoomyModel } from "./core/ZoomyModel";
import { DisplayElementVo } from "./dataObjects/DisplayElementVo";
import { PageElementVo } from "./dataObjects/PageElementVo";
import { StepVo } from "./dataObjects/StepVo";
import { ZoomVo } from "./dataObjects/ZoomVo";
import { LineElement } from "./elements/LineElement";
import { ShapeElement } from "./elements/ShapeElement";

export class ZoomPreview {
    private zm: ZoomyModel = ZoomyModel.getInstance();
    private element: HTMLElement | null = null;
    private index: number;
    private addArea: SVGForeignObjectElement | null = null;
    private svg: SVGElement | null = null;
    constructor(index: number) {
        this.index = index;
        const thumbPreviews: HTMLElement | null = document.querySelector('.thumbPreviews');

        //the nth child is the index + 1
        const child: number = index + 1;
        if (thumbPreviews) {
            this.element = thumbPreviews.childNodes[child] as HTMLElement;

            if (this.element) {
                //add svg element to this.element
                const svg: SVGElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttribute("width", "100%");
                svg.setAttribute("height", "100%");
                //  svg.setAttribute("viewBox", "0 0 100 100");
                svg.setAttribute("preserveAspectRatio", "xMidYMid meet");
                this.element.appendChild(svg);

                this.svg = svg;

                //add the foreign object to the svg
                const foreignObject: SVGForeignObjectElement = document.createElementNS("http://www.w3.org/2000/svg", "foreignObject");
                foreignObject.setAttribute("width", "100%");
                foreignObject.setAttribute("height", "100%");
                foreignObject.setAttribute("x", "0");
                foreignObject.setAttribute("y", "0");
                svg.appendChild(foreignObject);

                this.addArea = foreignObject;
            }
            else {
                console.log("element not found");
            }
        }
        // const tag: string = ".zoomPreview"+index.toString();
        // this.element=document.querySelector(tag) as HTMLElement;
        this.buildPreview();
    }
    public buildPreview() {
        if (this.zm.zoomPres == null) {
         //   console.log("zoomPres is null");
            return;
        }

        const step = this.zm.zoomPres.steps[this.index];
     //   console.log(step);

        //get the step elements
        let elements: any = step.elements;

        // console.log(elements);


        //loop through the elements
        for (let i: number = 0; i < elements.length; i++) {
            //get the element
            let element: PageElementVo = elements[i];
        //    console.log(element);


            //check to see if the element already exists
            // if (!this.currentElements.has(element.eid)) {

            //get the type
            let type: string = element.type;

            let html: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement = element.getHtml();

            if (element.type !== PageElementVo.TYPE_LINE && element.type !== PageElementVo.TYPE_SHAPE) {
                let css: CSSStyleDeclaration = element.getCss();
                html.setAttribute("style", css.cssText);
            }
            /**
            *add the data object to the current element map
            */
            // this.currentElements.set(element.eid, new DisplayElementVo(element.eid, html));

            if (this.addArea) {
                this.addArea.appendChild(html);
            }
        }
        this.setZoom();
    }
    

    setZoom() {

        let pageW = this.zm.pageW;
        let pageH = this.zm.pageH;

        let step: StepVo = this.zm.zoomPres!.steps[this.index];

        let zoomVo: ZoomVo = step.zoom;
        ////console.log(zoomVo);

        //turn percentages into pixels
        let xx: number = (zoomVo.x1 / 100) * pageW;
        let yy: number = (zoomVo.y1 / 100) * pageH;

        //measure the distance between the points
        let distX: number = (zoomVo.x2 - zoomVo.x1) / 100;
        let distY: number = (zoomVo.y2 - zoomVo.y1) / 100;

        //cut off the extra decimal places past 2 points
        distX = Math.round(distX * 100) / 100;
        distY = Math.round(distY * 100) / 100;

        let zoomX: number = (pageW * distX);
        let zoomY: number = (pageH * distY);

        let posString = xx.toString() + " " + yy.toString();
        posString += " " + zoomX + " " + zoomY;

        if (this.svg) {
            this.svg.setAttribute("viewBox", posString);
        }
       
    }
}