import { ZoomPresentationVo } from "../dataObjects/ZoomPresentationVo";

//make a singleton
export class ZoomyModel {

    public pageH: number = 0;
    public pageW: number = 0;

    private static instance: ZoomyModel;
    public zoomPres: ZoomPresentationVo | null = null;
    public mode:number = 0;

    public backgroundAudioVolume:number = 0.25;
    public backgroundAudioMuted:boolean = false;
    
    public audioVolume:number =.8;
    public audioSpeed:number = 1;
    public currentStep:number = 0;

    public isMobile:boolean = false;
    public isPlaying:boolean = false;
    public highlightingOn:boolean = true;
    
    private constructor() {
        //private constructor
        (window as any).zoomyModel = this;
    }
    public static getInstance(): ZoomyModel {
        if (!ZoomyModel.instance) {
            ZoomyModel.instance = new ZoomyModel();
        }
        return ZoomyModel.instance;
    }
    get stepCount(): number {
        if (this.zoomPres == null) return 0;
        return this.zoomPres.steps.length;
    }
    getFullPath(path: string) {
        if(path.substring(0,5) !== "https"){
            return "https://ttv5.s3.amazonaws.com/william/images/bookimages/" + path;
        } else{
            return path;
        }
    }

    getAudioPath(path: string) {
        return "./assets/audio/" + path;
      //  return "https://ttv5.s3.amazonaws.com/" + path;
    }
    
    getBackgroundAudioPath(path: string) {
        return "./assets/backgroundMusic/" + path;
        //return "https://ttv5.s3.amazonaws.com/" + path;
    }
   
    
}