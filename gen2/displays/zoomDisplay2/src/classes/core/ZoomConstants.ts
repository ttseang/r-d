export class ZoomConstants
{
    public static readonly ZOOM_IN = "zoomIn";
    public static readonly ZOOM_OUT = "zoomOut";
    public static readonly ZOOM_RESET = "zoomReset";

    
    //mode enum
    public static readonly MODES = {
        READ: 0,
        AUTO: 1,
        LISTEN: 2
    };

    public static readonly SPEEDS = {
        SLOW: 0.5,
        NORMAL: 1,
        FAST: 1.5
    };
    
}