import { ZoomyController } from "./ZoomyController";

export class UiController {
    private static instance: UiController;
    private zoomyController: ZoomyController = ZoomyController.getInstance();
    constructor() {
       (window as any).uiController = this;
       this.zoomyController.sendAlert = this.sendAlert;
    }

    public static getInstance(): UiController {
        if (!this.instance) {
            this.instance = new UiController();
        }
        return this.instance;
    }
    public closeBars() {
        
        const topBar: HTMLElement | null = document.querySelector('.fullWidthBar.top');
        const bottomBar: HTMLElement | null = document.querySelector('.fullWidthBar.bottom');
        if (topBar && bottomBar) {
            topBar.classList.add('closed');
            bottomBar.classList.add('closed');
        }
        this.hideGadgets();
        const barTab: HTMLElement | null = document.querySelector('.fullWidthBar .barTab');
        if (barTab) {
            barTab.classList.add('closed');
        }
    }
    public openBars() {
        const topBar: HTMLElement | null = document.querySelector('.fullWidthBar.top');
        const bottomBar: HTMLElement | null = document.querySelector('.fullWidthBar.bottom');
        if (topBar && bottomBar) {
            topBar.classList.remove('closed');
            bottomBar.classList.remove('closed');
        }

        const barTab: HTMLElement | null = document.querySelector('.fullWidthBar .barTab');
        if (barTab) {
            barTab.classList.remove('closed');
        }
    }
    public hideGadgets() {
        
        const volSlider: HTMLElement | null = document.querySelector('.volSlider');
        const speedMenu: HTMLElement | null = document.querySelector('.speedMenu');
        const voiceSlider: HTMLElement | null = document.querySelector('.voiceSlider');
        if (volSlider && speedMenu && voiceSlider) {
            volSlider.classList.add('hid');
            speedMenu.classList.add('hid');
            voiceSlider.classList.add('hid');
        }
        const gadgets: HTMLElement | null = document.querySelector('.gadgetGrid');
        if (gadgets) {
            gadgets.classList.add('invis');
        }
    }
    public showGadgets() {
        const volSlider: HTMLElement | null = document.querySelector('.volSlider');
        const speedMenu: HTMLElement | null = document.querySelector('.speedMenu');
        const voiceSlider: HTMLElement | null = document.querySelector('.voiceSlider');

        //if all are hidden, then return
        if (volSlider && speedMenu && voiceSlider) {
            if (volSlider.classList.contains('hid') && speedMenu.classList.contains('hid') && voiceSlider.classList.contains('hid')) {
                console.log('all hidden');
                this.hideGadgets();
                return;
            }
        }

        const gadgets: HTMLElement | null = document.querySelector('.gadgetGrid');
        if (gadgets) {
            gadgets.classList.remove('invis');
        }
    }
    public toggleVolume() {
        // Toggle volume
        const volSlider: HTMLElement | null = document.querySelector('.volSlider');
        if (volSlider) {
            volSlider.classList.toggle('hid');
        }
        this.showGadgets();
        this.zoomyController.setCorrectVolumeValues();
        
    }
    public toggleSpeed() {
        // Toggle speed
        const speedMenu: HTMLElement | null = document.querySelector('.speedMenu');
        if (speedMenu) {
            speedMenu.classList.toggle('hid');
        }
        this.showGadgets();
    }
    public toggleSpeech() {
        // Toggle speech
        const voiceSlider: HTMLElement | null = document.querySelector('.voiceSlider');
        if (voiceSlider) {
            voiceSlider.classList.toggle('hid');
        }
        this.showGadgets();
        this.zoomyController.setCorrectVolumeValues();
    }
    public sendAlert(message: string) {
        const alertBox: HTMLElement | null = document.querySelector('.alertBox');
        if (alertBox) {
            const para: HTMLElement | null = alertBox.querySelector('p');
            if (para)
            {
                para.innerHTML = message;
            }
           
            alertBox.classList.remove('hid');
            setTimeout(() => {
                alertBox.classList.add('hid');
            }, 2000);
        }
    }
}