import { ButtonControls } from "./core/ButtonControls";
import { ZoomConstants } from "./core/ZoomConstants";
import { ZoomyController } from "./core/ZoomyController";
import { ZoomyModel } from "./core/ZoomyModel";
import { ZoomLoader } from "./dataLoaders/ZoomLoader";
import { DisplayElementVo } from "./dataObjects/DisplayElementVo";
import { PageElementVo } from "./dataObjects/PageElementVo";
import { StepVo } from "./dataObjects/StepVo";
import { ZoomPresentationVo } from "./dataObjects/ZoomPresentationVo";
import { ZoomVo } from "./dataObjects/ZoomVo";
import { LineElement } from "./elements/LineElement";
import { ShapeElement } from "./elements/ShapeElement";
import { AudioPlayer } from "./util/AudioPlayer";

export class ZoomyDisplay {
    private zm: ZoomyModel = ZoomyModel.getInstance();
    private body: HTMLElement;
    private pageIndex: number = 0;
    private addArea: HTMLElement | null = null;

    private audioPlayer: AudioPlayer = AudioPlayer.getInstance();

    //a map of all the elements currently on the page
    private currentElements: Map<string, DisplayElementVo> = new Map<string, DisplayElementVo>();
    //record the old zoom coodinates to use in the zoom animation
    private oldZoom: string = "";


    private zoomyController: ZoomyController = ZoomyController.getInstance();
    private onLoad: Function;
    private svgDisplayListener: EventListener = this.setSVGDisplay.bind(this);

    constructor(onLoad: Function = () => { }) {
        this.body = document.body;
        this.onLoad = onLoad;

        //detect if mobile using user agent
        const ua = navigator.userAgent;
        if (ua.indexOf("Android") > -1 || ua.indexOf("iPhone") > -1 || ua.indexOf("iPad") > -1 || ua.indexOf("Mobile") > -1) {
            this.zm.isMobile = true;
            console.log("mobile");
        }

        //get the zoomy add area
        this.addArea = document.querySelector(".zoomAdd");

        this.setSVGDisplay();

        this.zoomyController.addResizeCallback(this.svgDisplayListener);
        //add the event listener to the window to get the size of the window
        window.onresize = () => {
            this.zoomyController.resize();
        };
        (window as any).zoomyDisplay = this;

        this.zoomyController.nextPage = this.nextPage.bind(this);
        this.zoomyController.prevPage = this.previousPage.bind(this);
        this.zoomyController.gotoPage = this.gotoPage.bind(this);
        this.zoomyController.initalModeSet = this.setAudio.bind(this);
        this.zoomyController.audioComplete = this.audioComplete.bind(this);
        this.zoomyController.onModeChange = this.onModeChanged.bind(this);
        this.zoomyController.goFullScreen = this.goFullScreeen.bind(this);

    }
    private goFullScreeen(): void {
        const elem: HTMLElement | null = document.documentElement;
        if (elem) {
            if (elem.requestFullscreen) {
                elem.requestFullscreen().then
                    (() => {
                        console.log("fullscreen");
                        this.zoomyController.sendAlert("Fullscreen");
                    }).catch((err) => {
                        console.warn("fullscreen not supported");
                        this.zoomyController.sendAlert("Fullscreen not supported");
                    });
                console.log("fullscreen");
            } else {
                console.warn("fullscreen not supported");
                this.zoomyController.sendAlert("Fullscreen not supported");
            }
        }
    }

    onModeChanged(mode: number) {
        console.log("mode changed", mode);
        if (mode === ZoomConstants.MODES.READ) {
            this.audioPlayer.stopAudio();
            return;
        }
        this.setAudio();
    }
    private setSVGDisplay(): void {
        //get the body height and width
        this.zm.pageH = this.body.clientHeight;
        this.zm.pageW = this.body.clientWidth;

        //set the svg height and width

        if (this.addArea) {
            this.addArea.style.height = this.zm.pageH + "px";
            this.addArea.style.width = this.zm.pageW + "px";
        }
        this.buildPage();
    }
    //load data
    public loadData(fileName: string): void {
        let loader: ZoomLoader = new ZoomLoader(this.makeDisplay.bind(this));
        loader.loadFile(fileName);
    }
    public loadPreview(fileData: string): void {
        let loader: ZoomLoader = new ZoomLoader(this.makeDisplay.bind(this));
        loader.loadFromString(fileData);
    }
    //make the display
    makeDisplay(zoomPres: ZoomPresentationVo): void {
        this.zm.zoomPres = zoomPres;
        // console.log(zoomPres);
        this.buildPage();
        this.onLoad();
    }
    deleteUnneeded() {
        const addArea: HTMLElement | null = document.querySelector(".zoomAdd");
        this.currentElements.forEach((el: DisplayElementVo) => {
            if (el.delFlag === true) {
                if (el.displayElement && addArea) {
                    addArea.removeChild(el.displayElement);
                    this.currentElements.delete(el.eid);
                }
            }
        });
    }
    removeAllElements() {
        const addArea: HTMLElement | null = document.querySelector(".zoomAdd");
        this.currentElements.forEach((el: DisplayElementVo) => {
            if (el.displayElement && addArea) {
                addArea.removeChild(el.displayElement);
                this.currentElements.delete(el.eid);
            }
        });
    }

    setAudio() {
        if (this.zm.mode === ZoomConstants.MODES.READ) {
            return;
        }
        let step: StepVo = this.zm.zoomPres!.steps[this.pageIndex];
        if (step.audio) {
            this.audioPlayer.playAudio(step.audio);
        }
        if (step.backgroundAudio) {
            this.audioPlayer.playBackgroundAudio(step.backgroundAudio);
        }
    }
    audioComplete() {
        if (this.zm.mode !== ZoomConstants.MODES.AUTO) {
            return;
        }
        this.nextPage();
        // this.zoomyController.setSliderPosition(this.pageIndex);
    }
    setZoom(hard: boolean = false) {

        let pageW = this.zm.pageW;
        let pageH = this.zm.pageH;

        let step: StepVo = this.zm.zoomPres!.steps[this.pageIndex];

        let zoomVo: ZoomVo = step.zoom;
        ////console.log(zoomVo);

        //turn percentages into pixels
        let xx: number = (zoomVo.x1 / 100) * pageW;
        let yy: number = (zoomVo.y1 / 100) * pageH;

        //measure the distance between the points
        let distX: number = (zoomVo.x2 - zoomVo.x1) / 100;
        let distY: number = (zoomVo.y2 - zoomVo.y1) / 100;

        //cut off the extra decimal places past 2 points
        distX = Math.round(distX * 100) / 100;
        distY = Math.round(distY * 100) / 100;

        let zoomX: number = (pageW * distX);
        let zoomY: number = (pageH * distY);

        let posString = xx.toString() + " " + yy.toString();
        posString += " " + zoomX + " " + zoomY;

        //  this.zm.posString = posString;

        //  console.log(posString);

        if (this.addArea) {
            //
            if (this.oldZoom === "") {
                this.addArea.setAttribute("viewBox", posString);
            }
            else {
                let zoomAnimation: SVGAnimateElement = (document.getElementById("zoom1") as unknown) as SVGAnimateElement;
                if (hard === false) {
                    zoomAnimation.setAttribute("values", this.oldZoom + ";" + posString);
                }
                else {
                    zoomAnimation.setAttribute("values", posString + ";" + posString);
                }

                zoomAnimation.beginElement();
            }

        }
        //record the position for animation
        this.oldZoom = posString;
    }
    //next page
    nextPage(): void {
        if (this.pageIndex === this.zm.zoomPres!.steps.length - 1) return;

        this.pageIndex++;
        this.zm.currentStep = this.pageIndex;
        // this.zoomyController.setSliderPosition(this.pageIndex);

        this.checkNavButtons();
        this.buildPage();
    }
    //previous page
    previousPage(): void {
        if (this.pageIndex === 0) return;
        this.pageIndex--;
        this.zm.currentStep = this.pageIndex;
        // this.zoomyController.setSliderPosition(this.pageIndex);

        this.checkNavButtons();
        this.buildPage();
    }
    private checkNavButtons(): void {
        const prevButton: HTMLElement | null = document.querySelector(".prev");
        const nextButton: HTMLElement | null = document.querySelector(".next");
        if (prevButton) {
            if (this.pageIndex === 0) {
                prevButton.classList.add("hid");
            }
            else {
                prevButton.classList.remove("hid");
            }
        }
        if (nextButton) {
            if (this.pageIndex === this.zm.zoomPres!.steps.length - 1) {
                nextButton.classList.add("hid");
            }
            else {
                nextButton.classList.remove("hid");
            }
        }
    }
    gotoPage(page: number): void {
        this.removeAllElements();
        this.pageIndex = page;
        this.checkNavButtons();
        this.buildPage();
    }
    //build the page
    buildPage(): void {
        if (this.zm.zoomPres == null) return;

        //set all the elements to be deleted
        this.currentElements.forEach((el: DisplayElementVo) => {
            el.delFlag = true;
        });

        //get the current step
        let step: StepVo = this.zm.zoomPres.steps[this.pageIndex];

        //get the step elements
        let elements: any = step.elements;

        if(this.zm.zoomPres.bgColor !== "#cccccc"){
            this.body.style.backgroundColor = this.zm.zoomPres.bgColor;
            this.body.style.backgroundImage = "none";
        } else {
            this.body.style.backgroundImage = "../assets/images/bg2.jpg";
        }


        //loop through the elements
        for (let i: number = 0; i < elements.length; i++) {
            //get the element
            let element: PageElementVo = elements[i];


            //check to see if the element already exists
            if (!this.currentElements.has(element.eid)) {

                //get the type
                let type: string = element.type;

                let html: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement = element.getHtml();

                if (element.type !== PageElementVo.TYPE_LINE && element.type !== PageElementVo.TYPE_SHAPE && element.subType !== PageElementVo.SUB_TYPE_SPECIAL) {
                    let css: CSSStyleDeclaration = element.getCss();
                    html.setAttribute("style", css.cssText);
                } 
                /**
                *add the data object to the current element map
                */
                this.currentElements.set(element.eid, new DisplayElementVo(element.eid, html));

                if (this.addArea) {
                    this.addArea.appendChild(html);
                }
            }
            else {
                //update the element
                let displayElementVo: DisplayElementVo | undefined = this.currentElements.get(element.eid);
                //don't delete the element
                if (displayElementVo) {
                    displayElementVo.delFlag = false;


                    switch (element.type) {
                        case PageElementVo.TYPE_LINE:

                            LineElement.updateLine(displayElementVo.displayElement as SVGAElement, element);

                            break;
                        case PageElementVo.TYPE_SHAPE:
                            ShapeElement.updateShape(displayElementVo.displayElement as SVGSVGElement, element);
                            break;

                        default:

                            //  console.log("update element", displayElementVo);

                            let css: CSSStyleDeclaration = element.getCss();
                            //  console.log(css.cssText);

                            displayElementVo.displayElement.setAttribute("style", css.cssText);
                            break;
                    }
                }
            }
        }
        this.deleteUnneeded();
        this.setZoom();
        this.setAudio();
        this.zoomyController.setSliderPosition(this.pageIndex);
    }
}