export class ApiConnect
{
  

    public getFileContent(lti:string,callback:Function)
    {
        
        let url: string = "https://tthq.me/api/pr/getequation/"+lti;
        fetch(url, {
            method: "post"
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        //console.log(json);                        
                        callback(json);
                    });
                }
            })
    }
   
}
export default ApiConnect;
