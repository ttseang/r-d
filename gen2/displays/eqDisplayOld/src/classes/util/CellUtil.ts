import { CellVo } from "../dataObjs/CellVo";

export class CellUtil
{
    public static dataToCell(data:any)
    {
        return new CellVo(data._text,data.classArray);
    }

    public static dataToCells(data:any[])
    {
        let cells:CellVo[]=[];

        for (let i:number=0;i<data.length;i++)
        {
            cells.push(CellUtil.dataToCell(data[i]));
        }
        return cells;
    }
    public static gridToCells(data:any[][])
    {
        let cells:CellVo[][]=[];

        for (let i:number=0;i<data.length;i++)
        {
            cells[i]=[];
            for (let j:number=0;j<data[i].length;j++)
            {
                //console.log(data[i][j]);

                cells[i].push(CellUtil.dataToCell(data[i][j]));
            }            
        }
        return cells;
    }
}