export enum ProblemTypes
{
    UNSUPORTED,
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION,
    FRACTIONS
}