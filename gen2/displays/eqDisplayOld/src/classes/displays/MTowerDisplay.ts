import { CellVo } from "../dataObjs/CellVo";
import { MTowerDisplayVo } from "../dataObjs/MTowerDisplayVo";

export class MTowerDisplay {
    private data: MTowerDisplayVo;

    constructor(data: MTowerDisplayVo) {
        this.data = data;
    }
    makeCols(index: number, cells: CellVo[]) {
        let cols: string[] = [];

        for (let j: number = 0; j < cells.length; j++) {
            let html: string = cells[j].toHtml();
            cols.push(html);
        }
        return "<span>" + cols.join("\n") + "</span>";
    }
    getTop() {
        let topRows: string[] = [];
        if (this.data.top) {

            for (let i: number = 0; i < this.data.top.length; i++) {
                let key: string = "topRow" + i.toString();
                let key2: string = "ph" + i.toString();
                topRows.unshift("<div></div>");
                topRows.unshift("<div>" + this.makeCols(i, this.data.top[i]) + "</div>")

            }

        }
        return topRows.join("\n");
    }
    getRows() {
        let opString: string = "\u00D7";

        let rows: string[] = [];

        if (this.data.rows) {
            for (let i: number = 0; i < this.data.rows.length; i++) {

                if (i === this.data.rows.length - 1) {
                    rows.push("<div>" + opString + "</div>");
                }
                else {
                    rows.push("<div></div>")
                }

                rows.push("<div>" + this.makeCols(i, this.data.rows[i]) + "</div>");
            }

        }

        return rows.join("\n");
    }
    getProducts() {
        let productRows: string[] = [];

        if (this.data.products) {
            for (let i: number = 0; i < this.data.products.length; i++) {
                let key: string = "productRows" + i.toString();

                productRows.push("<div>" + this.makeCols(i, this.data.products[i]) + "</div>");
            }

        }

        return productRows.join("\n");
    }
    getCarryRow() {
        if (this.data) {
            if (this.data.carryRow) {
                return this.makeCols(30, this.data.carryRow)
            }
        }
        return "";
    }
    getAnswer() {

        if (this.data.answer) {


            if (this.data.answer.length === 0) {
                return "";
            }
            if (this.data.products) {
                if (this.data.products.length === 0) {
                    return "<div id='answerGrid4'><div></div>" + this.makeCols(20, this.data.answer) + "</div>";
                }
                return "<div id='answerGrid3'><div></div>" + this.makeCols(20, this.data.answer) + "</div>";
            }
        }

        return "";
    }
    getHtml(id: string) {
        let html: string = "";

        html += "<div id='" + id + "' class='multiplyEquation'>";
        html += "<div id='carryString2'>";
        html += "<div></div>";
        html += this.getTop();
        html += "</div>";
        html += "<div id='mathGrid'>";
        html += this.getRows();
        html += "</div>";
        html += "<div id='carryString2'>";
        html += "<div></div>";
        html += this.getCarryRow();
        html += "</div>";
        html += "<div id='productRows'>";
        html += this.getProducts();
        html += "</div>";
        html += this.getAnswer();
        html += "</div>";

        return html;
    }
}