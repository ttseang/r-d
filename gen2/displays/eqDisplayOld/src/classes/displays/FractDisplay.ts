import { FractCellVo } from "../dataObjs/FractCellVo";
import { FractDisplayVo } from "../dataObjs/FractDisplayVo";

export class FractDisplay
{
    private data:FractDisplayVo;

    constructor(data:FractDisplayVo)
    {
        this.data=data;
        (window as any).fd=this;
    }
    getLen2()
    {
       let len:number=0;

        for (let i: number = 0; i < this.data.fracts.length; i++) {
            let fract: FractCellVo = this.data.fracts[i];
            //console.log("l="+fract.getLen())
            len+=fract.getLen();
        }
        len+=this.data.fracts.length-1;
        return len;
    }
   
    getStyle() {
        
        let len: number = this.getLen2();
        //console.log("len="+len);

        let cols:string='50px '.repeat(len);
        //console.log(cols);

        let style:string="display:grid;"
        style+="grid-template-columns:"+cols;

       
        return style;
       
    }
    getFracts() {
        
        let fArray: string[] = [];

        for (let i: number = 0; i < this.data.fracts.length; i++) {
            let fract: FractCellVo = this.data.fracts[i];
            if (fract.w > 0) {
                fArray.push('<div>'+fract.getWhole()+'</div>');
            }

            fArray.push('<div>'+fract.getTop()+'</div>');

            if (this.data.ops[i]) {
                fArray.push('<div>'+this.data.opCells[i].toHtml()+'</div>');
            }
            
        }
        
        for (let j: number = 0; j < this.data.fracts.length; j++) {
            let fract: FractCellVo = this.data.fracts[j];
            if (fract.w > 0) {
                fArray.push('<div></div>');
            }
            fArray.push('<div>'+fract.getBottom()+'</div>');
            fArray.push('<div></div>');
        }
        return fArray.join("");
    }
    getHtml(id:string)
    {
        //return "display here";
       let  html:string= '<div className=\'fequation\'';
       let style:string=this.getStyle();
       //console.log(style);

       html+='style="'+style+'">'+this.getFracts()+'<div>';

       return html;

       
    }
}