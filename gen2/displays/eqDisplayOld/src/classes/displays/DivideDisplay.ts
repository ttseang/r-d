import { CellVo } from "../dataObjs/CellVo";
import { DivideDisplayVo } from "../dataObjs/DivideDisplayVo";

export class DivideDisplay {
    private data: DivideDisplayVo;

    constructor(data: DivideDisplayVo) {
        this.data = data;
    }
   private makeCols(index: number, cells: CellVo[]) {
        let cols: string[] = [];

        for (let j: number = 0; j < cells.length; j++) {


            let html: string = cells[j].toHtml();
            cols.push(html);
        }
        return cols.join("\n");
    }
  private getProductRows() {
        let rows: string[] = [];
        for (let i: number = 0; i < this.data.products.length; i++) {          

           
            if (i/2!==Math.floor(i/2))
            {
                rows.push("<div></div>")
                rows.push("<div>"+this.makeCols(i, this.data.products[i])+"</div>");
            }
            else
            {
                rows.push("<div></div>")
                rows.push("<div><span class='bb'>"+this.makeCols(i, this.data.products[i])+"</span></div>");
            }
        }
        return rows.join("\n");
    }
    
    toHtml() {
        let html: string = "<div id='divDisplay'>";
        html += "<div id='solutionGrid'>";
        html += "<div></div>";
        html += "<div id='quotient'>" + this.makeCols(0, this.data.quotient) + "</div>";
        html += "<div id='divisor'>" + this.makeCols(1, this.data.divisor) + "</div>";
        html += "<div id='dividend'>" + this.makeCols(2, this.data.dividend) + "</div>";
        html += this.getProductRows();
        html += "</div>";
        html += "</div>";
        return html;
    }
}