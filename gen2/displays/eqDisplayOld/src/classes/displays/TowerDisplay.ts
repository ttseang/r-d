import { CellVo } from "../dataObjs/CellVo";
import { TowerDisplayVo } from "../dataObjs/TowerDisplayVo";

export class TowerDisplay
{
    private data:TowerDisplayVo;

    constructor(data:TowerDisplayVo)
    {
        this.data=data;
    }
    getTop()
    {
        let topRows: string[] = [];
        for (let i: number = 0; i < this.data.top.length; i++) {
           
            topRows.unshift("<div></div>");
            topRows.unshift("<div>"+this.makeCols(i, this.data.top[i])+"</div>");

        }
        return (topRows.join("\n"));
    }
    getRows()
    {
        let rows: string[] = [];
        for (let i: number = 0; i < this.data.rows.length; i++) {
            let rowKey: string = "mrow" + i.toString();
            let bKey: string = "blank" + i.toString();
            if (i === this.data.rows.length - 1) {
                rows.push("<div>"+this.data.operator+"</div>");
            }
            else {
                rows.push("<div></div>")
            }

            rows.push("<div>"+this.makeCols(i, this.data.rows[i])+"</div>");
        }
        return (rows.join("\n"));
    }
    makeCols(index: number, cells: CellVo[]) {
        let cols: string[] = [];
       
        for (let j: number = 0; j < cells.length; j++) {
            

            let html:string=cells[j].toHtml();
            cols.push(html);
        }
        return "<span>"+cols.join("\n")+"</span>";
    }
    getHtml(id:string)
    {
       let html:string="<div id='"+id+"' className='addEquation'>";
       html+="<div id='carryString3'>";
       html+="<div></div>";
       html+=this.getTop();
       html+="</div>";
       html+="<div id='mathGrid'>";
       html+=this.getRows();
       html+="</div>";
       html+="<div id='answerGrid2'>";
       html+="<div></div>";
       html+=this.makeCols(20, this.data.answer);
       html+="</div>";
       return html;
    }
}