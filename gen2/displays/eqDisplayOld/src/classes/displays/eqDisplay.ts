import { CacheManager } from "@teachingtextbooks/ttcachemanager";
import { ProblemTypes } from "../Constants";
import { DivideDisplayVo } from "../dataObjs/DivideDisplayVo";
import { FractCellVo } from "../dataObjs/FractCellVo";
import { FractDisplayVo } from "../dataObjs/FractDisplayVo";
import { MTowerDisplayVo } from "../dataObjs/MTowerDisplayVo";
import { TowerDisplayVo } from "../dataObjs/TowerDisplayVo";
import ApiConnect from "../util/ApiConnect";
import { CSSUtil } from "../util/CSSUtil";
import { StrUtil } from "../util/StrUtil";
import { DivideDisplay } from "./DivideDisplay";
import { FractDisplay } from "./FractDisplay";
import { MTowerDisplay } from "./MTowerDisplay";
import { TowerDisplay } from "./TowerDisplay";

export class EqDisplay {
    private expression: string = "";
    private data: any = {};
    private problemType: ProblemTypes = ProblemTypes.UNSUPORTED;
    private divID: string;
    private steps: string[] = [];
    public onReady: Function = () => { };
    public step: number = 0;
    private file:string="";
    private cm:CacheManager=CacheManager.getInstance();

    constructor(divID: string="") {
        CSSUtil.makeCSS();
        this.divID = divID;
        (window as any).eqDisplay = this;
    }
    loadFile(file: string, step: number = 0) {

        this.step = step;
        this.file=file;

       // //console.log("LOAD FILE"+file);

        const cachedData:string= this.cm.getFromCache(file,"");
        
        if (cachedData!=="")
        {
           // console.log("got from cache");
           // console.log(cachedData);
            this.data=JSON.parse(cachedData);
            this.formatData(this.data);
          
            return;
        }

       
        /*  fetch("./data/" + file)
             .then(response => response.json())
             .then(data => this.process(data)); */
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getFileContent(file, this.process.bind(this));
    }
    process(data: any) {
        //console.log(data);

        this.cm.saveToCache(this.file,JSON.stringify(data),60000);
       // this.cm.saveToLocal("eqDisplay");
        this.data = data;
        this.formatData(this.data);
    }
    formatData(data:any)
    {
        this.expression = data.eqstring;
        let stat: boolean = this.decideProblem();
        
        if (stat === false) {
            let div: HTMLElement | null = document.getElementById(this.divID);

            if (div) {
                div.style.width = "fit-content";

                div.innerHTML = "Invalid LTI Code";
            }
        }
    }

    decideProblem() {
       // console.log("decide problem");
       // console.log(this.expression);
        if (this.expression === undefined || this.expression === "") {
            return false;
        }

        let opArray: string[] = ["+", "-", "*", "/"];

        let problemTypes: ProblemTypes[] = [ProblemTypes.ADDITION, ProblemTypes.SUBTRACTION, ProblemTypes.MULTIPLICATION, ProblemTypes.DIVISION]

        this.problemType = ProblemTypes.UNSUPORTED;

        for (let i: number = 0; i < opArray.length; i++) {
            if (this.expression.includes(opArray[i])) {
                this.problemType = problemTypes[i];
            }
        }
        if (this.expression.includes("_")) {
            this.problemType = ProblemTypes.FRACTIONS;
        }

        //this.makeDisplay();
        this.makeSteps();
        this.showStep(this.step);
       // console.log("callback");
        this.onReady();
        return true;
    }
    showStep(step: number) {
        this.makeSteps();
        let div: HTMLElement | null = document.getElementById(this.divID);
       // console.log(div);
       // console.log(step);

        if (div) {
            div.style.width = "fit-content";
            let content: string = this.steps[step];
           // console.log(content);
            if (content === undefined) {
                content = "Step Not Found";
            }
            div.innerHTML = content;
        }
        else
        {
           // console.log(this.divID+" DIV NOT FOUND")
        }
    }
    getStep(step:number)
    {        
        let content: string = this.steps[step];
       /*  if (content === undefined) {
            content = "Step Not Found";
        } */
        return content;
    }
    makeSteps(id: string = "") {
        if (id === "") {
            id = StrUtil.getNextID();
        }
        let html: string = "";
        this.steps = [];

        switch (this.problemType) {

            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION:

                for (let i: number = 0; i < this.data.data.length; i++) {
                    let towerData: TowerDisplayVo = new TowerDisplayVo([], [], [], "");
                    towerData.fromObj(this.data.data[i]);

                    let td: TowerDisplay = new TowerDisplay(towerData);
                    html = td.getHtml(id);
                    html="<div class='tteq'>"+html+"</div>";
                    this.steps.push(html);
                }
                break;

            case ProblemTypes.MULTIPLICATION:
                for (let j: number = 0; j < this.data.data.length; j++) {
                    let mTowerData: MTowerDisplayVo = new MTowerDisplayVo([], [], [], [], [], "");
                    ////console.log(this.data.data[j]);
                    mTowerData.fromObj(this.data.data[j]);

                    let td: MTowerDisplay = new MTowerDisplay(mTowerData);
                    html = td.getHtml(id);
                    html="<div class='tteq'>"+html+"</div>";
                    this.steps.push(html);
                }
                break;

            case ProblemTypes.DIVISION:
                for (let k: number = 0; k < this.data.data.length; k++) {
                    let divData: DivideDisplayVo = new DivideDisplayVo([], [], [], [], []);
                    divData.fromObj(this.data.data[k]);

                    let dd: DivideDisplay = new DivideDisplay(divData);
                    html = dd.toHtml();
                    html="<div class='tteq'>"+html+"</div>";
                    this.steps.push(html);
                }

                break;

            case ProblemTypes.FRACTIONS:


                for (let k: number = 0; k < this.data.data.length; k++) {

                    let stepData: any = this.data.data[k];

                    let fractDisplayVo: FractDisplayVo = new FractDisplayVo([], []);
                    fractDisplayVo.fromObj(stepData);

                    let fractDisplay: FractDisplay = new FractDisplay(fractDisplayVo);
                    html = fractDisplay.getHtml(id);
                    html="<div class='tteq'>"+html+"</div>";
                    this.steps.push(html);
                }

        }

        //  return this.steps[step];
    }
}