import { CellUtil } from "../util/CellUtil";
import { CellVo } from "./CellVo";

export class DivideDisplayVo {
    public quotient: CellVo[];
    public divisor: CellVo[];
    public dividend: CellVo[];
    public products: CellVo[][];
    public remain: CellVo[];

    constructor(quotient: CellVo[], divisor: CellVo[], dividend: CellVo[], products: CellVo[][], remain: CellVo[]) {
        this.quotient = quotient;
        this.divisor = divisor;
        this.dividend = dividend;
        this.products = products;
        this.remain = remain;
    }
    fromObj(data:any)
    {
        let quotientData:any=data.quotient;
        let divisorData:any=data.divisor;
        let dividendData:any=data.dividend;
        let productData:any=data.products;
        let remainData:any=data.remain;

        this.quotient=CellUtil.dataToCells(quotientData);
        this.divisor=CellUtil.dataToCells(divisorData);
        this.dividend=CellUtil.dataToCells(dividendData);
        this.products=CellUtil.gridToCells(productData);
        this.remain=CellUtil.dataToCells(remainData);
        
    }
}