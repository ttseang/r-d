import { CellUtil } from "../util/CellUtil";
import { CellVo } from "./CellVo";

export class MTowerDisplayVo {
    public top: CellVo[][];
    public rows: CellVo[][];
    public operator: string;
    public products: CellVo[][];
    public carryRow: CellVo[];
    public answer: CellVo[];

    constructor(top: CellVo[][], rows: CellVo[][], products: CellVo[][], carryRow: CellVo[], answer: CellVo[], operator: string) {
        this.top = top;
        this.rows = rows;
        this.products = products;
        this.carryRow = carryRow;
        this.operator = operator;
        this.answer = answer;
    }
    public fromObj(data:any)
    {
        //console.log(data);
        
        let topData:any=data.top;
        let rowsData:any=data.rows;
        let answerData:any=data.answer;
        let productData:any=data.products;
        let carryData:any=data.carryRow;

        this.top=CellUtil.gridToCells(topData);
        this.rows=CellUtil.gridToCells(rowsData);
        this.answer=CellUtil.dataToCells(answerData);
        this.products=CellUtil.gridToCells(productData);

        this.operator=data.operator;


        //console.log(this);

    }
}