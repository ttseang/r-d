import { CellVo } from "./CellVo";

export class FractCellVo
{
    public top: CellVo[];
    public bottom:CellVo[];
    public whole:CellVo[];

    public t:number=0;
    public w:number=0;
    public b:number=0;

    constructor(top:CellVo[]=[],bottom:CellVo[]=[],whole:CellVo[]=[])
    {
        this.top=top;
        this.bottom=bottom;
        this.whole=whole;

        this.w=this.cellsToNumber(whole);
        this.b=this.cellsToNumber(bottom);
        this.t=this.cellsToNumber(top);
    }
    private cellsToNumber(cells:CellVo[])
    {
        let numString:string="";
        for (let i:number=0;i<cells.length;i++)
        {
            numString+=cells[i].text;
        }
        return parseInt(numString);
    }
    getLen():number
    {
        if (this.w===0)
        {
            return 1;
        }
        return 2;
    }
    getTop()
    {
        let html:string[]=[];
        for (let i:number=0;i<this.top.length;i++)
        {
          //  this.top[i].addClass("numerator");
            html.push(this.top[i].toHtml());
        }
        return html.join("");
    }
    getBottom()
    {
        let html:string[]=[];
        for (let i:number=0;i<this.bottom.length;i++)
        {
          //  this.bottom[i].addClass("denominator");
            html.push(this.bottom[i].toHtml());
        }
        return html.join("");
    }
    getWhole()
    {
        if (this.w===0)
        {
            return "";
        }
        let html:string[]=[];
        for (let i:number=0;i<this.whole.length;i++)
        {
            //this.whole[i].addClass("whole");
            html.push(this.whole[i].toHtml());
        }
        return html.join("");
    }
}