import { CellUtil } from "../util/CellUtil";
import { CellVo } from "./CellVo";

export class TowerDisplayVo {
    public top: CellVo[][];
    public rows: CellVo[][];
    public operator: string;
    public answer: CellVo[];

    constructor(top: CellVo[][], rows: CellVo[][], answer: CellVo[], operator: string) {
        this.top = top;
        this.rows = rows;
        this.operator = operator;
        this.answer = answer;
    }
    public fromObj(data:any)
    {
        let topData:any=data.top;
        let rowsData:any=data.rows;
        let answerData:any=data.answer;

        this.top=CellUtil.gridToCells(topData);
        this.rows=CellUtil.gridToCells(rowsData);
        this.answer=CellUtil.dataToCells(answerData);      

        this.operator=data.operator;


        //console.log(this);

    }
}