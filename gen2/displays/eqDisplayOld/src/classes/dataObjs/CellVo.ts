export class CellVo {
    //public text:string;
    public classArray: string[];
    public text: string="";


    constructor(text: string, classArray: string[]) {
        this.text = text;
        this.classArray = classArray;

    }


    toHtml() {
        let char: string = this.text;
        let cssClasses: string[] = this.classArray.slice();

        //do special here

        /* if (char === "@") {
            char = "0";
            cssClasses.push('hid');
        } */

        let html: string;

        // let key: string = "span_" + index.toString();

        if (cssClasses.length === 0) {
            html = "<span>" + char + "</span>";
        }
        else {
            let classString: string = cssClasses.join(" ");

            html = "<span class='" + classString + "'>" + char + "</span>";
        }
        return html;
    }
}