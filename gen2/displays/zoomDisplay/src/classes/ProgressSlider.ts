
export class ProgressSlider{
    public progressSilderOBJ: HTMLInputElement;
    public sliderKnob:HTMLElement;
    public progressBar:HTMLElement;
    private totalPresSteps: number = 0;
    private singleStepSection:number = 0;
    private sectionTracker:HTMLElement;
    private sectionTickContainer:HTMLElement;
    


    constructor(progSlider:HTMLInputElement, knob:HTMLElement, barFill:HTMLElement, previewText:HTMLElement, tickContainer:HTMLElement){
        this.progressSilderOBJ = progSlider;
        this.sliderKnob = knob;
        this.progressBar = barFill;
        this.sectionTracker = previewText;
        this.sectionTickContainer = tickContainer;
        this.progressSilderOBJ.addEventListener("input",()=>{
           this.setSliderPosition((Number(this.progressSilderOBJ.value)/this.singleStepSection));
        });

    }

    printSliderValue(){
        console.log("Slider Value NOW: " + this.progressSilderOBJ.value);
    }

    getNewStep(silderValue:number|string){
        return Number(silderValue)/this.singleStepSection;
    }

    setTotalPresentationSteps(numberOfSteps:number){
        this.totalPresSteps = numberOfSteps;
        console.log("number of presentation steps: " + this.totalPresSteps);
        this.setSliderSteps(this.totalPresSteps-1);
        this.setSectionText(0);
        this.createSectionMarks(this.sectionTickContainer);
    }

    setSliderSteps(totalSteps:number){
        this.singleStepSection = Math.round(100/totalSteps);
        this.progressSilderOBJ.step = String(this.singleStepSection);

    }

    setSliderPosition(currentPosition:string|number){
        currentPosition = Number(currentPosition);
        //console.log("This is the current value of the slider: " + (Number(currentPosition)*this.singleStepSection));
        this.progressSilderOBJ.value = String((Number(currentPosition)*this.singleStepSection));
        this.sliderKnob.style.left = String(this.progressSilderOBJ.value) + "%";
        this.progressBar.style.width = String(this.progressSilderOBJ.value) + "%";
        this.setSectionText(currentPosition);
        this.updateMarks(this.progressSilderOBJ.value);
    }

    setSectionText(currentPosition:number){
        this.sectionTracker.innerHTML = String(currentPosition+1) + "/" + String(this.totalPresSteps);
    }

    createSectionMarks(tickMarkDiv:HTMLElement){
        for(var i = 1; i < this.totalPresSteps-1; i++){
            const div = document.createElement("div");
            var sectionName = "section" + i.toString();
            div.id = sectionName;
            div.classList.add("sectionMarks");
            div.dataset.step = i.toString();
            div.style.left = `${this.singleStepSection * i}%`;

            // append
            tickMarkDiv.appendChild(div);
        }
    }

    updateMarks(sliderValue:string|number){
        for(var i=1; i < this.totalPresSteps-1; i++){
            var idName:string = 'section'+i;
                var markToChange:HTMLElement = document.getElementById(idName)!;
            if(Number(sliderValue) > (i*this.singleStepSection)){
                markToChange.style.backgroundColor ="#ffffff";
            } else{
                markToChange.style.backgroundColor = "#598CC9";
            }
        }
    }
    
}