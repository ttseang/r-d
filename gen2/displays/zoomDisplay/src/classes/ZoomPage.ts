import { HtmlObj, IScreen, SoundObj, SVGAnimation } from "svggame";
import { DisplayElementVo } from "../dataObjs/DisplayElementVo";
import { PageElementVo } from "../dataObjs/PageElementVo";
import { StepVo } from "../dataObjs/StepVo";
import { ZoomPresentationVo } from "../dataObjs/ZoomPresentationVo";
import { ZoomVo } from "../dataObjs/ZoomVo";
import { LineElement } from "./Elements/LineElement";
import { NumUtil } from "./util/NumUtil";
import { ZoomyController } from "./ZoomyController";
import { ZoomyModel } from "./ZoomyModel";

export class ZoomPage {
    private screen: IScreen;
    private audioHasEnded: boolean = false;

    private step: StepVo = new StepVo(0, []);

    //a map of all the elements currently on the page
    private currentElements: Map<string, DisplayElementVo> = new Map<string, DisplayElementVo>();

    //the svg element addArea 
    public svg: SVGSVGElement | null | undefined;

    //the foreignObject id=ptblank child of addArea where all the dynamic content is placed
    public mainParent: HTMLElement | null = null;

    private zm: ZoomyModel = ZoomyModel.getInstance();
    private zc: ZoomyController = ZoomyController.getInstance();

    //record the old zoom coodinates to use in the zoom animation
    private oldZoom: string = "";

    private audioPlayer: SoundObj | null = null;
    private backgroundPlayer: SoundObj | null = null;
    private lastBackgroundSound: string = "";

    constructor(screen: IScreen) {
        this.screen = screen;

        this.mainParent = document.getElementById("ptblank");
        this.svg = (document.getElementById("addArea") as unknown) as SVGSVGElement;
        //
        //
        //bind the step functions to the main controller
        this.zc.nextStep = this.nextStep.bind(this);
        this.zc.prevStep = this.prevStep.bind(this);
        this.zc.jumpStep = this.jumpStep.bind(this);
        this.zc.audioUpdated = this.audioUpdated.bind(this);
        this.zc.muteChanged = this.muteUpdated.bind(this);
        this.zc.modeChanged = this.modeChanged.bind(this);
        this.zc.playPause = this.playPause.bind(this);
        this.zc.playAudio = this.playAudio.bind(this);
        this.zc.pauseAudio = this.pauseAudio.bind(this);

        (window as any).zp = this;

        window.onresize = () => {
            this.zm.pageH = window.innerHeight;
            this.zm.pageW = window.innerWidth;

            if (this.mainParent) {
                this.mainParent.setAttribute("width", this.zm.pageW.toString());
                this.mainParent.setAttribute("height", this.zm.pageH.toString());
            }

            this.setZoom(true);
        }
    }
    setData(zoomPres: ZoomPresentationVo) {

        let w: number = zoomPres.pageW;
        let h: number = zoomPres.pageH;

        console.log(w + "x" + h);

        if (this.mainParent) {
            this.mainParent.setAttribute("width", w.toString());
            this.mainParent.setAttribute("height", h.toString());
        }


        this.zm.pageH = h;
        this.zm.pageW = w;

        this.step = zoomPres.steps[0];
        // //console.log(this.step);
        // this.buildFrame();
    }
    //TODO hook to buttons
    prevStep() {
        if (this.zm.currentStep > 0) {
            this.zm.currentStep--;
            this.pageChanged();
        }
        this.audioHasEnded = false;
    }

    nextStep() {
        if (this.zm.currentStep < this.zm.presentation.steps.length - 1) {
            this.zm.currentStep++;
            this.pageChanged();

        }
        this.audioHasEnded = false;
    }

    jumpStep(silderValue:number|string){
        console.log("What is the Slider Value: " + silderValue)
        var newStep = this.zm.navProgSlider.getNewStep(silderValue); 
        console.log("This is the NewStep value: " + newStep);
        console.log("This is the currentStep: " + this.zm.currentStep);
        if(this.zm.currentStep != newStep){
            this.zm.currentStep = newStep;
            this.pageChanged();
        }
        this.audioHasEnded = false;
    }
    private playPause() { 
        if (this.audioPlayer) {
            if (this.audioPlayer.sound) {

                if (this.zm.mode === "listen") {
                    if (this.audioHasEnded === true) {
                        this.zm.paused=false;
                        this.zc.nextStep();
                    }
                    else {
                        this.pauseAudio();
                    }
                }
                else {
                    this.pauseAudio();
                }
            }
        }
    }
    private pauseAudio() {
        if (this.audioPlayer) {
            if (this.audioPlayer.sound) {
                if (this.zm.paused === false) {
                    this.audioPlayer.play();
                }
                else {
                    this.audioPlayer.sound.pause();
                    // this.zm.paused=true;
                }
            }
        }
    }
    public playAudio() {
        let path: string = this.step.audio;
        console.log("Path=" + path);

        if (path === "" || this.zm.mode === "read") {
            // this.zc.showButtons();

            return;
        }

        let fullPath: string = this.zm.getAudioPath(path);
        console.log("fullPath=" + fullPath);

        if (this.audioPlayer) {
            this.audioPlayer.stop();
        }

        
        this.audioPlayer = new SoundObj(fullPath, false);
        if (this.zm.paused===true)
        {
            return;
        }
        this.audioPlayer.play();
        this.audioUpdated();
        this.zc.audioStarted();


        this.audioPlayer.sound.addEventListener("ended", () => {
            //this.zc.showButtons();
            this.audioHasEnded = true;
            this.zc.audioEnded();
        })
    }
    private muteUpdated() {
        if (this.backgroundPlayer) {
            if (this.zm.mute === true) {
                this.backgroundPlayer.stop();
            }
            else {
                this.backgroundPlayer.play();
            }
        }
    }
    private modeChanged() {

        //console.log(this.zm.mode);

        if (this.zm.mode === "listen") {
            this.playAudio();
        }
        else {
            if (this.audioPlayer) {
                this.audioPlayer.stop();
            }
        }
    }
    private audioUpdated() {
        if (this.audioPlayer) {
            this.audioPlayer.sound.volume = this.zm.svol / 100;
            this.audioPlayer.sound.playbackRate = this.zm.speed;
        }
        if (this.backgroundPlayer) {
            this.backgroundPlayer.sound.volume = this.zm.mvol / 100;
        }

    }
    public playBackgroundMusic() {
        let backgroundAudio: string = this.step.backgroundAudio;
        //console.log(this.step);
        //console.log(backgroundAudio);
        let fullPath: string = this.zm.getBackgroundAudioPath(backgroundAudio);
        console.log("backgroud music=" + fullPath);

        if (this.lastBackgroundSound !== backgroundAudio) {
            this.lastBackgroundSound = backgroundAudio;
            if (this.backgroundPlayer) {
                this.backgroundPlayer.stop();
            }
            this.backgroundPlayer = new SoundObj(fullPath, true);
            if (this.zm.mute === false) {
                this.backgroundPlayer.play();
                this.audioUpdated();
            }
        }
    }

    private pageChanged() {
        //console.log("current step=" + this.zm.currentStep.toString());
        this.step = this.zm.presentation.steps[this.zm.currentStep];
        this.buildFrame();
        this.zm.navProgSlider.setSliderPosition(this.zm.currentStep);
    }

    setZoom(hard: boolean = false) {

        let pageW = this.zm.pageW;
        let pageH = this.zm.pageH;

        let zoomVo: ZoomVo = this.step.zoom;
        ////console.log(zoomVo);

        //turn percentages into pixels
        let xx: number = (zoomVo.x1 / 100) * pageW;
        let yy: number = (zoomVo.y1 / 100) * pageH;

        //measure the distance between the points
        let distX: number = (zoomVo.x2 - zoomVo.x1) / 100;
        let distY: number = (zoomVo.y2 - zoomVo.y1) / 100;

        //cut off the extra decimal places past 2 points
        distX = Math.round(distX * 100) / 100;
        distY = Math.round(distY * 100) / 100;

        let zoomX: number = (pageW * distX);
        let zoomY: number = (pageH * distY);

        let posString = xx.toString() + " " + yy.toString();
        posString += " " + zoomX + " " + zoomY


        console.log(posString);

        if (this.svg) {
            // 
            if (this.oldZoom === "") {
                this.svg.setAttribute("viewBox", posString);
            }
            else {
                let zoomAnimation: SVGAnimateElement = (document.getElementById("zoom1") as unknown) as SVGAnimateElement;
                if (hard === false) {
                    zoomAnimation.setAttribute("values", this.oldZoom + ";" + posString);
                }
                else {
                    zoomAnimation.setAttribute("values", posString + ";" + posString);
                }

                zoomAnimation.beginElement();
            }

        }
        //record the position for animation
        this.oldZoom = posString;
    }
    deleteUnneeded() {
        this.currentElements.forEach((el: DisplayElementVo) => {
            if (el.delFlag === true) {
                if (el.displayElement && this.mainParent) {
                    this.mainParent.removeChild(el.displayElement);
                    this.currentElements.delete(el.eid);
                }
            }
        })
    }
    buildFrame(playTheAudio: boolean = true) {
        this.currentElements.forEach((el: DisplayElementVo) => { el.delFlag = true })
        // console.log("build frame");

        let elements: PageElementVo[] = this.step.elements;
        ////console.log(elements);
        for (let i: number = 0; i < elements.length; i++) {
            let element: PageElementVo = elements[i];
            if (this.mainParent) {

                /**
                 * does this element already exsist?
                 * if not add it
                 * if so update position and content
                 */

                if (!this.currentElements.has(element.eid)) {

                    /**
                     *create the HTMLElement from the data obect 
                     */
                    let displayElement: HTMLElement | SVGAElement | SVGLineElement = element.getHtml();

                    /**
                     *add the data object to the current element map 
                     */
                    this.currentElements.set(element.eid, new DisplayElementVo(element.eid, displayElement));

                    /**
                     * add the display object to the document
                     */
                    this.mainParent.append(displayElement);
                    // this.svg?.append(displayElement);

                    let cssStyle: CSSStyleDeclaration = element.getCSS();
                    this.zm.addStyle(element, cssStyle);
                }


                else {

                    let displayElementVo: DisplayElementVo | undefined = this.currentElements.get(element.eid);

                    ////console.log(displayElementVo);
                    if (displayElementVo) {

                        displayElementVo.delFlag = false;

                        if (element.type === PageElementVo.TYPE_LINE) {
                            //   //console.log("update line "+element.eid+" "+element.id);

                            LineElement.updateLine(displayElementVo.displayElement as SVGAElement, element);
                        }
                        else {
                            if (element.type===PageElementVo.TYPE_CARD)
                            {
                                if (element.subType===PageElementVo.SUB_TYPE_WHITE_CARD)
                                {
                                let textID:string=element.eid+"-text";
                                
                                let textStyle:CSSStyleDeclaration=element.getTextCSS();
                                
                                //Testing highlight
                                if(!this.zm.textHighlights){
                                    textStyle.color = '#000';
                                }
                                
                                this.zm.addStyleByID(textID,textStyle);

                                let textDiv:HtmlObj=new HtmlObj(textID);
                           
                                if (textDiv.el)
                                {
                                    textDiv.el.innerText=element.content[0];
                                }
                            }
                            if (element.subType===PageElementVo.SUB_TYPE_GREEN_CARD)
                            {
                                let textID:string=element.eid+"-text";
                                let titleID:string=element.eid+"-title";
                                
                                let textDiv:HtmlObj=new HtmlObj(textID);
                                let titleDiv:HtmlObj=new HtmlObj(titleID);

                                if (titleDiv.el)
                                {
                                    titleDiv.el.innerText=element.content[0];
                                }
                                if (textDiv.el)
                                {
                                    textDiv.el.innerText=element.content[1];
                                }
                            }
                            }
                            let css: CSSStyleDeclaration = element.getCSS();

                            displayElementVo.displayElement.setAttribute("style", css.cssText);

                            

                        }
                    }
                }
            }
        }
        this.deleteUnneeded();
        this.setZoom();
        if (playTheAudio === true) {
            this.playAudio();
            this.playBackgroundMusic();
        }

    }
}