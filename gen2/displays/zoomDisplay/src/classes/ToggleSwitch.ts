import { HtmlObj } from "svggame";
import { LinkUtil } from "./util/LinkUtil";

export class ToggleSwitch extends HtmlObj
{
    private knob:HtmlObj;
    private isOn:boolean=false;

    constructor(key:string)
    {
        super(key);
        this.knob=new HtmlObj("hknob");

        if (this.knob)
        {
            LinkUtil.makeLink("hknob",this.toggleOnOff.bind(this),"","")
        }

    }
    setPos()
    {
        if (this.knob)
        {
            if (this.isOn==true)
            {
                this.knob.x=50;
            }
            else
            {
                this.knob.x=20;
            }
        }       
    }
    toggleOnOff()
    {
        this.isOn=!this.isOn;
        this.setPos();
    }
}