import { StepVo } from "./StepVo";


export class ZoomPresentationVo
{
    public steps:StepVo[];
    public pageW:number;
    public pageH:number;

    constructor(steps:StepVo[]=[],pageW:number=0,pageH:number=0)
    {
        this.steps=steps;
        this.pageW=pageW;
        this.pageH=pageH;
    }
    fromObj(obj:any)
    {

        //original width

       /*  let orignalH:number=parseInt(obj.pageH);
        let orginalW:number=parseInt(obj.pageW);

        let windowW:number=window.innerWidth;
        //let windowH:number=window.innerHeight;

        let ratio:number=windowW/orginalW;

        let fullH:number=orignalH*ratio;
        let fullW:number=windowW; */
        /* let hh:number=parseInt(obj.pageH);
        let ww:number=parseInt(obj.pageW);
        console.log(ww+" "+hh);
        let ratio=hh/ww;
        console.log("ratio="+ratio);
        

        let windowWidth: number = Math.floor(window.innerWidth);
       // let ratio: number = windowWidth / ww;
     //   let fullH: number = Math.floor(windowWidth * ratio);
        
 */
     let fullH:number=window.innerHeight;
     let fullW: number = window.innerWidth;
        this.pageW=fullW;
        this.pageH=fullH;

        console.log(this.pageW,this.pageH);

        for (let i:number=0;i<obj.steps.length;i++)
        {
            let stepData:any=obj.steps[i];
            let stepVo:StepVo=new StepVo();
            stepVo.fromObj(stepData);
            this.steps.push(stepVo);
        }
    }
}