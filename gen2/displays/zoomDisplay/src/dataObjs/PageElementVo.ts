import { CardElement } from "../classes/Elements/CardElement";
import { LineElement } from "../classes/Elements/LineElement";
import { ZoomyController } from "../classes/ZoomyController";
import { ZoomyModel } from "../classes/ZoomyModel";
import { ElementExtrasVo } from "./ElementExtrasVo";
import { TextStyleVo } from "./TextStyleVo";

export class PageElementVo {

    public static TYPE_IMAGE: string = "IMAGE";
    public static TYPE_TEXT: string = "TEXT";
    public static TYPE_BACK_IMAGE = "BACK_IMAGE";
    public static TYPE_CARD: string = "CARD";
    public static TYPE_LINE: string = "LINE";

    public static SUB_TYPE_NONE: string = "none";
    public static SUB_TYPE_GUTTER: string = "gutter";
    public static SUB_TYPE_POPUP_LINK: string = "popuplink";

    public static SUB_TYPE_GREEN_CARD: string = "greenCard";
    public static SUB_TYPE_WHITE_CARD: string = "whiteCard";

    public delFlag: boolean = false;
    //unique id to instance
    public id: string;
    //id link to element
    //used to know if we need to add or update 
    //an element

    public eid: string;
    public type: string;
    public subType: string;
    public classes: string[];
    public content: string[];
    public x: number;
    public y: number;
    public w: number;
    public h: number;
    public textStyle:number=-1;

    public extras: ElementExtrasVo;

    public static elementID: number = 0;
    private zoomyModel: ZoomyModel = ZoomyModel.getInstance();
    private zoomyController: ZoomyController = ZoomyController.getInstance();

    private origins: string[];
    
   


    constructor(id: string = "", type: string = "", subType: string = "", classes: string[] = [], content: string[] = [], x: number = 0, y: number = 0, w: number = 0, h: number = 0) {
        this.id = id;
        this.type = type;
        this.subType = subType;
        this.classes = classes;
        this.content = content;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.eid = "instance0";

        this.origins = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
        this.extras = new ElementExtrasVo();

    }
    fromObj(obj: any) {
        //console.log("PE");
        //console.log(obj);

        PageElementVo.elementID++;
        this.id = "bookElement" + PageElementVo.elementID.toString();

        this.type = obj.type;
        this.subType = obj.subType;
        this.classes = obj.classes;
        this.content = obj.content;

        this.textStyle=parseInt(obj.textStyle);


        this.eid =obj.eid;
        this.x = parseFloat(obj.x);
        this.y = parseFloat(obj.y);
        this.w = parseFloat(obj.w);
        this.h = parseFloat(obj.h) || 0;

        this.extras.fromObj(obj.extras);

        /*  //console.log(obj.subtype);
         //console.log(this); */

    }
    getTextCSS()
    {
        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;
       // cssStyle.fontSize=this.extras.fontSize.toString()+"%";
        cssStyle.fontSize="100%";
        cssStyle.fontFamily=this.extras.fontName;
        cssStyle.color=this.extras.fontColor;

        return cssStyle;
    }
    getCSS() {
        //////console.log(this);

        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;

        if (this.type === PageElementVo.TYPE_LINE) {

            return cssStyle
        }

        cssStyle.position = "absolute";
        cssStyle.left = this.x.toString() + "%";
        cssStyle.top = this.y.toString() + "%";

        if (this.w !== 0) {
            cssStyle.width = this.w.toString() + "%";
        }
        if (this.type===PageElementVo.TYPE_CARD)
        {
            /* let h2:number=this.w/2.5;
            cssStyle.width = this.w.toString() + "vw";
            cssStyle.height=h2.toString()+"vw"; */
            cssStyle.width=this.w.toString()+"%";
            cssStyle.minHeight=this.h.toString()+"%";

           /*  cssStyle.fontSize=this.extras.fontSize.toString()+"%";
            cssStyle.fontFamily=this.extras.fontName;
            cssStyle.color=this.extras.fontColor; */

        }
        //
        //
        //
        if (this.type === PageElementVo.TYPE_TEXT && this.textStyle===-1) {
            cssStyle.color = this.extras.fontColor;           
            cssStyle.fontFamily = this.extras.fontName;
        }
        if (this.type===PageElementVo.TYPE_TEXT)
        {
            cssStyle.fontSize = this.extras.fontSize.toString() + "%";
        }

        if (this.type === PageElementVo.TYPE_BACK_IMAGE) {
            let image: string = "url(" + this.zoomyModel.getFullPath(this.content[0]) + ")";
            cssStyle.backgroundImage = image;
            cssStyle.backgroundPositionX = this.extras.backgroundPosX + "px";
            cssStyle.backgroundPositionY = this.extras.backgroundPosY + "px";
        }

        if (this.extras) {
            let transformString: string = "translate(" + this.origins[this.extras.orientation] + ")";


            if (this.extras.alpha) {
                cssStyle.opacity = this.extras.alpha.toString() + "%";
            }

            if (this.extras.rotation > 0) {
                let angle: number = ((this.extras.rotation) / 100) * 360;
                angle = Math.floor(angle);
                ////////console.log(angle);
                transformString += " rotate(" + angle.toString() + "deg)";
            }
            if (this.extras.skewY !== 0) {
                let skewAngleY: number = ((this.extras.skewY) / 100) * 360;
                skewAngleY = Math.floor(skewAngleY);
                ////////console.log(skewAngleY);
                transformString += " skewY(" + skewAngleY.toString() + "deg)";
            }
            if (this.extras.skewX !== 0) {
                let skewAngleX: number = ((this.extras.skewX) / 100) * 360;
                skewAngleX = Math.floor(skewAngleX);
                ////////console.log(skewAngleX);
                transformString += " skewX(" + skewAngleX.toString() + "deg)";
            }
            if (this.extras.flipV === true) {
                transformString += " scaleY(-1)";
            }
            if (this.extras.flipH === true) {
                transformString += " scaleX(-1)";
            }

            if (this.extras.borderThick > 0) {
                cssStyle.border = "solid";
                cssStyle.borderWidth = this.extras.borderThick.toString();
                cssStyle.borderColor = this.extras.borderColor;
            }
            cssStyle.transform = transformString;

            cssStyle.transitionProperty = "top, left, height, width";
            cssStyle.transitionDuration = "2s";
        }

        return cssStyle;
    }
    private addClasses(obj: any) {
        for (let i: number = 0; i < this.classes.length; i++) {
            obj.classList.add(this.classes[i]);
        }
    }
    getClasses() {
        let classArray: string[] = this.classes.slice();

        if (this.type === PageElementVo.TYPE_LINE) {
            return [];
        }

        if (this.type === PageElementVo.TYPE_CARD) {

            classArray.push("infoCard");
            switch (this.subType) {
                case PageElementVo.SUB_TYPE_GREEN_CARD:
                    classArray.push("green");
                    break;

                case PageElementVo.SUB_TYPE_WHITE_CARD:
                    classArray.push("white");
                    break;
            }
        }
        return classArray;
    }
    getHtml() {
        let ukey: string = "element" + this.id.toString();

        let style: any = this.getCSS();

     

        if (this.textStyle!==-1)
        {
            let textStyleVo:TextStyleVo | undefined=this.zoomyModel.styleMap.get(this.textStyle);
            //console.log(textStyleVo);

            if (textStyleVo)
            {
                for (let i:number=0;i<textStyleVo.classes.length;i++)
                {
                    this.classes.push(textStyleVo.classes[i]);
                }
            }
        }
        let classArray: string[] = this.getClasses();
        
        switch (this.type) {

            case PageElementVo.TYPE_IMAGE:

                const img: HTMLImageElement = document.createElement("img");
                img.id = this.id;
                img.src = this.zoomyModel.getFullPath(this.content[0]);
                this.addClasses(img);

                return img;

            case PageElementVo.TYPE_TEXT:

                const text: HTMLElement = document.createElement("article");
                text.id = this.id;
                text.innerText = this.content[0];
                this.addClasses(text);
                return text;

            case PageElementVo.TYPE_CARD:

                let card: CardElement = new CardElement(this, classArray);

                switch (this.subType) {
                    case PageElementVo.SUB_TYPE_GREEN_CARD:
                        return card.getCard1(this.id,this.eid);
                    case PageElementVo.SUB_TYPE_WHITE_CARD:
                        return card.getCard2(this.id,this.eid);
                }
            case PageElementVo.TYPE_LINE:

                console.log("MAKE LINE");
                let le: LineElement = new LineElement(this, classArray);
                return le.getLine();

        }

        let def: HTMLDivElement = document.createElement("div");
        def.innerText = "Not Found " + this.type + "-" + this.subType;
        return def;
    }

}