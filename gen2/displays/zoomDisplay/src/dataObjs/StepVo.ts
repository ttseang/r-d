import { NumUtil } from "../classes/util/NumUtil";
import { PageElementVo } from "./PageElementVo";
import { ZoomVo } from "./ZoomVo";

export class StepVo
{
    public static ElementID:number=0;
    public static StepCount:number=0;

    public id:number;
    public elements:PageElementVo[];
    public label:string="";
    public zoom:ZoomVo=new ZoomVo(0,0,100,100);

    public audio:string="";
    public audioName:string="";
    
    public backgroundAudioName:string="";
    public backgroundAudio:string="";

    constructor(id:number=-1,elements:PageElementVo[]=[])
    {        
        this.id=id;
        this.elements=elements;
        this.label="step-"+id.toString();
    }
    
    fromObj(obj:any)
    {
       
        let x1:number=NumUtil.convertAndRound(obj.zoom.x1);
        let y1:number=NumUtil.convertAndRound(obj.zoom.y1);
        let x2:number=NumUtil.convertAndRound(obj.zoom.x2);
        let y2:number=NumUtil.convertAndRound(obj.zoom.y2);        

        this.zoom=new ZoomVo(x1,y1,x2,y2);
        this.id=parseInt(obj.id);
        this.label="frame-"+this.id.toString();

        let elements:any[]=obj.elements;

        
        if (obj['audioName'])
        {
            this.audioName=obj.audioName;
        }
        if (obj['audio'])
        {
            this.audio=obj.audio;
        }
        if (obj['backgroundAudio'])
        {
            this.backgroundAudio=obj.backgroundAudio;
        }
        if (obj['backgroundAudioName'])
        {
            this.backgroundAudioName=obj.backgroundAudioName;
        }

        for (let i:number=0;i<elements.length;i++)
        {
            
            let pageElement:PageElementVo=new PageElementVo();
            pageElement.fromObj(elements[i]);
            this.elements.push(pageElement);
        }
    }
}