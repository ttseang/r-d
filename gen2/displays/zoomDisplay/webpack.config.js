const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const config = {
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.ts(x)?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.svg$/,
        use: 'file-loader'
      }
    ]
  },
  resolve: {
    extensions: [
      '.tsx',
      '.ts',
      '.js'
    ]
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: 'src/index.html' },
      { from: 'src/styles.css' },
      { from: 'src/Zoomy.css' },
      { from: 'src/bar.css' },
      { from: 'assets/images/ui/buttons/navbuttons/*' },
      { from: 'assets/images/ui/buttons/navbuttons2/*' },
      { from: 'assets/images/ui/nav/*' },
      { from: 'assets/images/ui/vol/*' },
      { from: 'src/TextAnimationStyles.css' },
      { from: 'assets/*' }],
    })
  ]
};

module.exports = config;