import { UiController } from "../core/UiController";
import { ZoomyController } from "../core/ZoomyController";
import { ZoomyModel } from "../core/ZoomyModel";

export class TrackSlider {
    private uiController: UiController = UiController.getInstance();
    private zoomyController: ZoomyController = ZoomyController.getInstance();
    private zoomyModel: ZoomyModel = ZoomyModel.getInstance();
    private element: HTMLElement;

    private onMouseDown: EventListener = () => { };
    private onMouseMove: EventListener = () => { };
    private onMouseUp: EventListener = () => { };
    private onMouseLeave: EventListener = () => { };

    private knob: HTMLElement;
    private track: HTMLElement;
    private value: number = 0;
    private stepNumber: number = 0;

    constructor(element: HTMLElement) {
        (window as any).trackSlider = this;
        this.element = element;
        this.knob = element.querySelector(".knob") as HTMLElement;
        this.track = element.querySelector(".track") as HTMLElement;

        this.zoomyController.setSliderPosition = this.setStep.bind(this);
        
        this.setListeners();

    }
    setListeners() {
        this.onMouseDown = this.startDrag.bind(this) as EventListener;
        this.onMouseMove = this.moveKnob.bind(this) as EventListener;
        this.onMouseUp = this.stopDrag.bind(this) as EventListener;
        this.onMouseLeave = this.stopDrag.bind(this) as EventListener;
        this.knob.addEventListener("mousedown", this.onMouseDown);
    }
    removeListeners() {
       // console.log("remove listeners");
        this.knob.removeEventListener("mousedown", this.onMouseDown);
        window.removeEventListener("mousemove", this.onMouseMove);
        window.removeEventListener("mouseup", this.onMouseUp);
        window.removeEventListener("mouseleave", this.onMouseLeave);
    }
    startDrag(e: MouseEvent) {
      //  console.log("start drag");
        window.addEventListener("mousemove", this.onMouseMove);
        window.addEventListener("mouseup", this.onMouseUp);
        window.addEventListener("mouseleave", this.onMouseLeave);

    }
    moveKnob(e: MouseEvent) {
    //    console.log("move knob horizontal");
        let knobX = e.clientX - this.track.getBoundingClientRect().left;
        if (knobX < 0) {
            knobX = 0;
        }
        if (knobX > this.track.getBoundingClientRect().width) {
            knobX = this.track.getBoundingClientRect().width;
        }
        this.knob.style.left = knobX + "px";
        this.value = knobX / this.track.getBoundingClientRect().width;

        //convert knobX to percentage
        this.value = knobX / this.track.getBoundingClientRect().width;

        const numberOfSteps = this.zoomyModel.stepCount;
        const stepSize: number = 100 / numberOfSteps;
        const percentage = this.value * 100;

        //get the step number based on the percentage and the step size
        this.stepNumber = Math.floor(percentage / stepSize);

        this.showPreviewThumb(this.stepNumber);
    }
    stopDrag(e: MouseEvent) {
        //console.log("stop drag");
        window.removeEventListener("mousemove", this.onMouseMove);
        window.removeEventListener("mouseup", this.onMouseUp);
        window.removeEventListener("mouseleave", this.onMouseLeave);
        this.zoomyController.gotoPage(this.stepNumber);
    }
    showPreviewThumb(index: number) {
        // console.log("showPreviewThumb: "+index);
        const thumbPreview: HTMLElement | null = document.querySelector('.thumbPreviews');
        if (thumbPreview) {
            const thumbs: NodeListOf<HTMLElement> = thumbPreview.querySelectorAll('li');
            thumbs.forEach((thumb: HTMLElement) => {
                thumb.classList.add('invis');
            });
            thumbs[index].classList.remove('invis');
        }
    }
    hidePreviewThumb(index: number) {
        const thumbPreview: HTMLElement | null = document.querySelector('.thumbPreviews');
        if (thumbPreview) {
            const thumbs: NodeListOf<HTMLElement> = thumbPreview.querySelectorAll('li');
            thumbs[index].classList.add('invis');
        }
    }
    public setStep(step: number) {
        this.stepNumber = step;
        const numberOfSteps = this.zoomyModel.stepCount;
        const stepSize: number = 100 / numberOfSteps;
        const percentage = step * stepSize+stepSize/2;
        const knobX = percentage / 100 * this.track.getBoundingClientRect().width;
        this.knob.style.left = knobX + "px";
        this.value = knobX / this.track.getBoundingClientRect().width;
    }
}