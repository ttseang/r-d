import { ISlider } from "../interfaces/ISlider";

export class Volslider implements ISlider {
    private element: HTMLElement;
    private knob: HTMLElement;
    private track: HTMLElement;
    private value: number;

    private onMouseDown: EventListener = () => { };
    private onMouseMove: EventListener = () => { };
    private onMouseUp: EventListener = () => { };
    private onMouseLeave: EventListener = () => { };
    private callback:Function=()=>{};

    constructor(element: HTMLElement) {
       
        this.element = element;
        this.knob = element.querySelector(".knob") as HTMLElement;
        this.track = element.querySelector(".track") as HTMLElement;
        (window as any).volslider = this;

       // this.track.style.border = "1px solid red";
        this.value = 0;

        
        //set event listeners for desktop
        this.onMouseDown = this.startDrag.bind(this) as EventListener;
        this.onMouseMove = this.moveKnob.bind(this) as EventListener;
        this.onMouseUp = this.stopDrag.bind(this) as EventListener;
        this.onMouseLeave = this.stopDrag.bind(this) as EventListener;

        if (this.knob && this.track && this.element) {
            this.setListener();
        }

    }
    //set listener for mouse down on the knob
    public setListener() {
        this.knob.addEventListener("mousedown", this.onMouseDown);
    }
    //remove listener for mouse down on the knob
    public removeListener() {
        this.knob.removeEventListener("mousedown", this.onMouseDown);
        window.removeEventListener("mousemove", this.onMouseMove);
    }
    private startDrag(e: MouseEvent) {
      //  console.log("start drag");
        window.addEventListener("mousemove", this.onMouseMove);
        window.addEventListener("mouseup", this.onMouseUp);
        window.addEventListener("mouseleave", this.onMouseLeave);
    }
    //move knob verticle when dragging
    public moveKnob(e: MouseEvent) {      
        
       //set the knob to clientY but not lower than 0 and not higher than the track height
        let knobY = e.clientY - this.track.getBoundingClientRect().top;
        if (knobY < 0) {
            knobY = 0;
        }
        if (knobY > this.track.getBoundingClientRect().height) {
            knobY = this.track.getBoundingClientRect().height;
        }
        this.knob.style.top = knobY + "px";
        //convert the knob position to a value between 0 and 1 with 0 being the top and 1 being the bottom
        
        this.value =Math.abs(1-( knobY / this.track.getBoundingClientRect().height));
        //convert the value to 3 decimal places
        this.value = Math.round(this.value * 1000) / 1000;
        
        console.log(this.value);
        this.callback(this.value);
    }
    public setPercent(percent: number) {
        this.value = percent;
        let pos = Math.abs(1-percent);
        this.knob.style.top = (this.track.getBoundingClientRect().height * pos) + "px";
    }
    public setCallback(callback:Function) {
        this.callback = callback;
    }
    //stop dragging
    public stopDrag(e: MouseEvent) {
        window.removeEventListener("mousemove", this.onMouseMove);
        window.removeEventListener("mouseup", this.onMouseUp);
        window.removeEventListener("mouseleave", this.onMouseLeave);
    }
  
}