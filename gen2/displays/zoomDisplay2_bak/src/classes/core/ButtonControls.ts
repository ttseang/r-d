import { ISlider } from "../interfaces/ISlider";
import { TrackSlider } from "../ui/trackSlider";
import { TrackSlider2 } from "../ui/trackSlider2";
import { Volslider } from "../ui/volslider";
import { Volslider2 } from "../ui/volslider2";
import { ZoomPreview } from "../zoomPreview";
import { UiController } from "./UiController";
import { ZoomConstants } from "./ZoomConstants";
import { ZoomyController } from "./ZoomyController";
import { ZoomyModel } from "./ZoomyModel";

export class ButtonControls {
    private uiController: UiController = UiController.getInstance();
    private zoomyController: ZoomyController = ZoomyController.getInstance();
    private zoomyModel: ZoomyModel = ZoomyModel.getInstance();

    private audioVolumeSlider: ISlider | null = null;
    private backgroundAudioSlider: ISlider | null = null;
    private seekBarSlider: TrackSlider | TrackSlider2 | null = null;

    constructor() {
        (window as any).buttonControls = this;
        this.setAttribution();
    }
    private setStartButtons() {

        const btnAutoStart: HTMLElement | null = document.querySelector('.bubbleGrid .btnAutoStart');
        const btnListenStart: HTMLElement | null = document.querySelector('.bubbleGrid .btnListenStart');
        const btnReadStart: HTMLElement | null = document.querySelector('.bubbleGrid .btnReadStart');

        if (btnAutoStart) {
            btnAutoStart.addEventListener('click', () => this.setMode(ZoomConstants.MODES.AUTO));
        }
        if (btnListenStart) {
            btnListenStart.addEventListener('click', () => this.setMode(ZoomConstants.MODES.LISTEN));
        }
        if (btnReadStart) {
            btnReadStart.addEventListener('click', () => this.setMode(ZoomConstants.MODES.READ));
        }

        this.zoomyController.setCorrectVolumeValues = this.updateVolumeControlsOnResize.bind(this);
        this.zoomyController.onAudioChanged = this.setPlayPauseButton.bind(this);
    }
    private setMode(mode: number) {
        //  console.log("SET MODE " + mode);
        this.zoomyModel.mode = mode;
        //remove all children from the bubbleGrid
        const bubbleGrid: HTMLElement | null = document.querySelector('.bubbleGrid');
        if (bubbleGrid) {
            bubbleGrid.innerHTML = '';

            //remove bubbleGrid
            document.body.removeChild(bubbleGrid as HTMLElement);
        }
        //remove shade
        const shade: HTMLElement | null = document.querySelector('.shade');
        if (shade) {
            document.body.removeChild(shade);
        }
        
        this.setMainListeners();
        this.zoomyController.initalModeSet();
        this.zoomyController.initalModeSet = () => { };
        this.setNavModeButtons();
    }
    private resetMode(mode: number) {
        this.zoomyModel.mode = mode;
        this.setNavModeButtons();
        this.zoomyController.onModeChange(mode);
    }
    private setMainListeners() {

        this.setToggleButtons();
        this.setNavBarListeners();
        this.setNavModeButtons();
        this.makeThumbPreviews();
        this.setVolumeControlListeners();
        this.setSpeedButtons();

    }
    private setToggleButtons() {
        const btnSpeed: HTMLElement | null = document.querySelector('.toggleButton.speed');
        const btnVolume: HTMLElement | null = document.querySelector('.toggleButton.music');
        const btnSpeech: HTMLElement | null = document.querySelector('.toggleButton.speech');
        const btnClose: HTMLElement | null = document.querySelector('.btnClose');
        const btnPlayPause: HTMLElement | null = document.querySelector('.toggleButton.play');

        if (btnSpeed) {
            btnSpeed.addEventListener('click', () => this.uiController.toggleSpeed());
        }
        if (btnVolume) {
            btnVolume.addEventListener('click', () => this.uiController.toggleVolume());
        }
        if (btnSpeech) {
            btnSpeech.addEventListener('click', () => this.uiController.toggleSpeech());
        }
        if (btnClose) {
            btnClose.addEventListener('click', () => this.uiController.closeBars());
        }
        if (btnPlayPause) {
            btnPlayPause.addEventListener('click', () => this.zoomyController.togglePlayPause());
        }
    }
    private setNavBarListeners() {
        const barTab: HTMLElement | null = document.querySelector('.fullWidthBar.top .barTab');
        if (barTab) {
            //check to see if the bar is closed when clicked
            barTab.addEventListener('click', () => {
                if (barTab.classList.contains('closed')) {
                    this.uiController.openBars();
                } else {
                    this.uiController.closeBars();
                }
            });
        }
    }
    private setPlayPauseButton() {
        const btnPlay: HTMLElement | null = document.querySelector('.toggleButton.play');
        if (btnPlay) {
            if (this.zoomyModel.isPlaying === true) {
                btnPlay.classList.add('pause');
            } else {
                btnPlay.classList.remove('pause');
            }
        }
    }
    private makeThumbPreviews() {
        //section list items
        const sectionList: HTMLElement | null = document.querySelector('.sections');
        const thumbPreview: HTMLElement | null = document.querySelector('.thumbPreviews');

        const sectionCount = this.zoomyModel.stepCount;
        console.log("sectionCount: " + sectionCount);

        if (sectionList && thumbPreview) {

            for (let i = 0; i < sectionCount; i++) {
                const sectionItem: HTMLElement = document.createElement('li');
                sectionItem.classList.add('section');
                sectionList.appendChild(sectionItem);

                const previewItem: HTMLElement = document.createElement('li');
                previewItem.classList.add('invis');
                thumbPreview.appendChild(previewItem);

                const zoomPreview: ZoomPreview = new ZoomPreview(i);
            }
        }
        //set css var --sections
        const root = document.querySelector('html') as HTMLElement;
        root.style.setProperty('--sections', sectionCount.toString());

        //nav buttons next and prev

        const btnNext: HTMLElement | null = document.querySelector('.navButton.next');
        if (btnNext) {
            //alert on click
            //    btnNext.addEventListener('click', () => alert('Next Page'));
            btnNext.addEventListener('click', () => this.zoomyController.nextPage());
        }

        const btnPrev: HTMLElement | null = document.querySelector('.navButton.prev');
        if (btnPrev) {
            btnPrev.addEventListener('click', () => this.zoomyController.prevPage());
        }

    }
    /**
        * Volume Controls
        */
    private setVolumeControlListeners() {
        const volSliderElement: HTMLElement = document.querySelector('.volSlider') as HTMLElement;

        if (this.zoomyModel.isMobile === true) {
            this.backgroundAudioSlider = new Volslider2(volSliderElement);
        }
        else {
            this.backgroundAudioSlider = new Volslider(volSliderElement);
        }

        this.backgroundAudioSlider = new Volslider2(volSliderElement);
        this.backgroundAudioSlider.setPercent(this.zoomyModel.backgroundAudioVolume);
        this.backgroundAudioSlider.setCallback(this.updateBackgroundVolume.bind(this));


        const voiceSliderElement: HTMLElement = document.querySelector('.voiceSlider') as HTMLElement;
        if (this.zoomyModel.isMobile === true) {
            this.audioVolumeSlider = new Volslider2(voiceSliderElement);
        }
        else {
            this.audioVolumeSlider = new Volslider(voiceSliderElement);
        }

        this.audioVolumeSlider.setPercent(this.zoomyModel.audioVolume);
        this.audioVolumeSlider.setCallback(this.updateAudioVolume.bind(this));


        const seekBar: HTMLElement = document.querySelector('.seekBar') as HTMLElement;

        if (this.zoomyModel.isMobile === true) {
            this.seekBarSlider = new TrackSlider2(seekBar);
        }
        else {
            this.seekBarSlider = new TrackSlider(seekBar);
        }

        this.zoomyController.addResizeCallback(this.updateVolumeControlsOnResize.bind(this));

        this.updateVolumeControlsOnResize();

        /** Mode Buttons from top bar */
        const btnListen: HTMLElement | null = document.querySelector('.toggleButton.listen');
        const btnRead: HTMLElement | null = document.querySelector('.toggleButton.read');
        const btnAuto: HTMLElement | null = document.querySelector('.toggleButton.autoPlay');

        if (btnListen) {
            btnListen.addEventListener('click', () => this.resetMode(ZoomConstants.MODES.LISTEN));
        }
        if (btnRead) {
            btnRead.addEventListener('click', () => this.resetMode(ZoomConstants.MODES.READ));
        }
        if (btnAuto) {
            btnAuto.addEventListener('click', () => this.resetMode(ZoomConstants.MODES.AUTO));
        }

        //mute button
        const btnMute: HTMLElement | null = document.querySelector('.btnMute');

        if (btnMute) {
            btnMute.addEventListener('click', () => {
                this.zoomyModel.backgroundAudioMuted = !this.zoomyModel.backgroundAudioMuted;
                if (this.zoomyModel.backgroundAudioMuted) {
                    btnMute.classList.add('muted');
                } else {
                    btnMute.classList.remove('muted');
                }
                this.zoomyController.backgroundAudioVolumeChanged();
            });
        }
        //set highlight button controls here because there is nowhere else to put it
        const btnHighlight: HTMLElement | null = document.querySelector('.toggleSwitch');
        if (btnHighlight) {
            //get the knob element
            const knob: HTMLElement | null = btnHighlight.querySelector('.toggleKnob');
            if (knob) {
                btnHighlight.addEventListener('click', () => {
                    this.zoomyModel.highlightingOn = !this.zoomyModel.highlightingOn;
                    if (this.zoomyModel.highlightingOn) {
                        knob.classList.add('selected');
                    } else {
                        knob.classList.remove('selected');
                    }
                });
            }
        }

        const btnCopyright: HTMLElement | null = document.querySelector('.toggleButton.copyright');

        if (this.zoomyModel.zoomPres) {
            if (this.zoomyModel.zoomPres.attribution !== "") {
                if (btnCopyright) {
                    //show the button
                    btnCopyright.classList.remove('hid');
                }
            }
        }

        if (btnCopyright) {
            btnCopyright.addEventListener('click', () => {
                //show the attribution
                const attribution: HTMLElement | null = document.querySelector('.copyrightOverlay');
                if (attribution) {
                    attribution.classList.toggle('hid');
                    if (attribution.classList.contains('hid')) {
                        btnCopyright.classList.remove('selected');
                    } else {
                        btnCopyright.classList.add('selected');
                    }
                }
            });
        }

        if (this.zoomyModel.isMobile === true) {
           //show the full screen button
            const btnFullScreen: HTMLElement | null = document.querySelector('.toggleButton.full');
            if (btnFullScreen) {
                btnFullScreen.classList.remove('hid');
                btnFullScreen.addEventListener('click', () => {
                    this.zoomyController.goFullScreen();
                }
                );
            }
        }
    }
    //show the current mode on the top bar toggle buttons
    private setNavModeButtons() {
        this.clearToggleButtons();
        switch (this.zoomyModel.mode) {
            case ZoomConstants.MODES.READ:
                const read: HTMLElement | null = document.querySelector('.toggleButton.read');
                if (read) {
                    read.classList.add('selected');
                }
                break;
            case ZoomConstants.MODES.LISTEN:
                const listen: HTMLElement | null = document.querySelector('.toggleButton.listen');
                if (listen) {
                    listen.classList.add('selected');
                }
                break;
            case ZoomConstants.MODES.AUTO:
                const auto: HTMLElement | null = document.querySelector('.toggleButton.autoPlay');
                if (auto) {
                    auto.classList.add('selected');
                }
                break;
        }
    }
    //set the correct percentage on the volume sliders
    private updateVolumeControlsOnResize() {
        if (this.audioVolumeSlider) {
            this.audioVolumeSlider.setPercent(this.zoomyModel.audioVolume);
        }
        if (this.backgroundAudioSlider) {
            this.backgroundAudioSlider.setPercent(this.zoomyModel.backgroundAudioVolume);
        }
        if (this.seekBarSlider) {
            this.seekBarSlider.setStep(this.zoomyModel.currentStep);
        }
    }
    private updateAudioVolume(percent: number) {
        this.zoomyModel.audioVolume = percent;
        this.zoomyController.audioVolumeChanged();
    }
    private updateBackgroundVolume(percent: number) {
        this.zoomyModel.backgroundAudioVolume = percent;
        this.zoomyController.backgroundAudioVolumeChanged();
    }
    private clearToggleButtons() {
        const buttons: NodeListOf<HTMLElement> = document.querySelectorAll('.navGrid .toggleButton');
        for (let i = 0; i < buttons.length; i++) {
            buttons[i].classList.remove('selected');
        }
    }
    private setSpeedButtons() {
        const btnFast: HTMLElement | null = document.querySelector('.speedButton.fast');
        const btnSlow: HTMLElement | null = document.querySelector('.speedButton.slow');
        const btnNormal: HTMLElement | null = document.querySelector('.speedButton.medium');

        if (btnFast) {
            btnFast.addEventListener('click', () => this.setSpeed(ZoomConstants.SPEEDS.FAST));
        }
        if (btnSlow) {
            btnSlow.addEventListener('click', () => this.setSpeed(ZoomConstants.SPEEDS.SLOW));
        }
        if (btnNormal) {
            btnNormal.addEventListener('click', () => this.setSpeed(ZoomConstants.SPEEDS.NORMAL));
        }

    }
    setSpeed(speed: number) {

        let allButtons: NodeListOf<HTMLElement> = document.querySelectorAll('.speedButton');
        for (let i = 0; i < allButtons.length; i++) {
            allButtons[i].classList.remove('selected');
        }
        switch (speed) {
            case ZoomConstants.SPEEDS.FAST:
                const btnFast: HTMLElement | null = document.querySelector('.speedButton.fast');
                if (btnFast) {
                    btnFast.classList.add('selected');
                }
                break;
            case ZoomConstants.SPEEDS.SLOW:
                const btnSlow: HTMLElement | null = document.querySelector('.speedButton.slow');
                if (btnSlow) {
                    btnSlow.classList.add('selected');
                }
                break;
            case ZoomConstants.SPEEDS.NORMAL:
                const btnNormal: HTMLElement | null = document.querySelector('.speedButton.medium');
                if (btnNormal) {
                    btnNormal.classList.add('selected');
                }
                break;
        }

        this.zoomyModel.audioSpeed = speed;
        this.zoomyController.audioSpeedChanged();
    }
    private setAttribution() {
        const attributionElement: HTMLElement | null = document.querySelector('.copyrightOverlay');
        if (attributionElement) {
            attributionElement.classList.remove('hid');
            //click once
            attributionElement.addEventListener('click', () => {
                attributionElement.removeEventListener('click', () => { });
                this.hideAttribution();
            });
        }
        const attributionParagraph: HTMLElement | null = document.querySelector('.copyrightOverlay>p');
        if (attributionParagraph) {
            if (this.zoomyModel.zoomPres) {
                if (this.zoomyModel.zoomPres.attribution !== "") {
                    attributionParagraph.innerHTML = this.zoomyModel.zoomPres.attribution;
                }
                else {
                    this.hideAttribution();
                }
            }
            else {
                this.hideAttribution();
            }
        }
    }

    private hideAttribution() {
        const attributionElement: HTMLElement | null = document.querySelector('.copyrightOverlay');
        if (attributionElement) {
            //hide the attribution
            attributionElement.classList.add('hid');
        }
        const btnCopyright: HTMLElement | null = document.querySelector('.toggleButton.copyright');
        if (btnCopyright) {
            //unselect the button
            btnCopyright.classList.remove('selected');
        }
        this.setStartButtons();
    }
}