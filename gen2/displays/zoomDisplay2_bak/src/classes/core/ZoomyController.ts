export class ZoomyController
{
    private static instance: ZoomyController;
    public nextPage:Function=()=>{};
    public prevPage:Function=()=>{};
    public gotoPage:Function=()=>{};
    public setMode:Function=()=>{};
    public audioComplete:Function=()=>{};
    public initalModeSet:Function=()=>{};
    public setSliderPosition:Function=()=>{};
    public audioSpeedChanged:Function=()=>{};
    public audioVolumeChanged:Function=()=>{};
    public backgroundAudioVolumeChanged:Function=()=>{};
    

    public onModeChange:Function=()=>{};
    public resizeCallbacks:Function[] = [];
    public setCorrectVolumeValues:Function=()=>{};
    public onAudioChanged:Function=()=>{};
    public togglePlayPause:Function=()=>{};
    public goFullScreen:Function=()=>{};

    public setSpeed:Function=()=>{};
    public sendAlert:Function=()=>{};

    private constructor() {
        //private constructor
    }
    public static getInstance(): ZoomyController {
        if (!ZoomyController.instance) {
            ZoomyController.instance = new ZoomyController();
            (window as any).zoomyController = ZoomyController.instance;
        }
        return ZoomyController.instance;
    }
    
    public addResizeCallback(callback:Function)
    {
        this.resizeCallbacks.push(callback);
    }
    public removeResizeCallback(callback:Function)
    {
        let index = this.resizeCallbacks.indexOf(callback);
        if (index > -1) {
            this.resizeCallbacks.splice(index, 1);
        }

    }
    public resize()
    {
        for (let i = 0; i < this.resizeCallbacks.length; i++) {
            this.resizeCallbacks[i]();
        }
    }
}