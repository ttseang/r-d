export class TextStyleVo
{
    public id:number;
    public name:string
    public type:string
    public classes:string[]
    constructor(id:number,name:string,type:string,classes:string[])
    {
        this.id=id;
        this.name=name;
        this.type=type;
        this.classes=classes;
    }
}