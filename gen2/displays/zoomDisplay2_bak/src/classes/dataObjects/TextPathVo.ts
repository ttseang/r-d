export class TextPathVo
{
    public label:string;
    public path:string;
    public anchorX:string;

    constructor(label:string,path:string,anchorX:Number)
    {
        this.label=label;
        this.path = path;
        this.anchorX = anchorX.toString();
    }
}