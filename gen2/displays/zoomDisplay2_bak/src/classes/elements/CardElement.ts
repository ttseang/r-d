import { PageElementVo } from "../dataObjects/PageElementVo";


export class CardElement {
    private pageElementVo: PageElementVo;
    private classArray: string[];
    constructor(pageElementVo: PageElementVo, classArray: string[]) {
        this.pageElementVo = pageElementVo;
        this.classArray = classArray;
    }
    /**
     * title and content
     */
    getCard1(id:string) {
        let container: HTMLDivElement = document.createElement("div");
        container.id=id;
        let titleDiv: HTMLDivElement = document.createElement("h2");
        let textDiv: HTMLElement = document.createElement("p");

        titleDiv.innerText = this.pageElementVo.content[0];
        textDiv.innerText = this.pageElementVo.content[1];

     //   titleDiv.id=eid+"-title";
     //   textDiv.id=eid+"-text";

        container.appendChild(titleDiv);
        container.appendChild(textDiv);

        for (let i: number = 0; i < this.classArray.length; i++) {
            container.classList.add(this.classArray[i]);
        }
        return container;
    }
    /**
     * just single text
     */
    getCard2(id:string) {
        let container: HTMLDivElement = document.createElement("div");
        container.id=id;
        let textDiv: HTMLElement = document.createElement("p");
        textDiv.innerText = this.pageElementVo.content[0];
        container.appendChild(textDiv);
       
        for (let i: number = 0; i < this.classArray.length; i++) {
            container.classList.add(this.classArray[i]);
        }
        return container;
    }
}