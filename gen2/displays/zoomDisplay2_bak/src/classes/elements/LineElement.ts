import { ZoomyModel } from "../core/ZoomyModel";
import { PageElementVo } from "../dataObjects/PageElementVo";
import { NumUtil } from "../util/NumUtil";


export class LineElement {
    private pageElementVo: PageElementVo;
    private classArray: string[];
    private zm: ZoomyModel = ZoomyModel.getInstance();

    constructor(pageElementVo: PageElementVo, classArray: string[]) {
        this.pageElementVo = pageElementVo;
        this.classArray = classArray;
    }
    getLine() {
        /* let x1: number = NumUtil.roundDec((this.pageElementVo.x / 100) * this.zm.pageW);
        let y1: number = NumUtil.roundDec((this.pageElementVo.y / 100) * this.zm.pageH);
        let x2: number = NumUtil.roundDec((this.pageElementVo.w / 100) * this.zm.pageW);
        let y2: number = NumUtil.roundDec((this.pageElementVo.h / 100) * this.zm.pageH); */

        let x1: number = NumUtil.roundDec(this.pageElementVo.x);
        let y1: number = NumUtil.roundDec(this.pageElementVo.y);
        let x2: number = NumUtil.roundDec(this.pageElementVo.w);
        let y2: number = NumUtil.roundDec(this.pageElementVo.h);

       // console.log(x1, x2, y1, y2);

        let def: HTMLElement | null = document.getElementById("line2");
        if (def) {
            let svgContainer: SVGAElement = def.cloneNode(true) as SVGAElement;
           // svgContainer.classList.add("rel");
            svgContainer.classList.add("fullLine");
           // svgContainer.id="test1";

           // let div1:HTMLDivElement=document.createElement("div");
          //  div1.setAttribute("xmlns","http://www.w3.org/1999/xhtml");

          
            let line: SVGLineElement | null = svgContainer.querySelector("#copyline2");
            if (line) {
                line.setAttribute("x1", x1.toString()+"%");
                line.setAttribute("y1", y1.toString()+"%");
                line.setAttribute("x2", x2.toString()+"%");
                line.setAttribute("y2", y2.toString()+"%");
                line.id = "line" + this.pageElementVo.eid.toString();

                let lineStyle: CSSStyleDeclaration = document.createElement("span").style;
                lineStyle.stroke = this.pageElementVo.extras.borderColor;
                lineStyle.strokeWidth = this.pageElementVo.extras.borderThick.toString() + "px";
                //lineStyle.transitionProperty="x1 y1 x2 y2";
                //lineStyle.transitionDuration="2s";

                line.setAttribute("style", lineStyle.cssText);
               
                svgContainer.appendChild(line);
             //   div1.appendChild(svgContainer);
            }
            //    let line: SVGLineElement = def.cloneNode(true) as SVGLineElement;
            
            return svgContainer;
        }
        let nf: HTMLElement = document.createElement("div");
        nf.innerText = "Not Found";
        return nf;
    }

    getLine2() {
        let x1: number = NumUtil.roundDec((this.pageElementVo.x / 100) * this.zm.pageW);
        let y1: number = NumUtil.roundDec((this.pageElementVo.y / 100) * this.zm.pageH);
        let x2: number = NumUtil.roundDec((this.pageElementVo.w / 100) * this.zm.pageW);
        let y2: number = NumUtil.roundDec((this.pageElementVo.h / 100) * this.zm.pageH);

    

        //console.log(x1, x2, y1, y2);

        let def: HTMLElement | null = document.getElementById("copyline2");
        if (def) {
            /* let svgContainer: SVGAElement = def.cloneNode(true) as SVGAElement;
            svgContainer.classList.add("rel");
            svgContainer.classList.add("full") */
            
            let line: SVGLineElement | null = def.cloneNode(true) as SVGLineElement;
            if (line) {
                line.setAttribute("x1", x1.toString());
                line.setAttribute("y1", y1.toString());
                line.setAttribute("x2", x2.toString());
                line.setAttribute("y2", y2.toString());
                line.id = "line" + this.pageElementVo.eid.toString();

                let lineStyle: CSSStyleDeclaration = document.createElement("span").style;
                lineStyle.stroke = this.pageElementVo.extras.borderColor;
                lineStyle.strokeWidth = this.pageElementVo.extras.borderThick.toString() + "px";
                //lineStyle.transitionProperty="x1 y1 x2 y2";
                //lineStyle.transitionDuration="2s";

                line.setAttribute("style", lineStyle.cssText);
            }
            //    let line: SVGLineElement = def.cloneNode(true) as SVGLineElement;
            return line;
        }
        let nf: HTMLElement = document.createElement("div");
        nf.innerText = "Not Found";
        return nf;
    }

    public static updateLine(container: SVGAElement, pageElementVo: PageElementVo) {
        let zm: ZoomyModel = ZoomyModel.getInstance();
        let x1: number = NumUtil.roundDec((pageElementVo.x / 100) * zm.pageW);
        let y1: number = NumUtil.roundDec((pageElementVo.y / 100) * zm.pageH);
        let x2: number = NumUtil.roundDec((pageElementVo.w / 100) * zm.pageW);
        let y2: number = NumUtil.roundDec((pageElementVo.h / 100) * zm.pageH);

        //console.log(x1, x2, y1, y2);
        let lineID: string = "line" + pageElementVo.eid.toString();

        let line: SVGLineElement | null = container.querySelector("#" + lineID);
        if (line) {
            //moveline
            let oldX1: string = line.getAttribute("x1") || "0";
            let oldX2: string = line.getAttribute("x2") || "0";
            let oldY1: string = line.getAttribute("y1") || "0";
            let oldY2: string = line.getAttribute("y2") || "0";

            //console.log(oldX1, oldX2, oldY2, oldY2);

            //   line.setAttribute("x1", x1.toString());
            /*  line.setAttribute("y1", y1.toString());
             line.setAttribute("x2", x2.toString());
             line.setAttribute("y2", y2.toString());
  */
            let lineAnimation: SVGAnimateElement = (line.querySelector("#movelinex1") as unknown) as SVGAnimateElement;
            lineAnimation.setAttribute("values", oldX1 + ";" + x1.toString());
            lineAnimation.beginElement();

            let lineAnimation2: SVGAnimateElement = (line.querySelector("#movelinex2") as unknown) as SVGAnimateElement;
            lineAnimation2.setAttribute("values", oldX2 + ";" + x2.toString());
            lineAnimation2.beginElement();

            let lineAnimation3: SVGAnimateElement = (line.querySelector("#moveliney1") as unknown) as SVGAnimateElement;
            lineAnimation3.setAttribute("values", oldY1 + ";" + y1.toString());
            lineAnimation3.beginElement();

            let lineAnimation4: SVGAnimateElement = (line.querySelector("#moveliney2") as unknown) as SVGAnimateElement;
            lineAnimation4.setAttribute("values", oldY2 + ";" + y2.toString());
            lineAnimation4.beginElement();


            setTimeout(() => {
                if (line) {
                    line.setAttribute("x1", x1.toString());
                    line.setAttribute("y1", y1.toString());
                    line.setAttribute("x2", x2.toString());
                    line.setAttribute("y2", y2.toString());
                }
            }, 600);
        }
        else {
            //console.log("line not found");
        }

    }
}