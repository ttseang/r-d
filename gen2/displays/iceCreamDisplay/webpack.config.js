const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const config = {
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.ts(x)?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.svg$/,
        use: 'file-loader'
      }
    ]
  },
  resolve: {
    extensions: [
      '.tsx',
      '.ts',
      '.js'
    ]
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: 'src/index.html' },
      { from: 'src/bar.css' },
      { from: 'src/styles.css' },
      { from: 'src/IceCream.css' },
      { from: 'src/Anims.css' },
      { from: 'src/TextAnimationStyles.css' },
      { from: 'assets/*' },
      { from: 'assets/animationMap.json' },
      { from: 'assets/textStyleMap.json' },
      { from: 'assets/images/ui/buttons/navbuttons/*' },
      { from: 'assets/images/ui/nav/*' }],
    })
  ]
};

module.exports = config;