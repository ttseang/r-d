
import { BaseScreen, IScreen, SvgObj, TextObj } from "svggame";
import { PresPage } from "../classes/PresPage";
import { PresLoader } from "../classes/PresLoader";
import { PresController } from "../classes/PresController";
import { PresModel } from "../classes/PresModel";
import { PresentationVo } from "../dataObjs/PresentationVo";
import { AnimationMapLoader } from "../classes/util/AnimationMapLoader";
import { TextStyleMapLoader } from "../classes/util/TextStyleMapLoader";
import { BottomBar } from "../classes/BottomBar";
import { NavBar } from "../classes/NavBar";

export class ScreenMain extends BaseScreen implements IScreen {
    private presPage: PresPage;
    private presModel: PresModel = PresModel.getInstance();
    private presController: PresController = PresController.getInstance();
    
    private navBar:NavBar;
    private bottomBar:BottomBar;
    
    //use true to compile for admin preview
    //use false for student display
    private compilePreview: boolean = true;

    constructor() {
        super("ScreenMain");
     //   this.gm.regFontSize("instructions", 26, 1000);
        this.presPage = new PresPage(this);

        this.presController.hideButtons=this.hideButtons.bind(this);
        this.presController.showButtons=this.showButtons.bind(this);

        this.navBar=new NavBar(this);
        this.bottomBar=new BottomBar(this,this.contentPercentChanged.bind(this));
        this.navBar.bottomBar=this.bottomBar;
        this.navBar.closeBar();
    }
    create() {
        super.create();

        (window as any).scene = this;
        
        this.loadAnimationMap();
       
       
    }
    
    contentPercentChanged(per:number)
    {
      //  console.log(per);
    }
    loadAnimationMap()
    {
        let aloader:AnimationMapLoader=new AnimationMapLoader(this.loadTextStyles.bind(this));
        aloader.load("./animationMap.json");
    }
    loadTextStyles()
    {
        let cssLoader:TextStyleMapLoader=new TextStyleMapLoader(this.loadPage.bind(this));
        cssLoader.load("./textStyleMap.json");
    }
    loadPage()
    {
        let loader: PresLoader = new PresLoader(this.pageLoaded.bind(this));
        if (this.compilePreview === true) {

            let bookContentDiv: HTMLDivElement | null | undefined = window.parent.document.getElementById("bookData") as HTMLDivElement;

            if (bookContentDiv) {
                let content: string = bookContentDiv.innerText;
                loader.loadFromString(content);
            }
        }
        else {
            loader.loadFile("./assets/ic5.json");
        }
    }
    pageLoaded(presData: PresentationVo) {
      
        this.presModel.presentation = presData;
        this.presPage.setData(presData);
        this.setButtons();
    }
    setButtons() {
        let btnNext: HTMLButtonElement = document.getElementById("btnNext") as HTMLButtonElement;
        let btnPrev: HTMLButtonElement = document.getElementById("btnPrev") as HTMLButtonElement;
        let btnStart: HTMLButtonElement = document.getElementById("btnStart") as HTMLButtonElement;

        btnNext.onclick = () => { this.presController.nextStep() };
        btnPrev.onclick = () => { this.presController.prevStep() };
        btnStart.onclick=()=>{
            btnStart.style.display="none";
            this.presController.nextStep();
        }
    }
    hideButtons()
    {
        let buttons:HTMLDivElement=document.getElementById("navButtons") as HTMLDivElement;
        buttons.style.display="none";
    }
    showButtons()
    {
        let buttons:HTMLDivElement=document.getElementById("navButtons") as HTMLDivElement;
        buttons.style.display="block";
    }
    doResize() {
        super.doResize();
    }
}