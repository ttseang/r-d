import { PageElementVo } from "./PageElementVo";

export class DisplayElementVo
{
    public eid:string;
    public displayElement:HTMLElement | SVGLineElement | SVGAElement;
    public delFlag:boolean=false;

    

    constructor(eid:string,displayElement:HTMLElement | SVGLineElement | SVGAElement)
    {
        this.eid=eid;
        this.displayElement=displayElement;
    }
  
}