import { PageElementVo } from "./PageElementVo";


export class StepVo
{
    public static ElementID:number=0;
    public static StepCount:number=0;

    public id:number;
    public elements:PageElementVo[];
    public label:string="";
    
    public audio:string="";
    public backgroundAudio:string="";

    constructor(id:number=-1,elements:PageElementVo[]=[])
    {        
        this.id=id;
        this.elements=elements;
        this.label="step-"+id.toString();
    }
    
    fromObj(obj:any)
    {
       
        this.id=parseInt(obj.id);
        this.label="frame-"+this.id.toString();

        let elements:any[]=obj.elements;

        for (let i:number=0;i<elements.length;i++)
        {
            
            let pageElement:PageElementVo=new PageElementVo();
            pageElement.fromObj(elements[i]);
            this.elements.push(pageElement);
        }

        this.audio=obj.audio;
        this.backgroundAudio=obj.bgAudio;
        console.log(this);
    }
}