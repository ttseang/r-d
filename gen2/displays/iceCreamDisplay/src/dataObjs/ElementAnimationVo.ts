export class ElementAnimationVo
{
    public move:number;
    public fade:number;
    public grow:number;
    public speed:number;

    constructor(move:number=-1,fade:number=-1,grow:number=-1,speed:number=0)
    {
        this.move=move;
        this.fade=fade;
        this.grow=grow;
        this.speed=speed;
    }
    isChecked(val:number)
    {
        if (this.move===val || this.fade===val || this.grow===val || this.speed===val)
        {
            return true;
        }
        return false;
    }
    fromObj(obj:any)
    {
        this.move=parseInt(obj.move);
        this.fade=parseInt(obj.fade);
        this.grow=parseInt(obj.grow);
        this.speed=parseInt(obj.speed);
    }
}