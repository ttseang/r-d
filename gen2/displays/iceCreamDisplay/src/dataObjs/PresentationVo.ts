import { StepVo } from "./StepVo";


export class PresentationVo
{
    public steps:StepVo[];
    public pageW:number;
    public pageH:number;

    constructor(steps:StepVo[]=[],pageW:number=0,pageH:number=0)
    {
        this.steps=steps;
        this.pageW=pageW;
        this.pageH=pageH;
    }
    fromObj(obj:any)
    {
        this.pageH=parseInt(obj.pageH);
        this.pageW=parseInt(obj.pageW);


        for (let i:number=0;i<obj.steps.length;i++)
        {
            let stepData:any=obj.steps[i];
            let stepVo:StepVo=new StepVo();
            stepVo.fromObj(stepData);
            this.steps.push(stepVo);
        }
    }
}