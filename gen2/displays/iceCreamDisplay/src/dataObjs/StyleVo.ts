export class StyleVo
{
    public color:string;
    public fontSize:string;
    public fontWeight:string;

    constructor(color:string,fontSize:string,fontWeight:string)
    {
        this.color=color;
        this.fontSize=fontSize;
        this.fontWeight=fontWeight;
    }
}