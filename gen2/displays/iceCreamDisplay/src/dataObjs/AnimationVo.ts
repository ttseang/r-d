export class AnimationVo {
    public name: string;
    public type: string;
    public subType: string;
    public inOut: string;
    public initClasses: string[];
    public animClasses: string[];

    public static TYPE_IMAGE: string = "IMAGE";
    public static TYPE_TEXT: string = "TEXT";

    public static TYPE_ALL: string = "all";

    public static DIR_IN: string = "in";
    public static DIR_OUT: string = "out";
    public static DIR_BOTH: string = "both";

    public static SUB_TYPE_MOVE: string = "move";
    public static SUB_TYPE_FADE: string = "fade";
    public static SUB_TYPE_SIZE: string = "size";
    public static SUB_TYPE_SPEED: string = "speed";

    public id: number;

    constructor(id: number, name: string, type: string, subType: string, inOut: string, initClasses: string[], animClasses: string[]) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.subType = subType;
        this.inOut = inOut;
        this.initClasses = initClasses;
        this.animClasses = animClasses;
    }
}