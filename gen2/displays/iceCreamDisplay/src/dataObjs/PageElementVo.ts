import { CardElement } from "../classes/Elements/CardElement";
import { PresController } from "../classes/PresController";
import { PresModel } from "../classes/PresModel";
import { AnimationVo } from "./AnimationVo";
import { ElementAnimationVo } from "./ElementAnimationVo";
import { ElementExtrasVo } from "./ElementExtrasVo";
import { TextStyleVo } from "./TextStyleVo";

export class PageElementVo {

    public static TYPE_IMAGE: string = "IMAGE";
    public static TYPE_TEXT: string = "TEXT";
    public static TYPE_BACK_IMAGE = "BACK_IMAGE";
    public static TYPE_CARD: string = "CARD";
    public static TYPE_LINE: string = "LINE";

    public static SUB_TYPE_NONE: string = "none";
    public static SUB_TYPE_GUTTER: string = "gutter";
    public static SUB_TYPE_POPUP_LINK: string = "popuplink";

    public static SUB_TYPE_GREEN_CARD: string = "greenCard";
    public static SUB_TYPE_WHITE_CARD: string = "whiteCard";

    public delFlag: boolean = false;
    //unique id to instance
    public id: string;
    //id link to element
    //used to know if we need to add or update 
    //an element

    public eid: string;
    public type: string;
    public subType: string;
    public classes: string[];
    public content: string[];
    public x: number;
    public y: number;
    public w: number;
    public h: number;

    public extras: ElementExtrasVo;
    public textStyle:number=-1;

    public animationIn: ElementAnimationVo = new ElementAnimationVo();
    public animationOut: ElementAnimationVo = new ElementAnimationVo();

    public static elementID: number = 0;
    private presModel: PresModel = PresModel.getInstance();

    private origins: string[];
    public initClasses: string[] = [];
    public animClasses:string[]=[];
    public exitClasses:string[]=[];


    public initClassName:string="";
    public standardClassName:string="";
    public exitClassName:string="";


    constructor(id: string = "", type: string = "", subType: string = "", classes: string[] = [], content: string[] = [], x: number = 0, y: number = 0, w: number = 0, h: number = 0) {
        this.id = id;
        this.type = type;
        this.subType = subType;
        this.classes = classes;
        this.content = content;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.eid = "instance0";

        this.origins = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
        this.extras = new ElementExtrasVo();

    }
    fromObj(obj: any) {
        PageElementVo.elementID++;
        this.id = "presElement" + PageElementVo.elementID.toString();

        this.type = obj.type;
        this.subType = obj.subType;
        this.classes = obj.classes;
        this.content = obj.content;

        this.textStyle=parseInt(obj.textStyle);

        this.eid = obj.eid;
        this.x = parseFloat(obj.x);
        this.y = parseFloat(obj.y);
        this.w = parseFloat(obj.w);
        this.h = parseFloat(obj.h) || 0;

        this.extras.fromObj(obj.extras);

        this.animationIn.fromObj(obj.animationIn);
        this.animationOut.fromObj(obj.animationOut);
        /*  console.log(obj.subtype);
         console.log(this); */

    }
    getInitalClasses() {
        this.getAnimClasses(this.animationIn.speed,"init");
        this.getAnimClasses(this.animationIn.move,"init");
        this.getAnimClasses(this.animationIn.fade,"init");
        this.getAnimClasses(this.animationIn.grow,"init");
        return this.initClasses;
    }
    getEffectClasses()
    {
      //  this.getAnimClasses(this.animationIn.speed,"anim");
        this.getAnimClasses(this.animationIn.move,"anim");
        this.getAnimClasses(this.animationIn.fade,"anim");
        this.getAnimClasses(this.animationIn.grow,"anim");
        return this.animClasses;
    }
    getExitClasses()
    {
        this.getAnimClasses(this.animationOut.speed,"exit");
        this.getAnimClasses(this.animationOut.move,"exit");
        this.getAnimClasses(this.animationOut.fade,"exit");
        this.getAnimClasses(this.animationOut.grow,"exit");
        return this.exitClasses;
    }
    private getAnimClasses(id: number,key:"init"|"anim"|"exit") {
        console.log("class id "+id.toString());

        if (id !== -1) {
            
            let animVo: AnimationVo | undefined = this.presModel.animationMap.get(id);

            if (animVo) {
                let className: string[] = animVo.initClasses;
                if (key==="anim" || key==="exit")
                {
                    className=animVo.animClasses;
                }
                
                for (let i: number = 0; i < className.length; i++) {

                    switch(key)
                    {
                        case "init":
                            this.initClasses.push(className[i]);
                            break;
                        case "anim":
                            this.animClasses.push(className[i]);
                            break;
                        case "exit":
                            this.exitClasses.push(className[i]);
                            break;
                    }   
                   
                    
                }
            }
        }

    }
    
    getInitCSS()
    {
        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;

        cssStyle.position = "absolute";
        
        if (this.animationIn.move===-1)
        {
           cssStyle.left = this.x.toString() + "%";
           cssStyle.top = this.y.toString() + "%";
        }
        if (this.w !== 0) {
            if (this.animationIn.grow===-1)
            {
                cssStyle.width = this.w.toString() + "%";
            }
            
        }

        //
        //
        //
        if (this.type === PageElementVo.TYPE_TEXT && this.textStyle===-1) {
            cssStyle.color = this.extras.fontColor;            
            cssStyle.fontFamily = this.extras.fontName;
        }

        if (this.type===PageElementVo.TYPE_TEXT)
        {
            cssStyle.fontSize = this.extras.fontSize.toString() + "px";
        }

        if (this.extras) {
            let transformString: string = "translate(" + this.origins[this.extras.orientation] + ")";


            if (this.extras.alpha) {
                if (this.animationIn.fade===-1)
                {
                 cssStyle.opacity = this.extras.alpha.toString() + "%";
                }
            }

            if (this.extras.rotation > 0) {
                let angle: number = ((this.extras.rotation) / 100) * 360;
                angle = Math.floor(angle);
                //////console.log(angle);
                transformString += " rotate(" + angle.toString() + "deg)";
            }
            if (this.extras.skewY !== 0) {
                let skewAngleY: number = ((this.extras.skewY) / 100) * 360;
                skewAngleY = Math.floor(skewAngleY);
                //////console.log(skewAngleY);
                transformString += " skewY(" + skewAngleY.toString() + "deg)";
            }
            if (this.extras.skewX !== 0) {
                let skewAngleX: number = ((this.extras.skewX) / 100) * 360;
                skewAngleX = Math.floor(skewAngleX);
                //////console.log(skewAngleX);
                transformString += " skewX(" + skewAngleX.toString() + "deg)";
            }
            if (this.extras.flipV === true) {
                transformString += " scaleY(-1)";
            }
            if (this.extras.flipH === true) {
                transformString += " scaleX(-1)";
            }

            if (this.extras.borderThick > 0) {
                cssStyle.border = "solid";
                cssStyle.borderWidth = this.extras.borderThick.toString();
                cssStyle.borderColor = this.extras.borderColor;
            }
            cssStyle.transform = transformString;

          // cssStyle.transitionProperty = "opacity";
           //cssStyle.transitionDuration = "10s";

        }
        console.log(cssStyle);

        return cssStyle;

    }
    getExitCSS()
    {
        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;

        cssStyle.position = "absolute";
        
        if (this.animationOut.move===-1)
        {
           cssStyle.left = this.x.toString() + "%";
           cssStyle.top = this.y.toString() + "%";
        }
        if (this.w !== 0) {
            if (this.animationOut.grow===-1)
            {
                cssStyle.width = this.w.toString() + "%";
            }
            
        }

        //
        //
        //
        if (this.type === PageElementVo.TYPE_TEXT && this.textStyle===-1) {
            cssStyle.color = this.extras.fontColor;            
            cssStyle.fontFamily = this.extras.fontName;
        }
        if (this.type===PageElementVo.TYPE_TEXT)
        {
            cssStyle.fontSize = this.extras.fontSize.toString() + "px";
        }
        if (this.extras) {
            let transformString: string = "translate(" + this.origins[this.extras.orientation] + ")";


            if (this.extras.alpha) {
                if (this.animationOut.fade===-1)
                {
                 cssStyle.opacity = this.extras.alpha.toString() + "%";
                }
            }

            if (this.extras.rotation > 0) {
                let angle: number = ((this.extras.rotation) / 100) * 360;
                angle = Math.floor(angle);
                //////console.log(angle);
                transformString += " rotate(" + angle.toString() + "deg)";
            }
            if (this.extras.skewY !== 0) {
                let skewAngleY: number = ((this.extras.skewY) / 100) * 360;
                skewAngleY = Math.floor(skewAngleY);
                //////console.log(skewAngleY);
                transformString += " skewY(" + skewAngleY.toString() + "deg)";
            }
            if (this.extras.skewX !== 0) {
                let skewAngleX: number = ((this.extras.skewX) / 100) * 360;
                skewAngleX = Math.floor(skewAngleX);
                //////console.log(skewAngleX);
                transformString += " skewX(" + skewAngleX.toString() + "deg)";
            }
            if (this.extras.flipV === true) {
                transformString += " scaleY(-1)";
            }
            if (this.extras.flipH === true) {
                transformString += " scaleX(-1)";
            }

            if (this.extras.borderThick > 0) {
                cssStyle.border = "solid";
                cssStyle.borderWidth = this.extras.borderThick.toString();
                cssStyle.borderColor = this.extras.borderColor;
            }
            cssStyle.transform = transformString;

          // cssStyle.transitionProperty = "opacity";
           //cssStyle.transitionDuration = "10s";

        }
        console.log(cssStyle);

        return cssStyle;

    }
    getCSS() {
        ////console.log(this);

        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;



        cssStyle.position = "absolute";
        cssStyle.left = this.x.toString() + "%";
        cssStyle.top = this.y.toString() + "%";



        if (this.w !== 0) {
            cssStyle.width = this.w.toString() + "%";
        }



        //
        //
        //
        if (this.type === PageElementVo.TYPE_TEXT && this.textStyle===-1) {
            cssStyle.color = this.extras.fontColor;           
            cssStyle.fontFamily = this.extras.fontName;
        }
        if (this.type===PageElementVo.TYPE_TEXT)
        {
            cssStyle.fontSize = this.extras.fontSize.toString() + "px";
        }

        
        if (this.extras) {
            let transformString: string = "translate(" + this.origins[this.extras.orientation] + ")";


            if (this.extras.alpha) {
                cssStyle.opacity = this.extras.alpha.toString() + "%";
            }

            if (this.extras.rotation > 0) {
                let angle: number = ((this.extras.rotation) / 100) * 360;
                angle = Math.floor(angle);
                //////console.log(angle);
                transformString += " rotate(" + angle.toString() + "deg)";
            }
            if (this.extras.skewY !== 0) {
                let skewAngleY: number = ((this.extras.skewY) / 100) * 360;
                skewAngleY = Math.floor(skewAngleY);
                //////console.log(skewAngleY);
                transformString += " skewY(" + skewAngleY.toString() + "deg)";
            }
            if (this.extras.skewX !== 0) {
                let skewAngleX: number = ((this.extras.skewX) / 100) * 360;
                skewAngleX = Math.floor(skewAngleX);
                //////console.log(skewAngleX);
                transformString += " skewX(" + skewAngleX.toString() + "deg)";
            }
            if (this.extras.flipV === true) {
                transformString += " scaleY(-1)";
            }
            if (this.extras.flipH === true) {
                transformString += " scaleX(-1)";
            }

            if (this.extras.borderThick > 0) {
                cssStyle.border = "solid";
                cssStyle.borderWidth = this.extras.borderThick.toString();
                cssStyle.borderColor = this.extras.borderColor;
            }
            cssStyle.transform = transformString;

          //  cssStyle.transitionProperty = "top, left, height, width, opacity";
          //  cssStyle.transitionDuration = "2s";


        }

        return cssStyle;
    }
    private addClasses(obj: any) {
        for (let i: number = 0; i < this.classes.length; i++) {
            obj.classList.add(this.classes[i]);
        }
    }
   
    getClasses() {
        let classArray: string[] = this.classes.slice();

        if (this.type === PageElementVo.TYPE_LINE) {
            return [];
        }

        if (this.type === PageElementVo.TYPE_CARD) {

            classArray.push("infoCard");
            switch (this.subType) {
                case PageElementVo.SUB_TYPE_GREEN_CARD:
                    classArray.push("green");
                    break;

                case PageElementVo.SUB_TYPE_WHITE_CARD:
                    classArray.push("white");
                    break;
            }
        }
        return classArray;
    }
    getHtml() {
        let ukey: string = "element" + this.id.toString();

        let style: any = this.getCSS();

        let classArray: string[] = this.getClasses();

        if (this.textStyle!==-1)
        {
            let textStyleVo:TextStyleVo | undefined=this.presModel.styleMap.get(this.textStyle);
            if (textStyleVo)
            {
                for (let i:number=0;i<textStyleVo.classes.length;i++)
                {
                    classArray.push(textStyleVo.classes[i]);
                }
            }
        }
        
        switch (this.type) {

            case PageElementVo.TYPE_IMAGE:

                const img: HTMLImageElement = document.createElement("img");
                img.id = this.id;
                img.src = this.presModel.getFullPath(this.content[0]);                
                this.classes=classArray;
                this.addClasses(img);
                return img;

            case PageElementVo.TYPE_TEXT:

                const text: HTMLElement = document.createElement("article");
                text.id = this.id;
                text.innerText = this.content[0];
                this.classes=classArray;
                this.addClasses(text);
                return text;

            case PageElementVo.TYPE_CARD:

                let card: CardElement = new CardElement(this, classArray);

                switch (this.subType) {
                    case PageElementVo.SUB_TYPE_GREEN_CARD:
                        return card.getCard1(this.id);
                    case PageElementVo.SUB_TYPE_WHITE_CARD:
                        return card.getCard2(this.id);
                }
        }       

        let def: HTMLDivElement = document.createElement("div");
        def.innerText = "Not Found " + this.type + "-" + this.subType;
        return def;
    }

}