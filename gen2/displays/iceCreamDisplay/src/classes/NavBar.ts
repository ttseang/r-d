import { IScreen } from "svggame";

import { BottomBar } from "./BottomBar";

import { HtmlObj } from "./HtmlObj";
import { LinkUtil } from "./util/LinkUtil";


export class NavBar extends HtmlObj {
    private btnUp: HtmlObj = new HtmlObj("btnNavUp");
    private btnDown: HtmlObj = new HtmlObj("btnNavDown");
    public bottomBar: BottomBar | null = null;
    private btnAuto: HtmlObj = new HtmlObj("btnAuto");
    private btnRead: HtmlObj = new HtmlObj("btnRead");
    private btnListen: HtmlObj = new HtmlObj("btnListen");

   
    private screen: IScreen;

    constructor(screen: IScreen) {
        super("topBar");

        this.screen = screen;

        this.btnDown.visible = false;
        if (this.btnUp.el) {
            this.btnUp.el.onclick = () => {
                this.closeBar();
            }
        }
        if (this.btnDown.el) {
            this.btnDown.el.onclick = () => {
                this.openBar();
            }
        }
        LinkUtil.makeLink("btnAuto", this.doAction.bind(this), "doAuto", "");
        LinkUtil.makeLink("btnListen", this.doAction.bind(this), "setMode", "listen");
        LinkUtil.makeLink("btnRead", this.doAction.bind(this), "setMode", "read");
    }
    public closeBar() {
        // this.y=-this.screen.gh*.05;
        // this.el?.setAttribute("top","-20%");
        if (this.el) {
            this.el.style.top = "-15vh";
        }

        this.btnUp.visible = false;
        this.btnDown.visible = true;
        if (this.bottomBar) {
            this.bottomBar.close();
        }

    }
    private openBar() {
        //  this.y=-this.screen.gh*.02;
       
        if (this.el) {
            this.el.style.top = "-2vh";
        }
        this.btnUp.visible = true;
        this.btnDown.visible = false;
        if (this.bottomBar) {
            this.bottomBar.open();
        }
    }
    private doAction(action: string, param: string) {

        switch (action) {
            case "doAuto":
              //  this.closeBar();
                //  this.mc.doAutoPlay();
                break;

            case "setMode":
                // this.bm.playMode=param;
                break;
        }

    }
}