import { AnimationVo } from "../../dataObjs/AnimationVo";
import { PresModel } from "../PresModel";

export class AnimationMapLoader
{
    private callback:Function;
    private ms:PresModel=PresModel.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    load(path:string)
    {
       // console.log("load map");
        fetch(path)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any)
    {
       // console.log(data);
        let animations:any[]=data.animations;
    
        for (let i:number=0;i<animations.length;i++)
        {
            let anim:any=animations[i];
         //   console.log(anim);
            
            let animationVo:AnimationVo=new AnimationVo(parseInt(anim.id),anim.name,anim.elementType,anim.animationType,"",anim.initClass,anim.animClass);
          //  console.log(animationVo);
            this.ms.animationMap.set(animationVo.id,animationVo);
        }
        this.callback();
    }
}