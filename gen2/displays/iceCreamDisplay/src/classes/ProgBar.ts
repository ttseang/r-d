import { HtmlObj } from "./HtmlObj"


export class ProgBar extends HtmlObj {
    public knob: HtmlObj = new HtmlObj("progKnob");

    private isDragging: boolean = false;
   // private gm: BookModel = BookModel.getInstance();
    public prevImage: HTMLImageElement;

    public callback: Function = () => { };

   

    private stepIndex: number = 0;

    constructor() {

        super("progBar");

        this.prevImage = document.getElementById("prevFrame") as HTMLImageElement;

        if (this.knob.el) {
            this.knob.el.onmousedown = () => {
                this.isDragging = true;
            }
            document.onmouseup = () => {
                if (this.isDragging == true) {

                    
                }
                this.isDragging = false;
            }
            document.onmousemove = (e: MouseEvent) => {
                if (this.isDragging) {
                    if (this.el) {
                        let tx: number = this.el.getBoundingClientRect().x;
                        let ww: number = this.el.getBoundingClientRect().width;
                        let xx: number = ((e.x - tx) / ww) * 100;

                        if (xx < 0) {
                            xx = 0;
                        }
                        if (xx > 100) {
                            xx = 100;
                        }

                        this.knob.x = xx * 0.95;
                        let per: number = Math.floor(xx);
                        this.callback(per);
                    }
                }
            }
        }
    }
}