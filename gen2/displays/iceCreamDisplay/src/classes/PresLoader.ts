import { PresentationVo } from "../dataObjs/PresentationVo";


export class PresLoader
{
    private callback:Function;

    constructor(callback:Function=()=>{})
    {
        this.callback=callback;
    }
    loadFile(file:string)
    {
        fetch(file)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    public loadFromString(content:string)
    {
        let obj:any=JSON.parse(content);
        this.process(obj);
    }
    private process(data:any)
    {
       // ////console.log(data);

        let pageData:any=data;
        
        let presentation:PresentationVo=new PresentationVo([],300,300);
        presentation.fromObj(pageData);

       // ////console.log(bookVo);
        this.callback(presentation);
    }
}