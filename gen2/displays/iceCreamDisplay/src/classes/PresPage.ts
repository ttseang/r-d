import { IScreen, SoundObj, SVGAnimation } from "svggame";
import { DisplayElementVo } from "../dataObjs/DisplayElementVo";
import { PageElementVo } from "../dataObjs/PageElementVo";
import { StepVo } from "../dataObjs/StepVo";
import { PresentationVo } from "../dataObjs/PresentationVo";
import { ZoomVo } from "../dataObjs/ZoomVo";

import { PresController } from "./PresController";
import { PresModel } from "./PresModel";

export class PresPage {
    private screen: IScreen;
    private audioPlayer:SoundObj | null=null;
    private backgroundPlayer:SoundObj | null=null;
    private lastBackgroundSound:string="";

   /*  private initClass: string = "";
    private standardClass:string="";
    private exitClass:string=""; */

    private step: StepVo = new StepVo(0, []);

    //a map of all the elements currently on the page
    private currentElements: Map<string, DisplayElementVo> = new Map<string, DisplayElementVo>();

    //the svg element addArea 
    public svg: SVGSVGElement | null | undefined;

    //the foreignObject id=ptblank child of addArea where all the dynamic content is placed
    public mainParent: HTMLElement | null = null;

    private pm: PresModel = PresModel.getInstance();
    private pc: PresController = PresController.getInstance();

    //record the old zoom coodinates to use in the zoom animation
    private oldZoom: string = "";

    constructor(screen: IScreen) {
        this.screen = screen;

        this.mainParent = document.getElementById("ptblank");
        this.svg = (document.getElementById("addArea") as unknown) as SVGSVGElement;
        //
        //
        //bind the step functions to the main controller
        this.pc.nextStep = this.nextStep.bind(this);
        this.pc.prevStep = this.prevStep.bind(this);
        this.pc.doEffects = this.doEffects.bind(this);
        this.pc.doExitEffects=this.doExitEffects.bind(this);

    }
    setData(zoomPres: PresentationVo) {


        let w: number = zoomPres.pageW;
        let h: number = zoomPres.pageH;

        let windowWidth: number = Math.floor(window.innerWidth * .99);
        let ratio: number = windowWidth / w;
        let fullH: number = Math.floor(h * ratio);
        let fullW: number = w * ratio;

        console.log("r=" + ratio);

        let w2: number = w * ratio;

        if (this.mainParent) {
            this.mainParent.setAttribute("width", fullW.toString());
            this.mainParent.setAttribute("height", fullH.toString());
            // this.mainParent.setAttribute("width", w.toString());
          //  this.mainParent.setAttribute("height", h.toString());
            
        }


        this.pm.pageH = h;
        this.pm.pageW = w;


        this.step = zoomPres.steps[0];
        // console.log(this.step);
        //this.buildFrame();

        
       
    }
    //TODO hook to buttons
    prevStep() {
        if (this.pm.currentStep > 0) {
            this.pc.hideButtons();
            this.pm.currentStep--;
            this.pageChanged();
        }
    }

    nextStep() {
        this.doExitEffects();
       
        if (this.pm.currentStep < this.pm.presentation.steps.length - 1) {
            this.pc.hideButtons();     
             setTimeout(() => {
                this.pm.currentStep++;
                this.pageChanged();     
            }, 2000);       

        }
    }
    private pageChanged() {
        this.step = this.pm.presentation.steps[this.pm.currentStep];
        this.buildFrame();        
    }
    private playAudio()
    {
        let path:string=this.step.audio;

        if (path==="")
        {
            this.pc.showButtons();
            return;
        }

        let fullPath:string=this.pm.getAudioPath(path);

        if (this.audioPlayer)
        {
            this.audioPlayer.stop();
        }

        this.audioPlayer=new SoundObj(fullPath,false);
        this.audioPlayer.play();

        console.log("play "+path);

        this.audioPlayer.sound.addEventListener("ended",()=>{
            this.pc.showButtons();
        })
    }
    private playBackgroundMusic()
    {
        let backgroundAudio:string=this.step.backgroundAudio;
        console.log(this.step);
        console.log(backgroundAudio);
        let fullPath:string=this.pm.getBackgroundAudioPath(backgroundAudio);

        if (this.lastBackgroundSound!==backgroundAudio)
        {
            this.lastBackgroundSound=backgroundAudio;
            if (this.backgroundPlayer)
            {
                this.backgroundPlayer.stop();
            }
            this.backgroundPlayer=new SoundObj(fullPath,true);
            this.backgroundPlayer.sound.volume=0.1;
            this.backgroundPlayer.play();
        }
    }

    deleteAllElements() {
        this.currentElements.forEach((el: DisplayElementVo) => {
           
                if (el.displayElement && this.mainParent) {
                    this.mainParent.removeChild(el.displayElement);
                    this.currentElements.delete(el.eid);
                }
            
        })
    }
    buildFrame() {

        this.deleteAllElements();

        this.currentElements.forEach((el: DisplayElementVo) => { el.delFlag = true })

        let elements: PageElementVo[] = this.step.elements;
        //console.log(elements);
        for (let i: number = 0; i < elements.length; i++) {
            let element: PageElementVo = elements[i];
            if (this.mainParent) {

                /**
                 * does this element already exsist?
                 * if not add it
                 * if so update position and content
                 */

                if (!this.currentElements.has(element.eid)) {

                    /**
                     *create the HTMLElement from the data obect 
                     */
                    let displayElement: HTMLElement | SVGAElement = element.getHtml();

                    /**
                     *add the data object to the current element map 
                     */
                    this.currentElements.set(element.eid, new DisplayElementVo(element.eid, displayElement));

                    /**
                     * add the display object to the document
                     */
                    this.mainParent.append(displayElement);
                    // this.svg?.append(displayElement);

                    let cssStyle: CSSStyleDeclaration = element.getInitCSS();
                    element.initClassName = this.pm.addStyleClass(element, cssStyle, "init");

                    let cssClasses: string[] = element.getInitalClasses();
                    console.log(cssClasses);

                    for (let k: number = 0; k < cssClasses.length; k++) {
                        // console.log(cssClasses[k]);
                        displayElement.classList.add(cssClasses[k]);
                    }
                   

                    displayElement.classList.add(element.initClassName);
                }


                else {

                    let displayElementVo: DisplayElementVo | undefined = this.currentElements.get(element.eid);

                    console.log(displayElementVo);
                    if (displayElementVo) {

                        displayElementVo.delFlag = false;

                        let css: CSSStyleDeclaration = element.getCSS();
                        displayElementVo.displayElement.setAttribute("style", css.cssText);
                        // console.log("update " + element.eid + " to " + css.left.toString() + " " + css.top.toString());

                    }
                }
            }
        }
        setTimeout(() => {
            this.doEffects();
        }, 200);
        //  this.deleteUnneeded();  0   

        this.playAudio();
        this.playBackgroundMusic();
    }

    doEffects() {
        let elements: PageElementVo[] = this.step.elements;
        //console.log(elements);
        for (let i: number = 0; i < elements.length; i++) {
            let element: PageElementVo = elements[i];
            if (this.mainParent) {

                let eid: string = element.eid;
                if (this.currentElements.has(eid)) {
                    let displayElementVo: DisplayElementVo | undefined = this.currentElements.get(element.eid);

                    if (displayElementVo) {
                        let cssStyle: CSSStyleDeclaration = element.getCSS();
                        element.standardClassName= this.pm.addStyleClass(element, cssStyle, "anim");

                        displayElementVo.displayElement.classList.add(element.standardClassName);

                        let effectClasses: string[] = element.getEffectClasses();
                        console.log(effectClasses);

                        for (let k: number = 0; k < effectClasses.length; k++) {
                            // console.log(effectClasses[k]);
                            displayElementVo.displayElement.classList.add(effectClasses[k]);
                        }
                        console.log(element);
                        console.log(element.initClassName);
                        displayElementVo.displayElement.classList.remove(element.initClassName);
                    }
                }
            }
        }
    }
    doExitEffects() {
        let elements: PageElementVo[] = this.step.elements;
        //console.log(elements);
        for (let i: number = 0; i < elements.length; i++) {
            let element: PageElementVo = elements[i];
            if (this.mainParent) {

                let eid: string = element.eid;
                if (this.currentElements.has(eid)) {
                    let displayElementVo: DisplayElementVo | undefined = this.currentElements.get(element.eid);

                    if (displayElementVo) {
                       /*  let cssStyle: CSSStyleDeclaration = element.getCSS();
                        let myClass: string = this.pm.addStyleClass(element, cssStyle, "anim");

                        displayElementVo.displayElement.classList.add(myClass); */


                        let cssStyle: CSSStyleDeclaration = element.getExitCSS();
                        element.exitClassName= this.pm.addStyleClass(element, cssStyle, "exit");
                        displayElementVo.displayElement.classList.add(element.exitClassName);

                        let exitClasses: string[] = element.getExitClasses();
                        console.log("exit classes");
                        console.log(exitClasses);

                        let oldClasses:string[]=element.initClasses;
                        console.log(oldClasses);

                        for (let j:number=0;j<oldClasses.length;j++)
                        {
                            displayElementVo.displayElement.classList.remove(oldClasses[j]);
                        }

                        for (let k: number = 0; k < exitClasses.length; k++) {
                             console.log(exitClasses[k]);
                            displayElementVo.displayElement.classList.add(exitClasses[k]);
                        }                      

                        console.log(element.standardClassName);
                        console.log(element.initClassName);

                        displayElementVo.displayElement.classList.remove(element.standardClassName);
                    }
                }
            }
        }
    }
}