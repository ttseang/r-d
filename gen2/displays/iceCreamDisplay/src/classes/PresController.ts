export class PresController
{
    private static instance:PresController | null=null;

    public nextStep:Function=()=>{};
    public prevStep:Function=()=>{};
    public doEffects:Function=()=>{};
    public doExitEffects:Function=()=>{};
    public showButtons:Function=()=>{};
    public hideButtons:Function=()=>{};
    
    constructor()
    {
        (window as any).zc=this;
    }
    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new PresController();
        }
        return this.instance;
    }
}