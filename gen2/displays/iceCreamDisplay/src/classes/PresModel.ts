import { AnimationVo } from "../dataObjs/AnimationVo";
import { PageElementVo } from "../dataObjs/PageElementVo";
import { PresentationVo } from "../dataObjs/PresentationVo";
import { TextStyleVo } from "../dataObjs/TextStyleVo";

export class PresModel
{
    public styleDiv:HTMLStyleElement | null=null;

    private static instance:PresModel | null=null;
    public pageH:number=0;
    public pageW:number=0;

    public presentation:PresentationVo=new PresentationVo();

    public currentStep:number=-1;

    public animationMap:Map<number,AnimationVo>=new Map<number,AnimationVo>();
    public styleMap:Map<number,TextStyleVo>=new Map<number,TextStyleVo>();

    constructor()
    {
        this.styleDiv=document.getElementById("elementStyles") as HTMLStyleElement;

        (window as any).zm=this;
    }
    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new PresModel();
        }
        return this.instance;
    }
    getFullPath(path:string)
    {
        return "https://ttv5.s3.amazonaws.com/william/images/bookimages/"+path;
    }
    
    getAudioPath(path:string)
    {
        return "https://ttv5.s3.amazonaws.com/william/audio/"+path;        
    }
    getBackgroundAudioPath(path:string)
    {
        return "https://ttv5.s3.amazonaws.com/william/audio/backgroundMusic/"+path;
    }
    addStyle(elementVo:PageElementVo,style:CSSStyleDeclaration)
    {        
        if (this.styleDiv)
        {            
           // //console.log(elementVo.id);
           // //console.log(style.style.cssText);

            let cRule="#"+elementVo.id+"{"+style.cssText+"}";
            ////console.log(cRule);
            this.styleDiv.append(cRule);
        }
    }
    addStyleClass(elementVo:PageElementVo,style:CSSStyleDeclaration,suffix:string)
    {
        //console.log(style)

        let cName:string=elementVo.id+suffix;
        console.log(cName);
        
        let cRule="."+cName+"{"+style.cssText+"}";
        if (this.styleDiv)
        {            
           // //console.log(elementVo.id);
           // //console.log(style.style.cssText);

           
            ////console.log(cRule);
            this.styleDiv.append(cRule);
        }
        else
        {
            console.log("STYLE DIV MISSING");
        }
        return cName;
    }
}