import { NotationDisplay } from "./notationDisplay";

window.onload=function()
{
    let notationDisplay: NotationDisplay = new NotationDisplay(12345.6789);
    notationDisplay.injectCss();

    let notation: string = notationDisplay.getHtml();
    console.log(notation);
    let div:HTMLDivElement | null=document.body.querySelector(".testDiv");
    if (div)
    {
        div.innerHTML = notation;
    }
    
}