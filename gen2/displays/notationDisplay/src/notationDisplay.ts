export class NotationDisplay
{
    private numValue: number;
    private wholeNumbers: string[]=[];
    private decimalNumbers: string[]=[];
    private vw:number=1;
    private digitCount: number = 0;

    constructor(numValue: number)
    {
        this.numValue = numValue;

        let numberArray: string[] = this.numValue.toString().split(".");
       
        let digitArray: string[] = this.numValue.toString().split("");
        let digitCount: number = digitArray.length;
        let decPos: number = digitArray.indexOf(".");
        //if not a whole number
        if (decPos !== -1) {
            digitCount--;
        }
        this.digitCount = digitCount;

        //get the vw value from css
        let vwString: string = getComputedStyle(document.documentElement).getPropertyValue('--vw');
        this.vw = parseFloat(vwString);

        //get the whole number
        let wholeNumber: string = parseInt(numberArray[0]).toLocaleString();
        this.wholeNumbers = wholeNumber.split("");

        //get the decimal number
        let decimalNumber: string = "";

        if (numberArray.length > 1)
        {
            decimalNumber = numberArray[1];
            this.decimalNumbers = decimalNumber.split("");
        }
        
    }
    getDigits(): string
    {
        let digits: string = "";

         //combine the whole number and decimal number
        let allDigits: string[]=this.wholeNumbers.slice();
        allDigits.push(".");
        allDigits=allDigits.concat(this.decimalNumbers);
     

         //loop through the digits in reverse order
        for (let i: number = allDigits.length - 1; i >= 0; i--)
        {
            let digit: string = allDigits[i];
           
            //get next digit
            let nextDigit: string = allDigits[i + 1];


            //if the next digit is a decimal point or comma then add it to the current digit
            switch (nextDigit) {
                case ".":
                    digit = digit + ".";
                    break;
                case ",":
                    digit = digit + ",";
                    break;

            }

             //if the digit is not a decimal point or comma then add it to the list
            //otherwise skip it
            if (digit !== "." && digit !== ",") {
                digits="<li>"+digit+"</li>"+digits;
            }
        }
       
        return digits;
    }
    injectCss()
    {
        let css: string =  ".notationGrid{display:grid;grid-template-columns:repeat(4,calc(var(--vw) * 1.2));grid-template-rows:calc(var(--vw) * 2) calc(var(--vw) * 8);grid-gap:.5rem;list-style:none}.notationGrid .word{transform-origin:center right;rotate:-55deg;text-align:right;position:relative;left:calc(var(--vw) * -15.5);top:calc(var(--vw) * -3);width:calc(var(--vw)*18);transition-property:rotate;transition-duration:.5s;transition-timing-function:ease-in-out}.notationGrid .word .decimalPoint{font-weight:bold}.notationGrid .word .comma{font-weight:bold}";
        //place the css in the head of the document
        
        let head: HTMLHeadElement | null = document.head;
        if (head)
        {
            let style: HTMLStyleElement = document.createElement("style");
            style.innerHTML = css;
            head.appendChild(style);
        }
    }
    getPlaceNames(): string
    {
        const placeNames: string = "ones,tens,hundreds,thousands,ten thousands,hundred thousands, millions,ten millions,hundred millions, billions,ten billions,hundred billions, trillions,ten trillions,hundred trillions, quadrillions,ten quadrillions,hundred quadrillions, quintillions,ten quintillions,hundred quintillions, sextillions,ten sextillions,hundred sextillions, septillions,ten septillions,hundred septillions, octillions,ten octillions,hundred octillions, nonillions,ten nonillions,hundred nonillions, decillions,ten decillions,hundred decillions, undecillions,duodecillions,tredecillions,quattuordecillions,quindecillions,sexdecillions,septendecillions,octodecillions,novemdecillions,vigintillions";
        const decimalPlaceNames: string = "tenths,hundredths,thousandths,ten thousandths,hundred thousandths, millionths,ten millionths,hundred millionths, billionths,ten billionths,hundred billionths, trillionths,ten trillionths,hundred trillionths, quadrillionths,ten quadrillionths,hundred quadrillionths, quintillionths,ten quintillionths,hundred quintillionths, sextillionths,ten sextillionths,hundred sextillionths, septillionths,ten septillionths,hundred septillionths, octillionths,ten octillionths,hundred octillionths, nonillionths,ten nonillionths,hundred nonillionths, decillionths,ten decillionths,hundred decillionths, undecillionths,duodecillionths,tredecillionths,quattuordecillionths,quindecillionths,sexdecillionths,septendecillionths,octodecillionths,novemdecillionths,vigintillionths";
    
        let placeNamesArray: string[] = placeNames.split(",");
        let decimalPlaceNamesArray: string[] = decimalPlaceNames.split(",");

        let placeString: string = "";

        let wholeNumberDigits: string[] = this.wholeNumbers.slice();
        let decimalPartDigits: string[] = this.decimalNumbers.slice();

        wholeNumberDigits.reverse();
        decimalPartDigits.reverse();

        let wholePlaces:string="";
        let decimalPlaces:string="";

        //if the decimal part is not zero
        if (decimalPartDigits[0] !== "0" || decimalPartDigits.length > 1) {

            //get the place names for the decimal part
            for (let i = 0; i < decimalPartDigits.length; i++) {
                decimalPlaces += "<li class='word'>" + decimalPlaceNamesArray[i] + "</li>";
            }
        }
        //get the place names for the whole number
        for (let i = 0; i < wholeNumberDigits.length-1; i++) {           
            wholePlaces = "<li class='word'>" + placeNamesArray[i] + "</li>"+wholePlaces;
        }
        placeString += wholePlaces;
        placeString += decimalPlaces;
        return placeString;
    }
    getHtml(): string
    {
        //grid-template-columns: repeat(var(--dcols), calc(var(--vw) * 1.2));
        //set the number of columns in the grid
        //make a css object for the number of columns
        let digitCountString: string = this.digitCount.toString();
        let styleString: string = "grid-template-columns:repeat("+digitCountString+", calc(var(--vw) * 1.2))";
        let html: string = "<ul class='notationGrid' style='"+styleString+"'>";       
        html+=this.getDigits();
        html+=this.getPlaceNames();
        html += "</ul>";
        return html;
    }
}