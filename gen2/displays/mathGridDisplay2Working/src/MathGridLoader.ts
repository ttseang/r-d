export class MathGridLoader {
    constructor() {

    }
    public getFileContent(lti: string, callback: Function) {
        //https://tthq.me/api/pr/getequation/1
        //https://tthq.me/api/pr/getequation/X.TEST.01
        let url: string = "https://tthq.me/api/pr/getequation/" + lti;
        fetch(url, {
            method: "post"
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        console.log("math grid loader", json);
                        callback(json);
                    });
                }
            })
    }
}