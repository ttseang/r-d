import { CellType, TCellVo } from "./TCellVo";


//gridData TCellVo[][]
export class TGridVo {
    public gridData: TCellVo[][];
    public rowCount: number;
    public colCount: number;
    public name: string = "step name";

    constructor(gridData: TCellVo[][]) {
        //gridData is a 2d array of TCellVo
        //each row is an array of TCellVo
        //columns are in the first dimension
        //rows are in the second dimension
        this.gridData = gridData;
        if (gridData.length===0)
        {
            this.rowCount=0;
            this.colCount=0;
        }
        else
        {
            this.rowCount = gridData[0].length;
            this.colCount = gridData.length;
        }
    }
   
    loadFromData(data:any)
    {

        let gridData:TCellVo[][]=[];
        for (let i:number=0;i<data.length;i++)
        {
            gridData[i]=[];
            for (let j:number=0;j<data[i].length;j++)
            {
                let cellData:any=data[i][j];
                let text:string[]=cellData.text;
                let cellClasses:string[]=cellData.cellClasses;
                let textClasses:string[][]=cellData.textClasses;
                let rowSize:number=cellData.rowSize;
                let colSize:number=cellData.colSize;
                let type:string=cellData.type;
                let adjustX:number[]=cellData.adjustX;
                let adjustY:number[]=cellData.adjustY;
                let cellType:CellType=TCellVo.stringToConstants(type);
                let cell:TCellVo=new TCellVo(text,colSize,rowSize,textClasses,cellClasses,cellType);
                cell.adjustX=adjustX;
                cell.adjustY=adjustY;
               
                gridData[i][j]=cell;
            }
        }
        
        this.gridData=gridData;
    }
   
}