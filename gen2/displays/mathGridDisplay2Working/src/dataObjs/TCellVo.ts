export enum CellType {
    Whole = "whole",
    Fraction = "fraction",
    SquareRoot = "sqrt",
    Power = "power",
    Subscript = "subscript",
    SubSup = "subSup",
    Divide = "divide",
    Divide2Col = "divide2Col",
    Column = "column",
    Row = "row",
    Carry = "carry",
    Borrow = "borrow",
    Grid = "grid"

}

export class TCellVo {
    public text: string[];
    public rowSize: number;
    public colSize: number;
    public textClasses: string[][];
    public cellClasses: string[] = [];
    public row: number = 0;
    public col: number = 0;
    public selected: boolean = false;
    public dragOver: boolean = false;
    public selectedIndex: number = 0;
    public selectedIndexes: number[] = [];
    public type: CellType;
    public adjustX: number[] = []
    public adjustY: number[] = [];

    constructor(text: string[], colSize: number, rowSize: number, textClasses: string[][] = [], cellClasses: string[] = [], type: CellType = CellType.Whole) {
        this.text = text;
        this.colSize = colSize;
        this.rowSize = rowSize;
        this.textClasses = textClasses;
        this.cellClasses = cellClasses;
        this.type = type;
    }
    public static stringToConstants(str: string): CellType {

        let types: CellType[] = [CellType.Whole, CellType.Column, CellType.Row, CellType.Fraction, CellType.Power, CellType.SquareRoot, CellType.Carry, CellType.Borrow];
        for (let i: number = 0; i < types.length; i++) {
            if (str === types[i]) {
                return types[i];
            }
        }
        return CellType.Whole;
    }
    public getHtml(): string {
        let text: string = "";

        let size: string = this.colSize.toString() + "_" + this.rowSize.toString();
        let baseClass: string = "cell_" + size;

        for (let i: number = 0; i < this.text.length; i++) {

            
            let adjustX: number = 0;
            let adjustY: number = 0;

            if (this.adjustX.length > i) {
                adjustX = this.adjustX[i];
                if (isNaN(adjustX) || adjustX === undefined || adjustX === null) {
                    adjustX = 0;
                }
            }
            if (this.adjustY.length > i) {
                adjustY = this.adjustY[i];
                if (isNaN(adjustY) || adjustY === undefined || adjustY === null) {
                    adjustY = 0;
                }
            }

            //make a relative position style for the cell
            //use --cellHeight and --cellWidth to set the size of the cell 
            //multiply by the adjustX and adjustY to set the position
            //make css object

            /* let style: string = "position:relative;";
        
            style+="left:calc(var(--cellWidth) * " + adjustX.toString() + ");";
            style+="top:calc(var(--cellHeight) * " + adjustY.toString() + ");"; */


            let text2: string = this.text[i];
            if (text2 === "") {
                text2 = " ";
            }

            
            //   let classCopy: string[] = this.cellClasses.slice();
           

            let classes: string[] = this.textClasses[i] || [];
            
            let cellClass=baseClass+"_"+i.toString();
            
            classes.push(cellClass);

            if (classes) {
                text += "<span class='" + classes.join(" ") + "'>" + text2 + "</span>";
            }
            else {
                text += " <span>" + text2 + "</span>";
            }

        }
        this.cellClasses.unshift("cell");

       
        this.cellClasses.unshift(baseClass);
        if (this.cellClasses.length > 0) {
            return "<div class='" + this.cellClasses.join(" ") + "'>" + text + "</div>";
        }
        return text;
    }
}