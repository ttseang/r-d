import { MathGrid } from "./MathGrid";

window.onload = function () {
    //TT.RD.SAMPLES
    //TT.RD.SIMPLE
    //MathGrid.injectCSS();
    let mathGrid: MathGrid = new MathGrid("TT.RD.SIMPLE", document.getElementsByClassName("mathGrid")[0] as HTMLDivElement, () => {
        console.log("MathGrid loaded");
        mathGrid.showStep(0);
    });
}