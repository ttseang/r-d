
import { BaseScreen, HtmlObj, IScreen, SvgObj, TextObj } from "svggame";
import { Book } from "../classes/Book";
import { BookPage } from "../classes/BookPage";
import { TextStyleMapLoader } from "../classes/TextStyleMapLoader";
import { BookVo } from "../dataObjs/book/BookVo";
import { BookLoader } from "../util/BookLoader";
import {NavBar} from "../classes/NavBar";
import { BottomBar } from "../classes/BottomBar";

export class ScreenMain extends BaseScreen implements IScreen {
    
    private bookLoader: BookLoader;
    private compilePreview:boolean=false;
    private navBar:NavBar;
    private bottomBar:BottomBar;
    private book:Book | null=null;
    constructor() {
        super("ScreenMain");
        //this.setFullSize();
        this.bookLoader = new BookLoader(this.bookDataLoaded.bind(this));
        this.navBar=new NavBar(this);
        this.bottomBar=new BottomBar(this,this.contentPercentChanged.bind(this));
        this.navBar.bottomBar=this.bottomBar;
    }
   /*  setFullSize()
    {
        let styleDiv:HTMLStyleElement=document.getElementById("elementStyles") as HTMLStyleElement;
        let ww:number=window.innerWidth;
        let hh:number=window.innerHeight;

        let ratio:number=ww/hh;

        let fullStyle:HTMLStyleElement=document.createElement("style");
        fullStyle.style.width="100%";
        fullStyle.style.aspectRatio=ratio.toString();
        
        let cRule=".full2{"+fullStyle.style.cssText+"}";
        ////console.log(cRule);
        styleDiv.append(cRule);
    } */
    create() {
        super.create();

        (window as any).scene = this;
        
        this.loadTextStyles();

        this.navBar.closeBar();
    }
    loadTextStyles()
    {
        let cssLoader:TextStyleMapLoader=new TextStyleMapLoader(this.loadPage.bind(this));
        cssLoader.load("./assets/textStyleMap.json");
    }
    loadPage()
    {        
        if (this.compilePreview === true) {
            /* let test:SvgObj=new SvgObj(this,"page"); */

            let bookContentDiv: HTMLDivElement | null | undefined = window.parent.document.getElementById("bookData") as HTMLDivElement;

            if (bookContentDiv) {
                let content: string = bookContentDiv.innerText;
                // //console.log(content);
                this.bookLoader.loadFromString(content);
            }

        }
        else {
            this.bookLoader.loadBook("./assets/book4.json");
        }
    }
    contentPercentChanged(per:number)
    {
      //  console.log(per);
    }
    bookDataLoaded(bookVo: BookVo) {
        this.book = new Book(this, bookVo);

        if (bookVo.pageHeight===300)
        {
            this.book.el?.setAttribute("y","50");
        }
       this.book.buildPages();

        //let test:SvgObj=new SvgObj(this,"ball");
    }

}