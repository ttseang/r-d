import { BookVo } from "../dataObjs/book/BookVo";
import { PageVo } from "../dataObjs/book/PageVo";

export class BookLoader
{
    private callback:Function;

    constructor(callback:Function=()=>{})
    {
        this.callback=callback;
    }
    loadBook(file:string)
    {
        fetch(file)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    public loadFromString(content:string)
    {
        let obj:any=JSON.parse(content);
        this.process(obj);
    }
    private process(data:any)
    {
       // //console.log(data);

        let pageData:any=data;
        
        let bookVo:BookVo=new BookVo();
        bookVo.fromObj(pageData);

       // //console.log(bookVo);
        this.callback(bookVo);
    }
}