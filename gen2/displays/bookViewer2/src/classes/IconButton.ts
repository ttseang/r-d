import { IScreen, SvgObj } from "svggame";

export class IconButton extends SvgObj
{
  
    private scene:IScreen;
    private callback:Function;
    public action:string="";
    public params:string="";

    constructor(scene:IScreen,key:string,action:string,params:string,callback:Function)
    {
        super(scene,key);
        this.scene=scene;
        this.callback=callback;
        this.action=action;
        this.params=params;

        if (this.el)
        {
            this.el.onclick=()=>{
                this.callback(this.action,this.params);
            }
        }
        
    }

   
}