import { TextStyleVo } from "../dataObjs/TextStyleVo";
import { BookModel } from "./BookModel";


export class TextStyleMapLoader
{
    private callback:Function;
    private ms:BookModel=BookModel.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    load(path:string)
    {
        //console.log("load map");
        fetch(path)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any)
    {
        //console.log(data);
        let textStyles:any[]=data.textStyles;

        for (let i:number=0;i<textStyles.length;i++)
        {
            let textStyle:any=textStyles[i];
            let textStyleVo:TextStyleVo=new TextStyleVo(parseInt(textStyle.id),textStyle.name,textStyle.type,textStyle.classes);
           // //console.log(textStyleVo);
        
           // this.ms.styleUtil.textStyles.push(textStyleVo);
            this.ms.styleMap.set(textStyleVo.id,textStyleVo);
           // this.ms.animationMap.set(animationVo.id,animationVo); 
        }
        this.callback();
    }
}