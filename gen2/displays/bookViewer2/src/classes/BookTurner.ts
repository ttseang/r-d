import { Book } from "./Book";
//import { Sheet } from "./Sheet";
import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { Sheet } from "./Sheet";

export class BookTurner {
  public isTurning: boolean = false;
  public rotation: number = 0;
  public target: number = 0;
  //  public sheet: Sheet | null = null;
  private book: Book;

  private back: Element | null | undefined;
  private backG: Element | null | undefined;
  private activePages: Element | null | undefined;
  private front: Element | null | undefined;
  private frontG: Element | null | undefined;
  private shadow: Element | null | undefined;
  public sheet: Sheet | null = null;

  constructor(book: Book) {
    this.book = book;
    this.shadow = book.svg?.querySelector('#pages>g.pgshadow');
    this.activePages = book.svg?.querySelector('#pages>g.activepg');
    this.front = this.activePages?.querySelector('g.right');
    this.frontG = this.front?.querySelector('g');
    this.back = this.activePages?.querySelector('g.left');
    this.backG = this.back?.querySelector('g');
  }

  load(sheet: Sheet, direction: number) {

    const newDirection = direction < 0 ? -1 : 1;
    this.isTurning = true;
    this.rotation = 90 * newDirection;
    this.target = -90 * newDirection;
    this.sheet = sheet;
    


    let frontEl: SVGForeignObjectElement | null = sheet.front.getPageEl();
    let backEl: SVGForeignObjectElement | null = sheet.back.getPageEl();
    if (frontEl) {
      this.frontG?.appendChild(frontEl);
    }
    else {
      ////console.log("missing front el");
    }
    if (backEl) {
      this.book.hide(backEl);
      this.backG?.appendChild(backEl);
      //console.log(backEl);
    }
    else {
      ////console.log("missing back el");
    }
    this.show();
    this.rotate(this.rotation);
    this.turn(newDirection);
    sheet.show("all");
  }
  show() {
    this.book.show(this.front as Element);
    this.book.show(this.back as Element);
    this.book.show(this.shadow as Element);
  }

  turn(direction = 1) {
    let interval = Math.round(1000 / this.book.bookVo.frameRate);
    let numSteps = Math.round(this.book.bookVo.duration / interval);
    let step = 0;

    this.turnBySteps(step, numSteps, interval, direction);
    //  this.unload(direction);
  }
  turnBySteps(step: number, numSteps: number, interval: number, direction?: number) {
    step += 1;
    let iterations = (this.target - this.rotation) / numSteps;
    let degreesToRotate = this.rotation + step * iterations;

    if (step >= numSteps) degreesToRotate = this.target;

    this.rotate(degreesToRotate);

    if (step < numSteps) {
     setTimeout(() => {
        this.turnBySteps(step, numSteps, interval, direction);
      }, interval);
    } else if (direction) {
      this.unload(direction);
    }
  }
  unload(direction = 1) {
    ////console.log("unload " + direction.toString())
    let sheet = this.sheet;
    ////console.log(sheet);

    this.isTurning = false;

    if (direction < 0) {
      this.book.stacks.right?.add(sheet as Sheet);
    } else {
      this.book.stacks.left?.add(sheet as Sheet);
    }

    this.sheet = null;
    GU.Elements.empty(this.frontG as Element);
    GU.Elements.empty(this.backG as Element);
    this.hide();

    // if (this.BB.stacks.left?.quantity) this.BB.ui?.prevBtn.enable();
    //if (this.BB.stacks.right?.quantity) this.BB.ui?.nextBtn.enable();
  }
  hide() {
    this.book.hide(this.front as Element);
    this.book.hide(this.back as Element);
    this.book.hide(this.shadow as Element);
  }
  rotate(degrees: number) {

    /*   this.back=this.book.currentSheet.left.el;
      this.front=this.book.currentSheet.right.el; */

    const newDegrees = Math.min(90, Math.max(-90, degrees));
    const seen = newDegrees < 0 ? this.back : this.front;
    const unseen = newDegrees < 0 ? this.front : this.back;
    

    this.book.hide(unseen as Element);
    this.book.show(seen as Element);
    this.distort(seen as SVGSVGElement, newDegrees);

    if (Math.abs(newDegrees) === 90) {
      this.book.hide(this.shadow as Element);
    } else {
      this.book.show(this.shadow as Element);
      this.distort(this.shadow as SVGSVGElement, newDegrees);
    }
  }
  distort(element: SVGSVGElement, degrees: number) {
    const radians = (degrees * Math.PI) / 180;
    const isBack = element == this.back;
    const isShadow = element == this.shadow;
    let sinX = (isBack ? -1 : 1) * Math.sin(radians);
    if (isShadow) sinX = sinX ** 3;

    const sinY =
      ((isBack || isShadow ? 1 : -1) * Math.cos(radians)) /
      (isShadow ? 16 : 6);

    const width = isBack ? this.book.bookVo.pageWidth : 0;
    const height = isBack ? this.book.bookVo.pageHeight : 0;
    const transform = this.book.svg?.createSVGTransform() as SVGTransform;
    const matrix = GU.Matrices.get(this.book.svg as SVGSVGElement, [
      sinX,
      sinY,
      0,
      1,
      0 - width * sinX,
      0 - height * sinY,
    ]);
    transform.setMatrix(matrix);
    element.transform.baseVal.clear();
    element.transform.baseVal.appendItem(transform as SVGTransform);
  }

}