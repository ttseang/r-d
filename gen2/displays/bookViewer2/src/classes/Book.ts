import { IScreen, SvgObj } from "svggame";
import { BookVo } from "../dataObjs/book/BookVo";
import { PageVo } from "../dataObjs/book/PageVo";
import { BookPage } from "./BookPage";
import { BookTurner } from "./BookTurner";
import { Sheet } from "./Sheet";
import { Stack } from "./Stack";
import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { PopUp } from "./PopUp";
import { BookController } from "./BookController";
import { PopUpVo } from "../dataObjs/book/PopUpVo";

export class Book extends SvgObj {
    public bookVo: BookVo;

    public pageIndex: number = 0;
    public rightPage: number = 0;
    public leftPage: number = -1;
    public pageCount: number = 0;

    private bookController: BookController = BookController.getInstance();

    public sheets: Sheet[] = [];
    public stacks: { left: Stack | null; right: Stack | null } = {
        left: null,
        right: null,
    };

    public defs: SVGDefsElement | null | undefined;

    private currentIndex: number = 0;
    public pages: BookPage[] = [];

    public popups: Map<string, PopUp> = new Map<string, PopUp>();

    public svg: SVGSVGElement | null | undefined;
    public popupArea: SVGSVGElement | null | undefined;

    public turner: BookTurner;
    public activePage: Element | null | undefined;

    private btnNext: SVGElement | null = null;
    private btnPrev: SVGElement | null = null;
    public addArea: HTMLElement | null = null;

    constructor(screen: IScreen, bookVo: BookVo) {
        super(screen, "boardbookobj");
        this.bookVo = bookVo;
        this.screen = screen;

        this.bookController.showPopup = this.showPopup.bind(this);
        this.bookController.hidePopup = this.hidePopup.bind(this);

        this.popupArea = (document.getElementById("popups") as unknown) as SVGSVGElement;

        this.addArea = document.getElementById("addArea");
        this.svg = this.addArea?.querySelector("#boardbookobj-1") as SVGSVGElement;

        //   //console.log(this.svg);

        if (this.svg) {
            this.activePage = this.svg?.querySelector('#pages>g.activepg');

            this.btnNext = this.svg.querySelector("#next_button");
            this.btnPrev = this.svg.querySelector("#prev_button");

            if (this.bookVo.pageHeight === 300) {
                this.btnNext?.setAttribute("transform", "translate(320,150) scale(0.75)");
                this.btnPrev?.setAttribute("transform", "translate(-320,150) scale(0.75)");
            }

            // this.btnNext?.setAttribute("y","50");

            if (this.btnNext) {
                this.btnNext.onclick = this.nextPage.bind(this);
            }
            if (this.btnPrev) {
                this.btnPrev.onclick = this.prevPage.bind(this);
            }
        }



        this.defs = document.getElementsByTagName("defs")[0];

        this.turner = new BookTurner(this);
        this.stacks = {
            left: new Stack(this, 'left'),
            right: new Stack(this, 'right'),
        };
    }
    buildPages() {


        let coverPage: BookPage = new BookPage(this.screen, this, 0, false);
        if (this.bookVo.cover) {
            coverPage.buildPage(this.bookVo.cover?.left);
        }

        this.pages.push(coverPage);

        let index: number = 0;

        for (let i: number = 0; i < this.bookVo.pages.length; i++) {
            index++;
            let leftPage: BookPage = new BookPage(this.screen, this, index, true);
            leftPage.buildPage(this.bookVo.pages[i].left);
            this.pages.push(leftPage);

            index++;

            let rightPage: BookPage = new BookPage(this.screen, this, index, false);
            rightPage.buildPage(this.bookVo.pages[i].right);
            this.pages.push(rightPage);

        }

        let backCover: BookPage = new BookPage(this.screen, this, 0, false);
        if (this.bookVo.cover) {
            backCover.buildPage(this.bookVo.cover.right);
        }
        index++;
        this.pages.push(backCover);


        this.pageCount = index + 1;
        //this.setPage(0);

        for (let i: number = 0; i < this.bookVo.popups.length; i++) {
            let popupVo: PopUpVo = this.bookVo.popups[i];
            let popup: PopUp = new PopUp(this.screen, this, popupVo);
            popup.buildPage();
            this.popups.set("popup" + popupVo.id.toString(), popup);
        }

        (window as any).book = this;
        this.loadSheets();
    }


    hidePages() {

    }
    public showPopup(id: number) {

        let popup: PopUp | undefined = this.popups.get("popup" + id.toString());

        if (popup && this.popupArea) {
            if (popup.el) {
                this.popupArea?.append(popup.el);
                let ww: number = this.bookVo.pageWidth * 2;

                let xx: number = (ww - popup.popupVo.w) / 2;
                let yy: number = ((this.bookVo.pageHeight) - popup.popupVo.h) / 2;

                popup.el.setAttribute("x", xx.toString());
                popup.el.setAttribute("y", yy.toString());
            }
        }
    }
    public hidePopup(popup: PopUp) {
        if (popup.el) {
            popup.el.remove();
        }
    }

    public hide(element: Element) {
        GU.Classes.add(element, 'hid');
    }

    public show(element: Element) {
        GU.Classes.remove(element, 'hid');
    }


    private loadSheets() {
        // Create sheets
        for (let index = 0; index < this.pageCount / 2; index++) {
            this.sheets.push(new Sheet(this, index));
        }

        // Create stacks
        for (let index = 0; index < this.sheets.length; index++) {
            //    //console.log("add stack");
            this.stacks.right?.add(this.sheets[this.sheets.length - 1 - index]);
        }
    }
    public nextPage() {
        if (this.stacks.right?.quantity && !this.turner?.isTurning) {

            this.pageIndex++;
            this.leftPage += 2;
            this.rightPage += 2;
            if (this.pageIndex == this.pageCount / 2) {
                this.rightPage = -1;
            }

            // GU.Events.fireNew('pageflip', this.book as HTMLElement);

            if (this.stacks.right) {
                const sheet = this.stacks.right.remove();
                if (sheet) this.turner?.load(sheet, 1);
            }

        }
    }
    public prevPage() {
        if (this.stacks.left?.quantity && !this.turner?.isTurning) {

            this.pageIndex--;
            if (this.pageIndex == 0) {
                this.rightPage = 0;
                this.leftPage = -1;
            }
            else {
                this.rightPage -= 2;
                this.leftPage -= 2;
            }
            const sheet = this.stacks.left.remove();


            if (sheet) this.turner?.load(sheet, -1);
        }
    }
    setPage(index: number) {
        if (index < 0) {
            index = 0;
        }
        if (index > this.pageCount) {
            index = this.pageCount - 2;
        }
        console.log(index);
        if (this.pageIndex !==index) {
            let diff: number = Math.abs(this.pageCount - index);
            console.log("diff="+diff);

            for (let i: number = 0; i < diff; i++) {
                console.log(i);
                if (this.pageIndex < index) {
                    this.nextPage();
                }
                else{
                    this.prevPage();
                }
            }
        }
    }
}