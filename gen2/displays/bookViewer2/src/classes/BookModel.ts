import { PageElementVo } from "../dataObjs/book/PageElementVo";
import { TextStyleVo } from "../dataObjs/TextStyleVo";

export class BookModel
{
    public imagePath:string;
    public styleDiv:HTMLStyleElement | null=null;
    public styleMap:Map<number,TextStyleVo>=new Map<number,TextStyleVo>();

    private static instance:BookModel | null=null;

    constructor()
    {
        this.imagePath="https://ttv5.s3.amazonaws.com/william/images/bookimages/";
        this.styleDiv=document.getElementById("elementStyles") as HTMLStyleElement;
    }
    public static getInstance():BookModel
    {
        if (this.instance===null)
        {
            this.instance=new BookModel();
        }
        return this.instance;
    }
    addStyle(elementVo:PageElementVo,style:HTMLStyleElement)
    {        
        if (this.styleDiv)
        {            
           // //console.log(elementVo.id);
           // //console.log(style.style.cssText);

            let cRule="#"+elementVo.id+"{"+style.style.cssText+"}";
            ////console.log(cRule);
            this.styleDiv.append(cRule);
        }
    }
    getFullPath(path:string)
    {
        return this.imagePath+path;
    }
}