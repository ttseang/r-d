import { IScreen, SvgObj } from "svggame";
import { PageElementVo } from "../dataObjs/book/PageElementVo";
import { Book } from "./Book";
import { BookModel } from "./BookModel";

export class BookPage extends SvgObj {
    private bookModel: BookModel = BookModel.getInstance();
    private book: Book;
    private contentArea: HTMLElement | null = null
   // public foreignObject: SVGForeignObjectElement | null = null;
    public folio: number;
    public pageNumber: number;
    public even: boolean;
    public odd: boolean;
    public left: boolean;
    public right: boolean;

    constructor(screen: IScreen, book: Book, index: number, onLeft: boolean) {
        super(screen, (onLeft == true) ? "lpageTemp" : "rpageTemp");
        //super(screen,"pageTemp");
        
        this.book = book;
        console.log(this.el);

        if (this.el) {
            this.el.id = "page" + index.toString();
            this.book.defs?.append(this.el);
            this.el.setAttribute("height",this.book.bookVo.pageHeight.toString());
            this.el.setAttribute("width",this.book.bookVo.pageWidth.toString());
            
           // this.contentArea = this.el.getElementsByClassName("pageContent")[0] as HTMLElement;
            this.contentArea=this.el.querySelector("div.pageContent");
            
            let pageSizeClass:string=(this.book.bookVo.pageHeight===300)?"psize2":"psize1";
            if(this.contentArea)
            {
                this.contentArea.classList.add(pageSizeClass);
            }
            //this.contentArea?.style={"height":this.book.bookVo.pageHeight.toString()+"px","width":this.book.bookVo.pageWidth.toString()+"px"};
            
        }
        
        this.folio = index + 1;
        this.pageNumber = this.folio;
        this.left=onLeft;
        this.right=!onLeft;
        this.even = !(this.pageNumber % 2);
        this.odd = !this.even;
        
    }
    public getPageEl()
    {
        return (this.el as unknown) as SVGForeignObjectElement;
    }
    
    buildPage(elements: PageElementVo[]) {
        for (let i: number = 0; i < elements.length; i++) {
            let element: PageElementVo = elements[i];

            if (this.contentArea) {
                this.contentArea.append(element.getHtml());
            }

            let cssStyle: HTMLStyleElement = element.getCSS();
            //   //console.log(cssStyle.style.cssText);
            this.bookModel.addStyle(element, cssStyle);
        }
    }
    hide()
    {
        this.book.hide(this.el as Element);
    }
    show()
    {
        this.book.show(this.el as Element);
    }
}