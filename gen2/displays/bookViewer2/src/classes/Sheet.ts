import { Book } from "./Book";
import { BookPage } from "./BookPage";



export class Sheet {
    front: BookPage;
    back: BookPage;
    stack: 'left' | 'right' | '';

    constructor(book: Book, index: number) {
      this.front = book.pages[2 * index];
      this.back = book.pages[1 + 2 * index];
      this.stack = '';
      this.hide();
    }
    
    hide() {
      this.front.hide();
      this.back.hide();
    }

    show(side = 'up') {
      const s = side.charAt(0);
      this.hide();

      switch (s) {
        case 'n':
          break;

        case 'a':
          this.back.show();
          this.front.show();
          break;

        case 'f':
          this.front.show();
          break;

        case 'b':
          this.back.show();
          break;

        case 'u':
          switch (this.stack) {
            case 'left':
              this.back.show();
              break;

            case 'right':
              this.front.show();

            default:
              break;
          }
          break;

        default:
          break;
      }
    }
  }