import { PosVo } from "svggame";
import { SpineChar } from "./SpineChar";
import { SpineTweenObj } from "./SpineTweenObj";


export class SpineGridTween {
    private gameObj: SpineChar;
    private tweenObj: SpineTweenObj;
    private autoStart: boolean;

    /**
     * Targets
     */
    private xTarget: number=0;
    private yTarget: number=0;
    private scaleXTarget: number=0;
    private scaleYTarget: number=0;


    /**
     * Increments
     *  */
    private xInc: number = 0;
    private yInc: number = 0;
    private scaleXInc: number = 0;
    private scaleYInc: number = 0;
    /**
     * Timer
     */
    private myTimer: any = null;
    private myTime: number = 0;

    constructor(gameObj: SpineChar, tweenObj: SpineTweenObj, autoStart: boolean = true) {
        this.gameObj = gameObj;
        this.tweenObj = tweenObj;
        this.autoStart = autoStart;
        if (!gameObj.currentPos) {
            console.warn("set position before using tween");
            return;
        }
        //
        //
        this.xTarget = gameObj.currentPos.x;
        this.yTarget = gameObj.currentPos.y;

        this.scaleXTarget = gameObj.scaleX;
        this.scaleYTarget = gameObj.scaleY;


        if (tweenObj.x != null) {
            this.xTarget = tweenObj.x;
        }
        if (tweenObj.y != null) {
            this.yTarget = tweenObj.y;
        }

        if (tweenObj.scaleX != null) {
            this.scaleXTarget = tweenObj.scaleX;
        }
        if (tweenObj.scaleY != null) {
            this.scaleYTarget = tweenObj.scaleY;
        }

        let duration2: number = tweenObj.duration / 5;
        this.myTime = tweenObj.duration;
        //
        //
        this.xInc = (this.xTarget - gameObj.currentPos.x) / duration2;
        this.yInc = (this.yTarget - gameObj.currentPos.y) / duration2;
        //
        //
        this.scaleXInc = (this.scaleXTarget - gameObj.scaleX) / duration2;
        this.scaleYInc = (this.scaleYTarget - gameObj.scaleY) / duration2;
        //
        //

        if (autoStart == true) {
            this.start();
        }
    }
    public start() {
        requestAnimationFrame(this.doStep.bind(this));
       /*  this.myTimer = setInterval(()=>{
            this.doStep();
        }, 5); */
    }
    private doStep() {
        this.myTime -= 5;

        if (this.myTime < 0) {
            /* this.gameObj.x = this.xTarget;
            this.gameObj.y = this.yTarget; */

            if (this.gameObj.currentPos == null) {
                this.gameObj.currentPos = new PosVo(this.xTarget, this.yTarget);
            }
            else {
                this.gameObj.currentPos.x = this.xTarget;
                this.gameObj.currentPos.y = this.yTarget;
            }


            clearInterval(this.myTimer);
            this.tweenObj.onComplete(this.gameObj);
            return;
        }

        if (this.gameObj.currentPos) {
            let xx: number = this.gameObj.currentPos.x + this.xInc;
            let yy: number = this.gameObj.currentPos.y + this.yInc;

            this.gameObj.currentPos.x=xx;
            this.gameObj.currentPos.y=yy;
        }

        /* this.gameObj.x += this.xInc;
        this.gameObj.y += this.yInc; */
        this.gameObj.scaleX += this.scaleXInc;
        this.gameObj.scaleY += this.scaleYInc;

        this.gameObj.updatePos();
        requestAnimationFrame(this.doStep.bind(this));
    }
}