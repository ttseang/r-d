export class BookController
{
    private static instance:BookController | null=null;

    public showPopup:Function=()=>{};
    public hidePopup:Function=()=>{};
    
    constructor()
    {
        
    }
    public static getInstance():BookController
    {
        if (this.instance===null)
        {
            this.instance=new BookController();
        }
        return this.instance;
    }
}