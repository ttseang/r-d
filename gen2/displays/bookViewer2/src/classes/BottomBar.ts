import { HtmlObj, IScreen } from "svggame";
import { ProgBar } from "./ProgBar";


export class BottomBar extends HtmlObj
{
    public progBar:ProgBar=new ProgBar();
    private screen:IScreen;
  
    constructor(screen:IScreen,progCallback:Function)
    {
        super("bottomBar");
        this.screen=screen;
        this.progBar.callback=progCallback;
    }
    public close()
    {
      //  this.y=100;
       // this.y=this.screen.gh*.5;
        if (this.el) {
            this.el.style.top = "20vh";
        }
    }
    public open()
    {
       // this.y=86;
       // this.y=0;
        if (this.el) {
            this.el.style.top = "4vh";
        }
    }
    
}