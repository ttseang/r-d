import { IScreen, SvgObj } from "svggame";
import { PageElementVo } from "../dataObjs/book/PageElementVo";
import { PopUpVo } from "../dataObjs/book/PopUpVo";
import { Book } from "./Book";
import { BookController } from "./BookController";
import { BookModel } from "./BookModel";

export class PopUp extends SvgObj {
    private bookModel: BookModel = BookModel.getInstance();
    private book: Book;
    private contentArea: HTMLElement | null = null
    public popupVo:PopUpVo;
    private btnClose:HTMLElement | null | undefined=null;
    private bookController:BookController=BookController.getInstance();

    constructor(screen: IScreen, book: Book,popupVo:PopUpVo) {
        super(screen, "bookpopup");
        this.book = book;
        this.popupVo=popupVo;

        if (this.el) {
            this.el.id = "popup" + popupVo.id.toString();
            this.book.defs?.append(this.el);
            this.el.setAttribute("height", this.popupVo.h.toString());
            this.el.setAttribute("width", this.popupVo.w.toString());
            this.contentArea = this.el.querySelector("div.popupContent");

            this.btnClose=this.el?.querySelector("#btnClose") as HTMLElement;

            
            if (this.btnClose)
            {
                this.btnClose.addEventListener('pointerup',()=>{this.bookController.hidePopup(this)});
               // this.bookController.hidePopup(this);
            }
        }
    }
    public getPageEl()
    {
        return (this.el as unknown) as SVGForeignObjectElement;
    }
    
    buildPage() {
        let elements: PageElementVo[]=this.popupVo.elements;

        for (let i: number = 0; i < elements.length; i++) {
            let element: PageElementVo = elements[i];

            if (this.contentArea) {
                this.contentArea.append(element.getHtml());
            }

            let cssStyle: HTMLStyleElement = element.getCSS();
            //   //console.log(cssStyle.style.cssText);
            this.bookModel.addStyle(element, cssStyle);
        }
    }
    hide()
    {
        this.book.hide(this.el as Element);
    }
    show()
    {
        this.book.show(this.el as Element);
    }
}