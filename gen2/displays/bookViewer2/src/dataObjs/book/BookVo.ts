import { PageVo } from "./PageVo";
import { PopUpVo } from "./PopUpVo";

export class BookVo {
    public cover: PageVo | null;
    public pages: PageVo[];
    public popups: PopUpVo[] = [];

    public duration: number = 300;
    public frameRate: number = 30;
    public pageWidth: number = 300;
    public pageHeight: number = 450;

    constructor(cover: PageVo | null = null, pages: PageVo[] = []) {
        this.cover = cover;
        this.pages = pages;
    }
    fromObj(bookData: any) {
        //console.log(bookData.popups);

        let pageData: any = bookData.pages;

        this.pageHeight = parseInt(bookData.pheight);
        this.pageWidth = parseInt(bookData.pwidth);

        let covers: PageVo = new PageVo();
        covers.fromObj(bookData.covers);
        this.cover = covers;

        for (let i: number = 0; i < pageData.length; i++) {
            let pageVo: PageVo = new PageVo();
            pageVo.fromObj(pageData[i]);
            this.pages.push(pageVo);
        }

        let popupData: any = bookData.popups;
        if (popupData) {
            for (let i: number = 0; i < popupData.length; i++) {
                let pdata: any = popupData[i];

                let popupVo: PopUpVo = new PopUpVo();
                popupVo.fromObj(pdata);
                this.popups.push(popupVo);
            }
        }
        //console.log(this);
    }
}