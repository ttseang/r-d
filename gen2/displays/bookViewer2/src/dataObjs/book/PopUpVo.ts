import { PageElementVo } from "./PageElementVo";

export class PopUpVo {
    public id: number;
    public h: number;
    public w: number;
    public elements: PageElementVo[] = [];

    constructor(id: number = 0, w: number = 0, h: number = 0) {
        this.id = id;
        this.w = w;
        this.h = h;
    }
    fromObj(pdata: any) {
        this.id = pdata.id;
        this.w = pdata.w;
        this.h = pdata.h;

        let elementData: any = pdata.elements;

        for (let i: number = 0; i < elementData.length; i++) {
            let pageElementVo: PageElementVo = new PageElementVo();
            pageElementVo.fromObj(elementData[i]);
            this.elements.push(pageElementVo);
        }
    }
}