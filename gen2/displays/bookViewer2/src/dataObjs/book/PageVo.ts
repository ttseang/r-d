import { PageElementVo } from "./PageElementVo";

export class PageVo
{
    public left:PageElementVo[];
    public right:PageElementVo[];

    constructor(left:PageElementVo[]=[],right:PageElementVo[]=[])
    {
        this.left=left;
        this.right=right;
    }
    fromObj(obj:any)
    {
        for (let i:number=0;i<obj.left.length;i++)
        {
           // //console.log(obj.left[i]);
            let pageElementVo:PageElementVo=new PageElementVo();
            pageElementVo.fromObj(obj.left[i]);
            this.left.push(pageElementVo);
        }
        for (let j:number=0;j<obj.right.length;j++)
        {
            ////console.log(obj.right[j]);
            let pageElementVo:PageElementVo=new PageElementVo();
            pageElementVo.fromObj(obj.right[j]);
            this.right.push(pageElementVo);
        }
    }
}