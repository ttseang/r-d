import { IGameObj, IScreen, Math2, SimpleTween, SvgObj, TweenObj } from "svggame";

export class ColorBurst {
    private scene: IScreen;
    public x: number;
    public y: number;
    public size: number;
    public count: number;
    public dist: number;
    public duration: number;
    public maxDist: number;
    public color: number;

    constructor(scene: IScreen, x: number = 0, y: number = 0, size: number = 5, count: number = 50, dist: number = 150, duration: number = 1000, maxDist: number = 300, color: number = 0xffffff) {
        this.scene = scene;
        this.x = x;
        this.y = y;
        this.size = size;
        this.count = count;
        this.dist = dist;
        this.duration = duration;
        this.maxDist = maxDist;
        this.color = color;
    }
    public start() {

        let keys:string[]=["redstar","bluestar","greenstar","yellowstar","orangestar"];

        for (let i = 0; i < this.count; i++) {
            //  let star = this.scene.add.sprite(this.x, this.y, "effectColorStars");

            let keyIndex:number=Math2.between(0,keys.length-1);
            let key:string=keys[keyIndex];
          

            let star: SvgObj = new SvgObj(this.scene,key);
            star.x=this.x;
            star.y=this.y;
           
            //
            //
            //        
           

           // //console.log(this.maxDist);

            let r = Math2.between(50, this.maxDist);
            let s = Math2.between(1, 100) / 1000;

         
            star.gameWRatio=s;
           /*  star.scaleX = s;
            star.scaleY = s; */
            let angle = i * (360 / this.count);
            let tx = this.x + r * Math.cos(angle);
            let ty = this.y + r * Math.sin(angle);

            let tweenObj: TweenObj = new TweenObj();
            tweenObj.duration = this.duration;
            tweenObj.x = tx;
            tweenObj.y = ty;
            tweenObj.alpha = 0;
            tweenObj.angle=Math2.between(0,360);

            tweenObj.onComplete = this.tweenDone.bind(this);

            let tw: SimpleTween = new SimpleTween(star, tweenObj);
            //  star.x=tx;
            // star.y=ty;
            /* this.scene.tweens.add({
                targets: star,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone.bind(this)
            }); */
        }

    }
    tweenDone(star: IGameObj) {
        star.destroy();
    }
}