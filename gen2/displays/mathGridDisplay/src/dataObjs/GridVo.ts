import { CellVo } from "./CellVo";
import { RowVo } from "./RowVo";

export class GridVo {
    public rowCount: number;
    public colCount: number;
    public rows: RowVo[];
    constructor(rows: RowVo[]) {
        this.rows = rows;

        this.rowCount = rows.length;
        let maxColCount = 0;
        for (let r = 0; r < this.rowCount; r++) {
            let row: RowVo = rows[r];
            ////console.log(row);

            if (row.cells.length > maxColCount) {
                maxColCount = row.cells.length;
            }
        }
        this.colCount = maxColCount;
    }
    clone() {
        let rows: RowVo[] = [];
        for (let r = 0; r < this.rowCount; r++) {
            let row: RowVo = this.rows[r];
            let cells: CellVo[] = [];
            for (let c = 0; c < this.colCount; c++) {
                let cell: CellVo = row.cells[c];
                let cell2: CellVo = new CellVo(cell.text, cell.textClasses.slice());
                cells.push(cell2);
            }
            rows.push(new RowVo(cells, []));
        }
        return new GridVo(rows);
    }
    addRow(atStart:boolean=false) {
        let cols: CellVo[] = [];
        for (let c = 0; c < this.colCount; c++) {
            cols.push(new CellVo('', []));
        }
        if (atStart)
        {
            this.rows.unshift(new RowVo(cols, []));
        }
        else
        {
            this.rows.push(new RowVo(cols, []));
        }
        this.rowCount++;
    }
    addColumn(atStart:boolean=false) {
        for (let r = 0; r < this.rowCount; r++) {
            if (atStart)
            {
                this.rows[r].cells.unshift(new CellVo('', []));
            }
            else
            {
                this.rows[r].cells.push(new CellVo('', []));
            }
        }
        this.colCount++;
    }
    removeRow(atStart:boolean=false) {
        if (atStart)
        {
            this.rows.shift();
        }
        else
        {
            this.rows.pop();
        }
        this.rowCount--;   

    }
    removeColumn(atStart:boolean=false) {
        for (let r = 0; r < this.rowCount; r++) {
            if (atStart)
            {
                this.rows[r].cells.shift();
            }
            else
            {
                this.rows[r].cells.pop();
            }
        }
        this.colCount--;

    }
    shiftUp() {
        //move all rows up one
        let firstRow: RowVo = this.rows[0];
        for (let r = 0; r < this.rowCount - 1; r++) {
            this.rows[r] = this.rows[r + 1];
        }
        this.rows[this.rowCount - 1] = firstRow;
    }
    shiftDown() {
        //move all rows down one
        let lastRow: RowVo = this.rows[this.rowCount - 1];
        for (let r = this.rowCount - 1; r > 0; r--) {
            this.rows[r] = this.rows[r - 1];
        }
        this.rows[0] = lastRow;
    }
    shiftLeft() {
        //move all columns left one
        for (let r = 0; r < this.rowCount; r++) {
            let firstCell: CellVo = this.rows[r].cells[0];
            for (let c = 0; c < this.colCount - 1; c++) {
                this.rows[r].cells[c] = this.rows[r].cells[c + 1];
            }
            this.rows[r].cells[this.colCount - 1] = firstCell;
        }

    }
    shiftRight() {
        //move all columns right one
        for (let r = 0; r < this.rowCount; r++) {
            let lastCell: CellVo = this.rows[r].cells[this.colCount - 1];
            for (let c = this.colCount - 1; c > 0; c--) {
                this.rows[r].cells[c] = this.rows[r].cells[c - 1];
            }
            this.rows[r].cells[0] = lastCell;
        }
    }
    shiftColumnUp(column: number) {
        //move all cells in the column up one
        let firstCell: CellVo = this.rows[0].cells[column];
        for (let r = 0; r < this.rowCount - 1; r++) {
            this.rows[r].cells[column] = this.rows[r + 1].cells[column];
        }
        this.rows[this.rowCount - 1].cells[column] = firstCell;
    }
    shiftColumnDown(column: number) {
        //move all cells in the column down one
        let lastCell: CellVo = this.rows[this.rowCount - 1].cells[column];
        for (let r = this.rowCount - 1; r > 0; r--) {
            this.rows[r].cells[column] = this.rows[r - 1].cells[column];
        }
        this.rows[0].cells[column] = lastCell;
    }
    shiftRowLeft(row: number) {
        //move all cells in the row left one
        let firstCell: CellVo = this.rows[row].cells[0];
        for (let c = 0; c < this.colCount - 1; c++) {
            this.rows[row].cells[c] = this.rows[row].cells[c + 1];
        }
        this.rows[row].cells[this.colCount - 1] = firstCell;
    }
    shiftRowRight(row: number) {
        //move all cells in the row right one
        let lastCell: CellVo = this.rows[row].cells[this.colCount - 1];
        for (let c = this.colCount - 1; c > 0; c--) {
            this.rows[row].cells[c] = this.rows[row].cells[c - 1];
        }
        this.rows[row].cells[0] = lastCell;
    }

    insertRowAbove(row: number) {
        let cols: CellVo[] = [];
        for (let c = 0; c < this.colCount; c++) {
            cols.push(new CellVo('', []));
        }
        this.rows.splice(row, 0, new RowVo(cols, []));
        this.rowCount++;
    }
    insertRowBelow(row: number) {
        let cols: CellVo[] = [];
        for (let c = 0; c < this.colCount; c++) {
            cols.push(new CellVo('', []));
        }
        this.rows.splice(row + 1, 0, new RowVo(cols, []));
        this.rowCount++;
    }
    insertColumnLeft(col: number) {
        for (let r = 0; r < this.rowCount; r++) {
            this.rows[r].cells.splice(col, 0, new CellVo('', []));
        }
        this.colCount++;
    }
    insertColumnRight(col: number) {
        for (let r = 0; r < this.rowCount; r++) {
            this.rows[r].cells.splice(col + 1, 0, new CellVo('', []));
        }
        this.colCount++;
    }
    deleteRow(row: number) {
        if (this.rows.length>1)
        {
            this.rows.splice(row, 1);
            this.rowCount--;
        }
    }
    deleteColumn(col: number) {
        if (this.colCount>1)
        {
            for (let r = 0; r < this.rowCount; r++) {
                this.rows[r].cells.splice(col, 1);
            }
            this.colCount--;
        }
    }

    fitToContents() {
        (window as any).grid = this;
        //check all rows, if all the cells are empty, remove the row
        let rowsToRemove: number[] = [];
        for (let r = 0; r < this.rowCount; r++) {
            let rowEmpty: boolean = true;
            for (let c = 0; c < this.colCount; c++) {
                if (this.rows[r].cells[c].text !== ' ') {
                    rowEmpty = false;
                    break;
                }
            }
            if (rowEmpty) {
                rowsToRemove.push(r);
            }
        }
        for (let r = rowsToRemove.length - 1; r >= 0; r--) {
            this.rows.splice(rowsToRemove[r], 1);
        }
        this.rowCount = this.rows.length;
        //check all columns, if all the cells are empty, remove the column
        let colsToRemove: number[] = [];
        for (let c = 0; c < this.colCount; c++) {
            let colEmpty: boolean = true;
            for (let r = 0; r < this.rowCount; r++) {
                if (this.rows[r].cells[c].text !== ' ') {
                    colEmpty = false;
                    break;
                }
            }
            if (colEmpty) {
                colsToRemove.push(c);
            }
        }
        for (let c = colsToRemove.length - 1; c >= 0; c--) {
            for (let r = 0; r < this.rowCount; r++) {
                this.rows[r].cells.splice(colsToRemove[c], 1);
            }
        }
        this.colCount = this.rows[0].cells.length;
        ////console.log(this);
    }
    getCells() {
        let cells: CellVo[][] = [];
        for (let r = 0; r < this.rowCount; r++) {
            cells[r] = [];
            for (let i: number = 0; i < this.rows[r].cells.length; i++) {
                let cell: CellVo = this.rows[r].cells[i];

                cells[r].push(cell);
            }
        }

        return cells;
    }
    getExportData() {
        let data:any[][] = [];
        for (let r = 0; r < this.rowCount; r++) {
            data[r] = [];
            for (let i: number = 0; i < this.rows[r].cells.length; i++) {
                let cell: CellVo = this.rows[r].cells[i];
                data[r].push(cell.getExport());
            }
        }
        return data;     
    }
}