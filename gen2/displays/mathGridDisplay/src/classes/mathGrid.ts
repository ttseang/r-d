import { CellVo } from "../dataObjs/CellVo";
import { GridVo } from "../dataObjs/GridVo";
import { RowVo } from "../dataObjs/RowVo";
import { MathGridLoader } from "./mathGridLoader";

export class MathGrid {
    public lti: string;
    public steps: GridVo[] = [];
    public currentStep: number = 0;
    public el: HTMLElement;
    private onReady: Function;
    constructor(lti: string, el: HTMLElement, onReady: Function) {
        this.lti = lti;
        this.el = el;
        this.onReady = onReady;
        this.loadContent();
    }
    private loadContent() {
        let mathGridLoader: MathGridLoader = new MathGridLoader();
        mathGridLoader.getFileContent(this.lti, this.onFileContentLoaded.bind(this));
    }
    private onFileContentLoaded(data: any) {
        console.log(data);
        this.steps = [];
        let steps: any[] = data.data.steps;

        for (let i: number = 0; i < steps.length; i++) {
            let grid: any = steps[i];

            let rowCount: number = grid.length;
            let rowVos: RowVo[] = [];

            for (let j: number = 0; j < rowCount; j++) {
                let row: any = grid[j];
                let colunmCount: number = row.length;
                let cellVos: CellVo[] = [];
                for (let k: number = 0; k < colunmCount; k++) {
                    let cell: any = row[k];
                    let textClasses: string = cell.textClasses || "";
                    let subTextClasses: string = cell.subTextClasses || "";
                    let superTextClasses: string = cell.superTextClasses || "";
                    let cellTextClasses: string = cell.cellTextClasses || "";

                    let superText: string = cell.superText || "";
                    let subText: string = cell.subText || "";


                    let textClassArray: string[] = textClasses.split(",");
                    let subTextClassArray: string[] = subTextClasses.split(",");
                    let superTextClassArray: string[] = superTextClasses.split(",");
                    let cellTextClassArray: string[] = cellTextClasses.split(",");


                    let cellVo: CellVo = new CellVo(cell.text, []);
                    cellVo.textClasses = textClassArray;
                    cellVo.subTextClasses = subTextClassArray;
                    cellVo.superTextClasses = superTextClassArray;
                    cellVo.cellClasses = cellTextClassArray;
                    cellVo.supertext = superText;
                    cellVo.subtext = subText;
                    cellVos.push(cellVo);
                }
                rowVos.push(new RowVo(cellVos, []));
            }
            this.steps.push(new GridVo(rowVos));
        }
        this.onReady();
        //   this.changeStep(2);
    }
    getText(cellVo: CellVo) {
        let textElement: HTMLElement = document.createElement('span');

        if (cellVo.subtext) {
            // let subClasses: string = cellVo.subTextClasses.join(' ');
            const sub = document.createElement('sub');
            sub.innerText = cellVo.subtext;
            cellVo.subTextClasses.forEach((className: string) => {
                if (className !== '') {
                    sub.classList.add(className);
                }
            });
            textElement.appendChild(sub);
        }
        if (cellVo.supertext) {

            let sup = document.createElement('sup');
            sup.innerText = cellVo.supertext;

            cellVo.subTextClasses.forEach((className: string) => {
                if (className !== '') {
                    sup.classList.add(className);
                }
            });
            textElement.appendChild(sup);
        }
        let cellText: HTMLElement = document.createElement('span');
        cellText.innerText = cellVo.text;
        cellVo.textClasses.forEach((className: string) => {
            if (className !== '') {
                cellText.classList.add(className);
            }
        });
        textElement.appendChild(cellText);
        return textElement;
    }
    changeStep(index: number) {
        this.currentStep = index;
        this.render();
    }
    render() {
        this.el.innerHTML = '';
        this.el.appendChild(this.getGrid());
    }
    getGrid() {
        //render this.state.grid as table
        let table: HTMLElement = document.createElement('table');
        table.className = 'previewTable';

        let grid: GridVo = this.steps[this.currentStep];
        console.log(grid);
        for (let r = 0; r < grid.rows.length; r++) {
            let row: RowVo = grid.rows[r];
            let rowObj: HTMLElement = document.createElement('tr');
            for (let c = 0; c < row.cells.length; c++) {
                let cell = row.cells[c];
                //let text = cell.text;
                let id: string = r + '_' + c;

                let cellClasses: string = cell.cellClasses.join(' ');
                const td: HTMLElement = document.createElement('td');
                td.id = id;
                td.className = cellClasses;

                td.appendChild(this.getText(cell));

                if (cellClasses !== '') {
                    td.classList.add(cellClasses);
                    //td.className = cellClasses;

                }
                rowObj.appendChild(td);
            }
            let rowClasses: string = row.rowClasses.join(' ');
            if (rowClasses !== '') {
                rowObj.classList.add(rowClasses);
            }
            table.appendChild(rowObj);
        }
        return table;
    }
}