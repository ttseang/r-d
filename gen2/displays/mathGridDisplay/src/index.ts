import { MathGrid } from "./classes/mathGrid";

window.onload = function () {
    let mathGrid: MathGrid = new MathGrid("TT.RD.JSONTEST", document.getElementsByClassName("mathGrid")[0] as HTMLDivElement, () => {
        console.log("MathGrid loaded");
        mathGrid.changeStep(2);
    });

}