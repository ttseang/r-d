"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScreenMain = void 0;
var BaseScreen_1 = require("../classes/core/BaseScreen");
var SvgObj_1 = require("../classes/core/gameobjects/SvgObj");
var ScreenMain = /** @class */ (function (_super) {
    __extends(ScreenMain, _super);
    function ScreenMain() {
        return _super.call(this, "ScreenMain") || this;
    }
    ScreenMain.prototype.create = function () {
        var testButton = new SvgObj_1.SvgObj(this, "testButton");
        this.center(testButton, true);
    };
    return ScreenMain;
}(BaseScreen_1.BaseScreen));
exports.ScreenMain = ScreenMain;
//# sourceMappingURL=ScreenMain.js.map