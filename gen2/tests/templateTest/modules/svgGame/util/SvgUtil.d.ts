export declare class SVGUtil {
    static getAttNum(el: HTMLElement, attName: string): number;
    static trimNum(num: number): number;
    static findTop(el: HTMLElement, tagName: any): HTMLElement;
}
