"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SVGUtil = void 0;
var SVGUtil = /** @class */ (function () {
    function SVGUtil() {
    }
    SVGUtil.getAttNum = function (el, attName) {
        if (el) {
            var val = el.getAttribute(attName);
            if (val == null) {
                return 0;
            }
            if (isNaN(parseInt(val))) {
                return 0;
            }
            return parseInt(val);
        }
        return 0;
    };
    SVGUtil.trimNum = function (num) {
        return Math.round(Math.floor(num * 1000) / 1000);
    };
    SVGUtil.findTop = function (el, tagName) {
        var obj = el;
        while (obj.tagName != tagName) {
            obj = obj.parentElement;
            if (obj == null || obj == undefined) {
                break;
            }
        }
        return obj;
    };
    return SVGUtil;
}());
exports.SVGUtil = SVGUtil;
//# sourceMappingURL=SvgUtil.js.map