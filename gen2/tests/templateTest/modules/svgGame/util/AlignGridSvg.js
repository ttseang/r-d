"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlignGrid = void 0;
var __1 = require("..");
var AlignGrid = /** @class */ (function () {
    function AlignGrid(rows, cols, gw, gh) {
        this.ch = 0;
        this.cw = 0;
        this.gw = 0;
        this.gh = 0;
        this.gridID = "gridLines";
        this.gw = gw;
        this.gh = gh;
        this.cw = Math.floor((gw / cols) * 1000) / 1000;
        this.ch = Math.floor((gh / rows) * 1000) / 1000;
    }
    AlignGrid.prototype.placeAt = function (col, row, obj) {
        var xx = this.cw * col;
        var yy = this.ch * row;
        obj.x = xx;
        obj.y = yy;
    };
    AlignGrid.prototype.showGrid = function () {
        //<path stroke-width="4" stroke="red" d="M0 0 L0 100 M100 0 L100 100 "></path>
        var p = "";
        for (var i = 0; i < this.gw; i += this.cw) {
            p += "M" + i.toString() + " 0 ";
            p += "L" + i.toString() + " " + this.gh.toString();
        }
        for (var i = 0; i < this.gh; i += this.ch) {
            p += "M0 " + i.toString() + " L" + this.gw + " " + i.toString();
        }
        // console.log(p);
        var p2 = '<path stroke-width="4" stroke="red" d="' + p + '"></path>';
        document.getElementById(this.gridID).innerHTML = p2;
    };
    AlignGrid.prototype.findNearestGridXY = function (xx, yy) {
        var row = Math.floor(yy / this.ch);
        var col = Math.floor(xx / this.cw);
        return new __1.PosVo(col, row);
    };
    AlignGrid.prototype.findNearestGridXYDec = function (xx, yy) {
        var row = yy / this.ch;
        var col = xx / this.cw;
        return new __1.PosVo(col, row);
    };
    return AlignGrid;
}());
exports.AlignGrid = AlignGrid;
//# sourceMappingURL=AlignGridSvg.js.map