import { IGameObj } from "../..";
import { FontSizeVo } from "./dataobjs/fontSizeVo";
import { GameOptions } from "./dataobjs/gameOptions";
export declare class GameManager {
    private static instance;
    gameOptions: GameOptions;
    gameID: string;
    gameEl: HTMLElement | null;
    gw: number;
    gh: number;
    private objMap;
    private defFontSize;
    fontSizes: Map<string, FontSizeVo>;
    constructor();
    static getInstance(): GameManager;
    regObj(id: string, obj: IGameObj): void;
    unRegObj(id: string): void;
    getObj(id: string): IGameObj;
    getFontSize(sizeKey: string, canvasWidth: number): number;
    regFontSize(key: string, defFontSize: number, canvasBase: number): void;
    private doResize;
}
