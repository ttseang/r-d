export declare class StyleVo {
    color: string;
    fontSize: string;
    fontWeight: string;
    constructor(color: string, fontSize: string, fontWeight: string);
}
