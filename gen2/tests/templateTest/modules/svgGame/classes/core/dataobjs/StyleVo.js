"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StyleVo = void 0;
var StyleVo = /** @class */ (function () {
    function StyleVo(color, fontSize, fontWeight) {
        this.color = color;
        this.fontSize = fontSize;
        this.fontWeight = fontWeight;
    }
    return StyleVo;
}());
exports.StyleVo = StyleVo;
//# sourceMappingURL=StyleVo.js.map