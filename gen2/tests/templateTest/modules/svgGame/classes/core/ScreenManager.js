"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScreenManager = void 0;
var ScreenManager = /** @class */ (function () {
    function ScreenManager() {
        this.screens = [];
        this.currentScreen = null;
    }
    ScreenManager.getInstance = function () {
        if (this.instance === null) {
            this.instance = new ScreenManager();
        }
        return this.instance;
    };
    ScreenManager.prototype.doResize = function () {
        if (this.currentScreen) {
            this.currentScreen.doResize();
        }
    };
    ScreenManager.prototype.startScreen = function (screen) {
        if (this.currentScreen != null) {
            this.currentScreen.destroy();
        }
        this.currentScreen = screen;
        this.currentScreen.start();
    };
    ScreenManager.prototype.findScreen = function (screenName) {
        for (var i = 0; i < this.screens.length; i++) {
            if (this.screens[i].key == screenName) {
                return this.screens[i];
            }
        }
        return null;
    };
    ScreenManager.prototype.changeScreen = function (screenName) {
        var screen = this.findScreen(screenName);
        if (!screen) {
            throw new Error("Screen Not Found");
        }
        this.startScreen(screen);
    };
    ScreenManager.prototype.init = function () {
        if (this.screens.length > 0) {
            this.startScreen(this.screens[0]);
            this.doResize();
        }
    };
    ScreenManager.instance = null;
    return ScreenManager;
}());
exports.ScreenManager = ScreenManager;
//# sourceMappingURL=ScreenManager.js.map