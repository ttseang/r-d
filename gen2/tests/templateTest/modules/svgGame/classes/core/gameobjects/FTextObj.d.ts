import { IScreen } from "../interfaces/IScreen";
import { SvgObj } from "./SvgObj";
export declare class FTextObj extends SvgObj {
    private textEl;
    constructor(screen: IScreen, key: string);
    static createNew(): void;
    setText(text: string): void;
    setFontSize(size: number): void;
}
