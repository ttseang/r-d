import { StyleVo } from "../dataObjs/StyleVo";
export declare class HtmlObj {
    el: HTMLElement | null;
    private _x;
    private _y;
    private _visible;
    id: string;
    constructor(id: string);
    private init;
    get x(): number;
    set x(value: number);
    get y(): number;
    set y(value: number);
    get visible(): boolean;
    set visible(value: boolean);
    flyTo(xx: number, yy: number): void;
    getStyle(className: string): StyleVo;
}
