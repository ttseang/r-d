"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextObj = void 0;
var SvgObj_1 = require("./SvgObj");
var TextObj = /** @class */ (function (_super) {
    __extends(TextObj, _super);
    function TextObj(screen, key) {
        return _super.call(this, screen, key) || this;
    }
    TextObj.createNew = function () {
        //implement here
    };
    TextObj.prototype.setText = function (text) {
        if (this.el) {
            console.log(text);
            this.el.textContent = text;
            this.updateSizes();
        }
    };
    TextObj.prototype.setFontSize = function (size) {
        if (this.el) {
            this.el.setAttribute("font-size", size.toString() + "px");
            this.updateSizes();
        }
    };
    return TextObj;
}(SvgObj_1.SvgObj));
exports.TextObj = TextObj;
//# sourceMappingURL=TextObj.js.map