export declare class SoundObj {
    private sound;
    constructor(src: string);
    play(): void;
    stop(): void;
}
