
import { BaseScreen, IScreen } from "svggame";

export class ScreenMain extends BaseScreen implements IScreen {


    constructor() {
        super("ScreenMain");



    }
    create() {
        super.create();
        (window as any).scene = this;

    }


    doResize() {
        super.doResize();

    }

}