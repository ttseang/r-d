
import { BaseScreen, IScreen,SvgObj } from "svggame";

export class ScreenMain extends BaseScreen implements IScreen {

    private box!:SvgObj;

    constructor() {
        super("ScreenMain");

        

    }
    create() {
        super.create();
        (window as any).scene = this;
        this.box=new SvgObj(this,"box");
        
        this.box.gameWRatio=.1;

       // this.grid?.showGrid();
        this.grid?.placeAt(5,5,this.box);
    }


    doResize() {
        super.doResize();

        this.box.gameWRatio=.1;

       // this.grid?.showGrid();
        this.grid?.placeAt(5,5,this.box);
    }

}