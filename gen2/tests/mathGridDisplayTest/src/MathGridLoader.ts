export class MathGridLoader {
    constructor() {

    }
    public async getFileContent(lti: string, callback: Function) {
        //https://tthq.me/api/pr/getequation/1
        //https://tthq.me/api/pr/getequation/X.TEST.01
        let url: string = "https://tthq.me/api/pr/getequation/" + encodeURIComponent(lti);
        try {
            await fetch(url, {
                method: "post",
                // mode: "no-cors", // no-cors, *cors, same-origin
            }).then(response => {
                if (response.ok) {
                    response.json().then(json => {
                        // console.log("math grid loader", lti, json);
                        if (!json.error) {
                            callback(json);
                        }
                        else {
                            console.log("error");
                        }
                    });
                }
            });
        } 
        catch(e) {
            console.log("error");
        }
    }
}