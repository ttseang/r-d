import { TCellVo } from "./dataObjs/TCellVo";
import { TGridVo } from "./dataObjs/TGridVo";

export class Tester
{
    private rowInput: HTMLInputElement;
    private colInput: HTMLInputElement;

    private btnShowRow:HTMLButtonElement;
    private btnShowCol:HTMLButtonElement;
constructor()
{
    this.rowInput = document.getElementsByClassName("rowNumber")[0] as HTMLInputElement;
    this.colInput = document.getElementsByClassName("columnNumber")[0] as HTMLInputElement;

    this.btnShowRow=document.getElementsByClassName("btnShowRow")[0] as HTMLButtonElement;
    this.btnShowCol=document.getElementsByClassName("btnShowCol")[0] as HTMLButtonElement;
}
public init(): void
{
    
    this.rowInput.value = "3";
    this.colInput.value = "3";
    this.rowInput.onchange = this.colInput.onchange = this.onInputChange.bind(this);
    this.onInputChange();

    this.btnShowRow.onclick=this.showRow.bind(this);
}
showRow()
{
    let tGridVo:TGridVo=(window as any).tGridVo;
    if (tGridVo)
    {
        console.log("tGridVo",tGridVo.getRow(parseInt(this.rowInput.value)));
    }
}
private onInputChange(): void
{
    let row: number = parseInt(this.rowInput.value);
    let col: number = parseInt(this.colInput.value);
    
    let tGridVo:TGridVo=(window as any).tGridVo;
    if (tGridVo)
    {
        console.log("tGridVo",tGridVo);
        let row:number=parseInt(this.rowInput.value);
        let col:number=parseInt(this.colInput.value);
        let cell:TCellVo=tGridVo.gridData[col][row];
        let text=cell.text.join("");
        if (text.length>0)
        {
            console.log("text",text);
        }
        else
        {
            console.log("text is empty");
        }
    }

}
}