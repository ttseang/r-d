import { MMLReader } from "./util/mmlReader";

window.onload=function()
{
    const mmlReader: MMLReader = new MMLReader();
    mmlReader.getFileContent("TT.RD.LATEX2",(ltiData:any)=>{
        
        let steps = JSON.parse(ltiData.data);
       
        let mml:string=steps[0].mml;
        
        let mmlDiv = document.getElementsByClassName("mathHolder")[0];
        mmlDiv.innerHTML = mml;
    });
}