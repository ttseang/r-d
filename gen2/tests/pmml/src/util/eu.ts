class EU
{
    
    public static apnd(elmt:HTMLElement, a:HTMLElement) {

        if (!a) return;
        if (Array.isArray(a)) {
            for (let i in a) this.apnd(elmt, a[i]);
            return;
        }
        elmt.appendChild(a);
    }
    public static append(elmt:HTMLElement, a:HTMLElement) {
        this.apnd(elmt, a);
        return elmt;
    }
    public static atcl(elmt:HTMLElement, a:string='') {
        if (Array.isArray(a)) return this.ca(elmt, a);
        if (typeof a == 'object') return this.atts(elmt, a);
        return this.ca(elmt, a);
    }
    public static att(elmt:HTMLElement, nam:string, val:string = '') {
        elmt.setAttribute(nam, String(val));
    }
    public static atts(elmt:HTMLElement, atts:any = {}) {
        for (let str in atts) {
            this.att(elmt, str, String(atts[str]));
        }
    }
    public static ca(elmt:HTMLElement,v:string='') {
        if (!v) return;
        if (Array.isArray(v)) {
            for (let i in v) this.ca(elmt, v[i]);
            return;
        }
        elmt.classList.add(v);
    }
    public static el(tag:string, ns:string) {
        return document.createElementNS(ns, tag);
    }
    public static mix(elmt:HTMLElement, a:any=[]) {
        for (let i in a) this.atcl(elmt, a[i]);
    }
    public static mt(elmt:HTMLElement) {
        while (elmt.firstChild) {
            elmt.removeChild(elmt.firstChild);
        }
    }
    public static l(elmt:HTMLElement, evt:string, func:any) {
        elmt.addEventListener(evt, func);
    }
    public static lc(elmt:HTMLElement, func:any) {
        elmt.addEventListener('click', func);
    }
    public static lcx(elmt:HTMLElement, func:any) {
        elmt.addEventListener('change', func);
    }
    public static lmd(elmt:HTMLElement, func:any) {
        elmt.addEventListener('mousedown', func);
    }
    public static lme(elmt:HTMLElement, func:any) {
        elmt.addEventListener('mouseenter', func);
    }
    public static lml(elmt:HTMLElement, func:any) {
        elmt.addEventListener('mouseleave', func);
    }
    public static lmm(elmt:HTMLElement, func:any) {
        elmt.addEventListener('mousemove', func);
    }
    public static lmo(elmt:HTMLElement, func:any) {
        elmt.addEventListener('mouseover', func);
    }
    public static lmt(elmt:HTMLElement, func:any) {
        elmt.addEventListener('mouseout', func);
    }
    public static lmu(elmt:HTMLElement, func:any) {
        elmt.addEventListener('mouseup', func);
    }
    public static ntx(elmt:HTMLElement, str:string) {
        let n = document.createTextNode(String(str));
        elmt.appendChild(n);
    }
    /* public static nu(tag:string, atts:any, ns:string) {
        let l = this.el(tag, ns);
        this.atts(l, atts);
        this.xtra(l);
        return l;
    }
    public static nuNS(tag:string, atts:any, ns:string) {
        let l = this.el(tag, ns);
        this.atts(l, atts);
        return l;
    } */
   /*  "xtra": function(elmt) {
		elmt.append = (...a)=> EU.append(elmt,a);
		elmt.ga = elmt.querySelectorAll;
		elmt.ge = elmt.querySelector;
		elmt.l = (e,f)=> EU.l(elmt,e,f);
		elmt.lc = (f)=> EU.lc(elmt,f);
		elmt.lcx = (f)=> EU.lcx(elmt,f);
		elmt.lmd = (f)=> EU.lmd(elmt,f);
		elmt.lme = (f)=> EU.lme(elmt,f);
		elmt.lml = (f)=> EU.lml(elmt,f);
		elmt.lmm = (f)=> EU.lmm(elmt,f);
		elmt.lmo = (f)=> EU.lmo(elmt,f);
		elmt.lmt = (f)=> EU.lmt(elmt,f);
		elmt.lmu = (f)=> EU.lmu(elmt,f);
		elmt.mt = (f)=> EU.mt(elmt);
		elmt.ntx = (str = ' ')=> EU.ntx(elmt, str);
		return elmt;
	}, */
}


/* const clamp = (num, min=0, max=1) => Math.min(Math.max(num, min), max);
const htmns = 'http://www.w3.org/1999/xhtml';
const mmlns = 'http://www.w3.org/1998/Math/MathML';
const svgns = 'http://www.w3.org/2000/svg';
const EU = {
	"apnd": function(elmt, a) {
		var i;
		if (!a) return;
		if (Array.isArray(a)) {
			for (i in a) EU.apnd(elmt, a[i]);
			return;
		}
		elmt.appendChild(a);
	},
	"append": function(elmt, a) {
		EU.apnd(elmt, a);
		return elmt;
	},
	"atcl": function(elmt, a='') {
		if (Array.isArray(a)) return EU.ca(elmt, a);
		if (typeof a == 'object') return EU.atts(elmt, a);
		return EU.ca(elmt, a);
	},
	"att": (elmt, nam, val = '')=> elmt.setAttribute(nam, String(val)),
	"atts": function(elmt, atts = {}) {
		var str;
		for (str in atts) {
			EU.att(elmt, str, String(atts[str]));
		}
	},
	"ca": function(elmt,v='') {
		var i;
		if (!v) return;
		if (Array.isArray(v)) {
			for (i in v) EU.ca(elmt, v[i]);
			return;
		}
		elmt.classList.add(v);
	},
	"el": (tag, ns)=> document.createElementNS(ns, tag),
	"mix": function(elmt, a=[]) {
		var i;
		for (i in a) EU.atcl(elmt, a[i]);
	},
	"mt": function(elmt) {
		while (elmt.firstChild) {
			elmt.removeChild(elmt.firstChild);
		}
	},
	"l": (elmt, evt, func)=> elmt.addEventListener(evt, func),
	"lc": (elmt, func)=> elmt.addEventListener('click', func),
	"lcx": (elmt, func)=> elmt.addEventListener('change', func),
	"lmd": (elmt, func)=> elmt.addEventListener('mousedown', func),
	"lme": (elmt, func)=> elmt.addEventListener('mouseenter', func),
	"lml": (elmt, func)=> elmt.addEventListener('mouseleave', func),
	"lmm": (elmt, func)=> elmt.addEventListener('mousemove', func),
	"lmo": (elmt, func)=> elmt.addEventListener('mouseover', func),
	"lmt": (elmt, func)=> elmt.addEventListener('mouseout', func),
	"lmu": (elmt, func)=> elmt.addEventListener('mouseup', func),
	"ntx": function(elmt, str) {
		let n = document.createTextNode(String(str));
		elmt.appendChild(n);
	},
	"nu": function(tag, atts, ns) {
		let l = EU.el(tag, ns);
		EU.atts(l, atts, ns);
		EU.xtra(l);
		return l;
	},
	"xtra": function(elmt) {
		elmt.append = (...a)=> EU.append(elmt,a);
		elmt.ga = elmt.querySelectorAll;
		elmt.ge = elmt.querySelector;
		elmt.l = (e,f)=> EU.l(elmt,e,f);
		elmt.lc = (f)=> EU.lc(elmt,f);
		elmt.lcx = (f)=> EU.lcx(elmt,f);
		elmt.lmd = (f)=> EU.lmd(elmt,f);
		elmt.lme = (f)=> EU.lme(elmt,f);
		elmt.lml = (f)=> EU.lml(elmt,f);
		elmt.lmm = (f)=> EU.lmm(elmt,f);
		elmt.lmo = (f)=> EU.lmo(elmt,f);
		elmt.lmt = (f)=> EU.lmt(elmt,f);
		elmt.lmu = (f)=> EU.lmu(elmt,f);
		elmt.mt = (f)=> EU.mt(elmt);
		elmt.ntx = (str = ' ')=> EU.ntx(elmt, str);
		return elmt;
	},
    "dragstart": function(elmt, func) {
        elmt.setAttribute('draggable', true);
        elmt.addEventListener('dragstart', func);
    },
    "dragover": function(elmt, func) {
        elmt.addEventListener('dragover', func);
    },
    "drop": function(elmt, func) {
        elmt.addEventListener('drop', func);
    },
    "makeDraggable": function(elmt, func = null) {
        func = func || function(event) {
            event.dataTransfer.setData('text/plain', event.target.id);
        };
        this.dragstart(elmt, func);
    },
    "makeDropzone": function(elmt, func = null) {
        this.dragover(elmt, function(event) {
            event.preventDefault();  // This is necessary to allow a drop
        });
        func = func || function(event) {
            event.preventDefault();  // This is necessary to prevent redirect
            let data = event.dataTransfer.getData('text/plain');
            event.target.appendChild(document.getElementById(data));
        };
        this.drop(elmt, func);
    },
};
export { clamp, EU, htmns, mmlns, svgns }; */