import { TTScrollbar } from "@teachingtextbooks/ttscrollbar";


export class ScrollTest {
    private textArea: HTMLTextAreaElement;
    private scrollGroup: HTMLElement;
    private ttScrollBar: TTScrollbar;
    constructor() {
        //get the text area reference
        this.textArea = document.getElementsByClassName("testText")[0] as HTMLTextAreaElement;

        //get the scroll group reference - this is the parent element that will contain the scrollbar and
        //the text area or whatever else you want to scroll
        this.scrollGroup = document.getElementsByClassName("scrollGroup")[0] as HTMLElement;

        //create the scrollbar passing in the scrollable element and the parent element to add the scrollbar to
        //as well as a callback function to call when the scrollbar is moved
        
        this.ttScrollBar = new TTScrollbar(this.textArea, this.scrollGroup, this.updateTextArea.bind(this));

        this.ttScrollBar.addCssToHead();
        this.ttScrollBar.setImage("track","./assets/scroll/scrollback.png");
        this.ttScrollBar.setImage("knob","./assets/scroll/thumbScroll.png");
        this.ttScrollBar.setImage("btnUp","./assets/scroll/arrowUp.png");
        this.ttScrollBar.setImage("btnDown","./assets/scroll/arrowDown.png");

        (window as any).scrollTest = this;
    }
    updateTextArea(percent: number) {
        if (this.textArea) {
            let textAreaHeight = this.textArea.scrollHeight - this.textArea.clientHeight;
            let px: number = textAreaHeight / 100 * percent;
            this.textArea.scrollTo(0, px);
        }
    }
}