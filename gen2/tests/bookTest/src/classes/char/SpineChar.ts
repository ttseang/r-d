

//import { SpinePlayer, Vector3 } from "@esotericsoftware/spine-player";

import { SpinePlayer, Vector3 } from "@esotericsoftware/spine-player";
import { PosVo, IScreen } from "svggame";
import { AnimationVo } from "../../dataObjs/AnimationVo";
import { SpineGridTween } from "./SpineGridTween";

import { SpineTween } from "./SpineTween";
import { SpineTweenObj } from "./SpineTweenObj";

export class SpineChar {
    private config: any = {};
    //private config2:SpinePlayerConfig={};

    private player: SpinePlayer;
    private animations: AnimationVo[];

    private divID: string;
    private _x: number = 0;
    private _y: number = 0;
    private _scaleX: number = 1;
    private _scaleY: number = 1;

    public realH: number = 3000;
    public realW: number = 6000;

    private _flip: boolean = false;


    public scene: IScreen;
    public currentPos: PosVo | null = null;

    public el: HTMLElement | null = null;

    constructor(scene: IScreen, divID: string, jsonUrl: string, atlasUrl: string, animations: AnimationVo[]) {
        //let config:SpinePlayerConfig={};



        this.scene = scene;
        this.divID = divID;
        this.animations = animations;

        this.config.atlasUrl = atlasUrl;
        this.config.jsonUrl = jsonUrl;
        this.config.viewport = {
            x: 0,
            y: 0,
            width: 300,
            height: 800
        }
        // this.config.atlasUrl = "./assets/Penny Loafer.atlas";
        //this.config.jsonUrl = "./assets/Penny Loafer.json";
        this.config.showControls = false;
        this.config.premultipliedAlpha = true;
        this.config.alpha = true;
        this.config.success = this.onSuccess.bind(this);

        this.el = document.getElementById(divID);
        if (this.el) {
            this.el.style.visibility = "hidden";
        }
        //console.log(this.config);

        this.player = new SpinePlayer(divID, this.config);

    }

    hide() {
        if (this.el) {
            this.el.style.display = "none";
        }
    }
    show() {
        if (this.el) {
            this.el.style.display = "block";
        }
    }
    getPosition(xx: number, yy: number, zz: number) {
        let w2s: Vector3 = this.player.sceneRenderer.camera.screenToWorld(new Vector3(xx, yy, zz), this.scene.gw, this.scene.gh);

        return w2s;
    }
    public centerMe() {
        let pos: Vector3 = this.getPosition(this.scene.gw / 2, this.scene.gh / 2, 0);
        this.x = pos.x;
        this.y = pos.y;
    }
    public updatePos() {
        if (this.currentPos) {
            this.placeOnGrid(this.currentPos?.x, this.currentPos?.y);
        }

    }
    public placeOnGrid(col: number, row: number) {
        if (this.scene.grid) {
            let xx: number = col * this.scene.grid.cw;
            let yy: number = row * this.scene.grid.ch;

            let pos: Vector3 = this.getPosition(xx, yy, 0);
            this.x = pos.x;
            this.y = pos.y;
        }

    }

    scaleToGameW(per: number) {
        let desiredWidth: number = this.scene.gw * per;


        if (this.player.skeleton) {
            let skelWidth: number = this.player.skeleton.data.width;

            let p2: number = desiredWidth / skelWidth;

            this.scaleX = p2;
            this.scaleY = p2;
        }


    }
    scaleToGameH(per: number) {
        let desiredHeight: number = this.scene.gw * per;

        if (this.player.skeleton) {
            let skelHeight: number = this.player.skeleton.data.height;

            let p2: number = desiredHeight / skelHeight;

            this.scaleX = p2;
            this.scaleY = p2;
        }

    }
    adjustY() {
        let displayHeight: number = this.realH * this.scaleY;
        this.y -= displayHeight;
    }
    adjustX() {
        let displayWidth: number = this.realW * this.scaleX;
        this.x -= displayWidth / 2;
    }

    onSuccess() {
        this.player.play();

        for (let i: number = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }
        setTimeout(() => {


            //trigger setters
            this.flip = this._flip;
            this.x = this._x;
            this.y = this._y;

            if (this.currentPos != null) {
                this.placeOnGrid(this.currentPos.x, this.currentPos.y);
            }

            this.scaleY = this._scaleY;
            this.scaleX = this._scaleX;

            if (this.el) {
                this.el.style.visibility = "visible";
            }
            this.scene.doResize();


        }, 1000);
    }
    setScale(scale: number) {
        this.scaleX = scale;
        this.scaleY = scale;
    }
    setAnimations(animations: AnimationVo[]) {
        this.animations = animations;
        for (let i: number = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }
    }
    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
        if (this.player) {
            if (this.player.skeleton) {
                this.player.skeleton.x = value;
            }
        }
    }
    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
        if (this.player) {
            if (this.player.skeleton) {

                //let sy:number=value-this.realH/2;

                this.player.skeleton.y = value;
            }
        }
    }

    public get scaleX(): number {
        return this._scaleX;
    }
    public set scaleX(value: number) {
        this._scaleX = value;
        if (this.player) {
            if (this.player.skeleton) {

                let v: number = value;
                if (this._flip == true) {
                    v = -v;
                }

                this.player.skeleton.scaleX = v;
            }
        }
    }

    public get scaleY(): number {
        return this._scaleY;
    }
    public set scaleY(value: number) {
        this._scaleY = value;
        if (this.player) {
            if (this.player.skeleton) {
                this.player.skeleton.scaleY = value;
            }
        }
    }
    public get flip(): boolean {
        return this._flip;
    }
    public set flip(value: boolean) {
        this._flip = value;
        this.scaleX = this._scaleX;
    }
    public moveOnGrid(xx: number, yy: number, duration: number = 1000, animations: AnimationVo[], endAnimations: AnimationVo[], flipOnEnd: boolean = false) {
        let tweenObj: SpineTweenObj = new SpineTweenObj();
        tweenObj.x = xx;
        tweenObj.y = yy;
        tweenObj.duration = duration;
        this.setAnimations(animations);

        tweenObj.onComplete = () => {
            this.setAnimations(endAnimations);
            if (flipOnEnd == true) {
                this.flip = !this.flip;
                if (this.currentPos) {
                    this.placeOnGrid(this.currentPos?.x, this.currentPos.y);
                }

            }
        }

        let tween: SpineGridTween = new SpineGridTween(this, tweenObj, true);

    }
    moveTo(xx: number, yy: number, duration: number = 1000, animations: AnimationVo[], endAnimations: AnimationVo[], flipOnEnd: boolean = false) {
        let tweenObj: SpineTweenObj = new SpineTweenObj();
        tweenObj.x = xx;
        tweenObj.y = yy;
        tweenObj.duration = duration;
        this.setAnimations(animations);

        tweenObj.onComplete = () => {
            this.setAnimations(endAnimations);
            if (flipOnEnd == true) {
                this.flip = !this.flip;
                if (this.currentPos) {
                    this.x = xx;
                    this.y = yy;
                    //this.placeOnGrid(this.currentPos?.x,this.currentPos.y);
                }

            }
        }

        let tween: SpineTween = new SpineTween(this, tweenObj, true);



    }
}