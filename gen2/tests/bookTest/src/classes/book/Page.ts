import { BoardBook } from "./BoardBook";
import { PlayButton } from "./PlayButton";
import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { Narrator } from "./Narrator";

export class Page {
  BB: BoardBook;
  page: number;
  folio: number;
  element: SVGForeignObjectElement;
  div: HTMLDivElement | null;
  even: boolean;
  odd: boolean;
  left: boolean;
  right: boolean;
  narrator: Narrator;
  playButton: PlayButton | null = null;
  playButtonHtml: HTMLElement | null | undefined;

  constructor(
    BB: BoardBook,
    foreignObject: SVGForeignObjectElement,
    index: number
  ) {
    this.BB = BB;
    this.folio = 1 + index;
    this.page = this.folio;
    this.element = foreignObject;
    this.div = this.element.querySelector('div');
    this.even = !(this.page % 2);
    this.odd = !this.even;
    this.left = this.even;
    this.right = this.odd;

    if (this.div?.classList.contains('left')) {
      this.left = true;
      this.right = false;
    }

    if (this.div?.classList.contains('right')) {
      this.left = false;
      this.right = true;
    }

    this.narrator = new Narrator(BB, this);
    this.playButtonHtml = this.div?.querySelector('.playpausebtn');

    if (this.playButtonHtml) {
      this.playButton = new PlayButton(this.playButtonHtml);

      this.playButtonHtml.addEventListener(
        'click',
        this.narrator.buttonClick.bind(this.narrator)
      );
    }

    this.hide();
  }

  hide() {
    this.BB.hide(this.element);
  }

  show() {
    this.BB.show(this.element);
  }
}