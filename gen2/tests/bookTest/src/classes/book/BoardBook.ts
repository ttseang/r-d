import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';

import { GameManager, IScreen, SvgObj } from "svggame";
import { PageVo } from '../../dataObjs/PageVo';
import { BookConfig } from './BookConfig';
import { Page } from "./Page";
import { Sheet } from './Sheet';
import { Stack } from './Stack';
import { Turner } from './Turner';
import { UserInterface } from './UserInterface';

export class BoardBook extends SvgObj {



  public config: BookConfig;

  public body: HTMLBodyElement | null = null;
  public main: HTMLElement | null | undefined;
  public book: HTMLObjectElement | null | undefined;
  public pages: Page[] = [];
  public sheets: Sheet[] = [];
  public pageCount:number=0;
  public pageData:PageVo[]=[];

  public stacks: { left: Stack | null; right: Stack | null } = {
    left: null,
    right: null,
  };
  turner: Turner | null = null;
  ui: UserInterface | null = null;
  defs: SVGDefsElement | null | undefined;
  svg: SVGSVGElement | null | undefined;

  //-------------|
  // Constructor |
  //-------------|

  constructor(screen: IScreen, config: BookConfig) {
    super(screen, "boardbookobj", "bookArea");

    this.config = config;
    this.body = GU.doc.querySelector('body');
    this.main = this.body?.querySelector('main');
    
    if (this.el) {
      this.book = this.el as HTMLObjectElement;
    }


    let aa: HTMLElement | null = document.getElementById("bookArea");
    this.svg = aa?.querySelector("#boardbookobj-1") as SVGSVGElement;

    this.defs = document.getElementsByTagName("defs")[0];
    

    this.stacks = {
      left: new Stack(this, 'left'),
      right: new Stack(this, 'right'),
    };
    this.turner = new Turner(this);
    this.ui = new UserInterface(this);
    this.loadContent();

    (window as any).book=this;
  }

  //---------|
  // Methods |
  //---------|
  public get pageIndex()
  {
    return this.ui?.pageIndex;
  }
  public get rightIndex()
  {
    return this.ui?.rightPage || -1;
  }
  public get leftIndex()
  {
    return this.ui?.leftPage || -1;
  }
  public hide(element: Element) {
    GU.Classes.add(element, 'hid');
  }

  public show(element: Element) {
    GU.Classes.remove(element, 'hid');
  }

  loadContent() {
    function definePages(BB: BoardBook, xml: XMLDocument) {
      const pages = xml.querySelectorAll('foreignObject');
      pages.forEach((page) => {
        BB.defs?.appendChild(page);
      });
      BB.loadPageData();
    }

    function failure(reason: any) {
      //console.log('loadContent() failed with reason: ' + reason);
    }

    GU.Asynchronous.call(this.config.templateFile, 'GET', null, 'xml', false)
      .then((result) => {
        definePages(this, result);
      })
      .catch((err) => {
        failure(err);
      });
  }
  loadPageData() {
    fetch("./assets/data.json")
    .then(response => response.json())
    .then(data => this.gotPageData(data));
  }
  gotPageData(data:any)
  {
    let pageJson:any=data.data.pages;
    let pageData:PageVo[]=[];

    for (let i:number=0;i<pageJson.length;i++)
    {
      let tempID:number=parseInt(pageJson[i].templateID);
      let pageVo:PageVo=new PageVo(tempID,pageJson[i].texts,pageJson[i].images,"");
      pageData.push(pageVo);
    }

    pageData[0].isCover=true;
    pageData[pageData.length-1].isCover=true;
    this.pageCount=pageData.length;
    this.pageData=pageData;

    this.loadPages(pageData);
  }
  loadPages(pageData: PageVo[]) {  

    for (let i: number = 0; i < pageData.length; i++) {
      this.loadAPage(i, pageData[i]);
    }
    this.loadSheets();
  }
  loadAPage(index: number, pageData: PageVo) {


    let templateName: string = "pt" + pageData.templateID.toString();
    let def: HTMLElement | null = document.getElementById(templateName);
    if (def) {
      let el: SVGForeignObjectElement = def.cloneNode(true) as SVGForeignObjectElement;

      let div1: HTMLElement = el.getElementsByTagName("div")[0];

      div1.classList.remove("left");
      div1.classList.remove("right");

      if (index / 2 == Math.floor(index / 2) && pageData.isCover==false) {
        div1.classList.add("right");
      }
      else {
        div1.classList.add("left");
      }

      let textF: NodeListOf<Element> = el.querySelectorAll("p,h1,h2,h3,h4,h5");

      for (let i: number = 0; i < pageData.text.length; i++) {
        if (textF[i]) {
          textF[i].innerHTML = pageData.text[i];
        }
      }

      let imageF: NodeListOf<HTMLElement> = el.querySelectorAll("figure");

      if (pageData.templateID!=6)
      {
     

      for (let i: number = 0; i < pageData.images.length; i++) {
        if (imageF[i]) {
          let image:string="./assets/"+pageData.images[i];
          imageF[i].style.backgroundImage="url("+image+")";
        }
      }
    }
    else
    {
        
        let divs:NodeListOf<HTMLElement>=el.querySelectorAll("div");

        if (divs[0] && pageData.images.length>0)
        {
            let spinArray:string[]=pageData.images[0].split("-");

            if (spinArray.length>0)
            {
               let spinName:string=spinArray[1];
               divs[0].id=spinName;
            }

           

        }
    }

      if (el)
      {      
      if (pageData.audio=="")
      {
         let playButton:HTMLElement | null=el.querySelector(".playpausebtn");
         if (playButton)
         {
            playButton.hidden=true;
         }
      }
    }

      this.pages.push(new Page(this, el, index));
    }


  }

 

  loadSheets() {
    // Create sheets
    for (let index = 0; index < this.pages.length / 2; index++) {
      this.sheets.push(new Sheet(this, index));
    }

    // Create stacks
    for (let index = 0; index < this.sheets.length; index++) {
      this.stacks.right?.add(this.sheets[this.sheets.length - 1 - index]);
    }

    // if (this.stacks.left?.quantity) this.ui?.prevBtn.enable();
    //if (this.stacks.right?.quantity) this.ui?.nextBtn.enable();
  }

}