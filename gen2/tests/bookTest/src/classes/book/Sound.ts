import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';

export class Sound {
    element: HTMLAudioElement;
    textBlock: number;

    constructor(fileName: string, textBlock = 0) {
      this.element = GU.Elements.createHTMLElement('audio', {
        preload: 'auto',
      }) as HTMLAudioElement;
      this.textBlock = textBlock;
      this.addSource(fileName);
    }

    addSource(fileName: string) {
      const source = GU.Elements.createHTMLElement('source', {
        src: `./assets/mp3/${fileName}.mp3`,
        type: 'audio/mpeg',
      });

      return this.element.appendChild(source);
    }

    toMilliseconds() {
      return 1000 * this.element.currentTime;
    }
  }