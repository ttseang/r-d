export class PlayButton {
    element: HTMLElement;
    mode: 'pause' | 'play' = 'play';

    constructor(element: HTMLElement) {
      this.element = element;
    }

    bePause() {
      this.mode = 'pause';
      this.element.classList.add('pause');
    }

    bePlay() {
      this.mode = 'play';
      this.element.classList.remove('pause');
    }
  }
