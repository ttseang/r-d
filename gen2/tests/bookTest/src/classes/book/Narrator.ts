
import { BoardBook } from "./BoardBook";
import { Page } from "./Page";
import { Sound } from "./Sound";
import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';

export class Narrator {
    audio: Sound[] | Sound | null = null;
    BB: BoardBook;
    currentHighlight: string | undefined;
    currentBlock: number | null = null;
    currentSound: Sound | null = null;
    engine: string | null = null;
    highLights: any[] | null = null;
    isPlaying = false;
    narrationHighlight: string | undefined;
    nodeList: NodeList | undefined;
    page: Page;
    parentNarrator: Narrator;
    svg: SVGSVGElement | null | undefined;
    svgClass = 'h0';
    textBlocks: HTMLElement[] = [];

    constructor(BB: BoardBook, page: Page) {
      this.BB = BB;
      this.page = page;
      this.parentNarrator = this;

      if (page.div?.classList.contains('SVGNHL')) {
        this.initSVG();
      } else if (page.div?.classList.contains('HTMNHL')) {
        this.initHTM();
      }

      BB.book?.addEventListener(
        'pageflip',
        this.forcePause.bind(this.parentNarrator)
      );
    }

    buttonClick() {
      this.parentNarrator.isPlaying
        ? this.parentNarrator.pause()
        : this.parentNarrator.play();
    }

    end() {
      let nextPosition: number | null = null;

      if (
        Number(this.parentNarrator.currentBlock) <
        this.parentNarrator.textBlocks.length - 1
      ) {
        nextPosition = 1 + Number(this.parentNarrator.currentBlock);
        this.parentNarrator.highlight(nextPosition);
        this.parentNarrator.play();
      } else {
        this.parentNarrator.highlight(nextPosition);
        this.parentNarrator.stop();
      }
    }

    forcePause() {
      if (this.parentNarrator.isPlaying) this.parentNarrator.pause();
    }

    highlight(block: number | null) {
      if (block === this.parentNarrator.currentBlock) return;

      this.parentNarrator.textBlocks[
        Number(this.parentNarrator.currentBlock)
      ].classList.remove('hilit');
      this.parentNarrator.currentBlock = block;

      if (block !== null)
        this.parentNarrator.textBlocks[block].classList.add('hilit');
    }

    htmHandler() {
      this.parentNarrator.highlight(this.parentNarrator.currentBlock);
    }

    initHTM() {
      this.engine = 'HTM';
      this.audio = [];
      this.nodeList = this.page.div?.querySelectorAll('*[data-nhl]');

      if (this.page.right && !this.page.playButton) {
        this.parentNarrator = this.BB.pages[this.BB.pages.length - 1].narrator;
      }

      this.nodeList?.forEach((node) => {
        const nodeElement = node as HTMLElement;
        this.parentNarrator.textBlocks.push(nodeElement);

        const currentHighlight = nodeElement.dataset.nhl || '';
        const currentSound = new Sound(currentHighlight);

        currentSound.element.addEventListener(
          'play',
          this.parentNarrator.start.bind(this)
        );
        currentSound.element.addEventListener(
          'ended',
          this.parentNarrator.end.bind(this)
        );
        currentSound.element.addEventListener(
          'pause',
          this.parentNarrator.stop.bind(this)
        );
        currentSound.element.addEventListener(
          'timeupdate',
          this.parentNarrator.htmHandler.bind(this)
        );

        (this.parentNarrator.audio as Sound[]).push(currentSound);
      });
    }

    initSVG() {
      this.engine = 'SVG';
      this.svg = this.page.div?.querySelector('svg');

      if (this.svg) {
        this.narrationHighlight = this.svg.dataset.nhl;

        if (this.narrationHighlight) {
          this.audio = new Sound(this.narrationHighlight);
          this.audio.element.addEventListener('play', this.start.bind(this));
          this.audio.element.addEventListener('ended', this.end.bind(this));
          this.audio.element.addEventListener('pause', this.stop.bind(this));
          this.audio.element.addEventListener(
            'timeupdate',
            this.svgHighlightHandler.bind(this)
          );
        }

        this.narrationHighlight = this.svg.dataset.hilites;

        if (this.narrationHighlight) {
          this.highLights = JSON.parse(this.narrationHighlight);
        }
      }
    }

    pause() {
      if (
        this.parentNarrator.svg &&
        this.parentNarrator.audio instanceof Sound
      ) {
        this.parentNarrator.audio.element.pause();
      } else if (Array.isArray(this.parentNarrator.audio)) {
        this.parentNarrator.audio[
          Number(this.parentNarrator.currentBlock)
        ].element.pause();
      }
    }

    play() {
      if (
        this.parentNarrator.svg &&
        this.parentNarrator.audio instanceof Sound
      ) {
        this.parentNarrator.audio.element.play();
      } else {
        if (this.parentNarrator.currentBlock === null)
          this.parentNarrator.highlight(0);
        if (Array.isArray(this.parentNarrator.audio)) {
          this.parentNarrator.audio[
            Number(this.parentNarrator.currentBlock)
          ].element.play();
        }
      }
    }

    start() {
      this.parentNarrator.isPlaying = true;
      this.parentNarrator.page.playButton?.bePause();
    }

    stop() {
      this.parentNarrator.isPlaying = false;
      this.parentNarrator.page.playButton?.bePlay();
    }

    svgHighlight(className: string) {
      if (className === this.svgClass) return;

      GU.Classes.remove(this.svg as Element, this.svgClass);

      this.svgClass = className;
      GU.Classes.add(this.svg as Element, this.svgClass);
    }

    svgHighlightHandler() {
      const time = (this.audio as Sound).toMilliseconds();
      let className = 'h0';

      this.highLights?.forEach((element: any) => {
        if (time >= element[0] && time < element[1]) className = element[2];
      });

      this.svgHighlight(className);
    }
  }