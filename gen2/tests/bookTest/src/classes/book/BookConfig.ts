export class BookConfig
{
    public templateFile:string;
    public duration:number;
    public frameRate:number;
    public pageWidth:number;
    public pageHeight:number;

    constructor(templateFile:string,duration:number,frameRate:number,pageWidth:number,pageHeight:number)
    {
        this.templateFile=templateFile;
        this.duration=duration;
        this.frameRate=frameRate;
        this.pageWidth=pageWidth;
        this.pageHeight=pageHeight;
    }
}
