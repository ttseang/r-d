import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { BoardBook } from './BoardBook';
import { Sheet } from './Sheet';

export class Turner {
    BB: BoardBook;
    back: Element | null | undefined;
    backG: Element | null | undefined;
    element: Element | null | undefined;
    front: Element | null | undefined;
    frontG: Element | null | undefined;
    rotation = 90;
    shadow: Element | null | undefined;
    sheet: Sheet | null = null;
    target = -90;
    isTurning = false;

    constructor(BB: BoardBook) {
      this.BB = BB;
      this.shadow = BB.svg?.querySelector('#pages>g.pgshadow');
      this.element = BB.svg?.querySelector('#pages>g.activepg');
      this.front = this.element?.querySelector('g.right');
      this.frontG = this.front?.querySelector('g');
      this.back = this.element?.querySelector('g.left');
      this.backG = this.back?.querySelector('g');
      this.hide();
    }

    distort(element: SVGSVGElement, degrees: number) {
      const radians = (degrees * Math.PI) / 180;
      const isBack = element == this.back;
      const isShadow = element == this.shadow;
      let sinX = (isBack ? -1 : 1) * Math.sin(radians);
      if (isShadow) sinX = sinX ** 3;

      const sinY =
        ((isBack || isShadow ? 1 : -1) * Math.cos(radians)) /
        (isShadow ? 16 : 6);

      const width = isBack ? this.BB.config.pageWidth : 0;
      const height = isBack ? this.BB.config.pageHeight : 0;
      const transform = this.BB.svg?.createSVGTransform() as SVGTransform;
      const matrix = GU.Matrices.get(this.BB.svg as SVGSVGElement, [
        sinX,
        sinY,
        0,
        1,
        0 - width * sinX,
        0 - height * sinY,
      ]);
      transform.setMatrix(matrix);
      element.transform.baseVal.clear();
      element.transform.baseVal.appendItem(transform as SVGTransform);
    }

    hide() {
      this.BB.hide(this.front as Element);
      this.BB.hide(this.back as Element);
      this.BB.hide(this.shadow as Element);
    }

    load(sheet: Sheet, direction = 1) {
      
      const newDirection = direction < 0 ? -1 : 1;
      this.isTurning = true;
      this.rotation = 90 * newDirection;
      this.target = -90 * newDirection;




      this.sheet = sheet;
      sheet.show('all');
      this.frontG?.appendChild(sheet.front.element);
      this.backG?.appendChild(sheet.back.element);
      this.show();
      this.rotate(this.rotation);
      this.turn(newDirection);
    }

    rotate(degrees: number) {
      const newDegrees = Math.min(90, Math.max(-90, degrees));
      const seen = newDegrees < 0 ? this.back : this.front;
      const unseen = newDegrees < 0 ? this.front : this.back;

      this.BB.hide(unseen as Element);
      this.BB.show(seen as Element);
      this.distort(seen as SVGSVGElement, newDegrees);

      if (Math.abs(newDegrees) === 90) {
        this.BB.hide(this.shadow as Element);
      } else {
        this.BB.show(this.shadow as Element);
        this.distort(this.shadow as SVGSVGElement, newDegrees);
      }
    }

    show() {
      this.BB.show(this.front as Element);
      this.BB.show(this.back as Element);
      this.BB.show(this.shadow as Element);
    }

    turn(direction = 1) {
      let interval = Math.round(1000 / this.BB.config.frameRate);
      let numSteps = Math.round(this.BB.config.duration / interval);
      let step = 0;

      this.turnBySteps(step, numSteps, interval, direction);
    //  this.unload(direction);
    }

    turnBySteps(
      step: number,
      numSteps: number,
      interval: number,
      direction?: number
    ) {
      step += 1;
      let iterations = (this.target - this.rotation) / numSteps;
      let degreesToRotate = this.rotation + step * iterations;

      if (step >= numSteps) degreesToRotate = this.target;

      this.rotate(degreesToRotate);

      if (step < numSteps) {
        setTimeout(() => {
          this.turnBySteps(step, numSteps, interval, direction);
        }, interval);
      } else if (direction) {
        this.unload(direction);
      }
    }

    unload(direction = 1) {
      let sheet = this.sheet;
      this.isTurning = false;

      if (direction < 0) {
        this.BB.stacks.right?.add(sheet as Sheet);
      } else {
        this.BB.stacks.left?.add(sheet as Sheet);
      }

      this.sheet = null;
      GU.Elements.empty(this.frontG as Element);
      GU.Elements.empty(this.backG as Element);
      this.hide();

     // if (this.BB.stacks.left?.quantity) this.BB.ui?.prevBtn.enable();
      //if (this.BB.stacks.right?.quantity) this.BB.ui?.nextBtn.enable();
    }
  }
