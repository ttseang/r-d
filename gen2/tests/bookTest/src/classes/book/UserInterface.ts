import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { BoardBook } from './BoardBook';
import { UIButton } from './UIButton';

export class UserInterface {
  BB: BoardBook;
  prevBtn: SVGElement | null = null;
  nextBtn: SVGElement | null = null;
  public pageIndex: number = 0;
  public rightPage:number=0;
  public leftPage:number=-1;

  constructor(BB: BoardBook) {
    this.BB = BB;
    if (this.BB.el) {
      let buttons: HTMLCollectionOf<SVGElement> = this.BB.el?.getElementsByClassName("uibutton") as HTMLCollectionOf<SVGElement>;
      this.prevBtn = buttons[0];
      this.nextBtn = buttons[1];
      this.nextBtn.onclick = () => {
        this.nextPage();
      }
      this.prevBtn.onclick = () => {
        this.prevPage();
      }
    }

    //this.prevBtn = new UIButton(this.BB, 'prev_button');
    //  this.prevBtn.element.addEventListener('click', this.prevPage.bind(this));
    //this.nextBtn = new UIButton(this.BB, 'next_button');
    //this.nextBtn.element.addEventListener('click', this.nextPage.bind(this));

    // Todo: Learn to make this work
    // this.prevBtn.addClickListener(this.prevPage, this);
    // this.nextBtn.addClickListener(this.nextPage, this);
  }

  public nextPage() {
    if (this.BB.stacks.right?.quantity && !this.BB.turner?.isTurning) {
      // this.BB.ui?.prevBtn.disable();
      // this.BB.ui?.nextBtn.disable();

      this.pageIndex++;
      this.leftPage+=2;
      this.rightPage+=2;
      if (this.pageIndex==this.BB.pageCount/2)
      {
        this.rightPage=-1;
      }

      GU.Events.fireNew('pageflip', this.BB.book as HTMLElement);

      const sheet = this.BB.stacks.right.remove();


      if (sheet) this.BB.turner?.load(sheet, 1);
    }
  }
  public prevPage() {
    if (this.BB.stacks.left?.quantity && !this.BB.turner?.isTurning) {
      //  this.BB.ui?.prevBtn.disable();
      //  this.BB.ui?.nextBtn.disable();
      this.pageIndex--;
      if (this.pageIndex==0)
      {
        this.rightPage=0;
        this.leftPage=-1;
      }
      else
      {
        this.rightPage-=2;
        this.leftPage-=2;
      }
      GU.Events.fireNew('pageflip', this.BB.book as HTMLElement);
      const sheet = this.BB.stacks.left.remove();


      if (sheet) this.BB.turner?.load(sheet, -1);
    }
  }
}
