import { BoardBook } from "./BoardBook";
import { Page } from "./Page";

export class Sheet {
    front: Page;
    back: Page;
    stack: 'left' | 'right' | '';

    constructor(BB: BoardBook, index: number) {
      this.front = BB.pages[2 * index];
      this.back = BB.pages[1 + 2 * index];
      this.stack = '';
      this.hide();
    }

    hide() {
      this.front.hide();
      this.back.hide();
    }

    show(side = 'up') {
      const s = side.charAt(0);
      this.hide();

      switch (s) {
        case 'n':
          break;

        case 'a':
          this.back.show();
          this.front.show();
          break;

        case 'f':
          this.front.show();
          break;

        case 'b':
          this.back.show();
          break;

        case 'u':
          switch (this.stack) {
            case 'left':
              this.back.show();
              break;

            case 'right':
              this.front.show();

            default:
              break;
          }
          break;

        default:
          break;
      }
    }
  }