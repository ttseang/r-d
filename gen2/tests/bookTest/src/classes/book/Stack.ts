import { BoardBook } from "./BoardBook";
import { Sheet } from "./Sheet";

import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';

export class Stack {
    BB: BoardBook;
    element?: Element | null | undefined;
    paper: Element | null | undefined;
    pageOrientation: 'back' | 'front';
    quantity = 0;
    sheets: Sheet[] = [];
    side: 'left' | 'right';

    constructor(BB: BoardBook, side: Stack['side']) {
      this.BB = BB;
      let elName:string='#pages>g.' + side + 'pg g';

      this.element = this.BB.book?.querySelector(elName);
      this.paper = this.BB.svg?.querySelector('#pages>g.' + side + 'pg use');
      this.pageOrientation = side === 'left' ? 'back' : 'front';
      this.side = side;
      this.BB.hide(this.paper as Element);
    }

    add(sheet: Sheet) {
      if (this.quantity) this.sheets[0].hide();
      
      sheet.stack = this.side;
      sheet.show();
      this.quantity = this.sheets.unshift(sheet);
      GU.Elements.empty(this.element as Element);

      let sheetEl=sheet[this.pageOrientation].element;
      
      this.element?.appendChild(sheet[this.pageOrientation].element);
      this.BB.show(this.paper as Element);
    }

    remove() {
      if (!this.quantity) return null;

      this.sheets[0].hide();
      const sheet = this.sheets.shift();
      this.quantity = this.sheets.length;
      GU.Elements.empty(this.element as Element);

      if (this.quantity) {
        this.sheets[0].show();
        this.element?.appendChild(this.sheets[0][this.pageOrientation].element);
      } else {
        this.BB.hide(this.paper as Element);
      }

      return sheet;
    }
  }