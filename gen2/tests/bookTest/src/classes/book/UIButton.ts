import { BoardBook } from "./BoardBook";

export class UIButton {
    BB: BoardBook;
    enabled = false;
    element: HTMLElement;

    constructor(BB: BoardBook, id: string) {
      this.BB = BB;
      this.element = this.BB.svg?.querySelector('#' + id) as HTMLElement;
    }

    enable() {
      this.enabled = true;
      this.element.classList.remove('disabled');
    }

    disable() {
      this.enabled = false;
      this.element.classList.add('disabled');
    }

    // Todo: Learn to make this work
    // addClickListener(callback: Function, thisReference: any) {
    //   this.element.addEventListener('click', callback().bind(thisReference));
    // }
  }