export class PageVo
{
    public templateID:number;
    public text:string[];
    public images:string[];
    public audio:string;
    public isCover:boolean=false;
    constructor(templateID:number,text:string[],images:string[],audio:string="")
    {
        this.templateID=templateID;
        if (text==undefined)
        {
            text=[];
        }
        this.text=text;
        this.images=images;
        this.audio=audio;
    }
    getTag()
    {
        if (this.images.length>0)
        {
            let tagArray:string[]=this.images[0].split("-");
            if (tagArray.length>0)
            {
                return tagArray[1];
            }
        }
        return "";
    }
}