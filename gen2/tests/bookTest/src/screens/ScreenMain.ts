
import { BaseScreen, IScreen, PosVo, SvgObj } from "svggame";
import { BoardBook } from "../classes/book/BoardBook";
import { BookConfig } from "../classes/book/BookConfig";


import { LinkUtil } from "../classes/util/LinkUtil";


import { WordVo } from "../dataObjs/WordVo";
import { SpineChar } from "../classes/char/SpineChar";
import { AnimationVo } from "../dataObjs/AnimationVo";

import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { PageVo } from "../dataObjs/PageVo";

export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private levels: WordVo[] = [];
    private levelIndex: number = -1;
    private currentWord: WordVo = new WordVo("", []);

    public book: BoardBook;

    private idleAnimations: AnimationVo[] = [];
    private walkAnimations: AnimationVo[] = [];
    private fidgetAnimations: AnimationVo[] = [];
    private fidgetAnimations2: AnimationVo[] = [];

    private spineChar!: SpineChar;

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);
        (window as any).scene = this;

        let config: BookConfig = new BookConfig("./assets/xml/testpages2.xml", 330, 30, 300, 300);
        this.book = new BoardBook(this, config);

        this.idleAnimations = [new AnimationVo("idle", true), new AnimationVo("Repeating animations/blink", true)];
        this.walkAnimations = [new AnimationVo("walk", true), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations = [new AnimationVo("fidget01", false), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations2 = [new AnimationVo("fidget02", false), new AnimationVo("Repeating animations/blink", true)];




        // this.book.x=200;
    }
    create() {
        super.create();

        this.book.gameWRatio = 0.5;

        this.book.el?.addEventListener("pageflip",this.onPageFlip.bind(this))

        this.makePuppet();
    }
    onPageFlip()
    {
        /* console.log("FLIP");
        console.log(this.book.pageIndex);

        console.log("left "+this.book.leftIndex);
        console.log("right "+this.book.rightIndex); */

        let leftType:number=-1;
        let rightType:number=-1;

        let leftPage:PageVo | null=null;
        let rightPage:PageVo | null=null;

        if (this.book.leftIndex!=-1)
        {
            leftPage=this.book.pageData[this.book.leftIndex];
            if (leftType)
            {
                leftType=leftPage.templateID;
            }            
        }
        if (this.book.rightIndex!=-1)
        {
            rightPage=this.book.pageData[this.book.rightIndex];
            if (rightPage)
            {
                rightType=rightPage.templateID;
            }
            
        }

       // console.log("left type "+leftType);
       // console.log("right type "+rightType);
        
        
        if (leftType==6 && leftPage)
        {
            let leftTag:string=leftPage.getTag();
          
            setTimeout(() => {
                this.attachSpine(leftTag);      
            }, 500);
        }

        if (rightType==6 && rightPage)
        {
            let rightTag:string=rightPage.getTag();
            setTimeout(() => {
                this.attachSpine(rightTag);    
            }, 500);
            
        }

    }
    attachSpine(tag:string)
    {
        console.log(tag);

        let div:HTMLElement | null | undefined=this.book.el?.querySelector("#"+tag);
        console.log(div);

        if (div)
        {
            switch(tag)
            {
                case "puppet":
                if (this.spineChar.el)
                {
                    div.append(this.spineChar.el);
                    this.spineChar.show();
                }
                
                break;
            }
        }
    }
    makePuppet() {
        this.spineChar = new SpineChar(this, "mascot", "./assets/char/Amka.json", "./assets/char/Amka.atlas", this.idleAnimations);
        //this.spineChar.currentPos = new PosVo(0, 0);


        setTimeout(() => {
            this.spineChar.scaleToGameW(.4);
            this.spineChar.hide();
            this.spineChar.x = 150;
            this.spineChar.y = -100;
        }, 1000);
        // this.spineChar.adjustY();
    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        data = data.data;

        for (let i: number = 0; i < data.length; i++) {
            let wordVo: WordVo = new WordVo(data[i].word, data[i].letters);
            this.levels.push(wordVo);
        }
        //console.log(data);
        this.nextLevel();



        LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", "");
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");
    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics2/" + image + ".svg";

    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
        switch (action) {
            case "reset":

                break;

            case "check":
                this.checkCorrect();
                break;
        }
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            console.log("out of words");
            return;
        }
        this.makeBoxes();
    }
    makeBoxes() {


    }

    checkCorrect() {



    }
    doResize() {
        super.doResize();


        // this.grid?.showGrid();


    }

}