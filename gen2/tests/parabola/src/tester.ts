import { ParabolaPathBuilder } from "./parabolaPathBuilder";
import { Point } from "./Point";

export class Tester
{
    //http://apex.infogridpacific.com/dcp/svg-primitives-parabolas.html#parabola
    constructor()
    {
        this.addEventListeners();
        this.onTextChange();
    }
    //listen for texta, textb, textc, textx1 and textx2 change events 
    addEventListeners()
    {
        let textA: HTMLInputElement = <HTMLInputElement>document.getElementById("texta");
        let textB: HTMLInputElement = <HTMLInputElement>document.getElementById("textb");
        let textC: HTMLInputElement = <HTMLInputElement>document.getElementById("textc");
        let textX1: HTMLInputElement = <HTMLInputElement>document.getElementById("textx1");
        let textX2: HTMLInputElement = <HTMLInputElement>document.getElementById("textx2");
        
        textA.addEventListener("change", this.onTextChange.bind(this));
        textB.addEventListener("change", this.onTextChange.bind(this));
        textC.addEventListener("change", this.onTextChange.bind(this));
        textX1.addEventListener("change", this.onTextChange.bind(this));
        textX2.addEventListener("change", this.onTextChange.bind(this));


    }
    onTextChange()
    {
        let textA: HTMLInputElement = <HTMLInputElement>document.getElementById("texta");
        let textB: HTMLInputElement = <HTMLInputElement>document.getElementById("textb");
        let textC: HTMLInputElement = <HTMLInputElement>document.getElementById("textc");
        let textX1: HTMLInputElement = <HTMLInputElement>document.getElementById("textx1");
        let textX2: HTMLInputElement = <HTMLInputElement>document.getElementById("textx2");

        let a: number = parseFloat(textA.value);
        let b: number = parseFloat(textB.value);
        let c: number = parseFloat(textC.value);
        let x1: number = parseFloat(textX1.value);
        let x2: number = parseFloat(textX2.value);



        let pathBuilder: ParabolaPathBuilder = new ParabolaPathBuilder();
       let path: string = pathBuilder.buildPath(a, b, c, x1, x2);
        //let path2: string = pathBuilder.buildPathWithQuadraticBezierCurves(a, b, c, x1, x2);
       //let path3:number[]=pathBuilder.calculateDerivatives(a, b, c, x1, x2);
      //  console.log(path3);
      //  console.log(path3[path3.length/2]);
        
        let pathElement: SVGPathElement = document.getElementById("test") as unknown as SVGPathElement;
        pathElement.setAttribute("d", path);
        
    }
    
    
}