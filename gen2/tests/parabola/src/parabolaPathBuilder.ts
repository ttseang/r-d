import { Point } from "./Point";


export class ParabolaPathBuilder {
    
    private pixelPerUnitX: number = 20;
    private pixelPerUnitY: number = 20;
   // private vertexControlPoint: Point = new Point(0, 2000);

    constructor() {

    }

    //build an SVG path string for a parabola with the given parameters
    // a is the coefficient that multiplies x²
  // b is the coefficient that multiplies x
  // c is the final coefficient
  // x1 is the start value for the x axis in a graph
  // x2 is the end value for the x axis in a graph
  //use the pixelPerUnitX and pixelPerUnitY properties to convert the graph coordinates to pixel coordinates
   public buildPath(a: number, b: number, c: number, x1: number, x2: number): string {
        let path: string = "";
        
        let x: number = x1;
        let y: number = a * x * x + b * x + c;
        let point: Point = new Point(x, y);
        let pixelPoint: Point = this.convertToPixelCoordinates(point);
        path += "M" + pixelPoint.x + " " + pixelPoint.y + " ";
        let xStep: number = 0.1;
        x += xStep;
        while (x <= x2) {
            y = a * x * x + b * x + c;
            point = new Point(x, y);
            pixelPoint = this.convertToPixelCoordinates(point);
            
            path += "L" + pixelPoint.x + " " + pixelPoint.y + " ";
            x += xStep;
        }
        return path;
    }
    //use quadratic bezier curves to draw a parabola with the given parameters
    // a is the coefficient that multiplies x²
    // b is the coefficient that multiplies x
    // c is the final coefficient
    // x1 is the start value for the x axis in a graph
    // x2 is the end value for the x axis in a graph
    //use the pixelPerUnitX and pixelPerUnitY properties to convert the graph coordinates to pixel coordinates
    public buildPathWithQuadraticBezierCurves(a: number, b: number, c: number, x1: number, x2: number): string {
        let path: string = "";
        let x: number = x1;
        let y: number = a * x * x + b * x + c;
        let point: Point = new Point(x, y);
        let pixelPoint: Point = this.convertToPixelCoordinates(point);
        path += "M" + pixelPoint.x + " " + pixelPoint.y + " ";
        let xStep: number = 0.005;
        x += xStep;
        while (x <= x2) {
            y = a * x * x + b * x + c;
            point = new Point(x, y);
            pixelPoint = this.convertToPixelCoordinates(point);
            let controlPoint: Point = new Point(x, 0);
            let pixelControlPoint: Point = this.convertToPixelCoordinates(controlPoint);
            path += "Q" + pixelControlPoint.x + " " + pixelControlPoint.y + " " + pixelPoint.x + " " + pixelPoint.y + " ";
            x += xStep;
        }



        return path;
    }
    convertToPixelCoordinates(point: Point): Point {
        let pixelPoint: Point = new Point(0, 0);
        pixelPoint.x = point.x * this.pixelPerUnitX;
        pixelPoint.y = point.y * this.pixelPerUnitY;
        //round to 4 decimal places
        pixelPoint.x = Math.round(pixelPoint.x * 10000) / 10000;
        pixelPoint.y = Math.round(pixelPoint.y * 10000) / 10000;
        return pixelPoint;
    }
   
    //calculate the cubic bezier control points for a parabola with the given parameters
    // a is the coefficient that multiplies x²
    // b is the coefficient that multiplies x
    // c is the final coefficient
    // x1 is the start value for the x axis in a graph
    // x2 is the end value for the x axis in a graph
    //use the pixelPerUnitX and pixelPerUnitY properties to convert the graph coordinates to pixel coordinates
    public calculateCubicBezierControlPoints(a: number, b: number, c: number, x1: number, x2: number): Point[] {
        let controlPoints: Point[] = [];
        let x: number = x1;
        let y: number = a * x * x + b * x + c;
        let point: Point = new Point(x, y);
        let pixelPoint: Point = this.convertToPixelCoordinates(point);
        controlPoints.push(pixelPoint);
        let xStep: number = 0.005;
        x += xStep;
        while (x <= x2) {
            y = a * x * x + b * x + c;
            point = new Point(x, y);
            pixelPoint = this.convertToPixelCoordinates(point);
            controlPoints.push(pixelPoint);
            x += xStep;
        }
        return controlPoints;
    }
    //calculate dx, dy, d2x, d2y, d3x, d3y for a parabola with the given parameters
    // a is the coefficient that multiplies x²
    // b is the coefficient that multiplies x
    // c is the final coefficient
    // x1 is the start value for the x axis in a graph
    // x2 is the end value for the x axis in a graph
    //use the pixelPerUnitX and pixelPerUnitY properties to convert the graph coordinates to pixel coordinates
    public calculateDerivatives(a: number, b: number, c: number, x1: number, x2: number): number[] {
        let derivatives: number[] = [];
        let x: number = x1;
        let y: number = a * x * x + b * x + c;
        let point: Point = new Point(x, y);
        let pixelPoint: Point = this.convertToPixelCoordinates(point);
        derivatives.push(pixelPoint.x);
        derivatives.push(pixelPoint.y);
        let xStep: number = 0.005;
        x += xStep;
        while (x <= x2) {
            y = a * x * x + b * x + c;
            point = new Point(x, y);
            pixelPoint = this.convertToPixelCoordinates(point);
            derivatives.push(pixelPoint.x);
            derivatives.push(pixelPoint.y);
            x += xStep;
        }
        return derivatives;
    }
}
