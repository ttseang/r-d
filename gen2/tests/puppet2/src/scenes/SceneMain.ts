import { SpineAnim } from "../classes/SpineAnim";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    
    private puppet:SpineAnim;

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
        this.load.setPath("./assets/");

        // this.load.spine("me", 'me_01.json', ['me_01.atlas'], false);
        this.load.spine("puppet", 'Penny Loafer.json', ['Penny Loafer.atlas']);
    }
    create() {
        super.create();
        this.makeGrid(11,11);
        this.grid.showPos();

        window['scene']=this;

        //let face:Phaser.GameObjects.Image=this.add.image(0,0,"face");

        let spine:SpinePlugin=(this as any).spine;


        this.puppet=new SpineAnim(this,spine,"puppet");
        this.puppet.setReal(3335.08, 3379.4);
        this.puppet.setOffSet(809, -3300);

        //walk,idle,fidget 1,repeating/blink
        this.puppet.playAnimation("repeating/blink", true);

       this.puppet.scaleToGameW(0.15);

       this.grid.placeAt(3,3,this.puppet as any);
    }
}