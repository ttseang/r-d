import Phaser from 'phaser'
import 'phaser/plugins/spine/dist/SpinePlugin'
import { SceneMain } from './scenes/SceneMain'



const config: Phaser.Types.Core.GameConfig = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
	scene: [SceneMain],
	plugins: {
		scene: [
			{ key: 'SpinePlugin', plugin: window.SpinePlugin, mapping: 'spine' }
		]
	}
}

export default new Phaser.Game(config)
