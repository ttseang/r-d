export class EqEngine {
    private expression: string;
    private orignal: string;

    private answer: string = "";

    private styleEl: HTMLStyleElement | null = null;
    private maxDec: number = 0;
    private maxWhole: number = 0;

    private showFirstTrail: boolean = false;

    private autoCalc: string = "calc";

    //private showLeads: boolean = false;

    constructor(expression: string) {
        this.expression = expression;
        this.orignal = expression;
        this.styleEl = document.getElementById("customCSS") as HTMLStyleElement;

        (window as any).engine = this;
    }

    getHtml() {
        //basic expressions

        if (this.expression === "") {
            return "enter an expression";
        }

        let answerArray: string[] = this.expression.split("=");

        if (answerArray.length > 2) {
            return "Invalid!";
        }
        if (answerArray.length > 0) {
            this.answer = answerArray[1];
            this.expression = answerArray[0];
        }
        let output: string[] = [];

        if (this.expression.includes("_")) {
            return this.getFractions();
        }

        let add: boolean = this.expression.includes("+");
        let sub: boolean = this.expression.includes("-");
        let mul: boolean = this.expression.includes("*");
        let division: boolean = this.expression.includes("/");

        let c: number = ((add === true) ? 1 : 0) + ((sub === true) ? 1 : 0) + ((mul === true) ? 1 : 0) + ((division === true) ? 1 : 0);

        if (c !== 1) {
            if (this.answer === "" || this.answer === undefined) {
                return this.expression;
            }
            return this.expression + "=" + this.getFormattedAnswer();
            // return "invalid";
        }

        if (c === 1) {
            if (division === false) {
                let symbol: string = "+";
                if (sub === true) {
                    symbol = "-";
                }
                if (mul == true) {
                    symbol = "*";
                }
                /**
                 * Simple
                 */

                let nums: string[] = this.expression.split(symbol);

                this.maxDec = this.findMaxDec(nums);
                this.maxWhole = this.findMaxWhole(nums);

                if (this.findDecPlaces(this.answer) > this.maxDec) {
                    this.maxDec = this.findDecPlaces(this.answer);
                }

                nums = this.addTrailDecZeros(nums.slice());

                nums = this.addLeadingZeros(nums.slice());

                output.push("<div id='mathGrid'>");
                for (let i: number = 0; i < nums.length; i++) {
                    let num: string = nums[i];
                    if (num !== undefined) {
                        num = this.fixDec(num);
                        if (i !== nums.length - 1) {
                            output.push("<div></div><div>" + num + "</div>");
                        }
                        else {
                            symbol = (mul === true) ? "\u00D7" : symbol;
                            output.push("<div>" + symbol + "</div><div>" + num + "</div>");
                        }
                    }
                }

                let answer: string = this.getFormattedAnswer();



                output.push("</div>");

                output.push("<div id='answerGrid'>");
                output.push("<div></div>");
                output.push("<div>" + answer + "</div>");
                output.push("</div>");
            }
            else {
                /**
                 * Division
                 */
                let nums: string[] = this.expression.split("/");

                let dividend: string = nums[0] || "?";
                let divisor: string = nums[1] || "?";


                let nums2: string[] = [this.answer, dividend];

                if (!this.answer) {
                    nums2 = [dividend];
                }
                console.log(nums2);

                this.maxDec = this.findMaxDec(nums2);
                this.maxWhole = this.findMaxWhole(nums2);
                ////console.log.log("MX="+this.maxDec);

                if (this.findDecPlaces(this.answer) > this.maxDec) {
                    this.maxDec = this.findDecPlaces(this.answer);
                }


                // nums = this.addTrailDecZeros(nums.slice());


                //  divisor=this.addZeroTrail(divisor);
                divisor = this.fixDec(divisor, "decp2");

                dividend = this.addZeroLead(dividend);
                dividend = this.addZeroTrail(dividend);
                dividend = this.fixDec(dividend);


                let answer: string = this.getFormattedAnswer();

                output.push("<div id='divGrid'>");
                output.push("<div></div>");
                output.push("<div id='quotient'>" + answer + "</div>");
                output.push("<div id='divisor'>" + divisor + "</div>");
                output.push("<div id='dividend'>" + dividend + "</div>");
                output.push("</div>");
                //  return "show division";
            }
        }
        else {
            //complex
        }

        return output.join("");
    }
    private getFractions() {
        let output: string[] = [];
        let exArray: string[] = this.expression.split(/[=*+-//]/);
        let ops: string[] = this.extractOps();
        let answerArray: string[] = [];

        if (this.answer === this.autoCalc) {
            let fanswer: string = this.orignal.replaceAll("_", "/");

            console.log(fanswer);
            let decAnswer = this.calcAnswer(fanswer);
            console.log(decAnswer);
            fanswer = this.decToFraction(parseFloat(decAnswer));
            console.log(fanswer);
        }

        if (this.answer) {
            answerArray = this.answer.split("_");
            console.log(answerArray);
        }



        let tops: string[] = [];
        let bottoms: string[] = [];

        for (let i: number = 0; i < exArray.length; i++) {
            let exp: string[] = exArray[i].split("_");
            tops.push(exp[0]);
            bottoms.push(exp[1]);
        }
        if (answerArray.length === 1) {
            tops.push("");
            bottoms.push("<span class='wholeFract'>" + answerArray[0] + "</span>");
            ops.push("=");
        }
        if (answerArray.length == 2) {
            tops.push(answerArray[0]);
            bottoms.push(answerArray[1]);
            ops.push("=");
        }
        output.push("<div id='fractgrid'>");
        for (let j: number = 0; j < tops.length; j++) {
            output.push("<div class='topfrac'>" + tops[j] + "</div>");
            output.push("<div></div>");
        }
        output.push("</div>");
        //
        //
        //
        output.push("<div id='fractgrid'>");
        for (let k: number = 0; k < bottoms.length; k++) {
            let op: string = ops[k];
            op = (op === "/") ? "÷" : op;

            output.push("<div class='bottomfrac'>" + bottoms[k] + "</div>");
            output.push("<div class='fop'>" + op + "</div>");
        }
        output.pop();
        output.push("</div>");

        let len: number = (tops.length * 2) - 1;
        let per: number = 100 / len;
        let perString: string = per.toString() + "% ";
        let colString: string = perString.repeat(len);


        let cssStyle: CSSStyleDeclaration = document.createElement("span").style;
        cssStyle.display = "grid";
        cssStyle.gridTemplateColumns = colString;
        cssStyle.gridTemplateRows = "auto";

        let fractStyleEl: HTMLStyleElement = document.getElementById("fractcss") as HTMLStyleElement;

        if (fractStyleEl) {
            fractStyleEl.innerHTML = "";
            this.addStyle2(fractStyleEl, "fractgrid", cssStyle);
        }


        return output.join("");

    }
    private extractOps() {
        let opArray: string[] = ["+", "-", "*", "/", "="];
        let ops: string[] = [];

        for (let i: number = 0; i < this.expression.length; i++) {
            let char: string = this.expression.charAt(i);
            if (opArray.includes(char)) {
                ops.push(char);
            }
        }
        return ops;
    }
    public addStyleByID(elementID: string, style: CSSStyleDeclaration) {
        if (this.styleEl) {
            let cRule = "#" + elementID + "{" + style.cssText + "}";

            this.styleEl.append(cRule);
        }
    }
    public addStyle2(element: HTMLStyleElement, elementID: string, style: CSSStyleDeclaration) {
        if (element) {
            let cRule = "#" + elementID + "{" + style.cssText + "}";

            element.append(cRule);
        }
    }
    private fixDec(num: string, cn: string = "decp") {
        num = num.replace(".", "<span class='" + cn + "'>.</span>");

        return num;
    }
    private addLeadingZeros(nums: string[]) {
        let max: number = this.maxWhole;
        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            let wholeLen: number = this.findWholePlaces(num);
            let dif: number = max - wholeLen;
            nums[i] = this.addZeroLead(num);
        }
        return nums;
    }
    private addTrailDecZeros(nums: string[]) {


        let max: number = this.maxDec;

        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            let decLen: number = this.findDecPlaces(num);
            let dif: number = max - decLen;

            //  ////console.log.log(num,max,decLen,dif);
            nums[i] = this.addZeroTrail(num);

        }
        return nums;
    }
    private addZeroLead(num: string) {
        let wholeLen: number = this.findWholePlaces(num);
        let zeroCount: number = this.maxWhole - wholeLen;
        //console.log.log(num);
        //console.log.log("whole="+wholeLen);
        //console.log.log("zc="+zeroCount);

        if (zeroCount < 1) {
            return num;
        }
        let zeros: string = "";


        zeros = "<span class='invis0'>0</span>".repeat(zeroCount);
        num = zeros + num;


        return num;
    }
    private addZeroTrail(num: string) {
        let decLen: number = this.findDecPlaces(num);
        let zeroCount: number = this.maxDec - decLen;
        ////console.log.log("zeroCount="+zeroCount);

        ////console.log.log(num);
        ////console.log.log("DecLen=" + decLen);
        ////console.log.log("max="+this.maxDec);

        if (zeroCount < 1) {
            return num;
        }

        let zeros: string;
        if (this.showFirstTrail === true && decLen < 1) {

            zeros = "<span class='invis0'>0</span>".repeat(zeroCount - 1);
            zeros = "0" + zeros;
        }
        else {

            zeros = "<span class='invis0'>0</span>".repeat(zeroCount);
        }

        if (decLen == 0) {

            if (this.showFirstTrail === true) {
                num += "." + zeros;
            }
            else {
                num += zeros;
            }
        }
        else {
            num += zeros;
        }

        return num;
    }
    private findDecPlaces(num: string) {
        if (num === undefined) {
            return 0;
        }
        let decArray = num.split(".");
        let dec: string = decArray[1];
        if (dec) {
            return dec.length;
        }
        return 0;
    }
    private findWholePlaces(num: string) {
        if (num === undefined) {
            return 0;
        }
        let wholeArray = num.split(".");
        let whole: string = wholeArray[0];

        if (whole) {
            return whole.length;
        }
        return 0;
    }
    private findMaxDec(nums: string[]) {
        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            let decCount: number = this.findDecPlaces(num);
            if (decCount > max) {
                max = decCount;
            }
        }
        return max;
    }
    private findMaxWhole(nums: string[]) {
        let max: number = 0;
        for (let i: number = 0; i < nums.length; i++) {
            let num: string = nums[i];
            if (!isNaN(parseFloat(num))) {
                let wholeCount: number = this.findWholePlaces(num);
                if (wholeCount > max) {
                    max = wholeCount;
                }
            }
        }
        return max;
    }
    private getFormattedAnswer() {
        let answer: string = this.answer;
        if (answer === this.autoCalc) {
            answer = this.calcAnswer(this.orignal);
        }
        if (answer !== "" && answer !== undefined) {
            answer = this.addZeroLead(answer);
            answer = this.addZeroTrail(answer);

            answer = this.fixDec(answer);
        }

        if (answer === undefined) {
            answer = "?";
        }
        return answer;
    }
    private calcAnswer(answerString: string) {
        let answerArray: string[] = answerString.split("=");
        let q: string = answerArray[0];
        let answer: number = 0;
        if (q) {
            answer = eval(q);

            answer = this.trimDec(answer.toString());

            console.log(q);
            console.log(answer);

            //let parser:ExpressionParser=new ExpressionParser("formula");
            //  console.log(parse(q));
        }
        return answer.toString();
    }
    private trimDec(num: string) {
        let num2: number = parseFloat(num);
        let dec: number = num2 - Math.floor(num2);
        if (dec.toString().length > 5) {
            return Math.floor(num2 * 10000) / 10000;
        }
        return num2;


    }
    private decToFraction(num: number) {

        let len: number = num.toString().length - 2;

        let denominator: number = Math.pow(10, len);
        let numerator = num * denominator;

        let divisor = this.calcGcd(numerator, denominator);

        console.log("d=" + divisor);

        numerator = numerator / divisor;
        denominator = denominator / divisor;

        numerator = Math.floor(numerator);
        denominator = Math.floor(denominator);

        console.log(numerator + "/" + denominator);

        return numerator.toString() + "/" + denominator.toString();
    }
    private calcGcd(a: number, b: number): number {
        if (b == 0) return a;
        //a = parseInt(a);
        // b = parseInt(b);
        return this.calcGcd(b, a % b);
    };
}