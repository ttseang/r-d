export class EqInput
{
    private el:HTMLInputElement | null;
    private callback:Function;

    constructor(id:string,callback:Function)
    {
        this.el=document.getElementById(id) as HTMLInputElement;
        this.callback=callback;

        if (this.el)
        {
            this.el.addEventListener("keyup",this.onChange.bind(this));
        }
    }
    private onChange(e:Event)
    {
        if (this.el)
        {
            this.callback(this.el.value);
        }
        
    }
}