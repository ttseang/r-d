import { EqEngine } from "./engine/eqEngine";
import { EqInput } from "./eqInput";

let mathDiv: HTMLElement | null = document.getElementById("mathDisplay");

let eqInput: EqInput = new EqInput("text1", (exp:string) => {
    let engine: EqEngine = new EqEngine(exp);
    let html: string = engine.getHtml();

    if (mathDiv) {
        mathDiv.innerHTML = html;
    }
});