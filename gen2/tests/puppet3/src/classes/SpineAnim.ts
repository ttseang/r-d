
import SpineGameObject from "phaser/plugins/spine/src/gameobject/SpineGameObject";
import { IBaseScene } from "ttphasercomps";

export class SpineAnim extends SpineGameObject {
    public scene: Phaser.Scene;

    private bscene: IBaseScene;

    private _realWidth: number;
    private _realHeight: number;

    private orealHeight:number;
    private orealWidth:number;

    private graphics: Phaser.GameObjects.Graphics;

    constructor(bscene: IBaseScene, spine: SpinePlugin, key: string) {
        super(bscene.getScene(), spine, 0, 0, key);

        this.bscene = bscene;
        this.scene = bscene.getScene();

        this.scene.sys.displayList.add(this as any);
        this.scene.sys.updateList.add(this as any);



        this._realWidth = -1;
        this._realHeight = -1;

        this.orealHeight=-1;
        this.orealWidth=-1;
    }

    scaleToGameW(per: number) {

      /*   let offsetScale = (this as any).displayWidth / this.realWidth;
        (this as any).displayWidth = this.bscene.getW() * per * offsetScale;
        (this as any).scaleY = (this as any).scaleX; */
        //console.log(this.dw);

        
        
        let offsetScale = this.dw / this.realWidth;
        console.log(this.dw,this.realWidth,this.bscene.getW(),per,offsetScale);

        console.log(this.bscene.getW() * per * offsetScale);
        
        this.dw = this.bscene.getW() * per * offsetScale;
        
        this.sy = this.sx;

       /*  console.log(this.realWidth);
        console.log(this.realHeight);

        console.log(this.sx,this.sy); */

        //1440/679
    }
    setReal(w: number, h: number) {
        this._realWidth = w;
        this._realHeight = h;
        this.orealWidth=w;
        this.orealHeight=h;
    }
    get realHeight() {
        if (this._realHeight != -1) {
            return this.orealHeight * this.sy;
        }
        return super.displayHeight;
    }
    get realWidth() {
        if (this._realWidth != -1) {
            return this.orealWidth * this.sx;
        }
        return super.displayWidth;
    }
    setOffSet(xx: number, yy: number) {
        let root = (this as any).getRootBone();
        root.x = xx;
        root.y = yy;
    }
    fixCenter() {
        this.xx -= this.dw / 2;
        this.yy -= this.dh / 2;
    }
    resetPos() {
        /*   this.x = 0;
          this.y = 0; */
    }
    centerMe() {
        //  Align.centerSpine(this, this.scene);
    }

    playAnimation(key: string, loop: boolean) {
        (this as any).play(key, loop);
    }
    set xx(val: number) {
        (this as any).x = val;
    }
    get xx() {
        return (this as any).x;
    }
    set yy(val: number) {
        (this as any).y = val;
    }
    get yy() {
        return (this as any).y;
    }
    set sx(val: number) {
        (this as any).scaleX = val;
    }
    get sx() {
        return (this as any).scaleX;
    }
    set sy(val: number) {
        (this as any).scaleY = val;
    }
    get sy() {
        return (this as any).scaleY;
    }
    set dw(val: number) {
        console.log("set to "+val.toString());
        (this as any).displayWidth = val;
    }
    set dh(val: number) {
        (this as any).displayHeight = val;
    }
    get dw() {
        return (this as any).displayWidth;
    }
    get dh() {
        return (this as any).displayHeight;
    }
    showDebugPos() {
        if (!this.graphics) {
            this.graphics = this.scene.add.graphics();

            this.graphics.fillStyle(0xffffff, 0.4);
        }
        else {
            this.graphics.clear();
            this.graphics.fillStyle(0xffffff, 0.4);
        }

        //   this.graphics.fillRect(this.x, this.y, this.displayWidth, this.displayHeight);
    }
}