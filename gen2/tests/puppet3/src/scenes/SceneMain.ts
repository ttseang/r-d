
import { CompLayout, CompManager, ButtonController, CompLoader } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { SpineAnim } from "../classes/SpineAnim";
import { BaseScene } from "./BaseScene";


export class SceneMain extends BaseScene {
    private compLayout:CompLayout;
    private cm:CompManager=CompManager.getInstance();
    private buttonController:ButtonController=ButtonController.getInstance();
    private compLoader:CompLoader;
   
    public puppet:SpineAnim;

    constructor() {
        super("SceneMain");
        this.compLoader=new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
         this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("arrows","./assets/arrows.png");
        this.load.image("qmark","./assets/questionmark.png");
        this.load.image("right","./assets/right.png");
        this.load.image("wrong","./assets/wrong.png");
        this.load.image("triangle","./assets/triangle.png");

        this.load.setPath("./assets/");
        this.load.spine("puppet", 'Penny Loafer.json', ['Penny Loafer.atlas']);
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        (window as any).scene=this;

        let spine:SpinePlugin=(this as any).spine;


        this.puppet=new SpineAnim(this,spine,"puppet");
        this.puppet.setReal(3335.08, 3379.4);
        this.puppet.setOffSet(809, -3300);

        //walk,idle,fidget 1,repeating/blink
        this.puppet.playAnimation("repeating/blink", true);

       this.puppet.scaleToGameW(0.15);

       this.grid.placeAt(1,5,this.puppet as any);

        this.cm.regFontSize("list",30,1200);
        this.cm.regFontSize("words",40,1200);

        this.compLayout=new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");     
        
        this.buttonController.callback=this.doButtonAction.bind(this);
    }
    compsLoaded()
    {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize=this.doResize.bind(this);

        this.cm.getComp("right").visible=false;
        this.cm.getComp("wrong").visible=false;

        this.children.bringToTop(this.puppet as any);
        this.loadGameData();
    }
    loadGameData()
    {
        /* fetch("./assets/words.json")
        .then(response => response.json())
        .then(data => this.gotGameData({ data })); */
    }
    gotGameData(data:any)
    {

    }
    doResize()
    {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();
        this.cm.doResize();
       
      //  this.puppet.scaleToGameW(0.15);

       this.grid.placeAt(1,5,this.puppet as any);

    }
    doButtonAction(action:string,params:string)
    {
        console.log(action);

        switch(action)
        {
            case "help":
                this.puppet.playAnimation("fidget 1",true);
            break;

            case "play":
                this.puppet.playAnimation("repeating/blink",true);
            break;

            case "reset":
                this.puppet.playAnimation("idle",true);
                break;

            case "check":
                this.puppet.playAnimation("walk",true);
            break;
        }
        
    }

}