export class PathMaker {
    constructor() {
        this.listenForSliderChanges();
    }
    /*
    * This function will return a path description for a quadratic path
    * @param a is the coefficient that multiplies x²
    * @param b is the coefficient that multiplies x
    * @param c is the final coefficient
    * @param x1 is the start value for the x axis in a graph
    * @param x2 is the end value for the x axis in a graph
    * @return pathDescription is the path description for the quadratic path
    * @example
    * let pathDescription = PathMaker.quadPath(1, 2, 3, 0, 10);
    * // pathDescription = "M0,3 L1,5 L2,9 L3,15 L4,23 L5,33 L6,45 L7,59 L8,75 L9,93 L10,113"
    */
    quadPath(a: number, b: number, c: number, x1: number, x2: number) {
        //draw a parabola using a quadratic path
        // a is the coefficient that multiplies x²
        // b is the coefficient that multiplies x
        // c is the final coefficient
        // x1 is the start value for the x axis in a graph
        // x2 is the end value for the x axis in a graph
        
        let pathDescription: string = "M" + x1 + "," + (a * x1 * x1 + b * x1 + c);
        for (let x = x1; x <= x2; x++) {
            pathDescription += " L" + x + "," + (a * x * x + b * x + c);
        }
        
        return pathDescription;
    }
    //listen for the sliders to change
    //class name slider1 to slider5

    listenForSliderChanges() {
        let sliders: HTMLCollection = document.getElementsByClassName("slider");
        console.log(sliders);
        for (let i = 0; i < sliders.length; i++) {
            let slider: HTMLInputElement = sliders[i] as HTMLInputElement;
            slider.addEventListener("input", this.updateValues.bind(this));
        }
    }
        

    updateValues() {

        //get the values from the sliders
        let a: number = parseFloat((document.querySelector(".slider1") as HTMLInputElement).value);
        let b: number = parseFloat((document.querySelector(".slider2") as HTMLInputElement).value);
        let c: number = parseFloat((document.querySelector(".slider3") as HTMLInputElement).value);
        let x1: number = parseFloat((document.querySelector(".slider4") as HTMLInputElement).value);
        let x2: number = parseFloat((document.querySelector(".slider5") as HTMLInputElement).value);

        console.log(a, b, c, x1, x2);

        let pathDescription: string = this.quadPath(a, b, c, x1, x2);
        let heart: SVGSVGElement = document.getElementsByClassName("heart")[0] as SVGSVGElement;
        console.log(heart);
        (window as any).heart = heart;
        if (heart) {

            heart.setAttribute("d", pathDescription);
        }
    }
}