import { IBaseScene } from "../../../../common/mods/ttcomps";
import 'phaser/plugins/spine/dist/SpinePlugin';

export class SpineAnim extends SpinePlugin.SpineGameObject
{
    constructor(bscene:IBaseScene,spine:SpinePlugin,key:string)
    {
        super(bscene.getScene(),spine,0,0,key);
    }
}