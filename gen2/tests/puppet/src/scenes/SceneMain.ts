
import { CompLayout, CompManager, ButtonController, CompLoader, BaseScene } from "ttphasercomps";
import * as SpinePlugin from "phaser/plugins/spine/dist/SpinePlugin";
import { SpineAnim } from "../classes/SpineAnim";

//import { Align } from "ttphasercomps/util/align";


export class SceneMain extends BaseScene {

    private puppet:SpineAnim;

    constructor() {
        super("SceneMain");

    }
    preload() {
        this.load.setPath("./assets/");

        // this.load.spine("me", 'me_01.json', ['me_01.atlas'], false);
        this.load.spine("puppet", 'Penny Loafer.json', ['Penny Loafer.atlas'], false);
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        (window as any).scene=this;
        this.puppet = new SpineAnim(this,this.spine, "puppet");

       /*  this.puppet = new SpineAnim(this, "puppet");
        this.puppet.setReal(3335.08, 3379.4);
        this.puppet.setOffSet(809, -3300); */

        //walk,idle,fidget 1,repeating/blink
        //this.puppet.play("repeating/blink", true);

    }

    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);


    }
    doButtonAction(action: string, params: string) {

    }

}