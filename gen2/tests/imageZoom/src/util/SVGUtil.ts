export class SVGUtil {
    public static setX(obj: SVGElement, value: number) {
        obj.setAttribute("x", value.toString());
    }
    public static setY(obj: SVGElement, value: number) {
        obj.setAttribute("y", value.toString());
    }
    public static setWidth(obj: SVGElement, value) {
        obj.setAttribute("width", value.toString());
    }
    public static setHeight(obj: SVGElement, value) {
        obj.setAttribute("height", value.toString());
    }
    public static scaleToGameW(obj: SVGElement, value:number,gw:number)
    {
        console.log("gw="+gw);

        let w2:number=gw*value;
        obj.setAttribute("width", w2.toString());
    }
    public static scaleToGameH(obj: SVGElement, value:number,gh:number)
    {
        console.log("gh="+gh);
        let h2:number=gh*value;
        obj.setAttribute("height", h2.toString());
    }
    public static setVis(obj: SVGElement, value:boolean)
    {
        let vis:string=(value==true)?"visible":"hidden";
        obj.setAttribute("visibility",vis);
    }
}