export class ImageVo
{
    public x:number;
    public y:number;
    public scale:number;
    public src:string;
    public border:boolean;

    constructor(x:number,y:number,scale:number,src:string,border:boolean)
    {
        this.x=x;
        this.y=y;
        this.scale=scale;
        this.src=src;
        this.border=border;
    }
}