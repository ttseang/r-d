import { CardVo } from "./CardVo";
import { ImageVo } from "./ImageVo";
import { LineVo } from "./LineVo";
import { QuizVo } from "./QuizVo";
import { ZoomVo } from "./ZoomVo";

export class StepVo
{
    public zoomVo:ZoomVo | null;
    public cardVo:CardVo[];
    public lineVo:LineVo[];
    public images:ImageVo[];
    public quizData:QuizVo[];
    public thumb:string="";
    public secs:number=5;
    public audio:string="";

    constructor(zoomVo:ZoomVo,cardVo:CardVo[],lineVo:LineVo[]=[],images:ImageVo[]=[],quizData:QuizVo[])
    {
        this.zoomVo=zoomVo;
        this.cardVo=cardVo;
        this.lineVo=lineVo;
        this.images=images;
        this.quizData=quizData;
    }
}