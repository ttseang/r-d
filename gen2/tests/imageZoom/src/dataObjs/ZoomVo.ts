export class ZoomVo
{
    public xx:number;
    public yy:number;
    public zoom:number;
    
    constructor(xx:number,yy:number,zoom:number)
    {
        this.xx=xx;
        this.yy=yy;
        this.zoom=zoom;
    }
    toString():string
    {
        let xx2:string=this.xx.toPrecision(3);
        let yy2:string=this.yy.toPrecision(3);
        let zoom2:string=this.zoom.toPrecision(3);

        return "new ZoomVo("+xx2+","+yy2+","+zoom2+")";
    }
}