export class LineVo
{
    public x1:number;
    public y1:number;
    public x2:number;
    public y2:number;
    public thick:number;
    public color:string;

    constructor(x1:number,y1:number,x2:number,y2:number,thick:number,color:string)
    {
        this.x1=x1;
        this.y1=y1;
        this.x2=x2;
        this.y2=y2;
        this.thick=thick;
        this.color=color;
    }
}

