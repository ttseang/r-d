export class QuizVo
{
    public question:string;
    public answers:string[];
    public correct:number;

    constructor(question:string,answers:string[],correct:number)
    {
        this.question=question;
        this.answers=answers;
        this.correct=correct;
    }
}