import { IBaseScene } from "ttphasercomps";

export interface ICard
{
    setText(key:string,text:string):void;
    zoomImage:Phaser.GameObjects.Image;
    id:string;
    x:number;
    y:number;
    width:number;
    height:number;
    el:HTMLElement;
    visible:boolean;
    flyTo(xx:number,yy:number);
    setHighlight(className:string,index:number);
    setToImage(bscene: IBaseScene, x1: number, y1: number, useTween: boolean)
}