import { Align, IBaseScene } from "ttphasercomps";
import { CardVo } from "../dataObjs/CardVo";
import { LineVo } from "../dataObjs/LineVo";
import { QuizVo } from "../dataObjs/QuizVo";
import { StepVo } from "../dataObjs/StepVo";
import { ZoomVo } from "../dataObjs/ZoomVo";
import { ICard } from "../interfaces/ICard";
import { GM } from "./GM";
import { InfoCard } from "./InfoCard";
import { MainController } from "./MainController";
import { QuizBox } from "./QuizBox";
import { SmallImage } from "./SmallImage";
import { SoundObj } from "./SoundObj";
import { StepClock } from "./StepClock";
import { SvgLine } from "./SvgLine";
import { TextCard } from "./TextCard";

export class DPage extends Phaser.GameObjects.Container {
    public cards: InfoCard[] = [];
    public textCards: TextCard[] = [];

    public zoomIndex: number = 1;
    public zsteps: StepVo[] = [];
    public stepIndex: number = 0;

    public lines: HTMLElement | null;
    public lineObjs: SvgLine[] = [];
    public imageFrames: SmallImage[] = [];
    public quizBox: QuizBox;
    public quizIndex: number = 0;

    private mainImage: Phaser.GameObjects.Image;
    private guideImage: Phaser.GameObjects.Image;
    private xx: number = 0;
    private yy: number = 0;

    private bscene: IBaseScene;
    public actionCallback: Function = () => { };
    private mc: MainController = MainController.getInstance();

    private allowTween: boolean = true;

    private stepClock: StepClock;
    private gm: GM = GM.getInstance();

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());

        this.bscene = bscene;
        this.setSize(bscene.getW(), bscene.getH());
        //
        //
        //
        this.stepClock = new StepClock(this.timeUp.bind(this));

        this.lines = document.getElementById("lines");

        this.mainImage = this.scene.add.image(0, 0, "horse").setOrigin(0, 0);
        this.guideImage = this.scene.add.image(0, 0, "horse").setOrigin(0, 0);
        this.guideImage.visible = false;

        this.quizBox = new QuizBox();
        this.quizBox.actionCallback = this.quizCallback.bind(this);
        //
        //
        //
        for (let i: number = 0; i < 4; i++) {
            this.lineObjs.push(new SvgLine(bscene, this.guideImage));
            this.cards.push(new InfoCard(i, this.guideImage));
            this.textCards.push(new TextCard(i, this.guideImage));
            this.imageFrames.push(new SmallImage(i, this.guideImage));
        }

        this.scene.input.keyboard.on("keydown", this.onKeyDown.bind(this));

        this.mc.setStep = this.setStep.bind(this);

        this.mc.doAutoPlay = this.startAutoPlay.bind(this);
    }
    startAutoPlay() {
        console.log("start auto play");

        if (this.gm.autoPlay == true) {
            this.gm.autoPlay = false;
            return;
        }
        this.gm.autoPlay = true;

        this.stepClock.setTime(this.zsteps[this.stepIndex].secs);
    }
    timeUp() {
        if (this.gm.autoPlay == false) {
            this.stepClock.stopClock();
            return;
        }
        if (this.stepIndex < this.zsteps.length - 1) {
            this.stepIndex++;
            this.doStep(this.zsteps[this.stepIndex], true);
            this.mc.updateProgBar(this.stepIndex);
            this.allowTween = true;


            this.stepClock.setTime(this.zsteps[this.stepIndex].secs);
        }
    }
    setStepData(steps: StepVo[]) {
        this.zsteps = steps;
        ////console.log(steps);
        this.allowTween = false;
        this.doStep(this.zsteps[0], false);
        this.doResize();
        if (this.gm.autoPlay == true) {
            this.stepClock.setTime(this.zsteps[0].secs);
        }
    }
    onKeyDown(e: KeyboardEvent) {
        // ////console.log(e.key);

        switch (e.key) {
            case "ArrowUp":

                if (e.ctrlKey == false) {

                    if (e.shiftKey == true) {
                        this.yy -= 0.1;
                    }
                    else {
                        this.yy -= 0.01;
                    }
                    this.setPos(this.xx, this.yy);
                }
                else {
                    if (e.shiftKey == false) {
                        this.zoomIndex += 0.01;
                    }
                    else {
                        this.zoomIndex += 0.1;
                    }

                    this.setZoom(this.zoomIndex);
                }
                break;

            case "ArrowDown":
                if (e.ctrlKey == false) {
                    if (e.shiftKey == true) {
                        this.yy += 0.1;
                    }
                    else {
                        this.yy += 0.01;
                    }
                    this.setPos(this.xx, this.yy);
                }
                else {
                    if (e.shiftKey == false) {
                        this.zoomIndex -= 0.01;
                    }
                    else {
                        this.zoomIndex -= 0.1;
                    }
                    this.setZoom(this.zoomIndex);
                }
                break;

            case "ArrowLeft":
                if (e.shiftKey == true) {
                    this.xx -= 0.1;
                }
                else {
                    this.xx -= 0.01;
                }

                this.setPos(this.xx, this.yy);
                break;

            case "ArrowRight":
                if (e.shiftKey == true) {
                    this.xx += 0.1;
                }
                else {
                    this.xx += 0.01;
                }
                this.setPos(this.xx, this.yy);
                break;
        }
        // ////console.log(this.xx, this.yy);
        this.setMainImageAtts();

    }
    prevStep() {
        if (this.stepIndex > 0) {
            this.stepIndex--;
            this.doStep(this.zsteps[this.stepIndex], true);
            this.mc.updateProgBar(this.stepIndex);
            this.allowTween = true;
        }
    }
    nextStep() {
        if (this.stepIndex < this.zsteps.length) {
            this.stepIndex++;
            this.doStep(this.zsteps[this.stepIndex], true);
            this.mc.updateProgBar(this.stepIndex);
            this.allowTween = true;
        }
    }
    setStep(step: number) {
        this.stepIndex = step;
        this.doStep(this.zsteps[this.stepIndex], false);
    }
    doStep(zstep: StepVo, doTween: boolean = true) {

        this.clearLines();
        this.clearCards();
        this.clearImages();

        let zoomVo: ZoomVo = zstep.zoomVo;
        //console.log(zoomVo);
        if (zoomVo) {
            this.setZoom(zoomVo.zoom);
            this.setPos(zoomVo.xx, zoomVo.yy);
        }

        for (let ii: number = 0; ii < zstep.cardVo.length; ii++) {
            let cardVo: CardVo = zstep.cardVo[ii];
            ////console.log(cardVo);

            let card: ICard;

            if (cardVo.type == "info") {
                card = this.cards[ii];
            }
            if (cardVo.type == "text") {
                card = this.textCards[ii];
            }



            if (cardVo) {
                card.visible = true;

                card.setText("heading", cardVo.heading);
                card.setText("content", this.makeTextLines(cardVo.content));

                if (cardVo.highlight > -1) {
                    card.setHighlight(".hl2", cardVo.highlight);
                }
                let xratio: number = this.guideImage.displayWidth / this.bscene.getW();
                let yratio: number = this.guideImage.displayHeight / this.bscene.getH();

                let startX: number = this.guideImage.x / this.bscene.getW();
                let startY: number = this.guideImage.y / this.bscene.getH();

                let xx: number = (cardVo.xx) * xratio;
                let yy: number = (cardVo.yy) * yratio;



                if (cardVo.relativeToImage === true) {
                    card.setToImage(this.bscene, xx, yy, doTween);
                }
                else {
                    if (doTween) {
                        card.flyTo(xx, yy);
                    }
                    else {
                        card.x = xx;
                        card.y = yy;
                    }
                }

            }
            else {
                card.visible = false;
            }
        }
        /*  for (let i: number = 0; i < zstep.lineVo.length; i++) {
             ////console.log(zstep.lineVo[i]);
 
             this.lineObjs[i].setLineFromObj(zstep.lineVo[i]);
             this.lineObjs[i].visible = true;
         } */

        if (doTween == true) {
            this.updateMainImage();
        }
        else {
            this.setMainImageAtts();
        }
        this.setLines();
        this.setImages(doTween);
        this.setQuiz();

        setTimeout(() => {
            this.checkForOverlap(this.textCards);
            this.checkForOverlap(this.cards);

        }, 250);

        console.log(zstep.audio);
        console.log(this.gm.playMode);

        if (zstep.audio != "" && this.gm.playMode == "listen") {
            this.playSound(zstep.audio);
        }
    }

    makeTextLines(texts: string[]) {
        let texts2: string[] = [];
        for (let i: number = 0; i < texts.length; i++) {
            texts2.push("<span class='hl1'>" + texts[i] + "</span>");

        }
        return texts2.join("");
    }
    getInfo() {
        //console.log("x:" + this.xx + ",y:" + this.yy + ",z:" + this.zoomIndex.toString());
    }
    setQuiz() {
        let zstep: StepVo = this.zsteps[this.stepIndex];
        if (zstep.quizData.length > 0) {
            this.actionCallback("hideButtons", "");
            this.quizIndex = 0;
            this.quizBox.visible = true;
            this.quizBox.reset();
            this.quizBox.makeNumbers(zstep.quizData.length, 0);
            this.nextQuizQuestion();
        }
        else {
            this.quizBox.visible = false;
        }
    }
    nextQuizQuestion() {
        let zstep: StepVo = this.zsteps[this.stepIndex];
        let quizVos: QuizVo[] = zstep.quizData;
        let quizVo: QuizVo = quizVos[this.quizIndex];
        this.quizBox.reset();
        this.quizBox.setData(quizVo);
        this.quizBox.makeNumbers(zstep.quizData.length, this.quizIndex);
    }
    quizCallback(action: string, param: string) {
        //console.log(action);

        switch (action) {
            case "nextQuestion":
                this.quizIndex++;
                if (this.quizIndex > this.zsteps[this.stepIndex].quizData.length - 1) {
                    this.quizBox.visible = false;
                    this.actionCallback("showButtons");
                    this.nextStep();
                    return;
                }
                this.nextQuizQuestion();
                break;
        }
    }
    setLines() {
        let zstep: StepVo = this.zsteps[this.stepIndex];

        for (let i: number = 0; i < zstep.lineVo.length; i++) {
            ////console.log(zstep.lineVo[i]);

            this.lineObjs[i].setLineFromObj(zstep.lineVo[i]);
            this.lineObjs[i].visible = true;
        }
    }
    setImages(useTween: boolean = true) {
        let zstep: StepVo = this.zsteps[this.stepIndex];

        for (let i: number = 0; i < zstep.images.length; i++) {
            // //console.log(zstep.images[i]);

            //   this.imageFrames[i].setFromObj(zstep.images[i]);
            this.imageFrames[i].setToImage(this.bscene, zstep.images[i], useTween);
            //this.imageFrames[i].setToImage2(this.bscene,zstep.images[i]);
            this.imageFrames[i].visible = true;
        }
    }

    clearLines() {
        for (let i: number = 0; i < this.lineObjs.length; i++) {
            this.lineObjs[i].visible = false;
        }

    }
    clearImages() {
        for (let i: number = 0; i < this.imageFrames.length; i++) {
            this.imageFrames[i].visible = false;
        }
    }
    clearCards() {
        for (let i: number = 0; i < this.cards.length; i++) {
            this.cards[i].visible = false;
            this.textCards[i].visible = false;
        }
    }
    setZoom(per: number) {
        this.zoomIndex = per;
        if (per > 0) {
            //  Align.scaleToGameH(this.guideImage, per, this.bscene);
            Align.scaleToGameW(this.guideImage, per, this.bscene);
            ////console.log(this.guideImage.displayWidth,this.guideImage.displayHeight);

            /*  if (this.guideImage.displayWidth < this.bscene.getW()) {
                 //console.log("use width");
                 Align.scaleToGameW(this.guideImage, per, this.bscene);
             } */
        }
        else {
            //console.log(per);

            this.guideImage.scaleX = Math.abs(per);
            this.guideImage.scaleY = Math.abs(per);
        }

    }
    setPos(xx: number, yy: number) {
        this.xx = xx;
        this.yy = yy;
        this.guideImage.x = -this.guideImage.displayWidth * xx;
        this.guideImage.y = -this.guideImage.displayHeight * yy;
    }
    setImageProps(xx: number, yy: number, per: number) {
        this.setPos(xx, yy);
        this.setZoom(per);
        this.updateMainImage();
    }
    updateMainImage() {
        this.scene.tweens.add({ duration: 500, targets: this.mainImage, x: this.guideImage.x, y: this.guideImage.y, displayWidth: this.guideImage.displayWidth, displayHeight: this.guideImage.displayHeight });
    }
    setMainImageAtts() {
        this.mainImage.x = this.guideImage.x;
        this.mainImage.y = this.guideImage.y;
        this.mainImage.displayHeight = this.guideImage.displayHeight;
        this.mainImage.displayWidth = this.guideImage.displayWidth;
    }
    resetCardPos() {
        let zstep: StepVo = this.zsteps[this.stepIndex];

        for (let ii: number = 0; ii < zstep.cardVo.length; ii++) {
            let cardVo: CardVo = zstep.cardVo[ii];
            ////console.log(cardVo);

            let card: ICard;

            if (cardVo.type == "info") {
                card = this.cards[ii];
            }
            if (cardVo.type == "text") {
                card = this.textCards[ii];
            }


            if (cardVo) {
                card.visible = true;


                let xratio: number = this.guideImage.displayWidth / this.bscene.getW();
                let yratio: number = this.guideImage.displayHeight / this.bscene.getH();

                let startX: number = this.guideImage.x / this.bscene.getW();
                let startY: number = this.guideImage.y / this.bscene.getH();

                let xx: number = (cardVo.xx) * xratio;
                let yy: number = (cardVo.yy) * yratio;

                //console.log(startX, startY);
                //console.log(xratio, yratio);
                //console.log(xx, yy);
                card.zoomImage = this.guideImage;
                if (cardVo.relativeToImage == true) {
                    card.setToImage(this.bscene, xx, yy, false);
                }
                else {
                    card.x = xx;
                    card.y = yy;
                }


            }
        }
        this.checkForOverlap(this.textCards);
        this.checkForOverlap(this.cards);

    }
    checkForOverlap(cards: ICard[]) {
        for (let i: number = 0; i < cards.length; i++) {
            for (let j: number = i + 1; j < cards.length; j++) {
                let card1: ICard = cards[i];
                let card2: ICard = cards[j];

                let rect1 = card1.el.getBoundingClientRect();
                let rect2 = card2.el.getBoundingClientRect();

                let overlap = !(rect1.right < rect2.left ||
                    rect1.left > rect2.right ||
                    rect1.bottom < rect2.top ||
                    rect1.top > rect2.bottom)


                if (overlap) {
                    card2.el.style.top = (rect1.bottom * 1.1).toString() + "px";
                }

            }
        }
    }
    public playSound(path: string) {
        let s: SoundObj = new SoundObj(path);
        s.play();
    }
    doResize() {
        // //console.log("doResize");
        this.setZoom(this.zoomIndex);
        this.setPos(this.xx, this.yy);
        this.setMainImageAtts();
        this.clearLines();
        this.setLines();
        this.setImages(true);
        this.resetCardPos();
    }
}