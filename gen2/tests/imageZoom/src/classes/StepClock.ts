export class StepClock
{
    private secs:number=0;
    private timer:NodeJS.Timeout;
    private callback:Function=()=>{};

    constructor(callback:Function)
    {
        this.callback=callback;
    }
    setTime(secs:number)
    {
        clearInterval(this.timer);
        this.secs=secs;
        this.timer=setInterval(this.tick.bind(this),1000)
    }
    stopClock()
    {
        clearInterval(this.timer);
    }
    tick()
    {
        this.secs--;
        if (this.secs==0)
        {
            clearInterval(this.timer);
            this.callback();
        }
    }
}