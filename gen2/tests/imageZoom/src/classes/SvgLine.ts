import { IBaseScene } from "../../../../common/mods/ttcomps";
import { LineVo } from "../dataObjs/LineVo";
import { SvgObj2 } from "./SvgObj2";

export class SvgLine extends SvgObj2 {
    private x1: number = 0;
    private y1: number = 0;
    private x2: number = 0;
    private y2: number = 0;
    private thick: number = 4;
    private color: string = "red";
    private zoomImage:Phaser.GameObjects.Image;

    constructor(bscene: IBaseScene,zoomImage:Phaser.GameObjects.Image) {
        super(bscene, "theline", "lines");
        this.zoomImage=zoomImage;
        if (this.el)
        {
            this.el.style.display="none";
        }
    }
    setLine(x1: number, y1: number, x2: number, y2: number, thick: number = 4, color: string = "red") {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.thick = thick;
        this.color = color;
        this.resetPos();
    }
    setLineFromObj(lineVo: LineVo) {       

        this.x1 = lineVo.x1;
        this.y1 = lineVo.y1;
        this.x2 = lineVo.x2;
        this.y2 = lineVo.y2;
        this.color=lineVo.color;
        this.thick=lineVo.thick;
        this.resetPos();
    }
    resetPos() {
        if (this.el) {
            let xx1: number =this.zoomImage.x+this.zoomImage.displayWidth * this.x1/100;
            let yy1: number = this.zoomImage.y+this.zoomImage.displayHeight * this.y1/100;
            let xx2: number =this.zoomImage.x+ this.zoomImage.displayWidth * this.x2/100;
            let yy2: number = this.zoomImage.y+this.zoomImage.displayHeight * this.y2/100;
            

            this.el.setAttribute("x1", xx1.toString());
            this.el.setAttribute("y1", yy1.toString());
            this.el.setAttribute("x2", xx2.toString());
            this.el.setAttribute("y2", yy2.toString());

            this.el.style.stroke=this.color;
            this.el.style.strokeWidth=(this.thick*this.zoomImage.scaleX).toString();

        }
    }
}