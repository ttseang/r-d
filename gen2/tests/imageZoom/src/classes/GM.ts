import { FileVo } from "../dataObjs/FileVo";
import { StepVo } from "../dataObjs/StepVo";

let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;

    public stepObjs:StepVo[]=[];
    public files:FileVo[]=[];

    private imageBaseW:number=0;
    private imageBaseH:number=0;

    public autoPlay:boolean=false;
    public playMode:string="read";

    constructor()
    {
        window['gm']=this;
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
    getCanvasRatio(ww:number,canvasScale:number)
    {
        let base:number=.25;
        let ratio:number=ww/base;
        return canvasScale*ratio;
    }
    /* getCanvasRatio(imageWidth:number,imageHeight:number,canvasWidth:number,canvasHeight:number)
    {
        let wratio = imageWidth / this.imageBaseW;   // calc ratio
        let ww = canvasWidth * wratio;   // get font size based on current width

        let hratio:number=imageHeight/this.imageBaseH;
        let hh=canvasHeight*hratio/this.imageBaseW;

        console.log(ww,hh);
    } */
}