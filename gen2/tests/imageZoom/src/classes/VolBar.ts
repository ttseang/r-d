import { IBaseScene } from "ttphasercomps";
import { SVGUtil } from "../util/SVGUtil";
import { SvgObj2 } from "./SvgObj2";

export class VolBar extends SvgObj2
{
    public well:SVGImageElement;
    public back:SVGImageElement;
    

    constructor(screen:IBaseScene)
    {
        super(screen,"volBar","bottomArea");
        this.screen=screen;
        console.log(screen);

       // console.log(this.el);

        if (this.el)
        {
            let images:HTMLCollection=this.el.children;
            console.log(images);
            this.back=images[0] as SVGImageElement;
            this.well=images[1] as SVGImageElement;


            SVGUtil.setX(this.well,-15);
            SVGUtil.setY(this.well,30);
            this.onResize();
        }

    }
    onResize()
    {
       // console.log(this.screen);
       // console.log(this.screen.getW());
        SVGUtil.scaleToGameW(this.well,.1,window.innerWidth);
        SVGUtil.scaleToGameH(this.well,.4,window.innerHeight);
    }
}