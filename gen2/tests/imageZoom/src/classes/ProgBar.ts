import { HtmlObj } from "./HtmlObj"
import { GM } from "./GM";
import { MainController } from "./MainController";

export class ProgBar extends HtmlObj {
    public knob: HtmlObj = new HtmlObj("progKnob");

    private isDragging: boolean = false;
    private gm: GM = GM.getInstance();
    public prevImage: HTMLImageElement;

    private mc: MainController = MainController.getInstance();

    private stepIndex: number = 0;

    constructor() {
        super("progBar");

        this.prevImage=document.getElementById("prevFrame") as HTMLImageElement;

        if (this.knob.el) {
            this.knob.el.onmousedown = () => {
                this.isDragging = true;
            }
            document.onmouseup = () => {
                if (this.isDragging == true) {

                    this.mc.setStep(this.stepIndex);
                }
                this.isDragging = false;
                this.prevImage.style.display="none";
            }
            document.onmousemove = (e: MouseEvent) => {
                if (this.isDragging) {
                    let tx: number = this.el.getBoundingClientRect().x;
                    let ww: number = this.el.getBoundingClientRect().width;
                    let xx: number = ((e.x - tx) / ww) * 100;

                    if (xx < 0) {
                        xx = 0;
                    }
                    if (xx > 100) {
                        xx = 100;
                    }

                    this.knob.x = xx * 0.95;

                    let per: number = Math.floor(xx);
                    let stepCount: number = this.gm.stepObjs.length;

                    let step: number = Math.floor((per / 100) * stepCount);
                    if (step>this.gm.stepObjs.length-1)
                    {
                        step=this.gm.stepObjs.length-1;
                    }
                   //   console.log(step);
                    this.stepIndex = step;
                    let image:string= "./assets/progThumbs/" + this.gm.stepObjs[step].thumb;
                    this.prevImage.src=image;
                    this.prevImage.style.display="block";
                }
            }
        }
        this.mc.updateProgBar = this.setStep.bind(this);
    }
    setStep(step: number) {
        let per: number = (step / this.gm.stepObjs.length) * 100;
        this.knob.x = per * 0.95;

        console.log(step);
            let image:string= "./assets/progThumbs/" + this.gm.stepObjs[step].thumb;
           this.prevImage.src=image;
        
    }
}