import { IBaseScene } from "ttphasercomps";
import { HtmlObj } from "./HtmlObj";
import { ProgBar } from "./ProgBar";
import { SvgObj2 } from "./SvgObj2";
import { VolBar } from "./VolBar";

export class BottomBar extends HtmlObj
{
    public progBar:ProgBar=new ProgBar();
    
    
    constructor(screen:IBaseScene)
    {
        super("bottomBar");
        
      
    }
    public close()
    {
        this.y=100;
    }
    public open()
    {
        this.y=86;
    }
    
}