import { IBaseScene } from "ttphasercomps";
import { ICard } from "../interfaces/ICard";
import { BaseCard } from "./BaseCard";
import { HtmlObj } from "./HtmlObj";

export class InfoCard extends BaseCard implements ICard {
    public zoomImage:Phaser.GameObjects.Image;

    constructor(index: number,zoomImage:Phaser.GameObjects.Image) {
        super("infoCard" + index.toString());
        this.zoomImage=zoomImage;
        //console.log(this.id);
    }
   
    setInfo(title: string, content: string) {
        if (this.el) {
            let texts: HTMLCollectionOf<HTMLDivElement> = this.el.getElementsByTagName("div");

            if (texts[0]) {
                texts[0].innerHTML = title;
            }
            if (texts[1]) {
                texts[1].innerHTML = content;
            }
        }
    }
    setText(key:string,text:string)
    {
        let texts: HTMLCollectionOf<HTMLDivElement> = this.el.getElementsByTagName("div");

        switch(key)
        {
            case "heading":
                if (texts[0]) {
                    texts[0].innerHTML = text;
                }
            break;

            case "content":
                if (texts[1]) {
                    texts[1].innerHTML = text;
                }
            break;
        }      
        
    }
    
    setHighlight(className:string,index:number)
    {
        
    }
}