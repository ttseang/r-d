let instance:MainController=null;

export class MainController
{
    public setStep:Function=()=>{};
    public updateProgBar:Function=()=>{};
    public doAutoPlay:Function=()=>{};

    static getInstance()
    {
        if (instance===null)
        {
            instance=new MainController();
        }
        return instance;
    }
}