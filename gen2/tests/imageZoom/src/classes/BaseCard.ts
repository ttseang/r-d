import { IBaseScene } from "ttphasercomps";
import { ICard } from "../interfaces/ICard";
import { HtmlObj } from "./HtmlObj";



export class BaseCard extends HtmlObj implements ICard {
    public zoomImage: Phaser.GameObjects.Image;
    private prevX:number=-100;
    private prevY:number=-100;
    private prevZoomX:number=-100;
    private prevZoomY:number=-100;

    constructor(id: string) {
        super(id);
    }
    setText(key: string, text: string): void {
        throw new Error("Method not implemented.");
    }
    flyTo(xx: number, yy: number) {
        throw new Error("Method not implemented.");
    }
    setHighlight(className: string, index: number) {
        throw new Error("Method not implemented.");
    }
    public get width() {
        if (!this.el) {
            return 0;
        }
        return this.el.getBoundingClientRect().width;
    }
    public get height() {
        if (!this.el) {
            return 0;
        }
        return this.el.getBoundingClientRect().height;
    }
    
    setToImage(bscene: IBaseScene, x1: number, y1: number, useTween: boolean) {

        if (x1==this.prevX && y1==this.prevY && this.zoomImage.scaleX==this.prevZoomX && this.zoomImage.scaleY==this.prevZoomY)
        {
            return;
        }
        this.prevX=x1;
        this.prevY=y1;
        this.prevZoomX=this.zoomImage.scaleX;
        this.prevZoomY=this.zoomImage.scaleY;


        let xratio: number = this.zoomImage.displayWidth / bscene.getW();
        let yratio: number = this.zoomImage.displayHeight / bscene.getH();

        let startX: number = this.zoomImage.x / bscene.getW();
        let startY: number = this.zoomImage.y / bscene.getH();

        let xx: number = x1 * xratio;
        let yy: number = y1 * yratio;

        


        /*  this.x=imageVo.x;
         this.y=imageVo.y; */
        if (useTween === true) {
            super.flyTo(xx, yy);

        }
        else {
            this.x = xx;
            this.y = yy;
        }
    }

}