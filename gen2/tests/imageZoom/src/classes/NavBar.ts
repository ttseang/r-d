import { LinkUtil } from "../util/LinkUtil";
import { BottomBar } from "./BottomBar";
import { GM } from "./GM";
import { HtmlObj } from "./HtmlObj";
import { MainController } from "./MainController";

export class NavBar extends HtmlObj
{
    private btnUp:HtmlObj=new HtmlObj("btnNavUp");
    private btnDown:HtmlObj=new HtmlObj("btnNavDown");
    public bottomBar:BottomBar | null=null;
    private btnAuto:HtmlObj=new HtmlObj("btnAuto");
    private btnRead:HtmlObj=new HtmlObj("btnRead");
    private btnListen:HtmlObj=new HtmlObj("btnListen");

    private mc:MainController=MainController.getInstance();
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("topBar");

        this.btnDown.visible=false;
        if (this.btnUp.el)
        {
            this.btnUp.el.onclick=()=>{
                this.closeBar();
            }
        }
        if (this.btnDown.el)
        {
            this.btnDown.el.onclick=()=>{
                this.openBar();
            }
        }
        LinkUtil.makeLink("btnAuto",this.doAction.bind(this),"doAuto","");
        LinkUtil.makeLink("btnListen",this.doAction.bind(this),"setMode","listen");
        LinkUtil.makeLink("btnRead",this.doAction.bind(this),"setMode","read");
    }
    private closeBar()
    {
        this.y=-15;
        this.btnUp.visible=false;
        this.btnDown.visible=true;
        this.bottomBar.close();
    }
    private openBar()
    {
        this.y=-2;
        this.btnUp.visible=true;
        this.btnDown.visible=false;
        if (this.bottomBar)
        {
            this.bottomBar.open();
        }
    }
    private doAction(action:string,param:string)
    {
        
        switch(action)
        {
            case "doAuto":
            this.closeBar();
            this.mc.doAutoPlay();
            break;

            case "setMode":
            this.gm.playMode=param;
            break;
        }
        
    }
}