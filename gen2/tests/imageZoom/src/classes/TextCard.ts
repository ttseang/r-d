import { IBaseScene } from "ttphasercomps";
import { StyleVo } from "../dataObjs/StyleVo";
import { ICard } from "../interfaces/ICard";
import { BaseCard } from "./BaseCard";
import { HtmlObj } from "./HtmlObj";

export class TextCard extends BaseCard implements ICard {
    public zoomImage: Phaser.GameObjects.Image;

    constructor(index: number, zoomImage: Phaser.GameObjects.Image) {
        super("textCard" + index.toString());
        this.zoomImage = zoomImage;
        //console.log(this.id);
    }
   
    setInfo(content: string) {
        if (this.el) {
            let texts: HTMLCollectionOf<HTMLDivElement> = this.el.getElementsByTagName("div");

            if (texts[0]) {
                texts[0].innerHTML = content;
            }
        }
    }
    setText(key: string, text: string) {
        let texts: HTMLCollectionOf<HTMLDivElement> = this.el.getElementsByTagName("div");

        if (key == "content") {
            if (texts[0]) {
                texts[0].innerHTML = text;
            }
        }
    }
    setBackground(key: string): void {

        if (this.el) {
            if (key == "none") {
                this.el.style.background = "none";
                
            }
            else {
                this.el.style.background = "rgba(255,255,255,0.6)";
                
            }
        }
    }
    setHighlight(className: string, index: number) {
        if (this.el) {
            let highText: HTMLCollectionOf<HTMLSpanElement> = this.el.getElementsByTagName('span');


            if (highText.length > index - 1) {
                let styleText: StyleVo = this.getStyle(className);

                requestAnimationFrame(() => {
                    highText[index].style.color = styleText.color;
                    highText[index].style.fontSize = styleText.fontSize;
                    highText[index].style.fontWeight = styleText.fontWeight;
                })


            }
        }
    }
}