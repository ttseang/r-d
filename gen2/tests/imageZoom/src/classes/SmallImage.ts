import { IBaseScene } from "ttphasercomps";
import { ImageVo } from "../dataObjs/ImageVo";
import { HtmlObj } from "./HtmlObj";

export class SmallImage extends HtmlObj {
    public zoomImage: Phaser.GameObjects.Image;

    constructor(index: number, zoomImage: Phaser.GameObjects.Image) {
        super("imageFrame" + index.toString());
        this.zoomImage = zoomImage;
    }
    setImage(src: string) {
        let imageTag: HTMLImageElement | null = this.getImageTag();
        if (imageTag) {
            imageTag.src = src;
        }
    }
    getImageTag() {
        if (this.el) {
            let texts: HTMLCollectionOf<HTMLImageElement> = this.el.getElementsByTagName("img");

            if (texts[0]) {
                return texts[0];
            }
        }
        return null;
    }

    setToImage(bscene: IBaseScene, imageVo: ImageVo, useTween: boolean) {
        let ww: number = this.zoomImage.displayWidth;
        let hh: number = this.zoomImage.displayHeight;
        let imageTag: HTMLImageElement | null = this.getImageTag();

      //  console.log("border=" + imageVo.border);


        if (imageTag) {
            if (imageVo.border == false) {

                imageTag.style.borderStyle = "none";

            }
            let w2: number = ww * imageVo.scale;
            //    console.log("w2="+w2);

            imageTag.src = imageVo.src;
            imageTag.width = w2;



            let xratio: number = this.zoomImage.displayWidth / bscene.getW();
            let yratio: number = this.zoomImage.displayHeight / bscene.getH();

            let startX: number = this.zoomImage.x / bscene.getW();
            let startY: number = this.zoomImage.y / bscene.getH();

            let xx: number = (imageVo.x) * xratio;
            let yy: number = (imageVo.y) * yratio;

            if (useTween == false) {
                this.x = imageVo.x;
                this.y = imageVo.y;
            }
            else {
                this.flyTo(xx, yy);
            }



        }

    }
    setFromObj(imageVo: ImageVo) {
        let ww: number = window.innerWidth;
        let hh: number = window.innerHeight;

        let imageTag: HTMLImageElement | null = this.getImageTag();
        if (imageTag) {
            let w2: number = ww * imageVo.scale;
            //console.log("w2="+w2);

            imageTag.src = imageVo.src;
            imageTag.width = w2;
            /*  this.x=imageVo.x;
             this.y=imageVo.y; */
            this.flyTo(imageVo.x, imageVo.y);
        }
    }
}