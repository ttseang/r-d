import { QuizVo } from "../dataObjs/QuizVo";
import { StyleVo } from "../dataObjs/StyleVo";
import { ICard } from "../interfaces/ICard";
import { HtmlObj } from "./HtmlObj";

export class QuizBox extends HtmlObj {

    public texts: HTMLCollectionOf<HTMLDivElement>;
    public questionText: HTMLDivElement;
    public answersText: HtmlObj[] = [];
    private data: QuizVo | null = null;
    public dart: HtmlObj;
    public btnCheck: HtmlObj;
    public selectedIndex: number = -1;
    public actionCallback:Function=()=>{};

    constructor() {
        super("quizBox");

        this.dart = new HtmlObj("dart");

        this.texts = this.el.getElementsByTagName("div");
      //  console.log(this.texts);
        let answerIndex: number = 0;

        for (let i: number = 0; i < this.texts.length; i++) {
            let t: HTMLDivElement = this.texts[i];
            if (t.id == "quizQuestion") {
                this.questionText = t;
            }
            if (t.className.startsWith("quizAnswer")) {
                let obj: HtmlObj = new HtmlObj("answer" + answerIndex.toString());
                
                this.answersText.push(obj);
                answerIndex++;
            }
        }
        this.btnCheck = new HtmlObj("btnCheck");
        this.btnCheck.el.onclick = this.checkAnswer.bind(this);
        //this.btnCheck.visible=false;
        this.btnCheck.alpha=0.5;

        this.reset();
    }
    setData(q: QuizVo) {
        //console.log(q);
        this.data = q;
        this.setQuestion(q.question);
        this.hideButtons();
        for (let i: number = 0; i < q.answers.length; i++) {
            this.setAnswer(i, q.answers[i]);
        }
      //  this.btnCheck.y = q.answers.length * 10 + 40;
      //  this.btnCheck.x=25;
    }
    makeNumbers(len:number,active:number)
    {
        let numberDiv:HTMLDivElement | null=document.getElementById("quizNumbers") as HTMLDivElement || null;
        if (numberDiv)
        {
            numberDiv.innerHTML="";

            for (let i:number=0;i<len;i++)
            {
                let i2:number=i+1;
                if (i==active)
                {
                    numberDiv.innerHTML+="<span class='qnumActive'>"+i2.toString()+"</span>";
                }
                else
                {
                    numberDiv.innerHTML+="<span class='qnum'>"+i2.toString()+"</span>";
                }
                
               // numberDiv.append("<span>"+i.toString()+"</span>");
            }
        }

    }
    setQuestion(text: string) {
        if (this.questionText) {
            this.questionText.innerHTML = text;
        }
    }
    setAnswer(index: number, text: string) {
        
        if (this.answersText[index]) {
            this.answersText[index].y = index * 10 + 40;
            this.answersText[index].x=25;
            this.answersText[index].visible = true;

             this.answersText[index].el.innerHTML=text;
            // this.answersText[index].style.top=(index*10+20).toString()+"%";
            this.answersText[index].el.onclick = () => {
                this.clickAnswer(index);
            }
        }

    }
    hideButtons() {
        for (let i: number = 0; i < this.answersText.length; i++) {
            this.answersText[i].visible=false;
        }
    }
    resetButtons() {
        for (let i: number = 0; i < this.answersText.length; i++) {
            this.answersText[i].el.style.backgroundColor = "white";
        }
    }
    clickAnswer(index: number) {
        console.log(index);
        this.selectedIndex=index;
        this.resetButtons();
        this.answersText[index].el.style.backgroundColor = "lightblue";
        this.btnCheck.alpha=1;

    }
    checkAnswer() {
        if (this.btnCheck.alpha<1)
        {
            return;
        }
        this.btnCheck.alpha=0.5;
        this.placeDart(this.data.correct);
        if (this.answersText[this.selectedIndex] && this.data.correct !== this.selectedIndex) {
            this.answersText[this.selectedIndex].el.style.backgroundColor = "red";
        }
        else
        {
            this.answersText[this.selectedIndex].el.style.backgroundColor = "lightgreen";
        }
        setTimeout(() => {
            this.actionCallback("nextQuestion");
        }, 2000);
    }
    reset() {
        //let dart:HTMLImageElement=document.getElementById("dart") as HTMLImageElement;
        this.dart.visible = false;

        this.dart.x=120;
        this.dart.y=0;

        for (let i: number = 0; i < this.answersText.length; i++) {
            this.answersText[i].el.style.backgroundColor="white";
        }
    }
    placeDart(index: number) {
        this.dart.visible=true;
        this.dart.y = this.answersText[index].y;
        this.dart.x = this.answersText[index].x+40;
        
        if (this.answersText[index]) {
            this.answersText[index].el.style.backgroundColor = "lightgreen";
        }

    }
}