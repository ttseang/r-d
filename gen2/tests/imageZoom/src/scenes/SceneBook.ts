import { BaseScene } from "ttphasercomps";
import { BottomBar } from "../classes/BottomBar";
import { DPage } from "../classes/DPage";
import { GM } from "../classes/GM";
import { HtmlObj } from "../classes/HtmlObj";
import { NavBar } from "../classes/NavBar";
import { LinkUtil } from "../util/LinkUtil";

export class SceneBook extends BaseScene {

    private gm: GM = GM.getInstance();

    public dPage: DPage
    public btnNext: HtmlObj;
    public btnPrev: HtmlObj;
    public topBar:NavBar=new NavBar();
    public bottomBar:BottomBar=new BottomBar(this);

    constructor() {
        super("SceneBook");
        this.btnNext = new HtmlObj("btnNext");
        this.btnPrev = new HtmlObj("btnPrev");
        this.btnPrev.visible = false;
        
    }
    preload() {
        
        for (let i: number = 0; i < this.gm.files.length; i++) {
            this.load.image(this.gm.files[i].key, this.gm.files[i].path);
        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);
     

        this.makePage();
        this.topBar.visible=true;
        this.topBar.bottomBar=this.bottomBar;
        this.bottomBar.visible=true;
        this.bottomBar.y=86;
    }
    makePage() {
        this.dPage = new DPage(this);
        this.dPage.actionCallback = this.doPageAction.bind(this);

        (window as any).scene = this;

        window.onresize = this.doResize.bind(this);

        //  this.loadPage();

        LinkUtil.makeLink("btnNext", this.doAction.bind(this), "turnPage", "forward");
        LinkUtil.makeLink("btnPrev", this.doAction.bind(this), "turnPage", "back");

        this.dPage.setStepData(this.gm.stepObjs);
    }
    doAction(action: string, param: string) {
        if (action == "turnPage") {
            if (param == "forward") {

                this.btnPrev.visible = true;
                this.dPage.nextStep();
                if (this.dPage.stepIndex == this.dPage.zsteps.length - 1) {
                    this.btnNext.visible = false;
                }
            }
            else {

                this.btnNext.visible = true;
                this.dPage.prevStep();
                if (this.dPage.stepIndex == 0) {
                    this.btnPrev.visible = false;
                }
            }
        }
    }
    
    doPageAction(action: string, param: string) {
        switch (action) {
            case "hideButtons":
                this.btnNext.visible = false;
                this.btnPrev.visible = false;
                break;
            case "showButtons":
                this.btnNext.visible = true;
                this.btnPrev.visible = true;
                break;
        }
    }
    doResize() {

       /*  var width = window.outerWidth;
        var height = window.outerHeight;

        width = Math.min(width, 1000);  // may not exceed 1000 maximum
        height = Math.min(height, 700);

        width = Math.max(width, 500);   // may not exceed 500 minimum
        height = Math.max(height, 350);

        height = width * 7 / 10;

        window.resizeTo(width,height); */


        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        console.log(w,h);

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);

        this.dPage.doResize();
    }
}