import { BaseScene } from "ttphasercomps";
import { GM } from "../classes/GM";
import { CardVo } from "../dataObjs/CardVo";
import { FileVo } from "../dataObjs/FileVo";
import { ImageVo } from "../dataObjs/ImageVo";
import { LineVo } from "../dataObjs/LineVo";
import { QuizVo } from "../dataObjs/QuizVo";
import { StepVo } from "../dataObjs/StepVo";
import { ZoomVo } from "../dataObjs/ZoomVo";

export class SceneData extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneData");
        this.loadPage();
    }
    loadPage() {
        fetch("./assets/pages/page2.json")
            .then(response => response.json())
            .then(data => this.process({ data }));
    }
    process(data: any) {

        let pl:any[]=data.data.preload;
        //console.log(pl);

        let files:FileVo[]=[];
        for (let i:number=0;i<pl.length;i++)
        {
            files.push(new FileVo(pl[i].key,pl[i].path));
        }
        //console.log(files);   
        this.gm.files=files;     

        let steps: any[] = data.data.steps;

        let stepObjs: StepVo[] = [];

        for (let i: number = 0; i < steps.length; i++) {
            let step: any = steps[i];

            let zoom: any = step.zoom;
            let cards: any[] = step.cards;
            let lines: any[] = step.lines;
            let images: any[] = step.images || [];
            let quizInfo:any[]=step.quiz || [];
            let thumb:string=step.thumb;
            let secs:string=step.secs;
            let audio:string=step.audio;

            let cardObjs: CardVo[] = [];
            let lineObjs: LineVo[] = [];
            let imageObjs: ImageVo[] = [];
            let quizObjs:QuizVo[]=[];
            
            for (let j: number = 0; j < cards.length; j++) {
                let relativetoimage:boolean=cards[j].relativetoimage || false;
                
              //  console.log(cards[j].relativetoimage);
                //console.log(relativetoimage);
                let background:string=cards[j].background;

                cardObjs.push(new CardVo(cards[j].type, cards[j].x, cards[j].y, cards[j].heading, cards[j].content, parseInt(cards[j].highlight),relativetoimage,background));
            }

            for (let k: number = 0; k < lines.length; k++) {
                lineObjs.push(new LineVo(lines[k].x1, lines[k].y1, lines[k].x2, lines[k].y2, lines[k].thick, lines[k].color));
            }

            for (let m: number = 0; m < images.length; m++) {
                let border:boolean=(images[m].border==1)?true:false;
                imageObjs.push(new ImageVo(parseFloat(images[m].x), parseFloat(images[m].y), parseFloat(images[m].scale), images[m].src,border));
            }

            for (let n:number=0;n<quizInfo.length;n++)
            {
                quizObjs.push(new QuizVo(quizInfo[n].q,quizInfo[n].a,parseInt(quizInfo[n].correct)));
            }
          //  //console.log(quizObjs);
            let stepVo:StepVo=new StepVo(new ZoomVo(zoom.x, zoom.y, zoom.z), cardObjs, lineObjs, imageObjs,quizObjs);
            stepVo.thumb=thumb;
            if (audio!="" && audio!=undefined)
            {
                stepVo.audio="./assets/audio/"+audio;
                console.log(stepVo.audio);
            }
           
            
            let secs2:number=parseInt(secs);
            if (isNaN(secs2))
            {
                secs2=10;
            }
            stepVo.secs=secs2;
            stepObjs.push(stepVo);

        }
        this.gm.stepObjs=stepObjs;
        this.scene.start("SceneBook");
      //  //console.log(stepObjs);
       // this.dPage.setStepData(stepObjs);
    }
}