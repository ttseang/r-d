const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: {
    app: './src/index.ts',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Phaser Game',
      template:"./src/index.html"
    }),
    new CopyPlugin({
      patterns: [
        { from: "public/assets", to: "assets" },{ from: "public/assets/audio", to: "assets/audio" },{ from: "src/style.css"}
      ],
    })
  ],
  output: {
    filename: 'bundle.min.js',
    path: path.resolve(__dirname, '../dist'),
    clean: true,
  },
  module: {
    rules: [
        {
            test: /\.tsx?$/,
            loader: 'ts-loader',
            exclude: /node_modules/,
        },
    ]
},
resolve: {
    extensions: [".tsx", ".ts", ".js"]
}
};