// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
//use std::process::Command;
use std::sync::{Arc, Mutex};
use tauri::{Manager,State};

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[derive(Default)]
struct Counter(Arc<Mutex<i32>>);
#[derive(Clone, serde::Serialize)]
struct Payload{
    message: String,
}

fn main() {
    tauri::Builder::default()
    
    .manage(Counter::default())
        .invoke_handler(tauri::generate_handler![open_file, print_file,read_file,add_count])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
//use crate::open::that;
extern crate open;
//use open::that;
#[tauri::command]
fn open_file(path: String)
{
   println!("trying to open!");
 
   match crate::open::that(&path)
    {
        Ok(()) => println!("Opened '{}' successfully.", path),
         Err(err) => eprintln!("An error occurred when opening '{}': {}", path, err),
    
    }
    //unresolved import `open` in this scope
    //to fix this, add `open` to the crate dependencies
    //and add `extern crate open;` to the crate root
    //for next loop but in rust
    
}

extern crate printers;
//use printers;
#[tauri::command]
fn print_file()
{
    //Printer::new().print("C:\\Users\\wcc19\\OneDrive\\Desktop\\text.txt");
    let printers = printers::get_printers();
    println!("Printers: {:?}", printers);
    let default_printer = &printers[0];
    println!("Default printer: {:?}", default_printer);  
    printers::print_file(&printers[0], "C:\\Users\\wcc19\\OneDrive\\Desktop\\text.txt");
  //print file as html
  
    println!("Trying to print");
}
use std::env;
use std::fs;



#[tauri::command]
fn read_file(path:String)->String
{
    let contents = fs::read_to_string(path)
        .expect("Should have been able to read the file");
    println!("With text:\n{}", contents);
    return contents;
}
#[tauri::command]
fn add_count(num: i32, counter: State<'_, Counter>,app_handle: tauri::AppHandle) -> String {
	let mut val = counter.0.lock().unwrap();
	*val += num;
   
     if *val>10
    {
        println!("{} is greater than 10",val);
        let main_window = app_handle.get_window("main").unwrap();
     //   main_window.emit_all("PROGRESS", Some(payload)).unwrap();
      main_window.emit_all("PROGRESS", Payload { message: "Tauri is awesome!".into() }).unwrap();
      //  app_handle.emit_all("backend-ping", Some(Payload("Hello from the backend!".to_string()))).unwrap();
    }
    format!("{val}")
}
