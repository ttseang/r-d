import { tauri } from "@tauri-apps/api";
import { invoke } from "@tauri-apps/api/tauri";
//import { Event as TauriEvent, listen } from '@tauri-apps/api/event';
const textArea = document.getElementById("ta") as HTMLTextAreaElement;
import { appWindow } from '@tauri-apps/api/window';

function openFile(path: string) {
  console.log("openFile");
  // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
  invoke("open_file", { path: path });

}
function printFile() {

  // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
  invoke("print_file", {});

}
async function upCount() {
  const countText = await invoke("add_count", { num: 2 }) as string;
  console.log(countText);
  const countDiv = document.querySelector(".clickCount") as HTMLDivElement;
  if (countDiv != null) {
    countDiv.innerText = countText;
  }
}
async function readFile(path: string) {
  let contents: string = await invoke("read_file", { path: path });
  textArea.value = contents;
}

window.addEventListener("DOMContentLoaded", async () => {
  console.log("DOMContentLoaded");
  document.querySelector("#btn1")?.addEventListener("click", () => openFile("https://www.google.com"));
  document.querySelector("#btn2")?.addEventListener("click", () => openFile("C:\\Users\\wcc19\\OneDrive\\Desktop\\DS82.pdf"));
  document.querySelector("#btn3")?.addEventListener("click", () => printFile());
  document.querySelector("#btn4")?.addEventListener("click", () => readFile("C:\\Users\\wcc19\\OneDrive\\Desktop\\text.txt"));
  document.querySelector("#btnCount")?.addEventListener("click", () => upCount());

  //show dev tools
 
  /* 
    listen("PROGRESS", function (evt: TauriEvent<any>) {
     // alert("PROGRESS",evt.payload);
      console.log("PROGRESS!!!!!!!!",evt.payload);
    }) */
    console.log("appWindow",appWindow);
  const unlistenProgress = await appWindow.listen(
    'PROGRESS',
    ({ event, payload }) => {
      console.log(event);
      console.log("PROGRESS!!!!!!!!", payload);
    }
  );
  
});
