import { invoke } from "@tauri-apps/api/tauri";
import { isPermissionGranted, requestPermission, sendNotification } from '@tauri-apps/api/notification';
import { BaseScreen } from "./classes/view/BaseScreen";
import { ServerController } from "./classes/mc/ServerController";

let greetInputEl: HTMLInputElement | null;
let greetMsgEl: HTMLElement | null;
const serverController: ServerController = ServerController.getInstance();

async function greet() {
  if (greetMsgEl && greetInputEl) {
    // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
    greetMsgEl.textContent = await invoke("greet", {
      name: greetInputEl.value,
    });
  }
}  

function notify(title:string="Tauri is awesome!",body:string="Tauri is awesome!") {

  if (permissionGranted) {
    sendNotification('Tauri is awesome!');
    sendNotification({ title, body });
  }
}

window.addEventListener("DOMContentLoaded", () => {
  console.log("DOMContentLoaded");
  greetInputEl = document.querySelector("#greet-input");
  greetMsgEl = document.querySelector("#greet-msg");
  document
    .querySelector("#greet-button")
    ?.addEventListener("click", () => {
      greet();
      notify();
    });
  
  //open dev tools
  //invoke("openDevTools");
//  console.log("main.ts");
   // makeMenu();

    

    serverController.showNotification=notify;
   const baseScreen:BaseScreen=new BaseScreen();
    baseScreen.loadServerList();
});

let permissionGranted = await isPermissionGranted();
if (!permissionGranted) {
  const permission = await requestPermission();
  permissionGranted = permission === 'granted';
}