import { ServerStatus } from "../ServerConstants";
import { StatusVo } from "./StatusVo";

//label and domain name
export class ServerVo {
    public cluster:number = 0;
    public label:string;
    public domainName:string;
    public statusVo:StatusVo;
    public loading:boolean = true;
    public constructor(cluster:number,label:string, domainName:string) {
        this.cluster = cluster;
        this.label = label;
        this.domainName = domainName;
        this.statusVo = new StatusVo(ServerStatus.Unknown, -1);
    }
}