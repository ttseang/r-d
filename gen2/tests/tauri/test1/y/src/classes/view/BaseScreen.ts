import { ClusterVo } from "../dataObjects/ClusterVo";
import { ServerVo } from "../dataObjects/ServerVo";
import { ServerController } from "../mc/ServerController";
import { ServerModel } from "../mc/ServerModel";
import { ServerChecker } from "../utility/ServerChecker";
import { ClusterScreen } from "./ClusterScreen";
import { Monitor } from "./Monitor";

export class BaseScreen
{
    private sm:ServerModel=ServerModel.getInstance();
    private monitor:Monitor | null=null;
    private clusterScreen:ClusterScreen | null=null;
    private serverController:ServerController=ServerController.getInstance();
    private serverChecker:ServerChecker = new ServerChecker();
 //   private mode:"cluster" | "monitor"="cluster";

    constructor()
    {
        //do nothing
        console.log("BaseScreen");
       // this.loadServerList();
    }
    
    loadServerList(): void {
        console.log("loadServerList");
        fetch("src/assets/serverList.json")
            .then(response => response.json())
            .then(data => this.serverListLoaded(data));
    }
    serverListLoaded(data: any): void {
        console.log(data);

        let clusters:any[] = data.clusters;
        for (let i: number = 0; i < clusters.length; i++) {
            let cluster: ClusterVo = new ClusterVo(clusters[i].id,clusters[i].label);
            this.sm.clusterList.push(cluster);
        }

        let servers: any[] = data.servers;
        for (let i: number = 0; i < servers.length; i++) {
            let server: ServerVo = new ServerVo(parseInt(servers[i].cluster),servers[i].label, servers[i].domain);
            this.sm.serverList.push(server);
        }
        
        this.serverChecker.start();
        
        this.showClusterList();

        this.serverController.showList=this.showMonitor.bind(this);
        this.serverController.showCluster=this.showClusterList.bind(this);
    }
    showMonitor(cluster:number):void
    {
        this.serverController.checkNow();
        this.clearScreen();
        this.monitor=new Monitor(cluster);
        this.monitor.start();
    }
    showClusterList():void
    {
        this.clearScreen();
        this.clusterScreen=new ClusterScreen();
        this.clusterScreen.start();
    }
    private clearScreen() {
        const main: HTMLElement | null = document.querySelector("main") || document.createElement("main");
        main.innerHTML = "";
    }
}