import { ServerVo } from "../dataObjects/ServerVo";
import { StatusVo } from "../dataObjects/StatusVo";
import { ServerController } from "../mc/ServerController";
import { ServerModel } from "../mc/ServerModel";
import { ServerChecker } from "../utility/ServerChecker";
import { StatusBar } from "./StatusBar";

export class Monitor {
    private sm: ServerModel = ServerModel.getInstance();
    public serverList: ServerVo[] = [];
    private serverController: ServerController=ServerController.getInstance();
    private cluster:number=0;
   
    public constructor(cluster:number) {
        //this.loadServerList();
        this.cluster=cluster;
        this.serverList=this.sm.getCluster(cluster)
       
    }
    public start(): void {
        this.makeScreen();
        this.serverController.updateDraw=this.makeRows.bind(this);
        this.serverController.updateTime=this.updateTimer.bind(this);
        this.makeRows();
      //  this.timeToCheckList();
    }
    private updateTimer(sec:number)
    {
        const secsTextHolder:HTMLElement=document.querySelector(".secondText") as HTMLElement;
        if (secsTextHolder)
        {
            secsTextHolder.innerHTML = sec.toString() + " seconds until next check";
        }
    }
    private makeScreen() {
        const main: HTMLElement | null = document.querySelector("main") || document.createElement("main");
        let section: HTMLElement = document.createElement("section");
        section.className = "v4monitor";

        let top: HTMLElement = document.createElement("div");
        top.className = "top";
        section.appendChild(top);


        let h2: HTMLElement = document.createElement("h2");
        h2.innerHTML = "4.0 App Server Endpoints";
        top.appendChild(h2);

        let clusterButton: HTMLElement = document.createElement("button");
        clusterButton.className = "clusterButton";
        clusterButton.innerHTML = "Show Clusters";
        top.appendChild(clusterButton);

        clusterButton.onclick=()=>{
            this.serverController.showCluster();
        }

        let ul: HTMLElement = document.createElement("ul");
        ul.className = "statusRows";
        section.appendChild(ul);

        let p: HTMLElement = document.createElement("p");
        p.className = "secondText";
        p.innerHTML = "30 seconds until next check";
        section.appendChild(p);

        main.appendChild(section);

    }
    
   /*  loadServerList(): void {
        fetch("./assets/serverList.json")
            .then(response => response.json())
            .then(data => this.serverListLoaded(data));
    }
    serverListLoaded(data: any): void {
        //console.log(data);
        let servers: any[] = data.servers;
        for (let i: number = 0; i < servers.length; i++) {
            let server: ServerVo = new ServerVo(servers[i].label, servers[i].domain);
            this.serverList.push(server);
        }
        // 
        this.timeToCheckList();
        // this.doTick();
    } */
    /* checkServerStatus(server: ServerVo): void {
        const serverChecker: ServerChecker = new ServerChecker();
        serverChecker.checkServerStatus(server, this.gotServerStatus.bind(this));
    }
    gotServerStatus(server: ServerVo): void {
        server.loading = false;
        this.makeRows();
        this.checkNextServer();
    } */

  /*   checkNextServer(): void {
        this.serverIndex++;
        if (this.serverIndex >= this.serverList.length) {
            this.serverIndex = 0;
            this.doTick();
            return;
        }
        this.checkServerStatus(this.serverList[this.serverIndex]);
    }
    timeToCheckList(): void {
        this.setAllToLoading();
        this.serverIndex = -1;
        this.checkNextServer();
    } */
    /* doTick(): void {
        this.timeToCheckSeconds--;
        if (this.secondsTextHolder) {
            this.secondsTextHolder.innerHTML = this.timeToCheckSeconds.toString() + " seconds until next check";
        }
        if (this.timeToCheckSeconds == 0) {
            this.timeToCheckSeconds = 31;
            this.timeToCheckList();
        }
        else {
            //  console.log("Seconds left: " + this.timeToCheckSeconds);
            setTimeout(() => {
                this.doTick();
            }, 1000);
        }
    } */

   
    makeRows() {
        this.serverList=this.sm.getCluster(this.cluster);
        const parent: HTMLElement | null = document.querySelector(".statusRows") || document.createElement("ul");
        if (parent.classList.contains("statusRows") == false) {
            parent.classList.add("statusRows");
        }
        //empty the parent
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }

        //make rows
        for (let i: number = 0; i < this.serverList.length; i++) {
            let statusBar: StatusBar = new StatusBar(this.serverList[i], parent);
        }
    }
}