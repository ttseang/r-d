import { ServerVo } from "../dataObjects/ServerVo";
import { StatusVo } from "../dataObjects/StatusVo";
import { ServerController } from "../mc/ServerController";
import { ServerModel } from "../mc/ServerModel";
import { ServerStatus } from "../ServerConstants";

export class ServerChecker {
    private sm: ServerModel = ServerModel.getInstance();
    private serverIndex: number = -1;
    private timeToCheckSeconds: number = 30;
    private serverController: ServerController = ServerController.getInstance();
    
    constructor() {
      //  this.serverController.checkNow=this.reset.bind(this);
    }
    start(): void {
        this.timeToCheckList();
    }
    reset(): void {
        this.timeToCheckSeconds = 30;
        this.timeToCheckList();
    }
    timeToCheckList(): void {
        this.setAllToLoading();
        this.serverIndex = -1;
        this.checkNextServer();
    }
    doTick(): void {
        this.timeToCheckSeconds--;
       // //console.log("Seconds left: " + this.timeToCheckSeconds);
        /* if (this.secondsTextHolder) {
            this.secondsTextHolder.innerHTML = this.timeToCheckSeconds.toString() + " seconds until next check";
        } */
        if (this.timeToCheckSeconds == 0) {
            this.timeToCheckSeconds = 31;
            this.timeToCheckList();
        }
        else {
            //  //console.log("Seconds left: " + this.timeToCheckSeconds);
            setTimeout(() => {
                this.doTick();
            }, 1000);
        }
        this.serverController.updateTime(this.timeToCheckSeconds);
    }
    setAllToLoading(): void {
        for (let i: number = 0; i < this.sm.serverList.length; i++) {
            this.sm.serverList[i].loading = true;
        }
        this.serverController.updateDraw();
    }
    checkNextServer(): void {
        this.serverIndex++;
        if (this.serverIndex >= this.sm.serverList.length) {
            this.serverIndex = 0;
            this.doTick();
            return;
        }
        this.checkServerStatus(this.sm.serverList[this.serverIndex]);
    }
    checkServerStatus(server: ServerVo) {

        //check the status of the server, also get the time it took to load

        const fullURL: string = "https://" + server.domainName;
        const startTime: number = new Date().getTime();
        //fetch the header of the server


        fetch(fullURL,{method:"Head"}).then(response => {
            const endTime: number = new Date().getTime();
            const time: number = endTime - startTime;

            if (response.status == 200) {
                //console.log("Server is up");
                server.statusVo = new StatusVo(ServerStatus.Up, time);
                server.loading = false;
                this.sm.serverList[this.serverIndex] = server;
                this.serverController.updateDraw();
                this.checkNextServer();
                //callback(server);
            }
            else {
            
                server.statusVo = new StatusVo(ServerStatus.Down, time);
                server.loading = false;
                this.sm.serverList[this.serverIndex] = server;
                this.serverController.updateDraw();
                this.checkNextServer();
              //  this.serverController.showNotification("Alert","Server is down");
               // callback(server);
            }
        }).catch(error => {
            console.log("Server is down", error);
            server.statusVo = new StatusVo(ServerStatus.Down, -1);
            server.loading = false;
            this.sm.serverList[this.serverIndex] = server;
            this.serverController.updateDraw();
            this.checkNextServer();
          //  this.serverController.showNotification("Alert","Server is down");

           // callback(server);
        });
    }
}