export class ServerController
{
    private static instance:ServerController;
    public updateDraw:Function=()=>{};
    public updateTime:Function=()=>{};
    public showList:Function=()=>{};
    public showCluster:Function=()=>{};
    public checkNow:Function=()=>{};
    public showNotification:Function=()=>{};
    private constructor()
    {
        //do nothing
    }
    static getInstance():ServerController
    {
        if (!ServerController.instance)
        {
            ServerController.instance=new ServerController();
            
        }
        return ServerController.instance;
    }
}