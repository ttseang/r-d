export class BGPlayer {
    private player: HTMLAudioElement
    private basePath:string;

    constructor(basePath:string="") {
        this.basePath=basePath;
        this.player = document.createElement("audio");
        this.player.id = "bgPlayer";
        this.player.loop = true;
    }
    setPath(path: string) {
        this.player.src =this.basePath+path;
        this.player.currentTime=0;
    }
    play() {
        this.player.play();
    }
    stop() {
        this.player.pause();
        this.player.currentTime=0;
    }
    pause() {
        this.player.pause;
    }
    setVol(vol: number) {
        if (vol>-1 && vol<101)
        {
            this.player.volume=vol/100;
            console.log(vol/100);
        }
    }
}