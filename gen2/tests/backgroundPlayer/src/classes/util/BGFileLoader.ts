import ApiConnect from "../ApiConnect";
import { BGFileVo } from "../dataObjs/BGFileVo";
import { MPVo } from "../dataObjs/MPVo";

export class BGFileLoader {
    private callback:Function;

    constructor(callback:Function) {
        this.callback=callback;
    }
    loadFiles() {
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getByTag("X.LOOPER.01", this.filesLoaded.bind(this));

    }
    filesLoaded(data: any) {
        let sounds: any[] = data.sounds;
        let audioFiles:BGFileVo[]=[];

        for (let i: number = 0; i < sounds.length; i++) {
            let sound: any = sounds[i];
            //
            //
            //
            let mp3Obj: any = sound.mp3;
            let mp4Obj: any = sound.mp4;
            //
            //
            //

            let mp3: MPVo = new MPVo(mp3Obj.id, mp3Obj.bytes, mp3Obj.dur, mp3Obj.etag, mp3Obj.path);
            let mp4: MPVo = new MPVo(mp4Obj.id, mp4Obj.bytes, mp4Obj.dur, mp4Obj.etag, mp4Obj.path);
            //
            //
            //

            let bGFileVo: BGFileVo = new BGFileVo(sound.name, sound.title, sound.kind, mp3, mp4, sound.tags);
            audioFiles.push(bGFileVo);
        }
        this.callback(audioFiles);
    }
}