//import { MainStorage } from "./MainStorage";

export class ApiConnect {
    private authKey: string = "DQho4TTbytnJc31puZNDxfhY";

    public save(data: any, callback: Function) {

        let url: string = "https://tthq.me/api/dv/endpointX";
        fetch(url, {
            method: "post",
            body: data,
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
    public getFileList(callback: Function,type:string) {

        let url: string = "https://tthq.me/api/dv/listX?type="+type;
        this.doPost(url,callback);
    }
    public getByTag(tagName:string,callback:Function)
    {
        let url:string="https://tthq.me/api/tag/find?t[]="+tagName;
        this.doPost(url,callback);

    }
    public getFileContent(lti:string,callback:Function)
    {
        let url: string = "https://tthq.me/api/dv/dataX/"+lti;
        this.doPost(url,callback);
    }
    public getInfoByCode(lti:string,callback:Function)
    {
        let url: string = "https://tthq.me/api/dv/listX?lti="+lti;
        this.doPost(url,callback);
    }
    public deleteFile(lti:string,callback:Function)
    {
        let url: string = "https://tthq.me/api/dv/deleteX/"+lti;
        fetch(url, {
            method: "delete",
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
    private doPost(url:string,callback:Function)
    {
        fetch(url, {
            method: "post",
            headers: new Headers({ 'Authorization': this.authKey })
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        callback(json);
                    });
                }
            })
    }
}
export default ApiConnect;
