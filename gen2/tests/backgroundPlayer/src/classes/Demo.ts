import { BGPlayer } from "./BGPlayer";
import { BGFileVo } from "./dataObjs/BGFileVo";
import { BGFileLoader } from "./util/BGFileLoader";

export class Demo
{
    private bgPlayer:BGPlayer;
    private fileLoader:BGFileLoader;
    private backgroundFiles:BGFileVo[]=[];
    private musicList:HTMLSelectElement;
    private volSlider:HTMLInputElement;


    constructor()
    {
        this.bgPlayer=new BGPlayer("https://ttv5.s3.amazonaws.com/");
        this.fileLoader=new BGFileLoader(this.gotFiles.bind(this));
        this.fileLoader.loadFiles();

        this.musicList=document.getElementById("musicList") as HTMLSelectElement;
        this.musicList.addEventListener("change",this.selectChanged.bind(this));

        this.volSlider=document.getElementById("volSlider") as HTMLInputElement;
        this.volSlider.addEventListener("pointermove",this.volChanged.bind(this));

    }
    gotFiles(files:BGFileVo[])
    {
        this.backgroundFiles=files;
        
        let optHtml:string="<option>None</option>";

        for (let i:number=0;i<files.length;i++)
        {
            optHtml+="<option>"+files[i].title+"</option>";
        }
        this.musicList.innerHTML=optHtml;

       // this.bgPlayer.setPath(files[0].mp3.path);
       // this.bgPlayer.play();
    }
    selectChanged(e:Event)
    {
     
        let index:number=this.musicList.selectedIndex;

        if (index===0)
        {
            this.bgPlayer.stop();
            return;
        }
      
        let file:BGFileVo=this.backgroundFiles[index-1];
        this.bgPlayer.setPath(file.mp3.path);
        this.bgPlayer.play();
    }
    volChanged(e:Event)
    {
        let obj:any=e.target;
       // console.log(obj.value)
       this.bgPlayer.setVol(parseInt(obj.value));
    }
}