export class MPVo {
    public id: number;
    public bytes: number;
    public duration: number;
    public etag: string;
    public path: string;

    constructor(id: number, bytes: number, duration: number, etag: string, path: string) {
        this.id=id;
        this.bytes=bytes;
        this.duration=duration;
        this.etag=etag;
        this.path=path;
    }
}