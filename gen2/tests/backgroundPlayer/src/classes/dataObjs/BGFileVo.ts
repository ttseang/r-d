import { MPVo } from "./MPVo";

export class BGFileVo {
    public name: string
    public title: string;
    public kind: string;
    public mp3: MPVo;
    public mp4: MPVo;
    public tags:string[];

    constructor(name: string, title: string, kind: string, mp3: MPVo, mp4: MPVo,tags:string[]) {
        this.name = name;
        this.title = title;
        this.kind = kind;
        this.mp3 = mp3;
        this.mp4 = mp4;
        this.tags=tags;
    }
}