import { MathGrid } from "@teachingtextbooks/mathgriddisplay/dist/MathGrid";

window.onload = function () {
    //teachtextbooks mathgrid

    //inject the standard mathgrid css into the page
    //only use this if you are not providing your own mathgrid css
    MathGrid.injectCSS();

    //get the element that will contain the mathgrid
    //it can be any element that you can get a reference to
    
    let el: HTMLElement = document.getElementsByClassName("mathGrid")[0] as HTMLElement;
    if (el) {
        const lti: string = "TT.RD.SIMPLE";
        //OTHER LTI OPTIONS FOR TESTING
        //const lti:string = "TT.RD.SAMPLES"; 

        //pass in the lti, the element, and a callback function

        const md: MathGrid = new MathGrid(lti, el,0, () => {
            //this callback is called when the mathgrid is ready to be used
            //you can use this to show a specific step
            // md.showStep(0);
        });
    }
}