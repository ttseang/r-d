import * as Showdown from "showdown";
import { HProbVo } from "./dataObjs/HProbVo";
import { SelectComp } from "./SelectComp";

export class HpEngine {
    private selectComp: SelectComp;
    private problems: HProbVo[] = [];
    private problemIndex: number = -1;

    private btnGo: HTMLButtonElement;
    private messageArea: HTMLDivElement;
    private instructionsArea:HTMLDivElement;

    private converter:Showdown.Converter=new Showdown.Converter();

    private clickLock: boolean = false;
    constructor() {
        this.selectComp = new SelectComp();
        this.btnGo = document.getElementById("btnGo") as HTMLButtonElement || document.createElement("button");
        if (this.btnGo.id !== "btnGo") {
            this.btnGo.id = "btnGo";
            this.btnGo.innerText = "GO";
            document.body.append(this.btnGo);
        }

        this.messageArea = document.getElementById("message") as HTMLDivElement || document.createElement("div");
        if (this.messageArea.id !== "message") {
            this.messageArea.id = "message";
            this.messageArea.innerText = "Message Here";
            document.body.append(this.messageArea);
        }

        this.instructionsArea = document.getElementById("instructions") as HTMLDivElement || document.createElement("div");
        if (this.instructionsArea.id !== "instructions") {
            this.instructionsArea.id = "instructions";
            this.instructionsArea.innerText = "Instructions Here";
            document.body.append(this.instructionsArea);
        }


        this.btnGo.addEventListener("click", this.checkMatch.bind(this));
        //   this.selectComp.setContent("Mr. Morton walked down the street.");
    }

    public nextProblem() {
        this.problemIndex++;
        if (this.problemIndex < this.problems.length) {
            let problem: HProbVo = this.problems[this.problemIndex];
            this.selectComp.setProblem(problem);
            this.setInstructions(problem.instructions);
            this.reset();
        }
        else
        {
            this.setMessage("Out of problems");
            return;
        }
        this.clickLock=false;
    }
    private checkMatch() {
        if (this.clickLock === true) {
            return;
        }
        this.clickLock = true;

        let problem: HProbVo = this.problems[this.problemIndex];
        let checkString: string = this.selectComp.selectedString;
        console.log(checkString);
        console.log(problem.matchString);

        if (problem.matchString === checkString) {
            console.log("correct");
            this.setMessage("That's right!");
            setTimeout(() => {
                this.nextProblem();
            }, 1000);
        }
        else {
            console.log("wrong");
            this.setMessage("Sorry that is not correct, try again!");
            setTimeout(() => {
                this.reset();
            }, 1000);
        }

    }
    reset()
    {
        this.setMessage("Highlight the words and then press the go button");
        this.clickLock=false;
    }
    setInstructions(msg:string)
    {
        let msg2:string=this.converter.makeHtml(msg);
        this.instructionsArea.innerHTML=msg2;
    }
    setMessage(msg: string) {
        this.messageArea.innerText=msg;
    }
    public loadFile(path: string) {
        fetch(path)
            .then(response => response.json())
            .then(data => this.process(data));
    }
    private process(data: any) {
        //console.log(data);
        for (let i: number = 0; i < data.sents.length; i++) {
            let obj: any = data.sents[i];
            let hProv: HProbVo = new HProbVo(obj.instructions, obj.text);
            this.problems.push(hProv);
        }

        this.nextProblem();
    }
}