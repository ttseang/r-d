export class HWordVo
{
    public content:string;
    public length:number;
    public start:number=0;
    public end:number=0;
    public middle:number=0;

    constructor(content:string)
    {
        this.content=content;
        this.length=content.length;
    }
    setStart(start:number)
    {
        this.start=start;
        this.end=this.start+this.length;
        this.middle=this.start+Math.floor(this.length/2);
        if (this.length/2!==Math.floor(this.length/2))
        {
            this.middle++;
        }
    }
    check(gStart:number,gEnd:number)
    {
        if (gStart<=this.start && gEnd>=this.end)
        {
            //console.log(this.content+" accept 1");
            return true;
        }

        if (gStart<=this.middle && gStart<=this.end && gEnd>=this.end)
        {
            //console.log(this.content+" accept 2");
            return true;
        }
        if (gStart<=this.start && gEnd>=this.middle && gEnd<=this.end)
        {
            //console.log(this.content+" accept 3 "+this.middle);
            return true;
        }

        return false;
    }
}