export class HProbVo
{
    public instructions:string;
    public text:string;
    public sent:string;
    public matchString:string="";

    constructor(instructions:string,text:string)
    {
        this.instructions=instructions;
        this.text=text;

        this.sent=text;

        this.sent=this.sent.replace("<r>","");
        this.sent=this.sent.replace("</r>","");

        //console.log(this.sent);

        let matches:RegExpMatchArray=text.match(/<r>(.*?)<\/r>/g) || [];
       // //console.log(matches);

        if (matches.length>0)
        {
            this.matchString=matches[0];
            this.matchString=this.matchString.replace("<r>","");
            this.matchString=this.matchString.replace("</r>","");
        }
       // //console.log(this.matchString);
    }
}