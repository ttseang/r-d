import { HProbVo } from "./dataObjs/HProbVo";
import { HWordVo } from "./dataObjs/HWordVo";

export class SelectComp {
    private text1: HTMLTextAreaElement;
    private content: string = "";
    private wordObjs: HWordVo[] = [];
    public selectedWords: HWordVo[] = [];
    public selectedString:string="";
    private currentProblem:HProbVo=new HProbVo("","");

    constructor() {
        this.text1 = document.getElementById('text1') as HTMLTextAreaElement || document.createElement("textarea");
        if (this.text1.id !== "text1") {
            this.text1.id = "text1";
            document.body.append(this.text1);
        }
        (window as any).scomp = this;

        this.text1.addEventListener("pointerup", this.onPointerUp.bind(this))
    }
    setProblem(problem:HProbVo) {

        this.currentProblem=problem;

        if (this.text1) {
            this.text1.value = problem.sent;
            this.content = problem.sent;
            this.makeWords();

            this.text1.style.height=this.text1.scrollHeight.toString()+"px";
            //  //console.log(this.text1.scrollHeight);
        }
    }
    makeWords() {
        let words: string[] = this.content.split(" ");
        let start: number = 0;
        this.wordObjs=[];

        for (let i: number = 0; i < words.length; i++) {
            let wordVo: HWordVo = new HWordVo(words[i]);
            wordVo.setStart(start);
            start += wordVo.length + 1;
            this.wordObjs.push(wordVo);
        }

    }
    onPointerUp(e: PointerEvent) {
        //console.log(this.text1.selectionStart, this.text1.selectionEnd);


        let gStart: number = this.text1.selectionStart;
        let gEnd: number = this.text1.selectionEnd;
        this.getSelectedWords(gStart, gEnd);
    }
    getSelectedWords(gStart: number, gEnd: number) {

        const punct:string=".!?,";

        
        this.selectedWords = [];

        for (let i: number = 0; i < this.wordObjs.length; i++) {
            if (this.wordObjs[i].check(gStart, gEnd) === true) {
                this.selectedWords.push(this.wordObjs[i]);
            }
        }
        let sWords:string[]=[];
        for (let j: number = 0; j < this.selectedWords.length; j++) {
            //console.log(this.selectedWords[j].content);
            sWords.push(this.selectedWords[j].content);

        }

        this.selectedString=sWords.join(" ");
        console.log(this.selectedString);

        if (this.selectedWords.length > 0) {
            let nStart: number = this.selectedWords[0].start;
            let nEnd: number = this.selectedWords[this.selectedWords.length - 1].end;

            console.log(nStart,nEnd);
            if (punct.includes(this.content.charAt(nStart)) && !punct.includes(this.currentProblem.sent.charAt(0)))
            {
                //console.log("DOT At Start"); 
                nStart++;
                //nEnd--;
            }
            while(this.content.charAt(nStart)===" " && nStart<this.content.length)
            {
                nStart++;
            }

            if (punct.includes(this.content.charAt(nEnd-1)))
            {
                let lastMatchChar:string=this.currentProblem.matchString.charAt(this.currentProblem.matchString.length-1);
                //console.log(this.currentProblem.matchString);
                //console.log(lastMatchChar);
                //console.log("DOT At End");
                if (!punct.includes(lastMatchChar))
                {
                    console.log("no puct at end");
                    nEnd--;
                }
              
            }

            console.log(nStart,nEnd);

            this.text1.selectionStart = nStart;
            this.text1.selectionEnd = nEnd;
            if (gStart !== nStart && gEnd !== nEnd) {
                console.log("CHANGE");
                this.getSelectedWords(nStart,nEnd);
            }
            else
            {
                this.selectedString=this.content.substring(nStart,nEnd);
            }
        }
    }

}