export class SolutionDemo
{
    private btnPlay: HTMLButtonElement;
    private btnRewind: HTMLButtonElement;
    
    private player:any;
    private isPlaying:boolean = true;
    private lessonArray: string[] = [];
    private lessonNameArray: string[] = [];
    constructor()
    {
        console.log("SolutionDemo constructor");
        this.btnPlay = document.querySelector(".btnPlay") as HTMLButtonElement;
        this.btnRewind = document.querySelector(".btnRewind") as HTMLButtonElement;
       
        this.player = document.querySelector(".rufflePlayer");
        this.addListeners();
        (window as any).rufflePlayer = this.player;
        (window as any).demo=this;
        const lessonSelect = document.querySelector('.solutionSelect') as HTMLSelectElement;
        lessonSelect.addEventListener('change', this.onSelectionChange.bind(this));

        this.loadJson();
    }
    loadJson()
    {
        //load the json file
        fetch("./assets/lessons.json")
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any)
    {
        console.log(data);
      
        //fill the lessonArray
        for (let i = 0; i < data.lessons.length; i++)
        {
            this.lessonArray.push(data.lessons[i].file);
            this.lessonNameArray.push(data.lessons[i].name);
        }
        this.fillSelect();
    }
    fillSelect()
    {
        //get the select element with class name of bgselect
        const lessonSelect = document.querySelector('.solutionSelect') as HTMLSelectElement;
        //loop through the bgNames array
        for (let i = 0; i < this.lessonNameArray.length; i++)
        {
            //create an option element
            const option = document.createElement('option');
            //set the value of the option to the file name
            option.value = this.lessonArray[i];
            //set the text of the option to the bg name
            option.text = this.lessonNameArray[i];
            //add the option to the select element
            lessonSelect.add(option);
        }
    }
    onSelectionChange()
    {
        //get the select element with class name of bgselect
        const lessonSelect = document.querySelector('.solutionSelect') as HTMLSelectElement;
        //get the selected index
        const selectedIndex = lessonSelect.selectedIndex;
        //get the selected option
        const selectedOption = lessonSelect.options[selectedIndex];
        //get the value of the selected option
        const selectedValue = selectedOption.value;
        const path:string = "./assets/"+selectedValue+".swf";
        console.log(path);
        if (this.player)
        {
            this.btnPlay.classList.add("paused");
            this.player.load(path);
        }
    }
    addListeners()
    {
        console.log("addListeners");
        this.btnPlay.addEventListener("click", this.playSolution.bind(this));
        this.btnRewind.addEventListener("click", this.rewindSolution.bind(this));
       
        //log out listener
       // this.player.onplay=()=>{ console.log("onPlay"); }
      //  this.player.onFSCommand=(command:string)=>{ console.log(command); }
        
    }
    playSolution()
    {
        console.log("playSolution");
        if (this.player.isPlaying)
        {
           // this.isPlaying = false;
            this.btnPlay.classList.remove("paused");
            
            this.player.pause();
                
                const playButton:HTMLElement=this.player.playButton;
                playButton.style.display="none";
            
            return;
        }
        if (this.player)
        {
            this.player.play();
            this.btnPlay.classList.add("paused");
           // this.isPlaying = true;
        }
    }
    
    rewindSolution()
    {
        console.log("rewindSolution");
        if (this.player)
        {
            this.player.load("./assets/TT.M3.L001.swf");
        }
    }
    loadSolution()
    {
        console.log("loadSolution");
    }
}