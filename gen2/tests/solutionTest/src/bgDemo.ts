export class BgDemo {
    private fileNames: string[] = [];
    private bgNames: string[] = [];
    private player:any;
    constructor() {
        this.player = document.getElementsByClassName("rufflePlayer2")[0];
        
        this.loadJson();
        (window as any).rufflePlayer2 = this.player;
        (window as any).demo=this;
    }
    loadJson() {
        //load the json file
        fetch("./assets/backgroundList.json")
            .then(response => response.json())
            .then(data => this.process(data));
    }
    process(data: any) {
        console.log(data);
        data = data.backgrounds;
        //process the json file
        for (let i = 0; i < data.length; i++) {
            this.fileNames.push(data[i].file);
            this.bgNames.push(data[i].name);
        }
        this.fillSelect();
    }
    fillSelect() {
        //get the select element with class name of bgselect
        const bgselect = document.querySelector('.bgselect') as HTMLSelectElement;
        //loop through the bgNames array
        for (let i = 0; i < this.bgNames.length; i++) {
            //create an option element
            const option = document.createElement('option');
            //set the value of the option to the file name
            option.value = this.fileNames[i];
            //set the text of the option to the bg name
            option.text = this.bgNames[i];
            //add the option to the select element
            bgselect.add(option);
        }
        //add an event listener to the select element
        bgselect.addEventListener('change', this.onSelectionChange);
    }
    onSelectionChange() {
        this.player = document.getElementsByClassName("rufflePlayer2")[0];
        //get the select element with class name of bgselect
        const bgselect = document.querySelector('.bgselect') as HTMLSelectElement;
        //get the selected option
        const selectedOption = bgselect.options[bgselect.selectedIndex];

        //set the default background color and image of html
          const body2:HTMLElement | null=document.querySelector("body.solution");
          if (body2)
          {
            body2.style.backgroundImage = "url('https://ttv4.s3.amazonaws.com/wallpaper/DN.jpg')";
            body2.style.backgroundColor = "hwb(196 38% 10%)";
          }
        

        //get the value of the selected option
        const selectedValue = selectedOption.value;
        if (selectedValue == "none") {
            return;
        }
        //set the background of the player to the selected value
        //this.player.load()
        let path:string = "https://ttv4.s3.amazonaws.com/wallpaper/" + selectedValue+".swf";
        console.log(selectedValue);
        this.player.addEventListener('loadedmetadata', () => {
            console.info(this.player.metadata);
          })

        this.player.load(path).then(() => {
            console.info("Ruffle successfully loaded the file");
            //turn off background color and image
            if (body2)
            {
                body2.style.backgroundImage = "none";
                body2.style.backgroundColor = "transparent";
            }
           
        }).catch((e:any) => {
            console.error(`Ruffle failed to load the file: ${e}`);
        });

       
    }
}