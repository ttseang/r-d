import { BookIndexVo } from "./classes/dataObjs/BookIndexVo";
import { SubIndexVo } from "./classes/dataObjs/SubIndexVo";

export class JsonMaker {
    private words: string[] = [];
    private bookIndexVos: BookIndexVo[] = [];
    constructor() {
        this.loadWordFile();
    }
    loadWordFile() {
        fetch('./words.txt').then(response => response.text()).then(text => {
            this.words = text.split('\r').map(word => word.trim());
            console.log(this.words);
            this.makeJson();
        });
    }
    makeJson() {
        for (let i = 0; i < this.words.length; i++) {
            //if the word length is 3 or more
            if (this.words[i].length >= 3) {
                //make a random page number
                let pageNum = Math.floor(Math.random() * 1000);
                let bookIndexVo: BookIndexVo = new BookIndexVo(this.words[i], pageNum, []);

                //pick a random number of words
                let numWords = Math.floor(Math.random() * 10);
                for (let j = 0; j < numWords; j++) {
                    //pick a random word
                    let word = this.words[Math.floor(Math.random() * this.words.length)];
                    //if the word length is 3 or more
                    if (word.length >= 3) {
                        //make a subIndexVo
                        //make a random subpage number
                        let subPageNum = Math.floor(Math.random() * 1000);
                        let subIndexVo: SubIndexVo = new SubIndexVo(word, subPageNum);

                        //add the subIndexVo to the bookIndexVo
                        bookIndexVo.subIndexes.push(subIndexVo);


                    }
                }
                //add the bookIndexVo to the bookIndexVos
                this.bookIndexVos.push(bookIndexVo);
            }

        }

        const textArea = document.getElementById('log') as HTMLTextAreaElement;
        textArea.value = JSON.stringify(this.bookIndexVos);
    }

}