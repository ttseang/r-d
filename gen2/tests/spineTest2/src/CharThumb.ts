import { SpinePlayer } from "@esotericsoftware/spine-player";
import { SpineVo } from "./classes/dataObjs/SpineVo";

export class CharThumb
{
    public element:HTMLElement;
   
    private spineVo:SpineVo;

    constructor(index:number,spineVo:SpineVo)
    {
        this.spineVo = spineVo;
        this.element = document.createElement("figure");
        //let className:string = "char-thumb"+index.toString();
        this.element.classList.add("spineCard");
    }

    render()
    {
        this.element.innerHTML=`<figcaption>${this.spineVo.charName}</figcaption>`;
        
        const config: any = {
            width: 200,
            height: 200,
            jsonUrl: this.spineVo.jsonUrl,
            atlasUrl: this.spineVo.atlasUrl,
            loop: true,
            autoplay: true,
            scale: 1,
            showControls: false,
            animation:this.spineVo.animation,
            alpha: true, // Enable player translucency
            backgroundColor: "#00000000" // Background is fully transparent
        };
        const player:SpinePlayer = new SpinePlayer(this.element, config);
        
        
    }
}