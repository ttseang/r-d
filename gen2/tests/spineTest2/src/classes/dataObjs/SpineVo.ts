//json atlas animation
export class SpineVo
{
    public charName:string;
    public animation:string;
    public jsonUrl:string;
    public atlasUrl:string;
    constructor(charName:string,animation:string="",jsonUrl:string="",atlasUrl:string="")
    {
        this.charName = charName;
        this.animation = animation;
        this.jsonUrl = jsonUrl;
        this.atlasUrl = atlasUrl;
        if (jsonUrl == "")
        {
            this.jsonUrl = "./assets/" + charName + ".json";
        }
        if (atlasUrl == "")
        {
            this.atlasUrl = "./assets/" + charName + ".atlas";
        }
    }
}