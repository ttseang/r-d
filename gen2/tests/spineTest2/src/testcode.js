(() => {
  "use strict";
  class t {
    constructor() {
      (this.entries = {}), (this.size = 0);
    }
    add(t) {
      let e = this.entries[t];
      return (this.entries[t] = !0), !e && (this.size++, !0);
    }
    addAll(t) {
      let e = this.size;
      for (var s = 0, i = t.length; s < i; s++) this.add(t[s]);
      return e != this.size;
    }
    contains(t) {
      return this.entries[t];
    }
    clear() {
      (this.entries = {}), (this.size = 0);
    }
  }
  class e {
    constructor(t = 0, e = 0, s = 0, i = 0) {
      (this.r = t), (this.g = e), (this.b = s), (this.a = i);
    }
    set(t, e, s, i) {
      return (
        (this.r = t), (this.g = e), (this.b = s), (this.a = i), this.clamp()
      );
    }
    setFromColor(t) {
      return (
        (this.r = t.r), (this.g = t.g), (this.b = t.b), (this.a = t.a), this
      );
    }
    setFromString(t) {
      return (
        (t = "#" == t.charAt(0) ? t.substr(1) : t),
        (this.r = parseInt(t.substr(0, 2), 16) / 255),
        (this.g = parseInt(t.substr(2, 2), 16) / 255),
        (this.b = parseInt(t.substr(4, 2), 16) / 255),
        (this.a = 8 != t.length ? 1 : parseInt(t.substr(6, 2), 16) / 255),
        this
      );
    }
    add(t, e, s, i) {
      return (
        (this.r += t), (this.g += e), (this.b += s), (this.a += i), this.clamp()
      );
    }
    clamp() {
      return (
        this.r < 0 ? (this.r = 0) : this.r > 1 && (this.r = 1),
        this.g < 0 ? (this.g = 0) : this.g > 1 && (this.g = 1),
        this.b < 0 ? (this.b = 0) : this.b > 1 && (this.b = 1),
        this.a < 0 ? (this.a = 0) : this.a > 1 && (this.a = 1),
        this
      );
    }
    static rgba8888ToColor(t, e) {
      (t.r = ((4278190080 & e) >>> 24) / 255),
        (t.g = ((16711680 & e) >>> 16) / 255),
        (t.b = ((65280 & e) >>> 8) / 255),
        (t.a = (255 & e) / 255);
    }
    static rgb888ToColor(t, e) {
      (t.r = ((16711680 & e) >>> 16) / 255),
        (t.g = ((65280 & e) >>> 8) / 255),
        (t.b = (255 & e) / 255);
    }
    static fromString(t) {
      return new e().setFromString(t);
    }
  }
  (e.WHITE = new e(1, 1, 1, 1)),
    (e.RED = new e(1, 0, 0, 1)),
    (e.GREEN = new e(0, 1, 0, 1)),
    (e.BLUE = new e(0, 0, 1, 1)),
    (e.MAGENTA = new e(1, 0, 1, 1));
  class s {
    static clamp(t, e, s) {
      return t < e ? e : t > s ? s : t;
    }
    static cosDeg(t) {
      return Math.cos(t * s.degRad);
    }
    static sinDeg(t) {
      return Math.sin(t * s.degRad);
    }
    static signum(t) {
      return t > 0 ? 1 : t < 0 ? -1 : 0;
    }
    static toInt(t) {
      return t > 0 ? Math.floor(t) : Math.ceil(t);
    }
    static cbrt(t) {
      let e = Math.pow(Math.abs(t), 1 / 3);
      return t < 0 ? -e : e;
    }
    static randomTriangular(t, e) {
      return s.randomTriangularWith(t, e, 0.5 * (t + e));
    }
    static randomTriangularWith(t, e, s) {
      let i = Math.random(),
        r = e - t;
      return i <= (s - t) / r
        ? t + Math.sqrt(i * r * (s - t))
        : e - Math.sqrt((1 - i) * r * (e - s));
    }
    static isPowerOfTwo(t) {
      return t && 0 == (t & (t - 1));
    }
  }
  (s.PI = 3.1415927),
    (s.PI2 = 2 * s.PI),
    (s.radiansToDegrees = 180 / s.PI),
    (s.radDeg = s.radiansToDegrees),
    (s.degreesToRadians = s.PI / 180),
    (s.degRad = s.degreesToRadians);
  class i {
    static arrayCopy(t, e, s, i, r) {
      for (let n = e, a = i; n < e + r; n++, a++) s[a] = t[n];
    }
    static arrayFill(t, e, s, i) {
      for (let r = e; r < s; r++) t[r] = i;
    }
    static setArraySize(t, e, s = 0) {
      let i = t.length;
      if (i == e) return t;
      if (((t.length = e), i < e)) for (let r = i; r < e; r++) t[r] = s;
      return t;
    }
    static ensureArrayCapacity(t, e, s = 0) {
      return t.length >= e ? t : i.setArraySize(t, e, s);
    }
    static newArray(t, e) {
      let s = new Array(t);
      for (let i = 0; i < t; i++) s[i] = e;
      return s;
    }
    static newFloatArray(t) {
      if (i.SUPPORTS_TYPED_ARRAYS) return new Float32Array(t);
      {
        let e = new Array(t);
        for (let t = 0; t < e.length; t++) e[t] = 0;
        return e;
      }
    }
    static newShortArray(t) {
      if (i.SUPPORTS_TYPED_ARRAYS) return new Int16Array(t);
      {
        let e = new Array(t);
        for (let t = 0; t < e.length; t++) e[t] = 0;
        return e;
      }
    }
    static toFloatArray(t) {
      return i.SUPPORTS_TYPED_ARRAYS ? new Float32Array(t) : t;
    }
    static toSinglePrecision(t) {
      return i.SUPPORTS_TYPED_ARRAYS ? Math.fround(t) : t;
    }
    static webkit602BugfixHelper(t, e) {}
    static contains(t, e, s = !0) {
      for (var i = 0; i < t.length; i++) if (t[i] == e) return !0;
      return !1;
    }
    static enumValue(t, e) {
      return t[e[0].toUpperCase() + e.slice(1)];
    }
  }
  i.SUPPORTS_TYPED_ARRAYS = "undefined" != typeof Float32Array;
  class r {
    constructor(t) {
      (this.items = new Array()), (this.instantiator = t);
    }
    obtain() {
      return this.items.length > 0 ? this.items.pop() : this.instantiator();
    }
    free(t) {
      t.reset && t.reset(), this.items.push(t);
    }
    freeAll(t) {
      for (let e = 0; e < t.length; e++) this.free(t[e]);
    }
    clear() {
      this.items.length = 0;
    }
  }
  class n {
    constructor(t = 0, e = 0) {
      (this.x = t), (this.y = e);
    }
    set(t, e) {
      return (this.x = t), (this.y = e), this;
    }
    length() {
      let t = this.x,
        e = this.y;
      return Math.sqrt(t * t + e * e);
    }
    normalize() {
      let t = this.length();
      return 0 != t && ((this.x /= t), (this.y /= t)), this;
    }
  }
  class a {
    constructor() {
      (this.maxDelta = 0.064),
        (this.framesPerSecond = 0),
        (this.delta = 0),
        (this.totalTime = 0),
        (this.lastTime = Date.now() / 1e3),
        (this.frameCount = 0),
        (this.frameTime = 0);
    }
    update() {
      let t = Date.now() / 1e3;
      (this.delta = t - this.lastTime),
        (this.frameTime += this.delta),
        (this.totalTime += this.delta),
        this.delta > this.maxDelta && (this.delta = this.maxDelta),
        (this.lastTime = t),
        this.frameCount++,
        this.frameTime > 1 &&
          ((this.framesPerSecond = this.frameCount / this.frameTime),
          (this.frameTime = 0),
          (this.frameCount = 0));
    }
  }
  class h {
    constructor(t) {
      if (!t) throw new Error("name cannot be null.");
      this.name = t;
    }
  }
  class o extends h {
    constructor(t) {
      super(t),
        (this.id = o.nextID++),
        (this.bones = null),
        (this.vertices = []),
        (this.worldVerticesLength = 0),
        (this.timelineAttachment = this);
    }
    computeWorldVertices(t, e, s, i, r, n) {
      s = r + (s >> 1) * n;
      let a = t.bone.skeleton,
        h = t.deform,
        o = this.vertices,
        l = this.bones;
      if (!l) {
        h.length > 0 && (o = h);
        let a = t.bone,
          l = a.worldX,
          c = a.worldY,
          d = a.a,
          u = a.b,
          m = a.c,
          f = a.d;
        for (let t = e, a = r; a < s; t += 2, a += n) {
          let e = o[t],
            s = o[t + 1];
          (i[a] = e * d + s * u + l), (i[a + 1] = e * m + s * f + c);
        }
        return;
      }
      let c = 0,
        d = 0;
      for (let t = 0; t < e; t += 2) {
        let t = l[c];
        (c += t + 1), (d += t);
      }
      let u = a.bones;
      if (0 == h.length)
        for (let t = r, e = 3 * d; t < s; t += n) {
          let s = 0,
            r = 0,
            n = l[c++];
          for (n += c; c < n; c++, e += 3) {
            let t = u[l[c]],
              i = o[e],
              n = o[e + 1],
              a = o[e + 2];
            (s += (i * t.a + n * t.b + t.worldX) * a),
              (r += (i * t.c + n * t.d + t.worldY) * a);
          }
          (i[t] = s), (i[t + 1] = r);
        }
      else {
        let t = h;
        for (let e = r, a = 3 * d, h = d << 1; e < s; e += n) {
          let s = 0,
            r = 0,
            n = l[c++];
          for (n += c; c < n; c++, a += 3, h += 2) {
            let e = u[l[c]],
              i = o[a] + t[h],
              n = o[a + 1] + t[h + 1],
              d = o[a + 2];
            (s += (i * e.a + n * e.b + e.worldX) * d),
              (r += (i * e.c + n * e.d + e.worldY) * d);
          }
          (i[e] = s), (i[e + 1] = r);
        }
      }
    }
    copyTo(t) {
      this.bones
        ? ((t.bones = new Array(this.bones.length)),
          i.arrayCopy(this.bones, 0, t.bones, 0, this.bones.length))
        : (t.bones = null),
        this.vertices &&
          ((t.vertices = i.newFloatArray(this.vertices.length)),
          i.arrayCopy(this.vertices, 0, t.vertices, 0, this.vertices.length)),
        (t.worldVerticesLength = this.worldVerticesLength),
        (t.timelineAttachment = this.timelineAttachment);
    }
  }
  o.nextID = 0;
  class l {
    constructor(t) {
      (this.id = l.nextID()),
        (this.start = 0),
        (this.digits = 0),
        (this.setupIndex = 0),
        (this.regions = new Array(t));
    }
    copy() {
      let t = new l(this.regions.length);
      return (
        i.arrayCopy(this.regions, 0, t.regions, 0, this.regions.length),
        (t.start = this.start),
        (t.digits = this.digits),
        (t.setupIndex = this.setupIndex),
        t
      );
    }
    apply(t, e) {
      let s = t.sequenceIndex;
      -1 == s && (s = this.setupIndex),
        s >= this.regions.length && (s = this.regions.length - 1);
      let i = this.regions[s];
      e.region != i && ((e.region = i), e.updateRegion());
    }
    getPath(t, e) {
      let s = t,
        i = (this.start + e).toString();
      for (let t = this.digits - i.length; t > 0; t--) s += "0";
      return (s += i), s;
    }
    static nextID() {
      return l._nextID++;
    }
  }
  var c;
  (l._nextID = 0),
    (function (t) {
      (t[(t.hold = 0)] = "hold"),
        (t[(t.once = 1)] = "once"),
        (t[(t.loop = 2)] = "loop"),
        (t[(t.pingpong = 3)] = "pingpong"),
        (t[(t.onceReverse = 4)] = "onceReverse"),
        (t[(t.loopReverse = 5)] = "loopReverse"),
        (t[(t.pingpongReverse = 6)] = "pingpongReverse");
    })(c || (c = {}));
  const d = [
    c.hold,
    c.once,
    c.loop,
    c.pingpong,
    c.onceReverse,
    c.loopReverse,
    c.pingpongReverse,
  ];
  class u {
    constructor(e, s, i) {
      if (((this.timelines = []), (this.timelineIds = new t()), !e))
        throw new Error("name cannot be null.");
      (this.name = e), this.setTimelines(s), (this.duration = i);
    }
    setTimelines(t) {
      if (!t) throw new Error("timelines cannot be null.");
      (this.timelines = t), this.timelineIds.clear();
      for (var e = 0; e < t.length; e++)
        this.timelineIds.addAll(t[e].getPropertyIds());
    }
    hasTimeline(t) {
      for (let e = 0; e < t.length; e++)
        if (this.timelineIds.contains(t[e])) return !0;
      return !1;
    }
    apply(t, e, s, i, r, n, a, h) {
      if (!t) throw new Error("skeleton cannot be null.");
      i &&
        0 != this.duration &&
        ((s %= this.duration), e > 0 && (e %= this.duration));
      let o = this.timelines;
      for (let i = 0, l = o.length; i < l; i++) o[i].apply(t, e, s, r, n, a, h);
    }
  }
  var m, f;
  !(function (t) {
    (t[(t.setup = 0)] = "setup"),
      (t[(t.first = 1)] = "first"),
      (t[(t.replace = 2)] = "replace"),
      (t[(t.add = 3)] = "add");
  })(m || (m = {})),
    (function (t) {
      (t[(t.mixIn = 0)] = "mixIn"), (t[(t.mixOut = 1)] = "mixOut");
    })(f || (f = {}));
  class g {
    constructor(t, e) {
      (this.propertyIds = e),
        (this.frames = i.newFloatArray(t * this.getFrameEntries()));
    }
    getPropertyIds() {
      return this.propertyIds;
    }
    getFrameEntries() {
      return 1;
    }
    getFrameCount() {
      return this.frames.length / this.getFrameEntries();
    }
    getDuration() {
      return this.frames[this.frames.length - this.getFrameEntries()];
    }
    static search1(t, e) {
      let s = t.length;
      for (let i = 1; i < s; i++) if (t[i] > e) return i - 1;
      return s - 1;
    }
    static search(t, e, s) {
      let i = t.length;
      for (let r = s; r < i; r += s) if (t[r] > e) return r - s;
      return i - s;
    }
  }
  class p extends g {
    constructor(t, e, s) {
      super(t, s),
        (this.curves = i.newFloatArray(t + 18 * e)),
        (this.curves[t - 1] = 1);
    }
    setLinear(t) {
      this.curves[t] = 0;
    }
    setStepped(t) {
      this.curves[t] = 1;
    }
    shrink(t) {
      let e = this.getFrameCount() + 18 * t;
      if (this.curves.length > e) {
        let t = i.newFloatArray(e);
        i.arrayCopy(this.curves, 0, t, 0, e), (this.curves = t);
      }
    }
    setBezier(t, e, s, i, r, n, a, h, o, l, c) {
      let d = this.curves,
        u = this.getFrameCount() + 18 * t;
      0 == s && (d[e] = 2 + u);
      let m = 0.03 * (i - 2 * n + h),
        f = 0.03 * (r - 2 * a + o),
        g = 0.006 * (3 * (n - h) - i + l),
        p = 0.006 * (3 * (a - o) - r + c),
        x = 2 * m + g,
        w = 2 * f + p,
        b = 0.3 * (n - i) + m + 0.16666667 * g,
        v = 0.3 * (a - r) + f + 0.16666667 * p,
        y = i + b,
        A = r + v;
      for (let t = u + 18; u < t; u += 2)
        (d[u] = y),
          (d[u + 1] = A),
          (b += x),
          (v += w),
          (x += g),
          (w += p),
          (y += b),
          (A += v);
    }
    getBezierValue(t, e, s, i) {
      let r = this.curves;
      if (r[i] > t) {
        let n = this.frames[e],
          a = this.frames[e + s];
        return a + ((t - n) / (r[i] - n)) * (r[i + 1] - a);
      }
      let n = i + 18;
      for (i += 2; i < n; i += 2)
        if (r[i] >= t) {
          let e = r[i - 2],
            s = r[i - 1];
          return s + ((t - e) / (r[i] - e)) * (r[i + 1] - s);
        }
      e += this.getFrameEntries();
      let a = r[n - 2],
        h = r[n - 1];
      return h + ((t - a) / (this.frames[e] - a)) * (this.frames[e + s] - h);
    }
  }
  class x extends p {
    constructor(t, e, s) {
      super(t, e, [s]);
    }
    getFrameEntries() {
      return 2;
    }
    setFrame(t, e, s) {
      (t <<= 1), (this.frames[t] = e), (this.frames[t + 1] = s);
    }
    getCurveValue(t) {
      let e = this.frames,
        s = e.length - 2;
      for (let i = 2; i <= s; i += 2)
        if (e[i] > t) {
          s = i - 2;
          break;
        }
      let i = this.curves[s >> 1];
      switch (i) {
        case 0:
          let i = e[s],
            r = e[s + 1];
          return r + ((t - i) / (e[s + 2] - i)) * (e[s + 2 + 1] - r);
        case 1:
          return e[s + 1];
      }
      return this.getBezierValue(t, s, 1, i - 2);
    }
  }
  class w extends p {
    constructor(t, e, s, i) {
      super(t, e, [s, i]);
    }
    getFrameEntries() {
      return 3;
    }
    setFrame(t, e, s, i) {
      (t *= 3),
        (this.frames[t] = e),
        (this.frames[t + 1] = s),
        (this.frames[t + 2] = i);
    }
  }
  class b extends x {
    constructor(t, e, s) {
      super(t, e, "0|" + s), (this.boneIndex = 0), (this.boneIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.bones[this.boneIndex];
      if (!h.active) return;
      if (s < this.frames[0]) {
        switch (n) {
          case m.setup:
            return void (h.rotation = h.data.rotation);
          case m.first:
            h.rotation += (h.data.rotation - h.rotation) * r;
        }
        return;
      }
      let o = this.getCurveValue(s);
      switch (n) {
        case m.setup:
          h.rotation = h.data.rotation + o * r;
          break;
        case m.first:
        case m.replace:
          o += h.data.rotation - h.rotation;
        case m.add:
          h.rotation += o * r;
      }
    }
  }
  class v extends w {
    constructor(t, e, s) {
      super(t, e, "1|" + s, "2|" + s),
        (this.boneIndex = 0),
        (this.boneIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.bones[this.boneIndex];
      if (!h.active) return;
      let o = this.frames;
      if (s < o[0]) {
        switch (n) {
          case m.setup:
            return (h.x = h.data.x), void (h.y = h.data.y);
          case m.first:
            (h.x += (h.data.x - h.x) * r), (h.y += (h.data.y - h.y) * r);
        }
        return;
      }
      let l = 0,
        c = 0,
        d = g.search(o, s, 3),
        u = this.curves[d / 3];
      switch (u) {
        case 0:
          let t = o[d];
          (l = o[d + 1]), (c = o[d + 2]);
          let e = (s - t) / (o[d + 3] - t);
          (l += (o[d + 3 + 1] - l) * e), (c += (o[d + 3 + 2] - c) * e);
          break;
        case 1:
          (l = o[d + 1]), (c = o[d + 2]);
          break;
        default:
          (l = this.getBezierValue(s, d, 1, u - 2)),
            (c = this.getBezierValue(s, d, 2, u + 18 - 2));
      }
      switch (n) {
        case m.setup:
          (h.x = h.data.x + l * r), (h.y = h.data.y + c * r);
          break;
        case m.first:
        case m.replace:
          (h.x += (h.data.x + l - h.x) * r), (h.y += (h.data.y + c - h.y) * r);
          break;
        case m.add:
          (h.x += l * r), (h.y += c * r);
      }
    }
  }
  class y extends x {
    constructor(t, e, s) {
      super(t, e, "1|" + s), (this.boneIndex = 0), (this.boneIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.bones[this.boneIndex];
      if (!h.active) return;
      if (s < this.frames[0]) {
        switch (n) {
          case m.setup:
            return void (h.x = h.data.x);
          case m.first:
            h.x += (h.data.x - h.x) * r;
        }
        return;
      }
      let o = this.getCurveValue(s);
      switch (n) {
        case m.setup:
          h.x = h.data.x + o * r;
          break;
        case m.first:
        case m.replace:
          h.x += (h.data.x + o - h.x) * r;
          break;
        case m.add:
          h.x += o * r;
      }
    }
  }
  class A extends x {
    constructor(t, e, s) {
      super(t, e, "2|" + s), (this.boneIndex = 0), (this.boneIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.bones[this.boneIndex];
      if (!h.active) return;
      if (s < this.frames[0]) {
        switch (n) {
          case m.setup:
            return void (h.y = h.data.y);
          case m.first:
            h.y += (h.data.y - h.y) * r;
        }
        return;
      }
      let o = this.getCurveValue(s);
      switch (n) {
        case m.setup:
          h.y = h.data.y + o * r;
          break;
        case m.first:
        case m.replace:
          h.y += (h.data.y + o - h.y) * r;
          break;
        case m.add:
          h.y += o * r;
      }
    }
  }
  class C extends w {
    constructor(t, e, s) {
      super(t, e, "3|" + s, "4|" + s),
        (this.boneIndex = 0),
        (this.boneIndex = s);
    }
    apply(t, e, i, r, n, a, h) {
      let o = t.bones[this.boneIndex];
      if (!o.active) return;
      let l,
        c,
        d = this.frames;
      if (i < d[0]) {
        switch (a) {
          case m.setup:
            return (o.scaleX = o.data.scaleX), void (o.scaleY = o.data.scaleY);
          case m.first:
            (o.scaleX += (o.data.scaleX - o.scaleX) * n),
              (o.scaleY += (o.data.scaleY - o.scaleY) * n);
        }
        return;
      }
      let u = g.search(d, i, 3),
        p = this.curves[u / 3];
      switch (p) {
        case 0:
          let t = d[u];
          (l = d[u + 1]), (c = d[u + 2]);
          let e = (i - t) / (d[u + 3] - t);
          (l += (d[u + 3 + 1] - l) * e), (c += (d[u + 3 + 2] - c) * e);
          break;
        case 1:
          (l = d[u + 1]), (c = d[u + 2]);
          break;
        default:
          (l = this.getBezierValue(i, u, 1, p - 2)),
            (c = this.getBezierValue(i, u, 2, p + 18 - 2));
      }
      if (((l *= o.data.scaleX), (c *= o.data.scaleY), 1 == n))
        a == m.add
          ? ((o.scaleX += l - o.data.scaleX), (o.scaleY += c - o.data.scaleY))
          : ((o.scaleX = l), (o.scaleY = c));
      else {
        let t = 0,
          e = 0;
        if (h == f.mixOut)
          switch (a) {
            case m.setup:
              (t = o.data.scaleX),
                (e = o.data.scaleY),
                (o.scaleX = t + (Math.abs(l) * s.signum(t) - t) * n),
                (o.scaleY = e + (Math.abs(c) * s.signum(e) - e) * n);
              break;
            case m.first:
            case m.replace:
              (t = o.scaleX),
                (e = o.scaleY),
                (o.scaleX = t + (Math.abs(l) * s.signum(t) - t) * n),
                (o.scaleY = e + (Math.abs(c) * s.signum(e) - e) * n);
              break;
            case m.add:
              (o.scaleX += (l - o.data.scaleX) * n),
                (o.scaleY += (c - o.data.scaleY) * n);
          }
        else
          switch (a) {
            case m.setup:
              (t = Math.abs(o.data.scaleX) * s.signum(l)),
                (e = Math.abs(o.data.scaleY) * s.signum(c)),
                (o.scaleX = t + (l - t) * n),
                (o.scaleY = e + (c - e) * n);
              break;
            case m.first:
            case m.replace:
              (t = Math.abs(o.scaleX) * s.signum(l)),
                (e = Math.abs(o.scaleY) * s.signum(c)),
                (o.scaleX = t + (l - t) * n),
                (o.scaleY = e + (c - e) * n);
              break;
            case m.add:
              (o.scaleX += (l - o.data.scaleX) * n),
                (o.scaleY += (c - o.data.scaleY) * n);
          }
      }
    }
  }
  class k extends x {
    constructor(t, e, s) {
      super(t, e, "3|" + s), (this.boneIndex = 0), (this.boneIndex = s);
    }
    apply(t, e, i, r, n, a, h) {
      let o = t.bones[this.boneIndex];
      if (!o.active) return;
      if (i < this.frames[0]) {
        switch (a) {
          case m.setup:
            return void (o.scaleX = o.data.scaleX);
          case m.first:
            o.scaleX += (o.data.scaleX - o.scaleX) * n;
        }
        return;
      }
      let l = this.getCurveValue(i) * o.data.scaleX;
      if (1 == n) a == m.add ? (o.scaleX += l - o.data.scaleX) : (o.scaleX = l);
      else {
        let t = 0;
        if (h == f.mixOut)
          switch (a) {
            case m.setup:
              (t = o.data.scaleX),
                (o.scaleX = t + (Math.abs(l) * s.signum(t) - t) * n);
              break;
            case m.first:
            case m.replace:
              (t = o.scaleX),
                (o.scaleX = t + (Math.abs(l) * s.signum(t) - t) * n);
              break;
            case m.add:
              o.scaleX += (l - o.data.scaleX) * n;
          }
        else
          switch (a) {
            case m.setup:
              (t = Math.abs(o.data.scaleX) * s.signum(l)),
                (o.scaleX = t + (l - t) * n);
              break;
            case m.first:
            case m.replace:
              (t = Math.abs(o.scaleX) * s.signum(l)),
                (o.scaleX = t + (l - t) * n);
              break;
            case m.add:
              o.scaleX += (l - o.data.scaleX) * n;
          }
      }
    }
  }
  class S extends x {
    constructor(t, e, s) {
      super(t, e, "4|" + s), (this.boneIndex = 0), (this.boneIndex = s);
    }
    apply(t, e, i, r, n, a, h) {
      let o = t.bones[this.boneIndex];
      if (!o.active) return;
      if (i < this.frames[0]) {
        switch (a) {
          case m.setup:
            return void (o.scaleY = o.data.scaleY);
          case m.first:
            o.scaleY += (o.data.scaleY - o.scaleY) * n;
        }
        return;
      }
      let l = this.getCurveValue(i) * o.data.scaleY;
      if (1 == n) a == m.add ? (o.scaleY += l - o.data.scaleY) : (o.scaleY = l);
      else {
        let t = 0;
        if (h == f.mixOut)
          switch (a) {
            case m.setup:
              (t = o.data.scaleY),
                (o.scaleY = t + (Math.abs(l) * s.signum(t) - t) * n);
              break;
            case m.first:
            case m.replace:
              (t = o.scaleY),
                (o.scaleY = t + (Math.abs(l) * s.signum(t) - t) * n);
              break;
            case m.add:
              o.scaleY += (l - o.data.scaleY) * n;
          }
        else
          switch (a) {
            case m.setup:
              (t = Math.abs(o.data.scaleY) * s.signum(l)),
                (o.scaleY = t + (l - t) * n);
              break;
            case m.first:
            case m.replace:
              (t = Math.abs(o.scaleY) * s.signum(l)),
                (o.scaleY = t + (l - t) * n);
              break;
            case m.add:
              o.scaleY += (l - o.data.scaleY) * n;
          }
      }
    }
  }
  class E extends w {
    constructor(t, e, s) {
      super(t, e, "5|" + s, "6|" + s),
        (this.boneIndex = 0),
        (this.boneIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.bones[this.boneIndex];
      if (!h.active) return;
      let o = this.frames;
      if (s < o[0]) {
        switch (n) {
          case m.setup:
            return (h.shearX = h.data.shearX), void (h.shearY = h.data.shearY);
          case m.first:
            (h.shearX += (h.data.shearX - h.shearX) * r),
              (h.shearY += (h.data.shearY - h.shearY) * r);
        }
        return;
      }
      let l = 0,
        c = 0,
        d = g.search(o, s, 3),
        u = this.curves[d / 3];
      switch (u) {
        case 0:
          let t = o[d];
          (l = o[d + 1]), (c = o[d + 2]);
          let e = (s - t) / (o[d + 3] - t);
          (l += (o[d + 3 + 1] - l) * e), (c += (o[d + 3 + 2] - c) * e);
          break;
        case 1:
          (l = o[d + 1]), (c = o[d + 2]);
          break;
        default:
          (l = this.getBezierValue(s, d, 1, u - 2)),
            (c = this.getBezierValue(s, d, 2, u + 18 - 2));
      }
      switch (n) {
        case m.setup:
          (h.shearX = h.data.shearX + l * r),
            (h.shearY = h.data.shearY + c * r);
          break;
        case m.first:
        case m.replace:
          (h.shearX += (h.data.shearX + l - h.shearX) * r),
            (h.shearY += (h.data.shearY + c - h.shearY) * r);
          break;
        case m.add:
          (h.shearX += l * r), (h.shearY += c * r);
      }
    }
  }
  class M extends x {
    constructor(t, e, s) {
      super(t, e, "5|" + s), (this.boneIndex = 0), (this.boneIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.bones[this.boneIndex];
      if (!h.active) return;
      if (s < this.frames[0]) {
        switch (n) {
          case m.setup:
            return void (h.shearX = h.data.shearX);
          case m.first:
            h.shearX += (h.data.shearX - h.shearX) * r;
        }
        return;
      }
      let o = this.getCurveValue(s);
      switch (n) {
        case m.setup:
          h.shearX = h.data.shearX + o * r;
          break;
        case m.first:
        case m.replace:
          h.shearX += (h.data.shearX + o - h.shearX) * r;
          break;
        case m.add:
          h.shearX += o * r;
      }
    }
  }
  class T extends x {
    constructor(t, e, s) {
      super(t, e, "6|" + s), (this.boneIndex = 0), (this.boneIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.bones[this.boneIndex];
      if (!h.active) return;
      if (s < this.frames[0]) {
        switch (n) {
          case m.setup:
            return void (h.shearY = h.data.shearY);
          case m.first:
            h.shearY += (h.data.shearY - h.shearY) * r;
        }
        return;
      }
      let o = this.getCurveValue(s);
      switch (n) {
        case m.setup:
          h.shearY = h.data.shearY + o * r;
          break;
        case m.first:
        case m.replace:
          h.shearY += (h.data.shearY + o - h.shearY) * r;
          break;
        case m.add:
          h.shearY += o * r;
      }
    }
  }
  class R extends p {
    constructor(t, e, s) {
      super(t, e, ["7|" + s, "8|" + s]),
        (this.slotIndex = 0),
        (this.slotIndex = s);
    }
    getFrameEntries() {
      return 5;
    }
    setFrame(t, e, s, i, r, n) {
      (t *= 5),
        (this.frames[t] = e),
        (this.frames[t + 1] = s),
        (this.frames[t + 2] = i),
        (this.frames[t + 3] = r),
        (this.frames[t + 4] = n);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.slots[this.slotIndex];
      if (!h.bone.active) return;
      let o = this.frames,
        l = h.color;
      if (s < o[0]) {
        let t = h.data.color;
        switch (n) {
          case m.setup:
            return void l.setFromColor(t);
          case m.first:
            l.add(
              (t.r - l.r) * r,
              (t.g - l.g) * r,
              (t.b - l.b) * r,
              (t.a - l.a) * r
            );
        }
        return;
      }
      let c = 0,
        d = 0,
        u = 0,
        f = 0,
        p = g.search(o, s, 5),
        x = this.curves[p / 5];
      switch (x) {
        case 0:
          let t = o[p];
          (c = o[p + 1]), (d = o[p + 2]), (u = o[p + 3]), (f = o[p + 4]);
          let e = (s - t) / (o[p + 5] - t);
          (c += (o[p + 5 + 1] - c) * e),
            (d += (o[p + 5 + 2] - d) * e),
            (u += (o[p + 5 + 3] - u) * e),
            (f += (o[p + 5 + 4] - f) * e);
          break;
        case 1:
          (c = o[p + 1]), (d = o[p + 2]), (u = o[p + 3]), (f = o[p + 4]);
          break;
        default:
          (c = this.getBezierValue(s, p, 1, x - 2)),
            (d = this.getBezierValue(s, p, 2, x + 18 - 2)),
            (u = this.getBezierValue(s, p, 3, x + 36 - 2)),
            (f = this.getBezierValue(s, p, 4, x + 54 - 2));
      }
      1 == r
        ? l.set(c, d, u, f)
        : (n == m.setup && l.setFromColor(h.data.color),
          l.add((c - l.r) * r, (d - l.g) * r, (u - l.b) * r, (f - l.a) * r));
    }
  }
  class Y extends p {
    constructor(t, e, s) {
      super(t, e, ["7|" + s]), (this.slotIndex = 0), (this.slotIndex = s);
    }
    getFrameEntries() {
      return 4;
    }
    setFrame(t, e, s, i, r) {
      (t <<= 2),
        (this.frames[t] = e),
        (this.frames[t + 1] = s),
        (this.frames[t + 2] = i),
        (this.frames[t + 3] = r);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.slots[this.slotIndex];
      if (!h.bone.active) return;
      let o = this.frames,
        l = h.color;
      if (s < o[0]) {
        let t = h.data.color;
        switch (n) {
          case m.setup:
            return (l.r = t.r), (l.g = t.g), void (l.b = t.b);
          case m.first:
            (l.r += (t.r - l.r) * r),
              (l.g += (t.g - l.g) * r),
              (l.b += (t.b - l.b) * r);
        }
        return;
      }
      let c = 0,
        d = 0,
        u = 0,
        f = g.search(o, s, 4),
        p = this.curves[f >> 2];
      switch (p) {
        case 0:
          let t = o[f];
          (c = o[f + 1]), (d = o[f + 2]), (u = o[f + 3]);
          let e = (s - t) / (o[f + 4] - t);
          (c += (o[f + 4 + 1] - c) * e),
            (d += (o[f + 4 + 2] - d) * e),
            (u += (o[f + 4 + 3] - u) * e);
          break;
        case 1:
          (c = o[f + 1]), (d = o[f + 2]), (u = o[f + 3]);
          break;
        default:
          (c = this.getBezierValue(s, f, 1, p - 2)),
            (d = this.getBezierValue(s, f, 2, p + 18 - 2)),
            (u = this.getBezierValue(s, f, 3, p + 36 - 2));
      }
      if (1 == r) (l.r = c), (l.g = d), (l.b = u);
      else {
        if (n == m.setup) {
          let t = h.data.color;
          (l.r = t.r), (l.g = t.g), (l.b = t.b);
        }
        (l.r += (c - l.r) * r), (l.g += (d - l.g) * r), (l.b += (u - l.b) * r);
      }
    }
  }
  class I extends x {
    constructor(t, e, s) {
      super(t, e, "8|" + s), (this.slotIndex = 0), (this.slotIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.slots[this.slotIndex];
      if (!h.bone.active) return;
      let o = h.color;
      if (s < this.frames[0]) {
        let t = h.data.color;
        switch (n) {
          case m.setup:
            return void (o.a = t.a);
          case m.first:
            o.a += (t.a - o.a) * r;
        }
        return;
      }
      let l = this.getCurveValue(s);
      1 == r
        ? (o.a = l)
        : (n == m.setup && (o.a = h.data.color.a), (o.a += (l - o.a) * r));
    }
  }
  class X extends p {
    constructor(t, e, s) {
      super(t, e, ["7|" + s, "8|" + s, "9|" + s]),
        (this.slotIndex = 0),
        (this.slotIndex = s);
    }
    getFrameEntries() {
      return 8;
    }
    setFrame(t, e, s, i, r, n, a, h, o) {
      (t <<= 3),
        (this.frames[t] = e),
        (this.frames[t + 1] = s),
        (this.frames[t + 2] = i),
        (this.frames[t + 3] = r),
        (this.frames[t + 4] = n),
        (this.frames[t + 5] = a),
        (this.frames[t + 6] = h),
        (this.frames[t + 7] = o);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.slots[this.slotIndex];
      if (!h.bone.active) return;
      let o = this.frames,
        l = h.color,
        c = h.darkColor;
      if (s < o[0]) {
        let t = h.data.color,
          e = h.data.darkColor;
        switch (n) {
          case m.setup:
            return (
              l.setFromColor(t), (c.r = e.r), (c.g = e.g), void (c.b = e.b)
            );
          case m.first:
            l.add(
              (t.r - l.r) * r,
              (t.g - l.g) * r,
              (t.b - l.b) * r,
              (t.a - l.a) * r
            ),
              (c.r += (e.r - c.r) * r),
              (c.g += (e.g - c.g) * r),
              (c.b += (e.b - c.b) * r);
        }
        return;
      }
      let d = 0,
        u = 0,
        f = 0,
        p = 0,
        x = 0,
        w = 0,
        b = 0,
        v = g.search(o, s, 8),
        y = this.curves[v >> 3];
      switch (y) {
        case 0:
          let t = o[v];
          (d = o[v + 1]),
            (u = o[v + 2]),
            (f = o[v + 3]),
            (p = o[v + 4]),
            (x = o[v + 5]),
            (w = o[v + 6]),
            (b = o[v + 7]);
          let e = (s - t) / (o[v + 8] - t);
          (d += (o[v + 8 + 1] - d) * e),
            (u += (o[v + 8 + 2] - u) * e),
            (f += (o[v + 8 + 3] - f) * e),
            (p += (o[v + 8 + 4] - p) * e),
            (x += (o[v + 8 + 5] - x) * e),
            (w += (o[v + 8 + 6] - w) * e),
            (b += (o[v + 8 + 7] - b) * e);
          break;
        case 1:
          (d = o[v + 1]),
            (u = o[v + 2]),
            (f = o[v + 3]),
            (p = o[v + 4]),
            (x = o[v + 5]),
            (w = o[v + 6]),
            (b = o[v + 7]);
          break;
        default:
          (d = this.getBezierValue(s, v, 1, y - 2)),
            (u = this.getBezierValue(s, v, 2, y + 18 - 2)),
            (f = this.getBezierValue(s, v, 3, y + 36 - 2)),
            (p = this.getBezierValue(s, v, 4, y + 54 - 2)),
            (x = this.getBezierValue(s, v, 5, y + 72 - 2)),
            (w = this.getBezierValue(s, v, 6, y + 90 - 2)),
            (b = this.getBezierValue(s, v, 7, y + 108 - 2));
      }
      if (1 == r) l.set(d, u, f, p), (c.r = x), (c.g = w), (c.b = b);
      else {
        if (n == m.setup) {
          l.setFromColor(h.data.color);
          let t = h.data.darkColor;
          (c.r = t.r), (c.g = t.g), (c.b = t.b);
        }
        l.add((d - l.r) * r, (u - l.g) * r, (f - l.b) * r, (p - l.a) * r),
          (c.r += (x - c.r) * r),
          (c.g += (w - c.g) * r),
          (c.b += (b - c.b) * r);
      }
    }
  }
  class L extends p {
    constructor(t, e, s) {
      super(t, e, ["7|" + s, "9|" + s]),
        (this.slotIndex = 0),
        (this.slotIndex = s);
    }
    getFrameEntries() {
      return 7;
    }
    setFrame(t, e, s, i, r, n, a, h) {
      (t *= 7),
        (this.frames[t] = e),
        (this.frames[t + 1] = s),
        (this.frames[t + 2] = i),
        (this.frames[t + 3] = r),
        (this.frames[t + 4] = n),
        (this.frames[t + 5] = a),
        (this.frames[t + 6] = h);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.slots[this.slotIndex];
      if (!h.bone.active) return;
      let o = this.frames,
        l = h.color,
        c = h.darkColor;
      if (s < o[0]) {
        let t = h.data.color,
          e = h.data.darkColor;
        switch (n) {
          case m.setup:
            return (
              (l.r = t.r),
              (l.g = t.g),
              (l.b = t.b),
              (c.r = e.r),
              (c.g = e.g),
              void (c.b = e.b)
            );
          case m.first:
            (l.r += (t.r - l.r) * r),
              (l.g += (t.g - l.g) * r),
              (l.b += (t.b - l.b) * r),
              (c.r += (e.r - c.r) * r),
              (c.g += (e.g - c.g) * r),
              (c.b += (e.b - c.b) * r);
        }
        return;
      }
      let d = 0,
        u = 0,
        f = 0,
        p = 0,
        x = 0,
        w = 0,
        b = g.search(o, s, 7),
        v = this.curves[b / 7];
      switch (v) {
        case 0:
          let t = o[b];
          (d = o[b + 1]),
            (u = o[b + 2]),
            (f = o[b + 3]),
            (p = o[b + 4]),
            (x = o[b + 5]),
            (w = o[b + 6]);
          let e = (s - t) / (o[b + 7] - t);
          (d += (o[b + 7 + 1] - d) * e),
            (u += (o[b + 7 + 2] - u) * e),
            (f += (o[b + 7 + 3] - f) * e),
            (p += (o[b + 7 + 4] - p) * e),
            (x += (o[b + 7 + 5] - x) * e),
            (w += (o[b + 7 + 6] - w) * e);
          break;
        case 1:
          (d = o[b + 1]),
            (u = o[b + 2]),
            (f = o[b + 3]),
            (p = o[b + 4]),
            (x = o[b + 5]),
            (w = o[b + 6]);
          break;
        default:
          (d = this.getBezierValue(s, b, 1, v - 2)),
            (u = this.getBezierValue(s, b, 2, v + 18 - 2)),
            (f = this.getBezierValue(s, b, 3, v + 36 - 2)),
            (p = this.getBezierValue(s, b, 4, v + 54 - 2)),
            (x = this.getBezierValue(s, b, 5, v + 72 - 2)),
            (w = this.getBezierValue(s, b, 6, v + 90 - 2));
      }
      if (1 == r)
        (l.r = d), (l.g = u), (l.b = f), (c.r = p), (c.g = x), (c.b = w);
      else {
        if (n == m.setup) {
          let t = h.data.color,
            e = h.data.darkColor;
          (l.r = t.r),
            (l.g = t.g),
            (l.b = t.b),
            (c.r = e.r),
            (c.g = e.g),
            (c.b = e.b);
        }
        (l.r += (d - l.r) * r),
          (l.g += (u - l.g) * r),
          (l.b += (f - l.b) * r),
          (c.r += (p - c.r) * r),
          (c.g += (x - c.g) * r),
          (c.b += (w - c.b) * r);
      }
    }
  }
  class P extends g {
    constructor(t, e) {
      super(t, ["10|" + e]),
        (this.slotIndex = 0),
        (this.slotIndex = e),
        (this.attachmentNames = new Array(t));
    }
    getFrameCount() {
      return this.frames.length;
    }
    setFrame(t, e, s) {
      (this.frames[t] = e), (this.attachmentNames[t] = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.slots[this.slotIndex];
      h.bone.active &&
        (a != f.mixOut
          ? s < this.frames[0]
            ? (n != m.setup && n != m.first) ||
              this.setAttachment(t, h, h.data.attachmentName)
            : this.setAttachment(
                t,
                h,
                this.attachmentNames[g.search1(this.frames, s)]
              )
          : n == m.setup && this.setAttachment(t, h, h.data.attachmentName));
    }
    setAttachment(t, e, s) {
      e.setAttachment(s ? t.getAttachment(this.slotIndex, s) : null);
    }
  }
  class F extends p {
    constructor(t, e, s, i) {
      super(t, e, ["11|" + s + "|" + i.id]),
        (this.slotIndex = 0),
        (this.slotIndex = s),
        (this.attachment = i),
        (this.vertices = new Array(t));
    }
    getFrameCount() {
      return this.frames.length;
    }
    setFrame(t, e, s) {
      (this.frames[t] = e), (this.vertices[t] = s);
    }
    setBezier(t, e, s, i, r, n, a, h, o, l, c) {
      let d = this.curves,
        u = this.getFrameCount() + 18 * t;
      0 == s && (d[e] = 2 + u);
      let m = 0.03 * (i - 2 * n + h),
        f = 0.03 * o - 0.06 * a,
        g = 0.006 * (3 * (n - h) - i + l),
        p = 0.018 * (a - o + 0.33333333),
        x = 2 * m + g,
        w = 2 * f + p,
        b = 0.3 * (n - i) + m + 0.16666667 * g,
        v = 0.3 * a + f + 0.16666667 * p,
        y = i + b,
        A = v;
      for (let t = u + 18; u < t; u += 2)
        (d[u] = y),
          (d[u + 1] = A),
          (b += x),
          (v += w),
          (x += g),
          (w += p),
          (y += b),
          (A += v);
    }
    getCurvePercent(t, e) {
      let s = this.curves,
        i = s[e];
      switch (i) {
        case 0:
          let s = this.frames[e];
          return (t - s) / (this.frames[e + this.getFrameEntries()] - s);
        case 1:
          return 0;
      }
      if (((i -= 2), s[i] > t)) {
        let r = this.frames[e];
        return (s[i + 1] * (t - r)) / (s[i] - r);
      }
      let r = i + 18;
      for (i += 2; i < r; i += 2)
        if (s[i] >= t) {
          let e = s[i - 2],
            r = s[i - 1];
          return r + ((t - e) / (s[i] - e)) * (s[i + 1] - r);
        }
      let n = s[r - 2],
        a = s[r - 1];
      return (
        a + ((1 - a) * (t - n)) / (this.frames[e + this.getFrameEntries()] - n)
      );
    }
    apply(t, e, s, r, n, a, h) {
      let l = t.slots[this.slotIndex];
      if (!l.bone.active) return;
      let c = l.getAttachment();
      if (!c) return;
      if (!(c instanceof o) || c.timelineAttachment != this.attachment) return;
      let d = l.deform;
      0 == d.length && (a = m.setup);
      let u = this.vertices,
        f = u[0].length,
        p = this.frames;
      if (s < p[0]) {
        switch (a) {
          case m.setup:
            return void (d.length = 0);
          case m.first:
            if (1 == n) return void (d.length = 0);
            d.length = f;
            let t = c;
            if (t.bones) for (n = 1 - n, x = 0; x < f; x++) d[x] *= n;
            else {
              let e = t.vertices;
              for (var x = 0; x < f; x++) d[x] += (e[x] - d[x]) * n;
            }
        }
        return;
      }
      if (((d.length = f), s >= p[p.length - 1])) {
        let t = u[p.length - 1];
        if (1 == n)
          if (a == m.add) {
            let e = c;
            if (e.bones) for (let e = 0; e < f; e++) d[e] += t[e];
            else {
              let s = e.vertices;
              for (let e = 0; e < f; e++) d[e] += t[e] - s[e];
            }
          } else i.arrayCopy(t, 0, d, 0, f);
        else
          switch (a) {
            case m.setup: {
              let e = c;
              if (e.bones) for (let e = 0; e < f; e++) d[e] = t[e] * n;
              else {
                let s = e.vertices;
                for (let e = 0; e < f; e++) {
                  let i = s[e];
                  d[e] = i + (t[e] - i) * n;
                }
              }
              break;
            }
            case m.first:
            case m.replace:
              for (let e = 0; e < f; e++) d[e] += (t[e] - d[e]) * n;
              break;
            case m.add:
              let e = c;
              if (e.bones) for (let e = 0; e < f; e++) d[e] += t[e] * n;
              else {
                let s = e.vertices;
                for (let e = 0; e < f; e++) d[e] += (t[e] - s[e]) * n;
              }
          }
        return;
      }
      let w = g.search1(p, s),
        b = this.getCurvePercent(s, w),
        v = u[w],
        y = u[w + 1];
      if (1 == n)
        if (a == m.add) {
          let t = c;
          if (t.bones)
            for (let t = 0; t < f; t++) {
              let e = v[t];
              d[t] += e + (y[t] - e) * b;
            }
          else {
            let e = t.vertices;
            for (let t = 0; t < f; t++) {
              let s = v[t];
              d[t] += s + (y[t] - s) * b - e[t];
            }
          }
        } else
          for (let t = 0; t < f; t++) {
            let e = v[t];
            d[t] = e + (y[t] - e) * b;
          }
      else
        switch (a) {
          case m.setup: {
            let t = c;
            if (t.bones)
              for (let t = 0; t < f; t++) {
                let e = v[t];
                d[t] = (e + (y[t] - e) * b) * n;
              }
            else {
              let e = t.vertices;
              for (let t = 0; t < f; t++) {
                let s = v[t],
                  i = e[t];
                d[t] = i + (s + (y[t] - s) * b - i) * n;
              }
            }
            break;
          }
          case m.first:
          case m.replace:
            for (let t = 0; t < f; t++) {
              let e = v[t];
              d[t] += (e + (y[t] - e) * b - d[t]) * n;
            }
            break;
          case m.add:
            let t = c;
            if (t.bones)
              for (let t = 0; t < f; t++) {
                let e = v[t];
                d[t] += (e + (y[t] - e) * b) * n;
              }
            else {
              let e = t.vertices;
              for (let t = 0; t < f; t++) {
                let s = v[t];
                d[t] += (s + (y[t] - s) * b - e[t]) * n;
              }
            }
        }
    }
  }
  class D extends g {
    constructor(t) {
      super(t, D.propertyIds), (this.events = new Array(t));
    }
    getFrameCount() {
      return this.frames.length;
    }
    setFrame(t, e) {
      (this.frames[t] = e.time), (this.events[t] = e);
    }
    apply(t, e, s, i, r, n, a) {
      if (!i) return;
      let h = this.frames,
        o = this.frames.length;
      if (e > s) this.apply(t, e, Number.MAX_VALUE, i, r, n, a), (e = -1);
      else if (e >= h[o - 1]) return;
      if (s < h[0]) return;
      let l = 0;
      if (e < h[0]) l = 0;
      else {
        l = g.search1(h, e) + 1;
        let t = h[l];
        for (; l > 0 && h[l - 1] == t; ) l--;
      }
      for (; l < o && s >= h[l]; l++) i.push(this.events[l]);
    }
  }
  D.propertyIds = ["12"];
  class B extends g {
    constructor(t) {
      super(t, B.propertyIds), (this.drawOrders = new Array(t));
    }
    getFrameCount() {
      return this.frames.length;
    }
    setFrame(t, e, s) {
      (this.frames[t] = e), (this.drawOrders[t] = s);
    }
    apply(t, e, s, r, n, a, h) {
      if (h == f.mixOut)
        return void (
          a == m.setup &&
          i.arrayCopy(t.slots, 0, t.drawOrder, 0, t.slots.length)
        );
      if (s < this.frames[0])
        return void (
          (a != m.setup && a != m.first) ||
          i.arrayCopy(t.slots, 0, t.drawOrder, 0, t.slots.length)
        );
      let o = g.search1(this.frames, s),
        l = this.drawOrders[o];
      if (l) {
        let e = t.drawOrder,
          s = t.slots;
        for (let t = 0, i = l.length; t < i; t++) e[t] = s[l[t]];
      } else i.arrayCopy(t.slots, 0, t.drawOrder, 0, t.slots.length);
    }
  }
  B.propertyIds = ["13"];
  class V extends p {
    constructor(t, e, s) {
      super(t, e, ["14|" + s]),
        (this.ikConstraintIndex = 0),
        (this.ikConstraintIndex = s);
    }
    getFrameEntries() {
      return 6;
    }
    setFrame(t, e, s, i, r, n, a) {
      (t *= 6),
        (this.frames[t] = e),
        (this.frames[t + 1] = s),
        (this.frames[t + 2] = i),
        (this.frames[t + 3] = r),
        (this.frames[t + 4] = n ? 1 : 0),
        (this.frames[t + 5] = a ? 1 : 0);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.ikConstraints[this.ikConstraintIndex];
      if (!h.active) return;
      let o = this.frames;
      if (s < o[0]) {
        switch (n) {
          case m.setup:
            return (
              (h.mix = h.data.mix),
              (h.softness = h.data.softness),
              (h.bendDirection = h.data.bendDirection),
              (h.compress = h.data.compress),
              void (h.stretch = h.data.stretch)
            );
          case m.first:
            (h.mix += (h.data.mix - h.mix) * r),
              (h.softness += (h.data.softness - h.softness) * r),
              (h.bendDirection = h.data.bendDirection),
              (h.compress = h.data.compress),
              (h.stretch = h.data.stretch);
        }
        return;
      }
      let l = 0,
        c = 0,
        d = g.search(o, s, 6),
        u = this.curves[d / 6];
      switch (u) {
        case 0:
          let t = o[d];
          (l = o[d + 1]), (c = o[d + 2]);
          let e = (s - t) / (o[d + 6] - t);
          (l += (o[d + 6 + 1] - l) * e), (c += (o[d + 6 + 2] - c) * e);
          break;
        case 1:
          (l = o[d + 1]), (c = o[d + 2]);
          break;
        default:
          (l = this.getBezierValue(s, d, 1, u - 2)),
            (c = this.getBezierValue(s, d, 2, u + 18 - 2));
      }
      n == m.setup
        ? ((h.mix = h.data.mix + (l - h.data.mix) * r),
          (h.softness = h.data.softness + (c - h.data.softness) * r),
          a == f.mixOut
            ? ((h.bendDirection = h.data.bendDirection),
              (h.compress = h.data.compress),
              (h.stretch = h.data.stretch))
            : ((h.bendDirection = o[d + 3]),
              (h.compress = 0 != o[d + 4]),
              (h.stretch = 0 != o[d + 5])))
        : ((h.mix += (l - h.mix) * r),
          (h.softness += (c - h.softness) * r),
          a == f.mixIn &&
            ((h.bendDirection = o[d + 3]),
            (h.compress = 0 != o[d + 4]),
            (h.stretch = 0 != o[d + 5])));
    }
  }
  class N extends p {
    constructor(t, e, s) {
      super(t, e, ["15|" + s]),
        (this.transformConstraintIndex = 0),
        (this.transformConstraintIndex = s);
    }
    getFrameEntries() {
      return 7;
    }
    setFrame(t, e, s, i, r, n, a, h) {
      let o = this.frames;
      (o[(t *= 7)] = e),
        (o[t + 1] = s),
        (o[t + 2] = i),
        (o[t + 3] = r),
        (o[t + 4] = n),
        (o[t + 5] = a),
        (o[t + 6] = h);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.transformConstraints[this.transformConstraintIndex];
      if (!h.active) return;
      let o,
        l,
        c,
        d,
        u,
        f,
        p = this.frames;
      if (s < p[0]) {
        let t = h.data;
        switch (n) {
          case m.setup:
            return (
              (h.mixRotate = t.mixRotate),
              (h.mixX = t.mixX),
              (h.mixY = t.mixY),
              (h.mixScaleX = t.mixScaleX),
              (h.mixScaleY = t.mixScaleY),
              void (h.mixShearY = t.mixShearY)
            );
          case m.first:
            (h.mixRotate += (t.mixRotate - h.mixRotate) * r),
              (h.mixX += (t.mixX - h.mixX) * r),
              (h.mixY += (t.mixY - h.mixY) * r),
              (h.mixScaleX += (t.mixScaleX - h.mixScaleX) * r),
              (h.mixScaleY += (t.mixScaleY - h.mixScaleY) * r),
              (h.mixShearY += (t.mixShearY - h.mixShearY) * r);
        }
        return;
      }
      let x = g.search(p, s, 7),
        w = this.curves[x / 7];
      switch (w) {
        case 0:
          let t = p[x];
          (o = p[x + 1]),
            (l = p[x + 2]),
            (c = p[x + 3]),
            (d = p[x + 4]),
            (u = p[x + 5]),
            (f = p[x + 6]);
          let e = (s - t) / (p[x + 7] - t);
          (o += (p[x + 7 + 1] - o) * e),
            (l += (p[x + 7 + 2] - l) * e),
            (c += (p[x + 7 + 3] - c) * e),
            (d += (p[x + 7 + 4] - d) * e),
            (u += (p[x + 7 + 5] - u) * e),
            (f += (p[x + 7 + 6] - f) * e);
          break;
        case 1:
          (o = p[x + 1]),
            (l = p[x + 2]),
            (c = p[x + 3]),
            (d = p[x + 4]),
            (u = p[x + 5]),
            (f = p[x + 6]);
          break;
        default:
          (o = this.getBezierValue(s, x, 1, w - 2)),
            (l = this.getBezierValue(s, x, 2, w + 18 - 2)),
            (c = this.getBezierValue(s, x, 3, w + 36 - 2)),
            (d = this.getBezierValue(s, x, 4, w + 54 - 2)),
            (u = this.getBezierValue(s, x, 5, w + 72 - 2)),
            (f = this.getBezierValue(s, x, 6, w + 90 - 2));
      }
      if (n == m.setup) {
        let t = h.data;
        (h.mixRotate = t.mixRotate + (o - t.mixRotate) * r),
          (h.mixX = t.mixX + (l - t.mixX) * r),
          (h.mixY = t.mixY + (c - t.mixY) * r),
          (h.mixScaleX = t.mixScaleX + (d - t.mixScaleX) * r),
          (h.mixScaleY = t.mixScaleY + (u - t.mixScaleY) * r),
          (h.mixShearY = t.mixShearY + (f - t.mixShearY) * r);
      } else
        (h.mixRotate += (o - h.mixRotate) * r),
          (h.mixX += (l - h.mixX) * r),
          (h.mixY += (c - h.mixY) * r),
          (h.mixScaleX += (d - h.mixScaleX) * r),
          (h.mixScaleY += (u - h.mixScaleY) * r),
          (h.mixShearY += (f - h.mixShearY) * r);
    }
  }
  class _ extends x {
    constructor(t, e, s) {
      super(t, e, "16|" + s),
        (this.pathConstraintIndex = 0),
        (this.pathConstraintIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.pathConstraints[this.pathConstraintIndex];
      if (!h.active) return;
      if (s < this.frames[0]) {
        switch (n) {
          case m.setup:
            return void (h.position = h.data.position);
          case m.first:
            h.position += (h.data.position - h.position) * r;
        }
        return;
      }
      let o = this.getCurveValue(s);
      n == m.setup
        ? (h.position = h.data.position + (o - h.data.position) * r)
        : (h.position += (o - h.position) * r);
    }
  }
  class O extends x {
    constructor(t, e, s) {
      super(t, e, "17|" + s),
        (this.pathConstraintIndex = 0),
        (this.pathConstraintIndex = s);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.pathConstraints[this.pathConstraintIndex];
      if (!h.active) return;
      if (s < this.frames[0]) {
        switch (n) {
          case m.setup:
            return void (h.spacing = h.data.spacing);
          case m.first:
            h.spacing += (h.data.spacing - h.spacing) * r;
        }
        return;
      }
      let o = this.getCurveValue(s);
      n == m.setup
        ? (h.spacing = h.data.spacing + (o - h.data.spacing) * r)
        : (h.spacing += (o - h.spacing) * r);
    }
  }
  class W extends p {
    constructor(t, e, s) {
      super(t, e, ["18|" + s]),
        (this.pathConstraintIndex = 0),
        (this.pathConstraintIndex = s);
    }
    getFrameEntries() {
      return 4;
    }
    setFrame(t, e, s, i, r) {
      let n = this.frames;
      (n[(t <<= 2)] = e), (n[t + 1] = s), (n[t + 2] = i), (n[t + 3] = r);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.pathConstraints[this.pathConstraintIndex];
      if (!h.active) return;
      let o,
        l,
        c,
        d = this.frames;
      if (s < d[0]) {
        switch (n) {
          case m.setup:
            return (
              (h.mixRotate = h.data.mixRotate),
              (h.mixX = h.data.mixX),
              void (h.mixY = h.data.mixY)
            );
          case m.first:
            (h.mixRotate += (h.data.mixRotate - h.mixRotate) * r),
              (h.mixX += (h.data.mixX - h.mixX) * r),
              (h.mixY += (h.data.mixY - h.mixY) * r);
        }
        return;
      }
      let u = g.search(d, s, 4),
        f = this.curves[u >> 2];
      switch (f) {
        case 0:
          let t = d[u];
          (o = d[u + 1]), (l = d[u + 2]), (c = d[u + 3]);
          let e = (s - t) / (d[u + 4] - t);
          (o += (d[u + 4 + 1] - o) * e),
            (l += (d[u + 4 + 2] - l) * e),
            (c += (d[u + 4 + 3] - c) * e);
          break;
        case 1:
          (o = d[u + 1]), (l = d[u + 2]), (c = d[u + 3]);
          break;
        default:
          (o = this.getBezierValue(s, u, 1, f - 2)),
            (l = this.getBezierValue(s, u, 2, f + 18 - 2)),
            (c = this.getBezierValue(s, u, 3, f + 36 - 2));
      }
      if (n == m.setup) {
        let t = h.data;
        (h.mixRotate = t.mixRotate + (o - t.mixRotate) * r),
          (h.mixX = t.mixX + (l - t.mixX) * r),
          (h.mixY = t.mixY + (c - t.mixY) * r);
      } else
        (h.mixRotate += (o - h.mixRotate) * r),
          (h.mixX += (l - h.mixX) * r),
          (h.mixY += (c - h.mixY) * r);
    }
  }
  class q extends g {
    constructor(t, e, s) {
      super(t, ["19|" + e + "|" + s.sequence.id]),
        (this.slotIndex = e),
        (this.attachment = s);
    }
    getFrameEntries() {
      return q.ENTRIES;
    }
    getSlotIndex() {
      return this.slotIndex;
    }
    getAttachment() {
      return this.attachment;
    }
    setFrame(t, e, s, i, r) {
      let n = this.frames;
      (n[(t *= q.ENTRIES)] = e),
        (n[t + q.MODE] = s | (i << 4)),
        (n[t + q.DELAY] = r);
    }
    apply(t, e, s, i, r, n, a) {
      let h = t.slots[this.slotIndex];
      if (!h.bone.active) return;
      let l = h.attachment,
        u = this.attachment;
      if (!(l == u || (l instanceof o && l.timelineAttachment == u))) return;
      let f = this.frames;
      if (s < f[0])
        return void ((n != m.setup && n != m.first) || (h.sequenceIndex = -1));
      let p = g.search(f, s, q.ENTRIES),
        x = f[p],
        w = f[p + q.MODE],
        b = f[p + q.DELAY];
      if (!this.attachment.sequence) return;
      let v = w >> 4,
        y = this.attachment.sequence.regions.length,
        A = d[15 & w];
      if (A != c.hold)
        switch (((v += ((s - x) / b + 1e-5) | 0), A)) {
          case c.once:
            v = Math.min(y - 1, v);
            break;
          case c.loop:
            v %= y;
            break;
          case c.pingpong: {
            let t = (y << 1) - 2;
            (v = 0 == t ? 0 : v % t), v >= y && (v = t - v);
            break;
          }
          case c.onceReverse:
            v = Math.max(y - 1 - v, 0);
            break;
          case c.loopReverse:
            v = y - 1 - (v % y);
            break;
          case c.pingpongReverse: {
            let t = (y << 1) - 2;
            (v = 0 == t ? 0 : (v + y - 1) % t), v >= y && (v = t - v);
          }
        }
      h.sequenceIndex = v;
    }
  }
  (q.ENTRIES = 3), (q.MODE = 1), (q.DELAY = 2);
  class z {
    constructor(e) {
      (this.tracks = new Array()),
        (this.timeScale = 1),
        (this.unkeyedState = 0),
        (this.events = new Array()),
        (this.listeners = new Array()),
        (this.queue = new $(this)),
        (this.propertyIDs = new t()),
        (this.animationsChanged = !1),
        (this.trackEntryPool = new r(() => new U())),
        (this.data = e);
    }
    static emptyAnimation() {
      return z._emptyAnimation;
    }
    update(t) {
      t *= this.timeScale;
      let e = this.tracks;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (!i) continue;
        (i.animationLast = i.nextAnimationLast),
          (i.trackLast = i.nextTrackLast);
        let r = t * i.timeScale;
        if (i.delay > 0) {
          if (((i.delay -= r), i.delay > 0)) continue;
          (r = -i.delay), (i.delay = 0);
        }
        let n = i.next;
        if (n) {
          let e = i.trackLast - n.delay;
          if (e >= 0) {
            for (
              n.delay = 0,
                n.trackTime +=
                  0 == i.timeScale ? 0 : (e / i.timeScale + t) * n.timeScale,
                i.trackTime += r,
                this.setCurrent(s, n, !0);
              n.mixingFrom;

            )
              (n.mixTime += t), (n = n.mixingFrom);
            continue;
          }
        } else if (i.trackLast >= i.trackEnd && !i.mixingFrom) {
          (e[s] = null), this.queue.end(i), this.clearNext(i);
          continue;
        }
        if (i.mixingFrom && this.updateMixingFrom(i, t)) {
          let t = i.mixingFrom;
          for (i.mixingFrom = null, t && (t.mixingTo = null); t; )
            this.queue.end(t), (t = t.mixingFrom);
        }
        i.trackTime += r;
      }
      this.queue.drain();
    }
    updateMixingFrom(t, e) {
      let s = t.mixingFrom;
      if (!s) return !0;
      let i = this.updateMixingFrom(s, e);
      return (
        (s.animationLast = s.nextAnimationLast),
        (s.trackLast = s.nextTrackLast),
        t.mixTime > 0 && t.mixTime >= t.mixDuration
          ? ((0 != s.totalAlpha && 0 != t.mixDuration) ||
              ((t.mixingFrom = s.mixingFrom),
              s.mixingFrom && (s.mixingFrom.mixingTo = t),
              (t.interruptAlpha = s.interruptAlpha),
              this.queue.end(s)),
            i)
          : ((s.trackTime += e * s.timeScale), (t.mixTime += e), !1)
      );
    }
    apply(t) {
      if (!t) throw new Error("skeleton cannot be null.");
      this.animationsChanged && this._animationsChanged();
      let e = this.events,
        s = this.tracks,
        r = !1;
      for (let a = 0, h = s.length; a < h; a++) {
        let h = s[a];
        if (!h || h.delay > 0) continue;
        r = !0;
        let o = 0 == a ? m.first : h.mixBlend,
          l = h.alpha;
        h.mixingFrom
          ? (l *= this.applyMixingFrom(h, t, o))
          : h.trackTime >= h.trackEnd && !h.next && (l = 0);
        let c = h.animationLast,
          d = h.getAnimationTime(),
          u = d,
          g = e;
        h.reverse && ((u = h.animation.duration - u), (g = null));
        let p = h.animation.timelines,
          x = p.length;
        if ((0 == a && 1 == l) || o == m.add)
          for (let e = 0; e < x; e++) {
            i.webkit602BugfixHelper(l, o);
            var n = p[e];
            n instanceof P
              ? this.applyAttachmentTimeline(n, t, u, o, !0)
              : n.apply(t, c, u, g, l, o, f.mixIn);
          }
        else {
          let e = h.timelineMode,
            s = h.shortestRotation,
            r = !s && h.timelinesRotation.length != x << 1;
          r && (h.timelinesRotation.length = x << 1);
          for (let n = 0; n < x; n++) {
            let a = p[n],
              d = e[n] == G ? o : m.setup;
            !s && a instanceof b
              ? this.applyRotateTimeline(
                  a,
                  t,
                  u,
                  l,
                  d,
                  h.timelinesRotation,
                  n << 1,
                  r
                )
              : a instanceof P
              ? this.applyAttachmentTimeline(a, t, u, o, !0)
              : (i.webkit602BugfixHelper(l, o),
                a.apply(t, c, u, g, l, d, f.mixIn));
          }
        }
        this.queueEvents(h, d),
          (e.length = 0),
          (h.nextAnimationLast = d),
          (h.nextTrackLast = h.trackTime);
      }
      for (
        var a = this.unkeyedState + Z, h = t.slots, o = 0, l = t.slots.length;
        o < l;
        o++
      ) {
        var c = h[o];
        if (c.attachmentState == a) {
          var d = c.data.attachmentName;
          c.setAttachment(d ? t.getAttachment(c.data.index, d) : null);
        }
      }
      return (this.unkeyedState += 2), this.queue.drain(), r;
    }
    applyMixingFrom(t, e, s) {
      let r = t.mixingFrom;
      r.mixingFrom && this.applyMixingFrom(r, e, s);
      let n = 0;
      0 == t.mixDuration
        ? ((n = 1), s == m.first && (s = m.setup))
        : ((n = t.mixTime / t.mixDuration),
          n > 1 && (n = 1),
          s != m.first && (s = r.mixBlend));
      let a = n < r.attachmentThreshold,
        h = n < r.drawOrderThreshold,
        o = r.animation.timelines,
        l = o.length,
        c = r.alpha * t.interruptAlpha,
        d = c * (1 - n),
        u = r.animationLast,
        g = r.getAnimationTime(),
        p = g,
        x = null;
      if (
        (r.reverse
          ? (p = r.animation.duration - p)
          : n < r.eventThreshold && (x = this.events),
        s == m.add)
      )
        for (let t = 0; t < l; t++) o[t].apply(e, u, p, x, d, s, f.mixOut);
      else {
        let t = r.timelineMode,
          n = r.timelineHoldMix,
          g = r.shortestRotation,
          w = !g && r.timelinesRotation.length != l << 1;
        w && (r.timelinesRotation.length = l << 1), (r.totalAlpha = 0);
        for (let v = 0; v < l; v++) {
          let l,
            y = o[v],
            A = f.mixOut,
            C = 0;
          switch (t[v]) {
            case G:
              if (!h && y instanceof B) continue;
              (l = s), (C = d);
              break;
            case j:
              (l = m.setup), (C = d);
              break;
            case K:
              (l = s), (C = c);
              break;
            case J:
              (l = m.setup), (C = c);
              break;
            default:
              l = m.setup;
              let t = n[v];
              C = c * Math.max(0, 1 - t.mixTime / t.mixDuration);
          }
          (r.totalAlpha += C),
            !g && y instanceof b
              ? this.applyRotateTimeline(
                  y,
                  e,
                  p,
                  C,
                  l,
                  r.timelinesRotation,
                  v << 1,
                  w
                )
              : y instanceof P
              ? this.applyAttachmentTimeline(y, e, p, l, a)
              : (i.webkit602BugfixHelper(C, s),
                h && y instanceof B && l == m.setup && (A = f.mixIn),
                y.apply(e, u, p, x, C, l, A));
        }
      }
      return (
        t.mixDuration > 0 && this.queueEvents(r, g),
        (this.events.length = 0),
        (r.nextAnimationLast = g),
        (r.nextTrackLast = r.trackTime),
        n
      );
    }
    applyAttachmentTimeline(t, e, s, i, r) {
      var n = e.slots[t.slotIndex];
      n.bone.active &&
        (s < t.frames[0]
          ? (i != m.setup && i != m.first) ||
            this.setAttachment(e, n, n.data.attachmentName, r)
          : this.setAttachment(
              e,
              n,
              t.attachmentNames[g.search1(t.frames, s)],
              r
            ),
        n.attachmentState <= this.unkeyedState &&
          (n.attachmentState = this.unkeyedState + Z));
    }
    setAttachment(t, e, s, i) {
      e.setAttachment(s ? t.getAttachment(e.data.index, s) : null),
        i && (e.attachmentState = this.unkeyedState + tt);
    }
    applyRotateTimeline(t, e, i, r, n, a, h, o) {
      if ((o && (a[h] = 0), 1 == r))
        return void t.apply(e, 0, i, null, 1, n, f.mixIn);
      let l = e.bones[t.boneIndex];
      if (!l.active) return;
      let c = 0,
        d = 0;
      if (i < t.frames[0])
        switch (n) {
          case m.setup:
            l.rotation = l.data.rotation;
          default:
            return;
          case m.first:
            (c = l.rotation), (d = l.data.rotation);
        }
      else
        (c = n == m.setup ? l.data.rotation : l.rotation),
          (d = l.data.rotation + t.getCurveValue(i));
      let u = 0,
        g = d - c;
      if (((g -= 360 * (16384 - ((16384.499999999996 - g / 360) | 0))), 0 == g))
        u = a[h];
      else {
        let t = 0,
          e = 0;
        o ? ((t = 0), (e = g)) : ((t = a[h]), (e = a[h + 1]));
        let i = t - (t % 360);
        u = g + i;
        let r = g >= 0,
          n = t >= 0;
        Math.abs(e) <= 90 &&
          s.signum(e) != s.signum(g) &&
          (Math.abs(t - i) > 180
            ? ((u += 360 * s.signum(t)), (n = r))
            : 0 != i
            ? (u -= 360 * s.signum(t))
            : (n = r)),
          n != r && (u += 360 * s.signum(t)),
          (a[h] = u);
      }
      (a[h + 1] = g), (l.rotation = c + u * r);
    }
    queueEvents(t, e) {
      let s = t.animationStart,
        i = t.animationEnd,
        r = i - s,
        n = t.trackLast % r,
        a = this.events,
        h = 0,
        o = a.length;
      for (; h < o; h++) {
        let e = a[h];
        if (e.time < n) break;
        e.time > i || this.queue.event(t, e);
      }
      let l = !1;
      for (
        l = t.loop
          ? 0 == r || n > t.trackTime % r
          : e >= i && t.animationLast < i,
          l && this.queue.complete(t);
        h < o;
        h++
      ) {
        let e = a[h];
        e.time < s || this.queue.event(t, e);
      }
    }
    clearTracks() {
      let t = this.queue.drainDisabled;
      this.queue.drainDisabled = !0;
      for (let t = 0, e = this.tracks.length; t < e; t++) this.clearTrack(t);
      (this.tracks.length = 0),
        (this.queue.drainDisabled = t),
        this.queue.drain();
    }
    clearTrack(t) {
      if (t >= this.tracks.length) return;
      let e = this.tracks[t];
      if (!e) return;
      this.queue.end(e), this.clearNext(e);
      let s = e;
      for (;;) {
        let t = s.mixingFrom;
        if (!t) break;
        this.queue.end(t), (s.mixingFrom = null), (s.mixingTo = null), (s = t);
      }
      (this.tracks[e.trackIndex] = null), this.queue.drain();
    }
    setCurrent(t, e, s) {
      let i = this.expandToIndex(t);
      (this.tracks[t] = e),
        (e.previous = null),
        i &&
          (s && this.queue.interrupt(i),
          (e.mixingFrom = i),
          (i.mixingTo = e),
          (e.mixTime = 0),
          i.mixingFrom &&
            i.mixDuration > 0 &&
            (e.interruptAlpha *= Math.min(1, i.mixTime / i.mixDuration)),
          (i.timelinesRotation.length = 0)),
        this.queue.start(e);
    }
    setAnimation(t, e, s = !1) {
      let i = this.data.skeletonData.findAnimation(e);
      if (!i) throw new Error("Animation not found: " + e);
      return this.setAnimationWith(t, i, s);
    }
    setAnimationWith(t, e, s = !1) {
      if (!e) throw new Error("animation cannot be null.");
      let i = !0,
        r = this.expandToIndex(t);
      r &&
        (-1 == r.nextTrackLast
          ? ((this.tracks[t] = r.mixingFrom),
            this.queue.interrupt(r),
            this.queue.end(r),
            this.clearNext(r),
            (r = r.mixingFrom),
            (i = !1))
          : this.clearNext(r));
      let n = this.trackEntry(t, e, s, r);
      return this.setCurrent(t, n, i), this.queue.drain(), n;
    }
    addAnimation(t, e, s = !1, i = 0) {
      let r = this.data.skeletonData.findAnimation(e);
      if (!r) throw new Error("Animation not found: " + e);
      return this.addAnimationWith(t, r, s, i);
    }
    addAnimationWith(t, e, s = !1, i = 0) {
      if (!e) throw new Error("animation cannot be null.");
      let r = this.expandToIndex(t);
      if (r) for (; r.next; ) r = r.next;
      let n = this.trackEntry(t, e, s, r);
      return (
        r
          ? ((r.next = n),
            (n.previous = r),
            i <= 0 && (i += r.getTrackComplete() - n.mixDuration))
          : (this.setCurrent(t, n, !0), this.queue.drain()),
        (n.delay = i),
        n
      );
    }
    setEmptyAnimation(t, e = 0) {
      let s = this.setAnimationWith(t, z.emptyAnimation(), !1);
      return (s.mixDuration = e), (s.trackEnd = e), s;
    }
    addEmptyAnimation(t, e = 0, s = 0) {
      let i = this.addAnimationWith(t, z.emptyAnimation(), !1, s);
      return (
        s <= 0 && (i.delay += i.mixDuration - e),
        (i.mixDuration = e),
        (i.trackEnd = e),
        i
      );
    }
    setEmptyAnimations(t = 0) {
      let e = this.queue.drainDisabled;
      this.queue.drainDisabled = !0;
      for (let e = 0, s = this.tracks.length; e < s; e++) {
        let s = this.tracks[e];
        s && this.setEmptyAnimation(s.trackIndex, t);
      }
      (this.queue.drainDisabled = e), this.queue.drain();
    }
    expandToIndex(t) {
      return t < this.tracks.length
        ? this.tracks[t]
        : (i.ensureArrayCapacity(this.tracks, t + 1, null),
          (this.tracks.length = t + 1),
          null);
    }
    trackEntry(t, e, s, i) {
      let r = this.trackEntryPool.obtain();
      return (
        r.reset(),
        (r.trackIndex = t),
        (r.animation = e),
        (r.loop = s),
        (r.holdPrevious = !1),
        (r.reverse = !1),
        (r.shortestRotation = !1),
        (r.eventThreshold = 0),
        (r.attachmentThreshold = 0),
        (r.drawOrderThreshold = 0),
        (r.animationStart = 0),
        (r.animationEnd = e.duration),
        (r.animationLast = -1),
        (r.nextAnimationLast = -1),
        (r.delay = 0),
        (r.trackTime = 0),
        (r.trackLast = -1),
        (r.nextTrackLast = -1),
        (r.trackEnd = Number.MAX_VALUE),
        (r.timeScale = 1),
        (r.alpha = 1),
        (r.mixTime = 0),
        (r.mixDuration = i ? this.data.getMix(i.animation, e) : 0),
        (r.interruptAlpha = 1),
        (r.totalAlpha = 0),
        (r.mixBlend = m.replace),
        r
      );
    }
    clearNext(t) {
      let e = t.next;
      for (; e; ) this.queue.dispose(e), (e = e.next);
      t.next = null;
    }
    _animationsChanged() {
      (this.animationsChanged = !1), this.propertyIDs.clear();
      let t = this.tracks;
      for (let e = 0, s = t.length; e < s; e++) {
        let s = t[e];
        if (s) {
          for (; s.mixingFrom; ) s = s.mixingFrom;
          do {
            (s.mixingTo && s.mixBlend == m.add) || this.computeHold(s),
              (s = s.mixingTo);
          } while (s);
        }
      }
    }
    computeHold(t) {
      let e = t.mixingTo,
        s = t.animation.timelines,
        i = t.animation.timelines.length,
        r = t.timelineMode;
      r.length = i;
      let n = t.timelineHoldMix;
      n.length = 0;
      let a = this.propertyIDs;
      if (e && e.holdPrevious)
        for (let t = 0; t < i; t++)
          r[t] = a.addAll(s[t].getPropertyIds()) ? J : K;
      else
        t: for (let h = 0; h < i; h++) {
          let i = s[h],
            o = i.getPropertyIds();
          if (a.addAll(o))
            if (
              !e ||
              i instanceof P ||
              i instanceof B ||
              i instanceof D ||
              !e.animation.hasTimeline(o)
            )
              r[h] = j;
            else {
              for (let s = e.mixingTo; s; s = s.mixingTo)
                if (!s.animation.hasTimeline(o)) {
                  if (t.mixDuration > 0) {
                    (r[h] = Q), (n[h] = s);
                    continue t;
                  }
                  break;
                }
              r[h] = J;
            }
          else r[h] = G;
        }
    }
    getCurrent(t) {
      return t >= this.tracks.length ? null : this.tracks[t];
    }
    addListener(t) {
      if (!t) throw new Error("listener cannot be null.");
      this.listeners.push(t);
    }
    removeListener(t) {
      let e = this.listeners.indexOf(t);
      e >= 0 && this.listeners.splice(e, 1);
    }
    clearListeners() {
      this.listeners.length = 0;
    }
    clearListenerNotifications() {
      this.queue.clear();
    }
  }
  z._emptyAnimation = new u("<empty>", [], 0);
  class U {
    constructor() {
      (this.animation = null),
        (this.previous = null),
        (this.next = null),
        (this.mixingFrom = null),
        (this.mixingTo = null),
        (this.listener = null),
        (this.trackIndex = 0),
        (this.loop = !1),
        (this.holdPrevious = !1),
        (this.reverse = !1),
        (this.shortestRotation = !1),
        (this.eventThreshold = 0),
        (this.attachmentThreshold = 0),
        (this.drawOrderThreshold = 0),
        (this.animationStart = 0),
        (this.animationEnd = 0),
        (this.animationLast = 0),
        (this.nextAnimationLast = 0),
        (this.delay = 0),
        (this.trackTime = 0),
        (this.trackLast = 0),
        (this.nextTrackLast = 0),
        (this.trackEnd = 0),
        (this.timeScale = 0),
        (this.alpha = 0),
        (this.mixTime = 0),
        (this.mixDuration = 0),
        (this.interruptAlpha = 0),
        (this.totalAlpha = 0),
        (this.mixBlend = m.replace),
        (this.timelineMode = new Array()),
        (this.timelineHoldMix = new Array()),
        (this.timelinesRotation = new Array());
    }
    reset() {
      (this.next = null),
        (this.previous = null),
        (this.mixingFrom = null),
        (this.mixingTo = null),
        (this.animation = null),
        (this.listener = null),
        (this.timelineMode.length = 0),
        (this.timelineHoldMix.length = 0),
        (this.timelinesRotation.length = 0);
    }
    getAnimationTime() {
      if (this.loop) {
        let t = this.animationEnd - this.animationStart;
        return 0 == t
          ? this.animationStart
          : (this.trackTime % t) + this.animationStart;
      }
      return Math.min(this.trackTime + this.animationStart, this.animationEnd);
    }
    setAnimationLast(t) {
      (this.animationLast = t), (this.nextAnimationLast = t);
    }
    isComplete() {
      return this.trackTime >= this.animationEnd - this.animationStart;
    }
    resetRotationDirections() {
      this.timelinesRotation.length = 0;
    }
    getTrackComplete() {
      let t = this.animationEnd - this.animationStart;
      if (0 != t) {
        if (this.loop) return t * (1 + ((this.trackTime / t) | 0));
        if (this.trackTime < t) return t;
      }
      return this.trackTime;
    }
  }
  class $ {
    constructor(t) {
      (this.objects = []), (this.drainDisabled = !1), (this.animState = t);
    }
    start(t) {
      this.objects.push(H.start),
        this.objects.push(t),
        (this.animState.animationsChanged = !0);
    }
    interrupt(t) {
      this.objects.push(H.interrupt), this.objects.push(t);
    }
    end(t) {
      this.objects.push(H.end),
        this.objects.push(t),
        (this.animState.animationsChanged = !0);
    }
    dispose(t) {
      this.objects.push(H.dispose), this.objects.push(t);
    }
    complete(t) {
      this.objects.push(H.complete), this.objects.push(t);
    }
    event(t, e) {
      this.objects.push(H.event), this.objects.push(t), this.objects.push(e);
    }
    drain() {
      if (this.drainDisabled) return;
      this.drainDisabled = !0;
      let t = this.objects,
        e = this.animState.listeners;
      for (let s = 0; s < t.length; s += 2) {
        let i = t[s],
          r = t[s + 1];
        switch (i) {
          case H.start:
            r.listener && r.listener.start && r.listener.start(r);
            for (let t = 0; t < e.length; t++) {
              let s = e[t];
              s.start && s.start(r);
            }
            break;
          case H.interrupt:
            r.listener && r.listener.interrupt && r.listener.interrupt(r);
            for (let t = 0; t < e.length; t++) {
              let s = e[t];
              s.interrupt && s.interrupt(r);
            }
            break;
          case H.end:
            r.listener && r.listener.end && r.listener.end(r);
            for (let t = 0; t < e.length; t++) {
              let s = e[t];
              s.end && s.end(r);
            }
          case H.dispose:
            r.listener && r.listener.dispose && r.listener.dispose(r);
            for (let t = 0; t < e.length; t++) {
              let s = e[t];
              s.dispose && s.dispose(r);
            }
            this.animState.trackEntryPool.free(r);
            break;
          case H.complete:
            r.listener && r.listener.complete && r.listener.complete(r);
            for (let t = 0; t < e.length; t++) {
              let s = e[t];
              s.complete && s.complete(r);
            }
            break;
          case H.event:
            let i = t[2 + s++];
            r.listener && r.listener.event && r.listener.event(r, i);
            for (let t = 0; t < e.length; t++) {
              let s = e[t];
              s.event && s.event(r, i);
            }
        }
      }
      this.clear(), (this.drainDisabled = !1);
    }
    clear() {
      this.objects.length = 0;
    }
  }
  var H;
  !(function (t) {
    (t[(t.start = 0)] = "start"),
      (t[(t.interrupt = 1)] = "interrupt"),
      (t[(t.end = 2)] = "end"),
      (t[(t.dispose = 3)] = "dispose"),
      (t[(t.complete = 4)] = "complete"),
      (t[(t.event = 5)] = "event");
  })(H || (H = {}));
  const G = 0,
    j = 1,
    K = 2,
    J = 3,
    Q = 4,
    Z = 1,
    tt = 2;
  class et {
    constructor(t) {
      if (((this.animationToMixTime = {}), (this.defaultMix = 0), !t))
        throw new Error("skeletonData cannot be null.");
      this.skeletonData = t;
    }
    setMix(t, e, s) {
      let i = this.skeletonData.findAnimation(t);
      if (!i) throw new Error("Animation not found: " + t);
      let r = this.skeletonData.findAnimation(e);
      if (!r) throw new Error("Animation not found: " + e);
      this.setMixWith(i, r, s);
    }
    setMixWith(t, e, s) {
      if (!t) throw new Error("from cannot be null.");
      if (!e) throw new Error("to cannot be null.");
      let i = t.name + "." + e.name;
      this.animationToMixTime[i] = s;
    }
    getMix(t, e) {
      let s = t.name + "." + e.name,
        i = this.animationToMixTime[s];
      return void 0 === i ? this.defaultMix : i;
    }
  }
  class st extends o {
    constructor(t) {
      super(t), (this.color = new e(1, 1, 1, 1));
    }
    copy() {
      let t = new st(this.name);
      return this.copyTo(t), t.color.setFromColor(this.color), t;
    }
  }
  class it extends o {
    constructor(t) {
      super(t),
        (this.endSlot = null),
        (this.color = new e(0.2275, 0.2275, 0.8078, 1));
    }
    copy() {
      let t = new it(this.name);
      return (
        this.copyTo(t),
        (t.endSlot = this.endSlot),
        t.color.setFromColor(this.color),
        t
      );
    }
  }
  class rt {
    constructor(t) {
      this._image = t;
    }
    getImage() {
      return this._image;
    }
  }
  var nt, at, ht, ot, lt, ct, dt, ut, mt, ft, gt;
  !(function (t) {
    (t[(t.Nearest = 9728)] = "Nearest"),
      (t[(t.Linear = 9729)] = "Linear"),
      (t[(t.MipMap = 9987)] = "MipMap"),
      (t[(t.MipMapNearestNearest = 9984)] = "MipMapNearestNearest"),
      (t[(t.MipMapLinearNearest = 9985)] = "MipMapLinearNearest"),
      (t[(t.MipMapNearestLinear = 9986)] = "MipMapNearestLinear"),
      (t[(t.MipMapLinearLinear = 9987)] = "MipMapLinearLinear");
  })(nt || (nt = {})),
    (function (t) {
      (t[(t.MirroredRepeat = 33648)] = "MirroredRepeat"),
        (t[(t.ClampToEdge = 33071)] = "ClampToEdge"),
        (t[(t.Repeat = 10497)] = "Repeat");
    })(at || (at = {}));
  class pt {
    constructor() {
      (this.u = 0),
        (this.v = 0),
        (this.u2 = 0),
        (this.v2 = 0),
        (this.width = 0),
        (this.height = 0),
        (this.degrees = 0),
        (this.offsetX = 0),
        (this.offsetY = 0),
        (this.originalWidth = 0),
        (this.originalHeight = 0);
    }
  }
  class xt {
    constructor(t) {
      (this.pages = new Array()), (this.regions = new Array());
      let e = new wt(t),
        s = new Array(4),
        r = {
          size: (t) => {
            (t.width = parseInt(s[1])), (t.height = parseInt(s[2]));
          },
          format: () => {},
          filter: (t) => {
            (t.minFilter = i.enumValue(nt, s[1])),
              (t.magFilter = i.enumValue(nt, s[2]));
          },
          repeat: (t) => {
            -1 != s[1].indexOf("x") && (t.uWrap = at.Repeat),
              -1 != s[1].indexOf("y") && (t.vWrap = at.Repeat);
          },
          pma: (t) => {
            t.pma = "true" == s[1];
          },
        };
      var n = {
        xy: (t) => {
          (t.x = parseInt(s[1])), (t.y = parseInt(s[2]));
        },
        size: (t) => {
          (t.width = parseInt(s[1])), (t.height = parseInt(s[2]));
        },
        bounds: (t) => {
          (t.x = parseInt(s[1])),
            (t.y = parseInt(s[2])),
            (t.width = parseInt(s[3])),
            (t.height = parseInt(s[4]));
        },
        offset: (t) => {
          (t.offsetX = parseInt(s[1])), (t.offsetY = parseInt(s[2]));
        },
        orig: (t) => {
          (t.originalWidth = parseInt(s[1])),
            (t.originalHeight = parseInt(s[2]));
        },
        offsets: (t) => {
          (t.offsetX = parseInt(s[1])),
            (t.offsetY = parseInt(s[2])),
            (t.originalWidth = parseInt(s[3])),
            (t.originalHeight = parseInt(s[4]));
        },
        rotate: (t) => {
          let e = s[1];
          "true" == e
            ? (t.degrees = 90)
            : "false" != e && (t.degrees = parseInt(e));
        },
        index: (t) => {
          t.index = parseInt(s[1]);
        },
      };
      let a = e.readLine();
      for (; a && 0 == a.trim().length; ) a = e.readLine();
      for (; a && 0 != a.trim().length && 0 != e.readEntry(s, a); )
        a = e.readLine();
      let h = null,
        o = null,
        l = null;
      for (; null !== a; )
        if (0 == a.trim().length) (h = null), (a = e.readLine());
        else if (h) {
          let t = new vt(h, a);
          for (;;) {
            let i = e.readEntry(s, (a = e.readLine()));
            if (0 == i) break;
            let r = n[s[0]];
            if (r) r(t);
            else {
              o || (o = []), l || (l = []), o.push(s[0]);
              let t = [];
              for (let e = 0; e < i; e++) t.push(parseInt(s[e + 1]));
              l.push(t);
            }
          }
          0 == t.originalWidth &&
            0 == t.originalHeight &&
            ((t.originalWidth = t.width), (t.originalHeight = t.height)),
            o &&
              o.length > 0 &&
              l &&
              l.length > 0 &&
              ((t.names = o), (t.values = l), (o = null), (l = null)),
            (t.u = t.x / h.width),
            (t.v = t.y / h.height),
            90 == t.degrees
              ? ((t.u2 = (t.x + t.height) / h.width),
                (t.v2 = (t.y + t.width) / h.height))
              : ((t.u2 = (t.x + t.width) / h.width),
                (t.v2 = (t.y + t.height) / h.height)),
            this.regions.push(t);
        } else {
          for (
            h = new bt(a.trim());
            0 != e.readEntry(s, (a = e.readLine()));

          ) {
            let t = r[s[0]];
            t && t(h);
          }
          this.pages.push(h);
        }
    }
    findRegion(t) {
      for (let e = 0; e < this.regions.length; e++)
        if (this.regions[e].name == t) return this.regions[e];
      return null;
    }
    setTextures(t, e = "") {
      for (let s of this.pages) s.setTexture(t.get(e + s.name));
    }
    dispose() {
      var t;
      for (let e = 0; e < this.pages.length; e++)
        null === (t = this.pages[e].texture) || void 0 === t || t.dispose();
    }
  }
  class wt {
    constructor(t) {
      (this.index = 0), (this.lines = t.split(/\r\n|\r|\n/));
    }
    readLine() {
      return this.index >= this.lines.length ? null : this.lines[this.index++];
    }
    readEntry(t, e) {
      if (!e) return 0;
      if (0 == (e = e.trim()).length) return 0;
      let s = e.indexOf(":");
      if (-1 == s) return 0;
      t[0] = e.substr(0, s).trim();
      for (let i = 1, r = s + 1; ; i++) {
        let s = e.indexOf(",", r);
        if (-1 == s) return (t[i] = e.substr(r).trim()), i;
        if (((t[i] = e.substr(r, s - r).trim()), (r = s + 1), 4 == i)) return 4;
      }
    }
  }
  class bt {
    constructor(t) {
      (this.minFilter = nt.Nearest),
        (this.magFilter = nt.Nearest),
        (this.uWrap = at.ClampToEdge),
        (this.vWrap = at.ClampToEdge),
        (this.texture = null),
        (this.width = 0),
        (this.height = 0),
        (this.pma = !1),
        (this.name = t);
    }
    setTexture(t) {
      (this.texture = t),
        t.setFilters(this.minFilter, this.magFilter),
        t.setWraps(this.uWrap, this.vWrap);
    }
  }
  class vt extends pt {
    constructor(t, e) {
      super(),
        (this.x = 0),
        (this.y = 0),
        (this.offsetX = 0),
        (this.offsetY = 0),
        (this.originalWidth = 0),
        (this.originalHeight = 0),
        (this.index = 0),
        (this.degrees = 0),
        (this.names = null),
        (this.values = null),
        (this.page = t),
        (this.name = e);
    }
  }
  class yt extends o {
    constructor(t, s) {
      super(t),
        (this.region = null),
        (this.regionUVs = []),
        (this.uvs = []),
        (this.triangles = []),
        (this.color = new e(1, 1, 1, 1)),
        (this.width = 0),
        (this.height = 0),
        (this.hullLength = 0),
        (this.edges = []),
        (this.parentMesh = null),
        (this.sequence = null),
        (this.tempColor = new e(0, 0, 0, 0)),
        (this.path = s);
    }
    updateRegion() {
      if (!this.region) throw new Error("Region not set.");
      let t = this.regionUVs;
      (this.uvs && this.uvs.length == t.length) ||
        (this.uvs = i.newFloatArray(t.length));
      let e = this.uvs,
        s = this.uvs.length,
        r = this.region.u,
        n = this.region.v,
        a = 0,
        h = 0;
      if (this.region instanceof vt) {
        let i = this.region,
          o = i.page.texture.getImage(),
          l = o.width,
          c = o.height;
        switch (i.degrees) {
          case 90:
            (r -= (i.originalHeight - i.offsetY - i.height) / l),
              (n -= (i.originalWidth - i.offsetX - i.width) / c),
              (a = i.originalHeight / l),
              (h = i.originalWidth / c);
            for (let i = 0; i < s; i += 2)
              (e[i] = r + t[i + 1] * a), (e[i + 1] = n + (1 - t[i]) * h);
            return;
          case 180:
            (r -= (i.originalWidth - i.offsetX - i.width) / l),
              (n -= i.offsetY / c),
              (a = i.originalWidth / l),
              (h = i.originalHeight / c);
            for (let i = 0; i < s; i += 2)
              (e[i] = r + (1 - t[i]) * a), (e[i + 1] = n + (1 - t[i + 1]) * h);
            return;
          case 270:
            (r -= i.offsetY / l),
              (n -= i.offsetX / c),
              (a = i.originalHeight / l),
              (h = i.originalWidth / c);
            for (let i = 0; i < s; i += 2)
              (e[i] = r + (1 - t[i + 1]) * a), (e[i + 1] = n + t[i] * h);
            return;
        }
        (r -= i.offsetX / l),
          (n -= (i.originalHeight - i.offsetY - i.height) / c),
          (a = i.originalWidth / l),
          (h = i.originalHeight / c);
      } else
        this.region
          ? ((a = this.region.u2 - r), (h = this.region.v2 - n))
          : ((r = n = 0), (a = h = 1));
      for (let i = 0; i < s; i += 2)
        (e[i] = r + t[i] * a), (e[i + 1] = n + t[i + 1] * h);
    }
    getParentMesh() {
      return this.parentMesh;
    }
    setParentMesh(t) {
      (this.parentMesh = t),
        t &&
          ((this.bones = t.bones),
          (this.vertices = t.vertices),
          (this.worldVerticesLength = t.worldVerticesLength),
          (this.regionUVs = t.regionUVs),
          (this.triangles = t.triangles),
          (this.hullLength = t.hullLength),
          (this.worldVerticesLength = t.worldVerticesLength));
    }
    copy() {
      if (this.parentMesh) return this.newLinkedMesh();
      let t = new yt(this.name, this.path);
      return (
        (t.region = this.region),
        t.color.setFromColor(this.color),
        this.copyTo(t),
        (t.regionUVs = new Array(this.regionUVs.length)),
        i.arrayCopy(this.regionUVs, 0, t.regionUVs, 0, this.regionUVs.length),
        (t.uvs = new Array(this.uvs.length)),
        i.arrayCopy(this.uvs, 0, t.uvs, 0, this.uvs.length),
        (t.triangles = new Array(this.triangles.length)),
        i.arrayCopy(this.triangles, 0, t.triangles, 0, this.triangles.length),
        (t.hullLength = this.hullLength),
        (t.sequence = null != this.sequence ? this.sequence.copy() : null),
        this.edges &&
          ((t.edges = new Array(this.edges.length)),
          i.arrayCopy(this.edges, 0, t.edges, 0, this.edges.length)),
        (t.width = this.width),
        (t.height = this.height),
        t
      );
    }
    computeWorldVertices(t, e, s, i, r, n) {
      null != this.sequence && this.sequence.apply(t, this),
        super.computeWorldVertices(t, e, s, i, r, n);
    }
    newLinkedMesh() {
      let t = new yt(this.name, this.path);
      return (
        (t.region = this.region),
        t.color.setFromColor(this.color),
        (t.timelineAttachment = this.timelineAttachment),
        t.setParentMesh(this.parentMesh ? this.parentMesh : this),
        null != t.region && t.updateRegion(),
        t
      );
    }
  }
  class At extends o {
    constructor(t) {
      super(t),
        (this.lengths = []),
        (this.closed = !1),
        (this.constantSpeed = !1),
        (this.color = new e(1, 1, 1, 1));
    }
    copy() {
      let t = new At(this.name);
      return (
        this.copyTo(t),
        (t.lengths = new Array(this.lengths.length)),
        i.arrayCopy(this.lengths, 0, t.lengths, 0, this.lengths.length),
        (t.closed = closed),
        (t.constantSpeed = this.constantSpeed),
        t.color.setFromColor(this.color),
        t
      );
    }
  }
  class Ct extends o {
    constructor(t) {
      super(t),
        (this.x = 0),
        (this.y = 0),
        (this.rotation = 0),
        (this.color = new e(0.38, 0.94, 0, 1));
    }
    computeWorldPosition(t, e) {
      return (
        (e.x = this.x * t.a + this.y * t.b + t.worldX),
        (e.y = this.x * t.c + this.y * t.d + t.worldY),
        e
      );
    }
    computeWorldRotation(t) {
      let e = s.cosDeg(this.rotation),
        i = s.sinDeg(this.rotation),
        r = e * t.a + i * t.b,
        n = e * t.c + i * t.d;
      return Math.atan2(n, r) * s.radDeg;
    }
    copy() {
      let t = new Ct(this.name);
      return (
        (t.x = this.x),
        (t.y = this.y),
        (t.rotation = this.rotation),
        t.color.setFromColor(this.color),
        t
      );
    }
  }
  class kt extends h {
    constructor(t, s) {
      super(t),
        (this.x = 0),
        (this.y = 0),
        (this.scaleX = 1),
        (this.scaleY = 1),
        (this.rotation = 0),
        (this.width = 0),
        (this.height = 0),
        (this.color = new e(1, 1, 1, 1)),
        (this.rendererObject = null),
        (this.region = null),
        (this.sequence = null),
        (this.offset = i.newFloatArray(8)),
        (this.uvs = i.newFloatArray(8)),
        (this.tempColor = new e(1, 1, 1, 1)),
        (this.path = s);
    }
    updateRegion() {
      if (!this.region) throw new Error("Region not set.");
      let t = this.region,
        e = this.uvs;
      if (null == t)
        return (
          (e[0] = 0),
          (e[1] = 0),
          (e[2] = 0),
          (e[3] = 1),
          (e[4] = 1),
          (e[5] = 1),
          (e[6] = 1),
          void (e[7] = 0)
        );
      let s = (this.width / this.region.originalWidth) * this.scaleX,
        i = (this.height / this.region.originalHeight) * this.scaleY,
        r = (-this.width / 2) * this.scaleX + this.region.offsetX * s,
        n = (-this.height / 2) * this.scaleY + this.region.offsetY * i,
        a = r + this.region.width * s,
        h = n + this.region.height * i,
        o = (this.rotation * Math.PI) / 180,
        l = Math.cos(o),
        c = Math.sin(o),
        d = this.x,
        u = this.y,
        m = r * l + d,
        f = r * c,
        g = n * l + u,
        p = n * c,
        x = a * l + d,
        w = a * c,
        b = h * l + u,
        v = h * c,
        y = this.offset;
      (y[0] = m - p),
        (y[1] = g + f),
        (y[2] = m - v),
        (y[3] = b + f),
        (y[4] = x - v),
        (y[5] = b + w),
        (y[6] = x - p),
        (y[7] = g + w),
        90 == t.degrees
          ? ((e[0] = t.u2),
            (e[1] = t.v2),
            (e[2] = t.u),
            (e[3] = t.v2),
            (e[4] = t.u),
            (e[5] = t.v),
            (e[6] = t.u2),
            (e[7] = t.v))
          : ((e[0] = t.u),
            (e[1] = t.v2),
            (e[2] = t.u),
            (e[3] = t.v),
            (e[4] = t.u2),
            (e[5] = t.v),
            (e[6] = t.u2),
            (e[7] = t.v2));
    }
    computeWorldVertices(t, e, s, i) {
      null != this.sequence && this.sequence.apply(t, this);
      let r = t.bone,
        n = this.offset,
        a = r.worldX,
        h = r.worldY,
        o = r.a,
        l = r.b,
        c = r.c,
        d = r.d,
        u = 0,
        m = 0;
      (u = n[0]),
        (m = n[1]),
        (e[s] = u * o + m * l + a),
        (e[s + 1] = u * c + m * d + h),
        (s += i),
        (u = n[2]),
        (m = n[3]),
        (e[s] = u * o + m * l + a),
        (e[s + 1] = u * c + m * d + h),
        (s += i),
        (u = n[4]),
        (m = n[5]),
        (e[s] = u * o + m * l + a),
        (e[s + 1] = u * c + m * d + h),
        (s += i),
        (u = n[6]),
        (m = n[7]),
        (e[s] = u * o + m * l + a),
        (e[s + 1] = u * c + m * d + h);
    }
    copy() {
      let t = new kt(this.name, this.path);
      return (
        (t.region = this.region),
        (t.rendererObject = this.rendererObject),
        (t.x = this.x),
        (t.y = this.y),
        (t.scaleX = this.scaleX),
        (t.scaleY = this.scaleY),
        (t.rotation = this.rotation),
        (t.width = this.width),
        (t.height = this.height),
        i.arrayCopy(this.uvs, 0, t.uvs, 0, 8),
        i.arrayCopy(this.offset, 0, t.offset, 0, 8),
        t.color.setFromColor(this.color),
        (t.sequence = null != this.sequence ? this.sequence.copy() : null),
        t
      );
    }
  }
  (kt.X1 = 0),
    (kt.Y1 = 1),
    (kt.C1R = 2),
    (kt.C1G = 3),
    (kt.C1B = 4),
    (kt.C1A = 5),
    (kt.U1 = 6),
    (kt.V1 = 7),
    (kt.X2 = 8),
    (kt.Y2 = 9),
    (kt.C2R = 10),
    (kt.C2G = 11),
    (kt.C2B = 12),
    (kt.C2A = 13),
    (kt.U2 = 14),
    (kt.V2 = 15),
    (kt.X3 = 16),
    (kt.Y3 = 17),
    (kt.C3R = 18),
    (kt.C3G = 19),
    (kt.C3B = 20),
    (kt.C3A = 21),
    (kt.U3 = 22),
    (kt.V3 = 23),
    (kt.X4 = 24),
    (kt.Y4 = 25),
    (kt.C4R = 26),
    (kt.C4G = 27),
    (kt.C4B = 28),
    (kt.C4A = 29),
    (kt.U4 = 30),
    (kt.V4 = 31);
  class St {
    constructor(t) {
      this.atlas = t;
    }
    loadSequence(t, e, s) {
      let i = s.regions;
      for (let r = 0, n = i.length; r < n; r++) {
        let n = s.getPath(e, r),
          a = this.atlas.findRegion(n);
        if (null == a)
          throw new Error(
            "Region not found in atlas: " + n + " (sequence: " + t + ")"
          );
        (i[r] = a), (i[r].renderObject = i[r]);
      }
    }
    newRegionAttachment(t, e, s, i) {
      let r = new kt(e, s);
      if (null != i) this.loadSequence(e, s, i);
      else {
        let t = this.atlas.findRegion(s);
        if (!t)
          throw new Error(
            "Region not found in atlas: " +
              s +
              " (region attachment: " +
              e +
              ")"
          );
        (t.renderObject = t), (r.region = t);
      }
      return r;
    }
    newMeshAttachment(t, e, s, i) {
      let r = new yt(e, s);
      if (null != i) this.loadSequence(e, s, i);
      else {
        let t = this.atlas.findRegion(s);
        if (!t)
          throw new Error(
            "Region not found in atlas: " + s + " (mesh attachment: " + e + ")"
          );
        (t.renderObject = t), (r.region = t);
      }
      return r;
    }
    newBoundingBoxAttachment(t, e) {
      return new st(e);
    }
    newPathAttachment(t, e) {
      return new At(e);
    }
    newPointAttachment(t, e) {
      return new Ct(e);
    }
    newClippingAttachment(t, e) {
      return new it(e);
    }
  }
  class Et {
    constructor(t, s, i) {
      if (
        ((this.index = 0),
        (this.parent = null),
        (this.length = 0),
        (this.x = 0),
        (this.y = 0),
        (this.rotation = 0),
        (this.scaleX = 1),
        (this.scaleY = 1),
        (this.shearX = 0),
        (this.shearY = 0),
        (this.transformMode = ht.Normal),
        (this.skinRequired = !1),
        (this.color = new e()),
        t < 0)
      )
        throw new Error("index must be >= 0.");
      if (!s) throw new Error("name cannot be null.");
      (this.index = t), (this.name = s), (this.parent = i);
    }
  }
  !(function (t) {
    (t[(t.Normal = 0)] = "Normal"),
      (t[(t.OnlyTranslation = 1)] = "OnlyTranslation"),
      (t[(t.NoRotationOrReflection = 2)] = "NoRotationOrReflection"),
      (t[(t.NoScale = 3)] = "NoScale"),
      (t[(t.NoScaleOrReflection = 4)] = "NoScaleOrReflection");
  })(ht || (ht = {}));
  class Mt {
    constructor(t, e, s) {
      if (
        ((this.parent = null),
        (this.children = new Array()),
        (this.x = 0),
        (this.y = 0),
        (this.rotation = 0),
        (this.scaleX = 0),
        (this.scaleY = 0),
        (this.shearX = 0),
        (this.shearY = 0),
        (this.ax = 0),
        (this.ay = 0),
        (this.arotation = 0),
        (this.ascaleX = 0),
        (this.ascaleY = 0),
        (this.ashearX = 0),
        (this.ashearY = 0),
        (this.a = 0),
        (this.b = 0),
        (this.c = 0),
        (this.d = 0),
        (this.worldY = 0),
        (this.worldX = 0),
        (this.sorted = !1),
        (this.active = !1),
        !t)
      )
        throw new Error("data cannot be null.");
      if (!e) throw new Error("skeleton cannot be null.");
      (this.data = t),
        (this.skeleton = e),
        (this.parent = s),
        this.setToSetupPose();
    }
    isActive() {
      return this.active;
    }
    update() {
      this.updateWorldTransformWith(
        this.ax,
        this.ay,
        this.arotation,
        this.ascaleX,
        this.ascaleY,
        this.ashearX,
        this.ashearY
      );
    }
    updateWorldTransform() {
      this.updateWorldTransformWith(
        this.x,
        this.y,
        this.rotation,
        this.scaleX,
        this.scaleY,
        this.shearX,
        this.shearY
      );
    }
    updateWorldTransformWith(t, e, i, r, n, a, h) {
      (this.ax = t),
        (this.ay = e),
        (this.arotation = i),
        (this.ascaleX = r),
        (this.ascaleY = n),
        (this.ashearX = a),
        (this.ashearY = h);
      let o = this.parent;
      if (!o) {
        let o = this.skeleton,
          l = i + 90 + h,
          c = o.scaleX,
          d = o.scaleY;
        return (
          (this.a = s.cosDeg(i + a) * r * c),
          (this.b = s.cosDeg(l) * n * c),
          (this.c = s.sinDeg(i + a) * r * d),
          (this.d = s.sinDeg(l) * n * d),
          (this.worldX = t * c + o.x),
          void (this.worldY = e * d + o.y)
        );
      }
      let l = o.a,
        c = o.b,
        d = o.c,
        u = o.d;
      switch (
        ((this.worldX = l * t + c * e + o.worldX),
        (this.worldY = d * t + u * e + o.worldY),
        this.data.transformMode)
      ) {
        case ht.Normal: {
          let t = i + 90 + h,
            e = s.cosDeg(i + a) * r,
            o = s.cosDeg(t) * n,
            m = s.sinDeg(i + a) * r,
            f = s.sinDeg(t) * n;
          return (
            (this.a = l * e + c * m),
            (this.b = l * o + c * f),
            (this.c = d * e + u * m),
            void (this.d = d * o + u * f)
          );
        }
        case ht.OnlyTranslation: {
          let t = i + 90 + h;
          (this.a = s.cosDeg(i + a) * r),
            (this.b = s.cosDeg(t) * n),
            (this.c = s.sinDeg(i + a) * r),
            (this.d = s.sinDeg(t) * n);
          break;
        }
        case ht.NoRotationOrReflection: {
          let t = l * l + d * d,
            e = 0;
          t > 1e-4
            ? ((t = Math.abs(l * u - c * d) / t),
              (l /= this.skeleton.scaleX),
              (d /= this.skeleton.scaleY),
              (c = d * t),
              (u = l * t),
              (e = Math.atan2(d, l) * s.radDeg))
            : ((l = 0), (d = 0), (e = 90 - Math.atan2(u, c) * s.radDeg));
          let o = i + a - e,
            m = i + h - e + 90,
            f = s.cosDeg(o) * r,
            g = s.cosDeg(m) * n,
            p = s.sinDeg(o) * r,
            x = s.sinDeg(m) * n;
          (this.a = l * f - c * p),
            (this.b = l * g - c * x),
            (this.c = d * f + u * p),
            (this.d = d * g + u * x);
          break;
        }
        case ht.NoScale:
        case ht.NoScaleOrReflection: {
          let t = s.cosDeg(i),
            e = s.sinDeg(i),
            o = (l * t + c * e) / this.skeleton.scaleX,
            m = (d * t + u * e) / this.skeleton.scaleY,
            f = Math.sqrt(o * o + m * m);
          f > 1e-5 && (f = 1 / f),
            (o *= f),
            (m *= f),
            (f = Math.sqrt(o * o + m * m)),
            this.data.transformMode == ht.NoScale &&
              l * u - c * d < 0 !=
                (this.skeleton.scaleX < 0 != this.skeleton.scaleY < 0) &&
              (f = -f);
          let g = Math.PI / 2 + Math.atan2(m, o),
            p = Math.cos(g) * f,
            x = Math.sin(g) * f,
            w = s.cosDeg(a) * r,
            b = s.cosDeg(90 + h) * n,
            v = s.sinDeg(a) * r,
            y = s.sinDeg(90 + h) * n;
          (this.a = o * w + p * v),
            (this.b = o * b + p * y),
            (this.c = m * w + x * v),
            (this.d = m * b + x * y);
          break;
        }
      }
      (this.a *= this.skeleton.scaleX),
        (this.b *= this.skeleton.scaleX),
        (this.c *= this.skeleton.scaleY),
        (this.d *= this.skeleton.scaleY);
    }
    setToSetupPose() {
      let t = this.data;
      (this.x = t.x),
        (this.y = t.y),
        (this.rotation = t.rotation),
        (this.scaleX = t.scaleX),
        (this.scaleY = t.scaleY),
        (this.shearX = t.shearX),
        (this.shearY = t.shearY);
    }
    getWorldRotationX() {
      return Math.atan2(this.c, this.a) * s.radDeg;
    }
    getWorldRotationY() {
      return Math.atan2(this.d, this.b) * s.radDeg;
    }
    getWorldScaleX() {
      return Math.sqrt(this.a * this.a + this.c * this.c);
    }
    getWorldScaleY() {
      return Math.sqrt(this.b * this.b + this.d * this.d);
    }
    updateAppliedTransform() {
      let t = this.parent;
      if (!t)
        return (
          (this.ax = this.worldX - this.skeleton.x),
          (this.ay = this.worldY - this.skeleton.y),
          (this.arotation = Math.atan2(this.c, this.a) * s.radDeg),
          (this.ascaleX = Math.sqrt(this.a * this.a + this.c * this.c)),
          (this.ascaleY = Math.sqrt(this.b * this.b + this.d * this.d)),
          (this.ashearX = 0),
          void (this.ashearY =
            Math.atan2(
              this.a * this.b + this.c * this.d,
              this.a * this.d - this.b * this.c
            ) * s.radDeg)
        );
      let e,
        i,
        r,
        n,
        a = t.a,
        h = t.b,
        o = t.c,
        l = t.d,
        c = 1 / (a * l - h * o),
        d = l * c,
        u = h * c,
        m = o * c,
        f = a * c,
        g = this.worldX - t.worldX,
        p = this.worldY - t.worldY;
      if (
        ((this.ax = g * d - p * u),
        (this.ay = p * f - g * m),
        this.data.transformMode == ht.OnlyTranslation)
      )
        (e = this.a), (i = this.b), (r = this.c), (n = this.d);
      else {
        switch (this.data.transformMode) {
          case ht.NoRotationOrReflection: {
            let t = Math.abs(a * l - h * o) / (a * a + o * o),
              e = a / this.skeleton.scaleX;
            (h = (-o / this.skeleton.scaleY) * t * this.skeleton.scaleX),
              (l = e * t * this.skeleton.scaleY),
              (c = 1 / (a * l - h * o)),
              (d = l * c),
              (u = h * c);
            break;
          }
          case ht.NoScale:
          case ht.NoScaleOrReflection:
            let t = s.cosDeg(this.rotation),
              e = s.sinDeg(this.rotation);
            (a = (a * t + h * e) / this.skeleton.scaleX),
              (o = (o * t + l * e) / this.skeleton.scaleY);
            let i = Math.sqrt(a * a + o * o);
            i > 1e-5 && (i = 1 / i),
              (a *= i),
              (o *= i),
              (i = Math.sqrt(a * a + o * o)),
              this.data.transformMode == ht.NoScale &&
                c < 0 !=
                  (this.skeleton.scaleX < 0 != this.skeleton.scaleY < 0) &&
                (i = -i);
            let r = s.PI / 2 + Math.atan2(o, a);
            (h = Math.cos(r) * i),
              (l = Math.sin(r) * i),
              (c = 1 / (a * l - h * o)),
              (d = l * c),
              (u = h * c),
              (m = o * c),
              (f = a * c);
        }
        (e = d * this.a - u * this.c),
          (i = d * this.b - u * this.d),
          (r = f * this.c - m * this.a),
          (n = f * this.d - m * this.b);
      }
      if (
        ((this.ashearX = 0),
        (this.ascaleX = Math.sqrt(e * e + r * r)),
        this.ascaleX > 1e-4)
      ) {
        let t = e * n - i * r;
        (this.ascaleY = t / this.ascaleX),
          (this.ashearY = -Math.atan2(e * i + r * n, t) * s.radDeg),
          (this.arotation = Math.atan2(r, e) * s.radDeg);
      } else
        (this.ascaleX = 0),
          (this.ascaleY = Math.sqrt(i * i + n * n)),
          (this.ashearY = 0),
          (this.arotation = 90 - Math.atan2(n, i) * s.radDeg);
    }
    worldToLocal(t) {
      let e = 1 / (this.a * this.d - this.b * this.c),
        s = t.x - this.worldX,
        i = t.y - this.worldY;
      return (
        (t.x = s * this.d * e - i * this.b * e),
        (t.y = i * this.a * e - s * this.c * e),
        t
      );
    }
    localToWorld(t) {
      let e = t.x,
        s = t.y;
      return (
        (t.x = e * this.a + s * this.b + this.worldX),
        (t.y = e * this.c + s * this.d + this.worldY),
        t
      );
    }
    worldToLocalRotation(t) {
      let e = s.sinDeg(t),
        i = s.cosDeg(t);
      return (
        Math.atan2(this.a * e - this.c * i, this.d * i - this.b * e) *
          s.radDeg +
        this.rotation -
        this.shearX
      );
    }
    localToWorldRotation(t) {
      t -= this.rotation - this.shearX;
      let e = s.sinDeg(t),
        i = s.cosDeg(t);
      return (
        Math.atan2(i * this.c + e * this.d, i * this.a + e * this.b) * s.radDeg
      );
    }
    rotateWorld(t) {
      let e = this.a,
        i = this.b,
        r = this.c,
        n = this.d,
        a = s.cosDeg(t),
        h = s.sinDeg(t);
      (this.a = a * e - h * r),
        (this.b = a * i - h * n),
        (this.c = h * e + a * r),
        (this.d = h * i + a * n);
    }
  }
  class Tt {
    constructor(t, e = "", s = new Rt()) {
      (this.pathPrefix = ""),
        (this.assets = {}),
        (this.errors = {}),
        (this.toLoad = 0),
        (this.loaded = 0),
        (this.textureLoader = t),
        (this.pathPrefix = e),
        (this.downloader = s);
    }
    start(t) {
      return this.toLoad++, this.pathPrefix + t;
    }
    success(t, e, s) {
      this.toLoad--, this.loaded++, (this.assets[e] = s), t && t(e, s);
    }
    error(t, e, s) {
      this.toLoad--, this.loaded++, (this.errors[e] = s), t && t(e, s);
    }
    loadAll() {
      return new Promise((t, e) => {
        let s = () => {
          this.isLoadingComplete()
            ? this.hasErrors()
              ? e(this.errors)
              : t(this)
            : requestAnimationFrame(s);
        };
        requestAnimationFrame(s);
      });
    }
    setRawDataURI(t, e) {
      this.downloader.rawDataUris[this.pathPrefix + t] = e;
    }
    loadBinary(t, e = () => {}, s = () => {}) {
      (t = this.start(t)),
        this.downloader.downloadBinary(
          t,
          (s) => {
            this.success(e, t, s);
          },
          (e, i) => {
            this.error(s, t, `Couldn't load binary ${t}: status ${e}, ${i}`);
          }
        );
    }
    loadText(t, e = () => {}, s = () => {}) {
      (t = this.start(t)),
        this.downloader.downloadText(
          t,
          (s) => {
            this.success(e, t, s);
          },
          (e, i) => {
            this.error(s, t, `Couldn't load text ${t}: status ${e}, ${i}`);
          }
        );
    }
    loadJson(t, e = () => {}, s = () => {}) {
      (t = this.start(t)),
        this.downloader.downloadJson(
          t,
          (s) => {
            this.success(e, t, s);
          },
          (e, i) => {
            this.error(s, t, `Couldn't load JSON ${t}: status ${e}, ${i}`);
          }
        );
    }
    loadTexture(t, e = () => {}, s = () => {}) {
      if (
        ((t = this.start(t)),
        "undefined" != typeof window &&
          "undefined" != typeof navigator &&
          window.document)
      ) {
        let i = new Image();
        (i.crossOrigin = "anonymous"),
          (i.onload = () => {
            this.success(e, t, this.textureLoader(i));
          }),
          (i.onerror = () => {
            this.error(s, t, `Couldn't load image: ${t}`);
          }),
          this.downloader.rawDataUris[t] &&
            (t = this.downloader.rawDataUris[t]),
          (i.src = t);
      } else
        fetch(t, { mode: "cors" })
          .then((e) =>
            e.ok
              ? e.blob()
              : (this.error(s, t, `Couldn't load image: ${t}`), null)
          )
          .then((t) =>
            t
              ? createImageBitmap(t, {
                  premultiplyAlpha: "none",
                  colorSpaceConversion: "none",
                })
              : null
          )
          .then((s) => {
            s && this.success(e, t, this.textureLoader(s));
          });
    }
    loadTextureAtlas(t, e = () => {}, s = () => {}, i) {
      let r = t.lastIndexOf("/"),
        n = r >= 0 ? t.substring(0, r + 1) : "";
      (t = this.start(t)),
        this.downloader.downloadText(
          t,
          (r) => {
            try {
              let a = new xt(r),
                h = a.pages.length,
                o = !1;
              for (let r of a.pages)
                this.loadTexture(
                  i ? i[r.name] : n + r.name,
                  (s, i) => {
                    o || (r.setTexture(i), 0 == --h && this.success(e, t, a));
                  },
                  (e, i) => {
                    o ||
                      this.error(
                        s,
                        t,
                        `Couldn't load texture atlas ${t} page image: ${e}`
                      ),
                      (o = !0);
                  }
                );
            } catch (e) {
              this.error(
                s,
                t,
                `Couldn't parse texture atlas ${t}: ${e.message}`
              );
            }
          },
          (e, i) => {
            this.error(
              s,
              t,
              `Couldn't load texture atlas ${t}: status ${e}, ${i}`
            );
          }
        );
    }
    get(t) {
      return this.assets[this.pathPrefix + t];
    }
    require(t) {
      t = this.pathPrefix + t;
      let e = this.assets[t];
      if (e) return e;
      let s = this.errors[t];
      throw Error("Asset not found: " + t + (s ? "\n" + s : ""));
    }
    remove(t) {
      t = this.pathPrefix + t;
      let e = this.assets[t];
      return e.dispose && e.dispose(), delete this.assets[t], e;
    }
    removeAll() {
      for (let t in this.assets) {
        let e = this.assets[t];
        e.dispose && e.dispose();
      }
      this.assets = {};
    }
    isLoadingComplete() {
      return 0 == this.toLoad;
    }
    getToLoad() {
      return this.toLoad;
    }
    getLoaded() {
      return this.loaded;
    }
    dispose() {
      this.removeAll();
    }
    hasErrors() {
      return Object.keys(this.errors).length > 0;
    }
    getErrors() {
      return this.errors;
    }
  }
  class Rt {
    constructor() {
      (this.callbacks = {}), (this.rawDataUris = {});
    }
    dataUriToString(t) {
      if (!t.startsWith("data:")) throw new Error("Not a data URI.");
      let e = t.indexOf("base64,");
      return -1 != e
        ? ((e += "base64,".length), atob(t.substr(e)))
        : t.substr(t.indexOf(",") + 1);
    }
    base64ToUint8Array(t) {
      for (
        var e = window.atob(t), s = e.length, i = new Uint8Array(s), r = 0;
        r < s;
        r++
      )
        i[r] = e.charCodeAt(r);
      return i;
    }
    dataUriToUint8Array(t) {
      if (!t.startsWith("data:")) throw new Error("Not a data URI.");
      let e = t.indexOf("base64,");
      if (-1 == e) throw new Error("Not a binary data URI.");
      return (e += "base64,".length), this.base64ToUint8Array(t.substr(e));
    }
    downloadText(t, e, s) {
      if (this.start(t, e, s)) return;
      if (this.rawDataUris[t]) {
        try {
          let e = this.rawDataUris[t];
          this.finish(t, 200, this.dataUriToString(e));
        } catch (e) {
          this.finish(t, 400, JSON.stringify(e));
        }
        return;
      }
      let i = new XMLHttpRequest();
      i.overrideMimeType("text/html"), i.open("GET", t, !0);
      let r = () => {
        this.finish(t, i.status, i.responseText);
      };
      (i.onload = r), (i.onerror = r), i.send();
    }
    downloadJson(t, e, s) {
      this.downloadText(
        t,
        (t) => {
          e(JSON.parse(t));
        },
        s
      );
    }
    downloadBinary(t, e, s) {
      if (this.start(t, e, s)) return;
      if (this.rawDataUris[t]) {
        try {
          let e = this.rawDataUris[t];
          this.finish(t, 200, this.dataUriToUint8Array(e));
        } catch (e) {
          this.finish(t, 400, JSON.stringify(e));
        }
        return;
      }
      let i = new XMLHttpRequest();
      i.open("GET", t, !0), (i.responseType = "arraybuffer");
      let r = () => {
        this.finish(t, i.status, i.response);
      };
      (i.onload = () => {
        200 == i.status || 0 == i.status
          ? this.finish(t, 200, new Uint8Array(i.response))
          : r();
      }),
        (i.onerror = r),
        i.send();
    }
    start(t, e, s) {
      let i = this.callbacks[t];
      try {
        if (i) return !0;
        this.callbacks[t] = i = [];
      } finally {
        i.push(e, s);
      }
    }
    finish(t, e, s) {
      let i = this.callbacks[t];
      delete this.callbacks[t];
      let r = 200 == e || 0 == e ? [s] : [e, s];
      for (let t = r.length - 1, e = i.length; t < e; t += 2)
        i[t].apply(null, r);
    }
  }
  class Yt {
    constructor(t, e) {
      if (
        ((this.bendDirection = 0),
        (this.compress = !1),
        (this.stretch = !1),
        (this.mix = 1),
        (this.softness = 0),
        (this.active = !1),
        !t)
      )
        throw new Error("data cannot be null.");
      if (!e) throw new Error("skeleton cannot be null.");
      (this.data = t),
        (this.mix = t.mix),
        (this.softness = t.softness),
        (this.bendDirection = t.bendDirection),
        (this.compress = t.compress),
        (this.stretch = t.stretch),
        (this.bones = new Array());
      for (let s = 0; s < t.bones.length; s++) {
        let i = e.findBone(t.bones[s].name);
        if (!i) throw new Error(`Couldn't find bone ${t.bones[s].name}`);
        this.bones.push(i);
      }
      let s = e.findBone(t.target.name);
      if (!s) throw new Error(`Couldn't find bone ${t.target.name}`);
      this.target = s;
    }
    isActive() {
      return this.active;
    }
    update() {
      if (0 == this.mix) return;
      let t = this.target,
        e = this.bones;
      switch (e.length) {
        case 1:
          this.apply1(
            e[0],
            t.worldX,
            t.worldY,
            this.compress,
            this.stretch,
            this.data.uniform,
            this.mix
          );
          break;
        case 2:
          this.apply2(
            e[0],
            e[1],
            t.worldX,
            t.worldY,
            this.bendDirection,
            this.stretch,
            this.data.uniform,
            this.softness,
            this.mix
          );
      }
    }
    apply1(t, e, i, r, n, a, h) {
      let o = t.parent;
      if (!o) throw new Error("IK bone must have parent.");
      let l = o.a,
        c = o.b,
        d = o.c,
        u = o.d,
        m = -t.ashearX - t.arotation,
        f = 0,
        g = 0;
      switch (t.data.transformMode) {
        case ht.OnlyTranslation:
          (f = e - t.worldX), (g = i - t.worldY);
          break;
        case ht.NoRotationOrReflection:
          let r = Math.abs(l * u - c * d) / (l * l + d * d),
            n = l / t.skeleton.scaleX,
            a = d / t.skeleton.scaleY;
          (c = -a * r * t.skeleton.scaleX),
            (u = n * r * t.skeleton.scaleY),
            (m += Math.atan2(a, n) * s.radDeg);
        default:
          let h = e - o.worldX,
            p = i - o.worldY,
            x = l * u - c * d;
          (f = (h * u - p * c) / x - t.ax), (g = (p * l - h * d) / x - t.ay);
      }
      (m += Math.atan2(g, f) * s.radDeg),
        t.ascaleX < 0 && (m += 180),
        m > 180 ? (m -= 360) : m < -180 && (m += 360);
      let p = t.ascaleX,
        x = t.ascaleY;
      if (r || n) {
        switch (t.data.transformMode) {
          case ht.NoScale:
          case ht.NoScaleOrReflection:
            (f = e - t.worldX), (g = i - t.worldY);
        }
        let s = t.data.length * p,
          o = Math.sqrt(f * f + g * g);
        if ((r && o < s) || (n && o > s && s > 1e-4)) {
          let t = (o / s - 1) * h + 1;
          (p *= t), a && (x *= t);
        }
      }
      t.updateWorldTransformWith(
        t.ax,
        t.ay,
        t.arotation + m * h,
        p,
        x,
        t.ashearX,
        t.ashearY
      );
    }
    apply2(t, e, i, r, n, a, h, o, l) {
      let c = t.ax,
        d = t.ay,
        u = t.ascaleX,
        m = t.ascaleY,
        f = u,
        g = m,
        p = e.ascaleX,
        x = 0,
        w = 0,
        b = 0;
      u < 0 ? ((u = -u), (x = 180), (b = -1)) : ((x = 0), (b = 1)),
        m < 0 && ((m = -m), (b = -b)),
        p < 0 ? ((p = -p), (w = 180)) : (w = 0);
      let v = e.ax,
        y = 0,
        A = 0,
        C = 0,
        k = t.a,
        S = t.b,
        E = t.c,
        M = t.d,
        T = Math.abs(u - m) <= 1e-4;
      !T || a
        ? ((y = 0), (A = k * v + t.worldX), (C = E * v + t.worldY))
        : ((y = e.ay),
          (A = k * v + S * y + t.worldX),
          (C = E * v + M * y + t.worldY));
      let R = t.parent;
      if (!R) throw new Error("IK parent must itself have a parent.");
      (k = R.a), (S = R.b), (E = R.c), (M = R.d);
      let Y,
        I,
        X = 1 / (k * M - S * E),
        L = A - R.worldX,
        P = C - R.worldY,
        F = (L * M - P * S) * X - c,
        D = (P * k - L * E) * X - d,
        B = Math.sqrt(F * F + D * D),
        V = e.data.length * p;
      if (B < 1e-4)
        return (
          this.apply1(t, i, r, !1, a, !1, l),
          void e.updateWorldTransformWith(
            v,
            y,
            0,
            e.ascaleX,
            e.ascaleY,
            e.ashearX,
            e.ashearY
          )
        );
      (L = i - R.worldX), (P = r - R.worldY);
      let N = (L * M - P * S) * X - c,
        _ = (P * k - L * E) * X - d,
        O = N * N + _ * _;
      if (0 != o) {
        o *= u * (p + 1) * 0.5;
        let t = Math.sqrt(O),
          e = t - B - V * u + o;
        if (e > 0) {
          let s = Math.min(1, e / (2 * o)) - 1;
          (s = (e - o * (1 - s * s)) / t),
            (N -= s * N),
            (_ -= s * _),
            (O = N * N + _ * _);
        }
      }
      t: if (T) {
        V *= u;
        let t = (O - B * B - V * V) / (2 * B * V);
        t < -1
          ? ((t = -1), (I = Math.PI * n))
          : t > 1
          ? ((t = 1),
            (I = 0),
            a &&
              ((k = (Math.sqrt(O) / (B + V) - 1) * l + 1),
              (f *= k),
              h && (g *= k)))
          : (I = Math.acos(t) * n),
          (k = B + V * t),
          (S = V * Math.sin(I)),
          (Y = Math.atan2(_ * k - N * S, N * k + _ * S));
      } else {
        (k = u * V), (S = m * V);
        let t = k * k,
          e = S * S,
          i = Math.atan2(_, N);
        E = e * B * B + t * O - t * e;
        let r = -2 * e * B,
          a = e - t;
        if (((M = r * r - 4 * a * E), M >= 0)) {
          let t = Math.sqrt(M);
          r < 0 && (t = -t), (t = 0.5 * -(r + t));
          let e = t / a,
            s = E / t,
            h = Math.abs(e) < Math.abs(s) ? e : s;
          if (h * h <= O) {
            (P = Math.sqrt(O - h * h) * n),
              (Y = i - Math.atan2(P, h)),
              (I = Math.atan2(P / m, (h - B) / u));
            break t;
          }
        }
        let h = s.PI,
          o = B - k,
          l = o * o,
          c = 0,
          d = 0,
          f = B + k,
          g = f * f,
          p = 0;
        (E = (-k * B) / (t - e)),
          E >= -1 &&
            E <= 1 &&
            ((E = Math.acos(E)),
            (L = k * Math.cos(E) + B),
            (P = S * Math.sin(E)),
            (M = L * L + P * P),
            M < l && ((h = E), (l = M), (o = L), (c = P)),
            M > g && ((d = E), (g = M), (f = L), (p = P))),
          O <= 0.5 * (l + g)
            ? ((Y = i - Math.atan2(c * n, o)), (I = h * n))
            : ((Y = i - Math.atan2(p * n, f)), (I = d * n));
      }
      let W = Math.atan2(y, v) * b,
        q = t.arotation;
      (Y = (Y - W) * s.radDeg + x - q),
        Y > 180 ? (Y -= 360) : Y < -180 && (Y += 360),
        t.updateWorldTransformWith(c, d, q + Y * l, f, g, 0, 0),
        (q = e.arotation),
        (I = ((I + W) * s.radDeg - e.ashearX) * b + w - q),
        I > 180 ? (I -= 360) : I < -180 && (I += 360),
        e.updateWorldTransformWith(
          v,
          y,
          q + I * l,
          e.ascaleX,
          e.ascaleY,
          e.ashearX,
          e.ashearY
        );
    }
  }
  class It {
    constructor(t, e, s) {
      (this.name = t), (this.order = e), (this.skinRequired = s);
    }
  }
  class Xt extends It {
    constructor(t) {
      super(t, 0, !1),
        (this.bones = new Array()),
        (this._target = null),
        (this.positionMode = ot.Fixed),
        (this.spacingMode = lt.Fixed),
        (this.rotateMode = ct.Chain),
        (this.offsetRotation = 0),
        (this.position = 0),
        (this.spacing = 0),
        (this.mixRotate = 0),
        (this.mixX = 0),
        (this.mixY = 0);
    }
    set target(t) {
      this._target = t;
    }
    get target() {
      if (this._target) return this._target;
      throw new Error("SlotData not set.");
    }
  }
  ((ft = ot || (ot = {}))[(ft.Fixed = 0)] = "Fixed"),
    (ft[(ft.Percent = 1)] = "Percent"),
    ((mt = lt || (lt = {}))[(mt.Length = 0)] = "Length"),
    (mt[(mt.Fixed = 1)] = "Fixed"),
    (mt[(mt.Percent = 2)] = "Percent"),
    (mt[(mt.Proportional = 3)] = "Proportional"),
    (function (t) {
      (t[(t.Tangent = 0)] = "Tangent"),
        (t[(t.Chain = 1)] = "Chain"),
        (t[(t.ChainScale = 2)] = "ChainScale");
    })(ct || (ct = {}));
  class Lt {
    constructor(t, e) {
      if (
        ((this.position = 0),
        (this.spacing = 0),
        (this.mixRotate = 0),
        (this.mixX = 0),
        (this.mixY = 0),
        (this.spaces = new Array()),
        (this.positions = new Array()),
        (this.world = new Array()),
        (this.curves = new Array()),
        (this.lengths = new Array()),
        (this.segments = new Array()),
        (this.active = !1),
        !t)
      )
        throw new Error("data cannot be null.");
      if (!e) throw new Error("skeleton cannot be null.");
      (this.data = t), (this.bones = new Array());
      for (let s = 0, i = t.bones.length; s < i; s++) {
        let i = e.findBone(t.bones[s].name);
        if (!i) throw new Error(`Couldn't find bone ${t.bones[s].name}.`);
        this.bones.push(i);
      }
      let s = e.findSlot(t.target.name);
      if (!s) throw new Error(`Couldn't find target bone ${t.target.name}`);
      (this.target = s),
        (this.position = t.position),
        (this.spacing = t.spacing),
        (this.mixRotate = t.mixRotate),
        (this.mixX = t.mixX),
        (this.mixY = t.mixY);
    }
    isActive() {
      return this.active;
    }
    update() {
      let t = this.target.getAttachment();
      if (!(t instanceof At)) return;
      let e = this.mixRotate,
        r = this.mixX,
        n = this.mixY;
      if (0 == e && 0 == r && 0 == n) return;
      let a = this.data,
        h = a.rotateMode == ct.Tangent,
        o = a.rotateMode == ct.ChainScale,
        l = this.bones,
        c = l.length,
        d = h ? c : c + 1,
        u = i.setArraySize(this.spaces, d),
        m = o ? (this.lengths = i.setArraySize(this.lengths, c)) : [],
        f = this.spacing;
      switch (a.spacingMode) {
        case lt.Percent:
          if (o)
            for (let t = 0, e = d - 1; t < e; t++) {
              let e = l[t],
                s = e.data.length;
              if (s < Lt.epsilon) m[t] = 0;
              else {
                let i = s * e.a,
                  r = s * e.c;
                m[t] = Math.sqrt(i * i + r * r);
              }
            }
          i.arrayFill(u, 1, d, f);
          break;
        case lt.Proportional:
          let t = 0;
          for (let e = 0, s = d - 1; e < s; ) {
            let s = l[e],
              i = s.data.length;
            if (i < Lt.epsilon) o && (m[e] = 0), (u[++e] = f);
            else {
              let r = i * s.a,
                n = i * s.c,
                a = Math.sqrt(r * r + n * n);
              o && (m[e] = a), (u[++e] = a), (t += a);
            }
          }
          if (t > 0) {
            t = (d / t) * f;
            for (let e = 1; e < d; e++) u[e] *= t;
          }
          break;
        default:
          let e = a.spacingMode == lt.Length;
          for (let t = 0, s = d - 1; t < s; ) {
            let s = l[t],
              i = s.data.length;
            if (i < Lt.epsilon) o && (m[t] = 0), (u[++t] = f);
            else {
              let r = i * s.a,
                n = i * s.c,
                a = Math.sqrt(r * r + n * n);
              o && (m[t] = a), (u[++t] = ((e ? i + f : f) * a) / i);
            }
          }
      }
      let g = this.computeWorldPositions(t, d, h),
        p = g[0],
        x = g[1],
        w = a.offsetRotation,
        b = !1;
      if (0 == w) b = a.rotateMode == ct.Chain;
      else {
        b = !1;
        let t = this.target.bone;
        w *= t.a * t.d - t.b * t.c > 0 ? s.degRad : -s.degRad;
      }
      for (let t = 0, i = 3; t < c; t++, i += 3) {
        let a = l[t];
        (a.worldX += (p - a.worldX) * r), (a.worldY += (x - a.worldY) * n);
        let c = g[i],
          d = g[i + 1],
          f = c - p,
          v = d - x;
        if (o) {
          let s = m[t];
          if (0 != s) {
            let t = (Math.sqrt(f * f + v * v) / s - 1) * e + 1;
            (a.a *= t), (a.c *= t);
          }
        }
        if (((p = c), (x = d), e > 0)) {
          let r = a.a,
            n = a.b,
            o = a.c,
            l = a.d,
            c = 0,
            d = 0,
            m = 0;
          if (
            ((c = h ? g[i - 1] : 0 == u[t + 1] ? g[i + 2] : Math.atan2(v, f)),
            (c -= Math.atan2(o, r)),
            b)
          ) {
            (d = Math.cos(c)), (m = Math.sin(c));
            let t = a.data.length;
            (p += (t * (d * r - m * o) - f) * e),
              (x += (t * (m * r + d * o) - v) * e);
          } else c += w;
          c > s.PI ? (c -= s.PI2) : c < -s.PI && (c += s.PI2),
            (c *= e),
            (d = Math.cos(c)),
            (m = Math.sin(c)),
            (a.a = d * r - m * o),
            (a.b = d * n - m * l),
            (a.c = m * r + d * o),
            (a.d = m * n + d * l);
        }
        a.updateAppliedTransform();
      }
    }
    computeWorldPositions(t, e, s) {
      let r = this.target,
        n = this.position,
        a = this.spaces,
        h = i.setArraySize(this.positions, 3 * e + 2),
        o = this.world,
        l = t.closed,
        c = t.worldVerticesLength,
        d = c / 6,
        u = Lt.NONE;
      if (!t.constantSpeed) {
        let m = t.lengths;
        d -= l ? 1 : 2;
        let f,
          g = m[d];
        switch (
          (this.data.positionMode == ot.Percent && (n *= g),
          this.data.spacingMode)
        ) {
          case lt.Percent:
            f = g;
            break;
          case lt.Proportional:
            f = g / e;
            break;
          default:
            f = 1;
        }
        o = i.setArraySize(this.world, 8);
        for (let i = 0, p = 0, x = 0; i < e; i++, p += 3) {
          let e = a[i] * f;
          n += e;
          let w = n;
          if (l) (w %= g), w < 0 && (w += g), (x = 0);
          else {
            if (w < 0) {
              u != Lt.BEFORE &&
                ((u = Lt.BEFORE), t.computeWorldVertices(r, 2, 4, o, 0, 2)),
                this.addBeforePosition(w, o, 0, h, p);
              continue;
            }
            if (w > g) {
              u != Lt.AFTER &&
                ((u = Lt.AFTER), t.computeWorldVertices(r, c - 6, 4, o, 0, 2)),
                this.addAfterPosition(w - g, o, 0, h, p);
              continue;
            }
          }
          for (; ; x++) {
            let t = m[x];
            if (!(w > t)) {
              if (0 == x) w /= t;
              else {
                let e = m[x - 1];
                w = (w - e) / (t - e);
              }
              break;
            }
          }
          x != u &&
            ((u = x),
            l && x == d
              ? (t.computeWorldVertices(r, c - 4, 4, o, 0, 2),
                t.computeWorldVertices(r, 0, 4, o, 4, 2))
              : t.computeWorldVertices(r, 6 * x + 2, 8, o, 0, 2)),
            this.addCurvePosition(
              w,
              o[0],
              o[1],
              o[2],
              o[3],
              o[4],
              o[5],
              o[6],
              o[7],
              h,
              p,
              s || (i > 0 && 0 == e)
            );
        }
        return h;
      }
      l
        ? ((c += 2),
          (o = i.setArraySize(this.world, c)),
          t.computeWorldVertices(r, 2, c - 4, o, 0, 2),
          t.computeWorldVertices(r, 0, 2, o, c - 4, 2),
          (o[c - 2] = o[0]),
          (o[c - 1] = o[1]))
        : (d--,
          (c -= 4),
          (o = i.setArraySize(this.world, c)),
          t.computeWorldVertices(r, 2, c, o, 0, 2));
      let m,
        f = i.setArraySize(this.curves, d),
        g = 0,
        p = o[0],
        x = o[1],
        w = 0,
        b = 0,
        v = 0,
        y = 0,
        A = 0,
        C = 0,
        k = 0,
        S = 0,
        E = 0,
        M = 0,
        T = 0,
        R = 0,
        Y = 0,
        I = 0;
      for (let t = 0, e = 2; t < d; t++, e += 6)
        (w = o[e]),
          (b = o[e + 1]),
          (v = o[e + 2]),
          (y = o[e + 3]),
          (A = o[e + 4]),
          (C = o[e + 5]),
          (k = 0.1875 * (p - 2 * w + v)),
          (S = 0.1875 * (x - 2 * b + y)),
          (E = 0.09375 * (3 * (w - v) - p + A)),
          (M = 0.09375 * (3 * (b - y) - x + C)),
          (T = 2 * k + E),
          (R = 2 * S + M),
          (Y = 0.75 * (w - p) + k + 0.16666667 * E),
          (I = 0.75 * (b - x) + S + 0.16666667 * M),
          (g += Math.sqrt(Y * Y + I * I)),
          (Y += T),
          (I += R),
          (T += E),
          (R += M),
          (g += Math.sqrt(Y * Y + I * I)),
          (Y += T),
          (I += R),
          (g += Math.sqrt(Y * Y + I * I)),
          (Y += T + E),
          (I += R + M),
          (g += Math.sqrt(Y * Y + I * I)),
          (f[t] = g),
          (p = A),
          (x = C);
      switch (
        (this.data.positionMode == ot.Percent && (n *= g),
        this.data.spacingMode)
      ) {
        case lt.Percent:
          m = g;
          break;
        case lt.Proportional:
          m = g / e;
          break;
        default:
          m = 1;
      }
      let X = this.segments,
        L = 0;
      for (let t = 0, i = 0, r = 0, d = 0; t < e; t++, i += 3) {
        let e = a[t] * m;
        n += e;
        let P = n;
        if (l) (P %= g), P < 0 && (P += g), (r = 0);
        else {
          if (P < 0) {
            this.addBeforePosition(P, o, 0, h, i);
            continue;
          }
          if (P > g) {
            this.addAfterPosition(P - g, o, c - 4, h, i);
            continue;
          }
        }
        for (; ; r++) {
          let t = f[r];
          if (!(P > t)) {
            if (0 == r) P /= t;
            else {
              let e = f[r - 1];
              P = (P - e) / (t - e);
            }
            break;
          }
        }
        if (r != u) {
          u = r;
          let t = 6 * r;
          for (
            p = o[t],
              x = o[t + 1],
              w = o[t + 2],
              b = o[t + 3],
              v = o[t + 4],
              y = o[t + 5],
              A = o[t + 6],
              C = o[t + 7],
              k = 0.03 * (p - 2 * w + v),
              S = 0.03 * (x - 2 * b + y),
              E = 0.006 * (3 * (w - v) - p + A),
              M = 0.006 * (3 * (b - y) - x + C),
              T = 2 * k + E,
              R = 2 * S + M,
              Y = 0.3 * (w - p) + k + 0.16666667 * E,
              I = 0.3 * (b - x) + S + 0.16666667 * M,
              L = Math.sqrt(Y * Y + I * I),
              X[0] = L,
              t = 1;
            t < 8;
            t++
          )
            (Y += T),
              (I += R),
              (T += E),
              (R += M),
              (L += Math.sqrt(Y * Y + I * I)),
              (X[t] = L);
          (Y += T),
            (I += R),
            (L += Math.sqrt(Y * Y + I * I)),
            (X[8] = L),
            (Y += T + E),
            (I += R + M),
            (L += Math.sqrt(Y * Y + I * I)),
            (X[9] = L),
            (d = 0);
        }
        for (P *= L; ; d++) {
          let t = X[d];
          if (!(P > t)) {
            if (0 == d) P /= t;
            else {
              let e = X[d - 1];
              P = d + (P - e) / (t - e);
            }
            break;
          }
        }
        this.addCurvePosition(
          0.1 * P,
          p,
          x,
          w,
          b,
          v,
          y,
          A,
          C,
          h,
          i,
          s || (t > 0 && 0 == e)
        );
      }
      return h;
    }
    addBeforePosition(t, e, s, i, r) {
      let n = e[s],
        a = e[s + 1],
        h = e[s + 2] - n,
        o = e[s + 3] - a,
        l = Math.atan2(o, h);
      (i[r] = n + t * Math.cos(l)),
        (i[r + 1] = a + t * Math.sin(l)),
        (i[r + 2] = l);
    }
    addAfterPosition(t, e, s, i, r) {
      let n = e[s + 2],
        a = e[s + 3],
        h = n - e[s],
        o = a - e[s + 1],
        l = Math.atan2(o, h);
      (i[r] = n + t * Math.cos(l)),
        (i[r + 1] = a + t * Math.sin(l)),
        (i[r + 2] = l);
    }
    addCurvePosition(t, e, s, i, r, n, a, h, o, l, c, d) {
      if (0 == t || isNaN(t))
        return (
          (l[c] = e), (l[c + 1] = s), void (l[c + 2] = Math.atan2(r - s, i - e))
        );
      let u = t * t,
        m = u * t,
        f = 1 - t,
        g = f * f,
        p = g * f,
        x = f * t,
        w = 3 * x,
        b = f * w,
        v = w * t,
        y = e * p + i * b + n * v + h * m,
        A = s * p + r * b + a * v + o * m;
      (l[c] = y),
        (l[c + 1] = A),
        d &&
          (l[c + 2] =
            t < 0.001
              ? Math.atan2(r - s, i - e)
              : Math.atan2(
                  A - (s * g + r * x * 2 + a * u),
                  y - (e * g + i * x * 2 + n * u)
                ));
    }
  }
  (Lt.NONE = -1), (Lt.BEFORE = -2), (Lt.AFTER = -3), (Lt.epsilon = 1e-5);
  class Pt {
    constructor(t, s) {
      if (
        ((this.darkColor = null),
        (this.attachment = null),
        (this.attachmentState = 0),
        (this.sequenceIndex = -1),
        (this.deform = new Array()),
        !t)
      )
        throw new Error("data cannot be null.");
      if (!s) throw new Error("bone cannot be null.");
      (this.data = t),
        (this.bone = s),
        (this.color = new e()),
        (this.darkColor = t.darkColor ? new e() : null),
        this.setToSetupPose();
    }
    getSkeleton() {
      return this.bone.skeleton;
    }
    getAttachment() {
      return this.attachment;
    }
    setAttachment(t) {
      this.attachment != t &&
        ((t instanceof o &&
          this.attachment instanceof o &&
          t.timelineAttachment == this.attachment.timelineAttachment) ||
          (this.deform.length = 0),
        (this.attachment = t),
        (this.sequenceIndex = -1));
    }
    setToSetupPose() {
      this.color.setFromColor(this.data.color),
        this.darkColor && this.darkColor.setFromColor(this.data.darkColor),
        this.data.attachmentName
          ? ((this.attachment = null),
            this.setAttachment(
              this.bone.skeleton.getAttachment(
                this.data.index,
                this.data.attachmentName
              )
            ))
          : (this.attachment = null);
    }
  }
  class Ft {
    constructor(t, e) {
      if (
        ((this.mixRotate = 0),
        (this.mixX = 0),
        (this.mixY = 0),
        (this.mixScaleX = 0),
        (this.mixScaleY = 0),
        (this.mixShearY = 0),
        (this.temp = new n()),
        (this.active = !1),
        !t)
      )
        throw new Error("data cannot be null.");
      if (!e) throw new Error("skeleton cannot be null.");
      (this.data = t),
        (this.mixRotate = t.mixRotate),
        (this.mixX = t.mixX),
        (this.mixY = t.mixY),
        (this.mixScaleX = t.mixScaleX),
        (this.mixScaleY = t.mixScaleY),
        (this.mixShearY = t.mixShearY),
        (this.bones = new Array());
      for (let s = 0; s < t.bones.length; s++) {
        let i = e.findBone(t.bones[s].name);
        if (!i) throw new Error(`Couldn't find bone ${t.bones[s].name}.`);
        this.bones.push(i);
      }
      let s = e.findBone(t.target.name);
      if (!s) throw new Error(`Couldn't find target bone ${t.target.name}.`);
      this.target = s;
    }
    isActive() {
      return this.active;
    }
    update() {
      (0 == this.mixRotate &&
        0 == this.mixX &&
        0 == this.mixY &&
        0 == this.mixScaleX &&
        0 == this.mixScaleX &&
        0 == this.mixShearY) ||
        (this.data.local
          ? this.data.relative
            ? this.applyRelativeLocal()
            : this.applyAbsoluteLocal()
          : this.data.relative
          ? this.applyRelativeWorld()
          : this.applyAbsoluteWorld());
    }
    applyAbsoluteWorld() {
      let t = this.mixRotate,
        e = this.mixX,
        i = this.mixY,
        r = this.mixScaleX,
        n = this.mixScaleY,
        a = this.mixShearY,
        h = 0 != e || 0 != i,
        o = this.target,
        l = o.a,
        c = o.b,
        d = o.c,
        u = o.d,
        m = l * u - c * d > 0 ? s.degRad : -s.degRad,
        f = this.data.offsetRotation * m,
        g = this.data.offsetShearY * m,
        p = this.bones;
      for (let m = 0, x = p.length; m < x; m++) {
        let x = p[m];
        if (0 != t) {
          let e = x.a,
            i = x.b,
            r = x.c,
            n = x.d,
            a = Math.atan2(d, l) - Math.atan2(r, e) + f;
          a > s.PI ? (a -= s.PI2) : a < -s.PI && (a += s.PI2), (a *= t);
          let h = Math.cos(a),
            o = Math.sin(a);
          (x.a = h * e - o * r),
            (x.b = h * i - o * n),
            (x.c = o * e + h * r),
            (x.d = o * i + h * n);
        }
        if (h) {
          let t = this.temp;
          o.localToWorld(t.set(this.data.offsetX, this.data.offsetY)),
            (x.worldX += (t.x - x.worldX) * e),
            (x.worldY += (t.y - x.worldY) * i);
        }
        if (0 != r) {
          let t = Math.sqrt(x.a * x.a + x.c * x.c);
          0 != t &&
            (t =
              (t +
                (Math.sqrt(l * l + d * d) - t + this.data.offsetScaleX) * r) /
              t),
            (x.a *= t),
            (x.c *= t);
        }
        if (0 != n) {
          let t = Math.sqrt(x.b * x.b + x.d * x.d);
          0 != t &&
            (t =
              (t +
                (Math.sqrt(c * c + u * u) - t + this.data.offsetScaleY) * n) /
              t),
            (x.b *= t),
            (x.d *= t);
        }
        if (a > 0) {
          let t = x.b,
            e = x.d,
            i = Math.atan2(e, t),
            r =
              Math.atan2(u, c) - Math.atan2(d, l) - (i - Math.atan2(x.c, x.a));
          r > s.PI ? (r -= s.PI2) : r < -s.PI && (r += s.PI2),
            (r = i + (r + g) * a);
          let n = Math.sqrt(t * t + e * e);
          (x.b = Math.cos(r) * n), (x.d = Math.sin(r) * n);
        }
        x.updateAppliedTransform();
      }
    }
    applyRelativeWorld() {
      let t = this.mixRotate,
        e = this.mixX,
        i = this.mixY,
        r = this.mixScaleX,
        n = this.mixScaleY,
        a = this.mixShearY,
        h = 0 != e || 0 != i,
        o = this.target,
        l = o.a,
        c = o.b,
        d = o.c,
        u = o.d,
        m = l * u - c * d > 0 ? s.degRad : -s.degRad,
        f = this.data.offsetRotation * m,
        g = this.data.offsetShearY * m,
        p = this.bones;
      for (let m = 0, x = p.length; m < x; m++) {
        let x = p[m];
        if (0 != t) {
          let e = x.a,
            i = x.b,
            r = x.c,
            n = x.d,
            a = Math.atan2(d, l) + f;
          a > s.PI ? (a -= s.PI2) : a < -s.PI && (a += s.PI2), (a *= t);
          let h = Math.cos(a),
            o = Math.sin(a);
          (x.a = h * e - o * r),
            (x.b = h * i - o * n),
            (x.c = o * e + h * r),
            (x.d = o * i + h * n);
        }
        if (h) {
          let t = this.temp;
          o.localToWorld(t.set(this.data.offsetX, this.data.offsetY)),
            (x.worldX += t.x * e),
            (x.worldY += t.y * i);
        }
        if (0 != r) {
          let t =
            (Math.sqrt(l * l + d * d) - 1 + this.data.offsetScaleX) * r + 1;
          (x.a *= t), (x.c *= t);
        }
        if (0 != n) {
          let t =
            (Math.sqrt(c * c + u * u) - 1 + this.data.offsetScaleY) * n + 1;
          (x.b *= t), (x.d *= t);
        }
        if (a > 0) {
          let t = Math.atan2(u, c) - Math.atan2(d, l);
          t > s.PI ? (t -= s.PI2) : t < -s.PI && (t += s.PI2);
          let e = x.b,
            i = x.d;
          t = Math.atan2(i, e) + (t - s.PI / 2 + g) * a;
          let r = Math.sqrt(e * e + i * i);
          (x.b = Math.cos(t) * r), (x.d = Math.sin(t) * r);
        }
        x.updateAppliedTransform();
      }
    }
    applyAbsoluteLocal() {
      let t = this.mixRotate,
        e = this.mixX,
        s = this.mixY,
        i = this.mixScaleX,
        r = this.mixScaleY,
        n = this.mixShearY,
        a = this.target,
        h = this.bones;
      for (let o = 0, l = h.length; o < l; o++) {
        let l = h[o],
          c = l.arotation;
        if (0 != t) {
          let e = a.arotation - c + this.data.offsetRotation;
          (e -= 360 * (16384 - ((16384.499999999996 - e / 360) | 0))),
            (c += e * t);
        }
        let d = l.ax,
          u = l.ay;
        (d += (a.ax - d + this.data.offsetX) * e),
          (u += (a.ay - u + this.data.offsetY) * s);
        let m = l.ascaleX,
          f = l.ascaleY;
        0 != i &&
          0 != m &&
          (m = (m + (a.ascaleX - m + this.data.offsetScaleX) * i) / m),
          0 != r &&
            0 != f &&
            (f = (f + (a.ascaleY - f + this.data.offsetScaleY) * r) / f);
        let g = l.ashearY;
        if (0 != n) {
          let t = a.ashearY - g + this.data.offsetShearY;
          (t -= 360 * (16384 - ((16384.499999999996 - t / 360) | 0))),
            (g += t * n);
        }
        l.updateWorldTransformWith(d, u, c, m, f, l.ashearX, g);
      }
    }
    applyRelativeLocal() {
      let t = this.mixRotate,
        e = this.mixX,
        s = this.mixY,
        i = this.mixScaleX,
        r = this.mixScaleY,
        n = this.mixShearY,
        a = this.target,
        h = this.bones;
      for (let o = 0, l = h.length; o < l; o++) {
        let l = h[o],
          c = l.arotation + (a.arotation + this.data.offsetRotation) * t,
          d = l.ax + (a.ax + this.data.offsetX) * e,
          u = l.ay + (a.ay + this.data.offsetY) * s,
          m = l.ascaleX * ((a.ascaleX - 1 + this.data.offsetScaleX) * i + 1),
          f = l.ascaleY * ((a.ascaleY - 1 + this.data.offsetScaleY) * r + 1),
          g = l.ashearY + (a.ashearY + this.data.offsetShearY) * n;
        l.updateWorldTransformWith(d, u, c, m, f, l.ashearX, g);
      }
    }
  }
  class Dt {
    constructor(t) {
      if (
        ((this._updateCache = new Array()),
        (this.skin = null),
        (this.scaleX = 1),
        (this.scaleY = 1),
        (this.x = 0),
        (this.y = 0),
        !t)
      )
        throw new Error("data cannot be null.");
      (this.data = t), (this.bones = new Array());
      for (let e = 0; e < t.bones.length; e++) {
        let s,
          i = t.bones[e];
        if (i.parent) {
          let t = this.bones[i.parent.index];
          (s = new Mt(i, this, t)), t.children.push(s);
        } else s = new Mt(i, this, null);
        this.bones.push(s);
      }
      (this.slots = new Array()), (this.drawOrder = new Array());
      for (let e = 0; e < t.slots.length; e++) {
        let s = t.slots[e],
          i = this.bones[s.boneData.index],
          r = new Pt(s, i);
        this.slots.push(r), this.drawOrder.push(r);
      }
      this.ikConstraints = new Array();
      for (let e = 0; e < t.ikConstraints.length; e++) {
        let s = t.ikConstraints[e];
        this.ikConstraints.push(new Yt(s, this));
      }
      this.transformConstraints = new Array();
      for (let e = 0; e < t.transformConstraints.length; e++) {
        let s = t.transformConstraints[e];
        this.transformConstraints.push(new Ft(s, this));
      }
      this.pathConstraints = new Array();
      for (let e = 0; e < t.pathConstraints.length; e++) {
        let s = t.pathConstraints[e];
        this.pathConstraints.push(new Lt(s, this));
      }
      (this.color = new e(1, 1, 1, 1)), this.updateCache();
    }
    updateCache() {
      this._updateCache.length = 0;
      let t = this.bones;
      for (let e = 0, s = t.length; e < s; e++) {
        let s = t[e];
        (s.sorted = s.data.skinRequired), (s.active = !s.sorted);
      }
      if (this.skin) {
        let t = this.skin.bones;
        for (let e = 0, s = this.skin.bones.length; e < s; e++) {
          let s = this.bones[t[e].index];
          do {
            (s.sorted = !1), (s.active = !0), (s = s.parent);
          } while (s);
        }
      }
      let e = this.ikConstraints,
        s = this.transformConstraints,
        i = this.pathConstraints,
        r = e.length,
        n = s.length,
        a = i.length,
        h = r + n + a;
      t: for (let t = 0; t < h; t++) {
        for (let s = 0; s < r; s++) {
          let i = e[s];
          if (i.data.order == t) {
            this.sortIkConstraint(i);
            continue t;
          }
        }
        for (let e = 0; e < n; e++) {
          let i = s[e];
          if (i.data.order == t) {
            this.sortTransformConstraint(i);
            continue t;
          }
        }
        for (let e = 0; e < a; e++) {
          let s = i[e];
          if (s.data.order == t) {
            this.sortPathConstraint(s);
            continue t;
          }
        }
      }
      for (let e = 0, s = t.length; e < s; e++) this.sortBone(t[e]);
    }
    sortIkConstraint(t) {
      if (
        ((t.active =
          t.target.isActive() &&
          (!t.data.skinRequired ||
            (this.skin && i.contains(this.skin.constraints, t.data, !0)))),
        !t.active)
      )
        return;
      let e = t.target;
      this.sortBone(e);
      let s = t.bones,
        r = s[0];
      if ((this.sortBone(r), 1 == s.length))
        this._updateCache.push(t), this.sortReset(r.children);
      else {
        let e = s[s.length - 1];
        this.sortBone(e),
          this._updateCache.push(t),
          this.sortReset(r.children),
          (e.sorted = !0);
      }
    }
    sortPathConstraint(t) {
      if (
        ((t.active =
          t.target.bone.isActive() &&
          (!t.data.skinRequired ||
            (this.skin && i.contains(this.skin.constraints, t.data, !0)))),
        !t.active)
      )
        return;
      let e = t.target,
        s = e.data.index,
        r = e.bone;
      this.skin && this.sortPathConstraintAttachment(this.skin, s, r),
        this.data.defaultSkin &&
          this.data.defaultSkin != this.skin &&
          this.sortPathConstraintAttachment(this.data.defaultSkin, s, r);
      for (let t = 0, e = this.data.skins.length; t < e; t++)
        this.sortPathConstraintAttachment(this.data.skins[t], s, r);
      let n = e.getAttachment();
      n instanceof At && this.sortPathConstraintAttachmentWith(n, r);
      let a = t.bones,
        h = a.length;
      for (let t = 0; t < h; t++) this.sortBone(a[t]);
      this._updateCache.push(t);
      for (let t = 0; t < h; t++) this.sortReset(a[t].children);
      for (let t = 0; t < h; t++) a[t].sorted = !0;
    }
    sortTransformConstraint(t) {
      if (
        ((t.active =
          t.target.isActive() &&
          (!t.data.skinRequired ||
            (this.skin && i.contains(this.skin.constraints, t.data, !0)))),
        !t.active)
      )
        return;
      this.sortBone(t.target);
      let e = t.bones,
        s = e.length;
      if (t.data.local)
        for (let t = 0; t < s; t++) {
          let s = e[t];
          this.sortBone(s.parent), this.sortBone(s);
        }
      else for (let t = 0; t < s; t++) this.sortBone(e[t]);
      this._updateCache.push(t);
      for (let t = 0; t < s; t++) this.sortReset(e[t].children);
      for (let t = 0; t < s; t++) e[t].sorted = !0;
    }
    sortPathConstraintAttachment(t, e, s) {
      let i = t.attachments[e];
      if (i) for (let t in i) this.sortPathConstraintAttachmentWith(i[t], s);
    }
    sortPathConstraintAttachmentWith(t, e) {
      if (!(t instanceof At)) return;
      let s = t.bones;
      if (s) {
        let t = this.bones;
        for (let e = 0, i = s.length; e < i; ) {
          let i = s[e++];
          for (i += e; e < i; ) this.sortBone(t[s[e++]]);
        }
      } else this.sortBone(e);
    }
    sortBone(t) {
      if (!t) return;
      if (t.sorted) return;
      let e = t.parent;
      e && this.sortBone(e), (t.sorted = !0), this._updateCache.push(t);
    }
    sortReset(t) {
      for (let e = 0, s = t.length; e < s; e++) {
        let s = t[e];
        s.active && (s.sorted && this.sortReset(s.children), (s.sorted = !1));
      }
    }
    updateWorldTransform() {
      let t = this.bones;
      for (let e = 0, s = t.length; e < s; e++) {
        let s = t[e];
        (s.ax = s.x),
          (s.ay = s.y),
          (s.arotation = s.rotation),
          (s.ascaleX = s.scaleX),
          (s.ascaleY = s.scaleY),
          (s.ashearX = s.shearX),
          (s.ashearY = s.shearY);
      }
      let e = this._updateCache;
      for (let t = 0, s = e.length; t < s; t++) e[t].update();
    }
    updateWorldTransformWith(t) {
      let e = this.getRootBone();
      if (!e) throw new Error("Root bone must not be null.");
      let i = t.a,
        r = t.b,
        n = t.c,
        a = t.d;
      (e.worldX = i * this.x + r * this.y + t.worldX),
        (e.worldY = n * this.x + a * this.y + t.worldY);
      let h = e.rotation + 90 + e.shearY,
        o = s.cosDeg(e.rotation + e.shearX) * e.scaleX,
        l = s.cosDeg(h) * e.scaleY,
        c = s.sinDeg(e.rotation + e.shearX) * e.scaleX,
        d = s.sinDeg(h) * e.scaleY;
      (e.a = (i * o + r * c) * this.scaleX),
        (e.b = (i * l + r * d) * this.scaleX),
        (e.c = (n * o + a * c) * this.scaleY),
        (e.d = (n * l + a * d) * this.scaleY);
      let u = this._updateCache;
      for (let t = 0, s = u.length; t < s; t++) {
        let s = u[t];
        s != e && s.update();
      }
    }
    setToSetupPose() {
      this.setBonesToSetupPose(), this.setSlotsToSetupPose();
    }
    setBonesToSetupPose() {
      let t = this.bones;
      for (let e = 0, s = t.length; e < s; e++) t[e].setToSetupPose();
      let e = this.ikConstraints;
      for (let t = 0, s = e.length; t < s; t++) {
        let s = e[t];
        (s.mix = s.data.mix),
          (s.softness = s.data.softness),
          (s.bendDirection = s.data.bendDirection),
          (s.compress = s.data.compress),
          (s.stretch = s.data.stretch);
      }
      let s = this.transformConstraints;
      for (let t = 0, e = s.length; t < e; t++) {
        let e = s[t],
          i = e.data;
        (e.mixRotate = i.mixRotate),
          (e.mixX = i.mixX),
          (e.mixY = i.mixY),
          (e.mixScaleX = i.mixScaleX),
          (e.mixScaleY = i.mixScaleY),
          (e.mixShearY = i.mixShearY);
      }
      let i = this.pathConstraints;
      for (let t = 0, e = i.length; t < e; t++) {
        let e = i[t],
          s = e.data;
        (e.position = s.position),
          (e.spacing = s.spacing),
          (e.mixRotate = s.mixRotate),
          (e.mixX = s.mixX),
          (e.mixY = s.mixY);
      }
    }
    setSlotsToSetupPose() {
      let t = this.slots;
      i.arrayCopy(t, 0, this.drawOrder, 0, t.length);
      for (let e = 0, s = t.length; e < s; e++) t[e].setToSetupPose();
    }
    getRootBone() {
      return 0 == this.bones.length ? null : this.bones[0];
    }
    findBone(t) {
      if (!t) throw new Error("boneName cannot be null.");
      let e = this.bones;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.data.name == t) return i;
      }
      return null;
    }
    findSlot(t) {
      if (!t) throw new Error("slotName cannot be null.");
      let e = this.slots;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.data.name == t) return i;
      }
      return null;
    }
    setSkinByName(t) {
      let e = this.data.findSkin(t);
      if (!e) throw new Error("Skin not found: " + t);
      this.setSkin(e);
    }
    setSkin(t) {
      if (t != this.skin) {
        if (t)
          if (this.skin) t.attachAll(this, this.skin);
          else {
            let e = this.slots;
            for (let s = 0, i = e.length; s < i; s++) {
              let i = e[s],
                r = i.data.attachmentName;
              if (r) {
                let e = t.getAttachment(s, r);
                e && i.setAttachment(e);
              }
            }
          }
        (this.skin = t), this.updateCache();
      }
    }
    getAttachmentByName(t, e) {
      let s = this.data.findSlot(t);
      if (!s) throw new Error(`Can't find slot with name ${t}`);
      return this.getAttachment(s.index, e);
    }
    getAttachment(t, e) {
      if (!e) throw new Error("attachmentName cannot be null.");
      if (this.skin) {
        let s = this.skin.getAttachment(t, e);
        if (s) return s;
      }
      return this.data.defaultSkin
        ? this.data.defaultSkin.getAttachment(t, e)
        : null;
    }
    setAttachment(t, e) {
      if (!t) throw new Error("slotName cannot be null.");
      let s = this.slots;
      for (let i = 0, r = s.length; i < r; i++) {
        let r = s[i];
        if (r.data.name == t) {
          let s = null;
          if (e && ((s = this.getAttachment(i, e)), !s))
            throw new Error("Attachment not found: " + e + ", for slot: " + t);
          return void r.setAttachment(s);
        }
      }
      throw new Error("Slot not found: " + t);
    }
    findIkConstraint(t) {
      if (!t) throw new Error("constraintName cannot be null.");
      let e = this.ikConstraints;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.data.name == t) return i;
      }
      return null;
    }
    findTransformConstraint(t) {
      if (!t) throw new Error("constraintName cannot be null.");
      let e = this.transformConstraints;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.data.name == t) return i;
      }
      return null;
    }
    findPathConstraint(t) {
      if (!t) throw new Error("constraintName cannot be null.");
      let e = this.pathConstraints;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.data.name == t) return i;
      }
      return null;
    }
    getBoundsRect() {
      let t = new n(),
        e = new n();
      return this.getBounds(t, e), { x: t.x, y: t.y, width: e.x, height: e.y };
    }
    getBounds(t, e, s = new Array(2)) {
      if (!t) throw new Error("offset cannot be null.");
      if (!e) throw new Error("size cannot be null.");
      let r = this.drawOrder,
        n = Number.POSITIVE_INFINITY,
        a = Number.POSITIVE_INFINITY,
        h = Number.NEGATIVE_INFINITY,
        o = Number.NEGATIVE_INFINITY;
      for (let t = 0, e = r.length; t < e; t++) {
        let e = r[t];
        if (!e.bone.active) continue;
        let l = 0,
          c = null,
          d = e.getAttachment();
        if (d instanceof kt)
          (l = 8),
            (c = i.setArraySize(s, l, 0)),
            d.computeWorldVertices(e, c, 0, 2);
        else if (d instanceof yt) {
          let t = d;
          (l = t.worldVerticesLength),
            (c = i.setArraySize(s, l, 0)),
            t.computeWorldVertices(e, 0, l, c, 0, 2);
        }
        if (c)
          for (let t = 0, e = c.length; t < e; t += 2) {
            let e = c[t],
              s = c[t + 1];
            (n = Math.min(n, e)),
              (a = Math.min(a, s)),
              (h = Math.max(h, e)),
              (o = Math.max(o, s));
          }
      }
      t.set(n, a), e.set(h - n, o - a);
    }
  }
  class Bt {
    constructor(t = 0, e, s) {
      (this.slotIndex = t), (this.name = e), (this.attachment = s);
    }
  }
  class Vt {
    constructor(t) {
      if (
        ((this.attachments = new Array()),
        (this.bones = Array()),
        (this.constraints = new Array()),
        !t)
      )
        throw new Error("name cannot be null.");
      this.name = t;
    }
    setAttachment(t, e, s) {
      if (!s) throw new Error("attachment cannot be null.");
      let i = this.attachments;
      t >= i.length && (i.length = t + 1), i[t] || (i[t] = {}), (i[t][e] = s);
    }
    addSkin(t) {
      for (let e = 0; e < t.bones.length; e++) {
        let s = t.bones[e],
          i = !1;
        for (let t = 0; t < this.bones.length; t++)
          if (this.bones[t] == s) {
            i = !0;
            break;
          }
        i || this.bones.push(s);
      }
      for (let e = 0; e < t.constraints.length; e++) {
        let s = t.constraints[e],
          i = !1;
        for (let t = 0; t < this.constraints.length; t++)
          if (this.constraints[t] == s) {
            i = !0;
            break;
          }
        i || this.constraints.push(s);
      }
      let e = t.getAttachments();
      for (let t = 0; t < e.length; t++) {
        var s = e[t];
        this.setAttachment(s.slotIndex, s.name, s.attachment);
      }
    }
    copySkin(t) {
      for (let e = 0; e < t.bones.length; e++) {
        let s = t.bones[e],
          i = !1;
        for (let t = 0; t < this.bones.length; t++)
          if (this.bones[t] == s) {
            i = !0;
            break;
          }
        i || this.bones.push(s);
      }
      for (let e = 0; e < t.constraints.length; e++) {
        let s = t.constraints[e],
          i = !1;
        for (let t = 0; t < this.constraints.length; t++)
          if (this.constraints[t] == s) {
            i = !0;
            break;
          }
        i || this.constraints.push(s);
      }
      let e = t.getAttachments();
      for (let t = 0; t < e.length; t++) {
        var s = e[t];
        s.attachment &&
          (s.attachment instanceof yt
            ? ((s.attachment = s.attachment.newLinkedMesh()),
              this.setAttachment(s.slotIndex, s.name, s.attachment))
            : ((s.attachment = s.attachment.copy()),
              this.setAttachment(s.slotIndex, s.name, s.attachment)));
      }
    }
    getAttachment(t, e) {
      let s = this.attachments[t];
      return s ? s[e] : null;
    }
    removeAttachment(t, e) {
      let s = this.attachments[t];
      s && delete s[e];
    }
    getAttachments() {
      let t = new Array();
      for (var e = 0; e < this.attachments.length; e++) {
        let s = this.attachments[e];
        if (s)
          for (let i in s) {
            let r = s[i];
            r && t.push(new Bt(e, i, r));
          }
      }
      return t;
    }
    getAttachmentsForSlot(t, e) {
      let s = this.attachments[t];
      if (s)
        for (let i in s) {
          let r = s[i];
          r && e.push(new Bt(t, i, r));
        }
    }
    clear() {
      (this.attachments.length = 0),
        (this.bones.length = 0),
        (this.constraints.length = 0);
    }
    attachAll(t, e) {
      let s = 0;
      for (let i = 0; i < t.slots.length; i++) {
        let r = t.slots[i],
          n = r.getAttachment();
        if (n && s < e.attachments.length) {
          let t = e.attachments[s];
          for (let e in t)
            if (n == t[e]) {
              let t = this.getAttachment(s, e);
              t && r.setAttachment(t);
              break;
            }
        }
        s++;
      }
    }
  }
  class Nt {
    constructor(t, s, i) {
      if (
        ((this.index = 0),
        (this.color = new e(1, 1, 1, 1)),
        (this.darkColor = null),
        (this.attachmentName = null),
        (this.blendMode = dt.Normal),
        t < 0)
      )
        throw new Error("index must be >= 0.");
      if (!s) throw new Error("name cannot be null.");
      if (!i) throw new Error("boneData cannot be null.");
      (this.index = t), (this.name = s), (this.boneData = i);
    }
  }
  !(function (t) {
    (t[(t.Normal = 0)] = "Normal"),
      (t[(t.Additive = 1)] = "Additive"),
      (t[(t.Multiply = 2)] = "Multiply"),
      (t[(t.Screen = 3)] = "Screen");
  })(dt || (dt = {})),
    (function (t) {
      (t[(t.Region = 0)] = "Region"),
        (t[(t.BoundingBox = 1)] = "BoundingBox"),
        (t[(t.Mesh = 2)] = "Mesh"),
        (t[(t.LinkedMesh = 3)] = "LinkedMesh"),
        (t[(t.Path = 4)] = "Path"),
        (t[(t.Point = 5)] = "Point"),
        (t[(t.Clipping = 6)] = "Clipping");
    })(ut || (ut = {}));
  class _t {
    constructor() {
      (this.minX = 0),
        (this.minY = 0),
        (this.maxX = 0),
        (this.maxY = 0),
        (this.boundingBoxes = new Array()),
        (this.polygons = new Array()),
        (this.polygonPool = new r(() => i.newFloatArray(16)));
    }
    update(t, e) {
      if (!t) throw new Error("skeleton cannot be null.");
      let s = this.boundingBoxes,
        r = this.polygons,
        n = this.polygonPool,
        a = t.slots,
        h = a.length;
      (s.length = 0), n.freeAll(r), (r.length = 0);
      for (let t = 0; t < h; t++) {
        let e = a[t];
        if (!e.bone.active) continue;
        let h = e.getAttachment();
        if (h instanceof st) {
          let t = h;
          s.push(t);
          let a = n.obtain();
          a.length != t.worldVerticesLength &&
            (a = i.newFloatArray(t.worldVerticesLength)),
            r.push(a),
            t.computeWorldVertices(e, 0, t.worldVerticesLength, a, 0, 2);
        }
      }
      e
        ? this.aabbCompute()
        : ((this.minX = Number.POSITIVE_INFINITY),
          (this.minY = Number.POSITIVE_INFINITY),
          (this.maxX = Number.NEGATIVE_INFINITY),
          (this.maxY = Number.NEGATIVE_INFINITY));
    }
    aabbCompute() {
      let t = Number.POSITIVE_INFINITY,
        e = Number.POSITIVE_INFINITY,
        s = Number.NEGATIVE_INFINITY,
        i = Number.NEGATIVE_INFINITY,
        r = this.polygons;
      for (let n = 0, a = r.length; n < a; n++) {
        let a = r[n],
          h = a;
        for (let r = 0, n = a.length; r < n; r += 2) {
          let n = h[r],
            a = h[r + 1];
          (t = Math.min(t, n)),
            (e = Math.min(e, a)),
            (s = Math.max(s, n)),
            (i = Math.max(i, a));
        }
      }
      (this.minX = t), (this.minY = e), (this.maxX = s), (this.maxY = i);
    }
    aabbContainsPoint(t, e) {
      return (
        t >= this.minX && t <= this.maxX && e >= this.minY && e <= this.maxY
      );
    }
    aabbIntersectsSegment(t, e, s, i) {
      let r = this.minX,
        n = this.minY,
        a = this.maxX,
        h = this.maxY;
      if (
        (t <= r && s <= r) ||
        (e <= n && i <= n) ||
        (t >= a && s >= a) ||
        (e >= h && i >= h)
      )
        return !1;
      let o = (i - e) / (s - t),
        l = o * (r - t) + e;
      if (l > n && l < h) return !0;
      if (((l = o * (a - t) + e), l > n && l < h)) return !0;
      let c = (n - e) / o + t;
      return (c > r && c < a) || ((c = (h - e) / o + t), c > r && c < a);
    }
    aabbIntersectsSkeleton(t) {
      return (
        this.minX < t.maxX &&
        this.maxX > t.minX &&
        this.minY < t.maxY &&
        this.maxY > t.minY
      );
    }
    containsPoint(t, e) {
      let s = this.polygons;
      for (let i = 0, r = s.length; i < r; i++)
        if (this.containsPointPolygon(s[i], t, e)) return this.boundingBoxes[i];
      return null;
    }
    containsPointPolygon(t, e, s) {
      let i = t,
        r = t.length,
        n = r - 2,
        a = !1;
      for (let t = 0; t < r; t += 2) {
        let r = i[t + 1],
          h = i[n + 1];
        if ((r < s && h >= s) || (h < s && r >= s)) {
          let o = i[t];
          o + ((s - r) / (h - r)) * (i[n] - o) < e && (a = !a);
        }
        n = t;
      }
      return a;
    }
    intersectsSegment(t, e, s, i) {
      let r = this.polygons;
      for (let n = 0, a = r.length; n < a; n++)
        if (this.intersectsSegmentPolygon(r[n], t, e, s, i))
          return this.boundingBoxes[n];
      return null;
    }
    intersectsSegmentPolygon(t, e, s, i, r) {
      let n = t,
        a = t.length,
        h = e - i,
        o = s - r,
        l = e * r - s * i,
        c = n[a - 2],
        d = n[a - 1];
      for (let t = 0; t < a; t += 2) {
        let a = n[t],
          u = n[t + 1],
          m = c * u - d * a,
          f = c - a,
          g = d - u,
          p = h * g - o * f,
          x = (l * f - h * m) / p;
        if (
          ((x >= c && x <= a) || (x >= a && x <= c)) &&
          ((x >= e && x <= i) || (x >= i && x <= e))
        ) {
          let t = (l * g - o * m) / p;
          if (
            ((t >= d && t <= u) || (t >= u && t <= d)) &&
            ((t >= s && t <= r) || (t >= r && t <= s))
          )
            return !0;
        }
        (c = a), (d = u);
      }
      return !1;
    }
    getPolygon(t) {
      if (!t) throw new Error("boundingBox cannot be null.");
      let e = this.boundingBoxes.indexOf(t);
      return -1 == e ? null : this.polygons[e];
    }
    getWidth() {
      return this.maxX - this.minX;
    }
    getHeight() {
      return this.maxY - this.minY;
    }
  }
  class Ot {
    constructor() {
      (this.convexPolygons = new Array()),
        (this.convexPolygonsIndices = new Array()),
        (this.indicesArray = new Array()),
        (this.isConcaveArray = new Array()),
        (this.triangles = new Array()),
        (this.polygonPool = new r(() => new Array())),
        (this.polygonIndicesPool = new r(() => new Array()));
    }
    triangulate(t) {
      let e = t,
        s = t.length >> 1,
        i = this.indicesArray;
      i.length = 0;
      for (let t = 0; t < s; t++) i[t] = t;
      let r = this.isConcaveArray;
      r.length = 0;
      for (let t = 0, n = s; t < n; ++t) r[t] = Ot.isConcave(t, s, e, i);
      let n = this.triangles;
      for (n.length = 0; s > 3; ) {
        let t = s - 1,
          a = 0,
          h = 1;
        for (;;) {
          t: if (!r[a]) {
            let n = i[t] << 1,
              o = i[a] << 1,
              l = i[h] << 1,
              c = e[n],
              d = e[n + 1],
              u = e[o],
              m = e[o + 1],
              f = e[l],
              g = e[l + 1];
            for (let n = (h + 1) % s; n != t; n = (n + 1) % s) {
              if (!r[n]) continue;
              let t = i[n] << 1,
                s = e[t],
                a = e[t + 1];
              if (
                Ot.positiveArea(f, g, c, d, s, a) &&
                Ot.positiveArea(c, d, u, m, s, a) &&
                Ot.positiveArea(u, m, f, g, s, a)
              )
                break t;
            }
            break;
          }
          if (0 == h) {
            do {
              if (!r[a]) break;
              a--;
            } while (a > 0);
            break;
          }
          (t = a), (a = h), (h = (h + 1) % s);
        }
        n.push(i[(s + a - 1) % s]),
          n.push(i[a]),
          n.push(i[(a + 1) % s]),
          i.splice(a, 1),
          r.splice(a, 1),
          s--;
        let o = (s + a - 1) % s,
          l = a == s ? 0 : a;
        (r[o] = Ot.isConcave(o, s, e, i)), (r[l] = Ot.isConcave(l, s, e, i));
      }
      return 3 == s && (n.push(i[2]), n.push(i[0]), n.push(i[1])), n;
    }
    decompose(t, e) {
      let s = t,
        i = this.convexPolygons;
      this.polygonPool.freeAll(i), (i.length = 0);
      let r = this.convexPolygonsIndices;
      this.polygonIndicesPool.freeAll(r), (r.length = 0);
      let n = this.polygonIndicesPool.obtain();
      n.length = 0;
      let a = this.polygonPool.obtain();
      a.length = 0;
      let h = -1,
        o = 0;
      for (let t = 0, l = e.length; t < l; t += 3) {
        let l = e[t] << 1,
          c = e[t + 1] << 1,
          d = e[t + 2] << 1,
          u = s[l],
          m = s[l + 1],
          f = s[c],
          g = s[c + 1],
          p = s[d],
          x = s[d + 1],
          w = !1;
        if (h == l) {
          let t = a.length - 4,
            e = Ot.winding(a[t], a[t + 1], a[t + 2], a[t + 3], p, x),
            s = Ot.winding(p, x, a[0], a[1], a[2], a[3]);
          e == o && s == o && (a.push(p), a.push(x), n.push(d), (w = !0));
        }
        w ||
          (a.length > 0
            ? (i.push(a), r.push(n))
            : (this.polygonPool.free(a), this.polygonIndicesPool.free(n)),
          (a = this.polygonPool.obtain()),
          (a.length = 0),
          a.push(u),
          a.push(m),
          a.push(f),
          a.push(g),
          a.push(p),
          a.push(x),
          (n = this.polygonIndicesPool.obtain()),
          (n.length = 0),
          n.push(l),
          n.push(c),
          n.push(d),
          (o = Ot.winding(u, m, f, g, p, x)),
          (h = l));
      }
      a.length > 0 && (i.push(a), r.push(n));
      for (let t = 0, e = i.length; t < e; t++) {
        if (((n = r[t]), 0 == n.length)) continue;
        let s = n[0],
          h = n[n.length - 1];
        a = i[t];
        let o = a.length - 4,
          l = a[o],
          c = a[o + 1],
          d = a[o + 2],
          u = a[o + 3],
          m = a[0],
          f = a[1],
          g = a[2],
          p = a[3],
          x = Ot.winding(l, c, d, u, m, f);
        for (let o = 0; o < e; o++) {
          if (o == t) continue;
          let e = r[o];
          if (3 != e.length) continue;
          let w = e[0],
            b = e[1],
            v = e[2],
            y = i[o],
            A = y[y.length - 2],
            C = y[y.length - 1];
          if (w != s || b != h) continue;
          let k = Ot.winding(l, c, d, u, A, C),
            S = Ot.winding(A, C, m, f, g, p);
          k == x &&
            S == x &&
            ((y.length = 0),
            (e.length = 0),
            a.push(A),
            a.push(C),
            n.push(v),
            (l = d),
            (c = u),
            (d = A),
            (u = C),
            (o = 0));
        }
      }
      for (let t = i.length - 1; t >= 0; t--)
        (a = i[t]),
          0 == a.length &&
            (i.splice(t, 1),
            this.polygonPool.free(a),
            (n = r[t]),
            r.splice(t, 1),
            this.polygonIndicesPool.free(n));
      return i;
    }
    static isConcave(t, e, s, i) {
      let r = i[(e + t - 1) % e] << 1,
        n = i[t] << 1,
        a = i[(t + 1) % e] << 1;
      return !this.positiveArea(s[r], s[r + 1], s[n], s[n + 1], s[a], s[a + 1]);
    }
    static positiveArea(t, e, s, i, r, n) {
      return t * (n - i) + s * (e - n) + r * (i - e) >= 0;
    }
    static winding(t, e, s, i, r, n) {
      let a = s - t,
        h = i - e;
      return r * h - n * a + a * e - t * h >= 0 ? 1 : -1;
    }
  }
  class Wt {
    constructor() {
      (this.triangulator = new Ot()),
        (this.clippingPolygon = new Array()),
        (this.clipOutput = new Array()),
        (this.clippedVertices = new Array()),
        (this.clippedTriangles = new Array()),
        (this.scratch = new Array()),
        (this.clipAttachment = null),
        (this.clippingPolygons = null);
    }
    clipStart(t, e) {
      if (this.clipAttachment) return 0;
      this.clipAttachment = e;
      let s = e.worldVerticesLength,
        r = i.setArraySize(this.clippingPolygon, s);
      e.computeWorldVertices(t, 0, s, r, 0, 2);
      let n = this.clippingPolygon;
      Wt.makeClockwise(n);
      let a = (this.clippingPolygons = this.triangulator.decompose(
        n,
        this.triangulator.triangulate(n)
      ));
      for (let t = 0, e = a.length; t < e; t++) {
        let e = a[t];
        Wt.makeClockwise(e), e.push(e[0]), e.push(e[1]);
      }
      return a.length;
    }
    clipEndWithSlot(t) {
      this.clipAttachment &&
        this.clipAttachment.endSlot == t.data &&
        this.clipEnd();
    }
    clipEnd() {
      this.clipAttachment &&
        ((this.clipAttachment = null),
        (this.clippingPolygons = null),
        (this.clippedVertices.length = 0),
        (this.clippedTriangles.length = 0),
        (this.clippingPolygon.length = 0));
    }
    isClipping() {
      return null != this.clipAttachment;
    }
    clipTriangles(t, e, s, r, n, a, h, o) {
      let l = this.clipOutput,
        c = this.clippedVertices,
        d = this.clippedTriangles,
        u = this.clippingPolygons,
        m = u.length,
        f = o ? 12 : 8,
        g = 0;
      (c.length = 0), (d.length = 0);
      t: for (let e = 0; e < r; e += 3) {
        let r = s[e] << 1,
          p = t[r],
          x = t[r + 1],
          w = n[r],
          b = n[r + 1];
        r = s[e + 1] << 1;
        let v = t[r],
          y = t[r + 1],
          A = n[r],
          C = n[r + 1];
        r = s[e + 2] << 1;
        let k = t[r],
          S = t[r + 1],
          E = n[r],
          M = n[r + 1];
        for (let t = 0; t < m; t++) {
          let e = c.length;
          if (!this.clip(p, x, v, y, k, S, u[t], l)) {
            let t = i.setArraySize(c, e + 3 * f);
            (t[e] = p),
              (t[e + 1] = x),
              (t[e + 2] = a.r),
              (t[e + 3] = a.g),
              (t[e + 4] = a.b),
              (t[e + 5] = a.a),
              o
                ? ((t[e + 6] = w),
                  (t[e + 7] = b),
                  (t[e + 8] = h.r),
                  (t[e + 9] = h.g),
                  (t[e + 10] = h.b),
                  (t[e + 11] = h.a),
                  (t[e + 12] = v),
                  (t[e + 13] = y),
                  (t[e + 14] = a.r),
                  (t[e + 15] = a.g),
                  (t[e + 16] = a.b),
                  (t[e + 17] = a.a),
                  (t[e + 18] = A),
                  (t[e + 19] = C),
                  (t[e + 20] = h.r),
                  (t[e + 21] = h.g),
                  (t[e + 22] = h.b),
                  (t[e + 23] = h.a),
                  (t[e + 24] = k),
                  (t[e + 25] = S),
                  (t[e + 26] = a.r),
                  (t[e + 27] = a.g),
                  (t[e + 28] = a.b),
                  (t[e + 29] = a.a),
                  (t[e + 30] = E),
                  (t[e + 31] = M),
                  (t[e + 32] = h.r),
                  (t[e + 33] = h.g),
                  (t[e + 34] = h.b),
                  (t[e + 35] = h.a))
                : ((t[e + 6] = w),
                  (t[e + 7] = b),
                  (t[e + 8] = v),
                  (t[e + 9] = y),
                  (t[e + 10] = a.r),
                  (t[e + 11] = a.g),
                  (t[e + 12] = a.b),
                  (t[e + 13] = a.a),
                  (t[e + 14] = A),
                  (t[e + 15] = C),
                  (t[e + 16] = k),
                  (t[e + 17] = S),
                  (t[e + 18] = a.r),
                  (t[e + 19] = a.g),
                  (t[e + 20] = a.b),
                  (t[e + 21] = a.a),
                  (t[e + 22] = E),
                  (t[e + 23] = M)),
              (e = d.length);
            let s = i.setArraySize(d, e + 3);
            (s[e] = g), (s[e + 1] = g + 1), (s[e + 2] = g + 2), (g += 3);
            continue t;
          }
          {
            let t = l.length;
            if (0 == t) continue;
            let s = y - S,
              r = k - v,
              n = p - k,
              u = S - x,
              m = 1 / (s * n + r * (x - S)),
              T = t >> 1,
              R = this.clipOutput,
              Y = i.setArraySize(c, e + T * f);
            for (let i = 0; i < t; i += 2) {
              let t = R[i],
                l = R[i + 1];
              (Y[e] = t),
                (Y[e + 1] = l),
                (Y[e + 2] = a.r),
                (Y[e + 3] = a.g),
                (Y[e + 4] = a.b),
                (Y[e + 5] = a.a);
              let c = t - k,
                d = l - S,
                g = (s * c + r * d) * m,
                p = (u * c + n * d) * m,
                x = 1 - g - p;
              (Y[e + 6] = w * g + A * p + E * x),
                (Y[e + 7] = b * g + C * p + M * x),
                o &&
                  ((Y[e + 8] = h.r),
                  (Y[e + 9] = h.g),
                  (Y[e + 10] = h.b),
                  (Y[e + 11] = h.a)),
                (e += f);
            }
            e = d.length;
            let I = i.setArraySize(d, e + 3 * (T - 2));
            T--;
            for (let t = 1; t < T; t++)
              (I[e] = g), (I[e + 1] = g + t), (I[e + 2] = g + t + 1), (e += 3);
            g += T + 1;
          }
        }
      }
    }
    clip(t, e, s, i, r, n, a, h) {
      let o,
        l = h,
        c = !1;
      a.length % 4 >= 2 ? ((o = h), (h = this.scratch)) : (o = this.scratch),
        (o.length = 0),
        o.push(t),
        o.push(e),
        o.push(s),
        o.push(i),
        o.push(r),
        o.push(n),
        o.push(t),
        o.push(e),
        (h.length = 0);
      let d = a,
        u = a.length - 4;
      for (let t = 0; ; t += 2) {
        let e = d[t],
          s = d[t + 1],
          i = d[t + 2],
          r = d[t + 3],
          n = e - i,
          a = s - r,
          m = o,
          f = o.length - 2,
          g = h.length;
        for (let t = 0; t < f; t += 2) {
          let o = m[t],
            l = m[t + 1],
            d = m[t + 2],
            u = m[t + 3],
            f = n * (u - r) - a * (d - i) > 0;
          if (n * (l - r) - a * (o - i) > 0) {
            if (f) {
              h.push(d), h.push(u);
              continue;
            }
            let t = u - l,
              n = d - o,
              a = t * (i - e) - n * (r - s);
            if (Math.abs(a) > 1e-6) {
              let c = (n * (s - l) - t * (e - o)) / a;
              h.push(e + (i - e) * c), h.push(s + (r - s) * c);
            } else h.push(e), h.push(s);
          } else if (f) {
            let t = u - l,
              n = d - o,
              a = t * (i - e) - n * (r - s);
            if (Math.abs(a) > 1e-6) {
              let c = (n * (s - l) - t * (e - o)) / a;
              h.push(e + (i - e) * c), h.push(s + (r - s) * c);
            } else h.push(e), h.push(s);
            h.push(d), h.push(u);
          }
          c = !0;
        }
        if (g == h.length) return (l.length = 0), !0;
        if ((h.push(h[0]), h.push(h[1]), t == u)) break;
        let p = h;
        ((h = o).length = 0), (o = p);
      }
      if (l != h) {
        l.length = 0;
        for (let t = 0, e = h.length - 2; t < e; t++) l[t] = h[t];
      } else l.length = l.length - 2;
      return c;
    }
    static makeClockwise(t) {
      let e = t,
        s = t.length,
        i = e[s - 2] * e[1] - e[0] * e[s - 1],
        r = 0,
        n = 0,
        a = 0,
        h = 0;
      for (let t = 0, o = s - 3; t < o; t += 2)
        (r = e[t]),
          (n = e[t + 1]),
          (a = e[t + 2]),
          (h = e[t + 3]),
          (i += r * h - a * n);
      if (!(i < 0))
        for (let t = 0, i = s - 2, r = s >> 1; t < r; t += 2) {
          let s = e[t],
            r = e[t + 1],
            n = i - t;
          (e[t] = e[n]), (e[t + 1] = e[n + 1]), (e[n] = s), (e[n + 1] = r);
        }
    }
  }
  class qt {
    constructor(t) {
      (this.intValue = 0),
        (this.floatValue = 0),
        (this.stringValue = null),
        (this.audioPath = null),
        (this.volume = 0),
        (this.balance = 0),
        (this.name = t);
    }
  }
  class zt {
    constructor(t, e) {
      if (
        ((this.intValue = 0),
        (this.floatValue = 0),
        (this.stringValue = null),
        (this.time = 0),
        (this.volume = 0),
        (this.balance = 0),
        !e)
      )
        throw new Error("data cannot be null.");
      (this.time = t), (this.data = e);
    }
  }
  class Ut extends It {
    constructor(t) {
      super(t, 0, !1),
        (this.bones = new Array()),
        (this._target = null),
        (this.bendDirection = 1),
        (this.compress = !1),
        (this.stretch = !1),
        (this.uniform = !1),
        (this.mix = 1),
        (this.softness = 0);
    }
    set target(t) {
      this._target = t;
    }
    get target() {
      if (this._target) return this._target;
      throw new Error("BoneData not set.");
    }
  }
  class $t {
    constructor() {
      (this.name = null),
        (this.bones = new Array()),
        (this.slots = new Array()),
        (this.skins = new Array()),
        (this.defaultSkin = null),
        (this.events = new Array()),
        (this.animations = new Array()),
        (this.ikConstraints = new Array()),
        (this.transformConstraints = new Array()),
        (this.pathConstraints = new Array()),
        (this.x = 0),
        (this.y = 0),
        (this.width = 0),
        (this.height = 0),
        (this.version = null),
        (this.hash = null),
        (this.fps = 0),
        (this.imagesPath = null),
        (this.audioPath = null);
    }
    findBone(t) {
      if (!t) throw new Error("boneName cannot be null.");
      let e = this.bones;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
    findSlot(t) {
      if (!t) throw new Error("slotName cannot be null.");
      let e = this.slots;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
    findSkin(t) {
      if (!t) throw new Error("skinName cannot be null.");
      let e = this.skins;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
    findEvent(t) {
      if (!t) throw new Error("eventDataName cannot be null.");
      let e = this.events;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
    findAnimation(t) {
      if (!t) throw new Error("animationName cannot be null.");
      let e = this.animations;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
    findIkConstraint(t) {
      if (!t) throw new Error("constraintName cannot be null.");
      let e = this.ikConstraints;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
    findTransformConstraint(t) {
      if (!t) throw new Error("constraintName cannot be null.");
      let e = this.transformConstraints;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
    findPathConstraint(t) {
      if (!t) throw new Error("constraintName cannot be null.");
      let e = this.pathConstraints;
      for (let s = 0, i = e.length; s < i; s++) {
        let i = e[s];
        if (i.name == t) return i;
      }
      return null;
    }
  }
  class Ht extends It {
    constructor(t) {
      super(t, 0, !1),
        (this.bones = new Array()),
        (this._target = null),
        (this.mixRotate = 0),
        (this.mixX = 0),
        (this.mixY = 0),
        (this.mixScaleX = 0),
        (this.mixScaleY = 0),
        (this.mixShearY = 0),
        (this.offsetRotation = 0),
        (this.offsetX = 0),
        (this.offsetY = 0),
        (this.offsetScaleX = 0),
        (this.offsetScaleY = 0),
        (this.offsetShearY = 0),
        (this.relative = !1),
        (this.local = !1);
    }
    set target(t) {
      this._target = t;
    }
    get target() {
      if (this._target) return this._target;
      throw new Error("BoneData not set.");
    }
  }
  class Gt {
    constructor(t) {
      (this.scale = 1),
        (this.linkedMeshes = new Array()),
        (this.attachmentLoader = t);
    }
    readSkeletonData(t) {
      let s = this.scale,
        r = new $t(),
        n = "string" == typeof t ? JSON.parse(t) : t,
        a = n.skeleton;
      if (
        (a &&
          ((r.hash = a.hash),
          (r.version = a.spine),
          (r.x = a.x),
          (r.y = a.y),
          (r.width = a.width),
          (r.height = a.height),
          (r.fps = a.fps),
          (r.imagesPath = a.images)),
        n.bones)
      )
        for (let t = 0; t < n.bones.length; t++) {
          let e = n.bones[t],
            a = null,
            h = Zt(e, "parent", null);
          h && (a = r.findBone(h));
          let o = new Et(r.bones.length, e.name, a);
          (o.length = Zt(e, "length", 0) * s),
            (o.x = Zt(e, "x", 0) * s),
            (o.y = Zt(e, "y", 0) * s),
            (o.rotation = Zt(e, "rotation", 0)),
            (o.scaleX = Zt(e, "scaleX", 1)),
            (o.scaleY = Zt(e, "scaleY", 1)),
            (o.shearX = Zt(e, "shearX", 0)),
            (o.shearY = Zt(e, "shearY", 0)),
            (o.transformMode = i.enumValue(ht, Zt(e, "transform", "Normal"))),
            (o.skinRequired = Zt(e, "skin", !1));
          let l = Zt(e, "color", null);
          l && o.color.setFromString(l), r.bones.push(o);
        }
      if (n.slots)
        for (let t = 0; t < n.slots.length; t++) {
          let s = n.slots[t],
            a = r.findBone(s.bone);
          if (!a)
            throw new Error(`Couldn't find bone ${s.bone} for slot ${s.name}`);
          let h = new Nt(r.slots.length, s.name, a),
            o = Zt(s, "color", null);
          o && h.color.setFromString(o);
          let l = Zt(s, "dark", null);
          l && (h.darkColor = e.fromString(l)),
            (h.attachmentName = Zt(s, "attachment", null)),
            (h.blendMode = i.enumValue(dt, Zt(s, "blend", "normal"))),
            r.slots.push(h);
        }
      if (n.ik)
        for (let t = 0; t < n.ik.length; t++) {
          let e = n.ik[t],
            i = new Ut(e.name);
          (i.order = Zt(e, "order", 0)), (i.skinRequired = Zt(e, "skin", !1));
          for (let t = 0; t < e.bones.length; t++) {
            let s = r.findBone(e.bones[t]);
            if (!s)
              throw new Error(
                `Couldn't find bone ${e.bones[t]} for IK constraint ${e.name}.`
              );
            i.bones.push(s);
          }
          let a = r.findBone(e.target);
          if (!a)
            throw new Error(
              `Couldn't find target bone ${e.target} for IK constraint ${e.name}.`
            );
          (i.target = a),
            (i.mix = Zt(e, "mix", 1)),
            (i.softness = Zt(e, "softness", 0) * s),
            (i.bendDirection = Zt(e, "bendPositive", !0) ? 1 : -1),
            (i.compress = Zt(e, "compress", !1)),
            (i.stretch = Zt(e, "stretch", !1)),
            (i.uniform = Zt(e, "uniform", !1)),
            r.ikConstraints.push(i);
        }
      if (n.transform)
        for (let t = 0; t < n.transform.length; t++) {
          let e = n.transform[t],
            i = new Ht(e.name);
          (i.order = Zt(e, "order", 0)), (i.skinRequired = Zt(e, "skin", !1));
          for (let t = 0; t < e.bones.length; t++) {
            let s = e.bones[t],
              n = r.findBone(s);
            if (!n)
              throw new Error(
                `Couldn't find bone ${s} for transform constraint ${e.name}.`
              );
            i.bones.push(n);
          }
          let a = e.target,
            h = r.findBone(a);
          if (!h)
            throw new Error(
              `Couldn't find target bone ${a} for transform constraint ${e.name}.`
            );
          (i.target = h),
            (i.local = Zt(e, "local", !1)),
            (i.relative = Zt(e, "relative", !1)),
            (i.offsetRotation = Zt(e, "rotation", 0)),
            (i.offsetX = Zt(e, "x", 0) * s),
            (i.offsetY = Zt(e, "y", 0) * s),
            (i.offsetScaleX = Zt(e, "scaleX", 0)),
            (i.offsetScaleY = Zt(e, "scaleY", 0)),
            (i.offsetShearY = Zt(e, "shearY", 0)),
            (i.mixRotate = Zt(e, "mixRotate", 1)),
            (i.mixX = Zt(e, "mixX", 1)),
            (i.mixY = Zt(e, "mixY", i.mixX)),
            (i.mixScaleX = Zt(e, "mixScaleX", 1)),
            (i.mixScaleY = Zt(e, "mixScaleY", i.mixScaleX)),
            (i.mixShearY = Zt(e, "mixShearY", 1)),
            r.transformConstraints.push(i);
        }
      if (n.path)
        for (let t = 0; t < n.path.length; t++) {
          let e = n.path[t],
            a = new Xt(e.name);
          (a.order = Zt(e, "order", 0)), (a.skinRequired = Zt(e, "skin", !1));
          for (let t = 0; t < e.bones.length; t++) {
            let s = e.bones[t],
              i = r.findBone(s);
            if (!i)
              throw new Error(
                `Couldn't find bone ${s} for path constraint ${e.name}.`
              );
            a.bones.push(i);
          }
          let h = e.target,
            o = r.findSlot(h);
          if (!o)
            throw new Error(
              `Couldn't find target slot ${h} for path constraint ${e.name}.`
            );
          (a.target = o),
            (a.positionMode = i.enumValue(
              ot,
              Zt(e, "positionMode", "Percent")
            )),
            (a.spacingMode = i.enumValue(lt, Zt(e, "spacingMode", "Length"))),
            (a.rotateMode = i.enumValue(ct, Zt(e, "rotateMode", "Tangent"))),
            (a.offsetRotation = Zt(e, "rotation", 0)),
            (a.position = Zt(e, "position", 0)),
            a.positionMode == ot.Fixed && (a.position *= s),
            (a.spacing = Zt(e, "spacing", 0)),
            (a.spacingMode != lt.Length && a.spacingMode != lt.Fixed) ||
              (a.spacing *= s),
            (a.mixRotate = Zt(e, "mixRotate", 1)),
            (a.mixX = Zt(e, "mixX", 1)),
            (a.mixY = Zt(e, "mixY", a.mixX)),
            r.pathConstraints.push(a);
        }
      if (n.skins)
        for (let t = 0; t < n.skins.length; t++) {
          let e = n.skins[t],
            s = new Vt(e.name);
          if (e.bones)
            for (let t = 0; t < e.bones.length; t++) {
              let i = e.bones[t],
                n = r.findBone(i);
              if (!n)
                throw new Error(`Couldn't find bone ${i} for skin ${e.name}.`);
              s.bones.push(n);
            }
          if (e.ik)
            for (let t = 0; t < e.ik.length; t++) {
              let i = e.ik[t],
                n = r.findIkConstraint(i);
              if (!n)
                throw new Error(
                  `Couldn't find IK constraint ${i} for skin ${e.name}.`
                );
              s.constraints.push(n);
            }
          if (e.transform)
            for (let t = 0; t < e.transform.length; t++) {
              let i = e.transform[t],
                n = r.findTransformConstraint(i);
              if (!n)
                throw new Error(
                  `Couldn't find transform constraint ${i} for skin ${e.name}.`
                );
              s.constraints.push(n);
            }
          if (e.path)
            for (let t = 0; t < e.path.length; t++) {
              let i = e.path[t],
                n = r.findPathConstraint(i);
              if (!n)
                throw new Error(
                  `Couldn't find path constraint ${i} for skin ${e.name}.`
                );
              s.constraints.push(n);
            }
          for (let t in e.attachments) {
            let i = r.findSlot(t);
            if (!i)
              throw new Error(`Couldn't find slot ${t} for skin ${e.name}.`);
            let n = e.attachments[t];
            for (let t in n) {
              let e = this.readAttachment(n[t], s, i.index, t, r);
              e && s.setAttachment(i.index, t, e);
            }
          }
          r.skins.push(s), "default" == s.name && (r.defaultSkin = s);
        }
      for (let t = 0, e = this.linkedMeshes.length; t < e; t++) {
        let e = this.linkedMeshes[t],
          s = e.skin ? r.findSkin(e.skin) : r.defaultSkin;
        if (!s) throw new Error(`Skin not found: ${e.skin}`);
        let i = s.getAttachment(e.slotIndex, e.parent);
        if (!i) throw new Error(`Parent mesh not found: ${e.parent}`);
        (e.mesh.timelineAttachment = e.inheritTimeline ? i : e.mesh),
          e.mesh.setParentMesh(i),
          null != e.mesh.region && e.mesh.updateRegion();
      }
      if (((this.linkedMeshes.length = 0), n.events))
        for (let t in n.events) {
          let e = n.events[t],
            s = new qt(t);
          (s.intValue = Zt(e, "int", 0)),
            (s.floatValue = Zt(e, "float", 0)),
            (s.stringValue = Zt(e, "string", "")),
            (s.audioPath = Zt(e, "audio", null)),
            s.audioPath &&
              ((s.volume = Zt(e, "volume", 1)),
              (s.balance = Zt(e, "balance", 0))),
            r.events.push(s);
        }
      if (n.animations)
        for (let t in n.animations) {
          let e = n.animations[t];
          this.readAnimation(e, t, r);
        }
      return r;
    }
    readAttachment(t, e, s, r, n) {
      let a = this.scale;
      switch (((r = Zt(t, "name", r)), Zt(t, "type", "region"))) {
        case "region": {
          let s = Zt(t, "path", r),
            i = this.readSequence(Zt(t, "sequence", null)),
            n = this.attachmentLoader.newRegionAttachment(e, r, s, i);
          if (!n) return null;
          (n.path = s),
            (n.x = Zt(t, "x", 0) * a),
            (n.y = Zt(t, "y", 0) * a),
            (n.scaleX = Zt(t, "scaleX", 1)),
            (n.scaleY = Zt(t, "scaleY", 1)),
            (n.rotation = Zt(t, "rotation", 0)),
            (n.width = t.width * a),
            (n.height = t.height * a),
            (n.sequence = i);
          let h = Zt(t, "color", null);
          return (
            h && n.color.setFromString(h),
            null != n.region && n.updateRegion(),
            n
          );
        }
        case "boundingbox": {
          let s = this.attachmentLoader.newBoundingBoxAttachment(e, r);
          if (!s) return null;
          this.readVertices(t, s, t.vertexCount << 1);
          let i = Zt(t, "color", null);
          return i && s.color.setFromString(i), s;
        }
        case "mesh":
        case "linkedmesh": {
          let i = Zt(t, "path", r),
            n = this.readSequence(Zt(t, "sequence", null)),
            h = this.attachmentLoader.newMeshAttachment(e, r, i, n);
          if (!h) return null;
          h.path = i;
          let o = Zt(t, "color", null);
          o && h.color.setFromString(o),
            (h.width = Zt(t, "width", 0) * a),
            (h.height = Zt(t, "height", 0) * a),
            (h.sequence = n);
          let l = Zt(t, "parent", null);
          if (l)
            return (
              this.linkedMeshes.push(
                new jt(h, Zt(t, "skin", null), s, l, Zt(t, "timelines", !0))
              ),
              h
            );
          let c = t.uvs;
          return (
            this.readVertices(t, h, c.length),
            (h.triangles = t.triangles),
            (h.regionUVs = c),
            null != h.region && h.updateRegion(),
            (h.edges = Zt(t, "edges", null)),
            (h.hullLength = 2 * Zt(t, "hull", 0)),
            h
          );
        }
        case "path": {
          let s = this.attachmentLoader.newPathAttachment(e, r);
          if (!s) return null;
          (s.closed = Zt(t, "closed", !1)),
            (s.constantSpeed = Zt(t, "constantSpeed", !0));
          let n = t.vertexCount;
          this.readVertices(t, s, n << 1);
          let h = i.newArray(n / 3, 0);
          for (let e = 0; e < t.lengths.length; e++) h[e] = t.lengths[e] * a;
          s.lengths = h;
          let o = Zt(t, "color", null);
          return o && s.color.setFromString(o), s;
        }
        case "point": {
          let s = this.attachmentLoader.newPointAttachment(e, r);
          if (!s) return null;
          (s.x = Zt(t, "x", 0) * a),
            (s.y = Zt(t, "y", 0) * a),
            (s.rotation = Zt(t, "rotation", 0));
          let i = Zt(t, "color", null);
          return i && s.color.setFromString(i), s;
        }
        case "clipping": {
          let s = this.attachmentLoader.newClippingAttachment(e, r);
          if (!s) return null;
          let i = Zt(t, "end", null);
          i && (s.endSlot = n.findSlot(i));
          let a = t.vertexCount;
          this.readVertices(t, s, a << 1);
          let h = Zt(t, "color", null);
          return h && s.color.setFromString(h), s;
        }
      }
      return null;
    }
    readSequence(t) {
      if (null == t) return null;
      let e = new l(Zt(t, "count", 0));
      return (
        (e.start = Zt(t, "start", 1)),
        (e.digits = Zt(t, "digits", 0)),
        (e.setupIndex = Zt(t, "setup", 0)),
        e
      );
    }
    readVertices(t, e, s) {
      let r = this.scale;
      e.worldVerticesLength = s;
      let n = t.vertices;
      if (s == n.length) {
        let t = i.toFloatArray(n);
        if (1 != r) for (let e = 0, s = n.length; e < s; e++) t[e] *= r;
        return void (e.vertices = t);
      }
      let a = new Array(),
        h = new Array();
      for (let t = 0, e = n.length; t < e; ) {
        let e = n[t++];
        h.push(e);
        for (let s = t + 4 * e; t < s; t += 4)
          h.push(n[t]),
            a.push(n[t + 1] * r),
            a.push(n[t + 2] * r),
            a.push(n[t + 3]);
      }
      (e.bones = h), (e.vertices = i.toFloatArray(a));
    }
    readAnimation(t, s, r) {
      let n = this.scale,
        a = new Array();
      if (t.slots)
        for (let s in t.slots) {
          let i = t.slots[s],
            n = r.findSlot(s);
          if (!n) throw new Error("Slot not found: " + s);
          let h = n.index;
          for (let t in i) {
            let s = i[t];
            if (!s) continue;
            let r = s.length;
            if ("attachment" == t) {
              let t = new P(r, h);
              for (let e = 0; e < r; e++) {
                let i = s[e];
                t.setFrame(e, Zt(i, "time", 0), Zt(i, "name", null));
              }
              a.push(t);
            } else if ("rgba" == t) {
              let t = new R(r, r << 2, h),
                i = s[0],
                n = Zt(i, "time", 0),
                o = e.fromString(i.color);
              for (let r = 0, a = 0; ; r++) {
                t.setFrame(r, n, o.r, o.g, o.b, o.a);
                let h = s[r + 1];
                if (!h) {
                  t.shrink(a);
                  break;
                }
                let l = Zt(h, "time", 0),
                  c = e.fromString(h.color),
                  d = i.curve;
                d &&
                  ((a = Qt(d, t, a, r, 0, n, l, o.r, c.r, 1)),
                  (a = Qt(d, t, a, r, 1, n, l, o.g, c.g, 1)),
                  (a = Qt(d, t, a, r, 2, n, l, o.b, c.b, 1)),
                  (a = Qt(d, t, a, r, 3, n, l, o.a, c.a, 1))),
                  (n = l),
                  (o = c),
                  (i = h);
              }
              a.push(t);
            } else if ("rgb" == t) {
              let t = new Y(r, 3 * r, h),
                i = s[0],
                n = Zt(i, "time", 0),
                o = e.fromString(i.color);
              for (let r = 0, a = 0; ; r++) {
                t.setFrame(r, n, o.r, o.g, o.b);
                let h = s[r + 1];
                if (!h) {
                  t.shrink(a);
                  break;
                }
                let l = Zt(h, "time", 0),
                  c = e.fromString(h.color),
                  d = i.curve;
                d &&
                  ((a = Qt(d, t, a, r, 0, n, l, o.r, c.r, 1)),
                  (a = Qt(d, t, a, r, 1, n, l, o.g, c.g, 1)),
                  (a = Qt(d, t, a, r, 2, n, l, o.b, c.b, 1))),
                  (n = l),
                  (o = c),
                  (i = h);
              }
              a.push(t);
            } else if ("alpha" == t) a.push(Kt(s, new I(r, r, h), 0, 1));
            else if ("rgba2" == t) {
              let t = new X(r, 7 * r, h),
                i = s[0],
                n = Zt(i, "time", 0),
                o = e.fromString(i.light),
                l = e.fromString(i.dark);
              for (let r = 0, a = 0; ; r++) {
                t.setFrame(r, n, o.r, o.g, o.b, o.a, l.r, l.g, l.b);
                let h = s[r + 1];
                if (!h) {
                  t.shrink(a);
                  break;
                }
                let c = Zt(h, "time", 0),
                  d = e.fromString(h.light),
                  u = e.fromString(h.dark),
                  m = i.curve;
                m &&
                  ((a = Qt(m, t, a, r, 0, n, c, o.r, d.r, 1)),
                  (a = Qt(m, t, a, r, 1, n, c, o.g, d.g, 1)),
                  (a = Qt(m, t, a, r, 2, n, c, o.b, d.b, 1)),
                  (a = Qt(m, t, a, r, 3, n, c, o.a, d.a, 1)),
                  (a = Qt(m, t, a, r, 4, n, c, l.r, u.r, 1)),
                  (a = Qt(m, t, a, r, 5, n, c, l.g, u.g, 1)),
                  (a = Qt(m, t, a, r, 6, n, c, l.b, u.b, 1))),
                  (n = c),
                  (o = d),
                  (l = u),
                  (i = h);
              }
              a.push(t);
            } else if ("rgb2" == t) {
              let t = new L(r, 6 * r, h),
                i = s[0],
                n = Zt(i, "time", 0),
                o = e.fromString(i.light),
                l = e.fromString(i.dark);
              for (let r = 0, a = 0; ; r++) {
                t.setFrame(r, n, o.r, o.g, o.b, l.r, l.g, l.b);
                let h = s[r + 1];
                if (!h) {
                  t.shrink(a);
                  break;
                }
                let c = Zt(h, "time", 0),
                  d = e.fromString(h.light),
                  u = e.fromString(h.dark),
                  m = i.curve;
                m &&
                  ((a = Qt(m, t, a, r, 0, n, c, o.r, d.r, 1)),
                  (a = Qt(m, t, a, r, 1, n, c, o.g, d.g, 1)),
                  (a = Qt(m, t, a, r, 2, n, c, o.b, d.b, 1)),
                  (a = Qt(m, t, a, r, 3, n, c, l.r, u.r, 1)),
                  (a = Qt(m, t, a, r, 4, n, c, l.g, u.g, 1)),
                  (a = Qt(m, t, a, r, 5, n, c, l.b, u.b, 1))),
                  (n = c),
                  (o = d),
                  (l = u),
                  (i = h);
              }
              a.push(t);
            }
          }
        }
      if (t.bones)
        for (let e in t.bones) {
          let s = t.bones[e],
            i = r.findBone(e);
          if (!i) throw new Error("Bone not found: " + e);
          let h = i.index;
          for (let t in s) {
            let e = s[t],
              i = e.length;
            if (0 != i)
              if ("rotate" === t) a.push(Kt(e, new b(i, i, h), 0, 1));
              else if ("translate" === t) {
                let t = new v(i, i << 1, h);
                a.push(Jt(e, t, "x", "y", 0, n));
              } else if ("translatex" === t) {
                let t = new y(i, i, h);
                a.push(Kt(e, t, 0, n));
              } else if ("translatey" === t) {
                let t = new A(i, i, h);
                a.push(Kt(e, t, 0, n));
              } else if ("scale" === t) {
                let t = new C(i, i << 1, h);
                a.push(Jt(e, t, "x", "y", 1, 1));
              } else if ("scalex" === t) {
                let t = new k(i, i, h);
                a.push(Kt(e, t, 1, 1));
              } else if ("scaley" === t) {
                let t = new S(i, i, h);
                a.push(Kt(e, t, 1, 1));
              } else if ("shear" === t) {
                let t = new E(i, i << 1, h);
                a.push(Jt(e, t, "x", "y", 0, 1));
              } else if ("shearx" === t) {
                let t = new M(i, i, h);
                a.push(Kt(e, t, 0, 1));
              } else if ("sheary" === t) {
                let t = new T(i, i, h);
                a.push(Kt(e, t, 0, 1));
              }
          }
        }
      if (t.ik)
        for (let e in t.ik) {
          let s = t.ik[e],
            i = s[0];
          if (!i) continue;
          let h = r.findIkConstraint(e);
          if (!h) throw new Error("IK Constraint not found: " + e);
          let o = r.ikConstraints.indexOf(h),
            l = new V(s.length, s.length << 1, o),
            c = Zt(i, "time", 0),
            d = Zt(i, "mix", 1),
            u = Zt(i, "softness", 0) * n;
          for (let t = 0, e = 0; ; t++) {
            l.setFrame(
              t,
              c,
              d,
              u,
              Zt(i, "bendPositive", !0) ? 1 : -1,
              Zt(i, "compress", !1),
              Zt(i, "stretch", !1)
            );
            let r = s[t + 1];
            if (!r) {
              l.shrink(e);
              break;
            }
            let a = Zt(r, "time", 0),
              h = Zt(r, "mix", 1),
              o = Zt(r, "softness", 0) * n,
              m = i.curve;
            m &&
              ((e = Qt(m, l, e, t, 0, c, a, d, h, 1)),
              (e = Qt(m, l, e, t, 1, c, a, u, o, n))),
              (c = a),
              (d = h),
              (u = o),
              (i = r);
          }
          a.push(l);
        }
      if (t.transform)
        for (let e in t.transform) {
          let s = t.transform[e],
            i = s[0];
          if (!i) continue;
          let n = r.findTransformConstraint(e);
          if (!n) throw new Error("Transform constraint not found: " + e);
          let h = r.transformConstraints.indexOf(n),
            o = new N(s.length, 6 * s.length, h),
            l = Zt(i, "time", 0),
            c = Zt(i, "mixRotate", 1),
            d = Zt(i, "mixX", 1),
            u = Zt(i, "mixY", d),
            m = Zt(i, "mixScaleX", 1),
            f = Zt(i, "mixScaleY", m),
            g = Zt(i, "mixShearY", 1);
          for (let t = 0, e = 0; ; t++) {
            o.setFrame(t, l, c, d, u, m, f, g);
            let r = s[t + 1];
            if (!r) {
              o.shrink(e);
              break;
            }
            let n = Zt(r, "time", 0),
              a = Zt(r, "mixRotate", 1),
              h = Zt(r, "mixX", 1),
              p = Zt(r, "mixY", h),
              x = Zt(r, "mixScaleX", 1),
              w = Zt(r, "mixScaleY", x),
              b = Zt(r, "mixShearY", 1),
              v = i.curve;
            v &&
              ((e = Qt(v, o, e, t, 0, l, n, c, a, 1)),
              (e = Qt(v, o, e, t, 1, l, n, d, h, 1)),
              (e = Qt(v, o, e, t, 2, l, n, u, p, 1)),
              (e = Qt(v, o, e, t, 3, l, n, m, x, 1)),
              (e = Qt(v, o, e, t, 4, l, n, f, w, 1)),
              (e = Qt(v, o, e, t, 5, l, n, g, b, 1))),
              (l = n),
              (c = a),
              (d = h),
              (u = p),
              (m = x),
              (f = w),
              (m = x),
              (i = r);
          }
          a.push(o);
        }
      if (t.path)
        for (let e in t.path) {
          let s = t.path[e],
            i = r.findPathConstraint(e);
          if (!i) throw new Error("Path constraint not found: " + e);
          let h = r.pathConstraints.indexOf(i);
          for (let t in s) {
            let e = s[t],
              r = e[0];
            if (!r) continue;
            let o = e.length;
            if ("position" === t) {
              let t = new _(o, o, h);
              a.push(Kt(e, t, 0, i.positionMode == ot.Fixed ? n : 1));
            } else if ("spacing" === t) {
              let t = new O(o, o, h);
              a.push(
                Kt(
                  e,
                  t,
                  0,
                  i.spacingMode == lt.Length || i.spacingMode == lt.Fixed
                    ? n
                    : 1
                )
              );
            } else if ("mix" === t) {
              let t = new W(o, 3 * o, h),
                s = Zt(r, "time", 0),
                i = Zt(r, "mixRotate", 1),
                n = Zt(r, "mixX", 1),
                l = Zt(r, "mixY", n);
              for (let a = 0, h = 0; ; a++) {
                t.setFrame(a, s, i, n, l);
                let o = e[a + 1];
                if (!o) {
                  t.shrink(h);
                  break;
                }
                let c = Zt(o, "time", 0),
                  d = Zt(o, "mixRotate", 1),
                  u = Zt(o, "mixX", 1),
                  m = Zt(o, "mixY", u),
                  f = r.curve;
                f &&
                  ((h = Qt(f, t, h, a, 0, s, c, i, d, 1)),
                  (h = Qt(f, t, h, a, 1, s, c, n, u, 1)),
                  (h = Qt(f, t, h, a, 2, s, c, l, m, 1))),
                  (s = c),
                  (i = d),
                  (n = u),
                  (l = m),
                  (r = o);
              }
              a.push(t);
            }
          }
        }
      if (t.attachments)
        for (let e in t.attachments) {
          let s = t.attachments[e],
            h = r.findSkin(e);
          if (!h) throw new Error("Skin not found: " + e);
          for (let t in s) {
            let e = s[t],
              o = r.findSlot(t);
            if (!o) throw new Error("Slot not found: " + t);
            let l = o.index;
            for (let t in e) {
              let s = e[t],
                r = h.getAttachment(l, t);
              for (let t in s) {
                let e = s[t],
                  h = e[0];
                if (h)
                  if ("deform" == t) {
                    let t = r.bones,
                      s = r.vertices,
                      o = t ? (s.length / 3) * 2 : s.length,
                      c = new F(e.length, e.length, l, r),
                      d = Zt(h, "time", 0);
                    for (let r = 0, a = 0; ; r++) {
                      let l,
                        u = Zt(h, "vertices", null);
                      if (u) {
                        l = i.newFloatArray(o);
                        let e = Zt(h, "offset", 0);
                        if ((i.arrayCopy(u, 0, l, e, u.length), 1 != n))
                          for (let t = e, s = t + u.length; t < s; t++)
                            l[t] *= n;
                        if (!t) for (let t = 0; t < o; t++) l[t] += s[t];
                      } else l = t ? i.newFloatArray(o) : s;
                      c.setFrame(r, d, l);
                      let m = e[r + 1];
                      if (!m) {
                        c.shrink(a);
                        break;
                      }
                      let f = Zt(m, "time", 0),
                        g = h.curve;
                      g && (a = Qt(g, c, a, r, 0, d, f, 0, 1, 1)),
                        (d = f),
                        (h = m);
                    }
                    a.push(c);
                  } else if ("sequence" == t) {
                    let t = new q(e.length, l, r),
                      s = 0;
                    for (let i = 0; i < e.length; i++) {
                      let r = Zt(h, "delay", s),
                        n = Zt(h, "time", 0),
                        a = c[Zt(h, "mode", "hold")],
                        o = Zt(h, "index", 0);
                      t.setFrame(i, n, a, o, r), (s = r), (h = e[i + 1]);
                    }
                    a.push(t);
                  }
              }
            }
          }
        }
      if (t.drawOrder) {
        let e = new B(t.drawOrder.length),
          s = r.slots.length,
          n = 0;
        for (let a = 0; a < t.drawOrder.length; a++, n++) {
          let h = t.drawOrder[a],
            o = null,
            l = Zt(h, "offsets", null);
          if (l) {
            o = i.newArray(s, -1);
            let t = i.newArray(s - l.length, 0),
              e = 0,
              n = 0;
            for (let s = 0; s < l.length; s++) {
              let i = l[s],
                a = r.findSlot(i.slot);
              if (!a) throw new Error("Slot not found: " + a);
              let h = a.index;
              for (; e != h; ) t[n++] = e++;
              o[e + i.offset] = e++;
            }
            for (; e < s; ) t[n++] = e++;
            for (let e = s - 1; e >= 0; e--) -1 == o[e] && (o[e] = t[--n]);
          }
          e.setFrame(n, Zt(h, "time", 0), o);
        }
        a.push(e);
      }
      if (t.events) {
        let e = new D(t.events.length),
          s = 0;
        for (let n = 0; n < t.events.length; n++, s++) {
          let a = t.events[n],
            h = r.findEvent(a.name);
          if (!h) throw new Error("Event not found: " + a.name);
          let o = new zt(i.toSinglePrecision(Zt(a, "time", 0)), h);
          (o.intValue = Zt(a, "int", h.intValue)),
            (o.floatValue = Zt(a, "float", h.floatValue)),
            (o.stringValue = Zt(a, "string", h.stringValue)),
            o.data.audioPath &&
              ((o.volume = Zt(a, "volume", 1)),
              (o.balance = Zt(a, "balance", 0))),
            e.setFrame(s, o);
        }
        a.push(e);
      }
      let h = 0;
      for (let t = 0, e = a.length; t < e; t++)
        h = Math.max(h, a[t].getDuration());
      r.animations.push(new u(s, a, h));
    }
  }
  class jt {
    constructor(t, e, s, i, r) {
      (this.mesh = t),
        (this.skin = e),
        (this.slotIndex = s),
        (this.parent = i),
        (this.inheritTimeline = r);
    }
  }
  function Kt(t, e, s, i) {
    let r = t[0],
      n = Zt(r, "time", 0),
      a = Zt(r, "value", s) * i,
      h = 0;
    for (let o = 0; ; o++) {
      e.setFrame(o, n, a);
      let l = t[o + 1];
      if (!l) return e.shrink(h), e;
      let c = Zt(l, "time", 0),
        d = Zt(l, "value", s) * i;
      r.curve && (h = Qt(r.curve, e, h, o, 0, n, c, a, d, i)),
        (n = c),
        (a = d),
        (r = l);
    }
  }
  function Jt(t, e, s, i, r, n) {
    let a = t[0],
      h = Zt(a, "time", 0),
      o = Zt(a, s, r) * n,
      l = Zt(a, i, r) * n,
      c = 0;
    for (let d = 0; ; d++) {
      e.setFrame(d, h, o, l);
      let u = t[d + 1];
      if (!u) return e.shrink(c), e;
      let m = Zt(u, "time", 0),
        f = Zt(u, s, r) * n,
        g = Zt(u, i, r) * n,
        p = a.curve;
      p &&
        ((c = Qt(p, e, c, d, 0, h, m, o, f, n)),
        (c = Qt(p, e, c, d, 1, h, m, l, g, n))),
        (h = m),
        (o = f),
        (l = g),
        (a = u);
    }
  }
  function Qt(t, e, s, i, r, n, a, h, o, l) {
    if ("stepped" == t) return e.setStepped(i), s;
    let c = r << 2,
      d = t[c],
      u = t[c + 1] * l,
      m = t[c + 2],
      f = t[c + 3] * l;
    return e.setBezier(s, i, r, n, h, d, u, m, f, a, o), s + 1;
  }
  function Zt(t, e, s) {
    return void 0 !== t[e] ? t[e] : s;
  }
  void 0 === Math.fround &&
    (Math.fround =
      ((gt = new Float32Array(1)),
      function (t) {
        return (gt[0] = t), gt[0];
      }));
  class te {
    constructor(t, e = { alpha: "true" }) {
      if (
        ((this.restorables = new Array()),
        t instanceof WebGLRenderingContext ||
          ("undefined" != typeof WebGL2RenderingContext &&
            t instanceof WebGL2RenderingContext))
      )
        (this.gl = t), (this.canvas = this.gl.canvas);
      else {
        let s = t;
        (this.gl = s.getContext("webgl2", e) || s.getContext("webgl", e)),
          (this.canvas = s),
          s.addEventListener("webglcontextlost", (t) => {
            t && t.preventDefault();
          }),
          s.addEventListener("webglcontextrestored", (t) => {
            for (let t = 0, e = this.restorables.length; t < e; t++)
              this.restorables[t].restore();
          });
      }
    }
    addRestorable(t) {
      this.restorables.push(t);
    }
    removeRestorable(t) {
      let e = this.restorables.indexOf(t);
      e > -1 && this.restorables.splice(e, 1);
    }
  }
  class ee {
    static getDestGLBlendMode(t) {
      switch (t) {
        case dt.Normal:
          return 771;
        case dt.Additive:
          return 1;
        case dt.Multiply:
        case dt.Screen:
          return 771;
        default:
          throw new Error("Unknown blend mode: " + t);
      }
    }
    static getSourceColorGLBlendMode(t, e = !1) {
      switch (t) {
        case dt.Normal:
        case dt.Additive:
          return e ? 1 : 770;
        case dt.Multiply:
          return 774;
        case dt.Screen:
          return 1;
        default:
          throw new Error("Unknown blend mode: " + t);
      }
    }
    static getSourceAlphaGLBlendMode(t) {
      switch (t) {
        case dt.Normal:
        case dt.Additive:
          return 1;
        case dt.Multiply:
          return 771;
        case dt.Screen:
          return 769;
        default:
          throw new Error("Unknown blend mode: " + t);
      }
    }
  }
  class se extends rt {
    constructor(t, e, s = !1) {
      super(e),
        (this.texture = null),
        (this.boundUnit = 0),
        (this.useMipMaps = !1),
        (this.context = t instanceof te ? t : new te(t)),
        (this.useMipMaps = s),
        this.restore(),
        this.context.addRestorable(this);
    }
    setFilters(t, e) {
      let s = this.context.gl;
      this.bind(),
        s.texParameteri(s.TEXTURE_2D, s.TEXTURE_MIN_FILTER, t),
        s.texParameteri(
          s.TEXTURE_2D,
          s.TEXTURE_MAG_FILTER,
          se.validateMagFilter(e)
        ),
        (this.useMipMaps = se.usesMipMaps(t)),
        this.useMipMaps && s.generateMipmap(s.TEXTURE_2D);
    }
    static validateMagFilter(t) {
      switch (t) {
        case nt.MipMap:
        case nt.MipMapLinearLinear:
        case nt.MipMapLinearNearest:
        case nt.MipMapNearestLinear:
        case nt.MipMapNearestNearest:
          return nt.Linear;
        default:
          return t;
      }
    }
    static usesMipMaps(t) {
      switch (t) {
        case nt.MipMap:
        case nt.MipMapLinearLinear:
        case nt.MipMapLinearNearest:
        case nt.MipMapNearestLinear:
        case nt.MipMapNearestNearest:
          return !0;
        default:
          return !1;
      }
    }
    setWraps(t, e) {
      let s = this.context.gl;
      this.bind(),
        s.texParameteri(s.TEXTURE_2D, s.TEXTURE_WRAP_S, t),
        s.texParameteri(s.TEXTURE_2D, s.TEXTURE_WRAP_T, e);
    }
    update(t) {
      let e = this.context.gl;
      this.texture || (this.texture = this.context.gl.createTexture()),
        this.bind(),
        se.DISABLE_UNPACK_PREMULTIPLIED_ALPHA_WEBGL &&
          e.pixelStorei(e.UNPACK_PREMULTIPLY_ALPHA_WEBGL, !1),
        e.texImage2D(
          e.TEXTURE_2D,
          0,
          e.RGBA,
          e.RGBA,
          e.UNSIGNED_BYTE,
          this._image
        ),
        e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MAG_FILTER, e.LINEAR),
        e.texParameteri(
          e.TEXTURE_2D,
          e.TEXTURE_MIN_FILTER,
          t ? e.LINEAR_MIPMAP_LINEAR : e.LINEAR
        ),
        e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_S, e.CLAMP_TO_EDGE),
        e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_T, e.CLAMP_TO_EDGE),
        t && e.generateMipmap(e.TEXTURE_2D);
    }
    restore() {
      (this.texture = null), this.update(this.useMipMaps);
    }
    bind(t = 0) {
      let e = this.context.gl;
      (this.boundUnit = t),
        e.activeTexture(e.TEXTURE0 + t),
        e.bindTexture(e.TEXTURE_2D, this.texture);
    }
    unbind() {
      let t = this.context.gl;
      t.activeTexture(t.TEXTURE0 + this.boundUnit),
        t.bindTexture(t.TEXTURE_2D, null);
    }
    dispose() {
      this.context.removeRestorable(this),
        this.context.gl.deleteTexture(this.texture);
    }
  }
  se.DISABLE_UNPACK_PREMULTIPLIED_ALPHA_WEBGL = !1;
  class ie extends Tt {
    constructor(t, e = "", s = new Rt()) {
      super((e) => new se(t, e), e, s);
    }
  }
  class re {
    constructor(t = 0, e = 0, s = 0) {
      (this.x = 0),
        (this.y = 0),
        (this.z = 0),
        (this.x = t),
        (this.y = e),
        (this.z = s);
    }
    setFrom(t) {
      return (this.x = t.x), (this.y = t.y), (this.z = t.z), this;
    }
    set(t, e, s) {
      return (this.x = t), (this.y = e), (this.z = s), this;
    }
    add(t) {
      return (this.x += t.x), (this.y += t.y), (this.z += t.z), this;
    }
    sub(t) {
      return (this.x -= t.x), (this.y -= t.y), (this.z -= t.z), this;
    }
    scale(t) {
      return (this.x *= t), (this.y *= t), (this.z *= t), this;
    }
    normalize() {
      let t = this.length();
      return (
        0 == t || ((t = 1 / t), (this.x *= t), (this.y *= t), (this.z *= t)),
        this
      );
    }
    cross(t) {
      return this.set(
        this.y * t.z - this.z * t.y,
        this.z * t.x - this.x * t.z,
        this.x * t.y - this.y * t.x
      );
    }
    multiply(t) {
      let e = t.values;
      return this.set(
        this.x * e[ne] + this.y * e[ae] + this.z * e[he] + e[oe],
        this.x * e[le] + this.y * e[ce] + this.z * e[de] + e[ue],
        this.x * e[me] + this.y * e[fe] + this.z * e[ge] + e[pe]
      );
    }
    project(t) {
      let e = t.values,
        s = 1 / (this.x * e[xe] + this.y * e[we] + this.z * e[be] + e[ve]);
      return this.set(
        (this.x * e[ne] + this.y * e[ae] + this.z * e[he] + e[oe]) * s,
        (this.x * e[le] + this.y * e[ce] + this.z * e[de] + e[ue]) * s,
        (this.x * e[me] + this.y * e[fe] + this.z * e[ge] + e[pe]) * s
      );
    }
    dot(t) {
      return this.x * t.x + this.y * t.y + this.z * t.z;
    }
    length() {
      return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }
    distance(t) {
      let e = t.x - this.x,
        s = t.y - this.y,
        i = t.z - this.z;
      return Math.sqrt(e * e + s * s + i * i);
    }
  }
  const ne = 0,
    ae = 4,
    he = 8,
    oe = 12,
    le = 1,
    ce = 5,
    de = 9,
    ue = 13,
    me = 2,
    fe = 6,
    ge = 10,
    pe = 14,
    xe = 3,
    we = 7,
    be = 11,
    ve = 15;
  class ye {
    constructor() {
      (this.temp = new Float32Array(16)), (this.values = new Float32Array(16));
      let t = this.values;
      (t[ne] = 1), (t[ce] = 1), (t[ge] = 1), (t[ve] = 1);
    }
    set(t) {
      return this.values.set(t), this;
    }
    transpose() {
      let t = this.temp,
        e = this.values;
      return (
        (t[ne] = e[ne]),
        (t[ae] = e[le]),
        (t[he] = e[me]),
        (t[oe] = e[xe]),
        (t[le] = e[ae]),
        (t[ce] = e[ce]),
        (t[de] = e[fe]),
        (t[ue] = e[we]),
        (t[me] = e[he]),
        (t[fe] = e[de]),
        (t[ge] = e[ge]),
        (t[pe] = e[be]),
        (t[xe] = e[oe]),
        (t[we] = e[ue]),
        (t[be] = e[pe]),
        (t[ve] = e[ve]),
        this.set(t)
      );
    }
    identity() {
      let t = this.values;
      return (
        (t[ne] = 1),
        (t[ae] = 0),
        (t[he] = 0),
        (t[oe] = 0),
        (t[le] = 0),
        (t[ce] = 1),
        (t[de] = 0),
        (t[ue] = 0),
        (t[me] = 0),
        (t[fe] = 0),
        (t[ge] = 1),
        (t[pe] = 0),
        (t[xe] = 0),
        (t[we] = 0),
        (t[be] = 0),
        (t[ve] = 1),
        this
      );
    }
    invert() {
      let t = this.values,
        e = this.temp,
        s =
          t[xe] * t[fe] * t[de] * t[oe] -
          t[me] * t[we] * t[de] * t[oe] -
          t[xe] * t[ce] * t[ge] * t[oe] +
          t[le] * t[we] * t[ge] * t[oe] +
          t[me] * t[ce] * t[be] * t[oe] -
          t[le] * t[fe] * t[be] * t[oe] -
          t[xe] * t[fe] * t[he] * t[ue] +
          t[me] * t[we] * t[he] * t[ue] +
          t[xe] * t[ae] * t[ge] * t[ue] -
          t[ne] * t[we] * t[ge] * t[ue] -
          t[me] * t[ae] * t[be] * t[ue] +
          t[ne] * t[fe] * t[be] * t[ue] +
          t[xe] * t[ce] * t[he] * t[pe] -
          t[le] * t[we] * t[he] * t[pe] -
          t[xe] * t[ae] * t[de] * t[pe] +
          t[ne] * t[we] * t[de] * t[pe] +
          t[le] * t[ae] * t[be] * t[pe] -
          t[ne] * t[ce] * t[be] * t[pe] -
          t[me] * t[ce] * t[he] * t[ve] +
          t[le] * t[fe] * t[he] * t[ve] +
          t[me] * t[ae] * t[de] * t[ve] -
          t[ne] * t[fe] * t[de] * t[ve] -
          t[le] * t[ae] * t[ge] * t[ve] +
          t[ne] * t[ce] * t[ge] * t[ve];
      if (0 == s) throw new Error("non-invertible matrix");
      let i = 1 / s;
      return (
        (e[ne] =
          t[de] * t[pe] * t[we] -
          t[ue] * t[ge] * t[we] +
          t[ue] * t[fe] * t[be] -
          t[ce] * t[pe] * t[be] -
          t[de] * t[fe] * t[ve] +
          t[ce] * t[ge] * t[ve]),
        (e[ae] =
          t[oe] * t[ge] * t[we] -
          t[he] * t[pe] * t[we] -
          t[oe] * t[fe] * t[be] +
          t[ae] * t[pe] * t[be] +
          t[he] * t[fe] * t[ve] -
          t[ae] * t[ge] * t[ve]),
        (e[he] =
          t[he] * t[ue] * t[we] -
          t[oe] * t[de] * t[we] +
          t[oe] * t[ce] * t[be] -
          t[ae] * t[ue] * t[be] -
          t[he] * t[ce] * t[ve] +
          t[ae] * t[de] * t[ve]),
        (e[oe] =
          t[oe] * t[de] * t[fe] -
          t[he] * t[ue] * t[fe] -
          t[oe] * t[ce] * t[ge] +
          t[ae] * t[ue] * t[ge] +
          t[he] * t[ce] * t[pe] -
          t[ae] * t[de] * t[pe]),
        (e[le] =
          t[ue] * t[ge] * t[xe] -
          t[de] * t[pe] * t[xe] -
          t[ue] * t[me] * t[be] +
          t[le] * t[pe] * t[be] +
          t[de] * t[me] * t[ve] -
          t[le] * t[ge] * t[ve]),
        (e[ce] =
          t[he] * t[pe] * t[xe] -
          t[oe] * t[ge] * t[xe] +
          t[oe] * t[me] * t[be] -
          t[ne] * t[pe] * t[be] -
          t[he] * t[me] * t[ve] +
          t[ne] * t[ge] * t[ve]),
        (e[de] =
          t[oe] * t[de] * t[xe] -
          t[he] * t[ue] * t[xe] -
          t[oe] * t[le] * t[be] +
          t[ne] * t[ue] * t[be] +
          t[he] * t[le] * t[ve] -
          t[ne] * t[de] * t[ve]),
        (e[ue] =
          t[he] * t[ue] * t[me] -
          t[oe] * t[de] * t[me] +
          t[oe] * t[le] * t[ge] -
          t[ne] * t[ue] * t[ge] -
          t[he] * t[le] * t[pe] +
          t[ne] * t[de] * t[pe]),
        (e[me] =
          t[ce] * t[pe] * t[xe] -
          t[ue] * t[fe] * t[xe] +
          t[ue] * t[me] * t[we] -
          t[le] * t[pe] * t[we] -
          t[ce] * t[me] * t[ve] +
          t[le] * t[fe] * t[ve]),
        (e[fe] =
          t[oe] * t[fe] * t[xe] -
          t[ae] * t[pe] * t[xe] -
          t[oe] * t[me] * t[we] +
          t[ne] * t[pe] * t[we] +
          t[ae] * t[me] * t[ve] -
          t[ne] * t[fe] * t[ve]),
        (e[ge] =
          t[ae] * t[ue] * t[xe] -
          t[oe] * t[ce] * t[xe] +
          t[oe] * t[le] * t[we] -
          t[ne] * t[ue] * t[we] -
          t[ae] * t[le] * t[ve] +
          t[ne] * t[ce] * t[ve]),
        (e[pe] =
          t[oe] * t[ce] * t[me] -
          t[ae] * t[ue] * t[me] -
          t[oe] * t[le] * t[fe] +
          t[ne] * t[ue] * t[fe] +
          t[ae] * t[le] * t[pe] -
          t[ne] * t[ce] * t[pe]),
        (e[xe] =
          t[de] * t[fe] * t[xe] -
          t[ce] * t[ge] * t[xe] -
          t[de] * t[me] * t[we] +
          t[le] * t[ge] * t[we] +
          t[ce] * t[me] * t[be] -
          t[le] * t[fe] * t[be]),
        (e[we] =
          t[ae] * t[ge] * t[xe] -
          t[he] * t[fe] * t[xe] +
          t[he] * t[me] * t[we] -
          t[ne] * t[ge] * t[we] -
          t[ae] * t[me] * t[be] +
          t[ne] * t[fe] * t[be]),
        (e[be] =
          t[he] * t[ce] * t[xe] -
          t[ae] * t[de] * t[xe] -
          t[he] * t[le] * t[we] +
          t[ne] * t[de] * t[we] +
          t[ae] * t[le] * t[be] -
          t[ne] * t[ce] * t[be]),
        (e[ve] =
          t[ae] * t[de] * t[me] -
          t[he] * t[ce] * t[me] +
          t[he] * t[le] * t[fe] -
          t[ne] * t[de] * t[fe] -
          t[ae] * t[le] * t[ge] +
          t[ne] * t[ce] * t[ge]),
        (t[ne] = e[ne] * i),
        (t[ae] = e[ae] * i),
        (t[he] = e[he] * i),
        (t[oe] = e[oe] * i),
        (t[le] = e[le] * i),
        (t[ce] = e[ce] * i),
        (t[de] = e[de] * i),
        (t[ue] = e[ue] * i),
        (t[me] = e[me] * i),
        (t[fe] = e[fe] * i),
        (t[ge] = e[ge] * i),
        (t[pe] = e[pe] * i),
        (t[xe] = e[xe] * i),
        (t[we] = e[we] * i),
        (t[be] = e[be] * i),
        (t[ve] = e[ve] * i),
        this
      );
    }
    determinant() {
      let t = this.values;
      return (
        t[xe] * t[fe] * t[de] * t[oe] -
        t[me] * t[we] * t[de] * t[oe] -
        t[xe] * t[ce] * t[ge] * t[oe] +
        t[le] * t[we] * t[ge] * t[oe] +
        t[me] * t[ce] * t[be] * t[oe] -
        t[le] * t[fe] * t[be] * t[oe] -
        t[xe] * t[fe] * t[he] * t[ue] +
        t[me] * t[we] * t[he] * t[ue] +
        t[xe] * t[ae] * t[ge] * t[ue] -
        t[ne] * t[we] * t[ge] * t[ue] -
        t[me] * t[ae] * t[be] * t[ue] +
        t[ne] * t[fe] * t[be] * t[ue] +
        t[xe] * t[ce] * t[he] * t[pe] -
        t[le] * t[we] * t[he] * t[pe] -
        t[xe] * t[ae] * t[de] * t[pe] +
        t[ne] * t[we] * t[de] * t[pe] +
        t[le] * t[ae] * t[be] * t[pe] -
        t[ne] * t[ce] * t[be] * t[pe] -
        t[me] * t[ce] * t[he] * t[ve] +
        t[le] * t[fe] * t[he] * t[ve] +
        t[me] * t[ae] * t[de] * t[ve] -
        t[ne] * t[fe] * t[de] * t[ve] -
        t[le] * t[ae] * t[ge] * t[ve] +
        t[ne] * t[ce] * t[ge] * t[ve]
      );
    }
    translate(t, e, s) {
      let i = this.values;
      return (i[oe] += t), (i[ue] += e), (i[pe] += s), this;
    }
    copy() {
      return new ye().set(this.values);
    }
    projection(t, e, s, i) {
      this.identity();
      let r = 1 / Math.tan((s * (Math.PI / 180)) / 2),
        n = (e + t) / (t - e),
        a = (2 * e * t) / (t - e),
        h = this.values;
      return (
        (h[ne] = r / i),
        (h[le] = 0),
        (h[me] = 0),
        (h[xe] = 0),
        (h[ae] = 0),
        (h[ce] = r),
        (h[fe] = 0),
        (h[we] = 0),
        (h[he] = 0),
        (h[de] = 0),
        (h[ge] = n),
        (h[be] = -1),
        (h[oe] = 0),
        (h[ue] = 0),
        (h[pe] = a),
        (h[ve] = 0),
        this
      );
    }
    ortho2d(t, e, s, i) {
      return this.ortho(t, t + s, e, e + i, 0, 1);
    }
    ortho(t, e, s, i, r, n) {
      this.identity();
      let a = 2 / (e - t),
        h = 2 / (i - s),
        o = -2 / (n - r),
        l = -(e + t) / (e - t),
        c = -(i + s) / (i - s),
        d = -(n + r) / (n - r),
        u = this.values;
      return (
        (u[ne] = a),
        (u[le] = 0),
        (u[me] = 0),
        (u[xe] = 0),
        (u[ae] = 0),
        (u[ce] = h),
        (u[fe] = 0),
        (u[we] = 0),
        (u[he] = 0),
        (u[de] = 0),
        (u[ge] = o),
        (u[be] = 0),
        (u[oe] = l),
        (u[ue] = c),
        (u[pe] = d),
        (u[ve] = 1),
        this
      );
    }
    multiply(t) {
      let e = this.temp,
        s = this.values,
        i = t.values;
      return (
        (e[ne] = s[ne] * i[ne] + s[ae] * i[le] + s[he] * i[me] + s[oe] * i[xe]),
        (e[ae] = s[ne] * i[ae] + s[ae] * i[ce] + s[he] * i[fe] + s[oe] * i[we]),
        (e[he] = s[ne] * i[he] + s[ae] * i[de] + s[he] * i[ge] + s[oe] * i[be]),
        (e[oe] = s[ne] * i[oe] + s[ae] * i[ue] + s[he] * i[pe] + s[oe] * i[ve]),
        (e[le] = s[le] * i[ne] + s[ce] * i[le] + s[de] * i[me] + s[ue] * i[xe]),
        (e[ce] = s[le] * i[ae] + s[ce] * i[ce] + s[de] * i[fe] + s[ue] * i[we]),
        (e[de] = s[le] * i[he] + s[ce] * i[de] + s[de] * i[ge] + s[ue] * i[be]),
        (e[ue] = s[le] * i[oe] + s[ce] * i[ue] + s[de] * i[pe] + s[ue] * i[ve]),
        (e[me] = s[me] * i[ne] + s[fe] * i[le] + s[ge] * i[me] + s[pe] * i[xe]),
        (e[fe] = s[me] * i[ae] + s[fe] * i[ce] + s[ge] * i[fe] + s[pe] * i[we]),
        (e[ge] = s[me] * i[he] + s[fe] * i[de] + s[ge] * i[ge] + s[pe] * i[be]),
        (e[pe] = s[me] * i[oe] + s[fe] * i[ue] + s[ge] * i[pe] + s[pe] * i[ve]),
        (e[xe] = s[xe] * i[ne] + s[we] * i[le] + s[be] * i[me] + s[ve] * i[xe]),
        (e[we] = s[xe] * i[ae] + s[we] * i[ce] + s[be] * i[fe] + s[ve] * i[we]),
        (e[be] = s[xe] * i[he] + s[we] * i[de] + s[be] * i[ge] + s[ve] * i[be]),
        (e[ve] = s[xe] * i[oe] + s[we] * i[ue] + s[be] * i[pe] + s[ve] * i[ve]),
        this.set(this.temp)
      );
    }
    multiplyLeft(t) {
      let e = this.temp,
        s = this.values,
        i = t.values;
      return (
        (e[ne] = i[ne] * s[ne] + i[ae] * s[le] + i[he] * s[me] + i[oe] * s[xe]),
        (e[ae] = i[ne] * s[ae] + i[ae] * s[ce] + i[he] * s[fe] + i[oe] * s[we]),
        (e[he] = i[ne] * s[he] + i[ae] * s[de] + i[he] * s[ge] + i[oe] * s[be]),
        (e[oe] = i[ne] * s[oe] + i[ae] * s[ue] + i[he] * s[pe] + i[oe] * s[ve]),
        (e[le] = i[le] * s[ne] + i[ce] * s[le] + i[de] * s[me] + i[ue] * s[xe]),
        (e[ce] = i[le] * s[ae] + i[ce] * s[ce] + i[de] * s[fe] + i[ue] * s[we]),
        (e[de] = i[le] * s[he] + i[ce] * s[de] + i[de] * s[ge] + i[ue] * s[be]),
        (e[ue] = i[le] * s[oe] + i[ce] * s[ue] + i[de] * s[pe] + i[ue] * s[ve]),
        (e[me] = i[me] * s[ne] + i[fe] * s[le] + i[ge] * s[me] + i[pe] * s[xe]),
        (e[fe] = i[me] * s[ae] + i[fe] * s[ce] + i[ge] * s[fe] + i[pe] * s[we]),
        (e[ge] = i[me] * s[he] + i[fe] * s[de] + i[ge] * s[ge] + i[pe] * s[be]),
        (e[pe] = i[me] * s[oe] + i[fe] * s[ue] + i[ge] * s[pe] + i[pe] * s[ve]),
        (e[xe] = i[xe] * s[ne] + i[we] * s[le] + i[be] * s[me] + i[ve] * s[xe]),
        (e[we] = i[xe] * s[ae] + i[we] * s[ce] + i[be] * s[fe] + i[ve] * s[we]),
        (e[be] = i[xe] * s[he] + i[we] * s[de] + i[be] * s[ge] + i[ve] * s[be]),
        (e[ve] = i[xe] * s[oe] + i[we] * s[ue] + i[be] * s[pe] + i[ve] * s[ve]),
        this.set(this.temp)
      );
    }
    lookAt(t, e, s) {
      let i = ye.xAxis,
        r = ye.yAxis,
        n = ye.zAxis;
      n.setFrom(e).normalize(),
        i.setFrom(e).normalize(),
        i.cross(s).normalize(),
        r.setFrom(i).cross(n).normalize(),
        this.identity();
      let a = this.values;
      return (
        (a[ne] = i.x),
        (a[ae] = i.y),
        (a[he] = i.z),
        (a[le] = r.x),
        (a[ce] = r.y),
        (a[de] = r.z),
        (a[me] = -n.x),
        (a[fe] = -n.y),
        (a[ge] = -n.z),
        ye.tmpMatrix.identity(),
        (ye.tmpMatrix.values[oe] = -t.x),
        (ye.tmpMatrix.values[ue] = -t.y),
        (ye.tmpMatrix.values[pe] = -t.z),
        this.multiply(ye.tmpMatrix),
        this
      );
    }
  }
  (ye.xAxis = new re()),
    (ye.yAxis = new re()),
    (ye.zAxis = new re()),
    (ye.tmpMatrix = new ye());
  class Ae {
    constructor(t, e) {
      (this.position = new re(0, 0, 0)),
        (this.direction = new re(0, 0, -1)),
        (this.up = new re(0, 1, 0)),
        (this.near = 0),
        (this.far = 100),
        (this.zoom = 1),
        (this.viewportWidth = 0),
        (this.viewportHeight = 0),
        (this.projectionView = new ye()),
        (this.inverseProjectionView = new ye()),
        (this.projection = new ye()),
        (this.view = new ye()),
        (this.viewportWidth = t),
        (this.viewportHeight = e),
        this.update();
    }
    update() {
      let t = this.projection,
        e = this.view,
        s = this.projectionView,
        i = this.inverseProjectionView,
        r = this.zoom,
        n = this.viewportWidth,
        a = this.viewportHeight;
      t.ortho(
        r * (-n / 2),
        r * (n / 2),
        r * (-a / 2),
        r * (a / 2),
        this.near,
        this.far
      ),
        e.lookAt(this.position, this.direction, this.up),
        s.set(t.values),
        s.multiply(e),
        i.set(s.values).invert();
    }
    screenToWorld(t, e, s) {
      let i = t.x,
        r = s - t.y - 1;
      return (
        (t.x = (2 * i) / e - 1),
        (t.y = (2 * r) / s - 1),
        (t.z = 2 * t.z - 1),
        t.project(this.inverseProjectionView),
        t
      );
    }
    worldToScreen(t, e, s) {
      return (
        t.project(this.projectionView),
        (t.x = (e * (t.x + 1)) / 2),
        (t.y = (s * (t.y + 1)) / 2),
        (t.z = (t.z + 1) / 2),
        t
      );
    }
    setViewport(t, e) {
      (this.viewportWidth = t), (this.viewportHeight = e);
    }
  }
  class Ce {
    constructor(t) {
      (this.mouseX = 0),
        (this.mouseY = 0),
        (this.buttonDown = !1),
        (this.touch0 = null),
        (this.touch1 = null),
        (this.initialPinchDistance = 0),
        (this.listeners = new Array()),
        (this.eventListeners = []),
        (this.element = t),
        this.setupCallbacks(t);
    }
    setupCallbacks(t) {
      let e = (e) => {
          if (e instanceof MouseEvent) {
            let s = t.getBoundingClientRect();
            (this.mouseX = e.clientX - s.left),
              (this.mouseY = e.clientY - s.top),
              this.listeners.map((t) => {
                this.buttonDown
                  ? t.dragged && t.dragged(this.mouseX, this.mouseY)
                  : t.moved && t.moved(this.mouseX, this.mouseY);
              });
          }
        },
        s = (i) => {
          if (i instanceof MouseEvent) {
            let r = t.getBoundingClientRect();
            (this.mouseX = i.clientX - r.left),
              (this.mouseY = i.clientY - r.top),
              (this.buttonDown = !1),
              this.listeners.map((t) => {
                t.up && t.up(this.mouseX, this.mouseY);
              }),
              document.removeEventListener("mousemove", e),
              document.removeEventListener("mouseup", s);
          }
        };
      t.addEventListener(
        "mousedown",
        (i) => {
          if (i instanceof MouseEvent) {
            let r = t.getBoundingClientRect();
            (this.mouseX = i.clientX - r.left),
              (this.mouseY = i.clientY - r.top),
              (this.buttonDown = !0),
              this.listeners.map((t) => {
                t.down && t.down(this.mouseX, this.mouseY);
              }),
              document.addEventListener("mousemove", e),
              document.addEventListener("mouseup", s);
          }
        },
        !0
      ),
        t.addEventListener("mousemove", e, !0),
        t.addEventListener("mouseup", s, !0),
        t.addEventListener(
          "wheel",
          (t) => {
            t.preventDefault();
            let e = t.deltaY;
            t.deltaMode == WheelEvent.DOM_DELTA_LINE && (e *= 8),
              t.deltaMode == WheelEvent.DOM_DELTA_PAGE && (e *= 24),
              this.listeners.map((e) => {
                e.wheel && e.wheel(t.deltaY);
              });
          },
          !0
        ),
        t.addEventListener(
          "touchstart",
          (e) => {
            if (!this.touch0 || !this.touch1) {
              let s = e.changedTouches.item(0);
              if (!s) return;
              let i = t.getBoundingClientRect(),
                r = s.clientX - i.left,
                n = s.clientY - i.top,
                a = new ke(s.identifier, r, n);
              if (
                ((this.mouseX = r),
                (this.mouseY = n),
                (this.buttonDown = !0),
                this.touch0)
              ) {
                if (!this.touch1) {
                  this.touch1 = a;
                  let t = this.touch1.x - this.touch0.x,
                    e = this.touch1.x - this.touch0.x;
                  (this.initialPinchDistance = Math.sqrt(t * t + e * e)),
                    this.listeners.map((t) => {
                      t.zoom &&
                        t.zoom(
                          this.initialPinchDistance,
                          this.initialPinchDistance
                        );
                    });
                }
              } else
                (this.touch0 = a),
                  this.listeners.map((t) => {
                    t.down && t.down(a.x, a.y);
                  });
            }
            e.preventDefault();
          },
          !1
        ),
        t.addEventListener(
          "touchmove",
          (e) => {
            if (this.touch0) {
              var s = e.changedTouches;
              let n = t.getBoundingClientRect();
              for (var i = 0; i < s.length; i++) {
                var r = s[i];
                let t = r.clientX - n.left,
                  e = r.clientY - n.top;
                this.touch0.identifier === r.identifier &&
                  ((this.touch0.x = this.mouseX = t),
                  (this.touch0.y = this.mouseY = e),
                  this.listeners.map((s) => {
                    s.dragged && s.dragged(t, e);
                  })),
                  this.touch1 &&
                    this.touch1.identifier === r.identifier &&
                    ((this.touch1.x = this.mouseX = t),
                    (this.touch1.y = this.mouseY = e));
              }
              if (this.touch0 && this.touch1) {
                let t = this.touch1.x - this.touch0.x,
                  e = this.touch1.x - this.touch0.x,
                  s = Math.sqrt(t * t + e * e);
                this.listeners.map((t) => {
                  t.zoom && t.zoom(this.initialPinchDistance, s);
                });
              }
            }
            e.preventDefault();
          },
          !1
        );
      let i = (e) => {
        if (this.touch0) {
          var s = e.changedTouches;
          let n = t.getBoundingClientRect();
          for (var i = 0; i < s.length; i++) {
            var r = s[i];
            let t = r.clientX - n.left,
              e = r.clientY - n.top;
            if (this.touch0.identifier === r.identifier) {
              if (
                ((this.touch0 = null),
                (this.mouseX = t),
                (this.mouseY = e),
                this.listeners.map((s) => {
                  s.up && s.up(t, e);
                }),
                !this.touch1)
              ) {
                this.buttonDown = !1;
                break;
              }
              (this.touch0 = this.touch1),
                (this.touch1 = null),
                (this.mouseX = this.touch0.x),
                (this.mouseX = this.touch0.x),
                (this.buttonDown = !0),
                this.listeners.map((t) => {
                  t.down && t.down(this.touch0.x, this.touch0.y);
                });
            }
            this.touch1 && this.touch1.identifier && (this.touch1 = null);
          }
        }
        e.preventDefault();
      };
      t.addEventListener("touchend", i, !1),
        t.addEventListener("touchcancel", i);
    }
    addListener(t) {
      this.listeners.push(t);
    }
    removeListener(t) {
      let e = this.listeners.indexOf(t);
      e > -1 && this.listeners.splice(e, 1);
    }
  }
  class ke {
    constructor(t, e, s) {
      (this.identifier = t), (this.x = e), (this.y = s);
    }
  }
  class Se {
    constructor(t, e, s) {
      (this.vertexShader = e),
        (this.fragmentShader = s),
        (this.vs = null),
        (this.fs = null),
        (this.program = null),
        (this.tmp2x2 = new Float32Array(4)),
        (this.tmp3x3 = new Float32Array(9)),
        (this.tmp4x4 = new Float32Array(16)),
        (this.vsSource = e),
        (this.fsSource = s),
        (this.context = t instanceof te ? t : new te(t)),
        this.context.addRestorable(this),
        this.compile();
    }
    getProgram() {
      return this.program;
    }
    getVertexShader() {
      return this.vertexShader;
    }
    getFragmentShader() {
      return this.fragmentShader;
    }
    getVertexShaderSource() {
      return this.vsSource;
    }
    getFragmentSource() {
      return this.fsSource;
    }
    compile() {
      let t = this.context.gl;
      try {
        if (
          ((this.vs = this.compileShader(t.VERTEX_SHADER, this.vertexShader)),
          !this.vs)
        )
          throw new Error("Couldn't compile vertex shader.");
        if (
          ((this.fs = this.compileShader(
            t.FRAGMENT_SHADER,
            this.fragmentShader
          )),
          !this.fs)
        )
          throw new Error("Couldn#t compile fragment shader.");
        this.program = this.compileProgram(this.vs, this.fs);
      } catch (t) {
        throw (this.dispose(), t);
      }
    }
    compileShader(t, e) {
      let s = this.context.gl,
        i = s.createShader(t);
      if (!i) throw new Error("Couldn't create shader.");
      if (
        (s.shaderSource(i, e),
        s.compileShader(i),
        !s.getShaderParameter(i, s.COMPILE_STATUS))
      ) {
        let t = "Couldn't compile shader: " + s.getShaderInfoLog(i);
        if ((s.deleteShader(i), !s.isContextLost())) throw new Error(t);
      }
      return i;
    }
    compileProgram(t, e) {
      let s = this.context.gl,
        i = s.createProgram();
      if (!i) throw new Error("Couldn't compile program.");
      if (
        (s.attachShader(i, t),
        s.attachShader(i, e),
        s.linkProgram(i),
        !s.getProgramParameter(i, s.LINK_STATUS))
      ) {
        let t = "Couldn't compile shader program: " + s.getProgramInfoLog(i);
        if ((s.deleteProgram(i), !s.isContextLost())) throw new Error(t);
      }
      return i;
    }
    restore() {
      this.compile();
    }
    bind() {
      this.context.gl.useProgram(this.program);
    }
    unbind() {
      this.context.gl.useProgram(null);
    }
    setUniformi(t, e) {
      this.context.gl.uniform1i(this.getUniformLocation(t), e);
    }
    setUniformf(t, e) {
      this.context.gl.uniform1f(this.getUniformLocation(t), e);
    }
    setUniform2f(t, e, s) {
      this.context.gl.uniform2f(this.getUniformLocation(t), e, s);
    }
    setUniform3f(t, e, s, i) {
      this.context.gl.uniform3f(this.getUniformLocation(t), e, s, i);
    }
    setUniform4f(t, e, s, i, r) {
      this.context.gl.uniform4f(this.getUniformLocation(t), e, s, i, r);
    }
    setUniform2x2f(t, e) {
      let s = this.context.gl;
      this.tmp2x2.set(e),
        s.uniformMatrix2fv(this.getUniformLocation(t), !1, this.tmp2x2);
    }
    setUniform3x3f(t, e) {
      let s = this.context.gl;
      this.tmp3x3.set(e),
        s.uniformMatrix3fv(this.getUniformLocation(t), !1, this.tmp3x3);
    }
    setUniform4x4f(t, e) {
      let s = this.context.gl;
      this.tmp4x4.set(e),
        s.uniformMatrix4fv(this.getUniformLocation(t), !1, this.tmp4x4);
    }
    getUniformLocation(t) {
      let e = this.context.gl;
      if (!this.program) throw new Error("Shader not compiled.");
      let s = e.getUniformLocation(this.program, t);
      if (!s && !e.isContextLost())
        throw new Error(`Couldn't find location for uniform ${t}`);
      return s;
    }
    getAttributeLocation(t) {
      let e = this.context.gl;
      if (!this.program) throw new Error("Shader not compiled.");
      let s = e.getAttribLocation(this.program, t);
      if (-1 == s && !e.isContextLost())
        throw new Error(`Couldn't find location for attribute ${t}`);
      return s;
    }
    dispose() {
      this.context.removeRestorable(this);
      let t = this.context.gl;
      this.vs && (t.deleteShader(this.vs), (this.vs = null)),
        this.fs && (t.deleteShader(this.fs), (this.fs = null)),
        this.program && (t.deleteProgram(this.program), (this.program = null));
    }
    static newColoredTextured(t) {
      let e = `\n\t\t\t\tattribute vec4 ${Se.POSITION};\n\t\t\t\tattribute vec4 ${Se.COLOR};\n\t\t\t\tattribute vec2 ${Se.TEXCOORDS};\n\t\t\t\tuniform mat4 ${Se.MVP_MATRIX};\n\t\t\t\tvarying vec4 v_color;\n\t\t\t\tvarying vec2 v_texCoords;\n\n\t\t\t\tvoid main () {\n\t\t\t\t\tv_color = ${Se.COLOR};\n\t\t\t\t\tv_texCoords = ${Se.TEXCOORDS};\n\t\t\t\t\tgl_Position = ${Se.MVP_MATRIX} * ${Se.POSITION};\n\t\t\t\t}\n\t\t\t`;
      return new Se(
        t,
        e,
        "\n\t\t\t\t#ifdef GL_ES\n\t\t\t\t\t#define LOWP lowp\n\t\t\t\t\tprecision mediump float;\n\t\t\t\t#else\n\t\t\t\t\t#define LOWP\n\t\t\t\t#endif\n\t\t\t\tvarying LOWP vec4 v_color;\n\t\t\t\tvarying vec2 v_texCoords;\n\t\t\t\tuniform sampler2D u_texture;\n\n\t\t\t\tvoid main () {\n\t\t\t\t\tgl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n\t\t\t\t}\n\t\t\t"
      );
    }
    static newTwoColoredTextured(t) {
      let e = `\n\t\t\t\tattribute vec4 ${Se.POSITION};\n\t\t\t\tattribute vec4 ${Se.COLOR};\n\t\t\t\tattribute vec4 ${Se.COLOR2};\n\t\t\t\tattribute vec2 ${Se.TEXCOORDS};\n\t\t\t\tuniform mat4 ${Se.MVP_MATRIX};\n\t\t\t\tvarying vec4 v_light;\n\t\t\t\tvarying vec4 v_dark;\n\t\t\t\tvarying vec2 v_texCoords;\n\n\t\t\t\tvoid main () {\n\t\t\t\t\tv_light = ${Se.COLOR};\n\t\t\t\t\tv_dark = ${Se.COLOR2};\n\t\t\t\t\tv_texCoords = ${Se.TEXCOORDS};\n\t\t\t\t\tgl_Position = ${Se.MVP_MATRIX} * ${Se.POSITION};\n\t\t\t\t}\n\t\t\t`;
      return new Se(
        t,
        e,
        "\n\t\t\t\t#ifdef GL_ES\n\t\t\t\t\t#define LOWP lowp\n\t\t\t\t\tprecision mediump float;\n\t\t\t\t#else\n\t\t\t\t\t#define LOWP\n\t\t\t\t#endif\n\t\t\t\tvarying LOWP vec4 v_light;\n\t\t\t\tvarying LOWP vec4 v_dark;\n\t\t\t\tvarying vec2 v_texCoords;\n\t\t\t\tuniform sampler2D u_texture;\n\n\t\t\t\tvoid main () {\n\t\t\t\t\tvec4 texColor = texture2D(u_texture, v_texCoords);\n\t\t\t\t\tgl_FragColor.a = texColor.a * v_light.a;\n\t\t\t\t\tgl_FragColor.rgb = ((texColor.a - 1.0) * v_dark.a + 1.0 - texColor.rgb) * v_dark.rgb + texColor.rgb * v_light.rgb;\n\t\t\t\t}\n\t\t\t"
      );
    }
    static newColored(t) {
      let e = `\n\t\t\t\tattribute vec4 ${Se.POSITION};\n\t\t\t\tattribute vec4 ${Se.COLOR};\n\t\t\t\tuniform mat4 ${Se.MVP_MATRIX};\n\t\t\t\tvarying vec4 v_color;\n\n\t\t\t\tvoid main () {\n\t\t\t\t\tv_color = ${Se.COLOR};\n\t\t\t\t\tgl_Position = ${Se.MVP_MATRIX} * ${Se.POSITION};\n\t\t\t\t}\n\t\t\t`;
      return new Se(
        t,
        e,
        "\n\t\t\t\t#ifdef GL_ES\n\t\t\t\t\t#define LOWP lowp\n\t\t\t\t\tprecision mediump float;\n\t\t\t\t#else\n\t\t\t\t\t#define LOWP\n\t\t\t\t#endif\n\t\t\t\tvarying LOWP vec4 v_color;\n\n\t\t\t\tvoid main () {\n\t\t\t\t\tgl_FragColor = v_color;\n\t\t\t\t}\n\t\t\t"
      );
    }
  }
  (Se.MVP_MATRIX = "u_projTrans"),
    (Se.POSITION = "a_position"),
    (Se.COLOR = "a_color"),
    (Se.COLOR2 = "a_color2"),
    (Se.TEXCOORDS = "a_texCoords"),
    (Se.SAMPLER = "u_texture");
  class Ee {
    constructor(t, e, s, i) {
      (this.attributes = e),
        (this.verticesBuffer = null),
        (this.verticesLength = 0),
        (this.dirtyVertices = !1),
        (this.indicesBuffer = null),
        (this.indicesLength = 0),
        (this.dirtyIndices = !1),
        (this.elementsPerVertex = 0),
        (this.context = t instanceof te ? t : new te(t)),
        (this.elementsPerVertex = 0);
      for (let t = 0; t < e.length; t++)
        this.elementsPerVertex += e[t].numElements;
      (this.vertices = new Float32Array(s * this.elementsPerVertex)),
        (this.indices = new Uint16Array(i)),
        this.context.addRestorable(this);
    }
    getAttributes() {
      return this.attributes;
    }
    maxVertices() {
      return this.vertices.length / this.elementsPerVertex;
    }
    numVertices() {
      return this.verticesLength / this.elementsPerVertex;
    }
    setVerticesLength(t) {
      (this.dirtyVertices = !0), (this.verticesLength = t);
    }
    getVertices() {
      return this.vertices;
    }
    maxIndices() {
      return this.indices.length;
    }
    numIndices() {
      return this.indicesLength;
    }
    setIndicesLength(t) {
      (this.dirtyIndices = !0), (this.indicesLength = t);
    }
    getIndices() {
      return this.indices;
    }
    getVertexSizeInFloats() {
      let t = 0;
      for (var e = 0; e < this.attributes.length; e++)
        t += this.attributes[e].numElements;
      return t;
    }
    setVertices(t) {
      if (((this.dirtyVertices = !0), t.length > this.vertices.length))
        throw Error(
          "Mesh can't store more than " + this.maxVertices() + " vertices"
        );
      this.vertices.set(t, 0), (this.verticesLength = t.length);
    }
    setIndices(t) {
      if (((this.dirtyIndices = !0), t.length > this.indices.length))
        throw Error(
          "Mesh can't store more than " + this.maxIndices() + " indices"
        );
      this.indices.set(t, 0), (this.indicesLength = t.length);
    }
    draw(t, e) {
      this.drawWithOffset(
        t,
        e,
        0,
        this.indicesLength > 0
          ? this.indicesLength
          : this.verticesLength / this.elementsPerVertex
      );
    }
    drawWithOffset(t, e, s, i) {
      let r = this.context.gl;
      (this.dirtyVertices || this.dirtyIndices) && this.update(),
        this.bind(t),
        this.indicesLength > 0
          ? r.drawElements(e, i, r.UNSIGNED_SHORT, 2 * s)
          : r.drawArrays(e, s, i),
        this.unbind(t);
    }
    bind(t) {
      let e = this.context.gl;
      e.bindBuffer(e.ARRAY_BUFFER, this.verticesBuffer);
      let s = 0;
      for (let i = 0; i < this.attributes.length; i++) {
        let r = this.attributes[i],
          n = t.getAttributeLocation(r.name);
        e.enableVertexAttribArray(n),
          e.vertexAttribPointer(
            n,
            r.numElements,
            e.FLOAT,
            !1,
            4 * this.elementsPerVertex,
            4 * s
          ),
          (s += r.numElements);
      }
      this.indicesLength > 0 &&
        e.bindBuffer(e.ELEMENT_ARRAY_BUFFER, this.indicesBuffer);
    }
    unbind(t) {
      let e = this.context.gl;
      for (let s = 0; s < this.attributes.length; s++) {
        let i = this.attributes[s],
          r = t.getAttributeLocation(i.name);
        e.disableVertexAttribArray(r);
      }
      e.bindBuffer(e.ARRAY_BUFFER, null),
        this.indicesLength > 0 && e.bindBuffer(e.ELEMENT_ARRAY_BUFFER, null);
    }
    update() {
      let t = this.context.gl;
      this.dirtyVertices &&
        (this.verticesBuffer || (this.verticesBuffer = t.createBuffer()),
        t.bindBuffer(t.ARRAY_BUFFER, this.verticesBuffer),
        t.bufferData(
          t.ARRAY_BUFFER,
          this.vertices.subarray(0, this.verticesLength),
          t.DYNAMIC_DRAW
        ),
        (this.dirtyVertices = !1)),
        this.dirtyIndices &&
          (this.indicesBuffer || (this.indicesBuffer = t.createBuffer()),
          t.bindBuffer(t.ELEMENT_ARRAY_BUFFER, this.indicesBuffer),
          t.bufferData(
            t.ELEMENT_ARRAY_BUFFER,
            this.indices.subarray(0, this.indicesLength),
            t.DYNAMIC_DRAW
          ),
          (this.dirtyIndices = !1));
    }
    restore() {
      (this.verticesBuffer = null), (this.indicesBuffer = null), this.update();
    }
    dispose() {
      this.context.removeRestorable(this);
      let t = this.context.gl;
      t.deleteBuffer(this.verticesBuffer), t.deleteBuffer(this.indicesBuffer);
    }
  }
  class Me {
    constructor(t, e, s) {
      (this.name = t), (this.type = e), (this.numElements = s);
    }
  }
  class Te extends Me {
    constructor() {
      super(Se.POSITION, Xe.Float, 2);
    }
  }
  class Re extends Me {
    constructor(t = 0) {
      super(Se.TEXCOORDS + (0 == t ? "" : t), Xe.Float, 2);
    }
  }
  class Ye extends Me {
    constructor() {
      super(Se.COLOR, Xe.Float, 4);
    }
  }
  class Ie extends Me {
    constructor() {
      super(Se.COLOR2, Xe.Float, 4);
    }
  }
  var Xe, Le;
  !(function (t) {
    t[(t.Float = 0)] = "Float";
  })(Xe || (Xe = {}));
  class Pe {
    constructor(t, e = !0, s = 10920) {
      if (
        ((this.drawCalls = 0),
        (this.isDrawing = !1),
        (this.shader = null),
        (this.lastTexture = null),
        (this.verticesLength = 0),
        (this.indicesLength = 0),
        (this.cullWasEnabled = !1),
        s > 10920)
      )
        throw new Error("Can't have more than 10920 triangles per batch: " + s);
      this.context = t instanceof te ? t : new te(t);
      let i = e
        ? [new Te(), new Ye(), new Re(), new Ie()]
        : [new Te(), new Ye(), new Re()];
      this.mesh = new Ee(t, i, s, 3 * s);
      let r = this.context.gl;
      (this.srcColorBlend = r.SRC_ALPHA),
        (this.srcAlphaBlend = r.ONE),
        (this.dstBlend = r.ONE_MINUS_SRC_ALPHA);
    }
    begin(t) {
      if (this.isDrawing)
        throw new Error(
          "PolygonBatch is already drawing. Call PolygonBatch.end() before calling PolygonBatch.begin()"
        );
      (this.drawCalls = 0),
        (this.shader = t),
        (this.lastTexture = null),
        (this.isDrawing = !0);
      let e = this.context.gl;
      e.enable(e.BLEND),
        e.blendFuncSeparate(
          this.srcColorBlend,
          this.dstBlend,
          this.srcAlphaBlend,
          this.dstBlend
        ),
        (this.cullWasEnabled = e.isEnabled(e.CULL_FACE)),
        this.cullWasEnabled && e.disable(e.CULL_FACE);
    }
    setBlendMode(t, e, s) {
      (this.srcColorBlend == t &&
        this.srcAlphaBlend == e &&
        this.dstBlend == s) ||
        ((this.srcColorBlend = t),
        (this.srcAlphaBlend = e),
        (this.dstBlend = s),
        !this.isDrawing) ||
        (this.flush(), this.context.gl.blendFuncSeparate(t, s, e, s));
    }
    draw(t, e, s) {
      t != this.lastTexture
        ? (this.flush(), (this.lastTexture = t))
        : (this.verticesLength + e.length > this.mesh.getVertices().length ||
            this.indicesLength + s.length > this.mesh.getIndices().length) &&
          this.flush();
      let i = this.mesh.numVertices();
      this.mesh.getVertices().set(e, this.verticesLength),
        (this.verticesLength += e.length),
        this.mesh.setVerticesLength(this.verticesLength);
      let r = this.mesh.getIndices();
      for (let t = this.indicesLength, e = 0; e < s.length; t++, e++)
        r[t] = s[e] + i;
      (this.indicesLength += s.length),
        this.mesh.setIndicesLength(this.indicesLength);
    }
    flush() {
      if (0 != this.verticesLength) {
        if (!this.lastTexture) throw new Error("No texture set.");
        if (!this.shader) throw new Error("No shader set.");
        this.lastTexture.bind(),
          this.mesh.draw(this.shader, this.context.gl.TRIANGLES),
          (this.verticesLength = 0),
          (this.indicesLength = 0),
          this.mesh.setVerticesLength(0),
          this.mesh.setIndicesLength(0),
          this.drawCalls++;
      }
    }
    end() {
      if (!this.isDrawing)
        throw new Error(
          "PolygonBatch is not drawing. Call PolygonBatch.begin() before calling PolygonBatch.end()"
        );
      (this.verticesLength > 0 || this.indicesLength > 0) && this.flush(),
        (this.shader = null),
        (this.lastTexture = null),
        (this.isDrawing = !1);
      let t = this.context.gl;
      t.disable(t.BLEND), this.cullWasEnabled && t.enable(t.CULL_FACE);
    }
    getDrawCalls() {
      return this.drawCalls;
    }
    dispose() {
      this.mesh.dispose();
    }
  }
  class Fe {
    constructor(t, s = 10920) {
      if (
        ((this.isDrawing = !1),
        (this.shapeType = Le.Filled),
        (this.color = new e(1, 1, 1, 1)),
        (this.shader = null),
        (this.vertexIndex = 0),
        (this.tmp = new n()),
        s > 10920)
      )
        throw new Error("Can't have more than 10920 triangles per batch: " + s);
      (this.context = t instanceof te ? t : new te(t)),
        (this.mesh = new Ee(t, [new Te(), new Ye()], s, 0));
      let i = this.context.gl;
      (this.srcColorBlend = i.SRC_ALPHA),
        (this.srcAlphaBlend = i.ONE),
        (this.dstBlend = i.ONE_MINUS_SRC_ALPHA);
    }
    begin(t) {
      if (this.isDrawing)
        throw new Error("ShapeRenderer.begin() has already been called");
      (this.shader = t), (this.vertexIndex = 0), (this.isDrawing = !0);
      let e = this.context.gl;
      e.enable(e.BLEND),
        e.blendFuncSeparate(
          this.srcColorBlend,
          this.dstBlend,
          this.srcAlphaBlend,
          this.dstBlend
        );
    }
    setBlendMode(t, e, s) {
      (this.srcColorBlend = t),
        (this.srcAlphaBlend = e),
        (this.dstBlend = s),
        this.isDrawing &&
          (this.flush(), this.context.gl.blendFuncSeparate(t, s, e, s));
    }
    setColor(t) {
      this.color.setFromColor(t);
    }
    setColorWith(t, e, s, i) {
      this.color.set(t, e, s, i);
    }
    point(t, e, s) {
      this.check(Le.Point, 1), s || (s = this.color), this.vertex(t, e, s);
    }
    line(t, e, s, i, r) {
      this.check(Le.Line, 2),
        this.mesh.getVertices(),
        this.vertexIndex,
        r || (r = this.color),
        this.vertex(t, e, r),
        this.vertex(s, i, r);
    }
    triangle(t, e, s, i, r, n, a, h, o, l) {
      this.check(t ? Le.Filled : Le.Line, 3),
        this.mesh.getVertices(),
        this.vertexIndex,
        h || (h = this.color),
        o || (o = this.color),
        l || (l = this.color),
        t
          ? (this.vertex(e, s, h), this.vertex(i, r, o), this.vertex(n, a, l))
          : (this.vertex(e, s, h),
            this.vertex(i, r, o),
            this.vertex(i, r, h),
            this.vertex(n, a, o),
            this.vertex(n, a, h),
            this.vertex(e, s, o));
    }
    quad(t, e, s, i, r, n, a, h, o, l, c, d, u) {
      this.check(t ? Le.Filled : Le.Line, 3),
        this.mesh.getVertices(),
        this.vertexIndex,
        l || (l = this.color),
        c || (c = this.color),
        d || (d = this.color),
        u || (u = this.color),
        t
          ? (this.vertex(e, s, l),
            this.vertex(i, r, c),
            this.vertex(n, a, d),
            this.vertex(n, a, d),
            this.vertex(h, o, u),
            this.vertex(e, s, l))
          : (this.vertex(e, s, l),
            this.vertex(i, r, c),
            this.vertex(i, r, c),
            this.vertex(n, a, d),
            this.vertex(n, a, d),
            this.vertex(h, o, u),
            this.vertex(h, o, u),
            this.vertex(e, s, l));
    }
    rect(t, e, s, i, r, n) {
      this.quad(t, e, s, e + i, s, e + i, s + r, e, s + r, n, n, n, n);
    }
    rectLine(t, e, s, i, r, n, a) {
      this.check(t ? Le.Filled : Le.Line, 8), a || (a = this.color);
      let h = this.tmp.set(r - s, e - i);
      h.normalize(), (n *= 0.5);
      let o = h.x * n,
        l = h.y * n;
      t
        ? (this.vertex(e + o, s + l, a),
          this.vertex(e - o, s - l, a),
          this.vertex(i + o, r + l, a),
          this.vertex(i - o, r - l, a),
          this.vertex(i + o, r + l, a),
          this.vertex(e - o, s - l, a))
        : (this.vertex(e + o, s + l, a),
          this.vertex(e - o, s - l, a),
          this.vertex(i + o, r + l, a),
          this.vertex(i - o, r - l, a),
          this.vertex(i + o, r + l, a),
          this.vertex(e + o, s + l, a),
          this.vertex(i - o, r - l, a),
          this.vertex(e - o, s - l, a));
    }
    x(t, e, s) {
      this.line(t - s, e - s, t + s, e + s),
        this.line(t - s, e + s, t + s, e - s);
    }
    polygon(t, e, s, i) {
      if (s < 3) throw new Error("Polygon must contain at least 3 vertices");
      this.check(Le.Line, 2 * s),
        i || (i = this.color),
        this.mesh.getVertices(),
        this.vertexIndex,
        (s <<= 1);
      let r = t[(e <<= 1)],
        n = t[e + 1],
        a = e + s;
      for (let h = e, o = e + s - 2; h < o; h += 2) {
        let e = t[h],
          s = t[h + 1],
          o = 0,
          l = 0;
        h + 2 >= a ? ((o = r), (l = n)) : ((o = t[h + 2]), (l = t[h + 3])),
          this.vertex(e, s, i),
          this.vertex(o, l, i);
      }
    }
    circle(t, e, i, r, n, a = 0) {
      if ((0 == a && (a = Math.max(1, (6 * s.cbrt(r)) | 0)), a <= 0))
        throw new Error("segments must be > 0.");
      n || (n = this.color);
      let h = (2 * s.PI) / a,
        o = Math.cos(h),
        l = Math.sin(h),
        c = r,
        d = 0;
      if (t) {
        this.check(Le.Filled, 3 * a + 3), a--;
        for (let t = 0; t < a; t++) {
          this.vertex(e, i, n), this.vertex(e + c, i + d, n);
          let t = c;
          (c = o * c - l * d),
            (d = l * t + o * d),
            this.vertex(e + c, i + d, n);
        }
        this.vertex(e, i, n), this.vertex(e + c, i + d, n);
      } else {
        this.check(Le.Line, 2 * a + 2);
        for (let t = 0; t < a; t++) {
          this.vertex(e + c, i + d, n);
          let t = c;
          (c = o * c - l * d),
            (d = l * t + o * d),
            this.vertex(e + c, i + d, n);
        }
        this.vertex(e + c, i + d, n);
      }
      (c = r), (d = 0), this.vertex(e + c, i + d, n);
    }
    curve(t, e, s, i, r, n, a, h, o, l) {
      this.check(Le.Line, 2 * o + 2), l || (l = this.color);
      let c = 1 / o,
        d = c * c,
        u = c * c * c,
        m = 3 * c,
        f = 3 * d,
        g = 6 * d,
        p = 6 * u,
        x = t - 2 * s + r,
        w = e - 2 * i + n,
        b = 3 * (s - r) - t + a,
        v = 3 * (i - n) - e + h,
        y = t,
        A = e,
        C = (s - t) * m + x * f + b * u,
        k = (i - e) * m + w * f + v * u,
        S = x * g + b * p,
        E = w * g + v * p,
        M = b * p,
        T = v * p;
      for (; o-- > 0; )
        this.vertex(y, A, l),
          (y += C),
          (A += k),
          (C += S),
          (k += E),
          (S += M),
          (E += T),
          this.vertex(y, A, l);
      this.vertex(y, A, l), this.vertex(a, h, l);
    }
    vertex(t, e, s) {
      let i = this.vertexIndex,
        r = this.mesh.getVertices();
      (r[i++] = t),
        (r[i++] = e),
        (r[i++] = s.r),
        (r[i++] = s.g),
        (r[i++] = s.b),
        (r[i++] = s.a),
        (this.vertexIndex = i);
    }
    end() {
      if (!this.isDrawing)
        throw new Error("ShapeRenderer.begin() has not been called");
      this.flush();
      let t = this.context.gl;
      t.disable(t.BLEND), (this.isDrawing = !1);
    }
    flush() {
      if (0 != this.vertexIndex) {
        if (!this.shader) throw new Error("No shader set.");
        this.mesh.setVerticesLength(this.vertexIndex),
          this.mesh.draw(this.shader, this.shapeType),
          (this.vertexIndex = 0);
      }
    }
    check(t, e) {
      if (!this.isDrawing)
        throw new Error("ShapeRenderer.begin() has not been called");
      if (this.shapeType == t) {
        if (!(this.mesh.maxVertices() - this.mesh.numVertices() < e)) return;
        this.flush();
      } else this.flush(), (this.shapeType = t);
    }
    dispose() {
      this.mesh.dispose();
    }
  }
  !(function (t) {
    (t[(t.Point = 0)] = "Point"),
      (t[(t.Line = 1)] = "Line"),
      (t[(t.Filled = 4)] = "Filled");
  })(Le || (Le = {}));
  class De {
    constructor(t) {
      (this.boneLineColor = new e(1, 0, 0, 1)),
        (this.boneOriginColor = new e(0, 1, 0, 1)),
        (this.attachmentLineColor = new e(0, 0, 1, 0.5)),
        (this.triangleLineColor = new e(1, 0.64, 0, 0.5)),
        (this.pathColor = new e().setFromString("FF7F00")),
        (this.clipColor = new e(0.8, 0, 0, 2)),
        (this.aabbColor = new e(0, 1, 0, 0.5)),
        (this.drawBones = !0),
        (this.drawRegionAttachments = !0),
        (this.drawBoundingBoxes = !0),
        (this.drawMeshHull = !0),
        (this.drawMeshTriangles = !0),
        (this.drawPaths = !0),
        (this.drawSkeletonXY = !1),
        (this.drawClipping = !0),
        (this.premultipliedAlpha = !1),
        (this.scale = 1),
        (this.boneWidth = 2),
        (this.bounds = new _t()),
        (this.temp = new Array()),
        (this.vertices = i.newFloatArray(2048)),
        (this.context = t instanceof te ? t : new te(t));
    }
    draw(t, e, s) {
      let r = e.x,
        n = e.y,
        a = this.context.gl,
        h = this.premultipliedAlpha ? a.ONE : a.SRC_ALPHA;
      t.setBlendMode(h, a.ONE, a.ONE_MINUS_SRC_ALPHA);
      let o = e.bones;
      if (this.drawBones) {
        t.setColor(this.boneLineColor);
        for (let e = 0, i = o.length; e < i; e++) {
          let i = o[e];
          if (s && s.indexOf(i.data.name) > -1) continue;
          if (!i.parent) continue;
          let r = i.data.length * i.a + i.worldX,
            n = i.data.length * i.c + i.worldY;
          t.rectLine(!0, i.worldX, i.worldY, r, n, this.boneWidth * this.scale);
        }
        this.drawSkeletonXY && t.x(r, n, 4 * this.scale);
      }
      if (this.drawRegionAttachments) {
        t.setColor(this.attachmentLineColor);
        let s = e.slots;
        for (let e = 0, i = s.length; e < i; e++) {
          let i = s[e],
            r = i.getAttachment();
          if (r instanceof kt) {
            let e = r,
              s = this.vertices;
            e.computeWorldVertices(i, s, 0, 2),
              t.line(s[0], s[1], s[2], s[3]),
              t.line(s[2], s[3], s[4], s[5]),
              t.line(s[4], s[5], s[6], s[7]),
              t.line(s[6], s[7], s[0], s[1]);
          }
        }
      }
      if (this.drawMeshHull || this.drawMeshTriangles) {
        let s = e.slots;
        for (let e = 0, i = s.length; e < i; e++) {
          let i = s[e];
          if (!i.bone.active) continue;
          let r = i.getAttachment();
          if (!(r instanceof yt)) continue;
          let n = r,
            a = this.vertices;
          n.computeWorldVertices(i, 0, n.worldVerticesLength, a, 0, 2);
          let h = n.triangles,
            o = n.hullLength;
          if (this.drawMeshTriangles) {
            t.setColor(this.triangleLineColor);
            for (let e = 0, s = h.length; e < s; e += 3) {
              let s = 2 * h[e],
                i = 2 * h[e + 1],
                r = 2 * h[e + 2];
              t.triangle(!1, a[s], a[s + 1], a[i], a[i + 1], a[r], a[r + 1]);
            }
          }
          if (this.drawMeshHull && o > 0) {
            t.setColor(this.attachmentLineColor), (o = 2 * (o >> 1));
            let e = a[o - 2],
              s = a[o - 1];
            for (let i = 0, r = o; i < r; i += 2) {
              let r = a[i],
                n = a[i + 1];
              t.line(r, n, e, s), (e = r), (s = n);
            }
          }
        }
      }
      if (this.drawBoundingBoxes) {
        let s = this.bounds;
        s.update(e, !0),
          t.setColor(this.aabbColor),
          t.rect(!1, s.minX, s.minY, s.getWidth(), s.getHeight());
        let i = s.polygons,
          r = s.boundingBoxes;
        for (let e = 0, s = i.length; e < s; e++) {
          let s = i[e];
          t.setColor(r[e].color), t.polygon(s, 0, s.length);
        }
      }
      if (this.drawPaths) {
        let s = e.slots;
        for (let e = 0, r = s.length; e < r; e++) {
          let r = s[e];
          if (!r.bone.active) continue;
          let n = r.getAttachment();
          if (!(n instanceof At)) continue;
          let a = n,
            h = a.worldVerticesLength,
            o = (this.temp = i.setArraySize(this.temp, h, 0));
          a.computeWorldVertices(r, 0, h, o, 0, 2);
          let l = this.pathColor,
            c = o[2],
            d = o[3],
            u = 0,
            m = 0;
          if (a.closed) {
            t.setColor(l);
            let e = o[0],
              s = o[1],
              i = o[h - 2],
              r = o[h - 1];
            (u = o[h - 4]),
              (m = o[h - 3]),
              t.curve(c, d, e, s, i, r, u, m, 32),
              t.setColor(De.LIGHT_GRAY),
              t.line(c, d, e, s),
              t.line(u, m, i, r);
          }
          h -= 4;
          for (let e = 4; e < h; e += 6) {
            let s = o[e],
              i = o[e + 1],
              r = o[e + 2],
              n = o[e + 3];
            (u = o[e + 4]),
              (m = o[e + 5]),
              t.setColor(l),
              t.curve(c, d, s, i, r, n, u, m, 32),
              t.setColor(De.LIGHT_GRAY),
              t.line(c, d, s, i),
              t.line(u, m, r, n),
              (c = u),
              (d = m);
          }
        }
      }
      if (this.drawBones) {
        t.setColor(this.boneOriginColor);
        for (let e = 0, i = o.length; e < i; e++) {
          let i = o[e];
          (s && s.indexOf(i.data.name) > -1) ||
            t.circle(!0, i.worldX, i.worldY, 3 * this.scale, De.GREEN, 8);
        }
      }
      if (this.drawClipping) {
        let s = e.slots;
        t.setColor(this.clipColor);
        for (let e = 0, r = s.length; e < r; e++) {
          let r = s[e];
          if (!r.bone.active) continue;
          let n = r.getAttachment();
          if (!(n instanceof it)) continue;
          let a = n,
            h = a.worldVerticesLength,
            o = (this.temp = i.setArraySize(this.temp, h, 0));
          a.computeWorldVertices(r, 0, h, o, 0, 2);
          for (let e = 0, s = o.length; e < s; e += 2) {
            let s = o[e],
              i = o[e + 1],
              r = o[(e + 2) % o.length],
              n = o[(e + 3) % o.length];
            t.line(s, i, r, n);
          }
        }
      }
    }
    dispose() {}
  }
  (De.LIGHT_GRAY = new e(192 / 255, 192 / 255, 192 / 255, 1)),
    (De.GREEN = new e(0, 1, 0, 1));
  class Be {
    constructor(t, e, s) {
      (this.vertices = t), (this.numVertices = e), (this.numFloats = s);
    }
  }
  class Ve {
    constructor(t, s = !0) {
      (this.premultipliedAlpha = !1),
        (this.tempColor = new e()),
        (this.tempColor2 = new e()),
        (this.vertexSize = 8),
        (this.twoColorTint = !1),
        (this.renderable = new Be([], 0, 0)),
        (this.clipper = new Wt()),
        (this.temp = new n()),
        (this.temp2 = new n()),
        (this.temp3 = new e()),
        (this.temp4 = new e()),
        (this.twoColorTint = s),
        s && (this.vertexSize += 4),
        (this.vertices = i.newFloatArray(1024 * this.vertexSize));
    }
    draw(t, e, s = -1, r = -1) {
      let n,
        a,
        h,
        o = this.clipper,
        l = this.premultipliedAlpha,
        c = this.twoColorTint,
        d = null,
        u = (this.temp, this.temp2, this.temp3, this.temp4, this.renderable),
        m = e.drawOrder,
        f = e.color,
        g = c ? 12 : 8,
        p = !1;
      -1 == s && (p = !0);
      for (let e = 0, x = m.length; e < x; e++) {
        let x = o.isClipping() ? 2 : g,
          w = m[e];
        if (!w.bone.active) {
          o.clipEndWithSlot(w);
          continue;
        }
        if ((s >= 0 && s == w.data.index && (p = !0), !p)) {
          o.clipEndWithSlot(w);
          continue;
        }
        r >= 0 && r == w.data.index && (p = !1);
        let b,
          v = w.getAttachment();
        if (v instanceof kt) {
          let t = v;
          (u.vertices = this.vertices),
            (u.numVertices = 4),
            (u.numFloats = x << 2),
            t.computeWorldVertices(w, u.vertices, 0, x),
            (a = Ve.QUAD_TRIANGLES),
            (n = t.uvs),
            (b = t.region.renderObject.page.texture),
            (h = t.color);
        } else {
          if (!(v instanceof yt)) {
            if (v instanceof it) {
              let t = v;
              o.clipStart(w, t);
              continue;
            }
            o.clipEndWithSlot(w);
            continue;
          }
          {
            let t = v;
            (u.vertices = this.vertices),
              (u.numVertices = t.worldVerticesLength >> 1),
              (u.numFloats = u.numVertices * x),
              u.numFloats > u.vertices.length &&
                (u.vertices = this.vertices = i.newFloatArray(u.numFloats)),
              t.computeWorldVertices(
                w,
                0,
                t.worldVerticesLength,
                u.vertices,
                0,
                x
              ),
              (a = t.triangles),
              (b = t.region.renderObject.page.texture),
              (n = t.uvs),
              (h = t.color);
          }
        }
        if (b) {
          let e = w.color,
            s = this.tempColor;
          (s.r = f.r * e.r * h.r),
            (s.g = f.g * e.g * h.g),
            (s.b = f.b * e.b * h.b),
            (s.a = f.a * e.a * h.a),
            l && ((s.r *= s.a), (s.g *= s.a), (s.b *= s.a));
          let i = this.tempColor2;
          w.darkColor
            ? (l
                ? ((i.r = w.darkColor.r * s.a),
                  (i.g = w.darkColor.g * s.a),
                  (i.b = w.darkColor.b * s.a))
                : i.setFromColor(w.darkColor),
              (i.a = l ? 1 : 0))
            : i.set(0, 0, 0, 1);
          let r = w.data.blendMode;
          if (
            (r != d &&
              ((d = r),
              t.setBlendMode(
                ee.getSourceColorGLBlendMode(d, l),
                ee.getSourceAlphaGLBlendMode(d),
                ee.getDestGLBlendMode(d)
              )),
            o.isClipping())
          ) {
            o.clipTriangles(u.vertices, u.numFloats, a, a.length, n, s, i, c);
            let e = new Float32Array(o.clippedVertices),
              r = o.clippedTriangles;
            t.draw(b, e, r);
          } else {
            let e = u.vertices;
            if (c)
              for (let t = 2, r = 0, a = u.numFloats; t < a; t += g, r += 2)
                (e[t] = s.r),
                  (e[t + 1] = s.g),
                  (e[t + 2] = s.b),
                  (e[t + 3] = s.a),
                  (e[t + 4] = n[r]),
                  (e[t + 5] = n[r + 1]),
                  (e[t + 6] = i.r),
                  (e[t + 7] = i.g),
                  (e[t + 8] = i.b),
                  (e[t + 9] = i.a);
            else
              for (let t = 2, i = 0, r = u.numFloats; t < r; t += g, i += 2)
                (e[t] = s.r),
                  (e[t + 1] = s.g),
                  (e[t + 2] = s.b),
                  (e[t + 3] = s.a),
                  (e[t + 4] = n[i]),
                  (e[t + 5] = n[i + 1]);
            let r = u.vertices.subarray(0, u.numFloats);
            t.draw(b, r, a);
          }
        }
        o.clipEndWithSlot(w);
      }
      o.clipEnd();
    }
  }
  Ve.QUAD_TRIANGLES = [0, 1, 2, 2, 3, 0];
  const Ne = [
      0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,
      0, 1, 1, 1, 1, 0, 0,
    ],
    _e = [0, 1, 2, 2, 3, 0],
    Oe = new e(1, 1, 1, 1);
  class We {
    constructor(t, e, s = !0) {
      (this.twoColorTint = !1),
        (this.activeRenderer = null),
        (this.canvas = t),
        (this.context = e instanceof te ? e : new te(e)),
        (this.twoColorTint = s),
        (this.camera = new Ae(t.width, t.height)),
        (this.batcherShader = s
          ? Se.newTwoColoredTextured(this.context)
          : Se.newColoredTextured(this.context)),
        (this.batcher = new Pe(this.context, s)),
        (this.shapesShader = Se.newColored(this.context)),
        (this.shapes = new Fe(this.context)),
        (this.skeletonRenderer = new Ve(this.context, s)),
        (this.skeletonDebugRenderer = new De(this.context));
    }
    dispose() {
      this.batcher.dispose(),
        this.batcherShader.dispose(),
        this.shapes.dispose(),
        this.shapesShader.dispose(),
        this.skeletonDebugRenderer.dispose();
    }
    begin() {
      this.camera.update(), this.enableRenderer(this.batcher);
    }
    drawSkeleton(t, e = !1, s = -1, i = -1) {
      this.enableRenderer(this.batcher),
        (this.skeletonRenderer.premultipliedAlpha = e),
        this.skeletonRenderer.draw(this.batcher, t, s, i);
    }
    drawSkeletonDebug(t, e = !1, s) {
      this.enableRenderer(this.shapes),
        (this.skeletonDebugRenderer.premultipliedAlpha = e),
        this.skeletonDebugRenderer.draw(this.shapes, t, s);
    }
    drawTexture(t, e, s, i, r, n) {
      this.enableRenderer(this.batcher), n || (n = Oe);
      var a = 0;
      (Ne[a++] = e),
        (Ne[a++] = s),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = 0),
        (Ne[a++] = 1),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0)),
        (Ne[a++] = e + i),
        (Ne[a++] = s),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = 1),
        (Ne[a++] = 1),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0)),
        (Ne[a++] = e + i),
        (Ne[a++] = s + r),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = 1),
        (Ne[a++] = 0),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0)),
        (Ne[a++] = e),
        (Ne[a++] = s + r),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = 0),
        (Ne[a++] = 0),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a] = 0)),
        this.batcher.draw(t, Ne, _e);
    }
    drawTextureUV(t, e, s, i, r, n, a, h, o, l) {
      this.enableRenderer(this.batcher), l || (l = Oe);
      var c = 0;
      (Ne[c++] = e),
        (Ne[c++] = s),
        (Ne[c++] = l.r),
        (Ne[c++] = l.g),
        (Ne[c++] = l.b),
        (Ne[c++] = l.a),
        (Ne[c++] = n),
        (Ne[c++] = a),
        this.twoColorTint &&
          ((Ne[c++] = 0), (Ne[c++] = 0), (Ne[c++] = 0), (Ne[c++] = 0)),
        (Ne[c++] = e + i),
        (Ne[c++] = s),
        (Ne[c++] = l.r),
        (Ne[c++] = l.g),
        (Ne[c++] = l.b),
        (Ne[c++] = l.a),
        (Ne[c++] = h),
        (Ne[c++] = a),
        this.twoColorTint &&
          ((Ne[c++] = 0), (Ne[c++] = 0), (Ne[c++] = 0), (Ne[c++] = 0)),
        (Ne[c++] = e + i),
        (Ne[c++] = s + r),
        (Ne[c++] = l.r),
        (Ne[c++] = l.g),
        (Ne[c++] = l.b),
        (Ne[c++] = l.a),
        (Ne[c++] = h),
        (Ne[c++] = o),
        this.twoColorTint &&
          ((Ne[c++] = 0), (Ne[c++] = 0), (Ne[c++] = 0), (Ne[c++] = 0)),
        (Ne[c++] = e),
        (Ne[c++] = s + r),
        (Ne[c++] = l.r),
        (Ne[c++] = l.g),
        (Ne[c++] = l.b),
        (Ne[c++] = l.a),
        (Ne[c++] = n),
        (Ne[c++] = o),
        this.twoColorTint &&
          ((Ne[c++] = 0), (Ne[c++] = 0), (Ne[c++] = 0), (Ne[c] = 0)),
        this.batcher.draw(t, Ne, _e);
    }
    drawTextureRotated(t, e, i, r, n, a, h, o, l) {
      this.enableRenderer(this.batcher), l || (l = Oe);
      let c = e + a,
        d = i + h,
        u = -a,
        m = -h,
        f = r - a,
        g = n - h,
        p = u,
        x = m,
        w = u,
        b = g,
        v = f,
        y = g,
        A = f,
        C = m,
        k = 0,
        S = 0,
        E = 0,
        M = 0,
        T = 0,
        R = 0,
        Y = 0,
        I = 0;
      if (0 != o) {
        let t = s.cosDeg(o),
          e = s.sinDeg(o);
        (k = t * p - e * x),
          (S = e * p + t * x),
          (Y = t * w - e * b),
          (I = e * w + t * b),
          (T = t * v - e * y),
          (R = e * v + t * y),
          (E = T + (k - Y)),
          (M = R + (S - I));
      } else
        (k = p), (S = x), (Y = w), (I = b), (T = v), (R = y), (E = A), (M = C);
      (k += c),
        (S += d),
        (E += c),
        (M += d),
        (T += c),
        (R += d),
        (Y += c),
        (I += d);
      var X = 0;
      (Ne[X++] = k),
        (Ne[X++] = S),
        (Ne[X++] = l.r),
        (Ne[X++] = l.g),
        (Ne[X++] = l.b),
        (Ne[X++] = l.a),
        (Ne[X++] = 0),
        (Ne[X++] = 1),
        this.twoColorTint &&
          ((Ne[X++] = 0), (Ne[X++] = 0), (Ne[X++] = 0), (Ne[X++] = 0)),
        (Ne[X++] = E),
        (Ne[X++] = M),
        (Ne[X++] = l.r),
        (Ne[X++] = l.g),
        (Ne[X++] = l.b),
        (Ne[X++] = l.a),
        (Ne[X++] = 1),
        (Ne[X++] = 1),
        this.twoColorTint &&
          ((Ne[X++] = 0), (Ne[X++] = 0), (Ne[X++] = 0), (Ne[X++] = 0)),
        (Ne[X++] = T),
        (Ne[X++] = R),
        (Ne[X++] = l.r),
        (Ne[X++] = l.g),
        (Ne[X++] = l.b),
        (Ne[X++] = l.a),
        (Ne[X++] = 1),
        (Ne[X++] = 0),
        this.twoColorTint &&
          ((Ne[X++] = 0), (Ne[X++] = 0), (Ne[X++] = 0), (Ne[X++] = 0)),
        (Ne[X++] = Y),
        (Ne[X++] = I),
        (Ne[X++] = l.r),
        (Ne[X++] = l.g),
        (Ne[X++] = l.b),
        (Ne[X++] = l.a),
        (Ne[X++] = 0),
        (Ne[X++] = 0),
        this.twoColorTint &&
          ((Ne[X++] = 0), (Ne[X++] = 0), (Ne[X++] = 0), (Ne[X] = 0)),
        this.batcher.draw(t, Ne, _e);
    }
    drawRegion(t, e, s, i, r, n) {
      this.enableRenderer(this.batcher), n || (n = Oe);
      var a = 0;
      (Ne[a++] = e),
        (Ne[a++] = s),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = t.u),
        (Ne[a++] = t.v2),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0)),
        (Ne[a++] = e + i),
        (Ne[a++] = s),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = t.u2),
        (Ne[a++] = t.v2),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0)),
        (Ne[a++] = e + i),
        (Ne[a++] = s + r),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = t.u2),
        (Ne[a++] = t.v),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0)),
        (Ne[a++] = e),
        (Ne[a++] = s + r),
        (Ne[a++] = n.r),
        (Ne[a++] = n.g),
        (Ne[a++] = n.b),
        (Ne[a++] = n.a),
        (Ne[a++] = t.u),
        (Ne[a++] = t.v),
        this.twoColorTint &&
          ((Ne[a++] = 0), (Ne[a++] = 0), (Ne[a++] = 0), (Ne[a] = 0)),
        this.batcher.draw(t.page.texture, Ne, _e);
    }
    line(t, e, s, i, r, n) {
      this.enableRenderer(this.shapes), this.shapes.line(t, e, s, i, r);
    }
    triangle(t, e, s, i, r, n, a, h, o, l) {
      this.enableRenderer(this.shapes),
        this.shapes.triangle(t, e, s, i, r, n, a, h, o, l);
    }
    quad(t, e, s, i, r, n, a, h, o, l, c, d, u) {
      this.enableRenderer(this.shapes),
        this.shapes.quad(t, e, s, i, r, n, a, h, o, l, c, d, u);
    }
    rect(t, e, s, i, r, n) {
      this.enableRenderer(this.shapes), this.shapes.rect(t, e, s, i, r, n);
    }
    rectLine(t, e, s, i, r, n, a) {
      this.enableRenderer(this.shapes),
        this.shapes.rectLine(t, e, s, i, r, n, a);
    }
    polygon(t, e, s, i) {
      this.enableRenderer(this.shapes), this.shapes.polygon(t, e, s, i);
    }
    circle(t, e, s, i, r, n = 0) {
      this.enableRenderer(this.shapes), this.shapes.circle(t, e, s, i, r, n);
    }
    curve(t, e, s, i, r, n, a, h, o, l) {
      this.enableRenderer(this.shapes),
        this.shapes.curve(t, e, s, i, r, n, a, h, o, l);
    }
    end() {
      this.activeRenderer === this.batcher
        ? this.batcher.end()
        : this.activeRenderer === this.shapes && this.shapes.end(),
        (this.activeRenderer = null);
    }
    resize(t) {
      let e = this.canvas;
      var s = window.devicePixelRatio || 1,
        i = Math.round(e.clientWidth * s),
        r = Math.round(e.clientHeight * s);
      if (
        ((e.width == i && e.height == r) || ((e.width = i), (e.height = r)),
        this.context.gl.viewport(0, 0, e.width, e.height),
        t === qe.Expand)
      )
        this.camera.setViewport(i, r);
      else if (t === qe.Fit) {
        let t = e.width,
          s = e.height,
          i = this.camera.viewportWidth,
          r = this.camera.viewportHeight,
          n = r / i < s / t ? i / t : r / s;
        this.camera.setViewport(t * n, s * n);
      }
      this.camera.update();
    }
    enableRenderer(t) {
      this.activeRenderer !== t &&
        (this.end(),
        t instanceof Pe
          ? (this.batcherShader.bind(),
            this.batcherShader.setUniform4x4f(
              Se.MVP_MATRIX,
              this.camera.projectionView.values
            ),
            this.batcherShader.setUniformi("u_texture", 0),
            this.batcher.begin(this.batcherShader),
            (this.activeRenderer = this.batcher))
          : t instanceof Fe
          ? (this.shapesShader.bind(),
            this.shapesShader.setUniform4x4f(
              Se.MVP_MATRIX,
              this.camera.projectionView.values
            ),
            this.shapes.begin(this.shapesShader),
            (this.activeRenderer = this.shapes))
          : (this.activeRenderer = this.skeletonDebugRenderer));
    }
  }
  var qe, ze;
  ((ze = qe || (qe = {}))[(ze.Stretch = 0)] = "Stretch"),
    (ze[(ze.Expand = 1)] = "Expand"),
    (ze[(ze.Fit = 2)] = "Fit");
  class Ue {
    constructor(t, e) {
      (this.time = new a()),
        e.pathPrefix || (e.pathPrefix = ""),
        e.app ||
          (e.app = {
            loadAssets: () => {},
            initialize: () => {},
            update: () => {},
            render: () => {},
            error: () => {},
          }),
        e.webglConfig && (e.webglConfig = { alpha: !0 }),
        (this.htmlCanvas = t),
        (this.context = new te(t, e.webglConfig)),
        (this.renderer = new We(t, this.context)),
        (this.gl = this.context.gl),
        (this.assetManager = new ie(this.context, e.pathPrefix)),
        (this.input = new Ce(t)),
        e.app.loadAssets && e.app.loadAssets(this);
      let s = () => {
          requestAnimationFrame(s),
            this.time.update(),
            e.app.update && e.app.update(this, this.time.delta),
            e.app.render && e.app.render(this);
        },
        i = () => {
          this.assetManager.isLoadingComplete()
            ? this.assetManager.hasErrors()
              ? e.app.error && e.app.error(this, this.assetManager.getErrors())
              : (e.app.initialize && e.app.initialize(this), s())
            : requestAnimationFrame(i);
        };
      requestAnimationFrame(i);
    }
    clear(t, e, s, i) {
      this.gl.clearColor(t, e, s, i), this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }
  }
  const $e = (function () {
    function t(t, e) {
      (this.window = t), (this.canvas = null), (this.thumbName = e);
    }
    return (
      (t.prototype.loadAssets = function (t) {
        (this.canvas = t),
          this.canvas.assetManager.loadText(this.thumbName + ".json"),
          this.canvas.assetManager.loadTextureAtlas(this.thumbName + ".atlas");
      }),
      (t.prototype.load = function () {
        var t = this.canvas.assetManager,
          e = t.require(this.thumbName + ".atlas"),
          s = new St(e),
          i = new Gt(s);
        i.scale = 0.4;
        var r = i.readSkeletonData(t.require(this.thumbName + ".json"));
        console.log("skeleton height: ", r.height, r.width),
          (this.skeleton = new Dt(r)),
          this.skeleton.setToSetupPose(),
          this.skeleton.updateWorldTransform(),
          (this.skeleton.x = -75),
          (this.skeleton.y = 350);
        var n = new et(r);
        this.state = new z(n);
        var a = this.skeleton.data.animations.map(function (t) {
          return t.name;
        });
        console.log("animation names:", a),
          this.state.setAnimation(0, a[0], !0),
          console.log(
            "canvas: ",
            this.canvas.htmlCanvas.width,
            this.canvas.htmlCanvas.height
          ),
          console.log("skeleton: ", this.skeleton.x, this.skeleton.y);
      }),
      (t.prototype.initialize = function (t) {
        this.load();
      }),
      (t.prototype.update = function (t, e) {
        this.state.update(e),
          this.state.apply(this.skeleton),
          this.skeleton.updateWorldTransform();
      }),
      (t.prototype.render = function (t) {
        var e = t.renderer;
        t.clear(0, 0, 0, 0),
          e.begin(),
          e.rectLine(!0, 0, 0, 0, 0, t.htmlCanvas.width),
          e.drawSkeleton(this.skeleton, !0),
          e.end();
      }),
      t
    );
  })();
  window.onload = function () {
    for (var t = 5, e = 5, s = 0; s < 16; s++) {
      var i = document.createElement("canvas");
      (i.id = "canvas" + s),
        document.body.appendChild(i),
        (i.width = 200),
        (i.height = 165),
        (i.style.pointerEvents = "none"),
        (i.style.position = "absolute"),
        (i.style.left = t + "px"),
        (i.style.top = e + "px"),
        console.log("i: ", s, s % 4, t, e),
        (t += 210),
        s > 0 && s % 4 == 3 && ((e += 175), (t = 5)),
        new Ue(i, {
          pathPrefix: "spine/",
          app: new $e(window, "BookishWoodchuck"),
          webglConfig: { alpha: !0, backgroundColor: 0 },
        });
    }
  };
})();
