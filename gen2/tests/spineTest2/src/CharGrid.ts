import { CharThumb } from "./CharThumb";
import { SpineVo } from "./classes/dataObjs/SpineVo";

export class CharGrid
{
    public element:HTMLElement;
    constructor()
    {
        this.element = document.createElement("div");
        this.element.classList.add("char-grid");
        this.makeCharThumbs();
        document.body.appendChild(this.element);
    }
    makeCharThumbs()
    {
        const thumbChars:SpineVo[] = [];
        thumbChars.push(new SpineVo("BookishWoodchuck","thumbnail"));
        thumbChars.push(new SpineVo("Amka","walk"));
        thumbChars.push(new SpineVo("spineboy","walk"));    


        for (let i:number = 0; i < thumbChars.length; i++)
        {
            const thumbChar:SpineVo = thumbChars[i];
            const charThumb:CharThumb = new CharThumb(i,thumbChar);
            console.log(charThumb);
            charThumb.render();
            this.element.appendChild(charThumb.element);
        }
    }
}