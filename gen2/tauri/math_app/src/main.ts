import { appWindow } from "@tauri-apps/api/window";
import { TauriFun } from "./core/taurifun";
import { MathUI } from "./MathUI";

window.addEventListener("DOMContentLoaded", () => {
    const tauriFun: TauriFun = TauriFun.getInstance(appWindow);
    tauriFun.logCallback = (msg: string) => {
        console.log(msg);
    };
    tauriFun.focusCallback = (hasFocus: boolean) => {
        console.log("focus changed to " + hasFocus);
    }
    tauriFun.networkCallback = (hasNetwork: boolean) => {
        console.log("network changed to " + hasNetwork);
    } 
    new MathUI();
});
