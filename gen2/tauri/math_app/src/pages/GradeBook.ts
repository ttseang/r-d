import { invoke } from "@tauri-apps/api";
import { TTScrollbar } from "@teachingtextbooks/ttscrollbar";
import { Pages } from "../constants/PageConstants";
import { TauriFun } from "../core/taurifun";
import { UIStorage } from "../core/UIStorage";
import { GradeVo } from "../dataObjects/GradeVo";
import { IPage } from "../interfaces/IPage";
import { BasePage } from "./BasePage";

export class GradeBook extends BasePage implements IPage {

    // private main:HTMLElement | null=null;
    // private header:HTMLElement | null=null;
    private backButton: HTMLButtonElement | null = null;
    private grades: GradeVo[] = [];
    private ttScrollBar: TTScrollbar | null = null;
    private tableScroller: HTMLDivElement | null = null;
    private table: HTMLTableElement | null = null;

    
    private ms:UIStorage=UIStorage.getInstance();
    private tauriFun: TauriFun = TauriFun.getInstance();
    
    constructor(pageID: number, callback: Function) {
        super(pageID, callback);
        this.callback = callback;
        this.pageID = pageID;

        (window as any).gradebook = this;

        // this.loadGrades();
    }
    protected setReferences(): void {

        this.backButton = document.querySelector(".tempbutton");
        this.tableScroller = document.querySelector(".scroller") as HTMLDivElement;
    }
    protected makeChildElements(): void {

        this.loadGrades();
    }
    protected setUpEventListeners(): void {
        if (this.backButton) {
            this.backButton.addEventListener("click", () => { this.callback(Pages.Actions.ChangePage, Pages.Page.Home) });
        }
       
    }
    setInactive(): void {
        super.setInactive();
        //remove all event listeners
        const body: HTMLElement = document.querySelector("body") as HTMLElement;
        body.classList.remove("gradebook");
        body.classList.remove("corky");

        //clean up the event listeners

        const buttons: NodeListOf<HTMLButtonElement> = document.querySelectorAll(".btnDetail");
        buttons.forEach((button: HTMLButtonElement) => {
            button.removeEventListener("click", () => { });
        });
    }

    private async loadGrades() {
        let data:string= await invoke("get_file_contents", { path: this.tauriFun.resourcePath+"\\m3gradebook.json" });
        console.log(data);
        let gradeData:any=JSON.parse(data);
        this.gradesLoaded(gradeData);
        /* fetch("./assets/m3gradebook.json")
            .then(response => response.json())
            .then(data => this.gradesLoaded(data)); */
    }


    private gradesLoaded(data: any) {
        console.log(data);

        let heading1: string = data.h1;
        let heading2: string = data.h2;

        let heading1Obj:HTMLHeadingElement = document.querySelectorAll("main header h3")[0] as HTMLHeadingElement;
        let heading2Obj:HTMLHeadingElement = document.querySelectorAll("main header h3")[1] as HTMLHeadingElement;

        if (heading1Obj) {
            heading1Obj.innerHTML = heading1;
        }
        if (heading2Obj) {
            heading2Obj.innerHTML = heading2;
        }
        
        for (let i: number = 0; i < data.rows.length; i++) {
            let gradeData: any = data.rows[i];
            let gradeVo: GradeVo = new GradeVo(i, gradeData.lti, gradeData.name, gradeData.score, gradeData.lastwork, gradeData.complete, gradeData.prescore, gradeData.rawscore, gradeData.probsdone, gradeData.probstotal, gradeData.bonuspoints, gradeData.probscorrect);
            gradeVo.addProblems(gradeData.problems);
            this.grades.push(gradeVo);
        }

        this.makeTable();
        const buttons: NodeListOf<HTMLButtonElement> = document.querySelectorAll(".btnDetail");
        buttons.forEach((button: HTMLButtonElement) => {
            button.addEventListener("click", () => {
                console.log(button.getAttribute("data-id"));
                let id: number = parseInt(button.getAttribute("data-id") as string);
                let gradeVo: GradeVo = this.grades[id];
                this.ms.selectedGrade = gradeVo;

                this.callback(Pages.Actions.ChangePage, Pages.Page.Details);
            });
        });
    }

    private makeTable() {


        const scrollbarHolder: HTMLDivElement = document.querySelector(".scrollbarHolder") as HTMLDivElement;

        this.table = document.createElement("table");
        let tbody: HTMLTableSectionElement = document.createElement("tbody");
        this.table.appendChild(tbody);

        if (this.tableScroller) {
            this.tableScroller.appendChild(this.table);
            for (let i: number = 0; i < this.grades.length; i++) {
                let tr: HTMLTableRowElement = document.createElement("tr");
                tbody.appendChild(tr);
                tr.innerHTML = this.grades[i].getHtml();
            }
        }

        if (this.tableScroller && this.table) {
            this.ttScrollBar = new TTScrollbar(this.tableScroller, null, this.updateScroll.bind(this));

            this.ttScrollBar.addCssToHead();
            this.ttScrollBar.setImage("track", "./assets/scroll/scrollback.png");
            this.ttScrollBar.setImage("knob", "./assets/scroll/thumbScroll.png");
            this.ttScrollBar.setImage("btnUp", "./assets/scroll/arrowUp.png");
            this.ttScrollBar.setImage("btnDown", "./assets/scroll/arrowDown.png");

            this.ttScrollBar.element.style.maxHeight = "100%";
            this.ttScrollBar.debug = false;
        }

        if (scrollbarHolder && this.ttScrollBar) {
            this.ttScrollBar.appendTo(scrollbarHolder);
        }


    }
    updateScroll(percent: number) {
        if (this.tableScroller) {
            const scrollHeight: number = this.tableScroller.scrollHeight - this.tableScroller.clientHeight;
            const px: number = scrollHeight * percent / 100;
            this.tableScroller.scrollTo(0, px);
        }
    }
    render(): void {
        const body: HTMLElement = document.querySelector("body") as HTMLElement;
        body.classList.add("gradebook");
        body.classList.add("corky");

        body.innerHTML = `<header><button type="button" class="tempbutton">Back</button>
        <h1 title="Math App Gradebook"></h1>
        <b>&#xA0;</b>
        </header>
        <main><header>
        <h3>Overall Course Average: 100%</h3>
        <h3>Course Time Left: 99 months, 30 days</h3>
    </header>
    
    <table>
				<thead>
					<tr>
						<th class="mr"></th>
						<th class="mr ml">Lesson</th>
						<th class="ml"></th>
						<th>Total<br />Problems</th>
						<th>Number<br />Completed</th>
						<th>Number<br />Correct</th>
						<th>Percentage&#xA0;Score
							<br /><small>(can include bonus points)</small></th>
						<th>Last Work<br />Done On</th>
						<th>Grade on<br />Each Problem</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="9">
                        <div class="scroller"></div>
                        </td>
				</tbody>
			</table>           
            <div class="scrollbarHolder"></div>
    </main>
    
    <menu class="gblegend">
        <li class="complete">Complete</li>
        <li class="delete">Delete Lesson</li>
        <li class="bonus">Points from Bonus Round</li>
    </menu>`;

    }

}