import { WebviewWindow } from '@tauri-apps/api/window';
import { BaseDirectory } from '@tauri-apps/api/path';
import { invoke } from '@tauri-apps/api';

export class TauriFun {

  //make a singleton
  private static instance: TauriFun;
  private appWindow: WebviewWindow;
  private lastFocusState: boolean = false;
  private lastNetworkState: boolean = false;
  public focusCallback: Function = () => { };
  public networkCallback: Function = () => { };
  public logCallback: Function = () => { };

  public resourcePath: string = "";

  constructor(appWindow: WebviewWindow) {
    //console.log("TauriFun constructor");
    this.appWindow = appWindow;
    this.setListeners();
    this.resolveResourcePath();
  }
  private async resolveResourcePath() {
    this.resourcePath = await invoke("get_asset_path");
  // this.resourcePath = await resolveResource("assets");
    console.log(this.resourcePath);
}
  public static getInstance(appWindow: WebviewWindow | null = null): TauriFun {
    {
      if (!TauriFun.instance) {
        if (appWindow == null) {
          throw new Error("TauriFun instance not created yet");
        }
        TauriFun.instance = new TauriFun(appWindow);
      }
      return TauriFun.instance;
    }
  }
  private async setListeners() {
    await this.appWindow.listen(
      'RUSTMSG',
      ({ event, payload }) => {
        console.log(event);

        switch ((payload as any).event) {
          case 'focus_changed':
            if (this.lastFocusState != (payload as any).params) {
              this.logCallback("Focus state changed to " + (payload as any).params);
              this.focusCallback((payload as any).params);
            }
            this.lastFocusState = (payload as any).params;
            break;

          case 'network_changed':
            if (this.lastNetworkState != (payload as any).params) {
              this.logCallback("Network state changed to " + (payload as any).params);
              this.networkCallback((payload as any).params);
            }
            this.lastNetworkState = (payload as any).params;
            break;

          case "warning":
            this.logCallback("Warning: " + (payload as any).params);
            break;

          case "info":
            this.logCallback("Info: " + (payload as any).params);
            break;
        }
      }
    );
    //listen for the escape key
  
    invoke("init_process");
  }
  public async readLocalFile(file: string, callback: Function) {
      
      //if the file name contains .txt remove it
      if (file.endsWith(".txt")) {
        file = file.substring(0, file.length - 4);
      }
  
      const fs = await import("@tauri-apps/api/fs");
      const contents = await fs.readTextFile(file, { dir: BaseDirectory.App });
      this.logCallback("File read status: " + contents);
      callback(contents);

  }
  public async readFile(file: string, callback: Function) {

    //if the file name contains .txt remove it
    if (file.endsWith(".txt")) {
      file = file.substring(0, file.length - 4);
    }

    const fs = await import("@tauri-apps/api/fs");
    const contents = await fs.readTextFile(file + ".txt", { dir: BaseDirectory.AppCache });
    this.logCallback("File read status: " + contents);
    callback(contents);
  }
  public async writeFile(file: string, contents: string, callback: Function) {

    //if the file name contains .txt remove it
    if (file.endsWith(".txt")) {
      file = file.substring(0, file.length - 4);
    }

    if (contents) {
      const fs = await import("@tauri-apps/api/fs");
      const options = { dir: BaseDirectory.AppCache, overwrite: true, recursive: true };

      const stat = await fs.writeTextFile(file + ".txt", contents, options);
      this.logCallback("File write status: " + stat);
    }
    callback();
  }
  public printContent(content: string) {
    if (!content) return;
    const windowObj = window.open("", "PrintWindow", "height=400,width=600");

    if (windowObj) {
      windowObj.document.write(content);
      windowObj.document.close();
      windowObj.focus();
      windowObj.print();
      windowObj.close();
    }
  }
  public exit_fullscreen() {
    this.appWindow.setFullscreen(false);
  }
}