import { HtmlObj } from "../../core/HTMLObj";

export class IndexCard extends HtmlObj
{
    public title:string;
    public page:number;
    public isSubIndex:boolean;

    constructor(title:string, page:number,isSubIndex:boolean)
    {
        super(document.createElement("li"));
        this.title = title;
        this.page = page;
        this.isSubIndex = isSubIndex;
        this.render();
    }
    render()
    {
        if (this.el)
        {
        if (this.isSubIndex)
        {
            this.el.classList.add("subIndex");
            this.el.innerHTML = `<p></p><p>${this.title}</p><p>${this.page}</p>`;
        }
        else
        {
            this.el.classList.add("index");
            this.el.innerHTML = `<p>${this.title}</p><p>${this.page}</p>`;
        }
    }

    }
}