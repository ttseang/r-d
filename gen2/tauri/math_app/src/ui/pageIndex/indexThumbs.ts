import { UIStorage } from "../../core/UIStorage";
import { HtmlObj } from "../../core/HTMLObj";
import { IndexCard } from "./indexCard";
import { BookIndexVo } from "../../dataObjects/BookIndexVo";
import { SubIndexVo } from "../../dataObjects/SubIndexVo";
import { IndexAnimationVo } from "../../dataObjects/IndexAnimationVo";


export class IndexThumbs extends HtmlObj
{
    private uiStorage: UIStorage = UIStorage.getInstance();
    private bookIndexVos: BookIndexVo[] = [];
    private pages:BookIndexVo[][]=[];
    private pageIndex:number=0;
    private nextArrow: HTMLButtonElement | null = null;
    private prevArrow: HTMLButtonElement | null = null;

    private currentLetter:string="A";
    private animationImage: HTMLImageElement | null = null;

    constructor()
    {
        super(document.createElement("menu"));
        //const main: HTMLElement | null = document.querySelector("main");
        const prevArrow: HTMLButtonElement | null = document.querySelector(".navButtons>button:nth-child(3)");
        //select the 2nd button of main>nav
        const nextarrow: HTMLButtonElement | null = document.querySelector(".navButtons>button:nth-child(4)");
        if (nextarrow && prevArrow) {
           
           // this.tocThumbs.setArrows(nextarrow, prevArrow);
            this.setArrows(nextarrow, prevArrow);
        }
        this.makeLetterTabs();
        this.getData("A");        
    }
    public getData(letter:string)
    {
        console.log("get data for letter: " + letter);
        this.currentLetter=letter;
        const clickedLetter: HTMLElement | null = document.querySelector(".letterTab." + letter);
        //if the clicked letter is disabled, do nothing
        if (clickedLetter && clickedLetter.classList.contains("greyout")) return;
        
        if (clickedLetter)
        {
            const letterTabs: NodeListOf<HTMLElement> = document.querySelectorAll(".letterTab");
            for (let i = 0; i < letterTabs.length; i++)
            {
                const tab = letterTabs[i];
                tab.classList.remove("active");
            }
            clickedLetter.classList.add("active");
        }
        this.pages = this.uiStorage.splitToPages(this.uiStorage.getIndexPagesByLetter(letter.toLowerCase()));
        this.bookIndexVos=this.pages[this.pageIndex];
        console.log(this.bookIndexVos);
        this.pageIndex=0;
        this.makeThumbs();
        this.checkForLimits();
    }
    makeLetterTabs(): void
    {
        const letterString="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const letterArray=letterString.split("");
        const letterTabs: HTMLElement | null = document.querySelector("main>nav>ul");
        if (letterTabs)
        {
            letterTabs.innerHTML = "";
            for (let i = 0; i < letterArray.length; i++)
            {
                const letter = letterArray[i];
                const tab = document.createElement("button");
                tab.classList.add("letterTab");
                tab.classList.add(letter);

                if (this.uiStorage.checkIfLetterHasPages(letter.toLowerCase()) == false)
                {
                    tab.classList.add("greyout");
                }

                tab.innerHTML = letter;
                tab.addEventListener("click", () => this.getData(letter));
                letterTabs.appendChild(tab);
            }
        }
    }
    getAnimation()
    {
        if (this.animationImage)
        {
            this.animationImage.remove();
            this.animationImage = null;
        }
        const indexAnimation:IndexAnimationVo | undefined=this.uiStorage.getAnimationByLetterAndPage(this.currentLetter,this.pageIndex);
        if(indexAnimation)
        {
            //add an image to the page
            const img:HTMLImageElement=document.createElement("img");
            img.src="./assets/animations/"+indexAnimation.animation+".gif";
            img.style.position="absolute";
            img.style.left=indexAnimation.x+"%";
            img.style.top=indexAnimation.y+"%";
            img.style.width="calc("+indexAnimation.w+"* var(--vw))";
            img.style.height="calc("+indexAnimation.h+"* var(--vw))";
            img.classList.add("animation");
            if (this.el)
            {
                this.el.appendChild(img);
            }
            this.animationImage=img;
        }
    }
    makeThumbs(): void
    {       
        if (this.el)
        {
            this.el.innerHTML = "";
        }
      

        for (let i = 0; i < this.bookIndexVos.length; i++)
        {
            let bookIndexVo: BookIndexVo = this.bookIndexVos[i];
            let thumb = new IndexCard(bookIndexVo.title,bookIndexVo.page, false);
            if (this.el)
            {
                if (thumb.el)
                {
                    this.el.appendChild(thumb.el);
                }
            }
            

            for (let j = 0; j < bookIndexVo.subIndexes.length; j++)
            {
                let subIndexVo: SubIndexVo = bookIndexVo.subIndexes[j];
                let subThumb = new IndexCard(subIndexVo.title,subIndexVo.page, true);
                if (this.el && subThumb.el)
                {
                    this.el.appendChild(subThumb.el);
                }
            }
        }
        this.getAnimation();
    }
    nextPage()
    {
        if(this.pageIndex<this.pages.length-1)
        {
            this.pageIndex++;
            this.bookIndexVos=this.pages[this.pageIndex];
            this.makeThumbs();
            this.checkForLimits();
        }
    }
    prevPage()
    {
        if(this.pageIndex>0)
        {
            this.pageIndex--;
            this.bookIndexVos=this.pages[this.pageIndex];
            this.makeThumbs();
            this.checkForLimits();
        }
    }
    setArrows(nextarrow: HTMLButtonElement, prevArrow: HTMLButtonElement): void {
        console.log("nextarrow", nextarrow);
        console.log("prevArrow", prevArrow);


        this.nextArrow = nextarrow;
        this.prevArrow = prevArrow;
        this.nextArrow.addEventListener("click", () => { this.nextPage() });
        this.prevArrow.addEventListener("click", () => { this.prevPage() });

        this.checkForLimits();
    }
    checkForLimits(): void {
        if (!this.nextArrow || !this.prevArrow) return;
        console.log("checkForLimits");
        console.log("pageIndex", this.pageIndex);
        if (this.pageIndex == 0) {
            this.prevArrow.classList.add("disabled");
            console.log("prevArrow disabled");
        }
        else {
            console.log("prevArrow enabled");
            this.prevArrow.classList.remove("disabled");
        }
        if (this.pageIndex == this.pages.length - 1) {
            console.log("nextArrow disabled");
            this.nextArrow.classList.add("disabled");
        }
        else {
            console.log("nextArrow enabled");
            this.nextArrow.classList.remove("disabled");
        }
    }
    hideArrowButtons(): void {
        if (!this.nextArrow || !this.prevArrow) return;
        this.nextArrow.classList.add("disabled");
        this.prevArrow.classList.add("disabled");
    }
    showArrowButtons(): void {
        if (!this.nextArrow || !this.prevArrow) return;
        this.nextArrow.classList.remove("disabled");
        this.prevArrow.classList.remove("disabled");
    }
}
