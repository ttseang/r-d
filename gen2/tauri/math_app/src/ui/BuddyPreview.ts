import { SpinePlayer } from "@esotericsoftware/spine-player";
import { BuddyVo } from "../dataObjects/BuddyVo";

export class BuddyPreview {
    public element: HTMLElement;
    private buddyVo: BuddyVo;
    constructor(buddyVo: BuddyVo) {
        console.log("BuddyPreview");
        this.element = document.createElement("div");
        this.element.classList.add("buddy-preview");
        this.buddyVo = buddyVo;

        this.element.onclick = this.close.bind(this);
    }
    close()
    {
        this.element.remove();
    }
    render() {
        this.element.innerHTML = `<div class="buddyPreview">       
            <h2>${this.buddyVo.title}</h2>
            <p>Buddy Description</p>
            <div class="spinePlayerContainer">
            </div>
            </div>`;

        let playerContainer: HTMLElement | null = this.element.querySelector(".spinePlayerContainer");
        if (playerContainer) {
            const config: any = {
                width: 200,
                height: 200,
                jsonUrl: this.buddyVo.spineVo.jsonUrl,
                atlasUrl: this.buddyVo.spineVo.atlasUrl,
                loop: true,
                autoplay: true,
                scale: 1,
                showControls: false,
                success: this.onSpineLoaded.bind(this),
                animation: this.buddyVo.spineVo.animation,
                alpha: true, // Enable player translucency
                backgroundColor: "#00000000" // Background is fully transparent
            };

             new SpinePlayer(playerContainer, config);

          //  const canvas: HTMLCanvasElement = player.canvas;
            if (this.buddyVo.spineVo.charClass !== "" && this.buddyVo.spineVo.charClass !== undefined) {
                playerContainer.classList.add(this.buddyVo.spineVo.charClass);
            }


        }

    }
    onSpineLoaded(_player: SpinePlayer) {
        
        console.log("Spine loaded!");
    }
}