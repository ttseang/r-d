import { HtmlObj } from "../core/HTMLObj";
import { WallpaperVo } from "../dataObjects/WallpaperVo";
import { WallpaperCard } from "./WallpaperCard";

export class WallpaperThumbs extends HtmlObj {
    private wallpaperVos: WallpaperVo[];
    private callback: Function;
    private wallpaperCards: WallpaperCard[] = [];
    private selectedIndex: number = 0;
    constructor(wallpaperVos: WallpaperVo[], callback: Function) {
        super(document.querySelector(".wpthumbnails"));
        this.wallpaperVos = wallpaperVos;
        this.callback = callback;
        this.makeCards();
    }
    private makeCards() {
        for (let i: number = 0; i < this.wallpaperVos.length; i++) {
            let wallpaperCard: WallpaperCard = new WallpaperCard(this.wallpaperVos[i], () => { this.cardClicked(i) });
            wallpaperCard.render();
            if (this.el) {
                if (wallpaperCard.el) {
                    this.el.appendChild(wallpaperCard.el);
                }
            }
            this.wallpaperCards.push(wallpaperCard);
        }
        this.wallpaperCards[this.selectedIndex].select();
    }
    public deselect() {
        this.wallpaperCards[this.selectedIndex].deselect();
    }
    private cardClicked(index: number) {
        this.wallpaperCards[this.selectedIndex].deselect();
        this.wallpaperCards[index].select();
        this.selectedIndex = index;
        this.callback(this.wallpaperCards[this.selectedIndex].wallpaperVo);
    }
}