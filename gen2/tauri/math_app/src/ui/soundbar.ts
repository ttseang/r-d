import { SoundVo } from "../dataObjects/SoundVo";
import { Slider } from "./slider";

export class Soundbar
{
    private slider:Slider=new Slider("soundbar",0,()=>{});
    public el:HTMLDivElement;
    private sound:SoundVo;
    private index:number;

    constructor(index:number,soundVo:SoundVo)
    {
        this.index=index;
        this.sound=soundVo;
        this.el=document.createElement("div");
        this.el.classList.add("soundbar");
        this.el.innerHTML=this.getHtml();
    }
    
   
    getHtml()
    { 
        return `<div>${this.slider.getHtml()}</div>
                <div><button class='soundButton' data-index='${this.index}'></button></div>
                <div class='title'>${this.sound.name}</div>`;
    }
   
}