export namespace Pages
{
    export enum Page {
        Home,
        Wallpaper,
        Grades,
        Settings,
        Details,
        Stickers,
        BookIndex
    }
    export enum Actions
    {
        ChangePage
    }
    export enum CompleteState {
        complete,
        incomplete,
        failed,
        notStarted
    }
    export enum TrinayState {
        yes,
        no,
        na
    }

    
}