import { Pages } from "../constants/PageConstants";

//details viewedHint viewedSolution usedSecondChance 
export class GradeDetailVo {
    public details: string;
    public graded:boolean;
    public viewedHint: Pages.TrinayState;
    public viewedSolution: Pages.TrinayState;
    public usedSecondChance: Pages.TrinayState;
    public id: number;
    public status: Pages.CompleteState;


    constructor(id: number,graded:boolean=false, details: string="", viewedHint: Pages.TrinayState=Pages.TrinayState.na, viewedSolution: Pages.TrinayState=Pages.TrinayState.na, usedSecondChance: Pages.TrinayState=Pages.TrinayState.na,status:Pages.CompleteState=Pages.CompleteState.notStarted) {
        this.id = id;
        this.details = details;
        this.viewedHint = viewedHint;
        this.viewedSolution = viewedSolution;
        this.usedSecondChance = usedSecondChance;
        this.status = status;
        this.graded = graded;
    }
    fillWithRandValues() {
        this.details = "details" + Math.floor(Math.random() * 100);

        const triVals: Pages.TrinayState[] = [Pages.TrinayState.no, Pages.TrinayState.yes, Pages.TrinayState.na];

        this.viewedHint = triVals[Math.floor(Math.random() * 3)];

        this.viewedSolution = triVals[Math.floor(Math.random() * 3)];
        this.usedSecondChance = triVals[Math.floor(Math.random() * 3)];

        this.graded = Math.random() > 0.5;

        const statuses = [Pages.CompleteState.complete, Pages.CompleteState.incomplete, Pages.CompleteState.failed, Pages.CompleteState.notStarted];
        this.status =statuses[Math.floor(Math.random() * statuses.length)];
        return this;
    }
    private getTriString(triState: Pages.TrinayState): string
    {
        let str: string[]=["<td>Yes</td>", "<td>No</td>", "<td class='na'>N/A</td>"];
        return str[triState];
    }
    getHtml(): string {

        let completeHtml: string = "";
        switch(this.status){
            case Pages.CompleteState.complete:
                completeHtml = "<span class='C'></span>";
                break;
            case Pages.CompleteState.incomplete:
                completeHtml = "<span class='I'></span>";
                break;
            case Pages.CompleteState.failed:
                completeHtml = "<span class='X'></span>";
                break;
            case Pages.CompleteState.notStarted:
                completeHtml = "";
                break;
        }
        
        let isGradedHtml: string =(this.graded==true)?`<td class="mr ml">${this.details}</td>`:`<td class="mr ml">${this.details} <small>(not graded)</small></td>`;
        let viewedHintHtml: string = this.getTriString(this.viewedHint);
        let viewedSolutionHtml: string = this.getTriString(this.viewedSolution);
        let usedSecondChanceHtml: string = this.getTriString(this.usedSecondChance);

        
        
        let statusHtml: string = `
        <tr><td class="mr">${completeHtml}</td>
        ${isGradedHtml}
        <td class="ml"><button>E</button></td>
        ${viewedHintHtml}
        ${usedSecondChanceHtml}
        ${viewedSolutionHtml}        
        </tr>`;
        
        return statusHtml;
    }
}