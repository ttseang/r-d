import { SpineVo } from "./SpineVo";

export class BuddyVo
{
    public name:string;
    public title:string;
    public locked:boolean;
    public isNew:boolean;
    public spineVo:SpineVo;
    public vthumb:string;

    constructor(name:string,title:string,vthumb:string,charName:string,animation:string,charClass:string,locked:boolean=false,isNew:boolean=false)
    {
        this.name=name;
        this.title=title;
        this.vthumb=vthumb;       
        this.locked=locked;
        this.isNew=isNew;
        this.spineVo = new SpineVo(charName,animation,charClass);
    }
}