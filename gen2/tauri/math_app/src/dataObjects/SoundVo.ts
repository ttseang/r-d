//data object sound name, sound file, on/off
export class SoundVo {
    public id: number;
    public name: string;
    public file: string;
    public on: boolean;
    constructor(id: number, name: string = "", file: string = "", on: boolean = false) {
        this.id = id;
        this.name = name;
        this.file = file;
        this.on = on;
    }
}