

//create a data object with the following string properties lti,hint,mark,second,solution
export class ProblemVo {
    public id: number;
    public lti: string;
    public hint: string;
    public mark: string;
    public second: string;
    public solution: string;
    private graded:boolean;
    public name: string;

    constructor(id: number,name:string, lti: string, hint: string, mark: string, second: string, solution: string) {
        this.id = id;
        this.lti = lti;
        this.hint = hint;
        this.mark = mark;
        this.second = second;
        this.solution = solution;        
        this.name = name;
        this.graded = name.toLocaleLowerCase().includes("practice")?false:true;;
    }
    
        getHtml(): string {

            let completeHtml: string = `<span class='${this.mark}'></span>`;
            let isGradedHtml: string =(this.graded==true)?`<td class="mr ml">${this.name}</td>`:`<td class="mr ml">${this.name} <small>(not graded)</small></td>`;
         
            let statusHtml: string = `
            <tr><td class="mr">${completeHtml}</td>
            ${isGradedHtml}
            <td class="ml"><button>E</button></td>
            <td>${this.hint}</td>
            <td>${this.second}</td>
           <td>${this.solution}</td>
            </tr>`;
            
            return statusHtml;
        }
        
}