export class IndexAnimationVo {
    public letter: string = "";
    public page: number = 0;
    public animation: string = "";
    public x: number = 0;
    public y: number = 0;
    public w:number=0;
    public h:number=0;

    constructor(letter: string, page: number, animation: string, x: number, y: number,w:number,h:number) {
        this.letter = letter;
        this.page = page;
        this.animation = animation;
        this.x = x;
        this.y = y;
        this.w=w;
        this.h=h;
    }
}