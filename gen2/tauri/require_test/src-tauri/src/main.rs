// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::fs;
use std::io::prelude::*;
use std::net::TcpStream;
use tauri::Manager;
use std::env;

//use std::process::Command;

//use a generic struct to pass information to the frontend and derive clone and serde::Deserialize
#[derive(Clone, serde::Serialize)]
struct Payload {
    event: String,
    params: String,
}
//struct to hold the state of the app
#[derive(Default)]
struct MyManager {
    // Add your custom fields here
    app_focus: bool,
    connected: bool,
}

extern crate winapi;
use winapi::um::winuser::{GetForegroundWindow, GetWindowThreadProcessId};

fn talk_to_frontend(event_string: String, param_string: String, _app_handle: tauri::AppHandle) {
    let main_window = _app_handle.get_window("main").unwrap();

    //send the message to the frontend
    main_window
        .emit_all(
            "RUSTMSG",
            Payload {
                event: event_string.to_string(),
                params: param_string.to_string(),
            },
        )
        .unwrap();
}
//check if the app is connected to the internet
fn is_connected() -> bool {
    match TcpStream::connect("google.com:80") {
        Ok(_) => true,
        Err(_) => false,
    }
}
 fn get_app_path()->std::path::PathBuf
{
    let exe_path = env::current_exe().expect("Failed to get current exe path");
    println!("Current exe path: {:?}", exe_path);
    return exe_path;
}
//get file content with path passed as a string
#[tauri::command]
fn get_file_contents(path: String) -> Result<String, String> {
    //add the path to the file to the app path
    let app_path = get_app_path();
    let path = app_path.parent().unwrap().join(path);
    println!("filename: {}", path.display());

    let result = fs::read_to_string(path);
    match result {
        Ok(content) => Ok(content),
        Err(e) => Err(format!("Error reading file: {}", e)),
    }
}
#[tauri::command]
fn file_put_contents(filename: &str, data: &[u8],_app_handle: tauri::AppHandle) -> Result<String, String> {
    //add the path to the file to the app path
    let app_path = get_app_path();
    let filename = app_path.parent().unwrap().join(filename);
    talk_to_frontend("info".to_string(), filename.display().to_string(), _app_handle.clone());
    print!("filename: {}", filename.display());
    let mut file = match fs::File::create(filename) {
        Ok(file) => file,
        Err(e) =>{
            talk_to_frontend("error".to_string(), e.to_string(), _app_handle.clone());
            return Err(e.to_string())
        },
    };
   
    match file.write_all(data) {
        Ok(_) => Ok("".to_string()),
        Err(e) =>{
            talk_to_frontend("error".to_string(), e.to_string(), _app_handle.clone());
            return Err(e.to_string())
        },
        }
}

fn update_connected(state: &mut MyManager) {
    let connected = is_connected();
    if connected == true {
        state.connected = true;
    } else {
        state.connected = false;
    }
}
//register focus changed event with tauri and pass state to the event
fn update_app_focus(state: &mut MyManager) {
    unsafe {
        let foreground_window = GetForegroundWindow();

        if foreground_window.is_null() {
            state.app_focus = false;
            return;
        }

        let mut process_id = 0;
        GetWindowThreadProcessId(foreground_window, &mut process_id);

        if process_id == 0 {
            state.app_focus = false;
        }

        let current_process_id = std::process::id();
        if process_id == current_process_id {
            state.app_focus = true;
        } else {
            state.app_focus = false;
        }
    }
}

#[tauri::command]
fn init_process(_app_handle: tauri::AppHandle) {
   // println!("Init process");
    let mut state = MyManager {
        app_focus: false,
        connected: false,
    };
   
    std::thread::spawn(move || {
        loop {
            //make a copy of the state app_focus
            let last_focus = state.app_focus;
            update_app_focus(&mut state);
            if last_focus != state.app_focus {
               // println!("App focus changed");
                talk_to_frontend(
                    "focus_changed".to_string(),
                    state.app_focus.to_string(),
                    _app_handle.clone(),
                );
            }
            let current_connected: bool = state.connected;
            update_connected(&mut state);
            if current_connected != state.connected {
               // println!("Network connection changed");
                talk_to_frontend(
                    "network_changed".to_string(),
                    state.connected.to_string(),
                    _app_handle.clone(),
                );
            }
          
            std::thread::sleep(std::time::Duration::from_secs(1));
        }
    });
}
fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            init_process,
            get_file_contents,
            file_put_contents            
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");

}
