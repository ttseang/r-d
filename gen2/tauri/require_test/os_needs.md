# V5 Apps

## OS Commands we'll be needing
Mostly, our apps will work within their own beautiful, elegant JavaScript scope. However, there are some tasks we know they will need to talk to the operating system to get done. I am going to try to list all the ones that come to mind.

### Detect network connection
Our apps should be able, at all times,

 - to know whether the device is online, and
 - whether the online connection is metered (mobile data) or unmetered (wifi or ethernet)
 
### Detect whether app is in focus
The app should know whether it is running in the foreground or the background of the device.

### Print
Our apps should be able to send pages to the device's printing interface.

### Write to filesystem
We will definitely need permission to write to at least some part of the filesystem, to save and edit files that our app will need to read.

### Invoke web browser and navigate to a link
We should be able to launch the user's preferred web browser and have it go to a particular URL.