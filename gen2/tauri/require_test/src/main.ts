//import { invoke } from "@tauri-apps/api/tauri";
//import { appWindow } from '@tauri-apps/api/window';
import {  appCacheDir, appConfigDir, appDataDir } from '@tauri-apps/api/path';
import { TauriFun } from "./taurifun";
import { appWindow } from "@tauri-apps/api/window";

let textInput: HTMLInputElement | null;
let statusText: HTMLTextAreaElement | null;
let tauriFun: TauriFun | null= null;

async function readFile2()
{
   if (tauriFun)
   {
      tauriFun.readFile("test1", (contents:string)=>{
          if (textInput)
          {
              textInput.value = contents;
          }
      });
   }  
}
async function writeFile2()
{
  //write with Tauri's file system API
  const contents = textInput?.value;
  if (tauriFun)
  {
    if (contents)
    {
      tauriFun.writeFile("test1",contents,()=>{});
    }
  }
}
async function showPaths()
{

 /*  App cache dir: C:\Users\wcc19\AppData\Local\com.teachingtextbook.rtest\
App data dir: C:\Users\wcc19\AppData\Roaming\com.teachingtextbook.rtest\
App config dir: C:\Users\wcc19\AppData\Roaming\com.teachingtextbook.rtest\ */

  const appCacheDirPath = await appCacheDir();
  addStatusMessage("App cache dir: " + appCacheDirPath);
  //App cache dir: C:\Users\wcc19\AppData\Local\com.teachingtextbook.rtest\
  
  const appDataDirPath = await appDataDir();
  addStatusMessage("App data dir: " + appDataDirPath);
  //App data dir: C:\Users\wcc19\AppData\Roaming\com.teachingtextbook.rtest\

 

  const appConfigDirPath = await appConfigDir();
  addStatusMessage("App config dir: " + appConfigDirPath);
//App config dir: C:\Users\wcc19\AppData\Roaming\com.teachingtextbook.rtest\

  
}
function printTextArea()
{
    if (textInput)
    {
       if (tauriFun)
       {
          tauriFun.printContent(textInput.value);
       }
    }
}

function addStatusMessage(msg: string)
{
  if (statusText)
  {
    //append to the text area
    statusText.value += msg + "\r\n";

    //trim the text area to 5 lines
    let lines = statusText.value.split("\r\n");
    if (lines.length > 5)
    {
      lines = lines.slice(lines.length - 5);
      statusText.value = lines.join("\r\n");
    }

  }
}

window.addEventListener("DOMContentLoaded", async () => {
  textInput = document.querySelector(".ta1");
  statusText = document.querySelector(".status-msgs");
  
  tauriFun=new TauriFun(appWindow);
  tauriFun.logCallback = (msg:string) => addStatusMessage(msg);
  
  document
    .querySelector(".btnPrint")
    ?.addEventListener("click", () => printTextArea());
  document.querySelector(".btnReadFile")?.addEventListener("click", () => readFile2());
  document.querySelector(".btnWriteFile")?.addEventListener("click", () => writeFile2());
  document.querySelector(".btnShowPaths")?.addEventListener("click", () => showPaths());
 /*  invoke("init_process"); */

  //listen to rust messages
  /*  await appWindow.listen(
    'RUSTMSG',
    ({ event, payload }) => {
      console.log(event);
    //  console.log((payload as any).event, (payload as any).params);

      switch ((payload as any).event) {
        case 'focus_changed':
          console.log('focus-changed!', (payload as any).params);
          if (lastFocusState != (payload as any).params)
          {
            addStatusMessage("Focus state changed to " + (payload as any).params);
          }
          lastFocusState = (payload as any).params;
          break;
        case 'network_changed':
          console.log('network_changed', (payload as any).params);
          if (lastNetworkState != (payload as any).params)
          {
            addStatusMessage("Network state changed to " + (payload as any).params);
          }
          lastNetworkState = (payload as any).params;
          break;
        case "warning":
          console.log('warning', (payload as any).params);
          addStatusMessage("Warning: " + (payload as any).params);
          break;
        case "info":
          console.log('info', (payload as any).params);
          addStatusMessage("Info: " + (payload as any).params);
          break;
    }
  }
  );*/

}); 

