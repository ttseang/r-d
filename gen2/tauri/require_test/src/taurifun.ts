import { WebviewWindow } from '@tauri-apps/api/window';
import { BaseDirectory } from '@tauri-apps/api/path';
import { invoke } from '@tauri-apps/api';

export class TauriFun {
    private appWindow: WebviewWindow;
    private lastFocusState: boolean = false;
    private lastNetworkState: boolean = false;
    public focusCallback: Function=()=>{};
    public networkCallback: Function=()=>{};
    public logCallback: Function=()=>{};
    constructor(appWindow:WebviewWindow) {
        console.log("TauriFun constructor");
        this.appWindow = appWindow;
        this.setListeners();
    }
    private async setListeners() {
        await this.appWindow.listen(
            'RUSTMSG',
            ({ event, payload }) => {
              console.log(event);
        
              switch ((payload as any).event) {
                case 'focus_changed':
                  console.log('focus-changed!', (payload as any).params);
                  if (this.lastFocusState != (payload as any).params)
                  {
                    this.logCallback("Focus state changed to " + (payload as any).params);
                    this.focusCallback((payload as any).params);
                  }
                  this.lastFocusState = (payload as any).params;
                  break;
                case 'network_changed':
                  console.log('network_changed', (payload as any).params);
                  if (this.lastNetworkState != (payload as any).params)
                  {
                    this.logCallback("Network state changed to " + (payload as any).params);
                    this.networkCallback((payload as any).params);
                  }
                  this.lastNetworkState = (payload as any).params;
                  break;
                case "warning":
                  console.log('warning', (payload as any).params);
                  this.logCallback("Warning: " + (payload as any).params);
                  break;
                case "info":
                  console.log('info', (payload as any).params);
                  this.logCallback("Info: " + (payload as any).params);
                  break;
            }
          }
          );
          invoke("init_process");
    }
    public async readFile(file:string,callback:Function) {

        //if the file name contains .txt remove it
        if (file.endsWith(".txt")) {
            file = file.substring(0, file.length - 4);
        }

        const fs = await import("@tauri-apps/api/fs");
        const contents = await fs.readTextFile(file+".txt", { dir: BaseDirectory.AppCache });
        this.logCallback("File read status: " + contents);
        callback(contents);
    }
    public async writeFile(file:string,contents: string,callback:Function) {

        //if the file name contains .txt remove it
        if (file.endsWith(".txt")) {
            file = file.substring(0, file.length - 4);
        }

        if (contents) {
            const fs = await import("@tauri-apps/api/fs");
            const options = { dir: BaseDirectory.AppCache, overwrite: true, recursive: true };

            const stat = await fs.writeTextFile(file+".txt", contents, options);
            this.logCallback("File write status: " + stat);
        }
        callback();
    }
    public printContent(content: string) {
        if (!content) return;
        const windowObj = window.open("", "PrintWindow", "height=400,width=600");
       
        if (windowObj) {
         windowObj.document.write(content);
         windowObj.document.close();
         windowObj.focus();
         windowObj.print();
         windowObj.close();
        }
       }
}