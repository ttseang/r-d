// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]


use local_ip_address::local_ip;
use if_addrs;

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}! You've been greeted from Rust!", name)
}
#[tauri::command]
fn getip()->String
{
    let my_local_ip = local_ip().unwrap();
    //converts the ip to a string
    let my_local_ip2 = my_local_ip.to_string();
    return format!("Your local IP address is: {}", my_local_ip2);
}
#[tauri::command]
fn getos()->String
{
    let os = std::env::consts::OS;
    return format!("Your OS is: {}", os);
}
//get architecture
#[tauri::command]
fn getarch()->String
{
    let arch = std::env::consts::ARCH;
    return format!("Your architecture is: {}", arch);
}
#[tauri::command]
fn getinterface()
{
    let ifaces = if_addrs::get_if_addrs().unwrap();
    println!("Got list of interfaces");
    println!("{:#?}", ifaces);
}
fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![greet,getip,getos,getarch,getinterface])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
