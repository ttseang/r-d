import { invoke } from "@tauri-apps/api/tauri";


let greetMsgEl: HTMLElement | null;
let ipEl: HTMLElement | null;
let osEl: HTMLElement | null;
let arch
async function greet(text: string) {

  if (!greetMsgEl) {
    greetMsgEl = document.getElementById("greet-msg");

  }
  if (!greetMsgEl) {
    return;
  }
  greetMsgEl.textContent = await invoke("greet", {
    name: text
  });
}
async function getOs() {
  osEl = document.getElementById("os");
  if (!osEl) {
    return;
  }
  osEl.textContent = await invoke("getos", {});
}
//get ip
async function getIp() {
 
  ipEl = document.getElementById("ipaddress");
  if (!ipEl) {
    return;
  }

  ipEl.textContent =await invoke("getip", {});
}
//get arch
async function getArch() {
   
  arch = document.getElementById("arch");
  if (!arch) {
    return;
  }

  arch.textContent =await invoke("getarch", {});
}
async function getNetwork()
{
  /* let network = document.getElementById("network");
  if (!network) {
    return;
  } */

  //network.textContent =await 
  invoke("getinterface", {});
}
window.addEventListener("DOMContentLoaded", () => {
  greet("Hello World!");
  getIp();
  getOs();
  getArch();
  getNetwork();
});
