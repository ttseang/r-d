import { BonusVo } from "./dataObjs/BonusVo";
import { ProblemVo } from "./dataObjs/ProblemVo";
import { QuestionVo } from "./dataObjs/QuestionVo";
import { BonusButtons } from "./gameParts/bonusButtons";
import { Clock } from "./gameParts/clock";
import { InputBox } from "./gameParts/inputBox";
import { QuestionBox } from "./gameParts/questionBox";
import { HTMLUtil } from "./util/HTMLUtil";

export class BonusRound {
    private bb: BonusButtons;
    private bonusInput: InputBox;
    private buttonGo: HTMLButtonElement;
    private questionBox: QuestionBox;
    private questionIndexBox: HTMLElement;
    private game: HTMLElement;

    private correct: number = 0;
    private totalQuestions: number = 0;

    //
    //
    //
    private questionIndex: number = -1;
    //  private questions: QuestionVo[] = [];
    private problems: ProblemVo[] = [];

    private currentQuestion: ProblemVo | null = null;
    //
    //
    //    
    private clickLock: boolean = false;
    //
    //
    //
    private btnSkip: HTMLButtonElement;
    private btnClear: HTMLButtonElement;
    private btnBack: HTMLButtonElement;

    private message: HTMLElement;
    private subMessage: HTMLElement;

    private bonusVo: BonusVo;

    private btnSelectRound:HTMLButtonElement;

    constructor(bonusVo: BonusVo) {


        this.bonusVo = bonusVo;
        this.totalQuestions = bonusVo.probCount;
        this.problems = bonusVo.probs;

        let clock: Clock = new Clock("clock", this.timeUp.bind(this));
        clock.setTime(bonusVo.time);
        clock.start();
        //
        //
        //
        this.bb = new BonusButtons("bonusbuttons", this.addNum.bind(this));
        this.bonusInput = new InputBox("bonusInput");
        this.questionBox = new QuestionBox("problemBox");

        this.game = HTMLUtil.find("game");

        this.questionIndexBox = HTMLUtil.find("indexBox");

        this.buttonGo = HTMLUtil.findButton("btnGo");

        this.btnSelectRound=HTMLUtil.findButton("btnSelectRound");

        this.btnSkip = HTMLUtil.findButton("btnSkip");
        this.btnClear = HTMLUtil.findButton("btnClear");
        this.btnBack = HTMLUtil.findButton("btnBack");

        this.message = HTMLUtil.find("message");
        this.subMessage = HTMLUtil.find("subMessage");

        this.btnSelectRound.onclick=()=>{this.restart()};

        this.setUp();

        (window as any).bonusRound=this;
    }
    restart()
    {
        HTMLUtil.hide("game");
        window.location.reload();
    }
    setUp() {
        HTMLUtil.hide("redX");
        HTMLUtil.hide("greenCheck");

        HTMLUtil.hide("message");
        HTMLUtil.hide("subMessage");

        this.setButtonListeners();
        HTMLUtil.show("game");
        this.nextQuestion();

        document.addEventListener("keydown",this.getKey.bind(this))
    }
    getKey(e:KeyboardEvent)
    {
        if (this.clickLock === true) {
            return;
        }
        console.log(e.key);
        if (!isNaN(parseInt(e.key)))
        {
           
            this.bonusInput.addInput(e.key);
        }
        if (e.key==="Enter")
        {
            this.check();
        }
        if (e.key==="Backspace" || e.key==="Delete")
        {
            this.bonusInput.removeLast();
        }
        if (e.key==="Escape")
        {
            this.bonusInput.reset();
        }
        if (e.key===" ")
        {
            this.skipQuestion();
        }
    }
    setButtonListeners() {
        this.buttonGo.onclick = this.check.bind(this);
        this.btnSkip.onclick = this.skipQuestion.bind(this);
        this.btnClear.onclick = () => {
            this.bonusInput.reset();
        }
        this.btnBack.onclick = () => {
            this.bonusInput.removeLast();
        }

    }
    addNum(num: string) {
        if (this.clickLock === true) {
            return;
        }
        this.bonusInput.addInput(num);
    }
    check() {
        if (this.bonusInput.inputString==="")
        {
            return;
        }
        if (this.clickLock === false) {

            if (this.currentQuestion) {
                this.clickLock = true;

                let answer: string = this.bonusInput.inputString;
                let answer2: string = this.currentQuestion.answer;
                console.log(answer);
                console.log(answer2);
                console.log(parseInt(answer2) === parseInt(answer));

                if (parseInt(answer2) === parseInt(answer)) {
                    this.bonusInput.changeColor("lightgreen");
                    this.bonusInput.changeTextColor("white");

                    HTMLUtil.show("greenCheck");
                    this.correct++;

                    setTimeout(() => {
                        this.reset();
                    }, 1000);
                }
                else {
                    this.bonusInput.changeColor("red");
                    this.bonusInput.changeTextColor("white");
                    HTMLUtil.show("redX");

                    setTimeout(() => {
                        //this.reset();
                        this.bonusInput.setInput(answer2);
                        this.bonusInput.changeColor("lightgreen");
                        this.bonusInput.changeTextColor("black");
                        HTMLUtil.hide("redX");
                        setTimeout(() => {
                            this.reset();
                        }, 1000);
                    }, 1500);
                }

            }
        }
    }
    reset() {

        HTMLUtil.hide("redX");
        HTMLUtil.hide("greenCheck");

        this.bonusInput.reset();

        this.bonusInput.changeColor("skyblue");
        this.bonusInput.changeTextColor("black");
        this.nextQuestion();
        this.clickLock = false;
    }
    skipQuestion() {
        this.clickLock = false;
        this.nextQuestion();
    }
    nextQuestion() {
        this.questionIndex++;
        if (this.questionIndex > this.problems.length - 1) {
            this.gameOver();
            return;
        }
        this.currentQuestion = this.problems[this.questionIndex];
        this.updateProgress();

        let q: string = this.currentQuestion.p1 + this.bonusVo.operator + this.currentQuestion.p2;

        this.questionBox.setProblem(q);
    }
    updateProgress() {
        this.questionIndexBox.innerText = (this.questionIndex + 1).toString() + "/" + this.totalQuestions.toString();
    }
    timeUp() {
        HTMLUtil.hide("game");

        this.message.innerText = "Time Is Up";
       
        let scoreMsg:string= "Your Score:" + this.correct.toString() + "/" + this.totalQuestions.toString();
        scoreMsg+="<br/>";
        scoreMsg+="You earned "+this.calcBonusPoints().toString()+" bonus points";
        this.subMessage.innerHTML =scoreMsg;

        HTMLUtil.show("message");
        HTMLUtil.show("subMessage");


    }
    calcBonusPoints()
    {
        let points:number=0;
        for (let i:number=this.bonusVo.points.length-1;i>-1;i--)
        {
            let point:number=this.bonusVo.points[i];
           // console.log("point="+point);
            
            if (points===0 && this.correct>point-1)
            {
                points=i+1;
                //console.log("POINTS");
            }
        }
        return points;
    }
    gameOver() {
        HTMLUtil.hide("game");

        this.message.innerText = "Game Over";
        let scoreMsg:string= "Your Score:" + this.correct.toString() + "/" + this.totalQuestions.toString();
        scoreMsg+="<br/>";
        scoreMsg+="You earned "+this.calcBonusPoints().toString()+" bonus points";
        this.subMessage.innerHTML =scoreMsg;

        HTMLUtil.show("message");
        HTMLUtil.show("subMessage");
    }
}