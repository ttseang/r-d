import { HTMLUtil } from "../util/HTMLUtil";

export class SelectScreen {
    private div: HTMLElement;
    private fileList: HTMLSelectElement | null = null;
    private btnSelect:HTMLButtonElement;

    private callback:Function;

    private files: string = "TT.M3.B01,TT.M3.B02,TT.M3.B03,TT.M3.B04,TT.M3.B05,TT.M3.B06,TT.M3.B07,TT.M3.B08,TT.M3.B09,TT.M3.B10,TT.M3.B11,TT.M3.B12,TT.M3.B13,TT.M3.B14,TT.M3.B15,TT.M3.B16,TT.M3.B17,TT.M3.B18,TT.M3.B19,TT.M3.B20,TT.M3.B21,TT.M3.B22,TT.M3.B23,TT.M4.B01,TT.M4.B02,TT.M4.B03,TT.M4.B04,TT.M4.B05,TT.M4.B06,TT.M4.B07,TT.M4.B08,TT.M4.B09,TT.M4.B10,TT.M4.B11,TT.M4.B12,TT.M4.B13,TT.M4.B14,TT.M4.B15,TT.M4.B16,TT.M4.B17,TT.M4.B18,TT.M4.B19,TT.M4.B20,TT.M4.B21,TT.M4.B22,TT.M4.B23,TT.M4.B24";
    private fileArray:string[]=[];

    constructor(callback:Function) {
        this.callback=callback;
        this.div = HTMLUtil.find("selectScreen");
        this.btnSelect=HTMLUtil.findButton("btnSelect");

        this.btnSelect.onclick=this.buttonClick.bind(this);

        this.fileList = document.getElementById("fileList") as HTMLSelectElement || document.createElement("select");
        this.fillList();
        
    }
    private buttonClick()
    {
        if (this.fileList)
        {           
            this.callback(this.fileArray[this.fileList.selectedIndex]);
        }
       
    }
    private fillList() {
        console.log(this.fileList);

        if (this.fileList) {
            
            this.fileArray = this.files.split(",");
            console.log(this.fileArray);

            for (let i: number = 0; i < this.fileArray.length; i++) {

                let optEl:HTMLOptionElement=document.createElement("option");
                optEl.innerText=this.fileArray[i];
                this.fileList.append(optEl);
            }
        }

    }
}