import { BonusRound } from "../bonusRound";
import { BonusVo } from "../dataObjs/BonusVo";
import { BRLoader } from "../util/BRLoader";
import { HTMLUtil } from "../util/HTMLUtil";
import { SelectScreen } from "./selectScreen";

export class MainScreen {
    private mode: number = 0;
    private selectScreen: SelectScreen;

    constructor() {
        //  this.loadRound("TT.M3.B01");
        HTMLUtil.hide("message");
        HTMLUtil.hide("subMessage");
        this.selectScreen = new SelectScreen(this.selectFile.bind(this));
    }
    private selectFile(file:string)
    {
        console.log(file);
        this.loadRound(file);
    }
    public loadRound(filename: string) {
        let brl: BRLoader = new BRLoader(this.roundLoaded.bind(this));
        brl.loadRound(filename);
    }
    private roundLoaded(bonusVo: BonusVo) {
        HTMLUtil.hide("selectScreen");
        let bounusRound: BonusRound = new BonusRound(bonusVo);

    }
}