export class InputBox {
    private div: HTMLDivElement;
    public inputString: string = "";
    constructor(divID: string) {
        this.div = document.getElementById(divID) as HTMLDivElement || document.createElement("div") as HTMLDivElement;
        if (!this.div.id) {
            this.div.id = divID;
        }
        this.updateDisplay();
    }
    changeColor(color: string) {
        if (this.div) {
            this.div.style.backgroundColor = color;
        }
    }
    changeTextColor(color: string) {
        if (this.div) {
            this.div.style.color = color;
        }
    }
    addInput(num: string) {
        if (this.inputString.length < 3) {
            this.inputString += num;
            this.updateDisplay();
        }

    }
    reset() {
        this.inputString = "";
        this.updateDisplay();
    }
    setInput(num: string) {
        this.div.innerHTML = "<span>" + num + "</span>";
    }
    removeLast() {
        if (this.inputString.length > 0) {
            this.inputString = this.inputString.substring(0, this.inputString.length - 1);
            this.updateDisplay();
        }

    }
    updateDisplay() {
        this.div.innerHTML = "<span>" + this.inputString + "</span><span class='bonusCursor'>|</span>";
    }
}