import { HTMLUtil } from "../util/HTMLUtil";

export class QuestionBox
{
    private div:HTMLElement;

    constructor(divID:string)
    {
        this.div=HTMLUtil.find(divID);

    }
    setProblem(text:string)
    {
        this.div.innerText=text;
    }
}