export class BonusButtons {
    private div: HTMLDivElement;
    private callback:Function;

    constructor(divID: string,callback:Function=()=>{}) {
        this.div = document.getElementById(divID) as HTMLDivElement || document.createElement("div") as HTMLDivElement;

        if (!this.div.id) {
            this.div.id = divID;
        }
        this.callback=callback;

        this.makeButtons();
    }
    private makeButtons() {

        for (let i: number = 0; i < 10; i++) {

            let buttonCode: HTMLButtonElement = document.createElement("button");
            buttonCode.innerText = i.toString();
            buttonCode.classList.add("squarebutton");
            buttonCode.id = "bbutton" + i.toString();
            buttonCode.onclick=()=>{this.callback(i)};
            this.div.append(buttonCode);

        }

    }
}