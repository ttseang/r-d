export class Clock {
    private time: number = 80;
    private handle: any;
    private div: HTMLElement;
    private callback: Function;

    constructor(divID: string, callback: Function = () => { }) {
        this.div=document.getElementById(divID) || document.createElement("div");
        if (!this.div.id)
        {
            this.div.id=divID;
        }
        
        this.callback = callback;
    }
    setTime(time: number) {
        this.time = time;
        this.div.innerText = this.formatTime();
    }
    private formatTime() {
        let mins: number = Math.floor(this.time / 60);
        let secs: number = this.time - mins*60;


        let minString: string = mins.toString();
        if (minString.length < 2) {
            minString = "0" + minString;
        }

        let secString: string = secs.toString();
        if (secString.length < 2) {
            secString = "0" + secString;
        }
        return minString + ":" + secString;
    }
    doTick() {
        this.time--;
        if (this.time === 0) {
            this.callback();
            this.stop();
        }
        this.div.innerText = this.formatTime();
    }
    start() {
        this.handle = setInterval(this.doTick.bind(this), 1000);
    }
    stop() {
        clearInterval(this.handle);
    }
}