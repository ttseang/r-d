import { BonusVo } from "../dataObjs/BonusVo";
import { ProblemVo } from "../dataObjs/ProblemVo";

export class BRLoader {
    private callback:Function;

    constructor(callback:Function) {
        this.callback=callback;
    }
    public loadRound(filename: string) {
        fetch("./assets/brd/" + filename + ".json")
            .then(response => response.json())
            .then(data => this.process(data));
    }
    private process(data: any) {
        console.log(data);

        let problems: any = data.probs;

        let problemObjs: ProblemVo[] = [];

        for (let i: number = 0; i < problems.length; i++) {
            let problem: any = problems[i];
            let problemVo: ProblemVo = new ProblemVo(problem[0], problem[1], problem[2]);
            problemObjs.push(problemVo);
        }

        let bonusVo:BonusVo=new BonusVo(data.lti,parseInt(data.time),parseInt(data.intro),problemObjs,parseInt(data.round),data.course,data.points,data.operator,parseInt(data.probcount));
        console.log(bonusVo);
        this.callback(bonusVo);
    }
}