import { QuestionVo } from "../dataObjs/QuestionVo";

export class QuestionGen
{
    constructor()
    {

    }
    makeQuestions()
    {
        let questionArray:QuestionVo[]=[];

        for(let i:number=0;i<40;i++)
        {
            let n1:number=Math.floor(Math.random()*10);
            let n2:number=Math.floor(Math.random()*10);

            let q:string=n1.toString()+"+"+n2.toString()+"=";
            let answer:number=n1+n2;

            let questionVo:QuestionVo=new QuestionVo(q,answer.toString());
            questionArray.push(questionVo);
        }

        let data:any=JSON.stringify(questionArray);
        console.log(data);
    }
}