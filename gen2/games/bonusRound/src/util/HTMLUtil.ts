export class HTMLUtil
{
    public static show(id:string)
    {
        let div:HTMLElement | null=document.getElementById(id);

        if (div)
        {
            div.style.visibility="visible";
        }
    }
    public static hide(id:string)
    {
        let div:HTMLElement | null=document.getElementById(id);

        if (div)
        {
            div.style.visibility="hidden";
        }
    }
    public static find(id:string)
    {
        let div:HTMLElement | null=document.getElementById(id);
        if (div===null)
        {
            div=document.createElement("div");
            div.id=id;
        }
        return div;
    }
    public static findButton(id:string)
    {
        let button:HTMLButtonElement | null=document.getElementById(id) as HTMLButtonElement;
        if (button===null)
        {
            button=document.createElement("button");
            button.id=id;
        }
        return button;
    }
}