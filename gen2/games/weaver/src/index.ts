import { DataLoader } from "./classes/DataLoader";
import { WeaverGame } from "./classes/WeaverGame";

window.onload=function()
{
    const dataLoader:DataLoader = new DataLoader(
        ()=>{
            const weaverGame:WeaverGame = new WeaverGame();
        }
    );
    dataLoader.load();
   
}