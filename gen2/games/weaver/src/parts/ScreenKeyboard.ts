export class ScreenKeyboard
{
    private container: HTMLElement;
    private keyboard: HTMLElement;
    private letters: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    private disabled: boolean = false;
    private callback: Function;
    constructor(container: HTMLElement,callback:Function)
    {
        console.log("ScreenKeyboard constructor");
        this.container = container;
        this.keyboard = document.createElement("div");
        this.callback = callback;
    }
    buildKeyboard()
    {
        console.log("ScreenKeyboard buildKeyboard");
       
        this.keyboard.classList.add("keyboard");
        for (let i = 0; i < this.letters.length; i++)
        {
            const letter: HTMLElement = document.createElement("div");
            letter.classList.add("letter");
            letter.innerHTML = this.letters[i];
            this.keyboard.appendChild(letter);
        }
        this.container.appendChild(this.keyboard);

        this.addListeners();
    }
    //add listeners to keyboard children
    addListeners()
    {
        const letters: NodeListOf<HTMLElement> = this.keyboard.querySelectorAll(".letter");
        for (let i = 0; i < letters.length; i++)
        {
            letters[i].addEventListener("click", (e: MouseEvent) => {
                const letter: HTMLElement = e.target as HTMLElement;
                this.clickLetter(letter.innerHTML);
            });
        }
    }
    clickLetter(letter: string)
    {
        if (this.disabled) return;
      // console.log("ScreenKeyboard clickLetter: " + letter);
        this.callback(letter);
    }
    disableKeyboard()
    {
        this.disabled = true;
        this.keyboard.classList.add("disabled");
    }
    enableKeyboard()
    {
        this.disabled = false;
        this.keyboard.classList.remove("disabled");
    }
}