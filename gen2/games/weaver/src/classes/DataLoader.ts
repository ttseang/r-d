import { GameModel } from "./GameModel";
import { WordVo } from "./WordVo";

export class DataLoader
{
    private callback: Function;
    constructor(callback:Function)
    {
       this.callback = callback;
    }
    load()
    {
        fetch("./assets/gameData.json")
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data: any)
    {
        console.log(data.gameData);
        //split the data into WordVo objects
        const gameData: WordVo[] = [];
        //log out each data object
        data.gameData.forEach((wordData: any) =>
        {
          console.log(wordData);
          let startWord: string = wordData[0];
            let targetWord: string = wordData[1];
            let moves: number = wordData[2];
            let wordList: string[] = wordData[3];
            let wordVo: WordVo = new WordVo(startWord, targetWord, moves, wordList);
            gameData.push(wordVo);

        });
        GameModel.getInstance().gameData = gameData;
        //load the dictionary
        this.loadDictionary();
      //  this.callback(data);
    }
    loadDictionary()
    {
        fetch("./assets/dict.txt").then(response => response.text()).then(data => this.proccessDictionary(data));
    }
    proccessDictionary(data: string)
    {
        const dictionary: string[] = data.split("\r\n");
        GameModel.getInstance().setDictionary(dictionary);
        this.callback();
    }

}