export class WordVo
{
  
    public startWord: string;
    public targetWord: string;
    public moves: number;
    public wordList: string[];
    constructor(startWord:string, targetWord:string,moves:number,wordList:string[])
    {
        this.startWord = startWord;
        this.targetWord = targetWord;
        this.moves = moves;
        this.wordList = wordList;
    }
}