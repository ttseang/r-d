import { WordVo } from "./WordVo";

export class GameModel
{
    //make a singleton
    private static _instance: GameModel;
    public gameData: WordVo[]=[];
    public dictionary: string[]=[];

    private dictMap: Map<string, string> = new Map<string, string>();

    public static getInstance(): GameModel
    {
        if (!GameModel._instance)
        {
            GameModel._instance = new GameModel();
        }
        return GameModel._instance;
    }
    constructor()
    {
        //do nothing
        (window as any).gameModel = this;
    }
    public setDictionary(dictionary: string[])
    {
        this.dictionary =dictionary;
        this.dictionary.forEach((word: string) =>
        {
            this.dictMap.set(word, word);
        });
    }
    public checkDictionary(word: string): boolean
    {
        return this.dictMap.has(word.toLowerCase());
    }
    //get ramdom level
    public getRandomLevel(): WordVo
    {
        const index: number = Math.floor(Math.random() * this.gameData.length);
        return this.gameData[index];

    }
}