import { ScreenKeyboard } from "../parts/ScreenKeyboard";
import { GameModel } from "./GameModel";
import { WordVo } from "./WordVo";

export class WeaverGame
{
    private keyboard: ScreenKeyboard;
    private currentWord: string="GAME";
    private currentWordArray: string[] = [];
    private targetWord: string = "SHOW";

    private currentIndex: number = 0;
    private currentLetter: string = "";
    //halt, heal, teal, tell, tall, toll, roll, rook
    private targetWordArray: string[] = [];
    private gameModel: GameModel=GameModel.getInstance();
    private levelVo:WordVo = new WordVo("xxxx","xxxx",0,[]);
    private alertMessage:HTMLElement;
    constructor()
    {
        const container: HTMLElement = document.querySelector(".keyboard-container") as HTMLElement;
        this.keyboard = new ScreenKeyboard(container,this.keyboardClicked.bind(this));
        this.keyboard.buildKeyboard();

        this.alertMessage = document.querySelector(".alertMessage") as HTMLElement;
        this.alertMessage.style.visibility="hidden";

        (window as any).weaverGame = this;
        this.keyboard.disableKeyboard();
        this.setButtonListeners();
        this.startLevel();
    }
    setButtonListeners()
    {
        const resetButton: HTMLElement = document.querySelector(".btnReset") as HTMLElement;
        resetButton.addEventListener("click",()=>{this.resetLevel()} );

        const solveButton: HTMLElement = document.querySelector(".btnSolve") as HTMLElement;
        solveButton.addEventListener("click",()=>{this.solve()} );
    }
    startLevel()
    {
        this.levelVo = this.gameModel.getRandomLevel();
        this.resetLevel(this.levelVo);
    }
    resetLevel(wordVo:WordVo=this.levelVo)
    {
        //empty the guessed words
        const guessedWordContainer: HTMLElement = document.querySelector(".guessedWords") as HTMLElement;
        guessedWordContainer.innerHTML = "";

        this.targetWord = wordVo.targetWord;
        this.currentWord = wordVo.startWord;

        //convert to uppercase
        this.targetWord = this.targetWord.toUpperCase();
        this.currentWord = this.currentWord.toUpperCase();

        this.setTargetWord(this.targetWord);
        this.setCurrentWord(this.currentWord);
    }
    setAlertMessage(message:string)
    {
        this.alertMessage.innerHTML=message;
        this.alertMessage.style.visibility="visible";
        setTimeout(() => {
            this.alertMessage.style.visibility="hidden";
        }, 2000);
    }
    setListeners()
    {
        const currentWordContainer: HTMLElement = document.querySelector(".currentWord .wordRow") as HTMLElement;
        //get children of currentWordContainer
        const currentWordChildren: HTMLCollection = currentWordContainer.children;
        for (let i = 0; i < currentWordChildren.length; i++)
        {
            const child: HTMLElement = currentWordChildren[i] as HTMLElement;
            child.addEventListener("click",()=>{this.letterClicked(i)} );
        }
    }
    removeListeners()
    {
        const currentWordContainer: HTMLElement = document.querySelector(".currentWord .wordRow") as HTMLElement;
        //get children of currentWordContainer
        const currentWordChildren: HTMLCollection = currentWordContainer.children;
        for (let i = 0; i < currentWordChildren.length; i++)
        {
            const child: HTMLElement = currentWordChildren[i] as HTMLElement;
            child.removeEventListener("click",()=>{this.letterClicked(i)} );
        }
    }
    letterClicked(index: number)
    {
        const currentWordContainer: HTMLElement = document.querySelector(".currentWord .wordRow") as HTMLElement;
        //get the child at index
        const child: HTMLElement = currentWordContainer.children[index] as HTMLElement;
        //remove the selected class from all children
        for (let i = 0; i < currentWordContainer.children.length; i++)
        {
            const child: HTMLElement = currentWordContainer.children[i] as HTMLElement;
            child.classList.remove("selected");
        }
        child.classList.add("selected");


        this.currentIndex = index;
        this.currentLetter = this.currentWordArray[index];
        this.keyboard.enableKeyboard();
    }
    resetOnInvalid()
    {
        const currentWordContainer: HTMLElement = document.querySelector(".currentWord .wordRow") as HTMLElement;
        //get the child at index
        

         //remove the selected class from all children
         for (let i = 0; i < currentWordContainer.children.length; i++)
         {
             const child: HTMLElement = currentWordContainer.children[i] as HTMLElement;
             child.classList.remove("selected");
         }
         this.keyboard.disableKeyboard();
    }
    keyboardClicked(letter: string)
    {
        this.keyboard.disableKeyboard();
        console.log("WeaverGame letterClicked: " + letter);
        console.log(this.currentWordArray);
        
        let currentCopy:string[] = this.currentWordArray.slice();
        currentCopy[this.currentIndex] = letter;
        let newWord:string=currentCopy.join("");

        //check if newWord is in dictionary
        if(this.gameModel.checkDictionary(newWord))
        {
            this.addGuessedWord(this.currentWord);
            console.log("WeaverGame newWord: " + newWord);
            this.setCurrentWord(newWord);
            if (newWord == this.targetWord)
            {
                setTimeout(() => {
                    alert("You Win!");
                this.startLevel();
                }, 2000);
                
            }
        }
        else
        {
            this.resetOnInvalid();
            console.log("WeaverGame invalid newWord: " + newWord);
           this.setAlertMessage("Not a valid word");
        }

       
    }
    setTargetWord(word: string)
    {
        this.targetWord = word;
        this.targetWordArray = word.split("");
        const targetWordContainer: HTMLElement = document.querySelector(".targetWord .wordRow") as HTMLElement;
        targetWordContainer.innerHTML = "";
        for (let i = 0; i < this.targetWordArray.length; i++)
        {
            const letter: HTMLElement = document.createElement("div");
            letter.classList.add("letterTile");
            letter.innerHTML = this.targetWordArray[i];
            targetWordContainer.appendChild(letter);
        }
    }
    setCurrentWord(word: string)
    {
        this.removeListeners();
        this.currentWord = word;
        this.currentWordArray = word.split("");
        const currentWordContainer: HTMLElement = document.querySelector(".currentWord .wordRow") as HTMLElement;
        currentWordContainer.innerHTML = "";
        for (let i = 0; i < this.currentWordArray.length; i++)
        {
            const letter: HTMLElement = document.createElement("div");
            letter.classList.add("letterTile");
            let text:string = this.currentWordArray[i];
            if (text == this.targetWord[i])
            {
                letter.classList.add("correct");
            }
           
            letter.innerHTML = this.currentWordArray[i];
            currentWordContainer.appendChild(letter);
        }
        this.setListeners();
    }
    addGuessedWord(word: string)
    {
        const guessedWordContainer: HTMLElement = document.querySelector(".guessedWords") as HTMLElement;
        const wordRow: HTMLElement = document.createElement("div");
        wordRow.classList.add("wordRow");
        for (let i = 0; i < word.length; i++)
        {
            const letter: HTMLElement = document.createElement("div");
            letter.classList.add("letterTile");
            letter.innerHTML = word[i];
            wordRow.appendChild(letter);
        }
        guessedWordContainer.prepend(wordRow);

    }
    solve()
    {
        const guessedWordContainer: HTMLElement = document.querySelector(".guessedWords") as HTMLElement;
        guessedWordContainer.innerHTML = "";
        let words: string[] = this.levelVo.wordList;
        for (let i = 0; i < words.length; i++)
        {
            this.addGuessedWord(words[i]);
        }
    }
}