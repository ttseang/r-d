import { ScreenVo } from "../dataObjects/ScreenVo";
import { IScreen } from "../interfaces/IScreen";
import { ScreenGame } from "../Screens/ScreenGame";
import { ScreenStart } from "../Screens/ScreenStart";

export class ScreenManager {
    private screens: ScreenVo[];
    private currentScreen: IScreen | null = null;
    constructor() {
        const startScreen: ScreenVo = new ScreenVo("start", new ScreenStart(this));
        const gameScreen: ScreenVo = new ScreenVo("game", new ScreenGame(this));
        this.screens = [startScreen, gameScreen];
        this.currentScreen = this.screens[0].screenClass;
        this.screens[0].screenClass.start();
    }
    public changeScreen(screenName: string): void {
        if (this.currentScreen) {
            this.currentScreen.destroy();
        }
        for (let i: number = 0; i < this.screens.length; i++) {
            if (this.screens[i].screenName == screenName) {
                this.currentScreen = this.screens[i].screenClass;
                this.currentScreen.start();
            }
        }
    }
}