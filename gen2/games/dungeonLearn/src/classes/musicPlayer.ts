//make a singleton
export class MusicPlayer 
{
    private static instance: MusicPlayer;
    private music: Phaser.Sound.BaseSound | null=null;
    private musicName: string;
    private musicVolume: number;
    private musicMuted: boolean;
    private scene:Phaser.Scene | null=null;
    private constructor() {
        this.musicName = "";
        this.musicVolume = 1;
        this.musicMuted = false;
    }
    public static getInstance(): MusicPlayer {
        if (!MusicPlayer.instance) {
            MusicPlayer.instance = new MusicPlayer();
        }
        return MusicPlayer.instance;
    }
    public setScene(scene:Phaser.Scene)
    {
        this.scene=scene;
    }
    public setMusic(musicName:string)
    {
        this.musicName=musicName;
        if (this.scene)
        {
            if (this.music)
            {
                this.music.stop();
                this.music=null;
            }
            this.music = this.scene.sound.add(this.musicName,{loop:true,volume:this.musicVolume,mute:this.musicMuted});
          //  this.music.loop = true;
           // this.music.volume=this.musicVolume;
           // this.music.setMute(this.musicMuted);
            this.music.play();
        }
    }
    playSound(soundName:string)
    {
        if (this.scene)
        {
            let sound:Phaser.Sound.BaseSound = this.scene.sound.add(soundName);
            sound.play();
        }
    }
    public stopMusic()
    {
        if (this.music)
        {
            this.music.stop();
            this.music=null;
        }
    }
}