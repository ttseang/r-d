import { QuestionSet } from "../dataObjects/QuestionSet";
import { QuestionVo } from "../dataObjects/QuestionVo";

export class DungeonModel
{
    //make a singleton
    private static instance: DungeonModel;
    public battleQuestions: QuestionSet = new QuestionSet();
    public roomQuestions: QuestionSet = new QuestionSet();
    public battleFlag: boolean = false;
    public buttonsSet: boolean = false;
    public questionsRight: number = 0;
    private constructor()
    {

    }
    public static getInstance(): DungeonModel
    {
        if (!DungeonModel.instance)
        {
            DungeonModel.instance = new DungeonModel();
        }
        return DungeonModel.instance;
    }
    shuffleArray(array: any[]) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }
    public getBattleQuestion(): QuestionVo
    {
       return this.battleQuestions.getQuestion();
    }
    public getRoomQuestion(): QuestionVo
    {
        return this.roomQuestions.getQuestion();
    }
}