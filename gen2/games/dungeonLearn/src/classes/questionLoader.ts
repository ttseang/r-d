import { QuestionSet } from "../dataObjects/QuestionSet";
import { QuestionVo } from "../dataObjects/QuestionVo";

export class QuestionLoader
{
    private callback:Function;

    constructor(callback:Function)
    {
        this.callback = callback;
    }
    loadFile(file: string)
    {
        //load json
        fetch("./assets/data/"+file+".json")
        .then(response => response.json())
        .then(data => this.process(data ));
    }
    process(data: any)
    {
        console.log(data);
        let questionSet:QuestionSet= new QuestionSet();
        //process json
        let questionData:QuestionVo[] = [];
        const format= data.format;
       // console.log("format: "+format);
        const questions = data.questions;
        for (let i = 0; i < questions.length; i++) {
            const question = questions[i];
           // console.log(question);
            //create question object
            let q:string = question.q;
            let answer:string = (format=="multiple")? question.r: question.a;
            let wrongChoices:string[] = (format=="multiple")? question.a: [];
            let questionVo:QuestionVo= new QuestionVo(q, answer, wrongChoices);
            questionVo.index = i;
            questionData.push(questionVo);
        }
        if (format=="translation")
        {
            for (let i = 0; i < questionData.length; i++) {
                const question = questionData[i];
                //pick 3 random wrong answers from the other questions
                let wrongChoices:string[] = [];
                for (let j = 0; j < 3; j++) {
                    let randomIndex = Math.floor(Math.random() * questionData.length);
                    while(randomIndex==question.index)
                    {
                        randomIndex = Math.floor(Math.random() * questionData.length);
                    }
                    wrongChoices.push(questionData[randomIndex].answer);
                }
                question.wrongChoices = wrongChoices;
            }
        }
        questionSet.questions = questionData;
        questionSet.instructions = data.instructions;
        questionSet.title = data.title;
        questionSet.randomizeQuestions();
        this.callback(questionSet);
    }
}