export interface IScreen {
    start(): void;
    destroy(): void;
}