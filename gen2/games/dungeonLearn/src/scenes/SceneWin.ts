import { BaseScene } from "phaser-utility";
import { MusicPlayer } from "../classes/musicPlayer";

export class SceneWin extends BaseScene
{
    constructor()
    {
        super("SceneWin");
    }
    create(): void {
        super.create();
        this.setSize();
        this.makeGrid(11,11);

        let bg:Phaser.GameObjects.Image=this.placeImage2("win",5,5,1);
        bg.setOrigin(0.5,0.5);
        bg.setInteractive();
        bg.once("pointerdown",this.startGame,this);

        const questionText = document.querySelector(".questionText");
        if (questionText) {
            questionText.innerHTML = "";
        }
        const musicPlayer:MusicPlayer=MusicPlayer.getInstance();
        musicPlayer.setMusic("winMusic");
    }
    startGame()
    {
        window.location.reload();
    }
    setSize() {
        let canvas = document.getElementById("droom");
        if (canvas) {
            let w = canvas.clientWidth;
            let h = canvas.clientHeight;
            console.log("w: " + w + " h: " + h);
            this.resetSize(w, h);
          

            /*  this.game.config.width = w;
             this.game.config.height = h; */
        }
    }
}