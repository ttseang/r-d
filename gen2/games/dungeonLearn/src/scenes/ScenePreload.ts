import { BaseScene } from "phaser-utility";
import { MusicPlayer } from "../classes/musicPlayer";

export class ScenePreload extends BaseScene {
  constructor() {
    super("ScenePreload");
  }

  init()
  {
     //listen to progress event
        this.load.on('progress', this.showProgress, this);
  }
  preload() {
    super.create();
    this.setSize();
    this.load.image("title", "./assets/screens/titleScreen.jpg");
    this.load.image("win", "./assets/screens/winScreen.jpg");
    this.load.image("over", "./assets/screens/gameOver.jpg");
    this.load.image("floorFrame", "./assets/floors/floorFrame.png");
    this.load.image("floor1", "./assets/floors/floor1.png");
    this.load.atlas("knight", "./assets/sprites/light.png", "./assets/sprites/light.json");
    this.load.atlas("items", "./assets/sprites/items.png", "./assets/sprites/items.json");
    this.load.atlas("doors", "./assets/sprites/doors.png", "./assets/sprites/doors.json");
    this.load.atlas("skeleton", "./assets/sprites/skeleton.png", "./assets/sprites/skeleton.json");
    //dragon
    this.load.atlas("dragon", "./assets/sprites/dragon.png", "./assets/sprites/dragon.json");
    //load backgrounds
    for (let i = 1; i < 8; i++) {
        this.load.image("bg" + i, "./assets/battlebg/" + i + ".png");
    }
    this.load.audio("roomMusic", "./assets/bgMusic/destiny.mp3");
    this.load.audio("battleMusic", "./assets/bgMusic/battle.mp3");
    this.load.audio("gameOverMusic", "./assets/bgMusic/gameOverMusic.wav");
    this.load.audio("winMusic", "./assets/bgMusic/winMusic.mp3");
    //load sound effects
    this.load.audio("skeleton_hit", "./assets/sfx/skeleton_hit.wav");
    this.load.audio("skeleton_die", "./assets/sfx/skeleton_die.wav");
    this.load.audio("skeleton_start", "./assets/sfx/skeleton_start.wav");

    this.load.audio("dragon_hit", "./assets/sfx/dragon_hit.wav");
    this.load.audio("dragon_die", "./assets/sfx/dragon_die.wav");
    this.load.audio("dragon_start", "./assets/sfx/dragon_start.mp3");

    this.load.audio("knight_hit", "./assets/sfx/knight_hit.wav");
    this.load.audio("knight_die", "./assets/sfx/knight_die.wav");

    this.load.audio("hit", "./assets/sfx/hit.mp3");
    this.load.audio("door", "./assets/sfx/door.mp3");
    this.load.audio("levelup", "./assets/sfx/level_up.wav");

  }
  //show progress bar

  showProgress(progress:number) {
       let per:number=progress*100;
       console.log("progress: "+per);
        let progressBar = this.add.graphics();
        progressBar.fillStyle(0x222222, 0.8);
        progressBar.fillRect(this.gw*.05, this.gh/2, this.gw*.9, this.gh*.1);
        let progressBox = this.add.graphics();
        progressBox.fillStyle(0xff0000, 0.8);
        progressBox.fillRect(this.gw*.05, this.gh/2, this.gw*.9*progress, this.gh*.1);
        
        let loadingText = this.make.text({
            x: this.gw*.05,
            y:this.gh/2,
            text: 'Loading...',
            style: {font: '20px monospace',color: '#ffffff'}
        });
  }
  setSize() {
    let canvas = document.getElementById("droom");
    if (canvas) {
        let w = canvas.clientWidth;
        let h = canvas.clientHeight;
        console.log("w: " + w + " h: " + h);
        this.resetSize(w, h);
        //this.canvasHeight = h;
       // this.canvasWidth = w;

        /*  this.game.config.width = w;
         this.game.config.height = h; */
    }
}
  create() {
    const musicPlayer:MusicPlayer=MusicPlayer.getInstance();
    musicPlayer.setScene(this);
    
      this.scene.start("SceneTitle");
  }
}