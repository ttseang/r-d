
import { Align, AlignGrid, IBaseScene, IGameObj } from "phaser-utility";
import { BaseScene } from "phaser-utility/scenes/BaseScene";
import { DungeonModel } from "../classes/dungeonModel";
import { MusicPlayer } from "../classes/musicPlayer";
import { QuestionVo } from "../dataObjects/QuestionVo";

export class SceneRoom extends BaseScene implements IBaseScene {
    private dfloor: Phaser.GameObjects.Sprite | null = null;
    private floorFrame: Phaser.GameObjects.Sprite | null = null;
    private doors: Phaser.GameObjects.Sprite[] = [];

    private musicPlayer: MusicPlayer = MusicPlayer.getInstance();

    private buttonText: string[] = ["A", "B", "C", "D"];

    private doorOffIndex: number = 0;
    private canvasWidth: number = 0;
    private canvasHeight: number = 0;

    private knightSpeed: number = .09;

    private doorPositions: any[] = [];
    private textPositions: any[] = [];
    private itemPositions: any[] = [];


    private walkIndex: number = 0;

    private knight: Phaser.GameObjects.Sprite | null = null;
    private items: Phaser.GameObjects.Sprite[] = [];
    private question: QuestionVo | null = null;
    private model: DungeonModel = DungeonModel.getInstance();

    constructor() {
        super("SceneRoom");
        this.doorPositions = [{ y: 1, x: 5.5 }, { y: 5, x: 10 }, { y: 9, x: 5.5 }, { y: 5, x: 1 }];
        this.textPositions = [{ y: 2, x: 4.5 }, { y: 6, x: 9 }, { y: 8, x: 4.5 }, { y: 6, x: 0.5 }];
        this.itemPositions = [{ y: 2, x: 2.5 }, { y: 2, x: 8 }, { y: 8, x: 2.5 }, { y: 8, x: 9 }];

        //get canvas size
        let canvas = document.getElementById("droom");
        this.setSize();
    }
    setSize() {
        let canvas = document.getElementById("droom");
        if (canvas) {
            let w = canvas.clientWidth;
            let h = canvas.clientHeight;
            // //console.log("w: " + w + " h: " + h);
            this.resetSize(w, h);
            this.canvasHeight = h;
            this.canvasWidth = w;

            /*  this.game.config.width = w;
             this.game.config.height = h; */
        }
    }
    preload() {
        /*  this.load.image("floorFrame", "./assets/floors/floorFrame.png");
         this.load.image("floor1", "./assets/floors/floor1.png");
         this.load.atlas("knight", "./assets/sprites/light.png", "./assets/sprites/light.json");
         this.load.atlas("items", "./assets/sprites/items.png", "./assets/sprites/items.json");
         this.load.atlas("doors", "./assets/sprites/doors.png", "./assets/sprites/doors.json"); */
    }
    create(): void {
        super.create();

        this.musicPlayer.setMusic("roomMusic");

        (window as any).scene = this;
        this.setSize();
        this.makeGrid(11, 11);
        //  this.grid.showNumbers();
        this.makeTheFloor();
        this.makeDoors();
        this.placeDoors();

        this.setDoorOff(2);
        window.addEventListener("resize", () => { this.onResize(); });


        this.makeAnimations();
        this.knight = this.placeImage2("knight", 5, 9, 0.2);
        this.knight.play("idle");
        for (let i = 0; i < 2; i++) {
            this.items[i] = this.placeImage2("items", 5, 2, 0.15);
        }
        if (this.model.buttonsSet == false) {
            this.setButtonListeners();
            this.model.buttonsSet = true;
        }

        this.resetItem();
        if (this.model.battleFlag == false) {
            this.getNextQuestion();
        }
        else {
            this.model.battleFlag = false;
            this.musicPlayer.playSound("levelup");
            this.setQuestionText();
        }
        this.controlsOn();
    }
    getNextQuestion() {
        console.log("get next question");
        this.question = this.model.getRoomQuestion();
        console.log(this.question);
        this.setQuestionText();

    }
    setQuestionText() {
        if (this.question) {
            const questionDiv = document.querySelector(".questionText") as HTMLElement;
            questionDiv.innerHTML = this.question.question;
            this.setAnswers();
        }

    }
    makeAnimations() {
        //make animations
        let frames = this.anims.generateFrameNames("knight", { prefix: "Walking_", start: 0, end: 17, zeroPad: 3, suffix: ".png" });
        //  //console.log(frames);
        this.anims.create({ key: "walk", frames: frames, frameRate: 12, repeat: -1 });

        //idle
        frames = this.anims.generateFrameNames("knight", { prefix: "Idle_", start: 0, end: 11, zeroPad: 3, suffix: ".png" });
        //  //console.log(frames);
        this.anims.create({ key: "idle", frames: frames, frameRate: 12, repeat: -1 });
        //attack
        frames = this.anims.generateFrameNames("knight", { prefix: "Attacking_", start: 0, end: 11, zeroPad: 3, suffix: ".png" });
        //   //console.log(frames);
        this.anims.create({ key: "attack", frames: frames, frameRate: 12, repeat: -1 });

        //die
        frames = this.anims.generateFrameNames("knight", { prefix: "Dying_", start: 0, end: 14, zeroPad: 3, suffix: ".png" });
        //   //console.log(frames);
        this.anims.create({ key: "die", frames: frames, frameRate: 12, repeat:0 });

        //hurt
        frames = this.anims.generateFrameNames("knight", { prefix: "Hurt_", start: 0, end: 11, zeroPad: 3, suffix: ".png" });
        this.anims.create({ key: "hurt", frames: frames, frameRate: 12, repeat: 0 });
    }
    //set button listeners
    setButtonListeners() {
        //set button listeners
        for (let i = 0; i < 4; i++) {
            const button: HTMLButtonElement = document.querySelector("#doorText" + i) as HTMLButtonElement;
            button.addEventListener("click", () => {
                this.walkToDoor(i);
            });
        }
    }
    removeButtonListeners() {
        //remove button listeners
        for (let i = 0; i < 4; i++) {
            const button: HTMLButtonElement = document.querySelector("#doorText" + i) as HTMLButtonElement;
            button.removeEventListener("click", () => {
                this.walkToDoor(i);
            });
        }
    }
    onResize() {
        // alert("resize");
        this.setSize();
        this.grid.hide();

        if (this.dfloor) {
            //if the height is greater than the width
            if (this.canvasHeight > this.canvasWidth) {
                Align.scaleToGameH(this.dfloor, 1, this);
            }
            else {
                Align.scaleToGameW(this.dfloor, 1, this);
            }

        }
        if (this.floorFrame) {
            if (this.canvasHeight > this.canvasWidth) {
                Align.scaleToGameH(this.floorFrame, 1, this);

            }
            else {
                Align.scaleToGameW(this.floorFrame, 1, this);
            }

        }

        this.placeDoors();
    }
    makeTheFloor() {
        this.setSize();
        // let d:number=Math.floor(Math.random()*8)+1;
        this.dfloor = this.placeImage("floor1", 60, 1);
        this.floorFrame = this.placeImage("floorFrame", 60, 1);
    }
    makeDoors() {
        this.doors = [];
        this.setSize();
        for (let i = 0; i < 4; i++) {
            //pick a random door
            let d: number = Math.floor(Math.random() * 8) + 1;
            let door: Phaser.GameObjects.Sprite = this.placeImage2("doors", this.doorPositions[i].x, this.doorPositions[i].y, .025);
            door.setFrame("door" + d + ".png");
            door.setInteractive();
            door.on("pointerdown", () => { }, this);
            this.doors.push(door);
        }
    }
    placeDoors() {
        this.setSize();
        for (let i = 0; i < this.doors.length; i++) {
            const door: Phaser.GameObjects.Sprite = this.doors[i];
           
            if (this.model.battleFlag == false) {
                let d: number = Math.floor(Math.random() * 8) + 1;
                door.setFrame("door" + d + ".png");
            }
            Align.scaleToGameW(door, .1, this);
            this.grid.placeAt2(this.doorPositions[i].x, this.doorPositions[i].y, door);
        }
    }
    setDoorOff(dir: number) {
        for (let i: number = 0; i < 4; i++) {
            this.doors[i].visible = true;
        }
        this.doors[dir].visible = false;
        this.doorOffIndex = dir;
        //doorText1 
        for (let i: number = 0; i < 4; i++) {
            let doorText: HTMLElement | null = document.querySelector("#doorText" + i);
            if (doorText) {
                doorText.style.visibility = "visible";
            }

        }
        const doorText2: HTMLElement | null = document.querySelector("#doorText" + dir);
        if (doorText2) {
            doorText2.style.visibility = "hidden";
        }

    }
    controlsOn() {
        const controlHtml = document.querySelector(".answerButtons");
        if (controlHtml) {
            controlHtml.classList.remove("hidden");
        }
        const battleControls = document.querySelector(".battleControls");
        if (battleControls) {
            battleControls.classList.add("hidden");
        }
        const battleButtons = document.querySelector(".battleButtons");
        if (battleButtons) {
            battleButtons.classList.add("hidden");
        }
    }
    buttonsOff() {
        for (let i: number = 0; i < 4; i++) {
            let doorText: HTMLElement | null = document.querySelector("#doorText" + i);
            if (doorText) {
                doorText.style.visibility = "hidden";
            }
        }
    }
    buttonsOn() {
        for (let i: number = 0; i < 4; i++) {
            let doorText: HTMLElement | null = document.querySelector("#doorText" + i);
            if (doorText) {
                doorText.style.visibility = "visible";
            }
        }
    }

    setAnswers() {
        if (this.question) {


            let answers: string[] = this.question.wrongChoices;
            answers = this.model.shuffleArray(answers);
            //limit to 2
            answers = answers.slice(0, 2);
            //add the correct answer
            answers.push(this.question.answer);
            //shuffle the array
            answers = this.model.shuffleArray(answers);
            //set the answer index


            for (let i = 0; i < 4; i++) {
                const button: HTMLButtonElement = document.querySelector("#answerButton" + i) as HTMLButtonElement;
                if (i != this.doorOffIndex) {
                    let answer: string = answers.shift() as string;
                    this.buttonText[i] = answer;
                    button.innerText = answer;
                }
            }
        }
    }
    walkToDoor(doorIndex: number) {
        console.log("walk to door " + doorIndex);
        this.buttonsOff();
        this.walkIndex = doorIndex;
        if (this.knight) {
            this.knight.play("walk");
            //get door position
            let door: Phaser.GameObjects.Sprite = this.doors[doorIndex];
            let doorPos: Phaser.Math.Vector2 = door.getCenter();
            //get knight position
            let knightPos: Phaser.Math.Vector2 = this.knight.getCenter();
            //get distance
            let distance: number = Phaser.Math.Distance.BetweenPoints(doorPos, knightPos);
            //get time
            let time: number = distance / this.knightSpeed;
            //turn x to face door
            if (doorPos.x < knightPos.x) {
                this.knight.flipX = true;
            }
            else {
                this.knight.flipX = false;
            }
            //move knight
            this.tweens.add({
                targets: this.knight, duration: time, x: doorPos.x, y: doorPos.y, onComplete: () => {
                    if (this.knight) {
                        this.knight.play("idle");
                    }
                    this.cameras.main.fade(300, 0, 0, 0);
                    //check answer
                    this.checkAnswer();

                }
            });
        }
    }
    checkAnswer() {
        console.log(this.buttonText[this.walkIndex]);
        this.playSound("door");
        if (this.question) {
            if (this.buttonText[this.walkIndex] == this.question.answer) {
                console.log("correct");
                this.model.questionsRight++;
                if (this.model.questionsRight == 3) {
                    this.musicPlayer.stopMusic();
                    this.scene.start("SceneWin");
                    return;
                }
                this.resetDoors();
                this.resetItem();
                this.getNextQuestion();
                setTimeout(() => {
                    this.cameras.main.fadeIn(200, 255, 255, 255);
                }, 500);

            }
            else {
                console.log("wrong");
                this.removeButtonListeners();
               
                setTimeout(() => {
                    this.model.battleFlag = true;
                    this.scene.start("SceneBattle");
                    this.cameras.main.fadeIn(200, 255, 255, 255);
                }, 500);
            }
        }
    }
    resetDoors() {

        let oposites: number[] = [2, 3, 0, 1];
        this.setDoorOff(oposites[this.walkIndex]);
        this.grid.placeAt(this.doorPositions[oposites[this.walkIndex]].x, this.doorPositions[oposites[this.walkIndex]].y, this.knight as IGameObj);
    }
    resetItem() {
        let uniqueIndexs = this.pickUnique(this.itemPositions);
        //console.log(uniqueIndexs);
        for (let i = 0; i < 2; i++) {

            //get the position
            let itemPos = this.itemPositions[uniqueIndexs[i]];
            //pick 2 unique indexs from the array
            //console.log(itemPos);
            //place the item
            this.grid.placeAt(uniqueIndexs[i].x, uniqueIndexs[i].y, this.items[i] as IGameObj);
            //set a random frame
            let item: number = Math.floor(Math.random() * 8) + 1;

            this.items[i].setFrame("Asset " + item + ".png");
        }

    }
    pickUnique(array: any[]) {
        //shuffle the array
        array.sort(() => Math.random() - 0.5);
        //return the first 2 items
        return [array[0], array[1]];
    }

}