import { Clock } from "phaser-comps";
import { Align, BaseScene, IBaseScene } from "phaser-utility";
import { DungeonModel } from "../classes/dungeonModel";
import { MusicPlayer } from "../classes/musicPlayer";
import { MonsterVo } from "../dataObjects/MonsterVo";
import { QuestionVo } from "../dataObjects/QuestionVo";

export class SceneBattle extends BaseScene implements IBaseScene {
    private knight: Phaser.GameObjects.Sprite | null = null;
    private monster: Phaser.GameObjects.Sprite | null = null;
    private canvasWidth: number = 0;
    private canvasHeight: number = 0;
    private timerText: Phaser.GameObjects.Text | null = null;
    private healthText: Phaser.GameObjects.Text | null = null;
    private monsterHealthText: Phaser.GameObjects.Text | null = null;

    private monsterKey: string = "dragon";
    private walkKey: string = "walk";
    private attackKey: string = "attack";
    private idleKey: string = "idle";
    private deathKey: string = "death";
    private hurtKey: string = "hurt";

    private seconds: number = 5;

    private question: QuestionVo | null = null;
    private model: DungeonModel = DungeonModel.getInstance();
    private timerFunction: Function = this.doTick.bind(this);
    private intervalID: number = 0;

    private knightHealth: number = 3;
    private monsterHealth: number = 3;

    private musicPlayer: MusicPlayer = MusicPlayer.getInstance();

    constructor() {
        super("SceneBattle");
        this.setBattleButtonListeners();
    }
    preload() {

      /*   this.load.atlas("skeleton", "./assets/sprites/skeleton.png", "./assets/sprites/skeleton.json");
        //dragon
        this.load.atlas("dragon", "./assets/sprites/dragon.png", "./assets/sprites/dragon.json");
        //load backgrounds
        for (let i = 1; i < 5; i++) {
            this.load.image("bg" + i, "./assets/battlebg/" + i + ".png");
        } */
    }
    create() {
        super.create();
        this.setSize();
    
        this.musicPlayer.setMusic("battleMusic");
        this.monsterHealth = 3;
        this.knightHealth = 3;
        this.seconds = 3;

        this.turnOffControls();
        this.clearQuestionText();

        this.makeGrid(11, 11);
        this.grid.showNumbers();

        //get random monster
        const monsters: MonsterVo[] = [new MonsterVo("dragon", 9, 7, .8), new MonsterVo("skeleton", 9, 8, .7)];
       
        const monsterVo: MonsterVo = monsters[Math.floor(Math.random() * monsters.length)];
        this.monsterKey = monsterVo.key;
      
        //set animation keys
        this.walkKey = this.monsterKey + "-walk";
        this.attackKey = this.monsterKey + "-attack";
        this.idleKey = this.monsterKey + "-idle";
        this.deathKey = this.monsterKey + "-death";
        this.hurtKey = this.monsterKey + "-hurt";

        if (this.monsterKey == "dragon") {
            this.deathKey = this.hurtKey;
        }

        //make background
        const randombg = Math.floor(Math.random() * 7) + 1;
        const bg = this.add.image(0, 0, "bg" + randombg);
        bg.setOrigin(0, 0);
        Align.scaleToGameW(bg, 1, this);

        //make timer
        this.timerText = this.add.text(0, 0, "5", { fontSize: "64px", color: "#ff0000" });
        this.grid.placeAt2(5, 2, this.timerText);

        //set health text
        this.healthText = this.add.text(0, 0, this.knightHealth.toString(), {fontFamily:"Arial",fontStyle:"bold", fontSize: "32px", color: "#ff0000",backgroundColor:"#000000" });
        this.grid.placeAt2(1, 2, this.healthText);

        //set monster health text
        this.monsterHealthText = this.add.text(0, 0,  this.monsterHealth.toString(), {fontFamily:"Arial",fontStyle:"bold", fontSize: "32px", color: "#ff0000",backgroundColor:"#000000" });
        this.grid.placeAt2(9, 2, this.monsterHealthText);

        //add sprites        
        //   console.log("monsterKey: " + this.monsterKey);
        this.monster = this.placeImage2(this.monsterKey, monsterVo.x, monsterVo.y, monsterVo.scale);
        this.knight = this.placeImage2("knight", 1, 8, .35);
        this.monster.flipX = true;

        //for debugging
        (window as any).battleScene = this;

        this.makeAnimations();

        //play animations
        this.monster.play(this.idleKey);
        this.knight.play("idle");

        let startSound:string=this.monsterKey+"_start";
        this.musicPlayer.playSound(startSound);

        this.startTimer();
    }
    startTimer() {
        this.stopTimer();
        this.seconds = 5;
        this.intervalID = setInterval(this.timerFunction, 1000);
    }
    stopTimer() {
        clearInterval(this.intervalID);
    }
    clearQuestionText() {
        const questionText = document.querySelector(".questionText");
        if (questionText) {
            questionText.innerHTML = this.model.battleQuestions.instructions;
        }
    }
    hideQuestionText() {
        const questionText = document.querySelector(".questionText");
        if (questionText) {
            questionText.innerHTML = "";
        }
    }
    getNextQuestion() {
        this.resetTimer();
        this.question = this.model.getBattleQuestion();
        const questionText = document.querySelector(".questionText");
        if (questionText) {
            questionText.innerHTML = this.question.question;
        }
        this.setAnswers();
        this.turnOnBattleButtons();
    }
    setAnswers() {
        //get all children of battlebuttons
        const battleButtons = document.querySelector(".battleButtons");
        if (battleButtons && this.question) {

            let answers: string[] = this.question.wrongChoices;
            answers = this.model.shuffleArray(answers);
            //limit to 2
            answers = answers.slice(0, 3);
            //add the correct answer
            answers.push(this.question.answer);
            //shuffle the array
            answers = this.model.shuffleArray(answers);

            const buttons = battleButtons.children;
            for (let i = 0; i < buttons.length; i++) {
                const button: HTMLButtonElement = buttons[i] as HTMLButtonElement;
                button.innerHTML = answers[i];
                button.classList.remove("hidden");
            }
        }
    }
    setBattleButtonListeners() {
        this.removeBattleButtonListeners();
        const battleButtons = document.querySelector(".battleButtons");
        if (battleButtons) {
            const buttons = battleButtons.children;
            for (let i = 0; i < buttons.length; i++) {
                const button: HTMLButtonElement = buttons[i] as HTMLButtonElement;
                button.addEventListener("click", () => { this.checkAnswer(i) });
            }
        }
    }
    removeBattleButtonListeners() {
        const battleButtons = document.querySelector(".battleButtons");
        if (battleButtons) {
            const buttons = battleButtons.children;
            for (let i = 0; i < buttons.length; i++) {
                const button: HTMLButtonElement = buttons[i] as HTMLButtonElement;
                button.removeEventListener("click", () => { this.checkAnswer(i) });
            }
        }
    }
    checkAnswer(index: number) {
        console.log("checkAnswer");
        this.stopTimer();
        const battleButtons = document.querySelector(".battleButtons");
        this.turnOffBattleButtons();

        if (battleButtons && this.question) {
            const buttons = battleButtons.children;
            const button: HTMLButtonElement = buttons[index] as HTMLButtonElement;
            let selectedAnswer = button.innerText;
           
            if (button.innerText == this.question.answer) {
                console.log("correct");
                this.knightAttack();
                
            }
            else {
                console.log("wrong");
                this.skeletonAttack();
            }
        }
    }
    doTick() {
        this.seconds--;
        console.log("doTick");

        if (this.timerText) {
            this.timerText.setText(this.seconds.toString());
        }
        if (this.seconds == 0) {
            if (this.question == null) {
                this.stopTimer();
                this.getNextQuestion();
                this.turnOnBattleButtons();
            }
            else {
                this.stopTimer();
                this.turnOffBattleButtons();
                this.skeletonAttack();
            }
        }
    }
    resetTimer() {
        this.seconds = 5;
        if (this.timerText) {
            this.timerText.setText(this.seconds.toString());
        }
       this.startTimer();
    }
    turnOffControls() {
        const controlHtml = document.querySelector(".answerButtons");
        if (controlHtml) {
            controlHtml.classList.add("hidden");
        }
        const battleControls = document.querySelector(".battleControls");
        if (battleControls) {
            battleControls.classList.remove("hidden");
        }
        const battleButtons = document.querySelector(".battleButtons");
        if (battleButtons) {
            battleButtons.classList.add("hidden");
        }
    }
    turnOnBattleButtons() {
        const battleButtons = document.querySelector(".battleButtons");
        if (battleButtons) {
            battleButtons.classList.remove("hidden");
        }
    }
    turnOffBattleButtons() {
        const battleButtons = document.querySelector(".battleButtons");
        if (battleButtons) {
            battleButtons.classList.add("hidden");
        }
    }
    setSize() {
        let canvas = document.getElementById("droom");
        if (canvas) {
            let w = canvas.clientWidth;
            let h = canvas.clientHeight;
            console.log("w: " + w + " h: " + h);
            this.resetSize(w, h);
            this.canvasHeight = h;
            this.canvasWidth = w;

            /*  this.game.config.width = w;
             this.game.config.height = h; */
        }
    }
    makeAnimations() {
        //Skeleton_01_1_S_IDLE_000.png
        let idleFrames = this.anims.generateFrameNames("skeleton", { start: 0, end: 9, zeroPad: 3, prefix: "Skeleton_01_1_S_IDLE_", suffix: ".png" });
        this.anims.create({ key: "skeleton-idle", frames: idleFrames, frameRate: 12, repeat: -1 });

        let walkFrames = this.anims.generateFrameNames("skeleton", { start: 0, end: 9, zeroPad: 3, prefix: "Skeleton_01_1_S_RUN_", suffix: ".png" });
        this.anims.create({ key: "skeleton-walk", frames: walkFrames, frameRate: 12, repeat: -1 });

        let attackFrames = this.anims.generateFrameNames("skeleton", { start: 0, end: 9, zeroPad: 3, prefix: "Skeleton_01_1_S_ATTAK_", suffix: ".png" });
        this.anims.create({ key: "skeleton-attack", frames: attackFrames, frameRate: 12, repeat: 0 });

        let deathFrames = this.anims.generateFrameNames("skeleton", { start: 0, end: 9, zeroPad: 3, prefix: "Skeleton_01_1_S_DIE_", suffix: ".png" });
        this.anims.create({ key: "skeleton-death", frames: deathFrames, frameRate: 12, repeat: 0 });

        let hurtFrames = this.anims.generateFrameNames("skeleton", { start: 0, end: 9, zeroPad: 3, prefix: "Skeleton_01_1_S_HURT_", suffix: ".png" });
        this.anims.create({ key: "skeleton-hurt", frames: hurtFrames, frameRate: 12, repeat: 0 });

        //dragon
        // Dragon_01_1_dragon_idle_000.png
        let dragonIdleFrames = this.anims.generateFrameNames("dragon", { start: 0, end: 9, zeroPad: 3, prefix: "Dragon_01_1_dragon_idle_", suffix: ".png" });
        this.anims.create({ key: "dragon-idle", frames: dragonIdleFrames, frameRate: 12, repeat: -1 });

        let dragonWalkFrames = this.anims.generateFrameNames("dragon", { start: 0, end: 9, zeroPad: 3, prefix: "Dragon_01_1_dragon_run_", suffix: ".png" });
        this.anims.create({ key: "dragon-walk", frames: dragonWalkFrames, frameRate: 12, repeat: -1 });

        let dragonAttackFrames = this.anims.generateFrameNames("dragon", { start: 0, end: 9, zeroPad: 3, prefix: "Dragon_01_1_dragon_Attack_", suffix: ".png" });
        this.anims.create({ key: "dragon-attack", frames: dragonAttackFrames, frameRate: 12, repeat: 0 });

        //    let dragonDeathFrames = this.anims.generateFrameNames("dragon", { start: 0, end: 9, zeroPad: 3, prefix: "Dragon_01_1_S_DIE_", suffix: ".png" });
        //   this.anims.create({ key: "dragon-death", frames: dragonDeathFrames, frameRate: 12, repeat: 0 });

        let dragonHurtFrames = this.anims.generateFrameNames("dragon", { start: 0, end: 9, zeroPad: 3, prefix: "Dragon_01_1_dragon_hurt_", suffix: ".png" });
        this.anims.create({ key: "dragon-hurt", frames: dragonHurtFrames, frameRate: 12, repeat: 0 });


    }
    knightAttack() {
        if (this.knight && this.monster) {

            let monsterHitSound:string=this.monsterKey+"_hit";
            this.musicPlayer.playSound(monsterHitSound);

            this.monster.setDepth(1);
            this.knight.setDepth(2);
            
            this.monster.play(this.attackKey);
            this.knight.play("attack");
            const orginalX = this.knight.x;
            //tween the knight to the skeleton and then back to the original position
            let tween = this.tweens.add({
                targets: this.knight,
                x: this.monster.x,
                ease: 'Power1',
                duration: 500,
                yoyo: false,
                repeat: 0,
                onComplete: () => {
                    
                    if (this.monster) {
                       
                        this.monster.play(this.hurtKey);
                        this.monster.once("animationcomplete", () => {
                            //tween the knight back to the original position
                            let tween = this.tweens.add({
                                targets: this.knight,
                                x: orginalX,
                                ease: 'Power1',
                                duration: 500,
                                yoyo: false,
                                repeat: 0,
                                onComplete: () => {
                                    if (this.knight) {
                                        this.knight.play("idle");
                                    }
                                }
                            });
                            this.monsterHealth--;
                            if (this.monster) {
                                if (this.monsterHealthText)
                                {
                                    this.monsterHealthText.setText(this.monsterHealth.toString());
                                }
                                 
                                if (this.monsterHealth <= 0) {
                                    this.monster.play(this.deathKey);
                                    let monsterDeathSound:string=this.monsterKey+"_die";
                                    this.musicPlayer.playSound(monsterDeathSound);
                                    this.monster.once("animationcomplete", () => {
                                        this.question=null;
                                        
                                        this.removeBattleButtonListeners();
                                        this.cameras.main.fade(300, 0, 0, 0);
                                        setTimeout(() => {
                                            
                                            this.scene.start("SceneRoom");
                                            this.cameras.main.fadeIn(200,255,255,255);
                                        }, 500);
                                       
                                    });
                                }
                                else {
                                   
                                    this.monster.play(this.idleKey);
                                    this.getNextQuestion();
                                }
                            }

                        });
                    }
                }
            });
        }
    }
    skeletonAttack() {
        if (this.knight && this.monster) {
            this.monster.setDepth(2);
            this.knight.setDepth(1);
            this.knight.play("attack");
            this.monster.play(this.attackKey);

            const orginalX = this.monster.x;
            //tween the skeleton to the knight and then back to the original position
            let tween = this.tweens.add({
                targets: this.monster,
                x: this.knight.x,
                ease: 'Power1',
                duration: 500,
                yoyo: false,
                repeat: 0,
                onComplete: () => {
                    this.musicPlayer.playSound("hit");
                    if (this.knight) {
                        this.knight.play("hurt");
                        this.musicPlayer.playSound("knight_hit");
                        this.knight.once("animationcomplete", () => {
                            //tween the skeleton back to the original position
                            let tween = this.tweens.add({
                                targets: this.monster,
                                x: orginalX,
                                ease: 'Power1',
                                duration: 500,
                                yoyo: false,
                                repeat: 0,
                                onComplete: () => {
                                    if (this.monster) {
                                        this.monster.play(this.idleKey);
                                    }
                                }
                            });
                            this.knightHealth--;
                            if (this.healthText)
                            {
                                this.healthText.setText(this.knightHealth.toString());
                            }
                            
                            if (this.knight) {
                                if (this.knightHealth <= 0) {
                                    this.musicPlayer.playSound("knight_die");
                                    this.knight.play("die");
                                    this.knight.once("animationcomplete", () => {
                                        this.question=null;
                                        this.removeBattleButtonListeners();
                                        this.cameras.main.fade(300, 0, 0, 0);
                                        this.turnOffBattleButtons();
                                        this.hideQuestionText();
                                        setTimeout(() => {
                                            this.musicPlayer.stopMusic();
                                            this.scene.start("SceneOver");
                                            this.cameras.main.fadeIn(200,255,255,255);
                                        }, 500);
                                    } );
                                }
                                else {
                                    this.knight.play("idle");
                                    this.getNextQuestion();
                                }
                            }

                        });
                    }
                }
            });
        }
    }
}
