import { BaseScene } from "phaser-utility";

export class SceneTitle extends BaseScene
{
    constructor()
    {
        super("SceneTitle");
    }
    create(): void {
        super.create();
        this.setSize();
        this.makeGrid(11,11);

        let bg:Phaser.GameObjects.Image=this.placeImage2("title",5,5,1);
        bg.setOrigin(0.5,0.5);
        bg.setInteractive();
        bg.once("pointerdown",this.startGame,this);
    }
    startGame()
    {
        this.scene.start("SceneRoom");
    }
    setSize() {
        let canvas = document.getElementById("droom");
        if (canvas) {
            let w = canvas.clientWidth;
            let h = canvas.clientHeight;
            console.log("w: " + w + " h: " + h);
            this.resetSize(w, h);
          

            /*  this.game.config.width = w;
             this.game.config.height = h; */
        }
    }
}