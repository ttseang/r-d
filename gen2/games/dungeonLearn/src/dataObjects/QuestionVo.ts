export class QuestionVo
{
    public question:string;
    public answer:string;
    public wrongChoices:string[];
    public index:number=0;
    constructor(question:string, answer:string, wrongChoices:string[])
    {
        this.question = question;
        this.answer = answer;
        this.wrongChoices = wrongChoices;
    }
}