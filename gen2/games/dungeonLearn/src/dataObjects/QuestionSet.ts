import { QuestionVo } from "./QuestionVo";

export class QuestionSet
{
    public questions: QuestionVo[] = [];
    public instructions: string = "";
    public title: string = "";
    private questionIndex: number = -1;

    public getQuestion(): QuestionVo
    {
        this.questionIndex++;
        if (this.questionIndex>=this.questions.length)
        {
            this.questionIndex=0;
            this.shuffleArray(this.questions);
        }
        return this.questions[this.questionIndex];
    }
    randomizeQuestions()
    {
        this.shuffleArray(this.questions);
    }
    shuffleArray(array: any[]) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }
}