export class MonsterVo
{
    public key:string;
    public x:number;
    public y:number;
    public scale:number;
    constructor(key:string,x:number,y:number,scale:number=.1)
    {
        this.key = key;
        this.x = x;
        this.y = y;
        this.scale = scale;
    }
}