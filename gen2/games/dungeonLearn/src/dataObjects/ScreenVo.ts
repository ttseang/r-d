import { IScreen } from "../interfaces/IScreen";

export class ScreenVo {
    public screenName: string;
    public screenClass: IScreen;
    constructor(screenName: string, screenClass: IScreen) {
        this.screenName = screenName;
        this.screenClass = screenClass;
    }
}