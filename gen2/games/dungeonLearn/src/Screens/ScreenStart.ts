import { DungeonModel } from "../classes/dungeonModel";
import { QuestionLoader } from "../classes/questionLoader";
import { ScreenManager } from "../classes/ScreenManager";
import { QuestionSet } from "../dataObjects/QuestionSet";
import { QuestionVo } from "../dataObjects/QuestionVo";
import { IScreen } from "../interfaces/IScreen";

export class ScreenStart implements IScreen {
    private roomQuestions: string = "romeo_juliet";
    private battleQuestions: string = "latin_phrases";
    private sm:ScreenManager;
    private dungeonModel:DungeonModel=DungeonModel.getInstance();
    
    constructor(sm:ScreenManager) {
        this.sm = sm;
    }
    start() {
        this.loadRoomQuestions();
    }

    loadRoomQuestions() {
        const questionLoader: QuestionLoader = new QuestionLoader(this.loadBattleQuestions.bind(this));
        questionLoader.loadFile(this.roomQuestions);
    }
    loadBattleQuestions(roomQuestionData:QuestionSet) {
        //console.log(roomQuestionData);
        this.dungeonModel.roomQuestions = roomQuestionData;
        const questionLoader: QuestionLoader = new QuestionLoader(this.questionLoaded.bind(this));
        questionLoader.loadFile(this.battleQuestions);

    }
    questionLoaded(battleQuestionData:QuestionSet) {
        //console.log(battleQuestionData);
        this.dungeonModel.battleQuestions = battleQuestionData;
        this.sm.changeScreen("game");

    }
    
    destroy() {
        //turn off control html elements

        const canvas: HTMLElement | null = document.querySelector(".main-game");
        if (canvas) {
            canvas.style.display = "none";
        }
    }
}