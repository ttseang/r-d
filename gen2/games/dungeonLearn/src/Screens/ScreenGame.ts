import { ScreenManager } from "../classes/ScreenManager";
import { IScreen } from "../interfaces/IScreen";
import { SceneBattle } from "../scenes/SceneBattle";
import { SceneOver } from "../scenes/SceneOver";
import { ScenePreload } from "../scenes/ScenePreload";
import { SceneRoom } from "../scenes/SceneRoom";
import { SceneTitle } from "../scenes/SceneTitle";
import { SceneWin } from "../scenes/SceneWin";

export class ScreenGame implements IScreen {
    private sm: ScreenManager;
    constructor(sm: ScreenManager) {
        this.sm = sm;
    }

    start() {
        //turn on control html elements
        const controlDiv: HTMLElement | null = document.querySelector(".controls");
        if (controlDiv) {
            controlDiv.style.display = "block";
        }
        const config: Phaser.Types.Core.GameConfig = {
            type: Phaser.WEBGL,
            width: "100%",
            height: "100%",
            backgroundColor: 'cccccc',
            parent: 'droom',
            pixelArt: false,
            roundPixels: true,
            scene: [ScenePreload,SceneTitle,SceneRoom,SceneBattle,SceneWin,SceneOver]

        };
        new Phaser.Game(config);
    }
    destroy(): void {
        const controlDiv: HTMLElement | null = document.querySelector(".controls");
        if (controlDiv) {
            controlDiv.style.display = "none";
        }
        //hide canvas
        const canvas: HTMLElement | null = document.querySelector(".main-game");
        if (canvas) {
            canvas.style.display = "none";
        }

    }
}