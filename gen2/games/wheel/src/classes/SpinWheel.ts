import { Align, IBaseScene } from "ttphasercomps";
import { WheelConfig } from "../dataObjs/WheelConfig";
import { DWheel } from "./DWheel";
import { SoundPlayer } from "./SoundPlayer";

export class SpinWheel extends Phaser.GameObjects.Container
{
    private prizes:Phaser.GameObjects.Container;
    private slices:number=6;
    private wheelRadius:number;
    private bscene:IBaseScene;
    private currentPercent: number=0;
    private callback:Function;

    constructor(bscene:IBaseScene,config:WheelConfig,callback:Function)
    {
        super(bscene.getScene());
        this.bscene=bscene;
        this.prizes=this.scene.add.container();
        
        this.slices=config.slices;
        
        this.wheelRadius=bscene.getW()*.22;
        this.callback=callback;

        this.angle=180;
       
     
        let wheel = new DWheel(bscene, config);

        let rim = this.scene.add.image(0, 0, 'rim');
        Align.scaleToGameW(rim, .45, bscene);
        //
        //wheel center
        //
        let wheelCenter = this.scene.add.image(0, 0, "center");
        Align.scaleToGameW(wheelCenter, .15, bscene);

        this.addPrizes();

        this.add(wheel);
        this.add(rim);
        this.add(wheelCenter);
        this.add(this.prizes);

        this.scene.add.existing(this);

        this.setSize(rim.displayWidth,rim.displayHeight);
    }
    addPrizes()
    {
        let deg2 = 360 / this.slices;
        let startDegrees = 0;
        for (var i = 0; i < this.slices; i++) {
            let i2 = i + 1;
            let key = "prize" + i2;
            //
            //
            //
            var icon = this.scene.add.image(this.wheelRadius * 0.6 * Math.cos(Phaser.Math.DegToRad(startDegrees + deg2 / 2)), this.wheelRadius * 0.6 * Math.sin(Phaser.Math.DegToRad(startDegrees + deg2 / 2)), key);
            Align.scaleToGameW(icon, .09, this.bscene);
            icon.angle = startDegrees + deg2 / 2 + 90;
            this.prizes.add(icon);
            
            startDegrees += deg2;
            // angle+=deg;
        }
    }
    spinMe()
    {
        const rounds:number = Phaser.Math.Between(5, 10);
     
        const backDegrees:number = Phaser.Math.Between(15, 90);
        
        this.currentPercent = 50;
        let prizeToGrant:number = Phaser.Math.Between(0, this.slices);
     
        let finalPos:number = this.getPos(prizeToGrant);
        let targetAngle:number = (360 * rounds) + finalPos + backDegrees;
        let duration:number = Phaser.Math.Between(4, 8) * 1000;
        //
        //
        //
        this.scene.tweens.add({
            // adding the wheel container to tween targets
            targets: [this],
            // angle destination
            angle: targetAngle,
            // tween duration
            duration: duration,
            // tween easing
            ease: "Cubic.easeOut",
            // callback scope
            callbackScope: this,
            // function to be executed once the tween has been completed
            onComplete: function (tween) {
                // another tween to rotate a bit in the opposite direction
                // 
                this.scene.tweens.add({
                    targets: [this],
                    angle: this.angle - backDegrees,
                    duration: Phaser.Math.Between(500, 2000),
                    ease: "Cubic.easeIn",
                    callbackScope: this,
                    onComplete:this.spinDone.bind(this)
                })
            }
        });        
    }
    spinDone()
    {                             
        SoundPlayer.playSound(this.bscene,"cash_register");
        this.callback();
    }
    getPos(pos: number) {
        let deg2 = 360 / this.slices;
        let angle = 360 - (pos * +deg2);
        ////console.log(angle);
        if (this.slices == 4) {
            angle -= deg2 / 2;
        }
        if (this.slices == 3) {
            angle -= 90;
        }
        if (this.slices == 5) {
            angle -= 18;
        }
        if (this.slices == 7) {
            angle += 13;
        }
        if (this.slices == 8) {
            angle += 22;
        }
        return angle;
    }
}