import { IBaseScene } from "ttphasercomps";
import { WheelConfig } from "../dataObjs/WheelConfig";

export class DWheel extends Phaser.GameObjects.Container {

    private graphics:Phaser.GameObjects.Graphics;
    private slices:number=6;
    private config:WheelConfig;
    private colors:number[];
    private deg:number=0;

    constructor(bscene:IBaseScene,config:WheelConfig) {
        super(bscene.getScene());
      
        this.slices = config.slices;
        this.config=config;
        this.graphics=this.scene.add.graphics();
        this.add(this.graphics);
        //
        //
        //
        //this.colors = [0xfadd51, 0x9bd454, 0x46bf6e, 0x5dc6d8, 0x527bda, 0x7a48ca, 0x5207c8, 0xbc44c8];
        this.colors=config.colors;
        
        this.makeSlices();
        this.scene.add.existing(this);
         //window.wheel=this;
    }
    makeTexture()
    {
        this.graphics.generateTexture("wheel",400,400);
       
        //this.graphics.destroy();
    }
    makeSlices() {
        this.deg = 360 / this.slices;
       // console.log(this.deg);
        let start = 0;
        for (var i = 0; i < this.slices; i++) {         
           // console.log(this.colors[i]);
            let color=this.colors[i];
            this.makeSlice(color, start, start + this.deg);
            start += this.deg;
        }
        
    }
    makeSlice(color:number, start:number, end:number) {
     //   console.log(start+"-"+end);
       // console.log(color);

        this.graphics.fillStyle(color, 1);
        this.graphics.slice(0, 0, this.config.width, Phaser.Math.DegToRad(start), Phaser.Math.DegToRad(end), false);
        this.graphics.fillPath();
    }
}