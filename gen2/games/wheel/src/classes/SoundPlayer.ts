import { IBaseScene } from "ttphasercomps";

export class SoundPlayer
{
    public static playSound(bscene:IBaseScene,key:string)
    {
        let sound:Phaser.Sound.BaseSound=bscene.getScene().sound.add(key);
        sound.play();
    }
}