export class WheelConfig
{
    public slices:number;
    public colors:number[];
    public width:number;

    constructor(slices:number,colors:number[],width:number)
    {
        this.slices=slices;
        this.colors=colors;
        this.width=width;
    }
}