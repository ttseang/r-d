export class SceneLoad extends Phaser.Scene
{
    constructor()
    {
        super("SceneLoad");
    }
    preload()
    {
        let pngArray = ['panelBack', 'title', 'rim', 'center','wheel_pin'];
        for (let i = 0; i < pngArray.length; i++) {
            this.loadPng(pngArray[i], "./assets/images/");
        }

        for (var i = 1; i < 7; i++) {
            let key = "prize" + i;
            let path = "./assets/images/prizes/" + i + ".png";
            this.load.image(key, path);
        }

        let wavArray = ['wheel_spin','click','cash_register'];
        for (let i = 0; i < wavArray.length; i++) {
            this.loadWav(wavArray[i], "./assets/audio/");
        }
    }
    loadPng(key, mainPath = "") {
        
        this.load.image(key, mainPath + key + ".png");
    }
    loadWav(key, mainPath = "") {
        this.load.audio(key, mainPath + key + ".wav");
    }
    create()
    {
        this.scene.start("SceneMain");
    }
}