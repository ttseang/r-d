
import { CompLayout, CompManager, ButtonController, CompLoader, IComp, TextComp } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { DWheel } from "../classes/DWheel";
import { GM } from "../classes/GM";
import { SoundPlayer } from "../classes/SoundPlayer";
import { SpinWheel } from "../classes/SpinWheel";
import { WheelConfig } from "../dataObjs/WheelConfig";

import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;

    private gm: GM = GM.getInstance();

    private wheelRadius: number = .3;
    private pinSize:number=.05;

    private canSpin: boolean = true;
 

    private spinWheel:SpinWheel | null =null;
   
    private wheelPin:Phaser.GameObjects.Image |null=null;

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.cm.regFontSize("list", 30, 1200);
        this.cm.regFontSize("words", 40, 1200);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        this.buttonController.callback = this.doButtonAction.bind(this);

        (window as any).scene=this;
    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);

        //helpText.textStyleVo.fontSizeKey
       // this.makeWheel();

       
      //  this.gotWheelData(null);
      this.getWheelData();
    }
    getWheelData()
    {
        fetch("./assets/wheelData.json")
        .then(response => response.json())
        .then(data => this.gotWheelData({ data }));
    }
    gotWheelData(data:any)
    {
        data=data.data.wheelData;
        
        let config:WheelConfig= new WheelConfig(data.slices, data.colors, this.getW()*.22);

       this.spinWheel=new SpinWheel(this,config,this.spinDone.bind(this));

       Align.scaleToGameW(this.spinWheel,this.wheelRadius,this);

       this.grid.placeAt(5, 4.5, this.spinWheel);
       this.wheelPin= this.placeImage2("wheel_pin", 5, 1, this.pinSize);

       this.doResize();
    }
    spinTheWheel() {
        if (this.canSpin == false) {
            return;
        }
        this.spinWheel.spinMe();
        SoundPlayer.playSound(this,"click");
        this.canSpin = false;

        SoundPlayer.playSound(this,"wheel_spin");

       
    }
    spinDone()
    {
        this.canSpin=true;
    }
    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);

        Align.scaleToGameW(this.spinWheel,this.wheelRadius,this);
        Align.scaleToGameW(this.wheelPin,this.pinSize,this);
        this.grid.placeAt(5, 4.5, this.spinWheel);
        this.grid.placeAt(5,1,this.wheelPin);

        this.wheelPin.x=this.spinWheel.x;
        this.wheelPin.y=this.spinWheel.y-this.spinWheel.displayHeight/2;

       
        this.cm.doResize();

    }
    doButtonAction(action: string, params: string) {
        console.log(action);
        if (action === "play") {
            this.spinTheWheel();
        }
    }

}