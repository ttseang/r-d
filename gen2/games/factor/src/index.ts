import "phaser";
import { SceneMain } from "./scenes/SceneMain";

window.onload=function()
{
    let config:Phaser.Types.Core.GameConfig=({width:480,height:640,scene:[SceneMain],backgroundColor:"#27ae60"});
    
    let game:Phaser.Game=new Phaser.Game(config);
}