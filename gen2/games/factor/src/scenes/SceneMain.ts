import * as Phaser from "phaser";
import { Align, BaseScene, PosVo } from "phaser-utility";
import { start } from "repl";
import { Block } from "../classes/Block";
import { Clock } from "../classes/Clock";

export class SceneMain extends BaseScene {
    private values: number[] = [];
    private firstBlock: Block | null = null;
    private clock:Clock |null=null;

    private firstSelected: Block | null = null;

    private targetText: Phaser.GameObjects.Text | null = null;
    private clickLock: boolean = false;
    public targetNumber: number = 0;

    private dog: Phaser.GameObjects.Sprite | null = null;
    private box: Phaser.GameObjects.Sprite | null = null;

    preload() {
        this.load.spritesheet("blocks", "./assets/blocks2.png", { frameWidth: 64, frameHeight: 80 });
        this.load.image("circle", "./assets/circle.png");
        this.load.atlas("dog", "./assets/dog.png", "./assets/dog.json");
        this.load.image("holder", "./assets/holder.png");

        this.load.audio("sad", "./assets/sad.mp3");
        this.load.audio("happy1", "./assets/happy1.mp3");
        this.load.audio("happy2", "./assets/happy2.mp3");
        this.load.audio("happy3", "./assets/happy3.mp3");
    }
    create() {
        super.create();
        (window as any).scene = this;
        this.makeGrid(11, 11);
        //   this.grid.show();

        this.box = this.placeImage("holder", 71, .8);
        this.grid.placeAt2(5.5, 6, this.box);
        this.box.setTint(0xE6CAA2);

        this.makeBlockGrid();

        this.makeAnim();

        let circle: Phaser.GameObjects.Sprite = this.placeImage("circle", 16, 0.25);
        circle.setTint(0x00a8ff);

        this.targetText = this.add.text(0, 0, "10", { fontFamily: "Arial", fontSize: "64px", color: "white", stroke: "black", strokeThickness: 6 });
        this.targetText.setOrigin(0.5, 0.5);
        this.grid.placeAt(5, 1, this.targetText);

        this.clock=new Clock(this,this.timeUp.bind(this));
        this.clock.setTime(120);
        this.clock.start();
        this.grid.placeAt(7,0.5,this.clock);

        this.resetNumber();

    }
    timeUp()
    {
        alert("time up");
        this.clickLock=true;
    }
    private makeAnim() {
        this.dog = this.add.sprite(0, 0, "dog");

        let dogFrames: Phaser.Types.Animations.AnimationFrame[] = this.anims.generateFrameNames("dog", { prefix: "Dog_Run_", start: 0, end: 5, zeroPad: 3, suffix: ".png" })
        let dogFramesIdle: Phaser.Types.Animations.AnimationFrame[] = this.anims.generateFrameNames("dog", { prefix: "Dog_Idle_", start: 0, end: 7, zeroPad: 3, suffix: ".png" })
        let dogFramesWin: Phaser.Types.Animations.AnimationFrame[] = this.anims.generateFrameNames("dog", { prefix: "Dog_Celebrate_", start: 0, end: 7, zeroPad: 3, suffix: ".png" })

        console.log(dogFrames);

        this.anims.create({
            key: "run",
            frames: dogFrames,
            frameRate: 48,
            repeat: -1
        })
        this.anims.create({
            key: "idle",
            frames: dogFramesIdle,
            frameRate: 12,
            repeat: -1
        })
        this.anims.create({
            key: "win",
            frames: dogFramesWin,
            frameRate: 24,
            repeat: -1
        })
        this.dog.play("idle");

        Align.scaleToGameW(this.dog, .2, this);

        this.grid.placeAtIndex(13, this.dog);
    }
    makeBlockGrid() {

        let blockColor: number = Math.floor(Math.random() * 6);
        let lastBlock: Block | null = null;

        for (let i: number = 0; i < 7; i++) {
            for (let j: number = 0; j < 6; j++) {

                let val: number = Math.floor(Math.random() * 12) + 1;

                this.values.push(val);

                let block: Block = new Block(this, val, blockColor, this.clickBlock.bind(this));
                if (this.firstBlock === null) {
                    this.firstBlock = block;
                }
                if (lastBlock) {
                    lastBlock.nextBlock = block;
                }
                lastBlock = block;

                blockColor++;
                if (blockColor > 9) {
                    blockColor = 0;
                }
                Align.scaleToGameW(block, 0.1, this);

                this.grid.placeAt(2 + i, 3 + j, block);
            }
        }
        this.mixUp();
    }
    getNumbers() {
        this.values = [];
        if (this.firstBlock) {
            this.firstBlock.addNum(this.addNum.bind(this))
        }
        this.mixUp();
        this.resetNumber();
    }
    addNum(value: number) {
        this.values.push(value);
    }
    mixUp() {
        for (let i: number = 0; i < 100; i++) {
            let r1: number = Math.floor(Math.random() * this.values.length);
            let r2: number = Math.floor(Math.random() * this.values.length);

            let temp: number = this.values[r1];
            this.values[r1] = this.values[r2];
            this.values[r2] = temp;
        }
    }
    resetDog() {
        let pos: PosVo = this.grid.getPosByIndex(13);
        console.log(pos);
        this.moveDog(pos.x, pos.y);
        setTimeout(() => {
            if (this.dog) {
                this.dog.flipX = false;
            }
        }, 1000);
    }
    moveDog(xx: number, yy: number) {
        if (this.dog) {
            this.dog.play("run");
            if (this.dog.x < xx) {
                this.dog.flipX = false;
            }
            else {
                this.dog.flipX = true;
            }
            this.tweens.add({
                targets: [this.dog], x: xx, y: yy, duration: 500, onComplete: () => {
                    this.dog?.play("idle");
                }
            })
        }

    }
    playHappySound() {
        let s: number = Math.floor(Math.random() * 3) + 1;
        let key: string = "happy" + s.toString();
        this.playSound(key);
    }
    clickBlock(block: Block) {
        if (this.clickLock === true) {
            return;
        }
        if (this.firstSelected === null) {
            this.firstSelected = block;
            this.firstSelected.select();
            //   this.moveDog(this.firstSelected.x,this.firstSelected.y);
            return;
        }
        if (block === this.firstSelected) {
            this.firstSelected.reset();
            this.firstSelected = null;
            this.resetDog();
            return;
        }
        this.clickLock = true;

        let value1: number = this.firstSelected.value;
        let value2: number = block.value;
        let answer: number = value1 * value2;

        console.log(value1, value2, answer);

        block.select();



        if (answer === this.targetNumber) {
            console.log("correct");
            this.firstSelected.visible = false;
            this.firstSelected.isOff = true;
            block.visible = false;
            block.isOff = true;
            this.firstSelected = null;
            this.getNumbers();

            this.moveDog(block.x, block.y);
            setTimeout(() => {
                this.dog?.play("win");
                this.playHappySound();
                setTimeout(() => {
                    this.clickLock = false;
                    this.resetDog();
                }, 400);
            }, 700);
        }
        else {
            setTimeout(() => {
                this.resetDog();
                this.playSound("sad");
                this.firstBlock?.resetAll();
                this.firstSelected = null;
                this.clickLock = false;
            }, 1000);

        }
    }
    resetNumber() {
        console.log(this.values);

        if (this.values.length > 1) {
            let num1: number = this.values.pop() || 0;
            let num2: number = this.values.pop() || 0;
            console.log(num1, num2);

            this.targetNumber = num1 * num2;


            if (this.targetText) {
                this.targetText.alpha = 0;
                this.targetText.setText(this.targetNumber.toString());
                this.tweens.add({ targets: [this.targetText], alpha: 1, time: 500 });
            }
        }
        else
        {
            setTimeout(() => {
                this.moveDog(this.gw/2,this.gh/2);
            }, 2000);
            this.clock?.stop();
            
        }

    }
}