import { IBaseScene } from "phaser-utility";

export class Clock extends Phaser.GameObjects.Container {
    private time: number = 80;

    private callback: Function;
    private handle: any;
    private text: Phaser.GameObjects.Text;

    constructor(scene: IBaseScene, callback: Function) {
        super(scene.getScene())
        this.callback = callback;
        this.text = this.scene.add.text(0, 0, "0:00", {fontFamily:"Arial",align:"center",fontSize:"45px",color:"white",fontStyle:"bold",stroke:"black",strokeThickness:6});
        this.add(this.text);

        this.scene.add.existing(this);
    }
    setTime(time: number) {
        this.time = time;
        this.text.setText(this.formatTime());
    }
    private formatTime() {
        let mins: number = Math.floor(this.time / 60);
        let secs: number = this.time - mins * 60;


        let minString: string = mins.toString();
        if (minString.length < 2) {
            minString = "0" + minString;
        }

        let secString: string = secs.toString();
        if (secString.length < 2) {
            secString = "0" + secString;
        }
        return minString + ":" + secString;
    }
    doTick() {
        this.time--;
        if (this.time === 0) {
            this.callback();
            this.stop();
        }
        this.text.setText(this.formatTime());
    }
    start() {
        this.handle = setInterval(this.doTick.bind(this), 1000);
    }
    stop() {
        clearInterval(this.handle);
    }
}