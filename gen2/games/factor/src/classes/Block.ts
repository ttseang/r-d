import { IBaseScene } from "phaser-utility";

export class Block extends Phaser.GameObjects.Container
{
    public back:Phaser.GameObjects.Sprite;
    private text:Phaser.GameObjects.Text;
    public value:number=0;
    public selected:boolean=false;
    public nextBlock:Block | null=null;
    public isOff:boolean=false;

    constructor(bscene:IBaseScene,value:number,backFrame:number,callback:Function)
    {
        super(bscene.getScene());
        this.value=value;

        this.back=this.scene.add.sprite(0,0,"blocks",backFrame);
        this.add(this.back);

        this.text=this.scene.add.text(0,0,value.toString(),{fontFamily:"Arial",align:"center",fontSize:"45px",color:"white",fontStyle:"bold",stroke:"black",strokeThickness:6});
        this.text.setOrigin(0.5,0.5);
        this.add(this.text);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
       
        
        this.scene.add.existing(this);

        this.back.setInteractive();
        this.back.on("pointerdown",()=>{callback(this)})
      
    }
    addNum(callback:Function)
    {
        if (this.isOff===false)
        {
            callback(this.value);
        }
        if (this.nextBlock)
        {
            this.nextBlock.addNum(callback);
        }
    }
    resetAll()
    {
        this.reset();
        if (this.nextBlock)
        {
            this.nextBlock.resetAll();
        }
    }
    reset()
    {
      //  console.log("reset");
        this.text.setColor("white");
        this.selected=false;
    }
    select()
    {
     //   console.log("select");
       /*  if (this.selected===true)
        {
            this.reset();
            
            return;
        } */
        this.text.setColor("red");
        this.selected=true;
    }
}