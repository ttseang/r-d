import { GM } from "../classes/GM";
import { WordVo } from "../dataObjs/WordVo";

export class SceneData extends Phaser.Scene
{
    constructor()
    {
        super("SceneData");
    }
    private gm:GM=GM.getInstance();
    create()
    {

        fetch("./assets/data.json")
        .then(response => response.json())
        .then(data => this.process(data));   
            
    }
    process(data:any)
    {
        
        data=data.data;
        let levels:WordVo[]=[];

        for (let i:number=0;i<data.length;i++)
        {
            
            let wordVo:WordVo=new WordVo(data[i].word,data[i].letters);
            levels.push(wordVo);
        }
        this.gm.levels=levels;

        this.scene.start("SceneMain");
    }
}