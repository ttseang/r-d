
import { GameObj } from "./gameObj";
import SoloTween, { SoloTweenEase } from "./solotween";

export class Game
{
   
   // private fruits:GameObj[]=[];
    private placeX:number=20;

    constructor()
    {
       /*  this.apple=new GameObj("apple1","assets/pics/lemon.png","falling");

        this.apple.myObj.onclick=this.destroyApple.bind(this); */
        this.addFruit();
        setInterval(this.addFruit.bind(this),1000);
    }
    addFruit()
    {
        let fruitNames:string[]=['apple','banana','cherries','kiwi','lemon','orange','pineapple','strawberry'];
        let rfruit:string=fruitNames[Math.floor(Math.random()*fruitNames.length)];
        console.log(rfruit);

        let fruit:GameObj=new GameObj("apple1","assets/pics/"+rfruit+".png","falling");
        fruit.x=this.placeX;
        fruit.y=10;
        
        fruit.onclick=this.popFruit.bind(this);
        
        let soloTween:SoloTween=new SoloTween(window,fruit.myObj.style,10,1,false,null,0,0);
        fruit.myTween=soloTween;
     //   this.fruits.push(fruit);

        soloTween.animate("top",100,500,0,SoloTweenEase.LINEAR,'px');

        this.placeX+=50;
        if (this.placeX>480)
        {
            this.placeX=20;
        }
    }
    popFruit(obj:GameObj)
    {
        console.log(obj);
        obj.removeTweens();
        obj.onclick=()=>{};

        let soloTween:SoloTween=new SoloTween(window,obj.style,0.2,1,false,null,0,0);
        soloTween.animate("top",parseFloat(obj.myObj.style.top),50,0,SoloTweenEase.LINEAR,'px');
        soloTween.onComplete=()=>{
            
                obj.removeTweens();
                //obj.myObj.remove();
                 let fadeTween:SoloTween=new SoloTween(window,obj.style,1,1,false,null,0,0);
                fadeTween.animate("opacity",1,0,2);

                fadeTween.onComplete=()=>{
                    obj.myObj.remove();
                }

               
            
        }
    }
    
    

}