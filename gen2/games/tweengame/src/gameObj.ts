import SoloTween from "./solotween";

export class GameObj {
    public myTween: SoloTween | null = null;
    public myObj: HTMLImageElement;
    private _onclick: Function = () => { };
    private _x:number=0;
    private _y:number=0;

    constructor(id: string, image: string, where: string) {
        this.myObj = document.createElement("img");
        this.myObj.src = image;
        this.myObj.classList.add('gameP');

        let wherePlace: HTMLElement | null = document.getElementById(where) || null;
        if (wherePlace) {
            wherePlace.appendChild(this.myObj);
        }
        this.myObj.onclick = () => {
            this._onclick(this);
        }
    }
    set onclick(fun: Function) {
        this._onclick = fun;
    }
    get style() {
        if (this.myObj) {
            return this.myObj.style;
        }
        return null;
    }
    removeTweens() {
        if (this.myTween) {
            this.myTween.dispose();
        }
    }
    get x()
    {
        return this._x;
    }
    get y()
    {
        return this._y;
    }
    set x(val:number)
    {
        this._x=val;
        this.myObj.style.left=val.toString()+"px";
    }
    set y(val:number)
    {
        this._y=val;
        this.myObj.style.top=val.toString()+"px";

    }
}