/**
 * SoloTween is a dependency-free class for handling animations
 * @author Alex T <alex.t.seraphim@googlemail.com>
 * @copyright @ 2006-2022 Teaching Textbooks. All rights reserved.
 */
 export default class SoloTween {

    private readonly id: number;

    private win: Window | null;
    private enterFrameListen: number;
    private timeStamp: number;
    private queueData: IQueueItem | null;
    private readonly data: ITweenData;
    private readonly frames: IFrame[];
    private readonly calling: ICallData;

    private _start: Function | null;
    private _progress: Function | null;
    private _complete: Function | null;
    private _reverse: Function | null;
    private _repeat: Function | null;
    private _stateChanged: Function | null;

    private static INSTANCE_ID: number = 0;
    private static QUEUE_ID: number = 0;
    private static queueData: Map<number, SoloTween[]> = new Map;
    private static queueHash: Map<SoloTween, number> = new Map;
    private static readonly TWEENS: Map<number, SoloTween> = new Map;
    private LOG_ENABLED: boolean = false;

    /**
     * SoloTween constructor
     * @param win - Window instance
     * @param target - animations target object
     * @param duration - animations duration, in seconds
     * @param repeats - amount of repeats
     * @param reversed - is animation reversed
     * @param pattern - animation properties pattern
     * @param reverseDelay - delay before reverse, in seconds
     * @param repeatDelay - delay between repeats, in seconds
     */
    constructor(
        win: Window,
        target: any,
        duration: number,
        repeats: number = 1,
        reversed: boolean = false,
        pattern: string | null = null,
        reverseDelay: number = 0,
        repeatDelay: number = 0
    ) {
        (window as any).st=this;
        this.id = SoloTween.INSTANCE_ID;
        SoloTween.INSTANCE_ID++;
        SoloTween.TWEENS.set(this.id, this);

        this.win = win;
        this._start = this._progress = this._complete = this._reverse = this._repeat = this._stateChanged = null;
        this.enterFrameListen = -1;
        this.timeStamp = -1;
        this.frames = [];
        this.queueData = null;

        this.data = {
            target: target,
            paused: false,
            launched: false,
            started: false,
            completed: false,
            properties: [],
            time: 0,
            duration: duration * 1000,
            delay: 0,
            initDelay: 0,
            repeats: repeats,
            repeating: false,
            repeatDelay: repeatDelay * 1000,
            repeatDelayed: 0,
            reversed: reversed,
            pattern: pattern,
            reversing: false,
            reverseCount: 0,
            repeatsCount: 0,
            reverseDelay: reverseDelay * 1000,
            reverseDelayed: 0
        };

        this.calling = {
            active: false,
            method: '',
            times: 0,
            duration: 0,
            ease: SoloTweenEase.LINEAR,
            delay: 0,
            initDelay: 0,
            frames: [],
            initFrames: []
        };
    }

    /* static public API * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     * Launch tweens queue
     * @param queue - an array of SoloTween instances
     * @return qid - unique queue ID
     * @static
     */
    static launchQueue(queue: SoloTween[]): number {
        if (!queue.length) {
            console.warn('SoloTween can\'t launch empty queue...');
            return -1;
        }

        const qid = SoloTween.QUEUE_ID;
        SoloTween.QUEUE_ID++;

        queue.map((tween) => {
            SoloTween.queueHash.set(tween, qid);
        });

        SoloTween.queueData.set(qid, [...queue]);
        SoloTween.proceedQueue(qid);

        return qid;
    }
    private static proceedQueue(qid: number): void {
        const queue = SoloTween.queueData.get(qid) || [];

        if (!queue.length) {
            // console.info(`SoloTween queue ${qid} complete`);
            SoloTween.queueData.delete(qid);
            return;
        }

        const tween = queue[0];
        if (tween) {
            tween.onComplete = SoloTween.queueTweenComplete;
            queue[0].launchQueue();
        } else {
            SoloTween.proceedQueue(qid);
        }
    }
    private static queueTweenComplete(tween: SoloTween): void {
        const qid = SoloTween.queueHash.get(tween);
        const queue = SoloTween.queueData.get(qid!) || [];
        queue.shift();

        SoloTween.queueHash.delete(tween);
        tween.dispose();
        SoloTween.proceedQueue(qid!);
    }

    /**
     * Dispose all active queues
     * @static
     */
    static disposeQueues(): void {
        const keys = SoloTween.queueData.keys();
        while (true) {
            const next = keys.next();
            if (next.done) break;
            SoloTween.disposeQueueByID(next.value);
        }
    }

    /**
     * Dispose queue by ID
     * @param qid - unique queue ID
     * @static
     */
    static disposeQueueByID(qid: number): void {
        if (!SoloTween.queueData.has(qid)) return;
        const queue = SoloTween.queueData.get(qid) || [];
        SoloTween.queueData.delete(qid);

        queue.map((tween) => {
            SoloTween.queueHash.delete(tween);
            tween.dispose();
        });
    }

    /**
     * Pause SoloTween instance by unique ID
     * @param id - unique instance ID
     * @static
     */
    static pauseByID(id: number): void {
        this.pauseResumeDisposeByID(id, true, false, false);
    }
    /**
     * Resume SoloTween instance by unique ID
     * @param id - unique instance ID
     * @static
     */
    static resumeByID(id: number): void {
        this.pauseResumeDisposeByID(id, false, false, false);
    }
    /**
     * Reset SoloTween instance by unique ID
     * @param id - unique instance ID
     * @static
     */
    static resetByID(id: number): void {
        this.pauseResumeDisposeByID(id, false, true, false);
    }
    /**
     * Dispose SoloTween instance by unique ID
     * @param id - unique instance ID
     * @static
     */
    static disposeByID(id: number): void {
        this.pauseResumeDisposeByID(id, false, true, true);
        SoloTween.TWEENS.delete(id);
    }
    private static pauseResumeDisposeByID(id: number, pause: boolean, reset: boolean, dispose: boolean): void {
        const tween = SoloTween.TWEENS.get(id);
        if (!tween) return;
        pause ?
            tween.pause() :
            reset ?
                tween.reset(true, false) :
                dispose ?
                    tween.dispose() :
                    tween.resume();
    }

    /**
     * Pause all SoloTween instances
     * @static
     */
    static pauseAll(): void {
        SoloTween.pauseResumeDispose(true, false, false);
    }

    /**
     * Resume all SoloTween instances
     * @static
     */
    static resumeAll(): void {
        SoloTween.pauseResumeDispose(false, false, false);
    }

    /**
     * Reset all SoloTween instances
     * @static
     */
    static resetAll(): void {
        SoloTween.pauseResumeDispose(false, true, false);
    }

    /**
     * Dispose all SoloTween instances
     * @static
     */
    static disposeAll(): void {
        SoloTween.pauseResumeDispose(false, false, true);
        SoloTween.TWEENS.clear();
    }
    private static pauseResumeDispose(pause: boolean, reset: boolean, dispose: boolean): void {
        const tweens = SoloTween.TWEENS.values();
        while (true) {
            const next = tweens.next();
            if (next.done) break;

            const tween = next.value;

            pause ?
                tween.pause() :
                reset ?
                    tween.reset(true, false) :
                    dispose ?
                        tween.dispose() :
                        tween.resume();
        }
    }

    /* instance public API * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     * Unique SoloTween instance ID
     * @return id - unique ID
     */
    getID(): number {
        return this.id;
    }

    /**
     * Animations target
     * @return target - target with any type
     */
    getTarget(): any {
        return this.data?.target;
    }

    /**
     * Whether animations were launched
     * @return launched - boolean value
     */
    isLaunched(): boolean {
        return this.data.launched;
    }

    /**
     * Whether animations were launched and still not completed
     * @return tweening - boolean value
     */
    isTweening(): boolean {
        return this.data.launched && !this.data.completed;
    }

    /**
     * Whether animations were paused
     * @return paused - boolean value
     */
    isPaused(): boolean {
        return this.data.paused;
    }

    /**
     * Whether animations were completed
     * @return completed - boolean value
     */
    isComplete(): boolean {
        return this.data.completed;
    }

    /**
     * Launch method calls
     * @param method - method to be called
     * @param times - amount of calls
     * @param delay - delay before first call, in seconds
     * @param ease - calls easing function
     */
    call(method: string | Function, times: number, delay: number = 0, ease: string | null = SoloTweenEase.LINEAR): void {
        if (this.data.launched) {
            console.warn('SoloTween already launched...');
            return;
        }

        this.calling.active = true;
        this.calling.method = method;
        this.calling.times = times;
        this.calling.duration = this.data.duration;
        this.calling.delay = this.calling.initDelay = delay * 1000;
        this.calling.ease = ease || SoloTweenEase.LINEAR;
        this.calling.frames.length = 0;

        const easing = SoloTweenEase.getMethod(this.calling.ease);

        let tm = 0;
        while (++tm <= times) {
            const flag = easing(tm, 0, this.calling.duration, times);
            this.calling.initFrames.push({
                time: flag,
            });
        }
        this.calling.frames = [...this.calling.initFrames];

        this.data.launched = true;
        this.timeStamp = (new Date).getTime();
        this.callingEnterFrame();
    }

    /**
     * Save animations data for queue case
     * @param property - array of properties to animate
     * @param delay - delay before launch, in seconds
     */
    queue(property: ISoloTweenProp[], delay: number): void {
        if (this.queueData) {
            console.warn('SoloTween queue already defined');
            return;
        }
        this.queueData = {
            properties: [...property],
            delay: delay
        };
    }

    /**
     * Launch animations in queue case
     * @return tween - current SoloTween instance
     */
    launchQueue(): SoloTween | null {
        if (!this.queueData) {
            console.warn('SoloTween queue wasn\'t defined...');
            return null;
        }
        this.animate(this.queueData.properties, this.queueData.delay);
        this.queueData = null;
        return this;
    }

    /**
     * Animate signature #1, for simple single animation
     * @param property - property to animate
     * @param from - start property value
     * @param to - end property value
     * @param delay - delay before launch, in seconds
     * @param ease - animation easing function
     * @param postfix - property postfix, if required
     */
    animate(property: string, from: number, to: number, delay?: number, ease?: string, postfix?: string): void;

    /**
     * Animate signature #2, with single ISoloTweenProp object
     * @param property - ISoloTweenProp data object
     * @param delay - delay before launch, in seconds
     */
    animate(property: ISoloTweenProp, delay?: number): void;

    /**
     * Animate signature #3, with multiple ISoloTweenProp objects
     * @param property - ISoloTweenProp data objects
     * @param delay - delay before launch, in seconds
     */
    animate(property: ISoloTweenProp[], delay?: number): void;

    /**
     * Common animate signature, for all cases
     * @param args - incoming data
     */
    animate(...args: any[]): void {
        if (!this.data) {
            this.dispose();
            return;
        } else if (this.data.launched) {
            console.warn('SoloTween already launched:', this.data.properties);
            return;
        }

        let delay: number = 0;
        if (typeof (args[0]) === 'string') {
            this.log('animate, signature #1');
            delay = args[3];
            this.data.properties = [
                {
                    key: null,
                    prop: args[0],
                    from: args[1],
                    to: args[2],
                    ease: args[4] || SoloTweenEase.LINEAR,
                    postfix: args[5] || ''
                }
            ];
        } else if (!Array.isArray(args[0])) {
            this.log('animate, signature #2');
            this.data.properties = [
                { ...args[0] }
            ];
            delay = args[1];
        } else if (Array.isArray(args[0])) {
            this.log('animate, signature #3');
            this.data.properties = [...args[0] as ISoloTweenProp[]];
            delay = args[1];
        }

        this.data.delay = this.data.initDelay = delay * 1000;
        this.data.launched = true;
        this.timeStamp = (new Date).getTime();

        if (this.data.duration > 0) {
            this.animateEnterFrame();
        } else {
            this.updateNormal(true);
            this.data.completed = true;
            this._start && this._start(this);
            this._progress && this._progress(this, 1);
            this._complete && this._complete(this);

        }
    }

    /**
     * Pause animation
     */
    pause(): void {
        if (this.data.paused) return;
        this.log('pause');
        this.data.paused = true;
        this._stateChanged && this._stateChanged(this);
    }

    /**
     * Resume animation
     */
    resume(): void {
        if (!this.data.paused) return;
        this.log('resume');
        this.timeStamp = (new Date).getTime();
        this.data.paused = false;
        this.requestFrame();
        this._stateChanged && this._stateChanged(this);
    }

    /**
     * Reset animation
     * @param full - whether reset is full (not full reset used in restarting animation for repeats)
     * @param restart - whether animation should be started after reset
     */
    reset(full: boolean, restart: boolean): void {
        this.log('reset:', full, restart);

        this.frames.length = 0;
        let tween = this.data;
        tween.time = 0;
        tween.reverseCount = 0;
        tween.reverseDelayed = 0;
        tween.repeatDelayed = 0;
        tween.started = tween.launched = tween.completed = tween.repeating = tween.reversing = false;

        this.calling.frames = [...this.calling.initFrames];

        this.cancelEnterFrame();

        if (full) {
            tween.repeatsCount = 0;
            tween.delay = tween.initDelay;
            this.calling.delay = this.calling.initDelay;

            const patterns: Map<string, string> = new Map;
            tween.properties.map((prop) => {
                SoloTween.updateProp(tween, prop.key || null, prop.prop, prop.from, prop.postfix || null, patterns);
            });
        }

        if (restart) {
            tween.launched = true;
            this.timeStamp = (new Date).getTime();
            this.requestFrame();
        }
    }

    /**
     * Declare event listener for start animation event
     * @param method - event listener
     */
    set onStart(method: Function) {
        this._start = method;
    }

    /**
     * Declare event listener for progress animation events
     * @param method - event listener
     */
    set onProgress(method: Function) {
        this._progress = method;
    }

    /**
     * Declare event listener for complete animation event
     * @param method - event listener
     */
    set onComplete(method: Function) {
        this._complete = method;
    }

    /**
     * Declare event listener for reverse animation event(s)
     * @param method - event listener
     */
    set onReverse(method: Function) {
        this._reverse = method;
    }

    /**
     * Declare event listener for repeat animation event(s)
     * @param method - event listener
     */
    set onRepeat(method: Function) {
        this._repeat = method;
    }

    /**
     * Declare event listener for state change events (paused/active)
     * @param method - event listener
     */
    set onStateChanged(method: Function) {
        this._stateChanged = method;
    }

    /**
     * Dispose SoloTween instance
     */
    dispose(): void {
        this.log('dispose');
        this.win = null;
        this.data.target = null;
        this.data.properties.length = 0;

        this.frames.length = 0;
        this.queueData = null;

        this._start = this._progress = this._complete = this._reverse = this._repeat = this._stateChanged = null;
        this.cancelEnterFrame();

        SoloTween.TWEENS.delete(this.id);
    }

    /* private methods * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     * Request to the enter frame
     * @private
     */
    private requestFrame(): void {
        if (!this.win) {
            return;
        }
        this.enterFrameListen = this.win.requestAnimationFrame(
            this.calling.active ?
                this.callingEnterFrame.bind(this) :
                this.animateEnterFrame.bind(this)
        );
    }

    /**
     * Handling enter frame event for method calls
     * @private
     */
    private callingEnterFrame(): void {
        const time = (new Date).getTime();
        const dt = time - this.timeStamp;
        this.timeStamp = time;
        if (this.data.paused) return;

        const call = this.calling;
        const tween = this.data;
        this.log('enter frame #5:', tween.properties, call.delay);

        if (call.delay > 0) {
            call.delay -= dt;
            if (call.delay > 0) {
                this.requestFrame();
                return;
            }
        }

        if (!tween.started) {
            this.log('enter frame #6, started');
            tween.started = true;
            this._start && this._start(this);
        }

        if (call.delay < 0) {
            tween.time = -call.delay;
            call.delay = 0;
        } else {
            tween.time += dt;
        }

        tween.time = Math.min(tween.time, call.duration);

        let fr = -1, found: boolean = false, frame: IKeyFrame | null = null;
        while (++fr < call.frames.length) {
            frame = call.frames[fr];
            if (tween.time >= frame.time) {
                call.frames.splice(fr, 1);
                fr--;
                found = true;

                if (call.method) {
                    typeof (call.method) == 'string' ?
                        tween.target.hasOwnProperty(call.method) && tween.target[call.method]() :
                        call.method();
                }
            }
        }
        if (found && frame && this._progress) {
            this._progress(this, frame.time / call.duration);
        }

        if (tween.time >= call.duration) {
            this.log('enter frame #7, completed');

            this.data.completed = true;
            this._complete && this._complete(this);
            this.cancelEnterFrame();
        } else {
            this.requestFrame();
        }
    }

    /**
     * Handling enter frame event for regular animations
     * @private
     */
    private animateEnterFrame(): void {
        const time = (new Date).getTime();
        const dt = time - this.timeStamp;
        this.timeStamp = time;
        if (this.data.paused) return;

        const tween = this.data;
        this.log('enter frame #1:', tween.properties, tween.delay);

        if (tween.delay > 0) {
            tween.delay -= dt;
            if (tween.delay > 0) {
                this.requestFrame();
                return;
            }
        }

        if (tween.repeating) {
            if (tween.repeatDelay > 0) {
                tween.repeatDelayed += dt;
                if (tween.repeatDelayed < tween.repeatDelay) {
                    this.requestFrame();
                    return;
                }
            }
        }

        if (!tween.started) {
            this.log('enter frame #2, started');
            tween.started = true;
            this._start && this._start(this);
        }

        if (tween.delay < 0) {
            tween.time = -tween.delay;
            tween.delay = 0;
        } else {
            tween.time += dt;
        }

        tween.time = Math.min(tween.time, tween.duration);

        if (tween.reversing) {
            if (tween.reverseDelay > 0) {
                tween.reverseDelayed += dt;
                if (tween.reverseDelayed < tween.reverseDelay) {
                    this.requestFrame();
                    return;
                }
            }

            const bool = this.updateReverse();
            if (bool) {
                this.requestFrame();
            } else {
                if ((tween.repeats > 1 || tween.repeats < 1) && this.restart()) {
                    this._repeat && this._repeat(this, tween.repeatsCount + 1, tween.repeats);
                    tween.repeating = true;
                    tween.launched = true;
                    this.requestFrame();
                } else {
                    this.data.completed = true;
                    this._complete && this._complete(this);
                    this.cancelEnterFrame();
                }

            }
            return;
        } else {
            this.updateNormal();
        }

        if (tween.time >= tween.duration) {
            this.log('enter frame #4, completed');

            if (tween.reversed) {
                tween.reversing = true;
                tween.reverseCount = this.frames.length;
                this._reverse && this._reverse(this);
                this.requestFrame();
            } else if ((tween.repeats > 1 || tween.repeats < 1) && this.restart()) {
                tween.launched = true;
                tween.repeating = true;
                this.requestFrame();
            } else {
                this.data.completed = true;
                this._complete && this._complete(this);
                this.cancelEnterFrame();
            }
        } else {
            this.requestFrame();
        }
    }

    /**
     * Cancel enter frame request
     * @private
     */
    private cancelEnterFrame(): void {
        this.win?.cancelAnimationFrame(this.enterFrameListen);
        this.enterFrameListen = -1;
    }

    /**
     * Restart animation, used in repeats
     * @private
     */
    private restart(): boolean {
        this.reset(false, false);
        let tween = this.data;

        if (tween.repeats > 1) tween.repeatsCount++;
        return tween.repeats <= 0 || (tween.repeats > 1 && tween.repeatsCount < tween.repeats);
    }

    /**
     * Updating properties per each frame for regular animation
     * @param single - boolean, used for zero duration case
     * @private
     */
    private updateNormal(single: boolean = false): void {
        if (!this.data.target) {
            /* seems like instance was disposed */
            return;
        }

        let tween = this.data;
        const frame: IFrame = { props: [] };

        const patterns: Map<string, string> = new Map;
        let progress = NaN;
        tween.properties.map((prop) => {
            let val;

            if (!single) {
                const ease = prop.ease || SoloTweenEase.LINEAR;
                const method = SoloTweenEase.getMethod(ease);

                val = method(tween.time, prop.from, (prop.to - prop.from), tween.duration);

                if (isNaN(progress)) {
                    progress = 1 - (prop.to - val) / (prop.to - prop.from);

                    if (isNaN(progress)) progress = 0;
                    progress = Math.min(progress, 1);
                    progress = Math.max(progress, 0);
                }
            } else {
                val = this.data.reversed ? prop.from : prop.to;
                if (isNaN(progress)) progress = 1;
            }

            this.log(`enter frame #3, "${prop.prop}", progress:', ${val}`);
            SoloTween.updateProp(tween, prop.key || null, prop.prop, val, prop.postfix || null, patterns);

            /* saving frame data */
            frame.props.push({
                key: prop.key || null,
                prop: prop.prop,
                value: val,
                postfix: prop.postfix || null
            });
        });

        if (tween.reversed) progress /= 2;
        this._progress && this._progress(this, progress);

        this.frames.push(frame);
    }

    /**
     * Updating properties per each frame for reverse animation
     * @private
     */
    private updateReverse(): boolean {
        if (!this.data.target) {
            /* seems like instance was disposed */
            return false;
        }

        let tween = this.data;
        tween.reverseCount--;

        const patterns: Map<string, string> = new Map;

        if (tween.reverseCount < 0) {
            tween.properties.map((prop) => {
                SoloTween.updateProp(tween, prop.key || null, prop.prop, prop.from, prop.postfix || null, patterns);
            });

            return false;
        } else {
            const frame = this.frames[tween.reverseCount];

            frame.props.map((data) => {
                SoloTween.updateProp(tween, data.key || null, data.prop, data.value, data.postfix || null, patterns);
            });
            let progress = (this.frames.length - tween.reverseCount) / this.frames.length / 2;
            this._progress && this._progress(this, .5 + progress);

            return true;
        }
    }

    /**
     * Updating single property
     * @param tween - current tween data object
     * @param key - property key (if presented)
     * @param prop - property name
     * @param val - property value
     * @param postfix - postfix (if required)
     * @param patterns - pattern (if presented)
     * @private
     * @static
     */
    private static updateProp(tween: ITweenData, key: string | null, prop: string, val: number, postfix: string | null, patterns: Map<string, string>): void {
        if (tween.pattern && key) {

            let value = patterns.has(prop) ?
                patterns.get(prop) :
                tween.pattern;

            value = value!.replace(key, val.toString());
            patterns.set(prop, value);

            if (tween.target.hasOwnProperty(prop) || (prop in tween.target)) {
                tween.target[prop] = value;
            }
        } else if (tween.target.hasOwnProperty(prop) || (prop in tween.target)) {
            tween.target[prop] = postfix ? val + postfix : val;
        }
    }

    /**
     * Log method
     * @param args
     * @private
     */
    private log(...args: any[]): void {
        if (!this.LOG_ENABLED) return;
        console.log.apply(this, args);
    }
}

/**
 * Single tween property data object interface
 */
export interface ISoloTweenProp {
    key?: string | null;
    prop: string,
    from: number;
    to: number;
    ease?: string;
    postfix?: string;
}

/**
 * Main tween data object interface
 * @package
 */
interface ITweenData {
    target: any;
    launched: boolean;
    paused: boolean;
    started: boolean;
    completed: boolean;
    properties: ISoloTweenProp[];
    time: number;
    duration: number;
    delay: number;
    initDelay: number;
    repeats: number;
    repeating: boolean;
    repeatDelay: number;
    repeatsCount: number;
    repeatDelayed: number;
    reversed: boolean;
    reversing: boolean;
    reverseDelay: number;
    reverseCount: number;
    reverseDelayed: number;
    pattern: string | null;
}

/**
 * Method calls data object interface
 * @package
 */
interface ICallData {
    active: boolean,
    method: string | Function,
    times: number,
    duration: number,
    ease: string,
    delay: number,
    initDelay: number;
    frames: IKeyFrame[],
    initFrames: IKeyFrame[];
}

/**
 * Saved animation values per single frame during the regular animation interface
 * @package
 */
interface IFrame {
    props: IPropValue[];
}

/**
 * Single frame time data interface for method calls
 * @package
 */
interface IKeyFrame {
    time: number;
}

/**
 * Single queue object data interface
 * @package
 */
interface IQueueItem {
    properties: ISoloTweenProp[],
    delay: number;
}

/**
 * Animation property data object interface
 * @package
 */
interface IPropValue {
    key: string | null;
    prop: string;
    value: number;
    postfix: string | null;
}

/**
 * Easing handling class
 */
export class SoloTweenEase {
    static LINEAR = 'linear';
    static IN_QUAD = 'in-quad';
    static OUT_QUAD = 'out-quad';
    static IN_OUT_QUAD = 'in-out-quad';
    static OUT_IN_QUAD = 'out-in-quad';
    static IN_SINE = 'in-sine';
    static OUT_SINE = 'out-sine';
    static IN_OUT_SINE = 'in-out-sine';
    static OUT_IN_SINE = 'out-in-sine';
    static IN_EXPO = 'in-expo';
    static OUT_EXPO = 'out-expo';
    static IN_OUT_EXPO = 'in-out-expo';
    static OUT_IN_EXPO = 'out-in-expo';
    static IN_CIRC = 'in-circ';
    static OUT_CIRC = 'out-circ';
    static IN_OUT_CIRC = 'in-out-circ';
    static OUT_IN_CIRC = 'out-in-circ';
    static IN_CUBIC = 'in-cubic';
    static OUT_CUBIC = 'out-cubic';
    static IN_OUT_CUBIC = 'in-out-cubic';
    static OUT_IN_CUBIC = 'out-in-cubic';
    static IN_QUART = 'in-quart';
    static OUT_QUART = 'out-quart';
    static IN_OUT_QUART = 'in-out-quart';
    static OUT_IN_QUART = 'out-in-quart';
    static IN_QUINT = 'in-quint';
    static OUT_QUINT = 'out-quint';
    static IN_OUT_QUINT = 'in-out-quint';
    static OUT_IN_QUINT = 'out-in-quint';
    static IN_ELASTIC = 'in-elastic';
    static OUT_ELASTIC = 'out-elastic';
    static IN_OUT_ELASTIC = 'in-out-elastic';
    static OUT_IN_ELASTIC = 'out-in-elastic';
    static IN_BACK = 'in-back';
    static OUT_BACK = 'out-back';
    static IN_OUT_BACK = 'in-out-back';
    static OUT_IN_BACK = 'out-in-back';
    static IN_BOUNCE = 'in-bounce';
    static OUT_BOUNCE = 'out-bounce';
    static IN_OUT_BOUNCE = 'in-out-bounce';
    static OUT_IN_BOUNCE = 'out-in-bounce';
    private static easeHash: Map<string, Function>;

    static init() {
        if (SoloTweenEase.easeHash) return;
        SoloTweenEase.easeHash = new Map<string, Function>();
        SoloTweenEase.easeHash.set(SoloTweenEase.LINEAR, SoloTweenEase._linear);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_QUAD, SoloTweenEase._inQuad);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_QUAD, SoloTweenEase._outQuad);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_QUAD, SoloTweenEase._inOutQuad);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_QUAD, SoloTweenEase._outInQuad);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_SINE, SoloTweenEase._inSine);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_SINE, SoloTweenEase._outSine);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_SINE, SoloTweenEase._inOutSine);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_SINE, SoloTweenEase._outInSine);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_EXPO, SoloTweenEase._inExpo);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_EXPO, SoloTweenEase._outExpo);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_EXPO, SoloTweenEase._inOutExpo);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_EXPO, SoloTweenEase._outInExpo);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_CIRC, SoloTweenEase._inCirc);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_CIRC, SoloTweenEase._outCirc);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_CIRC, SoloTweenEase._inOutCirc);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_CIRC, SoloTweenEase._outInCirc);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_CUBIC, SoloTweenEase._inCubic);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_CUBIC, SoloTweenEase._outCubic);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_CUBIC, SoloTweenEase._inOutCubic);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_CUBIC, SoloTweenEase._outInCubic);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_QUART, SoloTweenEase._inQuart);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_QUART, SoloTweenEase._outQuart);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_QUART, SoloTweenEase._inOutQuart);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_QUART, SoloTweenEase._outInQuart);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_QUINT, SoloTweenEase._inQuint);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_QUINT, SoloTweenEase._outQuint);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_QUINT, SoloTweenEase._inOutQuint);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_QUINT, SoloTweenEase._outInQuint);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_ELASTIC, SoloTweenEase._inElastic);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_ELASTIC, SoloTweenEase._outElastic);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_ELASTIC, SoloTweenEase._inOutElastic);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_ELASTIC, SoloTweenEase._outInElastic);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_BACK, SoloTweenEase._inBack);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_BACK, SoloTweenEase._outBack);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_BACK, SoloTweenEase._inOutBack);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_BACK, SoloTweenEase._outInBack);

        SoloTweenEase.easeHash.set(SoloTweenEase.IN_BOUNCE, SoloTweenEase._inBounce);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_BOUNCE, SoloTweenEase._outBounce);
        SoloTweenEase.easeHash.set(SoloTweenEase.IN_OUT_BOUNCE, SoloTweenEase._inOutBounce);
        SoloTweenEase.easeHash.set(SoloTweenEase.OUT_IN_BOUNCE, SoloTweenEase._outInBounce);
    }

    static getMethod(ease: string): Function {
        return SoloTweenEase.easeHash.get(ease) || this._linear;
    }

    private static _linear(t: number, b: number, c: number, d: number): number {
        return c * t / d + b;
    }

    private static _inQuad(t: number, b: number, c: number, d: number): number {
        return c * (t /= d) * t + b;
    }
    private static _outQuad(t: number, b: number, c: number, d: number): number {
        return -c * (t /= d) * (t - 2) + b;
    }
    private static _inOutQuad(t: number, b: number, c: number, d: number): number {
        if ((t /= d / 2) < 1) return c / 2 * t * t + b;
        return -c / 2 * ((--t) * (t - 2) - 1) + b;
    }
    private static _outInQuad(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outQuad(t * 2, b, c / 2, d);
        return SoloTweenEase._inQuad((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inSine(t: number, b: number, c: number, d: number): number {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    }
    private static _outSine(t: number, b: number, c: number, d: number): number {
        return c * Math.sin(t / d * (Math.PI / 2)) + b;
    }
    private static _inOutSine(t: number, b: number, c: number, d: number): number {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    }
    private static _outInSine(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outSine(t * 2, b, c / 2, d);
        return SoloTweenEase._inSine((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inExpo(t: number, b: number, c: number, d: number): number {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    }
    private static _outExpo(t: number, b: number, c: number, d: number): number {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    }
    private static _inOutExpo(t: number, b: number, c: number, d: number): number {
        if (t == 0) return b;
        if (t == d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
    private static _outInExpo(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outExpo(t * 2, b, c / 2, d);
        return SoloTweenEase._inExpo((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inCirc(t: number, b: number, c: number, d: number): number {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    }
    private static _outCirc(t: number, b: number, c: number, d: number): number {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    }
    private static _inOutCirc(t: number, b: number, c: number, d: number): number {
        if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    }
    private static _outInCirc(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outCirc(t * 2, b, c / 2, d);
        return SoloTweenEase._inCirc((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inCubic(t: number, b: number, c: number, d: number): number {
        return c * (t /= d) * t * t + b;
    }
    private static _outCubic(t: number, b: number, c: number, d: number): number {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    }
    private static _inOutCubic(t: number, b: number, c: number, d: number): number {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    }
    private static _outInCubic(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outCubic(t * 2, b, c / 2, d);
        return SoloTweenEase._inCubic((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inQuart(t: number, b: number, c: number, d: number): number {
        return c * (t /= d) * t * t * t + b;
    }
    private static _outQuart(t: number, b: number, c: number, d: number): number {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    }
    private static _inOutQuart(t: number, b: number, c: number, d: number): number {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    }
    private static _outInQuart(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outQuart(t * 2, b, c / 2, d);
        return SoloTweenEase._inQuart((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inQuint(t: number, b: number, c: number, d: number): number {
        return c * (t /= d) * t * t * t * t + b;
    }
    private static _outQuint(t: number, b: number, c: number, d: number): number {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    }
    private static _inOutQuint(t: number, b: number, c: number, d: number): number {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
    }
    private static _outInQuint(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outQuint(t * 2, b, c / 2, d);
        return SoloTweenEase._inQuint((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inElastic(t: number, b: number, c: number, d: number): number {
        let s = 1.70158;
        let p = 0;
        let a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        }
        else s = p / (2 * Math.PI) * Math.asin(c / a);
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    }
    private static _outElastic(t: number, b: number, c: number, d: number): number {
        let s = 1.70158;
        let p = 0;
        let a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        }
        else s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    }
    private static _inOutElastic(t: number, b: number, c: number, d: number): number {
        let s = 1.70158;
        let p = 0;
        let a = c;
        if (t == 0) return b;
        if ((t /= d / 2) == 2) return b + c;
        if (!p) p = d * (.3 * 1.5);
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        }
        else s = p / (2 * Math.PI) * Math.asin(c / a);
        if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
    }
    private static _outInElastic(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outElastic(t * 2, b, c / 2, d);
        return SoloTweenEase._inElastic((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _inBack(t: number, b: number, c: number, d: number): number {
        let s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    }
    private static _outBack(t: number, b: number, c: number, d: number): number {
        let s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    }
    private static _inOutBack(t: number, b: number, c: number, d: number): number {
        let s = 1.70158;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    }
    private static _outInBack(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outBack(t * 2, b, c / 2, d);
        return SoloTweenEase._inBack((t * 2) - d, b + c / 2, c / 2, d);
    }

    private static _outBounce(t: number, b: number, c: number, d: number): number {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        } else if (t < (2 / 2.75)) {
            return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
        } else if (t < (2.5 / 2.75)) {
            return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
        } else {
            return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
        }
    }
    private static _inBounce(t: number, b: number, c: number, d: number): number {
        return c - SoloTweenEase._outBounce(d - t, 0, c, d) + b;
    }
    private static _inOutBounce(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._inBounce(t * 2, 0, c, d) * .5 + b;
        else return SoloTweenEase._outBounce(t * 2 - d, 0, c, d) * .5 + c * .5 + b;
    }
    private static _outInBounce(t: number, b: number, c: number, d: number): number {
        if (t < d / 2) return SoloTweenEase._outBounce(t * 2, b, c / 2, d);
        return SoloTweenEase._inBounce((t * 2) - d, b + c / 2, c / 2, d);
    }

}

SoloTweenEase.init();