
import { ColorBurst } from "gameEffect";
import { CompLayout, CompManager, ButtonController, CompLoader, IComp } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { AnimationUtil } from "../classes/animUtil";
import { WordBox } from "../classes/WordBox";
import { ConVo } from "../dataObjs/ConVo";
import { PairVo } from "../dataObjs/PairVo";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;

    private words: PairVo[] = [];
    private boxes1: WordBox[] = [];
    private boxes2: WordBox[] = [];
    private col1: number = 3;
    private col2: number = 7;

    private upBox: WordBox;
    private downBox: WordBox;
    private pointerIsDown: boolean = false;

    private graphics: Phaser.GameObjects.Graphics;
    private boxLines: Phaser.GameObjects.Graphics;

    private connections: ConVo[] = [];
    private animationUtil: AnimationUtil = new AnimationUtil();
    private butterfly: Phaser.GameObjects.Sprite;
    private background: Phaser.GameObjects.Image;

    private stars:ColorBurst;

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("arrows", "./assets/arrows.png");

        this.load.image("background", "./assets/background.jpg");
        this.load.audio("rightSound", "./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound", "./assets/audio/quiz_wrong.wav");
        this.load.audio("pop", "./assets/audio/pop.wav");
        this.load.atlas("butterfly", "./assets/butterfly.png", "./assets/butterfly.json");

        ColorBurst.preload(this,"./assets/effects/colorStars.png");

    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.cm.regFontSize("list", 30, 1200);
        this.cm.regFontSize("words", 40, 1200);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        window['scene'] = this;

        this.stars=new ColorBurst(this,this.gw/2,this.gh/2);
        this.stars.count=50;

        this.buttonController.callback = this.doButtonAction.bind(this);
    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);
        this.animationUtil.makeButterfly(this, "butterfly");
        this.loadGameData();
    }
    loadGameData() {

        fetch("./assets/words.json")
            .then(response => response.json())
            .then(data => this.gotGameData({ data }));
    }
    gotGameData(data: any) {
        let levels: any = data.data.levels;
        let level = levels[0];
        let words = level.words;
        //console.log(words);

        for (let i: number = 0; i < words.length; i++) {
            let word1: string = words[i].word1;
            let word2: string = words[i].word2;

            this.words.push(new PairVo(word1, word2));
        }

        this.buildGame();
    }
    buildGame() {
        this.background = this.add.image(0, 0, "background");
        //Align.scaleToGameW(this.background,1.1,this);

        let win: IComp = this.cm.getComp("uiWindow");
        this.background.displayWidth = win.displayWidth * 0.99;
        this.background.displayHeight = win.displayHeight * 0.99;

        /* if (this.background.displayHeight<this.gh)
        {
            Align.scaleToGameH(this.background,1.1,this);
        } */
        Align.center(this.background, this);

        this.makeBoxes();
        this.mixUp(this.boxes1);
        this.mixUp(this.boxes2);
        //
        //
        //
        this.graphics = this.add.graphics();
        this.boxLines = this.add.graphics();
        this.boxLines.lineStyle(4, 0xff0000);
        //
        //
        //
        /*  this.input.on("gameobjectover", this.onOver.bind(this));
         this.input.on("gameobjectout", this.onOut.bind(this));
         this.input.on("pointerup", this.onUp.bind(this));
         this.input.on("pointerdown", this.onDown.bind(this)); */

        this.input.on("gameobjectdown", this.onDown.bind(this));
        this.input.on("gameobjectup", this.onUp.bind(this));



        this.butterfly = this.add.sprite(0, 0, "butterfly");
        Align.scaleToGameW(this.butterfly, 0.1, this);
        this.butterfly.play("idle");

        this.flyTo(this.gw * 0.1, this.gh * 0.1);
    }
    flyTo(xx: number, yy: number) {
        this.butterfly.play("fly");
        this.tweens.add({ targets: this.butterfly, duration: 500, y: yy, x: xx, onComplete: this.flyDone.bind(this) });
    }
    flyDone() {
        this.butterfly.play("idle");
    }
    onDown(pointer, obj) {
        //console.log("down");

        if (obj instanceof (WordBox)) {

            this.pointerIsDown = true;
            this.downBox = obj;
            this.flyTo(obj.x, obj.y);
        }
    }
    onUp(pointer, obj) {
        //console.log("on up");
        this.pointerIsDown = false;
        this.upBox = obj;
        if (this.downBox) {
            this.compare();
            this.redrawAllLines();
        }
        else {
            this.reset();
        }

    }
    doResize() {
        //console.log(this.boxes1);

        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
        // this.grid.showPos();
        this.cm.doResize();

        for (let i: number = 0; i < this.boxes1.length; i++) {
            let box1: WordBox = this.boxes1[i];
            let box2: WordBox = this.boxes2[i];

            box1.doResize();
            box2.doResize();

            //box1.alpha=.5;
        }

        let win: IComp = this.cm.getComp("uiWindow");
        this.background.displayWidth = win.displayWidth * 0.99;
        this.background.displayHeight = win.displayHeight * 0.99;

        /* if (this.background.displayHeight<this.gh)
        {
            Align.scaleToGameH(this.background,1.1,this);
        } */
        Align.center(this.background, this);
        Align.scaleToGameW(this.butterfly, 0.1, this);

        this.redrawAllLines();
    }
    checkLines() {
        if (this.connections.length==0)
        {
            return false;
        }
        for (let i: number = 0; i < this.connections.length; i++) {
            if (this.connections[i].box1.index !== this.connections[i].box2.index) {
                return false;
            }
        }
        return true;
    }
    doButtonAction(action: string, params: string) {
        if (action === "check") {
            let win: boolean = this.checkLines();
            if (win === true) {
                this.playSound("rightSound");
                this.stars.start();
            }
            else {
                this.playSound("wrongSound");
            }
        }
        if (action === "reset") {
            this.resetGame();
        }
    }
    playSound(key: string) {
        let sound: Phaser.Sound.BaseSound = this.sound.add(key);
        sound.play();
    }
    resetGame() {
        this.connections = [];
        this.boxLines.clear();


        for (let i: number = 0; i < this.boxes1.length; i++) {
            let box1: WordBox = this.boxes1[i];
            let box2: WordBox = this.boxes2[i];

            box1.setInteractive();
            box2.setInteractive();

            box1.setBackColor(0x3498db);
            box2.setBackColor(0x2ecc71);

        }
      
    }
    reset() {
        this.downBox = null;
        this.upBox = null;
        this.graphics.clear();
        this.flyTo(this.gw * 0.1, this.gh * 0.1);

    }
    compare() {
        if (this.upBox && this.downBox) {
            //   this.upBox.alpha = 0.5;
            //  this.downBox.alpha = 0.5;
            if (this.upBox.col === this.downBox.col) {
                this.reset();
                return;
            }
            /*   if (this.upBox.index !== this.downBox.index) {
                  //console.log("wrong");
                  //
                  //
                  //
                  let wrongSound:Phaser.Sound.BaseSound=this.sound.add("wrongSound");
                  wrongSound.play();
                  //
                  //
                  //
                  this.reset();
                  return;
              } */

            /*  let rightSound:Phaser.Sound.BaseSound=this.sound.add("rightSound");
                 rightSound.play(); */

            this.playSound("pop");

            this.upBox.setBackColor(0xf1c40f);
            this.downBox.setBackColor(0xf1c40f);
            //
            //
            //
            this.boxLines.moveTo(this.downBox.x, this.downBox.y);
            this.boxLines.lineTo(this.upBox.x, this.upBox.y);
            this.boxLines.strokePath();
            this.graphics.clear();

            this.connections.push(new ConVo(this.downBox, this.upBox));

            this.downBox.removeInteractive();
            this.upBox.removeInteractive();
            this.reset();
        }
        else {
            this.reset();
        }
    }
    private followMouse() {
        let distX: number = Math.abs(this.input.x - this.butterfly.x);
        let distY: number = Math.abs(this.input.y - this.butterfly.y);

        let dist: number = Math.sqrt((distX * distX) + (distY * distY));

        if (dist < this.gw * 0.1) {
            this.butterfly.x = this.input.x;
            this.butterfly.y = this.input.y;
        }
        else {
            this.butterfly.play("fly");
            if (this.butterfly.x < this.input.x) {
                this.butterfly.x += 10;
            }
            else {
                this.butterfly.x -= 10;
            }
            if (this.butterfly.y < this.input.y) {
                this.butterfly.y += 10;
            }
            else {
                this.butterfly.y -= 10;
            }

        }
    }
    private drawLine() {
        if (this.downBox) {
            this.graphics.clear();
            this.graphics.lineStyle(4, 0xff0000, 1);
            this.graphics.moveTo(this.downBox.x, this.downBox.y);
            this.graphics.lineTo(this.input.x, this.input.y);
            this.graphics.strokePath();
        }

    }
    private redrawAllLines() {
        this.boxLines.clear();
        this.boxLines.lineStyle(4, 0xff0000, 1);
        //console.log(this.connections);

        for (let i: number = 0; i < this.connections.length; i++) {
            let box1: WordBox = this.connections[i].box1;
            let box2: WordBox = this.connections[i].box2;

            this.boxLines.moveTo(box1.x, box1.y);
            this.boxLines.lineTo(box2.x, box2.y);

        }
        this.boxLines.strokePath();
    }
    private makeBoxes() {
        for (let i: number = 0; i < this.words.length; i++) {
            let pairVo: PairVo = this.words[i];

            let box1: WordBox = new WordBox(this, pairVo.word1);
            let box2: WordBox = new WordBox(this, pairVo.word2);
            //
            //
            //
            box1.setBackColor(0x3498db);
            box2.setBackColor(0x2ecc71);
            //
            //
            //
            box1.index = i;
            box2.index = i;
            //
            //
            //
            box1.col = this.col1;
            box2.col = this.col2;
            //
            //
            //
            this.boxes1.push(box1);
            this.boxes2.push(box2);
            //
            //
            //

            let row: number = 3 + i * 1.5;

            this.grid.placeAt(this.col1, row, box1);
            this.grid.placeAt(this.col2, row, box2);

            box1.row = row;
            box2.row = row;
        }
    }
    mixUp(boxes: WordBox[]) {
        for (let i: number = 0; i < 20; i++) {
            let r: number = Math.floor(Math.random() * boxes.length);
            let r2: number = Math.floor(Math.random() * boxes.length);

            /* let pos1: PosVo = new PosVo(boxes[r].x, boxes[r].y);
            let pos2: PosVo = new PosVo(boxes[r2].x, boxes[r2].y);

            boxes[r2].x = pos1.x;
            boxes[r2].y = pos1.y;

            boxes[r].x = pos2.x;
            boxes[r].y = pos2.y; */

            let temp: number = boxes[r].row;
            boxes[r].row = boxes[r2].row;
            boxes[r2].row = temp;

            boxes[r].doResize();
            boxes[r2].doResize();
        }
        //  boxes.sort((a: WordBox, b: WordBox) => { return (a.y > b.y) ? 1 : -1 })
    }

    update() {
        if (this.pointerIsDown && this.downBox !== null) {
            this.drawLine();
            this.followMouse();
        }
    }
}