export class PairVo {
    public word1: string;
    public word2: string;
    constructor(word1: string, word2: string) {
        this.word1 = word1;
        this.word2 = word2;
    }
}