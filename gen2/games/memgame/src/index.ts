
import { GameBoard } from "./classes/gameBoard";

window.onload = function () {

    const btnStart: HTMLElement = document.querySelector(".btnStart") as HTMLElement;
    btnStart.addEventListener("click", ()=>{
        const game:GameBoard= new GameBoard();
        btnStart.style.display = "none";
    });
   
}