import gsap from "gsap";
import { CardVo } from "./CardVo";
import { WordVo } from "./WordVo";
export class GameBoard {
    private board: HTMLElement;
    private words: WordVo[] = [];
    private cardWords: string[] = [];
    private cards: CardVo[] = [];
    private clickLock: boolean = true;
    private firstCard: CardVo | null = null;
    private secondCard: CardVo | null = null;
    private matches: number = 0;
    constructor() {
        this.board = document.querySelector(".gameBoard") as HTMLElement;
        this.loadData();
        (window as any).game = this;
    }
    loadData() {
        fetch("./assets/opposites.json")
            .then(response => response.json())
            .then(data => this.dataLoaded(data));
    }
    dataLoaded(data: any) {
       // //console.log(data);
        //turn the data.words into WordVo objects
        for (let i = 0; i < data.words.length; i++) {
            const word = data.words[i];
         //   //console.log(word);
            const wordVo = new WordVo(word.word1, word.word2);
            this.words.push(wordVo);
        }
        this.words = this.shuffle(this.words);

        for (let i: number = 0; i < 12; i++) {
            this.cardWords.push(this.words[i].word1);
            this.cardWords.push(this.words[i].word2);
        }
        this.cardWords = this.shuffle(this.cardWords);
        this.makeCards();
        this.flyAllCardsToPosition();

        setTimeout(() => {
            this.flipOverAllCards();
        }, 4000);
        setTimeout(() => {
            this.flipOverAllCardsBack();
        }, 6000);
        setTimeout(() => {
            this.clickLock = false;
        }, 6500);
        this.playSound("cardShuffle.wav");
    }
    private shuffle(array: any[]) {

        for (let i: number = 0; i < 30; i++) {
            let index1 = Math.floor(Math.random() * array.length);
            let index2 = Math.floor(Math.random() * array.length);
            let temp = array[index1];
            array[index1] = array[index2];
            array[index2] = temp;
        }
        return array;
    }

    public makeCards() {
        let row: number = 0;
        let col: number = 0;
        for (let i = 0; i < 20; i++) {
            const card = document.createElement("div");
            card.classList.add("gameCard");
            card.innerHTML = "<div class='front'></div><div class='back'>" + this.cardWords[i] + "</div>"

            let cardVo: CardVo = new CardVo(card, row, col, this.cardWords[i]);
            this.cards.push(cardVo);
            card.addEventListener("click", () => this.clickCard(cardVo));
            this.board.appendChild(card);

            col++;
            if (col > 5) {
                col = 0;
                row++;
            }

        }
    }
    public clickCard(card: CardVo) {
        if (this.clickLock == true) {
            return;
        }
        this.flipCard(card.element);
        this.playSound("cardFlip.mp3");
        if (this.firstCard == null) {
            this.firstCard = card;
            
            return;
        }
        this.clickLock = true;       

        this.secondCard = card;
       // this.alphaCards();

        let word1 = this.firstCard.value;
        let word2 = this.secondCard.value;
        if (this.checkWord(word1, word2) == true || this.checkWord(word2, word1) == true) {
            //console.log("match");
            this.playSound("quiz_right.wav");
            this.firstCard = null;
            this.secondCard = null;
            this.clickLock = false;
           /*  setTimeout(() => {
                this.unAlphaCards();
            }, 1000); */
           
            this.matches++;
            if (this.matches == 10) {
                setTimeout(() => {
                    alert("you win");
                }, 2000);
              
            }
            return;
        }
        else {
            //console.log("no match");
         //   this.playSound("quiz_wrong.wav");
            setTimeout(() => {
                if (this.firstCard != null || this.secondCard != null) {
                    if (this.firstCard?.element != null && this.secondCard?.element != null) {
                        this.flipCardBack(this.firstCard.element);
                        this.flipCardBack(this.secondCard.element,()=>{
                            this.firstCard = null;
                            this.secondCard = null;
                            this.clickLock = false;
                           // this.unAlphaCards();
                            //alert("no match");
                        });
                    }
                }
            }, 1000);

        }
    }
    public checkWord(word1: string, word2: string) {
        for (let i = 0; i < this.words.length; i++) {
            const word = this.words[i];
            //console.log(word.word1, word.word2, word1, word2);
            if (word.word1 == word1 && word.word2 == word2) {
                return true;
            }
        }
        return false;
    }
    public flipCard(card: HTMLElement,callback:Function=()=>{}) {

        gsap.set(card, {
            transformStyle: "preserve-3d",
            transformPerspective: 1000
        });
        const q = gsap.utils.selector(card);
        const front = q(".front");
        const back = q(".back");

        gsap.set(back, { rotationY: -180 });


        const tl = gsap.timeline({ paused: false })
            .to(front, { duration: 1, rotationY: 180 })
            .to(back, { duration: 1, rotationY: 0 }, 0)
            .to(card, { z: 50 }, 0)
            .to(card, { z: 0 }, 0.5);
            tl.eventCallback("onComplete", ()=>{
                callback();
            });

    }
    public flipCardBack(card: HTMLElement,callback:Function=()=>{}) {
        //console.log(card);
        gsap.set(card, {
            transformStyle: "preserve-3d",
            transformPerspective: 1000
        });
        const q = gsap.utils.selector(card);
        const front = q(".front");
        const back = q(".back");

        gsap.set(back, { rotationY: 0 });

        const tl = gsap.timeline({ paused: false })
            .to(front, { duration: 1, rotationY: 0 })
            .to(back, { duration: 1, rotationY: -180 }, 0)
            .to(card, { z: 50 }, 0)
            .to(card, { z: 0 }, 0.5);

        tl.eventCallback("onComplete", ()=>{
            callback();
        });
    }
    public flipOverAllCards() {
        for (let i = 0; i < this.cards.length; i++) {
            const card = this.cards[i];
            this.flipCard(card.element);
        }
    }
    public flipOverAllCardsBack() {
        for (let i = 0; i < this.cards.length; i++) {
            const card = this.cards[i];
            this.flipCardBack(card.element);
        }
    }
    public flyAllCardsToPosition() {
        let tl: gsap.core.Timeline = gsap.timeline({ paused: true });
        for (let i = 0; i < this.cards.length; i++) {
            const card = this.cards[i];
            const element: HTMLElement = card.element;
            gsap.set(element, { x: -1000 * (card.col + 1), opacity: 0 });
            //this.flyCardToPosition(card);
            tl.to(element, { duration: 1, x: 0, y: 0, opacity: 1 }, i * 0.1);
        }
        tl.play();
    }
    public flyCardToPosition(card: CardVo) {
        //get the current position of the card
        let element: HTMLElement = card.element;

        //set the card x position to zero
        gsap.set(element, { x: -1000 * card.col, opacity: 0 });

        //fly the card to the x,y position
        gsap.to(element, { duration: 1, x: 0, y: 0, opacity: 1 });

    }
    public alphaCards() {
        for (let i = 0; i < this.cards.length; i++) {
            const card = this.cards[i];
            if (card!=this.firstCard && card!=this.secondCard)
            {
                gsap.set(card.element, { duration: 1, opacity: 0.7 });
            }
           
        }
    }
    public unAlphaCards() {
        for (let i = 0; i < this.cards.length; i++) {
            const card = this.cards[i];
            gsap.set(card.element, { duration: .5, opacity: 1 });
        }
    }
    playSound(soundPath:string)
    {
        soundPath="./assets/"+soundPath;
        let audio = new Audio(soundPath);
        audio.play();

    }
}