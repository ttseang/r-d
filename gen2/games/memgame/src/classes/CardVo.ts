export class CardVo
{
    public element:HTMLElement;
    public row:number;
    public col:number;
    public value:string;
    constructor(element:HTMLElement,row:number, col:number, value:string,flipped:boolean = false)
    {
        this.element = element;
        this.row = row;
        this.col = col;
        this.value = value;
    }
}