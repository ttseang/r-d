import gsap from "gsap";
import Draggable from "gsap/Draggable";


export class CircleGame
{
    private picrotator:HTMLElement=document.querySelector(".picrotator") as HTMLElement;
    private ringCount:number=10;
    private ringArray:HTMLElement[]=[];
    constructor()
    {
        console.log(this.picrotator);
        gsap.registerPlugin(Draggable);
        this.picrotator.style.setProperty("--ringcount",this.ringCount.toString());

        let picIndex:number=Math.floor(Math.random()*17);
        if (picIndex>0)
        {
            this.picrotator.style.setProperty("--picurl","url(../assets/pic"+picIndex.toString()+".png)");
        }
        else
        {
            this.picrotator.style.setProperty("--picurl","url(../assets/pic"+picIndex.toString()+".jpg)");
        }

        this.makeRings();
       
        this.addListeners();
    }
    makeRings()
    {
        
        for(let i=0;i<this.ringCount;i++)
        {
            
            let ring:HTMLElement=document.createElement("figure");

            let ringIndex2:number=i+1;
            let ringClass:string="ring"+ringIndex2.toString();
            ring.classList.add(ringClass);

            let ringIndex:number=this.ringCount-i;
            ring.style.setProperty("--ring",ringIndex2.toString());
            this.picrotator.appendChild(ring);

            ring.style.transform = "rotate(" + Math.random() * 360 + "deg)";
            this.ringArray.push(ring);
        }
        this.fixCircles();
    }
  
    addListeners()
    {
        for(let i=0;i<this.ringCount;i++)
        {
            let ring:HTMLElement=this.ringArray[i];
            let drag = Draggable.create(ring, { inertia: true, type: "rotation", bounds: "body" })[0];
             drag.addEventListener("dragend", this.fixCircles.bind(this));
        }
    }
    fixCircles()
    {
        
        for (let i:number=0;i<this.ringCount;i++)
        {
            let ring:HTMLElement=this.ringArray[i];
            let transformString:string=ring.style.transform;
            let rotateString:string=transformString.substring(transformString.indexOf("rotate(")+7,transformString.indexOf("deg)"));
          
            let rotateNumber:number=Number(rotateString);
            let rotateNumber2:number=rotateNumber%360;
           
            let rotateNumber3:number=Math.round(rotateNumber2/15)*15;
            ring.style.transform = "rotate(" + rotateNumber3 + "deg)";
        }
        if (this.checkWin())
        {
           /*  alert("You win!"); */
            for (let i:number=0;i<this.ringCount;i++)
            {
                let ring:HTMLElement=this.ringArray[i];
                ring.classList.add("noBorder");
            }
        }
    }
    setPercentage(rightCount:number)
    {
        let percentage:number=Math.round(rightCount/this.ringCount*100);
        let percentageString:string=percentage.toString()+"%";
        let percentageDiv:HTMLElement=document.querySelector(".percentage") as HTMLElement;
        percentageDiv.innerHTML=percentageString;
    }
    checkWin()
    {
       
        let rightCount:number=0;
        for (let i:number=0;i<this.ringCount;i++)
        {
            let ring:HTMLElement=this.ringArray[i];
            let transformString:string=ring.style.transform;
            let rotateString:string=transformString.substring(transformString.indexOf("rotate(")+7,transformString.indexOf("deg)"));
          
            let rotateNumber:number=Number(rotateString);
            let rotateNumber2:number=rotateNumber%360;
           
            let rotateNumber3:number=Math.round(rotateNumber2/15)*15;
            if (rotateNumber3==0)
            {
                rightCount++;
            }
        }
        this.setPercentage(rightCount);
        if (rightCount==this.ringCount)
        {
            return true;
        }
        return false;
    }
}