import { GameObjects } from "phaser";
import { Align, BaseComp, IBaseScene, PosVo } from "../../../../common/mods/ttcomps";


export class Card extends Phaser.GameObjects.Sprite
{
    
    public scene:Phaser.Scene;
    public bscene:IBaseScene;
    public origin:PosVo=new PosVo(0,0);
    public pos:PosVo=new PosVo(0,0);

    constructor(bscene:IBaseScene,key:string,frame:string,pos:PosVo)
    {
        super(bscene.getScene(),0,0,key,frame);
        this.bscene=bscene;
        
        this.scene=bscene.getScene();
        this.origin=pos;
        this.pos=pos;
       
        this.scene.add.existing(this);
    }
    doResize()
    {
        this.bscene.getGrid().placeAt(this.pos.x,this.pos.y,this);
      //  Align.scaleToGameW(this,0.15,this.bscene);
       // this.displayWidth=this.bscene.cw*1.5;
        this.displayHeight=this.bscene.ch*2.25;
        this.scaleX=this.scaleY;
    }
    /* init()
    {
        this.origin=new PosVo(this.x,this.y);
    } */
    inRightPlace()
    {
        if (this.pos.x==this.origin.x && this.pos.y==this.origin.y)
        {
            return true;
        }
        return false;
    }
}