
import { StarBurst } from "ttgameeffects";
import { CompLayout, CompManager, ButtonController, CompLoader, PosVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { Card } from "../classes/card";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout:CompLayout;
    private cm:CompManager=CompManager.getInstance();
    private buttonController:ButtonController=ButtonController.getInstance();
    private compLoader:CompLoader;
    
    private cardArray: Card[] = [];
    private selectedCard: Card | null = null;
    private selectBox: Phaser.GameObjects.Image;
    private clickLock: Boolean = false;
    private starBurst:StarBurst;


    constructor() {
        super("SceneMain");
        this.compLoader=new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {

        StarBurst.preload(this,"./assets/effects/stars.png");

         this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("qmark","./assets/questionmark.png");
        this.load.image("right","./assets/right.png");
        this.load.image("wrong","./assets/wrong.png");
        this.load.image("triangle","./assets/triangle.png");
        //
        //
        //
        this.load.atlas("story1", "./assets/story1.png", "./assets/story1.json");
        this.load.image("background","./assets/background.jpg");
        this.load.audio("swapdone","./assets/swapdone.mp3");
        this.load.audio("tap","./assets/tap.mp3");
        this.load.audio("win","./assets/win.mp3");
        this.load.audio("wrongSound","./assets/quiz_wrong.wav");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.cm.regFontSize("list",30,1200);
        this.cm.regFontSize("words",40,1200);

        this.compLayout=new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");     

        this.starBurst=new StarBurst(this,this.gw/2,this.gh/2);
        this.starBurst.count=100;
        this.starBurst.tint=0xff0000;
        
        this.buttonController.callback=this.doButtonAction.bind(this);
    }
    compsLoaded()
    {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize=this.doResize.bind(this);

       // this.loadGameData();
       this.buildGame();
    }
    buildGame()
    {
        let startX:number=3.5;
        let startY:number=4;

        let posArray: PosVo[] = [new PosVo(0,0),new PosVo(1.5,0),new PosVo(3,0),new PosVo(0,2.5),new PosVo(1.5,2.5),new PosVo(3,2.5)];

        for (let i: number = 1; i < 7; i++) {
            let f: string = i.toString() + ".jpg";
            // console.log(f);
            let pos:PosVo=posArray[i-1];
            pos.x+=startX;
            pos.y+=startY;

            let card: Card = new Card(this, "story1", f,pos);
            this.cardArray.push(card);
            card.setInteractive();
            card.doResize();
            //card.alpha=0.5;
          //  this.grid.placeAt(posArray[i - 1].x, posArray[i - 1].y, card);
           
        }

        this.selectBox = this.add.image(0, 0, "holder");
        this.selectBox.setTint(0xffcc00);
        this.selectBox.visible = false;

        this.cm.getComp("wrong").visible=false;     
        this.cm.getComp("right").visible=false;     

        this.mixUp();

        this.input.on("gameobjectdown",this.selectCard.bind(this));


    }
    mixUp() {
        for (let i: number = 0; i < 50; i++) {
            let r1: number = Math.floor(Math.random() * this.cardArray.length);
            let r2: number = Math.floor(Math.random() * this.cardArray.length);

            let card1: Card = this.cardArray[r1];
            let card2: Card = this.cardArray[r2];

            let pos1: PosVo = card1.pos;
            let pos2: PosVo = card2.pos;

            card1.pos=pos2;
            card2.pos=pos1;

            let x1:number=card1.x;
            let y1:number=card1.y;

            let x2:number=card2.x;
            let y2:number=card2.y;

           /*  card2.x = x1;
            card2.y = y1;

            card1.x = pos2.x;
            card1.y = pos2.y; */

        }
        this.doResize();
    }
    selectCard(p: Phaser.Input.Pointer, obj: Card) {
        console.log("click");
        if (obj instanceof(Card)==false)
        {
            return;
        }
        if (this.clickLock == true) {
            return;
        }
        if (this.selectedCard == obj) {
            this.selectBox.visible = false;
            this.selectedCard = null;
            return;
        }
        this.playSound("tap");

        this.selectBox.x = obj.x;
        this.selectBox.y = obj.y;
        this.selectBox.displayWidth = obj.displayWidth * 1.1;
        this.selectBox.displayHeight = obj.displayHeight * 1.1;
        
        this.selectBox.visible = true;

        this.children.bringToTop(this.selectBox);
        this.children.bringToTop(obj);
       

        if (this.selectedCard === null) {
            this.selectedCard = obj;
            this.clickLock=false;
        }
        else {
            this.selectBox.visible=false;
            this.swapCards(this.selectedCard, obj);
        }
    }
    swapCards(card1: Card, card2: Card) {

           
        
        this.selectedCard=null;
        let pos1: PosVo = new PosVo(card1.x, card1.y);
        let pos2: PosVo = new PosVo(card2.x, card2.y);

        let cardPos1:PosVo=card1.pos;
        let cardPos2:PosVo=card2.pos;

        card1.pos=cardPos2;
        card2.pos=cardPos1;

        this.tweens.add({targets: card1,duration: 500,y:pos2.y,x:pos2.x});
        this.tweens.add({targets: card2,duration: 500,y:pos1.y,x:pos1.x});
        setTimeout(() => {
            this.swapDone();
        }, 600);
    }
    swapDone()
    {
       this.playSound("swapdone");

        
    }
    checkForWin()
    {
        if (this.checkWin()==false)
        {
            this.clickLock=false;
            this.playSound("wrongSound");
            this.cm.getComp("wrong").visible=true;     
            this.cm.getComp("right").visible=false;      
            setTimeout(() => {
                this.resetIcons();
            }, 1000);
            
        }
        else
        {
            this.cm.getComp("wrong").visible=false;
            this.cm.getComp("right").visible=true;

            console.log("WIN!");
            this.clickLock=true;
            this.playSound("win");
            this.starBurst.x=this.gw/2;
            this.starBurst.y=this.gh/2;

            this.starBurst.start();
        }
    }
    checkWin()
    {
        for (let i:number=0;i<this.cardArray.length;i++)
        {
            let card:Card=this.cardArray[i];
            if (card.inRightPlace()==false)
            {
                return false;
            }
        }
        return true;
    }
    playSound(soundKey:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(soundKey);
        sound.play();
    }
    doResize()
    {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();
        this.cm.doResize();
        for (let i:number=0;i<this.cardArray.length;i++)
        {
            this.cardArray[i].doResize();
        }
    }
    resetIcons()
    {
        this.cm.getComp("wrong").visible=false;     
        this.cm.getComp("right").visible=false;  
    }
    doButtonAction(action:string,params:string)
    {
        if (action==="check")
        {
            this.checkForWin();
        }
    }

}