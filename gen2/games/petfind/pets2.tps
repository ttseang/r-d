<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>public/assets/pets2.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (1).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,88,135,175</rect>
                <key>scale9Paddings</key>
                <rect>67,88,135,175</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (10).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,84,149,169</rect>
                <key>scale9Paddings</key>
                <rect>75,84,149,169</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (11).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,86,131,171</rect>
                <key>scale9Paddings</key>
                <rect>66,86,131,171</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (12).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,79,121,159</rect>
                <key>scale9Paddings</key>
                <rect>61,79,121,159</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (13).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,63,109,125</rect>
                <key>scale9Paddings</key>
                <rect>55,63,109,125</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (14).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,65,105,129</rect>
                <key>scale9Paddings</key>
                <rect>52,65,105,129</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (15).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,74,104,147</rect>
                <key>scale9Paddings</key>
                <rect>52,74,104,147</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (16).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,60,100,119</rect>
                <key>scale9Paddings</key>
                <rect>50,60,100,119</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (17).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,64,91,128</rect>
                <key>scale9Paddings</key>
                <rect>46,64,91,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (18).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,74,99,149</rect>
                <key>scale9Paddings</key>
                <rect>50,74,99,149</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (19).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,66,121,131</rect>
                <key>scale9Paddings</key>
                <rect>61,66,121,131</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (2).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,75,103,151</rect>
                <key>scale9Paddings</key>
                <rect>52,75,103,151</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (20).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,73,123,146</rect>
                <key>scale9Paddings</key>
                <rect>61,73,123,146</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (21).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,70,95,141</rect>
                <key>scale9Paddings</key>
                <rect>47,70,95,141</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (22).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,83,140,167</rect>
                <key>scale9Paddings</key>
                <rect>70,83,140,167</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (23).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,78,118,155</rect>
                <key>scale9Paddings</key>
                <rect>59,78,118,155</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (24).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,67,102,133</rect>
                <key>scale9Paddings</key>
                <rect>51,67,102,133</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (25).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,76,111,151</rect>
                <key>scale9Paddings</key>
                <rect>55,76,111,151</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (26).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,72,94,145</rect>
                <key>scale9Paddings</key>
                <rect>47,72,94,145</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (27).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,62,105,125</rect>
                <key>scale9Paddings</key>
                <rect>52,62,105,125</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (28).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>57,76,113,152</rect>
                <key>scale9Paddings</key>
                <rect>57,76,113,152</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (29).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,80,101,160</rect>
                <key>scale9Paddings</key>
                <rect>50,80,101,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (3).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,63,101,125</rect>
                <key>scale9Paddings</key>
                <rect>50,63,101,125</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (30).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,105,97,209</rect>
                <key>scale9Paddings</key>
                <rect>49,105,97,209</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (31).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,91,135,182</rect>
                <key>scale9Paddings</key>
                <rect>67,91,135,182</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (32).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>72,70,144,139</rect>
                <key>scale9Paddings</key>
                <rect>72,70,144,139</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (33).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,66,110,132</rect>
                <key>scale9Paddings</key>
                <rect>55,66,110,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (4).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,74,94,148</rect>
                <key>scale9Paddings</key>
                <rect>47,74,94,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (5).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,71,105,142</rect>
                <key>scale9Paddings</key>
                <rect>52,71,105,142</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (6).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,72,106,143</rect>
                <key>scale9Paddings</key>
                <rect>53,72,106,143</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (7).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,87,148,173</rect>
                <key>scale9Paddings</key>
                <rect>74,87,148,173</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (8).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,83,133,165</rect>
                <key>scale9Paddings</key>
                <rect>66,83,133,165</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (9).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,77,140,153</rect>
                <key>scale9Paddings</key>
                <rect>70,77,140,153</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (1).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (10).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (11).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (12).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (13).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (14).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (15).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (16).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (17).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (18).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (19).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (2).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (20).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (21).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (22).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (23).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (24).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (25).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (26).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (27).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (28).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (29).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (3).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (30).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (31).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (32).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (33).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (4).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (5).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (6).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (7).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (8).png</filename>
            <filename>../../../resource/different-type-vector-cartoon-cats-dogs-design/1x/pet (9).png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
