import { Align, IBaseScene, PosVo } from "ttphasercomps";

export class FImage extends Phaser.GameObjects.Sprite
{
    private bscene:IBaseScene;
    public wscale:number=0.1;
    public posVo:PosVo=new PosVo(0,0);
    public key:string="";
    public frameIndex:number=0;
    public nextImage:FImage;
    public myDepth:number=0;
   

    public frameName:string;

    constructor(bscene:IBaseScene,imageKey:string)
    {
        super(bscene.getScene(),0,0,imageKey);
        this.bscene=bscene;
        this.key=imageKey;

        this.scene.add.existing(this);
    }
    setMyFrame(frameName:string)
    {
        this.frameName=frameName;
        this.setFrame(frameName);
    }
    scaleMe(p:number)
    {
        this.wscale=p;
        Align.scaleToGameW(this,p,this.bscene);
    }
    findPlace()
    {        
        this.posVo=this.bscene.getGrid().findNearestGridXYDec(this.x,this.y);
    }
    checkMe(frameName:string)
    {
        if (frameName!==this.frameName)
        {
            this.visible=false;
        }
        if (this.nextImage)
        {
            this.nextImage.checkMe(frameName);
        }
    }
    doResize()
    {
        Align.scaleToGameW(this,this.wscale,this.bscene);
        this.bscene.getGrid().placeAt(this.posVo.x,this.posVo.y,this);
        if (this.nextImage)
        {
            this.nextImage.doResize();
        }
    }
    setPos(xx:number,yy:number)
    {
        this.posVo=new PosVo(xx,yy);
        this.bscene.getGrid().placeAt(xx,yy,this);
    }
}