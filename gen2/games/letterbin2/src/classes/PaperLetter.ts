import { CompManager, IBaseScene, TextStyleVo } from 'ttphasercomps';
export class PaperLetter extends Phaser.GameObjects.Container {
    private bscene: IBaseScene;
    private back: Phaser.GameObjects.Image;
    private text1: Phaser.GameObjects.Text;
    private cm:CompManager=CompManager.getInstance();
    private tsv:TextStyleVo;
    private hh:number=0;
    private ww:number=0;
    public dtype:string="paper";
    public index:number=0;

    private ox:number=0;
    private oy:number=0;

    public letter:string;

    constructor(bscene: IBaseScene,h:number,letter:string,textStyle:string) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.back = this.scene.add.image(0, 0, 'tpaper');
        this.tsv=this.cm.getTextStyle(textStyle);

        this.letter=letter;

        this.hh=h;
        this.ww=h/1.6;

        this.back.displayWidth=this.ww;
        this.back.displayHeight=this.hh;

        this.text1=this.scene.add.text(0,0,letter,{fontFamily:this.tsv.fontName,color:this.tsv.textColor}).setOrigin(0.5,0.5);
        this.add(this.back);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        this.scene.add.existing(this);

        this.doResize(h);
    }
    doResize(hh:number)
    {
        this.hh=hh;
        this.ww=hh/1.6;

        this.back.displayWidth=this.ww;
        this.back.displayHeight=this.hh;

        this.text1.setFontSize(this.cm.getFontSize("word",this.bscene.getW()))

        //this.sizeText();
    }
    initPos()
    {
        this.ox=this.x;
        this.oy=this.y;
    }
    resetPos()
    {
        this.x=this.ox;
        this.y=this.oy;
    }
    public setText(text:string)
    {
        this.letter=text;
        this.text1.setText(text);
        //this.sizeText();
    }
    /* sizeText()
    {
        this.text1.setFontSize(this.tsv.maxFontSize);
        let fs:number=this.tsv.maxFontSize;

        while(this.text1.displayWidth>this.ww)
        {
            fs--;
            this.text1.setFontSize(fs);
        }
    } */
    
}