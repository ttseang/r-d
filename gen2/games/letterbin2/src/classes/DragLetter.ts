
import { CompManager, IBaseScene, TextStyleVo } from "ttphasercomps";

export class DragLetter extends Phaser.GameObjects.Container
{
    private bscene:IBaseScene;
    private back:Phaser.GameObjects.Graphics;
    private text1:Phaser.GameObjects.Text;
    private cm:CompManager=CompManager.getInstance();
    private tsv:TextStyleVo;
    private hh:number=0;
    private ww:number=0;
    public dtype:string="letter";
    public index:number=0;

    public letter:string;

    private ox:number=0;
    private oy:number=0;

    constructor(bscene:IBaseScene,hh:number,letter:string,textStyle:string)
    {
        super(bscene.getScene());
        this.bscene=bscene;

        this.letter=letter;

        this.hh=hh;
        this.ww=hh/1.5;

        this.back=this.scene.add.graphics();
        this.back.lineStyle(2,0x000000);
        this.back.fillStyle(0xcccccc,1);
        this.back.fillRoundedRect(-this.ww/2,-this.hh/2,this.ww,hh,8);
        
        this.tsv=this.cm.getTextStyle(textStyle);
        
        this.text1=this.scene.add.text(0,0,letter,{fontFamily:this.tsv.fontName,color:this.tsv.textColor}).setOrigin(0.5,0.5);
        this.text1.setFontSize(this.cm.getFontSize("word",this.bscene.getW()));
        
        this.add(this.back);
        this.add(this.text1);

       // this.sizeText();

        this.setSize(this.ww,this.hh);

        this.scene.add.existing(this);

        //this.doResize(this.hh);
    }
    initPos()
    {
        this.ox=this.x;
        this.oy=this.y;
    }
    resetPos()
    {
        this.x=this.ox;
        this.y=this.oy;
    }
    setText(text:string)
    {
        this.letter=text;
        this.text1.setText(text);
      //  this.sizeText();
    }
    snapBack()
    {
        let tween:Phaser.Tweens.Tween=this.scene.add.tween({targets:[this],duration:200,x:this.ox,y:this.oy});
    }
    /* sizeText()
    {
        this.text1.setFontSize(this.tsv.maxFontSize);
        let fs:number=this.tsv.maxFontSize;

        while(this.text1.displayWidth>this.ww)
        {
            fs--;
            this.text1.setFontSize(fs);
        }
      //  this.text1.x=-this.ww/2;
      //  this.text1.y=this.hh/2;
    } */
    doResize(hh:number)
    {
        this.hh=hh;

        this.hh=hh;
        this.ww=hh/1.5;

        this.back.clear();
        this.back.lineStyle(2,0x000000);
        this.back.fillStyle(0xcccccc,1);
        this.back.fillRoundedRect(-this.ww/2,-this.hh/2,this.ww,hh,8);

        this.text1.setFontSize(this.cm.getFontSize("word",this.bscene.getW()));

       // this.sizeText();
        this.setSize(this.ww,this.hh);
    }

}