import { BaseComp, CompBack, IBaseScene } from "ttphasercomps";
import { BaseScene } from "../scenes/BaseScene";

export class LetterPost extends BaseComp
{
    private back:CompBack;
    private h:number;
    public index:number=0;

    constructor(bscene:IBaseScene,h:number,key:string)
    {
        super(bscene,key);
        this.bscene=bscene;
        this.h=h;

        this.back=new CompBack(this.bscene,h/1.5,h,this.cm.getBackStyle("bluewhite"));

        this.add(this.back);

        this.setSize(this.h/1.5,this.h);
        this.scene.add.existing(this);
    }   
    doResize2(hh:number)
    {
        this.h=hh;
        this.back.doResize(this.h/1.5,this.h);
        super.doResize();

    }
}