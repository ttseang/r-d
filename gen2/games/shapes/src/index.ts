import { ShapeGame } from "./classes/ShapeGame";
import { AddBar } from "./classes/AddBar";
import { ToolBar } from "./classes/ToolBar";

window.onload=function()
{
   const shapeGame: ShapeGame = new ShapeGame(); 
   const addBar:AddBar=new AddBar(shapeGame);
   const toolbar:ToolBar=new ToolBar(shapeGame);
}