export class Shapes
{
    static shapeNames: string[] = ["hex", "prl", "rhm", "sqr", "trg", "trz"];
    
    public static classToShape(className: string): string {
       
        let index: number = Shapes.shapeNames.indexOf(className);
        if (index == -1) {
            console.error(`Shape ${className} not found`);
            return "";
        }
        return Shapes.shapeNames[index];

    }
    public static hrefToShapeIndex(href: string): number {
        console.log(href);
        for (let i: number = 0; i < Shapes.shapeNames.length; i++) {
            if (href.includes(Shapes.shapeNames[i])) {
                return i;
            }
        }
        return -1;
    }
    public static indexToShape(index: number): string {
        if (index < 0 || index >= Shapes.shapeNames.length) {
            console.error(`Index ${index} out of range`);
            return "";
        }
        return Shapes.shapeNames[index];
    }
    public static shapeToIndex(shape: string): number {
        let index: number = Shapes.shapeNames.indexOf(shape);
        if (index == -1) {
            console.error(`Shape ${shape} not found`);
            return -1;
        }
        return index;
    }
}