import { Shapes } from "./Shapes";
import { SVGVo } from "./SVGVo";

export class ShapeGame {
    private main: SVGSVGElement;
    private pattern:SVGSVGElement;
    private board: SVGGElement;
    private defs: SVGDefsElement;
    //  private shapeNames: string[] = ["hexagon", "parallelogram", "rhombus", "square", "triangle", "trapezoid"];
    private currentShape: SVGPolygonElement | null = null;
    private shapeCount: number = 0;

    private mode: string = "move";

    constructor() {
        (window as any).shapeGame = this;
        this.main = document.querySelector("svg") as unknown as SVGSVGElement;
        this.defs = document.getElementById("defs") as unknown as SVGDefsElement;
        this.board = document.getElementById("board") as unknown as SVGGElement;
        this.pattern = document.getElementById("pattern") as unknown as SVGSVGElement;

        //on mouse move event
        document.addEventListener("mousemove", this.onMouseMove.bind(this));
        //on mouse up event set current shape to null
        document.addEventListener("mouseup", this.onMouseUp.bind(this));
       this.loadJson("./assets/puzzles/soccer.json");
    }
    public setMode(mode: string) {
        this.mode = mode;
        //change the .dragShape class cursor to move

        let dragShapes: NodeListOf<SVGGElement> = document.querySelectorAll(".dragShape");
        for (let i: number = 0; i < dragShapes.length; i++) {
            dragShapes[i].classList.remove("moveShape");
            dragShapes[i].classList.remove("rotateShape");
            dragShapes[i].classList.remove("delShape");

            switch (this.mode) {
                case "move":
                    dragShapes[i].classList.add("moveShape");
                    break;
                case "rotate":
                    dragShapes[i].classList.add("rotateShape");
                    break;
                case "delete":
                    dragShapes[i].classList.add("delShape");
                    break;
            }
        }

    }
    public addShape(key: string) {
        let shape: SVGElement | null = this.defs.querySelector(`#${key}`);
        if (shape == null) {
            console.error(`Shape ${key} not found`);
            return null;
        }
        this.shapeCount++;
        //create a new g element
        let g: SVGGElement = document.createElementNS("http://www.w3.org/2000/svg", "g") as unknown as SVGGElement;
        //add the bvl class
        g.classList.add("bvl");
        //create a new use element
        let use: SVGUseElement = document.createElementNS("http://www.w3.org/2000/svg", "use") as unknown as SVGUseElement;
        //set the href to the shape id
        use.setAttribute("href", `#${key}`);
        //set the id to shape + shapeCount
        use.setAttribute("id", `shape${this.shapeCount}`);
        //add the dragShape class
        use.classList.add("dragShape");
        //add the mouse down event
        use.addEventListener("mousedown", this.onMouseDown.bind(this));

        //put the use element in the g element
        g.appendChild(use);
        //add the g element to the board
        this.board.appendChild(g);
        //refresh the mode
        this.setMode(this.mode);
        return use;
    }


    private onMouseDown(event: MouseEvent) {
        let target: SVGElement | null = event.target as unknown as SVGElement;
        if (target == null) {
            console.error(`Target is null`);
            return;
        }
        if (this.mode == "delete") {
            //get all elements with the id of the target
            let elements: NodeListOf<SVGElement> = document.querySelectorAll(`#${target.id}`);
            //remove all elements with the id of the target
            for (let i: number = 0; i < elements.length; i++) {
                let parent: SVGElement | null = elements[i].parentElement as unknown as SVGElement;
                if (parent != null) {
                    parent.removeChild(elements[i]);
                    //remove the parent if it has no children
                    if (parent.childElementCount == 0) {
                        parent.parentElement?.removeChild(parent);
                    }
                }
                // elements[i].parentElement?.removeChild(elements[i]);
            }
            /* 
            if (target.classList.contains("dragShape")) {
                if (target.parentElement != null) {
                    target.parentElement.removeChild(target);
                }
              
             //  target.parentElement?.removeChild(target);
            }
            return; */
        }

        //  //console.log(target.id);
        this.currentShape = document.getElementById(target.id) as unknown as SVGPolygonElement;
        //  this.currentShape.setAttribute("transform", "translate(50,50), rotate(0)");
    }
    private onMouseUp(event: MouseEvent) {
        this.checkIfInPlace();
        this.currentShape = null;
    }
    private onMouseMove(event: MouseEvent) {
        if (this.currentShape == null) {
            return;
        }
        let mainBounds: DOMRect = this.main.getBoundingClientRect();

        const topCorner = this.main.createSVGPoint();
        topCorner.x = mainBounds.left;
        topCorner.y = mainBounds.top;
        const svgTopCorner = topCorner.matrixTransform(this.main.getScreenCTM()?.inverse());

        const bottomCorner = this.main.createSVGPoint();
        bottomCorner.x = mainBounds.left + mainBounds.width;
        bottomCorner.y = mainBounds.top + mainBounds.height;

        const svgBottomCorner = bottomCorner.matrixTransform(this.main.getScreenCTM()?.inverse());

        //shape bounds
        let shapeBounds: DOMRect = this.currentShape.getBoundingClientRect();
        let shapeBoundsTopCorner = this.main.createSVGPoint();
        shapeBoundsTopCorner.x = shapeBounds.left;
        shapeBoundsTopCorner.y = shapeBounds.top;

        let svgShapeBoundsTopCorner = shapeBoundsTopCorner.matrixTransform(this.main.getScreenCTM()?.inverse());

        let shapeBoundsBottomCorner = this.main.createSVGPoint();
        shapeBoundsBottomCorner.x = shapeBounds.left + shapeBounds.width;
        shapeBoundsBottomCorner.y = shapeBounds.top + shapeBounds.height;

        let svgShapeBoundsBottomCorner = shapeBoundsBottomCorner.matrixTransform(this.main.getScreenCTM()?.inverse());

        let shapeHeight: number = svgShapeBoundsBottomCorner.y - svgShapeBoundsTopCorner.y;
        let shapeWidth: number = svgShapeBoundsBottomCorner.x - svgShapeBoundsTopCorner.x;

        //shape center
        let shapeCenter = this.main.createSVGPoint();
        shapeCenter.x = shapeBounds.left + shapeBounds.width / 2;
        shapeCenter.y = shapeBounds.top + shapeBounds.height / 2;


        const pt = this.main.createSVGPoint();
        pt.x = event.clientX;
        pt.y = event.clientY;

        const svgP = pt.matrixTransform(this.main.getScreenCTM()?.inverse());

        if (this.mode == "move") {

            //     const pt = svg.createSVGPoint();
            //convert to svg coordinates

            //console.log(svgP.x, svgP.y);
            //get the x and y coordinates
            let x: number = svgP.x;
            let y: number = svgP.y;
           
           
            //round off to the nearest 4 decimal places
            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;

            //get the bounds of the main svg element


            //keep shape within bounds
            if (svgP.x < svgTopCorner.x + shapeWidth / 2) {
                x = svgTopCorner.x + shapeWidth / 2;
            }
            if (svgP.y < svgTopCorner.y + shapeHeight / 2) {
                y = svgTopCorner.y + shapeHeight / 2;
            }
            if (svgP.x > svgBottomCorner.x - shapeWidth / 2) {
                x = svgBottomCorner.x - shapeWidth / 2;
            }
            if (svgP.y > svgBottomCorner.y - shapeHeight / 2) {
                y = svgBottomCorner.y - shapeHeight / 2;
            }
            

            //get rotation from transform
            let transform: string = this.currentShape.getAttribute("transform") as string;
            //let rotation: number = Number(transform.split(",")[1].split(")")[0].split("(")[1]);

            let svgObj: SVGVo = new SVGVo();
            svgObj.fromTransform(transform);

            this.currentShape.setAttribute("transform", `translate(${x},${y}), rotate(${svgObj.rotation})`);

        }
        else if (this.mode == "rotate") {


            //console.log(this.currentShape);

            //get x and y from transform
            let transform: string = this.currentShape.getAttribute("transform") as string;
            //use regex to get the x and y values
            //console.log(transform);
            let svgObj: SVGVo = new SVGVo();
            //get class name from svg element
            let href: string = this.currentShape.getAttribute("href") as string;
            let shapeIndex: number = Shapes.hrefToShapeIndex(href);
            svgObj.shape = shapeIndex;
            svgObj.fromTransform(transform);


            let x: number = svgObj.x;
            let y: number = svgObj.y;

            //get the mouse position

            let mouseX: number = svgP.x;
            let mouseY: number = svgP.y;
            //get the center of the shape

            //get the angle between the mouse and the center of the shape
            let angle: number = Math.atan2(mouseY - y, mouseX - x) * (180 / Math.PI);
            //set the rotation
            svgObj.setRotation(angle);


            this.currentShape.setAttribute("transform", `translate(${x},${y}), rotate(${svgObj.rotation})`);
        }
    }
    public save() {

        let saveArray: any[] = [];
        let dragShapes: NodeListOf<SVGGElement> = document.querySelectorAll(".dragShape");
        for (let i: number = 0; i < dragShapes.length; i++) {
            let transform: string = dragShapes[i].getAttribute("transform") as string;
            let href: string = dragShapes[i].getAttribute("href") as string;
            let shapeIndex: number = Shapes.hrefToShapeIndex(href);
            console.log(shapeIndex);

             let svgObj: SVGVo = new SVGVo();
            svgObj.fromTransform(transform);
            svgObj.shape = shapeIndex;
            console.log(svgObj);
            saveArray.push(svgObj.getSaveArray());
        }

        let dataObj: any = { "shapes": saveArray };
        console.log(dataObj);
        return JSON.stringify(dataObj);
    }
    public loadJson(file: string) {
        fetch(file)
            .then(response => response.json())
            .then(data => this.processJson(data));
    }
    public processJson(data: any) {
        
        let shapes: any[] = data.shapes;
        console.log(shapes);
       for (let i: number = 0; i < shapes.length; i++) {
            let shape: any = shapes[i];
            console.log(shape);
             let shapeName = Shapes.indexToShape(shape[0]);
             console.log(shapeName);
            let x: number = shape[1];
            let y: number = shape[2];
            let rotation: number = shape[3];

            //round off to the nearest 4 decimal places
            x = Math.round(x * 10000) / 10000;
            y = Math.round(y * 10000) / 10000;

            //make new use element
            let use: SVGGElement = document.createElementNS("http://www.w3.org/2000/svg", "use");
            use.setAttribute("href", `#${shapeName}b`);
            use.setAttribute("class", "blank");
            use.setAttribute("transform", `translate(${x},${y}), rotate(${rotation})`);
            this.pattern.appendChild(use);
        }
    }
    //check if this.current is in place
    private checkIfInPlace() {
        if (this.currentShape == null) return;
       //loop through all the shapes with .blank class
        let blankShapes: NodeListOf<SVGGElement> = this.pattern.querySelectorAll(".blank");
        for (let i: number = 0; i < blankShapes.length; i++) {


            let blankShape: SVGGElement = blankShapes[i];
         //   console.log(blankShape);


            let blankSVO: SVGVo = new SVGVo();
            let transform: string = blankShape.getAttribute("transform") as string;
            console.log(transform);
            blankSVO.fromTransform(blankShape.getAttribute("transform") as string);
            blankSVO.shape = Shapes.hrefToShapeIndex(blankShape.getAttribute("href") as string);

             let currentSVO: SVGVo = new SVGVo();
            currentSVO.fromTransform(this.currentShape.getAttribute("transform") as string);
            currentSVO.shape = Shapes.hrefToShapeIndex(this.currentShape.getAttribute("href") as string);

            //check if the shapes are the same
            if (blankSVO.shape == currentSVO.shape) {
                //check if the x and y within 5 pixels
                if (Math.abs(blankSVO.x - currentSVO.x) < 5 && Math.abs(blankSVO.y - currentSVO.y) < 5) {
                    //check if the rotation is within 5 degrees
                    if (Math.abs(blankSVO.rotation - currentSVO.rotation) < 5) {
                        //snap to place
                        this.currentShape.setAttribute("transform", blankShape.getAttribute("transform") as string);
                        break;
                    }
                }
            }
        }

    }
    //create an svg point
    private createSVGPoint(x: number, y: number): SVGPoint {
        let pt: SVGPoint = this.main.createSVGPoint();
        pt.x = x;
        pt.y = y;
        return pt;
    }
}