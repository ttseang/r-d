import { ShapeGame } from "./ShapeGame";

export class AddBar
{
    private game:ShapeGame;
    private addBar:HTMLElement;
    constructor(game:ShapeGame)
    {
        this.game=game;
        this.addBar=document.querySelector(".addbar") as HTMLElement;
        this.addBar.addEventListener("click",this.onClick.bind(this));
    }
    private onClick(event:MouseEvent):void
    {
        let target:HTMLElement=event.target as HTMLElement;
        if(target==null)
        {
            console.error("Target is null");
            return;
        }
        console.log(target.id);
        let id:string=target.id;

        let key=id.replace("add","");
        console.log(key);
        if (key=="")
        {
           // console.error("Key is empty");
            return;
        }
        let shape:SVGElement | null=this.game.addShape(key);
        if(shape!=null)
        {
            shape.setAttribute("transform", "translate(10,10), rotate(0)");
           // shape.setAttribute("transform", "translate(150,150), rotate(0), scale(10)");
        }
      
    }
}