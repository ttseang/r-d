import { ShapeGame } from "./ShapeGame";

export class ToolBar
{
    private game:ShapeGame;
    private toolbar:HTMLElement;
    constructor(game:ShapeGame)
    {
        this.game=game;
        this.toolbar=document.querySelector(".toolbar") as HTMLElement;
        this.toolbar.addEventListener("click",this.onClick.bind(this));
    }
    private onClick(event:MouseEvent):void
    {
        let target:HTMLElement=event.target as HTMLElement;
        if(target==null)
        {
            console.error("Target is null");
            return;
        }
        //get all .toolBarItems
        let toolBarItems:NodeListOf<HTMLElement>=document.querySelectorAll(".toolbarItem");
        for(let i:number=0;i<toolBarItems.length;i++)
        {
            toolBarItems[i].classList.remove("selected");
        }

        target.classList.add("selected");


        console.log(target.id);
        let id:string=target.id;
        let key=id.replace("tool","");
        console.log(key);
        if (key=="")
        {
           // console.error("Key is empty");
            return;
        }
        this.game.setMode(key);
    }
}