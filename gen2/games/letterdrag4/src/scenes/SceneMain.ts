
import { ColorBurst } from "gameeffects";
import { GameObjects } from "phaser";
import { CompLayout, CompManager, ButtonController, CompLoader, ResponseImage, IComp, PosVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { GM } from "../classes/GM";
import { LetterBox } from "../classes/letterBox";
import { TargetBox } from "../classes/targetBox";
import { TargetBox3 } from "../classes/targetBox3";
import { WordVo } from "../dataObjs/WordVo";
import { WordVo2 } from "../dataObjs/WordVo2";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;

    private dragBox: LetterBox | null = null;
    private targetBoxes: TargetBox[] = [];
    private chars: string[] = ["A", "B", "C"];
    private level: number = -1;
    private levels: WordVo[] = [];
    private letterBoxes: LetterBox[] = [];

    private clickLock: boolean = false;

    public word: string;


    private gm: GM = GM.getInstance();


    private stars: ColorBurst;

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {

        this.levels = this.gm.levels;

        ColorBurst.preload(this, "./assets/effects/colorStars.png");

        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("arrows", "./assets/arrows.png");

        this.load.image("box", "./assets/box.png");

        this.load.audio("rightSound", "./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound", "./assets/audio/quiz_wrong.wav");


        for (let i: number = 0; i < this.levels.length; i++) {
            this.load.image(this.levels[i].word, "./assets/pics/" + this.levels[i].word + ".png");
        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);


        window['scene'] = this;


        this.stars = new ColorBurst(this, this.gw / 2, this.gh / 2);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        this.buttonController.callback = this.doButtonAction.bind(this);


    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);
        this.buildGame();
    }
    buildGame() {

        this.cm.getComp("wrong").visible = false;
        this.cm.getComp("right").visible = false;



        this.nextLevel();
        this.placeThings();
        this.mixUp();

    }
    placeThings() {

        let start: number = 5.5 - this.chars.length / 2;
        for (let i: number = 0; i < this.chars.length; i++) {
            let box: LetterBox = this.letterBoxes[i];
            let pos: PosVo = new PosVo(i + start, 4);
            this.grid.place(pos, box);
            box.place = pos;
        }

        let start2: number = 5.5 - this.word.length / 2;

        for (let j: number = 0; j < this.targetBoxes.length; j++) {
            let tb: TargetBox = this.targetBoxes[j];
            let pos2: PosVo = new PosVo(j + start2, 6);
            this.grid.place(pos2, tb);
            tb.place = pos2;
        }

    }
    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
        // this.grid.showPos();
        this.cm.doResize();



        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            let box: LetterBox = this.letterBoxes[i];
            box.doResize();
        }
        for (let j: number = 0; j < this.targetBoxes.length; j++) {
            let tb: TargetBox = this.targetBoxes[j];
            tb.doResize();

        }

    }

    nextLevel() {

        this.cm.getComp("wrong").visible = false;
        this.cm.getComp("right").visible = false;

        this.level++;
        if (this.level > this.levels.length - 1) {
            this.scene.start("SceneOver");
            return;
        }
        this.chars = this.levels[this.level].chars;
        this.word = this.levels[this.level].word;

        this.makeBoxes();
        this.placeThings();
        this.mixUp();
        this.clickLock = false;
    }
    hideBoxes() {
        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            this.letterBoxes[i].visible = false;
        }
    }
    destroyBoxes() {
        while (this.letterBoxes.length > 0) {
            this.letterBoxes.pop().destroy();
        }
        while (this.targetBoxes.length > 0) {
            this.targetBoxes.pop().destroy();
        }

    }
    reposBoxes() {
        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            let box: LetterBox = this.letterBoxes[i];
            box.doResize();
        }
    }
    makeBoxes() {
        //this.hideBoxes();
        this.destroyBoxes();
        for (let i: number = 0; i < this.chars.length; i++) {


            let box: LetterBox = this.addLetter(this.chars[i]);




            box.setScale(1, 1);
            //box.setPlace();
            box.setInteractive();
            box.visible = true;
        }

        for (let j: number = 0; j < this.word.length; j++) {
            let tb: TargetBox = this.targetBoxes[j];

            if (!tb) {
                tb = this.addTargetBox(0x00ff00);

            }
        }

        this.input.on("gameobjectdown", this.onDown.bind(this));
        this.input.on("pointermove", this.onMove.bind(this));
        this.input.on("pointerup", this.onUp.bind(this));
    }
    mixUp() {
        for (let i: number = 0; i < 10; i++) {
            let b1: number = Math.floor(Math.random() * this.letterBoxes.length);
            let b2: number = Math.floor(Math.random() * this.letterBoxes.length);

            let box1: LetterBox = this.letterBoxes[b1];
            let box2: LetterBox = this.letterBoxes[b2];


            let temp: PosVo = box1.place;
            box1.place = box2.place;
            box2.place = temp;


            this.grid.place(box1.place, box1);
            this.grid.place(box2.place, box2);
        }
    }

    addLetter(letter: string) {
        let box: LetterBox = new LetterBox(this, letter);
        this.letterBoxes.push(box);
        return box;
    }
    addTargetBox(color: number) {
        let targetBox: TargetBox = new TargetBox(this, color);
        this.targetBoxes.push(targetBox);
        return targetBox;
    }
    onDown(p: Phaser.Input.Pointer, letterBox: LetterBox) {
        if (this.clickLock == true) {
            return;
        }
        if (letterBox instanceof (LetterBox) == false) {
            return;
        }
        this.dragBox = letterBox;
        this.dragBox.orginal = new PosVo(this.dragBox.x, this.dragBox.y);

        this.children.bringToTop(letterBox);


    }
    onMove(p: Phaser.Input.Pointer) {
        if (this.dragBox) {
            this.dragBox.x = p.x;
            this.dragBox.y = p.y;

        }
    }
    onUp() {
        let found: boolean = this.checkTargets();
        if (found == false) {
            this.dragBox.snapBack();
        }
        this.dragBox = null;

    }
    doWinEffect() {
        //  this.mainText.visible = true;
        this.stars.start();
        this.playSound("rightSound");
        setTimeout(() => {
            this.nextLevel();

        }, 2000);
    }
    checkAll() {
        for (let i: number = 0; i < this.targetBoxes.length; i++) {
            let tb: TargetBox = this.targetBoxes[i];
            if (tb.setLetter !== this.chars[i]) {
                this.playSound("wrongSound");

                this.cm.getComp("wrong").visible = true;
                this.cm.getComp("right").visible = false;

                return;
            }
        }
        this.cm.getComp("wrong").visible = false;
        this.cm.getComp("right").visible = true;
        this.clickLock = true;
        this.doWinEffect();
    }
    checkTargets() {

        if (this.dragBox === null) {
            return;
        }

        for (let i: number = 0; i < this.targetBoxes.length; i++) {

            let targetBox: TargetBox = this.targetBoxes[i];


            let distY: number = Math.abs(this.dragBox.y - targetBox.y);
            let distX: number = Math.abs(this.dragBox.x - targetBox.x);

            if (distX < this.dragBox.displayWidth * 0.4 && distY < this.dragBox.displayHeight * 0.4) {
                this.dragBox.x = targetBox.x;
                this.dragBox.y = targetBox.y;
                targetBox.setLetter = this.dragBox.letter;
                this.dragBox.place = targetBox.place;
                this.dragBox.removeInteractive();
                return true;
            }

        }
        return false;
    }

    playSound(key: string) {
        let sound: Phaser.Sound.BaseSound = this.sound.add(key);
        sound.play();
    }

    doButtonAction(action: string, params: string) {
        if (action == "check") {
            this.checkAll();
        }
        if (action == "reset") {
            this.reposBoxes();
            this.dragBox = null;
            this.clickLock = false;
            this.cm.getComp("wrong").visible = false;
            this.cm.getComp("right").visible = false;

            this.placeThings();
            this.mixUp();
            for (let i: number = 0; i < this.letterBoxes.length; i++) {
                let box: LetterBox = this.letterBoxes[i];
                box.setInteractive();
            }
        }
    }

}