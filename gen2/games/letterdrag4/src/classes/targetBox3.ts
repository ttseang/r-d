import { GameObjects } from "phaser";
import { IBaseScene, Align, PosVo } from "ttphasercomps";


export class TargetBox3 extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;

    public correct:string="";
    public setLetter:string="";
    public text1:GameObjects.Text;
    
    private back:GameObjects.Image;
    private graphics:GameObjects.Graphics;
    private color:number=0;

    public place:PosVo=new PosVo(0,0);

    constructor(bscene:IBaseScene,color:number)
    {
        super(bscene.getScene())
        this.bscene=bscene;
        this.scene=bscene.getScene();

        this.color=color;
        
        this.back=this.scene.add.image(0,0,"box");
        
        this.text1=this.scene.add.text(0,0,"x",{"fontSize":"50px","color":"#ff0000"}).setOrigin(0.5,0.5);
       // this.text1.visible=false;

        this.graphics=this.scene.add.graphics();

        this.add(this.back);
        this.add(this.graphics);
        this.add(this.text1);

        this.scene.add.existing(this);

        this.doResize();
    }
    doResize()
    {
        Align.scaleToGameW(this.back,0.08,this.bscene);
        this.drawBorder();
        this.bscene.getGrid().place(this.place,this);
    }
    drawBorder()
    {
        
        this.graphics.clear();
        this.graphics.lineStyle(4,this.color);
        this.graphics.strokeRect(this.back.x-this.back.displayWidth/2,this.back.y-this.back.displayHeight/2,this.back.displayWidth,this.back.displayHeight);
        
    }
    setColor(color:number)
    {
        this.color=color;
        this.drawBorder();
      /*   this.graphics.clear();
        this.graphics.lineStyle(4,color);
        this.graphics.strokeRect(this.back.x-this.back.displayWidth/2,this.back.y-this.back.displayHeight/2,this.back.displayWidth,this.back.displayHeight);
       */
    }
    setCorrect(correct:string)
    {
        this.correct=correct;
        this.text1.setText(correct);
    }
    setText(letter:string)
    {
        this.setLetter=letter;
        this.text1.setText(letter);
    }
}