

import { CompLayout, CompManager, ButtonController, CompLoader, AlignGrid, IComp, PosVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { HotSpot } from "../classes/HotSpot";
import { Themro } from "../classes/Thermo";
import { HotSpotVo } from "../dataObjs/HotSpotVo";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;
    private thermo: Themro;
    private spots: HotSpot[] = [];
    private spotData: HotSpotVo[] = [];

    private spotGrid: AlignGrid;
    private dragFlag: boolean = false;

    private background: Phaser.GameObjects.Image;

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("th", "./assets/thermometer.png");

        this.load.image("kitchen", "./assets/kitchen.jpg");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);


        this.cm.regFontSize("list", 30, 1200);
        this.cm.regFontSize("words", 40, 1200);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        this.buttonController.callback = this.doButtonAction.bind(this);
    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);
        this.loadGameData();
        //  this.buildGame();
    }
    loadGameData() {
        fetch("./assets/hotspots.json")
            .then(response => response.json())
            .then(data => this.gotGameData({ data }));
    }
    gotGameData(data: any) {

        let spots: any = data.data.hotspots;

        for (let i: number = 0; i < spots.length; i++) {
            let spot: any = spots[i];
            let hotspotVo: HotSpotVo = new HotSpotVo(spot.xx, spot.yy, spot.ww, spot.hh, spot.per, spot.label, spot.item);
            this.spotData.push(hotspotVo);
        }
        this.buildGame();
    }
    buildGame() {

        this.background = this.add.image(0, 0, "kitchen");
        //Align.scaleToGameW(this.background,1.1,this);



        this.spotGrid = new AlignGrid(this, 11, 22, this.gw, this.gh);

        //     this.spotGrid.showPos();

        this.thermo = new Themro(this);
        this.thermo.setPer(0.5);

        this.grid.placeAt(5, 2, this.thermo);

        this.makeSpots();

        this.placeThings();

        this.input.on("gameobjectdown", this.onDown.bind(this));
        this.input.on("pointermove", this.onMove.bind(this));
        this.input.on("pointerup", this.onUp.bind(this));

    }
    makeSpots() {

        for (let i: number = 0; i < this.spotData.length; i++) {
            this.makeSpot(this.spotData[i]);
        }
        /* this.makeSpot(new HotSpotVo(5, 2, 2, 1, 0.25, "cold","snowman"));
        this.makeSpot(new HotSpotVo(5, 5, 2, 1, 1, "hot","kettle"));
        this.makeSpot(new HotSpotVo(8, 5, 1, 1, 0.2, "cold","milk"));
        this.makeSpot(new HotSpotVo(5, 7, 2, 1, 1, "hot","oven"));
        this.makeSpot(new HotSpotVo(12.5, 3.25, 2, 1, 0.7, "hot","microwave"));
        this.makeSpot(new HotSpotVo(10,8,1,1,0.2,"cold","ice"));
        this.makeSpot(new HotSpotVo(11,5,1,1,0.8,"hot","tea"));
        this.makeSpot(new HotSpotVo(14.5,5,1,1,0.8,"hot","coffee"));
        this.makeSpot(new HotSpotVo(16,2,3,7,0.2,"cold","fridge"));

        console.log(JSON.stringify(this.spotData)); */
    }
    makeSpot(hotspot: HotSpotVo) {

        let x2: number = -0.5;
        let y2: number = -0.5;

        /*   if (ww/2==Math.floor(ww/2))
          {
              x2=0.5;
          }
  
          if (hh/2==Math.floor(hh/2))
          {
              y2=0.5;
          } */

        let spot: HotSpot = new HotSpot(this, this.spotGrid, hotspot.xx + x2, hotspot.yy + y2, hotspot.ww, hotspot.hh, hotspot.per, hotspot.label, hotspot.item);
        this.spots.push(spot);

    }
    onDown(p: Phaser.Input.Pointer, obj: Themro) {
        if (obj != this.thermo) {
            return;
        }
        this.dragFlag = true;
    }
    onMove(p: Phaser.Input.Pointer) {
        if (this.dragFlag == true) {
            this.thermo.x = p.x;
            this.thermo.y = p.y;
            this.checkForSpot();
        }
    }
    onUp(p: Phaser.Input.Pointer) {
        if (this.dragFlag == true) {
            this.checkForSpot();

            let posVo: PosVo = this.grid.findNearestGridXYDec(this.thermo.x, this.thermo.y);            
            this.thermo.setPos(posVo);

        }
        this.dragFlag = false;
    }
    checkForSpot() {
        let spot: HotSpot | null = this.checkSpots();

        if (spot != null) {
            let per: number = spot.per;
            this.thermo.setPer(per);
        }
        else {
            this.thermo.setPer(0.5);
        }
    }
    checkSpots() {
        for (let i: number = 0; i < this.spots.length; i++) {
            let spot: HotSpot = this.spots[i];
            //    spot.angle=0;

            // let spotX: number = spot.x + spot.displayWidth / 2;


            let spotStartX: number = spot.x;//-spot.displayWidth/2;
            let spotStartY: number = spot.y;//-spot.displayHeight/2;

            let spotEndX: number = spot.x + spot.displayWidth;
            let spotEndY: number = spot.y + spot.displayHeight;

            if (this.thermo.x > spotStartX && this.thermo.y > spotStartY) {
                if (this.thermo.x < spotEndX && this.thermo.y < spotEndY) {
                    return spot;
                }
            }

            /* let distX: number = Math.abs(this.thermo.x - spotX);
            let distY: number = Math.abs(this.thermo.y - spot.y);

            if (distX < spot.displayWidth / 2) {
                if (distY < spot.displayHeight / 2) {
                    return spot;
                }
            }
 */
        }
    }
    placeThings() {
        let win: IComp = this.cm.getComp("uiWindow");


        this.background.displayWidth = win.displayWidth * 0.99;

        this.background.displayHeight = win.displayHeight * 0.99;

        Align.center(this.background, this);
    }
    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        // this.getGrid().hide();
        this.makeGrid(11, 11);
        // this.grid.showPos();
        this.cm.doResize();
        

        this.spotGrid.hide();
        this.spotGrid = new AlignGrid(this, 11, 22, this.gw, this.gh);

        //   this.spotGrid.showPos();

        for (let i: number = 0; i < this.spots.length; i++) {
            let spot: HotSpot = this.spots[i];
            spot.myGrid = this.spotGrid;
            spot.doResize();

        }

        this.placeThings();
        this.thermo.doResize();
    }
    doButtonAction(action: string, params: string) {

    }

}