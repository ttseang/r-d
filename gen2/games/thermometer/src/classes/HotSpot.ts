import Phaser from 'phaser';
import { AlignGrid, IBaseScene, PosVo } from 'ttphasercomps';
export class HotSpot extends Phaser.GameObjects.Container {
    private bscene: IBaseScene;
    private back: Phaser.GameObjects.Image;

    public label:string="cold";
    public item:string="thing";

    public per:number=0.5;
    private ww:number;
    private hh:number;
    private xx:number;
    private yy:number;

    private pos:PosVo;
    public myGrid:AlignGrid;

    constructor(bscene: IBaseScene,myGrid:AlignGrid,xx:number,yy:number,ww:number,hh:number,per:number,label:string,item:string) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.myGrid=myGrid;

        this.ww=ww;
        this.hh=hh;
        this.xx=xx;
        this.yy=yy;
        this.per=per;
        this.label=label;
        this.item=item;

        this.back = this.scene.add.image(0, 0, 'holder');
        this.back.setOrigin(0,0);
        this.back.setTint(0x00ff00);
        this.back.alpha=0;
        this.add(this.back);

        this.scene.add.existing(this);
        this.doResize();
    }
    doResize()
    {
        this.back.displayWidth=this.myGrid.cw*this.ww;
        this.back.displayHeight=this.myGrid.ch*this.hh;
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.myGrid.placeAt(this.xx,this.yy,this);
    }
}