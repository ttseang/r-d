import { Align, AlignGrid, IBaseScene, PosVo } from 'ttphasercomps';
export class Themro extends Phaser.GameObjects.Container {
    private bscene: IBaseScene;
    private back: Phaser.GameObjects.Image;
    private merc: Phaser.GameObjects.Image;

    private grid1:AlignGrid;

    private myScale:number=0.025;
    
    private per:number=1;
    public posVo:PosVo=new PosVo(5,2);

    constructor(bscene: IBaseScene) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.back = this.scene.add.image(0, 0, 'th')//.setOrigin(0,0);
        this.add(this.back);

        this.merc=this.scene.add.image(0,0,"holder").setOrigin(0.5,1);
        this.merc.setTint(0xff0000);
        this.add(this.merc);

        this.scene.add.existing(this);
        
        this.doResize();

        this.setInteractive();

        window['th']=this;
    }
    setPos(pos:PosVo)
    {
        this.posVo=pos;
    }
    setPer(per:number)
    {
        this.per=per;
        let hh:number=(this.back.displayHeight*0.75)*this.per;

        this.scene.add.tween({targets:this.merc,displayHeight:hh,duration:200});
    }
    doResize()
    {
        Align.scaleToGameW(this.back,this.myScale,this.bscene);

        this.grid1=new AlignGrid(this.bscene,11,11,this.back.displayWidth,this.back.displayHeight);

        this.merc.displayWidth=this.back.displayWidth/3
        this.merc.displayHeight=(this.back.displayHeight*0.75)*this.per;
       
        
        this.grid1.placeAt(-0.5,3,this.merc);
        this.setSize(this.back.displayWidth,this.back.displayHeight);


        this.bscene.getGrid().placeAt(this.posVo.x,this.posVo.y,this);
        this.x-=this.displayWidth;
       // this.y-=this.displayHeight;
    }
}