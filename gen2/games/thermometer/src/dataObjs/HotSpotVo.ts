export class HotSpotVo {
    public xx: number;
    public yy: number;
    public ww: number;
    public hh: number;
    public per: number;
    public label: string;
    public item: string

    constructor(xx: number, yy: number, ww: number, hh: number, per: number, label: string, item: string) {
        this.xx=xx;
        this.yy=yy;
        this.ww=ww;
        this.hh=hh;
        this.per=per;
        this.label=label;
        this.item=item;
    }
}