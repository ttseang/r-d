<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename>public/assets/pets.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>public/assets/pets.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (1).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>113,100,225,200</rect>
                <key>scale9Paddings</key>
                <rect>113,100,225,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (10).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,100,174,200</rect>
                <key>scale9Paddings</key>
                <rect>87,100,174,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (11).png</key>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (12).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>104,100,207,200</rect>
                <key>scale9Paddings</key>
                <rect>104,100,207,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (13).png</key>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (20).png</key>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (27).png</key>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (7).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,100,188,200</rect>
                <key>scale9Paddings</key>
                <rect>94,100,188,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (14).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,100,200,200</rect>
                <key>scale9Paddings</key>
                <rect>100,100,200,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (15).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>136,100,271,200</rect>
                <key>scale9Paddings</key>
                <rect>136,100,271,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (16).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,100,186,200</rect>
                <key>scale9Paddings</key>
                <rect>93,100,186,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (17).png</key>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (24).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>96,100,191,200</rect>
                <key>scale9Paddings</key>
                <rect>96,100,191,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (18).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>90,100,179,200</rect>
                <key>scale9Paddings</key>
                <rect>90,100,179,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (19).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,100,151,200</rect>
                <key>scale9Paddings</key>
                <rect>75,100,151,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (2).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,100,189,200</rect>
                <key>scale9Paddings</key>
                <rect>94,100,189,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (21).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>131,100,261,200</rect>
                <key>scale9Paddings</key>
                <rect>131,100,261,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (22).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>68,100,137,200</rect>
                <key>scale9Paddings</key>
                <rect>68,100,137,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (23).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>124,100,248,200</rect>
                <key>scale9Paddings</key>
                <rect>124,100,248,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (25).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>134,100,268,200</rect>
                <key>scale9Paddings</key>
                <rect>134,100,268,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (26).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>95,100,189,200</rect>
                <key>scale9Paddings</key>
                <rect>95,100,189,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (28).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,100,229,200</rect>
                <key>scale9Paddings</key>
                <rect>114,100,229,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (3).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>92,100,183,200</rect>
                <key>scale9Paddings</key>
                <rect>92,100,183,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (4).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,100,139,200</rect>
                <key>scale9Paddings</key>
                <rect>70,100,139,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (5).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,100,187,200</rect>
                <key>scale9Paddings</key>
                <rect>94,100,187,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (6).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>111,100,221,200</rect>
                <key>scale9Paddings</key>
                <rect>111,100,221,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (8).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,100,224,200</rect>
                <key>scale9Paddings</key>
                <rect>112,100,224,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (9).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>101,100,201,200</rect>
                <key>scale9Paddings</key>
                <rect>101,100,201,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (1).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (10).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (11).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (12).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (13).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (14).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (15).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (16).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (17).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (18).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (19).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (2).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (20).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (21).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (22).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (23).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (24).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (25).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (26).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (27).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (28).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (3).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (4).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (5).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (6).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (7).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (8).png</filename>
            <filename>../../../resource/findtheanimal/cats-dogs-icon-collection/400h/pet (9).png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
