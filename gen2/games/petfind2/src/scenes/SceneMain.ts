
import { ColorBurst, StarBurst } from "gameeffect";
import { GameObjects } from "phaser";
import { CompLayout, CompManager, ButtonController, CompLoader, ResponseImage, PosVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { SpineChar } from "../classes/char/SpineChar";
import { FImage } from "../classes/fimage";
import { LinkUtil } from "../classes/util/LinkUtil";
import { AnimationVo } from "../dataObjs/AnimationVo";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout:CompLayout;
    private cm:CompManager=CompManager.getInstance();
    private buttonController:ButtonController=ButtonController.getInstance();
    private compLoader:CompLoader;
    private fimages:FImage[]=[];
    private correct:number=4;
    private firstImage:FImage | null=null;

    private correctImage:FImage | null=null;
    private currentImage:FImage| null=null;
    private selectedImage:FImage | null=null;

    private stars:ColorBurst;
    private missingImage:ResponseImage;
    private spineChar!: SpineChar;

    private idleAnimations: AnimationVo[] = [];
    private walkAnimations: AnimationVo[] = [];
    private fidgetAnimations: AnimationVo[] = [];
    private fidgetAnimations2: AnimationVo[] = [];

    constructor() {
        super("SceneMain");
        this.compLoader=new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {

        ColorBurst.preload(this,"./assets/effects/colorStars.png");

        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("qmark","./assets/questionmark.png");
        this.load.image("right","./assets/right.png");
        this.load.image("wrong","./assets/wrong.png");
        this.load.image("triangle","./assets/triangle.png");
        this.load.image("missing","./assets/missing.png");
        this.load.atlas("pets","./assets/pets2.png","./assets/pets2.json");

        this.load.audio("rightSound","./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound","./assets/audio/quiz_wrong.wav");

    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.cm.regFontSize("list",30,1200);
        this.cm.regFontSize("words",10,1200);

        this.compLayout=new CompLayout(this);
       // this.compLoader.loadComps("./assets/layout.json");     
        
        this.buttonController.callback=this.doButtonAction.bind(this);

        window.onresize=this.doResize.bind(this);
        this.buildGame();

        this.idleAnimations = [new AnimationVo("idle", true), new AnimationVo("Repeating animations/blink", true)];
        this.walkAnimations = [new AnimationVo("walk", true), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations = [new AnimationVo("fidget01", false), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations2 = [new AnimationVo("fidget02", false), new AnimationVo("Repeating animations/blink", true)];

        

        this.spineChar = new SpineChar(this, "mascot", "./assets/char/Amka.json", "./assets/char/Amka.atlas", this.idleAnimations);
        this.spineChar.currentPos = new PosVo(5, 10);
        this.spineChar.scaleToGameW(.09);
        this.spineChar.adjustY();

        window['scene']=this;
    }
    compsLoaded()
    {
        this.compLayout.loadPage(this.cm.startPage);
       
        this.buildGame();
        //this.loadGameData();
    }
    buildGame()
    {

        this.stars=new ColorBurst(this,this.gw/2,this.gh/2);
        this.stars.count=100;
      


        this.missingImage=new ResponseImage(this,"missing",0.1,"missing");

        this.missingImage.setPos(1.8,4);

        this.correctImage=new FImage(this,"pets");
        this.selectedImage=new FImage(this,"pets");
        this.selectedImage.visible=false;
        
        this.selectedImage.scaleMe(0.05);
        this.correctImage.scaleMe(0.05);


        this.correctImage.setPos(1.8,4);
        this.selectedImage.setPos(1.8,7);
        
        

        this.resetGame();
        
        this.input.on("gameobjectdown",this.selectImage.bind(this));
        this.input.on("pointerup",this.onUp.bind(this));

        //set listeners

     //   LinkUtil.makeLink("resetButton",this.doButtonAction.bind(this),"reset","");
        LinkUtil.makeLink("checkButton",this.doButtonAction.bind(this),"check","");

    }
    
    selectImage(p:Phaser.Input.Pointer,f:FImage)
    {
        if (f instanceof(FImage))
        {
           
            this.currentImage=f;
            this.children.bringToTop(this.currentImage);
            this.selectedImage.setMyFrame(this.currentImage.frameName);
            this.selectedImage.visible=true;
          //  this.cm.getComp("selectText").visible=true;
        }
        

    }
    onUp()
    {
        if (this.currentImage)
        {
            this.currentImage.setDepth(this.currentImage.myDepth);
        }
    }
    resetGame()
    {

        for (let i:number=0;i<this.fimages.length;i++)
        {
            this.fimages[i].destroy();

        }
        this.fimages=[];
        this.firstImage=null;
        this.correct=Math.floor(Math.random()*33)+1;

     //   console.log(this.correct);

        this.placeAnimals();
        this.selectedImage.visible=false;
        this.selectedImage.frameName="";
       // this.cm.getComp("selectText").visible=false;
    }
    cheat()
    {
       // console.log(this.correctImage.frameName);
        this.firstImage.checkMe(this.correctImage.frameName);
    }
    placeAnimals()
    {

       // this.grid.showPos();

        let rightLimit:number=this.gw*0.8;
        let bottomLimit:number=this.gh*0.7;

        let startX:number=this.gw*0.35;
        let xx:number=startX;
        let yy:number=this.gh*0.3;

        let prevImage:FImage | null=null;
        let limit=100;

        for (let i:number=0;i<limit;i++)
        {
            let pic:FImage=new FImage(this,"pets");
            pic.setInteractive();
            this.fimages.push(pic);
          //  pic.visible=false;
           
            if (this.firstImage===null)
            {
                this.firstImage=pic;
            }

            if (prevImage)
            {
                prevImage.nextImage=pic;
            }

            let scale:number=Math.floor((Math.random()*10)+60)/1000;
           
            pic.scaleMe(scale);
            //Align.scaleToGameW(pic,0.1,this);

            this.setRandImage(pic,this.correct);
            
            pic.x=xx;
            pic.y=yy;

            let offsetX:number=Math.random()*(this.gw*.025);
            //-Math.random()*(this.gw*.025);
            let offsetY:number=Math.random()*(this.gh*.025);//-Math.random()*(this.gh*.05);

            pic.x+=offsetX;
            pic.y-=offsetY;

            xx+=pic.displayWidth;
            pic.myDepth=Math.floor(yy*5+xx);
            pic.setDepth(pic.myDepth);

            prevImage=pic;

            if (xx>rightLimit)
            {
                xx=startX;
                yy+=pic.displayHeight/2.5;
                if (yy>bottomLimit)
                {
                    i=bottomLimit;
                }
            }            
            pic.findPlace();
        }


        let pick:number=Math.floor(Math.random()*this.fimages.length);

        let key:string="pet ("+this.correct.toString()+").png";

        this.fimages[pick].setMyFrame(key);
        this.correctImage.setMyFrame(key);
    }
    setRandImage(pic:FImage,ignore:number)
    {
        let r:number=ignore;
        while(r===ignore)
        {
            r=Math.floor(Math.random()*31)+1;
        }
           
            let key:string="pet ("+r.toString()+").png";
            pic.frameIndex=r;
            pic.setMyFrame(key);
    }
    doResize()
    {
        console.log("RESIZE");
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.spineChar.scaleToGameW(0.09);
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();
    //    this.cm.doResize();
       this.firstImage.doResize();

       this.selectedImage.doResize();
       this.correctImage.doResize();
       this.missingImage.doResize();
    }
    playSound(key:string)
    {
        let sound:Phaser.Sound.BaseSound=this.sound.add(key);
        sound.play();
    }
    doButtonAction(action:string,params:string)
    {
       if (action=="check")
        {
            if (this.selectedImage.frameName===this.correctImage.frameName)
            {
                console.log("Correct");
                this.stars.x=this.currentImage.x;
                this.stars.y=this.currentImage.y;
                this.firstImage.checkMe(this.correctImage.frameName);
                this.playSound("rightSound");
                
                this.stars.start();

                this.spineChar.setAnimations(this.fidgetAnimations2);
                setTimeout(() => {
                    this.resetGame();
                }, 2000);
            }
            else
            {
                console.log("wrong");

                this.spineChar.setAnimations(this.fidgetAnimations);
                this.playSound("wrongSound");

                let wscale:number=this.correctImage.wscale;

                this.correctImage.scaleMe(wscale*1.5);
                setTimeout(() => {
                    this.correctImage.scaleMe(wscale);
                }, 1000);
            }
        }
    }

}