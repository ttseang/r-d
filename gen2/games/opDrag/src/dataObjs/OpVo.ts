export class OpVo
{
    public icons:string;
    public correct:number;

    constructor(icons:string,correct:number)
    {
        this.icons=icons;
        this.correct=correct;
    }
}