
import { DragEng, IDragObj, ITargetObj } from "dragEng";
import { ColorBurst } from "gameeffects";
import { CompLayout, CompManager, ButtonController, CompLoader, BaseScene, PosVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { DragIcon } from "../classes/DragIcon";
import { TargIcon } from "../classes/TargIcon";
import { OpVo } from "../dataObjs/OpVo";


export class SceneMain extends BaseScene {
    private compLayout:CompLayout;
    private cm:CompManager=CompManager.getInstance();
    private buttonController:ButtonController=ButtonController.getInstance();
    private compLoader:CompLoader;
    private dragEng:DragEng;
    private targBox:TargIcon;
    private selectedContent:string="";
    private correct:string="1";

    private level:number=-1;
    private levels:OpVo[]=[];
    private current:OpVo;
    private buttonLock:boolean=false;

    private colorBurst:ColorBurst;

    constructor() {
        super("SceneMain");
        this.compLoader=new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
         this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("qmark","./assets/questionmark.png");
        this.load.image("right","./assets/right.png");
        this.load.image("wrong","./assets/wrong.png");
        this.load.image("triangle","./assets/triangle.png");
        this.load.image("arrows","./assets/arrows.png");

        this.load.atlas("opicons","./assets/opIcons.png","./assets/opIcons.json");
        this.load.audio("rightSound", "./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound", "./assets/audio/quiz_wrong.wav");

        ColorBurst.preload(this,"./assets/effects/colorStars.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.cm.regFontSize("list",30,1200);
        this.cm.regFontSize("words",40,1200);

        this.compLayout=new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");     
        
        this.dragEng=new DragEng(this);

        this.buttonController.callback=this.doButtonAction.bind(this);

    }
    compsLoaded()
    {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize=this.doResize.bind(this);

        this.colorBurst=new ColorBurst(this,this.gw/2,this.gh/2);
        
        this.loadGameData();


       // this.buildGame();
    }
    loadGameData()
    {
         fetch("./assets/words.json")
        .then(response => response.json())
        .then(data => this.gotGameData({ data }));
    }
    gotGameData(data:any)
    {
        let words:any=data.data.words;
        console.log(words);

        for (let i:number=0;i<words.length;i++)
        {
            let opVo:OpVo=new OpVo(words[i].icons,words[i].correct);
            this.levels.push(opVo);
        }
        this.buildGame();
    }
    doResize()
    {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();
        this.cm.doResize();
       this.dragEng.doResize();
    }
    doButtonAction(action:string,params:string)
    {
        if (this.buttonLock==true)
        {
            return;
        }
        if (action=="reset")
        {
            this.dragEng.resetBoxes();
            this.targBox.setPlace(new PosVo(5,3.5));
            this.dragEng.clickLock=false;
            this.showIcons(false,false);
        }
        if (action=="check")
        {
            this.checkCorrect();
        }
    }
    /**
     * Game Starts here
     */
    buildGame()
    {
        this.dragEng.setListeners();
        this.dragEng.collideCallback=this.onDrop.bind(this);
        this.showIcons(false,false);
        this.nextLevel();
    }
    nextLevel()
    {
        this.level++;
        if (this.level>this.levels.length-1)
        {
            console.log("out of icons");
            return;
        }
        this.current=this.levels[this.level];
        this.makeBoxes();
        this.dragEng.clickLock=false;
        this.buttonLock=false;
        this.showIcons(false,false);
    }
    onDrop(dropObj:IDragObj,targObj:ITargetObj)
    {
        console.log(dropObj);
        console.log(targObj);

      //  dropObj.x=targObj.x+targObj.displayWidth;
        dropObj.setPlace(new PosVo(5.5,3.5));
        targObj.setPlace(new PosVo(4.5,3.5));

        this.selectedContent=dropObj.content;
        this.dragEng.clickLock=true;
    }
    showIcons(showRight:boolean,showWrong:boolean)
    {
        this.cm.getComp("right").visible=showRight;
        this.cm.getComp("wrong").visible=showWrong;

    }
    checkCorrect()
    {
        this.buttonLock=true;

        console.log(this.selectedContent);
        console.log(this.correct);

        if (this.selectedContent==this.correct)
        {
            console.log("correct");
            this.playSound("rightSound");
            this.colorBurst.start();
            setTimeout(() => {
                this.nextLevel();
            }, 2000);
            this.showIcons(true,false);
        }
        else
        {
            console.log("wrong");
            this.buttonLock=false;
            this.playSound("wrongSound");
            this.showIcons(false,true);
        }
    }
    makeBoxes()
    {

        this.dragEng.destoryDrags();
        this.dragEng.destroyTargs();
      

        let icons:string[]=this.current.icons.split(",");

        this.targBox=new TargIcon(this);
        this.targBox.setIndex(this.current.correct);

        this.correct=this.current.correct.toString();

        this.targBox.setPlace(new PosVo(5,3.5));
        this.dragEng.addTarget(this.targBox);

        for (let i:number=0;i<5;i++)
        {
            let i2:number=i+1;
            
            let dragIcon:DragIcon=new DragIcon(this);
            let index:number=parseInt(icons[i]);
            dragIcon.setIndex(index);
            dragIcon.setInteractive();

            let posVo:PosVo=new PosVo(3+i,7);
            dragIcon.setPlace(posVo);
            dragIcon.original=posVo;
            this.dragEng.addDrag(dragIcon);
        }
    }
}