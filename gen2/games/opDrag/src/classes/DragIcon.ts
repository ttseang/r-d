import { BaseDragObj, IDragObj } from "dragEng";
import { Align, IBaseScene } from "../../../../common/mods/ttcomps";

export class DragIcon extends BaseDragObj implements IDragObj
{
    private icon:Phaser.GameObjects.Sprite;

    constructor(bscene:IBaseScene)
    {
        super(bscene);

        this.icon=this.scene.add.sprite(0,0,"opicons");
        Align.scaleToGameW(this.icon,0.1,this.bscene);
        this.add(this.icon);

        this.setSize(this.icon.displayWidth,this.icon.displayHeight);

        this.scene.add.existing(this);
    }
    setIndex(index:number)
    {
        this.setContent(index.toString());
        console.log(this.content);
        let frameKey:string="a"+index.toString()+".png";
        this.icon.setFrame(frameKey);
    }
    doResize()
    {
        Align.scaleToGameW(this.icon,0.1,this.bscene);
        this.setPlace(this.place);
    }
}