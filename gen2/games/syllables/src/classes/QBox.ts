import { GameObjects } from "phaser";
import { IBaseScene } from "ttphasercomps";

import { WordPartVo } from "../dataObjs/WordPartVo";


export class QBox extends GameObjects.Container {
    private bscene: IBaseScene;
    public text1: GameObjects.Text;
    public text2: GameObjects.Text;
    private holder: GameObjects.Image;
    public text: string = "";
    private ww: number;

    private fs:number=24;

    constructor(bscene: IBaseScene, ww: number) {
        super(bscene.getScene());
        this.bscene = bscene;

        this.ww = ww;

        this.holder = this.scene.add.image(0, 0, "holder");
        this.text1 = this.scene.add.text(0, 0, "", { color: "black" }).setOrigin(0, 0.5);
        this.text2 = this.scene.add.text(0, 0, "", { color: "black" }).setOrigin(0, 0.5);

        this.text1.setFontSize(this.fs);
        this.text2.setFontSize(this.fs);

        this.text2.x = -this.text2.displayWidth;
        this.text2.visible = false;

       

        this.holder.displayWidth = ww;
        this.holder.displayHeight = this.text1.displayHeight * 1.5;
        this.holder.x = -this.holder.displayWidth / 2;

        this.add(this.holder);
        this.add(this.text1);
        this.add(this.text2);

        //this.holder.alpha=.2;
        this.holder.setTint(0xcccccc);

        let ww2: number = this.holder.displayWidth + this.text1.displayWidth;

        this.setSize(ww2 * 1.4, this.holder.displayHeight * 1.2);
        this.scene.add.existing(this);
    }
    setTint(color:number)
    {
        this.holder.setTint(color);
    }
    setFontSize(fs:number,ww:number)
    {
        this.ww = ww;
        this.fs=fs;
        this.text1.setFontSize(fs);
        this.text2.setFontSize(fs);
        this.holder.displayWidth = this.ww;
        this.holder.displayHeight = this.text1.displayHeight * 1.5;
        this.holder.x = -this.holder.displayWidth / 2;

        let ww2: number = this.holder.displayWidth + this.text1.displayWidth;

        this.setSize(ww2 * 1.4, this.holder.displayHeight * 1.2);
    }
    setWord(word: WordPartVo, ww: number) {
        this.text1.setText(word.part2);
        this.ww = ww;

        this.holder.displayWidth = this.ww;
        this.holder.displayHeight = this.text1.displayHeight * 1.5;
        this.holder.x = -this.holder.displayWidth / 2;

        let ww2: number = this.holder.displayWidth + this.text1.displayWidth;

        this.setSize(ww2 * 1.4, this.holder.displayHeight * 1.2);
    }
    setText(text: string) {

        this.text = text;
        this.text2.setText(text);
        this.text2.x = -this.text2.displayWidth;
        this.text2.visible = true;
    }

}