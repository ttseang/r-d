import { GameObjects } from "phaser";
import { BaseComp, CompBack } from "ttphasercomps";
import { IBaseScene } from "ttphasercomps/interfaces/IBaseScene";



export class ListBox extends BaseComp
{
    private listText:GameObjects.Text[]=[];
    private itemList:string[]=[];
    private cols:number=0;
    private colWidths:number[]=[];
    private fs:number=36;
    private compBack:CompBack;


    constructor(bscene:IBaseScene,itemList:string[],cols:number=3,backStyleKey:string)
    {
        super(bscene,"list");

        this.itemList=itemList;
        this.cols=cols;        
        
        this.compBack=new CompBack(bscene,100,100,this.cm.getBackStyle(backStyleKey));
        this.add(this.compBack);

        this.buildList();

        this.scene.add.existing(this);
    }
    doResize()
    {
        this.buildList();
        super.doResize();
        this.compBack.doResize(this.displayWidth,this.displayHeight);
    }
    public strikeFromList(word:string)
    {
        if (!this.itemList.includes(word))
        {
            return;
        }
        let ck:number=this.itemList.indexOf(word);
        this.itemList.splice(ck,1);         

        this.buildList();
       
    }
    destroyList()
    {
        for (let i:number=0;i<this.listText.length;i++)
        {
            this.listText[i].destroy();
        }
        this.listText=[];
    }
    buildList()
    {
        this.destroyList();
        for (let i:number=0;i<this.cols;i++)
        {
            this.colWidths[i]=0;
        }


        let col:number=0;
        let th:number=0;
        let ww:number=0;
            

        this.fs=this.cm.getFontSize("list",this.bscene.getW());

        for (let i:number=0;i<this.itemList.length;i++)
        {
            
            let tb:GameObjects.Text=this.scene.add.text(0,0,this.itemList[i],{"color":"black"});
            tb.setFontSize(this.fs);
            this.add(tb);
            this.listText.push(tb);

            if (this.colWidths[i]<tb.displayWidth)
            {
                this.colWidths[i]=tb.displayWidth;
            }
            col++;
            if (col==this.cols)
            {
                col=0;
            } 
            if (tb.displayHeight>th)
            {
                th=tb.displayHeight*1.1;
            }          
        }
        let hh:number=th;
        let xx:number=0;
        let yy:number=0;
        let col2:number=0;
        let yMod:number=1.6;
        let xMod:number=1.3;

        for (let i:number=0;i<this.itemList.length;i++)
        {
            this.listText[i].x=xx;
            this.listText[i].y=yy;

            xx+=this.colWidths[col2]*xMod;
            col2++;

            if (col2===this.cols)
            {
                col2=0;
                yy+=th*yMod;
                xx=0;
                hh+=th*yMod;
            }
        }

        for (let i:number=0;i<this.cols;i++)
        {
            //console.log(this.colWidths[i]);

            ww+=this.colWidths[i]*xMod;
        }

         for (let i:number=0;i<this.listText.length;i++)
        {
            this.listText[i].x+=ww*0.05;
            this.listText[i].y+=hh*0.05;
        }
     //   //console.log(ww,hh);

        this.compBack.doResize(ww,hh*1.1);
        
        this.compBack.y=hh/2;
        this.compBack.x=ww/2;

        this.setSize(ww,hh);
    }

}