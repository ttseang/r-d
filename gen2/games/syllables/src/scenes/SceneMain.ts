
import { Align, ButtonController, CompLayout, CompLoader, CompManager, IGameObj } from "ttphasercomps";
import { ListBox } from "../classes/ListBox";
import { QBox } from "../classes/QBox";
import { SylBox } from "../classes/SylBox";
import { WordPartVo } from "../dataObjs/WordPartVo";


import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private cl:CompLoader;
    private compLayout:CompLayout;
    private cm:CompManager=CompManager.getInstance();
    private wordList:string[]=[];
    private wordVos:WordPartVo[]=[];
    private currentWordVo:WordPartVo | null=null;

    private largeBox:number=0;
    private dragBoxes:SylBox[]=[];
    private qbox:QBox;
    private buttonController:ButtonController=ButtonController.getInstance();

    private dragBox:SylBox | null=null;
    private answer:string="";
    private qIndex:number=-1;
    private list:ListBox;


    constructor() {
        super("SceneMain");
        this.cl=new CompLoader(this.compsLoaded.bind(this))
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("qmark","./assets/questionmark.png");
        this.load.image("right","./assets/right.png");
        this.load.image("wrong","./assets/wrong.png");
        this.load.image("triangle","./assets/triangle.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.cm.regFontSize("list",30,1200);
        this.cm.regFontSize("words",40,1200);

        this.compLayout=new CompLayout(this);
        this.cl.loadComps("./assets/layout.json");     
        
        this.buttonController.callback=this.doButtonAction.bind(this);

        window['scene']=this;
    }
    compsLoaded()
    {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize=this.doResize.bind(this);
        this.loadGameData();
    }

    doResize()
    {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();
        this.cm.doResize();
        this.sizeBoxes();
    }
    loadGameData()
    {
        fetch("./assets/words.json")
        .then(response => response.json())
        .then(data => this.gotGameData({ data }));
    }
    gotGameData(data:any)
    {
        let words:any=data.data.words;
        for (let i:number=0;i<words.length;i++)
        {
            let word:string=words[i].word;
            let part1:string=words[i].p1;
            let part2:string=words[i].p2;
            let wrong:string=words[i].wrong;

            this.wordList.push(word);

            let wordVo:WordPartVo=new WordPartVo(word,part1,part2,wrong);
            this.wordVos.push(wordVo);
        }
       
        this.buildGame();
    }
    buildGame()
    {
       /*  this.cm.getComp("wrong").visible=false;
        this.cm.getComp("right").visible=false; */

        this.qbox=new QBox(this,0);
       

        //this.grid.placeAt(8,6,this.qbox);

        this.list=new ListBox(this,this.wordList,3,"white");
        this.list.setPos(1,4);

        this.getNext();

        //list.makeList(["one","two","three"]);

        this.input.on("gameobjectdown",this.onDown.bind(this));
        this.input.on("pointermove",this.onMove.bind(this));
        this.input.on("pointerup",this.onUp.bind(this));

    }
    onDown(p:Phaser.Input.Pointer,obj:SylBox)
    {
        if (obj instanceof(SylBox))
        {
            this.dragBox=obj;
            this.children.bringToTop(this.dragBox);
        }
    }
    onMove(p:Phaser.Input.Pointer)
    {
        if (this.dragBox)
        {
            this.dragBox.x=p.x;
            this.dragBox.y=p.y;
        }
    }
    onUp()
    {
        if (this.dragBox)
        {
            let xDist:number=Math.abs(this.dragBox.x-this.qbox.x);
            let yDist:number=Math.abs(this.dragBox.y-this.qbox.y);

            console.log(xDist,yDist);
            
            if (xDist>this.qbox.displayWidth/2 || yDist>this.qbox.displayHeight/2)
            {
                this.dragBox.snapBack();
            }
            else
            {
             
                this.resetBoxes();

                this.cm.getComp("right").visible=false;
                this.cm.getComp("wrong").visible=false;

               let partText:string=this.dragBox.text;
               this.dragBox.visible=false;
               console.log(partText);
                if (partText==this.currentWordVo.part1)
                {
                    console.log("correct");
                }
               this.answer=this.dragBox.text;
               this.qbox.setText(this.dragBox.text);
            }
        }
        this.dragBox=null;
    }
    doButtonAction(action:string,params:string)
    {
        if (action==="check")
        {
            this.checkCorrect();
        }
    }
    checkCorrect()
    {
        if (this.answer==="")
        {
            return;
        }
        if (this.answer==this.currentWordVo.part1)
        {
            this.cm.getComp("right").visible=true;
            this.qbox.setTint(0x00ff00);
            this.list.strikeFromList(this.currentWordVo.word);    
            setTimeout(() => {
                this.getNext();
            }, 1000);        
        }
        else
        {
            this.cm.getComp("wrong").visible=true;
            this.qbox.setTint(0xff0000);
        }
      
    }
    resetBoxes()
    {
        for (let i:number=0;i<this.dragBoxes.length;i++)
        {
            if (this.dragBox!=this.dragBoxes[i])
            {
                this.dragBoxes[i].resetPos();
            }            
        }
    }
    getNext()
    {
        this.qbox.setTint(0xcccccc);
        this.qbox.setText("");
        this.cm.getComp("wrong").visible=false;
        this.cm.getComp("right").visible=false;
        this.qIndex++;
        if (this.qIndex<this.wordVos.length-1)
        {
            this.setWordParts();
        }
        
    }
    setWordParts()
    {
        let wordVo:WordPartVo=this.wordVos[this.qIndex];
        this.currentWordVo=wordVo;

        this.qbox.setWord(wordVo,this.getW()*.1);       

        let parts:string[]=[];
        let wrongs:string[]=wordVo.wrong.split(",");

        parts.push(wordVo.part1);
        parts.push(wrongs[0]);
        parts.push(wrongs[1]);


        while(this.dragBoxes.length>0)
        {
            this.dragBoxes.pop().destroy();
        }

        for (let i:number=0;i<3;i++)
        {
            let box:SylBox=new SylBox(this,parts[i],i);
            box.setInteractive();
            this.dragBoxes.push(box);
            this.grid.placeAt(6,4,box);
        }
        this.sizeBoxes();
        this.mixBoxes();
    }
    mixBoxes()
    {
        for (let i:number=0;i<this.dragBoxes.length;i++)
        {
            let r1:number=Math.floor(Math.random()*3);
            let r2:number=Math.floor(Math.random()*3);
            
            let box1:SylBox=this.dragBoxes[r1];
            let box2:SylBox=this.dragBoxes[r2];

            let x1:number=box1.x;
            let y1:number=box1.y;

            let x2:number=box2.x;
            let y2:number=box2.y;

            box1.x=x2;
            box1.y=y2;

            box2.x=x1;
            box2.y=y1;
        }
    }
    sizeBoxes()
    {
        let fs:number=this.cm.getFontSize("words",this.getW());

        for (let i:number=0;i<this.dragBoxes.length;i++)
        {
            this.dragBoxes[i].setFontSize(fs);
        }
        this.largeBox=0;
        for (let i:number=0;i<this.dragBoxes.length;i++)
        {
            if (this.dragBoxes[i].displayWidth>this.largeBox)
            {
                this.largeBox=this.dragBoxes[i].displayWidth;
                this.grid.placeAt(6,4,this.dragBoxes[i]);
            }
        }

        let xx:number=this.dragBoxes[0].x;

        for (let j:number=0;j<this.dragBoxes.length;j++)
        {
            this.dragBoxes[j].setBackSize(this.largeBox);
            this.dragBoxes[j].x=xx;
            this.dragBoxes[j].y=this.dragBoxes[0].y;
            this.dragBoxes[j].initPos();
            xx+=this.dragBoxes[j].displayWidth*1.5;
        }

        this.qbox.setFontSize(fs,this.getW()*0.1);
        this.grid.placeAt(8,6,this.qbox);

       
    }
    
}