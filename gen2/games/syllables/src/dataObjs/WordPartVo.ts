export class WordPartVo
{
    public word:string;
    public part1:string;
    public part2:string;
    public wrong:string;

    public used:boolean=false;

    constructor(word:string,part1:string,part2:string,wrong:string)
    {
        this.word=word;
        this.part1=part1;
        this.part2=part2;
        this.wrong=wrong;
    }
}