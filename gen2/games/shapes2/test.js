(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["above-the-fold", "chosenPuzzleDescription"], {
    "143c": function(t, n, e) {},
    2909: function(t, n, e) {
        "use strict";
        function r(t) {
            if (Array.isArray(t)) {
                for (var n = 0, e = new Array(t.length); n < t.length; n++)
                    e[n] = t[n];
                return e
            }
        }
        function i(t) {
            if (Symbol.iterator in Object(t) || "[object Arguments]" === Object.prototype.toString.call(t))
                return Array.from(t)
        }
        function o() {
            throw new TypeError("Invalid attempt to spread non-iterable instance")
        }
        function u(t) {
            return r(t) || i(t) || o()
        }
        e.d(n, "a", (function() {
            return u
        }
        ))
    },
    "2d19": function(t, n, e) {
        "use strict";
        var r = e("950f")
          , i = e.n(r);
        i.a
    },
    4508: function(t, n, e) {
        "use strict";
        e.r(n);
        var r = function() {
            var t = this
              , n = t.$createElement
              , e = t._self._c || n;
            return e("div", {
                staticClass: "puzzle-img-hint"
            }, [t.isAnyPuzzleChosen ? e("BaseSvgPathPuzzle", {
                attrs: {
                    svgPath: t.chosenPuzzleImageSvgPath,
                    width: 150,
                    height: 150,
                    alt: t.chosenPuzzleName + " puzzle image hint"
                }
            }) : e("BaseCustomIconCirclePuzzle", {
                attrs: {
                    colorStyle: {
                        color: "black",
                        opacity: "0.3"
                    },
                    faSizeClass: "fa-10x"
                }
            })], 1)
        }
          , i = []
          , o = e("5530")
          , u = e("91bd")
          , a = e("8584")
          , s = e("2f62")
          , c = {
            components: {
                BaseSvgPathPuzzle: u["default"],
                BaseCustomIconCirclePuzzle: a["a"]
            },
            computed: Object(o["a"])({}, Object(s["c"])(["isAnyPuzzleChosen", "chosenPuzzleName", "chosenPuzzleImageSvgPath"]))
        }
          , l = c
          , h = (e("2d19"),
        e("2877"))
          , f = Object(h["a"])(l, r, i, !1, null, "4baa9def", null);
        n["default"] = f.exports
    },
    "4de4": function(t, n, e) {
        "use strict";
        var r = e("23e7")
          , i = e("b727").filter
          , o = e("1dde")
          , u = e("ae40")
          , a = o("filter")
          , s = u("filter");
        r({
            target: "Array",
            proto: !0,
            forced: !a || !s
        }, {
            filter: function(t) {
                return i(this, t, arguments.length > 1 ? arguments[1] : void 0)
            }
        })
    },
    "4df4": function(t, n, e) {
        "use strict";
        var r = e("0366")
          , i = e("7b0b")
          , o = e("9bdd")
          , u = e("e95a")
          , a = e("50c4")
          , s = e("8418")
          , c = e("35a1");
        t.exports = function(t) {
            var n, e, l, h, f, p, _ = i(t), v = "function" == typeof this ? this : Array, d = arguments.length, g = d > 1 ? arguments[1] : void 0, y = void 0 !== g, m = c(_), w = 0;
            if (y && (g = r(g, d > 2 ? arguments[2] : void 0, 2)),
            void 0 == m || v == Array && u(m))
                for (n = a(_.length),
                e = new v(n); n > w; w++)
                    p = y ? g(_[w], w) : _[w],
                    s(e, w, p);
            else
                for (h = m.call(_),
                f = h.next,
                e = new v; !(l = f.call(h)).done; w++)
                    p = y ? o(h, g, [l.value, w], !0) : l.value,
                    s(e, w, p);
            return e.length = w,
            e
        }
    },
    8418: function(t, n, e) {
        "use strict";
        var r = e("c04e")
          , i = e("9bf2")
          , o = e("5c6c");
        t.exports = function(t, n, e) {
            var u = r(n);
            u in t ? i.f(t, u, o(0, e)) : t[u] = e
        }
    },
    8584: function(t, n, e) {
        "use strict";
        var r = function() {
            var t = this
              , n = t.$createElement
              , e = t._self._c || n;
            return e("font-awesome-icon", {
                class: t.faSizeClass,
                style: t.colorStyle,
                attrs: {
                    icon: t.PUZZLE_PIECE_INNER_ICON,
                    transform: t.SHRINK_FIT_INSIDE,
                    mask: t.CIRCLE_OUTER_ICON
                }
            })
        }
          , i = []
          , o = (e("dca8"),
        {
            name: "BaseCustomIconCirclePuzzle",
            props: {
                colorStyle: {
                    type: Object
                },
                faSizeClass: {
                    type: String,
                    default: "fa-2x"
                }
            },
            created: function() {
                this.CIRCLE_OUTER_ICON = Object.freeze(["fas", "circle"]),
                this.PUZZLE_PIECE_INNER_ICON = Object.freeze(["fas", "puzzle-piece"]),
                this.SHRINK_FIT_INSIDE = "shrink-8"
            }
        })
          , u = o
          , a = e("2877")
          , s = Object(a["a"])(u, r, i, !1, null, null, null);
        n["a"] = s.exports
    },
    "91bd": function(t, n, e) {
        "use strict";
        e.r(n);
        var r = function() {
            var t = this
              , n = t.$createElement
              , e = t._self._c || n;
            return e("svg", {
                attrs: {
                    width: t.width,
                    height: t.height,
                    viewBox: t.viewBoxFitToMaxDim
                }
            }, [e("g", {
                style: t.transformFitToMaxDim
            }, [e("path", {
                staticClass: "puzzle-svg_path",
                attrs: {
                    d: t.svgPath,
                    alt: t.alt
                }
            })])])
        }
          , i = []
          , o = (e("99af"),
        e("a9e3"),
        e("ac1f"),
        e("5319"),
        e("498a"),
        e("27a8"));
        function u(t) {
            var n = /M|z/g
              , e = t.trim().replace(n, "")
              , r = Object(o["a"])(e);
            return r
        }
        function a(t) {
            var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1
              , e = t.x
              , r = t.y;
            return {
                x: n * e,
                y: n * r
            }
        }
        e("4160"),
        e("159b");
        function s(t) {
            var n = {
                x: -1 / 0,
                y: -1 / 0
            };
            return t.forEach((function(t) {
                t.x > n.x && (n.x = t.x),
                t.y > n.y && (n.y = t.y)
            }
            )),
            n
        }
        var c = {
            props: {
                width: {
                    type: Number,
                    default: 150
                },
                height: {
                    type: Number,
                    default: 150
                },
                svgPath: {
                    type: String,
                    required: !0
                },
                alt: {
                    type: String,
                    default: "Puzzle Image"
                }
            },
            computed: {
                boundingBox: function() {
                    var t = u(this.svgPath)
                      , n = s(t);
                    return n
                },
                scale: function() {
                    var t = this.boundingBox
                      , n = t.x
                      , e = t.y
                      , r = Math.max(n, e)
                      , i = .01
                      , o = r > i ? r : i
                      , u = 1 / o;
                    return u
                },
                viewBoxFitToMaxDim: function() {
                    var t = 0
                      , n = 0
                      , e = a(this.boundingBox, this.scale)
                      , r = e.x
                      , i = e.y
                      , o = {
                        MIN_X: t,
                        MIN_Y: n,
                        width: r,
                        height: i
                    };
                    return "".concat(o.MIN_X, " ").concat(o.MIN_Y, " ") + "".concat(o.width, " ").concat(o.height)
                },
                scaleToMaxDim: function() {
                    return "scale(".concat(this.scale, ")")
                },
                transformFitToMaxDim: function() {
                    return {
                        transform: this.scaleToMaxDim
                    }
                }
            }
        }
          , l = c
          , h = (e("ef4d"),
        e("2877"))
          , f = Object(h["a"])(l, r, i, !1, null, "75747cce", null);
        n["default"] = f.exports
    },
    "950f": function(t, n, e) {},
    "99af": function(t, n, e) {
        "use strict";
        var r = e("23e7")
          , i = e("d039")
          , o = e("e8b5")
          , u = e("861d")
          , a = e("7b0b")
          , s = e("50c4")
          , c = e("8418")
          , l = e("65f0")
          , h = e("1dde")
          , f = e("b622")
          , p = e("2d00")
          , _ = f("isConcatSpreadable")
          , v = 9007199254740991
          , d = "Maximum allowed index exceeded"
          , g = p >= 51 || !i((function() {
            var t = [];
            return t[_] = !1,
            t.concat()[0] !== t
        }
        ))
          , y = h("concat")
          , m = function(t) {
            if (!u(t))
                return !1;
            var n = t[_];
            return void 0 !== n ? !!n : o(t)
        }
          , w = !g || !y;
        r({
            target: "Array",
            proto: !0,
            forced: w
        }, {
            concat: function(t) {
                var n, e, r, i, o, u = a(this), h = l(u, 0), f = 0;
                for (n = -1,
                r = arguments.length; n < r; n++)
                    if (o = -1 === n ? u : arguments[n],
                    m(o)) {
                        if (i = s(o.length),
                        f + i > v)
                            throw TypeError(d);
                        for (e = 0; e < i; e++,
                        f++)
                            e in o && c(h, f, o[e])
                    } else {
                        if (f >= v)
                            throw TypeError(d);
                        c(h, f++, o)
                    }
                return h.length = f,
                h
            }
        })
    },
    "9bdd": function(t, n, e) {
        var r = e("825a")
          , i = e("2a62");
        t.exports = function(t, n, e, o) {
            try {
                return o ? n(r(e)[0], e[1]) : n(e)
            } catch (u) {
                throw i(t),
                u
            }
        }
    },
    a630: function(t, n, e) {
        var r = e("23e7")
          , i = e("4df4")
          , o = e("1c7e")
          , u = !o((function(t) {
            Array.from(t)
        }
        ));
        r({
            target: "Array",
            stat: !0,
            forced: u
        }, {
            from: i
        })
    },
    a6305: function(t, n, e) {
        "use strict";
        var r = e("143c")
          , i = e.n(r);
        i.a
    },
    a640: function(t, n, e) {
        "use strict";
        var r = e("d039");
        t.exports = function(t, n) {
            var e = [][t];
            return !!e && r((function() {
                e.call(null, n || function() {
                    throw 1
                }
                , 1)
            }
            ))
        }
    },
    a9e3: function(t, n, e) {
        "use strict";
        var r = e("83ab")
          , i = e("da84")
          , o = e("94ca")
          , u = e("6eeb")
          , a = e("5135")
          , s = e("c6b6")
          , c = e("7156")
          , l = e("c04e")
          , h = e("d039")
          , f = e("7c73")
          , p = e("241c").f
          , _ = e("06cf").f
          , v = e("9bf2").f
          , d = e("58a8").trim
          , g = "Number"
          , y = i[g]
          , m = y.prototype
          , w = s(f(m)) == g
          , x = function(t) {
            var n, e, r, i, o, u, a, s, c = l(t, !1);
            if ("string" == typeof c && c.length > 2)
                if (c = d(c),
                n = c.charCodeAt(0),
                43 === n || 45 === n) {
                    if (e = c.charCodeAt(2),
                    88 === e || 120 === e)
                        return NaN
                } else if (48 === n) {
                    switch (c.charCodeAt(1)) {
                    case 66:
                    case 98:
                        r = 2,
                        i = 49;
                        break;
                    case 79:
                    case 111:
                        r = 8,
                        i = 55;
                        break;
                    default:
                        return +c
                    }
                    for (o = c.slice(2),
                    u = o.length,
                    a = 0; a < u; a++)
                        if (s = o.charCodeAt(a),
                        s < 48 || s > i)
                            return NaN;
                    return parseInt(o, r)
                }
            return +c
        };
        if (o(g, !y(" 0o1") || !y("0b1") || y("+0x1"))) {
            for (var b, A = function(t) {
                var n = arguments.length < 1 ? 0 : t
                  , e = this;
                return e instanceof A && (w ? h((function() {
                    m.valueOf.call(e)
                }
                )) : s(e) != g) ? c(new y(x(n)), e, A) : x(n)
            }, M = r ? p(y) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","), C = 0; M.length > C; C++)
                a(y, b = M[C]) && !a(A, b) && v(A, b, _(y, b));
            A.prototype = m,
            m.constructor = A,
            u(i, g, A)
        }
    },
    cbb4: function(t, n, e) {
        "use strict";
        e.r(n);
        var r = function() {
            var t = this
              , n = t.$createElement
              , e = t._self._c || n;
            return e("div", {
                staticClass: "svg-board"
            })
        }
          , i = []
          , o = (e("a4d3"),
        e("e01a"),
        e("d28b"),
        e("99af"),
        e("4de4"),
        e("4160"),
        e("a630"),
        e("d81d"),
        e("d3b7"),
        e("6062"),
        e("3ca3"),
        e("159b"),
        e("ddb0"),
        e("2909"))
          , u = (e("96cf"),
        e("1da1"))
          , a = e("5530")
          , s = function(t, n) {
            return t < n ? -1 : t > n ? 1 : t >= n ? 0 : NaN
        }
          , c = function(t) {
            return 1 === t.length && (t = l(t)),
            {
                left: function(n, e, r, i) {
                    null == r && (r = 0),
                    null == i && (i = n.length);
                    while (r < i) {
                        var o = r + i >>> 1;
                        t(n[o], e) < 0 ? r = o + 1 : i = o
                    }
                    return r
                },
                right: function(n, e, r, i) {
                    null == r && (r = 0),
                    null == i && (i = n.length);
                    while (r < i) {
                        var o = r + i >>> 1;
                        t(n[o], e) > 0 ? i = o : r = o + 1
                    }
                    return r
                }
            }
        };
        function l(t) {
            return function(n, e) {
                return s(t(n), e)
            }
        }
        var h = c(s);
        h.right,
        h.left;
        var f = Array.prototype;
        f.slice,
        f.map,
        Math.sqrt(50),
        Math.sqrt(10),
        Math.sqrt(2);
        Array.prototype.slice;
        var p = {
            value: function() {}
        };
        function _() {
            for (var t, n = 0, e = arguments.length, r = {}; n < e; ++n) {
                if (!(t = arguments[n] + "") || t in r || /[\s.]/.test(t))
                    throw new Error("illegal type: " + t);
                r[t] = []
            }
            return new v(r)
        }
        function v(t) {
            this._ = t
        }
        function d(t, n) {
            return t.trim().split(/^|\s+/).map((function(t) {
                var e = ""
                  , r = t.indexOf(".");
                if (r >= 0 && (e = t.slice(r + 1),
                t = t.slice(0, r)),
                t && !n.hasOwnProperty(t))
                    throw new Error("unknown type: " + t);
                return {
                    type: t,
                    name: e
                }
            }
            ))
        }
        function g(t, n) {
            for (var e, r = 0, i = t.length; r < i; ++r)
                if ((e = t[r]).name === n)
                    return e.value
        }
        function y(t, n, e) {
            for (var r = 0, i = t.length; r < i; ++r)
                if (t[r].name === n) {
                    t[r] = p,
                    t = t.slice(0, r).concat(t.slice(r + 1));
                    break
                }
            return null != e && t.push({
                name: n,
                value: e
            }),
            t
        }
        v.prototype = _.prototype = {
            constructor: v,
            on: function(t, n) {
                var e, r = this._, i = d(t + "", r), o = -1, u = i.length;
                if (!(arguments.length < 2)) {
                    if (null != n && "function" !== typeof n)
                        throw new Error("invalid callback: " + n);
                    while (++o < u)
                        if (e = (t = i[o]).type)
                            r[e] = y(r[e], t.name, n);
                        else if (null == n)
                            for (e in r)
                                r[e] = y(r[e], t.name, null);
                    return this
                }
                while (++o < u)
                    if ((e = (t = i[o]).type) && (e = g(r[e], t.name)))
                        return e
            },
            copy: function() {
                var t = {}
                  , n = this._;
                for (var e in n)
                    t[e] = n[e].slice();
                return new v(t)
            },
            call: function(t, n) {
                if ((e = arguments.length - 2) > 0)
                    for (var e, r, i = new Array(e), o = 0; o < e; ++o)
                        i[o] = arguments[o + 2];
                if (!this._.hasOwnProperty(t))
                    throw new Error("unknown type: " + t);
                for (r = this._[t],
                o = 0,
                e = r.length; o < e; ++o)
                    r[o].value.apply(n, i)
            },
            apply: function(t, n, e) {
                if (!this._.hasOwnProperty(t))
                    throw new Error("unknown type: " + t);
                for (var r = this._[t], i = 0, o = r.length; i < o; ++i)
                    r[i].value.apply(n, e)
            }
        };
        var m = _;
        function w() {}
        var x = function(t) {
            return null == t ? w : function() {
                return this.querySelector(t)
            }
        }
          , b = function(t) {
            "function" !== typeof t && (t = x(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u, a = n[i], s = a.length, c = r[i] = new Array(s), l = 0; l < s; ++l)
                    (o = a[l]) && (u = t.call(o, o.__data__, l, a)) && ("__data__"in o && (u.__data__ = o.__data__),
                    c[l] = u);
            return new sn(r,this._parents)
        };
        function A() {
            return []
        }
        var M = function(t) {
            return null == t ? A : function() {
                return this.querySelectorAll(t)
            }
        }
          , C = function(t) {
            "function" !== typeof t && (t = M(t));
            for (var n = this._groups, e = n.length, r = [], i = [], o = 0; o < e; ++o)
                for (var u, a = n[o], s = a.length, c = 0; c < s; ++c)
                    (u = a[c]) && (r.push(t.call(u, u.__data__, c, a)),
                    i.push(u));
            return new sn(r,i)
        }
          , N = function(t) {
            return function() {
                return this.matches(t)
            }
        }
          , T = function(t) {
            "function" !== typeof t && (t = N(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u = n[i], a = u.length, s = r[i] = [], c = 0; c < a; ++c)
                    (o = u[c]) && t.call(o, o.__data__, c, u) && s.push(o);
            return new sn(r,this._parents)
        }
          , S = function(t) {
            return new Array(t.length)
        }
          , E = function() {
            return new sn(this._enter || this._groups.map(S),this._parents)
        };
        function k(t, n) {
            this.ownerDocument = t.ownerDocument,
            this.namespaceURI = t.namespaceURI,
            this._next = null,
            this._parent = t,
            this.__data__ = n
        }
        k.prototype = {
            constructor: k,
            appendChild: function(t) {
                return this._parent.insertBefore(t, this._next)
            },
            insertBefore: function(t, n) {
                return this._parent.insertBefore(t, n)
            },
            querySelector: function(t) {
                return this._parent.querySelector(t)
            },
            querySelectorAll: function(t) {
                return this._parent.querySelectorAll(t)
            }
        };
        var P = function(t) {
            return function() {
                return t
            }
        }
          , U = "$";
        function L(t, n, e, r, i, o) {
            for (var u, a = 0, s = n.length, c = o.length; a < c; ++a)
                (u = n[a]) ? (u.__data__ = o[a],
                r[a] = u) : e[a] = new k(t,o[a]);
            for (; a < s; ++a)
                (u = n[a]) && (i[a] = u)
        }
        function D(t, n, e, r, i, o, u) {
            var a, s, c, l = {}, h = n.length, f = o.length, p = new Array(h);
            for (a = 0; a < h; ++a)
                (s = n[a]) && (p[a] = c = U + u.call(s, s.__data__, a, n),
                c in l ? i[a] = s : l[c] = s);
            for (a = 0; a < f; ++a)
                c = U + u.call(t, o[a], a, o),
                (s = l[c]) ? (r[a] = s,
                s.__data__ = o[a],
                l[c] = null) : e[a] = new k(t,o[a]);
            for (a = 0; a < h; ++a)
                (s = n[a]) && l[p[a]] === s && (i[a] = s)
        }
        var z = function(t, n) {
            if (!t)
                return p = new Array(this.size()),
                c = -1,
                this.each((function(t) {
                    p[++c] = t
                }
                )),
                p;
            var e = n ? D : L
              , r = this._parents
              , i = this._groups;
            "function" !== typeof t && (t = P(t));
            for (var o = i.length, u = new Array(o), a = new Array(o), s = new Array(o), c = 0; c < o; ++c) {
                var l = r[c]
                  , h = i[c]
                  , f = h.length
                  , p = t.call(l, l && l.__data__, c, r)
                  , _ = p.length
                  , v = a[c] = new Array(_)
                  , d = u[c] = new Array(_)
                  , g = s[c] = new Array(f);
                e(l, h, v, d, g, p, n);
                for (var y, m, w = 0, x = 0; w < _; ++w)
                    if (y = v[w]) {
                        w >= x && (x = w + 1);
                        while (!(m = d[x]) && ++x < _)
                            ;
                        y._next = m || null
                    }
            }
            return u = new sn(u,r),
            u._enter = a,
            u._exit = s,
            u
        }
          , O = function() {
            return new sn(this._exit || this._groups.map(S),this._parents)
        }
          , R = function(t, n, e) {
            var r = this.enter()
              , i = this
              , o = this.exit();
            return r = "function" === typeof t ? t(r) : r.append(t + ""),
            null != n && (i = n(i)),
            null == e ? o.remove() : e(o),
            r && i ? r.merge(i).order() : i
        }
          , I = function(t) {
            for (var n = this._groups, e = t._groups, r = n.length, i = e.length, o = Math.min(r, i), u = new Array(r), a = 0; a < o; ++a)
                for (var s, c = n[a], l = e[a], h = c.length, f = u[a] = new Array(h), p = 0; p < h; ++p)
                    (s = c[p] || l[p]) && (f[p] = s);
            for (; a < r; ++a)
                u[a] = n[a];
            return new sn(u,this._parents)
        }
          , j = function() {
            for (var t = this._groups, n = -1, e = t.length; ++n < e; )
                for (var r, i = t[n], o = i.length - 1, u = i[o]; --o >= 0; )
                    (r = i[o]) && (u && 4 ^ r.compareDocumentPosition(u) && u.parentNode.insertBefore(r, u),
                    u = r);
            return this
        }
          , F = function(t) {
            function n(n, e) {
                return n && e ? t(n.__data__, e.__data__) : !n - !e
            }
            t || (t = Y);
            for (var e = this._groups, r = e.length, i = new Array(r), o = 0; o < r; ++o) {
                for (var u, a = e[o], s = a.length, c = i[o] = new Array(s), l = 0; l < s; ++l)
                    (u = a[l]) && (c[l] = u);
                c.sort(n)
            }
            return new sn(i,this._parents).order()
        };
        function Y(t, n) {
            return t < n ? -1 : t > n ? 1 : t >= n ? 0 : NaN
        }
        var H = function() {
            var t = arguments[0];
            return arguments[0] = this,
            t.apply(null, arguments),
            this
        }
          , q = function() {
            var t = new Array(this.size())
              , n = -1;
            return this.each((function() {
                t[++n] = this
            }
            )),
            t
        }
          , B = function() {
            for (var t = this._groups, n = 0, e = t.length; n < e; ++n)
                for (var r = t[n], i = 0, o = r.length; i < o; ++i) {
                    var u = r[i];
                    if (u)
                        return u
                }
            return null
        }
          , X = function() {
            var t = 0;
            return this.each((function() {
                ++t
            }
            )),
            t
        }
          , V = function() {
            return !this.node()
        }
          , $ = function(t) {
            for (var n = this._groups, e = 0, r = n.length; e < r; ++e)
                for (var i, o = n[e], u = 0, a = o.length; u < a; ++u)
                    (i = o[u]) && t.call(i, i.__data__, u, o);
            return this
        }
          , Z = "http://www.w3.org/1999/xhtml"
          , G = {
            svg: "http://www.w3.org/2000/svg",
            xhtml: Z,
            xlink: "http://www.w3.org/1999/xlink",
            xml: "http://www.w3.org/XML/1998/namespace",
            xmlns: "http://www.w3.org/2000/xmlns/"
        }
          , W = function(t) {
            var n = t += ""
              , e = n.indexOf(":");
            return e >= 0 && "xmlns" !== (n = t.slice(0, e)) && (t = t.slice(e + 1)),
            G.hasOwnProperty(n) ? {
                space: G[n],
                local: t
            } : t
        };
        function J(t) {
            return function() {
                this.removeAttribute(t)
            }
        }
        function Q(t) {
            return function() {
                this.removeAttributeNS(t.space, t.local)
            }
        }
        function K(t, n) {
            return function() {
                this.setAttribute(t, n)
            }
        }
        function tt(t, n) {
            return function() {
                this.setAttributeNS(t.space, t.local, n)
            }
        }
        function nt(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? this.removeAttribute(t) : this.setAttribute(t, e)
            }
        }
        function et(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? this.removeAttributeNS(t.space, t.local) : this.setAttributeNS(t.space, t.local, e)
            }
        }
        var rt = function(t, n) {
            var e = W(t);
            if (arguments.length < 2) {
                var r = this.node();
                return e.local ? r.getAttributeNS(e.space, e.local) : r.getAttribute(e)
            }
            return this.each((null == n ? e.local ? Q : J : "function" === typeof n ? e.local ? et : nt : e.local ? tt : K)(e, n))
        }
          , it = function(t) {
            return t.ownerDocument && t.ownerDocument.defaultView || t.document && t || t.defaultView
        };
        function ot(t) {
            return function() {
                this.style.removeProperty(t)
            }
        }
        function ut(t, n, e) {
            return function() {
                this.style.setProperty(t, n, e)
            }
        }
        function at(t, n, e) {
            return function() {
                var r = n.apply(this, arguments);
                null == r ? this.style.removeProperty(t) : this.style.setProperty(t, r, e)
            }
        }
        var st = function(t, n, e) {
            return arguments.length > 1 ? this.each((null == n ? ot : "function" === typeof n ? at : ut)(t, n, null == e ? "" : e)) : ct(this.node(), t)
        };
        function ct(t, n) {
            return t.style.getPropertyValue(n) || it(t).getComputedStyle(t, null).getPropertyValue(n)
        }
        function lt(t) {
            return function() {
                delete this[t]
            }
        }
        function ht(t, n) {
            return function() {
                this[t] = n
            }
        }
        function ft(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? delete this[t] : this[t] = e
            }
        }
        var pt = function(t, n) {
            return arguments.length > 1 ? this.each((null == n ? lt : "function" === typeof n ? ft : ht)(t, n)) : this.node()[t]
        };
        function _t(t) {
            return t.trim().split(/^|\s+/)
        }
        function vt(t) {
            return t.classList || new dt(t)
        }
        function dt(t) {
            this._node = t,
            this._names = _t(t.getAttribute("class") || "")
        }
        function gt(t, n) {
            var e = vt(t)
              , r = -1
              , i = n.length;
            while (++r < i)
                e.add(n[r])
        }
        function yt(t, n) {
            var e = vt(t)
              , r = -1
              , i = n.length;
            while (++r < i)
                e.remove(n[r])
        }
        function mt(t) {
            return function() {
                gt(this, t)
            }
        }
        function wt(t) {
            return function() {
                yt(this, t)
            }
        }
        function xt(t, n) {
            return function() {
                (n.apply(this, arguments) ? gt : yt)(this, t)
            }
        }
        dt.prototype = {
            add: function(t) {
                var n = this._names.indexOf(t);
                n < 0 && (this._names.push(t),
                this._node.setAttribute("class", this._names.join(" ")))
            },
            remove: function(t) {
                var n = this._names.indexOf(t);
                n >= 0 && (this._names.splice(n, 1),
                this._node.setAttribute("class", this._names.join(" ")))
            },
            contains: function(t) {
                return this._names.indexOf(t) >= 0
            }
        };
        var bt = function(t, n) {
            var e = _t(t + "");
            if (arguments.length < 2) {
                var r = vt(this.node())
                  , i = -1
                  , o = e.length;
                while (++i < o)
                    if (!r.contains(e[i]))
                        return !1;
                return !0
            }
            return this.each(("function" === typeof n ? xt : n ? mt : wt)(e, n))
        };
        function At() {
            this.textContent = ""
        }
        function Mt(t) {
            return function() {
                this.textContent = t
            }
        }
        function Ct(t) {
            return function() {
                var n = t.apply(this, arguments);
                this.textContent = null == n ? "" : n
            }
        }
        var Nt = function(t) {
            return arguments.length ? this.each(null == t ? At : ("function" === typeof t ? Ct : Mt)(t)) : this.node().textContent
        };
        function Tt() {
            this.innerHTML = ""
        }
        function St(t) {
            return function() {
                this.innerHTML = t
            }
        }
        function Et(t) {
            return function() {
                var n = t.apply(this, arguments);
                this.innerHTML = null == n ? "" : n
            }
        }
        var kt = function(t) {
            return arguments.length ? this.each(null == t ? Tt : ("function" === typeof t ? Et : St)(t)) : this.node().innerHTML
        };
        function Pt() {
            this.nextSibling && this.parentNode.appendChild(this)
        }
        var Ut = function() {
            return this.each(Pt)
        };
        function Lt() {
            this.previousSibling && this.parentNode.insertBefore(this, this.parentNode.firstChild)
        }
        var Dt = function() {
            return this.each(Lt)
        };
        function zt(t) {
            return function() {
                var n = this.ownerDocument
                  , e = this.namespaceURI;
                return e === Z && n.documentElement.namespaceURI === Z ? n.createElement(t) : n.createElementNS(e, t)
            }
        }
        function Ot(t) {
            return function() {
                return this.ownerDocument.createElementNS(t.space, t.local)
            }
        }
        var Rt = function(t) {
            var n = W(t);
            return (n.local ? Ot : zt)(n)
        }
          , It = function(t) {
            var n = "function" === typeof t ? t : Rt(t);
            return this.select((function() {
                return this.appendChild(n.apply(this, arguments))
            }
            ))
        };
        function jt() {
            return null
        }
        var Ft = function(t, n) {
            var e = "function" === typeof t ? t : Rt(t)
              , r = null == n ? jt : "function" === typeof n ? n : x(n);
            return this.select((function() {
                return this.insertBefore(e.apply(this, arguments), r.apply(this, arguments) || null)
            }
            ))
        };
        function Yt() {
            var t = this.parentNode;
            t && t.removeChild(this)
        }
        var Ht = function() {
            return this.each(Yt)
        };
        function qt() {
            var t = this.cloneNode(!1)
              , n = this.parentNode;
            return n ? n.insertBefore(t, this.nextSibling) : t
        }
        function Bt() {
            var t = this.cloneNode(!0)
              , n = this.parentNode;
            return n ? n.insertBefore(t, this.nextSibling) : t
        }
        var Xt = function(t) {
            return this.select(t ? Bt : qt)
        }
          , Vt = function(t) {
            return arguments.length ? this.property("__data__", t) : this.node().__data__
        }
          , $t = {}
          , Zt = null;
        if ("undefined" !== typeof document) {
            var Gt = document.documentElement;
            "onmouseenter"in Gt || ($t = {
                mouseenter: "mouseover",
                mouseleave: "mouseout"
            })
        }
        function Wt(t, n, e) {
            return t = Jt(t, n, e),
            function(n) {
                var e = n.relatedTarget;
                e && (e === this || 8 & e.compareDocumentPosition(this)) || t.call(this, n)
            }
        }
        function Jt(t, n, e) {
            return function(r) {
                var i = Zt;
                Zt = r;
                try {
                    t.call(this, this.__data__, n, e)
                } finally {
                    Zt = i
                }
            }
        }
        function Qt(t) {
            return t.trim().split(/^|\s+/).map((function(t) {
                var n = ""
                  , e = t.indexOf(".");
                return e >= 0 && (n = t.slice(e + 1),
                t = t.slice(0, e)),
                {
                    type: t,
                    name: n
                }
            }
            ))
        }
        function Kt(t) {
            return function() {
                var n = this.__on;
                if (n) {
                    for (var e, r = 0, i = -1, o = n.length; r < o; ++r)
                        e = n[r],
                        t.type && e.type !== t.type || e.name !== t.name ? n[++i] = e : this.removeEventListener(e.type, e.listener, e.capture);
                    ++i ? n.length = i : delete this.__on
                }
            }
        }
        function tn(t, n, e) {
            var r = $t.hasOwnProperty(t.type) ? Wt : Jt;
            return function(i, o, u) {
                var a, s = this.__on, c = r(n, o, u);
                if (s)
                    for (var l = 0, h = s.length; l < h; ++l)
                        if ((a = s[l]).type === t.type && a.name === t.name)
                            return this.removeEventListener(a.type, a.listener, a.capture),
                            this.addEventListener(a.type, a.listener = c, a.capture = e),
                            void (a.value = n);
                this.addEventListener(t.type, c, e),
                a = {
                    type: t.type,
                    name: t.name,
                    value: n,
                    listener: c,
                    capture: e
                },
                s ? s.push(a) : this.__on = [a]
            }
        }
        var nn = function(t, n, e) {
            var r, i, o = Qt(t + ""), u = o.length;
            if (!(arguments.length < 2)) {
                for (a = n ? tn : Kt,
                null == e && (e = !1),
                r = 0; r < u; ++r)
                    this.each(a(o[r], n, e));
                return this
            }
            var a = this.node().__on;
            if (a)
                for (var s, c = 0, l = a.length; c < l; ++c)
                    for (r = 0,
                    s = a[c]; r < u; ++r)
                        if ((i = o[r]).type === s.type && i.name === s.name)
                            return s.value
        };
        function en(t, n, e) {
            var r = it(t)
              , i = r.CustomEvent;
            "function" === typeof i ? i = new i(n,e) : (i = r.document.createEvent("Event"),
            e ? (i.initEvent(n, e.bubbles, e.cancelable),
            i.detail = e.detail) : i.initEvent(n, !1, !1)),
            t.dispatchEvent(i)
        }
        function rn(t, n) {
            return function() {
                return en(this, t, n)
            }
        }
        function on(t, n) {
            return function() {
                return en(this, t, n.apply(this, arguments))
            }
        }
        var un = function(t, n) {
            return this.each(("function" === typeof n ? on : rn)(t, n))
        }
          , an = [null];
        function sn(t, n) {
            this._groups = t,
            this._parents = n
        }
        function cn() {
            return new sn([[document.documentElement]],an)
        }
        sn.prototype = cn.prototype = {
            constructor: sn,
            select: b,
            selectAll: C,
            filter: T,
            data: z,
            enter: E,
            exit: O,
            join: R,
            merge: I,
            order: j,
            sort: F,
            call: H,
            nodes: q,
            node: B,
            size: X,
            empty: V,
            each: $,
            attr: rt,
            style: st,
            property: pt,
            classed: bt,
            text: Nt,
            html: kt,
            raise: Ut,
            lower: Dt,
            append: It,
            insert: Ft,
            remove: Ht,
            clone: Xt,
            datum: Vt,
            on: nn,
            dispatch: un
        };
        var ln = cn;
        var hn = function(t, n, e) {
            t.prototype = n.prototype = e,
            e.constructor = t
        };
        function fn(t, n) {
            var e = Object.create(t.prototype);
            for (var r in n)
                e[r] = n[r];
            return e
        }
        function pn() {}
        var _n = .7
          , vn = 1 / _n
          , dn = "\\s*([+-]?\\d+)\\s*"
          , gn = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)\\s*"
          , yn = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)%\\s*"
          , mn = /^#([0-9a-f]{3,8})$/
          , wn = new RegExp("^rgb\\(" + [dn, dn, dn] + "\\)$")
          , xn = new RegExp("^rgb\\(" + [yn, yn, yn] + "\\)$")
          , bn = new RegExp("^rgba\\(" + [dn, dn, dn, gn] + "\\)$")
          , An = new RegExp("^rgba\\(" + [yn, yn, yn, gn] + "\\)$")
          , Mn = new RegExp("^hsl\\(" + [gn, yn, yn] + "\\)$")
          , Cn = new RegExp("^hsla\\(" + [gn, yn, yn, gn] + "\\)$")
          , Nn = {
            aliceblue: 15792383,
            antiquewhite: 16444375,
            aqua: 65535,
            aquamarine: 8388564,
            azure: 15794175,
            beige: 16119260,
            bisque: 16770244,
            black: 0,
            blanchedalmond: 16772045,
            blue: 255,
            blueviolet: 9055202,
            brown: 10824234,
            burlywood: 14596231,
            cadetblue: 6266528,
            chartreuse: 8388352,
            chocolate: 13789470,
            coral: 16744272,
            cornflowerblue: 6591981,
            cornsilk: 16775388,
            crimson: 14423100,
            cyan: 65535,
            darkblue: 139,
            darkcyan: 35723,
            darkgoldenrod: 12092939,
            darkgray: 11119017,
            darkgreen: 25600,
            darkgrey: 11119017,
            darkkhaki: 12433259,
            darkmagenta: 9109643,
            darkolivegreen: 5597999,
            darkorange: 16747520,
            darkorchid: 10040012,
            darkred: 9109504,
            darksalmon: 15308410,
            darkseagreen: 9419919,
            darkslateblue: 4734347,
            darkslategray: 3100495,
            darkslategrey: 3100495,
            darkturquoise: 52945,
            darkviolet: 9699539,
            deeppink: 16716947,
            deepskyblue: 49151,
            dimgray: 6908265,
            dimgrey: 6908265,
            dodgerblue: 2003199,
            firebrick: 11674146,
            floralwhite: 16775920,
            forestgreen: 2263842,
            fuchsia: 16711935,
            gainsboro: 14474460,
            ghostwhite: 16316671,
            gold: 16766720,
            goldenrod: 14329120,
            gray: 8421504,
            green: 32768,
            greenyellow: 11403055,
            grey: 8421504,
            honeydew: 15794160,
            hotpink: 16738740,
            indianred: 13458524,
            indigo: 4915330,
            ivory: 16777200,
            khaki: 15787660,
            lavender: 15132410,
            lavenderblush: 16773365,
            lawngreen: 8190976,
            lemonchiffon: 16775885,
            lightblue: 11393254,
            lightcoral: 15761536,
            lightcyan: 14745599,
            lightgoldenrodyellow: 16448210,
            lightgray: 13882323,
            lightgreen: 9498256,
            lightgrey: 13882323,
            lightpink: 16758465,
            lightsalmon: 16752762,
            lightseagreen: 2142890,
            lightskyblue: 8900346,
            lightslategray: 7833753,
            lightslategrey: 7833753,
            lightsteelblue: 11584734,
            lightyellow: 16777184,
            lime: 65280,
            limegreen: 3329330,
            linen: 16445670,
            magenta: 16711935,
            maroon: 8388608,
            mediumaquamarine: 6737322,
            mediumblue: 205,
            mediumorchid: 12211667,
            mediumpurple: 9662683,
            mediumseagreen: 3978097,
            mediumslateblue: 8087790,
            mediumspringgreen: 64154,
            mediumturquoise: 4772300,
            mediumvioletred: 13047173,
            midnightblue: 1644912,
            mintcream: 16121850,
            mistyrose: 16770273,
            moccasin: 16770229,
            navajowhite: 16768685,
            navy: 128,
            oldlace: 16643558,
            olive: 8421376,
            olivedrab: 7048739,
            orange: 16753920,
            orangered: 16729344,
            orchid: 14315734,
            palegoldenrod: 15657130,
            palegreen: 10025880,
            paleturquoise: 11529966,
            palevioletred: 14381203,
            papayawhip: 16773077,
            peachpuff: 16767673,
            peru: 13468991,
            pink: 16761035,
            plum: 14524637,
            powderblue: 11591910,
            purple: 8388736,
            rebeccapurple: 6697881,
            red: 16711680,
            rosybrown: 12357519,
            royalblue: 4286945,
            saddlebrown: 9127187,
            salmon: 16416882,
            sandybrown: 16032864,
            seagreen: 3050327,
            seashell: 16774638,
            sienna: 10506797,
            silver: 12632256,
            skyblue: 8900331,
            slateblue: 6970061,
            slategray: 7372944,
            slategrey: 7372944,
            snow: 16775930,
            springgreen: 65407,
            steelblue: 4620980,
            tan: 13808780,
            teal: 32896,
            thistle: 14204888,
            tomato: 16737095,
            turquoise: 4251856,
            violet: 15631086,
            wheat: 16113331,
            white: 16777215,
            whitesmoke: 16119285,
            yellow: 16776960,
            yellowgreen: 10145074
        };
        function Tn() {
            return this.rgb().formatHex()
        }
        function Sn() {
            return Fn(this).formatHsl()
        }
        function En() {
            return this.rgb().formatRgb()
        }
        function kn(t) {
            var n, e;
            return t = (t + "").trim().toLowerCase(),
            (n = mn.exec(t)) ? (e = n[1].length,
            n = parseInt(n[1], 16),
            6 === e ? Pn(n) : 3 === e ? new zn(n >> 8 & 15 | n >> 4 & 240,n >> 4 & 15 | 240 & n,(15 & n) << 4 | 15 & n,1) : 8 === e ? Un(n >> 24 & 255, n >> 16 & 255, n >> 8 & 255, (255 & n) / 255) : 4 === e ? Un(n >> 12 & 15 | n >> 8 & 240, n >> 8 & 15 | n >> 4 & 240, n >> 4 & 15 | 240 & n, ((15 & n) << 4 | 15 & n) / 255) : null) : (n = wn.exec(t)) ? new zn(n[1],n[2],n[3],1) : (n = xn.exec(t)) ? new zn(255 * n[1] / 100,255 * n[2] / 100,255 * n[3] / 100,1) : (n = bn.exec(t)) ? Un(n[1], n[2], n[3], n[4]) : (n = An.exec(t)) ? Un(255 * n[1] / 100, 255 * n[2] / 100, 255 * n[3] / 100, n[4]) : (n = Mn.exec(t)) ? jn(n[1], n[2] / 100, n[3] / 100, 1) : (n = Cn.exec(t)) ? jn(n[1], n[2] / 100, n[3] / 100, n[4]) : Nn.hasOwnProperty(t) ? Pn(Nn[t]) : "transparent" === t ? new zn(NaN,NaN,NaN,0) : null
        }
        function Pn(t) {
            return new zn(t >> 16 & 255,t >> 8 & 255,255 & t,1)
        }
        function Un(t, n, e, r) {
            return r <= 0 && (t = n = e = NaN),
            new zn(t,n,e,r)
        }
        function Ln(t) {
            return t instanceof pn || (t = kn(t)),
            t ? (t = t.rgb(),
            new zn(t.r,t.g,t.b,t.opacity)) : new zn
        }
        function Dn(t, n, e, r) {
            return 1 === arguments.length ? Ln(t) : new zn(t,n,e,null == r ? 1 : r)
        }
        function zn(t, n, e, r) {
            this.r = +t,
            this.g = +n,
            this.b = +e,
            this.opacity = +r
        }
        function On() {
            return "#" + In(this.r) + In(this.g) + In(this.b)
        }
        function Rn() {
            var t = this.opacity;
            return t = isNaN(t) ? 1 : Math.max(0, Math.min(1, t)),
            (1 === t ? "rgb(" : "rgba(") + Math.max(0, Math.min(255, Math.round(this.r) || 0)) + ", " + Math.max(0, Math.min(255, Math.round(this.g) || 0)) + ", " + Math.max(0, Math.min(255, Math.round(this.b) || 0)) + (1 === t ? ")" : ", " + t + ")")
        }
        function In(t) {
            return t = Math.max(0, Math.min(255, Math.round(t) || 0)),
            (t < 16 ? "0" : "") + t.toString(16)
        }
        function jn(t, n, e, r) {
            return r <= 0 ? t = n = e = NaN : e <= 0 || e >= 1 ? t = n = NaN : n <= 0 && (t = NaN),
            new Hn(t,n,e,r)
        }
        function Fn(t) {
            if (t instanceof Hn)
                return new Hn(t.h,t.s,t.l,t.opacity);
            if (t instanceof pn || (t = kn(t)),
            !t)
                return new Hn;
            if (t instanceof Hn)
                return t;
            t = t.rgb();
            var n = t.r / 255
              , e = t.g / 255
              , r = t.b / 255
              , i = Math.min(n, e, r)
              , o = Math.max(n, e, r)
              , u = NaN
              , a = o - i
              , s = (o + i) / 2;
            return a ? (u = n === o ? (e - r) / a + 6 * (e < r) : e === o ? (r - n) / a + 2 : (n - e) / a + 4,
            a /= s < .5 ? o + i : 2 - o - i,
            u *= 60) : a = s > 0 && s < 1 ? 0 : u,
            new Hn(u,a,s,t.opacity)
        }
        function Yn(t, n, e, r) {
            return 1 === arguments.length ? Fn(t) : new Hn(t,n,e,null == r ? 1 : r)
        }
        function Hn(t, n, e, r) {
            this.h = +t,
            this.s = +n,
            this.l = +e,
            this.opacity = +r
        }
        function qn(t, n, e) {
            return 255 * (t < 60 ? n + (e - n) * t / 60 : t < 180 ? e : t < 240 ? n + (e - n) * (240 - t) / 60 : n)
        }
        function Bn(t, n, e, r, i) {
            var o = t * t
              , u = o * t;
            return ((1 - 3 * t + 3 * o - u) * n + (4 - 6 * o + 3 * u) * e + (1 + 3 * t + 3 * o - 3 * u) * r + u * i) / 6
        }
        hn(pn, kn, {
            copy: function(t) {
                return Object.assign(new this.constructor, this, t)
            },
            displayable: function() {
                return this.rgb().displayable()
            },
            hex: Tn,
            formatHex: Tn,
            formatHsl: Sn,
            formatRgb: En,
            toString: En
        }),
        hn(zn, Dn, fn(pn, {
            brighter: function(t) {
                return t = null == t ? vn : Math.pow(vn, t),
                new zn(this.r * t,this.g * t,this.b * t,this.opacity)
            },
            darker: function(t) {
                return t = null == t ? _n : Math.pow(_n, t),
                new zn(this.r * t,this.g * t,this.b * t,this.opacity)
            },
            rgb: function() {
                return this
            },
            displayable: function() {
                return -.5 <= this.r && this.r < 255.5 && -.5 <= this.g && this.g < 255.5 && -.5 <= this.b && this.b < 255.5 && 0 <= this.opacity && this.opacity <= 1
            },
            hex: On,
            formatHex: On,
            formatRgb: Rn,
            toString: Rn
        })),
        hn(Hn, Yn, fn(pn, {
            brighter: function(t) {
                return t = null == t ? vn : Math.pow(vn, t),
                new Hn(this.h,this.s,this.l * t,this.opacity)
            },
            darker: function(t) {
                return t = null == t ? _n : Math.pow(_n, t),
                new Hn(this.h,this.s,this.l * t,this.opacity)
            },
            rgb: function() {
                var t = this.h % 360 + 360 * (this.h < 0)
                  , n = isNaN(t) || isNaN(this.s) ? 0 : this.s
                  , e = this.l
                  , r = e + (e < .5 ? e : 1 - e) * n
                  , i = 2 * e - r;
                return new zn(qn(t >= 240 ? t - 240 : t + 120, i, r),qn(t, i, r),qn(t < 120 ? t + 240 : t - 120, i, r),this.opacity)
            },
            displayable: function() {
                return (0 <= this.s && this.s <= 1 || isNaN(this.s)) && 0 <= this.l && this.l <= 1 && 0 <= this.opacity && this.opacity <= 1
            },
            formatHsl: function() {
                var t = this.opacity;
                return t = isNaN(t) ? 1 : Math.max(0, Math.min(1, t)),
                (1 === t ? "hsl(" : "hsla(") + (this.h || 0) + ", " + 100 * (this.s || 0) + "%, " + 100 * (this.l || 0) + "%" + (1 === t ? ")" : ", " + t + ")")
            }
        }));
        var Xn = function(t) {
            var n = t.length - 1;
            return function(e) {
                var r = e <= 0 ? e = 0 : e >= 1 ? (e = 1,
                n - 1) : Math.floor(e * n)
                  , i = t[r]
                  , o = t[r + 1]
                  , u = r > 0 ? t[r - 1] : 2 * i - o
                  , a = r < n - 1 ? t[r + 2] : 2 * o - i;
                return Bn((e - r / n) * n, u, i, o, a)
            }
        }
          , Vn = function(t) {
            var n = t.length;
            return function(e) {
                var r = Math.floor(((e %= 1) < 0 ? ++e : e) * n)
                  , i = t[(r + n - 1) % n]
                  , o = t[r % n]
                  , u = t[(r + 1) % n]
                  , a = t[(r + 2) % n];
                return Bn((e - r / n) * n, i, o, u, a)
            }
        }
          , $n = function(t) {
            return function() {
                return t
            }
        };
        function Zn(t, n) {
            return function(e) {
                return t + e * n
            }
        }
        function Gn(t, n, e) {
            return t = Math.pow(t, e),
            n = Math.pow(n, e) - t,
            e = 1 / e,
            function(r) {
                return Math.pow(t + r * n, e)
            }
        }
        function Wn(t) {
            return 1 === (t = +t) ? Jn : function(n, e) {
                return e - n ? Gn(n, e, t) : $n(isNaN(n) ? e : n)
            }
        }
        function Jn(t, n) {
            var e = n - t;
            return e ? Zn(t, e) : $n(isNaN(t) ? n : t)
        }
        var Qn = function t(n) {
            var e = Wn(n);
            function r(t, n) {
                var r = e((t = Dn(t)).r, (n = Dn(n)).r)
                  , i = e(t.g, n.g)
                  , o = e(t.b, n.b)
                  , u = Jn(t.opacity, n.opacity);
                return function(n) {
                    return t.r = r(n),
                    t.g = i(n),
                    t.b = o(n),
                    t.opacity = u(n),
                    t + ""
                }
            }
            return r.gamma = t,
            r
        }(1);
        function Kn(t) {
            return function(n) {
                var e, r, i = n.length, o = new Array(i), u = new Array(i), a = new Array(i);
                for (e = 0; e < i; ++e)
                    r = Dn(n[e]),
                    o[e] = r.r || 0,
                    u[e] = r.g || 0,
                    a[e] = r.b || 0;
                return o = t(o),
                u = t(u),
                a = t(a),
                r.opacity = 1,
                function(t) {
                    return r.r = o(t),
                    r.g = u(t),
                    r.b = a(t),
                    r + ""
                }
            }
        }
        Kn(Xn),
        Kn(Vn);
        var te = function(t, n) {
            return t = +t,
            n = +n,
            function(e) {
                return t * (1 - e) + n * e
            }
        }
          , ne = /[-+]?(?:\d+\.?\d*|\.?\d+)(?:[eE][-+]?\d+)?/g
          , ee = new RegExp(ne.source,"g");
        function re(t) {
            return function() {
                return t
            }
        }
        function ie(t) {
            return function(n) {
                return t(n) + ""
            }
        }
        var oe, ue, ae = function(t, n) {
            var e, r, i, o = ne.lastIndex = ee.lastIndex = 0, u = -1, a = [], s = [];
            t += "",
            n += "";
            while ((e = ne.exec(t)) && (r = ee.exec(n)))
                (i = r.index) > o && (i = n.slice(o, i),
                a[u] ? a[u] += i : a[++u] = i),
                (e = e[0]) === (r = r[0]) ? a[u] ? a[u] += r : a[++u] = r : (a[++u] = null,
                s.push({
                    i: u,
                    x: te(e, r)
                })),
                o = ee.lastIndex;
            return o < n.length && (i = n.slice(o),
            a[u] ? a[u] += i : a[++u] = i),
            a.length < 2 ? s[0] ? ie(s[0].x) : re(n) : (n = s.length,
            function(t) {
                for (var e, r = 0; r < n; ++r)
                    a[(e = s[r]).i] = e.x(t);
                return a.join("")
            }
            )
        }, se = 0, ce = 0, le = 0, he = 1e3, fe = 0, pe = 0, _e = 0, ve = "object" === typeof performance && performance.now ? performance : Date, de = "object" === typeof window && window.requestAnimationFrame ? window.requestAnimationFrame.bind(window) : function(t) {
            setTimeout(t, 17)
        }
        ;
        function ge() {
            return pe || (de(ye),
            pe = ve.now() + _e)
        }
        function ye() {
            pe = 0
        }
        function me() {
            this._call = this._time = this._next = null
        }
        function we(t, n, e) {
            var r = new me;
            return r.restart(t, n, e),
            r
        }
        function xe() {
            ge(),
            ++se;
            var t, n = oe;
            while (n)
                (t = pe - n._time) >= 0 && n._call.call(null, t),
                n = n._next;
            --se
        }
        function be() {
            pe = (fe = ve.now()) + _e,
            se = ce = 0;
            try {
                xe()
            } finally {
                se = 0,
                Me(),
                pe = 0
            }
        }
        function Ae() {
            var t = ve.now()
              , n = t - fe;
            n > he && (_e -= n,
            fe = t)
        }
        function Me() {
            var t, n, e = oe, r = 1 / 0;
            while (e)
                e._call ? (r > e._time && (r = e._time),
                t = e,
                e = e._next) : (n = e._next,
                e._next = null,
                e = t ? t._next = n : oe = n);
            ue = t,
            Ce(r)
        }
        function Ce(t) {
            if (!se) {
                ce && (ce = clearTimeout(ce));
                var n = t - pe;
                n > 24 ? (t < 1 / 0 && (ce = setTimeout(be, t - ve.now() - _e)),
                le && (le = clearInterval(le))) : (le || (fe = ve.now(),
                le = setInterval(Ae, he)),
                se = 1,
                de(be))
            }
        }
        me.prototype = we.prototype = {
            constructor: me,
            restart: function(t, n, e) {
                if ("function" !== typeof t)
                    throw new TypeError("callback is not a function");
                e = (null == e ? ge() : +e) + (null == n ? 0 : +n),
                this._next || ue === this || (ue ? ue._next = this : oe = this,
                ue = this),
                this._call = t,
                this._time = e,
                Ce()
            },
            stop: function() {
                this._call && (this._call = null,
                this._time = 1 / 0,
                Ce())
            }
        };
        var Ne = function(t, n, e) {
            var r = new me;
            return n = null == n ? 0 : +n,
            r.restart((function(e) {
                r.stop(),
                t(e + n)
            }
            ), n, e),
            r
        }
          , Te = m("start", "end", "cancel", "interrupt")
          , Se = []
          , Ee = 0
          , ke = 1
          , Pe = 2
          , Ue = 3
          , Le = 4
          , De = 5
          , ze = 6
          , Oe = function(t, n, e, r, i, o) {
            var u = t.__transition;
            if (u) {
                if (e in u)
                    return
            } else
                t.__transition = {};
            Fe(t, e, {
                name: n,
                index: r,
                group: i,
                on: Te,
                tween: Se,
                time: o.time,
                delay: o.delay,
                duration: o.duration,
                ease: o.ease,
                timer: null,
                state: Ee
            })
        };
        function Re(t, n) {
            var e = je(t, n);
            if (e.state > Ee)
                throw new Error("too late; already scheduled");
            return e
        }
        function Ie(t, n) {
            var e = je(t, n);
            if (e.state > Ue)
                throw new Error("too late; already running");
            return e
        }
        function je(t, n) {
            var e = t.__transition;
            if (!e || !(e = e[n]))
                throw new Error("transition not found");
            return e
        }
        function Fe(t, n, e) {
            var r, i = t.__transition;
            function o(t) {
                e.state = ke,
                e.timer.restart(u, e.delay, e.time),
                e.delay <= t && u(t - e.delay)
            }
            function u(o) {
                var c, l, h, f;
                if (e.state !== ke)
                    return s();
                for (c in i)
                    if (f = i[c],
                    f.name === e.name) {
                        if (f.state === Ue)
                            return Ne(u);
                        f.state === Le ? (f.state = ze,
                        f.timer.stop(),
                        f.on.call("interrupt", t, t.__data__, f.index, f.group),
                        delete i[c]) : +c < n && (f.state = ze,
                        f.timer.stop(),
                        f.on.call("cancel", t, t.__data__, f.index, f.group),
                        delete i[c])
                    }
                if (Ne((function() {
                    e.state === Ue && (e.state = Le,
                    e.timer.restart(a, e.delay, e.time),
                    a(o))
                }
                )),
                e.state = Pe,
                e.on.call("start", t, t.__data__, e.index, e.group),
                e.state === Pe) {
                    for (e.state = Ue,
                    r = new Array(h = e.tween.length),
                    c = 0,
                    l = -1; c < h; ++c)
                        (f = e.tween[c].value.call(t, t.__data__, e.index, e.group)) && (r[++l] = f);
                    r.length = l + 1
                }
            }
            function a(n) {
                var i = n < e.duration ? e.ease.call(null, n / e.duration) : (e.timer.restart(s),
                e.state = De,
                1)
                  , o = -1
                  , u = r.length;
                while (++o < u)
                    r[o].call(t, i);
                e.state === De && (e.on.call("end", t, t.__data__, e.index, e.group),
                s())
            }
            function s() {
                for (var r in e.state = ze,
                e.timer.stop(),
                delete i[n],
                i)
                    return;
                delete t.__transition
            }
            i[n] = e,
            e.timer = we(o, 0, e.time)
        }
        var Ye, He, qe, Be, Xe = function(t, n) {
            var e, r, i, o = t.__transition, u = !0;
            if (o) {
                for (i in n = null == n ? null : n + "",
                o)
                    (e = o[i]).name === n ? (r = e.state > Pe && e.state < De,
                    e.state = ze,
                    e.timer.stop(),
                    e.on.call(r ? "interrupt" : "cancel", t, t.__data__, e.index, e.group),
                    delete o[i]) : u = !1;
                u && delete t.__transition
            }
        }, Ve = function(t) {
            return this.each((function() {
                Xe(this, t)
            }
            ))
        }, $e = 180 / Math.PI, Ze = {
            translateX: 0,
            translateY: 0,
            rotate: 0,
            skewX: 0,
            scaleX: 1,
            scaleY: 1
        }, Ge = function(t, n, e, r, i, o) {
            var u, a, s;
            return (u = Math.sqrt(t * t + n * n)) && (t /= u,
            n /= u),
            (s = t * e + n * r) && (e -= t * s,
            r -= n * s),
            (a = Math.sqrt(e * e + r * r)) && (e /= a,
            r /= a,
            s /= a),
            t * r < n * e && (t = -t,
            n = -n,
            s = -s,
            u = -u),
            {
                translateX: i,
                translateY: o,
                rotate: Math.atan2(n, t) * $e,
                skewX: Math.atan(s) * $e,
                scaleX: u,
                scaleY: a
            }
        };
        function We(t) {
            return "none" === t ? Ze : (Ye || (Ye = document.createElement("DIV"),
            He = document.documentElement,
            qe = document.defaultView),
            Ye.style.transform = t,
            t = qe.getComputedStyle(He.appendChild(Ye), null).getPropertyValue("transform"),
            He.removeChild(Ye),
            t = t.slice(7, -1).split(","),
            Ge(+t[0], +t[1], +t[2], +t[3], +t[4], +t[5]))
        }
        function Je(t) {
            return null == t ? Ze : (Be || (Be = document.createElementNS("http://www.w3.org/2000/svg", "g")),
            Be.setAttribute("transform", t),
            (t = Be.transform.baseVal.consolidate()) ? (t = t.matrix,
            Ge(t.a, t.b, t.c, t.d, t.e, t.f)) : Ze)
        }
        function Qe(t, n, e, r) {
            function i(t) {
                return t.length ? t.pop() + " " : ""
            }
            function o(t, r, i, o, u, a) {
                if (t !== i || r !== o) {
                    var s = u.push("translate(", null, n, null, e);
                    a.push({
                        i: s - 4,
                        x: te(t, i)
                    }, {
                        i: s - 2,
                        x: te(r, o)
                    })
                } else
                    (i || o) && u.push("translate(" + i + n + o + e)
            }
            function u(t, n, e, o) {
                t !== n ? (t - n > 180 ? n += 360 : n - t > 180 && (t += 360),
                o.push({
                    i: e.push(i(e) + "rotate(", null, r) - 2,
                    x: te(t, n)
                })) : n && e.push(i(e) + "rotate(" + n + r)
            }
            function a(t, n, e, o) {
                t !== n ? o.push({
                    i: e.push(i(e) + "skewX(", null, r) - 2,
                    x: te(t, n)
                }) : n && e.push(i(e) + "skewX(" + n + r)
            }
            function s(t, n, e, r, o, u) {
                if (t !== e || n !== r) {
                    var a = o.push(i(o) + "scale(", null, ",", null, ")");
                    u.push({
                        i: a - 4,
                        x: te(t, e)
                    }, {
                        i: a - 2,
                        x: te(n, r)
                    })
                } else
                    1 === e && 1 === r || o.push(i(o) + "scale(" + e + "," + r + ")")
            }
            return function(n, e) {
                var r = []
                  , i = [];
                return n = t(n),
                e = t(e),
                o(n.translateX, n.translateY, e.translateX, e.translateY, r, i),
                u(n.rotate, e.rotate, r, i),
                a(n.skewX, e.skewX, r, i),
                s(n.scaleX, n.scaleY, e.scaleX, e.scaleY, r, i),
                n = e = null,
                function(t) {
                    var n, e = -1, o = i.length;
                    while (++e < o)
                        r[(n = i[e]).i] = n.x(t);
                    return r.join("")
                }
            }
        }
        var Ke = Qe(We, "px, ", "px)", "deg)")
          , tr = Qe(Je, ", ", ")", ")");
        function nr(t, n) {
            var e, r;
            return function() {
                var i = Ie(this, t)
                  , o = i.tween;
                if (o !== e) {
                    r = e = o;
                    for (var u = 0, a = r.length; u < a; ++u)
                        if (r[u].name === n) {
                            r = r.slice(),
                            r.splice(u, 1);
                            break
                        }
                }
                i.tween = r
            }
        }
        function er(t, n, e) {
            var r, i;
            if ("function" !== typeof e)
                throw new Error;
            return function() {
                var o = Ie(this, t)
                  , u = o.tween;
                if (u !== r) {
                    i = (r = u).slice();
                    for (var a = {
                        name: n,
                        value: e
                    }, s = 0, c = i.length; s < c; ++s)
                        if (i[s].name === n) {
                            i[s] = a;
                            break
                        }
                    s === c && i.push(a)
                }
                o.tween = i
            }
        }
        var rr = function(t, n) {
            var e = this._id;
            if (t += "",
            arguments.length < 2) {
                for (var r, i = je(this.node(), e).tween, o = 0, u = i.length; o < u; ++o)
                    if ((r = i[o]).name === t)
                        return r.value;
                return null
            }
            return this.each((null == n ? nr : er)(e, t, n))
        };
        function ir(t, n, e) {
            var r = t._id;
            return t.each((function() {
                var t = Ie(this, r);
                (t.value || (t.value = {}))[n] = e.apply(this, arguments)
            }
            )),
            function(t) {
                return je(t, r).value[n]
            }
        }
        var or = function(t, n) {
            var e;
            return ("number" === typeof n ? te : n instanceof kn ? Qn : (e = kn(n)) ? (n = e,
            Qn) : ae)(t, n)
        };
        function ur(t) {
            return function() {
                this.removeAttribute(t)
            }
        }
        function ar(t) {
            return function() {
                this.removeAttributeNS(t.space, t.local)
            }
        }
        function sr(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = this.getAttribute(t);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function cr(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = this.getAttributeNS(t.space, t.local);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function lr(t, n, e) {
            var r, i, o;
            return function() {
                var u, a, s = e(this);
                if (null != s)
                    return u = this.getAttribute(t),
                    a = s + "",
                    u === a ? null : u === r && a === i ? o : (i = a,
                    o = n(r = u, s));
                this.removeAttribute(t)
            }
        }
        function hr(t, n, e) {
            var r, i, o;
            return function() {
                var u, a, s = e(this);
                if (null != s)
                    return u = this.getAttributeNS(t.space, t.local),
                    a = s + "",
                    u === a ? null : u === r && a === i ? o : (i = a,
                    o = n(r = u, s));
                this.removeAttributeNS(t.space, t.local)
            }
        }
        var fr = function(t, n) {
            var e = W(t)
              , r = "transform" === e ? tr : or;
            return this.attrTween(t, "function" === typeof n ? (e.local ? hr : lr)(e, r, ir(this, "attr." + t, n)) : null == n ? (e.local ? ar : ur)(e) : (e.local ? cr : sr)(e, r, n))
        };
        function pr(t, n) {
            return function(e) {
                this.setAttribute(t, n.call(this, e))
            }
        }
        function _r(t, n) {
            return function(e) {
                this.setAttributeNS(t.space, t.local, n.call(this, e))
            }
        }
        function vr(t, n) {
            var e, r;
            function i() {
                var i = n.apply(this, arguments);
                return i !== r && (e = (r = i) && _r(t, i)),
                e
            }
            return i._value = n,
            i
        }
        function dr(t, n) {
            var e, r;
            function i() {
                var i = n.apply(this, arguments);
                return i !== r && (e = (r = i) && pr(t, i)),
                e
            }
            return i._value = n,
            i
        }
        var gr = function(t, n) {
            var e = "attr." + t;
            if (arguments.length < 2)
                return (e = this.tween(e)) && e._value;
            if (null == n)
                return this.tween(e, null);
            if ("function" !== typeof n)
                throw new Error;
            var r = W(t);
            return this.tween(e, (r.local ? vr : dr)(r, n))
        };
        function yr(t, n) {
            return function() {
                Re(this, t).delay = +n.apply(this, arguments)
            }
        }
        function mr(t, n) {
            return n = +n,
            function() {
                Re(this, t).delay = n
            }
        }
        var wr = function(t) {
            var n = this._id;
            return arguments.length ? this.each(("function" === typeof t ? yr : mr)(n, t)) : je(this.node(), n).delay
        };
        function xr(t, n) {
            return function() {
                Ie(this, t).duration = +n.apply(this, arguments)
            }
        }
        function br(t, n) {
            return n = +n,
            function() {
                Ie(this, t).duration = n
            }
        }
        var Ar = function(t) {
            var n = this._id;
            return arguments.length ? this.each(("function" === typeof t ? xr : br)(n, t)) : je(this.node(), n).duration
        };
        function Mr(t, n) {
            if ("function" !== typeof n)
                throw new Error;
            return function() {
                Ie(this, t).ease = n
            }
        }
        var Cr = function(t) {
            var n = this._id;
            return arguments.length ? this.each(Mr(n, t)) : je(this.node(), n).ease
        }
          , Nr = function(t) {
            "function" !== typeof t && (t = N(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u = n[i], a = u.length, s = r[i] = [], c = 0; c < a; ++c)
                    (o = u[c]) && t.call(o, o.__data__, c, u) && s.push(o);
            return new ni(r,this._parents,this._name,this._id)
        }
          , Tr = function(t) {
            if (t._id !== this._id)
                throw new Error;
            for (var n = this._groups, e = t._groups, r = n.length, i = e.length, o = Math.min(r, i), u = new Array(r), a = 0; a < o; ++a)
                for (var s, c = n[a], l = e[a], h = c.length, f = u[a] = new Array(h), p = 0; p < h; ++p)
                    (s = c[p] || l[p]) && (f[p] = s);
            for (; a < r; ++a)
                u[a] = n[a];
            return new ni(u,this._parents,this._name,this._id)
        };
        function Sr(t) {
            return (t + "").trim().split(/^|\s+/).every((function(t) {
                var n = t.indexOf(".");
                return n >= 0 && (t = t.slice(0, n)),
                !t || "start" === t
            }
            ))
        }
        function Er(t, n, e) {
            var r, i, o = Sr(n) ? Re : Ie;
            return function() {
                var u = o(this, t)
                  , a = u.on;
                a !== r && (i = (r = a).copy()).on(n, e),
                u.on = i
            }
        }
        var kr = function(t, n) {
            var e = this._id;
            return arguments.length < 2 ? je(this.node(), e).on.on(t) : this.each(Er(e, t, n))
        };
        function Pr(t) {
            return function() {
                var n = this.parentNode;
                for (var e in this.__transition)
                    if (+e !== t)
                        return;
                n && n.removeChild(this)
            }
        }
        var Ur = function() {
            return this.on("end.remove", Pr(this._id))
        }
          , Lr = function(t) {
            var n = this._name
              , e = this._id;
            "function" !== typeof t && (t = x(t));
            for (var r = this._groups, i = r.length, o = new Array(i), u = 0; u < i; ++u)
                for (var a, s, c = r[u], l = c.length, h = o[u] = new Array(l), f = 0; f < l; ++f)
                    (a = c[f]) && (s = t.call(a, a.__data__, f, c)) && ("__data__"in a && (s.__data__ = a.__data__),
                    h[f] = s,
                    Oe(h[f], n, e, f, h, je(a, e)));
            return new ni(o,this._parents,n,e)
        }
          , Dr = function(t) {
            var n = this._name
              , e = this._id;
            "function" !== typeof t && (t = M(t));
            for (var r = this._groups, i = r.length, o = [], u = [], a = 0; a < i; ++a)
                for (var s, c = r[a], l = c.length, h = 0; h < l; ++h)
                    if (s = c[h]) {
                        for (var f, p = t.call(s, s.__data__, h, c), _ = je(s, e), v = 0, d = p.length; v < d; ++v)
                            (f = p[v]) && Oe(f, n, e, v, p, _);
                        o.push(p),
                        u.push(s)
                    }
            return new ni(o,u,n,e)
        }
          , zr = ln.prototype.constructor
          , Or = function() {
            return new zr(this._groups,this._parents)
        };
        function Rr(t, n) {
            var e, r, i;
            return function() {
                var o = ct(this, t)
                  , u = (this.style.removeProperty(t),
                ct(this, t));
                return o === u ? null : o === e && u === r ? i : i = n(e = o, r = u)
            }
        }
        function Ir(t) {
            return function() {
                this.style.removeProperty(t)
            }
        }
        function jr(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = ct(this, t);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function Fr(t, n, e) {
            var r, i, o;
            return function() {
                var u = ct(this, t)
                  , a = e(this)
                  , s = a + "";
                return null == a && (this.style.removeProperty(t),
                s = a = ct(this, t)),
                u === s ? null : u === r && s === i ? o : (i = s,
                o = n(r = u, a))
            }
        }
        function Yr(t, n) {
            var e, r, i, o, u = "style." + n, a = "end." + u;
            return function() {
                var s = Ie(this, t)
                  , c = s.on
                  , l = null == s.value[u] ? o || (o = Ir(n)) : void 0;
                c === e && i === l || (r = (e = c).copy()).on(a, i = l),
                s.on = r
            }
        }
        var Hr = function(t, n, e) {
            var r = "transform" === (t += "") ? Ke : or;
            return null == n ? this.styleTween(t, Rr(t, r)).on("end.style." + t, Ir(t)) : "function" === typeof n ? this.styleTween(t, Fr(t, r, ir(this, "style." + t, n))).each(Yr(this._id, t)) : this.styleTween(t, jr(t, r, n), e).on("end.style." + t, null)
        };
        function qr(t, n, e) {
            return function(r) {
                this.style.setProperty(t, n.call(this, r), e)
            }
        }
        function Br(t, n, e) {
            var r, i;
            function o() {
                var o = n.apply(this, arguments);
                return o !== i && (r = (i = o) && qr(t, o, e)),
                r
            }
            return o._value = n,
            o
        }
        var Xr = function(t, n, e) {
            var r = "style." + (t += "");
            if (arguments.length < 2)
                return (r = this.tween(r)) && r._value;
            if (null == n)
                return this.tween(r, null);
            if ("function" !== typeof n)
                throw new Error;
            return this.tween(r, Br(t, n, null == e ? "" : e))
        };
        function Vr(t) {
            return function() {
                this.textContent = t
            }
        }
        function $r(t) {
            return function() {
                var n = t(this);
                this.textContent = null == n ? "" : n
            }
        }
        var Zr = function(t) {
            return this.tween("text", "function" === typeof t ? $r(ir(this, "text", t)) : Vr(null == t ? "" : t + ""))
        };
        function Gr(t) {
            return function(n) {
                this.textContent = t.call(this, n)
            }
        }
        function Wr(t) {
            var n, e;
            function r() {
                var r = t.apply(this, arguments);
                return r !== e && (n = (e = r) && Gr(r)),
                n
            }
            return r._value = t,
            r
        }
        var Jr = function(t) {
            var n = "text";
            if (arguments.length < 1)
                return (n = this.tween(n)) && n._value;
            if (null == t)
                return this.tween(n, null);
            if ("function" !== typeof t)
                throw new Error;
            return this.tween(n, Wr(t))
        }
          , Qr = function() {
            for (var t = this._name, n = this._id, e = ri(), r = this._groups, i = r.length, o = 0; o < i; ++o)
                for (var u, a = r[o], s = a.length, c = 0; c < s; ++c)
                    if (u = a[c]) {
                        var l = je(u, n);
                        Oe(u, t, e, c, a, {
                            time: l.time + l.delay + l.duration,
                            delay: 0,
                            duration: l.duration,
                            ease: l.ease
                        })
                    }
            return new ni(r,this._parents,t,e)
        }
          , Kr = function() {
            var t, n, e = this, r = e._id, i = e.size();
            return new Promise((function(o, u) {
                var a = {
                    value: u
                }
                  , s = {
                    value: function() {
                        0 === --i && o()
                    }
                };
                e.each((function() {
                    var e = Ie(this, r)
                      , i = e.on;
                    i !== t && (n = (t = i).copy(),
                    n._.cancel.push(a),
                    n._.interrupt.push(a),
                    n._.end.push(s)),
                    e.on = n
                }
                ))
            }
            ))
        }
          , ti = 0;
        function ni(t, n, e, r) {
            this._groups = t,
            this._parents = n,
            this._name = e,
            this._id = r
        }
        function ei(t) {
            return ln().transition(t)
        }
        function ri() {
            return ++ti
        }
        var ii = ln.prototype;
        function oi(t) {
            return ((t *= 2) <= 1 ? t * t * t : (t -= 2) * t * t + 2) / 2
        }
        ni.prototype = ei.prototype = {
            constructor: ni,
            select: Lr,
            selectAll: Dr,
            filter: Nr,
            merge: Tr,
            selection: Or,
            transition: Qr,
            call: ii.call,
            nodes: ii.nodes,
            node: ii.node,
            size: ii.size,
            empty: ii.empty,
            each: ii.each,
            on: kr,
            attr: fr,
            attrTween: gr,
            style: Hr,
            styleTween: Xr,
            text: Zr,
            textTween: Jr,
            remove: Ur,
            tween: rr,
            delay: wr,
            duration: Ar,
            ease: Cr,
            end: Kr
        };
        var ui = {
            time: null,
            delay: 0,
            duration: 250,
            ease: oi
        };
        function ai(t, n) {
            var e;
            while (!(e = t.__transition) || !(e = e[n]))
                if (!(t = t.parentNode))
                    return ui.time = ge(),
                    ui;
            return e
        }
        var si = function(t) {
            var n, e;
            t instanceof ni ? (n = t._id,
            t = t._name) : (n = ri(),
            (e = ui).time = ge(),
            t = null == t ? null : t + "");
            for (var r = this._groups, i = r.length, o = 0; o < i; ++o)
                for (var u, a = r[o], s = a.length, c = 0; c < s; ++c)
                    (u = a[c]) && Oe(u, t, n, c, a, e || ai(u, n));
            return new ni(r,this._parents,t,n)
        };
        ln.prototype.interrupt = Ve,
        ln.prototype.transition = si;
        function ci(t) {
            return [+t[0], +t[1]]
        }
        function li(t) {
            return [ci(t[0]), ci(t[1])]
        }
        ["w", "e"].map(hi),
        ["n", "s"].map(hi),
        ["n", "w", "e", "s", "nw", "ne", "sw", "se"].map(hi);
        function hi(t) {
            return {
                type: t
            }
        }
        Math.cos,
        Math.sin,
        Math.PI,
        Math.max;
        Array.prototype.slice;
        var fi = Math.PI
          , pi = 2 * fi
          , _i = 1e-6
          , vi = pi - _i;
        function di() {
            this._x0 = this._y0 = this._x1 = this._y1 = null,
            this._ = ""
        }
        function gi() {
            return new di
        }
        di.prototype = gi.prototype = {
            constructor: di,
            moveTo: function(t, n) {
                this._ += "M" + (this._x0 = this._x1 = +t) + "," + (this._y0 = this._y1 = +n)
            },
            closePath: function() {
                null !== this._x1 && (this._x1 = this._x0,
                this._y1 = this._y0,
                this._ += "Z")
            },
            lineTo: function(t, n) {
                this._ += "L" + (this._x1 = +t) + "," + (this._y1 = +n)
            },
            quadraticCurveTo: function(t, n, e, r) {
                this._ += "Q" + +t + "," + +n + "," + (this._x1 = +e) + "," + (this._y1 = +r)
            },
            bezierCurveTo: function(t, n, e, r, i, o) {
                this._ += "C" + +t + "," + +n + "," + +e + "," + +r + "," + (this._x1 = +i) + "," + (this._y1 = +o)
            },
            arcTo: function(t, n, e, r, i) {
                t = +t,
                n = +n,
                e = +e,
                r = +r,
                i = +i;
                var o = this._x1
                  , u = this._y1
                  , a = e - t
                  , s = r - n
                  , c = o - t
                  , l = u - n
                  , h = c * c + l * l;
                if (i < 0)
                    throw new Error("negative radius: " + i);
                if (null === this._x1)
                    this._ += "M" + (this._x1 = t) + "," + (this._y1 = n);
                else if (h > _i)
                    if (Math.abs(l * a - s * c) > _i && i) {
                        var f = e - o
                          , p = r - u
                          , _ = a * a + s * s
                          , v = f * f + p * p
                          , d = Math.sqrt(_)
                          , g = Math.sqrt(h)
                          , y = i * Math.tan((fi - Math.acos((_ + h - v) / (2 * d * g))) / 2)
                          , m = y / g
                          , w = y / d;
                        Math.abs(m - 1) > _i && (this._ += "L" + (t + m * c) + "," + (n + m * l)),
                        this._ += "A" + i + "," + i + ",0,0," + +(l * f > c * p) + "," + (this._x1 = t + w * a) + "," + (this._y1 = n + w * s)
                    } else
                        this._ += "L" + (this._x1 = t) + "," + (this._y1 = n);
                else
                    ;
            },
            arc: function(t, n, e, r, i, o) {
                t = +t,
                n = +n,
                e = +e,
                o = !!o;
                var u = e * Math.cos(r)
                  , a = e * Math.sin(r)
                  , s = t + u
                  , c = n + a
                  , l = 1 ^ o
                  , h = o ? r - i : i - r;
                if (e < 0)
                    throw new Error("negative radius: " + e);
                null === this._x1 ? this._ += "M" + s + "," + c : (Math.abs(this._x1 - s) > _i || Math.abs(this._y1 - c) > _i) && (this._ += "L" + s + "," + c),
                e && (h < 0 && (h = h % pi + pi),
                h > vi ? this._ += "A" + e + "," + e + ",0,1," + l + "," + (t - u) + "," + (n - a) + "A" + e + "," + e + ",0,1," + l + "," + (this._x1 = s) + "," + (this._y1 = c) : h > _i && (this._ += "A" + e + "," + e + ",0," + +(h >= fi) + "," + l + "," + (this._x1 = t + e * Math.cos(i)) + "," + (this._y1 = n + e * Math.sin(i))))
            },
            rect: function(t, n, e, r) {
                this._ += "M" + (this._x0 = this._x1 = +t) + "," + (this._y0 = this._y1 = +n) + "h" + +e + "v" + +r + "h" + -e + "Z"
            },
            toString: function() {
                return this._
            }
        };
        var yi = "$";
        function mi() {}
        function wi(t, n) {
            var e = new mi;
            if (t instanceof mi)
                t.each((function(t, n) {
                    e.set(n, t)
                }
                ));
            else if (Array.isArray(t)) {
                var r, i = -1, o = t.length;
                if (null == n)
                    while (++i < o)
                        e.set(i, t[i]);
                else
                    while (++i < o)
                        e.set(n(r = t[i], i, t), r)
            } else if (t)
                for (var u in t)
                    e.set(u, t[u]);
            return e
        }
        mi.prototype = wi.prototype = {
            constructor: mi,
            has: function(t) {
                return yi + t in this
            },
            get: function(t) {
                return this[yi + t]
            },
            set: function(t, n) {
                return this[yi + t] = n,
                this
            },
            remove: function(t) {
                var n = yi + t;
                return n in this && delete this[n]
            },
            clear: function() {
                for (var t in this)
                    t[0] === yi && delete this[t]
            },
            keys: function() {
                var t = [];
                for (var n in this)
                    n[0] === yi && t.push(n.slice(1));
                return t
            },
            values: function() {
                var t = [];
                for (var n in this)
                    n[0] === yi && t.push(this[n]);
                return t
            },
            entries: function() {
                var t = [];
                for (var n in this)
                    n[0] === yi && t.push({
                        key: n.slice(1),
                        value: this[n]
                    });
                return t
            },
            size: function() {
                var t = 0;
                for (var n in this)
                    n[0] === yi && ++t;
                return t
            },
            empty: function() {
                for (var t in this)
                    if (t[0] === yi)
                        return !1;
                return !0
            },
            each: function(t) {
                for (var n in this)
                    n[0] === yi && t(this[n], n.slice(1), this)
            }
        };
        var xi = wi;
        function bi() {}
        var Ai = xi.prototype;
        function Mi(t, n) {
            var e = new bi;
            if (t instanceof bi)
                t.each((function(t) {
                    e.add(t)
                }
                ));
            else if (t) {
                var r = -1
                  , i = t.length;
                if (null == n)
                    while (++r < i)
                        e.add(t[r]);
                else
                    while (++r < i)
                        e.add(n(t[r], r, t))
            }
            return e
        }
        bi.prototype = Mi.prototype = {
            constructor: bi,
            has: Ai.has,
            add: function(t) {
                return t += "",
                this[yi + t] = t,
                this
            },
            remove: Ai.remove,
            clear: Ai.clear,
            values: Ai.keys,
            size: Ai.size,
            empty: Ai.empty,
            each: Ai.each
        };
        var Ci = Array.prototype;
        Ci.slice;
        var Ni = {}
          , Ti = null;
        if ("undefined" !== typeof document) {
            var Si = document.documentElement;
            "onmouseenter"in Si || (Ni = {
                mouseenter: "mouseover",
                mouseleave: "mouseout"
            })
        }
        function Ei(t, n, e) {
            return t = ki(t, n, e),
            function(n) {
                var e = n.relatedTarget;
                e && (e === this || 8 & e.compareDocumentPosition(this)) || t.call(this, n)
            }
        }
        function ki(t, n, e) {
            return function(r) {
                var i = Ti;
                Ti = r;
                try {
                    t.call(this, this.__data__, n, e)
                } finally {
                    Ti = i
                }
            }
        }
        function Pi(t) {
            return t.trim().split(/^|\s+/).map((function(t) {
                var n = ""
                  , e = t.indexOf(".");
                return e >= 0 && (n = t.slice(e + 1),
                t = t.slice(0, e)),
                {
                    type: t,
                    name: n
                }
            }
            ))
        }
        function Ui(t) {
            return function() {
                var n = this.__on;
                if (n) {
                    for (var e, r = 0, i = -1, o = n.length; r < o; ++r)
                        e = n[r],
                        t.type && e.type !== t.type || e.name !== t.name ? n[++i] = e : this.removeEventListener(e.type, e.listener, e.capture);
                    ++i ? n.length = i : delete this.__on
                }
            }
        }
        function Li(t, n, e) {
            var r = Ni.hasOwnProperty(t.type) ? Ei : ki;
            return function(i, o, u) {
                var a, s = this.__on, c = r(n, o, u);
                if (s)
                    for (var l = 0, h = s.length; l < h; ++l)
                        if ((a = s[l]).type === t.type && a.name === t.name)
                            return this.removeEventListener(a.type, a.listener, a.capture),
                            this.addEventListener(a.type, a.listener = c, a.capture = e),
                            void (a.value = n);
                this.addEventListener(t.type, c, e),
                a = {
                    type: t.type,
                    name: t.name,
                    value: n,
                    listener: c,
                    capture: e
                },
                s ? s.push(a) : this.__on = [a]
            }
        }
        var Di = function(t, n, e) {
            var r, i, o = Pi(t + ""), u = o.length;
            if (!(arguments.length < 2)) {
                for (a = n ? Li : Ui,
                null == e && (e = !1),
                r = 0; r < u; ++r)
                    this.each(a(o[r], n, e));
                return this
            }
            var a = this.node().__on;
            if (a)
                for (var s, c = 0, l = a.length; c < l; ++c)
                    for (r = 0,
                    s = a[c]; r < u; ++r)
                        if ((i = o[r]).type === s.type && i.name === s.name)
                            return s.value
        };
        function zi(t, n, e, r) {
            var i = Ti;
            t.sourceEvent = Ti,
            Ti = t;
            try {
                return n.apply(e, r)
            } finally {
                Ti = i
            }
        }
        var Oi = function() {
            var t, n = Ti;
            while (t = n.sourceEvent)
                n = t;
            return n
        }
          , Ri = function(t, n) {
            var e = t.ownerSVGElement || t;
            if (e.createSVGPoint) {
                var r = e.createSVGPoint();
                return r.x = n.clientX,
                r.y = n.clientY,
                r = r.matrixTransform(t.getScreenCTM().inverse()),
                [r.x, r.y]
            }
            var i = t.getBoundingClientRect();
            return [n.clientX - i.left - t.clientLeft, n.clientY - i.top - t.clientTop]
        }
          , Ii = function(t) {
            var n = Oi();
            return n.changedTouches && (n = n.changedTouches[0]),
            Ri(t, n)
        };
        function ji() {}
        var Fi = function(t) {
            return null == t ? ji : function() {
                return this.querySelector(t)
            }
        }
          , Yi = function(t) {
            "function" !== typeof t && (t = Fi(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u, a = n[i], s = a.length, c = r[i] = new Array(s), l = 0; l < s; ++l)
                    (o = a[l]) && (u = t.call(o, o.__data__, l, a)) && ("__data__"in o && (u.__data__ = o.__data__),
                    c[l] = u);
            return new yu(r,this._parents)
        };
        function Hi() {
            return []
        }
        var qi = function(t) {
            return null == t ? Hi : function() {
                return this.querySelectorAll(t)
            }
        }
          , Bi = function(t) {
            "function" !== typeof t && (t = qi(t));
            for (var n = this._groups, e = n.length, r = [], i = [], o = 0; o < e; ++o)
                for (var u, a = n[o], s = a.length, c = 0; c < s; ++c)
                    (u = a[c]) && (r.push(t.call(u, u.__data__, c, a)),
                    i.push(u));
            return new yu(r,i)
        }
          , Xi = function(t) {
            return function() {
                return this.matches(t)
            }
        }
          , Vi = function(t) {
            "function" !== typeof t && (t = Xi(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u = n[i], a = u.length, s = r[i] = [], c = 0; c < a; ++c)
                    (o = u[c]) && t.call(o, o.__data__, c, u) && s.push(o);
            return new yu(r,this._parents)
        }
          , $i = function(t) {
            return new Array(t.length)
        }
          , Zi = function() {
            return new yu(this._enter || this._groups.map($i),this._parents)
        };
        function Gi(t, n) {
            this.ownerDocument = t.ownerDocument,
            this.namespaceURI = t.namespaceURI,
            this._next = null,
            this._parent = t,
            this.__data__ = n
        }
        Gi.prototype = {
            constructor: Gi,
            appendChild: function(t) {
                return this._parent.insertBefore(t, this._next)
            },
            insertBefore: function(t, n) {
                return this._parent.insertBefore(t, n)
            },
            querySelector: function(t) {
                return this._parent.querySelector(t)
            },
            querySelectorAll: function(t) {
                return this._parent.querySelectorAll(t)
            }
        };
        var Wi = function(t) {
            return function() {
                return t
            }
        }
          , Ji = "$";
        function Qi(t, n, e, r, i, o) {
            for (var u, a = 0, s = n.length, c = o.length; a < c; ++a)
                (u = n[a]) ? (u.__data__ = o[a],
                r[a] = u) : e[a] = new Gi(t,o[a]);
            for (; a < s; ++a)
                (u = n[a]) && (i[a] = u)
        }
        function Ki(t, n, e, r, i, o, u) {
            var a, s, c, l = {}, h = n.length, f = o.length, p = new Array(h);
            for (a = 0; a < h; ++a)
                (s = n[a]) && (p[a] = c = Ji + u.call(s, s.__data__, a, n),
                c in l ? i[a] = s : l[c] = s);
            for (a = 0; a < f; ++a)
                c = Ji + u.call(t, o[a], a, o),
                (s = l[c]) ? (r[a] = s,
                s.__data__ = o[a],
                l[c] = null) : e[a] = new Gi(t,o[a]);
            for (a = 0; a < h; ++a)
                (s = n[a]) && l[p[a]] === s && (i[a] = s)
        }
        var to = function(t, n) {
            if (!t)
                return p = new Array(this.size()),
                c = -1,
                this.each((function(t) {
                    p[++c] = t
                }
                )),
                p;
            var e = n ? Ki : Qi
              , r = this._parents
              , i = this._groups;
            "function" !== typeof t && (t = Wi(t));
            for (var o = i.length, u = new Array(o), a = new Array(o), s = new Array(o), c = 0; c < o; ++c) {
                var l = r[c]
                  , h = i[c]
                  , f = h.length
                  , p = t.call(l, l && l.__data__, c, r)
                  , _ = p.length
                  , v = a[c] = new Array(_)
                  , d = u[c] = new Array(_)
                  , g = s[c] = new Array(f);
                e(l, h, v, d, g, p, n);
                for (var y, m, w = 0, x = 0; w < _; ++w)
                    if (y = v[w]) {
                        w >= x && (x = w + 1);
                        while (!(m = d[x]) && ++x < _)
                            ;
                        y._next = m || null
                    }
            }
            return u = new yu(u,r),
            u._enter = a,
            u._exit = s,
            u
        }
          , no = function() {
            return new yu(this._exit || this._groups.map($i),this._parents)
        }
          , eo = function(t, n, e) {
            var r = this.enter()
              , i = this
              , o = this.exit();
            return r = "function" === typeof t ? t(r) : r.append(t + ""),
            null != n && (i = n(i)),
            null == e ? o.remove() : e(o),
            r && i ? r.merge(i).order() : i
        }
          , ro = function(t) {
            for (var n = this._groups, e = t._groups, r = n.length, i = e.length, o = Math.min(r, i), u = new Array(r), a = 0; a < o; ++a)
                for (var s, c = n[a], l = e[a], h = c.length, f = u[a] = new Array(h), p = 0; p < h; ++p)
                    (s = c[p] || l[p]) && (f[p] = s);
            for (; a < r; ++a)
                u[a] = n[a];
            return new yu(u,this._parents)
        }
          , io = function() {
            for (var t = this._groups, n = -1, e = t.length; ++n < e; )
                for (var r, i = t[n], o = i.length - 1, u = i[o]; --o >= 0; )
                    (r = i[o]) && (u && 4 ^ r.compareDocumentPosition(u) && u.parentNode.insertBefore(r, u),
                    u = r);
            return this
        }
          , oo = function(t) {
            function n(n, e) {
                return n && e ? t(n.__data__, e.__data__) : !n - !e
            }
            t || (t = uo);
            for (var e = this._groups, r = e.length, i = new Array(r), o = 0; o < r; ++o) {
                for (var u, a = e[o], s = a.length, c = i[o] = new Array(s), l = 0; l < s; ++l)
                    (u = a[l]) && (c[l] = u);
                c.sort(n)
            }
            return new yu(i,this._parents).order()
        };
        function uo(t, n) {
            return t < n ? -1 : t > n ? 1 : t >= n ? 0 : NaN
        }
        var ao = function() {
            var t = arguments[0];
            return arguments[0] = this,
            t.apply(null, arguments),
            this
        }
          , so = function() {
            var t = new Array(this.size())
              , n = -1;
            return this.each((function() {
                t[++n] = this
            }
            )),
            t
        }
          , co = function() {
            for (var t = this._groups, n = 0, e = t.length; n < e; ++n)
                for (var r = t[n], i = 0, o = r.length; i < o; ++i) {
                    var u = r[i];
                    if (u)
                        return u
                }
            return null
        }
          , lo = function() {
            var t = 0;
            return this.each((function() {
                ++t
            }
            )),
            t
        }
          , ho = function() {
            return !this.node()
        }
          , fo = function(t) {
            for (var n = this._groups, e = 0, r = n.length; e < r; ++e)
                for (var i, o = n[e], u = 0, a = o.length; u < a; ++u)
                    (i = o[u]) && t.call(i, i.__data__, u, o);
            return this
        }
          , po = "http://www.w3.org/1999/xhtml"
          , _o = {
            svg: "http://www.w3.org/2000/svg",
            xhtml: po,
            xlink: "http://www.w3.org/1999/xlink",
            xml: "http://www.w3.org/XML/1998/namespace",
            xmlns: "http://www.w3.org/2000/xmlns/"
        }
          , vo = function(t) {
            var n = t += ""
              , e = n.indexOf(":");
            return e >= 0 && "xmlns" !== (n = t.slice(0, e)) && (t = t.slice(e + 1)),
            _o.hasOwnProperty(n) ? {
                space: _o[n],
                local: t
            } : t
        };
        function go(t) {
            return function() {
                this.removeAttribute(t)
            }
        }
        function yo(t) {
            return function() {
                this.removeAttributeNS(t.space, t.local)
            }
        }
        function mo(t, n) {
            return function() {
                this.setAttribute(t, n)
            }
        }
        function wo(t, n) {
            return function() {
                this.setAttributeNS(t.space, t.local, n)
            }
        }
        function xo(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? this.removeAttribute(t) : this.setAttribute(t, e)
            }
        }
        function bo(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? this.removeAttributeNS(t.space, t.local) : this.setAttributeNS(t.space, t.local, e)
            }
        }
        var Ao = function(t, n) {
            var e = vo(t);
            if (arguments.length < 2) {
                var r = this.node();
                return e.local ? r.getAttributeNS(e.space, e.local) : r.getAttribute(e)
            }
            return this.each((null == n ? e.local ? yo : go : "function" === typeof n ? e.local ? bo : xo : e.local ? wo : mo)(e, n))
        }
          , Mo = function(t) {
            return t.ownerDocument && t.ownerDocument.defaultView || t.document && t || t.defaultView
        };
        function Co(t) {
            return function() {
                this.style.removeProperty(t)
            }
        }
        function No(t, n, e) {
            return function() {
                this.style.setProperty(t, n, e)
            }
        }
        function To(t, n, e) {
            return function() {
                var r = n.apply(this, arguments);
                null == r ? this.style.removeProperty(t) : this.style.setProperty(t, r, e)
            }
        }
        var So = function(t, n, e) {
            return arguments.length > 1 ? this.each((null == n ? Co : "function" === typeof n ? To : No)(t, n, null == e ? "" : e)) : Eo(this.node(), t)
        };
        function Eo(t, n) {
            return t.style.getPropertyValue(n) || Mo(t).getComputedStyle(t, null).getPropertyValue(n)
        }
        function ko(t) {
            return function() {
                delete this[t]
            }
        }
        function Po(t, n) {
            return function() {
                this[t] = n
            }
        }
        function Uo(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? delete this[t] : this[t] = e
            }
        }
        var Lo = function(t, n) {
            return arguments.length > 1 ? this.each((null == n ? ko : "function" === typeof n ? Uo : Po)(t, n)) : this.node()[t]
        };
        function Do(t) {
            return t.trim().split(/^|\s+/)
        }
        function zo(t) {
            return t.classList || new Oo(t)
        }
        function Oo(t) {
            this._node = t,
            this._names = Do(t.getAttribute("class") || "")
        }
        function Ro(t, n) {
            var e = zo(t)
              , r = -1
              , i = n.length;
            while (++r < i)
                e.add(n[r])
        }
        function Io(t, n) {
            var e = zo(t)
              , r = -1
              , i = n.length;
            while (++r < i)
                e.remove(n[r])
        }
        function jo(t) {
            return function() {
                Ro(this, t)
            }
        }
        function Fo(t) {
            return function() {
                Io(this, t)
            }
        }
        function Yo(t, n) {
            return function() {
                (n.apply(this, arguments) ? Ro : Io)(this, t)
            }
        }
        Oo.prototype = {
            add: function(t) {
                var n = this._names.indexOf(t);
                n < 0 && (this._names.push(t),
                this._node.setAttribute("class", this._names.join(" ")))
            },
            remove: function(t) {
                var n = this._names.indexOf(t);
                n >= 0 && (this._names.splice(n, 1),
                this._node.setAttribute("class", this._names.join(" ")))
            },
            contains: function(t) {
                return this._names.indexOf(t) >= 0
            }
        };
        var Ho = function(t, n) {
            var e = Do(t + "");
            if (arguments.length < 2) {
                var r = zo(this.node())
                  , i = -1
                  , o = e.length;
                while (++i < o)
                    if (!r.contains(e[i]))
                        return !1;
                return !0
            }
            return this.each(("function" === typeof n ? Yo : n ? jo : Fo)(e, n))
        };
        function qo() {
            this.textContent = ""
        }
        function Bo(t) {
            return function() {
                this.textContent = t
            }
        }
        function Xo(t) {
            return function() {
                var n = t.apply(this, arguments);
                this.textContent = null == n ? "" : n
            }
        }
        var Vo = function(t) {
            return arguments.length ? this.each(null == t ? qo : ("function" === typeof t ? Xo : Bo)(t)) : this.node().textContent
        };
        function $o() {
            this.innerHTML = ""
        }
        function Zo(t) {
            return function() {
                this.innerHTML = t
            }
        }
        function Go(t) {
            return function() {
                var n = t.apply(this, arguments);
                this.innerHTML = null == n ? "" : n
            }
        }
        var Wo = function(t) {
            return arguments.length ? this.each(null == t ? $o : ("function" === typeof t ? Go : Zo)(t)) : this.node().innerHTML
        };
        function Jo() {
            this.nextSibling && this.parentNode.appendChild(this)
        }
        var Qo = function() {
            return this.each(Jo)
        };
        function Ko() {
            this.previousSibling && this.parentNode.insertBefore(this, this.parentNode.firstChild)
        }
        var tu = function() {
            return this.each(Ko)
        };
        function nu(t) {
            return function() {
                var n = this.ownerDocument
                  , e = this.namespaceURI;
                return e === po && n.documentElement.namespaceURI === po ? n.createElement(t) : n.createElementNS(e, t)
            }
        }
        function eu(t) {
            return function() {
                return this.ownerDocument.createElementNS(t.space, t.local)
            }
        }
        var ru = function(t) {
            var n = vo(t);
            return (n.local ? eu : nu)(n)
        }
          , iu = function(t) {
            var n = "function" === typeof t ? t : ru(t);
            return this.select((function() {
                return this.appendChild(n.apply(this, arguments))
            }
            ))
        };
        function ou() {
            return null
        }
        var uu = function(t, n) {
            var e = "function" === typeof t ? t : ru(t)
              , r = null == n ? ou : "function" === typeof n ? n : Fi(n);
            return this.select((function() {
                return this.insertBefore(e.apply(this, arguments), r.apply(this, arguments) || null)
            }
            ))
        };
        function au() {
            var t = this.parentNode;
            t && t.removeChild(this)
        }
        var su = function() {
            return this.each(au)
        };
        function cu() {
            var t = this.cloneNode(!1)
              , n = this.parentNode;
            return n ? n.insertBefore(t, this.nextSibling) : t
        }
        function lu() {
            var t = this.cloneNode(!0)
              , n = this.parentNode;
            return n ? n.insertBefore(t, this.nextSibling) : t
        }
        var hu = function(t) {
            return this.select(t ? lu : cu)
        }
          , fu = function(t) {
            return arguments.length ? this.property("__data__", t) : this.node().__data__
        };
        function pu(t, n, e) {
            var r = Mo(t)
              , i = r.CustomEvent;
            "function" === typeof i ? i = new i(n,e) : (i = r.document.createEvent("Event"),
            e ? (i.initEvent(n, e.bubbles, e.cancelable),
            i.detail = e.detail) : i.initEvent(n, !1, !1)),
            t.dispatchEvent(i)
        }
        function _u(t, n) {
            return function() {
                return pu(this, t, n)
            }
        }
        function vu(t, n) {
            return function() {
                return pu(this, t, n.apply(this, arguments))
            }
        }
        var du = function(t, n) {
            return this.each(("function" === typeof n ? vu : _u)(t, n))
        }
          , gu = [null];
        function yu(t, n) {
            this._groups = t,
            this._parents = n
        }
        function mu() {
            return new yu([[document.documentElement]],gu)
        }
        yu.prototype = mu.prototype = {
            constructor: yu,
            select: Yi,
            selectAll: Bi,
            filter: Vi,
            data: to,
            enter: Zi,
            exit: no,
            join: eo,
            merge: ro,
            order: io,
            sort: oo,
            call: ao,
            nodes: so,
            node: co,
            size: lo,
            empty: ho,
            each: fo,
            attr: Ao,
            style: So,
            property: Lo,
            classed: Ho,
            text: Vo,
            html: Wo,
            raise: Qo,
            lower: tu,
            append: iu,
            insert: uu,
            remove: su,
            clone: hu,
            datum: fu,
            on: Di,
            dispatch: du
        };
        var wu = mu
          , xu = function(t) {
            return "string" === typeof t ? new yu([[document.querySelector(t)]],[document.documentElement]) : new yu([[t]],gu)
        }
          , bu = function(t, n, e) {
            arguments.length < 3 && (e = n,
            n = Oi().changedTouches);
            for (var r, i = 0, o = n ? n.length : 0; i < o; ++i)
                if ((r = n[i]).identifier === e)
                    return Ri(t, r);
            return null
        };
        function Au() {
            Ti.stopImmediatePropagation()
        }
        var Mu = function() {
            Ti.preventDefault(),
            Ti.stopImmediatePropagation()
        }
          , Cu = function(t) {
            var n = t.document.documentElement
              , e = xu(t).on("dragstart.drag", Mu, !0);
            "onselectstart"in n ? e.on("selectstart.drag", Mu, !0) : (n.__noselect = n.style.MozUserSelect,
            n.style.MozUserSelect = "none")
        };
        function Nu(t, n) {
            var e = t.document.documentElement
              , r = xu(t).on("dragstart.drag", null);
            n && (r.on("click.drag", Mu, !0),
            setTimeout((function() {
                r.on("click.drag", null)
            }
            ), 0)),
            "onselectstart"in e ? r.on("selectstart.drag", null) : (e.style.MozUserSelect = e.__noselect,
            delete e.__noselect)
        }
        var Tu = function(t) {
            return function() {
                return t
            }
        };
        function Su(t, n, e, r, i, o, u, a, s, c) {
            this.target = t,
            this.type = n,
            this.subject = e,
            this.identifier = r,
            this.active = i,
            this.x = o,
            this.y = u,
            this.dx = a,
            this.dy = s,
            this._ = c
        }
        function Eu() {
            return !Ti.ctrlKey && !Ti.button
        }
        function ku() {
            return this.parentNode
        }
        function Pu(t) {
            return null == t ? {
                x: Ti.x,
                y: Ti.y
            } : t
        }
        function Uu() {
            return navigator.maxTouchPoints || "ontouchstart"in this
        }
        Su.prototype.on = function() {
            var t = this._.on.apply(this._, arguments);
            return t === this._ ? this : t
        }
        ;
        var Lu = function() {
            var t, n, e, r, i = Eu, o = ku, u = Pu, a = Uu, s = {}, c = m("start", "drag", "end"), l = 0, h = 0;
            function f(t) {
                t.on("mousedown.drag", p).filter(a).on("touchstart.drag", d).on("touchmove.drag", g).on("touchend.drag touchcancel.drag", y).style("touch-action", "none").style("-webkit-tap-highlight-color", "rgba(0,0,0,0)")
            }
            function p() {
                if (!r && i.apply(this, arguments)) {
                    var u = w("mouse", o.apply(this, arguments), Ii, this, arguments);
                    u && (xu(Ti.view).on("mousemove.drag", _, !0).on("mouseup.drag", v, !0),
                    Cu(Ti.view),
                    Au(),
                    e = !1,
                    t = Ti.clientX,
                    n = Ti.clientY,
                    u("start"))
                }
            }
            function _() {
                if (Mu(),
                !e) {
                    var r = Ti.clientX - t
                      , i = Ti.clientY - n;
                    e = r * r + i * i > h
                }
                s.mouse("drag")
            }
            function v() {
                xu(Ti.view).on("mousemove.drag mouseup.drag", null),
                Nu(Ti.view, e),
                Mu(),
                s.mouse("end")
            }
            function d() {
                if (i.apply(this, arguments)) {
                    var t, n, e = Ti.changedTouches, r = o.apply(this, arguments), u = e.length;
                    for (t = 0; t < u; ++t)
                        (n = w(e[t].identifier, r, bu, this, arguments)) && (Au(),
                        n("start"))
                }
            }
            function g() {
                var t, n, e = Ti.changedTouches, r = e.length;
                for (t = 0; t < r; ++t)
                    (n = s[e[t].identifier]) && (Mu(),
                    n("drag"))
            }
            function y() {
                var t, n, e = Ti.changedTouches, i = e.length;
                for (r && clearTimeout(r),
                r = setTimeout((function() {
                    r = null
                }
                ), 500),
                t = 0; t < i; ++t)
                    (n = s[e[t].identifier]) && (Au(),
                    n("end"))
            }
            function w(t, n, e, r, i) {
                var o, a, h, p = e(n, t), _ = c.copy();
                if (zi(new Su(f,"beforestart",o,t,l,p[0],p[1],0,0,_), (function() {
                    return null != (Ti.subject = o = u.apply(r, i)) && (a = o.x - p[0] || 0,
                    h = o.y - p[1] || 0,
                    !0)
                }
                )))
                    return function u(c) {
                        var v, d = p;
                        switch (c) {
                        case "start":
                            s[t] = u,
                            v = l++;
                            break;
                        case "end":
                            delete s[t],
                            --l;
                        case "drag":
                            p = e(n, t),
                            v = l;
                            break
                        }
                        zi(new Su(f,c,o,t,v,p[0] + a,p[1] + h,p[0] - d[0],p[1] - d[1],_), _.apply, _, [c, r, i])
                    }
            }
            return f.filter = function(t) {
                return arguments.length ? (i = "function" === typeof t ? t : Tu(!!t),
                f) : i
            }
            ,
            f.container = function(t) {
                return arguments.length ? (o = "function" === typeof t ? t : Tu(t),
                f) : o
            }
            ,
            f.subject = function(t) {
                return arguments.length ? (u = "function" === typeof t ? t : Tu(t),
                f) : u
            }
            ,
            f.touchable = function(t) {
                return arguments.length ? (a = "function" === typeof t ? t : Tu(!!t),
                f) : a
            }
            ,
            f.on = function() {
                var t = c.on.apply(c, arguments);
                return t === c ? f : t
            }
            ,
            f.clickDistance = function(t) {
                return arguments.length ? (h = (t = +t) * t,
                f) : Math.sqrt(h)
            }
            ,
            f
        }
          , Du = 4 / 11
          , zu = 6 / 11
          , Ou = 8 / 11
          , Ru = .75
          , Iu = 9 / 11
          , ju = 10 / 11
          , Fu = .9375
          , Yu = 21 / 22
          , Hu = 63 / 64
          , qu = 1 / Du / Du;
        function Bu(t) {
            return (t = +t) < Du ? qu * t * t : t < Ou ? qu * (t -= zu) * t + Ru : t < ju ? qu * (t -= Iu) * t + Fu : qu * (t -= Yu) * t + Hu
        }
        var Xu = function(t) {
            var n = +this._x.call(null, t)
              , e = +this._y.call(null, t);
            return Vu(this.cover(n, e), n, e, t)
        };
        function Vu(t, n, e, r) {
            if (isNaN(n) || isNaN(e))
                return t;
            var i, o, u, a, s, c, l, h, f, p = t._root, _ = {
                data: r
            }, v = t._x0, d = t._y0, g = t._x1, y = t._y1;
            if (!p)
                return t._root = _,
                t;
            while (p.length)
                if ((c = n >= (o = (v + g) / 2)) ? v = o : g = o,
                (l = e >= (u = (d + y) / 2)) ? d = u : y = u,
                i = p,
                !(p = p[h = l << 1 | c]))
                    return i[h] = _,
                    t;
            if (a = +t._x.call(null, p.data),
            s = +t._y.call(null, p.data),
            n === a && e === s)
                return _.next = p,
                i ? i[h] = _ : t._root = _,
                t;
            do {
                i = i ? i[h] = new Array(4) : t._root = new Array(4),
                (c = n >= (o = (v + g) / 2)) ? v = o : g = o,
                (l = e >= (u = (d + y) / 2)) ? d = u : y = u
            } while ((h = l << 1 | c) === (f = (s >= u) << 1 | a >= o));
            return i[f] = p,
            i[h] = _,
            t
        }
        function $u(t) {
            var n, e, r, i, o = t.length, u = new Array(o), a = new Array(o), s = 1 / 0, c = 1 / 0, l = -1 / 0, h = -1 / 0;
            for (e = 0; e < o; ++e)
                isNaN(r = +this._x.call(null, n = t[e])) || isNaN(i = +this._y.call(null, n)) || (u[e] = r,
                a[e] = i,
                r < s && (s = r),
                r > l && (l = r),
                i < c && (c = i),
                i > h && (h = i));
            if (s > l || c > h)
                return this;
            for (this.cover(s, c).cover(l, h),
            e = 0; e < o; ++e)
                Vu(this, u[e], a[e], t[e]);
            return this
        }
        var Zu = function(t, n) {
            if (isNaN(t = +t) || isNaN(n = +n))
                return this;
            var e = this._x0
              , r = this._y0
              , i = this._x1
              , o = this._y1;
            if (isNaN(e))
                i = (e = Math.floor(t)) + 1,
                o = (r = Math.floor(n)) + 1;
            else {
                var u, a, s = i - e, c = this._root;
                while (e > t || t >= i || r > n || n >= o)
                    switch (a = (n < r) << 1 | t < e,
                    u = new Array(4),
                    u[a] = c,
                    c = u,
                    s *= 2,
                    a) {
                    case 0:
                        i = e + s,
                        o = r + s;
                        break;
                    case 1:
                        e = i - s,
                        o = r + s;
                        break;
                    case 2:
                        i = e + s,
                        r = o - s;
                        break;
                    case 3:
                        e = i - s,
                        r = o - s;
                        break
                    }
                this._root && this._root.length && (this._root = c)
            }
            return this._x0 = e,
            this._y0 = r,
            this._x1 = i,
            this._y1 = o,
            this
        }
          , Gu = function() {
            var t = [];
            return this.visit((function(n) {
                if (!n.length)
                    do {
                        t.push(n.data)
                    } while (n = n.next)
            }
            )),
            t
        }
          , Wu = function(t) {
            return arguments.length ? this.cover(+t[0][0], +t[0][1]).cover(+t[1][0], +t[1][1]) : isNaN(this._x0) ? void 0 : [[this._x0, this._y0], [this._x1, this._y1]]
        }
          , Ju = function(t, n, e, r, i) {
            this.node = t,
            this.x0 = n,
            this.y0 = e,
            this.x1 = r,
            this.y1 = i
        }
          , Qu = function(t, n, e) {
            var r, i, o, u, a, s, c, l = this._x0, h = this._y0, f = this._x1, p = this._y1, _ = [], v = this._root;
            v && _.push(new Ju(v,l,h,f,p)),
            null == e ? e = 1 / 0 : (l = t - e,
            h = n - e,
            f = t + e,
            p = n + e,
            e *= e);
            while (s = _.pop())
                if (!(!(v = s.node) || (i = s.x0) > f || (o = s.y0) > p || (u = s.x1) < l || (a = s.y1) < h))
                    if (v.length) {
                        var d = (i + u) / 2
                          , g = (o + a) / 2;
                        _.push(new Ju(v[3],d,g,u,a), new Ju(v[2],i,g,d,a), new Ju(v[1],d,o,u,g), new Ju(v[0],i,o,d,g)),
                        (c = (n >= g) << 1 | t >= d) && (s = _[_.length - 1],
                        _[_.length - 1] = _[_.length - 1 - c],
                        _[_.length - 1 - c] = s)
                    } else {
                        var y = t - +this._x.call(null, v.data)
                          , m = n - +this._y.call(null, v.data)
                          , w = y * y + m * m;
                        if (w < e) {
                            var x = Math.sqrt(e = w);
                            l = t - x,
                            h = n - x,
                            f = t + x,
                            p = n + x,
                            r = v.data
                        }
                    }
            return r
        }
          , Ku = function(t) {
            if (isNaN(o = +this._x.call(null, t)) || isNaN(u = +this._y.call(null, t)))
                return this;
            var n, e, r, i, o, u, a, s, c, l, h, f, p = this._root, _ = this._x0, v = this._y0, d = this._x1, g = this._y1;
            if (!p)
                return this;
            if (p.length)
                while (1) {
                    if ((c = o >= (a = (_ + d) / 2)) ? _ = a : d = a,
                    (l = u >= (s = (v + g) / 2)) ? v = s : g = s,
                    n = p,
                    !(p = p[h = l << 1 | c]))
                        return this;
                    if (!p.length)
                        break;
                    (n[h + 1 & 3] || n[h + 2 & 3] || n[h + 3 & 3]) && (e = n,
                    f = h)
                }
            while (p.data !== t)
                if (r = p,
                !(p = p.next))
                    return this;
            return (i = p.next) && delete p.next,
            r ? (i ? r.next = i : delete r.next,
            this) : n ? (i ? n[h] = i : delete n[h],
            (p = n[0] || n[1] || n[2] || n[3]) && p === (n[3] || n[2] || n[1] || n[0]) && !p.length && (e ? e[f] = p : this._root = p),
            this) : (this._root = i,
            this)
        };
        function ta(t) {
            for (var n = 0, e = t.length; n < e; ++n)
                this.remove(t[n]);
            return this
        }
        var na = function() {
            return this._root
        }
          , ea = function() {
            var t = 0;
            return this.visit((function(n) {
                if (!n.length)
                    do {
                        ++t
                    } while (n = n.next)
            }
            )),
            t
        }
          , ra = function(t) {
            var n, e, r, i, o, u, a = [], s = this._root;
            s && a.push(new Ju(s,this._x0,this._y0,this._x1,this._y1));
            while (n = a.pop())
                if (!t(s = n.node, r = n.x0, i = n.y0, o = n.x1, u = n.y1) && s.length) {
                    var c = (r + o) / 2
                      , l = (i + u) / 2;
                    (e = s[3]) && a.push(new Ju(e,c,l,o,u)),
                    (e = s[2]) && a.push(new Ju(e,r,l,c,u)),
                    (e = s[1]) && a.push(new Ju(e,c,i,o,l)),
                    (e = s[0]) && a.push(new Ju(e,r,i,c,l))
                }
            return this
        }
          , ia = function(t) {
            var n, e = [], r = [];
            this._root && e.push(new Ju(this._root,this._x0,this._y0,this._x1,this._y1));
            while (n = e.pop()) {
                var i = n.node;
                if (i.length) {
                    var o, u = n.x0, a = n.y0, s = n.x1, c = n.y1, l = (u + s) / 2, h = (a + c) / 2;
                    (o = i[0]) && e.push(new Ju(o,u,a,l,h)),
                    (o = i[1]) && e.push(new Ju(o,l,a,s,h)),
                    (o = i[2]) && e.push(new Ju(o,u,h,l,c)),
                    (o = i[3]) && e.push(new Ju(o,l,h,s,c))
                }
                r.push(n)
            }
            while (n = r.pop())
                t(n.node, n.x0, n.y0, n.x1, n.y1);
            return this
        };
        function oa(t) {
            return t[0]
        }
        var ua = function(t) {
            return arguments.length ? (this._x = t,
            this) : this._x
        };
        function aa(t) {
            return t[1]
        }
        var sa = function(t) {
            return arguments.length ? (this._y = t,
            this) : this._y
        };
        function ca(t, n, e) {
            var r = new la(null == n ? oa : n,null == e ? aa : e,NaN,NaN,NaN,NaN);
            return null == t ? r : r.addAll(t)
        }
        function la(t, n, e, r, i, o) {
            this._x = t,
            this._y = n,
            this._x0 = e,
            this._y0 = r,
            this._x1 = i,
            this._y1 = o,
            this._root = void 0
        }
        function ha(t) {
            var n = {
                data: t.data
            }
              , e = n;
            while (t = t.next)
                e = e.next = {
                    data: t.data
                };
            return n
        }
        var fa = ca.prototype = la.prototype;
        fa.copy = function() {
            var t, n, e = new la(this._x,this._y,this._x0,this._y0,this._x1,this._y1), r = this._root;
            if (!r)
                return e;
            if (!r.length)
                return e._root = ha(r),
                e;
            t = [{
                source: r,
                target: e._root = new Array(4)
            }];
            while (r = t.pop())
                for (var i = 0; i < 4; ++i)
                    (n = r.source[i]) && (n.length ? t.push({
                        source: n,
                        target: r.target[i] = new Array(4)
                    }) : r.target[i] = ha(n));
            return e
        }
        ,
        fa.add = Xu,
        fa.addAll = $u,
        fa.cover = Zu,
        fa.data = Gu,
        fa.extent = Wu,
        fa.find = Qu,
        fa.remove = Ku,
        fa.removeAll = ta,
        fa.root = na,
        fa.size = ea,
        fa.visit = ra,
        fa.visitAfter = ia,
        fa.x = ua,
        fa.y = sa;
        Math.PI,
        Math.sqrt(5);
        var pa = function() {
            return Math.random()
        }
          , _a = (function t(n) {
            function e(t, e) {
                return t = null == t ? 0 : +t,
                e = null == e ? 1 : +e,
                1 === arguments.length ? (e = t,
                t = 0) : e -= t,
                function() {
                    return n() * e + t
                }
            }
            return e.source = t,
            e
        }(pa),
        function t(n) {
            function e(t, e) {
                var r, i;
                return t = null == t ? 0 : +t,
                e = null == e ? 1 : +e,
                function() {
                    var o;
                    if (null != r)
                        o = r,
                        r = null;
                    else
                        do {
                            r = 2 * n() - 1,
                            o = 2 * n() - 1,
                            i = r * r + o * o
                        } while (!i || i > 1);
                    return t + e * o * Math.sqrt(-2 * Math.log(i) / i)
                }
            }
            return e.source = t,
            e
        }(pa))
          , va = (function t(n) {
            function e() {
                var t = _a.source(n).apply(this, arguments);
                return function() {
                    return Math.exp(t())
                }
            }
            return e.source = t,
            e
        }(pa),
        function t(n) {
            function e(t) {
                return function() {
                    for (var e = 0, r = 0; r < t; ++r)
                        e += n();
                    return e
                }
            }
            return e.source = t,
            e
        }(pa));
        (function t(n) {
            function e(t) {
                var e = va.source(n)(t);
                return function() {
                    return e() / t
                }
            }
            return e.source = t,
            e
        }
        )(pa),
        function t(n) {
            function e(t) {
                return function() {
                    return -Math.log(1 - n()) / t
                }
            }
            return e.source = t,
            e
        }(pa);
        var da = Array.prototype;
        da.map,
        da.slice;
        var ga = /^(?:(.)?([<>=^]))?([+\-( ])?([$#])?(0)?(\d+)?(,)?(\.\d+)?(~)?([a-z%])?$/i;
        function ya(t) {
            if (!(n = ga.exec(t)))
                throw new Error("invalid format: " + t);
            var n;
            return new ma({
                fill: n[1],
                align: n[2],
                sign: n[3],
                symbol: n[4],
                zero: n[5],
                width: n[6],
                comma: n[7],
                precision: n[8] && n[8].slice(1),
                trim: n[9],
                type: n[10]
            })
        }
        function ma(t) {
            this.fill = void 0 === t.fill ? " " : t.fill + "",
            this.align = void 0 === t.align ? ">" : t.align + "",
            this.sign = void 0 === t.sign ? "-" : t.sign + "",
            this.symbol = void 0 === t.symbol ? "" : t.symbol + "",
            this.zero = !!t.zero,
            this.width = void 0 === t.width ? void 0 : +t.width,
            this.comma = !!t.comma,
            this.precision = void 0 === t.precision ? void 0 : +t.precision,
            this.trim = !!t.trim,
            this.type = void 0 === t.type ? "" : t.type + ""
        }
        ya.prototype = ma.prototype,
        ma.prototype.toString = function() {
            return this.fill + this.align + this.sign + this.symbol + (this.zero ? "0" : "") + (void 0 === this.width ? "" : Math.max(1, 0 | this.width)) + (this.comma ? "," : "") + (void 0 === this.precision ? "" : "." + Math.max(0, 0 | this.precision)) + (this.trim ? "~" : "") + this.type
        }
        ;
        var wa = function(t) {
            return Math.abs(t = Math.round(t)) >= 1e21 ? t.toLocaleString("en").replace(/,/g, "") : t.toString(10)
        };
        function xa(t, n) {
            if ((e = (t = n ? t.toExponential(n - 1) : t.toExponential()).indexOf("e")) < 0)
                return null;
            var e, r = t.slice(0, e);
            return [r.length > 1 ? r[0] + r.slice(2) : r, +t.slice(e + 1)]
        }
        var ba, Aa, Ma = function(t) {
            return t = xa(Math.abs(t)),
            t ? t[1] : NaN
        }, Ca = function(t, n) {
            return function(e, r) {
                var i = e.length
                  , o = []
                  , u = 0
                  , a = t[0]
                  , s = 0;
                while (i > 0 && a > 0) {
                    if (s + a + 1 > r && (a = Math.max(1, r - s)),
                    o.push(e.substring(i -= a, i + a)),
                    (s += a + 1) > r)
                        break;
                    a = t[u = (u + 1) % t.length]
                }
                return o.reverse().join(n)
            }
        }, Na = function(t) {
            return function(n) {
                return n.replace(/[0-9]/g, (function(n) {
                    return t[+n]
                }
                ))
            }
        }, Ta = function(t) {
            t: for (var n, e = t.length, r = 1, i = -1; r < e; ++r)
                switch (t[r]) {
                case ".":
                    i = n = r;
                    break;
                case "0":
                    0 === i && (i = r),
                    n = r;
                    break;
                default:
                    if (!+t[r])
                        break t;
                    i > 0 && (i = 0);
                    break
                }
            return i > 0 ? t.slice(0, i) + t.slice(n + 1) : t
        }, Sa = function(t, n) {
            var e = xa(t, n);
            if (!e)
                return t + "";
            var r = e[0]
              , i = e[1]
              , o = i - (ba = 3 * Math.max(-8, Math.min(8, Math.floor(i / 3)))) + 1
              , u = r.length;
            return o === u ? r : o > u ? r + new Array(o - u + 1).join("0") : o > 0 ? r.slice(0, o) + "." + r.slice(o) : "0." + new Array(1 - o).join("0") + xa(t, Math.max(0, n + o - 1))[0]
        }, Ea = function(t, n) {
            var e = xa(t, n);
            if (!e)
                return t + "";
            var r = e[0]
              , i = e[1];
            return i < 0 ? "0." + new Array(-i).join("0") + r : r.length > i + 1 ? r.slice(0, i + 1) + "." + r.slice(i + 1) : r + new Array(i - r.length + 2).join("0")
        }, ka = {
            "%": function(t, n) {
                return (100 * t).toFixed(n)
            },
            b: function(t) {
                return Math.round(t).toString(2)
            },
            c: function(t) {
                return t + ""
            },
            d: wa,
            e: function(t, n) {
                return t.toExponential(n)
            },
            f: function(t, n) {
                return t.toFixed(n)
            },
            g: function(t, n) {
                return t.toPrecision(n)
            },
            o: function(t) {
                return Math.round(t).toString(8)
            },
            p: function(t, n) {
                return Ea(100 * t, n)
            },
            r: Ea,
            s: Sa,
            X: function(t) {
                return Math.round(t).toString(16).toUpperCase()
            },
            x: function(t) {
                return Math.round(t).toString(16)
            }
        }, Pa = function(t) {
            return t
        }, Ua = Array.prototype.map, La = ["y", "z", "a", "f", "p", "n", "µ", "m", "", "k", "M", "G", "T", "P", "E", "Z", "Y"], Da = function(t) {
            var n = void 0 === t.grouping || void 0 === t.thousands ? Pa : Ca(Ua.call(t.grouping, Number), t.thousands + "")
              , e = void 0 === t.currency ? "" : t.currency[0] + ""
              , r = void 0 === t.currency ? "" : t.currency[1] + ""
              , i = void 0 === t.decimal ? "." : t.decimal + ""
              , o = void 0 === t.numerals ? Pa : Na(Ua.call(t.numerals, String))
              , u = void 0 === t.percent ? "%" : t.percent + ""
              , a = void 0 === t.minus ? "-" : t.minus + ""
              , s = void 0 === t.nan ? "NaN" : t.nan + "";
            function c(t) {
                t = ya(t);
                var c = t.fill
                  , l = t.align
                  , h = t.sign
                  , f = t.symbol
                  , p = t.zero
                  , _ = t.width
                  , v = t.comma
                  , d = t.precision
                  , g = t.trim
                  , y = t.type;
                "n" === y ? (v = !0,
                y = "g") : ka[y] || (void 0 === d && (d = 12),
                g = !0,
                y = "g"),
                (p || "0" === c && "=" === l) && (p = !0,
                c = "0",
                l = "=");
                var m = "$" === f ? e : "#" === f && /[boxX]/.test(y) ? "0" + y.toLowerCase() : ""
                  , w = "$" === f ? r : /[%p]/.test(y) ? u : ""
                  , x = ka[y]
                  , b = /[defgprs%]/.test(y);
                function A(t) {
                    var e, r, u, f = m, A = w;
                    if ("c" === y)
                        A = x(t) + A,
                        t = "";
                    else {
                        t = +t;
                        var M = t < 0 || 1 / t < 0;
                        if (t = isNaN(t) ? s : x(Math.abs(t), d),
                        g && (t = Ta(t)),
                        M && 0 === +t && "+" !== h && (M = !1),
                        f = (M ? "(" === h ? h : a : "-" === h || "(" === h ? "" : h) + f,
                        A = ("s" === y ? La[8 + ba / 3] : "") + A + (M && "(" === h ? ")" : ""),
                        b) {
                            e = -1,
                            r = t.length;
                            while (++e < r)
                                if (u = t.charCodeAt(e),
                                48 > u || u > 57) {
                                    A = (46 === u ? i + t.slice(e + 1) : t.slice(e)) + A,
                                    t = t.slice(0, e);
                                    break
                                }
                        }
                    }
                    v && !p && (t = n(t, 1 / 0));
                    var C = f.length + t.length + A.length
                      , N = C < _ ? new Array(_ - C + 1).join(c) : "";
                    switch (v && p && (t = n(N + t, N.length ? _ - A.length : 1 / 0),
                    N = ""),
                    l) {
                    case "<":
                        t = f + t + A + N;
                        break;
                    case "=":
                        t = f + N + t + A;
                        break;
                    case "^":
                        t = N.slice(0, C = N.length >> 1) + f + t + A + N.slice(C);
                        break;
                    default:
                        t = N + f + t + A;
                        break
                    }
                    return o(t)
                }
                return d = void 0 === d ? 6 : /[gprs]/.test(y) ? Math.max(1, Math.min(21, d)) : Math.max(0, Math.min(20, d)),
                A.toString = function() {
                    return t + ""
                }
                ,
                A
            }
            function l(t, n) {
                var e = c((t = ya(t),
                t.type = "f",
                t))
                  , r = 3 * Math.max(-8, Math.min(8, Math.floor(Ma(n) / 3)))
                  , i = Math.pow(10, -r)
                  , o = La[8 + r / 3];
                return function(t) {
                    return e(i * t) + o
                }
            }
            return {
                format: c,
                formatPrefix: l
            }
        };
        function za(t) {
            return Aa = Da(t),
            Aa.format,
            Aa.formatPrefix,
            Aa
        }
        za({
            decimal: ".",
            thousands: ",",
            grouping: [3],
            currency: ["$", ""],
            minus: "-"
        });
        var Oa = new Date
          , Ra = new Date;
        function Ia(t, n, e, r) {
            function i(n) {
                return t(n = 0 === arguments.length ? new Date : new Date(+n)),
                n
            }
            return i.floor = function(n) {
                return t(n = new Date(+n)),
                n
            }
            ,
            i.ceil = function(e) {
                return t(e = new Date(e - 1)),
                n(e, 1),
                t(e),
                e
            }
            ,
            i.round = function(t) {
                var n = i(t)
                  , e = i.ceil(t);
                return t - n < e - t ? n : e
            }
            ,
            i.offset = function(t, e) {
                return n(t = new Date(+t), null == e ? 1 : Math.floor(e)),
                t
            }
            ,
            i.range = function(e, r, o) {
                var u, a = [];
                if (e = i.ceil(e),
                o = null == o ? 1 : Math.floor(o),
                !(e < r) || !(o > 0))
                    return a;
                do {
                    a.push(u = new Date(+e)),
                    n(e, o),
                    t(e)
                } while (u < e && e < r);
                return a
            }
            ,
            i.filter = function(e) {
                return Ia((function(n) {
                    if (n >= n)
                        while (t(n),
                        !e(n))
                            n.setTime(n - 1)
                }
                ), (function(t, r) {
                    if (t >= t)
                        if (r < 0)
                            while (++r <= 0)
                                while (n(t, -1),
                                !e(t))
                                    ;
                        else
                            while (--r >= 0)
                                while (n(t, 1),
                                !e(t))
                                    ;
                }
                ))
            }
            ,
            e && (i.count = function(n, r) {
                return Oa.setTime(+n),
                Ra.setTime(+r),
                t(Oa),
                t(Ra),
                Math.floor(e(Oa, Ra))
            }
            ,
            i.every = function(t) {
                return t = Math.floor(t),
                isFinite(t) && t > 0 ? t > 1 ? i.filter(r ? function(n) {
                    return r(n) % t === 0
                }
                : function(n) {
                    return i.count(0, n) % t === 0
                }
                ) : i : null
            }
            ),
            i
        }
        var ja = Ia((function(t) {
            t.setMonth(0, 1),
            t.setHours(0, 0, 0, 0)
        }
        ), (function(t, n) {
            t.setFullYear(t.getFullYear() + n)
        }
        ), (function(t, n) {
            return n.getFullYear() - t.getFullYear()
        }
        ), (function(t) {
            return t.getFullYear()
        }
        ));
        ja.every = function(t) {
            return isFinite(t = Math.floor(t)) && t > 0 ? Ia((function(n) {
                n.setFullYear(Math.floor(n.getFullYear() / t) * t),
                n.setMonth(0, 1),
                n.setHours(0, 0, 0, 0)
            }
            ), (function(n, e) {
                n.setFullYear(n.getFullYear() + e * t)
            }
            )) : null
        }
        ;
        var Fa = ja
          , Ya = (ja.range,
        Ia((function(t) {
            t.setDate(1),
            t.setHours(0, 0, 0, 0)
        }
        ), (function(t, n) {
            t.setMonth(t.getMonth() + n)
        }
        ), (function(t, n) {
            return n.getMonth() - t.getMonth() + 12 * (n.getFullYear() - t.getFullYear())
        }
        ), (function(t) {
            return t.getMonth()
        }
        )))
          , Ha = (Ya.range,
        1e3)
          , qa = 6e4
          , Ba = 36e5
          , Xa = 864e5
          , Va = 6048e5;
        function $a(t) {
            return Ia((function(n) {
                n.setDate(n.getDate() - (n.getDay() + 7 - t) % 7),
                n.setHours(0, 0, 0, 0)
            }
            ), (function(t, n) {
                t.setDate(t.getDate() + 7 * n)
            }
            ), (function(t, n) {
                return (n - t - (n.getTimezoneOffset() - t.getTimezoneOffset()) * qa) / Va
            }
            ))
        }
        var Za = $a(0)
          , Ga = $a(1)
          , Wa = $a(2)
          , Ja = $a(3)
          , Qa = $a(4)
          , Ka = $a(5)
          , ts = $a(6)
          , ns = (Za.range,
        Ga.range,
        Wa.range,
        Ja.range,
        Qa.range,
        Ka.range,
        ts.range,
        Ia((function(t) {
            t.setHours(0, 0, 0, 0)
        }
        ), (function(t, n) {
            t.setDate(t.getDate() + n)
        }
        ), (function(t, n) {
            return (n - t - (n.getTimezoneOffset() - t.getTimezoneOffset()) * qa) / Xa
        }
        ), (function(t) {
            return t.getDate() - 1
        }
        )))
          , es = ns
          , rs = (ns.range,
        Ia((function(t) {
            t.setTime(t - t.getMilliseconds() - t.getSeconds() * Ha - t.getMinutes() * qa)
        }
        ), (function(t, n) {
            t.setTime(+t + n * Ba)
        }
        ), (function(t, n) {
            return (n - t) / Ba
        }
        ), (function(t) {
            return t.getHours()
        }
        )))
          , is = (rs.range,
        Ia((function(t) {
            t.setTime(t - t.getMilliseconds() - t.getSeconds() * Ha)
        }
        ), (function(t, n) {
            t.setTime(+t + n * qa)
        }
        ), (function(t, n) {
            return (n - t) / qa
        }
        ), (function(t) {
            return t.getMinutes()
        }
        )))
          , os = (is.range,
        Ia((function(t) {
            t.setTime(t - t.getMilliseconds())
        }
        ), (function(t, n) {
            t.setTime(+t + n * Ha)
        }
        ), (function(t, n) {
            return (n - t) / Ha
        }
        ), (function(t) {
            return t.getUTCSeconds()
        }
        )))
          , us = (os.range,
        Ia((function() {}
        ), (function(t, n) {
            t.setTime(+t + n)
        }
        ), (function(t, n) {
            return n - t
        }
        )));
        us.every = function(t) {
            return t = Math.floor(t),
            isFinite(t) && t > 0 ? t > 1 ? Ia((function(n) {
                n.setTime(Math.floor(n / t) * t)
            }
            ), (function(n, e) {
                n.setTime(+n + e * t)
            }
            ), (function(n, e) {
                return (e - n) / t
            }
            )) : us : null
        }
        ;
        us.range;
        function as(t) {
            return Ia((function(n) {
                n.setUTCDate(n.getUTCDate() - (n.getUTCDay() + 7 - t) % 7),
                n.setUTCHours(0, 0, 0, 0)
            }
            ), (function(t, n) {
                t.setUTCDate(t.getUTCDate() + 7 * n)
            }
            ), (function(t, n) {
                return (n - t) / Va
            }
            ))
        }
        var ss = as(0)
          , cs = as(1)
          , ls = as(2)
          , hs = as(3)
          , fs = as(4)
          , ps = as(5)
          , _s = as(6)
          , vs = (ss.range,
        cs.range,
        ls.range,
        hs.range,
        fs.range,
        ps.range,
        _s.range,
        Ia((function(t) {
            t.setUTCHours(0, 0, 0, 0)
        }
        ), (function(t, n) {
            t.setUTCDate(t.getUTCDate() + n)
        }
        ), (function(t, n) {
            return (n - t) / Xa
        }
        ), (function(t) {
            return t.getUTCDate() - 1
        }
        )))
          , ds = vs
          , gs = (vs.range,
        Ia((function(t) {
            t.setUTCMonth(0, 1),
            t.setUTCHours(0, 0, 0, 0)
        }
        ), (function(t, n) {
            t.setUTCFullYear(t.getUTCFullYear() + n)
        }
        ), (function(t, n) {
            return n.getUTCFullYear() - t.getUTCFullYear()
        }
        ), (function(t) {
            return t.getUTCFullYear()
        }
        )));
        gs.every = function(t) {
            return isFinite(t = Math.floor(t)) && t > 0 ? Ia((function(n) {
                n.setUTCFullYear(Math.floor(n.getUTCFullYear() / t) * t),
                n.setUTCMonth(0, 1),
                n.setUTCHours(0, 0, 0, 0)
            }
            ), (function(n, e) {
                n.setUTCFullYear(n.getUTCFullYear() + e * t)
            }
            )) : null
        }
        ;
        var ys = gs;
        gs.range;
        function ms(t) {
            if (0 <= t.y && t.y < 100) {
                var n = new Date(-1,t.m,t.d,t.H,t.M,t.S,t.L);
                return n.setFullYear(t.y),
                n
            }
            return new Date(t.y,t.m,t.d,t.H,t.M,t.S,t.L)
        }
        function ws(t) {
            if (0 <= t.y && t.y < 100) {
                var n = new Date(Date.UTC(-1, t.m, t.d, t.H, t.M, t.S, t.L));
                return n.setUTCFullYear(t.y),
                n
            }
            return new Date(Date.UTC(t.y, t.m, t.d, t.H, t.M, t.S, t.L))
        }
        function xs(t, n, e) {
            return {
                y: t,
                m: n,
                d: e,
                H: 0,
                M: 0,
                S: 0,
                L: 0
            }
        }
        function bs(t) {
            var n = t.dateTime
              , e = t.date
              , r = t.time
              , i = t.periods
              , o = t.days
              , u = t.shortDays
              , a = t.months
              , s = t.shortMonths
              , c = ks(i)
              , l = Ps(i)
              , h = ks(o)
              , f = Ps(o)
              , p = ks(u)
              , _ = Ps(u)
              , v = ks(a)
              , d = Ps(a)
              , g = ks(s)
              , y = Ps(s)
              , m = {
                a: L,
                A: D,
                b: z,
                B: O,
                c: null,
                d: Qs,
                e: Qs,
                f: rc,
                g: _c,
                G: dc,
                H: Ks,
                I: tc,
                j: nc,
                L: ec,
                m: ic,
                M: oc,
                p: R,
                q: I,
                Q: jc,
                s: Fc,
                S: uc,
                u: ac,
                U: sc,
                V: lc,
                w: hc,
                W: fc,
                x: null,
                X: null,
                y: pc,
                Y: vc,
                Z: gc,
                "%": Ic
            }
              , w = {
                a: j,
                A: F,
                b: Y,
                B: H,
                c: null,
                d: yc,
                e: yc,
                f: Ac,
                g: Dc,
                G: Oc,
                H: mc,
                I: wc,
                j: xc,
                L: bc,
                m: Mc,
                M: Cc,
                p: q,
                q: B,
                Q: jc,
                s: Fc,
                S: Nc,
                u: Tc,
                U: Sc,
                V: kc,
                w: Pc,
                W: Uc,
                x: null,
                X: null,
                y: Lc,
                Y: zc,
                Z: Rc,
                "%": Ic
            }
              , x = {
                a: N,
                A: T,
                b: S,
                B: E,
                c: k,
                d: Hs,
                e: Hs,
                f: Zs,
                g: Is,
                G: Rs,
                H: Bs,
                I: Bs,
                j: qs,
                L: $s,
                m: Ys,
                M: Xs,
                p: C,
                q: Fs,
                Q: Ws,
                s: Js,
                S: Vs,
                u: Ls,
                U: Ds,
                V: zs,
                w: Us,
                W: Os,
                x: P,
                X: U,
                y: Is,
                Y: Rs,
                Z: js,
                "%": Gs
            };
            function b(t, n) {
                return function(e) {
                    var r, i, o, u = [], a = -1, s = 0, c = t.length;
                    e instanceof Date || (e = new Date(+e));
                    while (++a < c)
                        37 === t.charCodeAt(a) && (u.push(t.slice(s, a)),
                        null != (i = Ms[r = t.charAt(++a)]) ? r = t.charAt(++a) : i = "e" === r ? " " : "0",
                        (o = n[r]) && (r = o(e, i)),
                        u.push(r),
                        s = a + 1);
                    return u.push(t.slice(s, a)),
                    u.join("")
                }
            }
            function A(t, n) {
                return function(e) {
                    var r, i, o = xs(1900, void 0, 1), u = M(o, t, e += "", 0);
                    if (u != e.length)
                        return null;
                    if ("Q"in o)
                        return new Date(o.Q);
                    if ("s"in o)
                        return new Date(1e3 * o.s + ("L"in o ? o.L : 0));
                    if (!n || "Z"in o || (o.Z = 0),
                    "p"in o && (o.H = o.H % 12 + 12 * o.p),
                    void 0 === o.m && (o.m = "q"in o ? o.q : 0),
                    "V"in o) {
                        if (o.V < 1 || o.V > 53)
                            return null;
                        "w"in o || (o.w = 1),
                        "Z"in o ? (r = ws(xs(o.y, 0, 1)),
                        i = r.getUTCDay(),
                        r = i > 4 || 0 === i ? cs.ceil(r) : cs(r),
                        r = ds.offset(r, 7 * (o.V - 1)),
                        o.y = r.getUTCFullYear(),
                        o.m = r.getUTCMonth(),
                        o.d = r.getUTCDate() + (o.w + 6) % 7) : (r = ms(xs(o.y, 0, 1)),
                        i = r.getDay(),
                        r = i > 4 || 0 === i ? Ga.ceil(r) : Ga(r),
                        r = es.offset(r, 7 * (o.V - 1)),
                        o.y = r.getFullYear(),
                        o.m = r.getMonth(),
                        o.d = r.getDate() + (o.w + 6) % 7)
                    } else
                        ("W"in o || "U"in o) && ("w"in o || (o.w = "u"in o ? o.u % 7 : "W"in o ? 1 : 0),
                        i = "Z"in o ? ws(xs(o.y, 0, 1)).getUTCDay() : ms(xs(o.y, 0, 1)).getDay(),
                        o.m = 0,
                        o.d = "W"in o ? (o.w + 6) % 7 + 7 * o.W - (i + 5) % 7 : o.w + 7 * o.U - (i + 6) % 7);
                    return "Z"in o ? (o.H += o.Z / 100 | 0,
                    o.M += o.Z % 100,
                    ws(o)) : ms(o)
                }
            }
            function M(t, n, e, r) {
                var i, o, u = 0, a = n.length, s = e.length;
                while (u < a) {
                    if (r >= s)
                        return -1;
                    if (i = n.charCodeAt(u++),
                    37 === i) {
                        if (i = n.charAt(u++),
                        o = x[i in Ms ? n.charAt(u++) : i],
                        !o || (r = o(t, e, r)) < 0)
                            return -1
                    } else if (i != e.charCodeAt(r++))
                        return -1
                }
                return r
            }
            function C(t, n, e) {
                var r = c.exec(n.slice(e));
                return r ? (t.p = l[r[0].toLowerCase()],
                e + r[0].length) : -1
            }
            function N(t, n, e) {
                var r = p.exec(n.slice(e));
                return r ? (t.w = _[r[0].toLowerCase()],
                e + r[0].length) : -1
            }
            function T(t, n, e) {
                var r = h.exec(n.slice(e));
                return r ? (t.w = f[r[0].toLowerCase()],
                e + r[0].length) : -1
            }
            function S(t, n, e) {
                var r = g.exec(n.slice(e));
                return r ? (t.m = y[r[0].toLowerCase()],
                e + r[0].length) : -1
            }
            function E(t, n, e) {
                var r = v.exec(n.slice(e));
                return r ? (t.m = d[r[0].toLowerCase()],
                e + r[0].length) : -1
            }
            function k(t, e, r) {
                return M(t, n, e, r)
            }
            function P(t, n, r) {
                return M(t, e, n, r)
            }
            function U(t, n, e) {
                return M(t, r, n, e)
            }
            function L(t) {
                return u[t.getDay()]
            }
            function D(t) {
                return o[t.getDay()]
            }
            function z(t) {
                return s[t.getMonth()]
            }
            function O(t) {
                return a[t.getMonth()]
            }
            function R(t) {
                return i[+(t.getHours() >= 12)]
            }
            function I(t) {
                return 1 + ~~(t.getMonth() / 3)
            }
            function j(t) {
                return u[t.getUTCDay()]
            }
            function F(t) {
                return o[t.getUTCDay()]
            }
            function Y(t) {
                return s[t.getUTCMonth()]
            }
            function H(t) {
                return a[t.getUTCMonth()]
            }
            function q(t) {
                return i[+(t.getUTCHours() >= 12)]
            }
            function B(t) {
                return 1 + ~~(t.getUTCMonth() / 3)
            }
            return (m.x = b(e, m),
            m.X = b(r, m),
            m.c = b(n, m),
            w.x = b(e, w),
            w.X = b(r, w),
            w.c = b(n, w),
            {
                format: function(t) {
                    var n = b(t += "", m);
                    return n.toString = function() {
                        return t
                    }
                    ,
                    n
                },
                parse: function(t) {
                    var n = A(t += "", !1);
                    return n.toString = function() {
                        return t
                    }
                    ,
                    n
                },
                utcFormat: function(t) {
                    var n = b(t += "", w);
                    return n.toString = function() {
                        return t
                    }
                    ,
                    n
                },
                utcParse: function(t) {
                    var n = A(t += "", !0);
                    return n.toString = function() {
                        return t
                    }
                    ,
                    n
                }
            })
        }
        var As, Ms = {
            "-": "",
            _: " ",
            0: "0"
        }, Cs = /^\s*\d+/, Ns = /^%/, Ts = /[\\^$*+?|[\]().{}]/g;
        function Ss(t, n, e) {
            var r = t < 0 ? "-" : ""
              , i = (r ? -t : t) + ""
              , o = i.length;
            return r + (o < e ? new Array(e - o + 1).join(n) + i : i)
        }
        function Es(t) {
            return t.replace(Ts, "\\$&")
        }
        function ks(t) {
            return new RegExp("^(?:" + t.map(Es).join("|") + ")","i")
        }
        function Ps(t) {
            var n = {}
              , e = -1
              , r = t.length;
            while (++e < r)
                n[t[e].toLowerCase()] = e;
            return n
        }
        function Us(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 1));
            return r ? (t.w = +r[0],
            e + r[0].length) : -1
        }
        function Ls(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 1));
            return r ? (t.u = +r[0],
            e + r[0].length) : -1
        }
        function Ds(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.U = +r[0],
            e + r[0].length) : -1
        }
        function zs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.V = +r[0],
            e + r[0].length) : -1
        }
        function Os(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.W = +r[0],
            e + r[0].length) : -1
        }
        function Rs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 4));
            return r ? (t.y = +r[0],
            e + r[0].length) : -1
        }
        function Is(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.y = +r[0] + (+r[0] > 68 ? 1900 : 2e3),
            e + r[0].length) : -1
        }
        function js(t, n, e) {
            var r = /^(Z)|([+-]\d\d)(?::?(\d\d))?/.exec(n.slice(e, e + 6));
            return r ? (t.Z = r[1] ? 0 : -(r[2] + (r[3] || "00")),
            e + r[0].length) : -1
        }
        function Fs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 1));
            return r ? (t.q = 3 * r[0] - 3,
            e + r[0].length) : -1
        }
        function Ys(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.m = r[0] - 1,
            e + r[0].length) : -1
        }
        function Hs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.d = +r[0],
            e + r[0].length) : -1
        }
        function qs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 3));
            return r ? (t.m = 0,
            t.d = +r[0],
            e + r[0].length) : -1
        }
        function Bs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.H = +r[0],
            e + r[0].length) : -1
        }
        function Xs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.M = +r[0],
            e + r[0].length) : -1
        }
        function Vs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 2));
            return r ? (t.S = +r[0],
            e + r[0].length) : -1
        }
        function $s(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 3));
            return r ? (t.L = +r[0],
            e + r[0].length) : -1
        }
        function Zs(t, n, e) {
            var r = Cs.exec(n.slice(e, e + 6));
            return r ? (t.L = Math.floor(r[0] / 1e3),
            e + r[0].length) : -1
        }
        function Gs(t, n, e) {
            var r = Ns.exec(n.slice(e, e + 1));
            return r ? e + r[0].length : -1
        }
        function Ws(t, n, e) {
            var r = Cs.exec(n.slice(e));
            return r ? (t.Q = +r[0],
            e + r[0].length) : -1
        }
        function Js(t, n, e) {
            var r = Cs.exec(n.slice(e));
            return r ? (t.s = +r[0],
            e + r[0].length) : -1
        }
        function Qs(t, n) {
            return Ss(t.getDate(), n, 2)
        }
        function Ks(t, n) {
            return Ss(t.getHours(), n, 2)
        }
        function tc(t, n) {
            return Ss(t.getHours() % 12 || 12, n, 2)
        }
        function nc(t, n) {
            return Ss(1 + es.count(Fa(t), t), n, 3)
        }
        function ec(t, n) {
            return Ss(t.getMilliseconds(), n, 3)
        }
        function rc(t, n) {
            return ec(t, n) + "000"
        }
        function ic(t, n) {
            return Ss(t.getMonth() + 1, n, 2)
        }
        function oc(t, n) {
            return Ss(t.getMinutes(), n, 2)
        }
        function uc(t, n) {
            return Ss(t.getSeconds(), n, 2)
        }
        function ac(t) {
            var n = t.getDay();
            return 0 === n ? 7 : n
        }
        function sc(t, n) {
            return Ss(Za.count(Fa(t) - 1, t), n, 2)
        }
        function cc(t) {
            var n = t.getDay();
            return n >= 4 || 0 === n ? Qa(t) : Qa.ceil(t)
        }
        function lc(t, n) {
            return t = cc(t),
            Ss(Qa.count(Fa(t), t) + (4 === Fa(t).getDay()), n, 2)
        }
        function hc(t) {
            return t.getDay()
        }
        function fc(t, n) {
            return Ss(Ga.count(Fa(t) - 1, t), n, 2)
        }
        function pc(t, n) {
            return Ss(t.getFullYear() % 100, n, 2)
        }
        function _c(t, n) {
            return t = cc(t),
            Ss(t.getFullYear() % 100, n, 2)
        }
        function vc(t, n) {
            return Ss(t.getFullYear() % 1e4, n, 4)
        }
        function dc(t, n) {
            var e = t.getDay();
            return t = e >= 4 || 0 === e ? Qa(t) : Qa.ceil(t),
            Ss(t.getFullYear() % 1e4, n, 4)
        }
        function gc(t) {
            var n = t.getTimezoneOffset();
            return (n > 0 ? "-" : (n *= -1,
            "+")) + Ss(n / 60 | 0, "0", 2) + Ss(n % 60, "0", 2)
        }
        function yc(t, n) {
            return Ss(t.getUTCDate(), n, 2)
        }
        function mc(t, n) {
            return Ss(t.getUTCHours(), n, 2)
        }
        function wc(t, n) {
            return Ss(t.getUTCHours() % 12 || 12, n, 2)
        }
        function xc(t, n) {
            return Ss(1 + ds.count(ys(t), t), n, 3)
        }
        function bc(t, n) {
            return Ss(t.getUTCMilliseconds(), n, 3)
        }
        function Ac(t, n) {
            return bc(t, n) + "000"
        }
        function Mc(t, n) {
            return Ss(t.getUTCMonth() + 1, n, 2)
        }
        function Cc(t, n) {
            return Ss(t.getUTCMinutes(), n, 2)
        }
        function Nc(t, n) {
            return Ss(t.getUTCSeconds(), n, 2)
        }
        function Tc(t) {
            var n = t.getUTCDay();
            return 0 === n ? 7 : n
        }
        function Sc(t, n) {
            return Ss(ss.count(ys(t) - 1, t), n, 2)
        }
        function Ec(t) {
            var n = t.getUTCDay();
            return n >= 4 || 0 === n ? fs(t) : fs.ceil(t)
        }
        function kc(t, n) {
            return t = Ec(t),
            Ss(fs.count(ys(t), t) + (4 === ys(t).getUTCDay()), n, 2)
        }
        function Pc(t) {
            return t.getUTCDay()
        }
        function Uc(t, n) {
            return Ss(cs.count(ys(t) - 1, t), n, 2)
        }
        function Lc(t, n) {
            return Ss(t.getUTCFullYear() % 100, n, 2)
        }
        function Dc(t, n) {
            return t = Ec(t),
            Ss(t.getUTCFullYear() % 100, n, 2)
        }
        function zc(t, n) {
            return Ss(t.getUTCFullYear() % 1e4, n, 4)
        }
        function Oc(t, n) {
            var e = t.getUTCDay();
            return t = e >= 4 || 0 === e ? fs(t) : fs.ceil(t),
            Ss(t.getUTCFullYear() % 1e4, n, 4)
        }
        function Rc() {
            return "+0000"
        }
        function Ic() {
            return "%"
        }
        function jc(t) {
            return +t
        }
        function Fc(t) {
            return Math.floor(+t / 1e3)
        }
        function Yc(t) {
            return As = bs(t),
            As.format,
            As.parse,
            As.utcFormat,
            As.utcParse,
            As
        }
        Yc({
            dateTime: "%x, %X",
            date: "%-m/%-d/%Y",
            time: "%-I:%M:%S %p",
            periods: ["AM", "PM"],
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            shortMonths: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        });
        var Hc = Ia((function(t) {
            t.setUTCDate(1),
            t.setUTCHours(0, 0, 0, 0)
        }
        ), (function(t, n) {
            t.setUTCMonth(t.getUTCMonth() + n)
        }
        ), (function(t, n) {
            return n.getUTCMonth() - t.getUTCMonth() + 12 * (n.getUTCFullYear() - t.getUTCFullYear())
        }
        ), (function(t) {
            return t.getUTCMonth()
        }
        ))
          , qc = (Hc.range,
        Ia((function(t) {
            t.setUTCMinutes(0, 0, 0)
        }
        ), (function(t, n) {
            t.setTime(+t + n * Ba)
        }
        ), (function(t, n) {
            return (n - t) / Ba
        }
        ), (function(t) {
            return t.getUTCHours()
        }
        )))
          , Bc = (qc.range,
        Ia((function(t) {
            t.setUTCSeconds(0, 0)
        }
        ), (function(t, n) {
            t.setTime(+t + n * qa)
        }
        ), (function(t, n) {
            return (n - t) / qa
        }
        ), (function(t) {
            return t.getUTCMinutes()
        }
        )));
        Bc.range;
        var Xc = function(t) {
            return "string" === typeof t ? new yu([document.querySelectorAll(t)],[document.documentElement]) : new yu([null == t ? [] : t],gu)
        }
          , Vc = function(t, n) {
            null == n && (n = Oi().touches);
            for (var e = 0, r = n ? n.length : 0, i = new Array(r); e < r; ++e)
                i[e] = Ri(t, n[e]);
            return i
        }
          , $c = m("start", "end", "cancel", "interrupt")
          , Zc = []
          , Gc = 0
          , Wc = 1
          , Jc = 2
          , Qc = 3
          , Kc = 4
          , tl = 5
          , nl = 6
          , el = function(t, n, e, r, i, o) {
            var u = t.__transition;
            if (u) {
                if (e in u)
                    return
            } else
                t.__transition = {};
            ul(t, e, {
                name: n,
                index: r,
                group: i,
                on: $c,
                tween: Zc,
                time: o.time,
                delay: o.delay,
                duration: o.duration,
                ease: o.ease,
                timer: null,
                state: Gc
            })
        };
        function rl(t, n) {
            var e = ol(t, n);
            if (e.state > Gc)
                throw new Error("too late; already scheduled");
            return e
        }
        function il(t, n) {
            var e = ol(t, n);
            if (e.state > Qc)
                throw new Error("too late; already running");
            return e
        }
        function ol(t, n) {
            var e = t.__transition;
            if (!e || !(e = e[n]))
                throw new Error("transition not found");
            return e
        }
        function ul(t, n, e) {
            var r, i = t.__transition;
            function o(t) {
                e.state = Wc,
                e.timer.restart(u, e.delay, e.time),
                e.delay <= t && u(t - e.delay)
            }
            function u(o) {
                var c, l, h, f;
                if (e.state !== Wc)
                    return s();
                for (c in i)
                    if (f = i[c],
                    f.name === e.name) {
                        if (f.state === Qc)
                            return Ne(u);
                        f.state === Kc ? (f.state = nl,
                        f.timer.stop(),
                        f.on.call("interrupt", t, t.__data__, f.index, f.group),
                        delete i[c]) : +c < n && (f.state = nl,
                        f.timer.stop(),
                        f.on.call("cancel", t, t.__data__, f.index, f.group),
                        delete i[c])
                    }
                if (Ne((function() {
                    e.state === Qc && (e.state = Kc,
                    e.timer.restart(a, e.delay, e.time),
                    a(o))
                }
                )),
                e.state = Jc,
                e.on.call("start", t, t.__data__, e.index, e.group),
                e.state === Jc) {
                    for (e.state = Qc,
                    r = new Array(h = e.tween.length),
                    c = 0,
                    l = -1; c < h; ++c)
                        (f = e.tween[c].value.call(t, t.__data__, e.index, e.group)) && (r[++l] = f);
                    r.length = l + 1
                }
            }
            function a(n) {
                var i = n < e.duration ? e.ease.call(null, n / e.duration) : (e.timer.restart(s),
                e.state = tl,
                1)
                  , o = -1
                  , u = r.length;
                while (++o < u)
                    r[o].call(t, i);
                e.state === tl && (e.on.call("end", t, t.__data__, e.index, e.group),
                s())
            }
            function s() {
                for (var r in e.state = nl,
                e.timer.stop(),
                delete i[n],
                i)
                    return;
                delete t.__transition
            }
            i[n] = e,
            e.timer = we(o, 0, e.time)
        }
        var al = function(t, n) {
            var e, r, i, o = t.__transition, u = !0;
            if (o) {
                for (i in n = null == n ? null : n + "",
                o)
                    (e = o[i]).name === n ? (r = e.state > Jc && e.state < tl,
                    e.state = nl,
                    e.timer.stop(),
                    e.on.call(r ? "interrupt" : "cancel", t, t.__data__, e.index, e.group),
                    delete o[i]) : u = !1;
                u && delete t.__transition
            }
        }
          , sl = function(t) {
            return this.each((function() {
                al(this, t)
            }
            ))
        };
        function cl(t, n) {
            var e, r;
            return function() {
                var i = il(this, t)
                  , o = i.tween;
                if (o !== e) {
                    r = e = o;
                    for (var u = 0, a = r.length; u < a; ++u)
                        if (r[u].name === n) {
                            r = r.slice(),
                            r.splice(u, 1);
                            break
                        }
                }
                i.tween = r
            }
        }
        function ll(t, n, e) {
            var r, i;
            if ("function" !== typeof e)
                throw new Error;
            return function() {
                var o = il(this, t)
                  , u = o.tween;
                if (u !== r) {
                    i = (r = u).slice();
                    for (var a = {
                        name: n,
                        value: e
                    }, s = 0, c = i.length; s < c; ++s)
                        if (i[s].name === n) {
                            i[s] = a;
                            break
                        }
                    s === c && i.push(a)
                }
                o.tween = i
            }
        }
        var hl = function(t, n) {
            var e = this._id;
            if (t += "",
            arguments.length < 2) {
                for (var r, i = ol(this.node(), e).tween, o = 0, u = i.length; o < u; ++o)
                    if ((r = i[o]).name === t)
                        return r.value;
                return null
            }
            return this.each((null == n ? cl : ll)(e, t, n))
        };
        function fl(t, n, e) {
            var r = t._id;
            return t.each((function() {
                var t = il(this, r);
                (t.value || (t.value = {}))[n] = e.apply(this, arguments)
            }
            )),
            function(t) {
                return ol(t, r).value[n]
            }
        }
        var pl = function(t, n) {
            var e;
            return ("number" === typeof n ? te : n instanceof kn ? Qn : (e = kn(n)) ? (n = e,
            Qn) : ae)(t, n)
        };
        function _l(t) {
            return function() {
                this.removeAttribute(t)
            }
        }
        function vl(t) {
            return function() {
                this.removeAttributeNS(t.space, t.local)
            }
        }
        function dl(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = this.getAttribute(t);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function gl(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = this.getAttributeNS(t.space, t.local);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function yl(t, n, e) {
            var r, i, o;
            return function() {
                var u, a, s = e(this);
                if (null != s)
                    return u = this.getAttribute(t),
                    a = s + "",
                    u === a ? null : u === r && a === i ? o : (i = a,
                    o = n(r = u, s));
                this.removeAttribute(t)
            }
        }
        function ml(t, n, e) {
            var r, i, o;
            return function() {
                var u, a, s = e(this);
                if (null != s)
                    return u = this.getAttributeNS(t.space, t.local),
                    a = s + "",
                    u === a ? null : u === r && a === i ? o : (i = a,
                    o = n(r = u, s));
                this.removeAttributeNS(t.space, t.local)
            }
        }
        var wl = function(t, n) {
            var e = vo(t)
              , r = "transform" === e ? tr : pl;
            return this.attrTween(t, "function" === typeof n ? (e.local ? ml : yl)(e, r, fl(this, "attr." + t, n)) : null == n ? (e.local ? vl : _l)(e) : (e.local ? gl : dl)(e, r, n))
        };
        function xl(t, n) {
            return function(e) {
                this.setAttribute(t, n.call(this, e))
            }
        }
        function bl(t, n) {
            return function(e) {
                this.setAttributeNS(t.space, t.local, n.call(this, e))
            }
        }
        function Al(t, n) {
            var e, r;
            function i() {
                var i = n.apply(this, arguments);
                return i !== r && (e = (r = i) && bl(t, i)),
                e
            }
            return i._value = n,
            i
        }
        function Ml(t, n) {
            var e, r;
            function i() {
                var i = n.apply(this, arguments);
                return i !== r && (e = (r = i) && xl(t, i)),
                e
            }
            return i._value = n,
            i
        }
        var Cl = function(t, n) {
            var e = "attr." + t;
            if (arguments.length < 2)
                return (e = this.tween(e)) && e._value;
            if (null == n)
                return this.tween(e, null);
            if ("function" !== typeof n)
                throw new Error;
            var r = vo(t);
            return this.tween(e, (r.local ? Al : Ml)(r, n))
        };
        function Nl(t, n) {
            return function() {
                rl(this, t).delay = +n.apply(this, arguments)
            }
        }
        function Tl(t, n) {
            return n = +n,
            function() {
                rl(this, t).delay = n
            }
        }
        var Sl = function(t) {
            var n = this._id;
            return arguments.length ? this.each(("function" === typeof t ? Nl : Tl)(n, t)) : ol(this.node(), n).delay
        };
        function El(t, n) {
            return function() {
                il(this, t).duration = +n.apply(this, arguments)
            }
        }
        function kl(t, n) {
            return n = +n,
            function() {
                il(this, t).duration = n
            }
        }
        var Pl = function(t) {
            var n = this._id;
            return arguments.length ? this.each(("function" === typeof t ? El : kl)(n, t)) : ol(this.node(), n).duration
        };
        function Ul(t, n) {
            if ("function" !== typeof n)
                throw new Error;
            return function() {
                il(this, t).ease = n
            }
        }
        var Ll = function(t) {
            var n = this._id;
            return arguments.length ? this.each(Ul(n, t)) : ol(this.node(), n).ease
        }
          , Dl = function(t) {
            "function" !== typeof t && (t = Xi(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u = n[i], a = u.length, s = r[i] = [], c = 0; c < a; ++c)
                    (o = u[c]) && t.call(o, o.__data__, c, u) && s.push(o);
            return new ch(r,this._parents,this._name,this._id)
        }
          , zl = function(t) {
            if (t._id !== this._id)
                throw new Error;
            for (var n = this._groups, e = t._groups, r = n.length, i = e.length, o = Math.min(r, i), u = new Array(r), a = 0; a < o; ++a)
                for (var s, c = n[a], l = e[a], h = c.length, f = u[a] = new Array(h), p = 0; p < h; ++p)
                    (s = c[p] || l[p]) && (f[p] = s);
            for (; a < r; ++a)
                u[a] = n[a];
            return new ch(u,this._parents,this._name,this._id)
        };
        function Ol(t) {
            return (t + "").trim().split(/^|\s+/).every((function(t) {
                var n = t.indexOf(".");
                return n >= 0 && (t = t.slice(0, n)),
                !t || "start" === t
            }
            ))
        }
        function Rl(t, n, e) {
            var r, i, o = Ol(n) ? rl : il;
            return function() {
                var u = o(this, t)
                  , a = u.on;
                a !== r && (i = (r = a).copy()).on(n, e),
                u.on = i
            }
        }
        var Il = function(t, n) {
            var e = this._id;
            return arguments.length < 2 ? ol(this.node(), e).on.on(t) : this.each(Rl(e, t, n))
        };
        function jl(t) {
            return function() {
                var n = this.parentNode;
                for (var e in this.__transition)
                    if (+e !== t)
                        return;
                n && n.removeChild(this)
            }
        }
        var Fl = function() {
            return this.on("end.remove", jl(this._id))
        }
          , Yl = function(t) {
            var n = this._name
              , e = this._id;
            "function" !== typeof t && (t = Fi(t));
            for (var r = this._groups, i = r.length, o = new Array(i), u = 0; u < i; ++u)
                for (var a, s, c = r[u], l = c.length, h = o[u] = new Array(l), f = 0; f < l; ++f)
                    (a = c[f]) && (s = t.call(a, a.__data__, f, c)) && ("__data__"in a && (s.__data__ = a.__data__),
                    h[f] = s,
                    el(h[f], n, e, f, h, ol(a, e)));
            return new ch(o,this._parents,n,e)
        }
          , Hl = function(t) {
            var n = this._name
              , e = this._id;
            "function" !== typeof t && (t = qi(t));
            for (var r = this._groups, i = r.length, o = [], u = [], a = 0; a < i; ++a)
                for (var s, c = r[a], l = c.length, h = 0; h < l; ++h)
                    if (s = c[h]) {
                        for (var f, p = t.call(s, s.__data__, h, c), _ = ol(s, e), v = 0, d = p.length; v < d; ++v)
                            (f = p[v]) && el(f, n, e, v, p, _);
                        o.push(p),
                        u.push(s)
                    }
            return new ch(o,u,n,e)
        }
          , ql = wu.prototype.constructor
          , Bl = function() {
            return new ql(this._groups,this._parents)
        };
        function Xl(t, n) {
            var e, r, i;
            return function() {
                var o = Eo(this, t)
                  , u = (this.style.removeProperty(t),
                Eo(this, t));
                return o === u ? null : o === e && u === r ? i : i = n(e = o, r = u)
            }
        }
        function Vl(t) {
            return function() {
                this.style.removeProperty(t)
            }
        }
        function $l(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = Eo(this, t);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function Zl(t, n, e) {
            var r, i, o;
            return function() {
                var u = Eo(this, t)
                  , a = e(this)
                  , s = a + "";
                return null == a && (this.style.removeProperty(t),
                s = a = Eo(this, t)),
                u === s ? null : u === r && s === i ? o : (i = s,
                o = n(r = u, a))
            }
        }
        function Gl(t, n) {
            var e, r, i, o, u = "style." + n, a = "end." + u;
            return function() {
                var s = il(this, t)
                  , c = s.on
                  , l = null == s.value[u] ? o || (o = Vl(n)) : void 0;
                c === e && i === l || (r = (e = c).copy()).on(a, i = l),
                s.on = r
            }
        }
        var Wl = function(t, n, e) {
            var r = "transform" === (t += "") ? Ke : pl;
            return null == n ? this.styleTween(t, Xl(t, r)).on("end.style." + t, Vl(t)) : "function" === typeof n ? this.styleTween(t, Zl(t, r, fl(this, "style." + t, n))).each(Gl(this._id, t)) : this.styleTween(t, $l(t, r, n), e).on("end.style." + t, null)
        };
        function Jl(t, n, e) {
            return function(r) {
                this.style.setProperty(t, n.call(this, r), e)
            }
        }
        function Ql(t, n, e) {
            var r, i;
            function o() {
                var o = n.apply(this, arguments);
                return o !== i && (r = (i = o) && Jl(t, o, e)),
                r
            }
            return o._value = n,
            o
        }
        var Kl = function(t, n, e) {
            var r = "style." + (t += "");
            if (arguments.length < 2)
                return (r = this.tween(r)) && r._value;
            if (null == n)
                return this.tween(r, null);
            if ("function" !== typeof n)
                throw new Error;
            return this.tween(r, Ql(t, n, null == e ? "" : e))
        };
        function th(t) {
            return function() {
                this.textContent = t
            }
        }
        function nh(t) {
            return function() {
                var n = t(this);
                this.textContent = null == n ? "" : n
            }
        }
        var eh = function(t) {
            return this.tween("text", "function" === typeof t ? nh(fl(this, "text", t)) : th(null == t ? "" : t + ""))
        };
        function rh(t) {
            return function(n) {
                this.textContent = t.call(this, n)
            }
        }
        function ih(t) {
            var n, e;
            function r() {
                var r = t.apply(this, arguments);
                return r !== e && (n = (e = r) && rh(r)),
                n
            }
            return r._value = t,
            r
        }
        var oh = function(t) {
            var n = "text";
            if (arguments.length < 1)
                return (n = this.tween(n)) && n._value;
            if (null == t)
                return this.tween(n, null);
            if ("function" !== typeof t)
                throw new Error;
            return this.tween(n, ih(t))
        }
          , uh = function() {
            for (var t = this._name, n = this._id, e = hh(), r = this._groups, i = r.length, o = 0; o < i; ++o)
                for (var u, a = r[o], s = a.length, c = 0; c < s; ++c)
                    if (u = a[c]) {
                        var l = ol(u, n);
                        el(u, t, e, c, a, {
                            time: l.time + l.delay + l.duration,
                            delay: 0,
                            duration: l.duration,
                            ease: l.ease
                        })
                    }
            return new ch(r,this._parents,t,e)
        }
          , ah = function() {
            var t, n, e = this, r = e._id, i = e.size();
            return new Promise((function(o, u) {
                var a = {
                    value: u
                }
                  , s = {
                    value: function() {
                        0 === --i && o()
                    }
                };
                e.each((function() {
                    var e = il(this, r)
                      , i = e.on;
                    i !== t && (n = (t = i).copy(),
                    n._.cancel.push(a),
                    n._.interrupt.push(a),
                    n._.end.push(s)),
                    e.on = n
                }
                ))
            }
            ))
        }
          , sh = 0;
        function ch(t, n, e, r) {
            this._groups = t,
            this._parents = n,
            this._name = e,
            this._id = r
        }
        function lh(t) {
            return wu().transition(t)
        }
        function hh() {
            return ++sh
        }
        var fh = wu.prototype;
        ch.prototype = lh.prototype = {
            constructor: ch,
            select: Yl,
            selectAll: Hl,
            filter: Dl,
            merge: zl,
            selection: Bl,
            transition: uh,
            call: fh.call,
            nodes: fh.nodes,
            node: fh.node,
            size: fh.size,
            empty: fh.empty,
            each: fh.each,
            on: Il,
            attr: wl,
            attrTween: Cl,
            style: Wl,
            styleTween: Kl,
            text: eh,
            textTween: oh,
            remove: Fl,
            tween: hl,
            delay: Sl,
            duration: Pl,
            ease: Ll,
            end: ah
        };
        var ph = {
            time: null,
            delay: 0,
            duration: 250,
            ease: oi
        };
        function _h(t, n) {
            var e;
            while (!(e = t.__transition) || !(e = e[n]))
                if (!(t = t.parentNode))
                    return ph.time = ge(),
                    ph;
            return e
        }
        var vh = function(t) {
            var n, e;
            t instanceof ch ? (n = t._id,
            t = t._name) : (n = hh(),
            (e = ph).time = ge(),
            t = null == t ? null : t + "");
            for (var r = this._groups, i = r.length, o = 0; o < i; ++o)
                for (var u, a = r[o], s = a.length, c = 0; c < s; ++c)
                    (u = a[c]) && el(u, t, n, c, a, e || _h(u, n));
            return new ch(r,this._parents,t,n)
        };
        wu.prototype.interrupt = sl,
        wu.prototype.transition = vh;
        function dh() {
            this._ = null
        }
        function gh(t) {
            t.U = t.C = t.L = t.R = t.P = t.N = null
        }
        function yh(t, n) {
            var e = n
              , r = n.R
              , i = e.U;
            i ? i.L === e ? i.L = r : i.R = r : t._ = r,
            r.U = i,
            e.U = r,
            e.R = r.L,
            e.R && (e.R.U = e),
            r.L = e
        }
        function mh(t, n) {
            var e = n
              , r = n.L
              , i = e.U;
            i ? i.L === e ? i.L = r : i.R = r : t._ = r,
            r.U = i,
            e.U = r,
            e.L = r.R,
            e.L && (e.L.U = e),
            r.R = e
        }
        function wh(t) {
            while (t.L)
                t = t.L;
            return t
        }
        dh.prototype = {
            constructor: dh,
            insert: function(t, n) {
                var e, r, i;
                if (t) {
                    if (n.P = t,
                    n.N = t.N,
                    t.N && (t.N.P = n),
                    t.N = n,
                    t.R) {
                        t = t.R;
                        while (t.L)
                            t = t.L;
                        t.L = n
                    } else
                        t.R = n;
                    e = t
                } else
                    this._ ? (t = wh(this._),
                    n.P = null,
                    n.N = t,
                    t.P = t.L = n,
                    e = t) : (n.P = n.N = null,
                    this._ = n,
                    e = null);
                n.L = n.R = null,
                n.U = e,
                n.C = !0,
                t = n;
                while (e && e.C)
                    r = e.U,
                    e === r.L ? (i = r.R,
                    i && i.C ? (e.C = i.C = !1,
                    r.C = !0,
                    t = r) : (t === e.R && (yh(this, e),
                    t = e,
                    e = t.U),
                    e.C = !1,
                    r.C = !0,
                    mh(this, r))) : (i = r.L,
                    i && i.C ? (e.C = i.C = !1,
                    r.C = !0,
                    t = r) : (t === e.L && (mh(this, e),
                    t = e,
                    e = t.U),
                    e.C = !1,
                    r.C = !0,
                    yh(this, r))),
                    e = t.U;
                this._.C = !1
            },
            remove: function(t) {
                t.N && (t.N.P = t.P),
                t.P && (t.P.N = t.N),
                t.N = t.P = null;
                var n, e, r, i = t.U, o = t.L, u = t.R;
                if (e = o ? u ? wh(u) : o : u,
                i ? i.L === t ? i.L = e : i.R = e : this._ = e,
                o && u ? (r = e.C,
                e.C = t.C,
                e.L = o,
                o.U = e,
                e !== u ? (i = e.U,
                e.U = t.U,
                t = e.R,
                i.L = t,
                e.R = u,
                u.U = e) : (e.U = i,
                i = e,
                t = e.R)) : (r = t.C,
                t = e),
                t && (t.U = i),
                !r)
                    if (t && t.C)
                        t.C = !1;
                    else {
                        do {
                            if (t === this._)
                                break;
                            if (t === i.L) {
                                if (n = i.R,
                                n.C && (n.C = !1,
                                i.C = !0,
                                yh(this, i),
                                n = i.R),
                                n.L && n.L.C || n.R && n.R.C) {
                                    n.R && n.R.C || (n.L.C = !1,
                                    n.C = !0,
                                    mh(this, n),
                                    n = i.R),
                                    n.C = i.C,
                                    i.C = n.R.C = !1,
                                    yh(this, i),
                                    t = this._;
                                    break
                                }
                            } else if (n = i.L,
                            n.C && (n.C = !1,
                            i.C = !0,
                            mh(this, i),
                            n = i.L),
                            n.L && n.L.C || n.R && n.R.C) {
                                n.L && n.L.C || (n.R.C = !1,
                                n.C = !0,
                                yh(this, n),
                                n = i.L),
                                n.C = i.C,
                                i.C = n.L.C = !1,
                                mh(this, i),
                                t = this._;
                                break
                            }
                            n.C = !0,
                            t = i,
                            i = i.U
                        } while (!t.C);
                        t && (t.C = !1)
                    }
            }
        };
        var xh = dh;
        function bh(t, n, e, r) {
            var i = [null, null]
              , o = Wh.push(i) - 1;
            return i.left = t,
            i.right = n,
            e && Mh(i, t, n, e),
            r && Mh(i, n, t, r),
            Zh[t.index].halfedges.push(o),
            Zh[n.index].halfedges.push(o),
            i
        }
        function Ah(t, n, e) {
            var r = [n, e];
            return r.left = t,
            r
        }
        function Mh(t, n, e, r) {
            t[0] || t[1] ? t.left === e ? t[1] = r : t[0] = r : (t[0] = r,
            t.left = n,
            t.right = e)
        }
        function Ch(t, n, e, r, i) {
            var o, u = t[0], a = t[1], s = u[0], c = u[1], l = a[0], h = a[1], f = 0, p = 1, _ = l - s, v = h - c;
            if (o = n - s,
            _ || !(o > 0)) {
                if (o /= _,
                _ < 0) {
                    if (o < f)
                        return;
                    o < p && (p = o)
                } else if (_ > 0) {
                    if (o > p)
                        return;
                    o > f && (f = o)
                }
                if (o = r - s,
                _ || !(o < 0)) {
                    if (o /= _,
                    _ < 0) {
                        if (o > p)
                            return;
                        o > f && (f = o)
                    } else if (_ > 0) {
                        if (o < f)
                            return;
                        o < p && (p = o)
                    }
                    if (o = e - c,
                    v || !(o > 0)) {
                        if (o /= v,
                        v < 0) {
                            if (o < f)
                                return;
                            o < p && (p = o)
                        } else if (v > 0) {
                            if (o > p)
                                return;
                            o > f && (f = o)
                        }
                        if (o = i - c,
                        v || !(o < 0)) {
                            if (o /= v,
                            v < 0) {
                                if (o > p)
                                    return;
                                o > f && (f = o)
                            } else if (v > 0) {
                                if (o < f)
                                    return;
                                o < p && (p = o)
                            }
                            return !(f > 0 || p < 1) || (f > 0 && (t[0] = [s + f * _, c + f * v]),
                            p < 1 && (t[1] = [s + p * _, c + p * v]),
                            !0)
                        }
                    }
                }
            }
        }
        function Nh(t, n, e, r, i) {
            var o = t[1];
            if (o)
                return !0;
            var u, a, s = t[0], c = t.left, l = t.right, h = c[0], f = c[1], p = l[0], _ = l[1], v = (h + p) / 2, d = (f + _) / 2;
            if (_ === f) {
                if (v < n || v >= r)
                    return;
                if (h > p) {
                    if (s) {
                        if (s[1] >= i)
                            return
                    } else
                        s = [v, e];
                    o = [v, i]
                } else {
                    if (s) {
                        if (s[1] < e)
                            return
                    } else
                        s = [v, i];
                    o = [v, e]
                }
            } else if (u = (h - p) / (_ - f),
            a = d - u * v,
            u < -1 || u > 1)
                if (h > p) {
                    if (s) {
                        if (s[1] >= i)
                            return
                    } else
                        s = [(e - a) / u, e];
                    o = [(i - a) / u, i]
                } else {
                    if (s) {
                        if (s[1] < e)
                            return
                    } else
                        s = [(i - a) / u, i];
                    o = [(e - a) / u, e]
                }
            else if (f < _) {
                if (s) {
                    if (s[0] >= r)
                        return
                } else
                    s = [n, u * n + a];
                o = [r, u * r + a]
            } else {
                if (s) {
                    if (s[0] < n)
                        return
                } else
                    s = [r, u * r + a];
                o = [n, u * n + a]
            }
            return t[0] = s,
            t[1] = o,
            !0
        }
        function Th(t, n, e, r) {
            var i, o = Wh.length;
            while (o--)
                Nh(i = Wh[o], t, n, e, r) && Ch(i, t, n, e, r) && (Math.abs(i[0][0] - i[1][0]) > Jh || Math.abs(i[0][1] - i[1][1]) > Jh) || delete Wh[o]
        }
        function Sh(t) {
            return Zh[t.index] = {
                site: t,
                halfedges: []
            }
        }
        function Eh(t, n) {
            var e = t.site
              , r = n.left
              , i = n.right;
            return e === i && (i = r,
            r = e),
            i ? Math.atan2(i[1] - r[1], i[0] - r[0]) : (e === r ? (r = n[1],
            i = n[0]) : (r = n[0],
            i = n[1]),
            Math.atan2(r[0] - i[0], i[1] - r[1]))
        }
        function kh(t, n) {
            return n[+(n.left !== t.site)]
        }
        function Ph(t, n) {
            return n[+(n.left === t.site)]
        }
        function Uh() {
            for (var t, n, e, r, i = 0, o = Zh.length; i < o; ++i)
                if ((t = Zh[i]) && (r = (n = t.halfedges).length)) {
                    var u = new Array(r)
                      , a = new Array(r);
                    for (e = 0; e < r; ++e)
                        u[e] = e,
                        a[e] = Eh(t, Wh[n[e]]);
                    for (u.sort((function(t, n) {
                        return a[n] - a[t]
                    }
                    )),
                    e = 0; e < r; ++e)
                        a[e] = n[u[e]];
                    for (e = 0; e < r; ++e)
                        n[e] = a[e]
                }
        }
        function Lh(t, n, e, r) {
            var i, o, u, a, s, c, l, h, f, p, _, v, d = Zh.length, g = !0;
            for (i = 0; i < d; ++i)
                if (o = Zh[i]) {
                    u = o.site,
                    s = o.halfedges,
                    a = s.length;
                    while (a--)
                        Wh[s[a]] || s.splice(a, 1);
                    a = 0,
                    c = s.length;
                    while (a < c)
                        p = Ph(o, Wh[s[a]]),
                        _ = p[0],
                        v = p[1],
                        l = kh(o, Wh[s[++a % c]]),
                        h = l[0],
                        f = l[1],
                        (Math.abs(_ - h) > Jh || Math.abs(v - f) > Jh) && (s.splice(a, 0, Wh.push(Ah(u, p, Math.abs(_ - t) < Jh && r - v > Jh ? [t, Math.abs(h - t) < Jh ? f : r] : Math.abs(v - r) < Jh && e - _ > Jh ? [Math.abs(f - r) < Jh ? h : e, r] : Math.abs(_ - e) < Jh && v - n > Jh ? [e, Math.abs(h - e) < Jh ? f : n] : Math.abs(v - n) < Jh && _ - t > Jh ? [Math.abs(f - n) < Jh ? h : t, n] : null)) - 1),
                        ++c);
                    c && (g = !1)
                }
            if (g) {
                var y, m, w, x = 1 / 0;
                for (i = 0,
                g = null; i < d; ++i)
                    (o = Zh[i]) && (u = o.site,
                    y = u[0] - t,
                    m = u[1] - n,
                    w = y * y + m * m,
                    w < x && (x = w,
                    g = o));
                if (g) {
                    var b = [t, n]
                      , A = [t, r]
                      , M = [e, r]
                      , C = [e, n];
                    g.halfedges.push(Wh.push(Ah(u = g.site, b, A)) - 1, Wh.push(Ah(u, A, M)) - 1, Wh.push(Ah(u, M, C)) - 1, Wh.push(Ah(u, C, b)) - 1)
                }
            }
            for (i = 0; i < d; ++i)
                (o = Zh[i]) && (o.halfedges.length || delete Zh[i])
        }
        var Dh, zh = [];
        function Oh() {
            gh(this),
            this.x = this.y = this.arc = this.site = this.cy = null
        }
        function Rh(t) {
            var n = t.P
              , e = t.N;
            if (n && e) {
                var r = n.site
                  , i = t.site
                  , o = e.site;
                if (r !== o) {
                    var u = i[0]
                      , a = i[1]
                      , s = r[0] - u
                      , c = r[1] - a
                      , l = o[0] - u
                      , h = o[1] - a
                      , f = 2 * (s * h - c * l);
                    if (!(f >= -Qh)) {
                        var p = s * s + c * c
                          , _ = l * l + h * h
                          , v = (h * p - c * _) / f
                          , d = (s * _ - l * p) / f
                          , g = zh.pop() || new Oh;
                        g.arc = t,
                        g.site = i,
                        g.x = v + u,
                        g.y = (g.cy = d + a) + Math.sqrt(v * v + d * d),
                        t.circle = g;
                        var y = null
                          , m = Gh._;
                        while (m)
                            if (g.y < m.y || g.y === m.y && g.x <= m.x) {
                                if (!m.L) {
                                    y = m.P;
                                    break
                                }
                                m = m.L
                            } else {
                                if (!m.R) {
                                    y = m;
                                    break
                                }
                                m = m.R
                            }
                        Gh.insert(y, g),
                        y || (Dh = g)
                    }
                }
            }
        }
        function Ih(t) {
            var n = t.circle;
            n && (n.P || (Dh = n.N),
            Gh.remove(n),
            zh.push(n),
            gh(n),
            t.circle = null)
        }
        var jh = [];
        function Fh() {
            gh(this),
            this.edge = this.site = this.circle = null
        }
        function Yh(t) {
            var n = jh.pop() || new Fh;
            return n.site = t,
            n
        }
        function Hh(t) {
            Ih(t),
            $h.remove(t),
            jh.push(t),
            gh(t)
        }
        function qh(t) {
            var n = t.circle
              , e = n.x
              , r = n.cy
              , i = [e, r]
              , o = t.P
              , u = t.N
              , a = [t];
            Hh(t);
            var s = o;
            while (s.circle && Math.abs(e - s.circle.x) < Jh && Math.abs(r - s.circle.cy) < Jh)
                o = s.P,
                a.unshift(s),
                Hh(s),
                s = o;
            a.unshift(s),
            Ih(s);
            var c = u;
            while (c.circle && Math.abs(e - c.circle.x) < Jh && Math.abs(r - c.circle.cy) < Jh)
                u = c.N,
                a.push(c),
                Hh(c),
                c = u;
            a.push(c),
            Ih(c);
            var l, h = a.length;
            for (l = 1; l < h; ++l)
                c = a[l],
                s = a[l - 1],
                Mh(c.edge, s.site, c.site, i);
            s = a[0],
            c = a[h - 1],
            c.edge = bh(s.site, c.site, null, i),
            Rh(s),
            Rh(c)
        }
        function Bh(t) {
            var n, e, r, i, o = t[0], u = t[1], a = $h._;
            while (a)
                if (r = Xh(a, u) - o,
                r > Jh)
                    a = a.L;
                else {
                    if (i = o - Vh(a, u),
                    !(i > Jh)) {
                        r > -Jh ? (n = a.P,
                        e = a) : i > -Jh ? (n = a,
                        e = a.N) : n = e = a;
                        break
                    }
                    if (!a.R) {
                        n = a;
                        break
                    }
                    a = a.R
                }
            Sh(t);
            var s = Yh(t);
            if ($h.insert(n, s),
            n || e) {
                if (n === e)
                    return Ih(n),
                    e = Yh(n.site),
                    $h.insert(s, e),
                    s.edge = e.edge = bh(n.site, s.site),
                    Rh(n),
                    void Rh(e);
                if (e) {
                    Ih(n),
                    Ih(e);
                    var c = n.site
                      , l = c[0]
                      , h = c[1]
                      , f = t[0] - l
                      , p = t[1] - h
                      , _ = e.site
                      , v = _[0] - l
                      , d = _[1] - h
                      , g = 2 * (f * d - p * v)
                      , y = f * f + p * p
                      , m = v * v + d * d
                      , w = [(d * y - p * m) / g + l, (f * m - v * y) / g + h];
                    Mh(e.edge, c, _, w),
                    s.edge = bh(c, t, null, w),
                    e.edge = bh(t, _, null, w),
                    Rh(n),
                    Rh(e)
                } else
                    s.edge = bh(n.site, s.site)
            }
        }
        function Xh(t, n) {
            var e = t.site
              , r = e[0]
              , i = e[1]
              , o = i - n;
            if (!o)
                return r;
            var u = t.P;
            if (!u)
                return -1 / 0;
            e = u.site;
            var a = e[0]
              , s = e[1]
              , c = s - n;
            if (!c)
                return a;
            var l = a - r
              , h = 1 / o - 1 / c
              , f = l / c;
            return h ? (-f + Math.sqrt(f * f - 2 * h * (l * l / (-2 * c) - s + c / 2 + i - o / 2))) / h + r : (r + a) / 2
        }
        function Vh(t, n) {
            var e = t.N;
            if (e)
                return Xh(e, n);
            var r = t.site;
            return r[1] === n ? r[0] : 1 / 0
        }
        var $h, Zh, Gh, Wh, Jh = 1e-6, Qh = 1e-12;
        function Kh(t, n, e) {
            return (t[0] - e[0]) * (n[1] - t[1]) - (t[0] - n[0]) * (e[1] - t[1])
        }
        function tf(t, n) {
            return n[1] - t[1] || n[0] - t[0]
        }
        function nf(t, n) {
            var e, r, i, o = t.sort(tf).pop();
            Wh = [],
            Zh = new Array(t.length),
            $h = new xh,
            Gh = new xh;
            while (1)
                if (i = Dh,
                o && (!i || o[1] < i.y || o[1] === i.y && o[0] < i.x))
                    o[0] === e && o[1] === r || (Bh(o),
                    e = o[0],
                    r = o[1]),
                    o = t.pop();
                else {
                    if (!i)
                        break;
                    qh(i.arc)
                }
            if (Uh(),
            n) {
                var u = +n[0][0]
                  , a = +n[0][1]
                  , s = +n[1][0]
                  , c = +n[1][1];
                Th(u, a, s, c),
                Lh(u, a, s, c)
            }
            this.edges = Wh,
            this.cells = Zh,
            $h = Gh = Wh = Zh = null
        }
        nf.prototype = {
            constructor: nf,
            polygons: function() {
                var t = this.edges;
                return this.cells.map((function(n) {
                    var e = n.halfedges.map((function(e) {
                        return kh(n, t[e])
                    }
                    ));
                    return e.data = n.site.data,
                    e
                }
                ))
            },
            triangles: function() {
                var t = []
                  , n = this.edges;
                return this.cells.forEach((function(e, r) {
                    if (o = (i = e.halfedges).length) {
                        var i, o, u, a = e.site, s = -1, c = n[i[o - 1]], l = c.left === a ? c.right : c.left;
                        while (++s < o)
                            u = l,
                            c = n[i[s]],
                            l = c.left === a ? c.right : c.left,
                            u && l && r < u.index && r < l.index && Kh(a, u, l) < 0 && t.push([a.data, u.data, l.data])
                    }
                }
                )),
                t
            },
            links: function() {
                return this.edges.filter((function(t) {
                    return t.right
                }
                )).map((function(t) {
                    return {
                        source: t.left.data,
                        target: t.right.data
                    }
                }
                ))
            },
            find: function(t, n, e) {
                var r, i, o = this, u = o._found || 0, a = o.cells.length;
                while (!(i = o.cells[u]))
                    if (++u >= a)
                        return null;
                var s = t - i.site[0]
                  , c = n - i.site[1]
                  , l = s * s + c * c;
                do {
                    i = o.cells[r = u],
                    u = null,
                    i.halfedges.forEach((function(e) {
                        var r = o.edges[e]
                          , a = r.left;
                        if (a !== i.site && a || (a = r.right)) {
                            var s = t - a[0]
                              , c = n - a[1]
                              , h = s * s + c * c;
                            h < l && (l = h,
                            u = a.index)
                        }
                    }
                    ))
                } while (null !== u);
                return o._found = r,
                null == e || l <= e * e ? i.site : null
            }
        };
        function ef() {}
        var rf = function(t) {
            return null == t ? ef : function() {
                return this.querySelector(t)
            }
        }
          , of = function(t) {
            "function" !== typeof t && (t = rf(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u, a = n[i], s = a.length, c = r[i] = new Array(s), l = 0; l < s; ++l)
                    (o = a[l]) && (u = t.call(o, o.__data__, l, a)) && ("__data__"in o && (u.__data__ = o.__data__),
                    c[l] = u);
            return new Bp(r,this._parents)
        };
        function uf() {
            return []
        }
        var af = function(t) {
            return null == t ? uf : function() {
                return this.querySelectorAll(t)
            }
        }
          , sf = function(t) {
            "function" !== typeof t && (t = af(t));
            for (var n = this._groups, e = n.length, r = [], i = [], o = 0; o < e; ++o)
                for (var u, a = n[o], s = a.length, c = 0; c < s; ++c)
                    (u = a[c]) && (r.push(t.call(u, u.__data__, c, a)),
                    i.push(u));
            return new Bp(r,i)
        }
          , cf = function(t) {
            return function() {
                return this.matches(t)
            }
        }
          , lf = function(t) {
            "function" !== typeof t && (t = cf(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u = n[i], a = u.length, s = r[i] = [], c = 0; c < a; ++c)
                    (o = u[c]) && t.call(o, o.__data__, c, u) && s.push(o);
            return new Bp(r,this._parents)
        }
          , hf = function(t) {
            return new Array(t.length)
        }
          , ff = function() {
            return new Bp(this._enter || this._groups.map(hf),this._parents)
        };
        function pf(t, n) {
            this.ownerDocument = t.ownerDocument,
            this.namespaceURI = t.namespaceURI,
            this._next = null,
            this._parent = t,
            this.__data__ = n
        }
        pf.prototype = {
            constructor: pf,
            appendChild: function(t) {
                return this._parent.insertBefore(t, this._next)
            },
            insertBefore: function(t, n) {
                return this._parent.insertBefore(t, n)
            },
            querySelector: function(t) {
                return this._parent.querySelector(t)
            },
            querySelectorAll: function(t) {
                return this._parent.querySelectorAll(t)
            }
        };
        var _f = function(t) {
            return function() {
                return t
            }
        }
          , vf = "$";
        function df(t, n, e, r, i, o) {
            for (var u, a = 0, s = n.length, c = o.length; a < c; ++a)
                (u = n[a]) ? (u.__data__ = o[a],
                r[a] = u) : e[a] = new pf(t,o[a]);
            for (; a < s; ++a)
                (u = n[a]) && (i[a] = u)
        }
        function gf(t, n, e, r, i, o, u) {
            var a, s, c, l = {}, h = n.length, f = o.length, p = new Array(h);
            for (a = 0; a < h; ++a)
                (s = n[a]) && (p[a] = c = vf + u.call(s, s.__data__, a, n),
                c in l ? i[a] = s : l[c] = s);
            for (a = 0; a < f; ++a)
                c = vf + u.call(t, o[a], a, o),
                (s = l[c]) ? (r[a] = s,
                s.__data__ = o[a],
                l[c] = null) : e[a] = new pf(t,o[a]);
            for (a = 0; a < h; ++a)
                (s = n[a]) && l[p[a]] === s && (i[a] = s)
        }
        var yf = function(t, n) {
            if (!t)
                return p = new Array(this.size()),
                c = -1,
                this.each((function(t) {
                    p[++c] = t
                }
                )),
                p;
            var e = n ? gf : df
              , r = this._parents
              , i = this._groups;
            "function" !== typeof t && (t = _f(t));
            for (var o = i.length, u = new Array(o), a = new Array(o), s = new Array(o), c = 0; c < o; ++c) {
                var l = r[c]
                  , h = i[c]
                  , f = h.length
                  , p = t.call(l, l && l.__data__, c, r)
                  , _ = p.length
                  , v = a[c] = new Array(_)
                  , d = u[c] = new Array(_)
                  , g = s[c] = new Array(f);
                e(l, h, v, d, g, p, n);
                for (var y, m, w = 0, x = 0; w < _; ++w)
                    if (y = v[w]) {
                        w >= x && (x = w + 1);
                        while (!(m = d[x]) && ++x < _)
                            ;
                        y._next = m || null
                    }
            }
            return u = new Bp(u,r),
            u._enter = a,
            u._exit = s,
            u
        }
          , mf = function() {
            return new Bp(this._exit || this._groups.map(hf),this._parents)
        }
          , wf = function(t, n, e) {
            var r = this.enter()
              , i = this
              , o = this.exit();
            return r = "function" === typeof t ? t(r) : r.append(t + ""),
            null != n && (i = n(i)),
            null == e ? o.remove() : e(o),
            r && i ? r.merge(i).order() : i
        }
          , xf = function(t) {
            for (var n = this._groups, e = t._groups, r = n.length, i = e.length, o = Math.min(r, i), u = new Array(r), a = 0; a < o; ++a)
                for (var s, c = n[a], l = e[a], h = c.length, f = u[a] = new Array(h), p = 0; p < h; ++p)
                    (s = c[p] || l[p]) && (f[p] = s);
            for (; a < r; ++a)
                u[a] = n[a];
            return new Bp(u,this._parents)
        }
          , bf = function() {
            for (var t = this._groups, n = -1, e = t.length; ++n < e; )
                for (var r, i = t[n], o = i.length - 1, u = i[o]; --o >= 0; )
                    (r = i[o]) && (u && 4 ^ r.compareDocumentPosition(u) && u.parentNode.insertBefore(r, u),
                    u = r);
            return this
        }
          , Af = function(t) {
            function n(n, e) {
                return n && e ? t(n.__data__, e.__data__) : !n - !e
            }
            t || (t = Mf);
            for (var e = this._groups, r = e.length, i = new Array(r), o = 0; o < r; ++o) {
                for (var u, a = e[o], s = a.length, c = i[o] = new Array(s), l = 0; l < s; ++l)
                    (u = a[l]) && (c[l] = u);
                c.sort(n)
            }
            return new Bp(i,this._parents).order()
        };
        function Mf(t, n) {
            return t < n ? -1 : t > n ? 1 : t >= n ? 0 : NaN
        }
        var Cf = function() {
            var t = arguments[0];
            return arguments[0] = this,
            t.apply(null, arguments),
            this
        }
          , Nf = function() {
            var t = new Array(this.size())
              , n = -1;
            return this.each((function() {
                t[++n] = this
            }
            )),
            t
        }
          , Tf = function() {
            for (var t = this._groups, n = 0, e = t.length; n < e; ++n)
                for (var r = t[n], i = 0, o = r.length; i < o; ++i) {
                    var u = r[i];
                    if (u)
                        return u
                }
            return null
        }
          , Sf = function() {
            var t = 0;
            return this.each((function() {
                ++t
            }
            )),
            t
        }
          , Ef = function() {
            return !this.node()
        }
          , kf = function(t) {
            for (var n = this._groups, e = 0, r = n.length; e < r; ++e)
                for (var i, o = n[e], u = 0, a = o.length; u < a; ++u)
                    (i = o[u]) && t.call(i, i.__data__, u, o);
            return this
        }
          , Pf = "http://www.w3.org/1999/xhtml"
          , Uf = {
            svg: "http://www.w3.org/2000/svg",
            xhtml: Pf,
            xlink: "http://www.w3.org/1999/xlink",
            xml: "http://www.w3.org/XML/1998/namespace",
            xmlns: "http://www.w3.org/2000/xmlns/"
        }
          , Lf = function(t) {
            var n = t += ""
              , e = n.indexOf(":");
            return e >= 0 && "xmlns" !== (n = t.slice(0, e)) && (t = t.slice(e + 1)),
            Uf.hasOwnProperty(n) ? {
                space: Uf[n],
                local: t
            } : t
        };
        function Df(t) {
            return function() {
                this.removeAttribute(t)
            }
        }
        function zf(t) {
            return function() {
                this.removeAttributeNS(t.space, t.local)
            }
        }
        function Of(t, n) {
            return function() {
                this.setAttribute(t, n)
            }
        }
        function Rf(t, n) {
            return function() {
                this.setAttributeNS(t.space, t.local, n)
            }
        }
        function If(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? this.removeAttribute(t) : this.setAttribute(t, e)
            }
        }
        function jf(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? this.removeAttributeNS(t.space, t.local) : this.setAttributeNS(t.space, t.local, e)
            }
        }
        var Ff = function(t, n) {
            var e = Lf(t);
            if (arguments.length < 2) {
                var r = this.node();
                return e.local ? r.getAttributeNS(e.space, e.local) : r.getAttribute(e)
            }
            return this.each((null == n ? e.local ? zf : Df : "function" === typeof n ? e.local ? jf : If : e.local ? Rf : Of)(e, n))
        }
          , Yf = function(t) {
            return t.ownerDocument && t.ownerDocument.defaultView || t.document && t || t.defaultView
        };
        function Hf(t) {
            return function() {
                this.style.removeProperty(t)
            }
        }
        function qf(t, n, e) {
            return function() {
                this.style.setProperty(t, n, e)
            }
        }
        function Bf(t, n, e) {
            return function() {
                var r = n.apply(this, arguments);
                null == r ? this.style.removeProperty(t) : this.style.setProperty(t, r, e)
            }
        }
        var Xf = function(t, n, e) {
            return arguments.length > 1 ? this.each((null == n ? Hf : "function" === typeof n ? Bf : qf)(t, n, null == e ? "" : e)) : Vf(this.node(), t)
        };
        function Vf(t, n) {
            return t.style.getPropertyValue(n) || Yf(t).getComputedStyle(t, null).getPropertyValue(n)
        }
        function $f(t) {
            return function() {
                delete this[t]
            }
        }
        function Zf(t, n) {
            return function() {
                this[t] = n
            }
        }
        function Gf(t, n) {
            return function() {
                var e = n.apply(this, arguments);
                null == e ? delete this[t] : this[t] = e
            }
        }
        var Wf = function(t, n) {
            return arguments.length > 1 ? this.each((null == n ? $f : "function" === typeof n ? Gf : Zf)(t, n)) : this.node()[t]
        };
        function Jf(t) {
            return t.trim().split(/^|\s+/)
        }
        function Qf(t) {
            return t.classList || new Kf(t)
        }
        function Kf(t) {
            this._node = t,
            this._names = Jf(t.getAttribute("class") || "")
        }
        function tp(t, n) {
            var e = Qf(t)
              , r = -1
              , i = n.length;
            while (++r < i)
                e.add(n[r])
        }
        function np(t, n) {
            var e = Qf(t)
              , r = -1
              , i = n.length;
            while (++r < i)
                e.remove(n[r])
        }
        function ep(t) {
            return function() {
                tp(this, t)
            }
        }
        function rp(t) {
            return function() {
                np(this, t)
            }
        }
        function ip(t, n) {
            return function() {
                (n.apply(this, arguments) ? tp : np)(this, t)
            }
        }
        Kf.prototype = {
            add: function(t) {
                var n = this._names.indexOf(t);
                n < 0 && (this._names.push(t),
                this._node.setAttribute("class", this._names.join(" ")))
            },
            remove: function(t) {
                var n = this._names.indexOf(t);
                n >= 0 && (this._names.splice(n, 1),
                this._node.setAttribute("class", this._names.join(" ")))
            },
            contains: function(t) {
                return this._names.indexOf(t) >= 0
            }
        };
        var op = function(t, n) {
            var e = Jf(t + "");
            if (arguments.length < 2) {
                var r = Qf(this.node())
                  , i = -1
                  , o = e.length;
                while (++i < o)
                    if (!r.contains(e[i]))
                        return !1;
                return !0
            }
            return this.each(("function" === typeof n ? ip : n ? ep : rp)(e, n))
        };
        function up() {
            this.textContent = ""
        }
        function ap(t) {
            return function() {
                this.textContent = t
            }
        }
        function sp(t) {
            return function() {
                var n = t.apply(this, arguments);
                this.textContent = null == n ? "" : n
            }
        }
        var cp = function(t) {
            return arguments.length ? this.each(null == t ? up : ("function" === typeof t ? sp : ap)(t)) : this.node().textContent
        };
        function lp() {
            this.innerHTML = ""
        }
        function hp(t) {
            return function() {
                this.innerHTML = t
            }
        }
        function fp(t) {
            return function() {
                var n = t.apply(this, arguments);
                this.innerHTML = null == n ? "" : n
            }
        }
        var pp = function(t) {
            return arguments.length ? this.each(null == t ? lp : ("function" === typeof t ? fp : hp)(t)) : this.node().innerHTML
        };
        function _p() {
            this.nextSibling && this.parentNode.appendChild(this)
        }
        var vp = function() {
            return this.each(_p)
        };
        function dp() {
            this.previousSibling && this.parentNode.insertBefore(this, this.parentNode.firstChild)
        }
        var gp = function() {
            return this.each(dp)
        };
        function yp(t) {
            return function() {
                var n = this.ownerDocument
                  , e = this.namespaceURI;
                return e === Pf && n.documentElement.namespaceURI === Pf ? n.createElement(t) : n.createElementNS(e, t)
            }
        }
        function mp(t) {
            return function() {
                return this.ownerDocument.createElementNS(t.space, t.local)
            }
        }
        var wp = function(t) {
            var n = Lf(t);
            return (n.local ? mp : yp)(n)
        }
          , xp = function(t) {
            var n = "function" === typeof t ? t : wp(t);
            return this.select((function() {
                return this.appendChild(n.apply(this, arguments))
            }
            ))
        };
        function bp() {
            return null
        }
        var Ap = function(t, n) {
            var e = "function" === typeof t ? t : wp(t)
              , r = null == n ? bp : "function" === typeof n ? n : rf(n);
            return this.select((function() {
                return this.insertBefore(e.apply(this, arguments), r.apply(this, arguments) || null)
            }
            ))
        };
        function Mp() {
            var t = this.parentNode;
            t && t.removeChild(this)
        }
        var Cp = function() {
            return this.each(Mp)
        };
        function Np() {
            var t = this.cloneNode(!1)
              , n = this.parentNode;
            return n ? n.insertBefore(t, this.nextSibling) : t
        }
        function Tp() {
            var t = this.cloneNode(!0)
              , n = this.parentNode;
            return n ? n.insertBefore(t, this.nextSibling) : t
        }
        var Sp = function(t) {
            return this.select(t ? Tp : Np)
        }
          , Ep = function(t) {
            return arguments.length ? this.property("__data__", t) : this.node().__data__
        }
          , kp = {}
          , Pp = null;
        if ("undefined" !== typeof document) {
            var Up = document.documentElement;
            "onmouseenter"in Up || (kp = {
                mouseenter: "mouseover",
                mouseleave: "mouseout"
            })
        }
        function Lp(t, n, e) {
            return t = Dp(t, n, e),
            function(n) {
                var e = n.relatedTarget;
                e && (e === this || 8 & e.compareDocumentPosition(this)) || t.call(this, n)
            }
        }
        function Dp(t, n, e) {
            return function(r) {
                var i = Pp;
                Pp = r;
                try {
                    t.call(this, this.__data__, n, e)
                } finally {
                    Pp = i
                }
            }
        }
        function zp(t) {
            return t.trim().split(/^|\s+/).map((function(t) {
                var n = ""
                  , e = t.indexOf(".");
                return e >= 0 && (n = t.slice(e + 1),
                t = t.slice(0, e)),
                {
                    type: t,
                    name: n
                }
            }
            ))
        }
        function Op(t) {
            return function() {
                var n = this.__on;
                if (n) {
                    for (var e, r = 0, i = -1, o = n.length; r < o; ++r)
                        e = n[r],
                        t.type && e.type !== t.type || e.name !== t.name ? n[++i] = e : this.removeEventListener(e.type, e.listener, e.capture);
                    ++i ? n.length = i : delete this.__on
                }
            }
        }
        function Rp(t, n, e) {
            var r = kp.hasOwnProperty(t.type) ? Lp : Dp;
            return function(i, o, u) {
                var a, s = this.__on, c = r(n, o, u);
                if (s)
                    for (var l = 0, h = s.length; l < h; ++l)
                        if ((a = s[l]).type === t.type && a.name === t.name)
                            return this.removeEventListener(a.type, a.listener, a.capture),
                            this.addEventListener(a.type, a.listener = c, a.capture = e),
                            void (a.value = n);
                this.addEventListener(t.type, c, e),
                a = {
                    type: t.type,
                    name: t.name,
                    value: n,
                    listener: c,
                    capture: e
                },
                s ? s.push(a) : this.__on = [a]
            }
        }
        var Ip = function(t, n, e) {
            var r, i, o = zp(t + ""), u = o.length;
            if (!(arguments.length < 2)) {
                for (a = n ? Rp : Op,
                null == e && (e = !1),
                r = 0; r < u; ++r)
                    this.each(a(o[r], n, e));
                return this
            }
            var a = this.node().__on;
            if (a)
                for (var s, c = 0, l = a.length; c < l; ++c)
                    for (r = 0,
                    s = a[c]; r < u; ++r)
                        if ((i = o[r]).type === s.type && i.name === s.name)
                            return s.value
        };
        function jp(t, n, e) {
            var r = Yf(t)
              , i = r.CustomEvent;
            "function" === typeof i ? i = new i(n,e) : (i = r.document.createEvent("Event"),
            e ? (i.initEvent(n, e.bubbles, e.cancelable),
            i.detail = e.detail) : i.initEvent(n, !1, !1)),
            t.dispatchEvent(i)
        }
        function Fp(t, n) {
            return function() {
                return jp(this, t, n)
            }
        }
        function Yp(t, n) {
            return function() {
                return jp(this, t, n.apply(this, arguments))
            }
        }
        var Hp = function(t, n) {
            return this.each(("function" === typeof n ? Yp : Fp)(t, n))
        }
          , qp = [null];
        function Bp(t, n) {
            this._groups = t,
            this._parents = n
        }
        function Xp() {
            return new Bp([[document.documentElement]],qp)
        }
        Bp.prototype = Xp.prototype = {
            constructor: Bp,
            select: of,
            selectAll: sf,
            filter: lf,
            data: yf,
            enter: ff,
            exit: mf,
            join: wf,
            merge: xf,
            order: bf,
            sort: Af,
            call: Cf,
            nodes: Nf,
            node: Tf,
            size: Sf,
            empty: Ef,
            each: kf,
            attr: Ff,
            style: Xf,
            property: Wf,
            classed: op,
            text: cp,
            html: pp,
            raise: vp,
            lower: gp,
            append: xp,
            insert: Ap,
            remove: Cp,
            clone: Sp,
            datum: Ep,
            on: Ip,
            dispatch: Hp
        };
        var Vp = Xp;
        Math.SQRT2;
        var $p = m("start", "end", "cancel", "interrupt")
          , Zp = []
          , Gp = 0
          , Wp = 1
          , Jp = 2
          , Qp = 3
          , Kp = 4
          , t_ = 5
          , n_ = 6
          , e_ = function(t, n, e, r, i, o) {
            var u = t.__transition;
            if (u) {
                if (e in u)
                    return
            } else
                t.__transition = {};
            u_(t, e, {
                name: n,
                index: r,
                group: i,
                on: $p,
                tween: Zp,
                time: o.time,
                delay: o.delay,
                duration: o.duration,
                ease: o.ease,
                timer: null,
                state: Gp
            })
        };
        function r_(t, n) {
            var e = o_(t, n);
            if (e.state > Gp)
                throw new Error("too late; already scheduled");
            return e
        }
        function i_(t, n) {
            var e = o_(t, n);
            if (e.state > Qp)
                throw new Error("too late; already running");
            return e
        }
        function o_(t, n) {
            var e = t.__transition;
            if (!e || !(e = e[n]))
                throw new Error("transition not found");
            return e
        }
        function u_(t, n, e) {
            var r, i = t.__transition;
            function o(t) {
                e.state = Wp,
                e.timer.restart(u, e.delay, e.time),
                e.delay <= t && u(t - e.delay)
            }
            function u(o) {
                var c, l, h, f;
                if (e.state !== Wp)
                    return s();
                for (c in i)
                    if (f = i[c],
                    f.name === e.name) {
                        if (f.state === Qp)
                            return Ne(u);
                        f.state === Kp ? (f.state = n_,
                        f.timer.stop(),
                        f.on.call("interrupt", t, t.__data__, f.index, f.group),
                        delete i[c]) : +c < n && (f.state = n_,
                        f.timer.stop(),
                        f.on.call("cancel", t, t.__data__, f.index, f.group),
                        delete i[c])
                    }
                if (Ne((function() {
                    e.state === Qp && (e.state = Kp,
                    e.timer.restart(a, e.delay, e.time),
                    a(o))
                }
                )),
                e.state = Jp,
                e.on.call("start", t, t.__data__, e.index, e.group),
                e.state === Jp) {
                    for (e.state = Qp,
                    r = new Array(h = e.tween.length),
                    c = 0,
                    l = -1; c < h; ++c)
                        (f = e.tween[c].value.call(t, t.__data__, e.index, e.group)) && (r[++l] = f);
                    r.length = l + 1
                }
            }
            function a(n) {
                var i = n < e.duration ? e.ease.call(null, n / e.duration) : (e.timer.restart(s),
                e.state = t_,
                1)
                  , o = -1
                  , u = r.length;
                while (++o < u)
                    r[o].call(t, i);
                e.state === t_ && (e.on.call("end", t, t.__data__, e.index, e.group),
                s())
            }
            function s() {
                for (var r in e.state = n_,
                e.timer.stop(),
                delete i[n],
                i)
                    return;
                delete t.__transition
            }
            i[n] = e,
            e.timer = we(o, 0, e.time)
        }
        var a_ = function(t, n) {
            var e, r, i, o = t.__transition, u = !0;
            if (o) {
                for (i in n = null == n ? null : n + "",
                o)
                    (e = o[i]).name === n ? (r = e.state > Jp && e.state < t_,
                    e.state = n_,
                    e.timer.stop(),
                    e.on.call(r ? "interrupt" : "cancel", t, t.__data__, e.index, e.group),
                    delete o[i]) : u = !1;
                u && delete t.__transition
            }
        }
          , s_ = function(t) {
            return this.each((function() {
                a_(this, t)
            }
            ))
        };
        function c_(t, n) {
            var e, r;
            return function() {
                var i = i_(this, t)
                  , o = i.tween;
                if (o !== e) {
                    r = e = o;
                    for (var u = 0, a = r.length; u < a; ++u)
                        if (r[u].name === n) {
                            r = r.slice(),
                            r.splice(u, 1);
                            break
                        }
                }
                i.tween = r
            }
        }
        function l_(t, n, e) {
            var r, i;
            if ("function" !== typeof e)
                throw new Error;
            return function() {
                var o = i_(this, t)
                  , u = o.tween;
                if (u !== r) {
                    i = (r = u).slice();
                    for (var a = {
                        name: n,
                        value: e
                    }, s = 0, c = i.length; s < c; ++s)
                        if (i[s].name === n) {
                            i[s] = a;
                            break
                        }
                    s === c && i.push(a)
                }
                o.tween = i
            }
        }
        var h_ = function(t, n) {
            var e = this._id;
            if (t += "",
            arguments.length < 2) {
                for (var r, i = o_(this.node(), e).tween, o = 0, u = i.length; o < u; ++o)
                    if ((r = i[o]).name === t)
                        return r.value;
                return null
            }
            return this.each((null == n ? c_ : l_)(e, t, n))
        };
        function f_(t, n, e) {
            var r = t._id;
            return t.each((function() {
                var t = i_(this, r);
                (t.value || (t.value = {}))[n] = e.apply(this, arguments)
            }
            )),
            function(t) {
                return o_(t, r).value[n]
            }
        }
        var p_ = function(t, n) {
            var e;
            return ("number" === typeof n ? te : n instanceof kn ? Qn : (e = kn(n)) ? (n = e,
            Qn) : ae)(t, n)
        };
        function __(t) {
            return function() {
                this.removeAttribute(t)
            }
        }
        function v_(t) {
            return function() {
                this.removeAttributeNS(t.space, t.local)
            }
        }
        function d_(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = this.getAttribute(t);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function g_(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = this.getAttributeNS(t.space, t.local);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function y_(t, n, e) {
            var r, i, o;
            return function() {
                var u, a, s = e(this);
                if (null != s)
                    return u = this.getAttribute(t),
                    a = s + "",
                    u === a ? null : u === r && a === i ? o : (i = a,
                    o = n(r = u, s));
                this.removeAttribute(t)
            }
        }
        function m_(t, n, e) {
            var r, i, o;
            return function() {
                var u, a, s = e(this);
                if (null != s)
                    return u = this.getAttributeNS(t.space, t.local),
                    a = s + "",
                    u === a ? null : u === r && a === i ? o : (i = a,
                    o = n(r = u, s));
                this.removeAttributeNS(t.space, t.local)
            }
        }
        var w_ = function(t, n) {
            var e = Lf(t)
              , r = "transform" === e ? tr : p_;
            return this.attrTween(t, "function" === typeof n ? (e.local ? m_ : y_)(e, r, f_(this, "attr." + t, n)) : null == n ? (e.local ? v_ : __)(e) : (e.local ? g_ : d_)(e, r, n))
        };
        function x_(t, n) {
            return function(e) {
                this.setAttribute(t, n.call(this, e))
            }
        }
        function b_(t, n) {
            return function(e) {
                this.setAttributeNS(t.space, t.local, n.call(this, e))
            }
        }
        function A_(t, n) {
            var e, r;
            function i() {
                var i = n.apply(this, arguments);
                return i !== r && (e = (r = i) && b_(t, i)),
                e
            }
            return i._value = n,
            i
        }
        function M_(t, n) {
            var e, r;
            function i() {
                var i = n.apply(this, arguments);
                return i !== r && (e = (r = i) && x_(t, i)),
                e
            }
            return i._value = n,
            i
        }
        var C_ = function(t, n) {
            var e = "attr." + t;
            if (arguments.length < 2)
                return (e = this.tween(e)) && e._value;
            if (null == n)
                return this.tween(e, null);
            if ("function" !== typeof n)
                throw new Error;
            var r = Lf(t);
            return this.tween(e, (r.local ? A_ : M_)(r, n))
        };
        function N_(t, n) {
            return function() {
                r_(this, t).delay = +n.apply(this, arguments)
            }
        }
        function T_(t, n) {
            return n = +n,
            function() {
                r_(this, t).delay = n
            }
        }
        var S_ = function(t) {
            var n = this._id;
            return arguments.length ? this.each(("function" === typeof t ? N_ : T_)(n, t)) : o_(this.node(), n).delay
        };
        function E_(t, n) {
            return function() {
                i_(this, t).duration = +n.apply(this, arguments)
            }
        }
        function k_(t, n) {
            return n = +n,
            function() {
                i_(this, t).duration = n
            }
        }
        var P_ = function(t) {
            var n = this._id;
            return arguments.length ? this.each(("function" === typeof t ? E_ : k_)(n, t)) : o_(this.node(), n).duration
        };
        function U_(t, n) {
            if ("function" !== typeof n)
                throw new Error;
            return function() {
                i_(this, t).ease = n
            }
        }
        var L_ = function(t) {
            var n = this._id;
            return arguments.length ? this.each(U_(n, t)) : o_(this.node(), n).ease
        }
          , D_ = function(t) {
            "function" !== typeof t && (t = cf(t));
            for (var n = this._groups, e = n.length, r = new Array(e), i = 0; i < e; ++i)
                for (var o, u = n[i], a = u.length, s = r[i] = [], c = 0; c < a; ++c)
                    (o = u[c]) && t.call(o, o.__data__, c, u) && s.push(o);
            return new cv(r,this._parents,this._name,this._id)
        }
          , z_ = function(t) {
            if (t._id !== this._id)
                throw new Error;
            for (var n = this._groups, e = t._groups, r = n.length, i = e.length, o = Math.min(r, i), u = new Array(r), a = 0; a < o; ++a)
                for (var s, c = n[a], l = e[a], h = c.length, f = u[a] = new Array(h), p = 0; p < h; ++p)
                    (s = c[p] || l[p]) && (f[p] = s);
            for (; a < r; ++a)
                u[a] = n[a];
            return new cv(u,this._parents,this._name,this._id)
        };
        function O_(t) {
            return (t + "").trim().split(/^|\s+/).every((function(t) {
                var n = t.indexOf(".");
                return n >= 0 && (t = t.slice(0, n)),
                !t || "start" === t
            }
            ))
        }
        function R_(t, n, e) {
            var r, i, o = O_(n) ? r_ : i_;
            return function() {
                var u = o(this, t)
                  , a = u.on;
                a !== r && (i = (r = a).copy()).on(n, e),
                u.on = i
            }
        }
        var I_ = function(t, n) {
            var e = this._id;
            return arguments.length < 2 ? o_(this.node(), e).on.on(t) : this.each(R_(e, t, n))
        };
        function j_(t) {
            return function() {
                var n = this.parentNode;
                for (var e in this.__transition)
                    if (+e !== t)
                        return;
                n && n.removeChild(this)
            }
        }
        var F_ = function() {
            return this.on("end.remove", j_(this._id))
        }
          , Y_ = function(t) {
            var n = this._name
              , e = this._id;
            "function" !== typeof t && (t = rf(t));
            for (var r = this._groups, i = r.length, o = new Array(i), u = 0; u < i; ++u)
                for (var a, s, c = r[u], l = c.length, h = o[u] = new Array(l), f = 0; f < l; ++f)
                    (a = c[f]) && (s = t.call(a, a.__data__, f, c)) && ("__data__"in a && (s.__data__ = a.__data__),
                    h[f] = s,
                    e_(h[f], n, e, f, h, o_(a, e)));
            return new cv(o,this._parents,n,e)
        }
          , H_ = function(t) {
            var n = this._name
              , e = this._id;
            "function" !== typeof t && (t = af(t));
            for (var r = this._groups, i = r.length, o = [], u = [], a = 0; a < i; ++a)
                for (var s, c = r[a], l = c.length, h = 0; h < l; ++h)
                    if (s = c[h]) {
                        for (var f, p = t.call(s, s.__data__, h, c), _ = o_(s, e), v = 0, d = p.length; v < d; ++v)
                            (f = p[v]) && e_(f, n, e, v, p, _);
                        o.push(p),
                        u.push(s)
                    }
            return new cv(o,u,n,e)
        }
          , q_ = Vp.prototype.constructor
          , B_ = function() {
            return new q_(this._groups,this._parents)
        };
        function X_(t, n) {
            var e, r, i;
            return function() {
                var o = Vf(this, t)
                  , u = (this.style.removeProperty(t),
                Vf(this, t));
                return o === u ? null : o === e && u === r ? i : i = n(e = o, r = u)
            }
        }
        function V_(t) {
            return function() {
                this.style.removeProperty(t)
            }
        }
        function $_(t, n, e) {
            var r, i, o = e + "";
            return function() {
                var u = Vf(this, t);
                return u === o ? null : u === r ? i : i = n(r = u, e)
            }
        }
        function Z_(t, n, e) {
            var r, i, o;
            return function() {
                var u = Vf(this, t)
                  , a = e(this)
                  , s = a + "";
                return null == a && (this.style.removeProperty(t),
                s = a = Vf(this, t)),
                u === s ? null : u === r && s === i ? o : (i = s,
                o = n(r = u, a))
            }
        }
        function G_(t, n) {
            var e, r, i, o, u = "style." + n, a = "end." + u;
            return function() {
                var s = i_(this, t)
                  , c = s.on
                  , l = null == s.value[u] ? o || (o = V_(n)) : void 0;
                c === e && i === l || (r = (e = c).copy()).on(a, i = l),
                s.on = r
            }
        }
        var W_ = function(t, n, e) {
            var r = "transform" === (t += "") ? Ke : p_;
            return null == n ? this.styleTween(t, X_(t, r)).on("end.style." + t, V_(t)) : "function" === typeof n ? this.styleTween(t, Z_(t, r, f_(this, "style." + t, n))).each(G_(this._id, t)) : this.styleTween(t, $_(t, r, n), e).on("end.style." + t, null)
        };
        function J_(t, n, e) {
            return function(r) {
                this.style.setProperty(t, n.call(this, r), e)
            }
        }
        function Q_(t, n, e) {
            var r, i;
            function o() {
                var o = n.apply(this, arguments);
                return o !== i && (r = (i = o) && J_(t, o, e)),
                r
            }
            return o._value = n,
            o
        }
        var K_ = function(t, n, e) {
            var r = "style." + (t += "");
            if (arguments.length < 2)
                return (r = this.tween(r)) && r._value;
            if (null == n)
                return this.tween(r, null);
            if ("function" !== typeof n)
                throw new Error;
            return this.tween(r, Q_(t, n, null == e ? "" : e))
        };
        function tv(t) {
            return function() {
                this.textContent = t
            }
        }
        function nv(t) {
            return function() {
                var n = t(this);
                this.textContent = null == n ? "" : n
            }
        }
        var ev = function(t) {
            return this.tween("text", "function" === typeof t ? nv(f_(this, "text", t)) : tv(null == t ? "" : t + ""))
        };
        function rv(t) {
            return function(n) {
                this.textContent = t.call(this, n)
            }
        }
        function iv(t) {
            var n, e;
            function r() {
                var r = t.apply(this, arguments);
                return r !== e && (n = (e = r) && rv(r)),
                n
            }
            return r._value = t,
            r
        }
        var ov = function(t) {
            var n = "text";
            if (arguments.length < 1)
                return (n = this.tween(n)) && n._value;
            if (null == t)
                return this.tween(n, null);
            if ("function" !== typeof t)
                throw new Error;
            return this.tween(n, iv(t))
        }
          , uv = function() {
            for (var t = this._name, n = this._id, e = hv(), r = this._groups, i = r.length, o = 0; o < i; ++o)
                for (var u, a = r[o], s = a.length, c = 0; c < s; ++c)
                    if (u = a[c]) {
                        var l = o_(u, n);
                        e_(u, t, e, c, a, {
                            time: l.time + l.delay + l.duration,
                            delay: 0,
                            duration: l.duration,
                            ease: l.ease
                        })
                    }
            return new cv(r,this._parents,t,e)
        }
          , av = function() {
            var t, n, e = this, r = e._id, i = e.size();
            return new Promise((function(o, u) {
                var a = {
                    value: u
                }
                  , s = {
                    value: function() {
                        0 === --i && o()
                    }
                };
                e.each((function() {
                    var e = i_(this, r)
                      , i = e.on;
                    i !== t && (n = (t = i).copy(),
                    n._.cancel.push(a),
                    n._.interrupt.push(a),
                    n._.end.push(s)),
                    e.on = n
                }
                ))
            }
            ))
        }
          , sv = 0;
        function cv(t, n, e, r) {
            this._groups = t,
            this._parents = n,
            this._name = e,
            this._id = r
        }
        function lv(t) {
            return Vp().transition(t)
        }
        function hv() {
            return ++sv
        }
        var fv = Vp.prototype;
        cv.prototype = lv.prototype = {
            constructor: cv,
            select: Y_,
            selectAll: H_,
            filter: D_,
            merge: z_,
            selection: B_,
            transition: uv,
            call: fv.call,
            nodes: fv.nodes,
            node: fv.node,
            size: fv.size,
            empty: fv.empty,
            each: fv.each,
            on: I_,
            attr: w_,
            attrTween: C_,
            style: W_,
            styleTween: K_,
            text: ev,
            textTween: ov,
            remove: F_,
            tween: h_,
            delay: S_,
            duration: P_,
            ease: L_,
            end: av
        };
        var pv = {
            time: null,
            delay: 0,
            duration: 250,
            ease: oi
        };
        function _v(t, n) {
            var e;
            while (!(e = t.__transition) || !(e = e[n]))
                if (!(t = t.parentNode))
                    return pv.time = ge(),
                    pv;
            return e
        }
        var vv = function(t) {
            var n, e;
            t instanceof cv ? (n = t._id,
            t = t._name) : (n = hv(),
            (e = pv).time = ge(),
            t = null == t ? null : t + "");
            for (var r = this._groups, i = r.length, o = 0; o < i; ++o)
                for (var u, a = r[o], s = a.length, c = 0; c < s; ++c)
                    (u = a[c]) && e_(u, t, n, c, a, e || _v(u, n));
            return new cv(r,this._parents,t,n)
        };
        Vp.prototype.interrupt = s_,
        Vp.prototype.transition = vv;
        function dv(t, n, e) {
            this.k = t,
            this.x = n,
            this.y = e
        }
        dv.prototype = {
            constructor: dv,
            scale: function(t) {
                return 1 === t ? this : new dv(this.k * t,this.x,this.y)
            },
            translate: function(t, n) {
                return 0 === t & 0 === n ? this : new dv(this.k,this.x + this.k * t,this.y + this.k * n)
            },
            apply: function(t) {
                return [t[0] * this.k + this.x, t[1] * this.k + this.y]
            },
            applyX: function(t) {
                return t * this.k + this.x
            },
            applyY: function(t) {
                return t * this.k + this.y
            },
            invert: function(t) {
                return [(t[0] - this.x) / this.k, (t[1] - this.y) / this.k]
            },
            invertX: function(t) {
                return (t - this.x) / this.k
            },
            invertY: function(t) {
                return (t - this.y) / this.k
            },
            rescaleX: function(t) {
                return t.copy().domain(t.range().map(this.invertX, this).map(t.invert, t))
            },
            rescaleY: function(t) {
                return t.copy().domain(t.range().map(this.invertY, this).map(t.invert, t))
            },
            toString: function() {
                return "translate(" + this.x + "," + this.y + ") scale(" + this.k + ")"
            }
        };
        var gv = new dv(1,0,0);
        function yv(t) {
            while (!t.__zoom)
                if (!(t = t.parentNode))
                    return gv;
            return t.__zoom
        }
        yv.prototype = dv.prototype;
        var mv = e("025e")
          , wv = e("2f62")
          , xv = function() {
            return e.e("pieces").then(e.t.bind(null, "41d3", 3))
        }
          , bv = function() {
            return e.e("solution-checker").then(e.t.bind(null, "4e79", 7))
        }
          , Av = {
            name: "PiecesOnBoard",
            data: function() {
                return {
                    tangram_board: null,
                    cur_moved_piece: null,
                    this_piece_dom: null,
                    pieces: null,
                    board_settings: {
                        board_pieces_drop_shadow: "drop-shadow( 10px 10px 10px rgba(0, 0, 0, 1))"
                    },
                    piecesSvgData: null,
                    pieces_settings: {
                        piece_dropped_snap: !0
                    },
                    resize_timeout_id: null,
                    board_width: window.innerWidth,
                    board_height: window.innerHeight,
                    PiecesLoader: xv,
                    SolutionChecker: bv
                }
            },
            computed: Object(a["a"])({}, Object(wv["c"])(["isAnyPuzzleChosen", "chosenPuzzle", "chosenPuzzleSolution", "gameState", "isDragPiecePlayTriggerOff", "piecesSettings", "chosenPuzzleImageSvgPath", "chosenPuzzleSvgPts", "animationCoordinationState"]), {
                piece_scale: function() {
                    var t = Math.min(this.board_width, this.board_height)
                      , n = 320
                      , e = 360
                      , r = 768
                      , i = 1024
                      , o = 1;
                    return t <= n ? .5 : t <= e ? .75 : t <= r ? 1 : t <= i ? 2 : t > i ? 2.25 : o
                },
                outer_unaccounted_padding: function() {
                    return this.board_width <= 320 ? 185 : 140
                }
            }),
            mounted: function() {
                var t = this;
                return Object(u["a"])(regeneratorRuntime.mark((function n() {
                    var e;
                    return regeneratorRuntime.wrap((function(n) {
                        while (1)
                            switch (n.prev = n.next) {
                            case 0:
                                return n.prev = 0,
                                n.next = 3,
                                t.PiecesLoader();
                            case 3:
                                e = n.sent,
                                t.piecesSvgData = Object(a["a"])({}, e).attrs,
                                t.set_game(),
                                window.addEventListener("resize", t.resize_handler),
                                n.next = 12;
                                break;
                            case 9:
                                n.prev = 9,
                                n.t0 = n["catch"](0),
                                console.log(n.t0);
                            case 12:
                            case "end":
                                return n.stop()
                            }
                    }
                    ), n, null, [[0, 9]])
                }
                )))()
            },
            beforeDestroy: function() {
                window.removeEventListener("resize", this.resize_handler),
                clearTimeout(this.resize_timeout_id)
            },
            watch: {
                gameState: function() {
                    "reset" === this.gameState && this.reset_game()
                }
            },
            methods: Object(a["a"])({}, Object(wv["d"])({
                setDraggedPieceTriggerOn: "SET_DRAGGED_PIECE_TRIGGER_ON",
                setPuzzleSolvingStateWon: "SET_PUZZLE_SOLVING_STATE_WON",
                setSolvedPuzzle: "SET_SOLVED_PUZZLE",
                setGameStateReset: "SET_GAME_STATE_RESET"
            }), {
                resize_handler: function() {
                    var t = this;
                    clearTimeout(this.resize_timeout_id),
                    this.resize_timeout_id = setTimeout((function() {
                        return t.reset_game()
                    }
                    ), 500)
                },
                reset_game: function() {
                    var t = this;
                    return Object(u["a"])(regeneratorRuntime.mark((function n() {
                        return regeneratorRuntime.wrap((function(n) {
                            while (1)
                                switch (n.prev = n.next) {
                                case 0:
                                    return t.remove_pieces(),
                                    t.remove_board(),
                                    n.prev = 2,
                                    n.next = 5,
                                    t.$nextTick();
                                case 5:
                                    t.set_game(),
                                    n.next = 11;
                                    break;
                                case 8:
                                    n.prev = 8,
                                    n.t0 = n["catch"](2),
                                    console.log("Error in reset", n.t0.stack);
                                case 11:
                                    console.log("I am in reset");
                                case 12:
                                case "end":
                                    return n.stop()
                                }
                        }
                        ), n, null, [[2, 8]])
                    }
                    )))()
                },
                remove_pieces: function() {
                    var t = this;
                    return Object(u["a"])(regeneratorRuntime.mark((function n() {
                        return regeneratorRuntime.wrap((function(n) {
                            while (1)
                                switch (n.prev = n.next) {
                                case 0:
                                    Xc("svg > .piece").data(t.piecesSvgData).exit().on("another_piece_to_snap_onto_found", null).on("piece_released", null).on("dragend", null).on("drag", null).on("dragstart", null).on("mouseenter", null).remove();
                                case 1:
                                case "end":
                                    return n.stop()
                                }
                        }
                        ), n)
                    }
                    )))()
                },
                remove_board: function() {
                    xu(".svg-board > svg").data(this.board_settings).exit().remove()
                },
                set_game: function() {
                    var t = this;
                    return Object(u["a"])(regeneratorRuntime.mark((function n() {
                        return regeneratorRuntime.wrap((function(n) {
                            while (1)
                                switch (n.prev = n.next) {
                                case 0:
                                    return n.next = 2,
                                    t.init_board();
                                case 2:
                                    t.render_pieces_on_board(),
                                    t.set_pieces_behavior();
                                case 4:
                                case "end":
                                    return n.stop()
                                }
                        }
                        ), n)
                    }
                    )))()
                },
                init_board: function() {
                    var t = this;
                    return Object(u["a"])(regeneratorRuntime.mark((function n() {
                        var e, r;
                        return regeneratorRuntime.wrap((function(n) {
                            while (1)
                                switch (n.prev = n.next) {
                                case 0:
                                    return e = "drop-shadow( 10px 10px 10px rgba(0, 0, 0, 1))",
                                    r = null,
                                    n.next = 4,
                                    new Promise((function(n) {
                                        r = setInterval((function() {
                                            if (console.log("Checking settlement"),
                                            console.log("this.animationCoordinationState"),
                                            console.log(t.animationCoordinationState),
                                            "settled" == t.animationCoordinationState)
                                                return clearInterval(r),
                                                void n()
                                        }
                                        ))
                                    }
                                    ),100);
                                case 4:
                                    t.board_width = window.innerWidth,
                                    t.board_height = window.innerHeight,
                                    t.tangram_board = xu(".svg-board").append("svg").style("filter", e).attr("width", t.board_width).attr("height", t.board_height).append("g");
                                case 7:
                                case "end":
                                    return n.stop()
                                }
                        }
                        ), n)
                    }
                    )))()
                },
                render_pieces_on_board: function() {
                    var t = this;
                    return Object(u["a"])(regeneratorRuntime.mark((function n() {
                        var e, r, i, o, u, a, s;
                        return regeneratorRuntime.wrap((function(n) {
                            while (1)
                                switch (n.prev = n.next) {
                                case 0:
                                    e = function(n) {
                                        return mv["e"](n, t.piece_scale)
                                    }
                                    ,
                                    r = function(t) {
                                        return mv["f"](t.piece_vertex_points.map(e))
                                    }
                                    ,
                                    i = mv["d"],
                                    o = i.x,
                                    u = i.y,
                                    a = "translate(".concat(o, ",").concat(u, ")"),
                                    s = 4,
                                    t.pieces = t.tangram_board.selectAll("polygon").data(mv["a"](t.piecesSvgData)).enter().append("polygon").attr("class", "piece").attr("id", (function(t) {
                                        return t.piece_id
                                    }
                                    )).attr("points", (function(t) {
                                        return r(t)
                                    }
                                    )).attr("piece_type", (function(t) {
                                        return t.piece_type
                                    }
                                    )).attr("piece_shape", (function(t) {
                                        return t.piece_shape
                                    }
                                    )).attr("piece_num_of_sides", (function(t) {
                                        return t.piece_num_of_sides
                                    }
                                    )).attr("cyclic_group_orientation", (function(t) {
                                        return t.cyclic_group_orientation
                                    }
                                    )).attr("cyclic_group_order", (function(t) {
                                        return t.cyclic_group_order
                                    }
                                    )).property("center", (function(t) {
                                        return e(t.piece_center)
                                    }
                                    )).attr("transform", a).style("fill", (function(t) {
                                        return t.fill_color
                                    }
                                    )).style("fill-opacity", (function(t) {
                                        return t.fill_opacity
                                    }
                                    )).style("stroke", (function(t) {
                                        return t.stroke
                                    }
                                    )).style("stroke-width", (function(t) {
                                        return t.stroke_width
                                    }
                                    )).attr("is_held_down", !1).attr("base_fill", (function(t) {
                                        return t.fill_color
                                    }
                                    )).attr("highlight_fill", (function(t) {
                                        return t.highlight_fill
                                    }
                                    )).attr("base_fill_opacity", (function(t) {
                                        return t.fill_opacity
                                    }
                                    )).attr("base_stroke", (function(t) {
                                        return t.stroke
                                    }
                                    )).attr("base_stroke_width", (function(t) {
                                        return t.stroke_width
                                    }
                                    )).attr("highlighted_stroke_width", (function(t) {
                                        return s * t.stroke_width
                                    }
                                    )).property("piece_snapped_neighbors_set", (function() {
                                        return new Set([])
                                    }
                                    )).property("piece_connected_component_set", (function() {
                                        return new Set([])
                                    }
                                    ));
                                case 6:
                                case "end":
                                    return n.stop()
                                }
                        }
                        ), n)
                    }
                    )))()
                },
                set_pieces_behavior: function() {
                    this.pieces.on("mouseenter", this.raise_piece).call(this.move_piece()).on("piece_released", this.piece_released_handler).on("another_piece_to_snap_onto_found", this.snap_to_another_piece)
                },
                piece_released_handler: function() {
                    var t = this;
                    return Object(u["a"])(regeneratorRuntime.mark((function n() {
                        var e, r, i, u, a, s, c, l, h, f;
                        return regeneratorRuntime.wrap((function(n) {
                            while (1)
                                switch (n.prev = n.next) {
                                case 0:
                                    if (e = t.piecesSettings.snapDroppedPiece,
                                    !e) {
                                        n.next = 23;
                                        break
                                    }
                                    if (r = t.find_piece_to_snap_onto(),
                                    !r) {
                                        n.next = 23;
                                        break
                                    }
                                    if (i = t.snap_to_another_piece(),
                                    !t.isAnyPuzzleChosen || !i) {
                                        n.next = 23;
                                        break
                                    }
                                    return u = t.pieces._groups[0],
                                    a = [u, t.piece_scale],
                                    s = t.chosenPuzzleSvgPts,
                                    c = !1,
                                    n.prev = 10,
                                    n.next = 13,
                                    t.SolutionChecker();
                                case 13:
                                    l = n.sent,
                                    h = l.compare,
                                    c = h.apply(void 0, a.concat([s])),
                                    n.next = 22;
                                    break;
                                case 18:
                                    n.prev = 18,
                                    n.t0 = n["catch"](10),
                                    console.log(n.t0),
                                    c = !1;
                                case 22:
                                    c && (f = t.piece_scale,
                                    t.setSolvedPuzzle({
                                        pieces: Object(o["a"])(u),
                                        scale: f
                                    }),
                                    t.setPuzzleSolvingStateWon(),
                                    t.setGameStateReset());
                                case 23:
                                case "end":
                                    return n.stop()
                                }
                        }
                        ), n, null, [[10, 18]])
                    }
                    )))()
                },
                raise_piece: function() {
                    xu(this).raise()
                },
                graph: function() {
                    var t = this;
                    function n(t) {
                        t.property("piece_snapped_neighbors_set").forEach((function(n) {
                            xu("#".concat(n)).property("piece_snapped_neighbors_set").delete(t.attr("id"))
                        }
                        ))
                    }
                    function e(t) {
                        t.property("piece_snapped_neighbors_set").clear()
                    }
                    function r(t) {
                        n(t),
                        e(t)
                    }
                    function i(t, n) {
                        xu("#".concat(t)).property("piece_snapped_neighbors_set").add(n)
                    }
                    function u(t, n) {
                        i(t, n),
                        i(n, t)
                    }
                    var a = function(n) {
                        for (var e = n.attr("piece_num_of_sides"), r = t.this_piece_dom.points, i = n._groups[0][0], a = t.pieces._groups[0], s = function(t) {
                            return t.id !== i.id
                        }, c = a.filter(s), l = t.$data.this_piece_dom.id, h = [], f = 0; f < e; f++)
                            h.push(new Set([l]));
                        for (var p = 1, _ = 6, v = Object(o["a"])(r), d = 0; d < e; d++) {
                            for (var g = h[d], y = 0; y < _; y++)
                                for (var m = c[y], w = Object(o["a"])(m.points), x = 0; x < w.length; x++) {
                                    var b = w[x]
                                      , A = v[d]
                                      , M = b.x - A.x
                                      , C = b.y - A.y
                                      , N = M * M + C * C;
                                    N < p && g.add(m.id)
                                }
                            if (g.size > 1)
                                for (var T = Array.from(g), S = T.length, E = 0; E < S; E++)
                                    for (var k = 0; k < S; k++)
                                        E < k && u(T[E], T[k])
                        }
                    }
                      , s = new Set;
                    function c(t) {
                        return l(t),
                        s
                    }
                    function l(t) {
                        s.add(t);
                        var n = xu("#".concat(t))
                          , e = n.property("piece_snapped_neighbors_set")
                          , r = !0
                          , i = !1
                          , o = void 0;
                        try {
                            for (var u, a = e[Symbol.iterator](); !(r = (u = a.next()).done); r = !0) {
                                var c = u.value;
                                console.log("this_piece_neighbor"),
                                console.log(c),
                                s.has(c) || l(c)
                            }
                        } catch (h) {
                            i = !0,
                            o = h
                        } finally {
                            try {
                                r || null == a.return || a.return()
                            } finally {
                                if (i)
                                    throw o
                            }
                        }
                    }
                    function h(t) {
                        var n = "black"
                          , e = 1
                          , r = "none"
                          , i = Bu
                          , o = 500
                          , u = 500
                          , a = 500;
                        t.forEach((function(t) {
                            var s = xu("#".concat(t));
                            s.transition().ease(i).style("fill", n).style("fill-opacity", e).style("stroke", r).duration(o).on("end", (function() {
                                s.transition().ease(i).style("fill", s.attr("base_fill")).style("stroke", s.attr("base_stroke")).style("stroke-width", s.attr("base_stroke_width")).delay(u).duration(a)
                            }
                            ))
                        }
                        ))
                    }
                    return {
                        cut_node_out: r,
                        add_biderectional_edge: u,
                        check_if_there_are_other_neighbors_after_snap: a,
                        get_connected_component: c,
                        show_connnected_component_animation: h
                    }
                },
                Transform: function() {
                    function t() {
                        var t = function(t, n) {
                            for (var e = 0; e < n.length; e++)
                                n[e].x += t.x,
                                n[e].y += t.y
                        }
                          , n = function(t, n) {
                            n.x += t.x,
                            n.y += t.y
                        }
                          , e = function(e, r, i) {
                            t(e, r),
                            n(e, i)
                        };
                        return {
                            vertices_by: t,
                            center_by: n,
                            piece: e
                        }
                    }
                    return {
                        translate: t
                    }
                },
                move_piece: function() {
                    var t, n, e, r, i, o, u = this, a = 10, s = 10, c = this.piece_scale + a, l = this.board_width - c, h = this.outer_unaccounted_padding + s, f = this.board_height - h, p = new Event("piece_released"), _ = {
                        get_polygon_center: function(t) {
                            for (var n = new Array(t.length), e = new Array(t.length), r = 0; r < t.length; r++)
                                n[r] = t[r].x,
                                e[r] = t[r].y;
                            var i = function(t, n) {
                                return t - n
                            }
                              , o = function(n) {
                                return (n[0] + n[t.length - 1]) / 2
                            }
                              , u = o(n.sort(i))
                              , a = o(e.sort(i));
                            return {
                                x: u,
                                y: a
                            }
                        },
                        this_piece_vertices_45_deg_clockwise: function() {
                            for (var t = r.x, n = r.y, o = Math.PI / 4, u = Math.cos(o), a = Math.sin(o), s = 0; s < e; s++) {
                                var c = i[s].x - t
                                  , l = i[s].y - n
                                  , h = u * c - a * l
                                  , f = a * c + u * l;
                                i[s].x = h + t,
                                i[s].y = f + n
                            }
                        },
                        get_positive_mod_n: function(t, n) {
                            var e = t % n;
                            return e < 0 ? n + e : e
                        },
                        this_piece_orientation_45_deg_clockwise: function() {
                            var n = parseInt(t.attr("cyclic_group_orientation"))
                              , e = parseInt(t.attr("cyclic_group_order"))
                              , r = this.get_positive_mod_n(n - 1, e);
                            t.attr("cyclic_group_orientation", r)
                        },
                        this_piece_45_deg_clockwise: function() {
                            this.this_piece_vertices_45_deg_clockwise(),
                            this.this_piece_orientation_45_deg_clockwise()
                        }
                    }, v = {
                        TOUCH_DRAG_DIST_TOL: 0,
                        MOUSE_DRAG_DIST_TOL: 1,
                        drag_detected: !1,
                        set_click_dist: function(t) {
                            o = t ? this.TOUCH_DRAG_DIST_TOL : this.MOUSE_DRAG_DIST_TOL
                        },
                        drag_detect: function(t) {
                            this.drag_detected = this.drag_detected || t.x >= 0 && t.x > o || t.y >= 0 && t.y > o || t.x < 0 && t.x < -o || t.y < 0 && t.y < -o
                        }
                    }, d = function() {
                        n = {
                            x: Ti.x,
                            y: Ti.y
                        },
                        e = t.attr("piece_num_of_sides"),
                        i = t._groups[0][0].points,
                        r = _.get_polygon_center(i),
                        g.is_this_piece_highlighted = !1,
                        v.drag_detected = !1
                    }, g = {
                        piece_highlight_state: !1,
                        toggle_this_piece_highlight: function() {
                            if (this.is_this_piece_highlighted) {
                                var n = t.attr("base_stroke_width")
                                  , e = t.attr("base_fill");
                                t.style("stroke-width", n).style("fill", e),
                                this.is_this_piece_highlighted = !1
                            } else {
                                var r = t.attr("highlighted_stroke_width")
                                  , i = t.attr("highlight_fill");
                                t.style("stroke-width", r).style("fill", i),
                                this.is_this_piece_highlighted = !0
                            }
                        }
                    }, y = {
                        this_piece_vertices_by: function(t) {
                            for (var n = 0; n < e; n++)
                                i[n].x += t.x,
                                i[n].y += t.y
                        },
                        this_piece_center_by: function(t) {
                            r.x += t.x,
                            r.y += t.y
                        },
                        this_piece: function(t) {
                            this.this_piece_vertices_by(t),
                            this.this_piece_center_by(t)
                        }
                    }, m = {
                        clip_pointer_x_within_board_boundary: function(t) {
                            return t < a ? a : l < t ? l : t
                        },
                        clip_pointer_y_within_board_boundary: function(t) {
                            return t < s ? s : f < t ? f : t
                        },
                        clip_pointer: function(t) {
                            var n = {
                                x: this.clip_pointer_x_within_board_boundary(t.x),
                                y: this.clip_pointer_y_within_board_boundary(t.y)
                            };
                            return n
                        }
                    };
                    function w() {
                        t = xu(this).raise().style("cursor", "grabbing"),
                        u.cur_moved_piece = t;
                        var n = t._groups[0][0];
                        u.this_piece_dom = n,
                        u.isDragPiecePlayTriggerOff && u.setDraggedPieceTriggerOn(),
                        d(),
                        u.graph().cut_node_out(t),
                        g.toggle_this_piece_highlight(),
                        v.set_click_dist(Vc(this).length > 0)
                    }
                    function x() {
                        var t = {
                            x: Ti.x,
                            y: Ti.y
                        }
                          , e = m.clip_pointer(t)
                          , r = {
                            x: e.x - n.x,
                            y: e.y - n.y
                        };
                        y.this_piece(r),
                        v.drag_detect(r),
                        n = e
                    }
                    function b() {
                        v.drag_detected ? (t.property("center", r),
                        this.dispatchEvent(p)) : _.this_piece_45_deg_clockwise(),
                        t.style("cursor", "grab"),
                        g.toggle_this_piece_highlight()
                    }
                    return Lu().on("start", w).on("drag", x).on("end", b).clickDistance(o)
                },
                find_piece_to_snap_onto: function() {
                    var t = this.cur_moved_piece
                      , n = this.$data.pieces._groups[0]
                      , e = this.this_piece_dom
                      , r = n
                      , i = function(t) {
                        return t.id !== e.id
                    }
                      , o = r.filter(i)
                      , u = e.points
                      , s = function(t) {
                        return Math.sqrt(t)
                    }
                      , c = function(t) {
                        return Math.pow(t, 2)
                    }
                      , l = {
                        x: 1 / 0,
                        y: 1 / 0,
                        L2: 1 / 0
                    }
                      , h = Object(a["a"])({}, l)
                      , f = null
                      , p = null;
                    o.forEach((function(t) {
                        f = t.points,
                        f.forEach((function(n) {
                            u.forEach((function(e) {
                                h.x = n.x - e.x,
                                h.y = n.y - e.y,
                                h.L2 = s(c(h.x) + c(h.y)),
                                h.L2 < l.L2 && (p = t,
                                l.x = h.x,
                                l.y = h.y,
                                l.L2 = h.L2)
                            }
                            ))
                        }
                        ))
                    }
                    ));
                    var _, v = 25;
                    if (l.L2 < v) {
                        var d = e.center;
                        _ = {
                            this_piece: e,
                            other_piece: p,
                            min_dist_x: l.x,
                            min_dist_y: l.y,
                            this_piece_center: d
                        },
                        t.property("another_piece_to_snap_onto_found_data", _)
                    }
                    return _
                },
                snap_to_another_piece: function() {
                    var t = this.cur_moved_piece
                      , n = t.property("another_piece_to_snap_onto_found_data")
                      , e = n.min_dist_x
                      , r = n.min_dist_y
                      , i = n.this_piece
                      , o = i.id
                      , u = n.this_piece_center
                      , a = this.this_piece_dom.points
                      , s = {
                        x: e,
                        y: r
                    };
                    this.Transform().translate().piece(s, a, u),
                    this.graph().check_if_there_are_other_neighbors_after_snap(t);
                    var c = this.graph().get_connected_component(o);
                    this.graph().show_connnected_component_animation(c);
                    var l = 7
                      , h = c.size === l
                      , f = []
                      , p = !0
                      , _ = !1
                      , v = void 0;
                    try {
                        for (var d, g = this.$data.pieces._groups[0][Symbol.iterator](); !(p = (d = g.next()).done); p = !0) {
                            var y = d.value
                              , m = {
                                piece_id: y.id,
                                points: y.attributes[2].value
                            };
                            f.push(m)
                        }
                    } catch (w) {
                        _ = !0,
                        v = w
                    } finally {
                        try {
                            p || null == g.return || g.return()
                        } finally {
                            if (_)
                                throw v
                        }
                    }
                    return console.log("player_assemble_svg_dom"),
                    console.log(f),
                    console.log(JSON.stringify(f, null, "  ")),
                    h
                }
            })
        }
          , Mv = Av
          , Cv = (e("a6305"),
        e("2877"))
          , Nv = Object(Cv["a"])(Mv, r, i, !1, null, "9edeb6e6", null);
        n["default"] = Nv.exports
    },
    ef4d: function(t, n, e) {
        "use strict";
        var r = e("f41b")
          , i = e.n(r);
        i.a
    },
    f41b: function(t, n, e) {}
}]);
//# sourceMappingURL=above-the-fold.dfb9acb5.js.map
