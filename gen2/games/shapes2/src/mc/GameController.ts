//make a singleton
export class GameController {
    private static instance: GameController;

    public updateMode:Function=()=>{};
    public updateConfirm:Function=()=>{};

    private constructor() {

    }
    public static getInstance(): GameController {
        if (GameController.instance == null) {
            GameController.instance = new GameController();
        }
        return GameController.instance;
    }
}