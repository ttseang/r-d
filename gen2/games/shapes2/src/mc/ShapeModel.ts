export class ShapeModel
{
    //singleton
    private static instance: ShapeModel;
    private constructor() { }
    public static getInstance(): ShapeModel {
        if (!ShapeModel.instance) {
            ShapeModel.instance = new ShapeModel();
        }
        return ShapeModel.instance;
    }    
}
/** implement later if needed */