export class BoundUtil {
    //the board and corner points
    private main: SVGSVGElement;
    private mainBounds: DOMRect;
    private svgTopCorner;
    private svgBottomCorner;

    private shapeWidth: number = 0;
    private shapeHeight: number = 0;

    constructor() {
        this.main = document.querySelector("svg") as unknown as SVGSVGElement;
        this.mainBounds = this.main.getBoundingClientRect();
        const topCorner = this.main.createSVGPoint();
        topCorner.x = this.mainBounds.left;
        topCorner.y = this.mainBounds.top;
        this.svgTopCorner = topCorner.matrixTransform(this.main.getScreenCTM()?.inverse());

        //make the bottom corner point
        const bottomCorner = this.main.createSVGPoint();
        bottomCorner.x = this.mainBounds.left + this.mainBounds.width;
        bottomCorner.y = this.mainBounds.top + this.mainBounds.height;
        this.svgBottomCorner = bottomCorner.matrixTransform(this.main.getScreenCTM()?.inverse());
    }
    public setSvgShape(svgShape: SVGUseElement) {

        const shapeBounds = svgShape.getBoundingClientRect();
        const shapeTopCorner = this.main.createSVGPoint();
        shapeTopCorner.x = shapeBounds.left;
        shapeTopCorner.y = shapeBounds.top;
        const svgShapeTopCorner = shapeTopCorner.matrixTransform(this.main.getScreenCTM()?.inverse());

        const shapeBottomCorner = this.main.createSVGPoint();
        shapeBottomCorner.x = shapeBounds.left + shapeBounds.width;
        shapeBottomCorner.y = shapeBounds.top + shapeBounds.height;
        const svgShapeBottomCorner = shapeBottomCorner.matrixTransform(this.main.getScreenCTM()?.inverse());

        this.shapeWidth = svgShapeBottomCorner.x - svgShapeTopCorner.x;
        this.shapeHeight = svgShapeBottomCorner.y - svgShapeTopCorner.y;

    }
    checkX(x: number): number {
        if (x < this.svgTopCorner.x + this.shapeWidth / 2) {
            x = this.svgTopCorner.x + this.shapeWidth / 2;
        }
        if (x > this.svgBottomCorner.x - this.shapeWidth / 2) {
            x = this.svgBottomCorner.x - this.shapeWidth / 2;
        }
        return x;
    }
    checkY(y: number): number {
        if (y < this.svgTopCorner.y + this.shapeHeight / 2) {
            y = this.svgTopCorner.y + this.shapeHeight / 2;
        }
        if (y > this.svgBottomCorner.y - this.shapeHeight / 2) {
            y = this.svgBottomCorner.y - this.shapeHeight / 2;
        }
        return y;
    }
}