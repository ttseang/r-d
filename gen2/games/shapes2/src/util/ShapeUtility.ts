import { IShape } from "../interfaces/IShape";
import { Hexagon } from "../shapes/Hexagon";
import { Parallelogram } from "../shapes/Parallelogram";
import { Rhombus } from "../shapes/Rhombus";
import { Square } from "../shapes/Square";
import { Trapezoid } from "../shapes/Trapezoid";
import { Triangle } from "../shapes/Triangle";

export class ShapeUtility {
    private definitions: IShape[] = [];
    private defTag: SVGDefsElement;
    constructor() {
        this.defTag = document.getElementsByTagName("defs")[0];

        this.definitions.push(new Hexagon());
        this.definitions.push(new Parallelogram());
        this.definitions.push(new Rhombus());
        this.definitions.push(new Square());
        this.definitions.push(new Triangle());
        this.definitions.push(new Trapezoid());

        this.addPolygons();
        this.addPatternPolygons();
    }
    public addPolygons() {
        for (let i = 0; i < this.definitions.length; i++) {
            //create a new polygon
            let polygon = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
            //set the id
            polygon.setAttribute("id", this.definitions[i].abbr);
            //set the class
            polygon.setAttribute("class", this.definitions[i].className);
            //set the points
            polygon.setAttribute("points", this.definitions[i].getPoints().toString());
            //add the polygon to the defs tag
            this.defTag.appendChild(polygon);

        }
    }
    public addPatternPolygons() {
        for (let i = 0; i < this.definitions.length; i++) {
            //create a new polygon
            let polygon = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
            //set the id
            polygon.setAttribute("id", this.definitions[i].abbr + "-pattern");
            //set the class
            polygon.setAttribute("class", "blank");
            //set the points
            polygon.setAttribute("points", this.definitions[i].getPoints().toString());
            //add the polygon to the defs tag
            this.defTag.appendChild(polygon);
            
        }
    }
    public static getShapeByIndex(index: number): IShape | null {
        switch (index) {
            case 0:
                return new Hexagon();
            case 1:
                return new Parallelogram();
            case 2:
                return new Rhombus();
            case 3:
                return new Square();
            case 4:
                return new Triangle();
            case 5:
                return new Trapezoid();
            default:
                return null;
        }
    }
    public static getShape(key: string): IShape | null {
        switch (key) {
            case "hex":
                return new Hexagon();
            case "prl":
                return new Parallelogram();
            case "rhm":
                return new Rhombus();
            case "sqr":
                return new Square();
            case "trg":
                return new Triangle();
            case "trz":
                return new Trapezoid();
            default:
                return null;
        }
    }
    
}
    
export enum ShapeType {
    Hexagon,
    Parallelogram,
    Rhombus,
    Square,
    Triangle,
    Trapezoid
}
/* <polygon id="hex" class="hexagon" points="12,0 6,-10.3923 -6,-10.3923 -12,0 -6,10.3923 6,10.3923" />
        <polygon id="prl" class="parallelogram" points="-3.1058,0 0,11.5911 3.1058,0 0,-11.5911" />
        <polygon id="rhm" class="rhombus" points="-6,0 0,10.3923 6,0 0,-10.3923" />
        <polygon id="sqr" class="square" points="-6,-6 6,-6 6,6 -6,6" />
        <polygon id="trg" class="triangle" points="0,-6.9282 -6,3.4641 6,3.4641" />
        <polygon id="trz" class="trapezoid" points="12,5.1962 6,-5.1962 -6,-5.1962 -12,5.1962" /> */