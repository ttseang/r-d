import { NearPointVo } from "../dataObjects/NearPointVo";
import { IShape } from "../interfaces/IShape";
import { ShapeUtility } from "./ShapeUtility";

export class SnapUtil {
    private main: SVGSVGElement;
    private mainBounds: DOMRect;
    private svgTopCorner;
    private svgBottomCorner;
    private board: SVGSVGElement;
    constructor() {
        this.main = document.querySelector("svg") as unknown as SVGSVGElement;
        this.board = document.querySelector("#board") as unknown as SVGSVGElement;

        this.mainBounds = this.main.getBoundingClientRect();
        const topCorner = this.main.createSVGPoint();
        topCorner.x = this.mainBounds.left;
        topCorner.y = this.mainBounds.top;
        this.svgTopCorner = topCorner.matrixTransform(this.main.getScreenCTM()?.inverse());
        const bottomCorner = this.main.createSVGPoint();
        bottomCorner.x = this.mainBounds.left + this.mainBounds.width;
        bottomCorner.y = this.mainBounds.top + this.mainBounds.height;
        this.svgBottomCorner = bottomCorner.matrixTransform(this.main.getScreenCTM()?.inverse());
    }
    public getGlobalPosition(x: number, y: number): { x: number, y: number } {
        const point = this.main.createSVGPoint();
        point.x = x;
        point.y = y;
        const svgPoint = point.matrixTransform(this.main.getScreenCTM()?.inverse());
        return { x: svgPoint.x, y: svgPoint.y };
    }
    public getGlobalDomPoint(dpoint: DOMPoint): DOMPoint {
        const point = this.main.createSVGPoint();
        point.x = dpoint.x;
        point.y = dpoint.y;
        const svgPoint = point.matrixTransform(this.main.getScreenCTM()?.inverse());
        return new DOMPoint(svgPoint.x, svgPoint.y);
    }
    public getDistanceBetweenPoints(point1: DOMPoint, point2: DOMPoint): number {
        let xDiff = point1.x - point2.x;
        let yDiff = point1.y - point2.y;
        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }
    public getDistanceBetweenShapes(shape1: IShape, shape2: IShape): number {
        //calculate the distance between the shapes using the pathagorean theorem
        let x1:number = shape1.x;
        let y1:number = shape1.y;
        let x2:number = shape2.x;
        let y2:number = shape2.y;
        let xDiff:number = x1 - x2;
        let yDiff:number = y1 - y2;
        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);

    }
    public getPointAtAngle(point: DOMPoint, angle: number, distance: number): DOMPoint {
        let x = point.x + distance * Math.cos(angle * Math.PI / 180);
        let y = point.y + distance * Math.sin(angle * Math.PI / 180);
        return new DOMPoint(x, y);
    }
    public getAngleBetweenPoints(point1: DOMPoint, point2: DOMPoint): number {
        let xDiff = point1.x - point2.x;
        let yDiff = point1.y - point2.y;
        return Math.atan2(yDiff, xDiff) * 180 / Math.PI;
    }
    public checkIfShapesAreTouching(shape1: IShape, shape2: IShape): boolean {
        let shape1Points: DOMPoint[] = shape1.getDomPoints();
        let shape2Points: DOMPoint[] = shape2.getDomPoints();
        for (let i = 0; i < shape1Points.length; i++) {
            for (let j = 0; j < shape2Points.length; j++) {
                let distance = this.getDistanceBetweenPoints(shape1Points[i], shape2Points[j]);
                //console.log(distance);
                if (distance < 1) {
                    return true;
                }
            }
        }
        return false;
    }
    public findClosestInverseAngle2(currentShape: IShape, shapes: IShape[]) {
        //loop through all the shapes
        let distance: number = 999;

        let shape: IShape;
        let nearest: CommonAnglesVo = new CommonAnglesVo();

        for (let i = 0; i < shapes.length; i++) {
            if (shapes[i] == currentShape) {
                continue;
            }
            //find the closest inverse angle
            let commonAngle: CommonAnglesVo = this.findClosestInverseAngle(currentShape, shapes[i]);
            //if the distance is less than the current distance
            if (commonAngle.distance < distance) {
                //set the distance to the new distance
                distance = commonAngle.distance;
                //set the nearest to the new nearest
                nearest = commonAngle;
                //set the shape to the new shape
                shape = shapes[i];
            }
        }
        return nearest;
    }
    public findClosestInverseAngle(shape1: IShape, shape2: IShape) {
        let commonAngles: CommonAnglesVo[] = this.findCommonInverseAnglesBetweenTwoShapes(shape1, shape2);
        let points1: DOMPoint[] = shape1.getCalculatedPoints();
        let points2: DOMPoint[] = shape2.getCalculatedPoints();
        let distance: number = 999;
        let angle: number = 0;
        for (let i = 0; i < commonAngles.length; i++) {
            let commonAngle: CommonAnglesVo = commonAngles[i];
            let start1: DOMPoint = points1[commonAngle.angle1];
            let end1: DOMPoint = points1[commonAngle.angle1 + 1];
            if (end1 == null || end1 == undefined) {
                end1 = points1[0];
            }
            let start2: DOMPoint = points2[commonAngle.angle2];
            let end2: DOMPoint = points2[commonAngle.angle2 + 1];
            if (end2 == null || end2 == undefined) {
                end2 = points2[0];
            }
            //get the distance between the two start points
            let startDistance: number = this.getDistanceBetweenPoints(start1, start2);
            //get the distance between the two end points
            let endDistance: number = this.getDistanceBetweenPoints(end1, end2);
            //average the two distances
            let avgDistance: number = (startDistance + endDistance) / 2;
            if (avgDistance < distance) {
                distance = avgDistance;
                // //console.log("distance: "+distance);
                angle = i;
            }

        }
        if (commonAngles[angle] == null || commonAngles[angle] == undefined) {
            return new CommonAnglesVo();
        }
        commonAngles[angle].shape = shape2;
        commonAngles[angle].distance = distance;
        return commonAngles[angle];
    }
    public findCommonAnglesBetweenTwoShapes(shape1: IShape, shape2: IShape): CommonAnglesVo[] {
        let angles1: number[] = shape1.getCalculatedAngles();
        let angles2: number[] = shape2.getCalculatedAngles();

        /*  //console.log(angles1);
         //console.log(angles2); */

        let lengths1: number[] = shape1.angleLengths;
        let lengths2: number[] = shape2.angleLengths;

        /*  //console.log(lengths1);
         //console.log(lengths2); */

        let commonAngles: CommonAnglesVo[] = [];
        for (let i = 0; i < angles1.length; i++) {
            for (let j = 0; j < angles2.length; j++) {

                if (angles1[i] == angles2[j] && lengths1[i] == lengths2[j]) {
                    //  //console.log(angles1[i] + " " + angles2[j]);
                    //  //console.log(lengths1[i] + " " + lengths2[j]);
                    let commonAnglesVo = new CommonAnglesVo();
                    commonAnglesVo.angle1 = i;
                    commonAnglesVo.angle2 = j;
                    commonAngles.push(commonAnglesVo);
                }
            }
        }
        return commonAngles;
    }
    public findCommonInverseAnglesBetweenTwoShapes(shape1: IShape, shape2: IShape): CommonAnglesVo[] {
        let angles1: number[] = shape1.getCalculatedAngles();
        let angles2: number[] = shape2.getCalculatedAngles();

        /* let hasSnaps1: boolean[] = shape1.hasSnaps;
        let hasSnaps2: boolean[] = shape2.hasSnaps; */

        //  //console.log(angles1);
        //   //console.log(angles2);

        let lengths1: number[] = shape1.angleLengths;
        let lengths2: number[] = shape2.angleLengths;

        //  //console.log(lengths1);
        //  //console.log(lengths2);

        let commonAngles: CommonAnglesVo[] = [];
        for (let i = 0; i < angles1.length; i++) {
            for (let j = 0; j < angles2.length; j++) {
                let angle1: number = angles1[i];
                let angle2: number = angles2[j];
                let hasSnap1: boolean = shape1.getAngleHasSnap(i);
                let hasSnap2: boolean = shape2.getAngleHasSnap(j);
                ////console.log("hasSnap1: " + hasSnap1);
                // //console.log("hasSnap2: " + hasSnap2);

                //check to see if the angles are opposite each other
                if (angle1 == angle2 + 180 || angle1 == angle2 - 180) {
                    //check to see if the lengths are the same
                    if (lengths1[i] == lengths2[j]) {
                        let commonAnglesVo = new CommonAnglesVo();
                        commonAnglesVo.angle1 = i;
                        commonAnglesVo.angle2 = j;
                        if (hasSnap1 == false && hasSnap2 == false) {
                            commonAngles.push(commonAnglesVo);
                        }

                    }

                }
            }
        }
        return commonAngles;
    }
    public findNearestPoint(shape1: IShape, shapes: IShape[]) {
        let distance: number = 999;

        let currentPoints: DOMPoint[] = shape1.getCalculatedPoints();
        //let currentLocalPoints:DOMPoint[] = shape1.getDomPoints();
        //console.log(currentPoints);
        let nearPoint: NearPointVo | null = null;
        for (let i = 0; i < shapes.length; i++) {
            if (shapes[i] == shape1) {
                continue;
            }
            let points: DOMPoint[] = shapes[i].getCalculatedPoints();
            //    let localPoints:DOMPoint[] = shapes[i].getDomPoints();
            for (let j = 0; j < points.length; j++) {
                for (let k = 0; k < currentPoints.length; k++) {
                    let currentDistance = this.getDistanceBetweenPoints(currentPoints[k], points[j]);
                    if (currentDistance < distance) {
                        distance = currentDistance;
                        nearPoint = new NearPointVo(shape1, shapes[i], k, j, currentDistance, currentPoints[k].x - points[j].x, currentPoints[k].y - points[j].y);
                    }
                }
            }
        }


       // //console.log(nearPoint);
        return nearPoint;
    }
    compareTwoShapes(currentShape: IShape, shape2: IShape) {

        let update: boolean = false;
        let commonAngle: CommonAnglesVo = this.findClosestInverseAngle(currentShape, shape2);
        if (commonAngle.shape) {
            // this.snapUtil.showTestLines(this.currentShape, commonAngle);

            //  //console.log(commonAngle);
            let shapePoints: DOMPoint[] = currentShape.getCalculatedPoints();
            let lineStart: DOMPoint = shapePoints[commonAngle.angle1];
            let lineEnd: DOMPoint = shapePoints[commonAngle.angle1 + 1];
            if (lineEnd == undefined) {
                lineEnd = shapePoints[0];
            }

            let line2Start: DOMPoint = commonAngle.shape.getCalculatedPoints()[commonAngle.angle2];
            let line2End: DOMPoint = commonAngle.shape.getCalculatedPoints()[commonAngle.angle2 + 1];
            if (line2End == undefined) {
                line2End = commonAngle.shape.getCalculatedPoints()[0];
            }

            //place the shape at the common angle

            // this.placeTestDot(lineStart.x, lineStart.y, "green");
            //  this.placeTestDot(line2End.x, line2End.y, "green");

            let totalDist: number = this.getDistanceBetweenPoints(lineStart, line2End);
            if (totalDist < 10) {
                let distX: number = line2End.x - lineStart.x;
                let distY: number = line2End.y - lineStart.y;

                currentShape.move(currentShape.x + distX, currentShape.y + distY);
                currentShape.setAngleSnap(commonAngle.angle1);
                commonAngle.shape.setAngleSnap(commonAngle.angle2);
                update = true;
            }

        }
        //console.log("update", update);
        return update;
    }
    findNearestShape(shape1: IShape, shapes: IShape[]) {
        let distance: number = 999;
        let nearShape: IShape | null = null;
        for (let i = 0; i < shapes.length; i++) {
            if (shapes[i].uid == shape1.uid) {
                continue;
            }
            let points: DOMPoint[] = shapes[i].getCalculatedPoints();
            for (let j = 0; j < points.length; j++) {
                let currentDistance = this.getDistanceBetweenPoints(shape1.getCalculatedPoints()[0], points[j]);
                if (currentDistance < distance) {
                    distance = currentDistance;
                    nearShape = shapes[i];
                }
            }
        }
        return nearShape;
    }
    findNearestShapeByPosition(shape1: IShape, shapes: IShape[]) {
        let distance: number = 999;
        let nearShape: IShape | null = null;
        for (let i = 0; i < shapes.length; i++) {
            if (shapes[i].uid == shape1.uid) {
                continue;
            }
            let x1: number = shape1.x;
            let y1: number = shape1.y;
            let x2: number = shapes[i].x;
            let y2: number = shapes[i].y;
            let currentDistance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

            if (currentDistance < distance) {
                distance = currentDistance;
                nearShape = shapes[i];
            }
        }
        return nearShape;
    }
    showAllDotsOnShape(shape: IShape) {
        let points: DOMPoint[] = shape.getCalculatedPoints();
        for (let i = 0; i < points.length; i++) {
            let point: DOMPoint = points[i];
            //console.log("x: " + point.x + " y: " + point.y + " index: " + i);
            this.placeTestDot(point.x, point.y, "green");
        }
    }
    showDotOnShape(shape: IShape, index: number, color: string = "green") {
        let points: DOMPoint[] = shape.getCalculatedPoints();
        let point: DOMPoint = points[index];
        this.placeTestDot(point.x, point.y, color);
    }
    placeTestDot(x: number, y: number, color: string = "red") {
        let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        let point: any = this.getGlobalPosition(x, y);
        circle.setAttribute("cx", x.toString());
        circle.setAttribute("cy", y.toString());
        circle.setAttribute("r", "2");
        circle.setAttribute("fill", color);
        this.board.appendChild(circle);
    }
    public showTestLines(currentShape: IShape, commonAngle: CommonAnglesVo) {
        let calcPoints: DOMPoint[] = currentShape.getCalculatedPoints();
        let shape1: IShape | null = commonAngle.shape;
        if (shape1 == null || shape1 == undefined) {
            return;
        }
        let points1: DOMPoint[] = shape1.getCalculatedPoints();
        let line1: DOMPoint[] = [];
        line1.push(points1[commonAngle.angle2]);
        if (points1[commonAngle.angle2 + 1] != undefined) {
            line1.push(points1[commonAngle.angle2 + 1]);
        }
        else {
            line1.push(points1[0]);
        }
        let colors: string[] = ["red", "blue", "green", "yellow", "purple", "orange", "pink", "brown", "black", "grey"];
        if (line1.length > 1) {
            for (let i = 0; i < line1.length; i++) {
                this.placeTestDot(line1[i].x, line1[i].y, colors[i]);
            }
        }

        let line2: DOMPoint[] = [];
        line2.push(calcPoints[commonAngle.angle1]);
        if (calcPoints[commonAngle.angle1 + 1] != undefined) {
            line2.push(calcPoints[commonAngle.angle1 + 1]);
        }
        else {
            line2.push(calcPoints[0]);
        }
        if (line2.length > 1) {
            for (let i = 0; i < line2.length; i++) {
                this.placeTestDot(line2[i].x, line2[i].y, colors[i]);
            }
        }
    }
    checkAllShapesForOverlap(shape1: IShape, shapes: IShape[]) {
        let overlap: boolean = false;
        for (let i = 0; i < shapes.length; i++) {
            let shape2: IShape = shapes[i];
            if (shape1 != shape2) {
                if (this.checkForOverlap(shape1, shape2)) {
                    overlap = true;
                    break;
                }
            }
        }
        return overlap;
    }

    checkForOverlap(shape1: IShape, shape2: IShape) {
        let calcPoints: DOMPoint[] = shape1.getCalculatedPoints();
        let points1: DOMPoint[] = shape2.getCalculatedPoints();
        let overlap: boolean = false;
        for (let i = 0; i < calcPoints.length; i++) {
            let point: DOMPoint = calcPoints[i];
            if (this.isPointInPoly(point, points1)) {
                overlap = true;
                break;
            }
        }
        return overlap;
    }
    isPointInPoly(point: DOMPoint, poly: DOMPoint[]) {
        let x = point.x, y = point.y;
        let inside = false;
        for (let i = 0, j = poly.length - 1; i < poly.length; j = i++) {
            let xi = poly[i].x, yi = poly[i].y;
            let xj = poly[j].x, yj = poly[j].y;

            let intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }

        return inside;
    }
}

export class CommonAnglesVo {
    public angle1: number = -1;
    public angle2: number = -1;
    public distance: number = 999;
    public shape: IShape | null = null;
    constructor(angle1: number = -1, angle2: number = -1) {
        this.angle1 = angle1;
        this.angle2 = angle2;
    }
}