import { IShape } from "../interfaces/IShape";

export class NearPointVo{
    public shape:IShape;
    public shape2:IShape;
    public index1:number;
    public index2:number;
    public distance:number;
    public xDistance:number;
    public yDistance:number;
    constructor(shape:IShape,shape2:IShape,index1:number,index2:number,distance:number,xDistance:number,yDistance:number){
        this.shape = shape;
        this.shape2 = shape2;
        this.index1 = index1;
        this.index2 = index2;
        this.distance = distance;
        this.xDistance = xDistance;
        this.yDistance = yDistance;
    }
}