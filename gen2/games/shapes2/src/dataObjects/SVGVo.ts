export class SVGVo {

    public x: number = 0;
    public y: number = 0;
    public rotation: number = 0;
    public shape: number = -1;
    public scale: number = 1;
    constructor(x: number = 0, y: number = 0, rotation: number = 0, shape: number = -1) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.shape = shape;
    }
    public fromTransform(transform: string): void {
        //use regex to get the x and y values
        let x: number = Number(transform.split(",")[0].split("(")[1]);
        let y: number = Number(transform.split(",")[1].split(")")[0]);
        let rotation: number = Number(transform.split(",")[2].split(")")[0].split("(")[1]);
        //let scale: number = Number(transform.split(",")[3].split(")")[0].split("(")[1]);

      

        this.x = x;
        this.y = y;
       
      //  this.scale = scale;
        this.setRotation(rotation);
    }
   public setRotation(deg: number): void {
        //convert to positive degrees
        deg = deg % 360;
        while (deg < 0) {
            deg = 360 + deg;
        }
        //deg = (deg + 360 * Math.ceil(Math.abs(deg) / 360)) % 360;
        //"hexagon", "parallelogram", "rhombus", "square", "triangle", "trapezoid"
        switch (this.shape) {

            /* case -1:
                this.rotation = deg;
                break; */

            case 0:
                //hexagon
                deg = deg % 60;
                break;
            //parallelogram
            case 1:
                deg = deg % 180;
                if (deg==180)
                {
                    deg=0;
                }
                break;
            //rhombus
            case 2:
                deg = deg % 180;
                if (deg==180)
                {
                    deg=0;
                }
                break;
            //square
            case 3:
                //keep the rotation between 0 and 90 degrees
                deg = deg % 90;
                break;
            //triangle
            case 4:
                deg = deg % 120;
                break;
            //trapezoid
            /* case 5:
                deg = deg % 180;
                break; */

        }
        //snap to the nearest 15 degrees
        deg = Math.round(deg / 15) * 15;
        deg = deg % 360;
        //(deg) => (15*Math.round(deg/15))%360;
        //console.log(deg);
        this.rotation = deg;
    }
    public getTransform(): string {
        return `translate(${this.x},${this.y}), rotate(${this.rotation})`;
        // return `translate(${this.x},${this.y}), rotate(${this.rotation}), scale(${this.scale})`;
    }
    public getSaveString(): string {
        //shape,x,y,rotation
        return `${this.shape},${this.x},${this.y},${this.rotation}`;
    }
    public getSaveArray(): Array<number> {
        return [this.shape, this.x, this.y, this.rotation];
    }
}