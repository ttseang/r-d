import { IShape } from "../interfaces/IShape";
import { BaseShape } from "./BaseShape";
import { ShapeType } from "../util/ShapeUtility";

export class Square extends BaseShape implements IShape {
    constructor() {
        super(ShapeType.Square);
        this.abbr = "sqr";
        this.className = "square";
        this.loadPoints("-6,-6 6,-6 6,6 -6,6");
        this.prune = 90;
    }    
}