import { IShape } from "../interfaces/IShape";
import { BaseShape } from "./BaseShape";
import { ShapeType } from "../util/ShapeUtility";

export class Trapezoid extends BaseShape implements IShape {
    constructor() {
        super(ShapeType.Trapezoid);
        this.abbr = "trz";
        this.className = "trapezoid";
        this.loadPoints("12,5.1962 6,-5.1962 -6,-5.1962 -12,5.1962");
        this.prune = 360;
    }    
}