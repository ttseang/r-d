import { IShape } from "../interfaces/IShape";
import { BaseShape } from "./BaseShape";
import { ShapeType } from "../util/ShapeUtility";

export class Parallelogram extends BaseShape implements IShape {
    constructor() {
        super(ShapeType.Parallelogram);
        this.abbr = "prl";
        this.className = "parallelogram";
        this.loadPoints("-3.1058,0 0,11.5911 3.1058,0 0,-11.5911");
        this.prune = 180;
    }
    
}