import { IShape } from "../interfaces/IShape";
import { BaseShape } from "./BaseShape";
import { ShapeType } from "../util/ShapeUtility";

export class Triangle extends BaseShape implements IShape {
    constructor() {
        super(ShapeType.Triangle);
        this.abbr = "trg";
        this.className = "triangle";
        this.loadPoints("0,-6.9282 -6,3.4641 6,3.4641");
        this.prune = 120;
    } 
}