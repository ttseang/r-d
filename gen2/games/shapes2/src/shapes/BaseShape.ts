import { IShape } from "../interfaces/IShape";
import { ShapeType } from "../util/ShapeUtility";

export class BaseShape implements IShape {
    protected originalX: number = 0;
    protected originalY: number = 0;
    public x: number = 0;
    public y: number = 0;
    private _angle: number = 0;
    public type: ShapeType;
    public uid: string = "";
    public abbr: string = "";
    public className: string = "";
    public classList: string[] = [];
    protected prune: number = 360;
    protected points: DOMPoint[] = [];
    protected nearest: number = 15;
    public lineAngles: number[] = [];
    public angleLengths: number[] = [];
    public angleHasSnap: boolean[] = [];
    public alpha: number = 1;
    public locked: boolean = false;
    constructor(type: ShapeType) {
        this.type = type;
    }
    get angle(): number {
        return this._angle;
    }
    set angle(angle: number) {
        //if the angle is negative, add 360 to it
        while (angle < 0) {
            angle += 360;
        }
        if (angle==this.prune)
        {
            angle = 0;
        }
        this._angle = angle;
    }
    resetToOriginalPosition(): void {
        this.x = this.originalX;
        this.y = this.originalY;
    }
    setOriginal(x: number, y: number): void {
        this.originalX = x;
        this.originalY = y;
    }
    move(x: number, y: number): void {
        //round the x and y to the nearest 4 decimal places
        this.x = Math.round(x * 10000) / 10000;
        this.y = Math.round(y * 10000) / 10000;
    }
    rotate(angle: number): void {
        //if the angle is negative, add 360 to it
        while (angle < 0) {
            angle += 360;
        }

        angle = angle % this.prune;
        angle = Math.round(angle / this.nearest) * this.nearest;
        if (angle===this.prune)
        {
            angle = 0;
        }
        this.angle = angle;
    }
    //load points 1,2 3,4 5,6 7,8 9,10 11,12 format
    //split on space and then map to DOMPoint
    //push to this.points
    loadPoints(points: string): void {
        let pointArray = points.split(" ");
        this.points = pointArray.map((point) => {
            let xy = point.split(",");
            return new DOMPoint(parseFloat(xy[0]), parseFloat(xy[1]));
        });
        this.calculateLineAngles();
        //set up anglehasSnap
        this.angleHasSnap = [];
        for (let i: number = 0; i < this.lineAngles.length; i++) {
            this.angleHasSnap.push(false);
        }
    }
    clearSnaps(): void {
        for (let i: number = 0; i < this.angleHasSnap.length; i++) {
            this.angleHasSnap[i] = false;
        }
    }
    setAngleSnap(angleIndex: number): void {
        this.angleHasSnap[angleIndex] = true;
    }
    getAngleHasSnap(index: number): boolean {
        {
            return this.angleHasSnap[index];
        }
    }
    getPoints(): string {
        //turn the points into a string to be used in the polygon
        let points: string[] = [];
        for (let i: number = 0; i < this.points.length; i++) {
            points.push(this.points[i].x + "," + this.points[i].y);
        }
        return points.join(" ");

    }
    public getRotatedPoints(): DOMPoint[] {
        let points: DOMPoint[] = [];
        for (let i: number = 0; i < this.points.length; i++) {
            let p: DOMPoint = this.points[i];
            //calculate the new x and y based on the shape's x, y, and angle
            let px: number = p.x * Math.cos(this.angle * Math.PI / 180) - p.y * Math.sin(this.angle * Math.PI / 180) + this.x;
            let py: number = p.x * Math.sin(this.angle * Math.PI / 180) + p.y * Math.cos(this.angle * Math.PI / 180) + this.y;
            points.push(new DOMPoint(px-this.x, py-this.y));
        }
        return points;
    }
    public getCalculatedPoints(): DOMPoint[] {
        let points: DOMPoint[] = [];
        for (let i: number = 0; i < this.points.length; i++) {
            let p: DOMPoint = this.points[i];
            //calculate the new x and y based on the shape's x, y, and angle
            let px: number = p.x * Math.cos(this.angle * Math.PI / 180) - p.y * Math.sin(this.angle * Math.PI / 180) + this.x;
            let py: number = p.x * Math.sin(this.angle * Math.PI / 180) + p.y * Math.cos(this.angle * Math.PI / 180) + this.y;

            points.push(new DOMPoint(px, py));
        }
        return points;
    }
    public getPointOffset(index: number): DOMPoint {
        let p: DOMPoint = this.points[index];
        let px: number = p.x * Math.cos(this.angle * Math.PI / 180) - p.y * Math.sin(this.angle * Math.PI / 180) + this.x;
        let py: number = p.x * Math.sin(this.angle * Math.PI / 180) + p.y * Math.cos(this.angle * Math.PI / 180) + this.y;
        return new DOMPoint(px, py);
    }
    private getLineAngle(p1: DOMPoint, p2: DOMPoint): number {
        let xDiff: number = p2.x - p1.x;
        let yDiff: number = p2.y - p1.y;
        let angle: number = Math.atan2(yDiff, xDiff) * 180 / Math.PI;
        //round off to the nearest integer
        angle = Math.round(angle);
        return angle;
    }
    protected calculateLineAngles(): void {
        this.lineAngles = [];
        for (let i: number = 0; i < this.points.length; i++) {
            let p1: DOMPoint = this.points[i];
            let p2: DOMPoint = this.points[(i + 1) % this.points.length];
            this.lineAngles.push(this.getLineAngle(p1, p2));
            this.angleLengths.push(this.measureAngleLen(p1, p2));
        }

    }
    
    getCalculatedAngles() {
        //make a copy of the angles
        let angles: number[] = this.lineAngles.slice();
        //rotate the angles by the shape's angle
        for (let i: number = 0; i < angles.length; i++) {
            angles[i] += this.angle;
        }
        return angles;
    }
    getDomPoints(): DOMPoint[] {
        return this.points;
    }
    measureAngleLen(point1: DOMPoint, point2: DOMPoint): number {
        let xDiff: number = point2.x - point1.x;
        let yDiff: number = point2.y - point1.y;
        let len: number = Math.sqrt(xDiff * xDiff + yDiff * yDiff);
        len = Math.round(len);
        return len;
    }
    distanceTo(shape: IShape): number {
        let xDiff: number = this.x - shape.x;
        let yDiff: number = this.y - shape.y;
        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }
    getTransform(): string {
        return "translate(" + this.x + "," + this.y + ") rotate(" + this.angle + ")";
    }
    getSaveData(): number[] {
        let data: number[] = [];
        data.push(this.type);
        data.push(this.x);
        data.push(this.y);
        data.push(this.angle);
        return data;
        //return this.type.toString() + "," + this.x + "," + this.y + "," + this.angle;
    }
    addClass(className: string){
        if (this.classList.indexOf(className) == -1) {
            this.classList.push(className);
        }
    }
    removeClass(className: string){
        let index: number = this.classList.indexOf(className);
        if (index != -1) {
            this.classList.splice(index, 1);
        }
    }
}
