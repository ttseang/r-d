import { IShape } from "../interfaces/IShape";
import { BaseShape } from "./BaseShape";
import { ShapeType } from "../util/ShapeUtility";

export class Hexagon extends BaseShape implements IShape {
    constructor() {
        super(ShapeType.Hexagon);
        this.abbr = "hex";
        this.className = "hexagon";
        this.loadPoints("12,0 6,-10.3923 -6,-10.3923 -12,0 -6,10.3923 6,10.3923");
        this.prune = 60;        
    }
    
}