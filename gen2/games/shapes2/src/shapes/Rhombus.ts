import { IShape } from "../interfaces/IShape";
import { BaseShape } from "./BaseShape";
import { ShapeType } from "../util/ShapeUtility";

export class Rhombus extends BaseShape implements IShape {
    constructor() {
        super(ShapeType.Rhombus);
        this.abbr = "rhm";
        this.className = "rhombus";
        this.loadPoints("-6,0 0,10.3923 6,0 0,-10.3923");
        this.prune = 180;
    }    
}