import { ShapeGame } from "./views/ShapeGame";
import { AddBar } from "./views/AddBar";
import { ToolBar } from "./views/ToolBar";
import { ShapeUtility } from "./util/ShapeUtility";



window.onload=function()
{
   new ShapeUtility();
   const shapeGame: ShapeGame = new ShapeGame(); 
   const addBar:AddBar=new AddBar(shapeGame);
   const toolbar:ToolBar=new ToolBar(shapeGame);
   
}