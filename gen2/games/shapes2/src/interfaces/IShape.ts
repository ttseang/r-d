import { ShapeType } from "../util/ShapeUtility";


export interface IShape
{
    uid: string;
    abbr: string;
    x: number;
    y: number;
    angle: number;
    type: ShapeType;
    className: string;
    alpha: number;
    getPoints(): string
    getDomPoints(): DOMPoint[];
    move(x: number, y: number): void;
    rotate(angleDiff: number): void;
    distanceTo(shape: IShape): number;
    getTransform(): string;
    getSaveData(): number[];
    lineAngles: number[];
    angleLengths: number[];
    getCalculatedAngles(): number[];
    getCalculatedPoints(): DOMPoint[];
    getRotatedPoints(): DOMPoint[];
    clearSnaps(): void;
    getAngleHasSnap(index: number): boolean;
    setAngleSnap(index: number): void;
    setOriginal(x: number, y: number): void;
    resetToOriginalPosition(): void;
    getPointOffset(index: number): DOMPoint;
    locked: boolean;
    classList: string[];
    addClass: (className: string) => void;
    removeClass: (className: string) => void;
}