import { GameController } from "../mc/GameController";
import { ShapeGame } from "./ShapeGame";

export class ToolBar
{
    private game:ShapeGame;
    private toolbar:HTMLElement;
    private gameController:GameController=GameController.getInstance();
    private canConfirm:boolean=false;
    constructor(game:ShapeGame)
    {
        this.game=game;
        this.toolbar=document.querySelector(".toolbar") as HTMLElement;
        this.toolbar.addEventListener("click",this.onClick.bind(this));
        //listen for a custom event named modeChanged
       
        this.gameController.updateMode=this.onModeChanged.bind(this);
        this.gameController.updateConfirm=this.onConfirmChanged.bind(this);
    }
    private onModeChanged(mode:string):void
    {
        
        let toolBarItems:NodeListOf<HTMLElement>=document.querySelectorAll(".toolbarItem");
        for(let i:number=0;i<toolBarItems.length;i++)
        {
            toolBarItems[i].classList.remove("selected");
        }
       // let id:string="tool"+mode;
        let tool:HTMLElement=document.getElementById(mode) as HTMLElement;
        tool.classList.add("selected");
    }
    private onConfirmChanged(confirm:boolean):void
    {
        console.log("confirm changed to " + confirm);
        let confirmButton:HTMLElement=document.getElementById("confirm") as HTMLElement;
        if(confirm)
        {
            confirmButton.classList.add("confirm");
            confirmButton.classList.remove("confirmDisable");
        }
        else
        {
            confirmButton.classList.add("confirmDisable");
            confirmButton.classList.remove("confirm");
        }
        this.canConfirm=confirm;
    }
    
    private onClick(event:MouseEvent):void
    {
        
        let target:HTMLElement=event.target as HTMLElement;
        if(target==null)
        {
            console.error("Target is null");
            return;
        }
        let id:string=target.id;
        let key=id.replace("tool","");
        console.log(key);
        if (key==="confirm" && this.canConfirm==false)
        {
            console.log("can't confirm");
            return;
        }
        //get all .toolBarItems
        let toolBarItems:NodeListOf<HTMLElement>=document.querySelectorAll(".toolbarItem");
        for(let i:number=0;i<toolBarItems.length;i++)
        {
            toolBarItems[i].classList.remove("selected");
        }

        target.classList.add("selected");
        if (key=="")
        {
            return;
        }
        this.game.setMode(key);
    }
}