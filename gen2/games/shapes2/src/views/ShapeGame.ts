import { NearPointVo } from "../dataObjects/NearPointVo";
import { IShape } from "../interfaces/IShape";
import { GameController } from "../mc/GameController";
import { BoundUtil } from "../util/BoundsUtil";
import { ShapeType, ShapeUtility } from "../util/ShapeUtility";
import { CommonAnglesVo, SnapUtil } from "../util/SnapUtil";


export class ShapeGame {
    private main: SVGSVGElement;
    private pattern: SVGSVGElement;
    private board: SVGGElement;


    private mode: string = "move";
    private shapes: IShape[] = [];
    private patternShapes: IShape[] = [];
    private shapeIndex: number = 0;
    private patternIndex: number = 0;
    private currentShape: IShape | null = null;
    private shapeMap: Map<string, IShape> = new Map<string, IShape>();
    private snapUtil: SnapUtil = new SnapUtil();

    public lastShape: IShape | null = null;

    public confirmShape: IShape | null = null;
    public nearestShape: IShape | null = null;


    private boundsUtil: BoundUtil = new BoundUtil();
    private gameController: GameController = GameController.getInstance();
    
    constructor() {
        (window as any).shapeGame = this;
        this.main = document.querySelector("svg") as unknown as SVGSVGElement;
        this.board = document.getElementById("board") as unknown as SVGGElement;
        this.pattern = document.getElementById("pattern") as unknown as SVGSVGElement;

        document.addEventListener("mousemove", this.onMouseMove.bind(this));
        // document.addEventListener("mouseup", this.onMouseUp.bind(this));
        document.addEventListener("mouseup", this.onUp2.bind(this));

        (window as any).shapeGame = this;
        /*  this.patternShapes.push(ShapeUtility.getShape("hex")!);
         this.patternShapes.push(ShapeUtility.getShape("prl")!);
         this.patternShapes.push(ShapeUtility.getShape("rmb")!);
         this.patternShapes.push(ShapeUtility.getShape("sqr")!);
         this.patternShapes.push(ShapeUtility.getShape("trp")!);
         this.patternShapes.push(ShapeUtility.getShape("tri")!);
         this.drawPattern(); */
        this.loadPattern("cat");
        // this.loadPattern("test2");
        // this.loadPattern("tritest");
    }
    loadPattern(file: string) {
        fetch("./assets/puzzles/" + file + ".json")
            .then(response => response.json())
            .then(data => this.process(data));
    }
    process(data: any) {
        //console.log(data);
        let shapes: any = data.shapes;

        for (let i = 0; i < shapes.length; i++) {
            let shapeData: number[] = shapes[i];
           // console.log(shapeData);
            let shapeType: number = shapeData[0];
            let x: number = shapeData[1];
            let y: number = shapeData[2];
            let angle: number = shapeData[3];

            let shape: IShape | null = ShapeUtility.getShapeByIndex(shapeType);
            if (shape) {
                this.patternIndex++;
                shape.x = x;
                shape.y = y;
                shape.angle = angle;
                shape.uid = "pattern" + this.patternIndex;
                this.patternShapes.push(shape);

            }

        }

        /*  for (let i: number = 0; i < this.patternShapes.length; i++) {
             let shape: IShape = this.patternShapes[i];
             for (let j: number = 0; j < this.patternShapes.length; j++) {
                 let shape2: IShape = this.patternShapes[j];
                 if (shape.uid != shape2.uid) {
                     this.snapUtil.compareTwoShapes(shape, shape2);
                 }
             }
         } */

       // console.log(this.patternShapes);
        this.drawPattern();
    }
    addShape(key: string): void {
        this.unselectAll();
        let shape: IShape | null = ShapeUtility.getShape(key);
        if (shape) {
            this.shapeIndex++;
            shape.x = -10;
            shape.y = 10;
            shape.uid = "shape" + this.shapeIndex;
            this.shapes.push(shape);
            this.shapeMap.set(shape.uid, shape);
            this.drawBoard();
            //this.setMode("move");
            this.gameController.updateConfirm(false);
        }
    }
    private drawPattern(): void {
        //clear the pattern svg
        for (let i = 0; i < this.patternShapes.length; i++) {
            //create a new use tag
            let use = document.createElementNS("http://www.w3.org/2000/svg", "use");

            let shape: IShape = this.patternShapes[i];

            //set the href
            use.setAttribute("href", "#" + shape.abbr + "-pattern");
            //set the class
            use.setAttribute("class", shape.className);

            //set the x,y and angle in the transform
            use.setAttribute("transform", shape.getTransform());

            //set the id
            use.setAttribute("id", this.patternShapes[i].uid);

            //add the use tag to the main svg
            this.pattern.appendChild(use);

        }

    }
    private drawBoard(): void {
        //clear the main svg
        //remove all the children

        while (this.board.firstChild) {
            this.board.removeChild(this.board.firstChild);
        }
        // this.main.innerHTML = "";
        //loop through the shapes
        for (let i = 0; i < this.shapes.length; i++) {
            //create a new use tag
            let use = document.createElementNS("http://www.w3.org/2000/svg", "use");
            //set the href
            use.setAttribute("href", "#" + this.shapes[i].abbr);
            //set the class
            use.setAttribute("class", this.shapes[i].className);

            //add the dragging class
            use.setAttribute("class", "dragShape");

            for (let j=0;j<this.shapes[i].classList.length;j++){
                use.classList.add(this.shapes[i].classList[j]);
            }

            use.setAttribute("opacity", this.shapes[i].alpha.toString());

            //set the x,y and angle in the transform
            use.setAttribute("transform", this.shapes[i].getTransform());

            //set the id
            use.setAttribute("id", this.shapes[i].uid);

            //set the onmousedown
            use.addEventListener("mousedown", this.onMouseDown.bind(this));

            //add the use tag to the main svg
            this.board.appendChild(use);
        }
        //refresh the mode
        this.setMode(this.mode);
    }
    setMode(key: string): void {
       // console.log("set mode " + key);
        this.mode = key;

        if (this.mode == "confirm") {
            this.gameController.updateConfirm(false);
            this.confirmTheShape();
            this.mode="move";
            this.gameController.updateMode("move");
            return;
        }

       
        let dragShapes: NodeListOf<SVGGElement> = document.querySelectorAll(".dragShape");

        
        this.gameController.updateMode(key);

        for (let i: number = 0; i < dragShapes.length; i++) {
            dragShapes[i].classList.remove("moveShape");
            dragShapes[i].classList.remove("rotateShape");
            dragShapes[i].classList.remove("delShape");

            switch (this.mode) {
                case "move":
                    dragShapes[i].classList.add("moveShape");
                    break;
                case "rotate":
                    dragShapes[i].classList.add("rotateShape");
                    break;
                case "delete":
                    dragShapes[i].classList.add("delShape");
                    break;
            }
        }
    }
    confirmTheShape(): void {
    

        if (this.confirmShape && this.nearestShape)
        {
       //     console.log("confirm the shape");
            //this.confirmShape.alpha = .1;
            this.confirmShape.x=this.nearestShape.x;
            this.confirmShape.y=this.nearestShape.y;
            this.confirmShape.angle=this.nearestShape.angle;
            this.confirmShape.locked=true;

            this.confirmShape.removeClass("selected");
            this.confirmShape.removeClass("inPlace");

            this.confirmShape = null;
            this.drawBoard();
            this.gameController.updateConfirm(false);
            this.checkWin();
        }

    }
    //this is the equivalent of loop through the shapes and find the one with the id
    //but much faster
    //it works much like an associative array
    getIShape(id: string): IShape | null {
        return this.shapeMap.get(id) || null;
    }
    onMouseDown(evt: MouseEvent): void {
        //get id from the target
        console.log("onMouseDown");

        this.unselectAll();
        let id = (evt.target as unknown as SVGElement).id;
        let ishape: IShape | null = this.getIShape(id);
        console.log("ishape " + ishape);

        if (ishape) {
            if (ishape.locked==true)
            {
                console.log("locked");
                return;
            }
            
            this.currentShape = ishape;
            this.currentShape.setOriginal(this.currentShape.x, this.currentShape.y);
            this.currentShape.addClass("selected");
            this.drawBoard();
        }
        if (this.mode == "copy") {
            if (this.currentShape) {
                let abbr: string = this.currentShape.abbr;
                this.addShape(abbr);
                let shape: IShape | null = this.shapes[this.shapes.length - 1];
                if (shape) {
                    shape.x = this.currentShape.x + 10;
                    shape.y = this.currentShape.y;
                    shape.angle = this.currentShape.angle;
                    shape.uid = "shape" + this.shapeIndex;
                    this.shapeMap.set(shape.uid, shape);

                    this.drawBoard();
                }
            }

        }
        if (this.mode == "delete") {
            this.shapes = this.shapes.filter((shape) => {
                return shape.uid != id;
            });
            this.shapeMap.delete(id);
            this.drawBoard();
        }
    }

    onMouseMove(evt: MouseEvent): void {

        const pt = this.main.createSVGPoint();
        pt.x = evt.clientX;
        pt.y = evt.clientY;
        const svgP = pt.matrixTransform(this.main.getScreenCTM()?.inverse());

        if (this.currentShape) {

            if (this.mode == "move") {

                let svgShape = document.getElementById(this.currentShape.uid) as unknown as SVGUseElement;
                let x = svgP.x;
                let y = svgP.y;

                //keep shape within bounds
                this.boundsUtil.setSvgShape(svgShape);
                x = this.boundsUtil.checkX(x);
                y = this.boundsUtil.checkY(y);

                this.currentShape.move(x, y);
                this.currentShape.alpha = 1;


                /* if (this.checkForOverlap(this.currentShape, this.shapes)) {
                    //  this.currentShape.alpha=0.5;
                    this.currentShape.resetToOriginalPosition();
                }
                else {
                    this.currentShape.setOriginal(x, y);
                    this.currentShape.alpha = 1;
                } */

            }
            else if (this.mode == "rotate") {

                //get the angle between the current shape and the mouse
                let x: number = svgP.x - this.currentShape.x;
                let y: number = svgP.y - this.currentShape.y;
                let angle: number = Math.atan2(y, x) * 180 / Math.PI;
                this.currentShape.rotate(angle);
            }
            this.drawBoard();
        }
    }
    resetSnaps() {
        for (let i: number = 0; i < this.shapes.length; i++) {
            let shape: IShape = this.shapes[i];
            shape.clearSnaps();
        }
        //compare all the shapes to each other
        for (let i: number = 0; i < this.shapes.length; i++) {
            let shape: IShape = this.shapes[i];
            for (let j: number = 0; j < this.shapes.length; j++) {
                let shape2: IShape = this.shapes[j];
                if (shape.uid != shape2.uid) {
                    this.snapUtil.compareTwoShapes(shape, shape2);
                }
            }
        }
        this.drawBoard();
    }
    onUp2(evt: MouseEvent): void {

        if (this.mode == "copy") {
            return;
        }
        if (this.shapes.length > 1 && this.currentShape) {
            //find the nearest 2 points
            let nearPoint: NearPointVo | null = this.snapUtil.findNearestPoint(this.currentShape, this.shapes);
            if (nearPoint) {
                if (nearPoint.distance < 10) {
                    let rotPoints1: DOMPoint[] = nearPoint.shape.getRotatedPoints();
                    let rotPoints2: DOMPoint[] = nearPoint.shape2.getRotatedPoints();
                    let point1: DOMPoint = rotPoints1[nearPoint.index1];
                    let point2: DOMPoint = rotPoints2[nearPoint.index2];

                    let distX: number = point1.x - point2.x;
                    let distY: number = point1.y - point2.y;

                    this.currentShape.move(nearPoint.shape2.x - distX, nearPoint.shape2.y - distY);
                    this.drawBoard();
                    
                }
            }
        }
        if (this.mode==="move" || this.mode==="rotate") {
            {
                if (this.currentShape) {
                    let inPlace:boolean=this.checkForConfirm();
                    if (inPlace==true)
                    {
                        this.currentShape.removeClass("selected");
                        this.currentShape.addClass("inPlace");
                        this.drawBoard();
                    }
                    this.gameController.updateConfirm(inPlace);
                }
            }     
        
       
        }
        this.lastShape = this.currentShape;
        this.currentShape = null;
    }
    checkForConfirm() {
        if (this.currentShape) {
            let nearestShape: IShape | null = this.snapUtil.findNearestShapeByPosition(this.currentShape, this.patternShapes);
            if (nearestShape) {
                this.nearestShape = nearestShape;
                //compare the shapes by distance, angle, and type
                let dist: number = this.snapUtil.getDistanceBetweenShapes(this.currentShape, nearestShape);

                let angle1: number = this.currentShape.angle;
                let angle2: number = nearestShape.angle;
                let angleDiff: number = Math.abs(angle1 - angle2);

                let type1: ShapeType = this.currentShape.type;
                let type2: ShapeType = nearestShape.type;
               // this.snapUtil.showAllDotsOnShape(nearestShape);
                if (type1 !== type2) {
                    console.log(type1);
                    console.log(type2);
                    
                  //  nearestShape.alpha=0.1;

                  //  console.log("type mismatch");
                    return false;
                }

                if (dist > 1) {
                  //  console.log("distance mismatch");
                    return false;
                }

                if (angleDiff > 2) {
                  //  console.log("angle mismatch");
                    return false;
                }
                this.confirmShape=this.currentShape;
               // alert("confirm");
                return true;
            }

        }
        return false;

    }
    checkForOverlap(shape: IShape, shapes: IShape[]): boolean {
        if (this.currentShape) {
            if (this.snapUtil.checkAllShapesForOverlap(this.currentShape, this.shapes)) {
                return true;
            }
        }
        return false;
    }

    checkWin() {
        if (this.shapes.length < this.patternShapes.length) {
            return false;
        }
        if (this.patternShapes.length == 0) {
            return false;
        }
        for (let i: number = 0; i < this.shapes.length; i++) {
            let shape: IShape = this.shapes[i];
            shape.alpha = 1;
            let nearestShape: IShape | null = this.snapUtil.findNearestShapeByPosition(shape, this.patternShapes);
            if (nearestShape) {
                let dist: number = this.snapUtil.getDistanceBetweenShapes(shape, nearestShape);
                if (dist > 15) {
                    console.log("not close enough");
                    console.log(dist);
                    shape.alpha = 0.5;
                    this.drawBoard();
                    return false;
                }
                else {
                    if (shape.type != nearestShape.type) {
                        console.log("not the right type");
                        shape.alpha = 0.5;
                        this.drawBoard();
                        return false;
                    }
                    let angleDiff: number = Math.abs(shape.angle - nearestShape.angle);
                    if (angleDiff > 10) {
                        console.log("angle not correct");
                        console.log(angleDiff);
                        console.log("expected: " + nearestShape.angle);
                        console.log("got: " + shape.angle);
                        shape.alpha = 0.5;
                        this.drawBoard();
                        return false;
                    }
                }
            }
        }
        this.drawBoard();
        alert("you win");
        return true;
    }
    unselectAll()
    {
        for (let i: number = 0; i < this.shapes.length; i++) {
            let shape: IShape = this.shapes[i];
            shape.removeClass("selected");
            shape.removeClass("inPlace");
        }
        //this.drawBoard();
    }

    saveFile() {
        let data: string = this.save();
        let blob: Blob = new Blob([data], { type: "text/plain" });
        let url: string = URL.createObjectURL(blob);
        let a: HTMLAnchorElement = document.createElement("a");
        a.click();
    }
    save() {
        let data: any[] = [];
        for (let i = 0; i < this.shapes.length; i++) {
            data.push(this.shapes[i].getSaveData());
        }
        let dataObj: any = {};
        dataObj.shapes = data;
        let dataStr: string = JSON.stringify(dataObj);
        //console.log(dataStr);
        return dataStr;
    }
}