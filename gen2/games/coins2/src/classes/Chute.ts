import { ITargetObj } from 'drageng';
import { Align, IBaseScene, PosVo } from 'ttphasercomps';
export class Chute extends Phaser.GameObjects.Container implements ITargetObj {
    private bscene: IBaseScene;
    private back: Phaser.GameObjects.Image;
    public content: string="";
    public correct: string;
    public original: PosVo;
    public place: PosVo;
    public myScale:number;
    public index:number=0;
    
    constructor(bscene: IBaseScene,myScale:number) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.back = this.scene.add.image(0, 0, 'tube').setOrigin(0.5,0);
        this.myScale=myScale;
        this.add(this.back);
        this.scene.add.existing(this);

        this.setSize(this.back.displayWidth,this.back.displayHeight);

        Align.scaleToGameW(this,myScale,this.bscene);
    }
    setColor(color: number): void {
       
    }
    
    setPlace(pos: PosVo): void {
        this.place=pos;
        this.bscene.getGrid().placeAt(pos.x,pos.y,this);
    }
    doResize(): void {
       Align.scaleToGameW(this,this.myScale,this.bscene);
       this.bscene.getGrid().placeAt(this.place.x,this.place.y,this);
    }
    
    setContent(content: string): void {
        this.content=content;
    }
    getContainer(): Phaser.GameObjects.Container {
        return this as Phaser.GameObjects.Container;
    }
}