import { IDragObj } from 'drageng';
import { Align, IBaseScene, PosVo } from 'ttphasercomps';

export class Coin extends Phaser.GameObjects.Container implements IDragObj {
    private bscene: IBaseScene;
    private back: Phaser.GameObjects.Image;
    public place:PosVo=new PosVo(0,0);
    private myScale:number=1;
    public canDrag: boolean=true;
    public content: string="";
    public original: PosVo=new PosVo(0,0);
    public resetPlace: PosVo=new PosVo(0,0);

    public inChute:boolean=false;
    public chuteIndex:number=0;
    public stackIndex:number=0;
    public value:number=0;
    
    constructor(bscene: IBaseScene, key: string, myScale: number) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.back = this.scene.add.image(0, 0, key);
        this.myScale=myScale;
        this.add(this.back);

      

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        Align.scaleToGameW(this,myScale,this.bscene);

        this.scene.add.existing(this);

    
    }
   
    snapBack(): void {
        if (this.inChute==true)
        {
            return;
        }
        this.scene.add.tween({targets:this,x:this.resetPlace.x,y:this.resetPlace.y,duration:500});
    }
    snapToPlace(xx:number,yy:number,callback:Function)
    {       
        this.scene.add.tween({targets:this,y:yy,x:xx,duration:200,onComplete:()=>{
            callback();
        }});
    }
    snapToPos(posVo:PosVo)
    {
        let realPos:PosVo=this.bscene.getGrid().getRealXY(posVo.x,posVo.y);
        console.log(realPos);
        this.scene.add.tween({targets:[this],y:realPos.y,x:realPos.x,duration:200,onComplete:()=>{
            this.inChute=false;
            this.setPlace(posVo);
        }});
    }
    setContent(content: string): void {
        this.content=content;
    }
    getContainer(): Phaser.GameObjects.Container {
       return this as Phaser.GameObjects.Container;
    }
    setPlace(pos:PosVo)
    {
        this.place=pos;
        
        if (this.original.x==0 && this.original.y==0)
        {
            this.original=this.place;
        }
        this.bscene.getGrid().placeAt(pos.x,pos.y,this);

       // this.resetPlace.x=this.x;
       // this.resetPlace.y=this.y;
    }
    doResize()
    {
        Align.scaleToGameW(this,this.myScale,this.bscene);
        this.bscene.getGrid().placeAt(this.place.x,this.place.y,this);
    }
}