
import { DragEng, IDragObj, ITargetObj } from "drageng";
import { ColorBurst } from "gameeffects";
import { GameObjects } from "phaser";
import { CompLayout, CompManager, ButtonController, CompLoader, PosVo, ResponseImage } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { Chute } from "../classes/Chute";
import { Coin } from "../classes/Coin";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;
    private dragEng: DragEng;

    private coinKeys: string[] = ["penny", "nickle", "dime", "quarter"];
    private coinScales: number[] = [0.05, 0.056, 0.047, 0.063];
    private coinVals: number[] = [1, 5, 10, 25];

    public valueGrid: number[][] = [];
    public stackCount: number[] = [];

    private labelCoins:Coin[]=[];
    private pointers:GameObjects.Image[]=[];

    private colorBurst:ColorBurst;

    constructor() {
        super("SceneMain");
        for (let i: number = 0; i < 4; i++) {
            this.valueGrid[i] = [];
            for (let j: number = 0; j < 4; j++) {
                this.valueGrid[i][j] = 0;
            }

            this.stackCount[i] = 0;
        }


        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("arrows", "./assets/arrows.png");

        this.load.image("penny", "./assets/penny.png");
        this.load.image("nickle", "./assets/nickle.png");
        this.load.image("dime", "./assets/dime.png");
        this.load.image("quarter", "./assets/quarter.png");

        this.load.image("tube", "./assets/singleTube.png");
        this.load.image("pointer","./assets/pointer.png");

        ColorBurst.preload(this, "./assets/effects/colorStars.png");

        this.load.audio("rightSound", "./assets/audio/quiz_right.wav");
        this.load.audio("wrongSound", "./assets/audio/quiz_wrong.wav");
        this.load.audio("click", "./assets/audio/click.wav");

    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.dragEng = new DragEng(this);

        (window as any).scene = this;

        
   //     this.grid.showNumbers();


        /*  
          this.dragEng.collideCallback=()=>{};
           */

        this.cm.regFontSize("list", 30, 1200);
        this.cm.regFontSize("words", 40, 1200);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        this.buttonController.callback = this.doButtonAction.bind(this);

        this.dragEng.collideCallback = this.onCollide.bind(this);
    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);
        this.buildGame();
    }
    buildGame() {

        this.showIcons(false,false);
        this.makeCoins();
        this.makeTargets();
        this.makeLabels();

        this.colorBurst=new ColorBurst(this,this.gw/2,this.gh/2,30);

        this.dragEng.setListeners();
        this.dragEng.clickLock = false;
        this.dragEng.upCallback = this.onUp.bind(this);

    }
    makeCoins() {
        
        for (let j: number = 0; j < 4; j++) {
            for (let i: number = 0; i < 4; i++) {
                /*  let xx: number = (Math.random() * 3) + 6
                 let yy: number = (Math.random() * 5) + 3; */

                let coin: Coin = new Coin(this, this.coinKeys[i], this.coinScales[i] / 1.25);
                coin.value = this.coinVals[i];
                coin.setPlace(this.getRandCoinSpot());
                coin.resetPlace = new PosVo(coin.x, coin.y);
                coin.canDrag = true;
                coin.setInteractive();

                coin.setContent(i.toString())

                this.dragEng.addDrag(coin);
            }

        }

    }
    makeLabels()
    {
        for (let i: number = 0; i < 4; i++) {
            /*  let xx: number = (Math.random() * 3) + 6
             let yy: number = (Math.random() * 5) + 3; */

            let coin: Coin = new Coin(this, this.coinKeys[i], this.coinScales[i] / 2);

            /* let chute:Chute=this.dragEng.targets[3-i] as Chute;

            coin.x=chute.x;
            coin.y=chute.y+chute.displayHeight+coin.displayHeight/2+20; */
            this.labelCoins.push(coin);

        }
        this.lineUpLabels();
    }
    lineUpLabels()
    {
        for (let i: number = 0; i < 4; i++) {
            /*  let xx: number = (Math.random() * 3) + 6
             let yy: number = (Math.random() * 5) + 3; */

            let coin: Coin = this.labelCoins[i];
            coin.doResize();

            let chute:Chute=this.dragEng.targets[3-i] as Chute;

            coin.x=chute.x;
            coin.y=chute.y+chute.displayHeight;
            this.labelCoins.push(coin);

            let pointer:Phaser.GameObjects.Image=this.pointers[i];

            Align.scaleToGameW(pointer,0.025,this);
            pointer.x=chute.x;
            pointer.y=chute.y;

            this.children.bringToTop(pointer);

        }
    }
    getRandCoinSpot() {
        let xx: number = (Math.random() * 2) + 7;
        let yy: number = (Math.random() * 5) + 3;

        return new PosVo(xx, yy);
    }
    makeTargets() {
        for (let i: number = 0; i < 4; i++) {
            let chute: Chute = new Chute(this, 0.1);

            chute.setPlace(new PosVo(2 + i * 1.2, 2));
            chute.setContent(i.toString());
            chute.index = i;

            this.dragEng.addTarget(chute);

            let pointer:Phaser.GameObjects.Image=this.add.image(0,0,"pointer");
           
            this.pointers.push(pointer);
        }
    }
    onUp(obj: IDragObj) {
        if (obj) {
            let coin: Coin = obj as Coin;
            if (coin.inChute == true) {
                coin.canDrag = true;
              //  coin.alpha = .5;
                //coin.inChute=false;

                this.valueGrid[coin.chuteIndex][coin.stackIndex] = 0;

                this.bringDown(coin.chuteIndex,coin.stackIndex);

                if (this.stackCount[coin.chuteIndex] > 0) {
                    this.stackCount[coin.chuteIndex]--;
                }

                let posVo: PosVo = this.getRandCoinSpot();
                console.log(posVo);
                coin.snapToPos(posVo);
                //  coin.setPlace(posVo);
            }
        }

    }
    onCollide(dropObj: IDragObj, targObj: ITargetObj) {
        //console.log(dropObj);
        // console.log(targObj);

        let targIndex: number = (targObj as Chute).index;
        console.log("tindex=" + targIndex.toString());


        let stackCount: number = this.stackCount[targIndex];
        console.log("stackCount=" + stackCount);

        if (stackCount == 4) {
            dropObj.snapBack();
            return;
        }

        let coin: Coin = dropObj as Coin;

        let xx: number = targObj.x;
        let yy: number = targObj.y;

        this.valueGrid[targIndex][stackCount] = coin.value;

        // coin.x=xx;
        // coin.y=yy+(targObj.displayHeight/5*(4-stackCount))


        /* let yPos:number=3-stackCount;

        let pos:PosVo=this.valueGrid[targIndex][yPos];
        console.log(pos);


        
        coin.snapToPlace(pos); */
        coin.canDrag = false;
        this.stackCount[targIndex] = stackCount + 1;

        coin.inChute = true;
        coin.chuteIndex = targIndex;
        coin.stackIndex = stackCount;

        let y2: number = targObj.y + (targObj.displayHeight / 5 * (4 - coin.stackIndex));
        coin.x = targObj.x;
        coin.snapToPlace(targObj.x, y2, () => { this.lineUpCoins(); })
        this.playSound("click");
    }
    bringDownValues(chuteIndex: number, stackIndex: number)
    {
        //console.log(stackIndex);
        for (let i:number=stackIndex;i<4;i++)
        {
          //  this.valueGrid[chuteIndex][i]=100;
            this.valueGrid[chuteIndex][i]= this.valueGrid[chuteIndex][i+1];
        }
        this.valueGrid[chuteIndex][3]=0;
    }
    bringDown(chuteIndex: number, stackIndex: number) {

        this.bringDownValues(chuteIndex,stackIndex);
        let coins: IDragObj[] = this.dragEng.dragObjs;

        for (let i: number = 0; i < coins.length; i++) {
            let coin: Coin = coins[i] as Coin;

            if (coin.inChute == true) {
                if (coin.chuteIndex == chuteIndex) {
                    if (coin.stackIndex > stackIndex) {
                        coin.stackIndex--;
                    }
                }
            }
        }
        this.lineUpCoins();
    }
    lineUpCoins() {
        console.log("line up");

        let coins: IDragObj[] = this.dragEng.dragObjs;

        for (let i: number = 0; i < coins.length; i++) {
            let coin: Coin = coins[i] as Coin;

            if (coin.inChute == true) {
                let chute: Chute = this.dragEng.targets[coin.chuteIndex] as Chute;

                coin.x = chute.x;
                coin.y = chute.y + (chute.displayHeight / 5 * (4 - coin.stackIndex));
            }
        }

    }
    resetCoins() {
        let coins: IDragObj[] = this.dragEng.dragObjs;

        for (let i: number = 0; i < coins.length; i++) {
            let coin: Coin = coins[i] as Coin;
            coin.inChute = false;
            coin.canDrag = true;
            coin.setPlace(this.getRandCoinSpot());

        }

        for (let i: number = 0; i < 4; i++) {

            for (let j: number = 0; j < 4; j++) {
                this.valueGrid[i][j] = 0;
            }

            this.stackCount[i] = 0;
        }
    }
    showIcons(showRight:boolean,showWrong:boolean)
    {
        this.cm.getComp("wrong").visible = showWrong;
        this.cm.getComp("right").visible = showRight;

        this.children.bringToTop(this.cm.getComp("wrong") as ResponseImage);
        this.children.bringToTop(this.cm.getComp("right") as ResponseImage);
    }
    doResize() {

        console.log("resize");

        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
   //     this.grid.showPos();
        this.cm.doResize();
        this.dragEng.doResize();
        this.lineUpCoins();
        this.lineUpLabels();
    }
    checkCoins() {
        let coinVals: number[] = [1, 5, 10, 25];
        coinVals.reverse();

        for (let i: number = 0; i < this.valueGrid.length; i++) {
            //console.log(this.valueGrid[i]);
            for (let j: number = 0; j < this.valueGrid[i].length; j++) {
                let val: number = this.valueGrid[i][j];
                // console.log(val);
                if (val != coinVals[i]) {
                    console.log("wrong");
                    this.playSound("wrongSound");
                    this.showIcons(false,true);
                    setTimeout(() => {
                        this.showIcons(false,false);
                    }, 1000);
                    return;
                }
            }
        }
        console.log("right");
        this.showIcons(true,false);
        this.playSound("rightSound");
        this.colorBurst.start();
        this.dragEng.clickLock=true;
    }
    playSound(key: string) {
        let sound: Phaser.Sound.BaseSound = this.sound.add(key);
        sound.play();
    }
    doButtonAction(action: string, params: string) {

        console.log(action);

        if (action == "reset") {
            this.resetCoins();
        }
        if (action == "check") {
            this.checkCoins();
        }
    }

}