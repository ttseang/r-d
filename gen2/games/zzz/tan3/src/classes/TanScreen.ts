export class TanScreen {
    private canvas: HTMLCanvasElement;
    private shapes: HTMLImageElement[] = [];
    private star: HTMLImageElement | undefined = undefined;
    private preloadImages: string[] = [];

    constructor() {
        //create canvas full screen and center it
        this.canvas = document.createElement("canvas");
        this.canvas.width = document.body.clientWidth;
        this.canvas.height = document.body.clientHeight;
        this.canvas.style.position = "absolute";
        this.canvas.style.left = "0px";
        this.canvas.style.top = "0px";
        this.canvas.style.zIndex = "0";
        this.canvas.style.backgroundColor = "black";
        document.body.appendChild(this.canvas);

        this.preload();
    }
    preload() {
        this.preloadImages.push("./assets/hex.png");
      //  this.preloadImages.push("./assets/star.png");
       // this.preloadImages.push("./assets/triangle.png");
       // this.preloadImages.push("./assets/square.png");
        this.preloadNext();
    }
    preloadNext() {
        console.log("len="+this.preloadImages.length);
        if (this.preloadImages.length > 0) {
            let path = this.preloadImages.shift();
            console.log("path="+path);
            if (path) {
                const image: HTMLImageElement = new Image();
                image.onload = () => {
                    console.log("loaded");
                    this.shapes.push(image);
                    this.preloadNext();
                }
                image.src = path;
            }
           
        }
        else {
            this.create();

        }
    }
    create() {
        alert("all preloaded");
        
    }
    loadImage(path: string): HTMLImageElement {
        const image: HTMLImageElement = new Image();
        image.onload = () => {
            this.draw(0, 0, image);
        }
        image.src = path;
        return image;
    }
    draw(x: number, y: number, image: HTMLImageElement) {
        //draw images
        const ctx = this.canvas.getContext("2d");
        if (ctx) {
           return ctx.drawImage(image, x, y);
        }
        return null;
    }
}