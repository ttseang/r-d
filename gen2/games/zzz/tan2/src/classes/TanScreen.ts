export class TanScreen {
    private shapeArray: string[] = ["hex", "rhomb1", "rhomb2", "square", "trap", "triangle"];
    private currentShape: HTMLElement | null = null;
    private shapeIndex: number = 0;
    private shapes: HTMLElement[] = [];
    private board: HTMLElement | null = null;
    constructor() {
        console.log("TanScreen");

        this.board = document.querySelector(".board");

        this.addShapesToToolbar();
        this.addListeners();
        (window as any).tanScreen = this;
        this.addShapeToCanvas("hex");
    }
    addListeners() {
        document.addEventListener("mousedown", (e: MouseEvent) => {
            this.startDrag(e);
        });
        if (this.board) {

            this.board.addEventListener("mousemove", (e: MouseEvent) => {
                this.doDrag2(e);
            });
        }
        document.addEventListener("pointerup", (e: MouseEvent) => {
            this.endDrag(e);
        });
    }
    doDrag2(e: MouseEvent) {
        if (this.currentShape && this.board) {
            //get body bounds
            let bodyBounds: DOMRect = document.body.getBoundingClientRect();
            let boardBounds: DOMRect = this.board.getBoundingClientRect();
            let x: number = e.clientX;// + boardBounds.left;
            let y: number = e.clientY;// + boardBounds.top;

          


            x-=bodyBounds.left;
            y-=bodyBounds.top;

            let shapeBounds: DOMRect = this.currentShape?.getBoundingClientRect();
            x -= shapeBounds.width/2;
            y -= shapeBounds.height / 2;


            let leftEdge:number=boardBounds.left-bodyBounds.left;
            let rightEdge:number=boardBounds.left+boardBounds.width-shapeBounds.width-bodyBounds.left;
            let bottomEdge:number=boardBounds.top+boardBounds.bottom-shapeBounds.height;

            if (x<leftEdge)
            {
                x=leftEdge;
            }
            if (x>rightEdge)
            {
                x=rightEdge;
    
            }
            if (y<0)
            {
                y=0;
            }
            if (y>bottomEdge)
            {
                y=bottomEdge;
            }
            this.currentShape.style.left = x + "px";
            this.currentShape.style.top = y + "px";
        }
    }
    addShapesToToolbar() {
        const shapebar: HTMLElement | null = document.querySelector(".shapebar");
        if (shapebar) {
            for (let i = 0; i < this.shapeArray.length; i++) {
                const shape: HTMLElement = document.createElement("div");
                shape.id = this.shapeArray[i];
                shape.className = "shapebox";
                let image: string = "./assets/" + this.shapeArray[i] + ".png";
                shape.style.backgroundImage = "url(" + image + ")";
                shapebar.appendChild(shape);

                shape.addEventListener("click", () => {
                    this.addShapeToCanvas(this.shapeArray[i]);
                });
            }
        }
    }
    addShapeToCanvas(shape: string) {

        if (this.board) {
            this.shapeIndex++;

            const newShape: HTMLElement = document.createElement("div");
            newShape.className = "shape";
            newShape.id = "shape" + this.shapeIndex;
            newShape.style.backgroundImage = "url(./assets/" + shape + ".png)";
            this.board.appendChild(newShape);
            this.shapes.push(newShape);
        }

    }
    startsWith(str: string, prefix: string) {
        return str.indexOf(prefix) === 0;
    }
    startDrag(e: MouseEvent) {

        if (e.target) {
            let id: string = (<HTMLElement>e.target).id;
            if (this.startsWith(id, "shape")) {
                this.currentShape = <HTMLElement>e.target;
            }
        }

    }
    doDrag(e: MouseEvent) {
        let bodyBounds: DOMRect = document.body.getBoundingClientRect();
        if (this.currentShape) {
            //remove the currentshape from the parent so it can be moved
            //without interference from the parent
            if (this.currentShape.parentElement) {
                this.currentShape.parentElement.removeChild(this.currentShape);
            }
            //add the shape to the board so it can be moved
            if (this.board) {
                this.board.appendChild(this.currentShape);
            }
            //subtract the document.body offset from the mouse position
            //so the shape is positioned correctly

            let yy: number = e.clientY - bodyBounds.top;
            let xx: number = e.clientX - bodyBounds.left;

            //subtract the shape offset from the mouse position
            //so the shape is positioned correctly
            if (this.currentShape) {
                let shapeBounds: DOMRect = this.currentShape.getBoundingClientRect();
                yy -= shapeBounds.height / 2;
                xx -= shapeBounds.width / 2;
            }
            //set the shape position to the mouse position
            this.currentShape.style.top = yy + "px";
            this.currentShape.style.left = xx + "px";

        }
       
        //get the bounds of the board
        //if the shape is outside the bounds, move it back inside



        if (this.board && this.currentShape) {
            let boardBounds: DOMRect = this.board.getBoundingClientRect();
            let shapeBounds: DOMRect = this.currentShape.getBoundingClientRect();
            let left2: number = bodyBounds.left + this.board.offsetLeft;

            let top2: number = bodyBounds.top + this.board.offsetTop;
            let right2: number = left2 + this.board.offsetWidth - bodyBounds.left;
            let bottom2: number = top2 + this.board.offsetHeight;
            if (shapeBounds.left < left2) {
                this.currentShape.style.left = "0px";
            }
            if (shapeBounds.top < top2) {
                this.currentShape.style.top = top2 + "px";
            }
            if (shapeBounds.right > right2) {
                this.currentShape.style.left = (right2 - shapeBounds.width) + "px";
            }
            if (shapeBounds.bottom > bottom2) {
                this.currentShape.style.top = (bottom2 - shapeBounds.height) + "px";
            }

        }



    }
    endDrag(e: MouseEvent) {

        if (this.currentShape && this.board)
        {
            //convert the shape position to a percentage of the board so the game can be scaled
            //get the bounds of the body
            let bodyBounds: DOMRect = document.body.getBoundingClientRect();
            //get the bounds of the shape
            let shapeBounds: DOMRect = this.currentShape.getBoundingClientRect();

            let perX:number=(shapeBounds.left/bodyBounds.width)*100;
            let perY:number=(shapeBounds.top/bodyBounds.height)*100;
            
            let leftPer:number=(bodyBounds.left/this.board.offsetWidth)*100;
            let topPer:number=(bodyBounds.top/this.board.offsetHeight)*100;
            
            perX-=leftPer;
            perY-=topPer;

            this.currentShape.style.left=perX+"%";
            this.currentShape.style.top=perY+"%";

          
        }

        this.currentShape = null;
        console.log("endDrag");
    }
    rotateShapeClockwise() {
        if (this.currentShape) {
            let currentRotation: number = parseInt(this.currentShape.style.transform.replace("rotate(", "").replace("deg)", ""));
            if (isNaN(currentRotation)) {
                currentRotation = 0;
            }
            this.currentShape.style.transform = "rotate(" + (currentRotation + 90) + "deg)";
        }
    }
    rotateShapeCounterClockwise() {
        if (this.currentShape) {
            let currentRotation: number = parseInt(this.currentShape.style.transform.replace("rotate(", "").replace("deg)", ""));
            if (isNaN(currentRotation)) {
                currentRotation = 0;
            }
            this.currentShape.style.transform = "rotate(" + (currentRotation - 90) + "deg)";
        }
    }
}