

import { DragEng, IDragObj, ITargetObj } from "dragEng";
import { ColorBurst } from "gameEffects";
import { CompLayout, CompManager, ButtonController, CompLoader, PosVo, BaseScene } from "../../../../common/mods/ttcomps";
import { TargetBox } from "../classes/TargetBox";
import { WordBox } from "../classes/WordBox";
import { WordShapeVo } from "../dataObjs/WordShapeVo";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;
    private dragEng: DragEng;

    private chosen:string="";
    private correct:string="fog";
    private words: string[] = ["word", "fog", "spot"];
    private shapes:string[]=["u","n","d"];

    private wordShapes:WordShapeVo[]=[];
    private level:number=-1;
    private colorBurst:ColorBurst;

    private buttonLock:boolean=false;

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("arrows","./assets/arrows.png");
        
        this.load.audio("rightSound", "./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound", "./assets/audio/quiz_wrong.wav");

        ColorBurst.preload(this,"./assets/effects/colorStars.png");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        this.dragEng = new DragEng(this);

        this.colorBurst=new ColorBurst(this,this.gw/2,this.gh/2);

        this.buttonController.callback = this.doButtonAction.bind(this);

        window['scene'] = this;

    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);
        this.loadGameData();
      //  this.buildGame();
    }
    loadGameData() {
         fetch("./assets/words.json")
        .then(response => response.json())
        .then(data => this.gotGameData({ data }));
    }
    gotGameData(data: any) {
       
        let words:any=data.data.words;

        for (let i:number=0;i<words.length;i++)
        {
            let wordObj:any=words[i];

            let wordShapeVo:WordShapeVo=new WordShapeVo(wordObj.words,wordObj.shape,wordObj.correct);
            console.log(wordShapeVo);
            this.wordShapes.push(wordShapeVo);
        }
        this.buildGame();
    }
    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
        // this.grid.showPos();
        this.cm.doResize();
        this.dragEng.doResize();
    }
    doButtonAction(action: string, params: string) {

        if (this.buttonLock==true)
        {
            return;
        }
        if (action==="check")
        {
            this.checkCorrect();
        }
        if (action=="reset")
        {
            this.dragEng.resetBoxes();
            this.dragEng.clickLock=false;
        }
    }
    /**
     * GAME STARTS HERE
     */
    buildGame() {
        
        this.dragEng.setListeners();
        this.dragEng.collideCallback=this.onDrop.bind(this);
        
        this.showIcons(false,false);
       
        this.nextLevel();
    }
    showIcons(showRight:boolean,showWrong:boolean)
    {
        this.cm.getComp("wrong").visible=false;
        this.cm.getComp("right").visible=false;
    }
    onDrop(dragObj:IDragObj,targ:ITargetObj)
    {
        this.chosen=dragObj.content;
     //   targ.setContent(dragObj.content);
        this.dragEng.clickLock=true;
    }
    checkCorrect()
    {
        if (this.correct==this.chosen)
        {
            console.log("Correct");
            this.showIcons(true,false);
            this.playSound("rightSound");
            this.colorBurst.start();

            this.buttonLock=true;

            setTimeout(() => {
                this.nextLevel();
                this.dragEng.clickLock=false;
            }, 2000);
        }
        else
        {
            this.showIcons(false,true);
            console.log("Wrong");
            this.playSound("wrongSound");
        }
    }
    nextLevel()
    {
        this.level++;
        if (this.level>this.wordShapes.length-1)
        {
            console.log("out of words");
            this.buttonLock=false;
            return;
        }
        let wordShapeVo:WordShapeVo=this.wordShapes[this.level];
        this.shapes=wordShapeVo.shape.split("");
        this.words=wordShapeVo.words.split(",");
        this.correct=wordShapeVo.correct;

        this.makeBoxes();

        this.buttonLock=false;
        this.dragEng.clickLock=false;
    }
   
    makeBoxes() {
        console.log("make boxes");

        this.dragEng.destoryDrags()
        this.dragEng.destroyTargs();

        for (let i: number = 0; i < this.words.length; i++) {
            let box: WordBox = new WordBox(this, this.words[i]);
            
            let place: PosVo = new PosVo(8, i * 1.5 + 3);
            box.setPlace(place);
            box.original = place;
            box.setInteractive();
            this.dragEng.addDrag(box);
        }
      //  this.grid.showPos();
        let tb: TargetBox = new TargetBox(this, this.shapes);
        tb.setPlace(new PosVo(5, 5));
        tb.original = tb.place;

        this.dragEng.addTarget(tb);
    }
}