import { BaseDragObj, IDragObj } from "dragEng";
import { Align, IBaseScene, TextStyleVo } from "../../../../common/mods/ttcomps";

export class WordBox extends BaseDragObj implements IDragObj {
    //private text1:Phaser.GameObjects.Text;
    private back: Phaser.GameObjects.Image;
    private word: string;
    private letterSpacing: number;
    private letterH: number;
    private textBoxes: Phaser.GameObjects.Text[] = [];
    
    constructor(bscene: IBaseScene, word: string) {
        super(bscene);
        // this.text1=this.scene.add.text(0,0,word);
        this.word = word;
        this.content=word;
        this.letterSpacing = this.bscene.cw * 0.5;
        
        this.back = this.scene.add.image(0, 0, "holder");
        this.back.setTint(0xcccccc);


        this.add(this.back);
        // this.add(this.text1);

        this.makeLetters();
        this.doResize();

        this.scene.add.existing(this);

        this.back.alpha = .1;
    }
    doResize() {
        this.bscene.getGrid().place(this.place, this);
        this.back.displayWidth = this.word.length * this.letterSpacing;
        this.back.displayHeight = this.letterH;

        this.makeLetters();

        this.setSize(this.back.displayWidth, this.back.displayHeight);
    }
    private placeLetters() {
        let style: TextStyleVo = this.cm.getTextStyle("standard");

        this.letterSpacing = this.bscene.cw * 0.5;

        let start: number = -this.letterSpacing * this.word.length / 2;
        for (let i: number = 0; i < this.textBoxes.length; i++) {
            let textBox: Phaser.GameObjects.Text = this.textBoxes[i];
            textBox.setFontSize(this.cm.getFontSize(style.fontSizeKey, this.bscene.getW()));
            textBox.x = start + this.letterSpacing * i;
           // textBox.x += textBox.displayWidth / 1.5;
        }
    }
    private makeLetters() {

        while(this.textBoxes.length>0)
        {
            this.textBoxes.pop().destroy();
        }

        let style: TextStyleVo = this.cm.getTextStyle("standard");

        let start: number = -this.letterSpacing * this.word.length / 2;

        for (let i: number = 0; i < this.word.length; i++) {
            let textBox: Phaser.GameObjects.Text = this.scene.add.text(start + this.letterSpacing * i, 0, this.word.substr(i, 1));

            textBox.setOrigin(0.5, 0.5);
            textBox.setColor(style.textColor);
            textBox.setFontSize(this.cm.getFontSize(style.fontSizeKey, this.bscene.getW()));
            this.textBoxes.push(textBox);
            //textBox.x += textBox.displayWidth / 2;
            this.letterH = textBox.displayHeight;
            this.add(textBox);
        }

        this.placeLetters();
    }
}