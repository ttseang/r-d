import { ITargetObj } from "dragEng";
import { BackStyleVo, CompBack, CompManager, IBaseScene, PosVo, TextStyleVo } from "../../../../common/mods/ttcomps";

export class TargetBox extends Phaser.GameObjects.Container implements ITargetObj {
    private types: string[];
    public correct: string;
    public original: PosVo;
    public place: PosVo;
    private graphics: Phaser.GameObjects.Graphics;
    private bscene: IBaseScene;
    private cm: CompManager = CompManager.getInstance();
    public content: string;


    constructor(bscene: IBaseScene, types: string[]) {
        super(bscene.getScene());
        this.types = types;
        this.bscene = bscene;

        this.graphics = this.scene.add.graphics();

        this.add(this.graphics);
        this.makeBacks();

        this.scene.add.existing(this);

        window['tb'] = this;
    }
    public getContainer()
    {
        return this;
    }
    setColor(color: number): void {
       
    }
   
    setPlace(pos: PosVo): void {
        this.place=pos;
        this.bscene.getGrid().place(pos,this);
    }
    doResize(): void {
        this.makeBacks();
        this.bscene.getGrid().place(this.place,this);
    }
    
    setContent(word:string)
    {
       this.content=this.content;
    }
    makeBacks() {
        let boxSpacing: number = this.bscene.cw * 0.5;
        let boxH: number = this.bscene.ch * 0.75;
        let startX:number=-this.types.length/2*boxSpacing-(boxSpacing/2);
        let startY:number=-boxH/2;

        let backStyleVo: BackStyleVo = this.cm.getBackStyle("targetbox");

        this.graphics.clear();
        this.graphics.lineStyle(backStyleVo.borderThick, backStyleVo.borderColor);
        this.graphics.fillStyle(backStyleVo.backColor, 1);

        let style: TextStyleVo = this.cm.getTextStyle("standard");

        for (let i: number = 0; i < this.types.length; i++) {

            let h: number = boxH;
            let w: number = boxSpacing * 0.99;
            let yy: number = 0;

            let type: string = this.types[i];

            if (type == "u") {
                h = boxH * 1.5;

                yy = -h / 3;
            }
            if (type == "d") {
                h = boxH * 1.5;;
                //yy=h*1.5;
            }

            let xx:number=startX+i*boxSpacing;

            this.graphics.fillRoundedRect(xx, startY+yy, w, h, 8);
            this.graphics.fill();
            this.graphics.strokeRoundedRect(xx, startY+yy, w, h, 8);
            this.graphics.stroke();

       

        }
    }
}