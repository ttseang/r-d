export class WordShapeVo
{
    public words:string;
    public shape:string;
    public correct:string;

    constructor(words:string,shape:string,correct:string)
    {
        this.words=words;
        this.shape=shape;
        this.correct=correct;
    }
}