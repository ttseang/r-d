
import { CompLayout, CompManager, ButtonController, CompLoader } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout:CompLayout;
    private cm:CompManager=CompManager.getInstance();
    private buttonController:ButtonController=ButtonController.getInstance();
    private compLoader:CompLoader;
   

    constructor() {
        super("SceneMain");
        this.compLoader=new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {
         this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check","./assets/check.png");
        this.load.image("qmark","./assets/questionmark.png");
        this.load.image("right","./assets/right.png");
        this.load.image("wrong","./assets/wrong.png");
        this.load.image("triangle","./assets/triangle.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        this.cm.regFontSize("list",30,1200);
        this.cm.regFontSize("words",40,1200);

        this.compLayout=new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");     
        
        this.buttonController.callback=this.doButtonAction.bind(this);
    }
    compsLoaded()
    {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize=this.doResize.bind(this);
        this.loadGameData();
    }
    loadGameData()
    {
        /* fetch("./assets/words.json")
        .then(response => response.json())
        .then(data => this.gotGameData({ data })); */
    }
    gotGameData(data:any)
    {

    }
    doResize()
    {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;
      
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
       // this.grid.showPos();
        this.cm.doResize();
       
    }
    doButtonAction(action:string,params:string)
    {

    }

}