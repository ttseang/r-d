import gsap from "gsap";
import Draggable from "gsap/Draggable";

export class SpinGame {

    private circleStyles: HTMLStyleElement;

    constructor() {
        this.circleStyles = document.createElement("style");
        console.log("SpinGame constructor");
        gsap.registerPlugin(Draggable);
        this.setUpCss();
     /*    window.addEventListener("resize",()=>{
            //set css variables
            //square size should get bigger as the window gets smaller

            const squareSize = Math.min(window.innerWidth, window.innerHeight) * .05;
            console.log("squareSize=" + squareSize);
            //set the css variables
            document.documentElement.style.setProperty("--square-size", squareSize.toString());
        }); */
    }
    setUpCss() {
        /* .square {
            position: absolute;
            
            background-image: url("../assets/pic1.png");
            background-position: center;
            background-repeat: no-repeat;
            border-radius: 50%;
        } */
        //pick a random pic between 1 and 14
        let r: number = Math.floor(Math.random() * 4) + 1;
        let pic: string = "../assets/l" + r + ".png";
        if (this.circleStyles == null) {
            this.circleStyles = document.createElement("style");
        }
        this.circleStyles.append(".square {position: absolute;background-image: url(\"./assets/" + pic + "\");background-position: center;background-repeat: no-repeat;border-radius: 50%;}");
        this.makeCircles();
    }
    makeCircles() {


        //get the width of the window
     /*    const width = window.innerWidth;
        const size:number=width/15;
        console.log("SIZE="+size);

        //set the css variables
        document.documentElement.style.setProperty("--square-size", size.toString()); */

        //get the container
        let container = document.querySelector(".squares") as HTMLElement;
        let circleCount = 12;
        let factor = .5;
        //loop through the circles
        for (let i = 0; i < circleCount; i++) {
            //create a new div
            let circle = document.createElement("div");
            //set the class to square
            circle.classList.add("square");
            //set the id to square + i
            circle.classList.add("square" + i);
            //add the circle to the container
            container.appendChild(circle);

            //create a style element
            if (this.circleStyles == null) {
                this.circleStyles = document.createElement("style");
            }
            //add the style to the style element
            // width: calc(var(--vw) * var(--square-size)*1);
            //height: calc(var(--vw) * var(--square-size)*1);
            let z: number = circleCount - i;
            let per:number=(i+1)*(100/circleCount);
            console.log("per="+per);
            
            this.circleStyles.append(".square" + i + "{width:" + per + "%;height: "+per+"%;z-index:" + z + ";}");
            //this.circleStyles.append(".square" + i + "{width: calc(var(--vw) * var(--square-size)*" + factor + ");height: calc(var(--vw) * var(--square-size)*" + factor + ");z-index:" + z + ";}");
            factor += 0.25;
        }
        //add the style element to the head
        document.head.appendChild(this.circleStyles);
        this.setUpCircles();
    }

    setUpCircles() {
        //get all the circles from .squares
        let circles = document.querySelectorAll(".square") as NodeListOf<HTMLElement>;
        //loop through the circles
        for (let i = 0; i < circles.length; i++) {
            let circle = circles[i];
            //set a random rotation
            //circle.style.transform = "rotate(" + Math.random() * 360 + "deg)";
            let drag = Draggable.create(".square" + i, { inertia: true, type: "rotation", bounds: "body" })[0];
            drag.addEventListener("dragend", this.fixCircles.bind(this));
        }
        this.fixCircles();
    }
    fixCircles() {
        console.log("fixCircles");
        //get all the circles from .squares
        let circles = document.querySelectorAll(".square") as NodeListOf<HTMLElement>;
        console.log(circles);
        let zeroCount = 0;
        //loop through the circles
        for (let i = 0; i < circles.length; i++) {
            let circle = circles[i];
            //get the rotation of the circle
            let rotation = circle.style.transform;
            //  console.log(rotation);
            //all  text between rotate( and deg) using regex
            let degText: string[] | null = rotation.match(/rotate\((.*?)deg\)/);
            if (degText != null) {
                // console.log(degText[0]);
                //remove the rotate( and deg) from the text
                let deg = degText[0].replace("rotate(", "").replace("deg)", "");
                //get the number of degrees
                let degNumber = Number(deg);
                console.log(degNumber);
                if (degNumber < 0) {
                    degNumber += 360;
                }
                if (degNumber > 360) {
                    degNumber -= 360;
                }
                if (degNumber == 360) {
                    degNumber = 0;
                }
                let closeFactor = 15;
                //find the closest multiple of closeFactor
                let closestMultiple = Math.round(degNumber / closeFactor) * closeFactor;
                console.log(closestMultiple);
                if (closestMultiple == 0 || closestMultiple == 360) {
                    zeroCount++;
                }
                //set the rotation to the closest multiple of closeFactor
                circle.style.transform = "rotate(" + closestMultiple + "deg)";
            }
            else {
                console.log("no match");
            }

        }
        console.log("zc=" + zeroCount);
        let percent = Math.round((zeroCount / circles.length) * 100);

        const percentageText = document.querySelector(".percentageText") as HTMLElement;
        percentageText.innerHTML = "Percentage: " + percent + "%";
        if (percent == 100) {

           // alert("You win!");
        }
    }

}