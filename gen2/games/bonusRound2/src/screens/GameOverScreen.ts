export class GameOverScreen {
    private element: HTMLElement;
    private questionData: HTMLElement;
    constructor() {
        this.element = document.getElementsByClassName("gameOverScreen")[0] as HTMLElement;
        this.questionData = this.element.querySelector(".questions") as HTMLElement;
    }
    public show() {
        this.element.classList.remove("hid");
    }   
    public hide() {
        this.element.classList.add("hid");
    }
    public setData(correctNumber:number,total:number)
    {
        if (this.questionData)
        {
            this.questionData.innerText="You got "+correctNumber+" out of "+total+" correct!";
        }
        else
        {
            console.log("question data not found");
        }
    }   
}