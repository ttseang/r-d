import { BonusButtons } from "../comps/bonusButtons";
import { Clock } from "../comps/clock";
import { BonusVo } from "../dataObjs/BonusVo";

export class BonusRoundScreen {
    private element: HTMLElement;
    private bonusButtons: BonusButtons;
    private gameData: BonusVo | undefined = undefined;

    private bonusInput: HTMLDivElement;
    private bonusInputAnswer: HTMLDivElement;
    private problemBox: HTMLDivElement;
    private questionIndex: number = 0;
    private answer: number = 0;
    private userAnswer: number = 0;
    private userAnswerString: string = "";
    private btnBackspace: HTMLButtonElement | undefined = undefined;
    private btnErase: HTMLButtonElement | undefined = undefined;
    private btnSkip: HTMLButtonElement | undefined = undefined;
    private btnGo: HTMLButtonElement | undefined = undefined;
    private btnSelectRound:HTMLButtonElement|undefined=undefined;
    private checkMark: HTMLDivElement | undefined = undefined;
    private xMark: HTMLDivElement | undefined = undefined;

    private indexBox: HTMLDivElement | undefined = undefined;
    
    public correctNumber: number = 0;
    public totalNumber: number = 0;
    private clickLock: boolean = false;

    private clock:Clock;

    public timeUpCallback:Function=()=>{};
    public outOfQuestionsCallback:Function=()=>{};

    constructor() {
        (window as any).bonusRoundScreen = this;
        //get by class
        this.element = document.getElementsByClassName("bonusGameScreen")[0] as HTMLElement;
        this.bonusButtons = new BonusButtons(document.getElementsByClassName("bonusbuttons")[0] as HTMLElement, this.buttonCallback.bind(this));

        this.bonusInput = document.getElementsByClassName("bonusInput")[0] as HTMLDivElement;
        this.bonusInputAnswer = this.bonusInput.querySelector(".answer") as HTMLDivElement;
        this.problemBox = document.getElementsByClassName("problemBox")[0] as HTMLDivElement;

        this.btnBackspace = document.getElementsByClassName("btnBack")[0] as HTMLButtonElement;
        this.btnErase = document.getElementsByClassName("btnClear")[0] as HTMLButtonElement;
        this.btnSkip = document.getElementsByClassName("btnSkip")[0] as HTMLButtonElement;
        this.btnSelectRound=document.getElementsByClassName("btnSelectRound")[0] as HTMLButtonElement;

        this.checkMark = document.getElementsByClassName("greenCheck")[0] as HTMLDivElement;
        this.xMark = document.getElementsByClassName("redX")[0] as HTMLDivElement;

        this.indexBox = document.getElementsByClassName("indexBox")[0] as HTMLDivElement;

        this.btnGo = document.getElementsByClassName("btnGo")[0] as HTMLButtonElement;

        if (this.btnGo) {
            this.btnGo.onclick = this.checkAnswer.bind(this);
        }

        if (this.btnBackspace) {
            this.btnBackspace.onclick = this.backspacePressed.bind(this);
        }
        if (this.btnErase) {
            this.btnErase.onclick = this.erasePressed.bind(this);
        }
        if (this.btnSkip) {
            this.btnSkip.onclick = this.skipPressed.bind(this);
        } 
        if (this.btnSelectRound)
        {
            this.btnSelectRound.onclick=()=>{
                //reload the page
                location.reload();
            }
        }
        this.clock=new Clock(document.getElementsByClassName("clock")[0] as HTMLElement,this.clockCallback.bind(this));
    }
    public show() {
        this.element.classList.remove("hid");
    }
    public hide() {
        this.element.classList.add("hid");
    }
    private buttonCallback(data: any) {
        console.log(data);
        if (this.bonusInput) {
            this.userAnswerString += data.toString();

            if (this.userAnswerString.length > 3) {

                this.userAnswerString = this.userAnswerString.substring(1);
            }

            //this is used to make a valid number -- no leading zeros
            this.userAnswer = parseFloat(this.userAnswerString);
            this.userAnswerString = this.userAnswer.toString();
            this.bonusInputAnswer.innerText = this.userAnswerString;
        }
    }
    public setBonusVo(bonusVo: BonusVo) {
        this.gameData = bonusVo;
        this.questionIndex = 0;
        this.totalNumber = this.gameData.probs.length;
        this.showQuestion();      

        this.clock.setTime(100);
        this.clock.start();
    }
    private showQuestion() {
        if (this.gameData) {
            let p1: string = this.gameData.probs[this.questionIndex].p1;
            let p2: string = this.gameData.probs[this.questionIndex].p2;
            let operator: string = this.gameData.operator;
            this.answer = parseFloat(this.gameData.probs[this.questionIndex].answer);
            let question: string = p1 + operator + p2;

            this.problemBox.innerText = question + "=";
        }

        if (this.indexBox) {
            this.indexBox.innerText = (this.questionIndex + 1).toString()+"/"+this.totalNumber.toString();
        }

    }
    backspacePressed() {
        if (this.clickLock) {
            return;
        }
        if (this.userAnswerString.length > 0) {
            this.userAnswerString = this.userAnswerString.substring(0, this.userAnswerString.length - 1);
            this.userAnswer = parseFloat(this.userAnswerString);
            this.bonusInputAnswer.innerText = this.userAnswerString;
        }
    }
    erasePressed() {
        if (this.clickLock) {
            return;
        }
        this.userAnswerString = "";
        this.userAnswer = 0;
        this.bonusInputAnswer.innerText = "";
    }
    skipPressed() {
        if (this.clickLock) {
            return;
        }
        this.nextQuestion();
    }
    checkAnswer() {
        if (this.userAnswerString.length === 0) {
            return;
        }
        if (this.clickLock) {
            return;
        }
        this.clickLock = true;
        if (this.userAnswer === this.answer) {
           // this.nextQuestion();
           //  alert("Correct!");
           this.correctNumber++;
              this.showCorrect();
        }
        else {
          //  alert("Wrong!");
            this.showWrong();
        }
    }
    showCorrect()
    {
        if (this.bonusInput)
        {
            this.bonusInput.classList.add("rightColor");
        }
        if (this.checkMark) {
            this.checkMark.classList.remove("hid");
        }
       
        
        //1.5 second delay
        setTimeout(this.nextQuestion.bind(this),1500);
    }
    showWrong()
    {
        if (this.bonusInput)
        {
            this.bonusInput.classList.add("wrongColor");
        }
        if (this.xMark) {
            this.xMark.classList.remove("hid");
        }
       
        //1.5 second delay
        setTimeout(this.nextQuestion.bind(this),1500);
    }
    nextQuestion() {
        this.questionIndex++;
        if (this.questionIndex < this.gameData!.probs.length) {

            this.showQuestion();
        }
        else {
           this.outOfQuestionsCallback();
        }
        //hide checkmark
        if (this.checkMark) {
            this.checkMark.classList.add("hid");
        }
        //hide xmark
        if (this.xMark) {
            this.xMark.classList.add("hid");
        }
        if (this.bonusInput)
        {
            this.bonusInput.classList.remove("rightColor");
            this.bonusInput.classList.remove("wrongColor");
        }
        //clear input
        this.userAnswer = 0;
        this.userAnswerString = "";
        if (this.bonusInput) {
            this.bonusInputAnswer.innerText = "";
        }
        this.clickLock = false;
    }

    private clockCallback() {
        alert("Time's Up!");
        this.timeUpCallback();
    }
}