import { BonusRound } from "../BonusRound";
import { BonusVo } from "../dataObjs/BonusVo";
import { BRLoader } from "../util/BRLoader";

export class SelectScreen {
    private element: HTMLElement;
    private files: string = "TT.M3.B01,TT.M3.B02,TT.M3.B03,TT.M3.B04,TT.M3.B05,TT.M3.B06,TT.M3.B07,TT.M3.B08,TT.M3.B09,TT.M3.B10,TT.M3.B11,TT.M3.B12,TT.M3.B13,TT.M3.B14,TT.M3.B15,TT.M3.B16,TT.M3.B17,TT.M3.B18,TT.M3.B19,TT.M3.B20,TT.M3.B21,TT.M3.B22,TT.M3.B23,TT.M4.B01,TT.M4.B02,TT.M4.B03,TT.M4.B04,TT.M4.B05,TT.M4.B06,TT.M4.B07,TT.M4.B08,TT.M4.B09,TT.M4.B10,TT.M4.B11,TT.M4.B12,TT.M4.B13,TT.M4.B14,TT.M4.B15,TT.M4.B16,TT.M4.B17,TT.M4.B18,TT.M4.B19,TT.M4.B20,TT.M4.B21,TT.M4.B22,TT.M4.B23,TT.M4.B24";
    private fileArray:string[]=[];
    private fileList:HTMLSelectElement;
    public callback:Function=()=>{};

    private btnSelect:HTMLElement;
    
    

    constructor() {
        this.element = document.getElementsByClassName("selectScreen")[0] as HTMLElement;
        this.fileList=document.getElementsByClassName("fileList")[0] as HTMLSelectElement;
        this.btnSelect=document.getElementsByClassName("btnSelect")[0] as HTMLElement;
       

        if (this.btnSelect)
        {
            this.btnSelect.onclick=this.buttonClick.bind(this);
        }
        this.fillList();
    }
    public show()
    {
        this.element.classList.remove("hid");
    }
    public hide()
    {
        this.element.classList.add("hid");
    }
    private buttonClick()
    {
        if (this.fileList)
        {           
           /*  this.callback(this.fileArray[this.fileList.selectedIndex]) */;
           this.loadRound(this.fileArray[this.fileList.selectedIndex]);
        }       
    }
    public loadRound(filename: string) {
        let brl: BRLoader = new BRLoader(this.roundLoaded.bind(this));
        brl.loadRound(filename);
    }
    private roundLoaded(bonusVo: BonusVo) {
        console.log(bonusVo);
        this.callback(bonusVo);
        this.hide();
     //   let bounusRound: BonusRound = new BonusRound(bonusVo);

    }
    private fillList() {
        console.log(this.fileList);

        if (this.fileList) {
            
            this.fileArray = this.files.split(",");
            console.log(this.fileArray);

            for (let i: number = 0; i < this.fileArray.length; i++) {

                let optEl:HTMLOptionElement=document.createElement("option");
                optEl.innerText=this.fileArray[i];
                this.fileList.append(optEl);
            }
        }

    }
}