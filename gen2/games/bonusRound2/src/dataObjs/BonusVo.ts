import { ProblemVo } from "./ProblemVo";

export class BonusVo {
    public lti: string;
    public time: number;
    public intro: number;
    public probs: ProblemVo[];
    public round: number;
    public course: string;
    public points: number[];
    public operator: string;
    public probCount: number;
    
    constructor(lti: string, time: number, intro: number, probs: ProblemVo[], round: number, course: string, points: number[], operator: string, probCount: number) {
        this.lti = lti;
        this.time = time;
        this.intro = intro;
        this.probs = probs;
        this.round = round;
        this.course = course;
        this.points = points;
        this.operator = operator;
        this.probCount = probCount;

    }
}