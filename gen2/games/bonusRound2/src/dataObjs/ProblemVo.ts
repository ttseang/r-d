export class ProblemVo {
    public p1: string;
    public p2: string;
    public answer: string;

    constructor(p1: string, p2: string, answer: string) {
        this.p1 = p1;
        this.p2 = p2;
        this.answer = answer;
    }
}