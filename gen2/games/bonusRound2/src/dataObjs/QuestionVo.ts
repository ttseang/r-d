export class QuestionVo {
    public q: string;
    public a: string;

    constructor(q: string, a: string) {
        this.q = q.toString().trim();
        this.a = a.toString().trim();
    }
}