import { BonusVo } from "./dataObjs/BonusVo";
import { BonusRoundScreen } from "./screens/BonusGameScreen";
import { GameOverScreen } from "./screens/GameOverScreen";
import { SelectScreen } from "./screens/SelectScreen";

export class BonusRound {
    private screenMode:number=0;
    private gameScreen:BonusRoundScreen;
    private selectScreen:SelectScreen;
    private gameOverScreen:GameOverScreen;

    constructor() {
        this.gameScreen=new BonusRoundScreen();
        this.gameScreen.hide();
        this.gameScreen.timeUpCallback=this.onTimeUp.bind(this);
        this.gameScreen.outOfQuestionsCallback=this.onOutOfQuestions.bind(this);

        this.selectScreen=new SelectScreen();
        this.selectScreen.callback=this.onRoundLoaded.bind(this);

        this.gameOverScreen=new GameOverScreen();
        
    }
    onTimeUp()
    {
        this.gameScreen.hide();
        this.gameOverScreen.setData(this.gameScreen.correctNumber,this.gameScreen.totalNumber);
        this.gameOverScreen.show();
    }
    onOutOfQuestions()
    {
        this.gameScreen.hide();
        this.gameOverScreen.setData(this.gameScreen.correctNumber,this.gameScreen.totalNumber);
        this.gameOverScreen.show();
    }
    onRoundLoaded(bonusVo:BonusVo)
    {
        this.gameScreen.setBonusVo(bonusVo);
        this.gameScreen.show();
    }

}