
import { ColorBurst } from "gameeffects";
import { GameObjects } from "phaser";
import { CompLayout, CompManager, ButtonController, CompLoader, ResponseImage, IComp, PosVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { GM } from "../classes/GM";
import { LetterBox } from "../classes/letterBox";
import { TargetBox } from "../classes/targetBox";
import { WordVo } from "../dataObjs/WordVo";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;

    private dragBox: LetterBox | null = null;
    private targetBoxes: TargetBox[] = [];
    private chars: string[] = ["A", "B", "C"];
    private level: number = -1;
    private levels: WordVo[] = [];
    private letterBoxes: LetterBox[] = [];
    private target1: TargetBox;
    private clickLock: boolean = false;

    public word: string;
    private mainImage: Phaser.GameObjects.Image;
    private mainText: Phaser.GameObjects.Text;
    private gm: GM = GM.getInstance();

    private chosenLetter: String;
    private targetPos: PosVo = new PosVo(4, 5);

    private stars: ColorBurst;

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {

        ColorBurst.preload(this, "./assets/effects/colorStars.png");

        this.load.image("holder", "./assets/holder.jpg");
        this.load.image("check", "./assets/check.png");
        this.load.image("qmark", "./assets/questionmark.png");
        this.load.image("right", "./assets/right.png");
        this.load.image("wrong", "./assets/wrong.png");
        this.load.image("triangle", "./assets/triangle.png");
        this.load.image("arrows", "./assets/arrows.png");

        this.load.image("box", "./assets/box.png");

        this.load.audio("rightSound", "./assets/audio/quiz_right.mp3");
        this.load.audio("wrongSound", "./assets/audio/quiz_wrong.wav");



        this.levels = this.gm.levels;

        for (let i: number = 0; i < this.levels.length; i++) {
            this.load.image(this.levels[i].word, "./assets/pics/" + this.levels[i].word + ".png");
        }
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.cm.regFontSize("list", 30, 1200);
        this.cm.regFontSize("words", 40, 1200);

        this.stars = new ColorBurst(this, this.gw / 2, this.gh / 2);


        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        this.buttonController.callback = this.doButtonAction.bind(this);
    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);
        this.buildGame();
    }
    buildGame() {

        this.target1 = this.addTargetBox(0x00ff00);
        this.grid.placeAt(this.targetPos.x, this.targetPos.y, this.target1);


        let fs: number = this.cm.getFontSize("words", this.gw);

        this.mainText = this.add.text(0, 0, "WORD", { "color": "#000000" }).setOrigin(0.5, 0.5);
        this.mainText.setFontSize(fs);

        this.grid.placeAt(7, 8, this.mainText);

        this.nextLevel();
    }

    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
        // this.grid.showPos();
        this.cm.doResize();

        Align.scaleToGameW(this.mainImage, 0.2, this);
        this.grid.placeAt(7, 5, this.mainImage);

        let fs: number = this.cm.getFontSize("words", this.gw);

        this.mainText.setFontSize(fs);

        this.grid.placeAt(7, 8, this.mainText);

        this.target1.doResize();
        this.grid.placeAt(4, 5, this.target1);

        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            let box: LetterBox = this.letterBoxes[i];
            box.doResize();
        }
    }
    nextLevel() {

        this.chosenLetter = "";
        this.cm.getComp("wrong").visible = false;
        this.cm.getComp("right").visible = false;

        this.level++;
        if (this.level > this.levels.length - 1) {

            this.scene.start("SceneOver");
            return;
        }
        this.chars = this.levels[this.level].chars;
        this.word = this.levels[this.level].word;
        if (this.mainImage) {
            this.mainImage.destroy();

        }
        this.mainText.setText(this.word);
        this.mainText.visible = false;

        this.mainImage = this.add.image(0, 0, this.word);

        Align.scaleToGameW(this.mainImage, 0.2, this);
        this.grid.placeAt(7, 5, this.mainImage);
        this.makeBoxes();
        this.clickLock = false;
    }
    hideBoxes() {
        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            this.letterBoxes[i].visible = false;
        }
    }
    reposBoxes() {
        for (let i: number = 0; i < this.letterBoxes.length; i++) {
            let box: LetterBox = this.letterBoxes[i];
            box.doResize();
            this.grid.placeAt(i + 3, 7, box);
            box.setPlace(new PosVo(i + 3, 7));
        }
    }
    makeBoxes() {
        this.hideBoxes();

        for (let i: number = 0; i < this.chars.length; i++) {
            let box: LetterBox;
            if (this.letterBoxes[i]) {
                box = this.letterBoxes[i];
                box.setLetter(this.chars[i]);
            }
            else {
                box = this.addLetter(this.chars[i]);
            }

            this.grid.placeAt(i + 3, 7, box);
            box.setPlace(new PosVo(i + 3, 7));
            box.visible = true;
        }


        this.target1.correct = this.word.substr(0, 1).toLowerCase();

        this.input.on("gameobjectdown", this.onDown.bind(this));
        this.input.on("pointermove", this.onMove.bind(this));
        this.input.on("pointerup", this.onUp.bind(this));
    }

    addLetter(letter: string) {
        let box: LetterBox = new LetterBox(this, letter);
        this.letterBoxes.push(box);
        return box;
    }
    addTargetBox(color: number) {
        let targetBox: TargetBox = new TargetBox(this, color);
        this.targetBoxes.push(targetBox);
        return targetBox;
    }
    onDown(p: Phaser.Input.Pointer, letterBox: LetterBox) {
        if (this.clickLock == true) {
            return;
        }
        if (letterBox instanceof (LetterBox) == false) {
            return;
        }
        this.dragBox = letterBox;
        this.dragBox.orginal = new PosVo(this.dragBox.x, this.dragBox.y);

        this.children.bringToTop(letterBox);


    }
    onMove(p: Phaser.Input.Pointer) {
        if (this.dragBox) {
            this.dragBox.x = p.x;
            this.dragBox.y = p.y;

        }
    }
    onUp() {
        let found: boolean = this.checkTargets();
        if (found == false) {
            this.dragBox.snapBack();
        }
        this.dragBox = null;

    }
    doWinEffect() {
        this.mainText.visible = true;
        this.stars.start();
        setTimeout(() => {
            this.nextLevel();
        }, 2000);
    }
    checkTargets() {

        if (this.dragBox === null) {
            return;
        }

        for (let i: number = 0; i < this.targetBoxes.length; i++) {
            let targetBox: TargetBox = this.targetBoxes[i];

            let distY: number = Math.abs(this.dragBox.y - targetBox.y);
            let distX: number = Math.abs(this.dragBox.x - targetBox.x);

            if (distX < this.dragBox.displayWidth * 0.4 && distY < this.dragBox.displayHeight * 0.4) {
                this.dragBox.x = targetBox.x;
                this.dragBox.y = targetBox.y;
                this.dragBox.setPlace(this.targetPos);

                this.chosenLetter = this.dragBox.letter;
                this.clickLock = true;
                return true;
            }
        }
        return false;
    }
    playSound(key: string) {
        let sound: Phaser.Sound.BaseSound = this.sound.add(key);
        sound.play();
    }
    checkCorrect() {

        if (this.chosenLetter == "") {
            return;
        }
        if (this.chosenLetter !== this.target1.correct) {

            this.cm.getComp("wrong").visible = true;
            this.cm.getComp("right").visible = false;
            this.playSound("wrongSound");
        }
        else {


            this.cm.getComp("wrong").visible = false;
            this.cm.getComp("right").visible = true;
            this.clickLock = true;
            this.playSound("rightSound");
            this.doWinEffect();
        }
    }
    doButtonAction(action: string, params: string) {
        if (action == "check") {
            this.checkCorrect();
        }
        if (action == "reset") {
            this.reposBoxes();
            this.dragBox = null;
            this.clickLock = false;
            this.cm.getComp("wrong").visible = false;
            this.cm.getComp("right").visible = false;
            this.chosenLetter = "";
        }
    }

}