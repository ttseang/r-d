import { GameObjects } from "phaser";
import { IBaseScene, Align } from "ttphasercomps";


export class TargetBox extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;

    private graphics: Phaser.GameObjects.Graphics;

    public correct: string = "";
    private myScale: number = 0.08;
    private color: number;

    private back: GameObjects.Image;
    constructor(bscene: IBaseScene, color: number) {
        super(bscene.getScene())
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.color = color;

        this.back = this.scene.add.image(0, 0, "box");
        Align.scaleToGameW(this.back, this.myScale, this.bscene);

        this.graphics = this.scene.add.graphics();
        this.drawBox();
        this.add(this.back);
        this.add(this.graphics);

        this.scene.add.existing(this);
    }
    drawBox() {
        this.graphics.clear();
        this.graphics.lineStyle(4, this.color);
        this.graphics.strokeRect(this.back.x - this.back.displayWidth / 2, this.back.y - this.back.displayHeight / 2, this.back.displayWidth, this.back.displayHeight);
    }
    doResize() {
        Align.scaleToGameW(this.back, this.myScale, this.bscene);
        this.drawBox();
    }
}