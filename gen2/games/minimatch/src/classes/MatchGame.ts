import { IconVo } from "../dataobjs/IconVo";
import { MatchVo } from "../dataobjs/MatchVo";
import { MatchModel } from "../mc/MatchModel";
import { MatchGrid } from "./MatchGrid";

export class MatchGame
{
    private ms:MatchModel= MatchModel.getInstance();
    private matchGrid:MatchGrid
    private level:number=0;
    constructor()
    {
        this.matchGrid = new MatchGrid(this.gridClicked.bind(this));
        this.loadIcons();
        this.listenToButtons();
    }
    private listenToButtons():void
    {
        const btnCheck:HTMLElement = document.querySelector(".btnCheck") as HTMLElement;
        btnCheck.onclick = this.checkMatches.bind(this);
        const btnReset:HTMLElement = document.querySelector(".btnReset") as HTMLElement;
        btnReset.onclick = this.reset.bind(this);
    }
    private loadIcons():void
    {
        // load icons from json file
        fetch("./assets/json/icons.json")
        .then(response => response.json())
        .then(data => this.process( data ));
    }
    private process(data:any):void
    {
        //console.log(data);
        let icons:IconVo[] = [];
        // process icons
        for (let i = 0; i < data.icons.length; i++)
        {
            let iconVo:IconVo= new IconVo(data.icons[i].id, data.icons[i].icon);
            icons.push(iconVo);
        }
        this.ms.icons = icons;
       // this.matchGrid.renderTest();
        this.loadLevel();
    }
    private loadLevel()
    {
        this.level++;
        if (this.level > 3)
        {
            alert("All levels complete!");
            return;
        }
        // load level from json file
        let fileName= "./assets/json/level"+this.level.toString()+".json";
        console.log(fileName);
        fetch(fileName)
        .then(response => response.json())
        .then(data => this.processLevel( data ));
    }
    private processLevel(data:any):void
    {
        this.ms.matches = [];
        this.ms.selected = [];

        //console.log(data);
        const matches:MatchVo[] = [];
        // process level
        for (let i = 0; i < data.matches.length; i++)
        {
            let match= data.matches[i];
            let matchVo:MatchVo= new MatchVo(match.m1, match.m2);
            matches.push(matchVo);
        }
        this.ms.matches = matches;
        this.ms.getGrid();
        this.matchGrid.render();
        this.makeSlots();
        this.reset();
    }
    private makeSlots():void
    {
        // make slots
        let slots:HTMLElement = document.querySelector(".slots") as HTMLElement;
        slots.innerHTML="";
        for (let i:number=0;i<9;i++)
        {
            let slot:HTMLElement = document.createElement("div");
            slot.className = "slot";
            slot.id = "slot" + i;
            slot.innerText="?";
            slots.appendChild(slot);

            if (i==1 || i==5)
            {
                let slot:HTMLElement = document.createElement("div");
                slot.className = "spacer";
             //   slot.innerText="|";
                slots.appendChild(slot);
            }
        }
        //take off last spacer
        if (slots.lastChild != null)
        {
            slots.removeChild(slots.lastChild);
        }
    }
    private gridClicked(id:number)
    {
        //console.log(id);
        if (this.ms.selected.includes(id))
        {
            return;
        }
        if (this.ms.selected.length >= 8)
        {
            return;
        }
        this.ms.selected.push(id);
        if (this.ms.selected.length == 8)
        {
            const btnCheck:HTMLElement = document.querySelector(".btnCheck") as HTMLElement;
            btnCheck.classList.remove("disabled");
        }
        this.renderSlots();
    }
    checkMatches():void
    {
        // check matches
        if (this.ms.selected.length != 8)
        {
            return;
        }
        let matches:number = 0;
        for (let i:number=0;i<4;i++)
        {
            let slotA:number= this.ms.selected[i*2];
            let slotB:number= this.ms.selected[i*2+1];
            let matchVo:MatchVo | null = this.ms.getMatchByID(slotA);
            if (matchVo)
            {
                if (matchVo.id1 == slotA && matchVo.id2 == slotB)
                {
                    matches++;
                }
                if (matchVo.id1 == slotB && matchVo.id2 == slotA)
                {
                    matches++;
                }
            }
          
            //console.log(slotA + " " + slotB);
        }
        //console.log("matches: " + matches);
        if (matches == 4)
        {
            alert("Correct! Get Ready For The Next Level");
            this.loadLevel();
        }
        else
        {
            this.showMessage("You have " + matches + " matches out of 4 correct");
        }
    }
    reset():void
    {
        // reset
        for (let i:number=0;i<8;i++)
        {
            let slot:HTMLElement = document.querySelector("#slot" + i) as HTMLElement;
            slot.innerHTML = "";
        }
        this.ms.selected = [];
        const btnCheck:HTMLElement = document.querySelector(".btnCheck") as HTMLElement;
        btnCheck.classList.add("disabled");
        //loop through grid and remove selected
        const children:NodeListOf<HTMLElement> = document.querySelectorAll(".match-icon");
        for (let i:number=0;i<children.length;i++)
        {
            children[i].classList.remove("selected");
        }
        this.showMessage("Find the 4 matches");
    }
    showMessage(msg:string):void
    {
        // show message
        const msgElement:HTMLElement = document.querySelector(".messageHolder") as HTMLElement;
        msgElement.innerText = msg;
        
        
    }
    renderSlots():void
    {
        //clear slots
        for (let i:number=0;i<8;i++)
        {
            let slot:HTMLElement = document.querySelector("#slot" + i) as HTMLElement;
            slot.innerHTML = "";
        }
        for (let i:number=0;i<this.ms.selected.length;i++)
        {
            let slot:HTMLElement = document.querySelector("#slot" + i) as HTMLElement;
            let iconVo:IconVo | null = this.ms.getIconByID(this.ms.selected[i]);
            if (iconVo)
            {
                let icon:string = iconVo.icon;
                let path: string = "./assets/icons/" + icon;
                slot.innerHTML = `<img src="${path}" alt="">`;
            }
           
        }
    }
}