import { IconVo } from "../dataobjs/IconVo";
import { MatchModel } from "../mc/MatchModel";

export class MatchGrid {
    private el: HTMLElement | null;;

    private ms: MatchModel = MatchModel.getInstance();
    private callback: Function;
    constructor(callback: Function) {
        this.callback = callback;
        this.el = document.querySelector(".match-grid") as HTMLElement;

    }
    private clickHandler(e: Event): void {
        //console.log(e);
        //add hid class to target
        let target: HTMLElement = e.target as HTMLElement;
        if (target.classList.contains("selected")) {
            return;
        }
        target.classList.add("selected");
        //get the data-id of the target
        let id: string | undefined = target.dataset.id;
        //call the callback function
        if (id != undefined) {
            this.callback(id);
        }
    }
    public render(): void {
        if (this.el != null) {
            this.el.innerHTML = "";
            for (let i = 0; i < this.ms.iconIDs.length; i++) {
                let id: number = this.ms.iconIDs[i];
                //  //console.log("id: " + id);
                let iconVo: IconVo | null = this.ms.getIconByID(id);
                if (iconVo) {


                    //console.log(iconVo);
                    let icon: string = iconVo.icon;
                    let path: string = "./assets/icons/" + icon;
                    //  //console.log(path);
                    let imageEl: HTMLImageElement = document.createElement("img");
                    imageEl.src = path;
                    imageEl.alt = "";


                    let block: HTMLDivElement = document.createElement("div");
                    block.className = "match-icon";
                    block.dataset.id = this.ms.iconIDs[i].toString();
                    block.appendChild(imageEl);
                    block.onclick = this.clickHandler.bind(this);

                    if (this.el != null) {
                        this.el.append(block);
                    }
                }
                else {
                    //console.log("icon not found " + id);
                }
            }
        }
    }
    public renderTest(): void {
        let html: string = "";
        for (let i = 0; i < this.ms.icons.length; i++) {
            let path: string = "./assets/icons/" + this.ms.icons[i].icon;
            html += `<div class="match-icon"><img src="${path}" alt="">${i}</div>`;
        }
        if (this.el != null) {
            this.el.innerHTML = html;
        }
    }
}