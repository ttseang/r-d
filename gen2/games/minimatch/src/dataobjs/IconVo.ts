export class IconVo
{
    public id:number;
    public icon:string;
    constructor(id:number, icon:string)
    {
        this.id = id;
        this.icon = icon;

    }
}