import { IconVo } from "../dataobjs/IconVo";
import { MatchVo } from "../dataobjs/MatchVo";

export class MatchModel
{
    //make it a singleton
    private static _instance:MatchModel;
    public icons:IconVo[] = [];
    public matches:MatchVo[]= [];
    public iconIDs:number[] = [];

    public selected:number[] = [];
    public selectedMatches:MatchVo[] = [];

    constructor()
    {
        (window as any).matchModel = this;
    }
    public static getInstance():MatchModel
    {
        if (MatchModel._instance == null)
        {
            MatchModel._instance = new MatchModel();
        }
        return MatchModel._instance;
    }
    public getMatchByID(id:number):MatchVo | null
    {
        let match:MatchVo | null = null;
        for (let i = 0; i < this.matches.length; i++)
        {
            if (this.matches[i].id1 == id || this.matches[i].id2 == id)
            {
                match = this.matches[i];
            }
        }
        return match;
    }
    public getGrid()
    {
        this.selected = [];
        this.selectedMatches = [];
        this.iconIDs = [];

        let matches:MatchVo[] = this.pickRandomMatches();
        this.selectedMatches=matches.slice();

        let ids:number[] = [];
        for (let i = 0; i < matches.length; i++)
        {
            ids.push(matches[i].id1);
            ids.push(matches[i].id2);
        }
        //add 8 random icon ids
       while (ids.length < 20)
       {
           let id:number = this.getRandIconID();
           if (ids.indexOf(id) == -1 && !this.isInMatches(id))
           {
               ids.push(id);
           }
       }
       this.iconIDs = ids;
         //shuffle the ids
             for (let i = ids.length - 1; i > 0; i--)
            {
                const j = Math.floor(Math.random() * (i + 1));
                [ids[i], ids[j]] = [ids[j], ids[i]];
            }   

    }
    private pickRandomMatches():MatchVo[]
    {
        let matches:MatchVo[] = [];
        for (let i = 0; i < 4; i++)
        {
            let match:MatchVo = this.matches[Math.floor(Math.random() * this.matches.length)];
            if (!this.checkAlreadyInMatches(matches, match))
            {
                matches.push(match);
            }
            else
            {
                i--;
            }
        }
        return matches;
    }
    private checkAlreadyInMatches(matches:MatchVo[],match:MatchVo):boolean
    {
        let isIn:boolean = false;
        for (let i = 0; i < matches.length; i++)
        {
            if (matches[i].id1 == match.id1 || matches[i].id1 == match.id2 || matches[i].id2 == match.id1 || matches[i].id2 == match.id2)
            {
                isIn = true;
            }
        }
        return isIn;
    }
    public getIconByID(id:number):IconVo | null
    {
        let icon:IconVo | null = null;
        for (let i = 0; i < this.icons.length; i++)
        {
            if (this.icons[i].id == id)
            {
                icon = this.icons[i];
            }
        }
        return icon;
    }
    
    private isInMatches(id:number):boolean
    {
        let isIn:boolean = false;
        for (let i = 0; i < this.matches.length; i++)
        {
            if (this.matches[i].id1 == id || this.matches[i].id2 == id)
            {
                isIn = true;
            }
        }
        return isIn;
    }
    private getRandIconID():number
    {
        let len:number = this.icons.length;
        let rand:number = Math.floor(Math.random() * len);
        let id:number = this.icons[rand].id;
        return id;
    }
    public getPath(id:number):string
    {
        let path:string = "";
        for (let i = 0; i < this.icons.length; i++)
        {
            if (this.icons[i].id == id)
            {
                path = "./assets/icons/"+this.icons[i].icon;
            }
        }
        return path;
    }
}