import { GameObjects } from "phaser";
import { IBaseScene, PosVo, Align, CompManager } from "ttphasercomps";


export class LetterBox extends GameObjects.Container
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;

    private back:GameObjects.Image;
    private textBox:GameObjects.Text;
    public orginal:PosVo=new PosVo(0,0);
    public startPos:PosVo=new PosVo(0,0);

    public letter:string="";

    private myScale:number=0.08;
    private graphics:GameObjects.Graphics;
    private cm:CompManager=CompManager.getInstance();
    public place:PosVo=new PosVo(0,0);

    constructor(bscene:IBaseScene,letter:string)
    {
        super(bscene.getScene())
        this.bscene=bscene;
        this.scene=bscene.getScene();
        
        this.letter=letter;
        this.back=this.scene.add.image(0,0,"box");
        Align.scaleToGameW(this.back,this.myScale,this.bscene);

        this.textBox=this.scene.add.text(0,0,letter,{fontSize:"100px",color:"#000000"}).setOrigin(0.5,0.5);
      /*   this.textBox=this.scene.add.bitmapText(0,0,"aff",letter).setOrigin(0.5,0.5);
        this.textBox.fontSize=120;
        this.textBox.setMaxWidth(this.back.displayWidth*0.7); */

        this.graphics=this.scene.add.graphics();
        this.drawBox();        
        this.add(this.back);
        this.add(this.textBox);
        this.add(this.graphics);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.setInteractive();
        this.scene.add.existing(this);        
        
        this.doResize();
    }
    drawBox()
    {
        this.graphics.clear();
        this.graphics.lineStyle(2,0x00000);
        this.graphics.strokeRect(this.back.x-this.back.displayWidth/2,this.back.y-this.back.displayHeight/2,this.back.displayWidth,this.back.displayHeight);
       
    }
    setStartPlace()
    {
        this.startPos=this.place;
    }
    doResize()
    {
        let fs:number=this.cm.getFontSize("letters",this.bscene.getW());
        this.textBox.setFontSize(fs);        

        Align.scaleToGameW(this.back,this.myScale,this.bscene);
        this.drawBox();

        this.bscene.getGrid().place(this.place,this);
    }
    setLetter(char:string)
    {
        this.letter=char;
        this.textBox.setText(char);
    }
    public resetStart()
    {
        this.bscene.getGrid().placeAt(this.startPos.x,this.startPos.y,this);
    }
    public resetPlace()
    {
        this.bscene.getGrid().placeAt(this.place.x,this.place.y,this);
    }
    public setPlace(posVo:PosVo)
    {
        this.place=posVo;        
    }
    public snapBack()
    {
        this.scene.tweens.add({targets: this,duration: 500,y:this.orginal.y,x:this.orginal.x});
    }
}