import { WordVo } from "../dataObjs/WordVo";
import { WordVo2 } from "../dataObjs/WordVo2";

let instance:GM=null;

export class GM
{
    public isMobile:boolean=false;
    public isPort:boolean=false;
    public isTablet:boolean=false;
   // public levels:WordVo[]=[];
    public levels2:WordVo2[]=[];

    constructor()
    {
        window['gm']=this;
    }
    static getInstance()
    {
        if (instance===null)
        {
            instance=new GM();
        }
        return instance;
    }
}