
import { CompLayout, CompManager, ButtonController, CompLoader, PosVo } from "ttphasercomps";
import { Align } from "ttphasercomps/util/align";
import { Box } from "../classes/Box";
import { Coin } from "../classes/Coin";
import { CoinStack } from "../classes/CoinStack";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    private compLayout: CompLayout;
    private cm: CompManager = CompManager.getInstance();
    private buttonController: ButtonController = ButtonController.getInstance();
    private compLoader: CompLoader;
    //
    //
    //

    private selectedCoin: Coin | null = null;
    private boxes: Box[] = [];
    private clickLock: boolean = false;
    private numberOfCoins: number = 30;
    private tubes: Phaser.GameObjects.Image;
    public stacks: CoinStack[] = [];

    private coins: Coin[] = [];

    constructor() {
        super("SceneMain");
        this.compLoader = new CompLoader(this.compsLoaded.bind(this));
    }
    preload() {

        this.load.setPath("./assets/");

        this.load.image("holder", "holder.jpg");
        this.load.image("check", "check.png");
        this.load.image("qmark", "questionmark.png");
        this.load.image("right", "right.png");
        this.load.image("wrong", "wrong.png");
        this.load.image("triangle", "triangle.png");


        this.load.image("button", "ui/buttons/1/1.png");
        this.load.image("penny", "penny.png");
        this.load.image("nickle", "nickle.png");
        this.load.image("dime", "dime.png");
        this.load.image("quarter", "quarter.png");
        this.load.image("box", "box.png");
        this.load.image("bg", "bg.jpg");
        this.load.image("tubes", "tubes.jpg");
        this.load.image("flatsilver", "flat_silver.png");
        this.load.image("flatpenny", "flat_penny.png");

        this.load.image("face", "face.png");

        this.load.audio("coin", "quiz_right.wav");
        this.load.audio("wrong", "quiz_wrong.wav");
        this.load.audio("select", "click.wav");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        this.cm.regFontSize("list", 30, 1200);
        this.cm.regFontSize("words", 40, 1200);

        this.compLayout = new CompLayout(this);
        this.compLoader.loadComps("./assets/layout.json");

        this.buttonController.callback = this.doButtonAction.bind(this);

        (window as any).scene = this;
    }
    compsLoaded() {
        this.compLayout.loadPage(this.cm.startPage);
        window.onresize = this.doResize.bind(this);
        this.makeChildren();
        //  this.loadGameData();
    }
    loadGameData() {
        /* fetch("./assets/words.json")
        .then(response => response.json())
        .then(data => this.gotGameData({ data })); */
    }
    gotGameData(data: any) {

    }
    makeChildren() {

        //    this.grid.showPos();

        /*    let bg: Phaser.GameObjects.Image = this.add.image(0, 0, "bg");
           bg.displayWidth = this.gw;
           bg.scaleY = bg.scaleX;
   
           if (this.gw < this.gh) {
               bg.displayHeight = this.gh;
               bg.scaleX = bg.scaleY;
           }
           Align.center(bg, this); */
        //
        //
        //
        this.tubes = this.add.image(0, 0, "tubes").setOrigin(0, 0);
        Align.scaleToGameW(this.tubes, 0.30, this);
        this.grid.placeAt(1, 1.5, this.tubes);
        //
        //
        //
        this.input.once("gameobjectdown", this.selectCoin.bind(this));

        this.makeBoxes();
        this.makeCoins();
    }
    makeBoxes() {
        let coinVals: string[] = ["1¢", "5¢", "10¢", "25¢"];
        coinVals.reverse();

        let stackScales: number[] = [0.055, 0.04, 0.045, 0.04];

        let keys: string[] = ["flatsilver", "flatsilver", "flatsilver", "flatpenny"];
        //
        //
        //
        for (let i: number = 0; i < 4; i++) {
            let box: Box = new Box(this, coinVals[i], 3 - i);
            this.boxes.push(box);
            Align.scaleToGameW(box,.05,this);

            box.place = new PosVo(1.5 + i * 0.78, 2.5);
            box.updatePos();
            //this.grid.placeAt(1.5 + i * 0.92, 3, box);
             box.alpha=.01;

            let coinStack: CoinStack = new CoinStack(this, keys[i], stackScales[i]);

            // this.grid.placeAt(1.5 + i * 0.92, 6.5, coinStack);
            coinStack.place = new PosVo(1.5 + i * 0.78, 6.4);
            coinStack.updatePos();
            this.stacks.push(coinStack);
        }
    }
    makeCoins() {
        let coinKeys: string[] = ["penny", "nickle", "dime", "quarter"];
        let coinScales: number[] = [0.05, 0.056, 0.047, 0.063];

        for (let i: number = 0; i < this.numberOfCoins; i++) {
            let index: number = Phaser.Math.Between(0, 3);
            let xx: number = (Math.random() * 3) + 6
            let yy: number = (Math.random() * 5) + 3;
            let coin: Coin = new Coin(this, coinKeys[index], index, xx, yy,coinScales[index]);
            this.coins.push(coin);
            coin.setInteractive();
         //   Align.scaleToGameW(coin, coinScales[index], this);


            this.grid.placeAt(xx, yy, coin);
        }
    }
    findNearestBox() {
        for (let i: number = 0; i < this.boxes.length; i++) {
            let box: Box = this.boxes[i];
            let boxEndX = box.x + box.displayWidth / 2;
            let boxEndY = box.y + box.displayHeight / 2;
            let boxStartX = box.x - box.displayWidth / 2;
            let boxStartY = box.y - box.displayHeight / 2;
            //
            //
            //
            if (this.selectedCoin.x > boxStartX && this.selectedCoin.x < boxEndX) {
                if (this.selectedCoin.y > boxStartY && this.selectedCoin.y < boxEndY) {
                    return box;
                }
            }
        }
        return null;
    }
    playSound(key: string) {
        let sound: Phaser.Sound.BaseSound = this.sound.add(key);
        sound.play();
    }
    selectCoin(pointer: Phaser.Input.Pointer, coin: Coin) {
        if (this.clickLock === true) {
            return;
        }
        this.playSound("select");
        //coin.alpha=0.5;
        this.selectedCoin = coin;
        this.children.bringToTop(this.selectedCoin);
        this.input.on("pointermove", this.dragCoin.bind(this));
        this.input.once("pointerup", this.dropCoin.bind(this));
    }
    dropCoin() {
        // this.selectedCoin.updatePos();

        let dropBox = this.findNearestBox();
      //  console.log(dropBox);

        if (dropBox) {
            //dropBox.alpha=.5;
            /* console.log(dropBox.index);
          //  console.log(this.selectedCoin.index); */


            if (dropBox.index !== this.selectedCoin.index) {
                this.playSound("wrong");
                this.selectedCoin.replace();
            }
            else {
                this.playSound("coin");
                this.selectedCoin.destroy();
                this.stacks[3 - dropBox.index].addCoin();
            }
        }
        else
        {
            this.selectedCoin.updateOnDrop();
        }

        this.selectedCoin = null;
        this.input.off("pointermove", this.dragCoin.bind(this));
        this.input.once("gameobjectdown", this.selectCoin.bind(this));

    }
    dragCoin(pointer: Phaser.Input.Pointer) {
        if (this.selectedCoin) {
            this.selectedCoin.x = pointer.x;
            this.selectedCoin.y = pointer.y;
        }
    }
    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
      //  this.grid.showPos();
        this.cm.doResize();
        //
        //
        //
        Align.scaleToGameW(this.tubes, 0.30, this);
        this.grid.placeAt(1, 1.5, this.tubes);

        for (let i: number = 0; i < this.boxes.length; i++) {
            this.stacks[i].updatePos();
            this.boxes[i].updatePos();
        }

        for (let i: number = 0; i < this.coins.length; i++) {
          //  console.log(this.coins[i]);

            if (this.coins[i]) {
                this.coins[i].updatePos();
            }
        }

    }
    doButtonAction(action: string, params: string) {

    }

}