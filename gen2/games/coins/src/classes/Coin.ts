import { GameObjects } from "phaser";
import { IBaseScene, PosVo, Align } from "../../../../common/mods/ttcomps";

import { GM } from "./GM";

export class Coin extends GameObjects.Sprite {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;

    public xx: number = 0;
    public yy: number = 0;


    private gm: GM = GM.getInstance();
    public index: number = 0;
    public startPos: PosVo;
    public place: PosVo;
    private cscale:number=0;

    constructor(bscene: IBaseScene, key: string, index: number, xx: number, yy: number,cscale:number) {
        super(bscene.getScene(), 0, 0, key);
        this.bscene = bscene;
        this.type = "coin";
        this.index = index;
        this.cscale=cscale;

        this.scene = bscene.getScene();

        this.scene.add.existing(this);
        this.xx = xx;
        this.yy = yy;
        bscene.getGrid().placeAt(xx, yy, this);
        //this.grid=bscene.getGrid();
        this.startPos = new PosVo(this.x, this.y);
        this.place = new PosVo(xx, yy);

        this.angle = Math.floor(Math.random() * 360);

        Align.scaleToGameW(this, cscale, this.bscene);
    }
    replace() {
        this.scene.tweens.add({ targets: this, duration: 250, y: this.startPos.y, x: this.startPos.x });
    }
    shrink() {
        this.scene.tweens.add({ targets: this, duration: 250, scaleX: 0, scaleY: 0, onComplete: this.shrinkDone.bind(this) });
    }
    drop() {
        this.scene.tweens.add({ targets: this, duration: 250, y: this.y + this.bscene.getW() / 4, onComplete: this.shrinkDone.bind(this) });
    }
    shrinkDone() {
        this.destroy();
    }
    updatePos() {
        Align.scaleToGameW(this, this.cscale, this.bscene);
        if (this.place) {
            this.bscene.getGrid().placeAt(this.place.x, this.place.y, this);
        }
    }
    updateOnDrop() {
        let posVo: PosVo = this.bscene.getGrid().findNearestGridXY(this.x, this.y);
        // console.log(posVo);
        //this.place=posVo;
        this.startPos.x = posVo.x;
        this.startPos.y = posVo.y;
        //  this.pos.posX=posVo.x;
        //  this.pos.posY=posVo.y;

    }
    /*
    update()
    {
        let rpos:ResizeVo=this.layout.getDefinition(this.gm.device,this.gm.orientation);
        Align.scaleToGameW(this,rpos.scale,this.bscene);

 //       this.updatePos();
        //this.pos=this.layout.getDefinition(this.gm.device,this.gm.orientation);
        this.grid=this.bscene.getGrid();
        this.grid.placeAt(this.pos.posX,this.pos.posY,this);

        this.startPos=new PosVo(this.x,this.y);
    } */
}