import { GameObjects } from "phaser";
import { IBaseScene, Align, PosVo } from "../../../../common/mods/ttcomps";


export class CoinStack extends Phaser.GameObjects.Container
{
    private stackSize:number=0;
    private coins:Phaser.GameObjects.Image[]=[];
    private key:string;
    private bscene:IBaseScene;
    private targetY:number=0;
    private coinScale:number=0;
    public place: PosVo;
    
    constructor(bscene:IBaseScene,key:string,coinScale:number)
    {
        super(bscene.getScene());
        this.key=key;
        this.bscene=bscene;
        this.coinScale=coinScale;
       /*  let face:GameObjects.Image=this.scene.add.image(0,0,"face");
        this.add(face); */
        
        this.scene.add.existing(this);
    }

   
    addCoin()
    {
        let coin:Phaser.GameObjects.Image=this.scene.add.image(0,0,this.key).setOrigin(0.5,0.5);
        Align.scaleToGameW(coin,this.coinScale,this.bscene);
        this.coins.push(coin);
        this.add(coin);
        this.lineUpCoins();
    }
    lineUpCoins()
    {
        let yy:number=0;
        let coin:Phaser.GameObjects.Image;

        for (let i:number=0;i<this.coins.length;i++)
        {
            coin=this.coins[i];
            yy-=coin.displayHeight;
            coin.y=yy;
            
        }
        
        this.targetY=yy;
        coin.y=-this.bscene.getH()/5;
        this.scene.add.tween({targets:coin,duration:500,y:this.targetY});
    }
    updatePos() {
        if (this.place) {
            this.bscene.getGrid().placeAt(this.place.x, this.place.y, this);
        }
        for (let i:number=0;i<this.coins.length;i++)
        {
            Align.scaleToGameW(this.coins[i],this.coinScale,this.bscene);
        }
    }
}