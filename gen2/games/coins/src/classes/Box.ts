import { GameObjects } from "phaser"
import { Align, IBaseScene, PosVo } from "../../../../common/mods/ttcomps";


export class Box extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    public index: number;
    private back: Phaser.GameObjects.Image;
    public place: PosVo;
    private bscene: IBaseScene;

    constructor(bscene: IBaseScene, text: string, index: number) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.index = index;
        //
        //
        //       
        this.back = this.scene.add.image(0, 0, "box");
        this.add(this.back);
        //
        //
        //
        let text1: Phaser.GameObjects.Text = this.scene.add.text(0, 0, text, { fontSize: "26px" }).setOrigin(0.5, 0.5);
        this.add(text1);
        //
        //
        //
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        this.scene.add.existing(this);
    }
    updatePos() {
        Align.scaleToGameW(this,.05,this.bscene);
        if (this.place) {
            this.bscene.getGrid().placeAt(this.place.x, this.place.y, this);
        }
    }
}