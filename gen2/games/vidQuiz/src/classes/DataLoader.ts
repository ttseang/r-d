import { QuizVo } from "../dataObjs/QuizVo";
import { QuizModel } from "./QuizModel";

export class DataLoader
{
    private callback:Function;
    private qm:QuizModel=QuizModel.getInstance();

    constructor(callback:Function)
    {
        this.callback=callback;
        this.qm=QuizModel.getInstance();
    }
    load()
    {
        fetch("../assets/quizData.json")
        .then(response => response.json())
        .then(data => this.process(data));
    }
    process(data:any)
    {
        console.log(data);

        let quizData:QuizVo[]=[];
        for(let i=0;i<data.quizData.length;i++)
        {
            let quizVo:QuizVo=new QuizVo("",[],"");
            quizVo.question=data.quizData[i].q;
            quizVo.correctAnswer=data.quizData[i].a;
            quizVo.answers=data.quizData[i].answers;            
            quizData.push(quizVo);
        }
        this.qm.quizData=quizData;
        this.callback();
    }
}