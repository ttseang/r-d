import { QuizVo } from "../dataObjs/QuizVo";

export class QuizModel
{
    //singleton
    private static instance:QuizModel;
    public quizData:QuizVo[]=[];
    
    public static getInstance():QuizModel
    {
        if(!QuizModel.instance)
        {
            QuizModel.instance = new QuizModel();
        }
        return QuizModel.instance;
    }
}