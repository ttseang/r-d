export class Player1
{
    public el:HTMLVideoElement;
    private totalTimePlayed:number = 0;
    private lastTime:number = 0;
    private isPlaying:boolean = false;
    private isPaused:boolean = false;
    private isStopped:boolean = true;

    private videoDuration:number = 0;
    private timer1:NodeJS.Timer;
    private callback:Function;

    constructor(el:HTMLVideoElement,callback:Function)
    {
        (window as any).player = this;
        this.el = el;
        this.callback=callback;

        this.videoDuration = this.el.duration;
        
        //set up an event listener to measure the play time of the video
        this.el.addEventListener("timeupdate", this.onTimeUpdate.bind(this));

        //on play, set the isPlaying flag to true
        this.el.addEventListener("play", this.onPlay.bind(this));
        //on pause, set the isPlaying flag to false
        this.el.addEventListener("pause", this.onPause.bind(this));
        
        //set up a timer to check the play time of the video and add a var to clear the timer when the video ends
        
        this.timer1 = setInterval(this.doTick.bind(this), 1000);
    }
    onPlay(e:Event)
    {
        this.isPlaying = true;
    }
    onPause(e:Event)
    {
        this.isPlaying = false;
    }
    doTick()
    {
        if(this.isPlaying)
        {
            this.totalTimePlayed++;
        }
    }
    onTimeUpdate(e:Event)
    {
       
        //if the video has ended, stop the video
        if(this.el.ended)
        {
           
            console.log(this.totalTimePlayed);
            console.log(this.el.duration);
            clearInterval(this.timer1);
        
            let verify:boolean=(this.el.duration-this.totalTimePlayed)<2;
            console.log(verify);

            if (!verify)
            {
                alert("You have not watched the entire video");
            }
            this.callback();
        }
    }
}