import { DataLoader } from "./DataLoader";
import { Player1 } from "./Player1";
import { Quiz } from "./Quiz";
import { QuizModel } from "./QuizModel";

export class VideoQuiz
{
    private player:Player1 | null=null;
    private videoElement:HTMLVideoElement | null=null;
    private videoContainer:HTMLElement | null=null;
    private quizContainer:HTMLElement | null=null;
    private quiz:Quiz | null=null;
    private qm:QuizModel = QuizModel.getInstance();
    
   
    private btnTakeQuiz:HTMLElement | null=null;

    constructor()
    {
        
        this.btnTakeQuiz=document.querySelector(".btnTakeQuiz") as HTMLElement;
        const dl:DataLoader=new DataLoader(this.start.bind(this));
        dl.load();
    }
    start()
    {
        console.log(this.qm.quizData);
         this.quizContainer= document.querySelector(".quizContainer") as HTMLElement;
        this.quiz=new Quiz(this.quizContainer,this.showVideo.bind(this));

        //set up video player
        this.videoContainer= document.querySelector(".playerContainer") as HTMLElement;
        this.videoElement= document.querySelector(".player1") as HTMLVideoElement;
        this.player = new Player1(this.videoElement,this.onVideoEnded.bind(this));
        //this.player.el.play();
       // this.onVideoEnded();
      //  this.getQuiz();
    }
    showVideo()
    {
        alert("show video");
        if (this.videoElement)
        {
            this.videoElement.classList.add(".hid");
        }
        if (this.quiz)
        {
            this.quiz.hide();
        }
    }
    onVideoEnded()
    {
        //turn on the quiz button   
        if (this.btnTakeQuiz)
        {
            this.btnTakeQuiz.classList.remove("hid");
            this.btnTakeQuiz.addEventListener("click",()=>{
                this.getQuiz();
            });
        }
           
    }
    getQuiz()
    {
        //remove the button listener
        if (this.btnTakeQuiz)
        {
            this.btnTakeQuiz.removeEventListener("click",()=>{
                this.getQuiz();
            });
        }
        if (this.videoElement)
        {
            this.videoElement.classList.remove(".hid");
        }
        if (this.quiz)
        {
            this.quiz.restartQuiz();
            this.quiz.show();
        }    
    }
}