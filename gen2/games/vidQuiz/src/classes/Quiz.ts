import { QuizVo } from "../dataObjs/QuizVo";
import { QuizModel } from "./QuizModel";

export class Quiz {
    private container: HTMLElement;
    private answersElement: HTMLElement;
    private answers: NodeListOf<HTMLElement>;
    private questions: QuizVo[] = [];
    private currentQuestion: QuizVo | null = null;
    private currentQuestionIndex: number = -1;
    private selectedAnswer: string | null = null;

    private btnCheck: HTMLElement;
    private btnRewatch: HTMLElement;

    private qm: QuizModel = QuizModel.getInstance();

    constructor(container: HTMLElement,videoCallback:Function) {
        this.container = container;
        //hide the video container
        this.container.style.display = "none";

        this.btnCheck = this.container.querySelector(".btnCheck") as HTMLElement;
        this.btnCheck.addEventListener("click", this.checkAnswer.bind(this));

        this.btnRewatch = this.container.querySelector(".rewatch") as HTMLElement;
        if (this.btnRewatch)
        {
            this.btnRewatch.addEventListener("click", ()=>{videoCallback();});
        }
      

        this.answersElement = this.container.querySelector(".answers") as HTMLElement;

        this.answers = this.answersElement.querySelectorAll(".answer");
        for (let i = 0; i < this.answers.length; i++) {
            this.answers[i].addEventListener("click", this.onSelectedAnswer.bind(this));
        }
        this.questions=this.qm.quizData;
        

        //set the first question
        this.nextQuestion();

        (window as any).quiz = this;
    }
    restartQuiz() {
        this.currentQuestionIndex = -1;
        this.nextQuestion();
    }

    onSelectedAnswer(e: Event) {

        this.btnCheck.classList.remove("nopress");

        let el: HTMLElement = e.currentTarget as HTMLElement;
        let answer: string = el.innerHTML;
        console.log("selected answer: " + answer);

        //remove selected class from all answers
        for (let i = 0; i < this.answers.length; i++) {
            this.answers[i].classList.remove("selected");
            this.answers[i].classList.add("unselected");
        }
        //add selected class to the selected answer
        el.classList.add("selected");
        el.classList.remove("unselected");
        console.log(el);
        this.selectedAnswer = answer;
    }
    show() {
        this.container.style.display = "block";
    }
    hide() {
        this.container.style.display = "none";
    }
    nextQuestion() {
        this.reset();
        this.currentQuestionIndex++;
        if (this.currentQuestionIndex >= this.questions.length) {
            this.currentQuestionIndex = 0;
            alert("Quiz is finished");
            return;
        }
        //get the h2 element from .question and set the question index
        let questionIndexElement: HTMLElement = this.container.querySelector(".question h3") as HTMLElement;
        questionIndexElement.innerHTML = "Question " + (this.currentQuestionIndex + 1) + " of " + this.questions.length;
        this.setQuestion(this.questions[this.currentQuestionIndex]);
    }
    setQuestion(questionVo: QuizVo) {
        this.currentQuestion = questionVo;
        let question: HTMLElement = this.container.querySelector(".question p") as HTMLElement;
        question.innerHTML = questionVo.question;
        let answers: string[] = questionVo.getAnswers();
        for (let i = 0; i < this.answers.length; i++) {
            this.answers[i].innerHTML = answers[i];
        }
    }
    reset() {
        //loop through the answers and remove .correct and .incorrect classes
        for (let i = 0; i < this.answers.length; i++) {
            this.answers[i].classList.remove("badAnswer");
            this.answers[i].classList.remove("correct");
            this.answers[i].classList.remove("incorrect");
            this.answers[i].classList.remove("selected");
            this.answers[i].classList.add("unselected");
        }
    }

    checkAnswer() {
        if (this.selectedAnswer == null) {
            alert("Please select an answer");
            return;
        }
        if (this.currentQuestion == null) {
            alert("Please select a question");
            return;
        }
        //loop through the answers and check if the selected answer is correct
        //if correct, then add .correct class to the answer
        //if incorrect, then add .incorrect class to the answer
        for (let i = 0; i < this.answers.length; i++) {
            //  console.log(this.answers[i].innerHTML);

            let answerText: string = this.answers[i].innerHTML;


            this.answers[i].classList.remove("unselected");
            this.answers[i].classList.remove("selected");
            this.answers[i].classList.remove("correct");
            this.answers[i].classList.remove("incorrect");


            //console.log(this.selectedAnswer);

            if (this.currentQuestion.isCorrectAnswer(answerText)) {
                if (this.selectedAnswer == answerText) {
                    this.answers[i].classList.add("correct");
                }
                else {
                    this.answers[i].classList.add("badAnswer");
                }
            } else {
                this.answers[i].classList.add("incorrect");
            }
            //console.log(this.answers[i].classList);

        }
        if (this.currentQuestion.isCorrectAnswer(this.selectedAnswer)) {
           // alert("Correct!");
        } else {
           // alert("Incorrect!");
        }
        //delay the next question by 1 second
        setTimeout(this.nextQuestion.bind(this), 1000);
       
    }
}