export class QuizVo
{
    public question:string;
    public answers:string[];
    public correctAnswer:string;

    constructor(question:string,answers:string[],correctAnswer:string)
    {
        this.question=question;
        this.answers=answers;
        this.correctAnswer=correctAnswer;
    }
    getAnswers():string[]
    {
        //make a copy of the answers array, add the correct answer to the copy, shuffle the copy, and return it
        let answers:string[] = this.answers.slice();
        answers.push(this.correctAnswer);
        answers = this.shuffle(answers);
        return answers;

    }
    isCorrectAnswer(answer:string):boolean
    {
        return answer.toLowerCase()==this.correctAnswer.toLowerCase();
    }
    shuffle(array:string[]):string[]
    {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            let temp= array[i];
            array[i]=array[j];
            array[j]=temp;
           // [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }
}