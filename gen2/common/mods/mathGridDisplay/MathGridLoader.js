var MathGridLoader = /** @class */ (function () {
    function MathGridLoader() {
    }
    MathGridLoader.prototype.getFileContent = function (lti, callback) {
        //https://tthq.me/api/pr/getequation/1
        //https://tthq.me/api/pr/getequation/X.TEST.01
        var url = "https://tthq.me/api/pr/getequation/" + lti;
        fetch(url, {
            method: "post"
        }).then(function (response) {
            if (response.ok) {
                response.json().then(function (json) {
                    ////console.log(json);                        
                    callback(json);
                });
            }
        });
    };
    return MathGridLoader;
}());
export { MathGridLoader };
//# sourceMappingURL=MathGridLoader.js.map