import { TCellVo } from "./dataObjs/TCellVo";
export declare class MathGrid {
    private lti;
    private el;
    private onReady;
    private steps;
    private stepIndex;
    constructor(lti: string, el: HTMLElement, onReady: Function);
    private loadContent;
    private onFileContentLoaded;
    showStep(stepIndex: number): void;
    getTestCell(): TCellVo;
    getGridHtml(): string;
    static injectCSS(): void;
}
