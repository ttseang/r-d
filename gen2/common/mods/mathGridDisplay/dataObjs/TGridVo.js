import { TCellVo } from "./TCellVo";
//gridData TCellVo[][]
var TGridVo = /** @class */ (function () {
    function TGridVo(gridData) {
        this.name = "step name";
        //gridData is a 2d array of TCellVo
        //each row is an array of TCellVo
        //columns are in the first dimension
        //rows are in the second dimension
        this.gridData = gridData;
        if (gridData.length === 0) {
            this.rowCount = 0;
            this.colCount = 0;
        }
        else {
            this.rowCount = gridData[0].length;
            this.colCount = gridData.length;
        }
    }
    TGridVo.prototype.loadFromData = function (data) {
        var gridData = [];
        for (var i = 0; i < data.length; i++) {
            gridData[i] = [];
            for (var j = 0; j < data[i].length; j++) {
                var cellData = data[i][j];
                var text = cellData.text;
                var cellClasses = cellData.cellClasses;
                var textClasses = cellData.textClasses;
                var rowSize = cellData.rowSize;
                var colSize = cellData.colSize;
                var type = cellData.type;
                var adjustX = cellData.adjustX;
                var adjustY = cellData.adjustY;
                var cellType = TCellVo.stringToConstants(type);
                var cell = new TCellVo(text, colSize, rowSize, textClasses, cellClasses, cellType);
                cell.adjustX = adjustX;
                cell.adjustY = adjustY;
                gridData[i][j] = cell;
            }
        }
        this.gridData = gridData;
    };
    return TGridVo;
}());
export { TGridVo };
//# sourceMappingURL=TGridVo.js.map