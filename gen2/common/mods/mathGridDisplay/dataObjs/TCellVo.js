export var CellType;
(function (CellType) {
    CellType["Whole"] = "whole";
    CellType["Fraction"] = "fraction";
    CellType["SquareRoot"] = "sqrt";
    CellType["Power"] = "power";
    CellType["Subscript"] = "subscript";
    CellType["SubSup"] = "subSup";
    CellType["Divide"] = "divide";
    CellType["Divide2Col"] = "divide2Col";
    CellType["Column"] = "column";
    CellType["Row"] = "row";
    CellType["Carry"] = "carry";
    CellType["Borrow"] = "borrow";
    CellType["Grid"] = "grid";
})(CellType || (CellType = {}));
var TCellVo = /** @class */ (function () {
    function TCellVo(text, colSize, rowSize, textClasses, cellClasses, type) {
        if (textClasses === void 0) { textClasses = []; }
        if (cellClasses === void 0) { cellClasses = []; }
        if (type === void 0) { type = CellType.Whole; }
        this.cellClasses = [];
        this.row = 0;
        this.col = 0;
        this.selected = false;
        this.dragOver = false;
        this.selectedIndex = 0;
        this.selectedIndexes = [];
        this.adjustX = [];
        this.adjustY = [];
        this.text = text;
        this.colSize = colSize;
        this.rowSize = rowSize;
        this.textClasses = textClasses;
        this.cellClasses = cellClasses;
        this.type = type;
    }
    TCellVo.stringToConstants = function (str) {
        var types = [CellType.Whole, CellType.Column, CellType.Row, CellType.Fraction, CellType.Power, CellType.SquareRoot, CellType.Carry, CellType.Borrow];
        for (var i = 0; i < types.length; i++) {
            if (str === types[i]) {
                return types[i];
            }
        }
        return CellType.Whole;
    };
    TCellVo.prototype.getHtml = function () {
        var text = "";
        var size = this.colSize.toString() + "_" + this.rowSize.toString();
        var baseClass = "cell_" + size;
        for (var i = 0; i < this.text.length; i++) {
            var adjustX = 0;
            var adjustY = 0;
            if (this.adjustX.length > i) {
                adjustX = this.adjustX[i];
                if (isNaN(adjustX) || adjustX === undefined || adjustX === null) {
                    adjustX = 0;
                }
            }
            if (this.adjustY.length > i) {
                adjustY = this.adjustY[i];
                if (isNaN(adjustY) || adjustY === undefined || adjustY === null) {
                    adjustY = 0;
                }
            }
            //make a relative position style for the cell
            //use --cellHeight and --cellWidth to set the size of the cell 
            //multiply by the adjustX and adjustY to set the position
            //make css object
            /* let style: string = "position:relative;";
        
            style+="left:calc(var(--cellWidth) * " + adjustX.toString() + ");";
            style+="top:calc(var(--cellHeight) * " + adjustY.toString() + ");"; */
            var text2 = this.text[i];
            if (text2 === "") {
                text2 = " ";
            }
            //   let classCopy: string[] = this.cellClasses.slice();
            var classes = this.textClasses[i] || [];
            var cellClass = baseClass + "_" + i.toString();
            classes.push(cellClass);
            if (classes) {
                text += "<span class='" + classes.join(" ") + "'>" + text2 + "</span>";
            }
            else {
                text += " <span>" + text2 + "</span>";
            }
        }
        this.cellClasses.unshift("cell");
        this.cellClasses.unshift(baseClass);
        if (this.cellClasses.length > 0) {
            return "<div class='" + this.cellClasses.join(" ") + "'>" + text + "</div>";
        }
        return text;
    };
    return TCellVo;
}());
export { TCellVo };
//# sourceMappingURL=TCellVo.js.map