export declare enum CellType {
    Whole = "whole",
    Fraction = "fraction",
    SquareRoot = "sqrt",
    Power = "power",
    Subscript = "subscript",
    SubSup = "subSup",
    Divide = "divide",
    Divide2Col = "divide2Col",
    Column = "column",
    Row = "row",
    Carry = "carry",
    Borrow = "borrow",
    Grid = "grid"
}
export declare class TCellVo {
    text: string[];
    rowSize: number;
    colSize: number;
    textClasses: string[][];
    cellClasses: string[];
    row: number;
    col: number;
    selected: boolean;
    dragOver: boolean;
    selectedIndex: number;
    selectedIndexes: number[];
    type: CellType;
    adjustX: number[];
    adjustY: number[];
    constructor(text: string[], colSize: number, rowSize: number, textClasses?: string[][], cellClasses?: string[], type?: CellType);
    static stringToConstants(str: string): CellType;
    getHtml(): string;
}
