import { TCellVo } from "./TCellVo";
export declare class TGridVo {
    gridData: TCellVo[][];
    rowCount: number;
    colCount: number;
    name: string;
    constructor(gridData: TCellVo[][]);
    loadFromData(data: any): void;
}
