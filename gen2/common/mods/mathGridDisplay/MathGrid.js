import { MathGridLoader } from "./MathGridLoader";
import { TGridVo } from "./dataObjs/TGridVo";
var MathGrid = /** @class */ (function () {
    function MathGrid(lti, el, onReady) {
        this.steps = [];
        this.stepIndex = 0;
        this.lti = lti;
        this.el = el;
        this.onReady = onReady;
        this.loadContent();
        window.mathGrid = this;
    }
    MathGrid.prototype.loadContent = function () {
        var mathGridLoader = new MathGridLoader();
        mathGridLoader.getFileContent(this.lti, this.onFileContentLoaded.bind(this));
    };
    MathGrid.prototype.onFileContentLoaded = function (data) {
        // console.log(data.data);
        var loadString = data.data;
        var loadObj = JSON.parse(loadString);
        var steps = [];
        var stepCount = loadObj.length;
        for (var i = 0; i < stepCount; i++) {
            var stepData = loadObj[i].gridData;
            var tGrid = new TGridVo([]);
            tGrid.loadFromData(stepData);
            steps.push(tGrid);
        }
        // console.log(steps);
        this.steps = steps;
        // this.onReady();
        this.showStep(0);
    };
    MathGrid.prototype.showStep = function (stepIndex) {
        this.stepIndex = stepIndex;
        //   console.log(this.getGridHtml());
        if (this.el) {
            this.el.innerHTML = this.getGridHtml();
        }
        else {
            console.log("no el");
        }
    };
    MathGrid.prototype.getTestCell = function () {
        var grid = this.steps[this.stepIndex];
        var firstCell = grid.gridData[0][0];
        return firstCell;
    };
    MathGrid.prototype.getGridHtml = function () {
        var grid = this.steps[this.stepIndex];
        console.log(grid);
        window.grid = grid;
        var gridData = grid.gridData;
        var colCount = gridData.length;
        var rowCount = 0;
        if (colCount > 0) {
            rowCount = gridData[0].length;
        }
        var rowHtml = [];
        var gridHtml = [];
        for (var j = 0; j < colCount; j++) {
            for (var i = 0; i < rowCount; i++) {
                var cell = gridData[j][i];
                console.log(cell.text.join(""));
                var cellHtml = cell.getHtml();
                rowHtml.push(cellHtml);
            }
            var row = rowHtml.join("");
            row = "<div>" + row + "</div>";
            gridHtml.push(row);
            rowHtml = [];
        }
        return gridHtml.join("");
    };
    MathGrid.injectCSS = function () {
        var cssString = "html{--defaultSize:6vw;--cellWidth:40px;--cellHeight:40px;--halfCellWidth:calc(var(--cellWidth) / 2);--halfCellHeight:calc(var(--cellHeight) / 2)}.mathGrid{display:flex;font-size:var(--defaultSize);width:100%}.cell{width:var(--cellWidth);height:var(--cellHeight)}.topline{border-top:solid 4px black!important}.bottomline{border-bottom:solid 4px black!important}.rightline{border-right:solid 4px black!important}.leftline{border-left:solid 4px black!important}.halfDown .cell_1_1_0{position:relative;top:50%}.underlineRow{border-bottom:2px solid black!important}.smallText{font-size:.8em}.largeText{font-size:1.2em}.redText{color:red}.greenText{color:green}.blueText{color:blue}.yellowText{color:yellow}.purpleText{color:purple}.orangeText{color:orange}.pinkText{color:pink}.brownText{color:brown}.blackText{color:black}.whiteText{color:white}.grayText{color:gray}.lightBlueText{color:lightblue}.lightGreenText{color:lightgreen}.boldText{font-weight:bold}.italicText{font-style:italic}.tableBorder{border:solid;padding:18px;width:150px;height:fit-content;display:flex;justify-content:center;align-items:flex-start}.strikethrough{position:relative;font-size:.8em}.strikethrough:before{position:absolute;content:\"\";left:0;top:calc(var(--cellHeight) / 3);right:0;border-top:1px solid;border-color:inherit;-webkit-transform:rotate(-5deg);-moz-transform:rotate(-5deg);-ms-transform:rotate(-5deg);-o-transform:rotate(-5deg);transform:rotate(-5deg)}.halfCircle{display:inline;border-right:2px black solid;border-radius:0 0 16px 0;text-align:right;padding-right:5px}.division-line{border-top:2px solid black;width:50px;margin:0 10px}.twoCol{display:flex;align-items:center;justify-content:center;align-content:center;flex-direction:row}.twoCol .cell_2_1_0{display:inline;text-align:right;font-size:calc(var(--defaultSize) /4)}.twoCol .cell_2_1_1{display:inline;text-align:left;font-size:calc(var(--defaultSize) /4)}.threeCol{display:flex;align-items:center;justify-content:center;align-content:center;height:var(--cellHeight)}.threeCol .cell_3_1_0{display:inline;text-align:right;font-size:calc(var(--defaultSize) /5)}.threeCol .cell_3_1_1{display:inline;text-align:center;font-size:calc(var(--defaultSize) /5)}.threeCol .cell_3_1_2{display:inline;text-align:left;font-size:calc(var(--defaultSize) /5)}.fourCol{display:flex;align-items:center;justify-content:center;align-content:center;height:var(--cellHeight)}.fourCol .cell_1_4_0{display:inline;text-align:right;font-size:calc(var(--defaultSize) /6)}.fourCol .cell_1_4_1{display:inline;text-align:center;font-size:calc(var(--defaultSize) /6)}.fourCol .cell_1_4_2{display:inline;text-align:center;font-size:calc(var(--defaultSize) /6)}.fourCol .cell_1_4_3{display:inline;text-align:left;font-size:calc(var(--defaultSize) /6)}.twoRows{display:flex;align-items:center;justify-content:center;align-content:center;flex-direction:column;font-size:calc(var(--defaultSize) /6)}.threeRows{display:flex;align-items:center;justify-content:center;align-content:center;flex-direction:column;font-size:calc(var(--defaultSize) /9)}.fourRows{display:flex;align-items:center;justify-content:center;align-content:center;flex-direction:column;font-size:calc(var(--defaultSize) /6)}.fraction{font-size:calc(var(--defaultSize) / 6)}.fraction .cell_1_2_0{border-bottom:solid 2px black}.fraction2x2{display:grid;grid-template-columns:repeat(2,1fr);font-size:calc(var(--defaultSize) / 6)}.fraction2x2 .cell_2_2_0{border-bottom:solid 2px black}.fraction2x2 .cell_2_2_1{border-bottom:solid 2px black}.fraction3x3{display:grid;grid-template-columns:repeat(3,1fr);font-size:calc(var(--defaultSize) / 6)}.fraction3x3 .cell_3_3_0{border-bottom:solid 2px black}.fraction3x3 .cell_3_3_1{border-bottom:solid 2px black}.fraction3x3 .cell_3_3_2{border-bottom:solid 2px black}.fraction4x4{display:grid;grid-template-columns:repeat(4,1fr);font-size:calc(var(--defaultSize) / 6)}.fraction4x4 .cell_4_4_0{border-bottom:solid 2px black}.fraction4x4 .cell_4_4_1{border-bottom:solid 2px black}.fraction4x4 .cell_4_4_2{border-bottom:solid 2px black}.fraction4x4 .cell_4_4_3{border-bottom:solid 2px black}.fs1{font-size:calc(var(--defaultSize) /10)!important}.fs2{font-size:calc(var(--defaultSize) /9)!important}.fs3{font-size:calc(var(--defaultSize) /8)!important}.fs4{font-size:calc(var(--defaultSize) /7)!important}.fs5{font-size:calc(var(--defaultSize) /6)!important}.fs6{font-size:calc(var(--defaultSize) /5)!important}.fs7{font-size:calc(var(--defaultSize) /4)!important}.fs8{font-size:calc(var(--defaultSize) /3)!important}.fs9{font-size:calc(var(--defaultSize) /2)!important}.fs10{font-size:calc(var(--defaultSize))}.sqrt .cell_1_1_0{position:relative;left:calc(var(--cellWidth) / 3.3);top:0;font-size:calc(var(--defaultSize) / 3)}.cell_1_2{display:flex;flex-wrap:nowrap;align-content:flex-start;justify-content:center;flex-direction:column;font-size:calc(var(--defaultSize) / 3);width:var(--cellWidth);height:var(--cellHeight);align-items:center}.cell_1_2_0{font-size:calc(var(--defaultSize) / 6)}.cell_1_2_1{font-size:calc(var(--defaultSize) / 6)}.cell_2_1{display:flex;flex-wrap:nowrap;align-content:flex-start;justify-content:center;flex-direction:row;font-size:calc(var(--defaultSize) / 3);width:var(--cellWidth);height:var(--cellHeight)}.cell_2_1_0{font-size:calc(var(--defaultSize) / 6)}.power .cell_2_1_0{font-size:calc(var(--defaultSize) / 6);position:relative;top:calc(var(--cellHeight) / 3)}.power .cell_2_1_1{font-size:calc(var(--defaultSize) / 6);position:relative;top:calc(var(--cellHeight) / 3)}.power2x2{display:flex;flex-wrap:nowrap;align-content:flex-start;justify-content:center;flex-direction:row;width:var(--cellWidth);height:var(--cellHeight)}.power2x2 .cell_2_2_0{position:relative;top:calc(var(--cellHeight) / 3);font-size:calc(var(--defaultSize) / 6)}.power2x2 .cell_2_2_1{position:relative;top:calc(var(--cellHeight) / 3);font-size:calc(var(--defaultSize) / 6)}.power2x2 .cell_2_2_2{font-size:calc(var(--defaultSize) / 8)}.power2x2 .cell_2_2_3{font-size:calc(var(--defaultSize) / 8)}.power3x3{display:flex;flex-wrap:nowrap;align-content:flex-start;justify-content:center;flex-direction:row;width:var(--cellWidth);height:var(--cellHeight)}.power3x3 .cell_3_3_0{position:relative;top:calc(var(--cellHeight) / 3);font-size:calc(var(--defaultSize) / 6)}.power3x3 .cell_3_3_1{position:relative;top:calc(var(--cellHeight) / 3);font-size:calc(var(--defaultSize) / 6)}.power3x3 .cell_3_3_2{position:relative;top:calc(var(--cellHeight) / 3);font-size:calc(var(--defaultSize) / 6)}.power3x3 .cell_3_3_3{font-size:calc(var(--defaultSize) / 8)}.power3x3 .cell_3_3_4{font-size:calc(var(--defaultSize) / 8)}.power3x3 .cell_3_3_5{font-size:calc(var(--defaultSize) / 8)}.cell_1_3{display:flex;flex-wrap:nowrap;align-content:flex-start;justify-content:center;flex-direction:row;font-size:calc(var(--defaultSize) /4);width:var(--cellWidth);height:var(--cellHeight)}.power .cell_2_1_1{font-size:calc(var(--defaultSize) / 6);position:relative;top:0}.divide .cell_1_1_0{display:inline;border-right:2px black solid;border-radius:0 0 16px 0;text-align:right;padding-right:5px;font-size:calc(var(--defaultSize) /4)}.divide2Col{display:flex;align-items:center;justify-content:center;align-content:center;height:var(--cellHeight)}.divide2Col .cell_2_1_0{display:inline;text-align:right;font-size:calc(var(--defaultSize) /4)}.divide2Col .cell_2_1_1{display:inline;border-right:2px black solid;border-radius:0 0 16px 0;text-align:right;padding-right:5px;font-size:calc(var(--defaultSize) /4)}.carry{display:flex;align-items:center;justify-content:center;align-content:center;height:var(--cellHeight)}.carry .cell_1_1_0{display:inline;position:relative;top:calc(var(--cellHeight) / 4);left:0;font-size:calc(var(--defaultSize) /6)}.borrow{display:flex;align-items:center;justify-content:center;align-content:center;height:var(--cellHeight)}.cell_1_1{display:flex;flex-wrap:nowrap;align-content:flex-start;justify-content:center;flex-direction:column;font-size:calc(var(--defaultSize) / 2);width:var(--cellWidth);height:var(--cellHeight);position:relative;align-items:center}.whole.cell_1_1{font-size:calc(var(--defaultSize) / 3)}.operator.cell_1_1{font-size:calc(var(--defaultSize) / 3)}";
        var style = document.createElement("style");
        style.innerHTML = cssString;
        document.head.appendChild(style);
    };
    return MathGrid;
}());
export { MathGrid };
//# sourceMappingURL=MathGrid.js.map