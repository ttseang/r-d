export declare class ButtonController {
    private static instance;
    callback: Function;
    constructor();
    static getInstance(): ButtonController;
    doAction(action: string, actionParam: string): void;
}
