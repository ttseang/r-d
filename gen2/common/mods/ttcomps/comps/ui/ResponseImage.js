"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseImage = void 0;
var __1 = require("../..");
var CompManager_1 = require("../CompManager");
var ResponseImage = /** @class */ (function (_super) {
    __extends(ResponseImage, _super);
    function ResponseImage(bscene, key, hscale, imageKey) {
        var _this = _super.call(this, bscene.getScene(), 0, 0, imageKey) || this;
        _this.posVo = new __1.PosVo(0, 0);
        _this.cm = CompManager_1.CompManager.getInstance();
        _this.bscene = bscene;
        _this.hscale = hscale;
        _this.key = key;
        __1.Align.scaleToGameW(_this, hscale, bscene);
        _this.scene.add.existing(_this);
        _this.cm.comps.push(_this);
        _this.cm.compMap.set(key, _this);
        return _this;
    }
    ResponseImage.prototype.doResize = function () {
        __1.Align.scaleToGameW(this, this.hscale, this.bscene);
        this.bscene.getGrid().placeAt(this.posVo.x, this.posVo.y, this);
    };
    ResponseImage.prototype.setPos = function (xx, yy) {
        this.posVo = new __1.PosVo(xx, yy);
        this.bscene.getGrid().placeAt(xx, yy, this);
    };
    return ResponseImage;
}(Phaser.GameObjects.Image));
exports.ResponseImage = ResponseImage;
//# sourceMappingURL=ResponseImage.js.map