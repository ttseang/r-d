"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.TriangleButton = void 0;
var __1 = require("../..");
var ButtonController_1 = require("../ButtonController");
var BaseComp_1 = require("./BaseComp");
var TriangleButton = /** @class */ (function (_super) {
    __extends(TriangleButton, _super);
    function TriangleButton(bscene, key, action, actionParam, buttonVo, angle) {
        if (angle === void 0) { angle = 0; }
        var _this = _super.call(this, bscene, key) || this;
        _this.posVo = new __1.PosVo(0, 0);
        _this.buttonController = ButtonController_1.ButtonController.getInstance();
        _this.buttonVo = buttonVo;
        _this.backStyleVo = _this.cm.getBackStyle(buttonVo.backStyle);
        //  let ww: number = this.bscene.getW() * buttonVo.hsize;
        var hh = _this.bscene.getH() * buttonVo.vsize;
        var ww = hh;
        _this.ww = ww;
        _this.hh = hh;
        //  hh=this.text1.displayHeight*3;
        _this.back = _this.scene.add.graphics();
        _this.border = _this.scene.add.graphics();
        _this.border.lineStyle(2, _this.backStyleVo.borderColor);
        _this.border.moveTo(0, 0);
        _this.border.lineTo(ww / 2, hh);
        _this.border.lineTo(-ww / 2, hh);
        _this.border.lineTo(0, 0);
        _this.border.stroke();
        _this.back.fillStyle(_this.backStyleVo.backColor, 1);
        _this.back.moveTo(0, 0);
        _this.back.lineTo(ww / 2, hh);
        _this.back.lineTo(-ww / 2, hh);
        _this.back.lineTo(0, 0);
        //this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        _this.back.fillPath();
        _this.add(_this.back);
        _this.add(_this.border);
        _this.setAngle(angle);
        _this.scene.add.existing(_this);
        _this.setSize(ww, hh);
        _this.setInteractive();
        _this.on("pointerdown", function () {
            _this.setBorder(_this.backStyleVo.borderPress);
            _this.buttonController.doAction(_this.action, _this.actionParam);
        });
        _this.on("pointerup", function () {
            _this.resetBorder();
        });
        _this.on("pointerover", function () {
            _this.setBorder(_this.backStyleVo.borderOver);
        });
        _this.on("pointerout", function () {
            _this.resetBorder();
        });
        window['btn'] = _this;
        return _this;
    }
    TriangleButton.prototype.doResize = function () {
        var ww = this.bscene.getW() * this.buttonVo.hsize;
        var hh = this.bscene.getH() * this.buttonVo.vsize;
        this.ww = ww;
        this.hh = hh;
        this.border.clear();
        this.back.clear();
        this.border.lineStyle(2, this.backStyleVo.borderColor);
        this.border.moveTo(0, 0);
        this.border.lineTo(ww / 2, hh);
        this.border.lineTo(-ww / 2, hh);
        this.border.lineTo(0, 0);
        this.border.stroke();
        this.back.fillStyle(this.backStyleVo.backColor, 1);
        this.back.moveTo(0, 0);
        this.back.lineTo(ww / 2, hh);
        this.back.lineTo(-ww / 2, hh);
        this.back.lineTo(0, 0);
        //this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.back.fillPath();
        this.setSize(ww, hh);
        _super.prototype.doResize.call(this);
    };
    TriangleButton.prototype.resetBorder = function () {
        this.border.clear();
        this.border.lineStyle(2, this.backStyleVo.borderColor);
        // this.border.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.border.moveTo(0, 0);
        this.border.lineTo(this.ww / 2, this.hh);
        this.border.lineTo(-this.ww / 2, this.hh);
        this.border.lineTo(0, 0);
        this.border.stroke();
    };
    TriangleButton.prototype.setBorder = function (color) {
        this.border.clear();
        this.border.lineStyle(2, color);
        this.border.moveTo(0, 0);
        this.border.lineTo(this.ww / 2, this.hh);
        this.border.lineTo(-this.ww / 2, this.hh);
        this.border.lineTo(0, 0);
        this.border.stroke();
    };
    return TriangleButton;
}(BaseComp_1.BaseComp));
exports.TriangleButton = TriangleButton;
//# sourceMappingURL=TriangleButton.js.map