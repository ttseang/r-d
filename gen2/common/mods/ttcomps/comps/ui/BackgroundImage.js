"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackgroundImage = void 0;
var BaseComp_1 = require("./BaseComp");
var BackgroundImage = /** @class */ (function (_super) {
    __extends(BackgroundImage, _super);
    function BackgroundImage(bscene, key, type, params) {
        var _this = _super.call(this, bscene, key) || this;
        console.log(params);
        if (type === "color") {
            _this.back = _this.scene.add.image(-_this.bscene.cw / 2, -_this.bscene.ch / 2, "holder").setOrigin(0, 0);
            _this.back.displayWidth = _this.bscene.getW();
            _this.back.displayHeight = _this.bscene.getH();
            _this.back.setTint(parseInt(params));
            _this.add(_this.back);
            _this.setPos(0, 0);
            _this.setSize(_this.back.displayWidth, _this.back.displayHeight);
        }
        _this.scene.add.existing(_this);
        return _this;
    }
    BackgroundImage.prototype.doResize = function () {
        this.back.displayWidth = this.bscene.getW();
        this.back.displayHeight = this.bscene.getH();
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        _super.prototype.doResize.call(this);
    };
    return BackgroundImage;
}(BaseComp_1.BaseComp));
exports.BackgroundImage = BackgroundImage;
//# sourceMappingURL=BackgroundImage.js.map