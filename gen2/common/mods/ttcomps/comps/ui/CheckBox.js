"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckBox = void 0;
var BaseComp_1 = require("./BaseComp");
var CompBack_1 = require("./CompBack");
var CheckBox = /** @class */ (function (_super) {
    __extends(CheckBox, _super);
    function CheckBox(bscene, key, vscale, backStyle) {
        var _this = _super.call(this, bscene, key) || this;
        var hh = _this.bscene.getH() * vscale;
        var ww = hh;
        _this.vscale = vscale;
        _this.backStyleVo = _this.cm.getBackStyle(backStyle);
        _this.back = new CompBack_1.CompBack(bscene, ww, hh, _this.backStyleVo);
        _this.check = _this.scene.add.image(0, 0, "check");
        _this.add(_this.back);
        _this.check.displayWidth = ww * 0.9;
        _this.check.displayHeight = hh * 0.9;
        _this.add(_this.back);
        _this.add(_this.check);
        _this.setSize(ww, hh);
        _this.scene.add.existing(_this);
        _this.setInteractive();
        _this.on("pointerdown", function () {
            _this.check.visible = !_this.check.visible;
        });
        return _this;
    }
    CheckBox.prototype.doResize = function () {
        _super.prototype.doResize.call(this);
        var hh = this.bscene.getH() * this.vscale;
        var ww = hh;
        this.check.displayWidth = ww * 0.9;
        this.check.displayHeight = hh * 0.9;
        this.back.doResize(ww, hh);
    };
    return CheckBox;
}(BaseComp_1.BaseComp));
exports.CheckBox = CheckBox;
//# sourceMappingURL=CheckBox.js.map