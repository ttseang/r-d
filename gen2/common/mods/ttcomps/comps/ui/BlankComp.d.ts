import { IBaseScene } from "../..";
import { BaseComp } from "./BaseComp";
export declare class BlankComp extends BaseComp {
    private back;
    private obj;
    constructor(bscene: IBaseScene, key: string, obj: any, style: string);
    doResize(): void;
}
