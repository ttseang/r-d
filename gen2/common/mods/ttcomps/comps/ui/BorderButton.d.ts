import { IComp, PosVo, IBaseScene } from "../..";
import { ButtonStyleVo } from "../../dataObjs/ButtonStyleVo";
import { BaseComp } from "./BaseComp";
export declare class BorderButton extends BaseComp implements IComp {
    private buttonVo;
    private back;
    private text1;
    private buttonController;
    private ww;
    private hh;
    action: string;
    actionParam: string;
    posVo: PosVo;
    constructor(bscene: IBaseScene, key: string, text: string, action: string, actionParam: string, buttonVo: ButtonStyleVo);
    setText(text: string): void;
    doResize(): void;
    resetBorder(): void;
    setBorder(color: number): void;
}
