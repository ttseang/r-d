import { IComp, PosVo, IBaseScene } from "../..";
export declare class ResponseImage extends Phaser.GameObjects.Image implements IComp {
    posVo: PosVo;
    private bscene;
    private hscale;
    private cm;
    key: string;
    constructor(bscene: IBaseScene, key: string, hscale: number, imageKey: string);
    doResize(): void;
    setPos(xx: number, yy: number): void;
}
