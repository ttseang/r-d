import { IComp, IBaseScene } from "../..";
import { ButtonStyleVo } from "../../dataObjs/ButtonStyleVo";
import { BaseComp } from "./BaseComp";
export declare class IconTextButton extends BaseComp implements IComp {
    private back;
    private icon;
    private buttonVo;
    private text1;
    private ww;
    private hh;
    text: string;
    action: string;
    actionParam: string;
    private buttonController;
    constructor(bscene: IBaseScene, key: string, text: string, iconKey: string, action: string, actionParam: string, buttonVo: ButtonStyleVo);
    doResize(): void;
    resetBorder(): void;
    setBorder(color: number): void;
}
