import { IComp, IBaseScene } from "../..";
import { BaseComp } from "./BaseComp";
export declare class UISlider extends BaseComp implements IComp {
    private back;
    private hscale;
    private vscale;
    private knob;
    private color;
    constructor(bscene: IBaseScene, key: string, hscale: number, vscale: number, color: number, knobColor: number);
    knobDown(p: Phaser.Input.Pointer): void;
    dragKnob(p: Phaser.Input.Pointer): void;
    knobUp(): void;
    doResize(): void;
}
export default UISlider;
