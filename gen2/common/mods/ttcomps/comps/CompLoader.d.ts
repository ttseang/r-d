export declare class CompLoader {
    private cm;
    private callback;
    constructor(callback: Function);
    loadComps(file: string): void;
    process(data: any): void;
}
