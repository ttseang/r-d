import { CompVo } from "./CompVo";
export declare class PageVo {
    pageName: string;
    comps: CompVo[];
    backgroundType: string;
    backgroundParams: string;
    constructor(pageName: string, comps: CompVo[]);
}
