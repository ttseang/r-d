export declare class AnchorVo {
    anchorTo: string;
    anchorX: number;
    anchorY: number;
    anchorInside: boolean;
    constructor(anchorTo: string, anchorX: number, anchorY: number, anchorInside: boolean);
}
