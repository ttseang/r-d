"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompVo = void 0;
var CompVo = /** @class */ (function () {
    function CompVo(key, type, text, icon, x, y, w, h, style, backstyle) {
        this.flipX = false;
        this.flipY = false;
        this.angle = 0;
        this.anchorVo = null;
        this.action = "";
        this.actionParam = "";
        this.key = key;
        this.type = type;
        this.text = text;
        this.icon = icon;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.style = style;
        this.backstyle = backstyle;
    }
    return CompVo;
}());
exports.CompVo = CompVo;
//# sourceMappingURL=CompVo.js.map