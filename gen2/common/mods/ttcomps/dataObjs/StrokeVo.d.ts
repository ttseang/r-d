export declare class StrokeVo {
    thickness: number;
    color: string;
    constructor(thickness: number, color: string);
}
