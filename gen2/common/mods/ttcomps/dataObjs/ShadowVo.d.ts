export declare class ShadowVo {
    x: number;
    y: number;
    color: string;
    blur: number;
    stroke: boolean;
    fill: boolean;
    constructor(x: number, y: number, color: string, blur: number, stroke: boolean, fill: boolean);
}
