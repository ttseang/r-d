"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextStyleVo = void 0;
var TextStyleVo = /** @class */ (function () {
    function TextStyleVo(fontName, textColor, maxFontSize) {
        this.autoSize = true;
        this.fontName = fontName;
        this.textColor = textColor;
        this.maxFontSize = maxFontSize;
    }
    return TextStyleVo;
}());
exports.TextStyleVo = TextStyleVo;
//# sourceMappingURL=TextStyleVo.js.map