export declare class ColorBurst {
    private scene;
    x: number;
    y: number;
    size: number;
    count: number;
    dist: number;
    duration: number;
    maxDist: number;
    color: number;
    constructor(scene: Phaser.Scene, x?: number, y?: number, size?: number, count?: number, dist?: number, duration?: number, maxDist?: number, color?: number);
    start(): void;
    tweenDone(tween: any, targets: any, custom: any): void;
    static preload(scene: Phaser.Scene, path: string): void;
}
