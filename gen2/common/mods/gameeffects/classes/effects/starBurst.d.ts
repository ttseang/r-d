export declare class StarBurst {
    scene: Phaser.Scene;
    count: number;
    size: number;
    dist: number;
    duration: number;
    maxDist: any;
    f: number;
    tint: number;
    x: number;
    y: number;
    constructor(scene: Phaser.Scene, x?: number, y?: number, size?: number, count?: number, dist?: number, duration?: number, maxDist?: number, f?: number, tint?: number);
    start(): void;
    tweenDone(tween: any, targets: any, custom: any): void;
    static preload(scene: any, path: any): void;
}
