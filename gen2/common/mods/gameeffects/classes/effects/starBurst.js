"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StarBurst = void 0;
var StarBurst = /** @class */ (function () {
    function StarBurst(scene, x, y, size, count, dist, duration, maxDist, f, tint) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (size === void 0) { size = 5; }
        if (count === void 0) { count = 25; }
        if (dist === void 0) { dist = 150; }
        if (duration === void 0) { duration = 1000; }
        if (maxDist === void 0) { maxDist = 300; }
        if (f === void 0) { f = 0; }
        if (tint === void 0) { tint = 0xffffff; }
        this.scene = scene;
        this.count = count;
        this.size = size;
        this.dist = dist;
        this.duration = duration;
        this.maxDist = maxDist;
        this.f = f;
        this.tint = tint;
        this.x = x;
        this.y = y;
    }
    StarBurst.prototype.start = function () {
        for (var i = 0; i < this.count; i++) {
            var star = this.scene.add.sprite(this.x, this.y, "effectStars");
            //
            //
            //
            star.setTint(this.tint);
            star.setFrame(this.f);
            star.setOrigin(0.5, 0.5);
            var r = Phaser.Math.Between(50, this.maxDist);
            var s = Phaser.Math.Between(1, 100) / 100;
            star.scaleX = s;
            star.scaleY = s;
            var angle = i * (360 / this.count);
            var tx = this.x + r * Math.cos(angle);
            var ty = this.y + r * Math.sin(angle);
            this.scene.children.bringToTop(star);
            //  star.x=tx;
            // star.y=ty;
            this.scene.tweens.add({
                targets: star,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                        scope: this
                    }]
            });
        }
    };
    StarBurst.prototype.tweenDone = function (tween, targets, custom) {
        targets[0].destroy();
    };
    StarBurst.preload = function (scene, path) {
        scene.load.spritesheet('effectStars', path, {
            frameWidth: 25,
            frameHeight: 25
        });
    };
    return StarBurst;
}());
exports.StarBurst = StarBurst;
//# sourceMappingURL=starBurst.js.map