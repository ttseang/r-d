export declare class Flare extends Phaser.GameObjects.Sprite {
    scene: Phaser.Scene;
    x: number;
    y: number;
    tint: number;
    scale: number;
    direction: number;
    duration: number;
    gameWidth: number;
    constructor(scene: Phaser.Scene, gameWidth: number, x?: number, y?: number, tint?: number);
    start(): void;
    tweenDone(): void;
    static preload(scene: Phaser.Scene, path: string): void;
}
