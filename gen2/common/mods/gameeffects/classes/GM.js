"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GM = void 0;
var instance = null;
var GM = /** @class */ (function () {
    function GM() {
        this.isMobile = false;
        this.isPort = false;
        this.isTablet = false;
        window['gm'] = this;
    }
    GM.getInstance = function () {
        if (instance === null) {
            instance = new GM();
        }
        return instance;
    };
    return GM;
}());
exports.GM = GM;
//# sourceMappingURL=GM.js.map