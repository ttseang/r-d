"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UIChild = exports.UIBlock = void 0;
/*jshint esversion: 6 */
var UIBlock = /** @class */ (function () {
    function UIBlock() {
        this._depth = 1;
        this._alpha = 1;
        //init private variables
        this._x = 0;
        this._y = 0;
        //
        //
        //keep track of this block's previous position
        this._oldX = 0;
        this._oldY = 0;
        //
        //
        this._visible = true;
        //
        //
        //needs to be set by developer
        this._displayWidth = 0;
        this._displayHeight = 0;
        //
        //
        //an array of the children
        this.children = [];
        //current child count
        //used for indexing
        this.childIndex = -1;
        //
        //used to identify this as a UIBlock to another UIBlock
        this.isPosBlock = true;
        this._depth = 1;
        this._alpha = 1;
    }
    Object.defineProperty(UIBlock.prototype, "depth", {
        get: function () {
            return this._depth;
        },
        set: function (val) {
            //console.log(val);
            this._depth = val;
            if (this.children.length > 0) {
                this.setChildDepth(this.children[0]);
            }
        },
        enumerable: false,
        configurable: true
    });
    UIBlock.prototype.setChildDepth = function (uiChild) {
        //console.log(child);
        var realDepth = this._depth * 100 + uiChild.childIndex;
        //  console.log(realDepth);
        uiChild.depth = realDepth;
        //  child.setDepth(realDepth);
        if (uiChild.nextChild != null) {
            this.setChildDepth(uiChild.nextChild);
        }
    };
    Object.defineProperty(UIBlock.prototype, "x", {
        //getters
        get: function () {
            return this._x;
        },
        set: function (val) {
            //record the current x into oldX
            this._oldX = this._x;
            //
            //update the value
            this._x = val;
            //
            //update the children
            this.updatePositions();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIBlock.prototype, "y", {
        get: function () {
            return this._y;
        },
        set: function (val) {
            //record the current y into oldY
            this._oldY = this._y;
            //
            //update the value
            this._y = val;
            //update the children
            this.updatePositions();
        },
        enumerable: false,
        configurable: true
    });
    //add a child
    UIBlock.prototype.add = function (obj) {
        //up the index
        this.childIndex++;
        //make a note of the index inside the child
        //  child.childIndex = this.childIndex;
        //add to the array
        var uiChild = new UIChild(obj);
        uiChild.childIndex = this.childIndex;
        this.children.push(uiChild);
        //build the linked list
        this.buildList();
    };
    /* removeAvatar(userID) {
         if (this.avatars[userID]) {
             var avatar = this.avatars[userID];
             if (avatar.prevAvatar) avatar.prevAvatar.nextAvatar = avatar.nextAvatar;
             avatar.destroy();
             delete this.avatars[userID];
         }
     }*/
    UIBlock.prototype.removeChild = function (uiChild) {
        //take the child off the array based on index
        this.children.splice(uiChild.childIndex, 1);
        //
        //rebuild the linked list
        this.buildList();
        //rebuild the indexes
        var len = this.children.length;
        for (var i = 0; i < len; i++) {
            this.children[i].childIndex = i;
        }
        //set the childIndex to the length of the array
        this.childIndex = len;
    };
    UIBlock.prototype.buildList = function () {
        var len = this.children.length;
        if (len > 1) {
            for (var i = 1; i < len; i++) {
                //set the current child to the previous child's nextChild property
                this.children[i - 1].nextChild = this.children[i];
            }
        }
        this.children[len - 1].nextChild = null;
    };
    UIBlock.prototype.willRender = function () {
    };
    Object.defineProperty(UIBlock.prototype, "displayWidth", {
        get: function () {
            return this._displayWidth;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIBlock.prototype, "displayHeight", {
        get: function () {
            return this._displayHeight;
        },
        enumerable: false,
        configurable: true
    });
    UIBlock.prototype.setSize = function (w, h) {
        this._displayWidth = w;
        this._displayHeight = h;
    };
    UIBlock.prototype.setXY = function (x, y) {
        this.x = x;
        this.y = y;
        this.updatePositions();
    };
    Object.defineProperty(UIBlock.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (val) {
            if (this._visible != val) {
                this._visible = val;
                if (this.children.length > 0) {
                    //send the first child to the updateChildVisible function
                    this.updateChildVisible(this.children[0], val);
                }
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIBlock.prototype, "alpha", {
        get: function () {
            return this._alpha;
        },
        //
        //
        //
        //
        set: function (val) {
            if (this._alpha != val) {
                this._alpha = val;
                if (this.children.length > 0) {
                    //send the first child to the updateChildalpha function
                    this.updateChildAlpha(this.children[0], val);
                }
            }
        },
        enumerable: false,
        configurable: true
    });
    /*  setScrollFactor(scroll:number) {
         //setScrollFactor
         if (this.children.length > 0) {
             //send the first child to the updateChildalpha function
             this.updateChildScroll(this.children[0], scroll);
         }
     } */
    /* updateChildScroll(uiChild:UIChild, scroll) {
        uiChild.child.setScrollFactor(scroll);
        if (uiChild.nextChild) {
            uiChild.nextChild.child.setScrollFactor(scroll);
        }
    } */
    UIBlock.prototype.updateChildAlpha = function (uiChild, alpha) {
        uiChild.child.alpha = alpha;
        if (uiChild.isPosBlock == true) {
            uiChild.child.alpha = alpha;
        }
        if (uiChild.nextChild != null) {
            //if the child has a nextChild call this function recursively 
            this.updateChildAlpha(uiChild.nextChild, alpha);
        }
    };
    UIBlock.prototype.updateChildVisible = function (uiChild, vis) {
        uiChild.child.visible = vis;
        if (uiChild.isPosBlock == true) {
            uiChild.child.visible = vis;
        }
        if (uiChild.nextChild != null) {
            //if the child has a nextChild call this function recursively 
            this.updateChildVisible(uiChild.nextChild, vis);
        }
    };
    UIBlock.prototype.updateChildPos = function (uiChild) {
        uiChild.child.y = uiChild.child.y - this._oldY + this._y;
        uiChild.child.x = uiChild.child.x - this._oldX + this._x;
        /*  if (uiChild.isPosBlock == true) {
             uiChild.updatePositions();
             
         } */
        if (uiChild.nextChild != null) {
            //if the child has a nextChild call this function recursively 
            this.updateChildPos(uiChild.nextChild);
        }
        //set the old values to the new
        this._oldX = this._x;
        this._oldY = this._y;
    };
    UIBlock.prototype.updatePositions = function () {
        if (this.children) {
            if (this.children.length > 0) {
                //send the first child to the updateChildPos function
                this.updateChildPos(this.children[0]);
            }
        }
    };
    UIBlock.prototype.getRelPos = function (uiChild) {
        return {
            x: uiChild.child.x - this.x,
            y: uiChild.child.y - this.y
        };
    };
    UIBlock.prototype.getChildren = function (myArray, child) {
        myArray.push(child);
        if (child.isPosBlock) {
            if (child.children.length > 0) {
                child.getChildren(myArray, child.children[0]);
            }
        }
        if (child.nextChild) {
            this.getChildren(myArray, child.nextChild);
        }
    };
    UIBlock.prototype.getAllChildren = function () {
        var childArray = [];
        if (this.children.length > 0) {
            this.getChildren(childArray, this.children[0]);
        }
        return childArray;
    };
    UIBlock.prototype.getChildAt = function (index) {
        return this.children[index];
    };
    /* setMask(mask)
    {
        this.getAllChildren().forEach(function(child) {
            child.setMask(mask);
        }.bind(this));
    } */
    UIBlock.prototype.setAngle = function (angle) {
        this.getAllChildren().forEach(function (child) {
            child.setAngle(angle);
        }.bind(this));
    };
    UIBlock.prototype.destroy = function () {
        var childArray = this.getAllChildren();
        this.childIndex = -1;
        //console.log(childArray);
        var len = childArray.length;
        for (var i = 0; i < len; i++) {
            childArray[i].destroy();
        }
        this.children.length = 0;
        childArray.length = 0;
    };
    return UIBlock;
}());
exports.UIBlock = UIBlock;
exports.default = UIBlock;
var UIChild = /** @class */ (function () {
    function UIChild(child) {
        this.nextChild = undefined;
        this.childIndex = 0;
        this.isPosBlock = false;
        this.depth = 0;
        this.child = child;
    }
    return UIChild;
}());
exports.UIChild = UIChild;
//# sourceMappingURL=UIBlock.js.map