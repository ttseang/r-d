import { TTScrollbar } from "./classes/TTScrollbar";
var ScrollTest = /** @class */ (function () {
    function ScrollTest() {
        //get the text area reference
        this.textArea = document.getElementsByClassName("testText")[0];
        //get the scroll group reference - this is the parent element that will contain the scrollbar and
        //the text area or whatever else you want to scroll
        this.scrollGroup = document.getElementsByClassName("scrollGroup")[0];
        //create the scrollbar passing in the scrollable element and the parent element to add the scrollbar to
        //as well as a callback function to call when the scrollbar is moved
        this.ttScrollBar = new TTScrollbar(this.textArea, null, this.updateTextArea.bind(this));
        this.ttScrollBar.setImage("track", "./assets/scroll/scrollback.png");
        this.ttScrollBar.setImage("knob", "./assets/scroll/thumbScroll.png");
        this.ttScrollBar.setImage("btnUp", "./assets/scroll/arrowUp.png");
        this.ttScrollBar.setImage("btnDown", "./assets/scroll/arrowDown.png");
        //add the scrollbar to the parent element
        this.ttScrollBar.appendTo(this.scrollGroup);
        window.scrollTest = this;
    }
    ScrollTest.prototype.updateTextArea = function (percent) {
        if (this.textArea) {
            var textAreaHeight = this.textArea.scrollHeight - this.textArea.clientHeight;
            var px = textAreaHeight / 100 * percent;
            this.textArea.scrollTo(0, px);
        }
    };
    return ScrollTest;
}());
export { ScrollTest };
//# sourceMappingURL=ScrollTest.js.map