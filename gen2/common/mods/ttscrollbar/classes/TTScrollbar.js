var TTScrollbar = /** @class */ (function () {
    function TTScrollbar(scrollTarget, addTo, callback) {
        //height of the  track and knob
        this.knobHeight = 0;
        this.trackHeight = 0;
        //keep track of the scroll position and the last good scroll position
        //if the user scrolls to the top or bottom of the scrollable element
        //we don't want to scroll the scrollable element any further
        this.scrollValue = 0;
        this.lastGoodPosition = 0;
        //increments for the up and down buttons
        this.increment = 5;
        this.debug = false;
        window.ttScrollBar = this;
        this.scrollTarget = scrollTarget;
        this.callback = callback;
        this.element = document.createElement("div");
        this.element.className = "tt-scrollbar";
        console.log("scrollTarget", scrollTarget);
        //set the height of the scrollbar to the height of the scrollable element
        if (addTo) {
            this.element.style.height = addTo.clientHeight + "px";
        }
        //make the html for the scrollbar
        this.element.innerHTML = "<div class='tt-scrollbar top-button'></div>\n        <div class='tt-scrollbar track'><div class=\"tt-scrollbar knob\"></div></div>        \n        <div class=\"tt-scrollbar bottom-button\"></div>";
        //extract the elements from the html
        this.knob = this.element.getElementsByClassName("tt-scrollbar knob")[0];
        this.track = this.element.getElementsByClassName("tt-scrollbar track")[0];
        this.upButton = this.element.getElementsByClassName("tt-scrollbar top-button")[0];
        this.downButton = this.element.getElementsByClassName("tt-scrollbar bottom-button")[0];
        //set the functions to use as event listeners
        this.knobDown = this.onKnobDown.bind(this);
        this.knobUp = this.onKnobUp.bind(this);
        this.knobMove = this.onKnobMove.bind(this);
        this.trackDown = this.onTrackDown.bind(this);
        this.upButtonClicked = this.onUpButtonClicked.bind(this);
        this.downButtonClicked = this.onDownButtonClicked.bind(this);
        this.mouseWheelTurned = this.onMouseWheel.bind(this);
        //add the scrollbar to the parent element
        if (addTo) {
            addTo.appendChild(this.element);
            this.setListeners();
        }
        //add a listener to the target to check if the user has scrolled the target
        //and update the scrollbar position
        //  
    }
    TTScrollbar.prototype.onTargetScroll = function () {
        this.knobHeight = this.knob.getBoundingClientRect().height;
        this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
        //convert the scroll position of the target to a scroll position for the scrollbar
        console.log("scrollTarget", this.scrollTarget);
        this.scrollValue = this.scrollTarget.scrollTop / (this.scrollTarget.scrollHeight - this.scrollTarget.clientHeight) * this.trackHeight;
        console.log("onTargetScroll", this.scrollValue);
        this.knob.style.top = this.scrollValue + "px";
    };
    TTScrollbar.prototype.setListeners = function () {
        //add the event listeners
        console.log("target", this.scrollTarget);
        this.removeListeners();
        this.knob.addEventListener("mousedown", this.knobDown);
        this.downButton.addEventListener("click", this.downButtonClicked);
        this.upButton.addEventListener("click", this.upButtonClicked);
        this.track.addEventListener("mousedown", this.trackDown);
        //check if device is a touch device
        if (window.ontouchstart !== undefined) {
            //alert("touch");
            this.knob.addEventListener("touchstart", this.knobDown);
            this.downButton.addEventListener("touchstart", this.downButtonClicked);
            this.upButton.addEventListener("touchstart", this.upButtonClicked);
            this.track.addEventListener("touchstart", this.trackDown);
            this.scrollTarget.addEventListener("scroll", this.onTargetScroll.bind(this));
        }
        else {
            this.knob.addEventListener("mousedown", this.knobDown);
            this.downButton.addEventListener("mousedown", this.downButtonClicked);
            this.upButton.addEventListener("mousedown", this.upButtonClicked);
            this.track.addEventListener("mousedown", this.trackDown);
            this.scrollTarget.addEventListener("mousewheel", this.mouseWheelTurned);
            this.element.addEventListener("mousewheel", this.mouseWheelTurned);
        }
        //record the height of the track and the knob
        this.knobHeight = this.knob.getBoundingClientRect().height;
        this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
    };
    TTScrollbar.prototype.removeListeners = function () {
        this.knob.removeEventListener("mousedown", this.knobDown);
        document.removeEventListener("mouseup", this.knobUp);
        document.removeEventListener("mousemove", this.knobMove);
        this.downButton.removeEventListener("click", this.downButtonClicked);
        this.upButton.removeEventListener("click", this.upButtonClicked);
        this.track.removeEventListener("mousedown", this.trackDown);
        this.scrollTarget.removeEventListener("mousewheel", this.mouseWheelTurned);
        this.element.removeEventListener("mousewheel", this.mouseWheelTurned);
    };
    TTScrollbar.prototype.appendTo = function (parent) {
        this.element.style.height = parent.clientHeight + "px";
        parent.appendChild(this.element);
        this.setListeners();
    };
    TTScrollbar.prototype.prependTo = function (parent) {
        this.element.style.height = parent.clientHeight + "px";
        parent.prepend(this.element);
        this.setListeners();
    };
    TTScrollbar.prototype.addClass = function (className) {
        this.element.classList.add(className);
    };
    TTScrollbar.prototype.removeClass = function (className) {
        this.element.classList.remove(className);
    };
    TTScrollbar.prototype.setImage = function (key, image) {
        // this.knob.style.backgroundImage = "url(" + image + ")";
        switch (key) {
            case "btnUp":
                this.upButton.style.backgroundImage = "url(" + image + ")";
                break;
            case "btnDown":
                this.downButton.style.backgroundImage = "url(" + image + ")";
                break;
            case "track":
                this.track.style.backgroundImage = "url(" + image + ")";
                break;
            case "knob":
                this.knob.style.backgroundImage = "url(" + image + ")";
                break;
        }
        this.knobHeight = this.knob.getBoundingClientRect().height;
        this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
    };
    TTScrollbar.prototype.onDestroy = function () {
        this.removeListeners();
        this.element.remove();
    };
    TTScrollbar.prototype.convertPxToPercent = function (px) {
        //take out the px
        var pxNum = parseFloat(px.substring(0, px.length - 2));
        return pxNum / this.trackHeight * 100;
    };
    TTScrollbar.prototype.convertToPercent = function (value) {
        return value / this.trackHeight * 100;
    };
    TTScrollbar.prototype.setScroll = function (scroll) {
        if (scroll < 0) {
            scroll = 0;
        }
        if (scroll > 100) {
            scroll = 100;
        }
        var pos = (this.trackHeight / 100) * scroll;
        this.knob.style.top = pos + "px";
        this.validateScrollValue();
        this.updateTarget();
    };
    TTScrollbar.prototype.onTrackDown = function (e) {
        var pos = e.clientY - this.track.getBoundingClientRect().top;
        this.knob.style.top = pos + "px";
        this.validateScrollValue();
        this.updateTarget();
    };
    TTScrollbar.prototype.onUpButtonClicked = function (e) {
        this.setScroll(this.scrollValue - this.increment);
        this.updateTarget();
    };
    TTScrollbar.prototype.onDownButtonClicked = function (e) {
        this.setScroll(this.scrollValue + this.increment);
        this.updateTarget();
    };
    TTScrollbar.prototype.onKnobDown = function (e) {
        e = e || window.event;
        e.preventDefault();
        this.knob.removeEventListener("mousedown", this.knobDown);
        document.addEventListener("mouseup", this.knobUp);
        document.addEventListener("mousemove", this.knobMove);
    };
    TTScrollbar.prototype.onKnobUp = function (e) {
        e = e || window.event;
        e.preventDefault();
        document.removeEventListener("mouseup", this.knobUp);
        document.removeEventListener("mousemove", this.knobMove);
        this.knob.addEventListener("mousedown", this.knobDown);
        this.scrollValue = this.convertPxToPercent(this.knob.style.top);
    };
    TTScrollbar.prototype.onKnobMove = function (e) {
        e = e || window.event;
        e.preventDefault();
        //move the knob
        this.lastGoodPosition = this.scrollValue;
        var knobPos = e.clientY - this.knob.offsetHeight;
        knobPos -= this.track.getBoundingClientRect().top;
        this.knob.style.top = knobPos + "px";
        // this.knob.style.top = (e.clientY - this.knob.offsetHeight) + "px";
        this.validateScrollValue();
        this.updateTarget();
    };
    TTScrollbar.prototype.onMouseWheel = function (e) {
        e = e || window.event;
        e.preventDefault();
        if (e.deltaY > 0) {
            this.setScroll(this.scrollValue + this.increment);
        }
        else {
            this.setScroll(this.scrollValue - this.increment);
        }
        this.updateTarget();
    };
    TTScrollbar.prototype.validateScrollValue = function () {
        this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
        this.scrollValue = this.convertPxToPercent(this.knob.style.top);
        if (this.debug === true) {
            console.log("scroll value: " + this.scrollValue);
        }
        if (this.scrollValue < 0 || this.scrollValue > 101) {
            this.scrollValue = this.lastGoodPosition;
            this.setScroll(this.lastGoodPosition);
            return;
            // this.knob.style.top=(this.lastGoodPosition/100*this.trackHeight)+"px";
        }
        this.lastGoodPosition = this.scrollValue;
    };
    TTScrollbar.prototype.updateTarget = function () {
        this.callback(this.scrollValue);
    };
    TTScrollbar.prototype.setScrollbarWidthModifier = function (mod) {
        document.documentElement.style.setProperty("--scrollbarModificator", mod.toString());
    };
    TTScrollbar.prototype.addCssToHead = function () {
        if (document.getElementById("scrollbarStyle") == null) {
            var style = document.createElement("style");
            style.id = "scrollbarStyle";
            style.innerHTML = "html{--vw:1vw;--scrollbarModificator:2;--scrollbarTrackModificator:3;--scrollbarWidth:calc(var(--vw) * var(--scrollbarModificator));--buttonHeight:var(--scrollbarWidth);--trackWidth:calc(var(--scrollbarWidth) / var(--scrollbarTrackModificator));--thumbWidth:calc(var(--scrollbarWidth) / var(--scrollbarTrackModificator)*2)}.tt-scrollbar{width:var(--scrollbarWidth);height:10px;max-height:100%;top:0;right:0;cursor:pointer}.tt-scrollbar .track{width:var(--trackWidth);height:calc(100% - var(--buttonHeight) * 2);margin:0 calc(var(--scrollbarWidth) / 2 - var(--trackWidth) / 2);background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%;cursor:pointer}.tt-scrollbar .knob{position:relative;width:var(--thumbWidth);height:var(--thumbWidth);top:0;left:-50%;cursor:pointer;background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%}.tt-scrollbar .top-button{width:var(--scrollbarWidth);height:var(--buttonHeight);background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%;cursor:pointer}.tt-scrollbar .bottom-button{width:var(--scrollbarWidth);height:var(--buttonHeight);background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%;cursor:pointer}@media (min-aspect-ratio:96/55){html{--vw:calc(1vh * 96 / 55)}}";
            document.head.appendChild(style);
        }
    };
    return TTScrollbar;
}());
export { TTScrollbar };
//# sourceMappingURL=TTScrollbar.js.map