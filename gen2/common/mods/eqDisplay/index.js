/* import { EqDisplay } from "./classes/displays/eqDisplay";
import "./css/mathStyles.css";

if (!module.exports) {
    window.addEventListener("load", () => {

        let currentStep = 0;
        const samples = document.querySelector("#samples")!;
        const ltis = ["GA.A", "GA.A2", "GA.S2", "GA.S3", "GA.S4", "GA.F2", "GA.D", "GA.M", "GA.M2", "GA.M3"];
    //   const ltis = ["GA.S4"];
        const eqDisplays: Array<EqDisplay> = [];

        ltis.forEach((lti, index) => {
            const sample = document.createElement("div") as HTMLElement;
            sample.classList.add("sample");
            sample.innerHTML = `
                <strong>${lti}</strong>
                <div></div>
            `;
            samples.appendChild(sample);

            let eq:EqDisplay = new EqDisplay(sample.querySelector("div")!);
            eq.loadFile(lti);

            eqDisplays.push(eq);
        });


        function showStep(step: number) {
            eqDisplays.forEach(eq => eq.showStep(step));

            // show the step number
            controls.querySelector("span")!.innerText = step.toString();
        }

        const controls = document.querySelector(".controls")!;
        controls.addEventListener("click", (event: Event) => {
            const element = event.target as HTMLElement;
            if (element.tagName.toLowerCase() === "button") {
                if (element.classList.contains("controls__prev")) {
                    currentStep--;
                }
                else {
                    currentStep++;
                }

                if (currentStep < 0) {
                    currentStep = 0;
                }

                showStep(currentStep);
            }
        });

        showStep(currentStep);
    });
} */
export { EqDisplay } from "./classes/displays/eqDisplay";
//# sourceMappingURL=index.js.map