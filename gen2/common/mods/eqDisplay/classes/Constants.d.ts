export declare enum ProblemTypes {
    UNSUPORTED = 0,
    ADDITION = 1,
    SUBTRACTION = 2,
    MULTIPLICATION = 3,
    DIVISION = 4,
    FRACTIONS = 5
}
export declare const Unicodes: {
    [type: string]: string;
};
