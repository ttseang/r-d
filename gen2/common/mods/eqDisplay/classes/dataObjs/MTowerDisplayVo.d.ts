import { CellVo } from "./CellVo";
export declare class MTowerDisplayVo {
    top: CellVo[][];
    rows: CellVo[][];
    operator: string;
    products: CellVo[][];
    carryRow: CellVo[];
    answer: CellVo[];
    constructor(top: CellVo[][], rows: CellVo[][], products: CellVo[][], carryRow: CellVo[], answer: CellVo[], operator: string);
    fromObj(data: any): void;
}
