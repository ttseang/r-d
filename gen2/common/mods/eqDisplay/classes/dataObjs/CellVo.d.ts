export declare class CellVo {
    text: string;
    classArray: string[];
    constructor(text: string, classArray: string[]);
    toHtml(): string;
}
