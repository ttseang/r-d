var CellVo = /** @class */ (function () {
    function CellVo(text, classArray) {
        this.text = "";
        this.text = text;
        this.classArray = classArray;
    }
    CellVo.prototype.toHtml = function () {
        var cssClasses = this.classArray.slice();
        // TODO
        var index = cssClasses.findIndex(function (css) { return css === "bb"; });
        if (index >= 0) {
            cssClasses.splice(index, 1);
        }
        return "<span ".concat(cssClasses.length ? "class=\"".concat(cssClasses.join(" "), "\"") : "", ">").concat(this.text, "</span>");
    };
    return CellVo;
}());
export { CellVo };
//# sourceMappingURL=CellVo.js.map