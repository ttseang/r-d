import { CellVo } from "./CellVo";
export declare class DivideDisplayVo {
    quotient: CellVo[];
    divisor: CellVo[];
    dividend: CellVo[];
    products: CellVo[][];
    remain: CellVo[];
    constructor(quotient: CellVo[], divisor: CellVo[], dividend: CellVo[], products: CellVo[][], remain: CellVo[]);
    fromObj(data: any): void;
}
