import { CellVo } from "./CellVo";
export declare class FractCellVo {
    top: CellVo[];
    bottom: CellVo[];
    whole: CellVo[];
    t: number;
    w: number;
    b: number;
    constructor(top?: CellVo[], bottom?: CellVo[], whole?: CellVo[]);
    private cellsToNumber;
    getLen(): number;
    getTop(): string;
    getBottom(): string;
    getWhole(): string;
}
