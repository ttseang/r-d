import { CellUtil } from "../util/CellUtil";
var DivideDisplayVo = /** @class */ (function () {
    function DivideDisplayVo(quotient, divisor, dividend, products, remain) {
        this.quotient = quotient;
        this.divisor = divisor;
        this.dividend = dividend;
        this.products = products;
        this.remain = remain;
    }
    DivideDisplayVo.prototype.fromObj = function (data) {
        var quotientData = data.quotient;
        var divisorData = data.divisor;
        var dividendData = data.dividend;
        var productData = data.products;
        var remainData = data.remain;
        this.quotient = CellUtil.dataToCells(quotientData);
        this.divisor = CellUtil.dataToCells(divisorData);
        this.dividend = CellUtil.dataToCells(dividendData);
        this.products = CellUtil.gridToCells(productData);
        this.remain = CellUtil.dataToCells(remainData);
    };
    return DivideDisplayVo;
}());
export { DivideDisplayVo };
//# sourceMappingURL=DivideDisplayVo.js.map