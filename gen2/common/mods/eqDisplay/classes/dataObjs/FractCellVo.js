var FractCellVo = /** @class */ (function () {
    function FractCellVo(top, bottom, whole) {
        if (top === void 0) { top = []; }
        if (bottom === void 0) { bottom = []; }
        if (whole === void 0) { whole = []; }
        this.t = 0;
        this.w = 0;
        this.b = 0;
        this.top = top;
        this.bottom = bottom;
        this.whole = whole;
        this.w = this.cellsToNumber(whole);
        this.b = this.cellsToNumber(bottom);
        this.t = this.cellsToNumber(top);
    }
    FractCellVo.prototype.cellsToNumber = function (cells) {
        var numString = "";
        for (var i = 0; i < cells.length; i++) {
            numString += cells[i].text;
        }
        return parseInt(numString);
    };
    FractCellVo.prototype.getLen = function () {
        if (this.w === 0) {
            return 1;
        }
        return 2;
    };
    FractCellVo.prototype.getTop = function () {
        var html = [];
        for (var i = 0; i < this.top.length; i++) {
            //  this.top[i].addClass("numerator");
            html.push(this.top[i].toHtml());
        }
        return html.join("");
    };
    FractCellVo.prototype.getBottom = function () {
        var html = [];
        for (var i = 0; i < this.bottom.length; i++) {
            //  this.bottom[i].addClass("denominator");
            html.push(this.bottom[i].toHtml());
        }
        return html.join("");
    };
    FractCellVo.prototype.getWhole = function () {
        if (this.w === 0) {
            return "";
        }
        var html = [];
        for (var i = 0; i < this.whole.length; i++) {
            //this.whole[i].addClass("whole");
            html.push(this.whole[i].toHtml());
        }
        return html.join("");
    };
    return FractCellVo;
}());
export { FractCellVo };
//# sourceMappingURL=FractCellVo.js.map