import { CellVo } from "./CellVo";
import { FractCellVo } from "./FractCellVo";
export declare class FractDisplayVo {
    fracts: FractCellVo[];
    ops: string[];
    opCells: CellVo[];
    constructor(fracts: FractCellVo[], ops: string[]);
    fromObj(data: any): void;
}
