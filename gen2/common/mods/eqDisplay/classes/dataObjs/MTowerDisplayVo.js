import { CellUtil } from "../util/CellUtil";
var MTowerDisplayVo = /** @class */ (function () {
    function MTowerDisplayVo(top, rows, products, carryRow, answer, operator) {
        this.top = top;
        this.rows = rows;
        this.products = products;
        this.carryRow = carryRow;
        this.operator = operator;
        this.answer = answer;
    }
    MTowerDisplayVo.prototype.fromObj = function (data) {
        //console.log(data);
        var topData = data.top;
        var rowsData = data.rows;
        var answerData = data.answer;
        var productData = data.products;
        var carryData = data.carryRow;
        this.top = CellUtil.gridToCells(topData);
        this.rows = CellUtil.gridToCells(rowsData);
        this.answer = CellUtil.dataToCells(answerData);
        this.products = CellUtil.gridToCells(productData);
        this.operator = data.operator;
        //console.log(this);
    };
    return MTowerDisplayVo;
}());
export { MTowerDisplayVo };
//# sourceMappingURL=MTowerDisplayVo.js.map