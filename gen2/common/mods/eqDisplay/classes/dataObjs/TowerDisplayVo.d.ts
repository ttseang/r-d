import { CellVo } from "./CellVo";
export declare class TowerDisplayVo {
    top: CellVo[][];
    rows: CellVo[][];
    operator: string;
    answer: CellVo[];
    constructor(top: CellVo[][], rows: CellVo[][], answer: CellVo[], operator: string);
    fromObj(data: any): void;
}
