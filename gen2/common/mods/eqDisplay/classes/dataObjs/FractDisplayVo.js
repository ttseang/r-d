import { CellUtil } from "../util/CellUtil";
import { CellVo } from "./CellVo";
import { FractCellVo } from "./FractCellVo";
var FractDisplayVo = /** @class */ (function () {
    function FractDisplayVo(fracts, ops) {
        this.opCells = [];
        this.fracts = fracts;
        this.ops = ops.slice();
        for (var i = 0; i < this.ops.length; i++) {
            this.opCells.push(new CellVo(this.ops[i], ['fop']));
        }
    }
    FractDisplayVo.prototype.fromObj = function (data) {
        // //console.log(data);
        var cells = [];
        for (var i = 0; i < data.fracts.length; i++) {
            var fractData = data.fracts[i];
            var top_1 = CellUtil.dataToCells(fractData.top);
            var bottom = CellUtil.dataToCells(fractData.bottom);
            var whole = CellUtil.dataToCells(fractData.whole);
            var fCellVo = new FractCellVo(top_1, bottom, whole);
            cells.push(fCellVo);
        }
        this.fracts = cells;
        this.ops = data.ops;
        for (var i = 0; i < this.ops.length; i++) {
            this.opCells.push(new CellVo(this.ops[i], ['fop']));
        }
    };
    return FractDisplayVo;
}());
export { FractDisplayVo };
//# sourceMappingURL=FractDisplayVo.js.map