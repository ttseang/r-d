import { CellUtil } from "../util/CellUtil";
var TowerDisplayVo = /** @class */ (function () {
    function TowerDisplayVo(top, rows, answer, operator) {
        this.top = top;
        this.rows = rows;
        this.operator = operator;
        this.answer = answer;
    }
    TowerDisplayVo.prototype.fromObj = function (data) {
        var topData = data.top;
        var rowsData = data.rows;
        var answerData = data.answer;
        this.top = CellUtil.gridToCells(topData);
        this.rows = CellUtil.gridToCells(rowsData);
        this.answer = CellUtil.dataToCells(answerData);
        this.operator = data.operator;
    };
    return TowerDisplayVo;
}());
export { TowerDisplayVo };
//# sourceMappingURL=TowerDisplayVo.js.map