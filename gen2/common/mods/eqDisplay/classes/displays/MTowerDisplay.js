import { Unicodes } from "../Constants";
import { CellUtil } from "../util/CellUtil";
var MTowerDisplay = /** @class */ (function () {
    function MTowerDisplay(data, columns) {
        this.data = data;
        this.columns = columns + 1;
    }
    MTowerDisplay.prototype.getTop = function () {
        var _this = this;
        var topRows = [];
        var top = this.data.top;
        if (top) {
            top.forEach(function (t, index) {
                var cols = CellUtil.makeCols(index, t, _this.columns);
                topRows.push("<div class=\"row small\" style=\"grid-template-columns: repeat(".concat(_this.columns, ", 1fr);\">").concat(cols.join(" "), "</div>"));
            });
        }
        return topRows.join("\n");
    };
    MTowerDisplay.prototype.getRows = function () {
        var _this = this;
        var rowArray = [];
        var rows = this.data.rows;
        if (rows) {
            rows.forEach(function (row, index) {
                var cols = CellUtil.makeCols(index, row, _this.columns);
                // THIS IS THE HACK FOR SPACING!
                if (index === rows.length - 1) {
                    cols[0] = "<span>&nbsp;".concat(Unicodes["x"], "&nbsp;</span>");
                }
                rowArray.push("<div class=\"row\" style=\"grid-template-columns: repeat(".concat(_this.columns, ", 1fr)\">").concat(cols.join(" "), "</div>"));
            });
        }
        return rowArray.join("\n");
    };
    MTowerDisplay.prototype.getProducts = function () {
        var _this = this;
        var productRows = [];
        var products = this.data.products;
        if (products) {
            products.forEach(function (product, index) {
                var cols = CellUtil.makeCols(index, product, _this.columns);
                productRows.push("<div class=\"row\" style=\"grid-template-columns: repeat(".concat(_this.columns, ", 1fr)\">").concat(cols.join(" "), "</div>"));
            });
        }
        return productRows.join("\n");
    };
    MTowerDisplay.prototype.getCarryRow = function () {
        var carryRow = this.data.carryRow;
        if (carryRow.length) {
            var cols = CellUtil.makeCols(30, carryRow, this.columns);
            return "<div class=\"row\" style=\"grid-template-columns: repeat(".concat(this.columns, ", 1fr)\">").concat(cols.join(" "), "</div>");
        }
        return "";
    };
    MTowerDisplay.prototype.getAnswer = function () {
        var _a = this.data, answer = _a.answer, products = _a.products;
        if (answer) {
            if (answer.length === 0) {
                return "";
            }
            if (products) {
                var cols = CellUtil.makeCols(20, answer, this.columns);
                return "\n                    ".concat(products.length ? "<hr />" : "", "\n                    <div class=\"row answer\" style=\"grid-template-columns: repeat(").concat(this.columns, ", 1fr)\">\n                        ").concat(cols.join(" "), "\n                    </div>\n                ");
            }
        }
        return "";
    };
    MTowerDisplay.prototype.getHtml = function () {
        return "\n            <div class=\"equation multiply\">\n                ".concat(this.getTop(), "\n                ").concat(this.getRows(), "\n                <hr />\n                ").concat(this.getCarryRow(), "\n                ").concat(this.getProducts(), "\n                ").concat(this.getAnswer(), "\n            </div>\n        ");
    };
    return MTowerDisplay;
}());
export { MTowerDisplay };
//# sourceMappingURL=MTowerDisplay.js.map