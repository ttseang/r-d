import { Unicodes } from "../Constants";
var FractDisplay = /** @class */ (function () {
    function FractDisplay(data) {
        this.data = data;
    }
    FractDisplay.prototype.getLen2 = function () {
        var len = 0;
        var fracts = this.data.fracts;
        fracts.forEach(function (fract) { return len += fract.getLen(); });
        len += fracts.length - 1;
        return len;
    };
    FractDisplay.prototype.getStyle = function () {
        var len = this.getLen2();
        return "grid-template-columns: repeat(".concat(len, ", 1fr)");
    };
    FractDisplay.prototype.getFracts = function () {
        var fArray = [];
        var _a = this.data, fracts = _a.fracts, ops = _a.ops, opCells = _a.opCells;
        fracts.forEach(function (fract, index) {
            if (fract.w > 0) {
                fArray.push("<div>".concat(fract.getWhole(), "</div>"));
            }
            fArray.push("<div>".concat(fract.getTop(), "</div>"));
            if (ops[index]) {
                if (Unicodes[ops[index]]) {
                    opCells[index].text = Unicodes[ops[index]];
                }
                fArray.push("<div>".concat(opCells[index].toHtml(), "</div>"));
            }
        });
        fracts.forEach(function (fract) {
            if (fract.w > 0) {
                fArray.push("<div></div>");
            }
            fArray.push("<div>".concat(fract.getBottom(), "</div>"));
            fArray.push("<div></div>");
        });
        return fArray.join("");
    };
    FractDisplay.prototype.getHtml = function () {
        return "\n            <div class=\"equation fraction\" style=\"".concat(this.getStyle(), "\">\n                ").concat(this.getFracts(), "\n            </div>");
    };
    return FractDisplay;
}());
export { FractDisplay };
//# sourceMappingURL=FractDisplay.js.map