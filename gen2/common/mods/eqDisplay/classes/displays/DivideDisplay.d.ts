import { DivideDisplayVo } from "../dataObjs/DivideDisplayVo";
export declare class DivideDisplay {
    private data;
    constructor(data: DivideDisplayVo);
    private getProductRows;
    toHtml(): string;
}
