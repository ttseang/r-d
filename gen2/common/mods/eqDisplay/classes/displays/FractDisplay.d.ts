import { FractDisplayVo } from "../dataObjs/FractDisplayVo";
export declare class FractDisplay {
    private data;
    constructor(data: FractDisplayVo);
    getLen2(): number;
    getStyle(): string;
    getFracts(): string;
    getHtml(): string;
}
