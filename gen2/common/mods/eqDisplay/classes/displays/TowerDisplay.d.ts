import { TowerDisplayVo } from "../dataObjs/TowerDisplayVo";
export declare class TowerDisplay {
    private data;
    private columns;
    constructor(data: TowerDisplayVo, columns: number);
    getTop(): string;
    getRows(): string;
    getAnswer(): string;
    getHtml(): string;
}
