export declare class EqDisplay {
    private expression;
    private data;
    private problemType;
    private container;
    private steps;
    isReady: boolean;
    onReady: Function;
    step: number;
    private file;
    private cm;
    constructor(container: HTMLElement | string);
    loadFile(file: string, step?: number): void;
    process(data: any): void;
    formatData(data: any): void;
    decideProblem(): boolean;
    showStep(step: number): void;
    getStep(step: number): string;
    makeSteps(): void;
}
