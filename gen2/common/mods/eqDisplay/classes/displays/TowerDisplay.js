import { Unicodes } from "../Constants";
import { CellUtil } from "../util/CellUtil";
var TowerDisplay = /** @class */ (function () {
    function TowerDisplay(data, columns) {
        this.data = data;
        this.columns = columns + 1; // add one for the operator
    }
    TowerDisplay.prototype.getTop = function () {
        var _this = this;
        var topRows = [];
        this.data.top.forEach(function (top, index) {
            var cols = CellUtil.makeCols(index, top, _this.columns);
            topRows.unshift("<div class=\"row small\" style=\"grid-template-columns: repeat(".concat(_this.columns, ", 1fr);\">").concat(cols.join(" "), "</div>"));
        });
        return topRows.join("\n");
    };
    TowerDisplay.prototype.getRows = function () {
        var _this = this;
        var rowArray = [];
        var _a = this.data, rows = _a.rows, operator = _a.operator;
        rows.forEach(function (row, index) {
            var cols = CellUtil.makeCols(index, row, _this.columns);
            // THIS IS THE HACK FOR SPACING!
            if (index === rows.length - 1) {
                cols[0] = "<span>&nbsp;".concat(Unicodes[operator], "&nbsp;</span>");
            }
            rowArray.push("<div class=\"row\" style=\"grid-template-columns: repeat(".concat(_this.columns, ", 1fr)\">").concat(cols.join(" "), "</div>"));
        });
        return rowArray.join("\n");
    };
    TowerDisplay.prototype.getAnswer = function () {
        var cols = CellUtil.makeCols(20, this.data.answer, this.columns);
        return "<div class=\"row answer\" style=\"grid-template-columns: repeat(".concat(this.columns, ", 1fr)\">").concat(cols.join(" "), "</div>");
    };
    TowerDisplay.prototype.getHtml = function () {
        return "\n            <div class=\"equation addition\">\n                ".concat(this.getTop(), "\n                ").concat(this.getRows(), "\n                <hr>\n                ").concat(this.getAnswer(), "\n            </div>\n        ");
    };
    return TowerDisplay;
}());
export { TowerDisplay };
//# sourceMappingURL=TowerDisplay.js.map