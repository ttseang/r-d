import ApiConnect from "../util/ApiConnect";
import { CacheManager } from "@teachingtextbooks/ttcachemanager";
import { ProblemTypes } from "../Constants";
import { DivideDisplayVo } from "../dataObjs/DivideDisplayVo";
import { FractDisplayVo } from "../dataObjs/FractDisplayVo";
import { MTowerDisplayVo } from "../dataObjs/MTowerDisplayVo";
import { TowerDisplayVo } from "../dataObjs/TowerDisplayVo";
import { CSSUtil } from "../util/CSSUtil";
import { DivideDisplay } from "./DivideDisplay";
import { FractDisplay } from "./FractDisplay";
import { MTowerDisplay } from "./MTowerDisplay";
import { TowerDisplay } from "./TowerDisplay";
var EqDisplay = /** @class */ (function () {
    function EqDisplay(container) {
        this.expression = "";
        this.data = {};
        this.problemType = ProblemTypes.UNSUPORTED;
        this.steps = [];
        this.isReady = false;
        this.onReady = function () { };
        this.step = 0;
        this.file = "";
        this.cm = CacheManager.getInstance();
        CSSUtil.makeCSS();
        this.container = typeof container === "string" ? document.querySelector(container) : container;
        window.eqDisplay = this;
    }
    EqDisplay.prototype.loadFile = function (file, step) {
        if (step === void 0) { step = 0; }
        this.isReady = false;
        this.step = step;
        this.file = file;
        var cachedData = this.cm.getFromCache(file, "");
        if (cachedData) {
            this.data = JSON.parse(cachedData);
            this.formatData(this.data);
            return;
        }
        var apiConnect = new ApiConnect();
        apiConnect.getFileContent(file, this.process.bind(this));
    };
    EqDisplay.prototype.process = function (data) {
        this.cm.saveToCache(this.file, JSON.stringify(data), 60000);
        this.data = data;
        this.formatData(this.data);
    };
    EqDisplay.prototype.formatData = function (data) {
        this.expression = data.eqstring;
        var stat = this.decideProblem();
        if (stat === false) {
            if (this.container) {
                this.container.style.width = "fit-content";
                this.container.innerHTML = "Invalid LTI Code";
            }
        }
    };
    EqDisplay.prototype.decideProblem = function () {
        // console.log("decide problem");
        // console.log(this.expression);
        if (this.expression === undefined || this.expression === "") {
            return false;
        }
        var opArray = ["+", "-", "*", "/"];
        var problemTypes = [ProblemTypes.ADDITION, ProblemTypes.SUBTRACTION, ProblemTypes.MULTIPLICATION, ProblemTypes.DIVISION];
        this.problemType = ProblemTypes.UNSUPORTED;
        for (var i = 0; i < opArray.length; i++) {
            if (this.expression.includes(opArray[i])) {
                this.problemType = problemTypes[i];
            }
        }
        if (this.expression.includes("_")) {
            this.problemType = ProblemTypes.FRACTIONS;
        }
        // mark we are ready
        this.isReady = true;
        this.makeSteps();
        this.showStep(this.step);
        this.onReady();
        return true;
    };
    EqDisplay.prototype.showStep = function (step) {
        // it's not ready, so overwrite the step property so when it is ready it will show that step
        if (!this.isReady) {
            this.step = step;
            return;
        }
        this.step = step;
        if (this.container) {
            this.container.style.width = "fit-content";
            var content = this.getStep(step);
            if (content === undefined) {
                content = "Step Not Found";
            }
            this.container.innerHTML = content;
        }
        else {
            console.warn("DIV NOT FOUND");
        }
    };
    EqDisplay.prototype.getStep = function (step) {
        return this.steps[step];
    };
    EqDisplay.prototype.makeSteps = function () {
        var _this = this;
        this.steps = [];
        // find the max columns
        var maxColumns = 0;
        if (this.problemType === ProblemTypes.ADDITION || this.problemType === ProblemTypes.SUBTRACTION || this.problemType === ProblemTypes.MULTIPLICATION) {
            var checkDecimals_1 = function (columns) {
                columns.reverse();
                for (var i = 0; i < columns.length; i++) {
                    var text = columns[i]._text;
                    if (text === "." || text === ",") {
                        var symClass = (text === "." ? "decimal" : "comma");
                        columns[i + 1].classArray.push(symClass);
                        columns.splice(i, 1); // remove it
                        i--; // back track
                    }
                }
                columns.reverse();
            };
            this.data.data.forEach(function (data) {
                data.top.forEach(function (top) {
                    checkDecimals_1(top);
                    maxColumns = Math.max(maxColumns, top.length);
                });
                data.rows.forEach(function (row) {
                    checkDecimals_1(row);
                    maxColumns = Math.max(maxColumns, row.length);
                });
                // multiplication
                (data.products || []).forEach(function (product) {
                    checkDecimals_1(product);
                    maxColumns = Math.max(maxColumns, product.length);
                });
                checkDecimals_1(data.answer);
                maxColumns = Math.max(maxColumns, data.answer.length);
            });
            // the max number of columns
            this.data.maxColumns = maxColumns;
        }
        switch (this.problemType) {
            case ProblemTypes.ADDITION:
            case ProblemTypes.SUBTRACTION: {
                this.data.data.forEach(function (data) {
                    var towerData = new TowerDisplayVo([], [], [], "");
                    towerData.fromObj(data);
                    var td = new TowerDisplay(towerData, maxColumns);
                    _this.steps.push("<div class=\"tteq\">".concat(td.getHtml(), "</div>"));
                });
                break;
            }
            case ProblemTypes.MULTIPLICATION: {
                this.data.data.forEach(function (data) {
                    var mTowerData = new MTowerDisplayVo([], [], [], [], [], "");
                    mTowerData.fromObj(data);
                    var td = new MTowerDisplay(mTowerData, maxColumns);
                    _this.steps.push("<div class=\"tteq\">".concat(td.getHtml(), "</div>"));
                });
                break;
            }
            case ProblemTypes.DIVISION: {
                this.data.data.forEach(function (data) {
                    var divData = new DivideDisplayVo([], [], [], [], []);
                    divData.fromObj(data);
                    var dd = new DivideDisplay(divData);
                    _this.steps.push("<div class=\"tteq\">".concat(dd.toHtml(), "</div>"));
                });
                break;
            }
            case ProblemTypes.FRACTIONS: {
                this.data.data.forEach(function (data) {
                    var fractDisplayVo = new FractDisplayVo([], []);
                    fractDisplayVo.fromObj(data);
                    var fractDisplay = new FractDisplay(fractDisplayVo);
                    _this.steps.push("<div class=\"tteq\">".concat(fractDisplay.getHtml(), "</div>"));
                });
                break;
            }
        }
    };
    return EqDisplay;
}());
export { EqDisplay };
//# sourceMappingURL=eqDisplay.js.map