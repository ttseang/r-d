import { MTowerDisplayVo } from "../dataObjs/MTowerDisplayVo";
export declare class MTowerDisplay {
    private data;
    private columns;
    constructor(data: MTowerDisplayVo, columns: number);
    getTop(): string;
    getRows(): string;
    getProducts(): string;
    getCarryRow(): string;
    getAnswer(): string;
    getHtml(): string;
}
