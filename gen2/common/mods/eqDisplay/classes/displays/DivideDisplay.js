import { CellUtil } from "../util/CellUtil";
var DivideDisplay = /** @class */ (function () {
    function DivideDisplay(data) {
        this.data = data;
    }
    DivideDisplay.prototype.getProductRows = function () {
        var rows = [];
        this.data.products.forEach(function (product, index) {
            // TODO - need to figure out the hidden?
            var bottom = !(index % 2);
            rows.push("<div></div>");
            rows.push("<div>&nbsp;&nbsp;&nbsp;".concat((bottom ? "<span class=\"bottomBorder\">" : "") + CellUtil.makeCols(index, product, 0).join("\n") + (bottom ? "</span>" : ""), "</div>"));
        });
        return rows.join("\n");
    };
    DivideDisplay.prototype.toHtml = function () {
        var _a = this.data, quotient = _a.quotient, divisor = _a.divisor, dividend = _a.dividend;
        return "\n            <div class=\"equation division\">\n                <div></div>\n\n                <div class=\"quotient\">&nbsp;&nbsp;&nbsp;".concat(CellUtil.makeCols(0, quotient, 0), "</div>\n                <div class=\"divisor\">").concat(CellUtil.makeCols(0, divisor, 0).join("\n"), "&nbsp;</div>\n                <div class=\"dividend\">\n                    <div class=\"inner\">\n                        <i class=\"circle\"></i>\n                        &nbsp;&nbsp;&nbsp;").concat(CellUtil.makeCols(0, dividend, 0).join("\n"), "\n\n                    </div>\n                </div>\n                ").concat(this.getProductRows(), "\n            </div>\n        ");
    };
    return DivideDisplay;
}());
export { DivideDisplay };
//# sourceMappingURL=DivideDisplay.js.map