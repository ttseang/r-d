var StrUtil = /** @class */ (function () {
    function StrUtil() {
    }
    StrUtil.getNextID = function () {
        StrUtil.index++;
        return "customID_".concat(StrUtil.index);
    };
    StrUtil.index = 0;
    return StrUtil;
}());
export { StrUtil };
//# sourceMappingURL=StrUtil.js.map