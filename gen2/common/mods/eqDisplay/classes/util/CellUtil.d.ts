import { CellVo } from "../dataObjs/CellVo";
export declare class CellUtil {
    static makeCols(index: number, cells: CellVo[], columnCount: number): string[];
    static dataToCell(data: any): CellVo;
    static dataToCells(data: any[]): CellVo[];
    static gridToCells(data: any[][]): CellVo[][];
}
