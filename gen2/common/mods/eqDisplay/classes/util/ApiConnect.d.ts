export declare class ApiConnect {
    endPoint: string;
    fetchWithTimeout(url: string, options?: any): Promise<Response>;
    getFileContent(lti: string, callback: Function): void;
}
export default ApiConnect;
