import { CellVo } from "../dataObjs/CellVo";
var CellUtil = /** @class */ (function () {
    function CellUtil() {
    }
    CellUtil.makeCols = function (index, cells, columnCount) {
        var cols = [];
        cells.forEach(function (cell) { return cols.push(cell.toHtml()); });
        while (cols.length < columnCount) {
            cols.unshift("<span>&nbsp;</span>");
        }
        return cols;
    };
    CellUtil.dataToCell = function (data) {
        return new CellVo(data._text, data.classArray);
    };
    CellUtil.dataToCells = function (data) {
        var cells = [];
        data.forEach(function (d) { return cells.push(CellUtil.dataToCell(d)); });
        return cells;
    };
    CellUtil.gridToCells = function (data) {
        var cells = [];
        data.forEach(function (d1, i) {
            cells[i] = [];
            d1.forEach(function (d2, j) {
                cells[i].push(CellUtil.dataToCell(d2));
            });
        });
        return cells;
    };
    return CellUtil;
}());
export { CellUtil };
//# sourceMappingURL=CellUtil.js.map