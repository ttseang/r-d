export var ProblemTypes;
(function (ProblemTypes) {
    ProblemTypes[ProblemTypes["UNSUPORTED"] = 0] = "UNSUPORTED";
    ProblemTypes[ProblemTypes["ADDITION"] = 1] = "ADDITION";
    ProblemTypes[ProblemTypes["SUBTRACTION"] = 2] = "SUBTRACTION";
    ProblemTypes[ProblemTypes["MULTIPLICATION"] = 3] = "MULTIPLICATION";
    ProblemTypes[ProblemTypes["DIVISION"] = 4] = "DIVISION";
    ProblemTypes[ProblemTypes["FRACTIONS"] = 5] = "FRACTIONS";
})(ProblemTypes || (ProblemTypes = {}));
;
export var Unicodes = {
    "+": "\u002B",
    "-": "\u2212",
    "x": "\u00D7",
    "*": "\u2022",
    "<": "\u003C",
    ">": "\u003E",
    "=": "\u003D"
};
//# sourceMappingURL=Constants.js.map