"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.IconButton = void 0;
var svggame_1 = require("svggame");
var IconButton = /** @class */ (function (_super) {
    __extends(IconButton, _super);
    function IconButton(scene, key, action, params, callback) {
        var _this = _super.call(this, scene, key) || this;
        _this.action = "";
        _this.params = "";
        _this.scene = scene;
        _this.callback = callback;
        _this.action = action;
        _this.params = params;
        if (_this.el) {
            _this.el.onclick = function () {
                _this.callback(_this.action, _this.params);
            };
        }
        return _this;
    }
    return IconButton;
}(svggame_1.SvgObj));
exports.IconButton = IconButton;
//# sourceMappingURL=IconButton.js.map