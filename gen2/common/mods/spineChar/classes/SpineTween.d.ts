import { SpineChar } from "./SpineChar";
import { SpineTweenObj } from "./SpineTweenObj";
export declare class SpineTween {
    private gameObj;
    private tweenObj;
    private autoStart;
    /**
     * Targets
     */
    private xTarget;
    private yTarget;
    private scaleXTarget;
    private scaleYTarget;
    /**
     * Increments
     *  */
    private xInc;
    private yInc;
    private scaleXInc;
    private scaleYInc;
    /**
     * Timer
     */
    private myTimer;
    private myTime;
    constructor(gameObj: SpineChar, tweenObj: SpineTweenObj, autoStart?: boolean);
    start(): void;
    private doStep;
}
