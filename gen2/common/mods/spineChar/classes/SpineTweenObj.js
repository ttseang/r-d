"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpineTweenObj = void 0;
var SpineTweenObj = /** @class */ (function () {
    function SpineTweenObj() {
        this.x = null;
        this.y = null;
        this.scaleX = null;
        this.scaleY = null;
        this.duration = 1000;
        this.onComplete = function () { };
    }
    return SpineTweenObj;
}());
exports.SpineTweenObj = SpineTweenObj;
//# sourceMappingURL=SpineTweenObj.js.map