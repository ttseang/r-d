"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpineChar = void 0;
//import { SpinePlayer, Vector3 } from "@esotericsoftware/spine-player";
var spine_player_1 = require("../../node_modules/@esotericsoftware/spine-player");
var SpineTween_1 = require("./SpineTween");
var SpineTweenObj_1 = require("./SpineTweenObj");
var SpineChar = /** @class */ (function () {
    function SpineChar(scene, divID, jsonUrl, atlasUrl, animations) {
        //let config:SpinePlayerConfig={};
        this.config = {};
        this._x = 0;
        this._y = 0;
        this._scaleX = 1;
        this._scaleY = 1;
        this.realH = 3000;
        this.realW = 6000;
        this._flip = false;
        this.currentPos = null;
        this.el = null;
        this.scene = scene;
        this.divID = divID;
        this.animations = animations;
        this.config.atlasUrl = atlasUrl;
        this.config.jsonUrl = jsonUrl;
        this.config.viewport = {
            x: 0,
            y: 0,
            width: this.scene.gw,
            height: this.scene.gh
        };
        // this.config.atlasUrl = "./assets/Penny Loafer.atlas";
        //this.config.jsonUrl = "./assets/Penny Loafer.json";
        this.config.showControls = false;
        this.config.premultipliedAlpha = true;
        this.config.alpha = true;
        this.config.success = this.onSuccess.bind(this);
        this.el = document.getElementById(divID);
        if (this.el) {
            this.el.style.visibility = "hidden";
        }
        //console.log(this.config);
        this.player = new spine_player_1.SpinePlayer(divID, this.config);
    }
    SpineChar.prototype.getPosition = function (xx, yy, zz) {
        var w2s = this.player.sceneRenderer.camera.screenToWorld(new spine_player_1.Vector3(xx, yy, zz), this.scene.gw, this.scene.gh);
        return w2s;
    };
    SpineChar.prototype.centerMe = function () {
        var pos = this.getPosition(this.scene.gw / 2, this.scene.gh / 2, 0);
        this.x = pos.x;
        this.y = pos.y;
    };
    SpineChar.prototype.placeOnGrid = function (col, row) {
        if (this.scene.grid) {
            var xx = col * this.scene.grid.cw;
            var yy = row * this.scene.grid.ch;
            var pos = this.getPosition(xx, yy, 0);
            this.x = pos.x;
            this.y = pos.y;
        }
    };
    SpineChar.prototype.scaleToGameW = function (per) {
        var desiredWidth = this.scene.gw * per;
        if (this.player.skeleton) {
            var skelWidth = this.player.skeleton.data.width;
            var p2 = desiredWidth / skelWidth;
            this.scaleX = p2;
            this.scaleY = p2;
        }
    };
    SpineChar.prototype.scaleToGameH = function (per) {
        var desiredHeight = this.scene.gw * per;
        if (this.player.skeleton) {
            var skelHeight = this.player.skeleton.data.height;
            var p2 = desiredHeight / skelHeight;
            this.scaleX = p2;
            this.scaleY = p2;
        }
    };
    SpineChar.prototype.adjustY = function () {
        var displayHeight = this.realH * this.scaleY;
        this.y -= displayHeight;
    };
    SpineChar.prototype.adjustX = function () {
        var displayWidth = this.realW * this.scaleX;
        this.x -= displayWidth / 2;
    };
    SpineChar.prototype.onSuccess = function () {
        var _this = this;
        this.player.play();
        for (var i = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }
        setTimeout(function () {
            //trigger setters
            _this.flip = _this._flip;
            _this.x = _this._x;
            _this.y = _this._y;
            if (_this.currentPos != null) {
                _this.placeOnGrid(_this.currentPos.x, _this.currentPos.y);
            }
            _this.scaleY = _this._scaleY;
            _this.scaleX = _this._scaleX;
            if (_this.el) {
                _this.el.style.visibility = "visible";
            }
            _this.scene.doResize();
        }, 1000);
    };
    SpineChar.prototype.setScale = function (scale) {
        this.scaleX = scale;
        this.scaleY = scale;
    };
    SpineChar.prototype.setAnimations = function (animations) {
        this.animations = animations;
        for (var i = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }
    };
    Object.defineProperty(SpineChar.prototype, "x", {
        get: function () {
            return this._x;
        },
        set: function (value) {
            this._x = value;
            if (this.player) {
                if (this.player.skeleton) {
                    this.player.skeleton.x = value;
                }
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpineChar.prototype, "y", {
        get: function () {
            return this._y;
        },
        set: function (value) {
            this._y = value;
            if (this.player) {
                if (this.player.skeleton) {
                    //let sy:number=value-this.realH/2;
                    this.player.skeleton.y = value;
                }
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpineChar.prototype, "scaleX", {
        get: function () {
            return this._scaleX;
        },
        set: function (value) {
            this._scaleX = value;
            if (this.player) {
                if (this.player.skeleton) {
                    var v = value;
                    if (this._flip == true) {
                        v = -v;
                    }
                    this.player.skeleton.scaleX = v;
                }
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpineChar.prototype, "scaleY", {
        get: function () {
            return this._scaleY;
        },
        set: function (value) {
            this._scaleY = value;
            if (this.player) {
                if (this.player.skeleton) {
                    this.player.skeleton.scaleY = value;
                }
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpineChar.prototype, "flip", {
        get: function () {
            return this._flip;
        },
        set: function (value) {
            this._flip = value;
            this.scaleX = this._scaleX;
        },
        enumerable: false,
        configurable: true
    });
    SpineChar.prototype.moveTo = function (xx, yy, duration, animations, endAnimations, flipOnEnd) {
        var _this = this;
        if (duration === void 0) { duration = 1000; }
        if (flipOnEnd === void 0) { flipOnEnd = false; }
        var tweenObj = new SpineTweenObj_1.SpineTweenObj();
        tweenObj.x = xx;
        tweenObj.y = yy;
        tweenObj.duration = duration;
        this.setAnimations(animations);
        tweenObj.onComplete = function () {
            _this.setAnimations(endAnimations);
            if (flipOnEnd == true) {
                _this.flip = !_this.flip;
            }
        };
        var tween = new SpineTween_1.SpineTween(this, tweenObj, true);
    };
    return SpineChar;
}());
exports.SpineChar = SpineChar;
//# sourceMappingURL=SpineChar.js.map