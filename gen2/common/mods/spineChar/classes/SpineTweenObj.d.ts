export declare class SpineTweenObj {
    x: number | null;
    y: number | null;
    scaleX: number | null;
    scaleY: number | null;
    duration: number;
    onComplete: Function;
    constructor();
}
