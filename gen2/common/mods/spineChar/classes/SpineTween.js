"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpineTween = void 0;
var SpineTween = /** @class */ (function () {
    function SpineTween(gameObj, tweenObj, autoStart) {
        if (autoStart === void 0) { autoStart = true; }
        /**
         * Increments
         *  */
        this.xInc = 0;
        this.yInc = 0;
        this.scaleXInc = 0;
        this.scaleYInc = 0;
        /**
         * Timer
         */
        this.myTimer = null;
        this.myTime = 0;
        this.gameObj = gameObj;
        this.tweenObj = tweenObj;
        this.autoStart = autoStart;
        //
        //
        this.xTarget = gameObj.x;
        this.yTarget = gameObj.y;
        this.scaleXTarget = gameObj.scaleX;
        this.scaleYTarget = gameObj.scaleY;
        if (tweenObj.x != null) {
            this.xTarget = tweenObj.x;
        }
        if (tweenObj.y != null) {
            this.yTarget = tweenObj.y;
        }
        if (tweenObj.scaleX != null) {
            this.scaleXTarget = tweenObj.scaleX;
        }
        if (tweenObj.scaleY != null) {
            this.scaleYTarget = tweenObj.scaleY;
        }
        var duration2 = tweenObj.duration / 5;
        this.myTime = tweenObj.duration;
        //
        //
        this.xInc = (this.xTarget - gameObj.x) / duration2;
        this.yInc = (this.yTarget - gameObj.y) / duration2;
        //
        //
        this.scaleXInc = (this.scaleXTarget - gameObj.scaleX) / duration2;
        this.scaleYInc = (this.scaleYTarget - gameObj.scaleY) / duration2;
        //
        //
        if (autoStart == true) {
            this.start();
        }
    }
    SpineTween.prototype.start = function () {
        this.myTimer = setInterval(this.doStep.bind(this), 5);
    };
    SpineTween.prototype.doStep = function () {
        this.myTime -= 5;
        if (this.myTime < 0) {
            this.gameObj.x = this.xTarget;
            this.gameObj.y = this.yTarget;
            clearInterval(this.myTimer);
            this.tweenObj.onComplete(this.gameObj);
            return;
        }
        this.gameObj.x += this.xInc;
        this.gameObj.y += this.yInc;
        this.gameObj.scaleX += this.scaleXInc;
        this.gameObj.scaleY += this.scaleYInc;
    };
    return SpineTween;
}());
exports.SpineTween = SpineTween;
//# sourceMappingURL=SpineTween.js.map