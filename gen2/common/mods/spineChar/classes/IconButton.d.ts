import { IScreen, SvgObj } from "svggame";
export declare class IconButton extends SvgObj {
    private scene;
    private callback;
    action: string;
    params: string;
    constructor(scene: IScreen, key: string, action: string, params: string, callback: Function);
}
