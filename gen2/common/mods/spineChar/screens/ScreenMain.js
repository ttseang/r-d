"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScreenMain = void 0;
var svggame_1 = require("svggame");
var IconButton_1 = require("../classes/IconButton");
var SpineChar_1 = require("../classes/SpineChar");
var AnimationVo_1 = require("../dataObjs/AnimationVo");
var ScreenMain = /** @class */ (function (_super) {
    __extends(ScreenMain, _super);
    function ScreenMain() {
        var _this = _super.call(this, "ScreenMain") || this;
        _this.buttons = [];
        //sounds
        _this.idleAnimations = [];
        _this.walkAnimations = [];
        _this.fidgetAnimations = [];
        _this.gm.regFontSize("instructions", 26, 1000);
        return _this;
    }
    ScreenMain.prototype.create = function () {
        _super.prototype.create.call(this);
        window.scene = this;
        this.uiWindow = new svggame_1.SvgObj(this, "uiWindow");
        this.uiWindow.gameWRatio = .9;
        var scale = this.gw / 4000;
        this.idleAnimations = [new AnimationVo_1.AnimationVo("idle", true), new AnimationVo_1.AnimationVo("Repeating animations/blink", true)];
        this.walkAnimations = [new AnimationVo_1.AnimationVo("walk", true), new AnimationVo_1.AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations = [new AnimationVo_1.AnimationVo("fidget01", false), new AnimationVo_1.AnimationVo("Repeating animations/blink", true)];
        this.spineChar = new SpineChar_1.SpineChar(this, "char", "./assets/char/Amka.json", "./assets/char/Amka.atlas", this.idleAnimations);
        this.spineChar.currentPos = new svggame_1.PosVo(2, 10);
        this.spineChar.scaleToGameH(.2);
        //
        //
        //
        this.btnPlay = new IconButton_1.IconButton(this, "btnPlay", "play", "", this.onbuttonPressed.bind(this));
        this.btnReset = new IconButton_1.IconButton(this, "btnReset", "reset", "", this.onbuttonPressed.bind(this));
        this.btnCheck = new IconButton_1.IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
        this.btnHelp = new IconButton_1.IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));
        this.buttons = [this.btnHelp, this.btnPlay, this.btnReset, this.btnCheck];
    };
    ScreenMain.prototype.onbuttonPressed = function (action, params) {
        console.log(action);
        /*  switch (action) {
             case "reset":
                 this.dragEng.clickLock = false;
                 this.dragEng.resetBoxes();
                 break;
 
             case "check":
                 this.checkCorrect();
                 break;
         } */
    };
    ScreenMain.prototype.doResize = function () {
        var _a, _b, _c;
        _super.prototype.doResize.call(this);
        // this.grid?.showGrid();
        this.uiWindow.displayWidth = this.gw * 0.9;
        this.uiWindow.displayHeight = this.gh * 0.70;
        this.center(this.uiWindow, true);
        for (var i = 0; i < this.buttons.length; i++) {
            this.buttons[i].gameWRatio = 0.05;
            (_a = this.grid) === null || _a === void 0 ? void 0 : _a.placeAt(2 + i * 2, 9.7, this.buttons[i]);
        }
        this.spineChar.scaleToGameH(.2);
        if (this.spineChar.currentPos) {
            this.spineChar.placeOnGrid((_b = this.spineChar.currentPos) === null || _b === void 0 ? void 0 : _b.x, (_c = this.spineChar.currentPos) === null || _c === void 0 ? void 0 : _c.y);
        }
    };
    return ScreenMain;
}(svggame_1.BaseScreen));
exports.ScreenMain = ScreenMain;
//# sourceMappingURL=ScreenMain.js.map