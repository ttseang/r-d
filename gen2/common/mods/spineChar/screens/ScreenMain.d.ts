import { BaseScreen, IScreen } from "svggame";
export declare class ScreenMain extends BaseScreen implements IScreen {
    private box;
    private uiWindow;
    private btnCheck;
    private btnPlay;
    private btnReset;
    private btnHelp;
    private buttons;
    private idleAnimations;
    private walkAnimations;
    private fidgetAnimations;
    private spineChar;
    constructor();
    create(): void;
    onbuttonPressed(action: string, params: string): void;
    doResize(): void;
}
