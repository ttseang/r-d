import { IGameObj, IScreen } from "svggame";
export declare class ColorBurst {
    private scene;
    x: number;
    y: number;
    size: number;
    count: number;
    dist: number;
    duration: number;
    maxDist: number;
    color: number;
    constructor(scene: IScreen, x?: number, y?: number, size?: number, count?: number, dist?: number, duration?: number, maxDist?: number, color?: number);
    start(): void;
    tweenDone(star: IGameObj): void;
}
