"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ColorBurst = void 0;
var svggame_1 = require("svggame");
var ColorBurst = /** @class */ (function () {
    function ColorBurst(scene, x, y, size, count, dist, duration, maxDist, color) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (size === void 0) { size = 5; }
        if (count === void 0) { count = 50; }
        if (dist === void 0) { dist = 150; }
        if (duration === void 0) { duration = 1000; }
        if (maxDist === void 0) { maxDist = 300; }
        if (color === void 0) { color = 0xffffff; }
        this.scene = scene;
        this.x = x;
        this.y = y;
        this.size = size;
        this.count = count;
        this.dist = dist;
        this.duration = duration;
        this.maxDist = maxDist;
        this.color = color;
    }
    ColorBurst.prototype.start = function () {
        var keys = ["redstar", "bluestar", "greenstar", "yellowstar", "orangestar"];
        for (var i = 0; i < this.count; i++) {
            //  let star = this.scene.add.sprite(this.x, this.y, "effectColorStars");
            var keyIndex = svggame_1.Math2.between(0, keys.length - 1);
            var key = keys[keyIndex];
            var star = new svggame_1.SvgObj(this.scene, key);
            star.x = this.x;
            star.y = this.y;
            //
            //
            //        
            // console.log(this.maxDist);
            var r = svggame_1.Math2.between(50, this.maxDist);
            var s = svggame_1.Math2.between(1, 100) / 1000;
            star.gameWRatio = s;
            /*  star.scaleX = s;
             star.scaleY = s; */
            var angle = i * (360 / this.count);
            var tx = this.x + r * Math.cos(angle);
            var ty = this.y + r * Math.sin(angle);
            var tweenObj = new svggame_1.TweenObj();
            tweenObj.duration = this.duration;
            tweenObj.x = tx;
            tweenObj.y = ty;
            tweenObj.alpha = 0;
            tweenObj.angle = svggame_1.Math2.between(0, 360);
            tweenObj.onComplete = this.tweenDone.bind(this);
            var tw = new svggame_1.SimpleTween(star, tweenObj);
            //  star.x=tx;
            // star.y=ty;
            /* this.scene.tweens.add({
                targets: star,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone.bind(this)
            }); */
        }
    };
    ColorBurst.prototype.tweenDone = function (star) {
        star.destroy();
    };
    return ColorBurst;
}());
exports.ColorBurst = ColorBurst;
//# sourceMappingURL=ColorBurst.js.map