"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpinePlayer = exports.SpineTweenObj = exports.SpineTween = exports.SpineChar = void 0;
var SpineChar_1 = require("./classes/SpineChar");
Object.defineProperty(exports, "SpineChar", { enumerable: true, get: function () { return SpineChar_1.SpineChar; } });
var SpineTween_1 = require("./classes/SpineTween");
Object.defineProperty(exports, "SpineTween", { enumerable: true, get: function () { return SpineTween_1.SpineTween; } });
var SpineTweenObj_1 = require("./classes/SpineTweenObj");
Object.defineProperty(exports, "SpineTweenObj", { enumerable: true, get: function () { return SpineTweenObj_1.SpineTweenObj; } });
var spine_player_1 = require("@esotericsoftware/spine-player");
Object.defineProperty(exports, "SpinePlayer", { enumerable: true, get: function () { return spine_player_1.SpinePlayer; } });
/* import { GameOptions, SVGGame } from "svggame"
import { ScreenMain } from "./screens/ScreenMain";

window.onload=()=>{
    
   

    let opts:GameOptions=new GameOptions();
    opts.useFull=true;
    opts.addAreaID="addArea";
    opts.screens=[new ScreenMain()]

    let game:SVGGame=new SVGGame("myCanvas",opts);
} */
//# sourceMappingURL=index.js.map