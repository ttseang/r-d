"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var svggame_1 = require("svggame");
var ScreenMain_1 = require("./screens/ScreenMain");
window.onload = function () {
    var opts = new svggame_1.GameOptions();
    opts.useFull = true;
    opts.addAreaID = "addArea";
    opts.screens = [new ScreenMain_1.ScreenMain()];
    var game = new svggame_1.SVGGame("myCanvas", opts);
};
//# sourceMappingURL=old_index.js.map