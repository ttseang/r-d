export declare class AnimationVo {
    name: string;
    loop: boolean;
    constructor(name: string, loop: boolean);
}
