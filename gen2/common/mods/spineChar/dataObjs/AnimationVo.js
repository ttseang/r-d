"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnimationVo = void 0;
var AnimationVo = /** @class */ (function () {
    function AnimationVo(name, loop) {
        this.name = name;
        this.loop = loop;
    }
    return AnimationVo;
}());
exports.AnimationVo = AnimationVo;
//# sourceMappingURL=AnimationVo.js.map