export declare class WordVo {
    chars: string[];
    word: string;
    constructor(word: string, chars: string[]);
}
