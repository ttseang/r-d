import { IBaseScene, PosVo, CompManager } from "ttphasercomps";
import { IDragObj } from "../interfaces/IDragObj";
export declare class BaseDragObj extends Phaser.GameObjects.Container implements IDragObj {
    scene: Phaser.Scene;
    protected bscene: IBaseScene;
    protected cm: CompManager;
    content: string;
    canDrag: boolean;
    original: PosVo;
    place: PosVo;
    resetPlace: PosVo;
    constructor(bscene: IBaseScene);
    getContainer(): Phaser.GameObjects.Container;
    doResize(): void;
    setContent(content: string): void;
    setPlace(posVo: PosVo): void;
    snapBack(): void;
}
