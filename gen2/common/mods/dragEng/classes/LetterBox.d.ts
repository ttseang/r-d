import { GameObjects } from "phaser";
import { IBaseScene, PosVo } from "ttphasercomps";
import { IDragObj } from "../interfaces/IDragObj";
export declare class LetterBox extends GameObjects.Container implements IDragObj {
    scene: Phaser.Scene;
    private bscene;
    private back;
    private textBox;
    content: string;
    canDrag: boolean;
    private myScale;
    private graphics;
    private cm;
    original: PosVo;
    place: PosVo;
    resetPlace: PosVo;
    constructor(bscene: IBaseScene, content: string);
    getContainer(): Phaser.GameObjects.Container;
    drawBox(): void;
    doResize(): void;
    setContent(content: string): void;
    setPlace(posVo: PosVo): void;
    snapBack(): void;
}
