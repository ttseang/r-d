"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LetterBox = void 0;
var phaser_1 = require("phaser");
var ttphasercomps_1 = require("ttphasercomps");
var LetterBox = /** @class */ (function (_super) {
    __extends(LetterBox, _super);
    function LetterBox(bscene, content) {
        var _this = _super.call(this, bscene.getScene()) || this;
        _this.content = "";
        _this.canDrag = true;
        _this.myScale = 0.08;
        _this.cm = ttphasercomps_1.CompManager.getInstance();
        _this.original = new ttphasercomps_1.PosVo(0, 0);
        _this.place = new ttphasercomps_1.PosVo(0, 0);
        _this.resetPlace = new ttphasercomps_1.PosVo(0, 0);
        _this.bscene = bscene;
        _this.scene = bscene.getScene();
        _this.content = content;
        _this.back = _this.scene.add.image(0, 0, "holder");
        ttphasercomps_1.Align.scaleToGameW(_this.back, _this.myScale, _this.bscene);
        _this.textBox = _this.scene.add.text(0, 0, content, { fontSize: "100px", color: "#000000" }).setOrigin(0.5, 0.5);
        /*   this.textBox=this.scene.add.bitmapText(0,0,"aff",letter).setOrigin(0.5,0.5);
          this.textBox.fontSize=120;
          this.textBox.setMaxWidth(this.back.displayWidth*0.7); */
        _this.graphics = _this.scene.add.graphics();
        _this.drawBox();
        _this.add(_this.back);
        _this.add(_this.textBox);
        _this.add(_this.graphics);
        _this.setSize(_this.back.displayWidth, _this.back.displayHeight);
        _this.setInteractive();
        _this.scene.add.existing(_this);
        _this.doResize();
        return _this;
    }
    LetterBox.prototype.getContainer = function () {
        return this;
    };
    LetterBox.prototype.drawBox = function () {
        this.graphics.clear();
        this.graphics.lineStyle(2, 0x00000);
        this.graphics.strokeRect(this.back.x - this.back.displayWidth / 2, this.back.y - this.back.displayHeight / 2, this.back.displayWidth, this.back.displayHeight);
    };
    LetterBox.prototype.doResize = function () {
        var fs = this.cm.getFontSize("letters", this.bscene.getW());
        this.textBox.setFontSize(fs);
        ttphasercomps_1.Align.scaleToGameW(this.back, this.myScale, this.bscene);
        this.drawBox();
        this.bscene.getGrid().place(this.place, this);
    };
    LetterBox.prototype.setContent = function (content) {
        this.content = content;
        this.textBox.setText(content);
    };
    LetterBox.prototype.setPlace = function (posVo) {
        this.place = posVo;
        this.bscene.getGrid().place(this.place, this);
    };
    LetterBox.prototype.snapBack = function () {
        this.scene.tweens.add({ targets: this, duration: 500, y: this.resetPlace.y, x: this.resetPlace.x });
    };
    return LetterBox;
}(phaser_1.GameObjects.Container));
exports.LetterBox = LetterBox;
//# sourceMappingURL=LetterBox.js.map