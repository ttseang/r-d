"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseTargObj = void 0;
var ttphasercomps_1 = require("ttphasercomps");
;
var BaseTargObj = /** @class */ (function (_super) {
    __extends(BaseTargObj, _super);
    function BaseTargObj(bscene) {
        var _this = _super.call(this, bscene.getScene()) || this;
        _this.cm = ttphasercomps_1.CompManager.getInstance();
        _this.content = "";
        _this.original = new ttphasercomps_1.PosVo(0, 0);
        _this.place = new ttphasercomps_1.PosVo(0, 0);
        _this.resetPlace = new ttphasercomps_1.PosVo(0, 0);
        _this.bscene = bscene;
        _this.scene = bscene.getScene();
        return _this;
    }
    BaseTargObj.prototype.setColor = function (color) {
        this.color = color;
    };
    BaseTargObj.prototype.getContainer = function () {
        return this;
    };
    BaseTargObj.prototype.doResize = function () {
    };
    BaseTargObj.prototype.setContent = function (content) {
        this.content = content;
    };
    BaseTargObj.prototype.setPlace = function (posVo) {
        this.place = posVo;
        this.bscene.getGrid().place(this.place, this);
    };
    return BaseTargObj;
}(Phaser.GameObjects.Container));
exports.BaseTargObj = BaseTargObj;
//# sourceMappingURL=BaseTargObj.js.map