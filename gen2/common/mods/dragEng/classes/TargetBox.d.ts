import { IBaseScene, PosVo } from "ttphasercomps";
import { ITargetObj } from "../interfaces/ITargetObj";
export declare class TargetBox extends Phaser.GameObjects.Container implements ITargetObj {
    scene: Phaser.Scene;
    private bscene;
    private graphics;
    correct: string;
    private myScale;
    private color;
    private back;
    place: PosVo;
    original: PosVo;
    content: string;
    constructor(bscene: IBaseScene, color: number);
    setContent(content: string): void;
    getContainer(): this;
    setPlace(place: PosVo): void;
    setColor(color: number): void;
    drawBox(): void;
    doResize(): void;
}
