"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.TargetBox = void 0;
var ttphasercomps_1 = require("ttphasercomps");
var TargetBox = /** @class */ (function (_super) {
    __extends(TargetBox, _super);
    function TargetBox(bscene, color) {
        var _this = _super.call(this, bscene.getScene()) || this;
        _this.correct = "";
        _this.myScale = 0.08;
        _this.place = new ttphasercomps_1.PosVo(0, 0);
        _this.original = new ttphasercomps_1.PosVo(0, 0);
        _this.content = "";
        _this.bscene = bscene;
        _this.scene = bscene.getScene();
        _this.color = color;
        _this.back = _this.scene.add.image(0, 0, "holder");
        ttphasercomps_1.Align.scaleToGameW(_this.back, _this.myScale, _this.bscene);
        _this.graphics = _this.scene.add.graphics();
        _this.drawBox();
        _this.add(_this.back);
        _this.add(_this.graphics);
        _this.scene.add.existing(_this);
        return _this;
    }
    TargetBox.prototype.setContent = function (content) {
        this.content = content;
    };
    TargetBox.prototype.getContainer = function () {
        return this;
    };
    TargetBox.prototype.setPlace = function (place) {
        this.place = place;
        this.bscene.getGrid().place(this.place, this);
    };
    TargetBox.prototype.setColor = function (color) {
        this.color = color;
        this.drawBox();
    };
    TargetBox.prototype.drawBox = function () {
        this.graphics.clear();
        this.graphics.lineStyle(4, this.color);
        this.graphics.strokeRect(this.back.x - this.back.displayWidth / 2, this.back.y - this.back.displayHeight / 2, this.back.displayWidth, this.back.displayHeight);
    };
    TargetBox.prototype.doResize = function () {
        this.bscene.getGrid().place(this.place, this);
        ttphasercomps_1.Align.scaleToGameW(this.back, this.myScale, this.bscene);
        this.drawBox();
    };
    return TargetBox;
}(Phaser.GameObjects.Container));
exports.TargetBox = TargetBox;
//# sourceMappingURL=TargetBox.js.map