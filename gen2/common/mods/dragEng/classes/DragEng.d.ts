import { IBaseScene } from "ttphasercomps";
import { IDragObj } from "../interfaces/IDragObj";
import { ITargetObj } from "../interfaces/ITargetObj";
export declare class DragEng {
    private bscene;
    private scene;
    targets: ITargetObj[];
    dragObjs: IDragObj[];
    clickLock: boolean;
    private dragBox;
    private chosenContent;
    upCallback: Function;
    dropCallback: Function;
    collideCallback: Function;
    constructor(bscene: IBaseScene);
    addDrag(dragObj: IDragObj): void;
    addTarget(target: ITargetObj): void;
    destroyDrags(): void;
    destroyTargs(): void;
    doResize(): void;
    setListeners(): void;
    onDown(p: Phaser.Input.Pointer, dragObj: IDragObj): void;
    onMove(p: Phaser.Input.Pointer): void;
    onUp(): void;
    resetBoxes(): void;
    getTargContent(): string;
    checkTargets(): boolean;
}
