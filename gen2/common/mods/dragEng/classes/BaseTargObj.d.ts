import { IBaseScene, PosVo, CompManager } from "ttphasercomps";
import { ITargetObj } from "..";
export declare class BaseTargObj extends Phaser.GameObjects.Container implements ITargetObj {
    scene: Phaser.Scene;
    protected bscene: IBaseScene;
    protected cm: CompManager;
    content: string;
    protected color: number;
    original: PosVo;
    place: PosVo;
    resetPlace: PosVo;
    correct: string;
    constructor(bscene: IBaseScene);
    setColor(color: number): void;
    getContainer(): Phaser.GameObjects.Container;
    doResize(): void;
    setContent(content: string): void;
    setPlace(posVo: PosVo): void;
}
