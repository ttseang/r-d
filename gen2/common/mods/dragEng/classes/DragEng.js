"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DragEng = void 0;
var ttphasercomps_1 = require("ttphasercomps");
var DragEng = /** @class */ (function () {
    function DragEng(bscene) {
        this.targets = [];
        this.dragObjs = [];
        this.clickLock = false;
        this.dragBox = null;
        this.chosenContent = "";
        this.upCallback = function () { };
        this.dropCallback = function () { };
        this.collideCallback = function () { };
        this.bscene = bscene;
        this.scene = bscene.getScene();
    }
    DragEng.prototype.addDrag = function (dragObj) {
        dragObj.type = "dragObj";
        this.dragObjs.push(dragObj);
    };
    DragEng.prototype.addTarget = function (target) {
        this.targets.push(target);
    };
    DragEng.prototype.destroyDrags = function () {
        for (var i = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].getContainer().destroy();
        }
    };
    DragEng.prototype.destroyTargs = function () {
        for (var i = 0; i < this.targets.length; i++) {
            this.targets[i].getContainer().destroy();
        }
    };
    DragEng.prototype.doResize = function () {
        for (var i = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].doResize();
        }
        for (var i = 0; i < this.targets.length; i++) {
            this.targets[i].doResize();
        }
    };
    DragEng.prototype.setListeners = function () {
        this.scene.input.on("gameobjectdown", this.onDown.bind(this));
        this.scene.input.on("pointermove", this.onMove.bind(this));
        this.scene.input.on("pointerup", this.onUp.bind(this));
    };
    DragEng.prototype.onDown = function (p, dragObj) {
        if (this.clickLock == true) {
            return;
        }
        if (dragObj.type != "dragObj") {
            return;
        }
        this.dragBox = dragObj;
        this.dragBox.resetPlace = new ttphasercomps_1.PosVo(this.dragBox.x, this.dragBox.y);
        this.scene.children.bringToTop(dragObj.getContainer());
    };
    DragEng.prototype.onMove = function (p) {
        if (this.dragBox) {
            this.dragBox.x = p.x;
            this.dragBox.y = p.y;
        }
    };
    DragEng.prototype.onUp = function () {
        if (this.dragBox) {
            this.upCallback(this.dragBox);
            if (this.dragBox.canDrag == false) {
                return;
            }
            var found = this.checkTargets();
            if (found == false) {
                this.dragBox.snapBack();
            }
            else {
                this.dropCallback();
            }
        }
        this.dragBox = null;
    };
    DragEng.prototype.resetBoxes = function () {
        for (var i = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].setPlace(this.dragObjs[i].original);
        }
    };
    DragEng.prototype.getTargContent = function () {
        var content = "";
        for (var i = 0; i < this.targets.length; i++) {
            content += this.targets[i].content;
        }
        return content;
    };
    DragEng.prototype.checkTargets = function () {
        if (this.dragBox === null) {
            return;
        }
        console.log(this.dragBox);
        for (var i = 0; i < this.targets.length; i++) {
            var targetBox = this.targets[i];
            var distY = Math.abs(this.dragBox.y - targetBox.y);
            var distX = Math.abs(this.dragBox.x - targetBox.x);
            if (distX < this.dragBox.displayWidth * 0.4 && distY < this.dragBox.displayHeight * 0.4) {
                /* this.dragBox.x = targetBox.x;
                this.dragBox.y = targetBox.y; */
                this.dragBox.setPlace(targetBox.place);
                this.chosenContent = this.dragBox.content;
                targetBox.content = this.dragBox.content;
                this.collideCallback(this.dragBox, targetBox);
                // this.clickLock = true;
                return true;
            }
        }
        return false;
    };
    return DragEng;
}());
exports.DragEng = DragEng;
//# sourceMappingURL=DragEng.js.map