"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//import Phaser = require("phaser");
var phaser_1 = __importDefault(require("phaser"));
var SceneMain_1 = require("./scenes/SceneMain");
var GM_1 = require("./classes/GM");
var gm = GM_1.GM.getInstance();
var isMobile = navigator.userAgent.indexOf("Mobile");
var isTablet = navigator.userAgent.indexOf("Tablet");
var isIpad = navigator.userAgent.indexOf("iPad");
if (isTablet != -1 || isIpad != -1) {
    gm.isTablet = true;
    isMobile = 1;
}
var w = 600;
var h = 400;
//
//
if (isMobile != -1) {
    gm.isMobile = true;
}
w = window.innerWidth;
h = window.innerHeight;
if (w < h) {
    gm.isPort = true;
}
var config = {
    type: phaser_1.default.AUTO,
    width: w,
    height: h,
    backgroundColor: 'cccccc',
    parent: 'phaser-game',
    scene: [SceneMain_1.SceneMain]
};
new phaser_1.default.Game(config);
//# sourceMappingURL=oldindex.js.map