import { PosVo } from "ttphasercomps";
export interface ITargetObj {
    visible: boolean;
    alpha: number;
    displayWidth: number;
    displayHeight: number;
    scaleX: number;
    scaleY: number;
    x: number;
    y: number;
    destroy(): void;
    setColor(color: number): void;
    correct: string;
    original: PosVo;
    place: PosVo;
    setPlace(pos: PosVo): void;
    doResize(): void;
    content: string;
    setContent(content: string): void;
    getContainer(): Phaser.GameObjects.Container;
}
