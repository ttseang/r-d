import { PosVo } from "ttphasercomps";
export interface IDragObj {
    type: string;
    canDrag: boolean;
    visible: boolean;
    alpha: number;
    displayWidth: number;
    displayHeight: number;
    scaleX: number;
    scaleY: number;
    x: number;
    y: number;
    destroy(): void;
    content: string;
    original: PosVo;
    place: PosVo;
    resetPlace: PosVo;
    snapBack(): void;
    setPlace(pos: PosVo): void;
    setContent(content: string): void;
    getContainer(): Phaser.GameObjects.Container;
    doResize(): void;
}
