import { BaseScene } from "./BaseScene";
export declare class SceneMain extends BaseScene {
    private dragEng;
    private word;
    constructor();
    preload(): void;
    create(): void;
    resetBoxes(): void;
    makeBoxes(): void;
    checkCorrect(): void;
    doResize(): void;
}
