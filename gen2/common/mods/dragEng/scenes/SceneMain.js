"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.SceneMain = void 0;
var ttphasercomps_1 = require("ttphasercomps");
var DragEng_1 = require("../classes/DragEng");
var LetterBox_1 = require("../classes/LetterBox");
var TargetBox_1 = require("../classes/TargetBox");
var BaseScene_1 = require("./BaseScene");
var SceneMain = /** @class */ (function (_super) {
    __extends(SceneMain, _super);
    function SceneMain() {
        var _this = _super.call(this, "SceneMain") || this;
        _this.word = "Hello";
        return _this;
    }
    SceneMain.prototype.preload = function () {
        this.load.image("holder", "./assets/holder.jpg");
    };
    SceneMain.prototype.create = function () {
        _super.prototype.create.call(this);
        this.makeGrid(11, 11);
        window['scene'] = this;
        this.dragEng = new DragEng_1.DragEng(this);
        window.onresize = this.doResize.bind(this);
        this.makeBoxes();
    };
    SceneMain.prototype.resetBoxes = function () {
        this.dragEng.resetBoxes();
    };
    SceneMain.prototype.makeBoxes = function () {
        for (var i = 0; i < this.word.length; i++) {
            var box = new LetterBox_1.LetterBox(this, this.word.substr(i, 1));
            box.setInteractive();
            box.setPlace(new ttphasercomps_1.PosVo(3 + i, 5));
            box.original = box.place;
            this.dragEng.addDrag(box);
        }
        for (var i = 0; i < this.word.length; i++) {
            var targ = new TargetBox_1.TargetBox(this, 0xff0000);
            targ.setPlace(new ttphasercomps_1.PosVo(3 + i, 8));
            this.dragEng.addTarget(targ);
        }
        this.dragEng.setListeners();
        this.dragEng.dropCallback = this.checkCorrect.bind(this);
    };
    SceneMain.prototype.checkCorrect = function () {
        var checkString = this.dragEng.getTargContent();
        if (checkString == this.word) {
            console.log("correct");
        }
        else {
            console.log("wrong");
        }
    };
    SceneMain.prototype.doResize = function () {
        var w = window.innerWidth;
        var h = window.innerHeight;
        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
        this.dragEng.doResize();
        // this.grid.showPos();
        //this.cm.doResize();
    };
    return SceneMain;
}(BaseScene_1.BaseScene));
exports.SceneMain = SceneMain;
//# sourceMappingURL=SceneMain.js.map