import { IGameObj, PosVo } from "..";
export declare class AlignGrid {
    ch: number;
    cw: number;
    private gw;
    private gh;
    gridID: string;
    constructor(rows: number, cols: number, gw: number, gh: number);
    placeAt(col: number, row: number, obj: IGameObj): void;
    showGrid(): void;
    findNearestGridXY(xx: number, yy: number): PosVo;
    findNearestGridXYDec(xx: number, yy: number): PosVo;
}
