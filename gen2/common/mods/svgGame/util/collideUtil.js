"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollideUtil = void 0;
var CollideUtil = /** @class */ (function () {
    function CollideUtil() {
    }
    CollideUtil.checkOverlap = function (obj1, obj2) {
        var r1 = obj1.getBoundingClientRect(); //BOUNDING BOX OF THE FIRST OBJECT
        var r2 = obj2.getBoundingClientRect(); //BOUNDING BOX OF THE SECOND OBJECT
        //CHECK IF THE TWO BOUNDING BOXES OVERLAP
        return !(r2.left > r1.right ||
            r2.right < r1.left ||
            r2.top > r1.bottom ||
            r2.bottom < r1.top);
    };
    return CollideUtil;
}());
exports.CollideUtil = CollideUtil;
//# sourceMappingURL=collideUtil.js.map