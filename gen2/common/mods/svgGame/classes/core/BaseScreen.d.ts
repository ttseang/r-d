import { AlignGrid } from "../../util/AlignGridSvg";
import { GameManager } from "./GameManager";
import { SvgObj } from "./gameobjects/SvgObj";
import { TextObj } from "./gameobjects/TextObj";
import { IGameObj } from "./interfaces/IGameObj";
import { IScreen } from "./interfaces/IScreen";
export declare class BaseScreen implements IScreen {
    key: string;
    protected gm: GameManager;
    private screenManager;
    private children;
    grid: AlignGrid | null;
    constructor(key: string);
    start(): void;
    create(): void;
    destroy(): void;
    addExisting(obj: IGameObj): void;
    addImage(key: string): SvgObj;
    addImageAt(key: string, x: number, y: number): SvgObj;
    addText(text: string, textKey?: string): TextObj;
    addTextAt(text: string, x: number, y: number, textKey?: string): TextObj;
    centerW(obj: IGameObj, adjust?: boolean): void;
    centerH(obj: IGameObj, adjust?: boolean): void;
    placeOnGrid(col: number, row: number, obj: IGameObj, adjust?: boolean): void;
    tweenGridPos(col: number, row: number, obj: IGameObj, duration: number, callback?: Function): void;
    center(obj: IGameObj, adjust?: boolean): void;
    /**
     * game height;
     */
    get gh(): number;
    /**
     * game width
     */
    get gw(): number;
    changeScreen(nScreen: string): void;
    doResize(): void;
    playSound(path: string): void;
}
