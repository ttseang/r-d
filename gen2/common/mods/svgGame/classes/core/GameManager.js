"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GameManager = void 0;
var fontSizeVo_1 = require("./dataobjs/fontSizeVo");
var gameOptions_1 = require("./dataobjs/gameOptions");
var ScreenManager_1 = require("./ScreenManager");
var GameManager = /** @class */ (function () {
    function GameManager() {
        this.gameOptions = new gameOptions_1.GameOptions();
        this.gameID = "";
        this.gameEl = null;
        this.gw = 0;
        this.gh = 0;
        this.objMap = new Map();
        this.defFontSize = new fontSizeVo_1.FontSizeVo(70, 1000);
        this.fontSizes = new Map();
        window.gm = this;
        window.onresize = this.doResize.bind(this);
    }
    GameManager.getInstance = function () {
        if (this.instance == null) {
            this.instance = new GameManager();
        }
        return this.instance;
    };
    GameManager.prototype.regObj = function (id, obj) {
        //console.log("register " + id);
        this.objMap.set(id, obj);
    };
    GameManager.prototype.unRegObj = function (id) {
        this.objMap.delete(id);
    };
    GameManager.prototype.getObj = function (id) {
        if (this.objMap.has(id)) {
            return this.objMap.get(id);
        }
        return null;
    };
    GameManager.prototype.getFontSize = function (sizeKey, canvasWidth) {
        var fontSizeVo = this.defFontSize;
        if (this.fontSizes.has(sizeKey)) {
            fontSizeVo = this.fontSizes.get(sizeKey);
        }
        var fontSize = fontSizeVo.defFontSize;
        var fontBase = fontSizeVo.canvasBase;
        var ratio = fontSize / fontBase; // calc ratio
        var size = canvasWidth * ratio; // get font size based on current width
        return (size | 0);
    };
    GameManager.prototype.regFontSize = function (key, defFontSize, canvasBase) {
        this.fontSizes.set(key, new fontSizeVo_1.FontSizeVo(defFontSize, canvasBase));
    };
    GameManager.prototype.doResize = function () {
        if (this.gameOptions.useFull == true) {
            if (this.gameEl) {
                var b = document.body.getBoundingClientRect();
                // let b: any = (this.gameEl as any).getBBox();
                this.gw = b.width;
                this.gh = b.height;
            }
        }
        ScreenManager_1.ScreenManager.getInstance().doResize();
    };
    return GameManager;
}());
exports.GameManager = GameManager;
//# sourceMappingURL=GameManager.js.map