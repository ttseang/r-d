"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FontSizeVo = void 0;
var FontSizeVo = /** @class */ (function () {
    function FontSizeVo(defFontSize, canvasBase) {
        this.defFontSize = defFontSize;
        this.canvasBase = canvasBase;
    }
    return FontSizeVo;
}());
exports.FontSizeVo = FontSizeVo;
//# sourceMappingURL=fontSizeVo.js.map