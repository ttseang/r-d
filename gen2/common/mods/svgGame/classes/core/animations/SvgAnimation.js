"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SVGAnimation = void 0;
var SVGAnimation = /** @class */ (function () {
    function SVGAnimation(id) {
        this.id = id;
        this.el = document.getElementById(id);
        console.log(this.el);
        window.anim = this;
    }
    SVGAnimation.prototype.onComplete = function (callback) {
        if (this.el) {
            this.el.addEventListener("endEvent", function () {
                callback();
            });
        }
    };
    SVGAnimation.prototype.start = function () {
        if (this.el) {
            this.el.beginElement();
        }
    };
    SVGAnimation.prototype.stop = function () {
        if (this.el) {
            this.el.endElement();
        }
    };
    return SVGAnimation;
}());
exports.SVGAnimation = SVGAnimation;
//# sourceMappingURL=SvgAnimation.js.map