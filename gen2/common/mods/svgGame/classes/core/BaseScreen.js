"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseScreen = void 0;
var AlignGridSvg_1 = require("../../util/AlignGridSvg");
var GameManager_1 = require("./GameManager");
var SoundObj_1 = require("./gameobjects/SoundObj");
var SvgObj_1 = require("./gameobjects/SvgObj");
var TextObj_1 = require("./gameobjects/TextObj");
var ScreenManager_1 = require("./ScreenManager");
var SimpleTween_1 = require("./tweens/SimpleTween");
var TweenObj_1 = require("./tweens/TweenObj");
var BaseScreen = /** @class */ (function () {
    function BaseScreen(key) {
        this.gm = GameManager_1.GameManager.getInstance();
        this.screenManager = ScreenManager_1.ScreenManager.getInstance();
        this.children = [];
        this.grid = null;
        this.key = key;
    }
    BaseScreen.prototype.start = function () {
        this.grid = new AlignGridSvg_1.AlignGrid(11, 11, this.gm.gw, this.gm.gh);
        this.create();
    };
    BaseScreen.prototype.create = function () {
    };
    BaseScreen.prototype.destroy = function () {
        var _a;
        while (this.children.length > 0) {
            (_a = this.children.pop()) === null || _a === void 0 ? void 0 : _a.destroy();
        }
    };
    BaseScreen.prototype.addExisting = function (obj) {
        this.children.push(obj);
    };
    BaseScreen.prototype.addImage = function (key) {
        var svgObj = new SvgObj_1.SvgObj(this, key);
        this.addExisting(svgObj);
        return svgObj;
    };
    BaseScreen.prototype.addImageAt = function (key, x, y) {
        var svgObj = this.addImage(key);
        svgObj.x = x;
        svgObj.y = y;
        return svgObj;
    };
    BaseScreen.prototype.addText = function (text, textKey) {
        if (textKey === void 0) { textKey = "defaultText"; }
        var textObj = new TextObj_1.TextObj(this, textKey);
        textObj.setText(text);
        this.addExisting(textObj);
        return textObj;
    };
    BaseScreen.prototype.addTextAt = function (text, x, y, textKey) {
        if (textKey === void 0) { textKey = "defaultText"; }
        var textObj = this.addText(textKey, text);
        textObj.x = x;
        textObj.y = y;
        return textObj;
    };
    BaseScreen.prototype.centerW = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        obj.x = this.gw / 2;
        if (adjust == true) {
            obj.x -= obj.displayWidth / 2;
        }
    };
    BaseScreen.prototype.centerH = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        obj.y = this.gh / 2;
        if (adjust) {
            obj.y -= obj.displayHeight / 2;
        }
    };
    BaseScreen.prototype.placeOnGrid = function (col, row, obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        var xx = this.grid.cw * col;
        var yy = this.grid.ch * row;
        obj.x = xx;
        obj.y = yy;
        if (adjust) {
            obj.y += this.grid.ch / 2 - obj.displayHeight / 2;
            obj.x += this.grid.cw / 2 - obj.displayWidth / 2;
        }
    };
    BaseScreen.prototype.tweenGridPos = function (col, row, obj, duration, callback) {
        if (callback === void 0) { callback = function () { }; }
        var tweenObj = new TweenObj_1.TweenObj();
        var xx = this.grid.cw * col;
        var yy = this.grid.ch * row;
        tweenObj.x = xx;
        tweenObj.y = yy;
        tweenObj.onComplete = callback;
        tweenObj.duration = duration;
        var tween = new SimpleTween_1.SimpleTween(obj, tweenObj);
    };
    BaseScreen.prototype.center = function (obj, adjust) {
        if (adjust === void 0) { adjust = false; }
        this.centerW(obj, adjust);
        this.centerH(obj, adjust);
    };
    Object.defineProperty(BaseScreen.prototype, "gh", {
        /**
         * game height;
         */
        get: function () {
            return this.gm.gh;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BaseScreen.prototype, "gw", {
        /**
         * game width
         */
        get: function () {
            return this.gm.gw;
        },
        enumerable: false,
        configurable: true
    });
    BaseScreen.prototype.changeScreen = function (nScreen) {
        //  console.log("change to "+nScreen);
        this.screenManager.changeScreen(nScreen);
    };
    BaseScreen.prototype.doResize = function () {
        //can override in screen
        /*   this.grid.hide();
          this.grid.resetSize(); */
        this.grid = new AlignGridSvg_1.AlignGrid(11, 11, this.gm.gw, this.gm.gh);
        for (var i = 0; i < this.children.length; i++) {
            this.children[i].doResize();
        }
    };
    BaseScreen.prototype.playSound = function (path) {
        var s = new SoundObj_1.SoundObj(path);
        s.play();
    };
    return BaseScreen;
}());
exports.BaseScreen = BaseScreen;
//# sourceMappingURL=BaseScreen.js.map