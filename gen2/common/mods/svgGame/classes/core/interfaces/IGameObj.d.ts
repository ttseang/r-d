export interface IGameObj {
    x: number;
    y: number;
    elID: string;
    el: HTMLElement | null;
    displayWidth: number;
    displayHeight: number;
    visible: boolean;
    alpha: number;
    angle: number;
    scaleX: number;
    scaleY: number;
    skewX: number;
    skewY: number;
    destroy(): void;
    doResize(): void;
    gameWRatio: number;
    gameHRatio: number;
    adjust(): void;
    updateScale(): void;
}
