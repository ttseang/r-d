"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlObj = void 0;
var StyleVo_1 = require("../dataObjs/StyleVo");
var HtmlObj = /** @class */ (function () {
    function HtmlObj(id) {
        this._x = 0;
        this._y = 0;
        this._visible = false;
        this.el = document.getElementById(id);
        this.id = id;
        // this.init();
    }
    HtmlObj.prototype.init = function () {
        if (this.el) {
            this.x = parseFloat(this.el.style.left);
            this.y = parseFloat(this.el.style.top);
        }
    };
    Object.defineProperty(HtmlObj.prototype, "x", {
        get: function () {
            return this._x;
        },
        set: function (value) {
            this._x = value;
            if (this.el) {
                this.el.style.left = value.toString() + "%";
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(HtmlObj.prototype, "y", {
        get: function () {
            return this._y;
        },
        set: function (value) {
            this._y = value;
            if (this.el) {
                this.el.style.top = value.toString() + "%";
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(HtmlObj.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (value) {
            this._visible = value;
            if (this.el) {
                if (this.visible == true) {
                    this.el.style.display = "block";
                }
                else {
                    this.el.style.display = "none";
                }
            }
        },
        enumerable: false,
        configurable: true
    });
    HtmlObj.prototype.flyTo = function (xx, yy) {
        if (this.el) {
            //  //console.log(this.el);
            this.el.style.transition = "top 2s,left 2s,width 2s";
            this.x = xx;
            this.y = yy;
        }
    };
    HtmlObj.prototype.getStyle = function (className) {
        var cssText = "";
        var myRule = new StyleVo_1.StyleVo("red", "3vh", "normal");
        //console.log("looking for "+className);
        var rules = document.styleSheets[0].cssRules;
        for (var i = 0; i < rules.length; i++) {
            var rule = rules.item(i);
            if (rule) {
                var ruleName = rule.selectorText;
                //console.log("looking at "+ruleName);
                if (ruleName == className) {
                    //console.log(rule);
                    cssText = rule.cssText;
                    var rule2 = rule;
                    if (rule2.style.color) {
                        myRule.color = rule2.style.color;
                    }
                    if (rule2.style.fontSize) {
                        myRule.fontSize = rule2.style.fontSize;
                    }
                    if (rule2.style.fontWeight) {
                        myRule.fontWeight = rule2.style.fontWeight;
                    }
                    //   myRule=(rule as CSSStyleRule).style;
                }
            }
        }
        return myRule;
    };
    return HtmlObj;
}());
exports.HtmlObj = HtmlObj;
//# sourceMappingURL=HtmlObj.js.map