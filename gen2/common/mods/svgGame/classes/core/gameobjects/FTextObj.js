"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.FTextObj = void 0;
var SvgObj_1 = require("./SvgObj");
var FTextObj = /** @class */ (function (_super) {
    __extends(FTextObj, _super);
    function FTextObj(screen, key) {
        var _this = _super.call(this, screen, key) || this;
        _this.textEl = null;
        return _this;
    }
    FTextObj.createNew = function () {
        //implement here
    };
    FTextObj.prototype.setText = function (text) {
        if (this.el) {
            console.log(text);
            if (this.el) {
                this.textEl = this.el.getElementsByTagName("p")[0];
                console.log(this.textEl);
                this.textEl.innerHTML = text;
            }
            //this.el.textContent=text;
            this.updateSizes();
        }
    };
    FTextObj.prototype.setFontSize = function (size) {
        if (this.el) {
            this.el.setAttribute("font-size", size.toString() + "px");
            this.updateSizes();
        }
    };
    return FTextObj;
}(SvgObj_1.SvgObj));
exports.FTextObj = FTextObj;
//# sourceMappingURL=FTextObj.js.map