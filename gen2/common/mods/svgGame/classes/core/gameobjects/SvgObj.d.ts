import { IGameObj } from "../interfaces/IGameObj";
import { IScreen } from "../interfaces/IScreen";
export declare class SvgObj implements IGameObj {
    id: string;
    el: HTMLElement | null;
    elID: string;
    private _gameWRatio;
    private _gameHRatio;
    private _x;
    private _y;
    private _angle;
    private _visible;
    private _width;
    private _height;
    private _displayWidth;
    private _displayHeight;
    private _scaleX;
    private _scaleY;
    private _skewX;
    private _skewY;
    private static count;
    private gm;
    screen: IScreen;
    private _alpha;
    constructor(screen: IScreen, id: string, addArea?: string);
    protected updateSizes(): void;
    onClick(callback: Function): void;
    static getInstanceName(key: string): string;
    incRot(rot: number): void;
    get angle(): number;
    set angle(value: number);
    get width(): number;
    set width(value: number);
    get height(): number;
    set height(value: number);
    get displayWidth(): number;
    set displayWidth(value: number);
    get displayHeight(): number;
    set displayHeight(value: number);
    get scaleX(): number;
    set scaleX(value: number);
    get scaleY(): number;
    set scaleY(value: number);
    setScale(scale: number): void;
    get skewX(): number;
    set skewX(value: number);
    get skewY(): number;
    set skewY(value: number);
    get alpha(): number;
    set alpha(value: number);
    get visible(): boolean;
    set visible(value: boolean);
    destroy(): void;
    getAttNum(attName: string): number;
    get y(): number;
    set y(value: number);
    get x(): number;
    set x(value: number);
    private updateTransform;
    private roundVal;
    incX(val: number): void;
    incY(val: number): void;
    getBoundingClientRect(): DOMRect;
    get gameWRatio(): number;
    set gameWRatio(value: number);
    get gameHRatio(): number;
    set gameHRatio(value: number);
    adjust(): void;
    adjustForward(): void;
    updateScale(): void;
    doResize(): void;
}
