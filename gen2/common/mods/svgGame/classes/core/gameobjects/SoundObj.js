"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SoundObj = void 0;
var SoundObj = /** @class */ (function () {
    function SoundObj(src, loop) {
        if (loop === void 0) { loop = false; }
        this.sound = document.createElement("audio");
        this.sound.src = src;
        this.sound.setAttribute("preload", "auto");
        this.sound.setAttribute("controls", "none");
        this.sound.style.display = "none";
        this.sound.loop = loop;
        document.body.appendChild(this.sound);
    }
    SoundObj.prototype.play = function () {
        this.sound.play();
    };
    SoundObj.prototype.stop = function () {
        this.sound.pause();
    };
    return SoundObj;
}());
exports.SoundObj = SoundObj;
//# sourceMappingURL=SoundObj.js.map