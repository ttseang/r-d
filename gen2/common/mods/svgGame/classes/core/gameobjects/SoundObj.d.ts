export declare class SoundObj {
    sound: HTMLAudioElement;
    constructor(src: string, loop?: boolean);
    play(): void;
    stop(): void;
}
