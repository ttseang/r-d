"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TweenObj = void 0;
var TweenObj = /** @class */ (function () {
    function TweenObj() {
        this.x = null;
        this.y = null;
        this.alpha = null;
        this.angle = null;
        this.displayWidth = null;
        this.displayHeight = null;
        this.scaleX = null;
        this.scaleY = null;
        this.skewX = null;
        this.skewY = null;
        this.duration = 1000;
        this.onComplete = function () { };
    }
    return TweenObj;
}());
exports.TweenObj = TweenObj;
//# sourceMappingURL=TweenObj.js.map