import { IGameObj } from "../interfaces/IGameObj";
import { TweenObj } from "./TweenObj";
export declare class SimpleTween {
    private gameObj;
    private tweenObj;
    private autoStart;
    /**
     * Targets
     */
    private xTarget;
    private yTarget;
    private alphaTarget;
    private angleTarget;
    private htarget;
    private wtarget;
    private scaleXTarget;
    private scaleYTarget;
    private skewXTarget;
    private skewYTarget;
    /**
     * Increments
     *  */
    private xInc;
    private yInc;
    private alphaInc;
    private angleInc;
    private widthInc;
    private heightInc;
    private scaleXInc;
    private scaleYInc;
    private skewXInc;
    private skewYInc;
    /**
     * Timer
     */
    private myTimer;
    private myTime;
    constructor(gameObj: IGameObj, tweenObj: TweenObj, autoStart?: boolean);
    start(): void;
    private doStep;
}
