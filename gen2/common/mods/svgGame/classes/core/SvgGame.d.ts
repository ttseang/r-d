import { GameOptions } from "./dataobjs/gameOptions";
export declare class SVGGame {
    private gameEl;
    gw: number;
    gh: number;
    private screenManager;
    private gameManager;
    constructor(gameID: string, gameOptions: GameOptions);
}
