export declare class ArrowDrawCanvas {
    private startX;
    private startY;
    private rx;
    private ry;
    curvePosition: string;
    private drawArrow;
    color: string;
    private isDown;
    private element;
    private parentElement;
    private active;
    onComplete: Function;
    onStart: Function;
    private arrowStartX;
    private arrowStartY;
    private message;
    private messagePos;
    private messageColor;
    private textPointer;
    private textPointerFill;
    private containerW;
    private containerH;
    usePercentage: boolean;
    private moveFunction;
    private upFunction;
    private downFunction;
    private drawOnce;
    constructor(element: SVGSVGElement, parentElement: HTMLElement);
    setArrowStart(xx: number, yy: number): void;
    setPointer(text: string, fill: string): void;
    setMessage(message: string, position: number, color?: string): void;
    startDebug(): void;
    start(once?: boolean): void;
    stop(): void;
    startRectDrag(e: MouseEvent): void;
    onMouseMove(e: MouseEvent): void;
    stopDragRect(e: MouseEvent): void;
    getPercentData(): string | null;
    getData(): string | null;
    testData(data: string): void;
    testResize(): void;
    setDebug(debug: boolean): void;
}
