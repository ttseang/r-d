import { ArrowDrawCanvas } from "./ArrowDrawCanvas";
var ArrowTest = /** @class */ (function () {
    function ArrowTest() {
        var _this = this;
        var drawArrowArea = document.getElementById("drawArrowArea");
        var mainContent = document.getElementById("mainContent") || document.createElement("div");
        this.canvas = new ArrowDrawCanvas(drawArrowArea, mainContent);
        this.canvas.usePercentage = true;
        this.canvas.onComplete = this.showInfo.bind(this);
        // canvas.start();
        this.canvas.start();
        var btnTest = document.getElementById("btnTest");
        btnTest.onclick = function () {
            _this.resizeDrawArrowArea();
        };
    }
    ArrowTest.prototype.resizeDrawArrowArea = function () {
        /*  const drawArrowArea: SVGSVGElement = document.getElementById("drawArrowArea") as unknown as SVGSVGElement;
         drawArrowArea.style.width = "300px";
         drawArrowArea.style.height = "300px"; */
        var mainContent = document.getElementById("mainContent") || document.createElement("div");
        var s = Math.floor(Math.random() * 1000) + 50;
        mainContent.style.width = s.toString() + "px";
        mainContent.style.height = s.toString() + "px";
        this.canvas.testResize();
    };
    ArrowTest.prototype.showInfo = function (data) {
        console.log(this.canvas.getData());
        console.log(data);
        this.canvas.testData(data);
    };
    return ArrowTest;
}());
export { ArrowTest };
//# sourceMappingURL=ArrowTest.js.map