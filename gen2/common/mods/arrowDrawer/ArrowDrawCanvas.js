import { ArrowUtil, SvgArrow } from "@teachingtextbooks/svgarrow";
var ArrowDrawCanvas = /** @class */ (function () {
    function ArrowDrawCanvas(element, parentElement) {
        this.startX = 0;
        this.startY = 0;
        /*  private endY: number = 0;
         private endX: number = 0; */
        this.rx = 0;
        this.ry = 0;
        this.curvePosition = "cw";
        //private dragRect: SvgObj;
        //  private selectRect: SvgObj;
        this.drawArrow = null;
        this.color = "black";
        this.isDown = false;
        this.active = true;
        this.onComplete = function () { };
        this.onStart = function () { };
        this.arrowStartX = -1;
        this.arrowStartY = -1;
        this.message = "";
        this.messagePos = 0;
        this.messageColor = "black";
        //arrow pointer
        this.textPointer = "&#9733";
        this.textPointerFill = "black";
        this.containerW = 0;
        this.containerH = 0;
        this.usePercentage = false;
        this.drawOnce = false;
        this.element = element;
        this.parentElement = parentElement;
        ArrowUtil.makeCss();
        ArrowUtil.makeDefs();
        this.moveFunction = this.onMouseMove.bind(this);
        this.upFunction = this.stopDragRect.bind(this);
        this.downFunction = this.startRectDrag.bind(this);
        if (this.element) {
            this.drawArrow = new SvgArrow("arrowtemp2", this.element);
            //  this.drawArrow.usePercentages=true;
            if (this.drawArrow.el) {
                this.element.append(this.drawArrow.el);
            }
        }
        else {
            console.warn("drawArrowArea missing");
        }
    }
    ArrowDrawCanvas.prototype.setArrowStart = function (xx, yy) {
        this.arrowStartX = xx;
        this.arrowStartY = yy;
    };
    ArrowDrawCanvas.prototype.setPointer = function (text, fill) {
        this.textPointer = text;
        this.textPointerFill = fill;
    };
    ArrowDrawCanvas.prototype.setMessage = function (message, position, color) {
        if (color === void 0) { color = "black"; }
        this.message = message;
        this.messagePos = position;
        this.messageColor = color;
    };
    ArrowDrawCanvas.prototype.startDebug = function () {
        var _this = this;
        if (this.parentElement) {
            this.parentElement.onmousedown = function (e) {
                // console.log(e.clientX,e.clientY);
                if (_this.parentElement) {
                    var bb = _this.parentElement.getBoundingClientRect();
                    var px = bb.left;
                    var py = bb.top;
                    var xx = e.clientX - px;
                    var yy = e.clientY - py;
                    //    console.log(xx,yy);
                }
            };
        }
    };
    ArrowDrawCanvas.prototype.start = function (once) {
        if (once === void 0) { once = true; }
        this.drawOnce = once;
        if (this.drawArrow) {
            if (this.drawArrow.el) {
                this.drawArrow.el.id = "drawArrow";
                // this.drawArrow.setEnd("arrowpoint");
                this.drawArrow.setFill(this.color);
            }
            this.drawArrow.visible = false;
            if (this.parentElement) {
                this.parentElement.addEventListener("mousedown", this.downFunction);
                // this.parentElement.addEventListener("mouseup", this.upFunction);
                // this.parentElement.addEventListener("mousemove", this.moveFunction);
            }
        }
    };
    ArrowDrawCanvas.prototype.stop = function () {
        //probably not needed anymore but just in case
        if (this.parentElement) {
            this.parentElement.removeEventListener("mousedown", this.downFunction);
            this.parentElement.removeEventListener("mouseup", this.upFunction);
            this.parentElement.removeEventListener("mousemove", this.moveFunction);
        }
        this.active = false;
    };
    ArrowDrawCanvas.prototype.startRectDrag = function (e) {
        //console.log(e);
        if (this.active === false) {
            return;
        }
        if (!this.drawArrow) {
            return;
        }
        this.isDown = true;
        if (!this.parentElement) {
            return;
        }
        this.onStart();
        var bb = this.parentElement.getBoundingClientRect();
        var px = bb.left;
        var py = bb.top;
        var xx = e.clientX - px;
        var yy = e.clientY - py;
        // console.log(xx,yy);
        if (this.arrowStartX !== -1 && this.arrowStartY !== -1) {
            this.startX = this.arrowStartX;
            this.startY = this.arrowStartY;
        }
        else {
            this.startX = xx;
            this.startY = yy;
        }
        this.drawArrow.messageColor = this.messageColor;
        this.drawArrow.message = this.message;
        this.drawArrow.messagePos = this.messagePos;
        this.drawArrow.x = this.startX;
        this.drawArrow.y = this.startY;
        this.drawArrow.visible = true;
        if (this.parentElement) {
            this.parentElement.removeEventListener("mousedown", this.downFunction);
            this.parentElement.addEventListener("mouseup", this.upFunction);
            this.parentElement.addEventListener("mousemove", this.moveFunction);
        }
    };
    ArrowDrawCanvas.prototype.onMouseMove = function (e) {
        // console.log("MOVE");
        if (this.active === false) {
            return;
        }
        if (!this.drawArrow) {
            return;
        }
        if (this.isDown === true) {
            //we need to get the relative position
            //by subtracting the current mouse position
            if (!this.parentElement) {
                return;
            }
            var bb = this.parentElement.getBoundingClientRect();
            var px = bb.left;
            var py = bb.top;
            var xx = e.clientX - px;
            var yy = e.clientY - py;
            var w = xx - this.startX;
            var h = yy - this.startY;
            var angle = 0, perc = 0.5;
            switch (this.curvePosition) {
                case "cw": {
                    angle = -45;
                    perc = 0.75;
                    break;
                }
                case "ccw": {
                    angle = 45;
                    perc = 0.75;
                    break;
                }
            }
            var _a = ArrowUtil.getRotate(0, 0, w, h, angle), rx = _a.rx, ry = _a.ry;
            this.rx = ArrowUtil.lerp(0, rx, perc);
            this.ry = ArrowUtil.lerp(0, ry, perc);
            /**set the properties on the arrow
            DON'T draw on the element
            we need to be able to edit and rebuild
            based on the properties**/
            this.drawArrow.setCurve(this.rx, this.ry);
            this.drawArrow.x = this.startX;
            this.drawArrow.y = this.startY;
            this.drawArrow.setLen(w);
            this.drawArrow.setH(h);
            //this.drawArrow.setFill(this.color);
            //this.addEndings(this.drawArrow, this.endingIndex);
        }
    };
    ArrowDrawCanvas.prototype.stopDragRect = function (e) {
        if (this.active === false) {
            return;
        }
        if (!this.drawArrow) {
            return;
        }
        if (!this.parentElement) {
            return;
        }
        var bb = this.parentElement.getBoundingClientRect();
        var px = bb.left;
        var py = bb.top;
        var xx = e.clientX - px;
        var yy = e.clientY - py;
        /*  this.endX = xx;
         this.endY = yy; */
        if (e.clientY < 100) {
            return;
        }
        this.isDown = false;
        // this.dragRect.visible=false;
        //   this.drawArrow.visible = false;
        if (this.usePercentage === true) {
            this.onComplete(this.drawArrow.exportPercentData());
        }
        else {
            this.onComplete(this.drawArrow.exportData());
        }
        if (this.parentElement) {
            if (this.drawOnce === false) {
                this.parentElement.addEventListener("mousedown", this.downFunction);
            }
            this.parentElement.removeEventListener("mouseup", this.upFunction);
            this.parentElement.removeEventListener("mousemove", this.moveFunction);
        }
    };
    ArrowDrawCanvas.prototype.getPercentData = function () {
        if (this.drawArrow) {
            return this.drawArrow.exportPercentData();
        }
        return null;
    };
    ArrowDrawCanvas.prototype.getData = function () {
        if (this.drawArrow) {
            return this.drawArrow.exportData();
        }
        return null;
    };
    ArrowDrawCanvas.prototype.testData = function (data) {
        if (this.drawArrow) {
            this.drawArrow.usePercentages = true;
            this.drawArrow.fromString(data);
        }
    };
    ArrowDrawCanvas.prototype.testResize = function () {
        if (this.drawArrow) {
            this.drawArrow.onResize();
        }
    };
    ArrowDrawCanvas.prototype.setDebug = function (debug) {
        if (this.drawArrow) {
            this.drawArrow.debug = debug;
        }
    };
    return ArrowDrawCanvas;
}());
export { ArrowDrawCanvas };
//# sourceMappingURL=ArrowDrawCanvas.js.map