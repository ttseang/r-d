import { SvgObj, PosVo, IScreen } from "C:/Users/USER/TT/r-d/gen2/common/mods/svgGame";
import { IDragObj } from "../interfaces/IDragObj";
export declare class BaseDragObj extends SvgObj implements IDragObj {
    type: string;
    canDrag: boolean;
    content: string;
    original: PosVo;
    place: PosVo;
    resetPlace: PosVo;
    constructor(screen: IScreen, key: string);
    doResize(): void;
    snapBack(): void;
    private snapDone;
    initPlace(pos: PosVo): void;
    setPlace(pos: PosVo): void;
    setContent(content: string): void;
    getContainer(): SvgObj;
}
