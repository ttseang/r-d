"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Box = void 0;
var BaseDragObj_1 = require("./BaseDragObj");
var Box = /** @class */ (function (_super) {
    __extends(Box, _super);
    function Box(iScreen) {
        var _this = _super.call(this, iScreen, "wordBox") || this;
        _this.textEl = null;
        _this.text = "";
        return _this;
    }
    Box.prototype.setText = function (text) {
        this.text = text;
        this.content = text;
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];
            this.textEl.innerHTML = text;
        }
    };
    Box.prototype.doResize = function () {
        this.updateScale();
        _super.prototype.doResize.call(this);
    };
    return Box;
}(BaseDragObj_1.BaseDragObj));
exports.Box = Box;
//# sourceMappingURL=Box.js.map