import { IScreen, PosVo, SvgObj } from "C:/Users/USER/TT/r-d/gen2/common/mods/svgGame";
import { IDragTarg } from "../interfaces/IDragTarg";
export declare class BaseTargObj extends SvgObj implements IDragTarg {
    correct: string;
    original: PosVo;
    place: PosVo;
    content: string;
    color: number;
    constructor(screen: IScreen, key: string);
    setColor(color: number): void;
    setPlace(pos: PosVo): void;
    setContent(content: string): void;
    getContainer(): SvgObj;
    doResize(): void;
}
