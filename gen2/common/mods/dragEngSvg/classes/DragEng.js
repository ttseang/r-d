"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DragEng = void 0;
var svgGame_1 = require("C:/Users/USER/TT/r-d/gen2/common/mods/svgGame");
var DragEng = /** @class */ (function () {
    function DragEng(bscreen) {
        this.targets = [];
        this.dragObjs = [];
        this.clickLock = false;
        this.dragBox = null;
        this.lb = null;
        this.downCallback = function () { };
        this.upCallback = function () { };
        this.dropCallback = function () { };
        this.collideCallback = function () { };
        this.snapBackOnContent = true;
        this.setContentOnCollide = true;
        this.gm = svgGame_1.GameManager.getInstance();
        this.dragMap = new Map();
        this.snapToTarget = false;
        this.bscreen = bscreen;
    }
    DragEng.prototype.addDrag = function (dragObj) {
        dragObj.type = "dragObj";
        this.lb = dragObj;
        // console.log(dragObj.elID);
        this.dragMap.set(dragObj.elID, dragObj);
        this.dragObjs.push(dragObj);
    };
    DragEng.prototype.addTarget = function (target) {
        this.targets.push(target);
    };
    DragEng.prototype.destoryDrags = function () {
        for (var i = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].getContainer().destroy();
        }
        this.dragObjs = [];
    };
    DragEng.prototype.destroyTargs = function () {
        for (var i = 0; i < this.targets.length; i++) {
            this.targets[i].getContainer().destroy();
        }
        this.targets = [];
    };
    DragEng.prototype.doResize = function () {
        for (var i = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].doResize();
        }
        for (var i = 0; i < this.targets.length; i++) {
            this.targets[i].doResize();
        }
    };
    DragEng.prototype.setListeners = function () {
        document.onpointerdown = this.onDown.bind(this);
        document.onpointermove = this.onPointerMove.bind(this);
        document.onpointerup = this.onUp.bind(this);
    };
    DragEng.prototype.mixUpBoxes = function () {
        for (var i = 0; i < 10; i++) {
            var r1 = Math.floor(Math.random() * this.dragObjs.length);
            var r2 = Math.floor(Math.random() * this.dragObjs.length);
            if (r1 != r2) {
                var box1 = this.dragObjs[r1];
                var box2 = this.dragObjs[r2];
                var place1 = box1.place;
                var place2 = box2.place;
                box1.initPlace(place2);
                box2.initPlace(place1);
            }
            /*  box1.place=place2;
             box2.place=place1;
  
             box1.original=place2;
             box2.original=place1; */
        }
    };
    DragEng.prototype.onDown = function (p) {
        if (this.clickLock == true) {
            return;
        }
        var target = p.target;
        if (target) {
            var top_1 = svgGame_1.SVGUtil.findTop(target, "g");
            if (top_1) {
                //top.parentElement?.appendChild(top);
                var targID = top_1.id;
                //console.log(targID);
                var obj = this.dragMap.get(targID);
                if ((obj === null || obj === void 0 ? void 0 : obj.canDrag) == false) {
                    return;
                }
                window.dt = obj;
                //   console.log(obj);
                if (obj) {
                    this.dragBox = obj;
                }
                this.downCallback(obj);
                //bring to top
                //top.setAttribute("zIndex","1000");
                /*  let addArea:HTMLElement|null=document.getElementById(this.gm.gameOptions.addAreaID);
                 if (addArea)
                 {
                     
                    // addArea.appendChild(top);
                 } */
            }
        }
        /* if (dragObj.type!="dragObj")
        {
            return;
        }
        this.dragBox = dragObj;
        this.dragBox.resetPlace = new PosVo(this.dragBox.x, this.dragBox.y); */
        // this.scene.children.bringToTop(dragObj.getContainer());
    };
    DragEng.prototype.onPointerMove = function (p) {
        if (this.dragBox) {
            this.dragBox.x = p.clientX - this.dragBox.displayWidth / 2;
            this.dragBox.y = p.clientY - this.dragBox.displayHeight / 2;
        }
    };
    DragEng.prototype.onUp = function () {
        var hit = this.checkTargets();
        if (hit == false) {
            if (this.dragBox) {
                console.log("snap 1");
                this.dragBox.snapBack();
            }
        }
        else {
            if (this.dragBox && this.bscreen) {
                if (this.bscreen.grid) {
                    var place = this.bscreen.grid.findNearestGridXYDec(this.dragBox.x, this.dragBox.y);
                    this.dragBox.setPlace(place);
                }
            }
            this.dropCallback();
        }
        this.dragBox = null;
    };
    DragEng.prototype.resetBoxes = function () {
        for (var i = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].setPlace(this.dragObjs[i].original);
            this.dragObjs[i].canDrag = true;
        }
        for (var i = 0; i < this.targets.length; i++) {
            this.targets[i].content = "";
        }
    };
    DragEng.prototype.getTargetContent = function () {
        var content = "";
        for (var i = 0; i < this.targets.length; i++) {
            content += this.targets[i].content;
        }
        return content;
    };
    DragEng.prototype.checkTargets = function () {
        if (this.dragBox === null) {
            return false;
        }
        for (var i = 0; i < this.targets.length; i++) {
            var targetBox = this.targets[i];
            if (svgGame_1.CollideUtil.checkOverlap(this.dragBox.getContainer(), targetBox.getContainer())) {
                this.dragBox.setPlace(targetBox.place);
                if (this.snapBackOnContent == true) {
                    if (targetBox.content != "") {
                        console.log("snap 2");
                        this.dragBox.snapBack();
                        return;
                    }
                }
                if (this.snapToTarget) {
                    this.dragBox.x = targetBox.x + targetBox.displayWidth / 2 - this.dragBox.displayWidth / 2;
                    this.dragBox.y = targetBox.y + targetBox.displayHeight / 2 - this.dragBox.displayHeight / 2;
                }
                //  this.chosenContent = this.dragBox.content;
                if (this.setContentOnCollide == true) {
                    targetBox.content = this.dragBox.content;
                }
                this.collideCallback(this.dragBox, targetBox);
                // this.clickLock = true;
                return true;
            }
        }
        return false;
    };
    return DragEng;
}());
exports.DragEng = DragEng;
//# sourceMappingURL=DragEng.js.map