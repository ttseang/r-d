import { IScreen } from "svggame";
import { IDragObj } from "../interfaces/IDragObj";
import { BaseDragObj } from "./BaseDragObj";
export declare class Box extends BaseDragObj implements IDragObj {
    private textEl;
    text: string;
    constructor(iScreen: IScreen);
    setText(text: string): void;
    doResize(): void;
}
