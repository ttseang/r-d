import { IScreen } from "svggame";
import { IDragTarg } from "../interfaces/IDragTarg";
import { BaseTargObj } from "./BaseTargObj";
export declare class TargBox extends BaseTargObj implements IDragTarg {
    private textEl;
    text: string;
    constructor(iScreen: IScreen);
    doResize(): void;
}
