"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDragObj = void 0;
var svgGame_1 = require("C:/Users/USER/TT/r-d/gen2/common/mods/svgGame");
var BaseDragObj = /** @class */ (function (_super) {
    __extends(BaseDragObj, _super);
    function BaseDragObj(screen, key) {
        var _this = _super.call(this, screen, key) || this;
        _this.type = "";
        _this.canDrag = true;
        _this.content = "";
        _this.original = new svgGame_1.PosVo(0, 0);
        _this.place = new svgGame_1.PosVo(0, 0);
        _this.resetPlace = new svgGame_1.PosVo(0, 0);
        return _this;
    }
    BaseDragObj.prototype.doResize = function () {
        var _a;
        (_a = this.screen.grid) === null || _a === void 0 ? void 0 : _a.placeAt(this.place.x, this.place.y, this);
    };
    BaseDragObj.prototype.snapBack = function () {
        var _this = this;
        // throw new Error("Method not implemented.");
        this.canDrag = false;
        this.screen.tweenGridPos(this.original.x, this.original.y, this, 300, function () { _this.snapDone(); });
    };
    BaseDragObj.prototype.snapDone = function () {
        this.canDrag = true;
    };
    BaseDragObj.prototype.initPlace = function (pos) {
        var _a;
        this.original = pos;
        this.place = pos;
        (_a = this.screen.grid) === null || _a === void 0 ? void 0 : _a.placeAt(pos.x, pos.y, this);
    };
    BaseDragObj.prototype.setPlace = function (pos) {
        var _a;
        this.place = pos;
        (_a = this.screen.grid) === null || _a === void 0 ? void 0 : _a.placeAt(pos.x, pos.y, this);
    };
    BaseDragObj.prototype.setContent = function (content) {
        this.content = content;
    };
    BaseDragObj.prototype.getContainer = function () {
        return this;
    };
    return BaseDragObj;
}(svgGame_1.SvgObj));
exports.BaseDragObj = BaseDragObj;
//# sourceMappingURL=BaseDragObj.js.map