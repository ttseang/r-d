"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseTargObj = void 0;
var svgGame_1 = require("C:/Users/USER/TT/r-d/gen2/common/mods/svgGame");
var BaseTargObj = /** @class */ (function (_super) {
    __extends(BaseTargObj, _super);
    function BaseTargObj(screen, key) {
        var _this = _super.call(this, screen, key) || this;
        _this.correct = "";
        _this.original = new svgGame_1.PosVo(0, 0);
        _this.place = new svgGame_1.PosVo(0, 0);
        _this.content = "";
        _this.color = 0;
        return _this;
    }
    BaseTargObj.prototype.setColor = function (color) {
        this.color = color;
    };
    BaseTargObj.prototype.setPlace = function (pos) {
        var _a;
        this.place = pos;
        (_a = this.screen.grid) === null || _a === void 0 ? void 0 : _a.placeAt(pos.x, pos.y, this);
    };
    BaseTargObj.prototype.setContent = function (content) {
        this.content = content;
    };
    BaseTargObj.prototype.getContainer = function () {
        return this;
    };
    BaseTargObj.prototype.doResize = function () {
        var _a;
        (_a = this.screen.grid) === null || _a === void 0 ? void 0 : _a.placeAt(this.place.x, this.place.y, this);
    };
    return BaseTargObj;
}(svgGame_1.SvgObj));
exports.BaseTargObj = BaseTargObj;
//# sourceMappingURL=BaseTargObj.js.map