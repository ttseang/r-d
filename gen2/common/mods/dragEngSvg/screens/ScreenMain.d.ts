import { BaseScreen } from "svggame";
import { IDragObj } from "../interfaces/IDragObj";
import { IDragTarg } from "../interfaces/IDragTarg";
export declare class ScreenMain extends BaseScreen {
    private dragEng;
    constructor();
    create(): void;
    doResize(): void;
    onCollide(d: IDragObj, t: IDragTarg): void;
    onDrop(): void;
    onUp(): void;
}
