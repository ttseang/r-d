"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScreenMain = void 0;
var svggame_1 = require("svggame");
var Box_1 = require("../classes/Box");
var DragEng_1 = require("../classes/DragEng");
var TargBox_1 = require("../classes/TargBox");
var ScreenMain = /** @class */ (function (_super) {
    __extends(ScreenMain, _super);
    function ScreenMain() {
        var _this = _super.call(this, "ScreenMain") || this;
        window.scene = _this;
        return _this;
    }
    ScreenMain.prototype.create = function () {
        // this.grid?.showGrid();
        //make the engine and set options
        this.dragEng = new DragEng_1.DragEng(this);
        this.dragEng.snapToTarget = true;
        this.dragEng.collideCallback = this.onCollide.bind(this);
        this.dragEng.upCallback = this.onUp.bind(this);
        this.dragEng.dropCallback = this.onDrop.bind(this);
        //make drop target boxes
        for (var i = 0; i < 4; i++) {
            var tBox = new TargBox_1.TargBox(this);
            tBox.gameWRatio = 0.08;
            //place on grid
            tBox.setPlace(new svggame_1.PosVo(3 + i * 2, 2));
            //add target to the engine
            this.dragEng.addTarget(tBox);
        }
        //test word
        var word = "fish";
        for (var i = 0; i < 4; i++) {
            var box = new Box_1.Box(this);
            box.setText(word.substr(i, 1));
            //scale
            box.gameWRatio = 0.05;
            /* box.displayWidth = this.grid!.cw;
            box.displayHeight = this.grid!.ch; */
            //place on grid
            box.initPlace(new svggame_1.PosVo(3 + i * 2, 1));
            //add the drag box the engine
            this.dragEng.addDrag(box);
        }
        this.dragEng.mixUpBoxes();
        //activate the built in listeners
        this.dragEng.setListeners();
    };
    ScreenMain.prototype.doResize = function () {
        console.log("RESIZE");
        //place custom resize here
        _super.prototype.doResize.call(this);
        this.grid.showGrid();
    };
    ScreenMain.prototype.onCollide = function (d, t) {
        console.log("collide");
        d.canDrag = false;
    };
    ScreenMain.prototype.onDrop = function () {
        console.log("dropped");
        console.log(this.dragEng.getTargetContent());
        //check drop content here
    };
    ScreenMain.prototype.onUp = function () {
        console.log("up");
    };
    return ScreenMain;
}(svggame_1.BaseScreen));
exports.ScreenMain = ScreenMain;
//# sourceMappingURL=ScreenMain.js.map