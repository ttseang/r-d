import { CacheManager } from "./cacheManager";
export class TestScreen {
    constructor() {
        this.cache = CacheManager.getInstance();
        this.btnGet = document.getElementById("btnGet");
        this.btnSet = document.getElementById("btnSet");
        this.btnLocal = document.getElementById("btnLocal");
        this.btnLoad = document.getElementById("btnLoad");
        this.btnGet.onclick = this.getData.bind(this);
        this.btnSet.onclick = this.setData.bind(this);
        this.btnLocal.onclick = this.saveToLocal.bind(this);
        this.btnLoad.onclick = this.loadFromLocal.bind(this);
    }
    setData() {
        let elKey = document.getElementById("keyText");
        let elVal = document.getElementById("valueText");
        let elEx = document.getElementById("exVal");
        let keyText = elKey.value;
        let valText = elVal.value;
        let exText = elEx.value;
        console.log(keyText + " " + valText + " " + exText);
        this.cache.saveToCache(keyText, valText, parseInt(exText));
    }
    getData() {
        let elKey = document.getElementById("rkey");
        let keyText = elKey.value;
        console.log(keyText);
        let value = this.cache.getFromCache(keyText, "not found");
        let outputSpan = document.getElementById("outputtext");
        outputSpan.innerText = value;
    }
    saveToLocal() {
        this.cache.saveToLocal("myapp");
    }
    loadFromLocal() {
        this.cache.loadFromLocal("myapp");
    }
}
//# sourceMappingURL=testScreen.js.map