# ttcachemanager

ttcachemanager is used for caching data from URL calls

## Installation

Use the package manager to install ttcachemanager.

```
npm i @teachingtextbooks/ttcachemanager
```

## Usage & Methods

### Setup

The CacheManager is a singleton that uses a getInstance function to return an instance
This will allow you to use the cache manager in different parts of your code and access 
the same cache data

```
import {CacheManager} from "ttcachemanager";
let cm:CacheManager=CacheManager.getInstance();
```
### saveToCache

#### purpose

saves a value to the cache by key for a set length of time

#### parameters

key:string - a unique key string to save the value
value:string - the data you want to store
expires:number (optional) - the time in seconds you want the data to persist. Use 0 ify ou don't want the data to expire

#### returns

void

#### example

Save without expiration

```
cm.saveToCache("myKey","something I want to save",0);
```

Save for 20 minutes - 60 secs x 20

```
cm.saveToCache("myKey","something I want to save",1200);
```

### getFromCache

#### purpose

Retrieves a value from the cache by if it exists. If no key is found
the default value

#### parameters

-key:string - the key that was used to save the value
-defaultValue:string - the value to be given if the cache data is not found

#### returns

string

The value of the cached entry or the default value passed in the call


#### example

```
 cm.getFromCache("mykey","nothing found")
```

### removeFromCache
#### purpose
to manually remove data from the cache
#### parameters

key:string - the key that was used to save the value

#### returns
void

#### example
```
 cm.removeFromCache("myKey");
```

### saveToLocal

#### purpose

saves the entire cache to local file - this is useful if the user refreshes the screen

#### parameters

appname:string - a unique string key used to save the cache

#### returns

void

#### example
```
 cm.saveToLocal("myapp");
```

### loadFromLocal

#### purpose

restores the cache from the local storage

#### parameters

appname:string - a unique string key that was used to save the cache

#### returns

boolean true or false depending if the cache could be restored or not

#### example
```
cm.loadFromLocal("myapp");
```