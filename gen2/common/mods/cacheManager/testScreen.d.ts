export declare class TestScreen {
    private btnGet;
    private btnSet;
    private btnLocal;
    private btnLoad;
    private cache;
    constructor();
    setData(): void;
    getData(): void;
    saveToLocal(): void;
    loadFromLocal(): void;
}
