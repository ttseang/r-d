export class CacheManager {
    constructor() {
        this.cacheMap = new Map();
    }
    static getInstance() {
        if (this.instance === null) {
            this.instance = new CacheManager();
        }
        return this.instance;
    }
    saveToCache(key, value, expires = 0) {
        expires = expires * 1000;
        let cacheVo = new CacheVo(key, value, expires);
        this.cacheMap.set(key, cacheVo);
    }
    removeFromCache(key) {
        return this.cacheMap.delete(key);
    }
    getFromCache(key, defaultValue) {
        if (this.cacheMap.has(key)) {
            let cacheVo = this.cacheMap.get(key);
            if (cacheVo) {
                let expires = cacheVo.expires;
                if (expires !== 0) {
                    let created = cacheVo.created;
                    let now = new Date().getTime();
                    let timeLimit = expires + created;
                    let diff = now - timeLimit;
                    if (now > timeLimit) {
                        this.cacheMap.delete(key);
                        return defaultValue;
                    }
                    return cacheVo.value;
                }
                else {
                    return cacheVo.value;
                }
            }
            else {
                return defaultValue;
            }
        }
        return defaultValue;
    }
    saveToLocal(appname) {
        let cacheArray = [];
        this.cacheMap.forEach(((value, key, map) => {
            cacheArray.push(JSON.stringify(value));
        }));
        let cacheString = cacheArray.join("|");
        localStorage.setItem("ls-" + appname, cacheString);
    }
    loadFromLocal(appname) {
        let loadString = localStorage.getItem("ls-" + appname);
        if (loadString) {
            let loadArray = loadString.split("|");
            for (let i = 0; i < loadArray.length; i++) {
                let cacheVo = new CacheVo("", "", 0);
                let data = JSON.parse(loadArray[i]);
                cacheVo.fromObj(data);
                this.cacheMap.set(cacheVo.key, cacheVo);
            }
        }
    }
}
CacheManager.instance = null;
class CacheVo {
    constructor(key, value, expires = 0) {
        this.key = key;
        this.value = value;
        this.created = new Date().getTime();
        this.expires = expires;
    }
    fromObj(data) {
        this.key = data.key;
        this.value = data.value;
        this.created = parseInt(data.created);
        this.expires = parseInt(data.expires);
    }
}
//# sourceMappingURL=cacheManager.js.map