export declare class CacheManager {
    private static instance;
    private cacheMap;
    constructor();
    static getInstance(): CacheManager;
    saveToCache(key: string, value: string, expires?: number): void;
    removeFromCache(key: string): boolean;
    getFromCache(key: string, defaultValue: string): string;
    saveToLocal(appname: string): void;
    loadFromLocal(appname: string): void;
}
