export declare class ArrowVo {
    x: number;
    y: number;
    startX: number;
    startY: number;
    endX: number;
    endY: number;
    cx: number;
    cy: number;
    fill: string;
    message: string;
    messagePos: number;
    messageColor: string;
    messageSize: string;
    textPointerHtml: string;
    textPointerFill: string;
    textPointerSize: string;
    endMark: string;
    startMark: string;
    animateSpeed: number;
    strokeWidth: number;
    constructor(x?: number, y?: number, startX?: number, startY?: number, endX?: number, endY?: number, cx?: number, cy?: number, fill?: string, startMark?: string, endMark?: string);
    fromObj(obj: any): void;
}
