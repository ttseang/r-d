var ArrowVo = /** @class */ (function () {
    function ArrowVo(x, y, startX, startY, endX, endY, cx, cy, fill, startMark, endMark) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        if (startX === void 0) { startX = 0; }
        if (startY === void 0) { startY = 0; }
        if (endX === void 0) { endX = 0; }
        if (endY === void 0) { endY = 0; }
        if (cx === void 0) { cx = 0; }
        if (cy === void 0) { cy = 0; }
        if (fill === void 0) { fill = "black"; }
        if (startMark === void 0) { startMark = ""; }
        if (endMark === void 0) { endMark = ""; }
        //location of the arrow on the stage
        this.x = 0;
        this.y = 0;
        //start of the arrow line - almost always 0,0
        this.startX = 0;
        this.startY = 0;
        //end of the arrow
        this.endX = 400;
        this.endY = 0;
        //curve position
        this.cx = 200;
        this.cy = -150;
        this.message = "";
        this.messagePos = 0;
        this.messageColor = "black";
        this.messageSize = "medium";
        this.textPointerHtml = "&#x25b6;";
        this.textPointerFill = "black";
        this.textPointerSize = "medium";
        this.animateSpeed = 2;
        this.strokeWidth = 2;
        this.x = x;
        this.y = y;
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.cx = cx;
        this.cy = cy;
        this.fill = fill;
        this.startMark = startMark;
        this.endMark = endMark;
    }
    ArrowVo.prototype.fromObj = function (obj) {
        this.x = parseFloat(obj.x);
        this.y = parseFloat(obj.y);
        this.startX = parseFloat(obj.startX);
        this.startY = parseFloat(obj.startY);
        this.endX = parseFloat(obj.endX);
        this.endY = parseFloat(obj.endY);
        this.cx = parseFloat(obj.cx);
        this.cy = parseFloat(obj.cy);
        this.strokeWidth = parseFloat(obj.strokeWidth);
        this.fill = obj.fill;
        this.message = obj.message;
        this.messagePos = parseFloat(obj.messagePos);
        this.messageColor = obj.messageColor;
        this.messageSize = obj.messageSize;
        this.textPointerHtml = obj.textPointerHtml;
        this.textPointerFill = obj.textPointerFill;
        this.textPointerSize = obj.textPointerSize;
        this.endMark = obj.endMark;
        this.startMark = obj.startMark;
        this.animateSpeed = parseFloat(obj.animateSpeed);
    };
    return ArrowVo;
}());
export { ArrowVo };
//# sourceMappingURL=ArrowVo.js.map