var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { ArrowVo } from "./dataObjs/ArrowVo";
import { SvgObj } from "./SvgObj";
//import { ArrowUtil } from "./util/ArrowUtil";
var SvgArrow = /** @class */ (function (_super) {
    __extends(SvgArrow, _super);
    /**
     * Creates an instance of SvgArrow.
     * @param {string} templateID - this is the id of the template in the svg code currently arrowtemp2
     * @param {(SVGSVGElement | null)} [container=null]
     * @memberof SvgArrow
     */
    function SvgArrow(templateID, container) {
        if (container === void 0) { container = null; }
        var _this = _super.call(this, templateID) || this;
        //start and end points of the arrow svg container
        _this.arrowPositionX = 0;
        _this.arrowPositionY = 0;
        //the starting point of the arrow
        //almost always 0,0
        _this.startX = 0;
        _this.startY = 0;
        //relative end point of arrow
        _this.endX = 400;
        _this.endY = 0;
        //point of the curve
        _this.cx = 200;
        _this.cy = -150;
        //drawing arrow speed
        _this.animateSpeed = 2;
        //element for the drawing head of the arrow
        _this.tp = null;
        //message for the arrow
        _this.messagePath = null;
        //pointer attributes
        _this.textPointerSize = "medium";
        _this.textPointerFill = "black";
        _this.textPointerHtml = "&#x25b6;";
        //the length and height of the arrow
        _this.len = 0;
        _this.h = 0;
        //color of the arrow
        _this.fill = "black";
        //color of the arrow to restore when mouseout
        _this.lastFill = "black";
        _this.strokeWidth = 2;
        //classes for the end and start markers
        _this.startClass = "";
        _this.endClass = "";
        //used for setting controls in the probpox
        _this.endIndex = 0;
        _this.colorIndex = 0;
        //element where we draw the line
        _this.lineElement = null;
        //mask - element where dots are added to reveal the line
        _this.maskArea = null;
        /**Message Atrributes */
        //text label for the arrow
        _this.message = "";
        //percentage for the message
        _this.messagePos = 0;
        //message color
        _this.messageColor = "black";
        //message stroke
        _this.messageStroke = "yellow";
        //message font size
        _this.messageSize = "medium";
        //permited classes - helps with marker color
        _this.colorClasses = ['red', 'blue', 'black', 'yellow', 'green',];
        //record ccw,cw or straight. Used for controls
        _this.curvePosition = "";
        //the current position of the line drawn
        _this.drawLineStep = 0;
        //time in miliseconds
        _this.drawLineSpeed = 1000;
        //the number of units to draw with each step
        _this.drawLineInc = 1;
        /* the container is the parent element of the arrow
        when the parent is resized, the arrow is resized by calling onResize
       it will scale to a percentage of the parent based on the start and end points
        and the curve point of the line
     */
        _this.container = null;
        _this.containerW = 0;
        _this.containerH = 0;
        //we use percentages to scale the line arrow to the container
        //but we use pixles when drawing the line in the editor
        _this.usePercentages = false;
        _this.debug = false;
        window.arrow = _this;
        _this.container = container;
        if (_this.container) {
            _this.containerW = _this.container.getBoundingClientRect().width;
            _this.containerH = _this.container.getBoundingClientRect().height;
        }
        //I know normally we use classes instead of ids but the defs tag is a special case
        //we need to be able to add defs to the svg and need a unique id
        //to avoid adding the same defs multiple times
        _this.defs = document.getElementById("svgArrowDefs");
        //draw the initial path
        _this.makeDString();
        //this.el is the svg element of the arrow
        //created by the SVGOBj class when we call super(templateID)
        //if the element is not null
        //add the css class to the arrow element
        //the mask and the line element are svg child elements of the arrow svg definition
        if (_this.el) {
            _this.el.classList.add("svgArrow");
            _this.lineElement = _this.el.getElementsByTagName("path")[0];
            _this.lineElement.id = _this.elID + "line";
            _this.maskArea = _this.el.getElementsByTagName("mask")[0];
            _this.maskArea.id = _this.elID + "mask";
        }
        return _this;
    }
    //this will remove the element from the dom
    SvgArrow.prototype.remove = function () {
        if (this.el) {
            this.el.remove();
        }
    };
    //duplicate of onResize?
    //I'll leave it for now but remove it later if it's not needed
    SvgArrow.prototype.resetContainer = function (container) {
        this.container = container;
        if (this.container) {
            this.containerW = this.container.getBoundingClientRect().width;
            this.containerH = this.container.getBoundingClientRect().height;
        }
        this.makeDString();
    };
    //called when the window or container is resized
    SvgArrow.prototype.onResize = function () {
        if (this.container) {
            this.containerW = this.container.getBoundingClientRect().width;
            this.containerH = this.container.getBoundingClientRect().height;
            this.makeDString();
        }
    };
    SvgArrow.prototype.setResizeListener = function () {
        window.addEventListener("resize", this.onResize.bind(this));
    };
    //used for converting the color name to a class
    SvgArrow.prototype.capFirst = function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };
    //set one of the pretermined color classes
    //we need to limit to certain colors so we can
    //link to the markers
    SvgArrow.prototype.setFill = function (color) {
        for (var i = 0; i < this.colorClasses.length; i++) {
            var colorName2 = "pointerArrow" + this.capFirst(this.colorClasses[i]);
            this.removeStyle(this.colorClasses[i]);
        }
        var colorName = "pointerArrow" + this.capFirst(color);
        this.addStyle(colorName);
        this.fill = color;
        if (this.startClass !== "") {
            this.setStart(this.startClass);
        }
        if (this.endClass !== "") {
            this.setEnd(this.endClass);
        }
        this.makeDString();
    };
    //add or remove styles
    SvgArrow.prototype.addStyle = function (style) {
        if (this.lineElement) {
            this.lineElement.classList.add(style);
        }
    };
    SvgArrow.prototype.removeStyle = function (style) {
        if (this.lineElement) {
            //  ////console.log("remove "+style);
            this.lineElement.classList.remove(style);
        }
    };
    //most of these were used for debugging and testing but I'll leave them in for now
    //TODO:confirm unsed and remove later
    SvgArrow.prototype.setStrokeWidth = function (w) {
        this.strokeWidth = w;
        this.makeDString();
    };
    SvgArrow.prototype.setCurve = function (cx, cy) {
        this.cx = cx;
        this.cy = cy;
        this.makeDString();
    };
    SvgArrow.prototype.flipCurveY = function () {
        this.cy = -this.cy;
        this.makeDString();
    };
    SvgArrow.prototype.flipUp = function () {
        this.cy = -Math.abs(this.cy);
        this.makeDString();
    };
    SvgArrow.prototype.flipDown = function () {
        this.cy = Math.abs(this.cy);
        this.makeDString();
    };
    SvgArrow.prototype.setCurveH = function (cy) {
        this.cy = cy;
        this.makeDString();
    };
    SvgArrow.prototype.setLen = function (x2) {
        this.endX = x2;
        this.len = this.endX - this.startX;
        this.makeDString();
    };
    SvgArrow.prototype.setH = function (y2) {
        this.endY = y2;
        this.h = this.endY - this.startY;
        this.makeDString();
    };
    //set the marker at the start of the line
    SvgArrow.prototype.setStart = function (classString) {
        if (this.lineElement) {
            this.lineElement.style.display = "block";
            this.startClass = classString;
            if (classString === "none") {
                this.lineElement.setAttribute("marker-start", "");
            }
            else {
                var fullClass = classString + this.fill;
                this.lineElement.setAttribute("marker-start", "url(#" + fullClass + ")");
            }
        }
    };
    //set the marker at the end of the line
    SvgArrow.prototype.setEnd = function (classString) {
        if (this.lineElement) {
            this.endClass = classString;
            if (classString === "none") {
                this.lineElement.setAttribute("marker-end", "");
            }
            else {
                var fullClass = classString + this.fill;
                this.lineElement.setAttribute("marker-end", "url(#" + fullClass + ")");
            }
        }
    };
    //redraw the ending point of the line
    SvgArrow.prototype.resetEndLine = function (endX, endY) {
        this.endX = endX;
        this.endY = endY;
        this.makeDString();
    };
    //set the mask
    SvgArrow.prototype.showMask = function () {
        if (this.lineElement && this.maskArea) {
            this.lineElement.setAttribute("mask", "url(#" + this.maskArea.id + ")");
        }
    };
    //remove the mask
    SvgArrow.prototype.hideMask = function () {
        if (this.lineElement) {
            this.lineElement.removeAttribute("mask");
        }
    };
    //create a dot with and x and y on a line point
    //add that dot to the mask
    SvgArrow.prototype.addDotAt = function (per) {
        if (this.lineElement && this.maskArea) {
            var totalLen = this.lineElement.getTotalLength();
            var per2 = (per / 100) * totalLen;
            var p = this.lineElement.getPointAtLength(per2);
            /*  let xx2: number = p.x;
             let yy2: number = p.y; */
            var svgns = "http://www.w3.org/2000/svg";
            var dotW = this.strokeWidth * 4;
            var dot = document.createElementNS(svgns, "circle");
            dot.setAttribute("cx", p.x.toString());
            dot.setAttribute("cy", p.y.toString());
            dot.setAttribute("r", dotW.toString());
            dot.setAttribute("fill", "white");
            this.maskArea.append(dot);
        }
    };
    //add the arrow head to the line
    SvgArrow.prototype.addProgress = function () {
        if (this.tp) {
            this.tp.remove();
        }
        var text1 = this.defs.getElementsByTagName("text")[0];
        text1 = text1.cloneNode(true);
        text1.id = this.elID + "text";
        text1.setAttribute("fill", this.textPointerFill);
        if (this.el) {
            this.tp = text1.getElementsByTagName("textPath")[0];
            this.tp.innerHTML = this.textPointerHtml;
            this.tp.style.fontSize = this.textPointerSize;
            this.tp.setAttribute("visibility", "visible");
            this.tp.setAttribute("href", "#" + this.elID + "line");
            this.el.appendChild(text1);
            window.tp = this.tp;
        }
    };
    //move the arrow head to a position on the line
    SvgArrow.prototype.setProgress = function (per) {
        if (!this.tp) {
            this.addProgress();
        }
        if (this.tp) {
            this.tp.setAttribute("visibility", "visible");
            this.tp.setAttribute("startOffset", per.toString() + "%");
        }
    };
    //start the arrow moving
    SvgArrow.prototype.startProgress = function (speed, inc) {
        var _this = this;
        if (inc === void 0) { inc = 0.5; }
        if (this.maskArea) {
            while (this.maskArea.firstChild) {
                this.maskArea.removeChild(this.maskArea.firstChild);
            }
        }
        this.showMask();
        this.drawLineSpeed = speed;
        this.drawLineInc = inc;
        this.drawLineStep = 0;
        if (!this.tp) {
            this.addProgress();
        }
        this.setProgress(0);
        setTimeout(function () {
            _this.advanceProgress();
        }, this.drawLineSpeed);
    };
    //advance the positon of were the line is drawn
    SvgArrow.prototype.advanceProgress = function () {
        var _this = this;
        this.drawLineStep += this.drawLineInc;
        this.addDotAt(this.drawLineStep);
        this.setProgress(this.drawLineStep);
        if (this.drawLineStep < 100) {
            setTimeout(function () {
                _this.advanceProgress();
            }, this.drawLineSpeed);
        }
        else {
            if (this.tp) {
                this.tp.setAttribute("visibility", "hidden");
            }
            this.hideMask();
        }
    };
    //place a label on the line
    SvgArrow.prototype.setMessage = function (msg, per) {
        if (per === void 0) { per = 0; }
        this.message = msg;
        if (this.messagePath) {
            this.messagePath.remove();
            this.messagePath = null;
        }
        if (msg === "") {
            return;
        }
        var text2 = this.defs.getElementsByTagName("text")[1];
        text2 = text2.cloneNode(true);
        text2.id = this.elID + "message";
        if (text2 && this.el) {
            this.messagePath = text2.getElementsByTagName("textPath")[0];
            this.messagePath.setAttribute("href", "#" + this.elID + "line");
            this.messagePath.innerHTML = msg;
            this.messagePath.setAttribute("fill", this.messageColor);
            //  this.messagePath.setAttribute("stroke",this.messageStroke);
            this.messagePath.setAttribute("strokeWidth", "2px");
            this.messagePath.setAttribute("startOffset", per.toString() + "%");
            this.el.appendChild(text2);
        }
    };
    //set the message position
    SvgArrow.prototype.setMessagePos = function (per) {
        if (per === void 0) { per = 0; }
        this.messagePos = per;
        if (this.messagePath) {
            this.messagePath.setAttribute("startOffset", per.toString() + "%");
        }
    };
    SvgArrow.prototype.setMessageSize = function (size) {
        this.messageSize = size;
        if (this.messagePath) {
            this.messagePath.style.fontSize = size;
        }
    };
    //draw the path inside the line element
    SvgArrow.prototype.makeDString = function () {
        var startX = this.startX;
        var startY = this.startY;
        var cx = this.cx;
        var cy = this.cy;
        var endX = this.endX;
        var endY = this.endY;
        var strokeWidth = this.strokeWidth;
        // //console.log("%="+this.usePercentages);
        if (this.usePercentages === true && this.container) {
            var nArrowPosX = (this.arrowPositionX / 100) * this.containerW;
            var nArrowPosY = (this.arrowPositionY / 100) * this.containerH;
            this.x = nArrowPosX;
            this.y = nArrowPosY;
            startX = (startX / 100) * this.containerW;
            startY = (startY / 100) * this.containerH;
            //
            //
            cx = (cx / 100) * this.containerW;
            cy = (cy / 100) * this.containerH;
            //
            //
            endX = (endX / 100) * this.containerW;
            endY = (endY / 100) * this.containerH;
            strokeWidth = (strokeWidth / 100) * this.containerW;
            if (this.debug === true) {
                console.log("change " + this.arrowPositionX + "% to " + nArrowPosX + "px");
                console.log("change " + this.arrowPositionY + "% to " + nArrowPosY + "px");
                console.log("change endX " + this.endX + "% to " + endX + "px");
                console.log("containerW " + this.containerW);
                console.log("change endY " + this.endY + "% to " + endY + "px");
                console.log("containerH " + this.containerH);
                console.log("change strokeWidth " + this.strokeWidth + "% to " + strokeWidth + "px");
                //  //console.log("startX", startX, "startY", startY, "cx", cx, "cy", cy, "endX", endX, "endY", endY);
            }
        }
        //make the path string for the svg element
        var dstring = "M" + startX.toString() + "," + startY.toString() + " Q" + cx.toString() + "," + cy.toString() + " " + endX.toString() + "," + endY.toString();
        if (this.debug === true) {
            console.log("dstring", dstring);
        }
        if (this.lineElement) {
            this.lineElement.setAttribute("stroke-width", strokeWidth.toString() + "px");
            this.lineElement.setAttribute("d", dstring);
        }
        this.setMessage(this.message, this.messagePos);
        this.setMessageSize(this.messageSize);
    };
    //get the data for export
    SvgArrow.prototype.exportData = function () {
        var arrowVo = new ArrowVo(this.x, this.y, this.startX, this.startY, this.endX, this.endY, this.cx, this.cy, this.fill, this.startClass, this.endClass);
        arrowVo.message = this.message;
        arrowVo.messagePos = this.messagePos;
        arrowVo.messageSize = this.messageSize;
        arrowVo.messageColor = this.messageColor;
        arrowVo.textPointerHtml = this.textPointerHtml;
        arrowVo.textPointerSize = this.textPointerSize;
        arrowVo.textPointerFill = this.textPointerFill;
        arrowVo.strokeWidth = this.strokeWidth;
        arrowVo.animateSpeed = this.animateSpeed;
        return JSON.stringify(arrowVo);
    };
    //convert pixel to percentage and return an arrowVo object
    SvgArrow.prototype.exportPercentData = function () {
        var arrowPosPercentX = 0;
        var arrowPosPercentY = 0;
        var startX = this.startX;
        var startY = this.startY;
        var cx = this.cx;
        var cy = this.cy;
        var endX = this.endX;
        var endY = this.endY;
        var strokeWidth = this.strokeWidth;
        if (this.container) {
            arrowPosPercentX = (this.x / this.containerW) * 100;
            arrowPosPercentY = (this.y / this.containerH) * 100;
            startX = this.trimNumber((startX / this.containerW) * 100);
            startY = this.trimNumber((startY / this.containerH) * 100);
            //
            //
            cx = this.trimNumber((cx / this.containerW) * 100);
            cy = this.trimNumber((cy / this.containerH) * 100);
            //
            //
            endX = this.trimNumber((endX / this.containerW) * 100);
            endY = this.trimNumber((endY / this.containerH) * 100);
            strokeWidth = this.trimNumber((strokeWidth / this.containerW) * 100);
        }
        var arrowVo = new ArrowVo(arrowPosPercentX, arrowPosPercentY, startX, startY, endX, endY, cx, cy, this.fill, this.startClass, this.endClass);
        arrowVo.message = this.message;
        arrowVo.messagePos = this.messagePos;
        arrowVo.animateSpeed = this.animateSpeed;
        arrowVo.strokeWidth = strokeWidth;
        return JSON.stringify(arrowVo);
    };
    SvgArrow.prototype.fromString = function (objString) {
        var obj = JSON.parse(objString);
        this.fromObj(obj);
    };
    SvgArrow.prototype.fromObj = function (obj) {
        var arrowVo = new ArrowVo();
        arrowVo.fromObj(obj);
        this.startX = arrowVo.startX;
        this.startY = arrowVo.startY;
        this.endX = arrowVo.endX;
        this.endY = arrowVo.endY;
        this.cx = arrowVo.cx;
        this.cy = arrowVo.cy;
        this.fill = arrowVo.fill;
        this.setCurve(this.cx, this.cy);
        //  this.x = arrowVo.x;
        //  this.y = arrowVo.y;
        this.arrowPositionX = arrowVo.x;
        this.arrowPositionY = arrowVo.y;
        if (this.usePercentages === false) {
            this.x = arrowVo.x;
            this.y = arrowVo.y;
        }
        this.messageSize = arrowVo.messageSize;
        this.message = arrowVo.message;
        this.messagePos = arrowVo.messagePos;
        this.messageColor = arrowVo.messageColor;
        this.textPointerHtml = arrowVo.textPointerHtml;
        this.textPointerFill = arrowVo.textPointerFill;
        this.textPointerSize = arrowVo.textPointerSize;
        this.startClass = arrowVo.startMark;
        this.endClass = arrowVo.endMark;
        this.animateSpeed = arrowVo.animateSpeed;
        this.strokeWidth = arrowVo.strokeWidth;
        this.makeDString();
        this.setFill(this.fill);
        this.setMessage(this.message, this.messagePos);
        this.setMessageSize(this.messageSize);
    };
    SvgArrow.prototype.trimNumber = function (num) {
        return Math.round(num * 1000) / 1000;
    };
    SvgArrow.prototype.setDebug = function (debug) {
        this.debug = debug;
    };
    return SvgArrow;
}(SvgObj));
export { SvgArrow };
//# sourceMappingURL=SVGArrow.js.map