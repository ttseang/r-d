import { IGameObj } from "./IGameObj";
export declare class TestStage {
    private stageEl;
    constructor(stageID: string);
    addObj(gameObj: IGameObj): void;
}
