var LinkUtil = /** @class */ (function () {
    function LinkUtil() {
    }
    LinkUtil.makeLink = function (buttonID, callback, action, params) {
        var v = document.getElementById(buttonID);
        if (v) {
            v.addEventListener("click", function () {
                //console.log("click");
                callback(action, params);
            });
            //console.log(v);
        }
    };
    return LinkUtil;
}());
export { LinkUtil };
//# sourceMappingURL=LinkUtil.js.map