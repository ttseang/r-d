var ArrowUtil = /** @class */ (function () {
    function ArrowUtil() {
    }
    ArrowUtil.lerp = function (start, end, t) {
        return (1 - t) * start + t * end;
    };
    ArrowUtil.getRotate = function (cx, cy, ex, ey, angle) {
        var radians = (Math.PI / 180) * angle;
        var cos = Math.cos(radians);
        var sin = Math.sin(radians);
        var x = (cos * (ex - cx)) - (sin * (ey - cy)) + cx;
        var y = (cos * (ey - cy)) + (sin * (ex - cx)) + cy;
        return { rx: x, ry: y };
    };
    ;
    ArrowUtil.getAngle = function (x1, y1, x2, y2) {
        var dx = x2 - x1;
        var dy = y2 - y1;
        var angle = Math.atan2(dy, dx) * 180 / Math.PI;
        return angle;
    };
    ArrowUtil.makeArrowArea = function (id, parent) {
        if (!document.getElementById(id)) {
            var parentElement = document.getElementById(parent);
            if (parentElement) {
                var element = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                element.id = id;
                element.classList.add("svgFull");
                parentElement.appendChild(element);
            }
            else {
                console.warn("missing parent " + parent);
            }
        }
        // <svg id="addArea" x="0" y="0" class="full">
        // </svg>
    };
    ArrowUtil.makeCss = function () {
        if (!document.getElementById("arrowStyle")) {
            var styleEl = document.createElement("style");
            styleEl.id = "arrowStyle";
            styleEl.innerHTML = ".svgArrow{stroke-width:2px}.svgFull{width:100%;height:100%}.svgArrow .pointerArrowRed{stroke:red}.svgArrow .pointerArrowBlue{stroke:blue!important}.svgArrow .pointerArrowBlack{stroke:black!important}.svgArrow .pointerArrowYellow{stroke:yellow!important}.svgArrow .pointerArrowGreen{stroke:green!important}";
            document.head.appendChild(styleEl);
        }
    };
    ArrowUtil.makeDefs = function () {
        if (!document.getElementById("svgArrowDefs")) {
            var element = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            element.id = "svgArrowDefs";
            element.style.display = "block";
            element.style.backgroundColor = "black";
            element.style.width = "0px";
            element.style.height = "0px";
            //  element.setAttribute("visibility", "hidden");
            // element.setAttributeNS(null, "viewBox", "0 0 10 10");
            //    element.setAttributeNS(null, "preserveAspectRatio", "xMinYMin meet");
            var svgStarPath = "M 50 0 L 60 35 L 100 35 L 70 60 L 80 100 L 50 75 L 20 100 L 30 60 L 0 35 L 40 35 Z";
            element.innerHTML = " <defs id='arrowDefs'>\n            <style type=\"text/css\">\n                .hints path {\n                    fill: none;\n                    stroke: hsl(200, 0%, 60%);\n                    stroke-width: 10;\n                    stroke-linecap: round;\n                    stroke-linejoin: round;\n                    marker-end: url(#arrowhead);\n                }\n\n                .hints use,\n                #arrowhead {\n                    fill: hsl(200, 0%, 60%);\n                    font-family: 'Trebuchet MS';\n                    font-size: 80px;\n                }\n            </style>\n            <path class=\"link\" id=\"path1\" d=\"M0 0 L200 400A300 300 0 0 1 490 150\"></path>\n            <marker id=\"arrowpointblack\" fill=\"black\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\"\n                markerHeight=\"6\" orient=\"auto-start-reverse\">\n                <path d=\"M 0 0 L 10 5 L 0 10 z\" />\n            </marker>\n            <marker id=\"arrowpointred\" fill=\"red\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\" markerHeight=\"6\"\n                orient=\"auto-start-reverse\">\n                <path d=\"M 0 0 L 10 5 L 0 10 z\" />\n            </marker>\n            <marker id=\"arrowpointblue\" fill=\"blue\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\"\n                markerHeight=\"6\" orient=\"auto-start-reverse\">\n                <path d=\"M 0 0 L 10 5 L 0 10 z\" />\n            </marker>\n            <marker id=\"arrowpointyellow\" fill=\"yellow\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\" markerHeight=\"6\"\n            orient=\"auto-start-reverse\">\n            <path d=\"M 0 0 L 10 5 L 0 10 z\" />\n        </marker>\n        <marker id=\"arrowpointgreen\" fill=\"green\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\" markerHeight=\"6\"\n            orient=\"auto-start-reverse\">\n            <path d=\"M 0 0 L 10 5 L 0 10 z\" />\n        </marker>\n\n        <marker id=\"arrowdotblack\" fill=\"black\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\"\n                markerHeight=\"6\" orient=\"auto-start-reverse\">\n                <circle cx=\"5\" cy=\"5\" r=\"5\" fill=\"black\" />\n        </marker>\n\n        <marker id=\"arrowdotred\" fill=\"red\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\"\n                markerHeight=\"6\" orient=\"auto-start-reverse\">\n                <circle cx=\"5\" cy=\"5\" r=\"5\" fill=\"red\" />\n        </marker>\n\n        <marker id=\"arrowdotgreen\" fill=\"green\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\"\n                markerHeight=\"6\" orient=\"auto-start-reverse\">\n                <circle cx=\"5\" cy=\"5\" r=\"5\" fill=\"green\" />\n        </marker>\n\n        <marker id=\"arrowdotyellow\" fill=\"yellow\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\"\n                markerHeight=\"6\" orient=\"auto-start-reverse\">\n                <circle cx=\"5\" cy=\"5\" r=\"5\" fill=\"yellow\" />\n        </marker>\n        <marker id=\"arrowdotblue\" fill=\"blue\" viewBox=\"0 0 10 10\" refX=\"5\" refY=\"5\" markerWidth=\"6\"\n                markerHeight=\"6\" orient=\"auto-start-reverse\">\n                <circle cx=\"5\" cy=\"5\" r=\"5\" fill=\"blue\" />\n        </marker>\n        \n                \n        <marker id=\"arrowstarblack\" fill=\"black\" viewBox=\"0 0 100 100\" refX=\"50\" refY=\"50\" markerWidth=\"6\"\n        markerHeight=\"6\" orient=\"auto-start-reverse\">\n        <path d=\"".concat(svgStarPath, "\" fill=\"black\" />                \n        </marker>\n        <marker id=\"arrowstarred\" fill=\"red\" viewBox=\"0 0 100 100\" refX=\"50\" refY=\"50\" markerWidth=\"6\"\n        markerHeight=\"6\" orient=\"auto-start-reverse\">\n        <path d=\"").concat(svgStarPath, "\" fill=\"red\" />\n        </marker>\n        <marker id=\"arrowstargreen\" fill=\"green\" viewBox=\"0 0 100 100\" refX=\"50\" refY=\"50\" markerWidth=\"6\"\n        markerHeight=\"6\" orient=\"auto-start-reverse\">\n        <path d=\"").concat(svgStarPath, "\" fill=\"green\" />\n        </marker>\n        <marker id=\"arrowstaryellow\" fill=\"yellow\" viewBox=\"0 0 100 100\" refX=\"50\" refY=\"50\" markerWidth=\"6\"\n        markerHeight=\"6\" orient=\"auto-start-reverse\">\n        <path d=\"").concat(svgStarPath, "\" fill=\"yellow\" />\n        </marker>\n        <marker id=\"arrowstarblue\" fill=\"blue\" viewBox=\"0 0 100 100\" refX=\"50\" refY=\"50\" markerWidth=\"6\"\n        markerHeight=\"6\" orient=\"auto-start-reverse\">\n        <path d=\"").concat(svgStarPath, "\" fill=\"blue\" />\n        </marker>\n         <marker id=\"arrowhead\" markerWidth=\"3\" markerHeight=\"5\" orient=\"auto\" refY=\"2.5\">\n                <polygon points=\"0 0, 3 2.5, 0 5\" />\n            </marker>\n            <marker id = \"mm\" viewBox = \"0 0 10 10\" refX = \"5\" refY = \"5\" markerUnits = \"strokeWidth\" markerWidth = \"3\" markerHeight = \"3\" stroke = \"lightblue\" stroke-width = \"2\" fill = \"none\" orient = \"auto\">\n                <path d = \"M 0 0 L 10 10 M 0 10 L 10 0\"/>\n            </marker>\n            \n            <g id=\"arrowtemp2\">\n                <path id=\"arrowpath\" class=\"hints black\" d=\"M100,200 Q250,100 400,200\" stroke-width=\"3px\"\n                    fill=\"transparent\" />\n                    <mask id=\"maskArea\">                        \n                    </mask>\n            </g>\n            <text id=\"tp\">\n                <textPath text-anchor=\"middle\" href=\"#arrowpath\" dominant-baseline=\"central\">&#x25b6;<animate id=\"arrowmove\"attributeName=\"startOffset\" begin=\"indefinite\" from=\"0%\" to=\"100%\" dur=\"10s\"></animate>\n                </textPath>\n                \n              </text>\n              <text id=\"textMessage\">\n                <textPath href=\"#arrowpath\" class=\"lineMessage\">Your message here</textPath>\n              </text>\n        </defs>");
            document.body.appendChild(element);
        }
    };
    return ArrowUtil;
}());
export { ArrowUtil };
//# sourceMappingURL=ArrowUtil.js.map