export declare class LinkUtil {
    constructor();
    static makeLink(buttonID: string, callback: Function, action: string, params: string): void;
}
