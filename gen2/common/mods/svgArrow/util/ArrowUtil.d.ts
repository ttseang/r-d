export declare class ArrowUtil {
    constructor();
    static lerp(start: number, end: number, t: number): number;
    static getRotate(cx: number, cy: number, ex: number, ey: number, angle: number): {
        rx: number;
        ry: number;
    };
    static getAngle(x1: number, y1: number, x2: number, y2: number): number;
    static makeArrowArea(id: string, parent: string): void;
    static makeCss(): void;
    static makeDefs(): void;
}
