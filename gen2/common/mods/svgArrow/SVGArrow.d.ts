import { SvgObj } from "./SvgObj";
export declare class SvgArrow extends SvgObj {
    arrowPositionX: number;
    arrowPositionY: number;
    startX: number;
    startY: number;
    endX: number;
    endY: number;
    cx: number;
    cy: number;
    animateSpeed: number;
    private tp;
    private messagePath;
    textPointerSize: string;
    textPointerFill: string;
    textPointerHtml: string;
    len: number;
    h: number;
    fill: string;
    lastFill: string;
    strokeWidth: number;
    startClass: string;
    endClass: string;
    endIndex: number;
    colorIndex: number;
    lineElement: SVGGeometryElement | null;
    maskArea: SVGMaskElement | null;
    /**Message Atrributes */
    message: string;
    messagePos: number;
    messageColor: string;
    messageStroke: string;
    messageSize: string;
    private colorClasses;
    private defs;
    curvePosition: string;
    private drawLineStep;
    private drawLineSpeed;
    private drawLineInc;
    private container;
    private containerW;
    private containerH;
    usePercentages: boolean;
    debug: boolean;
    /**
     * Creates an instance of SvgArrow.
     * @param {string} templateID - this is the id of the template in the svg code currently arrowtemp2
     * @param {(SVGSVGElement | null)} [container=null]
     * @memberof SvgArrow
     */
    constructor(templateID: string, container?: SVGSVGElement | null);
    remove(): void;
    resetContainer(container: SVGSVGElement): void;
    onResize(): void;
    setResizeListener(): void;
    private capFirst;
    setFill(color: string): void;
    addStyle(style: string): void;
    removeStyle(style: string): void;
    setStrokeWidth(w: number): void;
    setCurve(cx: number, cy: number): void;
    flipCurveY(): void;
    flipUp(): void;
    flipDown(): void;
    setCurveH(cy: number): void;
    setLen(x2: number): void;
    setH(y2: number): void;
    setStart(classString: string): void;
    setEnd(classString: string): void;
    resetEndLine(endX: number, endY: number): void;
    showMask(): void;
    hideMask(): void;
    addDotAt(per: number): void;
    addProgress(): void;
    setProgress(per: number): void;
    startProgress(speed: number, inc?: number): void;
    advanceProgress(): void;
    setMessage(msg: string, per?: number): void;
    setMessagePos(per?: number): void;
    setMessageSize(size: string): void;
    makeDString(): void;
    exportData(): string;
    exportPercentData(): string;
    fromString(objString: string): void;
    fromObj(obj: any): void;
    private trimNumber;
    setDebug(debug: boolean): void;
}
