import { BaseScreen } from "../classes/core/BaseScreen"
import { SvgObj } from "../classes/core/gameobjects/SvgObj";

export class ScreenMain extends BaseScreen
{
    constructor()
    {
        super("ScreenMain");
    }
    create()
    {
        let testButton:SvgObj=new SvgObj(this,"testButton");

        this.center(testButton,true);
        
    }
}