export class TweenObj {
    public x: number | null = null
    public y: number | null = null;
    public alpha: number | null = null;
    public angle:number | null=null;
    public displayWidth:number|null=null;
    public displayHeight:number|null=null;
    public scaleX:number|null=null;
    public scaleY:number|null=null;
    public skewX:number|null=null;
    public skewY:number|null=null;
    
    public duration:number=1000;
    
    public onComplete:Function=()=>{};

    constructor() {

    }
}