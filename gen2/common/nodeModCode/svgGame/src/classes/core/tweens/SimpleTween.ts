import { IGameObj } from "../interfaces/IGameObj";
import { TweenObj } from "./TweenObj";

export class SimpleTween {
    private gameObj: IGameObj;
    private tweenObj: TweenObj;
    private autoStart: boolean;

    /**
     * Targets
     */
    private xTarget: number;
    private yTarget: number;
    private alphaTarget: number;
    private angleTarget:number;
    private htarget:number;
    private wtarget:number;
    private scaleXTarget:number;
    private scaleYTarget:number;
    private skewXTarget:number;
    private skewYTarget:number;

    /**
     * Increments
     *  */ 
    private xInc: number = 0;
    private yInc: number = 0;
    private alphaInc: number = 0;
    private angleInc:number=0;
    private widthInc:number=0;
    private heightInc:number=0;
    private scaleXInc:number=0;
    private scaleYInc:number=0;
    private skewXInc:number=0;
    private skewYInc:number=0;
    /**
     * Timer
     */
    private myTimer: any = null;
    private myTime: number = 0;

    constructor(gameObj: IGameObj, tweenObj: TweenObj, autoStart: boolean = true) {
        this.gameObj = gameObj;
        this.tweenObj = tweenObj;
        this.autoStart = autoStart;
        //
        //
        this.xTarget = gameObj.x;
        this.yTarget = gameObj.y;
        this.alphaTarget = gameObj.alpha;
        this.angleTarget=gameObj.angle;
        this.htarget=gameObj.displayHeight;
        this.wtarget=gameObj.displayWidth;
        this.scaleXTarget=gameObj.scaleX;
        this.scaleYTarget=gameObj.scaleY;
        this.skewXTarget=gameObj.skewX;
        this.skewYTarget=gameObj.skewY;

        if (tweenObj.x != null) {
            this.xTarget = tweenObj.x;
        }
        if (tweenObj.y != null) {
            this.yTarget = tweenObj.y;
        }
        if (tweenObj.alpha != null) {
            this.alphaTarget = tweenObj.alpha;
        }
        if (tweenObj.angle!=null)
        {
            this.angleTarget=tweenObj.angle;
        }
        if (tweenObj.displayWidth!=null)
        {
            this.wtarget=tweenObj.displayWidth;
        }
        if (tweenObj.displayHeight!=null)
        {
            this.htarget=tweenObj.displayHeight;
        }
        if (tweenObj.scaleX!=null)
        {
            this.scaleXTarget=tweenObj.scaleX;
        }
        if (tweenObj.scaleY!=null)
        {
            this.scaleYTarget=tweenObj.scaleY;
        }
        if (tweenObj.skewX!=null)
        {
            this.skewXTarget=tweenObj.skewX;
        }
        if (tweenObj.skewY!=null)
        {
            this.skewYTarget=tweenObj.skewY;
        }
        let duration2: number = tweenObj.duration / 5;
        this.myTime = tweenObj.duration;
        //
        //
        this.xInc = (this.xTarget - gameObj.x) / duration2;
        this.yInc = (this.yTarget - gameObj.y) / duration2;
        //
        //
        this.alphaInc = (this.alphaTarget - gameObj.alpha) / duration2;
        this.angleInc=(this.angleTarget-gameObj.angle)/duration2;
        //
        //
        this.heightInc=(this.htarget-gameObj.displayHeight)/duration2;
        this.widthInc=(this.wtarget-gameObj.displayWidth)/duration2;
        //
        //
        this.scaleXInc=(this.scaleXTarget-gameObj.scaleX)/duration2;
        this.scaleYInc=(this.scaleYTarget-gameObj.scaleY)/duration2;
        //
        //
        this.skewXInc=(this.skewXTarget-gameObj.skewX)/duration2;
        this.skewYInc=(this.skewYTarget-gameObj.skewY)/duration2;

        if (autoStart == true) {
            this.start();
        }
    }
    public start() {
        this.myTimer = setInterval(this.doStep.bind(this), 5);
    }
    private doStep() {
        this.myTime -= 5;

        if (this.myTime < 0) {
            this.gameObj.x = this.xTarget;
            this.gameObj.y = this.yTarget;
            this.gameObj.alpha = this.alphaTarget;
            this.gameObj.angle=this.angleTarget;

            clearInterval(this.myTimer);
            this.tweenObj.onComplete(this.gameObj);
            return;
        }
        this.gameObj.x += this.xInc;
        this.gameObj.y += this.yInc;
        this.gameObj.alpha += this.alphaInc;
        this.gameObj.angle+=this.angleInc;
        this.gameObj.displayHeight+=this.heightInc;
        this.gameObj.displayWidth+=this.widthInc;
        this.gameObj.scaleX+=this.scaleXInc;
        this.gameObj.scaleY+=this.scaleYInc;
        this.gameObj.skewX+=this.skewXInc;
        this.gameObj.skewY+=this.skewYInc;
        
    }
}