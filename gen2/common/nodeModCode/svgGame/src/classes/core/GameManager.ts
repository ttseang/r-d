import { IGameObj, SVGUtil } from "../..";
import { FontSizeVo } from "./dataobjs/fontSizeVo";
import { GameOptions } from "./dataobjs/gameOptions";
import { ScreenManager } from "./ScreenManager";

export class GameManager {
    private static instance: GameManager | null;
    public gameOptions: GameOptions = new GameOptions();
    public gameID: string = "";
    public gameEl: HTMLElement | null = null;
    public gw: number = 0;
    public gh: number = 0;
    private objMap: Map<string, IGameObj> = new Map<string, IGameObj>();
    
    private defFontSize:FontSizeVo=new FontSizeVo(70,1000);
    public fontSizes:Map<string,FontSizeVo>=new Map<string,FontSizeVo>();

    constructor() {
        (window as any).gm = this;
        window.onresize = this.doResize.bind(this);
    }
    public static getInstance(): GameManager {
        if (this.instance == null) {
            this.instance = new GameManager();
        }
        return this.instance;
    }
    regObj(id: string, obj: IGameObj) {
        //console.log("register " + id);

        this.objMap.set(id, obj);
    }
    unRegObj(id: string) {
        this.objMap.delete(id);
    }
    getObj(id: string) {
        if (this.objMap.has(id)) {
            return this.objMap.get(id);
        }
        return null;
    }
    public getFontSize(sizeKey:string,canvasWidth:number) {

        let fontSizeVo:FontSizeVo=this.defFontSize;

        if (this.fontSizes.has(sizeKey))
        {
            fontSizeVo=this.fontSizes.get(sizeKey);
        }

        let fontSize:number=fontSizeVo.defFontSize;
        let fontBase:number=fontSizeVo.canvasBase;

        var ratio = fontSize / fontBase;   // calc ratio
        var size = canvasWidth * ratio;   // get font size based on current width
        return (size|0);
    }
    public regFontSize(key:string,defFontSize:number,canvasBase:number)
    {
        this.fontSizes.set(key,new FontSizeVo(defFontSize,canvasBase));
    }
    private doResize() {

        if (this.gameOptions.useFull == true) {

            if (this.gameEl) {
                let b:DOMRect=document.body.getBoundingClientRect();

               // let b: any = (this.gameEl as any).getBBox();
                this.gw = b.width;
                this.gh = b.height;
            }
        }

        ScreenManager.getInstance().doResize();
    }
}