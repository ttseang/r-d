import { AlignGrid } from "../../util/AlignGridSvg";
import { GameManager } from "./GameManager";
import { SoundObj } from "./gameobjects/SoundObj";
import { SvgObj } from "./gameobjects/SvgObj";
import { TextObj } from "./gameobjects/TextObj";
import { IGameObj } from "./interfaces/IGameObj";
import { IScreen } from "./interfaces/IScreen";
import { ScreenManager } from "./ScreenManager";
import { SimpleTween } from "./tweens/SimpleTween";
import { TweenObj } from "./tweens/TweenObj";

export class BaseScreen implements IScreen {
    public key: string;
    protected gm: GameManager = GameManager.getInstance();
    private screenManager: ScreenManager = ScreenManager.getInstance();

    private children: IGameObj[] = [];

    public grid: AlignGrid | null = null;

    constructor(key: string) {
        this.key = key;

    }
    public start() {
        this.grid = new AlignGrid(11, 11, this.gm.gw, this.gm.gh);
        this.create();
    }
    public create() {

    }
    public destroy() {
        while (this.children.length > 0) {
            this.children.pop()?.destroy();
        }
    }
    public addExisting(obj: IGameObj) {
        this.children.push(obj);
    }
    public addImage(key: string) {
        let svgObj: SvgObj = new SvgObj(this, key);
        this.addExisting(svgObj);
        return svgObj;
    }
    public addImageAt(key: string, x: number, y: number) {
        let svgObj: SvgObj = this.addImage(key);
        svgObj.x = x;
        svgObj.y = y;
        return svgObj;
    }
    public addText(text: string, textKey: string = "defaultText") {
        let textObj: TextObj = new TextObj(this, textKey);
        textObj.setText(text);
        this.addExisting(textObj);
        return textObj;
    }
    public addTextAt(text: string, x: number, y: number, textKey: string = "defaultText") {
        let textObj: TextObj = this.addText(textKey, text);
        textObj.x = x;
        textObj.y = y;
        return textObj;
    }
    public centerW(obj: IGameObj, adjust: boolean = false) {
        obj.x = this.gw / 2;
        if (adjust == true) {
            obj.x -= obj.displayWidth / 2;
        }
    }
    public centerH(obj: IGameObj, adjust: boolean = false) {
        obj.y = this.gh / 2;
        if (adjust) {
            obj.y -= obj.displayHeight / 2;
        }
    }
    public placeOnGrid(col: number, row: number, obj: IGameObj, adjust: boolean = false) {
        let xx: number = this.grid.cw * col;
        let yy: number = this.grid.ch * row;

        obj.x = xx;
        obj.y = yy;

        if (adjust) {
            obj.y += this.grid!.ch / 2 - obj.displayHeight / 2;
            obj.x += this.grid!.cw / 2 - obj.displayWidth / 2;
        }
    }
    public tweenGridPos(col: number, row: number, obj: IGameObj, duration: number, callback: Function = () => { }) {
        let tweenObj: TweenObj = new TweenObj();
        let xx: number = this.grid.cw * col;
        let yy: number = this.grid.ch * row;

        tweenObj.x = xx;
        tweenObj.y = yy;
        tweenObj.onComplete = callback;
        tweenObj.duration = duration;

        let tween: SimpleTween = new SimpleTween(obj, tweenObj);

    }
    public center(obj: IGameObj, adjust: boolean = false): void {
        this.centerW(obj, adjust);
        this.centerH(obj, adjust);
    }
    /**
     * game height;
     */
    public get gh(): number {
        return this.gm.gh;
    }
    /**
     * game width
     */
    public get gw(): number {
        return this.gm.gw;
    }
    public changeScreen(nScreen: string): void {
        //  console.log("change to "+nScreen);
        this.screenManager.changeScreen(nScreen);
    }
    public doResize() {
        //can override in screen
        /*   this.grid.hide();
          this.grid.resetSize(); */

        this.grid = new AlignGrid(11, 11, this.gm.gw, this.gm.gh);
        for (let i: number = 0; i < this.children.length; i++) {
            this.children[i].doResize();
        }
    }
    public playSound(path:string)
    {
        let s:SoundObj=new SoundObj(path);
        s.play();
    }
}