import { StyleVo } from "../dataObjs/StyleVo";

export class HtmlObj {
    public el: HTMLElement | null;

    private _x: number=0;
    private _y: number=0;
    private _visible: boolean=false;
    public id:string;

    constructor(id: string) {
        this.el = document.getElementById(id);
        this.id=id;
       // this.init();
    }
    private init()
    {
        if (this.el)
        {
            this.x=parseFloat(this.el.style.left);
            this.y=parseFloat(this.el.style.top);
        }
        
    }
    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
        if (this.el) {
            this.el.style.left = value.toString() + "%";
        }
    }
    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
        if (this.el) {
            this.el.style.top = value.toString() + "%";
        }
    }
    public get visible(): boolean {
        return this._visible;
    }
    public set visible(value: boolean) {
        this._visible = value;

        if (this.el) {
            if (this.visible == true) {
                this.el.style.display = "block";
            }
            else {
                this.el.style.display = "none";
            }
        }

    }
    public flyTo(xx: number, yy: number) {
        if (this.el) {

          //  //console.log(this.el);
            this.el.style.transition = "top 2s,left 2s,width 2s";

            this.x = xx;
            this.y = yy;

        }
    }
    getStyle(className:string) {
        
        let cssText:string="";
        let myRule:StyleVo=new StyleVo("red","3vh","normal");

        //console.log("looking for "+className);

        var rules:CSSRuleList = document.styleSheets[0].cssRules;
        for (var i = 0; i < rules.length; i++) {       
             let rule:CSSRule | null=rules.item(i);
            if (rule)
            {
             let ruleName:string=(rule as CSSStyleRule).selectorText;
            //console.log("looking at "+ruleName);
            if (ruleName==className)
             {
                //console.log(rule);
                cssText=rule.cssText;
                let rule2:CSSStyleRule=rule as CSSStyleRule;
                
                if (rule2.style.color)
                {
                    myRule.color=rule2.style.color;
                }
                if (rule2.style.fontSize)
                {
                    myRule.fontSize=rule2.style.fontSize;
                }
                if (rule2.style.fontWeight)
                {
                    myRule.fontWeight=rule2.style.fontWeight;
                }

             //   myRule=(rule as CSSStyleRule).style;
             } 
            }
        }
        return myRule;
    }
}