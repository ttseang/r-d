import { IScreen } from "../interfaces/IScreen";
import { SvgObj } from "./SvgObj";

export class FTextObj extends SvgObj
{
    private textEl: HTMLParagraphElement | null = null;

    constructor(screen:IScreen,key:string)
    {
        super(screen,key);
    }
    static createNew()
    {
        //implement here
    }
  
    setText(text:string)
    {
        if (this.el)
        {
            console.log(text);
            if (this.el) {
                this.textEl = this.el.getElementsByTagName("p")[0];
                console.log(this.textEl);
                this.textEl.innerHTML = text;                                               
            }
            //this.el.textContent=text;
            this.updateSizes();
        }
    }
    setFontSize(size:number)
    {
        if (this.el)
        {
            this.el.setAttribute("font-size",size.toString()+"px");
            this.updateSizes();
        }
    }
}