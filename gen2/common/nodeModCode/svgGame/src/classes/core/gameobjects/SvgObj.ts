import { GameManager } from "../GameManager";
import { IGameObj } from "../interfaces/IGameObj";
import { IScreen } from "../interfaces/IScreen";

export class SvgObj implements IGameObj {
    public id: string;
    public el: HTMLElement | null = null;

    public elID: string = "";

    private _gameWRatio: number = -1;
    private _gameHRatio: number = -1;


    private _x: number = 0;
    private _y: number = 0;
    private _angle: number = 0;
    private _visible: boolean = true;
    private _width: number = 0;
    private _height: number = 0;
    private _displayWidth: number = 0;
    private _displayHeight: number = 0;
    private _scaleX: number = 1;
    private _scaleY: number = 1;

    private _skewX: number = 0;
    private _skewY: number = 0;

    private static count: number = 0;
    private gm: GameManager = GameManager.getInstance();
    public screen: IScreen;
    private _alpha: number = 1;



    constructor(screen: IScreen, id: string, addArea: string = "") {
        this.id = id;
        this.screen = screen;



        let def: HTMLElement | null = document.getElementById(id);
        if (def) {
            this.el = def.cloneNode(true) as HTMLElement;
            this.elID = SvgObj.getInstanceName(id);
            this.el.id = this.elID;
            if (this.el) {

                this._x = this.getAttNum("x");
                this._y = this.getAttNum("y");

                //   let addArea: string = "playArea";

                if (this.gm.gameOptions && addArea == "") {
                    addArea = this.gm.gameOptions.addAreaID;
                }
                document.getElementById(addArea)?.appendChild(this.el);

                let bounds: DOMRect = this.el.getBoundingClientRect();
                this._height = bounds.height;
                this._width = bounds.width;
                this._displayHeight = this._height;
                this._displayWidth = this._width;

                this.gm.regObj(this.elID, this);
            }
        }
        screen.addExisting(this);

    }
    protected updateSizes() {
        let bounds: DOMRect = this.el.getBoundingClientRect();
        this._height = bounds.height;
        this._width = bounds.width;
        this._displayHeight = this._height;
        this._displayWidth = this._width;
    }
    public onClick(callback: Function) {
        if (this.el) {
            this.el.onclick = () => { callback() };
        }
    }
    static getInstanceName(key: string) {
        SvgObj.count++;
        return key + "-" + SvgObj.count.toString();
    }
    public incRot(rot: number) {
        this.angle = this.angle + rot;
    }
    public get angle(): number {
        return this._angle;
    }
    public set angle(value: number) {
        while (value > 360) {
            value -= 360;
        }
        this._angle = value;
        if (this.el) {
            let w2: number = this.displayWidth / 2;
            let h2: number = this.displayHeight / 2;

            w2 += this.x;
            h2 += this.y;

            this.updateTransform();
        }
    }
    public get width(): number {
        return this._width;
    }
    public set width(value: number) {
        this._width = value;
        /* if (this.el) {
            if (this.useC == false) {
                this.el.setAttribute("width", value.toString());
            }
            else {
                this.el.setAttribute("r", value.toString());
            }
        } */
    }
    public get height(): number {
        return this._height;
    }
    public set height(value: number) {
        this._height = value;
        /* if (this.el) {
            if (this.useC == false) {
                this.el.setAttribute("height", value.toString());
            }
            else {
                this.el.setAttribute("r", value.toString());
            }
        } */
    }
    public get displayWidth(): number {
        return this._displayWidth;
    }
    public set displayWidth(value: number) {
        this._displayWidth = value;
        /*  if (this.el) {
             if (this.useC == false) {
                 this.el.setAttribute("width", value.toString());
             }
             else {
                 this.el.setAttribute("r", value.toString());
             }
         } */

        this._scaleX = value / this.width;
        this.updateTransform();
    }

    public get displayHeight(): number {
        return this._displayHeight;
    }
    public set displayHeight(value: number) {
        this._displayHeight = value;
        /*    if (this.el) {
               if (this.useC == false) {
                   this.el.setAttribute("height", value.toString());
               }
               else {
                   this.el.setAttribute("r", value.toString());
               }
           } */
        this._scaleY = value / this._height;
        this.updateTransform();
    }
    public get scaleX(): number {
        return this._scaleX;
    }
    public set scaleX(value: number) {
        this._scaleX = value;
        this._displayWidth = this.width * value;
        this.updateTransform();
    }

    public get scaleY(): number {
        return this._scaleY;
    }
    public set scaleY(value: number) {
        this._scaleY = value;
        this._displayHeight = this.height * value;
        this.updateTransform();
    }

    public setScale(scale: number) {
        this._scaleX = scale;
        this._scaleY = scale;
        this.updateTransform();
    }

    public get skewX(): number {
        return this._skewX;
    }
    public set skewX(value: number) {
        this._skewX = value;
        this.updateTransform();
    }

    public get skewY(): number {
        return this._skewY;
    }
    public set skewY(value: number) {
        this._skewY = value;
        this.updateTransform();
    }

    public get alpha(): number {
        return this._alpha;
    }
    public set alpha(value: number) {
        this._alpha = value;
        if (this.el) {
            this.el.setAttribute("opacity", value.toString());
        }
    }
    public get visible(): boolean {
        return this._visible;
    }
    public set visible(value: boolean) {
        this._visible = value;
        if (this.el) {
            this.el.style.display = (value == true) ? "block" : "none";
        }
    }
    destroy() {
        if (this.el) {
            this.gm.unRegObj(this.id);
            this.el.remove();
        }
    }
    getAttNum(attName: string) {
        if (this.el) {
            let val: string | null = this.el.getAttribute(attName);
            if (val == null) {
                return 0;
            }
            if (isNaN(parseInt(val))) {
                return 0;
            }
            return parseInt(val);
        }
        return 0;
    }
    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
        this.updateTransform();
    }

    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
        this.updateTransform();

    }
    private updateTransform() {
        let w2: number = this.displayWidth / 2;
        let h2: number = this.displayHeight / 2;

        w2 += this.x;
        h2 += this.y;

        w2 = this.roundVal(w2);
        h2 = this.roundVal(h2);

        let rotString: string = "rotate(" + this.roundVal(this.angle).toString() + "," + this.roundVal(w2).toString() + "," + this.roundVal(h2).toString() + ")";

        let posString: string = "translate(" + this.roundVal(this.x).toString() + "," + this.roundVal(this.y).toString() + ")";
        let scaleString: string = "scale(" + this.roundVal(this._scaleX).toString() + "," + this.roundVal(this._scaleY).toString() + ")";
        let skewString: string = "skewX(" + this.roundVal(this._skewX).toString() + ") skewY(" + this.roundVal(this._skewY).toString() + ")";

        let transString: string = rotString + " " + posString + " " + scaleString + " " + skewString;

        // console.log(transString);

        if (this.el) {
            this.el.setAttribute("transform", transString);
        }
    }
    private roundVal(num: number): number {
        return Math.floor(num * 1000) / 1000;
    }
    public incX(val: number) {
        this.x = this._x + val;
    }
    public incY(val: number) {
        this.y = this._y + val;

    }
    public getBoundingClientRect() {
        if (this.el) {
            return this.el.getBoundingClientRect();
        }
        return new DOMRect(0, 0, 100, 100);
    }
    public get gameWRatio(): number {
        return this._gameWRatio;
    }
    public set gameWRatio(value: number) {
        this._gameWRatio = value;
        this.displayWidth = this.screen.gw * value;
        this.scaleY = this.scaleX;
    }
    public get gameHRatio(): number {
        return this._gameHRatio;
    }
    public set gameHRatio(value: number) {
        this._gameHRatio = value;
        this.displayHeight = this.screen.gh * value;
        this.scaleX = this.scaleY;
    }
    adjust() {
        this.y -= this.displayHeight / 2;
        this.x -= this.displayWidth / 2;
    }
    adjustForward() {
        this.y += this.displayHeight / 2;
        this.x += this.displayWidth / 2;
    }
    updateScale() {
        if (this._gameWRatio != -1) {
            this.gameWRatio = this._gameWRatio;
        }
        else {
            if (this._gameHRatio != -1) {
                this.gameHRatio = this._gameHRatio;
            }
        }
    }
    public doResize() {
        //override in class
    }

}