export class FontSizeVo
{
    public defFontSize:number;
    public canvasBase:number;

    constructor(defFontSize:number,canvasBase:number)
    {
        this.defFontSize=defFontSize;
        this.canvasBase=canvasBase;
    }
}