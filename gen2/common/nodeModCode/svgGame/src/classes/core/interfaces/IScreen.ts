import { AlignGrid } from "../../..";
import { IGameObj } from "./IGameObj";

export interface IScreen
{
    start():void;
    destroy():void;
    key:string;
    grid:AlignGrid | null;
    center(pbj:IGameObj,adjust:boolean):void;
    centerH(obj:IGameObj,adjust:boolean):void;
    centerW(obj:IGameObj,adjust:boolean):void;
    changeScreen(nScreen:string):void;
    addExisting(obj:IGameObj):void;
    gh:number;
    gw:number;
    doResize():void;
    tweenGridPos(col:number,row:number,obj:IGameObj,duration:number,callback:Function):void;
    playSound(path:string):void;
}