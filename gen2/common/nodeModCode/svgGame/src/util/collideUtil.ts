import { SvgObj } from "../classes/core/gameobjects/SvgObj";


export class CollideUtil
{
    public static checkOverlap(obj1:SvgObj, obj2:SvgObj) {
        let r1:DOMRect = obj1.getBoundingClientRect();    //BOUNDING BOX OF THE FIRST OBJECT
        let r2:DOMRect = obj2.getBoundingClientRect();    //BOUNDING BOX OF THE SECOND OBJECT
    
        //CHECK IF THE TWO BOUNDING BOXES OVERLAP
      return !(r2.left > r1.right || 
               r2.right < r1.left || 
               r2.top > r1.bottom ||
               r2.bottom < r1.top);
    }
}