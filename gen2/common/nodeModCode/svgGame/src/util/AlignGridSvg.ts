import { IGameObj, PosVo } from "..";

export class AlignGrid {
    public ch: number = 0;
    public cw: number = 0;
    private gw: number = 0;
    private gh: number = 0;
    public gridID: string = "gridLines";

    constructor(rows: number, cols: number, gw: number, gh: number) {
        this.gw = gw;
        this.gh = gh;

        this.cw = Math.floor((gw / cols)*1000)/1000;
        this.ch = Math.floor((gh / rows)*1000)/1000;

    }
    placeAt(col: number, row: number, obj: IGameObj) {
        let xx: number = this.cw * col;
        let yy: number = this.ch * row;

        obj.x = xx;
        obj.y = yy;
    }
    showGrid() {
        //<path stroke-width="4" stroke="red" d="M0 0 L0 100 M100 0 L100 100 "></path>
        let p: string = "";
        for (let i = 0; i < this.gw; i += this.cw) {
            p += "M" + i.toString() + " 0 ";
            p += "L" + i.toString() + " " + this.gh.toString();
        }
        for (let i = 0; i < this.gh; i += this.ch) {
            p += "M0 " + i.toString() + " L" + this.gw + " " + i.toString();
        }

       // console.log(p);

        let p2: string = '<path stroke-width="4" stroke="red" d="' + p + '"></path>'
        document.getElementById(this.gridID).innerHTML = p2;

    }
    findNearestGridXY(xx: number, yy: number): PosVo {
        let row = Math.floor(yy / this.ch);
        let col = Math.floor(xx / this.cw);
        return new PosVo(col, row);
    }
    findNearestGridXYDec(xx: number, yy: number): PosVo {
        let row = yy / this.ch;
        let col = xx / this.cw;
        return new PosVo(col, row);
    }
}