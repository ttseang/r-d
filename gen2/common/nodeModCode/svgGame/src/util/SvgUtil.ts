export class SVGUtil
{
    public static getAttNum(el:HTMLElement,attName: string) {
        if (el) {
            let val: string | null = el.getAttribute(attName);
            if (val == null) {
                return 0;
            }
            if (isNaN(parseInt(val))) {
                return 0;
            }
            return parseInt(val);
        }
        return 0;
    }
    public static trimNum(num:number)
    {
        return Math.round(Math.floor(num*1000)/1000);
    }
    public static findTop(el:HTMLElement,tagName)
    {
        let obj:HTMLElement=el;
        while(obj.tagName!=tagName)
        {
            obj=obj.parentElement;
            if (obj==null || obj==undefined)
            {
                break;
            }

        }
        return obj;
    }
}