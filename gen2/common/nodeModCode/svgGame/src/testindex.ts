import { GameOptions } from "./classes/core/dataobjs/gameOptions";
import { SVGGame } from "./classes/core/SvgGame"

import { ScreenMain } from "./screens/ScreenMain";

//require("./template.html");


window.onload=()=>{

    let options:GameOptions=new GameOptions();
    options.screens=[new ScreenMain()];
    options.addAreaID="addHere";
    options.useFull=true;
    
    let game:SVGGame=new SVGGame("myCanvas",options);
}
