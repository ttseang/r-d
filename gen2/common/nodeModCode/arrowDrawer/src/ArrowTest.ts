import { ArrowDrawCanvas } from "./ArrowDrawCanvas";

export class ArrowTest {
    private canvas: ArrowDrawCanvas;

    constructor() {
        let drawArrowArea: SVGSVGElement = document.getElementById("drawArrowArea") as unknown as SVGSVGElement;
        let mainContent: HTMLElement | null = document.getElementById("mainContent") || document.createElement("div");

       
            this.canvas = new ArrowDrawCanvas(drawArrowArea, mainContent);
            this.canvas.usePercentage = true;
            this.canvas.onComplete = this.showInfo.bind(this);
            // canvas.start();
            this.canvas.start();
        

        const btnTest: HTMLButtonElement = document.getElementById("btnTest") as HTMLButtonElement;
        btnTest.onclick = () => {
            this.resizeDrawArrowArea();
        }
    }
    resizeDrawArrowArea() {
       /*  const drawArrowArea: SVGSVGElement = document.getElementById("drawArrowArea") as unknown as SVGSVGElement;
        drawArrowArea.style.width = "300px";
        drawArrowArea.style.height = "300px"; */

        const mainContent: HTMLElement | null = document.getElementById("mainContent") || document.createElement("div");
        
        const s = Math.floor(Math.random() * 1000)+50;
        

        mainContent.style.width = s.toString()+"px";
        mainContent.style.height = s.toString()+"px";

        this.canvas.testResize();
        
    }
    showInfo(data: string) {
        console.log(this.canvas.getData());
        console.log(data);
        
        this.canvas.testData(data);
    }
}