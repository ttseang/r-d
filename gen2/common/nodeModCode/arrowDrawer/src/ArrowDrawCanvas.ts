import { ArrowUtil, SvgArrow } from "@teachingtextbooks/svgarrow";

export class ArrowDrawCanvas {
    private startX: number = 0;
    private startY: number = 0;

    /*  private endY: number = 0;
     private endX: number = 0; */

    private rx: number = 0;
    private ry: number = 0;

    public curvePosition = "cw";

    //private dragRect: SvgObj;
    //  private selectRect: SvgObj;

    private drawArrow: SvgArrow | null = null;
    

    public color: string = "black";
    private isDown: boolean = false;

    private element: SVGSVGElement | null;
    private parentElement: HTMLElement | null;
    private active: boolean = true;
    public onComplete: Function = () => { };
    public onStart: Function = () => { };

    private arrowStartX: number = -1;
    private arrowStartY: number = -1;

    private message: string = "";
    private messagePos: number = 0;
    private messageColor: string = "black";

    //arrow pointer
    private textPointer: string = "&#9733";
    private textPointerFill: string = "black";


    private containerW: number = 0;
    private containerH: number = 0;

    public usePercentage: boolean = false;

    private moveFunction: EventListener;
    private upFunction: EventListener;
    private downFunction: EventListener;

    private drawOnce:boolean=false;

    constructor(element: SVGSVGElement, parentElement: HTMLElement) {


        this.element = element;
        this.parentElement = parentElement;


        ArrowUtil.makeCss();
        ArrowUtil.makeDefs();

        this.moveFunction = this.onMouseMove.bind(this) as EventListener;
        this.upFunction = this.stopDragRect.bind(this) as EventListener;
        this.downFunction = this.startRectDrag.bind(this) as EventListener;


        if (this.element) {
            this.drawArrow = new SvgArrow("arrowtemp2", this.element);
          //  this.drawArrow.usePercentages=true;
            if (this.drawArrow.el) {
                this.element.append(this.drawArrow.el);
            }

        }
        else {
            console.warn("drawArrowArea missing");
        }

    }
    setArrowStart(xx: number, yy: number) {
        this.arrowStartX = xx;
        this.arrowStartY = yy;
    }
    setPointer(text: string, fill: string) {
        this.textPointer = text;
        this.textPointerFill = fill;
    }
    setMessage(message: string, position: number, color: string = "black") {
        this.message = message;
        this.messagePos = position;
        this.messageColor = color;
    }
    startDebug() {
        if (this.parentElement) {
            this.parentElement.onmousedown = (e: MouseEvent) => {
                // console.log(e.clientX,e.clientY);
                if (this.parentElement) {
                    const bb: DOMRect = this.parentElement.getBoundingClientRect();
                    const px: number = bb.left;
                    const py: number = bb.top;
                    const xx: number = e.clientX - px;
                    const yy: number = e.clientY - py;
                    //    console.log(xx,yy);
                }
            }
        }

    }
    start(once:boolean=true) {

        this.drawOnce=once;

        if (this.drawArrow) {
            if (this.drawArrow.el) {
                this.drawArrow.el.id = "drawArrow";
                // this.drawArrow.setEnd("arrowpoint");
                this.drawArrow.setFill(this.color);
            }

            this.drawArrow.visible = false;

            if (this.parentElement) {
                this.parentElement.addEventListener("mousedown", this.downFunction);
               // this.parentElement.addEventListener("mouseup", this.upFunction);
               // this.parentElement.addEventListener("mousemove", this.moveFunction);
            }
        }
    }
    stop() {
        //probably not needed anymore but just in case

        if (this.parentElement) {
            this.parentElement.removeEventListener("mousedown", this.downFunction);
            this.parentElement.removeEventListener("mouseup", this.upFunction);
            this.parentElement.removeEventListener("mousemove", this.moveFunction);
        }
        this.active = false;
    }
   
    startRectDrag(e: MouseEvent) {
        //console.log(e);
        if (this.active === false) {
            return;
        }
        if (!this.drawArrow) {
            return;
        }

        this.isDown = true;
        if (!this.parentElement) {
            return;
        }

        this.onStart();

        const bb: DOMRect = this.parentElement.getBoundingClientRect();
        const px: number = bb.left;
        const py: number = bb.top;
        const xx: number = e.clientX - px;
        const yy: number = e.clientY - py;

        // console.log(xx,yy);

        if (this.arrowStartX !== -1 && this.arrowStartY !== -1) {
            this.startX = this.arrowStartX;
            this.startY = this.arrowStartY;
        }
        else {
            this.startX = xx;
            this.startY = yy;
        }

        this.drawArrow.messageColor = this.messageColor;
        this.drawArrow.message = this.message;
        this.drawArrow.messagePos = this.messagePos;

        this.drawArrow.x = this.startX;
        this.drawArrow.y = this.startY;
        this.drawArrow.visible = true;
        if (this.parentElement) {
            this.parentElement.removeEventListener("mousedown", this.downFunction);
            this.parentElement.addEventListener("mouseup", this.upFunction);
            this.parentElement.addEventListener("mousemove", this.moveFunction);
        }
    }
    onMouseMove(e: MouseEvent) {
       // console.log("MOVE");
        if (this.active === false) {
            return;
        }
        if (!this.drawArrow) {
            return;
        }
        if (this.isDown === true) {
            //we need to get the relative position
            //by subtracting the current mouse position

            if (!this.parentElement) {
                return;
            }

            const bb: DOMRect = this.parentElement.getBoundingClientRect();
            const px: number = bb.left;
            const py: number = bb.top;
            const xx: number = e.clientX - px;
            const yy: number = e.clientY - py;

            let w: number = xx - this.startX;
            let h: number = yy - this.startY;
            let angle = 0, perc = 0.5;


            switch (this.curvePosition) {
                case "cw": { angle = -45; perc = 0.75; break; }
                case "ccw": { angle = 45; perc = 0.75; break; }
            }
            let { rx, ry } = ArrowUtil.getRotate(0, 0, w, h, angle);
            this.rx = ArrowUtil.lerp(0, rx, perc);
            this.ry = ArrowUtil.lerp(0, ry, perc);


            /**set the properties on the arrow
            DON'T draw on the element
            we need to be able to edit and rebuild
            based on the properties**/

            this.drawArrow.setCurve(this.rx, this.ry);
            this.drawArrow.x = this.startX;
            this.drawArrow.y = this.startY;
            this.drawArrow.setLen(w);
            this.drawArrow.setH(h);
            //this.drawArrow.setFill(this.color);

            //this.addEndings(this.drawArrow, this.endingIndex);

        }
    }
    stopDragRect(e: MouseEvent) {
        if (this.active === false) {
            return;
        }
        if (!this.drawArrow) {
            return;
        }
        if (!this.parentElement) {
            return;
        }

        const bb: DOMRect = this.parentElement.getBoundingClientRect();
        const px: number = bb.left;
        const py: number = bb.top;
        const xx: number = e.clientX - px;
        const yy: number = e.clientY - py;

        /*  this.endX = xx;
         this.endY = yy; */

        if (e.clientY < 100) {
            return;
        }
        this.isDown = false;
        // this.dragRect.visible=false;

     //   this.drawArrow.visible = false;


        if (this.usePercentage === true) {
            
            this.onComplete(this.drawArrow.exportPercentData());
        }
        else {
            this.onComplete(this.drawArrow.exportData());
        }

        if (this.parentElement) {
            if (this.drawOnce===false)
            {
                this.parentElement.addEventListener("mousedown", this.downFunction);
            }            
            this.parentElement.removeEventListener("mouseup", this.upFunction);
            this.parentElement.removeEventListener("mousemove", this.moveFunction);
        }
    }
    public getPercentData()
    {
        if (this.drawArrow) {
            return this.drawArrow.exportPercentData();
        }
        return null;
    }
   public  getData() {
        if (this.drawArrow) {
            return this.drawArrow.exportData();
        }
        return null;
    }
    public testData(data:string)
    {
        if (this.drawArrow)
        {
            this.drawArrow.usePercentages=true;
            this.drawArrow.fromString(data);
        }
       
    }
    public testResize()
    {
        if (this.drawArrow)
        {
            this.drawArrow.onResize();
        }
    }
    public setDebug(debug:boolean)
    {
        if (this.drawArrow)
        {
            this.drawArrow.debug=debug;
        }
    }
}