
import { Align, PosVo } from "ttphasercomps";
import { DragEng } from "../classes/DragEng";
import { LetterBox } from "../classes/LetterBox";
import { TargetBox } from "../classes/TargetBox";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {

    private dragEng: DragEng;
    private word: string = "Hello";

    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("holder", "./assets/holder.jpg");
    }
    create() {
        super.create();
        this.makeGrid(11, 11);

        window['scene']=this;

        this.dragEng = new DragEng(this);

        window.onresize = this.doResize.bind(this);

        this.makeBoxes();

    }
    resetBoxes()
    {
        this.dragEng.resetBoxes();
    }
    makeBoxes() {

        for (let i: number = 0; i < this.word.length; i++) {
            let box: LetterBox = new LetterBox(this, this.word.substr(i, 1));
            box.setInteractive();
            box.setPlace(new PosVo(3 + i, 5));
            box.original = box.place;
            this.dragEng.addDrag(box);
        }

        for (let i: number = 0; i < this.word.length; i++) {
            let targ: TargetBox = new TargetBox(this, 0xff0000);
            targ.setPlace(new PosVo(3 + i, 8));
            this.dragEng.addTarget(targ);
        }
        this.dragEng.setListeners();
        this.dragEng.dropCallback=this.checkCorrect.bind(this);

    }
    checkCorrect() {
        let checkString: string = this.dragEng.getTargContent();
        if (checkString == this.word) {
            console.log("correct");
        }
        else {
            console.log("wrong");
        }
    }
    doResize() {
        let w: number = window.innerWidth;
        let h: number = window.innerHeight;

        this.resetSize(w, h);
        this.scale.resize(w, h);
        this.getGrid().hide();
        this.makeGrid(11, 11);
        this.dragEng.doResize();
        // this.grid.showPos();
        //this.cm.doResize();

    }
}