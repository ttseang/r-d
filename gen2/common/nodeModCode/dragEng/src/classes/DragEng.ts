import { IBaseScene, PosVo } from "ttphasercomps";
import { IDragObj } from "../interfaces/IDragObj";
import { ITargetObj } from "../interfaces/ITargetObj";

export class DragEng {
    private bscene: IBaseScene;
    private scene: Phaser.Scene;

    public targets: ITargetObj[] = [];
    public dragObjs: IDragObj[] = [];
    public clickLock: boolean = false;
    private dragBox: IDragObj | null = null;
    private chosenContent: string = "";

    public upCallback: Function = () => { };
    public dropCallback: Function = () => { };
    public collideCallback: Function = () => { }



    constructor(bscene: IBaseScene) {
        this.bscene = bscene;
        this.scene = bscene.getScene();


    }
    public addDrag(dragObj: IDragObj) {
        dragObj.type = "dragObj";
        this.dragObjs.push(dragObj);
    }
    public addTarget(target: ITargetObj) {
        this.targets.push(target);
    }
    public destroyDrags() {
        for (let i: number = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].getContainer().destroy();
        }
    }
    public destroyTargs() {
        for (let i: number = 0; i < this.targets.length; i++) {
            this.targets[i].getContainer().destroy();
        }
    }
    doResize() {
        for (let i: number = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].doResize();
        }
        for (let i: number = 0; i < this.targets.length; i++) {
            this.targets[i].doResize();
        }
    }
    public setListeners() {
        this.scene.input.on("gameobjectdown", this.onDown.bind(this));
        this.scene.input.on("pointermove", this.onMove.bind(this));
        this.scene.input.on("pointerup", this.onUp.bind(this));
    }
    onDown(p: Phaser.Input.Pointer, dragObj: IDragObj) {
        if (this.clickLock == true) {
            return;
        }
        if (dragObj.type != "dragObj") {
            return;
        }
        this.dragBox = dragObj;
        this.dragBox.resetPlace = new PosVo(this.dragBox.x, this.dragBox.y);

        this.scene.children.bringToTop(dragObj.getContainer());

    }
    onMove(p: Phaser.Input.Pointer) {
        if (this.dragBox) {
            this.dragBox.x = p.x;
            this.dragBox.y = p.y;
        }
    }
    onUp() {
        if (this.dragBox) {
            this.upCallback(this.dragBox);
            if (this.dragBox.canDrag == false) {
                return;
            }

            let found: boolean = this.checkTargets();
            if (found == false) {
                this.dragBox.snapBack();
            }
            else {
                this.dropCallback();
            }

           
        }
        this.dragBox = null;
    }
    resetBoxes() {
        for (let i: number = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].setPlace(this.dragObjs[i].original);
        }
    }
    getTargContent() {
        let content: string = "";
        for (let i: number = 0; i < this.targets.length; i++) {
            content += this.targets[i].content;
        }
        return content;
    }
    checkTargets() {

        if (this.dragBox === null) {
            return;
        }

        console.log(this.dragBox);

        for (let i: number = 0; i < this.targets.length; i++) {
            let targetBox: ITargetObj = this.targets[i];

            let distY: number = Math.abs(this.dragBox.y - targetBox.y);
            let distX: number = Math.abs(this.dragBox.x - targetBox.x);

            if (distX < this.dragBox.displayWidth * 0.4 && distY < this.dragBox.displayHeight * 0.4) {
                /* this.dragBox.x = targetBox.x;
                this.dragBox.y = targetBox.y; */
                this.dragBox.setPlace(targetBox.place);

                this.chosenContent = this.dragBox.content;
                targetBox.content = this.dragBox.content;
                this.collideCallback(this.dragBox, targetBox);
                // this.clickLock = true;
                return true;
            }
        }
        return false;
    }
}