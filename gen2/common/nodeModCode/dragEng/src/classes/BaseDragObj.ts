
import { IBaseScene, PosVo, Align, CompManager } from "ttphasercomps";
import { IDragObj } from "../interfaces/IDragObj";


export class BaseDragObj extends Phaser.GameObjects.Container implements IDragObj {
    public scene: Phaser.Scene;
    protected bscene: IBaseScene;

    protected cm:CompManager=CompManager.getInstance();

    public content: string = "";

    public canDrag: boolean = true;

    public original: PosVo = new PosVo(0, 0);
    public place: PosVo = new PosVo(0, 0);
    public resetPlace: PosVo = new PosVo(0, 0);

    constructor(bscene: IBaseScene) {
        super(bscene.getScene())
        this.bscene = bscene;
        this.scene = bscene.getScene();
    }

    getContainer(): Phaser.GameObjects.Container {
        return this;
    }
    

    doResize() {
       
    }
    setContent(content: string) {
        this.content=content;
    }

    public setPlace(posVo: PosVo) {
        this.place = posVo;
        this.bscene.getGrid().place(this.place, this);
    }
    public snapBack()
    {
        this.scene.tweens.add({ targets: this, duration: 500, y: this.resetPlace.y, x: this.resetPlace.x });
    }
}