
import { IBaseScene, PosVo, Align, CompManager } from "ttphasercomps";
import { ITargetObj } from "..";
;


export class BaseTargObj extends Phaser.GameObjects.Container implements ITargetObj {
    public scene: Phaser.Scene;
    protected bscene: IBaseScene;

    protected cm: CompManager = CompManager.getInstance();

    public content: string = "";
    protected color: number;

    public original: PosVo = new PosVo(0, 0);
    public place: PosVo = new PosVo(0, 0);
    public resetPlace: PosVo = new PosVo(0, 0);
    public correct: string;

    constructor(bscene: IBaseScene) {
        super(bscene.getScene())
        this.bscene = bscene;
        this.scene = bscene.getScene();
    }
    setColor(color: number): void {
        this.color = color;
    }
    
    getContainer(): Phaser.GameObjects.Container {
        return this;
    }


    doResize() {

    }
    setContent(content: string) {
        this.content=content;
    }

    public setPlace(posVo: PosVo) {
        this.place = posVo;
        this.bscene.getGrid().place(this.place, this);
    }

}