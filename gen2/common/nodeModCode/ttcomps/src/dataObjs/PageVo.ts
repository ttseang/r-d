import { CompVo } from "./CompVo";

export class PageVo
{
    public pageName:string="";
    public comps:CompVo[];
    public backgroundType:string="color";
    public backgroundParams:string="0xff0000";
    
    constructor(pageName:string,comps:CompVo[])
    {
        this.pageName=pageName;
        this.comps=comps;
    }
}