export class BackStyleVo
{
    public backColor:number;
    public borderColor:number;
    public borderPress:number;
    public borderOver:number;
    public borderThick:number;
    public round:boolean=true;
    constructor(backColor:number,borderThick:number, borderColor:number, borderPress:number, borderOver:number)
    {
        this.backColor=backColor;
        this.borderThick=borderThick;
        this.borderColor=borderColor;
        this.borderPress=borderPress;
        this.borderOver=borderOver;
    }
}