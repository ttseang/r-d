import { AnchorVo } from "./AnchorVo";

export class CompVo
{
    public key:string;
    public type:string;
    public x:number;
    public y:number;
    public w:number;
    public h:number;
    public style:string;
    public backstyle:string;
    public text:string;

    public flipX:boolean=false;
    public flipY:boolean=false;
    public angle:number=0;

    public anchorVo:AnchorVo=null;
    public icon:string;

    public action:string="";
    public actionParam:string="";

    constructor(key:string,type:string,text:string,icon:string,x:number,y:number,w:number,h:number,style:string,backstyle:string)
    {
        this.key=key;
        this.type=type;
        this.text=text;
        this.icon=icon;
        this.x=x;
        this.y=y;
        this.w=w;
        this.h=h;
        this.style=style;
        this.backstyle=backstyle;
    }
}