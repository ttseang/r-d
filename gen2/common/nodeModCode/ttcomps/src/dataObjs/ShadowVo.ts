export class ShadowVo {
    public x: number;
    public y: number;
    public color: string;
    public blur: number;
    public stroke: boolean;
    public fill: boolean;
    constructor(x: number, y: number, color: string, blur: number, stroke: boolean, fill: boolean) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.blur = blur;
        this.stroke = stroke;
        this.fill = fill;
    }
}