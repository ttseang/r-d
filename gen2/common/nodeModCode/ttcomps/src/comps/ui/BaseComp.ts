
import { IComp, PosVo, IBaseScene, BackStyleVo, TextStyleVo } from "../..";
import CompManager from "../CompManager";


export class BaseComp extends Phaser.GameObjects.Container implements IComp {
    public posVo: PosVo = new PosVo(0, 0);
    public bscene: IBaseScene;
    protected cm: CompManager = CompManager.getInstance();

    private anchorObj: IComp | null = null;
    private anchorX: number = 0;
    private anchorY: number = 0;
    private anchorInside: boolean = false;


    private insideObj: IComp | null = null;
    private insidePerW: number = 0;
    private insiderPerH: number = 0;

    protected backStyleVo: BackStyleVo;
    protected textStyleVo: TextStyleVo;

    public ingoreRepos: boolean = false;

    public key: string;

    constructor(bscene: IBaseScene, key: string) {
        super(bscene.getScene());
        this.bscene = bscene;
        this.cm.comps.push(this);
        this.cm.compMap.set(key, this);
    }
    doResize() {
        if (this.ingoreRepos === false) {
            this.bscene.getGrid().placeAt(this.posVo.x, this.posVo.y, this);
        }
        this.reAnchor();
        this.reposRel();
    }
    setPos(xx: number, yy: number) {
        this.posVo = new PosVo(xx, yy);

        if (this.ingoreRepos === false) {
            this.bscene.getGrid().placeAt(xx, yy, this);
        }
    }
    anchorTo(obj: IComp, anchorX: number, anchorY: number, inside: boolean) {
        this.insideObj = null;
        this.anchorObj = obj;
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.anchorInside = inside;
        this.reAnchor();
    }
    placeInside(obj: IComp, perW: number, perH: number) {
        this.anchorObj = null;
        this.insideObj = obj;
        this.insidePerW = perW;
        this.insiderPerH = perH;
        this.reposRel();
    }
    reposRel() {
        if (this.insideObj === null) {
            return;
        }
        let xx: number = this.insideObj.displayWidth * this.insidePerW;
        let yy: number = this.insideObj.displayHeight * this.insiderPerH;

        let startX: number = this.insideObj.x - this.insideObj.displayWidth / 2 + this.displayWidth / 2;
        let startY: number = this.insideObj.y - this.insideObj.displayHeight / 2 + this.displayHeight / 2;

        this.x = startX + xx;
        this.y = startY + yy;
    }
    reAnchor() {
        if (this.anchorObj === null) {
            return;
        }
        if (this.anchorX != -1) {
            if (this.anchorInside == false) {


                switch (this.anchorX) {
                    case 0:

                        this.x = this.anchorObj.x - this.anchorObj.displayWidth / 2 + this.displayWidth / 2;
                        break;

                    case 0.5:

                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2;

                        break;

                    case 1:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2 - this.displayWidth / 2;
                        break;
                }
            }
            else {
                switch (this.anchorX) {
                    case 0:

                        this.x = this.anchorObj.x - this.anchorObj.displayWidth / 2 + this.displayWidth * 2;
                        break;

                    case 0.5:

                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2;

                        break;

                    case 1:
                        this.x = this.anchorObj.x + this.anchorObj.displayWidth / 2 - this.displayWidth / 1.5;
                        break;
                }
            }
        }

        if (this.anchorY != -1) {
            if (this.anchorInside == false) {


                switch (this.anchorY) {
                    case 0:

                        this.y = this.anchorObj.y - this.anchorObj.displayHeight / 2 + this.displayHeight / 2;
                        break;

                    case 0.5:

                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2;

                        break;

                    case 1:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2 - this.displayHeight / 2;
                        break;
                }
            }
            else {
                switch (this.anchorY) {
                    case 0:

                        this.y = this.anchorObj.y - this.anchorObj.displayHeight / 2 + this.displayHeight;
                        break;

                    case 0.5:

                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2;

                        break;

                    case 1:
                        this.y = this.anchorObj.y + this.anchorObj.displayHeight / 2 - this.displayHeight / 1.5;
                        break;
                }
            }
        }
    }
}