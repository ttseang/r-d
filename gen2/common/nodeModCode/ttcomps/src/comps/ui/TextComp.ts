import { GameObjects } from "phaser";
import { IComp, TextStyleVo, IBaseScene } from "../..";

import { BaseComp } from "./BaseComp"

export class TextComp extends BaseComp implements IComp {
    private text1: GameObjects.Text;
    public ww: number = 0;
    public textStyleVo: TextStyleVo;
    private maxWidth:number;

    constructor(bscene: IBaseScene, key: string, text: string, maxWidth: number, textStyle: string) {
        super(bscene, key);

        this.maxWidth=maxWidth;
        this.ww = bscene.getW()*maxWidth;
        console.log(this.ww);

        let textStyleVo: TextStyleVo = this.cm.getTextStyle(textStyle);
        this.textStyleVo = textStyleVo;

       // //console.log(textStyleVo);

        this.text1 = this.scene.add.text(0, 0, text).setOrigin(0.5, 0.5);
        this.text1.setFontFamily(textStyleVo.fontName);
        this.text1.setColor(textStyleVo.textColor);

        if (textStyleVo.strokeVo) {
            //console.log(textStyleVo.strokeVo);
            this.text1.setStroke(textStyleVo.strokeVo.color, textStyleVo.strokeVo.thickness);
        }

        if (textStyleVo.shadowVo) {
            this.text1.setShadow(textStyleVo.shadowVo.x, textStyleVo.shadowVo.y, textStyleVo.shadowVo.color, textStyleVo.shadowVo.blur, textStyleVo.shadowVo.stroke, textStyleVo.shadowVo.fill);
        }

        if (textStyleVo.autoSize===false)
        {
            let fs:number=this.cm.getFontSize(textStyleVo.fontSizeKey,this.bscene.getW());
            this.text1.setFontSize(fs);
        }
        else
        {
            this.sizeText();
        }
        

        this.add(this.text1);
       // 
        this.scene.add.existing(this);
    }
    setText(text:string)
    {
        this.text1.setText(text);
    }
    setColor(c: string) {
        this.text1.setColor(c);
    }
    sizeText() {
        this.text1.setFontSize(this.textStyleVo.maxFontSize);

        let fs: number = parseInt(this.text1.style.fontSize.split("px")[0]);
        while (this.text1.displayWidth > this.ww * 0.95) {
            fs--;
            this.text1.setFontSize(fs);
        }
    }
    doResize() {
        this.ww = this.bscene.getW()*this.maxWidth;
        this.sizeText();
        super.doResize();
    }
}