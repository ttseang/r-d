
import { IBaseScene, BackStyleVo } from "../..";
import { BaseComp } from "./BaseComp";
import { CompBack } from "./CompBack";

export class BlankComp extends BaseComp
{
    private back: CompBack;
    private obj:any;

    constructor(bscene:IBaseScene,key:string,obj:any,style:string)
    {
        super(bscene,key);

        this.obj=obj;
        let backStyleVo:BackStyleVo=this.cm.getBackStyle(style);

        this.back=new CompBack(bscene,obj.displayWidth,obj.displayHeight,backStyleVo);
        
        

        this.add(this.back);
        this.add(obj);

        this.setSize(obj.displayWidth,obj.displayHeight);

        this.scene.add.existing(this);
    }
    doResize()
    {
        this.back.doResize(this.obj.displayWidth,this.obj.displayHeight);
        super.doResize();
    }
}