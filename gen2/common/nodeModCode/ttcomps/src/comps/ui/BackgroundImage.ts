import { GameObjects } from "phaser";
import IBaseScene from "../../interfaces/IBaseScene";
import { IComp } from "../../interfaces/IComp";

import { BaseComp } from "./BaseComp";

export class BackgroundImage extends BaseComp implements IComp
{
    private back:GameObjects.Image;

    constructor(bscene:IBaseScene,key:string,type:string,params:string)
    {
        super(bscene,key);

        console.log(params);

        if (type==="color")
        {
            this.back=this.scene.add.image(-this.bscene.cw/2,-this.bscene.ch/2,"holder").setOrigin(0,0);
            this.back.displayWidth=this.bscene.getW();
            this.back.displayHeight=this.bscene.getH();

            this.back.setTint(parseInt(params));
            this.add(this.back);
            this.setPos(0,0);
            this.setSize(this.back.displayWidth,this.back.displayHeight);
        }

        this.scene.add.existing(this);
    }
    doResize()
    {
        this.back.displayWidth=this.bscene.getW();
        this.back.displayHeight=this.bscene.getH();
        this.setSize(this.back.displayWidth,this.back.displayHeight);
        super.doResize();
    }
}