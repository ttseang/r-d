import { GameObjects } from "phaser";
import { IComp, PosVo, IBaseScene } from "../..";
import { ButtonStyleVo } from "../../dataObjs/ButtonStyleVo";

import { ButtonController } from "../ButtonController";
import { BaseComp } from "./BaseComp";
import { CompBack } from "./CompBack";
import { TextComp } from "./TextComp";

export class BorderButton extends BaseComp implements IComp {

    private buttonVo: ButtonStyleVo;
    private back: CompBack;


    private text1: TextComp
    private buttonController:ButtonController=ButtonController.getInstance();

    private ww: number;
    private hh: number;


    public action: string;
    public actionParam: string;
    public posVo: PosVo = new PosVo(0, 0);

    constructor(bscene: IBaseScene, key: string, text: string, action: string, actionParam: string, buttonVo: ButtonStyleVo) {
        super(bscene, key);
        this.buttonVo = buttonVo;
        // let ww: number = this.bscene.getW() * buttonVo.hsize;
        let hh: number = this.bscene.getH() * buttonVo.vsize;
        let ww: number = this.bscene.getW() * buttonVo.hsize;

        this.action = action;
        this.actionParam = actionParam;

        this.ww = ww;
        this.hh = hh;

        this.backStyleVo = this.cm.getBackStyle(buttonVo.backStyle);
        this.textStyleVo=this.cm.getTextStyle(buttonVo.textStyle);

        console.log(buttonVo.hsize);

        this.text1=new TextComp(this.bscene,key,text,buttonVo.hsize,buttonVo.textStyle);
        this.text1.ingoreRepos=true;
       
        //  hh=this.text1.displayHeight*3;

        this.back = new CompBack(bscene, this.ww, this.hh, this.backStyleVo);


        this.add(this.back);

        this.add(this.text1);

        this.scene.add.existing(this);
        this.setSize(ww, hh);


        this.setInteractive();

        this.on("pointerdown", () => {
            this.setBorder(this.backStyleVo.borderPress);
            this.buttonController.doAction(this.action,this.actionParam);
        })
        this.on("pointerup", () => {
            this.resetBorder();
        });
        this.on("pointerover", () => {
            this.setBorder(this.backStyleVo.borderOver);
        });
        this.on("pointerout", () => {
            this.resetBorder();
        });

       // this.sizeText();

        window['btn'] = this;
    }
    
    setText(text: string) {
        this.text1.setText(text);
    }
    doResize() {
        //  let ww: number = this.bscene.getW() * this.buttonVo.hsize;
        let hh: number = this.bscene.getH() * this.buttonVo.vsize;
        let ww: number = hh * 2;
        this.ww = ww;
        this.hh = hh;


        /* this.back.clear();


        this.back.fillStyle(this.backStyleVo.backColor, 1);
        this.back.fillRoundedRect(-ww / 2, -hh / 2, ww, hh, 8);
        this.back.fillPath(); */

        this.back.doResize(this.ww, this.hh);

        this.setSize(ww, hh);

        super.doResize();

        this.text1.sizeText();
        this.text1.x=0;
        this.text1.y=0;
        
    }

    resetBorder() {
        this.back.lineStyle(this.backStyleVo.borderThick, this.backStyleVo.borderColor);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    }
    setBorder(color: number) {
        this.back.lineStyle(this.backStyleVo.borderThick, color);
        this.back.strokeRoundedRect(-this.ww / 2, -this.hh / 2, this.ww, this.hh, 8);
        this.back.stroke();
    }
}