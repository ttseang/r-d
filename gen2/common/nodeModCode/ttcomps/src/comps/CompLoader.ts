import { AnchorVo, BackStyleVo, CompManager, CompVo, PageVo, ShadowVo, StrokeVo, TextStyleVo } from "..";
import { ButtonStyleVo } from "../dataObjs/ButtonStyleVo";

export class CompLoader {
    private cm: CompManager = CompManager.getInstance();
    private callback: Function;

    constructor(callback: Function) {
        this.callback = callback;
    }
    public loadComps(file: string) {
        fetch(file)
            .then(response => response.json())
            .then(data => this.process({ data }));
    }
    public process(data: any) {
        let info: any = data.data.info;
        let startPage: string = info.startPage;
        //console.log(startPage);
        this.cm.startPage = startPage;

        //textstyles

        let textStyles: any = data.data.textStyles;

        if (textStyles) {
            for (let i: number = 0; i < textStyles.length; i++) {
                let style: any = textStyles[i];
                let key: string = style.key;
                let ff: string = style.fontFamily;
                let color: string = style.color;
                let maxSize: number = parseFloat(style.maxSize);
                let stroke: any = style.stroke;
                let strokeVo: StrokeVo | null = null;
                let fontSizeKey:string=style.fontSize || "default";

                let autoSize:boolean=(style.fillToFit==true);
                console.log("autoSize="+autoSize);

                if (stroke) {
                    let strokeThick: number = parseFloat(stroke.thick);
                    let strokeColor: string = stroke.color;
                    strokeVo = new StrokeVo(strokeThick, strokeColor);

                }
                let shadow: any = style.shadow;
                let shadowVo: ShadowVo | null = null;

                if (shadow) {
                    let shadowX: number = parseFloat(shadow.x);
                    let shadowY: number = parseFloat(shadow.y);
                    let shadowColor: string = shadow.color;
                    let shadowBlur: number = parseFloat(shadow.blur);
                    let shadowStroke: boolean = shadow.stroke;
                    let shadowFill: boolean = shadow.fill;

                    shadowVo = new ShadowVo(shadowX, shadowY, shadowColor, shadowBlur, shadowStroke, shadowFill);
                }

                let textStyleVo: TextStyleVo = new TextStyleVo(ff, color, maxSize);
                textStyleVo.autoSize=autoSize;
                textStyleVo.fontSizeKey=fontSizeKey;
                textStyleVo.shadowVo = shadowVo;
                textStyleVo.strokeVo = strokeVo;

                this.cm.regTextStyle(key, textStyleVo);
            }
        }

        //backstyles

        let backStyles: any = data.data.backStyles;
        if (backStyles) {
            for (let i: number = 0; i < backStyles.length; i++) {
                let style: any = backStyles[i];
                let key: string = style.key;
                let backColor: number = parseInt(style.backColor);
                let borderColor: number = parseInt(style.borderColor);
                let borderOver: number = parseInt(style.borderOver);
                let borderPress: number = parseInt(style.borderPress);
                let borderThick: number = parseInt(style.borderThick);

                let backStyle: BackStyleVo = new BackStyleVo(backColor, borderThick, borderColor, borderPress, borderOver);
                this.cm.regBackStyle(key, backStyle);
            }
        }

        //button styles

        let buttonStyles: any = data.data.buttonStyles;
        for (let i: number = 0; i < buttonStyles.length; i++) {
            let buttonStyle = buttonStyles[i];

            let key: string = buttonStyle.key;
            let buttonTextStyle: string = buttonStyle.textStyle;
            let vsize: number = parseFloat(buttonStyle.vsize);
            let hsize: number = parseFloat(buttonStyle.hsize);
            let backStyle: string = buttonStyle.backStyle;
            let buttonStyleVo: ButtonStyleVo = new ButtonStyleVo(buttonTextStyle, hsize, vsize, backStyle);

            this.cm.regButtonStyle(key, buttonStyleVo);
        }

        let fontSizes: any = data.data.fontSizes;

        if (fontSizes) {
            for (let i: number = 0; i < fontSizes.length; i++) {
                let fontSize: any = fontSizes[i];
                let key: string = fontSize.key;
                let base: number = parseFloat(fontSize.base);
                let size: number = parseFloat(fontSize.size);

                console.log(key, base, size);

                this.cm.regFontSize(key, size, base);
            }
        }

        //pages

        let pages: any = data.data.pages;

        for (let k: number = 0; k < pages.length; k++) {


            let page: any = data.data.pages[k];

            let pageVo: PageVo = new PageVo(page.name, []);
            pageVo.backgroundType = page.backgroundType;
            pageVo.backgroundParams = page.backgroundParams;

            //comps

            let comps: any = page.components;
            for (let i: number = 0; i < comps.length; i++) {
                let comp: any = comps[i];

                let id: string = comp.id;
                let type: string = comp.type;
                let text: string = comp.text || "";
                let x: number = parseFloat(comp.x);
                let y: number = parseFloat(comp.y);
                let h: number = parseFloat(comp.h) || -1;
                let w: number = parseFloat(comp.w) || -1;
                let style: string = comp.style || "";
                let backStyle: string = comp.backStyle || "";

                let icon: string = comp.icon || "";
                let action: string = comp.action || "";
                let actionParam: string = comp.actionParam || "";


                let flipX: boolean = comp.flipX || false;
                let flipY: boolean = comp.flipY || false;

                let angle: number = comp.angle || 0;
                //
                //anchor

                let anchorVo: AnchorVo = null;
                let anchorTo: string = comp.anchorTo;

                if (anchorTo) {
                    let anchorY: number = parseInt(comp.anchorY);
                    let anchorX: number = parseInt(comp.anchorX);
                    let anchorInside: boolean = comp.anchorInside;

                    anchorVo = new AnchorVo(anchorTo, anchorX, anchorY, anchorInside);
                }

                let compVo: CompVo = new CompVo(id, type, text, icon, x, y, w, h, style, backStyle);
                compVo.anchorVo = anchorVo;
                compVo.flipX = flipX;
                compVo.flipY = flipY;
                compVo.angle = angle;

                compVo.action = action;
                compVo.actionParam = actionParam;

                // //console.log(compVo);

                pageVo.comps.push(compVo);
                // this.cm.compDefs.push(compVo);
            }
            this.cm.pageDefs.push(pageVo);
        }
        this.callback();
    }
}