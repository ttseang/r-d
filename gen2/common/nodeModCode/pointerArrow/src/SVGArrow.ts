
import { ArrowVo } from "./dataObjs/ArrowVo";
import { SvgObj } from "./SvgObj";
//import { ArrowUtil } from "./util/ArrowUtil";

export class SvgArrow extends SvgObj {

    //start and end points of the arrow svg container
    public arrowPositionX: number = 0;
    public arrowPositionY: number = 0;

    //the starting point of the arrow
    //almost always 0,0
    public startX: number = 0;
    public startY: number = 0;

    //relative end point of arrow
    public endX: number = 400;
    public endY: number = 0;

    //point of the curve
    public cx: number = 200;
    public cy: number = -150;

    //drawing arrow speed
    public animateSpeed: number = 2;

    //element for the drawing head of the arrow
    private tp: SVGTextPathElement | null = null;

    //message for the arrow
    private messagePath: SVGTextPathElement | null = null;

    //pointer attributes
    public textPointerSize: string = "medium";
    public textPointerFill: string = "black";
    public textPointerHtml: string = "&#x25b6;";

    //the length and height of the arrow
    public len: number = 0;
    public h: number = 0;

    //color of the arrow
    public fill: string = "black";

    //color of the arrow to restore when mouseout
    public lastFill: string = "black";


    public strokeWidth: number = 2;

    //classes for the end and start markers
    public startClass: string = "";
    public endClass: string = "";

    //used for setting controls in the probpox
    public endIndex: number = 0;
    public colorIndex: number = 0;

    //element where we draw the line
    public lineElement: SVGGeometryElement | null = null;

    //mask - element where dots are added to reveal the line
    public maskArea: SVGMaskElement | null = null;


    /**Message Atrributes */

    //text label for the arrow
    public message: string = "";

    //percentage for the message
    public messagePos: number = 0;

    //message color
    public messageColor: string = "black";

    //message stroke
    public messageStroke: string = "yellow";

    //message font size
    public messageSize: string = "medium";

    //permited classes - helps with marker color
    private colorClasses: string[] = ['red', 'blue', 'black', 'yellow', 'green',];

    //the SVG definition tag
    private defs: SVGDefsElement;

    //record ccw,cw or straight. Used for controls
    public curvePosition: string = "";

    //the current position of the line drawn
    private drawLineStep: number = 0;

    //time in miliseconds
    private drawLineSpeed: number = 1000;

    //the number of units to draw with each step
    private drawLineInc: number = 1;

    /* the container is the parent element of the arrow
    when the parent is resized, the arrow is resized by calling onResize
   it will scale to a percentage of the parent based on the start and end points
    and the curve point of the line
 */
    private container: SVGSVGElement | null = null;
    private containerW: number = 0;
    private containerH: number = 0;

    //we use percentages to scale the line arrow to the container
    //but we use pixles when drawing the line in the editor
    public usePercentages: boolean = false;

    public debug:boolean=false;

/**
 * Creates an instance of SvgArrow.
 * @param {string} templateID - this is the id of the template in the svg code currently arrowtemp2
 * @param {(SVGSVGElement | null)} [container=null]
 * @memberof SvgArrow
 */
constructor(templateID: string, container: SVGSVGElement | null = null) {
        super(templateID);

        (window as any).arrow = this;

        this.container = container;

        if (this.container) {
            this.containerW = this.container.getBoundingClientRect().width;
            this.containerH = this.container.getBoundingClientRect().height;
        }

        //I know normally we use classes instead of ids but the defs tag is a special case
        //we need to be able to add defs to the svg and need a unique id
        //to avoid adding the same defs multiple times
        this.defs = document.getElementById("svgArrowDefs") as unknown as SVGDefsElement;

        //draw the initial path
        this.makeDString();

        //this.el is the svg element of the arrow
        //created by the SVGOBj class when we call super(templateID)

        //if the element is not null
        //add the css class to the arrow element
        //the mask and the line element are svg child elements of the arrow svg definition

        if (this.el) {

            this.el.classList.add("svgArrow");

            this.lineElement = this.el.getElementsByTagName("path")[0];
            this.lineElement.id = this.elID + "line";

            this.maskArea = this.el.getElementsByTagName("mask")[0];
            this.maskArea.id = this.elID + "mask";
        }
    }
    //this will remove the element from the dom
    public remove() {
        if (this.el) {
            this.el.remove();
        }
    }
    
    //duplicate of onResize?
    //I'll leave it for now but remove it later if it's not needed
     public resetContainer(container: SVGSVGElement) {
        this.container = container;

        if (this.container) {
            this.containerW = this.container.getBoundingClientRect().width;
            this.containerH = this.container.getBoundingClientRect().height;
        }
        this.makeDString();
    } 
    //called when the window or container is resized
    public onResize() {
        if (this.container) {            
            this.containerW = this.container.getBoundingClientRect().width;
            this.containerH = this.container.getBoundingClientRect().height;            
            this.makeDString();
        }
    }
    public setResizeListener() {
        window.addEventListener("resize", this.onResize.bind(this));
    }
    //used for converting the color name to a class
    private capFirst(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    //set one of the pretermined color classes
    //we need to limit to certain colors so we can
    //link to the markers
    public setFill(color: string) {
        for (let i: number = 0; i < this.colorClasses.length; i++) {
            const colorName2: string = "pointerArrow" + this.capFirst(this.colorClasses[i]);
            this.removeStyle(this.colorClasses[i]);
        }
        const colorName: string = "pointerArrow" + this.capFirst(color);

        this.addStyle(colorName);
        this.fill = color;
        if (this.startClass !== "") {
            this.setStart(this.startClass);
        }
        if (this.endClass !== "") {
            this.setEnd(this.endClass);
        }
        this.makeDString();
    }

    //add or remove styles
    public addStyle(style: string) {
        if (this.lineElement) {
            this.lineElement.classList.add(style);
        }
    }
    public removeStyle(style: string) {
        if (this.lineElement) {
            //  ////console.log("remove "+style);
            this.lineElement.classList.remove(style);
        }
    }
    //most of these were used for debugging and testing but I'll leave them in for now
    //TODO:confirm unsed and remove later
    
    public setStrokeWidth(w: number) {
        this.strokeWidth = w;
        this.makeDString();
    }
    public setCurve(cx: number, cy: number) {
        this.cx = cx;
        this.cy = cy;
        this.makeDString();
    }
    public flipCurveY() {
        this.cy = -this.cy;
        this.makeDString();
    }
    public flipUp() {
        this.cy = -Math.abs(this.cy);
        this.makeDString();
    }
    public flipDown() {
        this.cy = Math.abs(this.cy);
        this.makeDString();
    }
    public setCurveH(cy: number) {
        this.cy = cy;
        this.makeDString();
    }
    public setLen(x2: number) {
        this.endX = x2;
        this.len = this.endX - this.startX;
        this.makeDString();
    }
    public setH(y2: number) {
        this.endY = y2;
        this.h = this.endY - this.startY;
        this.makeDString();
    }
    //set the marker at the start of the line

    public setStart(classString: string) {

        if (this.lineElement) {
            this.lineElement.style.display = "block";
            this.startClass = classString;
            if (classString === "none") {
                this.lineElement.setAttribute("marker-start", "");
            }
            else {
                let fullClass: string = classString + this.fill;

                this.lineElement.setAttribute("marker-start", "url(#" + fullClass + ")");
            }
        }
    }
    //set the marker at the end of the line

    public setEnd(classString: string) {

        if (this.lineElement) {
            this.endClass = classString;
            if (classString === "none") {
                this.lineElement.setAttribute("marker-end", "");
            }
            else {
                let fullClass: string = classString + this.fill;
                this.lineElement.setAttribute("marker-end", "url(#" + fullClass + ")");
            }
        }
    }

    //redraw the ending point of the line

    public resetEndLine(endX: number, endY: number) {
        this.endX = endX;
        this.endY = endY;
        this.makeDString();
    }

    //set the mask

    public showMask() {
        if (this.lineElement && this.maskArea) {
            this.lineElement.setAttribute("mask", "url(#" + this.maskArea.id + ")");
        }
    }

    //remove the mask

    public hideMask() {
        if (this.lineElement) {
            this.lineElement.removeAttribute("mask");
        }
    }
    //create a dot with and x and y on a line point
    //add that dot to the mask

    public addDotAt(per: number) {
        if (this.lineElement && this.maskArea) {

            let totalLen: number = this.lineElement.getTotalLength();
            let per2: number = (per / 100) * totalLen;

            let p: DOMPoint = this.lineElement.getPointAtLength(per2);
            /*  let xx2: number = p.x;
             let yy2: number = p.y; */

            const svgns = "http://www.w3.org/2000/svg";

            let dotW: number = this.strokeWidth * 4;

            let dot: SVGCircleElement = document.createElementNS(svgns, "circle");
            dot.setAttribute("cx", p.x.toString());
            dot.setAttribute("cy", p.y.toString());
            dot.setAttribute("r", dotW.toString());
            dot.setAttribute("fill", "white");


            this.maskArea.append(dot);

        }
    }

    //add the arrow head to the line

    public addProgress() {
        if (this.tp) {
            this.tp.remove();
        }
        let text1: SVGTextElement = this.defs.getElementsByTagName("text")[0];

        text1 = text1.cloneNode(true) as SVGTextElement;
        text1.id = this.elID + "text";

        text1.setAttribute("fill", this.textPointerFill);

        if (this.el) {
            this.tp = text1.getElementsByTagName("textPath")[0];
            this.tp.innerHTML = this.textPointerHtml;
            this.tp.style.fontSize = this.textPointerSize;

            this.tp.setAttribute("visibility", "visible");
            this.tp.setAttribute("href", "#" + this.elID + "line");
            this.el.appendChild(text1);
            (window as any).tp = this.tp;
        }
    }

    //move the arrow head to a position on the line

    setProgress(per: number) {
        if (!this.tp) {
            this.addProgress();
        }
        if (this.tp) {
            this.tp.setAttribute("visibility", "visible");
            this.tp.setAttribute("startOffset", per.toString() + "%");
        }
    }
    //start the arrow moving

    startProgress(speed: number, inc: number = 0.5) {
        if (this.maskArea) {
            while (this.maskArea.firstChild) {
                this.maskArea.removeChild(this.maskArea.firstChild);
            }
        }
        this.showMask();
        this.drawLineSpeed = speed;

        this.drawLineInc = inc;
        this.drawLineStep = 0;

        if (!this.tp) {
            this.addProgress();
        }
        this.setProgress(0);

        setTimeout(() => {
            this.advanceProgress();
        }, this.drawLineSpeed);
    }

    //advance the positon of were the line is drawn

    advanceProgress() {
        this.drawLineStep += this.drawLineInc;
        this.addDotAt(this.drawLineStep);
        this.setProgress(this.drawLineStep);
        if (this.drawLineStep < 100) {
            setTimeout(() => {
                this.advanceProgress();
            }, this.drawLineSpeed);
        }
        else {
            if (this.tp) {
                this.tp.setAttribute("visibility", "hidden");
            }
            this.hideMask();
        }
    }


    //place a label on the line

    setMessage(msg: string, per: number = 0) {
        this.message = msg;
        if (this.messagePath) {
            this.messagePath.remove();
            this.messagePath = null;
        }
        if (msg === "") {
            return;
        }
        let text2: SVGTextElement = this.defs.getElementsByTagName("text")[1];

        text2 = text2.cloneNode(true) as SVGTextElement;
        text2.id = this.elID + "message";
        if (text2 && this.el) {
            this.messagePath = text2.getElementsByTagName("textPath")[0];
            this.messagePath.setAttribute("href", "#" + this.elID + "line");
            this.messagePath.innerHTML = msg;
            this.messagePath.setAttribute("fill", this.messageColor);
            //  this.messagePath.setAttribute("stroke",this.messageStroke);
            this.messagePath.setAttribute("strokeWidth", "2px");
            this.messagePath.setAttribute("startOffset", per.toString() + "%");
            this.el.appendChild(text2);
        }
    }

    //set the message position

    setMessagePos(per: number = 0) {
        this.messagePos = per;
        if (this.messagePath) {
            this.messagePath.setAttribute("startOffset", per.toString() + "%");
        }
    }

    setMessageSize(size: string) {
        this.messageSize = size;
        if (this.messagePath) {
            this.messagePath.style.fontSize = size;
        }
    }

    //draw the path inside the line element

    public makeDString() {

        let startX: number = this.startX;
        let startY: number = this.startY;
        let cx: number = this.cx;
        let cy: number = this.cy;
        let endX: number = this.endX;
        let endY: number = this.endY;
        let strokeWidth: number = this.strokeWidth;

        // //console.log("%="+this.usePercentages);


        if (this.usePercentages === true && this.container) {


            let nArrowPosX: number = (this.arrowPositionX / 100) * this.containerW;
            let nArrowPosY: number = (this.arrowPositionY / 100) * this.containerH;



            this.x = nArrowPosX;
            this.y = nArrowPosY;

            startX = (startX / 100) * this.containerW;
            startY = (startY / 100) * this.containerH;

            //
            //
            cx = (cx / 100) * this.containerW;
            cy = (cy / 100) * this.containerH;
            //
            //
            endX = (endX / 100) * this.containerW;
            endY = (endY / 100) * this.containerH;

            strokeWidth = (strokeWidth / 100) * this.containerW;

            if (this.debug === true) {
                console.log("change " + this.arrowPositionX + "% to " + nArrowPosX + "px");
                console.log("change " + this.arrowPositionY + "% to " + nArrowPosY + "px");
                console.log("change endX " + this.endX + "% to " + endX + "px");
                console.log("containerW " + this.containerW);

                console.log("change endY " + this.endY + "% to " + endY + "px");
                console.log("containerH " + this.containerH);
                console.log("change strokeWidth " + this.strokeWidth + "% to " + strokeWidth + "px");

                //  //console.log("startX", startX, "startY", startY, "cx", cx, "cy", cy, "endX", endX, "endY", endY);
            }


        }
        
        //make the path string for the svg element


        let dstring: string = "M" + startX.toString() + "," + startY.toString() + " Q" + cx.toString() + "," + cy.toString() + " " + endX.toString() + "," + endY.toString();
        if (this.debug === true) {
            console.log("dstring", dstring);
        }
        

        if (this.lineElement) {
            this.lineElement.setAttribute("stroke-width", strokeWidth.toString() + "px");
            this.lineElement.setAttribute("d", dstring);
        }

        this.setMessage(this.message, this.messagePos);
        this.setMessageSize(this.messageSize);
    }

    //get the data for export

    public exportData() {
        let arrowVo: ArrowVo = new ArrowVo(this.x, this.y, this.startX, this.startY, this.endX, this.endY, this.cx, this.cy, this.fill, this.startClass, this.endClass);

        arrowVo.message = this.message;
        arrowVo.messagePos = this.messagePos;
        arrowVo.messageSize = this.messageSize;
        arrowVo.messageColor = this.messageColor;

        arrowVo.textPointerHtml = this.textPointerHtml;
        arrowVo.textPointerSize = this.textPointerSize;
        arrowVo.textPointerFill = this.textPointerFill;

        arrowVo.strokeWidth=this.strokeWidth;

        arrowVo.animateSpeed = this.animateSpeed;

        return JSON.stringify(arrowVo);
    }

    //convert pixel to percentage and return an arrowVo object

    public exportPercentData() {

        let arrowPosPercentX: number = 0;
        let arrowPosPercentY: number = 0;

        let startX: number = this.startX;
        let startY: number = this.startY;

        let cx: number = this.cx;
        let cy: number = this.cy;

        let endX: number = this.endX;
        let endY: number = this.endY;

        let strokeWidth: number = this.strokeWidth;

        if (this.container) {


            arrowPosPercentX = (this.x / this.containerW) * 100;
            arrowPosPercentY = (this.y / this.containerH) * 100;

            startX = this.trimNumber((startX / this.containerW) * 100);
            startY = this.trimNumber((startY / this.containerH) * 100);
            //
            //
            cx = this.trimNumber((cx / this.containerW) * 100);
            cy = this.trimNumber((cy / this.containerH) * 100);
            //
            //
            endX = this.trimNumber((endX / this.containerW) * 100);
            endY = this.trimNumber((endY / this.containerH) * 100);

            strokeWidth = this.trimNumber((strokeWidth / this.containerW) * 100);
        }

        let arrowVo: ArrowVo = new ArrowVo(arrowPosPercentX, arrowPosPercentY, startX, startY, endX, endY, cx, cy, this.fill, this.startClass, this.endClass);
        arrowVo.message = this.message;
        arrowVo.messagePos = this.messagePos;
        arrowVo.animateSpeed = this.animateSpeed;
        arrowVo.strokeWidth=strokeWidth;

        return JSON.stringify(arrowVo);
    }

    public fromString(objString: string) {
        let obj: any = JSON.parse(objString);
        this.fromObj(obj);
    }
    public fromObj(obj: any) {
        let arrowVo: ArrowVo = new ArrowVo();
        arrowVo.fromObj(obj);

        this.startX = arrowVo.startX;
        this.startY = arrowVo.startY;

        this.endX = arrowVo.endX;
        this.endY = arrowVo.endY;

        this.cx = arrowVo.cx;
        this.cy = arrowVo.cy;

        this.fill = arrowVo.fill;


        this.setCurve(this.cx, this.cy);
        //  this.x = arrowVo.x;
        //  this.y = arrowVo.y;

        this.arrowPositionX = arrowVo.x;
        this.arrowPositionY = arrowVo.y;

        if (this.usePercentages === false) {
            this.x = arrowVo.x;
            this.y = arrowVo.y;
        }

        this.messageSize = arrowVo.messageSize;
        this.message = arrowVo.message;
        this.messagePos = arrowVo.messagePos;
        this.messageColor = arrowVo.messageColor;

        this.textPointerHtml = arrowVo.textPointerHtml;
        this.textPointerFill = arrowVo.textPointerFill;
        this.textPointerSize = arrowVo.textPointerSize;



        this.startClass = arrowVo.startMark;
        this.endClass = arrowVo.endMark;

        this.animateSpeed = arrowVo.animateSpeed;
        this.strokeWidth=arrowVo.strokeWidth;

        this.makeDString();

        this.setFill(this.fill);


        this.setMessage(this.message, this.messagePos);
        this.setMessageSize(this.messageSize);

    }

    private trimNumber(num: number): number {
        return Math.round(num * 1000) / 1000;
    }
    public setDebug(debug: boolean) {
        this.debug = debug;
    }
}