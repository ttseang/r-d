export class ArrowUtil {
    constructor() {

    }
    public static lerp(start: number, end: number, t: number) {
        return (1 - t) * start + t * end
    }
    public static getRotate(cx: number, cy: number, ex: number, ey: number, angle: number) {
        let radians = (Math.PI / 180) * angle;
        let cos = Math.cos(radians);
        let sin = Math.sin(radians);
        let x = (cos * (ex - cx)) - (sin * (ey - cy)) + cx;
        let y = (cos * (ey - cy)) + (sin * (ex - cx)) + cy;
        return { rx: x, ry: y };
    };
    public static getAngle(x1: number, y1: number, x2: number, y2: number) {
        var dx = x2 - x1;
        var dy = y2 - y1;
        var angle = Math.atan2(dy, dx) * 180 / Math.PI;
        return angle;
    }
    public static makeArrowArea(id: string, parent: string) {
        if (!document.getElementById(id)) {

            const parentElement: HTMLElement = document.getElementById(parent) as HTMLElement;
            if (parentElement) {
                const element: SVGSVGElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                element.id = id;

                element.classList.add("svgFull");

                parentElement.appendChild(element);

            }
            else {
                console.warn("missing parent " + parent);
            }
        }
        // <svg id="addArea" x="0" y="0" class="full">

        // </svg>

    }
    public static makeCss() {
        if (!document.getElementById("arrowStyle")) {

            const styleEl: HTMLStyleElement = document.createElement("style");
            styleEl.id = "arrowStyle";
            styleEl.innerHTML = `.svgArrow{stroke-width:2px}.svgFull{width:100%;height:100%}.svgArrow .pointerArrowRed{stroke:red}.svgArrow .pointerArrowBlue{stroke:blue!important}.svgArrow .pointerArrowBlack{stroke:black!important}.svgArrow .pointerArrowYellow{stroke:yellow!important}.svgArrow .pointerArrowGreen{stroke:green!important}`
            document.head.appendChild(styleEl);
        }

    }
    public static makeDefs() {
        if (!document.getElementById("svgArrowDefs")) {
            const element = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            element.id = "svgArrowDefs";
            element.style.display = "block";
            element.style.backgroundColor = "black";
            element.style.width = "0px";
            element.style.height = "0px";
            
          //  element.setAttribute("visibility", "hidden");
           // element.setAttributeNS(null, "viewBox", "0 0 10 10");
        //    element.setAttributeNS(null, "preserveAspectRatio", "xMinYMin meet");

            const svgStarPath:string="M 50 0 L 60 35 L 100 35 L 70 60 L 80 100 L 50 75 L 20 100 L 30 60 L 0 35 L 40 35 Z";
            
            
            element.innerHTML = ` <defs id='arrowDefs'>
            <style type="text/css">
                .hints path {
                    fill: none;
                    stroke: hsl(200, 0%, 60%);
                    stroke-width: 10;
                    stroke-linecap: round;
                    stroke-linejoin: round;
                    marker-end: url(#arrowhead);
                }

                .hints use,
                #arrowhead {
                    fill: hsl(200, 0%, 60%);
                    font-family: 'Trebuchet MS';
                    font-size: 80px;
                }
            </style>
            <path class="link" id="path1" d="M0 0 L200 400A300 300 0 0 1 490 150"></path>
            <marker id="arrowpointblack" fill="black" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6"
                markerHeight="6" orient="auto-start-reverse">
                <path d="M 0 0 L 10 5 L 0 10 z" />
            </marker>
            <marker id="arrowpointred" fill="red" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6" markerHeight="6"
                orient="auto-start-reverse">
                <path d="M 0 0 L 10 5 L 0 10 z" />
            </marker>
            <marker id="arrowpointblue" fill="blue" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6"
                markerHeight="6" orient="auto-start-reverse">
                <path d="M 0 0 L 10 5 L 0 10 z" />
            </marker>
            <marker id="arrowpointyellow" fill="yellow" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6" markerHeight="6"
            orient="auto-start-reverse">
            <path d="M 0 0 L 10 5 L 0 10 z" />
        </marker>
        <marker id="arrowpointgreen" fill="green" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6" markerHeight="6"
            orient="auto-start-reverse">
            <path d="M 0 0 L 10 5 L 0 10 z" />
        </marker>

        <marker id="arrowdotblack" fill="black" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6"
                markerHeight="6" orient="auto-start-reverse">
                <circle cx="5" cy="5" r="5" fill="black" />
        </marker>

        <marker id="arrowdotred" fill="red" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6"
                markerHeight="6" orient="auto-start-reverse">
                <circle cx="5" cy="5" r="5" fill="red" />
        </marker>

        <marker id="arrowdotgreen" fill="green" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6"
                markerHeight="6" orient="auto-start-reverse">
                <circle cx="5" cy="5" r="5" fill="green" />
        </marker>

        <marker id="arrowdotyellow" fill="yellow" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6"
                markerHeight="6" orient="auto-start-reverse">
                <circle cx="5" cy="5" r="5" fill="yellow" />
        </marker>
        <marker id="arrowdotblue" fill="blue" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="6"
                markerHeight="6" orient="auto-start-reverse">
                <circle cx="5" cy="5" r="5" fill="blue" />
        </marker>
        
                
        <marker id="arrowstarblack" fill="black" viewBox="0 0 100 100" refX="50" refY="50" markerWidth="6"
        markerHeight="6" orient="auto-start-reverse">
        <path d="${svgStarPath}" fill="black" />                
        </marker>
        <marker id="arrowstarred" fill="red" viewBox="0 0 100 100" refX="50" refY="50" markerWidth="6"
        markerHeight="6" orient="auto-start-reverse">
        <path d="${svgStarPath}" fill="red" />
        </marker>
        <marker id="arrowstargreen" fill="green" viewBox="0 0 100 100" refX="50" refY="50" markerWidth="6"
        markerHeight="6" orient="auto-start-reverse">
        <path d="${svgStarPath}" fill="green" />
        </marker>
        <marker id="arrowstaryellow" fill="yellow" viewBox="0 0 100 100" refX="50" refY="50" markerWidth="6"
        markerHeight="6" orient="auto-start-reverse">
        <path d="${svgStarPath}" fill="yellow" />
        </marker>
        <marker id="arrowstarblue" fill="blue" viewBox="0 0 100 100" refX="50" refY="50" markerWidth="6"
        markerHeight="6" orient="auto-start-reverse">
        <path d="${svgStarPath}" fill="blue" />
        </marker>
         <marker id="arrowhead" markerWidth="3" markerHeight="5" orient="auto" refY="2.5">
                <polygon points="0 0, 3 2.5, 0 5" />
            </marker>
            <marker id = "mm" viewBox = "0 0 10 10" refX = "5" refY = "5" markerUnits = "strokeWidth" markerWidth = "3" markerHeight = "3" stroke = "lightblue" stroke-width = "2" fill = "none" orient = "auto">
                <path d = "M 0 0 L 10 10 M 0 10 L 10 0"/>
            </marker>
            
            <g id="arrowtemp2">
                <path id="arrowpath" class="hints black" d="M100,200 Q250,100 400,200" stroke-width="3px"
                    fill="transparent" />
                    <mask id="maskArea">                        
                    </mask>
            </g>
            <text id="tp">
                <textPath text-anchor="middle" href="#arrowpath" dominant-baseline="central">&#x25b6;<animate id="arrowmove"attributeName="startOffset" begin="indefinite" from="0%" to="100%" dur="10s"></animate>
                </textPath>
                
              </text>
              <text id="textMessage">
                <textPath href="#arrowpath" class="lineMessage">Your message here</textPath>
              </text>
        </defs>`;
            document.body.appendChild(element);
        }

    }
}