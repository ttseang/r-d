export class ArrowVo
{
    //location of the arrow on the stage
    public x:number=0;
    public y:number=0;

    //start of the arrow line - almost always 0,0
    public startX: number = 0;
    public startY: number = 0;

    //end of the arrow
    public endX: number = 400;
    public endY: number = 0;

    //curve position
    public cx: number = 200;
    public cy: number = -150;

    public fill: string;
    public message:string="";
    public messagePos:number=0;
    public messageColor:string="black";
    public messageSize:string="medium";

    public textPointerHtml:string="&#x25b6;";
    public textPointerFill:string="black";
    public textPointerSize:string="medium";

    public endMark:string;
    public startMark:string;

    public animateSpeed:number=2;
    
    public strokeWidth:number=2;

    constructor(x:number=0,y:number=0,startX:number=0,startY:number=0,endX:number=0,endY:number=0,cx:number=0,cy:number=0,fill:string="black",startMark:string="",endMark:string="")
    {
        this.x=x;
        this.y=y;

        this.startX=startX;
        this.startY=startY;
        this.endX=endX;
        this.endY=endY;
        this.cx=cx;
        this.cy=cy;
        this.fill=fill;
        this.startMark=startMark;
        this.endMark=endMark;
    }
    fromObj(obj:any)
    {
        this.x=parseFloat(obj.x);
        this.y=parseFloat(obj.y);

        this.startX=parseFloat(obj.startX);
        this.startY=parseFloat(obj.startY);

        this.endX=parseFloat(obj.endX);
        this.endY=parseFloat(obj.endY);

        this.cx=parseFloat(obj.cx);
        this.cy=parseFloat(obj.cy);

        this.strokeWidth=parseFloat(obj.strokeWidth);

        this.fill=obj.fill;
        this.message=obj.message;
        this.messagePos=parseFloat(obj.messagePos);
        this.messageColor=obj.messageColor;
        this.messageSize=obj.messageSize;

        this.textPointerHtml=obj.textPointerHtml;
        this.textPointerFill=obj.textPointerFill;
        this.textPointerSize=obj.textPointerSize;        

        this.endMark=obj.endMark;
        this.startMark=obj.startMark;

        this.animateSpeed=parseFloat(obj.animateSpeed);
    }
}