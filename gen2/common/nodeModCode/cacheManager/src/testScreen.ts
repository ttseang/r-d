import { CacheManager } from "./cacheManager";

export class TestScreen
{
    private btnGet:HTMLButtonElement;
    private btnSet:HTMLButtonElement;
    private btnLocal:HTMLButtonElement;
    private btnLoad:HTMLButtonElement;

    private cache:CacheManager=CacheManager.getInstance();

    constructor()
    {
        this.btnGet=document.getElementById("btnGet") as HTMLButtonElement;
        this.btnSet=document.getElementById("btnSet") as HTMLButtonElement;
        this.btnLocal=document.getElementById("btnLocal") as HTMLButtonElement;
        this.btnLoad=document.getElementById("btnLoad") as HTMLButtonElement;

        this.btnGet.onclick=this.getData.bind(this);
        this.btnSet.onclick=this.setData.bind(this);
        this.btnLocal.onclick=this.saveToLocal.bind(this);
        this.btnLoad.onclick=this.loadFromLocal.bind(this);
    }

    setData()
    {
        let elKey:HTMLInputElement =document.getElementById("keyText") as HTMLInputElement;
        let elVal:HTMLInputElement=document.getElementById("valueText") as HTMLInputElement;
        let elEx:HTMLInputElement=document.getElementById("exVal") as HTMLInputElement;


        let keyText:string=elKey.value;
        let valText:string=elVal.value;
        let exText:string=elEx.value;

        console.log(keyText+" "+valText+" "+exText);

        this.cache.saveToCache(keyText,valText,parseInt(exText));
    }
    getData()
    {
        let elKey:HTMLInputElement =document.getElementById("rkey") as HTMLInputElement;
        let keyText:string=elKey.value;

        console.log(keyText);

        let value:string=this.cache.getFromCache(keyText,"not found");

        let outputSpan:HTMLSpanElement=document.getElementById("outputtext") as HTMLSpanElement;
        outputSpan.innerText=value;
    }
    saveToLocal()
    {
        this.cache.saveToLocal("myapp");
    }
    loadFromLocal()
    {
        this.cache.loadFromLocal("myapp");
    }
}
