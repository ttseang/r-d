export class CacheManager {
    private static instance: CacheManager | null = null;
    private cacheMap: Map<string, CacheVo> = new Map<string, CacheVo>();

    constructor() {

    }
    /**
     *
     *
     * @static
     * @return {*} 
     * @memberof CacheManager
     */
    public static getInstance() {
        if (this.instance === null) {
            this.instance = new CacheManager();
        }
        return this.instance;
    }
    /**
     *
     *
     * @param {string} key
     * @param {string} value
     * @param {number} [expires=0]
     * @memberof CacheManager
     */
    public saveToCache(key: string, value: string, expires: number = 0) {

        expires = expires * 1000;
        let cacheVo: CacheVo = new CacheVo(key, value, expires);
        this.cacheMap.set(key, cacheVo);

    }
    public removeFromCache(key: string) {
        return this.cacheMap.delete(key);
    }
    public getFromCache(key: string, defaultValue: string) {

        if (this.cacheMap.has(key)) {
            let cacheVo: CacheVo | undefined = this.cacheMap.get(key);
            if (cacheVo) {
                let expires: number = cacheVo.expires;
                if (expires !== 0) {
                    let created: number = cacheVo.created;
                    let now: number = new Date().getTime();
                    let timeLimit: number = expires + created;

                    let diff: number = now - timeLimit;

                    if (now > timeLimit) {
                        this.cacheMap.delete(key);
                        return defaultValue;
                    }
                    return cacheVo.value;
                }
                else {
                    return cacheVo.value;
                }
            }
            else {
                return defaultValue;
            }
        }
        return defaultValue;
    }
    saveToLocal(appname: string) {
        let cacheArray: string[] = [];

        this.cacheMap.forEach(((value: CacheVo, key: string, map: Map<string, CacheVo>) => {

            cacheArray.push(JSON.stringify(value))
        }));

        let cacheString: string = cacheArray.join("|");


        localStorage.setItem("ls-" + appname, cacheString);
    }
    loadFromLocal(appname: string) {
        let loadString: string | null = localStorage.getItem("ls-" + appname);
        if (loadString) {
            let loadArray: string[] = loadString.split("|");
            for (let i: number = 0; i < loadArray.length; i++) {

                let cacheVo: CacheVo = new CacheVo("", "", 0);
                let data: any = JSON.parse(loadArray[i])
                cacheVo.fromObj(data);
                this.cacheMap.set(cacheVo.key, cacheVo);
            }
        }
    }
}

class CacheVo {
    public key: string;
    public value: string;
    public created: number;
    public expires: number;

    constructor(key: string, value: string, expires: number = 0) {
        this.key = key;
        this.value = value;
        this.created = new Date().getTime();
        this.expires = expires;
    }
    fromObj(data: any) {
        this.key = data.key;
        this.value = data.value;
        this.created = parseInt(data.created);
        this.expires = parseInt(data.expires);
    }
}