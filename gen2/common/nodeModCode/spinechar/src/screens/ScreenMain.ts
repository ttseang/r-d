
import { BaseScreen, IScreen, PosVo, SvgObj } from "svggame";

import { IconButton } from "../classes/IconButton";
import { SpineChar } from "../classes/SpineChar";

import { AnimationVo } from "../dataObjs/AnimationVo";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private uiWindow!: SvgObj;
    private btnCheck!: SvgObj;
    private btnPlay!: SvgObj;
    private btnReset!: SvgObj;
    private btnHelp!: SvgObj;

    private buttons: SvgObj[] = [];


    //sounds

    private idleAnimations: AnimationVo[] = [];
    private walkAnimations: AnimationVo[] = [];
    private fidgetAnimations: AnimationVo[] = [];

    private spineChar!: SpineChar;

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);

    }
    create() {
        super.create();

        (window as any).scene = this;

        this.uiWindow = new SvgObj(this, "uiWindow");
        this.uiWindow.gameWRatio = .9;

        let scale: number = this.gw / 4000;

        this.idleAnimations = [new AnimationVo("idle", true), new AnimationVo("Repeating animations/blink", true)];
        this.walkAnimations = [new AnimationVo("walk", true), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations = [new AnimationVo("fidget01", false), new AnimationVo("Repeating animations/blink", true)];

        this.spineChar = new SpineChar(this, "char", "./assets/char/Amka.json", "./assets/char/Amka.atlas", this.idleAnimations);
        this.spineChar.currentPos = new PosVo(2, 10);
        this.spineChar.scaleToGameH(.2);

        //
        //
        //

        this.btnPlay = new IconButton(this, "btnPlay", "play", "", this.onbuttonPressed.bind(this));
        this.btnReset = new IconButton(this, "btnReset", "reset", "", this.onbuttonPressed.bind(this));
        this.btnCheck = new IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
        this.btnHelp = new IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));

        this.buttons = [this.btnHelp, this.btnPlay, this.btnReset, this.btnCheck];


    }



    onbuttonPressed(action: string, params: string) {
        console.log(action);
        /*  switch (action) {
             case "reset":
                 this.dragEng.clickLock = false;
                 this.dragEng.resetBoxes();
                 break;
 
             case "check":
                 this.checkCorrect();
                 break;
         } */
    }

    doResize() {
        super.doResize();


        // this.grid?.showGrid();


        this.uiWindow.displayWidth = this.gw * 0.9;
        this.uiWindow.displayHeight = this.gh * 0.70;

        this.center(this.uiWindow, true);

        for (let i: number = 0; i < this.buttons.length; i++) {
            this.buttons[i].gameWRatio = 0.05;
            this.grid?.placeAt(2 + i * 2, 9.7, this.buttons[i]);
        }

        this.spineChar.scaleToGameH(.2);
        if (this.spineChar.currentPos) {
          this.spineChar.placeOnGrid(this.spineChar.currentPos?.x, this.spineChar.currentPos?.y);
        }
    }

}