 /* export { SpineChar } from "./classes/SpineChar";
export { SpineTween } from "./classes/SpineTween";
export { SpineTweenObj } from "./classes/SpineTweenObj";
export {SpinePlayer} from "@esotericsoftware/spine-player";
*/
import { GameOptions, SVGGame } from "svggame"
import { ScreenMain } from "./screens/ScreenMain"; 

window.onload=()=>{
    
   

    let opts:GameOptions=new GameOptions();
    opts.useFull=true;
    opts.addAreaID="addArea";
    opts.screens=[new ScreenMain()]

    let game:SVGGame=new SVGGame("myCanvas",opts);
}
