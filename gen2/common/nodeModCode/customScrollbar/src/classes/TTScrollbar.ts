export class TTScrollbar {
    //scrollbar elements
    private scrollTarget: HTMLElement;
    public element: HTMLElement;
    public knob: HTMLElement;
    public track: HTMLElement;
    private upButton: HTMLElement;
    private downButton: HTMLElement;

    //functions used as event listeners so we can remove them later
    //and yet still use the bind(this) to keep the correct this reference

    private knobDown: Function;
    private knobUp: Function;
    private knobMove: Function;
    private trackDown: Function;
    private upButtonClicked: Function;
    private downButtonClicked: Function;
    private mouseWheelTurned: Function;

    //height of the  track and knob

    private knobHeight: number = 0;
    private trackHeight: number = 0;

    //keep track of the scroll position and the last good scroll position
    //if the user scrolls to the top or bottom of the scrollable element
    //we don't want to scroll the scrollable element any further

    public scrollValue: number = 0;
    private lastGoodPosition: number = 0;

    //increments for the up and down buttons
    public increment: number = 5;

    //the callback function to call when the scrollbar is moved
    private callback: Function;

    public debug:boolean = false;

    constructor(scrollTarget: HTMLElement, addTo: HTMLElement | null, callback: Function) {
        (window as any).ttScrollBar = this;

        this.scrollTarget = scrollTarget;
        this.callback = callback;
        this.element = document.createElement("div");
        this.element.className = "tt-scrollbar";

        console.log("scrollTarget", scrollTarget);

        //set the height of the scrollbar to the height of the scrollable element
        if (addTo) {
            this.element.style.height = addTo.clientHeight + "px";
        }


        //make the html for the scrollbar

        this.element.innerHTML = `<div class='tt-scrollbar top-button'></div>
        <div class='tt-scrollbar track'><div class="tt-scrollbar knob"></div></div>        
        <div class="tt-scrollbar bottom-button"></div>`;

        //extract the elements from the html

        this.knob = this.element.getElementsByClassName("tt-scrollbar knob")[0] as HTMLElement;
        this.track = this.element.getElementsByClassName("tt-scrollbar track")[0] as HTMLElement;
        this.upButton = this.element.getElementsByClassName("tt-scrollbar top-button")[0] as HTMLElement;
        this.downButton = this.element.getElementsByClassName("tt-scrollbar bottom-button")[0] as HTMLElement;

        //set the functions to use as event listeners

        this.knobDown = this.onKnobDown.bind(this);
        this.knobUp = this.onKnobUp.bind(this);
        this.knobMove = this.onKnobMove.bind(this);
        this.trackDown = this.onTrackDown.bind(this);
        this.upButtonClicked = this.onUpButtonClicked.bind(this);
        this.downButtonClicked = this.onDownButtonClicked.bind(this);
        this.mouseWheelTurned = this.onMouseWheel.bind(this);


        //add the scrollbar to the parent element
        if (addTo) {
            addTo.appendChild(this.element);
            this.setListeners();
        }       
        //add a listener to the target to check if the user has scrolled the target
        //and update the scrollbar position
     //  
    }
    onTargetScroll() {       
        this.knobHeight = this.knob.getBoundingClientRect().height;
        this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
        
       //convert the scroll position of the target to a scroll position for the scrollbar
       console.log("scrollTarget", this.scrollTarget);
        this.scrollValue = this.scrollTarget.scrollTop / (this.scrollTarget.scrollHeight - this.scrollTarget.clientHeight) * this.trackHeight;
        console.log("onTargetScroll", this.scrollValue);
        this.knob.style.top = this.scrollValue + "px";
    }
    setListeners() {
        //add the event listeners
        console.log("target", this.scrollTarget);

        this.removeListeners();
        this.knob.addEventListener("mousedown", this.knobDown as EventListener);
        this.downButton.addEventListener("click", this.downButtonClicked as EventListener);
        this.upButton.addEventListener("click", this.upButtonClicked as EventListener);
        this.track.addEventListener("mousedown", this.trackDown as EventListener);

        //check if device is a touch device
        if (window.ontouchstart !== undefined) {
            //alert("touch");
            this.knob.addEventListener("touchstart", this.knobDown as EventListener);
            this.downButton.addEventListener("touchstart", this.downButtonClicked as EventListener);
            this.upButton.addEventListener("touchstart", this.upButtonClicked as EventListener);
            this.track.addEventListener("touchstart", this.trackDown as EventListener);
            this.scrollTarget.addEventListener("scroll", this.onTargetScroll.bind(this));
        }
        else
        {
            this.knob.addEventListener("mousedown", this.knobDown as EventListener);
            this.downButton.addEventListener("mousedown", this.downButtonClicked as EventListener);
            this.upButton.addEventListener("mousedown", this.upButtonClicked as EventListener);
            this.track.addEventListener("mousedown", this.trackDown as EventListener);
            this.scrollTarget.addEventListener("mousewheel", this.mouseWheelTurned as EventListener);
            this.element.addEventListener("mousewheel", this.mouseWheelTurned as EventListener);
        }

       
        
         //record the height of the track and the knob
         this.knobHeight = this.knob.getBoundingClientRect().height;
         this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
    }
    removeListeners() {
        this.knob.removeEventListener("mousedown", this.knobDown as EventListener);
        document.removeEventListener("mouseup", this.knobUp as EventListener);
        document.removeEventListener("mousemove", this.knobMove as EventListener);
        this.downButton.removeEventListener("click", this.downButtonClicked as EventListener);
        this.upButton.removeEventListener("click", this.upButtonClicked as EventListener);
        this.track.removeEventListener("mousedown", this.trackDown as EventListener);
        this.scrollTarget.removeEventListener("mousewheel", this.mouseWheelTurned as EventListener);
        this.element.removeEventListener("mousewheel", this.mouseWheelTurned as EventListener);
    }
    appendTo(parent: HTMLElement) {
        this.element.style.height = parent.clientHeight + "px";
        parent.appendChild(this.element);
        this.setListeners();
    }
    prependTo(parent: HTMLElement) {
        this.element.style.height = parent.clientHeight + "px";
        parent.prepend(this.element);
        this.setListeners();
    }
    addClass(className: string) {
        this.element.classList.add(className);
    }
    removeClass(className: string) {
        this.element.classList.remove(className);
    }
    setImage(key: "btnUp" | "btnDown" | "track" | "knob", image: string) {
        // this.knob.style.backgroundImage = "url(" + image + ")";
        switch (key) {
            case "btnUp":
                this.upButton.style.backgroundImage = "url(" + image + ")";
                break;

            case "btnDown":
                this.downButton.style.backgroundImage = "url(" + image + ")";
                break;

            case "track":
                this.track.style.backgroundImage = "url(" + image + ")";
                break;

            case "knob":
                this.knob.style.backgroundImage = "url(" + image + ")";
                break;
        }
        this.knobHeight = this.knob.getBoundingClientRect().height;
        this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
    }
    onDestroy() {
        this.removeListeners();
        this.element.remove();
    }
    convertPxToPercent(px: string): number {
        //take out the px
        let pxNum = parseFloat(px.substring(0, px.length - 2));
        return pxNum / this.trackHeight * 100;
    }
    convertToPercent(value: number): number {
        return value / this.trackHeight * 100;
    }
    setScroll(scroll: number) {
        if (scroll < 0) {
            scroll = 0;
        }
        if (scroll > 100) {
            scroll = 100;
        }
        let pos: number = (this.trackHeight / 100) * scroll;
        this.knob.style.top = pos + "px";
        this.validateScrollValue();
        this.updateTarget();
    }
    onTrackDown(e: MouseEvent) {
        let pos: number = e.clientY - this.track.getBoundingClientRect().top;


        this.knob.style.top = pos + "px";
        this.validateScrollValue();
        this.updateTarget();
    }
    onUpButtonClicked(e: MouseEvent) {
        this.setScroll(this.scrollValue - this.increment);
        this.updateTarget();
    }
    onDownButtonClicked(e: MouseEvent) {
        this.setScroll(this.scrollValue + this.increment);
        this.updateTarget();
    }
    onKnobDown(e: MouseEvent) {
        e = e || window.event;
        e.preventDefault();

        this.knob.removeEventListener("mousedown", this.knobDown as EventListener);
        document.addEventListener("mouseup", this.knobUp as EventListener);
        document.addEventListener("mousemove", this.knobMove as EventListener);
    }
    onKnobUp(e: MouseEvent) {
        e = e || window.event;
        e.preventDefault();

        document.removeEventListener("mouseup", this.knobUp as EventListener);
        document.removeEventListener("mousemove", this.knobMove as EventListener);
        this.knob.addEventListener("mousedown", this.knobDown as EventListener);

        this.scrollValue = this.convertPxToPercent(this.knob.style.top);
    }
    onKnobMove(e: MouseEvent) {
        e = e || window.event;
        e.preventDefault();
        //move the knob
        this.lastGoodPosition = this.scrollValue;
        let knobPos: number = e.clientY - this.knob.offsetHeight;
        knobPos -= this.track.getBoundingClientRect().top;
        this.knob.style.top = knobPos + "px";
        // this.knob.style.top = (e.clientY - this.knob.offsetHeight) + "px";
        this.validateScrollValue();
        this.updateTarget();
    }
    onMouseWheel(e: WheelEvent) {
        e = e || window.event;
        e.preventDefault();
        if (e.deltaY > 0) {
            this.setScroll(this.scrollValue + this.increment);
        }
        else {
            this.setScroll(this.scrollValue - this.increment);
        }
        this.updateTarget();
    }

    validateScrollValue() {
        this.trackHeight = this.track.getBoundingClientRect().height - this.knobHeight;
        this.scrollValue = this.convertPxToPercent(this.knob.style.top);
        if (this.debug===true) {
            console.log("scroll value: " + this.scrollValue);
        }
     
        if (this.scrollValue < 0 || this.scrollValue > 101) {
            this.scrollValue = this.lastGoodPosition;
            this.setScroll(this.lastGoodPosition);
            return;
            // this.knob.style.top=(this.lastGoodPosition/100*this.trackHeight)+"px";
        }
        this.lastGoodPosition = this.scrollValue;
    }
    updateTarget() {
        this.callback(this.scrollValue);
    }
    setScrollbarWidthModifier(mod: number) {
        document.documentElement.style.setProperty("--scrollbarModificator", mod.toString());
    }
    addCssToHead() {

        if (document.getElementById("scrollbarStyle") == null) {
            let style = document.createElement("style");
            style.id = "scrollbarStyle";
            style.innerHTML = `html{--vw:1vw;--scrollbarModificator:2;--scrollbarTrackModificator:3;--scrollbarWidth:calc(var(--vw) * var(--scrollbarModificator));--buttonHeight:var(--scrollbarWidth);--trackWidth:calc(var(--scrollbarWidth) / var(--scrollbarTrackModificator));--thumbWidth:calc(var(--scrollbarWidth) / var(--scrollbarTrackModificator)*2)}.tt-scrollbar{width:var(--scrollbarWidth);height:10px;max-height:100%;top:0;right:0;cursor:pointer}.tt-scrollbar .track{width:var(--trackWidth);height:calc(100% - var(--buttonHeight) * 2);margin:0 calc(var(--scrollbarWidth) / 2 - var(--trackWidth) / 2);background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%;cursor:pointer}.tt-scrollbar .knob{position:relative;width:var(--thumbWidth);height:var(--thumbWidth);top:0;left:-50%;cursor:pointer;background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%}.tt-scrollbar .top-button{width:var(--scrollbarWidth);height:var(--buttonHeight);background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%;cursor:pointer}.tt-scrollbar .bottom-button{width:var(--scrollbarWidth);height:var(--buttonHeight);background-image:none;background-repeat:no-repeat;background-position:center;background-size:100% 100%;cursor:pointer}@media (min-aspect-ratio:96/55){html{--vw:calc(1vh * 96 / 55)}}`;
            document.head.appendChild(style);
        }
    }
}