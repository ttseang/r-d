export class StarBurst {
    public scene:Phaser.Scene;

    public count:number;
    public size:number;
    public dist:number;
    public duration:number;
    public maxDist;
    public f:number;
    public tint:number;
    public x:number;
    public y:number;

    constructor(scene:Phaser.Scene,x:number=0,y:number=0,size:number=5,count:number=25,dist:number=150,duration:number=1000,maxDist:number=300,f:number=0,tint:number=0xffffff) {
        this.scene =scene;
        
        this.count=count;
        this.size=size;
        this.dist=dist;
        this.duration=duration;
        this.maxDist=maxDist;
        this.f=f;
        this.tint=tint;
        this.x=x;
        this.y=y;
    }
    start()
    {
        for (let i = 0; i < this.count; i++) {
            let star = this.scene.add.sprite(this.x, this.y, "effectStars");
            //
            //
            //
            star.setTint(this.tint);
            star.setFrame(this.f);
            star.setOrigin(0.5, 0.5);
            let r = Phaser.Math.Between(50, this.maxDist);
            let s = Phaser.Math.Between(1, 100) / 100;
            star.scaleX = s;
            star.scaleY = s;
            let angle = i * (360 / this.count);
            let tx = this.x + r * Math.cos(angle);
            let ty = this.y + r * Math.sin(angle);

            this.scene.children.bringToTop(star);
            
            //  star.x=tx;
            // star.y=ty;
            this.scene.tweens.add({
                targets: star,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                    scope: this
                }]
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    static preload(scene, path) {
        scene.load.spritesheet('effectStars', path, {
            frameWidth: 25,
            frameHeight: 25
        });
    }
}