
export class ColorBurst {
    private scene: Phaser.Scene;
    public x: number;
    public y: number;
    public size:number;
    public count:number;
    public dist:number;
    public duration:number;
    public maxDist:number;
    public color:number;

    constructor(scene: Phaser.Scene, x: number = 0, y: number = 0, size: number = 5, count: number = 25, dist: number = 150, duration: number = 1000, maxDist: number = 300, color: number = 0xffffff)
    {
        this.scene = scene;
        this.x=x;
        this.y=y;
        this.size=size;
        this.count=count;
        this.dist=dist;
        this.duration=duration;
        this.maxDist=maxDist;
        this.color=color;
    }
    public start()
    {
        for (let i = 0; i < this.count; i++) {
            let star = this.scene.add.sprite(this.x, this.y, "effectColorStars");
            //
            //
            //
            let f = Phaser.Math.Between(0, 14);
            star.setFrame(f);
            star.setOrigin(0.5, 0.5);
            let r = Phaser.Math.Between(50, this.maxDist);
            let s = Phaser.Math.Between(1, 100) / 100;
            star.scaleX = s;
            star.scaleY = s;
            let angle = i * (360 / this.count);
            let tx = this.x + r * Math.cos(angle);
            let ty = this.y + r * Math.sin(angle);
           
            //  star.x=tx;
            // star.y=ty;
            this.scene.tweens.add({
                targets: star,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone.bind(this)
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    static preload(scene: Phaser.Scene, path: string) {
        scene.load.spritesheet('effectColorStars', path, {
            frameWidth: 26,
            frameHeight: 26
        });
    }
}