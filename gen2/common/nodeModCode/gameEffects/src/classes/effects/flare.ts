
export class Flare extends Phaser.GameObjects.Sprite {
    public scene: Phaser.Scene;
    public x:number;
    public y:number;
    public tint:number;
    public scale:number=.2;
    public direction:number=-1;
    public duration:number=1000;
    public gameWidth:number
    constructor(scene:Phaser.Scene,gameWidth:number, x: number = 0, y: number = 0, tint: number = 0xffffff) {

        super(scene, x, y, "flare");
        this.tint=tint;
        this.x=x;
        this.y=y;
        this.gameWidth=gameWidth;
    }
    public start()
    {   
        

        this.setTint(this.tint);

        let finalAngle = 270 * this.direction;
        this.displayWidth=this.scale*this.gameWidth;
        this.scaleY=this.scaleX;

        this.scene.add.existing(this);
        this.scene.tweens.add({
            targets: this,
            duration: this.duration,
            alpha: 0,
            angle: finalAngle,
            onComplete: this.tweenDone.bind(this)

        });
    }
    tweenDone() {
        this.destroy();
    }
    static preload(scene: Phaser.Scene, path: string) {
        scene.load.image('flare', path);
    }
}