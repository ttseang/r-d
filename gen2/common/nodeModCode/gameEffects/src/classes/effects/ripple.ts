export class Ripple {
    public scene: Phaser.Scene;
    public color: number;
    public size: number;
    public x:number;
    public y:number;
    public count:number;
    public dist:number;
    public duration:number;
    
    constructor(scene:Phaser.Scene, x: number = 0, y: number = 0, count: number = 25, size: number = 5, dist: number = 150, duration: number = 1000, color: number = 0xffffff) {
        this.scene = scene;
        this.size = size;
        //
        //
        //
        this.color = color;
        this.x=x;
        this.y=y;
        this.count=count;
        this.size=size;
        this.dist=dist;
        this.duration=duration;

    }

    public start()
    {

        for (let i = 0; i < this.count; i++) {
            let s = this.getSpark();
            s.x = this.x;
            s.y = this.y;
            //
            //
            //
            let angle = i * (360 / this.count);
            //  let r = game.rnd.integerInRange(50, 100);
            let tx = this.x + this.dist * Math.cos(angle);
            let ty = this.y + this.dist * Math.sin(angle);
            this.scene.tweens.add({
                targets: s,
                duration: this.duration,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone,
                onCompleteParams: [{
                    scope: this
                }]
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    getSpark() {
        let s = this.scene.add.graphics();
        s.fillStyle(this.color, 1);
        s.fillCircle(0, 0, this.size);
        return s;
    }
}