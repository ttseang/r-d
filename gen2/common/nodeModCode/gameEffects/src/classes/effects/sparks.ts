
export class Sparks {
    public scene:Phaser.Scene;
    public color:number;
    public size:number;
    public count:number;
    public x:number;
    public y:number;
    
    constructor(scene:Phaser.Scene,x:number=0,y:number=0,size:number=1,color:number=0xffffff,count:number=25) {
        // super(config.scene);
        this.scene=scene;
        this.color=color;
        this.size=size;
        this.count=count;
        this.x=x;
        this.y=y;
    }

    public start()
    {
        for (let i = 0; i < this.count;i++) {
            let s = this.getSpark();
            s.x = this.x;
            s.y = this.y;
            //   this.add(s);
            let angle = i * (360 / this.count);
            let r = Phaser.Math.Between(50, 100);
            let tx = this.x + r * Math.cos(angle);
            let ty = this.y + r * Math.sin(angle);
            this.scene.tweens.add({
                targets: s,
                duration: 1000,
                alpha: 0,
                y: ty,
                x: tx,
                onComplete: this.tweenDone.bind(this)
            });
        }
    }
    tweenDone(tween, targets, custom) {
        targets[0].destroy();
    }
    getSpark() {
        let s = this.scene.add.graphics();
        s.fillStyle(this.color, 1);
        s.fillCircle(0, 0, this.size);
        return s;
    }
}