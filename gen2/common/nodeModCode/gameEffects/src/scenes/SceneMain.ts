import { ColorBurst } from "../classes/effects/colorBurst";
import { Flare } from "../classes/effects/flare";
import { Ripple } from "../classes/effects/ripple";
import { Sparks } from "../classes/effects/sparks";
import { StarBurst } from "../classes/effects/starBurst";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        StarBurst.preload(this,"./assets/effects/stars.png");
        ColorBurst.preload(this,"./assets/effects/colorStars.png");
        Flare.preload(this,"./assets/effects/flare.png");

    }
    create() {
        super.create();
        this.makeGrid(11,11);

        /* let sb:StarBurst=new StarBurst(this,100,100);
        sb.start(); */

       /*  let cb:ColorBurst=new ColorBurst(this,100,100);
        cb.start(); */

        /* let fl:Flare=new Flare(this,100,100);
        fl.start(); */

      /*   let ripple:Ripple=new Ripple(this,100,100);
        ripple.start(); */

        let sparks:Sparks=new Sparks(this,100,100);
        sparks.color=0xff0000;
        sparks.start();
    }
}