import { PosVo, SvgObj } from "svggame";

export interface IDragObj
{
    type:string;
    elID:string;
    el:HTMLElement | null;
    canDrag:boolean;
    visible:boolean;
    alpha:number;
    displayWidth:number;
    displayHeight:number;
    scaleX:number;
    scaleY:number;
    x:number;
    y:number;
    destroy():void;
    content:string;
    original:PosVo;
    place:PosVo;
    resetPlace:PosVo;    
    snapBack():void;
    initPlace(pos:PosVo):void;
    setPlace(pos:PosVo):void;
    setContent(content:string):void;
    getContainer():SvgObj
    doResize():void;
    updateScale():void;
}