import { BaseScreen, PosVo, SvgObj } from "svggame"
import { Box } from "../classes/Box";
import { DragEng } from "../classes/DragEng";
import { TargBox } from "../classes/TargBox";
import { IDragObj } from "../interfaces/IDragObj";
import { IDragTarg } from "../interfaces/IDragTarg";

export class ScreenMain extends BaseScreen {

    private dragEng!: DragEng;

    constructor() {
        super("ScreenMain");
        (window as any).scene = this;
    }

    create() {

        // this.grid?.showGrid();

        //make the engine and set options
        this.dragEng = new DragEng(this);
        this.dragEng.snapToTarget = true;

        this.dragEng.collideCallback = this.onCollide.bind(this);
        this.dragEng.upCallback = this.onUp.bind(this);
        this.dragEng.dropCallback = this.onDrop.bind(this);

        //make drop target boxes

        for (let i: number = 0; i < 4; i++) {
            let tBox: TargBox = new TargBox(this);
            tBox.gameWRatio=0.08;
            //place on grid
            tBox.setPlace(new PosVo(3 + i * 2, 2));

            //add target to the engine
            this.dragEng.addTarget(tBox);
        }

        //test word
        let word: string = "fish";

        for (let i: number = 0; i < 4; i++) {
            let box: Box = new Box(this);
            box.setText(word.substr(i, 1));

            //scale
            box.gameWRatio=0.05;
            /* box.displayWidth = this.grid!.cw;
            box.displayHeight = this.grid!.ch; */

            //place on grid
            box.initPlace(new PosVo(3 + i * 2, 1));

            //add the drag box the engine
            this.dragEng.addDrag(box);
        }

        this.dragEng.mixUpBoxes();

        //activate the built in listeners
        this.dragEng.setListeners();

    }
    doResize() {
        console.log("RESIZE");
        //place custom resize here
        super.doResize();
        this.grid.showGrid();
    }
    onCollide(d: IDragObj, t: IDragTarg) {
        console.log("collide");
        d.canDrag = false;
    }
    onDrop() {
        console.log("dropped");
        console.log(this.dragEng.getTargetContent());
        //check drop content here
    }
    onUp() {
        console.log("up");
    }
}