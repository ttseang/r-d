
export { BaseDragObj } from "./classes/BaseDragObj";
export { BaseTargObj } from "./classes/BaseTargObj";
export { DragEng } from "./classes/DragEng";
export {IDragObj} from "./interfaces/IDragObj"
export {IDragTarg} from "./interfaces/IDragTarg";


/* import { GameOptions, SVGGame } from "svggame"
import { ScreenMain } from "./screens/ScreenMain";

window.onload=()=>{

    let opts:GameOptions=new GameOptions();
    opts.useFull=true;
    opts.addAreaID="addArea";
    opts.screens=[new ScreenMain()]

    let game:SVGGame=new SVGGame("myCanvas",opts);
} */
