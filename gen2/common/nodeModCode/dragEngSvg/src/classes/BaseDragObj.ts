


import { SvgObj,PosVo,IScreen } from "C:/Users/USER/TT/r-d/gen2/common/mods/svgGame";
import { IDragObj } from "../interfaces/IDragObj";

export class BaseDragObj extends SvgObj implements IDragObj {
   public type: string = "";
   public canDrag: boolean = true;
   public content: string = "";
   public original: PosVo = new PosVo(0, 0);
   public place: PosVo = new PosVo(0, 0);
   public resetPlace: PosVo = new PosVo(0, 0);

   constructor(screen: IScreen, key: string) {
      super(screen, key);

   }

   doResize() {
      this.screen.grid?.placeAt(this.place.x, this.place.y, this);
   }
   snapBack(): void {
      // throw new Error("Method not implemented.");
      this.canDrag = false;
      this.screen.tweenGridPos(this.original.x, this.original.y, this, 300, () => { this.snapDone() });
   }
   private snapDone() {
      this.canDrag = true;
   }
   initPlace(pos: PosVo) {
      this.original = pos;
      this.place = pos;
      this.screen.grid?.placeAt(pos.x, pos.y, this);
   }
   setPlace(pos: PosVo): void {
      this.place = pos;
      this.screen.grid?.placeAt(pos.x, pos.y, this);
   }
   setContent(content: string): void {
      this.content = content;
   }
   getContainer(): SvgObj {
      return this as SvgObj;
   }
}