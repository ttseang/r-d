import { CollideUtil, GameManager, IGameObj, IScreen, PosVo, SVGUtil } from "C:/Users/USER/TT/r-d/gen2/common/mods/svgGame";
import { IDragObj } from "../interfaces/IDragObj";
import { IDragTarg } from "../interfaces/IDragTarg";
import { Box } from "./Box";

export class DragEng {

    public targets: IDragTarg[] = [];
    public dragObjs: IDragObj[] = [];
    public clickLock: boolean = false;
    private dragBox: IDragObj | null = null;
    public lb: IDragObj | null = null;

    public downCallback:Function=()=>{};
    public upCallback: Function = () => { };
    public dropCallback: Function = () => { };
    public collideCallback: Function = () => { }
    public snapBackOnContent: boolean = true;
    public setContentOnCollide:boolean=true;

    private bscreen: IScreen;
    private gm: GameManager = GameManager.getInstance();

    private dragMap: Map<string, IDragObj> = new Map<string, IDragObj>();
    public snapToTarget: boolean = false;

    public constructor(bscreen: IScreen) {
        this.bscreen = bscreen;
    }
    public addDrag(dragObj: IDragObj) {
        dragObj.type = "dragObj";
        this.lb = dragObj;
        // console.log(dragObj.elID);
        this.dragMap.set(dragObj.elID, dragObj);
        this.dragObjs.push(dragObj);
    }
    public addTarget(target: IDragTarg) {
        this.targets.push(target);
    }
    public destoryDrags() {
        for (let i: number = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].getContainer().destroy();
        }
        this.dragObjs=[];
    }
    public destroyTargs() {
        for (let i: number = 0; i < this.targets.length; i++) {
            this.targets[i].getContainer().destroy();
        }
        this.targets=[];
    }
    doResize() {
        for (let i: number = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].doResize();
        }
        for (let i: number = 0; i < this.targets.length; i++) {
            this.targets[i].doResize();
        }
    }

    public setListeners() {
        document.onpointerdown = this.onDown.bind(this);
        document.onpointermove = this.onPointerMove.bind(this);
        document.onpointerup = this.onUp.bind(this);
    }
    public mixUpBoxes() {


        for (let i: number = 0; i < 10; i++) {
            let r1: number = Math.floor(Math.random() * this.dragObjs.length);
            let r2: number = Math.floor(Math.random() * this.dragObjs.length);

            if (r1 != r2) {
                let box1: IDragObj = this.dragObjs[r1];
                let box2: IDragObj = this.dragObjs[r2];


                let place1: PosVo = box1.place;
                let place2: PosVo = box2.place;

                box1.initPlace(place2);
                box2.initPlace(place1);
            }

            /*  box1.place=place2;
             box2.place=place1;
  
             box1.original=place2;
             box2.original=place1; */
        }
    }
    public onDown(p: PointerEvent) {

        if (this.clickLock == true) {
            return;
        }
        let target: EventTarget | null = p.target;

        if (target) {



            let top: HTMLElement | null | undefined = SVGUtil.findTop(target as HTMLElement, "g");

            if (top) {



                //top.parentElement?.appendChild(top);

                let targID: string = top.id;
                //console.log(targID);



                let obj: IDragObj | undefined = this.dragMap.get(targID);

                if (obj?.canDrag == false) {
                    return;
                }
                (window as any).dt = obj;

                //   console.log(obj);
                if (obj) {
                    this.dragBox = obj;
                }
                this.downCallback(obj);
                //bring to top
                //top.setAttribute("zIndex","1000");
                /*  let addArea:HTMLElement|null=document.getElementById(this.gm.gameOptions.addAreaID);
                 if (addArea)
                 {
                     
                    // addArea.appendChild(top);
                 } */

            }
        }
        /* if (dragObj.type!="dragObj")
        {
            return;
        }
        this.dragBox = dragObj;
        this.dragBox.resetPlace = new PosVo(this.dragBox.x, this.dragBox.y); */

        // this.scene.children.bringToTop(dragObj.getContainer());
    }
    public onPointerMove(p: PointerEvent) {
        if (this.dragBox) {
            this.dragBox.x = p.clientX - this.dragBox.displayWidth / 2;
            this.dragBox.y = p.clientY - this.dragBox.displayHeight / 2;
        }
    }
    public onUp() {

        let hit: boolean = this.checkTargets();
        if (hit == false) {
            if (this.dragBox) {
                console.log("snap 1");
                this.dragBox.snapBack();
            }
        }
        else {
            if (this.dragBox && this.bscreen) {
                if (this.bscreen.grid) {
                    let place: PosVo = this.bscreen.grid.findNearestGridXYDec(this.dragBox.x, this.dragBox.y);
                    this.dragBox.setPlace(place);
                }
            }


            this.dropCallback();
        }

        this.dragBox = null;
    }
    public resetBoxes() {
        for (let i: number = 0; i < this.dragObjs.length; i++) {
            this.dragObjs[i].setPlace(this.dragObjs[i].original);
            this.dragObjs[i].canDrag = true;
        }
        for (let i: number = 0; i < this.targets.length; i++) {
            this.targets[i].content = "";
        }
    }
    public getTargetContent() {
        let content: string = "";
        for (let i: number = 0; i < this.targets.length; i++) {
            content += this.targets[i].content;
        }
        return content;
    }
    public checkTargets() {
        if (this.dragBox === null) {
            return false;
        }

        for (let i: number = 0; i < this.targets.length; i++) {
            let targetBox: IDragTarg = this.targets[i];



            if (CollideUtil.checkOverlap(this.dragBox.getContainer(), targetBox.getContainer())) {

                this.dragBox.setPlace(targetBox.place);

                if (this.snapBackOnContent == true) {
                    if (targetBox.content != "") {
                        console.log("snap 2");
                        this.dragBox.snapBack();
                        return;
                    }
                }

                if (this.snapToTarget) {
                    this.dragBox.x = targetBox.x + targetBox.displayWidth / 2 - this.dragBox.displayWidth / 2;
                    this.dragBox.y = targetBox.y + targetBox.displayHeight / 2 - this.dragBox.displayHeight / 2;
                }
                //  this.chosenContent = this.dragBox.content;
                if (this.setContentOnCollide==true)
                {
                    targetBox.content = this.dragBox.content;
                }
                
                this.collideCallback(this.dragBox, targetBox);
                // this.clickLock = true;
                return true;
            }
        }
        return false;
    }

}