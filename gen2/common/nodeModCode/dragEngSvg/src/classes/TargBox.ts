import { IScreen } from "svggame";
import { IDragTarg } from "../interfaces/IDragTarg";
import { BaseTargObj } from "./BaseTargObj";

export class TargBox extends BaseTargObj implements IDragTarg
{
    private textEl:SVGTextElement |null=null;
    public text:string="";
    constructor(iScreen:IScreen)
    {
        super(iScreen,"targetBox");
    }    
    
    doResize()
    {
        this.updateScale();
        super.doResize();
    }
}