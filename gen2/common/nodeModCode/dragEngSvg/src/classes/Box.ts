import { IScreen, SvgObj } from "svggame";
import { IDragObj } from "../interfaces/IDragObj";
import { BaseDragObj } from "./BaseDragObj";

export class Box extends BaseDragObj implements IDragObj
{
    private textEl:SVGTextElement |null=null;
    public text:string="";
    constructor(iScreen:IScreen)
    {
        super(iScreen,"wordBox");
    }

    setText(text:string)
    {
        this.text=text;
        this.content=text;
        if (this.el)
        {
            this.textEl=this.el.getElementsByTagName("text")[0];
            
            this.textEl.innerHTML=text;
        }
    }
    doResize()
    {
        this.updateScale();
        super.doResize();
    }
}