import { IScreen, PosVo, SvgObj } from "C:/Users/USER/TT/r-d/gen2/common/mods/svgGame"
import { IDragTarg } from "../interfaces/IDragTarg";

export class BaseTargObj extends SvgObj implements IDragTarg
{
    public correct: string="";
    public original: PosVo=new PosVo(0,0);
    public place: PosVo=new PosVo(0,0);
    public content: string="";
    public color:number=0;

    constructor(screen:IScreen,key:string)
    {
        super(screen,key);
    }
    setColor(color: number): void {
        this.color=color;
    }
   
    setPlace(pos: PosVo): void {
        this.place=pos;
        this.screen.grid?.placeAt(pos.x,pos.y,this);
    }
    
    setContent(content: string): void {
        this.content=content;
    }
    getContainer(): SvgObj {
        return this as SvgObj;
    }
    doResize()
    {
        this.screen.grid?.placeAt(this.place.x,this.place.y,this);
    }
}