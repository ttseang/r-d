export class MPVo
{
    public bytes:number;
    public dur:number;
    public etag:string;
    public id:number;
    public path:string;

    constructor(bytes:number, dur:number, etag:string, id:number, path:string)
    {
        this.bytes=bytes;
        this.dur=dur;
        this.etag=etag;
        this.id=id;
        this.path=path;
    }
}