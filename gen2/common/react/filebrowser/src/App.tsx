import React from 'react';

import './App.css';
import SoundMediaBrowser from './filebrowser/screens/SoundMediaBrowser';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MediaSoundVo } from './filebrowser/dataObjs/MediaSoundVo';

function App() {
  return (
    <SoundMediaBrowser key="soundbrowser" tag='TT.RD.BM' path={'https://ttv5.s3.amazonaws.com/'} cancelCallback={()=>{alert("cancel")}} callback={(sound:MediaSoundVo)=>{console.log(sound.title)}}></SoundMediaBrowser>
  );
}

export default App;
