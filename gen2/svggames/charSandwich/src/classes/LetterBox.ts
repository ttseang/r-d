import { BaseDragObj, IDragObj } from "dragengsvg";
import { IScreen, SvgObj } from "svggame";

export class LetterBox extends SvgObj {
    public text: string = "";
    private textEl: SVGTextElement | null = null;

    constructor(scene: IScreen,addArea:string="") {
        super(scene, "letterbox",addArea);
        // this.setContent("B");
    }
    setText(text: string) {
        this.text = text;
      
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            this.textEl.innerHTML = text;
        }
    }
    doResize() {
        this.updateScale();
        super.doResize();
    }
}