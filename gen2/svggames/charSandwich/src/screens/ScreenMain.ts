
import { BaseScreen, IScreen, PosVo, SvgObj, TextObj } from "svggame";
import { SpineChar } from "../classes/char/SpineChar";

import { IconButton } from "../classes/IconButton";
import { LetterBox } from "../classes/LetterBox";
import { AnimationVo } from "../dataObjs/AnimationVo";





export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private uiWindow!: SvgObj;
    private btnCheck!: SvgObj;
    private btnPlay!: SvgObj;
    private btnReset!: SvgObj;
    private btnHelp!: SvgObj;

    private buttons: SvgObj[] = [];
    private instructionText: TextObj | null = null;
  
    

    
    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

  

    
    private idleAnimations: AnimationVo[] = [];
    private walkAnimations: AnimationVo[] = [];
    private fidgetAnimations: AnimationVo[] = [];

    private spineChar!: SpineChar;
 
    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);


       
        

        

    }
    create() {
        super.create();

        (window as any).scene = this;

        this.uiWindow = new SvgObj(this, "uiWindow");
        this.uiWindow.gameWRatio = .9;

        let scale: number = this.gw / 4000


        this.btnPlay = new IconButton(this, "btnPlay", "play", "", this.onbuttonPressed.bind(this));
        this.btnReset = new IconButton(this, "btnReset", "reset", "", this.onbuttonPressed.bind(this));
        this.btnCheck = new IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
        this.btnHelp = new IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));

        this.buttons = [this.btnHelp, this.btnPlay, this.btnReset, this.btnCheck];


        this.instructionText = new TextObj(this, "instText");


        let word="Amka!"
        for (let i:number=0;i<5;i++)
        {
            let addArea:string="addArea";
            if (i/2===Math.floor(i/2))
            {
                addArea="topBun";
            }
            let letterBox:LetterBox=new LetterBox(this,addArea);
            letterBox.setText(word.substr(i,1));
            this.placeOnGrid(i*1.1+2,5,letterBox);
        }


        this.idleAnimations = [new AnimationVo("idle", true), new AnimationVo("Repeating animations/blink", true)];
        this.walkAnimations = [new AnimationVo("walk", true), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations = [new AnimationVo("fidget01", false), new AnimationVo("Repeating animations/blink", true)];

        this.spineChar = new SpineChar(this, "char", "./assets/char/Amka.json", "./assets/char/Amka.atlas", this.idleAnimations);
        this.spineChar.currentPos = new PosVo(2, 6);
        this.spineChar.scaleToGameH(.2);

        setTimeout(() => {
          //  this.spineChar.moveTo(1200,this.spineChar.y,10000,this.walkAnimations,this.idleAnimations,true);
            this.spineChar.moveOnGrid(8,6,4000,this.walkAnimations,this.idleAnimations,true);
        }, 2000);

    }



    onbuttonPressed(action: string, params: string) {
        console.log(action);
        /*  switch (action) {
             case "reset":
                 this.dragEng.clickLock = false;
                 this.dragEng.resetBoxes();
                 break;
 
             case "check":
                 this.checkCorrect();
                 break;
         } */
    }

    doResize() {
        super.doResize();


        // this.grid?.showGrid();


        this.uiWindow.displayWidth = this.gw * 0.9;
        this.uiWindow.displayHeight = this.gh * 0.70;

        this.center(this.uiWindow, true);

        for (let i: number = 0; i < this.buttons.length; i++) {
            this.buttons[i].gameWRatio = 0.05;
            this.grid?.placeAt(2 + i * 2, 9.7, this.buttons[i]);
        }

        this.spineChar.scaleToGameW(.1);
        if (this.spineChar.currentPos)
        {
            this.spineChar.placeOnGrid(this.spineChar.currentPos?.x,this.spineChar.currentPos?.y);
        }
        

    }

}