export class ArrowVo
{
    //location of the arrow on the stage
    public x:number=0;
    public y:number=0;

    //start of the arrow line - almost always 0,0
    public x1: number = 0;
    public y1: number = 0;

    //end of the arrow
    public x2: number = 400;
    public y2: number = 0;

    //curve position
    public cx: number = 200;
    public cy: number = -150;

    public fill: string;
    public message:string="";
    public messagePos:number=0;

    public endMark:string;
    public startMark:string;

    public animateSpeed:number=2;

    constructor(x:number=0,y:number=0,x1:number=0,y1:number=0,x2:number=0,y2:number=0,cx:number=0,cy:number=0,fill:string="black",startMark:string="",endMark:string="")
    {
        this.x=x;
        this.y=y;

        this.x1=x1;
        this.y1=y1;
        this.x2=x2;
        this.y2=y2;
        this.cx=cx;
        this.cy=cy;
        this.fill=fill;
        this.startMark=startMark;
        this.endMark=endMark;
    }
    fromObj(obj:any)
    {
        this.x=parseFloat(obj.x);
        this.y=parseFloat(obj.y);

        this.x1=parseFloat(obj.x1);
        this.y1=parseFloat(obj.y1);

        this.x2=parseFloat(obj.x2);
        this.y2=parseFloat(obj.y2);

        this.cx=parseFloat(obj.cx);
        this.cy=parseFloat(obj.cy);

        this.fill=obj.fill;
        this.message=obj.message;
        this.messagePos=parseFloat(obj.messagePos);

        this.endMark=obj.endMark;
        this.startMark=obj.startMark;

        this.animateSpeed=parseFloat(obj.animateSpeed);
    }
}