export class PointerArrow
{
    //the starting point of the arrow
    //almost always 0,0
    public x1: number = 0;
    public y1: number = 0;

    //relative end point of arrow
    public x2: number = 400;
    public y2: number = 0;

    //point of the curve
    public cx: number = 200;
    public cy: number = -150;

    //drawing arrow speed
    public animateSpeed: number = 2;

    //element for the drawing head of the arrow
    private tp: SVGTextPathElement | null = null;

    //message for the arrow
    private messagePath: SVGTextPathElement | null = null;

    //the length and height of the arrow
    public len: number = 0;
    public h: number = 0;

  

    //color of the arrow
    public fill: string = "black";

    //color of the arrow to restore when mouseout
    public lastFill: string = "black";


    public strokeWidth: number = 2;

    //classes for the end and start markers
    public startClass: string = "";
    public endClass: string = "";

    //used for setting controls in the probpox
    public endIndex: number = 0;
    public colorIndex: number = 0;

    //element where we draw the line
    public lineElement: SVGGeometryElement | null = null;

    //mask - element where dots are added to reveal the line
    public maskArea: SVGMaskElement | null = null;

    //text label for the arrow
    public message: string = "";
    //percentage for the message
    public messagePos: number = 0;

    //permited classes - helps with marker color
    private colorClasses: string[] = ['red', 'blue', 'black', 'yellow'];

    //the SVG definition tag
    private defs: SVGDefsElement;

    //record ccw,cw or straight. Used for controls
    public curvePosition: string = "";

    //the current position of the line drawn
    private drawLineStep: number = 0;

    //time in miliseconds
    private drawLineSpeed: number = 1000;

    //the number of units to draw with each step
    private drawLineInc: number = 1;

    private element:SVGSVGElement;

    constructor(templateID:string,element:SVGSVGElement)
    {
        this.element=element;
        this.defs=document.getElementById("svgArrowDefs") as unknown as SVGDefsElement;

    }
    public makeDString() {
        let dstring: string = "M" + this.x1.toString() + "," + this.y1.toString() + " Q" + this.cx.toString() + "," + this.cy.toString() + " " + this.x2.toString() + "," + this.y2.toString();
        console.log(dstring);

        if (this.lineElement) {
            this.lineElement.setAttribute("stroke-width", this.strokeWidth.toString() + "px");
            this.lineElement.setAttribute("d", dstring);
        }
    }
}