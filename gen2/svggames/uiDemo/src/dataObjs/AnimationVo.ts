export class AnimationVo
{
    public name:string;
    public loop:boolean;

    constructor(name:string,loop:boolean)
    {
        this.name=name;
        this.loop=loop;
    }
}