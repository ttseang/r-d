
import { BaseScreen, GameManager, IScreen, Math2, PosVo, SvgObj, TextObj } from "svggame";
import { DragEng, IDragObj, IDragTarg } from "dragengsvg";
import { IconButton } from "../classes/IconButton";
import { LetterBox } from "../classes/LetterBox";
import { WordVo } from "../dataObjs/WordVo";
import { TargetBox } from "../classes/TargetBox";

import { SpineChar } from "../classes/SpineChar";
import { AnimationVo } from "../dataObjs/AnimationVo";
import { HintBox } from "../classes/comps/HintBox";
import { InputBox } from "../classes/comps/InputBox";
import { SvgTextComp } from "../classes/comps/SvgTextComp";
import { SvgComp } from "../classes/comps/SvgComp";
import { MsgBox } from "../classes/comps/MsgBox";


export class ScreenMain extends BaseScreen implements IScreen {
  private box!: SvgObj;


  private uiWindow!: SvgObj;
  private btnCheck!: IconButton;
  private btnHome!: IconButton;
  private btnList!: IconButton;
  private btnHelp!: IconButton;

  private buttons: IconButton[] = [];
  private inputBox!: InputBox;

  private hintBox!: HintBox;
  private text1!: SvgTextComp;
  private text2!: SvgTextComp;

  private btnTest!: SvgObj;
  private msgBox!: MsgBox;

  private spineChar!: SpineChar;
  private idleAnimations: AnimationVo[] = [];
  private walkAnimations: AnimationVo[] = [];
  private fidgetAnimations: AnimationVo[] = [];

  constructor() {
    super("ScreenMain");
    this.gm.regFontSize("instructions", 26, 1000);
    this.gm.regFontSize("story", 20, 1200);
    setTimeout(() => {
      //this.spineChar.moveX(-10,walkAnimations,charAnimations,3000);
    }, 2000);

  }
  create() {
    super.create();
    //  this.grid?.showGrid();

    this.idleAnimations = [new AnimationVo("idle", true), new AnimationVo("Repeating animations/blink", true)];
    this.walkAnimations = [new AnimationVo("walk", true), new AnimationVo("Repeating animations/blink", true)];
    this.fidgetAnimations = [new AnimationVo("fidget01", false), new AnimationVo("Repeating animations/blink", true)];


    this.spineChar = new SpineChar(this, "char", "./assets/char/Amka.json", "./assets/char/Amka.atlas", this.idleAnimations);
    this.spineChar.flip = false;
    /*   this.spineChar.scaleX=.05;
      this.spineChar.scaleY=.05; */
    this.spineChar.startPos = new PosVo(2, 10);
    this.spineChar.scaleToGameH(.2);
    //this.spineChar.setPerPos(50,50);
    //  this.spineChar.adjustY();
    //  this.spineChar.x=-3500;
    //  this.spineChar.y=-400;
    //   this.spineChar.setPos(new PosVo(-110,-30));


    (window as any).scene = this;

    this.uiWindow = new SvgObj(this, "uiWindow");
    this.uiWindow.gameWRatio = .9;

    // this.spineChar.setPos(new PosVo(4,4));

    let scale: number = this.gw / 4000;


    this.btnHome = new IconButton(this, "btnHome", "play", "", this.onbuttonPressed.bind(this));
    this.btnHome.setPos(new PosVo(1.67, 0.5));
    this.btnList = new IconButton(this, "btnList", "reset", "", this.onbuttonPressed.bind(this));
    this.btnList.setPos(new PosVo(9, 0.5));

    this.btnCheck = new IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
    this.btnHelp = new IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));

    //saving to use later
    this.btnCheck.visible = false;
    this.btnHelp.visible = false;

    this.buttons = [this.btnHelp, this.btnHome, this.btnList, this.btnCheck];

    this.inputBox = new InputBox(this);


    let yy: number = 4;
    let xx: number = 3;
    this.inputBox.setPos(new PosVo(xx + 1.9, yy));
    this.inputBox.gameWRatio = .125;
    //  this.inputBox.visible=false;
    // this.center(this.inputBox,true);

    this.text1 = new SvgTextComp(this, "blanktext");
    this.text1.setPos(new PosVo(xx, yy));
    this.text1.setText("In the story there were 2");
    this.text1.setFontKey("story");
    // this.text1.gameWRatio=.15;

    this.text2 = new SvgTextComp(this, "blanktext");
    this.text2.setPos(new PosVo(xx + 3.45, yy));
    this.text2.setText("in the forest (fox)");
    this.text2.setFontKey("story");
    //this.text2.gameWRatio=.1;

    this.hintBox = new HintBox(this);

    this.uiWindow.updateScale();

    //   this.makeBlank("In the story there were 2 <box> in the forest (fox)");

    this.hintBox.setText("Plural means more than\n  one So change <tspan fill='blue'>fox</tspan> to more\n than one");

    this.hintBox.visible = false;


    //  this.btnTest=new SvgObj(this,"longButton");

    this.msgBox = new MsgBox(this);
    this.msgBox.setText("Not Quite, but you can try again!");
    this.msgBox.setButtonText(0, "Try Again");
    this.msgBox.setButtonText(1, "See The Answer");
    this.msgBox.setPos(new PosVo(0, 85));

    this.msgBox.visible = false;



  }



  onbuttonPressed(action: string, params: string) {
    //console.log(action);
    switch (action) {
      case "play":
        this.hintBox.visible = true;
        break;

      case "reset":
        this.msgBox.visible = true;
        break;
    }
  }

  doResize() {
    super.doResize();


    //   this.grid?.showGrid();


    this.uiWindow.displayWidth = this.gw * 0.70;
    this.uiWindow.displayHeight = this.gh * 0.70;

    this.center(this.uiWindow, true);

    for (let i: number = 0; i < this.buttons.length; i++) {
      this.buttons[i].gameWRatio = 0.035;
      this.buttons[i].updatePos();
      //this.grid?.placeAt(2 + i * 2, 9.7, this.buttons[i]);
    }


    this.text1.updatePos();
    this.text2.updatePos();
    this.text1.doResize();
    this.text2.doResize();
    this.inputBox.doResize();
    this.inputBox.updatePos();
    this.inputBox.adjustY();
    this.center(this.msgBox, true);
    this.msgBox.updatePos();
    //  this.spineChar.reset();
    this.spineChar.scaleToGameH(.2);
    if (this.spineChar.startPos) {
      this.spineChar.placeOnGrid(this.spineChar.startPos?.x, this.spineChar.startPos?.y);
    }

    //this.spineChar.adjustY();
    //  this.spineChar.updatePos();
    //this.center(this.inputBox,true);
    // this.hintBox.doResize();
  }

}