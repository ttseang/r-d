import { IScreen } from "svggame";
import { SvgComp } from "./SvgComp";

export class InputBox extends SvgComp
{
    private input:HTMLInputElement;
    private back:SVGRectElement;

    constructor(bscene:IScreen)
    {
        super(bscene,"inputBox");

        this.input=this.el?.getElementsByTagName('input')[0] as HTMLInputElement;

        this.back=this.el?.getElementsByTagName('rect')[0] as SVGRectElement;

       (window as any).box=this;

    }
    doResize()
    {
        if (this.gameWRatio!=-1)
        {
            this.displayWidth=this.scene.gw*this.gameWRatio;
            this.scaleY=this.scaleX;
        }
        else
        {
            if (this.gameHRatio!=-1)
            {
                this.displayHeight=this.scene.gh*this.gameHRatio;
                this.scaleX=this.scaleY;
            }
        }
    }
    adjustY()
    {
        this.y-=this.displayHeight/2;
    }
}