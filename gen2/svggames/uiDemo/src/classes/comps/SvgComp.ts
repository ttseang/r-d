import { IScreen, PosVo, SvgObj } from "svggame";

export class SvgComp extends SvgObj
{
    public posVo:PosVo=new PosVo(0,0);
    public scene:IScreen;

    constructor(scene:IScreen,id:string)
    {
        super(scene,id);
        this.scene=scene;
    }
    public setPos(posVo:PosVo)
    {
        this.posVo=posVo;
        this.scene.grid?.placeAt(posVo.x,posVo.y,this);
    }
    updatePos()
    {
        this.scene.grid?.placeAt(this.posVo.x,this.posVo.y,this);
    }
}