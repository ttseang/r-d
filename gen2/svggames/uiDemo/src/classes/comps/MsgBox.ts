import { IScreen } from "svggame";
import { SvgComp } from "./SvgComp";

export class MsgBox extends SvgComp {
    private textEls: HTMLCollectionOf<SVGTextElement> | null = null;
    public text: string = "";
    public button1: HTMLElement | null = null;
    public button2: HTMLElement | null = null;


    constructor(scene: IScreen) {
        super(scene, "msgBox");
        this.scene = scene;
        if (this.el) {
            let buttons: HTMLCollectionOf<Element> = this.el.getElementsByClassName("msgBoxButton");

            if (buttons.length > 0) {
                this.button1 = buttons[0] as HTMLElement;
                this.button1.onclick = () => {
                    this.onPress(1);
                }
            }
            if (buttons.length > 1) {
                this.button2 = buttons[1] as HTMLElement;
                this.button2.onclick = () => {
                    this.onPress(2);
                }
            }

        }
    }
    onPress(index: number) {
        switch (index) {
            case 1:
                this.visible = false;
                break;

            case 2:
                alert("Show the answer");
                break;
        }
    }
    setText(text: string) {
        this.text = text;
        this.visible = true;
        //console.log(text);

        let textArray: string[] = text.split("\n");

        if (this.el) {
            this.textEls = this.el.getElementsByTagName("text");
            for (let i: number = 0; i < textArray.length; i++) {
                this.textEls[i + 1].innerHTML = textArray[i];
            }
        }
    }
    setButtonText(index:number,text:string)
    {
        if (!this.button1)
        {
            //console.log("no button");
            return;
        }
      /*   if (index==1 && !this.button2)
        {
            return;
        } */

        let button:HTMLElement=this.button1;
        //console.log(button);

        if (index==1)
        {
            if (this.button2)
            {
                button=this.button2;
            }           
            else
            {
                return;
            }
        }

        let textEl:SVGTextElement=button.getElementsByTagName("text")[0];
        if (textEl)
        {
            textEl.innerHTML=text;
        }
    }

}