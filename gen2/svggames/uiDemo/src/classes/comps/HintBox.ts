import { IScreen, SvgObj } from "svggame";

export class HintBox extends SvgObj
{
    private bscene:IScreen;
    public text: string = "";
    private textEls: HTMLCollectionOf<SVGTextElement> | null=null;
    private btnClose:HTMLElement | null=null;

    constructor(bscene:IScreen)
    {
        super(bscene,'hintbox');
        this.bscene=bscene;       
        this.visible=false;
        if (this.el)
        {
            this.btnClose=this.el.getElementsByClassName("btnClose")[0] as HTMLElement;
            //console.log(this.btnClose);

            if (this.btnClose)
            {
                (window as any).btnClose=this.btnClose;

                this.btnClose.onclick=()=>{
                    //alert("click");
                    this.visible=false;
                }
            }
          
        }
    }
    setText(text: string) {
        this.text = text;
        this.visible=true;
        //console.log(text);

        let textArray:string[]=text.split("\n");

        if (this.el) {
            this.textEls = this.el.getElementsByTagName("text");
            for (let i:number=0;i<textArray.length;i++)
            {
                this.textEls[i+1].innerHTML = textArray[i];
            }            
        }
    }
    doResize()
    {
       /*  let back:SvgObj=new SvgObj(this.bscene,"hintBack");
        back.displayWidth=this.bscene.gw*0.4;
        back.displayHeight=this.bscene.gh*0.4; */
        this.bscene.center(this,true);
      //  this.displayHeight=this.bscene.gh*0.4;
     //   this.displayWidth=this.bscene.gw;
      //  this.x=this.bscene.gw/2-this.displayWidth;
      //  this.y=this.bscene.gh/2-this.displayHeight/2;

        super.doResize();

        
       // this.adjust();
        //
    }
}