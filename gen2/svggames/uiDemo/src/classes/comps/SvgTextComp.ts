import { GameManager, IScreen, PosVo, SvgObj, TextObj } from "svggame";

export class SvgTextComp extends TextObj
{
    public posVo:PosVo=new PosVo(0,0);
    public scene:IScreen;
    public fontKey:string="";
    
    
    constructor(scene:IScreen,id:string)
    {
        super(scene,id);
        this.scene=scene;
    }
    public setFontKey(key:string)
    {
       let gm:GameManager=GameManager.getInstance();
        this.fontKey=key;
        this.doResize();
    }
    public setPos(posVo:PosVo)
    {
        this.posVo=posVo;
        this.scene.grid?.placeAt(posVo.x,posVo.y,this);
    }
    public doResize()
    {
        let gm:GameManager=GameManager.getInstance();
        
        let fs:number=gm.getFontSize(this.fontKey,this.scene.gw);
        

        if (this.el)
        {
            this.el.style.fontSize=fs.toString()+"px";
        }
    }
    updatePos()
    {
        this.scene.grid?.placeAt(this.posVo.x,this.posVo.y,this);
    }
}