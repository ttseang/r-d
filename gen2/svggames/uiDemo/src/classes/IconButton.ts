import { IScreen, PosVo, SvgObj } from "svggame";

export class IconButton extends SvgObj
{
  
    private scene:IScreen;
    private callback:Function;
    public action:string="";
    public params:string="";
    public posVo:PosVo=new PosVo(0,0);

    constructor(scene:IScreen,key:string,action:string,params:string,callback:Function)
    {
        super(scene,key);
        this.scene=scene;
        this.callback=callback;
        this.action=action;
        this.params=params;

        if (this.el)
        {
            this.el.onclick=()=>{
                this.callback(this.action,this.params);
            }
        }        
    }
    public setPos(posVo:PosVo)
    {
        this.posVo=posVo;
        this.scene.grid?.placeAt(posVo.x,posVo.y,this);
    }
    updatePos()
    {
        this.scene.grid?.placeAt(this.posVo.x,this.posVo.y,this);
    }
}