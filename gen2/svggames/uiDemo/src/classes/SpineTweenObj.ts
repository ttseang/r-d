export class SpineTweenObj {
    public x: number | null = null;
    public y: number | null = null;

    public scaleX:number|null=null;
    public scaleY:number|null=null;
  
    

    public duration:number=1000;
    
    public onComplete:Function=()=>{};

    constructor() {

    }
}