export class WordVo {
    public chars: string[];
    public word: string = "";

    constructor(word: string, chars: string[]) {
        this.word = word;
        this.chars = chars;

    }
}