import { BaseTargObj, IDragTarg } from "dragengsvg";
import { IScreen } from "svggame";


export class TargetBox extends BaseTargObj implements IDragTarg
{
    private textEl:SVGTextElement |null=null;
    public text:string="";
    constructor(iScreen:IScreen,key:string)
    {
        super(iScreen,key);
    }    
    
    doResize()
    {
        this.updateScale();
        super.doResize();
    }
}