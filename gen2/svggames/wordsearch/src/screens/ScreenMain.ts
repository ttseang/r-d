
import { BaseScreen, IScreen, PosVo, SvgObj } from "svggame";
import { SpineChar } from "../classes/char/SpineChar";
import { LetterCell } from "../classes/letterCell";
import { MarkLine } from "../classes/markLine";


import { LinkUtil } from "../classes/util/LinkUtil";
import { WordWindow } from "../classes/wordWindow";
import { WSModel } from "../classes/WSModel";


import { AnimationVo } from "../dataObjs/AnimationVo";
import { LineVo } from "../dataObjs/LineVo";
import { WordVo } from "../dataObjs/WordVo";
//import { SpineChar } from "spinechar";


export class ScreenMain extends BaseScreen implements IScreen {

    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wm: WSModel = WSModel.getInstance();
    //  private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private contentWindow: HTMLElement | null;


    //private mainImage: SvgObj | null = null;

    private letterGridObj: HTMLElement | null;
    private letterGrid: LetterCell[][] = [];
    private firstLetter: LetterCell | null = null;

    private startLetter: LetterCell | null = null;
    private endLetter: LetterCell | null = null;

    private mouseIsDown: boolean = false;


    private wordMap: Map<string, WordVo> = new Map<string, WordVo>();
    private wordWindow: WordWindow = new WordWindow();

    private lineData: LineVo[] = [];

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("letter", 45, 1000);

        this.contentWindow = document.getElementById("contentWindow");

        this.letterGridObj = document.getElementById("letterGrid");

    }
    create() {
        super.create();

        (window as any).scene = this;


        //this.makeGrid();



        this.loadData();
    }

    loadData() {


        fetch("./assets/words/level1.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {



        let boardLetters: string[] = data.level.board.split("|");
        let words: any = data.level.words;


        for (let i: number = 0; i < words.length; i++) {
            let wordVo: WordVo = new WordVo(words[i].word, words[i].color);

            this.wordMap.set(wordVo.word, wordVo);


        }

        this.wordWindow.setWords(this.mapToArray());
        //  this.words=data.level.words.split(",");

        this.makeGrid(boardLetters);

        document.onmousedown = this.onDown.bind(this);
        document.onmouseup = this.onUp.bind(this);
        document.onmousemove = this.showDragLine.bind(this);



        LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", "");
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");

        //  this.addLine(0, 0, 9, 9,"red");
        this.doResize();


        //  this.showLine(0,5,0,9);
    }
    mapToArray() {
        let words: WordVo[] = [];
        this.wordMap.forEach((wordObj: WordVo) => {
            words.push(wordObj);
        });
        return words;
    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics2/" + image + ".svg";

    }
    onbuttonPressed(action: string, params: string) {

        switch (action) {
            case "reset":

                break;

            case "check":
                this.checkCorrect();
                break;
        }
    }
    makeGrid(chars: string[]) {
        let lastCell: LetterCell | null = null;

        for (let i: number = 0; i < 10; i++) {
            this.letterGrid[i] = [];
            for (let j: number = 0; j < 10; j++) {
                let letter: LetterCell = new LetterCell(this);
                letter.gridX = j;
                letter.gridY = i;

                this.letterGrid[i][j] = letter;
                if (lastCell != null) {
                    lastCell.nextLetter = letter;
                }
                lastCell = letter;
                //letter.onClick(()=>{this.clickLetter(letter)})  

                let char: string = chars[i].split("")[j];

                letter.setText(char);
            }
        }

        this.firstLetter = this.letterGrid[0][0];
    }

    onDown(e: MouseEvent) {
        this.mouseIsDown = true;
        let downLetter: LetterCell | null | undefined = this.findLetter(e.x, e.y);
        if (downLetter) {
            this.startLetter = downLetter;
            
        }
    }
    onUp(e: MouseEvent) {
        this.mouseIsDown = false;
        let upLetter: LetterCell | null | undefined = this.findLetter(e.x, e.y);
        if (upLetter) {
            this.endLetter = upLetter;
            this.checkWord();
            this.startLetter = null;
        }
        else {
            if (this.firstLetter && this.startLetter) {
                /*  console.log("search for nearest");
                 this.firstLetter.checkDist(e.x,e.y);
                 this.endLetter=this.wm.nearestLetter;
                 if (this.endLetter)
                 {
                     this.endLetter.visible=false;
                 }     */
                /* this.endLetter = this.wm.nearestLetter;
                if (this.endLetter != null) {
                    this.checkWord();
                    this.startLetter = null;
                } */
            }
        }
    }
   
    checkWord() {
        if (this.startLetter && this.endLetter) {

            let word: string = this.getWord();


            if (this.wordMap.has(word)) {
                let wordVo = this.wordMap.get(word) || new WordVo("", "red");

                if (wordVo.used == false) {
                    wordVo.used = true;
                    this.addLine(this.startLetter.gridX, this.startLetter.gridY, this.endLetter.gridX, this.endLetter.gridY, wordVo?.color);
                    this.wordWindow.setWords(this.mapToArray());
                    this.playSound(this.rightSound);
                }
            }
            this.clearDragLine();
            this.startLetter = null;
            this.endLetter = null;
        }
    }
    getWord() {
        let word: string = "";

        if (this.startLetter && this.endLetter) {

            // this.startLetter.visible=false;
            //  this.endLetter.visible=false;



            let startX: number = this.startLetter.gridX;
            let startY: number = this.startLetter.gridY;
            //
            //
            let endX: number = this.endLetter.gridX;
            let endY: number = this.endLetter.gridY;
            //
            //
            let diffX: number = Math.abs(startX - endX);
            let diffY: number = Math.abs(startY - endY);
            //
            //


            if (diffX === 0 && diffY !== 0) {
                word = this.getColSlice(startX, startY, endY);
            }
            if (diffY === 0 && diffX !== 0) {

                word = this.getRowSlice(startY, startX, endX);
            }
            if (diffX === diffY) {

                word = this.getDiagSlice(startX, startY, endY, endX);
            }

        }
        return word;
    }
    addLine(x1: number, y1: number, x2: number, y2: number, color: string) {
        this.lineData.push(new LineVo(x1, y1, x2, y2, 0.25, color));
        this.showLine(x1, y1, x2, y2, color);
    }
    rebuildLines() {
        let lineArea: HTMLElement | null = document.getElementById("lines");
        if (lineArea) {
            lineArea.innerHTML = "";
        }
        for (let i: number = 0; i < this.lineData.length; i++) {
            let lineVo: LineVo = this.lineData[i];
            this.showLine(lineVo.x1, lineVo.y1, lineVo.x2, lineVo.y2, lineVo.color);
        }
    }
    showLine(x1: number, y1: number, x2: number, y2: number, color: string) {
        if (this.letterGridObj) {


            let sLetter: LetterCell = this.letterGrid[y1][x1];
            let eLetter: LetterCell = this.letterGrid[y2][x2];

            let b2: DOMRect = this.letterGridObj.getBoundingClientRect();
            let startX: number = b2.left;
            let startY: number = b2.top;
            //
            //
            let startRect: DOMRect = sLetter.getBoundingClientRect();
            let endRect: DOMRect = eLetter.getBoundingClientRect();

            let letterStartY: number = startRect.top + startRect.height / 2;
            let letterStartX: number = startRect.left;

            let letterEndY: number = endRect.top + endRect.height;
            let letterEndX: number = endRect.left + endRect.width;

            if (y1 == y2) {

                letterStartY = startRect.top + startRect.height / 2;
                letterStartX = startRect.left;

                letterEndY = endRect.top + endRect.height / 2;
                letterEndX = endRect.right;
            }

            if (x1 == x2) {


                letterStartY = startRect.top + startRect.height / 2;
                letterStartX = startRect.left + startRect.width / 2;

                letterEndY = endRect.top + startRect.height / 2;
                letterEndX = endRect.left + startRect.width / 2;;

            }


            let line: MarkLine = new MarkLine(this);
            line.setLine(letterStartX, letterStartY, letterEndX, letterEndY, 0.028, color);
            line.visible = true;
        }
    }
    clearDragLine() {
        let dragArea: HTMLElement | null = document.getElementById("draglineArea");
        if (dragArea) {
            dragArea.innerHTML = "";
        }
    }
    showDragLine(e: MouseEvent) {



        this.clearDragLine();
        if (this.mouseIsDown == false) {
            return;
        }
        if (!this.startLetter) {
            this.startLetter = this.findLetter(e.x, e.y) || null;

            if (this.startLetter == null) {
                return;
            }

        }
        if (this.firstLetter) {
            this.firstLetter.checkDist(e.x, e.y);
            // this.endLetter=this.wm.nearestLetter;
            
        }

        let xx: number = e.x;
        let yy: number = e.y;


        if (this.startLetter && this.letterGridObj) {

            let startRect: DOMRect = this.startLetter.getBoundingClientRect();


            let letterStartY: number = startRect.top + startRect.height / 2;
            let letterStartX: number = startRect.left + startRect.width / 2;



            let line: MarkLine = new MarkLine(this, "dragline", "draglineArea");
            line.setLine(letterStartX, letterStartY, xx, yy, 0.028, "yellow");
            line.alpha = 0.5;
            line.visible = true;
        }
    }
    findLetter(x: number, y: number) {
        if (this.letterGridObj) {
            let b2: DOMRect = this.letterGridObj.getBoundingClientRect();

            if (x > b2.left && x < b2.right * 1.5) {
                if (y > b2.top && y < b2.bottom * 1.5) {
                    return this.firstLetter?.checkPos(x, y);
                }
            }
        }
        return null;
    }
    getRowSlice(row: number, start: number, end: number) {
        if (end < start) {
            let temp: number = end;
            end = start;
            start = temp;
        }

        let word: string = "";
        for (let i: number = start; i < end + 1; i++) {
            //this.board.letterBoxes[row][i].alpha = .5;
            word += this.letterGrid[row][i].getText();
            //  this.selectionObj.selectArray.push(this.board.letterBoxes[i][row]);
        }

        return word;
    }
    getColSlice(col: number, start: number, end: number) {



        if (end < start) {
            let temp: number = end;
            end = start;
            start = temp;
        }


        let word: string = "";
        for (let i: number = start; i < end + 1; i++) {
            word += this.letterGrid[i][col].getText();
            //  this.selectionObj.selectArray.push(this.board.letterBoxes[col][i]);
        }
        return word;
    }
    getDiagSlice(startCol: number, startRow: number, endRow: number, endCol: number) {

        let yDir: number = 1;
        let xDir: number = 1;

        if (endRow < startRow) {
            yDir = -1;
        }

        if (endCol < startCol) {
            xDir = -1;
        }


        let len: number = Math.abs(startRow - endRow) + 1;
        let word: string = "";


        let row: number = startRow;
        let col: number = startCol;

        for (let i: number = 0; i < len; i++) {
            /* let xx: number = startRow + i*xDir;
            let yy: number = startCol + i*yDir; */

            word += this.letterGrid[row][col].getText();
            //  this.selectionObj.selectArray.push(this.board.letterBoxes[row][col]);
            row += yDir;
            col += xDir;


        }
        return word;

    }
    clickLetter(cell: LetterCell) {
        cell.visible = false;
    }
    checkCorrect() {

    }




    doResize() {
        super.doResize();

        if (this.letterGrid.length) {
            for (let i: number = 0; i < 10; i++) {
                for (let j: number = 0; j < 10; j++) {
                    let letter: LetterCell = this.letterGrid[i][j];
                    if (letter) {
                        letter.setFontSize(this.gm.getFontSize("letter", this.gh));
                        letter.x = j * this.gw * 0.04;
                        letter.y = i * this.gh * 0.06 + 30;
                    }
                }

            }
            // this.grid?.showGrid();


            this.rebuildLines();
        }

    }
    alignLettersToWindow() {
        if (this.contentWindow && this.letterGridObj) {
            let b1: DOMRect = this.contentWindow.getBoundingClientRect();
            // let b1:DOMRect=document.body.getBoundingClientRect();
            let b2: DOMRect = this.letterGridObj.getBoundingClientRect();
            console.log(b1);
            console.log(b2);
            // let b3: DOMRect = this.letterGrid[0][0].getBoundingClientRect();

            let r: number = b1.right - b2.width - b1.width * 0.05;
            let t: number = b1.top + b1.height * 0.1;

            this.letterGridObj.setAttribute("y", t.toString());
            this.letterGridObj.setAttribute("x", r.toString());



        }


    }
}