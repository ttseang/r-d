export class WordVo
{
    public color:string;
    public word:string="";
    public used:boolean;

    constructor(word:string,color:string,used:boolean=false)
    {
        this.word=word;
        this.color=color;
        this.used=false;
    }
}