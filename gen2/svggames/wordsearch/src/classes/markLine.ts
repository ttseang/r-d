import { IScreen, SvgObj } from "svggame";
import { IBaseScene } from "../../../../common/mods/ttcomps";
import { LineVo } from "../dataObjs/LineVo";


export class MarkLine extends SvgObj {
    private x1: number = 0;
    private y1: number = 0;
    private x2: number = 0;
    private y2: number = 0;
    private thick: number = .0004;
    private color: string = "red";
   
    private bscene:IScreen;

    constructor(bscene: IScreen,id:string="theline",area:string="lines") {
        super(bscene, id,area);
        this.bscene=bscene;
        if (this.el)
        {
            this.el.style.display="none";
        }
    }
    setLine(x1: number, y1: number, x2: number, y2: number, thick: number =.025, color: string = "red") {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.thick = thick;
        this.color = color;
        this.resetPos();
    }
    setLineFromObj(lineVo: LineVo) {       

        this.x1 = lineVo.x1;
        this.y1 = lineVo.y1;
        this.x2 = lineVo.x2;
        this.y2 = lineVo.y2;
        this.color=lineVo.color;
        this.thick=lineVo.thick;
        this.resetPos();
    }

    resetPos() {
        if (this.el) {
           /*  let xx1: number =this.zoomImage.x+this.zoomImage.displayWidth * this.x1/100;
            let yy1: number = this.zoomImage.y+this.zoomImage.displayHeight * this.y1/100;
            let xx2: number =this.zoomImage.x+ this.zoomImage.displayWidth * this.x2/100;
            let yy2: number = this.zoomImage.y+this.zoomImage.displayHeight * this.y2/100; */
            

            this.el.setAttribute("x1", this.x1.toString());
            this.el.setAttribute("y1", this.y1.toString());
            this.el.setAttribute("x2", this.x2.toString());
            this.el.setAttribute("y2", this.y2.toString());

            this.el.style.stroke=this.color;
            this.el.style.strokeWidth=(this.thick*this.bscene.gw).toString(); 

        }
    }
}