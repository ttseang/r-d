import { WordVo } from "../dataObjs/WordVo";
import { HtmlObj } from "./HtmlObj";

export class WordWindow extends HtmlObj {
    constructor() {
        super("wordWindow");
    }
    public setWords(words: WordVo[]) {
        if (this.el) {
            let wordContent: string = "";
            let textDiv: HTMLElement = this.el.getElementsByTagName("div")[0];

            if (textDiv) {
                for (let i: number = 0; i < words.length; i++) {
                    if (words[i].used == false) {
                        wordContent += '<div class="windowWord">' + words[i].word + '</div>';
                    }
                    else {
                        wordContent += '<div class="windowWord2">' + words[i].word + '</div>';
                    }
                }
                textDiv.innerHTML = wordContent;
            }
        }
    }
}