import { LetterCell } from "./letterCell";

let instance: WSModel | null = null;

export class WSModel {

    public nearestLetter:LetterCell | null=null;
    public lastLetter:LetterCell|null=null;
    public dist:number=1000;
    

    constructor() {

    }
    static getInstance() {
        if (instance === null) {
            instance = new WSModel();
        }
        return instance;
    }
}