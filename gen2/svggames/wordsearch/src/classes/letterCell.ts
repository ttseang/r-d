import { IScreen, SvgObj } from "svggame";
import { WSModel } from "./WSModel";


export class LetterCell extends SvgObj {
    public nextLetter: LetterCell | null = null;
    public gridX: number = 0;
    public gridY: number = 0;
    private text: string = "";
    private wm: WSModel = WSModel.getInstance();

    constructor(screen: IScreen) {
        super(screen, "letter", "letterGrid");
        
    }
    checkDist(xx: number, yy: number) {
        if (this.el) {
            let rect: DOMRect = this.el.getBoundingClientRect();
            let dist = Math.sqrt((xx - rect.x) * (xx - rect.x) + (yy -rect.y) * (yy -rect.y));
            if (dist < this.wm.dist) {
                this.wm.nearestLetter = this;
                this.wm.dist = dist;
                
            }
        }
        if (this.nextLetter) {
            this.nextLetter.checkDist(xx, yy);
        }
    }
    setText(text: string) {
        this.text = text;
        if (this.el) {

            this.el.textContent = text;
            this.updateSizes();
        }
    }
    getText() {
        return this.text;
    }
    setFontSize(size: number) {
        if (this.el) {
            this.el.setAttribute("font-size", size.toString() + "px");
            this.updateSizes();
        }
    }
    public checkPos(xx: number, yy: number): LetterCell | null {
        if (this.el) {
            let bounds: DOMRect = this.el?.getBoundingClientRect();
            if (xx > bounds.left && xx < bounds.right && yy > bounds.top && yy < bounds.bottom) {
                return this;
            }
        }

        if (this.nextLetter) {
            return this.nextLetter.checkPos(xx, yy);
        }
        return null;
    }
}