import { BaseTargObj, IDragTarg } from "dragengsvg";
import { IScreen } from "svggame";


export class TargetBox extends BaseTargObj implements IDragTarg
{
    private textEl:SVGTextElement |null=null;
    public text:string="";
    constructor(iScreen:IScreen)
    {
        super(iScreen,"targetBox");
    }    
    
    doResize()
    {
        this.updateScale();
        super.doResize();
    }
}