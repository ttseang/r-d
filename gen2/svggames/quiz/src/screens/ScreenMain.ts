
import { BaseScreen, IScreen, SvgObj } from "svggame";


import { LinkUtil } from "../classes/util/LinkUtil";


import { WordVo } from "../dataObjs/WordVo";
//import { SpineChar } from "spinechar";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private levels: WordVo[] = [];
    private levelIndex: number = -1;
    private currentWord: WordVo = new WordVo("", []);

    

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);


        setTimeout(() => {
            //this.spineChar.moveX(-10,walkAnimations,charAnimations,3000);
        }, 2000);

    }
    create() {
        super.create();

        (window as any).scene = this;

        /**
        * SET CALLBACK ON THE DRAG ENGINE
        */

       
        this.loadData();
    }

    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        data = data.data;

        for (let i: number = 0; i < data.length; i++) {
            let wordVo: WordVo = new WordVo(data[i].word, data[i].letters);
            this.levels.push(wordVo);
        }
        //console.log(data);
        this.nextLevel();
       


        LinkUtil.makeLink("resetButton",this.onbuttonPressed.bind(this),"reset","");
        LinkUtil.makeLink("checkButton",this.onbuttonPressed.bind(this),"check","");
    }
    setImage(image:string)
    {
        let img:HTMLImageElement=document.getElementById("mainImage") as HTMLImageElement;
        img.src="./assets/pics2/"+image+".svg";

    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
          switch (action) {
             case "reset":
                
                 break;
 
             case "check":
                 this.checkCorrect();
                 break;
         }
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            console.log("out of words");
            return;
        }
        this.makeBoxes();
    }
    makeBoxes() {      


    }
   
    checkCorrect() {

      
       
    }
    doResize() {
        super.doResize();


        // this.grid?.showGrid();


    }

}