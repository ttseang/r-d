export class CardVo
{
    public xx:number;
    public yy:number;
    public heading:string;
    public content:string[]
    public type:string;
    public highlight:number;
    public relativeToImage:boolean;

    constructor(type:string,xx:number,yy:number,heading:string,content:string[],highlight:number,relativeToImage:boolean)
    {
        this.type=type;
        this.xx=xx;
        this.yy=yy;
        this.heading=heading;
        this.content=content;
        this.highlight=highlight;
        this.relativeToImage=relativeToImage;
    }
}