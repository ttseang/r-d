import { BaseDragObj, IDragObj } from "dragengsvg";
import { IScreen } from "svggame";

export class LetterBox extends BaseDragObj implements IDragObj {
    public text: string = "";
    private textEl: SVGTextElement | null = null;

    constructor(scene: IScreen) {
        super(scene, "letterbox");
        // this.setContent("B");
    }
    setText(text: string) {
        this.text = text;
        this.content = text;
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            this.textEl.innerHTML = text;
        }
    }
    doResize() {
        this.updateScale();
        super.doResize();
    }
}