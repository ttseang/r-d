
import { BaseScreen, GameManager, IScreen, Math2, PosVo, SvgObj, TextObj } from "svggame";
import { DragEng, IDragObj, IDragTarg } from "dragengsvg";
import { IconButton } from "../classes/IconButton";
import { LetterBox } from "../classes/LetterBox";
import { WordVo } from "../dataObjs/WordVo";
import { TargetBox } from "../classes/TargetBox";
import { ColorBurst } from "../effects/ColorBurst";
import { SpineChar } from "../classes/SpineChar";
import { AnimationVo } from "../dataObjs/AnimationVo";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;
    private dragEng: DragEng;

    private uiWindow!: SvgObj;
    private btnCheck!: SvgObj;
    private btnPlay!: SvgObj;
    private btnReset!: SvgObj;
    private btnHelp!: SvgObj;

    private buttons: SvgObj[] = [];
    private instructionText: TextObj | null = null;
    private levels: WordVo[] = [];
    private levelIndex: number = -1;
    private currentWord: WordVo = new WordVo("", []);

    private targetBox: TargetBox | null = null;

    private mainImage: SvgObj | null = null;
    private correctLetter: string = "";

    //sounds

    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private colorBurst: ColorBurst | null = null;

    private spineChar:SpineChar;

    private idleAnimations:AnimationVo[];
    private walkAnimations:AnimationVo[];
    private fidgetAnimations:AnimationVo[];

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);

        this.dragEng = new DragEng(this);
        this.dragEng.snapToTarget = true;

        this.idleAnimations=[new AnimationVo("idle",true),new AnimationVo("Repeating animations/blink",true)];
        this.walkAnimations=[new AnimationVo("walk",true),new AnimationVo("Repeating animations/blink",true)];
        this.fidgetAnimations=[new AnimationVo("fidget01",false),new AnimationVo("Repeating animations/blink",true)];


        this.spineChar=new SpineChar("char","./assets/char/Amka.json","./assets/char/Amka.atlas",this.idleAnimations);
        this.spineChar.flip=true;
        this.spineChar.scaleX=.4;
        this.spineChar.scaleY=.4;
        this.spineChar.x=4000;
        this.spineChar.y=-1400;
        
        

        setTimeout(() => {
            //this.spineChar.moveX(-10,walkAnimations,charAnimations,3000);
        }, 2000);
    
    }
    create() {
        super.create();

        this.colorBurst = new ColorBurst(this, this.gw / 2, this.gh / 2);
        (window as any).scene = this;

        this.uiWindow = new SvgObj(this, "uiWindow");
        this.uiWindow.gameWRatio = .9;

        let scale:number=this.gw/4000

        this.spineChar.setScale(scale);

        setTimeout(() => {
            let posX:number=-this.gw/.4;
            let posY:number=this.gh/10;
        console.log(posX,posY);

            this.spineChar.moveTo(posX,posY,6000,this.walkAnimations,this.idleAnimations,true);
        }, 2000);
       

        //
        //
        //
        this.btnPlay = new IconButton(this, "btnPlay", "play", "", this.onbuttonPressed.bind(this));
        this.btnReset = new IconButton(this, "btnReset", "reset", "", this.onbuttonPressed.bind(this));
        this.btnCheck = new IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
        this.btnHelp = new IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));

        this.buttons = [this.btnHelp, this.btnPlay, this.btnReset, this.btnCheck];

        /**
         * SET CALLBACK ON THE DRAG ENGINE
         */

        this.dragEng.upCallback = this.onUp.bind(this)
        this.dragEng.dropCallback = this.onDrop.bind(this);
        this.dragEng.collideCallback = this.onCollide.bind(this);


        this.instructionText = new TextObj(this, "instText");

        this.loadData();

        // this.dragEng.setListeners();
    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        data = data.data;

        for (let i: number = 0; i < data.length; i++) {
            let wordVo: WordVo = new WordVo(data[i].word, data[i].letters);
            this.levels.push(wordVo);
        }
        //console.log(data);
        this.nextLevel();
        this.dragEng.setListeners();
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            //console.log("out of words");
            return;
        }
        if (this.mainImage) {
            this.mainImage.destroy();

        }

        this.currentWord = this.levels[this.levelIndex];

        this.correctLetter = this.currentWord.word.substr(0, 1).toLowerCase();

        //console.log(this.currentWord.word);

        this.mainImage = new SvgObj(this, this.currentWord.word);
        this.mainImage.gameWRatio = 0.2;
        this.grid?.placeAt(7, 3, this.mainImage);

        this.makeBoxes();
    }
    makeBoxes() {

        this.dragEng.resetBoxes();
        this.dragEng.destoryDrags();
        this.dragEng.destroyTargs();

        this.targetBox = new TargetBox(this);
        this.targetBox.gameWRatio=0.08;
        this.targetBox.setPlace(new PosVo(4, 3));

        this.dragEng.addTarget(this.targetBox);

        for (let i: number = 0; i < this.currentWord.chars.length; i++) {
            let box: LetterBox = new LetterBox(this);
            box.setText(this.currentWord.chars[i]);
            box.gameWRatio = 0.08;
            box.initPlace(new PosVo(3 + i, 6));
            this.dragEng.addDrag(box);
        }

        if (this.colorBurst) {
            this.colorBurst.x = this.targetBox.x;
            this.colorBurst.y = this.targetBox.y;
        }

    }
    onbuttonPressed(action: string, params: string) {
        //console.log(action);
        switch (action) {
            case "reset":
                this.dragEng.clickLock = false;
                this.dragEng.resetBoxes();
                break;

            case "check":
                this.checkCorrect();
                break;
        }
    }
    onCollide(d: IDragObj, t: IDragTarg) {
        //console.log("collide");
        d.canDrag = false;


        this.dragEng.clickLock = true;
    }
    onDrop() {
        //console.log("dropped");
        //console.log(this.dragEng.getTargetContent());

        //check drop content here
    }
    onUp() {
        //console.log("up");
    }
    doResize() {
        super.doResize();


       // this.grid?.showGrid();

         console.log(this.gw,this.gh);
        // this.uiWindow.updateScale();
        if (this.instructionText) {
            this.instructionText.setFontSize(this.gm.getFontSize("instructions", this.gm.gw));
            this.grid?.placeAt(5, 2, this.instructionText);
            this.centerW(this.instructionText, true);
        }

        this.uiWindow.displayWidth = this.gw * 0.9;
        this.uiWindow.displayHeight = this.gh * 0.70;

        this.center(this.uiWindow, true);

        for (let i: number = 0; i < this.buttons.length; i++) {
            this.buttons[i].gameWRatio = 0.05;
            this.grid?.placeAt(2 + i * 2, 9.7, this.buttons[i]);
        }

        this.dragEng.doResize();

        if (this.mainImage)
        {
            this.mainImage?.updateScale();
            this.grid?.placeAt(7, 3, this.mainImage);
        }
       
    }
    checkCorrect() {

        if (this.correctLetter == "") {
            return;
        }
        if (this.correctLetter !== this.dragEng.getTargetContent()) {
            //console.log("Wrong");
            this.playSound(this.wrongSound);
            /*  this.cm.getComp("wrong").visible = true;
             this.cm.getComp("right").visible = false;
             this.playSound("wrongSound"); */
        }
        else {


            //console.log("Right");
            this.playSound(this.rightSound);

            this.spineChar.setAnimations(this.fidgetAnimations);

            setTimeout(() => {
                this.spineChar.setAnimations(this.idleAnimations);
            }, 1000);

            //this.colorBurst?.start();
            setTimeout(() => {
                this.dragEng.clickLock = false;
                this.nextLevel();
            }, 1500);
            /* this.cm.getComp("wrong").visible = false;
            this.cm.getComp("right").visible = true;
            this.clickLock = true;
            this.playSound("rightSound");
            this.doWinEffect(); */
        }
    }
}