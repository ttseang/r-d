
import { IDragObj, IDragTarg } from "dragengsvg";
import { DragEng } from "dragengsvg/classes/DragEng";
import { AlignGrid, BaseScreen, HtmlObj, IScreen, PosVo, SvgObj, TextObj } from "svggame";


import { TargetBox } from "../classes/TargetBox";
import { TWordBox } from "../classes/TwordBox";
import { LinkUtil } from "../classes/util/LinkUtil";
import { WordBox } from "../classes/WordBox";


import { WordVo } from "../dataObjs/WordVo";
//import { SpineChar } from "spinechar";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private levels: WordVo[] = [];
    private levelIndex: number = -1;
    private currentWord: WordVo = new WordVo([], "", "");

    //private mainImage: SvgObj | null = null;
    private correctWord: string = "";

    private wrongIcon: HtmlObj | null = null;
    private rightIcon: HtmlObj | null = null;

    private targetBox: TargetBox | null = null;
    private dragEng: DragEng;

    private currentBox: IDragObj | null = null;
    private nextFlag: boolean = false;
    private usedWords: Array<string> = [];
    private twordBoxes: TWordBox[] = [];

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);



        this.dragEng = new DragEng(this);
        this.dragEng.snapToTarget = true;




    }
    create() {
        super.create();

        (window as any).scene = this;

        /**
        * SET CALLBACK ON THE DRAG ENGINE
        */

        this.dragEng.upCallback = this.onUp.bind(this)
        this.dragEng.dropCallback = this.onDrop.bind(this);
        this.dragEng.collideCallback = this.onCollide.bind(this);
        this.dragEng.downCallback = this.startDrag.bind(this);
        

        //this.getPrevData();
        this.loadData();


    }
    
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        data = data.data;

        console.log(data);

        for (let i: number = 0; i < data.length; i++) {

            let dragWords: string[] = data[i].dragwords.split(",");
            let correct: string = data[i].correct;

            let wordVo: WordVo = new WordVo(dragWords, data[i].twords, correct);

            this.levels.push(wordVo);
        }
        //console.log(data);
        this.nextLevel();
        this.dragEng.setListeners();


     //   LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", "");
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");
        LinkUtil.makeLink("btnNext", this.onbuttonPressed.bind(this), "next", "");
    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics2/" + image + ".svg";

    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
        switch (action) {
            case "reset":
                if (this.nextFlag == false) {
                    this.dragEng.clickLock = false;
                    if (this.rightIcon) {
                        this.rightIcon.visible = false;
                    }
                    if (this.wrongIcon) {
                        this.wrongIcon.visible = false;
                    }
                    this.dragEng.resetBoxes();
                }
                break;

            case "check":
                this.checkCorrect();
                break;

            case "next":
                if (this.nextFlag == true) {
                    this.nextFlag = false;
                    this.nextLevel();
                }
                break;
        }
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            alert("out of words");
            return;
        }
        this.dragEng.clickLock = false;
        /*  if (this.mainImage) {
             this.mainImage.destroy();
 
         } */

        this.currentWord = this.levels[this.levelIndex];

        this.correctWord = this.currentWord.correct;

        if (this.rightIcon) {
            this.rightIcon.visible = false;
        }
        if (this.wrongIcon) {
            this.wrongIcon.visible = false;
        }



        // this.setImage(this.currentWord.word);


        this.makeBoxes();

        this.doResize();
    }
    makeBoxes() {


        for (let i: number = 0; i < this.twordBoxes.length; i++) {
            let tbox: TWordBox = this.twordBoxes[i];
            tbox.destroy();
        }
        this.dragEng.resetBoxes();
        this.dragEng.destoryDrags();
        this.dragEng.destroyTargs();

        let tstring: string[] = this.currentWord.twords.split(",");

        let xx: number = 5 - tstring.length / 2;
        let yy: number = 7.5;


        for (let i: number = 0; i < tstring.length; i++) {
            let tbox: TWordBox = new TWordBox(this);
            this.twordBoxes.push(tbox);
            tbox.gameWRatio = 0.08;
            tbox.setPlace(new PosVo(xx, yy));
            xx++;
            tbox.setText(tstring[i]);
        }


        this.targetBox = new TargetBox(this);
        this.targetBox.gameWRatio = 0.08;
        this.targetBox.setPlace(new PosVo(xx, yy));
        this.targetBox.text = "?";
        this.dragEng.addTarget(this.targetBox);


        let pos: PosVo[] = [new PosVo(3, 3), new PosVo(5, 3), new PosVo(7, 3), new PosVo(3, 5), new PosVo(5, 5), new PosVo(7, 5)];
        let pindex: number = -1;

        for (let i: number = 0; i < this.currentWord.dragwords.length; i++) {

            let word: string = this.currentWord.dragwords[i];
            if (this.usedWords.indexOf(word) < 0) {
                pindex++;
                let box: WordBox = new WordBox(this);

                box.setText(word);

                box.gameWRatio = 0.08;
                box.initPlace(pos[pindex]);
                this.dragEng.addDrag(box);
            }
        }
        this.wrongIcon = new HtmlObj("wrongIcon");

        this.rightIcon = new HtmlObj("rightIcon");

        this.rightIcon.visible = false;
        this.wrongIcon.visible = false;
    }
    startDrag(d: IDragObj) {
        this.currentBox = d;
    }
    onCollide(d: IDragObj, t: IDragTarg) {
        console.log("collide");
        d.canDrag = false;
        this.dragEng.clickLock = true;
    }
    onDrop() {
        //console.log("dropped");
        console.log(this.dragEng.getTargetContent());

        //check drop content here
    }
    onUp() {
        //console.log("up");
    }
    boxesOff() {
        for (let i: number = 0; i < this.dragEng.dragObjs.length; i++) {
            let box: IDragObj = this.dragEng.dragObjs[i];
            if (box != this.currentBox) {
                box.alpha = .5;
            }
        }
    }
    checkCorrect() {

        if (this.correctWord == "") {
            return;
        }
        if (this.dragEng.getTargetContent() == this.currentWord.correct) {
            if (this.nextFlag == false) {
                this.boxesOff();
                this.nextFlag = true;
                this.playSound(this.rightSound);

                this.usedWords.push(this.currentWord.correct);

                if (this.rightIcon) {
                    this.rightIcon.visible = true;
                }
            }
        }
        else {
            this.nextFlag=true;
            this.usedWords.push(this.currentWord.correct);
            if (this.wrongIcon) {
                this.wrongIcon.visible = true;
            }
            this.playSound(this.wrongSound);
        }
    }
    doResize() {
        super.doResize();


    }

}