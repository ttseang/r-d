import { IScreen, SVGAnimation, SvgObj, TweenObj } from "svggame";
import { ArrowVo } from "../dataObjs/ArrowVo";
import { ArrowUtil } from "./util/ArrowUtil";

export class SvgArrow extends SvgObj {

    //the starting point of the arrow
    //almost always 0,0
    public x1: number = 0;
    public y1: number = 0;

    //relative end point of arrow
    public x2: number = 400;
    public y2: number = 0;

    //point of the curve
    public cx: number = 200;
    public cy: number = -150;

    //drawing arrow speed
    public animateSpeed: number = 2;

    //element for the drawing head of the arrow
    private tp: SVGTextPathElement | null = null;

    //message for the arrow
    private messagePath: SVGTextPathElement | null = null;

    //the length and height of the arrow
    public len: number = 0;
    public h: number = 0;


    //color of the arrow
    public fill: string = "black";

    //color of the arrow to restore when mouseout
    public lastFill: string = "black";


    public strokeWidth: number = 2;

    //classes for the end and start markers
    public startClass: string = "";
    public endClass: string = "";

    //used for setting controls in the probpox
    public endIndex: number = 0;
    public colorIndex: number = 0;

    //element where we draw the line
    public lineElement: SVGGeometryElement | null = null;

    //mask - element where dots are added to reveal the line
    public maskArea: SVGMaskElement | null = null;

    //text label for the arrow
    public message: string = "";
    //percentage for the message
    public messagePos: number = 0;

    //permited classes - helps with marker color
    private colorClasses: string[] = ['red', 'blue', 'black', 'yellow'];

    //the SVG definition tag
    private defs: SVGDefsElement;

    //record ccw,cw or straight. Used for controls
    public curvePosition: string = "";

    //the current position of the line drawn
    private drawLineStep: number = 0;

    //time in miliseconds
    private drawLineSpeed: number = 1000;

    //the number of units to draw with each step
    private drawLineInc: number = 1;


    constructor(screen: IScreen, id: string) {
        super(screen, id);
        (window as any).arrow = this;

        this.defs = document.getElementsByTagName("defs")[0];

        //draw the initial path
        this.makeDString();

        //this.el is the svg element of the error
        //created by the SVGOBj class

        if (this.el) {

            this.lineElement = this.el.getElementsByTagName("path")[0];
            this.lineElement.id = this.elID + "line";

            this.maskArea = this.el.getElementsByTagName("mask")[0];
            this.maskArea.id = this.elID + "mask";

        }
    }
    public remove() {
        if (this.el) {
            this.el.remove();
        }
    }

    //set one of the pretermined color classes
    //we need to limit to certain colors so we can
    //link to the markers
    public setFill(color: string) {
        for (let i: number = 0; i < this.colorClasses.length; i++) {
            this.removeStyle(this.colorClasses[i]);
        }
        this.addStyle(color);
        this.fill = color;
        if (this.startClass !== "") {
            this.setStart(this.startClass);
        }
        if (this.endClass !== "") {
            this.setEnd(this.endClass);
        }
        this.makeDString();
    }
    //add or remove styles

    public addStyle(style: string) {
        if (this.lineElement) {
            this.lineElement.classList.add(style);
        }
    }
    public removeStyle(style: string) {
        if (this.lineElement) {
            //  console.log("remove "+style);
            this.lineElement.classList.remove(style);
        }
    }
    public setStrokeWidth(w: number) {
        this.strokeWidth = w;
        this.makeDString();
    }

    public setCurve(cx: number, cy: number) {
        this.cx = cx;
        this.cy = cy;
        this.makeDString();
    }
    public flipCurveY() {
        this.cy = -this.cy;
        this.makeDString();
    }
    public flipUp() {
        this.cy = -Math.abs(this.cy);
        this.makeDString();
    }
    public flipDown() {
        this.cy = Math.abs(this.cy);
        this.makeDString();
    }
    public setCurveH(cy: number) {
        this.cy = cy;
        this.makeDString();
    }
    public setLen(x2: number) {
        this.x2 = x2;
        this.len = this.x2 - this.x1;
        this.makeDString();
    }
    public setH(y2: number) {
        this.y2 = y2;
        this.h = this.y2 - this.y1;
        this.makeDString();
    }
    //set the marker at the start of the line

    public setStart(classString: string) {

        if (this.lineElement) {
            this.startClass = classString;
            if (classString === "none") {
                this.lineElement.setAttribute("marker-start", "");
            }
            else {
                let fullClass: string = classString + this.fill;

                this.lineElement.setAttribute("marker-start", "url(#" + fullClass + ")");
            }
        }
    }
    //set the marker at the end of the line

    public setEnd(classString: string) {

        if (this.lineElement) {
            this.endClass = classString;
            if (classString === "none") {
                this.lineElement.setAttribute("marker-end", "");
            }
            else {
                let fullClass: string = classString + this.fill;
                this.lineElement.setAttribute("marker-end", "url(#" + fullClass + ")");
            }
        }

    }
    
    //redraw the ending point of the line

    public resetEndLine(x2: number, y2: number) {
        this.x2 = x2;
        this.y2 = y2;
        this.makeDString();
    }
    
    //set the mask

    public showMask() {
        if (this.lineElement && this.maskArea) {
            this.lineElement.setAttribute("mask", "url(#" + this.maskArea.id + ")");
        }
    }

    //remove the mask

    public hideMask() {
        if (this.lineElement) {
            this.lineElement.removeAttribute("mask");
        }
    }
    //create a dot with and x and y on a line point
    //add that dot to the mask

    public addDotAt(per: number) {
        if (this.lineElement && this.maskArea) {

            let totalLen: number = this.lineElement.getTotalLength();
            let per2: number = (per / 100) * totalLen;

            let p: DOMPoint = this.lineElement.getPointAtLength(per2);
            let xx2: number = p.x;
            let yy2: number = p.y;

            const svgns = "http://www.w3.org/2000/svg";

            let dot: SVGCircleElement = document.createElementNS(svgns, "circle");
            dot.setAttribute("cx", p.x.toString());
            dot.setAttribute("cy", p.y.toString());
            dot.setAttribute("r", this.strokeWidth.toString());
            dot.setAttribute("fill", "white");


            this.maskArea.append(dot);

        }
    }

    //add the arrow head to the line

    public addProgress() {
        if (this.tp) {
            this.tp.remove();
        }
        let text1: SVGTextElement = this.defs.getElementsByTagName("text")[0];

        text1 = text1.cloneNode(true) as SVGTextElement;
        text1.id = this.elID + "text";

        if (this.el) {
            this.tp = text1.getElementsByTagName("textPath")[0];
            this.tp.setAttribute("href", "#" + this.elID + "line");
            this.el.appendChild(text1);
            (window as any).tp = this.tp;
        }
    }

    //move the arrow head to a position on the line

    setProgress(per: number) {
        if (!this.tp) {
            this.addProgress();
        }
        if (this.tp) {
            this.tp.setAttribute("startOffset", per.toString() + "%");
        }

    }
    //start the arrow moving

    startProgress(speed: number, inc: number = 0.5) {
        if (this.maskArea) {
            while (this.maskArea.firstChild) {
                this.maskArea.removeChild(this.maskArea.firstChild);
            }
        }
        this.showMask();
        this.drawLineSpeed = speed;

        this.drawLineInc = inc;
        this.drawLineStep = 0;

        if (!this.tp) {
            this.addProgress();
        }
        this.setProgress(0);

        setTimeout(() => {
            this.advanceProgress();
        }, this.drawLineSpeed);
    }

    //advance the positon of were the line is drawn

    advanceProgress() {
        this.drawLineStep += this.drawLineInc;
        this.addDotAt(this.drawLineStep);
        this.setProgress(this.drawLineStep);
        if (this.drawLineStep < 100) {
            setTimeout(() => {
                this.advanceProgress();
            }, this.drawLineSpeed);
        }
        else {
            this.hideMask();
        }
    }
  

    //place a label on the line

    setMessage(msg: string, per: number = 0) {
        this.message = msg;
        if (this.messagePath) {
            this.messagePath.remove();
            this.messagePath = null;
        }
        if (msg === "") {
            return;
        }
        let text2: SVGTextElement = this.defs.getElementsByTagName("text")[1];

        text2 = text2.cloneNode(true) as SVGTextElement;
        text2.id = this.elID + "message";
        if (text2 && this.el) {
            this.messagePath = text2.getElementsByTagName("textPath")[0];
            this.messagePath.setAttribute("href", "#" + this.elID + "line");
            this.messagePath.innerHTML = msg;
            this.messagePath.setAttribute("startOffset", per.toString() + "%");
            this.el.appendChild(text2);
        }
    }

    //set the message position

    setMessagePos(per: number = 0) {
        this.messagePos = per;
        if (this.messagePath) {
            this.messagePath.setAttribute("startOffset", per.toString() + "%");
        }
    }

    //draw the path inside the line element

    public makeDString() {
        let dstring: string = "M" + this.x1.toString() + "," + this.y1.toString() + " Q" + this.cx.toString() + "," + this.cy.toString() + " " + this.x2.toString() + "," + this.y2.toString();
        console.log(dstring);

        if (this.lineElement) {
            this.lineElement.setAttribute("stroke-width", this.strokeWidth.toString() + "px");
            this.lineElement.setAttribute("d", dstring);
        }
    }
    
    //get the data for export

    public exportData() {
        let arrowVo: ArrowVo = new ArrowVo(this.x, this.y, this.x1, this.y1, this.x2, this.y2, this.cx, this.cy, this.fill, this.startClass, this.endClass);
        arrowVo.message = this.message;
        arrowVo.messagePos = this.messagePos;
        arrowVo.animateSpeed = this.animateSpeed;

        return JSON.stringify(arrowVo);
    }
    public fromString(objString: string) {
        let obj: any = JSON.parse(objString);

        let arrowVo: ArrowVo = new ArrowVo();
        arrowVo.fromObj(obj);
        console.log(arrowVo);

       
        this.x = arrowVo.x;
        this.y = arrowVo.y;

        this.x1 = arrowVo.x1;
        this.y1 = arrowVo.y1;

        this.x2 = arrowVo.x2;
        this.y2 = arrowVo.y2;

        this.cx = arrowVo.cx;
        this.cy = arrowVo.cy;

        this.fill = arrowVo.fill;

        this.message = arrowVo.message;
        this.messagePos = arrowVo.messagePos;


        this.startClass = arrowVo.startMark;
        this.endClass = arrowVo.endMark;

        this.animateSpeed = arrowVo.animateSpeed;


        this.makeDString();

        this.setFill(this.fill);

        this.setMessage(this.message, this.messagePos);

    }
}