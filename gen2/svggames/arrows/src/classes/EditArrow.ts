import { IScreen } from "svggame";
import { SvgArrow } from "./SvgArrow";

export class EditArrow extends SvgArrow {
  //function for when the arrow is clicked
  public callback: Function = () => { };
  
    constructor(screen: IScreen, id: string)
    {
        super(screen, id);
        

        if (this.el)
        {
            this.el.onclick = () => {
                this.callback(this);
            }
            this.el.onmouseover = () => {
                this.lastFill = this.fill;
                this.setFill("yellow");
            }
            this.el.onmouseout = () => {
                this.setFill(this.lastFill);
            }
        }
    }
}