export class ArrowUtil
{
    constructor()
    {

    }
   public static lerp(start: number, end: number, t: number) {
        return (1 - t) * start + t * end
    }
    public static getRotate(cx: number, cy: number, ex: number, ey: number, angle: number) {
        let radians = (Math.PI / 180) * angle;
        let cos = Math.cos(radians);
        let sin = Math.sin(radians);
        let x = (cos * (ex - cx)) - (sin * (ey - cy)) + cx;
        let y = (cos * (ey - cy)) + (sin * (ex - cx)) + cy;
        return { rx: x, ry: y };
    };
    public static getAngle(x1:number, y1:number, x2:number, y2:number) {
        var dx = x2 - x1;
        var dy = y2 - y1;
        var angle = Math.atan2(dy, dx) * 180 / Math.PI;
        return angle;
      }
}