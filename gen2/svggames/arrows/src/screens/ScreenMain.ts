import { BaseScreen, IScreen, SVGAnimation, SvgObj } from "svggame";
import { EditArrow } from "../classes/EditArrow";
import { IconButton } from "../classes/IconButton";
import { SvgArrow } from "../classes/SvgArrow";
import { ArrowUtil } from "../classes/util/ArrowUtil";

//import { SpineChar } from "spinechar";


export class ScreenMain extends BaseScreen implements IScreen {

    private startX: number = 0;
    private startY: number = 0;

    private endY: number = 0;
    private endX: number = 0;

    //private dragRect: SvgObj;
    //  private selectRect: SvgObj;

    private drawArrow: SvgArrow;

    private onMove: Function;
    private isDown: boolean = false;

    private mode: string = "edit";

    private selectedArrow: EditArrow | null = null;

    private dropdownStyle: HTMLSelectElement;
    private dropdownStyle2: HTMLSelectElement;

    //private dropdownCurve: HTMLSelectElement;
    private dropdownEnd: HTMLSelectElement;
    private dropdownEnd2: HTMLSelectElement;

    private btnAddStraight: HTMLButtonElement;
    private btnAddCW: HTMLButtonElement;
    private btnAddCCW: HTMLButtonElement;
    private btnEditEndPoint: HTMLButtonElement;
    private btnChangeEndStraight: HTMLButtonElement;
    private btnChangeEndCCW: HTMLButtonElement;
    private btnChangeEndCW: HTMLButtonElement;
    private btnPaste: HTMLButtonElement;
    private btnExtract: HTMLButtonElement;


    private btnAnimate: HTMLButtonElement;


    private xpos: HTMLInputElement;
    private ypos: HTMLInputElement;
    private txtMessage: HTMLInputElement;

    private curveSlider: HTMLInputElement;
    private wslide: HTMLInputElement;
    private animateslide: HTMLInputElement;

    private color: string = "black";
    private curvePosition = "-";

    private rx: number = 0;
    private ry: number = 0;

    private styleIndex: number = 0;
    private endingIndex: number = 0;
    private editEndChoices: HTMLElement;



    constructor() {
        super("ScreenMain");

        //this.selectRect = new SvgObj(this, "dragRect");
        this.drawArrow = new SvgArrow(this, "arrowtemp2");

        //  this.gm.regFontSize("instructions", 26, 1000);

        (window as any).sm = this;
        this.onMove = this.onMouseMove.bind(this);



        this.dropdownStyle = document.getElementById("styleSelect") as HTMLSelectElement || document.createElement("select");
        this.dropdownStyle2 = document.getElementById("styleSelect2") as HTMLSelectElement || document.createElement("select");

        this.dropdownEnd = document.getElementById("arrowSelect") as HTMLSelectElement || document.createElement("select");
        this.dropdownEnd2 = document.getElementById("arrowSelect2") as HTMLSelectElement || document.createElement("select");


        this.editEndChoices = document.getElementById("editEndChoices") as HTMLElement || document.createElement("div");

        this.btnAddStraight = this.getButton("btnAddStraight");
        this.btnAddCW = this.getButton("btnAddCW");
        this.btnAddCCW = this.getButton("btnAddCCW");
        this.btnEditEndPoint = this.getButton("btnEditEndPoint");

        this.btnChangeEndStraight = this.getButton("changeEndStraight");
        this.btnChangeEndCCW = this.getButton("changeEndCCW");
        this.btnChangeEndCW = this.getButton("changeEndCW");
        this.btnPaste = this.getButton("btnPaste");
        this.btnExtract = this.getButton("btnExtract");

        this.btnAnimate = this.getButton("animateLine");

        this.btnAddStraight.onclick = () => {
            this.startAddArrow("straight");
        }
        this.btnAddCW.onclick = () => {
            this.startAddArrow("cw");
        }
        this.btnAddCCW.onclick = () => {
            this.startAddArrow("ccw");
        }

        this.btnEditEndPoint.onclick = () => {
            this.showEditEndChoices(true);
        }

        this.btnChangeEndCCW.onclick = () => {
            this.setEditEndMode("ccw");
        }
        this.btnChangeEndCW.onclick = () => {
            this.setEditEndMode("cw");
        }
        this.btnChangeEndStraight.onclick = () => {
            this.setEditEndMode("straight");
        }
        this.btnPaste.onclick = () => {
            this.addArrowFromText();
        }
        this.btnExtract.onclick = () => {
            this.extractDataFromArrow();
        }

        this.xpos = document.getElementById("xpos") as HTMLInputElement || document.createElement("input");
        this.ypos = document.getElementById("ypos") as HTMLInputElement || document.createElement("input");
        this.txtMessage = document.getElementById("txtMessage") as HTMLInputElement || document.createElement("input");

        this.xpos.oninput = this.changeXPos.bind(this);
        this.ypos.oninput = this.changeYPos.bind(this);
        this.txtMessage.oninput = this.changeMessage.bind(this);

        this.curveSlider = document.getElementById("curveslide") as HTMLInputElement || document.createElement("input");
        this.wslide = document.getElementById("wslide") as HTMLInputElement || document.createElement("input");
        this.animateslide = document.getElementById("animateslide") as HTMLInputElement || document.createElement("input");


        this.dropdownStyle.onchange = this.changeSelectedStyle.bind(this);
        this.dropdownStyle2.onchange = this.selectStyle.bind(this);

        this.dropdownEnd.onchange = this.selectedArrowEnd.bind(this);
        this.dropdownEnd2.onchange = this.selectEnd.bind(this);

        this.curveSlider.oninput = this.changeCurve.bind(this);
        this.wslide.oninput = this.changeStrokeWidth.bind(this);
        this.animateslide.oninput = this.changeAnimationSpeed.bind(this);

        this.btnAnimate.onclick = () => {
            if (this.selectedArrow) {
               // this.selectedArrow.animateProgress();
                this.selectedArrow.startProgress(.1,this.selectedArrow.animateSpeed/4);
            }
        }
    }
    private getButton(id: string) {
        return document.getElementById(id) as HTMLButtonElement || document.createElement("button");
    }
    changeXPos(e: any) {
        if (this.selectedArrow) {
            this.selectedArrow.x = e.target.value;
        }
    }
    changeYPos(e: any) {
        if (this.selectedArrow) {
            this.selectedArrow.y = e.target.value;
        }
    }
    changeMessage(e: any) {
        if (this.selectedArrow) {
            this.selectedArrow.setMessage(e.target.value);
        }
    }
    showEditEndChoices(val: boolean) {
        if (val == true) {
            this.btnEditEndPoint.style.display = "none";
            this.editEndChoices.style.display = "block";
        }
        else {
            this.btnEditEndPoint.style.display = "block";
            this.editEndChoices.style.display = "none";
        }
    }
    changeLen(e: any) {
        if (this.selectedArrow) {
            this.selectedArrow.setLen(e.target.value)
        }
    }
    changeCurve(e: any) {
        if (this.selectedArrow) {
            this.selectedArrow.setCurveH(e.target.value)
        }
    }
    changeAnimationSpeed(e: any) {
        if (this.selectedArrow) {
            this.selectedArrow.animateSpeed = e.target.value;
        }
    }
    changeStrokeWidth(e: any) {
        if (this.selectedArrow) {
            this.selectedArrow.setStrokeWidth(e.target.value);
        }
    }
    selectStyle(e: any) {
        this.color = e.target.value.toLowerCase();
        this.styleIndex = e.target.selectedIndex;

    }
    changeSelectedStyle(e: any) {
        if (this.selectedArrow) {

            this.selectedArrow.setFill(e.target.value.toLowerCase());
        }
        this.color = e.target.value.toLowerCase();
    }
    selectCurve(e: any) {
        this.curvePosition = e.target.value;
    }
    selectEnd(e: any) {
        let index: number = e.target.selectedIndex;
        this.endingIndex = index;

    }
    selectedArrowEnd(e: any) {
        let index: number = e.target.selectedIndex;
        if (this.selectedArrow) {
            this.addEndings(this.selectedArrow, index);
        }
    }
    addEndings(arrow: EditArrow | SvgArrow, index: number) {
        arrow.endIndex = index;
        switch (index) {
            case 0:
                arrow.setEnd("arrowpoint");
                arrow.setStart("");
                break;

            case 1:
                arrow.setEnd("");
                arrow.setStart("arrowpoint");
                break;

            case 2:
                arrow.setEnd("");
                arrow.setStart("");
                break;

            case 3:
                arrow.setEnd("arrowpoint");
                arrow.setStart("arrowpoint");
                break;
        }

    }
    setEditMode() {
        this.mode = "edit";

    }
    setAddMode() {
        if (this.selectedArrow) {
            // this.selectedArrow.remove();
            this.selectedArrow.removeStyle("selectedArrow");
            this.selectedArrow = null;
        }
        this.mode = "add";

    }
    setEditEndMode(curve: string = "straight") {
        if (this.selectedArrow) {
            this.startX = this.selectedArrow.x;
            this.startY = this.selectedArrow.y;

            this.mode = "editEnd";
            this.curvePosition = curve;
            this.isDown = true;
            document.addEventListener("mousemove", this.onMove.bind(this));
        }
    }
    create() {
        super.create();

        (window as any).scene = this;


        this.drawArrow = new SvgArrow(this, "arrowtemp2");
        if (this.drawArrow.el) {
            this.drawArrow.el.id = "drawArrow";
            this.drawArrow.setEnd("arrowpoint");
            this.drawArrow.setFill(this.color);
        }

        this.drawArrow.visible = false;

         document.addEventListener("mousedown", this.startRectDrag.bind(this));
        document.addEventListener("mouseup", this.stopDragRect.bind(this));
        document.addEventListener("mousemove", this.onMove.bind(this));

       


    }
    startAddArrow(curve: string) {
        this.curvePosition = curve;
        this.mode = "add";
        if (this.selectedArrow) {
            // this.selectedArrow.remove();
            this.selectedArrow.removeStyle("selectedArrow");
            this.selectedArrow.alpha = .5;
            this.selectedArrow = null;
        }
    }
    selectArrow(arrow: EditArrow) {

        this.showEditEndChoices(false);

        if (this.selectedArrow) {
            if (this.selectedArrow.el) {
                this.selectedArrow.removeStyle("selectedArrow");
                this.selectedArrow.alpha = .5;
            }
        }
        if (arrow === null) {
            return;
        }
        this.selectedArrow = arrow;
        if (this.selectedArrow.el) {
            this.setEditMode();
            this.selectedArrow.addStyle("selectedArrow");
            this.selectedArrow.alpha = 1;
        }
        //  console.log("endIndex="+arrow.endIndex);

        this.dropdownEnd.selectedIndex = arrow.endIndex;
        this.dropdownStyle.selectedIndex = arrow.colorIndex;
        this.curveSlider.value = this.selectedArrow.cy.toString();
        this.wslide.value = this.selectedArrow.strokeWidth.toString();
        this.animateslide.value = this.selectedArrow.animateSpeed.toString();

        this.xpos.value = arrow.x.toString();
        this.ypos.value = arrow.y.toString();

    }
    startRectDrag(e: MouseEvent) {

        if (this.mode !== "add") {
            return;
        }
        if (e.clientY < 100) {
            return;
        }
        this.isDown = true;
        this.startX = e.clientX;
        this.startY = e.clientY;


        this.drawArrow.x = e.clientX;
        this.drawArrow.y = e.clientY;
        this.drawArrow.visible = true;
    }
    onMouseMove(e: MouseEvent) {
        if (this.isDown === true) {
            //we need to get the relative position
            //by subtracting the current mouse position
            let w: number = e.clientX - this.startX;
            let h: number = e.clientY - this.startY;
            let angle = 0, perc = 0.5;


            switch (this.curvePosition) {
                case "cw": { angle = -45; perc = 0.75; break; }
                case "ccw": { angle = 45; perc = 0.75; break; }
            }
            let { rx, ry } = ArrowUtil.getRotate(0, 0, w, h, angle);
            this.rx = ArrowUtil.lerp(0, rx, perc);
            this.ry = ArrowUtil.lerp(0, ry, perc);

            if (this.mode === "editEnd") {
                if (this.selectedArrow) {
                    this.selectedArrow.curvePosition=this.curvePosition;
                    this.selectedArrow.setCurve(this.rx, this.ry);
                    this.selectedArrow.x = this.startX;
                    this.selectedArrow.y = this.startY;
                    this.selectedArrow.setLen(w);
                    this.selectedArrow.setH(h);
                    this.selectedArrow.setFill(this.color);
                }
            }
            else {
                /**set the properties on the arrow
                DON'T draw on the element
                we need to be able to edit and rebuild
                based on the properties**/

                this.drawArrow.setCurve(this.rx, this.ry);
                this.drawArrow.x = this.startX;
                this.drawArrow.y = this.startY;
                this.drawArrow.setLen(w);
                this.drawArrow.setH(h);
                this.drawArrow.setFill(this.color);

                this.addEndings(this.drawArrow, this.endingIndex);
            }
        }
    }
    stopDragRect(e: MouseEvent) {
        document.removeEventListener("mousemove", this.onMove.bind(this));
        this.endX = e.clientX;
        this.endY = e.clientY;


        if (this.mode == "editEnd") {
            if (this.selectedArrow) {
                let w: number = this.endX - this.startX;
                let h: number = this.endY - this.startY;
                this.selectedArrow.setCurve(this.rx, this.ry);
                this.selectedArrow.x = this.startX;
                this.selectedArrow.y = this.startY;
                this.selectedArrow.setLen(w);
                this.selectedArrow.setH(h);
                this.selectedArrow.setFill(this.color);
            }
            this.mode = "";
        }
        if (this.mode !== "add") {
            return;
        }
        if (e.clientY < 100) {
            return;
        }
        this.isDown = false;
        // this.dragRect.visible=false;

        this.drawArrow.visible = false;
        this.addArrow();

    }
    addArrowFromText() {
        let textArea: HTMLTextAreaElement | null = document.getElementById("pasteText") as HTMLTextAreaElement | null;
        if (textArea) {
            let text: string = textArea.value;
         //   console.log(text);

            this.addArrow();
            if (this.selectedArrow) {
                this.selectedArrow.fromString(text);
                this.selectArrow(this.selectedArrow);
            }
        }
    }
    extractDataFromArrow() {
        let textArea: HTMLTextAreaElement | null = document.getElementById("pasteText") as HTMLTextAreaElement | null;
        if (textArea) {
            if (this.selectedArrow) {
                textArea.value = this.selectedArrow.exportData();
            }
        }
    }
    addArrow() {


        let w: number = this.endX - this.startX;
        let h: number = this.endY - this.startY;

        console.log(this.startX,this.startY);
        console.log(this.endX,this.endY);
        console.log(w,h);

        let arrow: EditArrow = new EditArrow(this, "arrowtemp2");
        arrow.setCurve(this.rx, this.ry);
        arrow.x = this.startX;
        arrow.y = this.startY;
        arrow.setLen(w);
        arrow.setH(h);
        arrow.setFill(this.color);
        arrow.curvePosition=this.curvePosition;

        arrow.colorIndex = this.styleIndex;
        this.addEndings(arrow, this.endingIndex);

        //arrow.setEnd("arrowpoint");


        arrow.callback = this.selectArrow.bind(this);

        this.selectArrow(arrow);
        this.setEditMode();
    }
    /* lerp(start: number, end: number, t: number) {
        return (1 - t) * start + t * end
    }
    getRotate(cx: number, cy: number, ex: number, ey: number, angle: number) {
        let radians = (Math.PI / 180) * angle;
        let cos = Math.cos(radians);
        let sin = Math.sin(radians);
        let x = (cos * (ex - cx)) - (sin * (ey - cy)) + cx;
        let y = (cos * (ey - cy)) + (sin * (ex - cx)) + cy;
        return { rx: x, ry: y };
    }; */
    doResize() {
        super.doResize();

        // this.grid?.showGrid();
    }

}