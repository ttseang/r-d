import { BaseScreen, IScreen, SvgObj } from "svggame";
import { IconButton } from "../classes/IconButton";
import { SvgArrow } from "../classes/SvgArrow";

//import { SpineChar } from "spinechar";


export class xScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;
    private startX: number = 0;
    private startY: number = 0;

    private endY: number = 0;
    private endX: number = 0;

    //private dragRect: SvgObj;
  //  private selectRect: SvgObj;

    private drawArrow: SvgArrow;

    private onMove: Function;
    private isDown: boolean = false;

    private mode: string = "add";

    private btnEdit: HTMLButtonElement;
    private btnAdd: HTMLButtonElement;

    private selectedArrow: SvgArrow | null = null;

    private dropdownStyle:HTMLSelectElement;
    private dropdownCurve:HTMLSelectElement;
    private dropdownEnd:HTMLSelectElement;

    private lenSlider:HTMLInputElement;
    private curveSlider:HTMLInputElement;
    private color:string="black";

    private curvePosition = "-"; // - straight, cw clockwise, ccw counter-clockwise

    constructor() {
        super("ScreenMain");

        //this.selectRect = new SvgObj(this, "dragRect");
        this.drawArrow = new SvgArrow(this, "arrowtemp");

        //  this.gm.regFontSize("instructions", 26, 1000);

        (window as any).sm = this;
        this.onMove = this.onMouseMove.bind(this);

        this.btnEdit = document.getElementById("btnEdit") as HTMLButtonElement || document.createElement("button");
        this.btnAdd = document.getElementById("btnAdd") as HTMLButtonElement || document.createElement("button");

        this.dropdownStyle=document.getElementById("styleSelect") as HTMLSelectElement || document.createElement("select");
        this.dropdownCurve=document.getElementById("selectCurve") as HTMLSelectElement || document.createElement("select");
        this.dropdownEnd=document.getElementById("arrowSelect") as HTMLSelectElement || document.createElement("select");


        this.lenSlider=document.getElementById("lenslide")  as HTMLInputElement || document.createElement("input");
        this.curveSlider=document.getElementById("curveslide") as HTMLInputElement || document.createElement("input");


        this.btnEdit.onclick = () => {
           this.setEditMode();
        }
        this.btnAdd.onclick = () => {
           this.setAddMode();
        }
        this.dropdownStyle.onchange=this.selectStyle.bind(this);
        this.dropdownCurve.onchange=this.selectCurve.bind(this);
        this.dropdownEnd.onchange=this.selectEnd.bind(this);

        this.lenSlider.oninput=this.changeLen.bind(this);
        this.curveSlider.oninput=this.changeCurve.bind(this);
    }
    changeLen(e:any)
    {
        if (this.selectedArrow)
        {
            this.selectedArrow.setLen(e.target.value)
        }
    }
    changeCurve(e:any)
    {
        if (this.selectedArrow)
        {
            this.selectedArrow.setCurveH(e.target.value)
        }
    }
    selectStyle(e:any)
    {
        console.log(e.target.value);
        this.color=e.target.value.toLowerCase();
        if (this.selectedArrow)
        {
            
            this.selectedArrow.setFill(e.target.value.toLowerCase());

          //  this.selectedArrow.addClass(e.target.value.toLowerCase());
        }
    }
    selectCurve(e:any)
    {
        this.curvePosition = e.target.value;
        // console.log(e.target.selectedIndex);
        // if (this.selectedArrow)
        // {
        //     if (e.target.selectedIndex===0)
        //     {
        //         this.selectedArrow.flipUp();
        //     }
        //     else
        //     {
        //         this.selectedArrow.flipDown();
        //     }
        // }

    }
    selectEnd(e:any)
    {
        let index:number=e.target.selectedIndex;
        if (this.selectedArrow)
        {
        switch(index)
        {
            case 0:
                this.selectedArrow.setEnd("arrowpoint");
                this.selectedArrow.setStart("");
            break;

            case 1:
                this.selectedArrow.setEnd("");
                this.selectedArrow.setStart("arrowpoint");
            break;

            case 2:
                this.selectedArrow.setEnd("");
                this.selectedArrow.setStart("");
            break;

            case 3:
                this.selectedArrow.setEnd("arrowpoint");
                this.selectedArrow.setStart("arrowpoint");
            break;
        }
    }
    }
    setEditMode()
    {
        this.mode = "edit";
        this.btnAdd.style.backgroundColor = "cadetblue";
        this.btnEdit.style.backgroundColor = "green";
    }
    setAddMode()
    {
        if (this.selectedArrow)
        {
            this.selectedArrow.remove();
            this.selectedArrow=null;
        }
        this.mode = "add";
        this.btnAdd.style.backgroundColor = "green";
        this.btnEdit.style.backgroundColor = "cadetblue";
    }
    create() {
        super.create();

        (window as any).scene = this;

       
        this.drawArrow = new SvgArrow(this, "arrowtemp");
        if (this.drawArrow.el)
        {
            this.drawArrow.el.id="drawArrow";
            this.drawArrow.setEnd("arrowpoint");
            this.drawArrow.setFill(this.color);
        }
       
        this.drawArrow.visible = false;

       

        document.addEventListener("mousedown", this.startRectDrag.bind(this));
        document.addEventListener("mouseup", this.stopDragRect.bind(this));
        document.addEventListener("mousemove", this.onMove.bind(this));

    }
    selectArrow(arrow: SvgArrow) {

        
        this.selectedArrow = arrow;
        if (this.selectedArrow.el) {
            let hh: number = this.selectedArrow.el.getBoundingClientRect().height;
            
            this.setEditMode();
        //    this.selectedArrow.addClass("glow");
        }
        
        this.curveSlider.value=this.selectedArrow.cy.toString();
        this.lenSlider.value=this.selectedArrow.len.toString();
   
    }
    startRectDrag(e: MouseEvent) {

        if (this.mode !== "add") {
            return;
        }
        if (e.clientY < 100) {
            return;
        }
        this.isDown = true;
        this.startX = e.clientX;
        this.startY = e.clientY;
        
        // OLD
        // this.drawArrow.x = e.clientX;
        // this.drawArrow.y = e.clientY;
        this.drawArrow.visible = true;
    }
    onMouseMove(e: MouseEvent) {
        if (this.isDown === true) {
            let w: number = e.clientX - this.startX;
            let h: number = e.clientY - this.startY;

            // OLD
            // this.drawArrow.setLen(w);
            // this.drawArrow.setH(h);
            // this.drawArrow.setFill(this.color);

            const lerp = (start: number, end: number, t: number) => (1 - t) * start + t * end;

            const getDistance = (x1: number, y1: number, x2: number, y2: number) => {
                const dx = x2 - x1, dy = y2 - y1;
                return Math.sqrt(dx * dx + dy * dy);
            };

            const getAngle = (cx: number, cy: number, ex: number, ey: number, toDeg: boolean = false) => {
                let theta = Math.atan2(ey - cy, ex - cx); // range (-PI, PI]
                return toDeg ? theta * 180 / Math.PI : theta;
            };

            const getRotate = (cx: number, cy: number, ex: number, ey: number, angle: number) => {
                let radians = (Math.PI / 180) * angle;
                let cos = Math.cos(radians);
                let sin = Math.sin(radians);
                let x = (cos * (ex - cx)) - (sin * (ey - cy)) + cx;
                let y = (cos * (ey - cy)) + (sin * (ex - cx)) + cy;
                return { x: x, y: y};
            };

            let angle = 0, perc = 0.5;
            switch (this.curvePosition) {
                case "cw": { angle = -45; perc = 0.75; break; }
                case "ccw": { angle = 45; perc = 0.75; break; }
            }

            let { x, y } = getRotate(this.startX, this.startY, e.clientX, e.clientY, angle);
            x = lerp(this.startX, x, perc);
            y = lerp(this.startY, y, perc);
            console.log(x,y);
            let drawString:string=`M ${this.startX} ${this.startY} Q ${x} ${y} ${e.clientX} ${e.clientY}`;
            console.log(drawString);
            
            this.drawArrow.el!.setAttribute("d", drawString);

            //  this.drawArrow.el?.setAttribute("height", h.toString());

        }
    }
    stopDragRect(e: MouseEvent) {
        if (this.mode !== "add") {
            return;
        }
        if (e.clientY < 100) {
            return;
        }
        this.isDown = false;
        // this.dragRect.visible=false;
        document.removeEventListener("mousemove", this.onMove.bind(this));
        this.endX = e.clientX;
        this.endY = e.clientY;
        
        // OLD
        // this.drawArrow.visible = false;
        // this.addArrow();
        
    }
    addArrow() {
        console.log("add arrow");
        let arrow: SvgArrow = new SvgArrow(this, "arrowtemp");
        arrow.x = this.startX;
        arrow.y = this.startY;
        

        switch(this.dropdownEnd.selectedIndex)
        {
            case 0:
                arrow.setEnd("arrowpoint");
                arrow.setStart("");
            break;

            case 1:
                arrow.setEnd("");
                arrow.setStart("arrowpoint");
            break;

            case 2:
                arrow.setEnd("");
                arrow.setStart("");
            break;

            case 3:
                arrow.setEnd("arrowpoint");
                arrow.setStart("arrowpoint");
            break;
        }
        //arrow.setEnd("arrowpoint");

        arrow.setFill(this.color);

        console.log(this.startX,this.startY,this.endX,this.endY);
        
        let w: number = this.endX - this.startX;
        let h: number = this.endY - this.startY;

        arrow.setLen(w);
        arrow.setH(h);

       // arrow.callback = this.selectArrow.bind(this);

        this.drawArrow.setLen(w);
        this.drawArrow.setH(h);

        this.selectArrow(arrow);
        this.setEditMode();
    }
    doResize() {
        super.doResize();

        // this.grid?.showGrid();
    }

}