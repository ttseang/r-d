
import { IDragObj, IDragTarg } from "dragengsvg";
import { DragEng } from "dragengsvg/classes/DragEng";
import { BaseScreen, IScreen, PosVo, SvgObj, TextObj } from "svggame";
import { SpineChar } from "../classes/char/SpineChar";

import { IconButton } from "../classes/IconButton";
import { LetterBox } from "../classes/LetterBox";

import { TargetBox } from "../classes/TargetBox";
import { LinkUtil } from "../classes/util/LinkUtil";


import { AnimationVo } from "../dataObjs/AnimationVo";
import { WordVo } from "../dataObjs/WordVo";
//import { SpineChar } from "spinechar";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private levels: WordVo[] = [];
    private levelIndex: number = -1;
    private currentWord: WordVo = new WordVo("", []);

    //private mainImage: SvgObj | null = null;
    private correctLetter: string = "";


    private targetBox: TargetBox | null = null;
    private dragEng: DragEng;

    private idleAnimations: AnimationVo[] = [];
    private walkAnimations: AnimationVo[] = [];
    private fidgetAnimations: AnimationVo[] = [];
    private fidgetAnimations2: AnimationVo[] = [];

    private spineChar!: SpineChar;

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);

        this.dragEng = new DragEng(this);
        this.dragEng.snapToTarget = true;

        this.idleAnimations = [new AnimationVo("idle", true), new AnimationVo("Repeating animations/blink", true)];
        this.walkAnimations = [new AnimationVo("walk", true), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations = [new AnimationVo("fidget01", false), new AnimationVo("Repeating animations/blink", true)];
        this.fidgetAnimations2 = [new AnimationVo("fidget02", false), new AnimationVo("Repeating animations/blink", true)];

        this.spineChar = new SpineChar(this, "mascot", "./assets/char/Amka.json", "./assets/char/Amka.atlas", this.idleAnimations);
        this.spineChar.currentPos = new PosVo(5, 10);
        this.spineChar.scaleToGameH(.2);
        this.spineChar.adjustY();


        setTimeout(() => {
            //this.spineChar.moveX(-10,walkAnimations,charAnimations,3000);
        }, 2000);

    }
    create() {
        super.create();

        (window as any).scene = this;

        /**
        * SET CALLBACK ON THE DRAG ENGINE
        */

        this.dragEng.upCallback = this.onUp.bind(this)
        this.dragEng.dropCallback = this.onDrop.bind(this);
        this.dragEng.collideCallback = this.onCollide.bind(this);

        this.loadData();
    }

    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        data = data.data;

        for (let i: number = 0; i < data.length; i++) {
            let wordVo: WordVo = new WordVo(data[i].word, data[i].letters);
            this.levels.push(wordVo);
        }
        //console.log(data);
        this.nextLevel();
        this.dragEng.setListeners();


        LinkUtil.makeLink("resetButton",this.onbuttonPressed.bind(this),"reset","");
        LinkUtil.makeLink("checkButton",this.onbuttonPressed.bind(this),"check","");
    }
    setImage(image:string)
    {
        let img:HTMLImageElement=document.getElementById("mainImage") as HTMLImageElement;
        img.src="./assets/pics2/"+image+".svg";

    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
          switch (action) {
             case "reset":
                 this.dragEng.clickLock = false;
                 this.dragEng.resetBoxes();
                 break;
 
             case "check":
                 this.checkCorrect();
                 break;
         }
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            console.log("out of words");
            return;
        }
       /*  if (this.mainImage) {
            this.mainImage.destroy();

        } */

        this.currentWord = this.levels[this.levelIndex];

        this.correctLetter = this.currentWord.word.substr(0, 1).toLowerCase();

        this.setImage(this.currentWord.word);
        console.log(this.currentWord.word);
        
       /*  this.mainImage = new SvgObj(this, this.currentWord.word,"addArea");
        this.mainImage.gameWRatio = 0.2;
        this.grid?.placeAt(7, 3, this.mainImage); */

        this.makeBoxes();
    }
    makeBoxes() {

      
        this.dragEng.resetBoxes();
        this.dragEng.destoryDrags();
        this.dragEng.destroyTargs();

        this.targetBox = new TargetBox(this);
        this.targetBox.gameWRatio = 0.08;
        this.targetBox.setPlace(new PosVo(4, 3));

        this.dragEng.addTarget(this.targetBox);

        for (let i: number = 0; i < this.currentWord.chars.length; i++) {
            let box: LetterBox = new LetterBox(this);
            box.setText(this.currentWord.chars[i]);
            box.gameWRatio = 0.08;
            box.initPlace(new PosVo(3 + i, 6));
            this.dragEng.addDrag(box);
        }



    }
    onCollide(d: IDragObj, t: IDragTarg) {
        //console.log("collide");
        d.canDrag = false;
        this.dragEng.clickLock = true;
    }
    onDrop() {
        //console.log("dropped");
        //console.log(this.dragEng.getTargetContent());

        //check drop content here
    }
    onUp() {
        //console.log("up");
    }
    checkCorrect() {

        if (this.correctLetter == "") {
            return;
        }
        if (this.correctLetter !== this.dragEng.getTargetContent()) {

            this.playSound(this.wrongSound);
            this.spineChar.setAnimations(this.fidgetAnimations);
        }
        else {


            //console.log("Right");
            this.playSound(this.rightSound);

            this.spineChar.setAnimations(this.fidgetAnimations2);

            setTimeout(() => {
                this.spineChar.setAnimations(this.idleAnimations);
            }, 1000);


            setTimeout(() => {
                this.dragEng.clickLock = false;
                this.nextLevel();
            }, 1500);
        }
    }
    doResize() {
        super.doResize();


        // this.grid?.showGrid();


    }

}