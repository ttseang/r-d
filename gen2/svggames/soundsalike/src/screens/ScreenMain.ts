
import { BaseScreen, HtmlObj, IScreen, SvgObj } from "svggame";
import { PicCard } from "../classes/picCard";


import { LinkUtil } from "../classes/util/LinkUtil";
import { CardVo } from "../dataObjs/CardVo";
import { LevelVo } from "../dataObjs/LevelVo";




export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;
    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";


    private levelIndex: number = -1;

    public cards: PicCard[] = [];
    public bigCard: PicCard;
    public levels: LevelVo[] = [];

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);

        this.bigCard = new PicCard("bigcard", this.hideBigCard.bind(this), () => { });
        this.bigCard.setImage("cow");
        setTimeout(() => {
            //this.spineChar.moveX(-10,walkAnimations,charAnimations,3000);
        }, 2000);

    }
    create() {
        super.create();

        (window as any).scene = this;

        for (let i = 0; i < 3; i++) {
            let card: PicCard = new PicCard("picard" + i, this.showBigCard.bind(this), this.selectCard.bind(this));
            card.visible = true;
            card.x = 15 + i * 25;
            card.y = 30;
            this.cards.push(card);
            card.setImage("shrimp");
            card.setSelected(false);
        }

         this.getPrevData();
     //   this.loadData();
    }
    getPrevData()
    {
        let dataDiv:HTMLElement | null=parent.document.getElementById("gameData");
        if (dataDiv)
        {
            let dataString:string=dataDiv.innerText;
            console.log(dataString);
            let data:any=JSON.parse(dataString);
            this.gotData(data);
        }
        

    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        let instructions:string=data.instructions;
        console.log(instructions);

        let inDiv:HTMLElement | null=document.getElementById("instructions");
        if (inDiv)
        {
            inDiv.innerText=instructions;
        }

        data = data.levels;
       

        console.log(data);
        for (let i: number = 0; i < data.length; i++) {
            let cardData: any = data[i].cards;
            console.log(cardData);
             let levelVo: LevelVo = new LevelVo(i, []);
            for (let j: number = 0; j < cardData.length; j++) {
                let cardVo: CardVo = new CardVo(cardData[j].image, cardData[j].correct);
                levelVo.cards.push(cardVo);
            }
            this.levels.push(levelVo);
        }
        //console.log(data);
        this.nextLevel();



        LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", "");
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");
    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics2/" + image + ".svg";

    }
    showBigCard(card: PicCard) {
        this.showAllCards();
        this.bigCard.setImage(card.image);
        this.bigCard.visible = true;
        card.visible = false;
    }
    hideBigCard(card: PicCard) {
        this.bigCard.visible = false;
        this.showAllCards();
    }
    showAllCards() {
        for (let i: number = 0; i < this.cards.length; i++) {
            this.cards[i].visible = true;
        }
    }
    resetCards() {
        for (let i: number = 0; i < this.cards.length; i++) {
            this.cards[i].setSelected(false);
        }
    }
    selectCard(card: PicCard) {
        card.setSelected(!card.selected);
    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
        switch (action) {
            case "reset":
                this.resetCards();
                break;

            case "check":
                let isCorrect:boolean=this.checkCorrect();
                console.log(isCorrect);
                if (isCorrect==true)
                {
                    this.playSound(this.rightSound);
                    this.nextLevel();
                }
                else
                {
                    this.playSound(this.wrongSound);
                    this.resetCards();
                }
                break;
        }
    }

    nextLevel() {
        this.resetCards();
         this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            console.log("out of words");
            alert("Out of Cards");
            return;
        }

        this.makeCards();
    }
    makeCards() {
        let currentLevel:LevelVo=this.levels[this.levelIndex];
        console.log(currentLevel);
        let cardData:CardVo[]=currentLevel.cards;
        for (let i:number=0;i<cardData.length;i++)
        {
            this.cards[i].setImage(cardData[i].image);
        }
    }

    checkCorrect() {

        let currentLevel:LevelVo=this.levels[this.levelIndex];
        let cardData:CardVo[]=currentLevel.cards;

        for (let i:number=0;i<cardData.length;i++)
        {
            if (this.cards[i].selected!=cardData[i].correct)
            {
                return false;
            }
        }
        return true;
    }
    doResize() {
        super.doResize();


        // this.grid?.showGrid();


    }

}