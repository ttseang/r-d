export class CardVo
{
    public image:string;
    public correct:boolean;

    constructor(image:string,correct:boolean)
    {
        this.image=image;
        this.correct=correct;
    }
}