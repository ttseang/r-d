import { HtmlObj } from "svggame";

export class PicCard extends HtmlObj {
    private btnResize: HTMLImageElement | null = null;
    private mainImage: HTMLImageElement | null = null;
    public image: string = "";
    public selected: boolean = false;

    constructor(key: string, iconCallback: Function, cardCallback: Function) {
        super(key);
        if (this.el) {
            let pics: HTMLCollectionOf<HTMLImageElement> = this.el.getElementsByTagName("img");
            this.btnResize = pics[1];
            this.mainImage = pics[0];

            this.btnResize.onclick = (e: MouseEvent) => {
                iconCallback(this);

                e.stopImmediatePropagation();
            }
            this.el.onclick = () => {
                cardCallback(this);
            }
        }
    }
    public setImage(path: string) {
        this.image = path;
        if (this.mainImage) {
            this.mainImage.src = "./assets/pics/" + path;
        }

    }
    public setSelected(val: boolean) {
        this.selected = val;
        if (this.el) {
            let selectVal: string = (val == true) ? "pselected" : "unselected";
            this.el.classList.remove("unselected");
            this.el.classList.remove("pselected");
            this.el.classList.add(selectVal);
            //=["piccard",selectVal,"shadow"];
        }
    }
}