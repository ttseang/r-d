import { GameOptions, SVGGame } from "@teachingtextbooks/svggame";
import { TanScreen } from "./classes/screens/TanScreen";


window.onload=function()
{
    let opts:GameOptions=new GameOptions();
    opts.useFull=true;
    opts.addAreaID="addArea";
    opts.screens=[new TanScreen()]

    let game:SVGGame=new SVGGame("myCanvas",opts);
}