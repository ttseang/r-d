import { BaseScreen, IScreen } from "@teachingtextbooks/svggame";
import { SvgObj } from "@teachingtextbooks/svggame/classes/core/gameobjects/SvgObj";

export class TanScreen extends BaseScreen implements IScreen {
    private shapeArray: string[] = ["hexagon", "trapezoid", "rhombus", "triangle", "square", "rhombus2"];
    private currentItem: SvgObj | null = null;
    private shapes: SvgObj[] = [];
    private shapeCount: number = 0;
    private addArea: SVGSVGElement;
    constructor() {
        super("TanScreen");
        this.addArea=document.getElementById("addArea") as unknown as SVGSVGElement;
        (window as any).tanScreen = this;
    }
    create() {
        super.create();
        this.addShapesToToolbar();
       // document.addEventListener("pointerdown", (e) => this.getItemClicked(e));
        document.addEventListener("pointermove", (e) => this.moveItem(e));
     //   document.addEventListener("pointerup", (e) => this.dropItem(e));
    }
    addShapesToToolbar() {
        for (let i = 0; i < this.shapeArray.length; i++) {

            //make rectangle
            let rect: SvgObj = new SvgObj(this, "rect", "toolbar");
            rect.x = 8;
            rect.y = 3 + i * 8;
            rect.setScale(8);

            let svgObj: SvgObj = new SvgObj(this, this.shapeArray[i], "toolbar");
            svgObj.x = 12;
            svgObj.y = 7 + i * 8;
            svgObj.setScale(.06);
            // svgObj.el?.setAttribute("trasnform-origin", "50% 50%");
            console.log(svgObj.displayWidth);
            svgObj.el?.addEventListener("click", () => {
                this.addShapeToCanvas(this.shapeArray[i]);
            });
        }
    }
    addShapeToCanvas(shape: string) {
        let svgObj: SvgObj = new SvgObj(this, shape, "addArea");
        svgObj.setScale(.05);
        svgObj.x = 50;
        svgObj.y = 25;
        if (svgObj.el) {
            svgObj.el.id = "shape" + this.shapeCount;
        }
        svgObj.el?.addEventListener("click", () => {
            this.itemClicked(svgObj);
        });
        this.shapes.push(svgObj);
        this.currentItem = svgObj;
    }
    itemClicked(svgObj: SvgObj) {
        console.log("item clicked");
        this.currentItem = svgObj;
    }

   /*  getItemClicked(e: PointerEvent) {
        //  console.log("item clicked");
        let target = e.target as HTMLElement;
        //  console.log(target);
        if (target) {
            if (this.startsWith(target.id, "shape")) {
                console.log("shape clicked");
                this.currentItem = this.findShape(target.id);
               
            }
        }
    } */
    findShape(id: string) {
        for (let i = 0; i < this.shapes.length; i++) {
            if (this.shapes[i].el?.id == id) {
                return this.shapes[i];
            }
        }
        return null;
    }
    moveItem(e: PointerEvent) {
        e.preventDefault();
       //put item under mouse
        if (this.currentItem && this.addArea) {
           //convert mouse position to a percentage of the add area
            let x = e.clientX - this.addArea.getBoundingClientRect().left;
            let y = e.clientY - this.addArea.getBoundingClientRect().top;
            let xPct = x / this.addArea.clientWidth;
            let yPct = y / this.addArea.clientHeight;
            //convert percentage to a position in the add area
            let xAdd = xPct * this.addArea.clientWidth;
            let yAdd = yPct * this.addArea.clientHeight;
            //set the position of the item
            this.currentItem.x = xAdd;
            this.currentItem.y = yAdd;
            
        }
    }
    dropItem(e: PointerEvent) {
        if (this.currentItem) {
          
          //  this.currentItem = null;
        }
    }

    startsWith(str: string, prefix: string) {
        return str.slice(0, prefix.length) == prefix;
    }

}