import { IScreen, SvgObj } from "svggame";

export class LetterBox extends SvgObj {
    public text: string = "";
    private textEl: SVGTextElement | null = null;
    public row: number = -1;
    public col: number = -1;
    public back: SVGRectElement | null | undefined = null;
    private callback: Function = () => { };
    public enabled: boolean = true;

    constructor(scene: IScreen) {
        super(scene, "letterbox");
        this.back = this.el?.querySelector("#boxBack");


        // this.setContent("B");
    }
    setEnabled(val: boolean) {
        this.enabled = val;
        if (this.back) {
            if (val === true) {
                this.back.setAttribute("fill", "#00ff00");
            }
            else {
                this.back.setAttribute("fill", "#ff0000");
            }
        }
    }
    reset() {
        this.enabled=true;
        if (this.back) {
            this.back.setAttribute("fill", "wheat");
        }
    }
    setCallback(f: Function) {
        this.callback = f;
        if (this.back) {
            this.back.onclick = () => { this.callback(this) }
        }
    }
    setText(text: string) {
        this.text = text;

        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            this.textEl.innerHTML = text;
        }
    }

    doResize() {
        this.updateScale();
        super.doResize();
    }
}