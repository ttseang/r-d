
import { AlignGrid, BaseScreen, IScreen, SvgObj, TextObj } from "svggame";

import { IconButton } from "../classes/IconButton";
import { LetterBox } from "../classes/LetterBox";


export class ScreenMain extends BaseScreen implements IScreen {

    public boxes: LetterBox[][] = [];

    private uiWindow!: SvgObj;
    public words: string[] = [];
    public gameWords: string[] = [];

    public selectedWord: string = "";
    public selectedLetters: LetterBox[] = [];

    private buttons: SvgObj[] = [];
    private instructionText: TextObj | null = null;

    private rowFlag: boolean = false;

    private selectedRow: number = -1;
    private selectedCol: number = -1;

    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";
    private clickLock: boolean = false;

    private btnReset!: IconButton;
    private levels:any[]=[];
    private levelIndex:number=0;

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);

        for (let i: number = 0; i < 4; i++) {
            this.boxes[i] = [];
        }
    }
    create() {
        super.create();

        (window as any).scene = this;

        this.uiWindow = new SvgObj(this, "uiWindow");
        this.uiWindow.gameWRatio = .9;

        let scale: number = this.gw / 4000;

        this.grid = new AlignGrid(20, 20, this.gw, this.gh);
      //  this.grid.showGrid();

        this.btnReset = new IconButton(this, "btnReset", "reset", "", this.onbuttonPressed.bind(this));
        this.btnReset.gameWRatio = 0.025;
        /*  this.btnPlay = new IconButton(this, "btnPlay", "play", "", this.onbuttonPressed.bind(this));
         
         this.btnCheck = new IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
         this.btnHelp = new IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));
 
         this.buttons = [this.btnHelp, this.btnPlay, this.btnReset, this.btnCheck]; */
        // this.instructionText = new TextObj(this, "instText");

        for (let i: number = 0; i < 4; i++) {
            for (let j: number = 0; j < 4; j++) {
                let letterbox = new LetterBox(this);
                letterbox.gameWRatio = .07;
                letterbox.setText("-");
                this.boxes[i][j] = letterbox;
            }
        }



        this.loadWords();

    }
    loadWords() {
        fetch("./assets/words.txt")
            .then(response => response.text())
            .then(data => this.gotWords(data));
    }
    gotWords(data: any) {
        //console.log(data);
        this.words = data.split("|");

       /*  for (let i: number = 0; i < 4; i++) {
            let r: number = Math.floor(Math.random() * this.words.length);
            this.gameWords.push(this.words[r]);
        } */

        this.loadLevels();
    }
    loadLevels()
    {
        fetch("./assets/levels.json")
        .then(response => response.json())
        .then(data => this.levelsLoaded({ data }));
    }
    levelsLoaded(data:any)
    {
        console.log(data);
        this.levels=data.data.levels;
        this.setLevel(0);
        setTimeout(() => {
            this.placeItems();
        }, 500);
    }
    setLevel(level:number)
    {
        this.gameWords=this.levels[level].words;

    }
    select(letterbox: LetterBox) {
        if (letterbox.enabled === false) {
            return;
        }
        if (this.clickLock === true) {
            return;
        }
        this.playSound(this.popSound);
        
        this.selectedCol = letterbox.col;
        this.selectedRow = letterbox.row;

        this.selectedWord += letterbox.text;

        if (this.selectedWord.length === 4) {
            this.clickLock = true;
        }

        this.rowFlag = !this.rowFlag;

        for (let i: number = 0; i < 4; i++) {
            for (let j: number = 0; j < 4; j++) {
                let letterbox2 = this.boxes[i][j];
                if (this.rowFlag === true) {
                    if (this.selectedRow === letterbox2.row) {
                        letterbox2.setEnabled(true);
                    }
                    else {
                        letterbox2.setEnabled(false);
                    }
                }
                else {
                    if (this.selectedCol === letterbox2.col) {
                        letterbox2.setEnabled(true);
                    }
                    else {
                        letterbox2.setEnabled(false);
                    }
                }
                if (this.selectedWord.length === 4) {
                    letterbox2.setEnabled(false);
                }
            }

        }
        letterbox.setEnabled(false);
        this.showWord();
    }
    placeItems() {
        this.clickLock=false;
        if (this.grid) {
            for (let i: number = 0; i < 4; i++) {
                for (let j: number = 0; j < 4; j++) {
                    let letterbox = this.boxes[i][j];
                    this.grid.placeAt(3.5 + i * 1.2, 2 + j * 1.8, letterbox);
                    letterbox.setText(this.gameWords[j].substring(i, i + 1));
                    letterbox.setCallback(this.select.bind(this));

                    letterbox.col = i;
                    letterbox.row = j;
                }
            }
        }
        this.grid?.placeAt(9, 8, this.btnReset);
    }
    showWord() {

        for (let i: number = 0; i < this.selectedLetters.length; i++) {
            this.selectedLetters[i].destroy();
        }
        this.selectedLetters=[];
        for (let i: number = 0; i < this.selectedWord.length; i++) {
            let letterbox: LetterBox = new LetterBox(this);
            letterbox.gameWRatio = 0.075;
            letterbox.setText(this.selectedWord.substring(i, i + 1));
            this.selectedLetters.push(letterbox);
            if (this.grid) {
                this.grid.placeAt(3.5 + i * 1.2, 0, letterbox);
            }
        }
        if (this.selectedWord.length === 4) {
            this.checkWord()
        }
    }
    nextLevel()
    {
        this.levelIndex++;
        if (this.levelIndex>this.levels.length-1)
        {
            alert("Out Of Levels");
            this.clickLock=true;
            return;
        }
        this.setLevel(this.levelIndex);

        for (let i: number = 0; i < 4; i++) {
            for (let j: number = 0; j < 4; j++) {
                let letterbox = this.boxes[i][j];
                letterbox.reset();
                letterbox.setText("");
            }
        }
        this.selectedWord="";
        this.showWord();

        setTimeout(() => {
            this.placeItems();
        }, 1000);
    }
    checkWord() {
        if (this.words.includes(this.selectedWord)) {
            this.playSound(this.rightSound);
            setTimeout(() => {
                this.nextLevel();
            }, 1000);
        }
        else {
            this.playSound(this.wrongSound);
        }
    }
    mixUp() {
        for (let i: number = 0; i < 20; i++) {
            let xx: number = Math.floor(Math.random() * 4);
            let yy: number = Math.floor(Math.random() * 4);
            let letterbox = this.boxes[xx][yy];


            let xx2: number = Math.floor(Math.random() * 4);
            let yy2: number = Math.floor(Math.random() * 4);
            let letterbox2 = this.boxes[xx2][yy2];

            let letter1: string = letterbox.text;
            let letter2: string = letterbox2.text;

            letterbox2.setText(letter1);
            letterbox.setText(letter2);

        }
    }
    doReset() {
        this.selectedWord = "";
        this.rowFlag = false;
        this.clickLock=false;

        for (let i: number = 0; i < 4; i++) {
            for (let j: number = 0; j < 4; j++) {
                let letterbox = this.boxes[i][j];
                letterbox.reset();
            }
        }
        for (let i: number = 0; i < this.selectedLetters.length; i++) {
            this.selectedLetters[i].destroy();
        }
    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
        if (action === "reset") {
            this.doReset();
        }
    }

    doResize() {
        super.doResize();

        
        // this.grid?.showGrid();


        this.uiWindow.displayWidth = this.gw * 0.9;
        this.uiWindow.displayHeight = this.gh * 0.70;

        this.center(this.uiWindow, true);

        if (this.grid) {


            for (let i: number = 0; i < 4; i++) {
                for (let j: number = 0; j < 4; j++) {
                    let letterbox = this.boxes[i][j];

                    this.grid.placeAt(3.5 + i * 1.2, 2 + j * 1.8, letterbox);

                }
            }

            this.grid?.placeAt(9, 8, this.btnReset);

            this.showWord();
          
        }
        /* for (let i: number = 0; i < this.buttons.length; i++) {
            this.buttons[i].gameWRatio = 0.05;
            this.grid?.placeAt(2 + i * 2, 9.7, this.buttons[i]);
        } */


    }

}