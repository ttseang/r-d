import { IScreen, PosVo, SvgObj } from "svggame";

export class TWordBox extends SvgObj {
    public text: string = "";
    private textEl: SVGTextElement | null = null;
    private place:PosVo=new PosVo(0,0);

    constructor(scene: IScreen) {
        super(scene, "twordbox");
        // this.setContent("B");
    }
    setText(text: string) {
        this.text = text;
        
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            this.textEl.innerHTML = text;
        }
    }
    setPlace(pos: PosVo): void {
        this.place = pos;
        this.screen.grid?.placeAt(pos.x, pos.y, this);
     }
    doResize() {
        this.screen.grid?.placeAt(this.place.x, this.place.y, this);
        this.updateScale();
        super.doResize();
    }
}