import { HtmlObj } from "svggame";

export class TextWindow extends HtmlObj
{
    constructor()
    {
        super("textWindow");
    }
    setText(text:string)
    {
        if (this.el)
        {
            let tb:HTMLElement=this.el.getElementsByTagName("div")[0];
            tb.innerHTML=text;
        }
    }
}