import { CardVo } from "./CardVo";

export class LevelVo {
    public cards:CardVo[]=[];
    public index:number=0;
    public text:string;

    constructor(index:number,text:string,cards:CardVo[]) {
        this.index=index;
        this.text=text;
        this.cards=cards;
    }
}