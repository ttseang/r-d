
import { IDragObj, IDragTarg } from "dragengsvg";
import { DragEng } from "dragengsvg/classes/DragEng";
import { AlignGrid, BaseScreen, HtmlObj, IScreen, PosVo, SvgObj, TextObj } from "svggame";
import { LetterBox } from "../classes/LetterBox";


import { TargetBox } from "../classes/TargetBox";
import { TWordBox } from "../classes/TwordBox";
import { LinkUtil } from "../classes/util/LinkUtil";
import { WordBox } from "../classes/WordBox";
import { LetterRowVo } from "../dataObjs/LetterRowVo";
import { LetterVo } from "../dataObjs/LetterVo";
import { SingleLetterGame } from "../dataObjs/SingleLetterGame";


import { WordVo } from "../dataObjs/WordVo";
//import { SpineChar } from "spinechar";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";


    private levelIndex: number = -1;
    
    private droppedLetter:LetterVo | null=null;

    private wrongIcon: HtmlObj | null = null;
    private rightIcon: HtmlObj | null = null;

    private targetBox: TargetBox | null = null;
    private dragEng: DragEng;

    private currentBox: IDragObj | null = null;
    private nextFlag: boolean = false;

    private gameData: SingleLetterGame = new SingleLetterGame("", []);

    private instructionText: HtmlObj | null = null;

    

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);



        this.dragEng = new DragEng(this);
        this.dragEng.snapToTarget = true;



    }
    create() {
        super.create();

        (window as any).scene = this;

        /**
        * SET CALLBACK ON THE DRAG ENGINE
        */

        this.dragEng.upCallback = this.onUp.bind(this)
        this.dragEng.dropCallback = this.onDrop.bind(this);
        this.dragEng.collideCallback = this.onCollide.bind(this);
        this.dragEng.downCallback = this.startDrag.bind(this);

        //this.loadData();
        this.getPrevData();
        this.instructionText = new HtmlObj("instructions");


    }
    getPrevData() {
        let dataDiv: HTMLElement | null = parent.document.getElementById("gameData");
        if (dataDiv) {
            let dataString: string = dataDiv.innerText;
            console.log(dataString);
            let data: any = JSON.parse(dataString);
            this.gotData(data);
        }

    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {

        console.log(data);

        this.gameData.instructions = data.instructions;

        let levelData: any = data.levels;
        for (let i: number = 0; i < levelData.length; i++) {
            //console.log(levelData[i]);
            let level: LetterRowVo = new LetterRowVo(levelData[i].image, []);

            let letterData = levelData[i].letters;
            for (let j: number = 0; j < letterData.length; j++) {
                //console.log(letterData[j]);
                level.letters.push(new LetterVo(letterData[j].letter, letterData[j].correct));
            }
            this.gameData.cards.push(level);
        }

        console.log(this.gameData);

        this.nextLevel();
        this.dragEng.setListeners();

        this.setInstruction(this.gameData.instructions);

        //   LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", ""); */
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");
        LinkUtil.makeLink("btnNext", this.onbuttonPressed.bind(this), "next", "");
    }
    setInstruction(text: string) {

        if (!this.instructionText)
        {
            this.instructionText = new HtmlObj("instructions");
        }

        if (this.instructionText) {
            if (this.instructionText.el) {
                this.instructionText.el.innerText = text;
            }
        }

    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics/" + image;
    }
    onbuttonPressed(action: string, params: string) {
        //console.log(action);
        switch (action) {
            case "reset":
                if (this.nextFlag == false) {
                    this.dragEng.clickLock = false;
                    if (this.rightIcon) {
                        this.rightIcon.visible = false;
                    }
                    if (this.wrongIcon) {
                        this.wrongIcon.visible = false;
                    }
                    this.dragEng.resetBoxes();
                }
                break;

            case "check":
                this.checkCorrect();
                break;

            case "next":
                if (this.nextFlag == true) {
                    this.nextFlag = false;
                    this.nextLevel();
                }
                break;
        }
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.gameData.cards.length - 1) {
            alert("out of words");
            return;
        }
        this.dragEng.clickLock = false;
         
        

        /* this.currentWord = this.levels[this.levelIndex];

        this.correctWord = this.currentWord.correct; */

        if (this.rightIcon) {
            this.rightIcon.visible = false;
        }
        if (this.wrongIcon) {
            this.wrongIcon.visible = false;
        }
        this.setImage(this.gameData.cards[this.levelIndex].image);
        // this.setImage(this.currentWord.word);

        this.makeBoxes();
        
        this.doResize();
    }
    
    makeBoxes() {

        this.dragEng.resetBoxes();
        this.dragEng.destoryDrags();
        this.dragEng.destroyTargs();

        this.targetBox=new TargetBox(this);
        this.targetBox.gameWRatio=0.08;
        this.targetBox.setPlace(new PosVo(7,3));
        this.dragEng.addTarget(this.targetBox);

        for (let i: number = 0; i < this.gameData.cards[this.levelIndex].letters.length; i++) {
            let letter: LetterVo = this.gameData.cards[this.levelIndex].letters[i];
            //console.log(letter);

            let letterBox: LetterBox = new LetterBox(this);
            letterBox.canDrag = true;
            letterBox.index = i;
            letterBox.gameWRatio = 0.08;
            letterBox.setLetterVo(this.gameData.cards[this.levelIndex].letters[i]);
           // letterBox.setText(this.gameData.cards[this.levelIndex].letters[i].text);
            letterBox.initPlace(new PosVo(4 + i, 7));

            this.dragEng.addDrag(letterBox);
        }

        this.wrongIcon = new HtmlObj("wrongIcon");

        this.rightIcon = new HtmlObj("rightIcon");

        this.rightIcon.visible = false;
        this.wrongIcon.visible = false;
    }
    startDrag(d: IDragObj) {
        this.currentBox = d;
    }
    onCollide(d: IDragObj, t: IDragTarg) {
        console.log("collide");
        d.canDrag = false;

        this.droppedLetter=(d as LetterBox).letterVo;

        this.dragEng.clickLock = true;
    }
    onDrop() {
        console.log("dropped");
        console.log(this.dragEng.getTargetContent());
       
        //check drop content here
    }
    onUp() {
        console.log("up");
    }
    boxesOff() {
        for (let i: number = 0; i < this.dragEng.dragObjs.length; i++) {
            let box: IDragObj = this.dragEng.dragObjs[i];
            if (box != this.currentBox) {
                box.alpha = .5;
            }
        }
    }
    checkCorrect() {

        console.log(this.droppedLetter);

        // let correct:boolean=this.dragEng.getTargetContent() == this.currentWord.correct;
        let correct: boolean;
        
        if (this.droppedLetter===null)
        {
            correct=false;
        }
        else
        {
            correct=this.droppedLetter.correct;
        }

        if (correct) {
            if (this.nextFlag == false) {
                this.boxesOff();
                this.nextFlag = true;
                this.playSound(this.rightSound);

                //  this.usedWords.push(this.currentWord.correct);

                if (this.rightIcon) {
                    this.rightIcon.visible = true;
                }
            }
        }
        else {
            this.nextFlag = true;
            // this.usedWords.push(this.currentWord.correct);
            if (this.wrongIcon) {
                this.wrongIcon.visible = true;
            }
            this.playSound(this.wrongSound);
        }
    }
    doResize() {
        super.doResize();


    }

}