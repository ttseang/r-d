import { LetterVo } from "./LetterVo";

export class LetterRowVo
{
    public letters:LetterVo[];
    public image:string;

    constructor(image:string,letters:LetterVo[])
    {
        this.image=image;
        this.letters=letters;
    }
}