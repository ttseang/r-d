export class LetterVo
{
    public text:string;
    public correct:boolean;

    constructor(text:string,correct:boolean)
    {
        this.text=text;
        this.correct=correct;
    }
}