export class WordVo {
    public dragwords: string[]
    public twords: string;
    public correct: string;
    constructor(dragwords: string[], twords: string, correct: string) {
        this.dragwords = dragwords;
        this.twords = twords;
        this.correct = correct;
    }
}