
import { IDragObj, IDragTarg } from "dragengsvg";
import { AlignGrid, BaseScreen, HtmlObj, IScreen, PosVo, SvgObj, TextObj } from "svggame";
import { LetterBox } from "../classes/LetterBox";


import { LinkUtil } from "../classes/util/LinkUtil";

import { LetterRowVo } from "../dataObjs/LetterRowVo";
import { LetterVo } from "../dataObjs/LetterVo";
import { SingleLetterGame } from "../dataObjs/SingleLetterGame";



export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";


    private levelIndex: number = -1;
    
    private droppedLetter:LetterVo | null=null;

    private wrongIcon: HtmlObj | null = null;
    private rightIcon: HtmlObj | null = null;

   
    private nextFlag: boolean = false;

    private gameData: SingleLetterGame = new SingleLetterGame("", []);

    private instructionText: HtmlObj | null = null;

    private selectedLetter:LetterBox  | null=null;

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);



        



    }
    create() {
        super.create();

        (window as any).scene = this;

        /**
        * SET CALLBACK ON THE DRAG ENGINE
        */

       

       // this.loadData();
        this.getPrevData();
        this.instructionText = new HtmlObj("instructions");


    }
    getPrevData() {
        let dataDiv: HTMLElement | null = parent.document.getElementById("gameData");
        if (dataDiv) {
            let dataString: string = dataDiv.innerText;
            console.log(dataString);
            let data: any = JSON.parse(dataString);
            this.gotData(data);
        }

    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {

        console.log(data);

        this.gameData.instructions = data.instructions;

        let levelData: any = data.levels;
        for (let i: number = 0; i < levelData.length; i++) {
            //console.log(levelData[i]);
            let level: LetterRowVo = new LetterRowVo(levelData[i].image, []);

            let letterData = levelData[i].letters;
            for (let j: number = 0; j < letterData.length; j++) {
                //console.log(letterData[j]);
                level.letters.push(new LetterVo(letterData[j].letter, letterData[j].correct));
            }
            this.gameData.cards.push(level);
        }

        console.log(this.gameData);

        this.nextLevel();
       

        this.setInstruction(this.gameData.instructions);

        //   LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", ""); */
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");
        LinkUtil.makeLink("btnNext", this.onbuttonPressed.bind(this), "next", "");
    }
    setInstruction(text: string) {

        if (!this.instructionText)
        {
            this.instructionText = new HtmlObj("instructions");
        }

        if (this.instructionText) {
            if (this.instructionText.el) {
                this.instructionText.el.innerText = text;
            }
        }

    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics/" + image;
    }
    onbuttonPressed(action: string, params: string) {
        //console.log(action);
        switch (action) {
            case "reset":
                if (this.nextFlag == false) {
                   
                    if (this.rightIcon) {
                        this.rightIcon.visible = false;
                    }
                    if (this.wrongIcon) {
                        this.wrongIcon.visible = false;
                    }
                   
                }
                break;

            case "check":
                this.checkCorrect();
                break;

            case "next":
                if (this.nextFlag == true) {
                    this.nextFlag = false;
                    this.nextLevel();
                }
                break;
        }
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.gameData.cards.length - 1) {
            alert("out of words");
            return;
        }
     
         
        

        /* this.currentWord = this.levels[this.levelIndex];

        this.correctWord = this.currentWord.correct; */

        if (this.rightIcon) {
            this.rightIcon.visible = false;
        }
        if (this.wrongIcon) {
            this.wrongIcon.visible = false;
        }
        this.setImage(this.gameData.cards[this.levelIndex].image);
        // this.setImage(this.currentWord.word);

        this.makeBoxes();
        
        this.doResize();
    }
    
    makeBoxes() {

       

        for (let i: number = 0; i < this.gameData.cards[this.levelIndex].letters.length; i++) {
            let letter: LetterVo = this.gameData.cards[this.levelIndex].letters[i];
            //console.log(letter);

            let letterBox: LetterBox = new LetterBox(this);
          
            letterBox.index = i;
            letterBox.gameWRatio = 0.08;
            letterBox.setLetterVo(this.gameData.cards[this.levelIndex].letters[i]);
           // letterBox.setText(this.gameData.cards[this.levelIndex].letters[i].text);
            letterBox.initPlace(new PosVo(6 + i, 4));
            letterBox.setClick(this.clickLetter.bind(this));
            
        }

        this.wrongIcon = new HtmlObj("wrongIcon");

        this.rightIcon = new HtmlObj("rightIcon");

        this.rightIcon.visible = false;
        this.wrongIcon.visible = false;
    }
    
    clickLetter(letter:LetterBox)
    {
        console.log(letter);
        if (this.selectedLetter)
        {
            this.selectedLetter.setFill("wheat");
        }
        letter.setFill("green");
        this.selectedLetter=letter;        
    }
   
   
    checkCorrect() {

       
        if (this.selectedLetter===null)
        {
            return;
        }
        // let correct:boolean=this.dragEng.getTargetContent() == this.currentWord.correct;
        let letterVo:LetterVo | null=this.selectedLetter.letterVo;
        if (letterVo===null)
        {
            return;
        }
        let correct: boolean=letterVo.correct;     
        

        if (correct) {
            if (this.nextFlag == false) {
               // this.boxesOff();
                this.nextFlag = true;
                this.playSound(this.rightSound);

                //  this.usedWords.push(this.currentWord.correct);

                if (this.rightIcon) {
                    this.rightIcon.visible = true;
                }
            }
        }
        else {
            this.nextFlag = true;
            // this.usedWords.push(this.currentWord.correct);
            if (this.wrongIcon) {
                this.wrongIcon.visible = true;
            }
            this.playSound(this.wrongSound);
        }
    }
    doResize() {
        super.doResize();


    }

}