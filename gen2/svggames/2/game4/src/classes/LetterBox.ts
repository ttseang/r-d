import { BaseDragObj, IDragObj } from "dragengsvg";
import { IScreen } from "svggame";
import { LetterVo } from "../dataObjs/LetterVo";

export class LetterBox extends BaseDragObj implements IDragObj {
    public text: string = "";
    private textEl: SVGTextElement | null = null;
    public index:number=0;
    public letterVo:LetterVo | null=null;

    constructor(scene: IScreen) {
        super(scene, "letterbox");
        // this.setContent("B");
    }
    public setLetterVo(letterVo:LetterVo)
    {
        this.letterVo=letterVo;
        this.setText(letterVo.text);
    }
    setText(text: string) {
        this.text = text;
        this.content = text;
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            this.textEl.innerHTML = text;
        }
    }
    setFill(color:string)
    {
        if (this.el)
        {
           let fill:SVGElement | null=this.el.querySelector("#lbfill");
           if (fill)
           {
               fill.setAttribute("fill",color);
           }
        }
    }
    setClick(callback:Function)
    {
        this.onClick(()=>{callback(this)});
    }
    doResize() {
        this.updateScale();
        super.doResize();
    }
}