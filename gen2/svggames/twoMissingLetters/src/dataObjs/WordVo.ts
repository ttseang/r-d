export class WordVo {
    public word: string;
    public letters: string[];
    public blanks: string;
    public image: string;
    public audio:string;
    constructor(word: string, letters: string[], blanks: string, image: string,audio:string) {
        this.word = word;
        this.letters = letters;
        this.blanks = blanks;
        this.image = image;
        this.audio=audio;
    }
}