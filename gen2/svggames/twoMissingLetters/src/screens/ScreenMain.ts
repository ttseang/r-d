
import { IDragObj, IDragTarg } from "dragengsvg";
import { DragEng } from "dragengsvg/classes/DragEng";
import { AlignGrid, BaseScreen, HtmlObj, IScreen, PosVo, SvgObj, TextObj } from "svggame";


import { TargetBox } from "../classes/TargetBox";
import { TWordBox } from "../classes/TwordBox";
import { LinkUtil } from "../classes/util/LinkUtil";
import { WordBox } from "../classes/WordBox";


import { WordVo } from "../dataObjs/WordVo";
//import { SpineChar } from "spinechar";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;


    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private levels: WordVo[] = [];
    private levelIndex: number = -1;
    private currentWord: WordVo = new WordVo("", [], "", "", "");

    private mainImage: HtmlObj | null = null;
    private correctWord: string = "";

    private wrongIcon: HtmlObj | null = null;
    private rightIcon: HtmlObj | null = null;

    private targetBox: TargetBox | null = null;
    private dragEng: DragEng;

    private currentBox: IDragObj | null = null;
    private nextFlag: boolean = false;
    private usedWords: Array<string> = [];
    private twordBoxes: TWordBox[] = [];
    private rightWordText: HtmlObj = new HtmlObj("rightWord");
    private qindexText: HtmlObj = new HtmlObj("qindex");

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);

        this.mainImage = new HtmlObj("mainImage");

        this.dragEng = new DragEng(this);
        this.dragEng.snapToTarget = true;


        this.rightWordText.visible = false;

    }
    create() {
        super.create();

        (window as any).scene = this;

        /**
        * SET CALLBACK ON THE DRAG ENGINE
        */

        this.dragEng.upCallback = this.onUp.bind(this)
        this.dragEng.dropCallback = this.onDrop.bind(this);
        this.dragEng.collideCallback = this.onCollide.bind(this);
        this.dragEng.downCallback = this.startDrag.bind(this);



          this.loadData();
        //this.getPrevData();

    }
    getPrevData() {
        let dataDiv: HTMLElement | null = parent.document.getElementById("gameData");
        if (dataDiv) {
            let dataString: string = dataDiv.innerText;
            console.log(dataString);
            let data: any = JSON.parse(dataString);
            this.gotData(data);
        }

    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        data = data.data;

        console.log(data);

        for (let i: number = 0; i < data.length; i++) {

            let word: string = data[i].word;
            let letters: string[] = data[i].letters;
            let blanks: string = data[i].blanks;
            let image: string = data[i].image;
            let audio: string = data[i].audio;

            let wordVo: WordVo = new WordVo(word, letters, blanks, image, audio);

            this.levels.push(wordVo);
        }
        //console.log(data);
        this.nextLevel();
        this.dragEng.setListeners();


        //   LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", "");
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");
        LinkUtil.makeLink("btnNext", this.onbuttonPressed.bind(this), "next", "");

        LinkUtil.makeLink("hintButton", this.onbuttonPressed.bind(this), "hint", "");
    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics/" + image;

    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
        switch (action) {
            case "reset":
                if (this.nextFlag == false) {
                    this.dragEng.clickLock = false;
                    if (this.rightIcon) {
                        this.rightIcon.visible = false;
                    }
                    if (this.wrongIcon) {
                        this.wrongIcon.visible = false;
                    }
                    this.dragEng.resetBoxes();
                }
                break;

            case "check":
                this.checkCorrect();
                break;

            case "next":
                if (this.nextFlag == true) {
                    this.nextFlag = false;
                    this.nextLevel();
                }
                break;
            case "hint":
                this.sayWord();
                break;
        }
    }
    sayWord() {
        this.playSound("./assets/audio/" + this.currentWord.audio);
    }
    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            alert("out of words");
            return;
        }

        if (this.qindexText.el) {
            let l2: number = this.levelIndex + 1;
            this.qindexText.el.innerText = l2.toString() + ".";
            this.qindexText.visible = true;
        }

        this.dragEng.clickLock = false;
        this.rightWordText.visible = false;

        this.currentWord = this.levels[this.levelIndex];

        this.correctWord = this.currentWord.word;

        if (this.rightIcon) {
            this.rightIcon.visible = false;
        }
        if (this.wrongIcon) {
            this.wrongIcon.visible = false;
        }



        this.setImage(this.currentWord.image);


        this.makeBoxes();
        this.sayWord();

        this.doResize();
    }
    makeBoxes() {



        this.dragEng.resetBoxes();
        this.dragEng.destoryDrags();
        this.dragEng.destroyTargs();

        for (let i: number = 0; i < this.currentWord.blanks.length; i++) {
            let tb: TargetBox = new TargetBox(this);
            tb.gameWRatio = 0.05;
            tb.setPlace(new PosVo(3 + i * 0.5, 6));
            let letter: string = this.currentWord.blanks.substring(i, i + 1);

            tb.setText(letter);


            this.dragEng.addTarget(tb);

        }

        for (let i: number = 0; i < this.currentWord.letters.length; i++) {
            let box: WordBox = new WordBox(this);
            box.gameWRatio = 0.05;
            box.setText(this.currentWord.letters[i]);
            box.initPlace(new PosVo(2 + i * 1.4, 4));

            this.dragEng.addDrag(box);
        }



        this.wrongIcon = new HtmlObj("wrongIcon");

        this.rightIcon = new HtmlObj("rightIcon");

        this.rightIcon.visible = false;
        this.wrongIcon.visible = false;
    }
    startDrag(d: IDragObj) {
        this.currentBox = d;
        let box: WordBox = d as WordBox;
        if (box.target) {
            box.target.content = "";
            box.target = null;
        }
    }
    onCollide(d: IDragObj, t: IDragTarg) {
        console.log("collide");
        let box: WordBox = d as WordBox;
        box.target = t;
        //  d.canDrag = false;
        //  this.dragEng.clickLock = true;
    }
    onDrop() {
        //console.log("dropped");
        console.log(this.dragEng.getTargetContent());

        //check drop content here
    }
    onUp() {
        //console.log("up");
    }
    boxesOff() {
        for (let i: number = 0; i < this.dragEng.dragObjs.length; i++) {
            let box: IDragObj = this.dragEng.dragObjs[i];
            if (box != this.currentBox) {
                box.alpha = .5;
            }
        }
    }
    checkCorrect() {

        if (this.correctWord == "") {
            return;
        }

        let correct: boolean = this.dragEng.getTargetContent() == this.currentWord.word;
        // let correct: boolean = true;
        if (correct) {
            if (this.nextFlag == false) {
                this.boxesOff();
                this.nextFlag = true;
                this.playSound(this.rightSound);

                //  this.usedWords.push(this.currentWord.correct);

                if (this.rightIcon) {
                    this.rightIcon.visible = true;
                }
            }
        }
        else {
            this.nextFlag = true;
            // this.usedWords.push(this.currentWord.correct);
            if (this.rightWordText.el) {
                this.rightWordText.el.innerText = this.currentWord.word;
                this.rightWordText.visible = true;
            }
            if (this.wrongIcon) {
                this.wrongIcon.visible = true;
            }
            this.playSound(this.wrongSound);
        }
    }
    doResize() {
        super.doResize();


    }

}