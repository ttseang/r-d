import { BaseTargObj, IDragTarg } from "dragengsvg";
import { IScreen } from "svggame";


export class TargetBox extends BaseTargObj implements IDragTarg {
    private textEl: SVGTextElement | null = null;
    public text: string = "";
    private back: SVGElement | null = null;
    public canDrop: boolean = true;

    constructor(iScreen: IScreen) {
        super(iScreen, "targetBox");

        if (this.el) {
            this.back = this.el.getElementsByTagName("rect")[0];

        }
    }
    setText(text: string) {
        this.text = text;
        this.content = text;
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            if (text !== "_") {
                this.textEl.innerHTML = text;
                if (this.back) {
                    this.back.style.display = "none";
                    this.canDrop = false;
                }
            }
            else {
                this.textEl.innerHTML = "";
                this.content="";
            }

        }
    }
    doResize() {
        this.updateScale();
        super.doResize();
    }
}