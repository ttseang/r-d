import { BaseDragObj, IDragObj, IDragTarg } from "dragengsvg";
import { IScreen } from "svggame";

export class WordBox extends BaseDragObj implements IDragObj {
    public text: string = "";
    private textEl: SVGTextElement | null = null;
    public target:IDragTarg | null=null;
    
    constructor(scene: IScreen) {
        super(scene, "wordbox");
        // this.setContent("B");
    }
    setText(text: string) {
        this.text = text;
        this.content = text;
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            this.textEl.innerHTML = text;
        }
    }
    setStrike(value: boolean) {

        if (this.textEl) {
            this.textEl.classList.remove("strike");
            if (value == true) {
                this.textEl.classList.add("strike");
            }
        }
    }
    bringToTop() {
        if (this.el) {
            this.el.style.zIndex = "1000";
        }
    }
    doResize() {
        this.updateScale();
        super.doResize();
    }
}