
import { BaseScreen, HtmlObj, IScreen, SvgObj } from "svggame";
import { CardWord } from "../classes/cardWord";
import { PicCard } from "../classes/picCard";


import { LinkUtil } from "../classes/util/LinkUtil";
import { CardVo } from "../dataObjs/CardVo";
import { LevelVo } from "../dataObjs/LevelVo";




export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;
    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";


    private levelIndex: number = -1;

    public cards: PicCard[] = [];
    public bigCard: PicCard;
    public levels: LevelVo[] = [];

    private instructions:string="";
    public instructText:HtmlObj=new HtmlObj("instructions");
    public levelText:HtmlObj=new HtmlObj("questIndex");

    private wrongIcon: HtmlObj | null = null;
    private rightIcon: HtmlObj | null = null;
    private redX:HtmlObj | null=null;
    private greenCheck:HtmlObj|null=null;
    private nextFlag:boolean=false;
    private checkFlag:boolean=false;

    private cardWords:CardWord[]=[];

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 26, 1000);

        this.bigCard = new PicCard("bigcard", this.hideBigCard.bind(this), () => { });
       // this.bigCard.setImage("cow.png");
        

    }
    create() {
        super.create();

        (window as any).scene = this;

        for (let i = 0; i < 3; i++) {
            let card: PicCard = new PicCard("picard" + i, this.showBigCard.bind(this), this.selectCard.bind(this));
            card.visible = true;
            card.x = 15 + i * 25;
            card.y = 30;
            this.cards.push(card);
            //card.setImage("shrimp.png");
            card.setSelected(false);

            let cardWord:CardWord=new CardWord(i);
            cardWord.visible=false;
            this.cardWords.push(cardWord);
        }

        this.cards[0].canSelect=false;

        let divLine:SvgObj=new SvgObj(this,"divider");

       // this.loadData();
       this.getPrevData();

        this.wrongIcon = new HtmlObj("wrongIcon");

        this.rightIcon = new HtmlObj("rightIcon");

        this.rightIcon.visible = false;
        this.wrongIcon.visible = false;

        this.redX=new HtmlObj("redX");
        this.redX.visible=false;

        this.greenCheck=new HtmlObj("greenCheck");
        this.greenCheck.visible=false;
    }
    getPrevData()
    {
        let dataDiv:HTMLElement | null=parent.document.getElementById("gameData");
        if (dataDiv)
        {
            let dataString:string=dataDiv.innerText;
            console.log(dataString);
            let data:any=JSON.parse(dataString);
            this.gotData(data);
        }
        

    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        this.instructions=data.instructions;
        console.log(this.instructions);

        /* let inDiv:HTMLElement | null=document.getElementById("instructions");
        if (inDiv)
        {
            inDiv.innerText=this.instructions;
        } */

        data = data.levels;
        console.log(data);
        for (let i: number = 0; i < data.length; i++) {
            let cardData: any = data[i].cards;
            console.log(cardData);
             let levelVo: LevelVo = new LevelVo(i, []);
            for (let j: number = 0; j < cardData.length; j++) {
                let cardVo: CardVo = new CardVo(cardData[j].word,cardData[j].image, cardData[j].correct);
                levelVo.cards.push(cardVo);
            }
            this.levels.push(levelVo);
        }
        //console.log(data);
        this.nextLevel();



        LinkUtil.makeLink("resetButton", this.onbuttonPressed.bind(this), "reset", "");
        LinkUtil.makeLink("checkButton", this.onbuttonPressed.bind(this), "check", "");
        LinkUtil.makeLink("nextButton", this.onbuttonPressed.bind(this), "next", "");
    }
    setImage(image: string) {
        let img: HTMLImageElement = document.getElementById("mainImage") as HTMLImageElement;
        img.src = "./assets/pics2/" + image + ".svg";

    }
    showBigCard(card: PicCard) {
        this.showAllCards();
        this.bigCard.setImage(card.image);
        this.bigCard.visible = true;
        card.visible = false;
    }
    hideBigCard(card: PicCard) {
        this.bigCard.visible = false;
        this.showAllCards();
    }
    showAllCards() {
        for (let i: number = 0; i < this.cards.length; i++) {
            this.cards[i].visible = true;
        }
    }
    resetCards() {
        for (let i: number = 0; i < this.cards.length; i++) {
            this.cards[i].setSelected(false);
        }
        if (this.redX)
        {
            this.redX.visible=false;
        }
        if (this.greenCheck)
        {
            this.greenCheck.visible=false;
        }
        if (this.wrongIcon)
        {
            this.wrongIcon.visible=false;
        }
        if (this.rightIcon)
        {
            this.rightIcon.visible=false;
        }
        this.hideWords();
        
    }
    selectCard(card: PicCard) {
        card.setSelected(!card.selected);
        this.checkFlag=true;
    }
    onbuttonPressed(action: string, params: string) {
        console.log(action);
        switch (action) {
            case "reset":
                this.resetCards();
                break;

            case "check":
                if (this.checkFlag==false)
                {
                    return;
                }
                let isCorrect:boolean=this.checkCorrect();
                console.log(isCorrect);
                if (isCorrect==true)
                {
                    this.playSound(this.rightSound);
                    if (this.rightIcon)
                    {
                        this.rightIcon.visible=true;
                    }
                  //  this.nextLevel();
                }
                else
                {
                    this.playSound(this.wrongSound);
                    if (this.wrongIcon)
                    {
                        this.wrongIcon.visible=true;
                    }
                    if (this.greenCheck)
                    {
                        this.greenCheck.visible=true;
                    }
                    if (this.redX)
                    {
                        this.redX.visible=true;
                    }
                  //  this.resetCards();
                }
                this.showWords();
                this.nextFlag=true;
                break;

            case "next":
                if (this.nextFlag==true)
                {
                    this.nextLevel();
                }
                
                break;
        }
    }

    nextLevel() {
        this.nextFlag=false;
        this.checkFlag=false;

        this.resetCards();
         this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            console.log("out of words");
            alert("Out of Cards");
            return;
        }
        if (this.levelText.el)
        {
            let lv:string=(this.levelIndex+1).toString();
            this.levelText.el.innerText=lv+".";
        }
        this.makeCards();

    }
    hideWords()
    {
        for(let i:number=0;i<3;i++)
        {
            this.cardWords[i].visible=false;
        }
    }
    showWords()
    {
        for(let i:number=0;i<3;i++)
        {
            this.cardWords[i].visible=true;
        }
    }
    makeCards() {
        let currentLevel:LevelVo=this.levels[this.levelIndex];
        console.log(currentLevel);
        let cardData:CardVo[]=currentLevel.cards;
        for (let i:number=0;i<cardData.length;i++)
        {
            this.cards[i].setImage(cardData[i].image);
            this.cardWords[i].setText(cardData[i].word);

            this.cardWords[i].x=this.cards[i].x+17;
            this.cardWords[i].y=this.cards[i].y+36;

            if (cardData[i].correct==false)
            {
                if (this.redX)
                {
                    this.redX.x=this.cards[i].x+8;
                    this.redX.y=this.cards[i].y+40;
                }
            }
            else
            {
                if (this.greenCheck)
                {
                    this.greenCheck.x=this.cards[i].x+8;
                    this.greenCheck.y=this.cards[i].y+40;
                }
            }
        }
        if (this.instructText.el)
        {
            let wordInstructions:string=this.instructions;
            wordInstructions=wordInstructions.replace("[word]",cardData[0].word);
            this.instructText.el.innerText=wordInstructions;
         //   this.instructText.el.innerText="Pick the picture that rhymes with "+cardData[0].word;
        }
        
    }

    checkCorrect() {

        let currentLevel:LevelVo=this.levels[this.levelIndex];
        let cardData:CardVo[]=currentLevel.cards;

        for (let i:number=0;i<cardData.length;i++)
        {
            if (this.cards[i].selected!=cardData[i].correct)
            {
                return false;
            }
        }
        return true;
    }
    doResize() {
        super.doResize();


        // this.grid?.showGrid();


    }

}