export class CardVo
{
    public image:string;
    public correct:boolean;
    public word:string;
    constructor(word:string,image:string,correct:boolean)
    {
        this.word=word;
        this.image=image;
        this.correct=correct;
    }
}