import { CardVo } from "./CardVo";

export class LevelVo {
    public cards:CardVo[]=[];
    public index:number=0;

    constructor(index:number,cards:CardVo[]) {
        this.index=index;
        this.cards=cards;
    }
}