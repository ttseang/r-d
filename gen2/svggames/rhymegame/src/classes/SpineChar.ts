import { SpinePlayer, SpinePlayerConfig } from "@esotericsoftware/spine-player";
import { AnimationVo } from "../dataObjs/AnimationVo";
import { SpineTween } from "./SpineTween";
import { SpineTweenObj } from "./SpineTweenObj";

export class SpineChar {
    private config: any = {};
    //private config2:SpinePlayerConfig={};

    private player: SpinePlayer;
    private animations: AnimationVo[];

    private divID: string;
    private _x: number = 0;
    private _y: number = 0;
    private _scaleX: number = 1;
    private _scaleY: number = 1;

    public realH:number=3000;
    public realW:number=6000;

    private _flip: boolean = false;
    

    public el: HTMLElement | null = null;

    constructor(divID: string, jsonUrl: string, atlasUrl: string, animations: AnimationVo[]) {
        //let config:SpinePlayerConfig={};



        this.divID = divID;
        this.animations = animations;

        this.config.atlasUrl = atlasUrl;
        this.config.jsonUrl = jsonUrl;
        // this.config.atlasUrl = "./assets/Penny Loafer.atlas";
        //this.config.jsonUrl = "./assets/Penny Loafer.json";
        this.config.showControls = false;
        this.config.premultipliedAlpha = true;
        this.config.alpha = true;
        this.config.success = this.onSuccess.bind(this);

        this.el = document.getElementById(divID);
        if (this.el) {
            this.el.style.visibility = "hidden";
        }
        console.log(this.config);

        this.player = new SpinePlayer(divID, this.config);

    }


    onSuccess() {
        this.player.play();
        /* this.player.animationState.setAnimation(0,"walk",true);
        this.player.animationState.setAnimation(1,"repeating/blink",true);  */

        for (let i: number = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }

        setTimeout(() => {

            
               /*  this.player.skeleton.x = this._x;
                this.player.skeleton.y = this._y;
                this.player.skeleton.scaleX = this._scaleX;
                this.player.skeleton.scaleY = this._scaleY; */

                //trigger setters
                this.flip=this._flip;
                this.x=this._x;
                this.y=this._y;
                this.scaleY=this._scaleY;
                this.scaleX=this._scaleX;

                if (this.el) {
                    this.el.style.visibility = "visible";
                }
            
        }, 1000);
    }
    setScale(scale:number)
    {
        this.scaleX=scale;
        this.scaleY=scale;
    }
    setAnimations(animations: AnimationVo[]) {
        this.animations = animations;
        for (let i: number = 0; i < this.animations.length; i++) {
            this.player.animationState.setAnimation(i, this.animations[i].name, this.animations[i].loop);
        }
    }
    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
        if (this.player) {
            if (this.player.skeleton) {
                this.player.skeleton.x = value;
            }
        }
    }
    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
        if (this.player) {
            if (this.player.skeleton) {

                //let sy:number=value-this.realH/2;

                this.player.skeleton.y = value;
            }
        }
    }

    public get scaleX(): number {
        return this._scaleX;
    }
    public set scaleX(value: number) {
        this._scaleX = value;
        if (this.player) {
            if (this.player.skeleton) {

                let v:number=value;
                if (this._flip==true)
                {
                    v=-v;
                }

                this.player.skeleton.scaleX = v;
            }
        }
    }

    public get scaleY(): number {
        return this._scaleY;
    }
    public set scaleY(value: number) {
        this._scaleY = value;
        if (this.player) {
            if (this.player.skeleton) {
                this.player.skeleton.scaleY = value;
            }
        }
    }
    public get flip(): boolean {
        return this._flip;
    }
    public set flip(value: boolean) {
        this._flip = value;
        this.scaleX=this._scaleX;
    }
    moveTo(xx: number, yy: number,duration:number=1000,animations:AnimationVo[],endAnimations:AnimationVo[],flipOnEnd:boolean=false) {
        let tweenObj:SpineTweenObj=new SpineTweenObj();
        tweenObj.x=xx;
        tweenObj.y=yy;
        tweenObj.duration=duration;
        this.setAnimations(animations);

        tweenObj.onComplete=()=>{
            this.setAnimations(endAnimations);
            if (flipOnEnd==true)
            {
                this.flip=!this.flip;
            }
        }

        let tween:SpineTween=new SpineTween(this,tweenObj,true);


        
    }
}