import { HtmlObj } from "svggame";

export class CardWord extends HtmlObj {
    public text:string="";

    constructor(index:number)
    {
        super("cardWord"+index.toString());
    }
    setText(text: string) {
        this.text = text;
        
        if (this.el) {
            this.el.innerText=text;
        }
    }
   
}