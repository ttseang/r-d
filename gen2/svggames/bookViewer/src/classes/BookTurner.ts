import { Book } from "./Book";
//import { Sheet } from "./Sheet";
import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { Sheet } from "./Sheet";

export class BookTurner {
    public isTurning: boolean = false;
    public rotation: number = 0;
    public target: number = 0;
  //  public sheet: Sheet | null = null;
    private book:Book;

    private back: Element | null | undefined;
    private backG: Element | null | undefined;
    private element: Element | null | undefined;
    private front: Element | null | undefined;
    private frontG: Element | null | undefined;
    private shadow: Element | null | undefined;
    public sheet:Sheet | null=null;

    constructor(book:Book) {
        this.book=book;
    }

    load(sheet: Sheet, direction: number) {
        const newDirection = direction < 0 ? -1 : 1;
        this.isTurning = true;
        this.rotation = 90 * newDirection;
        this.target = -90 * newDirection;
        this.sheet = sheet;
        this.isTurning = true;
        this.rotation = 90 * newDirection;
        this.target = -90 * newDirection;

        sheet.show("all");
    } 
    rotate(degrees: number) {

      /*   this.back=this.book.currentSheet.left.el;
        this.front=this.book.currentSheet.right.el; */

        const newDegrees = Math.min(90, Math.max(-90, degrees));
        const seen = newDegrees < 0 ? this.back : this.front;
        const unseen = newDegrees < 0 ? this.front : this.back;
  
        this.book.hide(unseen as Element);
        this.book.show(seen as Element);
        this.distort(seen as SVGSVGElement, newDegrees);
  
         if (Math.abs(newDegrees) === 90) {
          this.book.hide(this.shadow as Element);
        } else {
          this.book.show(this.shadow as Element);
          this.distort(this.shadow as SVGSVGElement, newDegrees);
        }
      }
    distort(element: SVGSVGElement, degrees: number) {
        const radians = (degrees * Math.PI) / 180;
        const isBack = element == this.back;
        const isShadow = element == this.shadow;
        let sinX = (isBack ? -1 : 1) * Math.sin(radians);
        if (isShadow) sinX = sinX ** 3;
  
        const sinY =
          ((isBack || isShadow ? 1 : -1) * Math.cos(radians)) /
          (isShadow ? 16 : 6);
  
        const width = isBack ? this.book.bookVo.pageWidth : 0;
        const height = isBack ? this.book.bookVo.pageHeight : 0;
        const transform = this.book.svg?.createSVGTransform() as SVGTransform;
        const matrix = GU.Matrices.get(this.book.svg as SVGSVGElement, [
          sinX,
          sinY,
          0,
          1,
          0 - width * sinX,
          0 - height * sinY,
        ]);
        transform.setMatrix(matrix);
        element.transform.baseVal.clear();
        element.transform.baseVal.appendItem(transform as SVGTransform);
      }
  
}