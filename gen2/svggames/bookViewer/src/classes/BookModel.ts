import { PageElementVo } from "../dataObjs/book/PageElementVo";

export class BookModel
{
    public imagePath:string;
    public styleDiv:HTMLStyleElement | null=null;

    private static instance:BookModel | null=null;

    constructor()
    {
        this.imagePath="./assets/images/bookimages/";
        this.styleDiv=document.getElementById("elementStyles") as HTMLStyleElement;
    }
    public static getInstance():BookModel
    {
        if (this.instance===null)
        {
            this.instance=new BookModel();
        }
        return this.instance;
    }
    addStyle(elementVo:PageElementVo,style:HTMLStyleElement)
    {        
        if (this.styleDiv)
        {            
           // console.log(elementVo.id);
           // console.log(style.style.cssText);

            let cRule="#"+elementVo.id+"{"+style.style.cssText+"}";
            //console.log(cRule);
            this.styleDiv.append(cRule);
        }
    }
    getFullPath(path:string)
    {
        return this.imagePath+path;
    }
}