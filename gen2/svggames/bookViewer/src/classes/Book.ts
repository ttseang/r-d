import { IScreen, SvgObj } from "svggame";
import { BookVo } from "../dataObjs/book/BookVo";
import { PageVo } from "../dataObjs/book/PageVo";
import { BookPage } from "./BookPage";
import { BookTurner } from "./BookTurner";
import { Sheet } from "./Sheet";
import { Stack } from "./Stack";


export class Book extends SvgObj {
    public bookVo: BookVo;

    public pageIndex: number = 0;
    public rightPage: number = 0;
    public leftPage: number = -1;
    public pageCount: number = 0;

    public sheets: Sheet[] = [];
    public stacks: { left: Stack | null; right: Stack | null } = {
        left: null,
        right: null,
    };

    public defs: SVGDefsElement | null | undefined;

    private currentIndex: number = 0;
    public pages: BookPage[] = [];

    public svg: SVGSVGElement | null | undefined;
    public turner: BookTurner;
    public activePage: Element | null | undefined;

    constructor(screen: IScreen, bookVo: BookVo) {
        super(screen, "boardbookobj");
        this.bookVo = bookVo;
        this.screen = screen;

        let aa: HTMLElement | null = document.getElementById("addArea");
        this.svg = aa?.querySelector("#boardbookobj-1") as SVGSVGElement;

        console.log(this.svg);

        if (this.svg) {
            this.activePage = this.svg?.querySelector('#pages>g.activepg');

        }



        // this.pages=new SvgObj(screen,"pages");

        //   this.svg = aa?.querySelector("#boardbookobj") as SVGSVGElement;
        this.defs = document.getElementsByTagName("defs")[0];

        this.turner = new BookTurner(this);
        this.stacks = {
            left: new Stack(this, 'left'),
            right: new Stack(this, 'right'),
        };
    }
    buildPages() {
        let index: number = -1

        for (let i: number = 0; i < this.bookVo.pages.length; i++) {
            index++;
            let leftPage: BookPage = new BookPage(this.screen, this, index, true);
            leftPage.buildPage(this.bookVo.pages[i].left);
            this.pages.push(leftPage);

            index++;

            let rightPage: BookPage = new BookPage(this.screen, this, index, false);
            rightPage.buildPage(this.bookVo.pages[i].right);
            this.pages.push(rightPage);

        }
        this.pageCount = index + 1;
        //this.setPage(0);

        (window as any).book = this;
        this.loadSheets();
    }
    loadAPage(index: number) {

        let pageVo: PageVo = this.bookVo.pages[index];

        let pageName: string = "page" + index.toString();
        let def: HTMLElement | null = document.getElementById(pageName);
        //console.log(def);

        if (def) {
            let el: SVGForeignObjectElement = def.cloneNode(true) as SVGForeignObjectElement;

            let div1: HTMLElement = el.getElementsByTagName("div")[0];

            div1.classList.remove("left");
            div1.classList.remove("right");

            if (index / 2 == Math.floor(index / 2) && index != 0) {
                div1.classList.add("right");
            }
            else {
                div1.classList.add("left");
            }


        }
    }
    hidePages() {

    }


    hide(el: Element) {
        el.setAttribute("visible", "false");
    }
    show(el: Element) {
        el.setAttribute("visible", "true");
    }
    private loadSheets() {
        // Create sheets
        for (let index = 0; index < this.pageCount / 2; index++) {
            this.sheets.push(new Sheet(this, index));
        }

        // Create stacks
        for (let index = 0; index < this.sheets.length; index++) {
            //    console.log("add stack");
            this.stacks.right?.add(this.sheets[this.sheets.length - 1 - index]);
        }

        // if (this.stacks.left?.quantity) this.ui?.prevBtn.enable();
        //if (this.stacks.right?.quantity) this.ui?.nextBtn.enable();
    }
    public nextPage() {
        if (this.stacks.right?.quantity && !this.turner?.isTurning) {
            // this.ui?.prevBtn.disable();
            // this.ui?.nextBtn.disable();

            this.pageIndex++;
            this.leftPage += 2;
            this.rightPage += 2;
            if (this.pageIndex == this.pageCount / 2) {
                this.rightPage = -1;
            }

            // GU.Events.fireNew('pageflip', this.book as HTMLElement);

            if (this.stacks.right) {
                const sheet = this.stacks.right.remove();
                if (sheet) this.turner?.load(sheet, 1);
            }

        }
    }
    public prevPage() {
        if (this.stacks.left?.quantity && !this.turner?.isTurning) {
            //  this.ui?.prevBtn.disable();
            //  this.ui?.nextBtn.disable();
            this.pageIndex--;
            if (this.pageIndex == 0) {
                this.rightPage = 0;
                this.leftPage = -1;
            }
            else {
                this.rightPage -= 2;
                this.leftPage -= 2;
            }
            //  GU.Events.fireNew('pageflip', this.book as HTMLElement);
            const sheet = this.stacks.left.remove();


            if (sheet) this.turner?.load(sheet, -1);
        }
    }
}