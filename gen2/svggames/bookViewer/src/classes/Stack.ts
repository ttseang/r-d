
import { TTUtilities as GU } from '@teachingtextbooks/tt-utilities';
import { Book } from './Book';
import { Sheet } from './Sheet';

export class Stack {
    public book: Book;
    public element?: Element | null | undefined;
    public paper: Element | null | undefined;
    public pageOrientation: 'back' | 'front'='front';
    public quantity:number = 0;
    public sheets: Sheet[] = [];
    public side: 'left' | 'right'='left';

    constructor(book: Book, side: Stack['side']) {
      this.book = book;
      let elName:string='#pages>g.' + side + 'pg g';
      
      console.log(elName);

      if (this.book.svg)
      {
      
      this.element = this.book.svg.querySelector(elName);

      console.log(this.element);

      this.paper = this.book.svg?.querySelector('#pages>g.' + side + 'pg use');
      this.pageOrientation = side === 'left' ? 'back' : 'front';
      this.side = side;
      this.book.hide(this.paper as Element);
      }
      else
      {
          //console.log("active page not ready");
      }
    }

    add(sheet: Sheet) {
    
      //console.log("add sheet to stack");

      if (this.quantity)
      {
        this.sheets[0].hide();
      }
      
      sheet.stack = this.side;
      sheet.show();
      
      this.quantity = this.sheets.unshift(sheet);
      if (this.element)
      {
        GU.Elements.empty(this.element as Element);
      }    

      

      let sheetEl:HTMLElement | null=sheet[this.pageOrientation].el;
      //console.log(sheetEl);

      if (sheetEl)
      {
        this.element?.appendChild(sheetEl);
      }
      if (this.paper)
      {
        this.book.show(this.paper as Element);
      }
      
    }

    remove() {
      if (!this.quantity) return null;

      this.sheets[0].hide();
      const sheet = this.sheets.shift();
      this.quantity = this.sheets.length;


      GU.Elements.empty(this.element as Element);

      if (this.quantity) {
        this.sheets[0].show();
        let sheetEl:HTMLElement | null=this.sheets[0][this.pageOrientation].el;
        if (sheetEl)
        {
            this.element?.appendChild(sheetEl);
        }       
      } else {
        this.book.hide(this.paper as Element);
      }

      return sheet;
    }
  }