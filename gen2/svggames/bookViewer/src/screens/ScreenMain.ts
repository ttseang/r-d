
import { BaseScreen, HtmlObj, IScreen, SvgObj, TextObj } from "svggame";
import { Book } from "../classes/Book";
import { BookPage } from "../classes/BookPage";
import { BookVo } from "../dataObjs/book/BookVo";
import { BookLoader } from "../util/BookLoader";

export class ScreenMain extends BaseScreen implements IScreen {


    private bookLoader:BookLoader;
  

    constructor() {
        super("ScreenMain");
        this.bookLoader=new BookLoader(this.bookDataLoaded.bind(this));

        
        
    }
    create() {
        super.create();

        (window as any).scene = this;

        /* let test:SvgObj=new SvgObj(this,"page"); */
        
        let test:SvgObj=new SvgObj(this,"ball");
        
        this.bookLoader.loadBook("./assets/book3.json");
        

    }
    bookDataLoaded(bookVo:BookVo)
    {
       /*  let page:BookPage=new BookPage(this);
        //page.y=100;
      //  page.x=100;

        page.buildPage(bookVo.pages[0].right); */
        let book:Book=new Book(this,bookVo);
        book.buildPages();
    }

}