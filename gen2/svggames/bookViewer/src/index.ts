import { GameOptions, SVGGame } from "svggame"
import { ScreenMain } from "./screens/ScreenMain";

window.onload=()=>{
    
   

    let opts:GameOptions=new GameOptions();
    opts.useFull=true;
    opts.addAreaID="addArea";
    opts.screens=[new ScreenMain()]

    let game:SVGGame=new SVGGame("boardbookobj",opts);
}
