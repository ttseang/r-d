import { PageVo } from "./PageVo";

export class BookVo
{
    public cover:PageVo[];
    public pages:PageVo[];

    public duration:number=330;
    public frameRate:number=30;
    public pageWidth:number=300;
    public pageHeight:number=450;

    constructor(cover:PageVo[]=[],pages:PageVo[]=[])
    {
        this.cover=cover;
        this.pages=pages;
    }
    fromObj(bookData:any)
    {
        //console.log(bookData);

        for (let i:number=0;i<bookData.length;i++)
        {
            let pageVo:PageVo=new PageVo();
            pageVo.fromObj(bookData[i]);
            this.pages.push(pageVo);
        }
    }
}