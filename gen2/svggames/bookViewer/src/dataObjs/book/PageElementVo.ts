import { BookModel } from "../../classes/BookModel";
import { ElementExtrasVo } from "./ElementExtrasVo";

export class PageElementVo {

    public static TYPE_IMAGE: string = "IMAGE";
    public static TYPE_TEXT: string = "TEXT";
    public static TYPE_BACK_IMAGE = "BACK_IMAGE";

    public static SUB_TYPE_NONE: string = "none";
    public static SUB_TYPE_GUTTER: string = "gutter";

    public id: string;
    public type: string;
    public subType: string;
    public classes: string[];
    public content: string[];
    public x: number;
    public y: number;
    public w: number;
    public h: number;

    public extras: ElementExtrasVo;

    public static elementID: number = 0;
    private bookModel: BookModel = BookModel.getInstance();
    private origins: string[];

    constructor(id: string = "", type: string = "", subType: string = "", classes: string[] = [], content: string[] = [], x: number = 0, y: number = 0, w: number = 0, h: number = 0) {
        this.id = id;
        this.type = type;
        this.subType = subType;
        this.classes = classes;
        this.content = content;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.origins = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
        this.extras = new ElementExtrasVo();

    }
    fromObj(obj: any) {
        PageElementVo.elementID++;
        this.id = "bookElement" + PageElementVo.elementID.toString();


        this.type = obj.type;
        this.subType = obj.subType;
        this.classes = obj.classes;
        this.content = obj.content;
        this.x = parseFloat(obj.x);
        this.y = parseFloat(obj.y);
        this.w = parseFloat(obj.w);

        this.extras.fromObj(obj.extras);
    }
    getCSS() {
        let cssStyle: HTMLStyleElement = document.createElement("style");
        cssStyle.style.position = "absolute";
        cssStyle.style.left = this.x.toString() + "%";
        cssStyle.style.top = this.y.toString() + "%";

        if (this.w !== 0) {
            cssStyle.style.width = this.w.toString() + "%";
        }

        //
        //
        //
        if (this.type === PageElementVo.TYPE_TEXT) {
            cssStyle.style.color = this.extras.fontColor;
            cssStyle.style.fontSize = this.extras.fontSize.toString() + "px";
            cssStyle.style.fontFamily = this.extras.fontName;
        }

        if (this.type===PageElementVo.TYPE_BACK_IMAGE)
        {
            let image: string = "url(" + this.bookModel.getFullPath(this.content[0]) + ")";
            cssStyle.style.backgroundImage = image;
            cssStyle.style.backgroundPositionX=this.extras.backgroundPosX+"px";
            cssStyle.style.backgroundPositionY=this.extras.backgroundPosY+"px";
        }

        if (this.extras) {
            let transformString: string = "translate(" + this.origins[this.extras.orientation] + ")";


            if (this.extras.alpha) {
                cssStyle.style.opacity = this.extras.alpha.toString()+"%";
            }

            if (this.extras.rotation > 0) {
                let angle: number = ((this.extras.rotation) / 100) * 360;
                angle = Math.floor(angle);
                //console.log(angle);
                transformString += " rotate(" + angle.toString() + "deg)";
            }
            if (this.extras.skewY !== 0) {
                let skewAngleY: number = ((this.extras.skewY) / 100) * 360;
                skewAngleY = Math.floor(skewAngleY);
                //console.log(skewAngleY);
                transformString += " skewY(" + skewAngleY.toString() + "deg)";
            }
            if (this.extras.skewX !== 0) {
                let skewAngleX: number = ((this.extras.skewX) / 100) * 360;
                skewAngleX = Math.floor(skewAngleX);
                //console.log(skewAngleX);
                transformString += " skewX(" + skewAngleX.toString() + "deg)";
            }
            if (this.extras.flipV === true) {
                transformString += " scaleY(-1)";
            }
            if (this.extras.flipH === true) {
                transformString += " scaleX(-1)";
            }

            if (this.extras.borderThick > 0) {
                cssStyle.style.border = "solid";
                cssStyle.style.borderWidth = this.extras.borderThick.toString();
                cssStyle.style.borderColor = this.extras.borderColor;
            }
            cssStyle.style.transform=transformString;
            
        }

        return cssStyle;
    }
    private addClasses(obj:any)
    {
        for (let i:number=0;i<this.classes.length;i++)
        {
            obj.classList.add(this.classes[i]);
        }
    }
    getHtml() {


        switch (this.type) {
            case PageElementVo.TYPE_IMAGE:

                const img: HTMLImageElement = document.createElement("img");
                img.id = this.id;
                img.src = this.bookModel.getFullPath(this.content[0]);

                this.addClasses(img);

                return img;

            case PageElementVo.TYPE_TEXT:
                const text: HTMLElement = document.createElement("article");
                text.id = this.id;
                text.innerText = this.content[0];
                this.addClasses(text);
                return text;

            case PageElementVo.TYPE_BACK_IMAGE:

                const div: HTMLDivElement = document.createElement("div");
                div.id=this.id;
                this.addClasses(div);
                return div;

        }

        let def: HTMLDivElement = document.createElement("div");
        def.innerText = "Not Found " + this.type;
        return def;
    }
}

/* if (this.props.elementVo.type === ElementVo.TYPE_BACK_IMAGE) {

    if (elementVo.subType !== ElementVo.SUB_TYPE_GUTTER) {
        return (<div key={ukey} id={ukey} ref={this.ref} style={style} onClick={() => { this.props.callback(this.props.elementVo) }} className={classes} ></div>)
    }
    else {
        return (<div key={ukey} id={ukey} ref={this.ref} style={style} onClick={() => { this.props.callback(this.props.elementVo) }} className={classes} ></div>)
    }
} */