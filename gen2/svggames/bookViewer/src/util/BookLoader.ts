import { BookVo } from "../dataObjs/book/BookVo";
import { PageVo } from "../dataObjs/book/PageVo";

export class BookLoader
{
    private callback:Function;

    constructor(callback:Function=()=>{})
    {
        this.callback=callback;
    }
    loadBook(file:string)
    {
        fetch(file)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    private process(data:any)
    {
       // console.log(data);

        let pageData:any=data.pages;

        let bookVo:BookVo=new BookVo();
        bookVo.fromObj(pageData);

       // console.log(bookVo);
        this.callback(bookVo);
    }
}