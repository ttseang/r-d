
import { BaseScreen, GameManager, IScreen, Math2, PosVo, SvgObj, TextObj } from "svggame";
import { DragEng, IDragObj, IDragTarg } from "dragengsvg";
import { IconButton } from "../classes/IconButton";
import { LetterBox } from "../classes/LetterBox";
import { WordVo } from "../dataObjs/WordVo";
import { TargetBox } from "../classes/TargetBox";
import { ColorBurst } from "../effects/ColorBurst";


export class ScreenMain extends BaseScreen implements IScreen {
    private box!: SvgObj;
    private dragEng: DragEng;

    private uiWindow!: SvgObj;
    private btnCheck!: SvgObj;
    private btnPlay!: SvgObj;
    private btnReset!: SvgObj;
    private btnHelp!: SvgObj;

    private buttons: SvgObj[] = [];
    private instructionText: TextObj | null = null;
    private levels: WordVo[] = [];
    private levelIndex: number = -1;
    private currentWord: WordVo = new WordVo("", [], "");

    private targetBox: TargetBox | null = null;
    private targetBox2: TargetBox | null = null;

    private mainImage: SvgObj | null = null;
    private correctLetter: string = "";

    private mainImageX: number = 3;
    private mainImageY: number = 5;

    //sounds

    private popSound: string = "./assets/audio/pop.wav";
    private rightSound: string = "./assets/audio/quiz_right.mp3";
    private wrongSound: string = "./assets/audio/quiz_wrong.wav";

    private colorBurst: ColorBurst | null = null;
    private show: string = "";

    constructor() {
        super("ScreenMain");
        this.gm.regFontSize("instructions", 20, 1000);

        this.dragEng = new DragEng(this);
        this.dragEng.snapToTarget = true;
        this.dragEng.snapBackOnContent = false;
        this.dragEng.setContentOnCollide = false;
    }
    create() {
        super.create();

        this.colorBurst = new ColorBurst(this, this.gw / 2, this.gh / 2);
        (window as any).scene = this;

        this.uiWindow = new SvgObj(this, "uiWindow");
        this.uiWindow.gameWRatio = .9;


        //
        //
        //
        this.btnPlay = new IconButton(this, "btnPlay", "play", "", this.onbuttonPressed.bind(this));
        this.btnReset = new IconButton(this, "btnReset", "reset", "", this.onbuttonPressed.bind(this));
        this.btnCheck = new IconButton(this, "btnCheck", "check", "", this.onbuttonPressed.bind(this));
        this.btnHelp = new IconButton(this, "btnHelp", "help", "", this.onbuttonPressed.bind(this));

        this.buttons = [this.btnHelp, this.btnPlay, this.btnReset, this.btnCheck];

        /**
         * SET CALLBACK ON THE DRAG ENGINE
         */

        this.dragEng.upCallback = this.onUp.bind(this)
        this.dragEng.dropCallback = this.onDrop.bind(this);
        this.dragEng.collideCallback = this.onCollide.bind(this);


        this.instructionText = new TextObj(this, "instText");

        this.loadData();

        // this.dragEng.setListeners();
    }
    loadData() {
        fetch("./assets/data.json")
            .then(response => response.json())
            .then(data => this.gotData(data));
    }
    gotData(data: any) {
        data = data.data;

        for (let i: number = 0; i < data.length; i++) {
            let wordVo: WordVo = new WordVo(data[i].word, data[i].letters, data[i].show);
            this.levels.push(wordVo);
        }
        //console.log(data);
        this.nextLevel();
        this.dragEng.setListeners();
    }

    nextLevel() {

        this.levelIndex++;
        if (this.levelIndex > this.levels.length - 1) {
            //console.log("out of words");
            return;
        }
        if (this.mainImage) {
            this.mainImage.destroy();

        }

        this.currentWord = this.levels[this.levelIndex];
        this.show = this.currentWord.show;
        this.correctLetter = this.currentWord.word.substr(0, 1).toLowerCase() + this.currentWord.word.substr(-1, 1);
        console.log(this.correctLetter);

        //console.log(this.currentWord.word);

        this.mainImage = new SvgObj(this, this.currentWord.word);
        this.mainImage.gameWRatio = 0.2;
        this.grid?.placeAt(this.mainImageX, this.mainImageY, this.mainImage);
        this.mainImage.adjust();

        this.makeBoxes();
    }
    makeBoxes() {

        this.dragEng.resetBoxes();
        this.dragEng.destoryDrags();
        this.dragEng.destroyTargs();

        let targetKeys: string[] = ["targetBox", "targetBox2", "targetBox3"];

        let start: number = 7.5 - this.currentWord.word.length / 2;

        if (this.currentWord.word.length == 3) {
            start = 6;
        }


        for (let i: number = 0; i < this.currentWord.word.length; i++) {
            let key: string = targetKeys[0];
            let showLetter: string = this.show.substr(i, 1);

            /* if (i==this.currentWord.word.length-1)
            {
                key=targetKeys[1];
            } */
            if (showLetter == "-") {
                key = targetKeys[2]
            }

            let tb: TargetBox = new TargetBox(this, key);
            tb.gameWRatio = 0.08;
            tb.setPlace(new PosVo(start + i, 6));
            tb.setText(showLetter);
            this.dragEng.addTarget(tb);
        }

        /*  this.targetBox = new TargetBox(this,"targetBox");
         this.targetBox.gameWRatio=0.08;
         this.targetBox.setPlace(new PosVo(2, 3));
 
         this.targetBox2 = new TargetBox(this,"targetBox2");
         this.targetBox2.gameWRatio=0.08;
         this.targetBox2.setPlace(new PosVo(8, 3)); */

        /* this.dragEng.addTarget(this.targetBox);
        this.dragEng.addTarget(this.targetBox2); */



        for (let i: number = 0; i < this.currentWord.chars.length; i++) {
            let box: LetterBox = new LetterBox(this);
            box.setText(this.currentWord.chars[i]);
            box.gameWRatio = 0.08;
            box.initPlace(new PosVo(6 + i, 3.5));
            this.dragEng.addDrag(box);
        }

        if (this.colorBurst) {
            this.colorBurst.x = this.gw / 2;
            this.colorBurst.y = this.gh / 2;
        }

    }
    onbuttonPressed(action: string, params: string) {
        //console.log(action);
        switch (action) {
            case "reset":
                this.dragEng.clickLock = false;
                this.dragEng.resetBoxes();

                for (let i: number = 0; i < this.dragEng.targets.length; i++) {
                    let t: IDragTarg = this.dragEng.targets[i];
                    let showLetter = this.show[i];
                    console.log(showLetter);
                    t.content = showLetter;

                }

                break;

            case "check":
                this.checkCorrect();
                break;
        }
    }
    onCollide(d: IDragObj, t: IDragTarg) {
        console.log("collide");

        console.log(t);

        if (t.content != "-") {
            d.snapBack();
            return;
        }
        d.canDrag = false;
        t.content = d.content;

        this.dragEng.clickLock = true;
    }
    onDrop() {
       // console.log("dropped");
       
    }
    onUp() {
        //console.log("up");
    }
    doResize() {
        super.doResize();
     //   this.grid?.showGrid();
        if (this.instructionText) {
            this.instructionText.setFontSize(this.gm.getFontSize("instructions", this.gm.gw));
            this.grid?.placeAt(5, 2, this.instructionText);
            this.centerW(this.instructionText, true);
        }

        this.uiWindow.displayWidth = this.gw * 0.9;
        this.uiWindow.displayHeight = this.gh * 0.70;

        this.center(this.uiWindow, true);

        for (let i: number = 0; i < this.buttons.length; i++) {
            this.buttons[i].gameWRatio = 0.05;
            this.grid?.placeAt(2 + i * 2, 9.7, this.buttons[i]);
        }

        this.dragEng.doResize();

        if (this.mainImage) {
            this.mainImage?.updateScale();
            this.grid?.placeAt(this.mainImageX, this.mainImageY, this.mainImage);
            this.mainImage.adjust();
        }

    }
    checkCorrect() {

        if (this.correctLetter == "") {
            return;
        }
        if (this.currentWord.word !== this.dragEng.getTargetContent()) {
            //console.log("Wrong");
            this.playSound(this.wrongSound);

        }
        else {

            this.playSound(this.rightSound);
            this.colorBurst?.start();
            setTimeout(() => {
                this.dragEng.clickLock = false;
                this.nextLevel();
            }, 1500);
        }
    }
}