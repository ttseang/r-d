export class WordVo
{
    public chars:string[];
    public word:string="";
    public show:string="";

    constructor(word:string,chars:string[],show:string)
    {
        this.word=word;
        this.chars=chars;
        this.show=show;
    }
}