import { BaseTargObj, IDragTarg } from "dragengsvg";
import { IScreen } from "svggame";


export class TargetBox extends BaseTargObj implements IDragTarg
{
    private textEl:SVGTextElement |null=null;
    public text:string="";
    constructor(iScreen:IScreen,key:string)
    {
        super(iScreen,key);
    }    
    setText(text: string) {
        this.text = text;
        this.content = text;
        if (this.el) {
            this.textEl = this.el.getElementsByTagName("text")[0];

            this.textEl.innerHTML = text;
        }
    }
    doResize()
    {
        this.updateScale();
        super.doResize();
    }
}