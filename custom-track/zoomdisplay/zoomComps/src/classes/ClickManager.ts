import { UIManager } from "./UiManager";

export class ClickManager {
    private uiManager: UIManager = UIManager.getInstance();
    private started: boolean = false;
    constructor() {
        document.onmousedown = this.onMouseDown.bind(this);
        document.onmouseup = this.onMouseUp.bind(this);
    }

    onMouseDown(e: MouseEvent) {
        const target: HTMLElement = e.target as HTMLElement;
        let id: string | null = target.id;

        if (id === null || id === "") {
            id = target.classList[0];
        }
        if (id === "toggleButton") {
            id = target.classList[1] || target.classList[0];
        }
        console.log(id);
        if (this.started) {
            switch (id) {

                //button to open the bars
                case "btnNavDown":

                    this.uiManager.hideGroup("gadgets");
                    this.uiManager.toggleBars();

                    break;

                //buttons to turn on the gadgets
                case "music":
                    this.uiManager.hide("speedBalloon");
                    this.uiManager.hide("speechSlider");
                    // this.uiManager.toggle("volumeSlider");
                    this.uiManager.toggle("mobileVolume");
                    break;
                case "speech":
                    this.uiManager.hide("volumeSlider");
                    this.uiManager.hide("speedBalloon");
                    this.uiManager.toggle("mobileVolume");
                    this.uiManager.toggle("speechSlider");
                    break;

                case "speed":
                    this.uiManager.hide("volumeSlider");
                    this.uiManager.hide("speechSlider");
                    this.uiManager.hide("mobileVolume");                    
                    this.uiManager.toggle("speedBalloon");
                    break;

                case "barTab":
                    this.uiManager.hideGroup("gadgets");
                    this.uiManager.toggleBars();
                    break;

                case "read":
                    //set mode read
                    // this.uiManager.closeBars();
                    this.uiManager.removeElementClass("toggleButton autoPlay", "selected");
                    this.uiManager.removeElementClass("toggleButton listen", "selected");
                    this.uiManager.addElementClass("toggleButton read", "selected");
                    break;
                case "listen":
                    //set mode listen
                    //  this.uiManager.closeBars();
                    this.uiManager.removeElementClass("toggleButton autoPlay", "selected");
                    this.uiManager.removeElementClass("toggleButton read", "selected");
                    this.uiManager.addElementClass("toggleButton listen", "selected");
                    break;
                case "autoPlay":
                    //set mode autoPlay
                    //  this.uiManager.closeBars();
                    this.uiManager.removeElementClass("toggleButton listen", "selected");
                    this.uiManager.removeElementClass("toggleButton read", "selected");
                    this.uiManager.addElementClass("toggleButton autoPlay", "selected");
                    break;
                case "speedSlow":
                    //set speed slow
                    this.uiManager.removeElementClass("speedNormal", "selected");
                    this.uiManager.removeElementClass("speedFast", "selected");
                    this.uiManager.addElementClass("speedSlow", "selected");
                    break;
                case "speedNormal":
                    //set speed normal
                    this.uiManager.removeElementClass("speedSlow", "selected");
                    this.uiManager.removeElementClass("speedFast", "selected");
                    this.uiManager.addElementClass("speedNormal", "selected");
                    break;
                case "speedFast":
                    //set speed fast
                    this.uiManager.removeElementClass("speedSlow", "selected");
                    this.uiManager.removeElementClass("speedNormal", "selected");
                    this.uiManager.addElementClass("speedFast", "selected");
                    break;


            }
        } else {
            switch (id) {
                case "btnAutoStart":
                    this.uiManager.hideGroup("gadgets");
                    this.uiManager.hide("bubbleGrid");
                    this.uiManager.toggleBars();
                    this.uiManager.hide("darkScreen");
                    this.started = true;
                    //  this.zoomyModel.mode = "auto";
                    break;

                case "btnListenStart":
                    this.uiManager.hideGroup("gadgets");
                    this.uiManager.hide("bubbleGrid");
                    this.uiManager.toggleBars();
                    this.uiManager.hide("darkScreen");
                    this.started = true;
                    //   this.zoomyModel.mode = "listen";
                    break;

                case "btnReadStart":
                    this.uiManager.hideGroup("gadgets");
                    this.uiManager.hide("bubbleGrid");
                    this.uiManager.toggleBars();
                    this.uiManager.hide("darkScreen");
                    this.started = true;
                    //   this.zoomyModel.mode = "read";
                    break;
            }
        }
    }
    onMouseUp(e: MouseEvent) {
        const target: HTMLElement = e.target as HTMLElement;
        let id: string | null = target.id;

        if (id === null || id === "") {
            id = target.classList[0];
        }
    }
}