export class UIVo
{
    public id:string;
    public group:string;
    public element:HTMLElement;
    constructor(id:string,group:string,element:HTMLElement)
    {
        this.id = id;
        this.group = group;
        this.element = element;
    }
}