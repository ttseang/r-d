import { CodeVo } from "../dataObjs/CodeVo";
import { CardTypes, PageElementTypes, PageElementVo } from "../dataObjs/PageElementVo";
import { GreenCardElement } from "../elements/cards/GreenCardElement";
import { WhiteCardElement } from "../elements/cards/WhiteCardElement";
import { ImageElement } from "../elements/ImageElement";
import { iElement } from "../interfaces/iElement";

export class ElementFactory {
    public static getElement(pageElement: PageElementVo): CodeVo | null {

        let element:iElement|null=null;

        switch (pageElement.type) {
            case PageElementTypes.IMAGE:
                element=new ImageElement(pageElement);
              
                return new CodeVo(element.getHTML(),element.getCss());

            case PageElementTypes.CARD:

            switch(pageElement.subType){
                case CardTypes.GREEN:
                 element=new GreenCardElement(pageElement);
                 return new CodeVo(element.getHTML(),element.getCss());

                default:
                    element=new WhiteCardElement(pageElement);
                    return new CodeVo(element.getHTML(),element.getCss());
            }
        }

        return null;
    }
   
}