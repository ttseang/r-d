import { PageElementVo } from "../dataObjs/PageElementVo";
import { iElement } from "../interfaces/iElement";
import { Paths } from "../util/Paths";

export class ImageElement implements iElement {

    private pageElement: PageElementVo;
    constructor(pageElement: PageElementVo) {
        this.pageElement = pageElement;
    }

    getHTML(): HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement {
        const img: HTMLImageElement = document.createElement("img");
        img.id = this.pageElement.eid;
        let content: string = this.pageElement.content[0];
        img.src = Paths.FULL_PATH + content;
        return img;
    }
    getCss(): CSSStyleDeclaration {
        let css: CSSStyleDeclaration = document.createElement("div").style;
        css.position = "absolute";
        css.left = this.pageElement.x + "%";
        css.top = this.pageElement.y + "%";

        if (this.pageElement.w > 0) {
            css.width = this.pageElement.w + "%";
        }
        return css;
    }

}