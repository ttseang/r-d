import { PageElementVo } from "../../dataObjs/PageElementVo";

export class WhiteCardElement
{
    private pageElement: PageElementVo;
    constructor(pageElement: PageElementVo) {
        this.pageElement = pageElement;
    }

    getHTML(): HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement {
        const div: HTMLDivElement = document.createElement("div");
        div.id = this.pageElement.eid;
        div.className = "whiteCard";

        let text: string = this.pageElement.content[0];

        let p: HTMLParagraphElement = document.createElement("p");
        p.innerText = text;
        div.appendChild(p);

        return div;
    }
    getCss(): CSSStyleDeclaration {
        let css: CSSStyleDeclaration = document.createElement("div").style;
        css.position = "absolute";
        css.left = this.pageElement.x + "%";
        css.top = this.pageElement.y + "%";

        if (this.pageElement.w > 0) {
            css.width = this.pageElement.w + "%";
        }
        return css;
    }
}