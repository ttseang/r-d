
import { DisplayElementVo } from "./dataObjs/DisplayElementVo";
import { PageElementVo } from "./dataObjs/PageElementVo";
import { StepVo } from "./dataObjs/StepVo";
import { ZoomPresentationVo } from "./dataObjs/ZoomPresentationVo";
import { ElementFactory } from "./util/ElementFactory";
import { CSSManager } from "./managers/CssManager";
import { CodeVo } from "./dataObjs/CodeVo";
import { ZoomVo } from "./dataObjs/ZoomVo";

export class ZoomPage {

    //the page container is an svg element and is the parent of the element container
    //and the zoom animation
    private pageContainer: SVGAElement;

    //the element container is a foreignObject element and is the parent of all the elements on the page
    public elementContainer: SVGSVGElement | null | undefined;

    //this controls the zoom animations
    private zoomElement: SVGAnimateElement | null = null;

    //the current page number
    public currentPageNumber: number = 0;

    //this is all the data for all the pages
    public zoomData: ZoomPresentationVo | null = null;

    //this is the data for the current page that contains the page elements
    public currentStep: StepVo | null = null;

    //this is the data for the current page elements
    public currentElements: PageElementVo[] = [];

    //used for text elements and images
    private origins: string[] = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];

    //this map is used to keep track of the elements that are currently on the page
    private elementMap: Map<string, DisplayElementVo> = new Map<string, DisplayElementVo>();

    //css manager keeps track of the css for each element and cleans up the css when the element is removed
    //or updates it when the element is updated
    private cssManager: CSSManager;

    //the previous zoom level
    private oldZoom: string = "";

    private pageW: number = 0;
    private pageH: number = 0;

    constructor(pageContainer: SVGAElement, styleContainer: HTMLStyleElement) {
        this.pageContainer = pageContainer;

        //for debugging - make the zoomPage object available in the console
        //remove before production
        (window as any).zoomPage = this;

        //find the element container
        if (this.pageContainer) {
            this.elementContainer = this.pageContainer.querySelector(".elementContainer") as SVGSVGElement;
            this.zoomElement = this.pageContainer.querySelector(".zoom1") as SVGAnimateElement;
        }
        this.cssManager = new CSSManager(styleContainer);
    }

    setData(zoomData: ZoomPresentationVo) {

        this.zoomData = zoomData;

        //get the size data from the zoomData object

        let w: number = zoomData.pageW;
        let h: number = zoomData.pageH;

        //get page width to height ratio
        let ratio: number = w / h;

        //set the page width to the window width and the height to the window width divided by the ratio
        w = window.innerWidth;
        h = w / ratio;

        //set the page container size
        if (this.elementContainer)
        {
            this.elementContainer.setAttribute("width", w.toString());
            this.elementContainer.setAttribute("height", h.toString());
        }
     /*    this.pageContainer.setAttribute("width", w.toString());
        this.pageContainer.setAttribute("height", h.toString()); */

        this.pageW = w;
        this.pageH = h;

    }
    public setPage(currentPageNumber: number) {
        //update the current page number
        this.currentPageNumber = currentPageNumber;


        if (this.zoomData) {
            //update the current step and current elements
            this.currentStep = this.zoomData.steps[currentPageNumber];
            this.currentElements = this.currentStep.elements;
        }

        //add and remove elements from the page
        this.updatePage();
    }
    private updatePage() {
        this.elementMap.forEach((value: DisplayElementVo, key: string) => { value.delFlag = true; });

        //loop through the current elements and add them to the page
        for (let i: number = 0; i < this.currentElements.length; i++) {
            let element: PageElementVo = this.currentElements[i];
            //  console.log(element);
            /**
                 * does this element already exsist?
                 * if not add it
                 * if so update position and content
                 */
            if (!this.elementMap.has(element.eid)) {
                /**
                 *create the HTMLElement from the data obect
                 */

                let codeVo: CodeVo | null = ElementFactory.getElement(element);

                if (codeVo === null) {
                    return;
                }

                let displayElement: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement | null = codeVo.element;
                /**
                *add the data object to the current element map
                */
                if (displayElement === null) {
                    return;
                }

                this.elementMap.set(element.eid, new DisplayElementVo(element.eid, displayElement));
                //add the element to the page
                if (this.elementContainer) {
                    this.elementContainer.appendChild(displayElement);
                }

                //set the element position by setting the style
                let css: CSSStyleDeclaration = codeVo.css;
                if (css) {
                    this.cssManager.addStyle(element.eid, css);
                }

            }
            else {
                //already exsists so update the position and content
                let displayElementVo: DisplayElementVo | undefined = this.elementMap.get(element.eid);
                if (displayElementVo) {
                    let displayElement: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement | null = displayElementVo.displayElement;
                    displayElementVo.delFlag = false;
                   /*  if (displayElement) {
                        let css: CSSStyleDeclaration = displayElement.style;
                        css.left = element.x + "%";
                        css.top = element.y + "%";
                        if (element.w > 0) {
                            css.width = element.w + "%";
                        }
                    } */
                }
            }
        }
        this.removeDeletedElements();
        this.setZoom();
    }
    removeDeletedElements() {
        //loop through the element map and remove any elements that have been flagged for deletion
        this.elementMap.forEach((value: DisplayElementVo, key: string) => {
            if (value.delFlag) {
                //remove the element from the page
                if (this.elementContainer) {
                    this.elementContainer.removeChild(value.displayElement);
                }
                //remove the element from the element map
                this.elementMap.delete(key);
                //remove the element css
                this.cssManager.removeStyle(key);
            }
        });
    }
    private setZoom() {
        if (!this.currentStep) {
            return;
        }
        let zoomVo: ZoomVo = this.currentStep.zoom;

        //turn percentages into pixels
        let xx: number = (zoomVo.x1 / 100) * this.pageW;
        let yy: number = (zoomVo.y1 / 100) * this.pageH;

        //round to 3 decimal places
        xx = Math.round(xx * 1000) / 1000;
        yy = Math.round(yy * 1000) / 1000;

        //measure the distance between the points
        let distX: number = (zoomVo.x2 - zoomVo.x1) / 100;
        let distY: number = (zoomVo.y2 - zoomVo.y1) / 100;

        //cut off the extra decimal places past 2 points
        distX = Math.round(distX * 100) / 100;
        distY = Math.round(distY * 100) / 100;

        //multiply the distance by the page width and height to get the zoom width and height
        let zoomX: number = (this.pageW * distX);
        let zoomY: number = (this.pageH * distY);

        //round to 3 decimal places
        zoomX = Math.round(zoomX * 1000) / 1000;
        zoomY = Math.round(zoomY * 1000) / 1000;

        //set the zoom animation
        let posString = xx.toString() + " " + yy.toString();
        posString += " " + zoomX + " " + zoomY;

        console.log(this.oldZoom);
        console.log(posString);

        if (this.zoomElement)
            if (this.oldZoom === "") {
                this.pageContainer.setAttribute("viewBox", posString);
            }
            else {

                if (this.oldZoom !== posString) {
                    console.log("not equal");
                    this.zoomElement.setAttribute("values", this.oldZoom + ";" + posString);
                    this.zoomElement.beginElement();
                }

            }


        this.oldZoom = posString;
    }

}
