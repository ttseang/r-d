import { ZoomPage } from "./ZoomPage";
import { ZoomPresentationVo } from "./dataObjs/ZoomPresentationVo";
import { ZoomPresLoader } from "./util/ZoomPresLoader";

export class ZoomDisplay
{
    private loader:ZoomPresLoader;
    private zoomPage:ZoomPage;
    constructor()
    {
        const pageContainer:SVGAElement=document.getElementsByClassName("pageContainer")[0] as SVGAElement;
        const styleContainer:HTMLStyleElement=document.getElementsByClassName("elementStyles")[0] as HTMLStyleElement;

        this.zoomPage=new ZoomPage(pageContainer,styleContainer);

        this.loader=new ZoomPresLoader(this.presLoaded.bind(this));
        this.loader.loadFile("./assets/files/TTZZ.json");

    }


    private presLoaded(data:any)
    {
       
        let zoomPres:ZoomPresentationVo=new ZoomPresentationVo([],300,300);
        zoomPres.fromObj(data);
        this.zoomPage.setData(zoomPres);
        this.zoomPage.setPage(0);
    }
    
}