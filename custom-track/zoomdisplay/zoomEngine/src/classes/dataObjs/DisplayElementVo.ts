
export class DisplayElementVo
{
    public eid:string;
    public displayElement:HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement;
    public delFlag:boolean=false;

    constructor(eid:string,displayElement:HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement)
    {
        this.eid=eid;
        this.displayElement=displayElement;
    }
  
}