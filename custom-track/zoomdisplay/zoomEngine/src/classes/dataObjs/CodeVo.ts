export class CodeVo {
    public element: HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement;
    public css: CSSStyleDeclaration;
    constructor(element: HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement, css: CSSStyleDeclaration) {
        this.element = element;
        this.css = css;
    }
}