export class AudioPlayer
{
    public isPlaying:boolean=false;
    private audioElement:HTMLAudioElement;
    constructor()
    {
        (window as any).ap=this;
        //create an audio element
        this.audioElement=document.createElement("audio");
    }
    public playAudio(path:string)
    {
        if (this.isPlaying)
        {
            this.pauseAudio();

        }
        this.audioElement.src=path;
        this.audioElement.play();
        this.isPlaying=true;
        //listen for end
        this.audioElement.addEventListener("ended",this.audioEnded.bind(this));
    }
    public audioEnded()
    {
        this.isPlaying=false;
    }
    public pauseAudio()
    {
        this.audioElement.pause();
        this.isPlaying=false;
    }
   
}