import { ZoomPresentationVo } from "../../dataObjects/ZoomPresentationVo";



export class ZoomPresLoader
{
    private callback:Function;

    constructor(callback:Function=()=>{})
    {
        this.callback=callback;
    }
    loadFile(file:string)
    {
        fetch(file)
        .then(response => response.json())
        .then(data => this.process(data));
    }
    public loadFromString(content:string)
    {
        let obj:any=JSON.parse(content);
        this.process(obj);
    }
    private process(data:any)
    {
       // //////////console.log(data);

        let pageData:any=data;
        
        let zoomPres:ZoomPresentationVo=new ZoomPresentationVo([],300,300);
        zoomPres.fromObj(pageData);

       // //////////console.log(bookVo);
        this.callback(zoomPres);
    }
}