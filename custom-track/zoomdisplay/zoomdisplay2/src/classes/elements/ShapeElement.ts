
import { PageElementVo } from "../../dataObjects/PageElementVo";
import { ZoomyModel } from "../ZoomyModel";

export class ShapeElement {
    private pageElementVo: PageElementVo;
    private classArray: string[];
    private zm: ZoomyModel = ZoomyModel.getInstance();

    constructor(pageElementVo: PageElementVo, classArray: string[]) {
        this.pageElementVo = pageElementVo;
        this.classArray = classArray;
    }
    getShape() {

        //make an svg element
        let svg: SVGSVGElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
        //   svg.setAttribute("preserveAspectRatio", "none");
        //set the svg's id attribute to the eid of the pageElementVo
        svg.id = "svg" + this.pageElementVo.eid.toString();

        //place in absolute position
        svg.classList.add("fullLine");


        //make a path element
        let path: SVGPathElement = document.createElementNS("http://www.w3.org/2000/svg", "path");
        //set the path's d attribute to the 1st element of the content array of the pageElementVo
        path.setAttribute("d", this.pageElementVo.content[0]);

        //get the transform string
        let transformString: string = ShapeElement.getTransform(this.pageElementVo);

        //  //set the path's transform attribute to the transform string
        // path.setAttribute("transform", transformString);

        //create a css style declaration
        let cssStyle: CSSStyleDeclaration = document.createElement("div").style;
        cssStyle.transform = transformString;
        cssStyle.transitionProperty = "all";
        cssStyle.transitionDuration = "2s";

         //opacity
         let opacity: number = this.pageElementVo.extras.alpha/100;
         cssStyle.opacity=opacity.toString();

        //add the css style to the path element
        path.setAttribute("style", cssStyle.cssText);


        //set the path's fill attribute to the fill color of the pageElementVo
        path.setAttribute("fill", this.pageElementVo.extras.backgroundColor);

        //set the path's stroke attribute to the border color of the pageElementVo
        path.setAttribute("stroke", this.pageElementVo.extras.borderColor);

        //set the path's stroke-width attribute to the border thickness of the pageElementVo
        path.setAttribute("stroke-width", this.pageElementVo.extras.borderThick.toString());

        //append the path to the svg
        svg.appendChild(path);

        //return the svg
        return svg;


    }
    public static updateShape(element: SVGSVGElement, pageElementVo: PageElementVo) {
        //get the path element
        let path: SVGPathElement = <SVGPathElement>element.childNodes[0];

        //set the path's d attribute to the 1st element of the content array of the pageElementVo
        path.setAttribute("d", pageElementVo.content[0]);

        //get the transform string
        let transformString: string = ShapeElement.getTransform(pageElementVo);

        //create a css style declaration
        let cssStyle: CSSStyleDeclaration = document.createElement("div").style;
        cssStyle.transitionProperty = "all";
        cssStyle.transitionDuration = "2s";
        cssStyle.transform = transformString;

        //opacity
        let opacity: number = pageElementVo.extras.alpha/100;
        cssStyle.opacity=opacity.toString();
        

        //add the css style to the path element
        path.setAttribute("style", cssStyle.cssText);



        //set the path's fill attribute to the fill color of the pageElementVo
        path.setAttribute("fill", pageElementVo.extras.backgroundColor);

        //set the path's stroke attribute to the border color of the pageElementVo
        path.setAttribute("stroke", pageElementVo.extras.borderColor);

        //set the path's stroke-width attribute to the border thickness of the pageElementVo
        path.setAttribute("stroke-width", pageElementVo.extras.borderThick.toString());

    }

    public static getTransform(pageElementVo: PageElementVo) {
        let transformString: string = "";

        //add position
        let x: number = pageElementVo.x;
        let y: number = pageElementVo.y;
        transformString += " translate(" + x.toString() + "%," + y.toString() + "%)";

        //add rotation
        let angle: number = ((pageElementVo.extras.rotation) / 100) * 360;
        angle = Math.floor(angle);
        transformString += " rotate(" + angle.toString() + "deg)";

        //set the shape to the width of the screen
        //get the width of the screen
        let screenWidth: number = window.innerWidth;
        //the width of the shape is 100px
        let shapeWidth: number = 100;
        //the scale is the width of the screen divided by the width of the shape
        let scale: number = screenWidth / shapeWidth;
        //and then adjust that scale by the width of the pageElementVo
        scale = scale * (pageElementVo.w / 100);
        //set scale
        //let scale: number = ((pageElementVo.w) / 100);
        if (isNaN(scale) === true) {
            scale = 1;
        }
        transformString += " scale(" + scale.toString() + ")";

        if (pageElementVo.extras.skewY !== 0) {
            let skewAngleY: number = ((pageElementVo.extras.skewY) / 100) * 360;
            skewAngleY = Math.floor(skewAngleY);
            transformString += " skewY(" + skewAngleY.toString() + "deg)";
        }
        if (pageElementVo.extras.skewX !== 0) {
            let skewAngleX: number = ((pageElementVo.extras.skewX) / 100) * 360;
            skewAngleX = Math.floor(skewAngleX);
            transformString += " skewX(" + skewAngleX.toString() + "deg)";
        }
        if (pageElementVo.extras.flipV === true) {
            transformString += " scaleY(-1)";
        }
        if (pageElementVo.extras.flipH === true) {
            transformString += " scaleX(-1)";
        }

        return transformString;
    }
    getShapeStyle() {
        let style: any = {};
        style.fill = this.pageElementVo.extras.backgroundColor;
        style.stroke = this.pageElementVo.extras.borderColor;
        style.strokeWidth = this.pageElementVo.extras.borderThick;
        style.opacity = this.pageElementVo.extras.alpha.toString() + "%";
        style.transform = ShapeElement.getTransform(this.pageElementVo);

        return style;
    }
}