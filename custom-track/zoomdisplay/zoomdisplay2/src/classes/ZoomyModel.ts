import { TextStyleVo } from "../dataObjects/TextStyleVo";
import { ZoomPresentationVo } from "../dataObjects/ZoomPresentationVo";

export class ZoomyModel {
    private static instance: ZoomyModel;
    public mode: "auto" | "listen" | "read" = "auto";
    public speed: "slow" | "normal" | "fast" = "normal";
    public voiceSpeed: "slow" | "normal" | "fast" = "normal";

    public pageH: number = 0;
    public pageW: number = 0;

    public bgColor: string = "#eeeeee";
    public currentPage: number = 0;
    public presentation: ZoomPresentationVo = new ZoomPresentationVo();
    public preview: ZoomPresentationVo = new ZoomPresentationVo();
    public styleMap: Map<number, TextStyleVo> = new Map<number, TextStyleVo>();

    public paused: boolean = false;
    public mute: boolean = false;

    public posString: string = "";
    //text highlighting is off or on
    public textHighlights: boolean = false;
    public ratio:number=96/55;
    
    private constructor() {
        (window as any).zm = this;
    }
    public static getInstance(): ZoomyModel {
        if (ZoomyModel.instance == null) {
            ZoomyModel.instance = new ZoomyModel();
        }
        return ZoomyModel.instance;
    }

    public getCurvePath(index: number) {
        switch (index) {
            case 0:
                return "";
            case 1:
                return "M73.2,148.6c4-6.1,65.5-96.8,178.6-95.6c111.3,1.2,170.8,90.3,175.1,97";
            case 2:
                return "M 0 0 C 37.5 56.25 306.25 156.25 612.5 -37.5";
        }
        return "";
    }
    prevPage()
    {
        this.currentPage--;
        if (this.currentPage < 0) {
            this.currentPage = 0;
        }
        //console.log("prevPage: " + this.currentPage);
    }
    nextPage()
    {
        this.currentPage++;
        if (this.currentPage > this.presentation.steps.length-1) {
            this.currentPage = this.presentation.steps.length-1;
        }
        //console.log("nextPage: " + this.currentPage);
    }
    getAudio()
    {
        if(this.currentPage < this.presentation.steps.length)
        {
            let audio:string= this.presentation.steps[this.currentPage].audio;
            return this.getAudioPath(audio);
        }
        return "";
    }
    getFullPath(path: string) {
        return "https://ttv5.s3.amazonaws.com/william/images/bookimages/" + path;
    }

    getAudioPath(path: string) {

        return "https://ttv5.s3.amazonaws.com/" + path;
    }
    getBackgroundAudioPath(path: string) {
        return "https://ttv5.s3.amazonaws.com/" + path;
    }
}