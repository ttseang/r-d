import { HtmlObj } from "@teachingtextbooks/svggame";
import { PageElementVo } from "../dataObjects/PageElementVo";
import { StepVo } from "../dataObjects/StepVo";
import { ZoomPresentationVo } from "../dataObjects/ZoomPresentationVo";
import { DisplayElementVo } from "../dataObjects/DisplayElementVo";
import { LineElement } from "./elements/LineElement";
import { ShapeElement } from "./elements/ShapeElement";
import { ZoomyModel } from "./ZoomyModel";
import { ZoomVo } from "../dataObjects/ZoomVo";
import { UIManager } from "./UiManager";
import { ZoomyController } from "./ZoomyController";

export class ZoomPage {
    private zm: ZoomyModel = ZoomyModel.getInstance();
    private zoomyController: ZoomyController = ZoomyController.getInstance();
    private uiManager: UIManager = UIManager.getInstance();

    public currentPageNumber: number = 0;
    public zoomData: ZoomPresentationVo | null = null;
    public currentStep: StepVo | null = null;
    private currentElements: PageElementVo[] = [];
    private elementMap: Map<string, DisplayElementVo> = new Map<string, DisplayElementVo>();
    //
    //the foreignObject id=ptblank child of addArea where all the dynamic content is placed
    //
    public mainParent: HTMLElement | null = null;
    //
    //the svg element addArea
    //
    public svg: SVGSVGElement | null | undefined;
    public addArea: string = "addArea";
    public mainParentId: string = "ptblank";
    private oldZoom: string = "";
    private isPreview: boolean = false;



    constructor(isPreview: boolean = false) {
        (window as any).zp = this;
        this.isPreview = isPreview;
    }
    load(zoomData: ZoomPresentationVo, w: number = -1, h: number = -1) {
        ////console.log("LOAD");
        //console.log(zoomData);
        this.zoomData = zoomData;
        this.mainParent = document.getElementById(this.mainParentId);
        this.svg = (document.getElementById(this.addArea) as unknown) as SVGSVGElement;


        if (w === -1) {
            w = zoomData.pageW;
        }
        if (h === -1) {
            h = zoomData.pageH;
        }

        //console.log("w: " + w + " h: " + h);
        /*  w = zoomData.pageW;
         h = zoomData.pageH; */

        //get page width to height ratio
        let ratio: number = w / h;
        ////console.log("ratio: " + ratio);

        //set the page width to the window width and the height to the window width divided by the ratio
        //w = window.innerWidth;
        // h = w / ratio;

        //////console.log(w + "x" + h);

        if (this.mainParent) {
            if (this.isPreview) {
                this.mainParent.setAttribute("width", "140px");
                this.mainParent.setAttribute("height", "100px");
            }
            else {

                this.mainParent.setAttribute("width", w.toString());
                this.mainParent.setAttribute("height", h.toString());
            }
        }
        else {
            ////console.log("mainParent is null");
        }
    }
    public setPage(page: number) {
        this.currentPageNumber = page;
        if (this.zoomData) {
            this.currentStep = this.zoomData.steps[page];
            //////console.log(this.currentStep);
            this.currentElements = this.currentStep.elements;
            this.buildPage();
        }
    }
    private buildPage(): void {

        this.elementMap.forEach((value: DisplayElementVo, key: string) => { value.delFlag = true; });

        this.currentElements.forEach(element => {

            element.delFlag = true;
            if (this.mainParent) {

                /**
                 * does this element already exsist?
                 * if not add it
                 * if so update position and content
                 */
                if (!this.elementMap.has(element.eid)) {
                    ////console.log("adding element " + element.eid);
                    /**
                     *create the HTMLElement from the data obect
                     */
                    let displayElement: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement = element.getHtml();

                    /**
                     *add the data object to the current element map
                     */
                    this.elementMap.set(element.eid, new DisplayElementVo(element.eid, displayElement));

                    /**
                     * add the display object to the document
                     */
                    this.mainParent.append(displayElement);
                    // this.svg?.append(displayElement);

                    let cssStyle: CSSStyleDeclaration = element.getCSS();
                    this.uiManager.addStyle(element, cssStyle);
                }


                else {
                    // ////console.log("updating element " + element.eid);
                    let displayElementVo: DisplayElementVo | undefined = this.elementMap.get(element.eid);


                    if (displayElementVo) {

                        displayElementVo.delFlag = false;

                        switch (element.type) {
                            case PageElementVo.TYPE_LINE:
                                LineElement.updateLine(displayElementVo.displayElement as SVGAElement, element);
                                break;

                            case PageElementVo.TYPE_SHAPE:
                                ShapeElement.updateShape(displayElementVo.displayElement as SVGSVGElement, element);
                                break;

                            case PageElementVo.TYPE_CARD:
                                if (element.subType === PageElementVo.SUB_TYPE_WHITE_CARD) {
                                    let textID: string = element.eid + "-text";

                                    let textStyle: CSSStyleDeclaration = element.getTextCSS();

                                    /* //Testing highlight
                                    if (!this.zm.textHighlights) {
                                        textStyle.color = '#000';
                                    }

                                    this.zm.addStyleByID(textID, textStyle); */

                                    let textDiv: HtmlObj = new HtmlObj(textID);

                                    if (textDiv.el) {
                                        textDiv.el.innerText = element.content[0];
                                    }
                                }
                                if (element.subType === PageElementVo.SUB_TYPE_GREEN_CARD) {
                                    let textID: string = element.eid + "-text";
                                    let titleID: string = element.eid + "-title";

                                    let textDiv: HtmlObj = new HtmlObj(textID);
                                    let titleDiv: HtmlObj = new HtmlObj(titleID);

                                    if (titleDiv.el) {
                                        titleDiv.el.innerText = element.content[0];
                                    }
                                    if (textDiv.el) {
                                        textDiv.el.innerText = element.content[1];
                                    }
                                }

                                break;



                            default:

                                let css: CSSStyleDeclaration = element.getCSS();

                                displayElementVo.displayElement.setAttribute("style", css.cssText);

                                break;
                        }
                    }
                }
            }
        }
            // this.deleteUnneeded();
            // this.setZoom();
            /* if (playTheAudio === true) {
                this.playAudio();
                this.playBackgroundMusic();
            //}
 */
        );
        this.deleteUnneeded();
        this.setZoom();
        if (this.isPreview === false) {
            this.zoomyController.pageIsReady();
        }
    }
    setZoom(hard: boolean = false) {
        if (!this.currentStep) {
            return;
        }
        let pageW = this.zm.pageW;
        let pageH = this.zm.pageH;

        let zoomVo: ZoomVo = this.currentStep.zoom;
        ////////console.log(zoomVo);

        //turn percentages into pixels
        let xx: number = (zoomVo.x1 / 100) * pageW;
        let yy: number = (zoomVo.y1 / 100) * pageH;

        //round to 3 decimal places
        xx = Math.round(xx * 1000) / 1000;
        yy = Math.round(yy * 1000) / 1000;

        //measure the distance between the points
        let distX: number = (zoomVo.x2 - zoomVo.x1) / 100;
        let distY: number = (zoomVo.y2 - zoomVo.y1) / 100;

        //cut off the extra decimal places past 2 points
        distX = Math.round(distX * 100) / 100;
        distY = Math.round(distY * 100) / 100;

        let zoomX: number = (pageW * distX);
        let zoomY: number = (pageH * distY);

        //round to 3 decimal places
        zoomX = Math.round(zoomX * 1000) / 1000;
        zoomY = Math.round(zoomY * 1000) / 1000;


        let posString = xx.toString() + " " + yy.toString();
        posString += " " + zoomX + " " + zoomY;

        this.zm.posString = posString;

        //  ////console.log(posString);

        if (this.svg) {
            //
            if (this.oldZoom === "") {
                this.svg.setAttribute("viewBox", posString);
            }
            else {
                let zoomAnimation: SVGAnimateElement = (document.getElementById("zoom1") as unknown) as SVGAnimateElement;
                if (hard === false) {
                    zoomAnimation.setAttribute("values", this.oldZoom + ";" + posString);
                }
                else {
                    zoomAnimation.setAttribute("values", posString + ";" + posString);
                }

                zoomAnimation.beginElement();
            }

        }
        //record the position for animation
        this.oldZoom = posString;
    }
    deleteUnneeded() {
        //  ////console.log("delete unneeded");
        this.elementMap.forEach((el: DisplayElementVo) => {
            //       ////console.log(el.delFlag);
            if (el.delFlag === true) {
                if (el.displayElement && this.mainParent) {
                    this.mainParent.removeChild(el.displayElement);
                    this.elementMap.delete(el.eid);
                }
            }
        });
    }
}