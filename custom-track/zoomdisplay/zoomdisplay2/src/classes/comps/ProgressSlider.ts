import { UIManager } from "../UiManager";
import { ZoomPage } from "../ZoomPage";
import { ZoomyModel } from "../ZoomyModel";

export class ProgressSlider {
    private numberOfTicks: number = 2;
    private tickMarks: HTMLElement | null = null;
    private uiManager: UIManager = UIManager.getInstance();
    private zoomySlider: HTMLInputElement | null = null;
    public previewWindow: ZoomPage = new ZoomPage(true);
    private zm: ZoomyModel = ZoomyModel.getInstance();
    private callback: Function;
    constructor(callback: Function) {
        this.callback = callback;
        this.numberOfTicks=this.zm.presentation.steps.length;
        this.tickMarks = this.uiManager.getSingleElement("tickmarks");
        this.zoomySlider = this.uiManager.getSingleElement("zoomySlider") as HTMLInputElement;
        // if(this.tickMarks == null) throw new Error("ProgressSlider: constructor: tickMarks is null");
        this.makeTickMarks();
        this.previewWindow.addArea = "paddArea";
        this.previewWindow.mainParentId = "pptblank";
        // this.previewWindow.load(this.zm.presentation,100,100);

        (window as any).ps = this;
        /*   if (this.zoomySlider)
          {
              this.zoomySlider.addEventListener("onMouseUp",this.onSliderChange.bind(this));
          } */
        if (this.zoomySlider) {
             this.zoomySlider.addEventListener("input",this.onSliderMove.bind(this));
        }
    }
    makeTickMarks(): void {

        let tickMarkWidth: number = 100 / this.numberOfTicks;

        if (this.tickMarks) {
            for (let i = 1; i < this.numberOfTicks; i++) {
                let tickMark: HTMLElement = document.createElement("div");
                tickMark.classList.add("tickmark");
                //set the left position
                tickMark.style.left = (tickMarkWidth * i) + "%";
                this.tickMarks.appendChild(tickMark);
            }
        }
    }
    onSliderMove() {
        ////console.log("onSliderMove");
        this.uiManager.show("previewHolder");
        this.onSliderChange();
    }
    setStep(step:number)
    {
        if (this.zoomySlider) {
            let tickMarkWidth: number = 100 / this.numberOfTicks;
            let newPos: number = (tickMarkWidth * step);
            this.zoomySlider.value = newPos.toString();
            this.previewWindow.setPage(step);
            this.callback(newPos,step);
        }
       // this.onSliderChange();
    }
    onSliderChange(): void {
        let tickMarkWidth: number = 100 / this.numberOfTicks;
        //when the slider changes place the knob in the correct position between the tick marks
        let slider: HTMLInputElement = this.zoomySlider as HTMLInputElement;
        let sliderValue: number = parseInt(slider.value);
        //////console.log("sliderValue: " + sliderValue);
        let nearestTickMark: number = Math.round(sliderValue / tickMarkWidth);
        if (nearestTickMark > this.numberOfTicks - 1) nearestTickMark = this.numberOfTicks - 1;

    //      ////console.log("nearestTickMark: " + nearestTickMark);
        let newPos: number = (tickMarkWidth * nearestTickMark);
        this.previewWindow.setPage(nearestTickMark);
         // ////console.log("newPos: " + newPos);
        slider.value = newPos.toString();      

        this.callback(newPos,nearestTickMark);
    }
}