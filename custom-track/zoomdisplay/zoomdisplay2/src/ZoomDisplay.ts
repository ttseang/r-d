
import { UIManager } from "./classes/UiManager";
import { ZoomPage } from "./classes/ZoomPage";
import { ZoomyController } from "./classes/ZoomyController";
import { ZoomyModel } from "./classes/ZoomyModel";
import { ProgressSlider } from "./classes/comps/ProgressSlider";
import { AudioPlayer } from "./classes/util/AudioPlayer";
import { ZoomPresLoader } from "./classes/util/ZoomPresLoader";
import { ZoomPresentationVo } from "./dataObjects/ZoomPresentationVo";

export class ZoomDisplay {
    private uiManager: UIManager = UIManager.getInstance();
    private zoomyModel: ZoomyModel = ZoomyModel.getInstance();
    private zoomController: ZoomyController = ZoomyController.getInstance();
    private compilePreview: boolean = false;
    private zoomPage: ZoomPage | null = null;
    private audioPlayer:AudioPlayer = new AudioPlayer();

    private progressSlider: ProgressSlider | null = null;

    constructor() {
        document.onmousedown = this.onMouseDown.bind(this);
        document.onmouseup = this.onMouseUp.bind(this);
        this.zoomController.pageIsReady=this.pageIsReady.bind(this);
    }

    onMouseDown(e: MouseEvent) {
        const target: HTMLElement = e.target as HTMLElement;
        let id: string | null = target.id;

        if (id === null || id === "") {
            id = target.classList[0];
        }
        //  ////console.log("ZoomDisplay: onMouseDown: id: " + id);
        switch (id) {

            //button to open the bars
            case "btnNavDown":
                this.uiManager.hideGroup("gadgets");
                this.uiManager.toggleBars();
                break;

            //buttons to turn on the gadgets
            case "btnVolumeMusic":
                this.uiManager.hide("speedBalloon");
                this.uiManager.hide("speechSlider");
                this.uiManager.toggle("volumeSlider");
                break;
            case "btnVolume":
                this.uiManager.hide("volumeSlider");
                this.uiManager.hide("speedBalloon");
                this.uiManager.toggle("speechSlider");
                break;
            case "btnSpeed":
                this.uiManager.hide("volumeSlider");
                this.uiManager.toggle("speedBalloon");
                this.uiManager.hide("speechSlider");
                break;

            //start buttons
            case "btnAutoStart":
                this.uiManager.hideGroup("gadgets");
                this.uiManager.toggleGroup("bubbleGrid");
                this.uiManager.toggleBars();
                this.zoomyModel.mode = "auto";
                break;

            case "btnListenStart":
                this.uiManager.hideGroup("gadgets");
                this.uiManager.toggleGroup("bubbleGrid");
                this.uiManager.toggleBars();
                this.zoomyModel.mode = "listen";
                break;

            case "btnReadStart":
                this.uiManager.hideGroup("gadgets");
                this.uiManager.toggleGroup("bubbleGrid");
                this.uiManager.toggleBars();
                this.zoomyModel.mode = "read";
                break;

            //mode buttons
            case "btnAuto":
                this.zoomyModel.mode = "auto";
                break;

            case "btnRead":
                this.zoomyModel.mode = "read";
                break;

            case "btnListen":
                this.zoomyModel.mode = "listen";
                break;

            case "btnNext":
                let step: number = this.zoomyModel.currentPage;
                this.zoomyModel.nextPage();
                if (step !== this.zoomyModel.currentPage) {
                   
                    step=this.zoomyModel.currentPage;
                    if (this.zoomPage)
                    {
                        //console.log("change page");
                        if (this.progressSlider)
                        {
                            this.progressSlider.setStep(step);
                        }
                    }
                    
                }
                break;
            case "btnPrev":
                step = this.zoomyModel.currentPage;
                this.zoomyModel.prevPage();
                if (step !== this.zoomyModel.currentPage) {
                    step=this.zoomyModel.currentPage;
                    if (this.zoomPage)
                    {
                        if (this.progressSlider)
                        {
                            this.progressSlider.setStep(step);
                        }
                    }
                }
                break;

            //play speed buttons
            case "speedSlow":
                this.zoomyModel.speed = "slow";
                break;

            case "speedNormal":
                this.zoomyModel.speed = "normal";
                break;

            case "speedFast":
                this.zoomyModel.speed = "fast";
                break;

            case "hknob":
            case "highlightSwitch":
                this.uiManager.toggleElementClass("hknob", "selected");

            case "progressSlider":
                this.uiManager.show("previewHolder");

            default:
          //      ////console.log("ZoomDisplay: onMouseDown: id: " + id);
                break;
        }

        this.updateUI();
    }
    sliderUpdate(per:number,page:number)
    {
        ////console.log("ZoomDisplay: sliderUpdate: per: " + per);
        let previewWindow:HTMLElement | null=this.uiManager.getSingleElement("previewHolder");
        if(previewWindow)
        {
            previewWindow.style.left=per+"%";
        }
        if(this.zoomPage)
        {
            this.zoomyModel.currentPage=page;
            this.zoomPage.setPage(page);
        }
    }
    // update the UI based on the current state of the model
    updateUI() {
        if (this.uiManager.allGroupHasClass("gadgets", "hid")) {
            // ////console.log("ZoomDisplay: updateUI: gadgets are hidden");
            this.uiManager.removeGroupClass("gadgetParent", "open");
        }
        else {
            //  ////console.log("ZoomDisplay: updateUI: gadgets are visible");
            this.uiManager.addGroupClass("gadgetParent", "open");
        }
        switch (this.zoomyModel.mode) {
            case "auto":
                this.uiManager.changeGroupClass("tbButtons", "off", "on");
                this.uiManager.changeElementClass("btnAuto", "on", "off");
                break;
            case "listen":
                this.uiManager.changeGroupClass("tbButtons", "off", "on");
                this.uiManager.changeElementClass("btnListen", "on", "off");
                break;
            case "read":
                this.uiManager.changeGroupClass("tbButtons", "off", "on");
                this.uiManager.changeElementClass("btnRead", "on", "off");
                break;

            default:
                break;

        }

        this.uiManager.changeGroupClass("playspeed", "", "selected");
        switch (this.zoomyModel.speed) {
            case "slow":
                this.uiManager.changeElementClass("speedSlow", "selected", "");
                break;
            case "normal":
                this.uiManager.changeElementClass("speedNormal", "selected", "");
                break;
            case "fast":
                this.uiManager.changeElementClass("speedFast", "selected", "");
                break;

        }

        if (this.uiManager.barsOpen === true) {
            this.uiManager.changeElementClass("btnNavDown", "navFlip");
        }
        else {
            this.uiManager.changeElementClass("btnNavDown", "", "navFlip");
        }

    }
    onMouseUp(e: MouseEvent) {
        const target: HTMLElement = e.target as HTMLElement;
        let id: string | null = target.id;
        if (id === null || id === "") {
            id = target.classList[0];
        }
        switch (id) {
            //progress slider
            case "progressSlider":
                if (this.progressSlider) {
                    this.progressSlider.onSliderChange();
                }
             //   this.uiManager.hide("previewHolder");
                break;
            }
            // ////console.log("ZoomDisplay: onMouseUp: id: " + id);
        }
        //the page assets have loaded and the page is ready to be displayed
        //we can now play the audio
        pageIsReady()
        {
            let audioPath:string=this.zoomyModel.getAudio();
            if (audioPath !== "")
            {
                this.audioPlayer.playAudio(audioPath);
            }
        }
        //this loads the data from the database or a file
        loadPage() {
            let loader: ZoomPresLoader = new ZoomPresLoader(this.pageLoaded.bind(this));

            if (this.compilePreview === true) {

                let bookContentDiv: HTMLDivElement | null | undefined = window.parent.document.getElementById("bookData") as HTMLDivElement;

                if (bookContentDiv) {
                    let content: string = bookContentDiv.innerText;
                    loader.loadFromString(content);
                }
            }
            else {
                //    loader.loadFile("./assets/bigText.json");
                //loader.loadFile("./assets/zoomTest1.json");
                //    loader.loadFile("./assets/curveText.json");
                //    loader.loadFile("./assets/alfl_01.json");
                 // loader.loadFile("./assets/demo4.json");
                  loader.loadFile("./assets/TTZZ.json");
                //  loader.loadFile("./assets/mars.json");
            }
        }
        //the file or database data has been loaded
        pageLoaded(zoomData: ZoomPresentationVo) {
          //  ////console.log("ZoomDisplay: pageLoaded: zoomData: ");
          //  ////console.log(zoomData);
            this.zoomyModel.presentation = zoomData;
            this.zoomyModel.pageH = zoomData.pageH;
            this.zoomyModel.pageW = zoomData.pageW;
            
            this.progressSlider = new ProgressSlider(this.sliderUpdate.bind(this));
            this.progressSlider.previewWindow.load(zoomData,100,100*this.zoomyModel.ratio);
            this.progressSlider.previewWindow.setPage(0);

            this.zoomPage = new ZoomPage();
            this.zoomPage.load(zoomData);
            this.zoomPage.setPage(0);
            
            // this.updateUI();
        }
    }