export class TextPathVo
{
    public label:string;
    public pathIndex:number;
    public anchorX:string;

    constructor(label:string,pathIndex:number,anchorX:number)
    {
        this.label=label;
        this.pathIndex = pathIndex;
        this.anchorX = anchorX.toString();
    }
}