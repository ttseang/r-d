
import { BaseScreen, IScreen, HtmlObj } from "@teachingtextbooks/svggame";
import { BottomBar } from "../classes/BottomBar";
import { Gadgets } from "../classes/Gadgets";
import { NavBar } from "../classes/NavBar";
import { PreviewPage } from "../classes/PreviewPage";
import { LinkUtil } from "../classes/util/LinkUtil";
import { TextStyleMapLoader } from "../classes/util/TextStyleMapLoader";
import { VolBar } from "../classes/VolBar";
import { ZoomPage } from "../classes/ZoomPage";
import { ZoomPresLoader } from "../classes/ZoomPresLoader";
import { ZoomyController } from "../classes/ZoomyController";
import { ZoomyModel } from "../classes/ZoomyModel";
import { ZoomPresentationVo } from "../dataObjs/ZoomPresentationVo";

export class ScreenMain extends BaseScreen implements IScreen {
    private zoomPage: ZoomPage;
    private previewPage: PreviewPage;
    private zm: ZoomyModel = ZoomyModel.getInstance();
    private zc: ZoomyController = ZoomyController.getInstance();

    private navBar: NavBar;
    private bottomBar: BottomBar;

    //use true to compile for admin preview
    //use false for student display
    private compilePreview: boolean = false;
    private volBar: VolBar;
    private volBar2: VolBar;

    private gadgets: Gadgets;

    private overlay: HtmlObj = new HtmlObj("overlay");
    private copyrightOverlay: HtmlObj = new HtmlObj("copyrightOverlay");
    private buttonStart: HtmlObj = new HtmlObj("btnStart");
    private bubble: HtmlObj = new HtmlObj("bubble");
    private bubbleGrid: HtmlObj = new HtmlObj("bubbleGrid");

    constructor() {
        super("ScreenMain");
        (window as any).sm = this;

        this.buttonStart.visible = false;

        this.volBar = new VolBar("whitewell", "volknob1", "music", this.zm.mvol);
        this.volBar2 = new VolBar("whitewell2", "volknob2", "narration", this.zm.svol);

        this.zoomPage = new ZoomPage(this);
        this.previewPage = new PreviewPage(this);


        this.gadgets = new Gadgets();

        this.navBar = new NavBar(this);
        this.bottomBar = new BottomBar(this, () => { });
        this.navBar.bottomBar = this.bottomBar;
        this.navBar.closeBar();
        this.bottomBar.open();


    }
    create() {
        super.create();

        (window as any).scene = this;

        this.loadTextStyles();

        this.zc.audioEnded = this.audioEnded.bind(this);
        this.zc.audioStarted = this.audioStarted.bind(this);

        document.onpointermove = (e: PointerEvent) => {
            this.zc.documentMove(e);
        };
        document.onpointerup = (e: PointerEvent) => {
            this.zc.documentUp(e);
        };

    }
    loadTextStyles() {
        let cssLoader: TextStyleMapLoader = new TextStyleMapLoader(this.loadPage.bind(this));
        cssLoader.load("./assets/textStyleMap.json");
    }
    loadPage() {
        let loader: ZoomPresLoader = new ZoomPresLoader(this.pageLoaded.bind(this));

        if (this.compilePreview === true) {

            let bookContentDiv: HTMLDivElement | null | undefined = window.parent.document.getElementById("bookData") as HTMLDivElement;

            if (bookContentDiv) {
                let content: string = bookContentDiv.innerText;
                loader.loadFromString(content);
            }
        }
        else {
        //    loader.loadFile("./assets/bigText.json");
            loader.loadFile("./assets/zoomTest1.json");
          //  loader.loadFile("./assets/curveText.json");
        //     loader.loadFile("./assets/alfl_01.json");
          //    loader.loadFile("./assets/demo4.json");
        }
    }
    pageLoaded(zoomData: ZoomPresentationVo) {
        //console.log(data);
        console.log("Page loaded");
        this.zm.presentation = zoomData;
        this.zm.preview = zoomData;
        this.zoomPage.setData(zoomData);
        this.previewPage.setData(zoomData);
        this.zm.navProgSlider.setTotalPresentationSteps(this.zm.presentation.steps.length);
        this.setButtons();
        this.setSlider();

        this.buttonStart.visible = true;
        LinkUtil.makeLink("btnStart", this.startPres.bind(this), "", "");
        this.zoomPage.buildFrame(false);
        this.previewPage.buildFrame(false);

        LinkUtil.makeLink("btnAutoStart", this.doStart.bind(this), "auto", "");
        LinkUtil.makeLink("btnReadStart", this.doStart.bind(this), "read", "");
        LinkUtil.makeLink("btnListenStart", this.doStart.bind(this), "listen", "");

        document.body.style.backgroundColor = this.zm.bgColor;
    }
    audioEnded() {
        this.gadgets.audioEnded();
        this.nextPage();
    }
    audioStarted() {
        this.gadgets.audioStarted();
    }
    nextPage() {
        //console.log(this.zm.playMode);

        if (this.zm.mode === "auto") {
            this.zc.nextStep();
        }
    }
    doStart(action: string) {
       console.log("doStart", action);
        switch (action) {
            case "auto":
                this.zm.mode = "auto";
                this.zoomPage.playAudio();
                break;

            case "listen":
                this.zm.mode = "listen";
                this.zoomPage.playAudio();
                break;

            case "read":
                this.zm.mode = "read";
                break;

        }
        this.overlay.visible = false;
        this.bubble.visible = false;
        this.bubbleGrid.visible = false;
        
        this.gadgets.updateModeButtons();
        this.navBar.closeBar();
        this.zoomPage.playBackgroundMusic();
    }

    startPres() {
        this.buttonStart.visible = false;
        this.overlay.visible = false;
    }
    setButtons() {
        let btnNext: HTMLButtonElement = document.getElementById("btnNext") as HTMLButtonElement;
        let btnPrev: HTMLButtonElement = document.getElementById("btnPrev") as HTMLButtonElement;

        btnNext.onclick = () => { this.zc.nextStep(); };
        btnPrev.onclick = () => { this.zc.prevStep(); };
    }

    setSlider() {
        let knobSlider: HTMLInputElement = document.getElementById("progressSlider") as HTMLInputElement;

        knobSlider.onmouseup = () => { this.zc.jumpStep(knobSlider.value); };
    }

    doResize() {
        super.doResize();
    }

}