import { StepVo } from "./StepVo";


export class ZoomPresentationVo
{
    public steps:StepVo[];
    public pageW:number;
    public pageH:number;
    public attribution:string;
    public bgColor:string;

    constructor(steps:StepVo[]=[],pageW:number=0,pageH:number=0,backgroundColor:string="#eeeeee")
    {
        this.steps=steps;
        this.pageW=pageW;
        this.pageH=pageH;
        this.attribution = "not set";
        this.bgColor = backgroundColor;
    }
    fromObj(obj:any)
    {

       
     let fullH:number=window.innerHeight;
     let fullW: number = window.innerWidth;
        this.pageW=fullW;
        this.pageH=fullH;
        this.attribution = obj.attribution;
        this.bgColor = obj.bgColor;
        console.log(this.pageW,this.pageH);

        for (let i:number=0;i<obj.steps.length;i++)
        {
            let stepData:any=obj.steps[i];
            let stepVo:StepVo=new StepVo();
            stepVo.fromObj(stepData);
            this.steps.push(stepVo);
        }
    }
}