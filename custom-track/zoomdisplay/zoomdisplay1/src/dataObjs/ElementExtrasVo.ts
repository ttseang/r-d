import { PageElementVo } from "./PageElementVo";
import { TextPathVo } from "./TextPathVo";

export class ElementExtrasVo
{
    public type:string="";

    public alpha:number;
    public rotation:number;
    public flipH:boolean;
    public flipV:boolean;
    public orientation:number;
    public skewX:number;
    public skewY:number;

    public borderColor:string;
    public borderThick:number;
    public fontSize:number;
    public fontColor:string;
    public fontName:string;
    public backgroundColor:string;

    public backgroundPosX:number;
    public backgroundPosY:number;
    public backgroundSizeW:number;
    public backgroundSizeH:number;

    public popupID:number=-1;
    public textPath:TextPathVo=new TextPathVo("",0,0);

    constructor()
    {
        this.alpha=100;
        this.rotation=0;
        this.flipH=false;
        this.flipV=false;
        this.orientation=0;
        this.borderColor="#000000";
        this.backgroundColor="#ffffff";
        this.borderThick=0;
        this.skewX=0;
        this.skewY=0;

        this.fontSize=16;
        this.fontColor="#000000";
        this.fontName="Arial";

        this.backgroundPosX=0;
        this.backgroundPosY=0;
        this.backgroundSizeH=100;
        this.backgroundSizeW=100;
    }
   
    fromObj(obj:any)
    {
        this.alpha=parseFloat(obj.alpha);
        this.rotation=parseFloat(obj.rotation);
        this.flipH=(obj.flipH===true || obj.flipH==="true")?true:false;
        this.flipV=(obj.flipV===true || obj.flipV==="true")?true:false;
        this.orientation=parseInt(obj.orientation);
        this.borderColor=obj.borderColor;
        this.borderThick=parseFloat(obj.borderThick);
        this.skewX=parseFloat(obj.skewX);
        this.skewY=parseFloat(obj.skewY);

        this.fontSize=parseFloat(obj.fontSize);
        this.fontColor=obj.fontColor;
        this.fontName=obj.fontName;

        this.backgroundPosX=parseFloat(obj.backgroundPosX);
        this.backgroundPosY=parseFloat(obj.backgroundPosY);
        this.backgroundSizeH=parseFloat(obj.backgroundSizeH);
        this.backgroundSizeW=parseFloat(obj.backgroundSizeW);
        
        this.popupID=parseInt(obj.popupID);

        if (this.type===PageElementVo.TYPE_SHAPE)
        {
            /* "styleName": "default",
                            "strokeColor": "#ff0000",
                            "strokeWidth": 1,
                            "fillColor": "#ffffff" */
            this.backgroundColor=obj.backgroundColor;
            this.borderThick=parseFloat(obj.borderThick);
            this.borderColor=obj.borderColor;
          //  console.log("ElementExtrasVo.fromObj() this.borderColor="+this.borderColor);
        }

        if (obj.textCurve)
        {
            this.textPath=new TextPathVo(obj.textCurve.label,parseInt(obj.textCurve.pathIndex),parseInt(obj.textCurve.anchorX));
        }
    }
}