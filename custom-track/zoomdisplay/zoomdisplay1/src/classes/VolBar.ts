import { HtmlObj } from "@teachingtextbooks/svggame";
import { ZoomyController } from "./ZoomyController";

export class VolBar extends HtmlObj {
    public knob: HtmlObj;
    private per: number = 0;
    private isDragging: boolean = false;
    public active: boolean = false;
    private zc: ZoomyController = ZoomyController.getInstance();
    private type: string;

    constructor(mainEl: string, knobID: string, type: string, initVal: number) {
        super(mainEl);
        this.type = type;

        // (window as any).volBar = this;
        this.knob = new HtmlObj(knobID);

        if (this.knob.el) {

            this.knob.y = 75 - initVal;

            this.knob.el.onpointerdown = () => {
                this.isDragging = true;
                this.zc.documentMove = this.moveKnob.bind(this);
                this.zc.documentUp = this.onUp.bind(this);
            };
        }
    }
    onUp(e: PointerEvent) {
        if (this.isDragging == true) {

            //this.releaseCallback(this.per);
            this.zc.documentMove = () => { };
            this.zc.documentUp = () => { };
        }
        this.isDragging = false;
    }
    moveKnob(e: PointerEvent) {
        if (this.isDragging) {
            if (this.el) {
                let ty: number = this.el.getBoundingClientRect().y;
                let hh: number = this.el.getBoundingClientRect().height;


                let yy: number = ((e.y - ty) / hh) * 100;

                if (yy < 0) {
                    yy = 0;
                }
                if (yy > 75) {
                    yy = 75;
                }


                let per: number = Math.floor(yy);
                this.per = per;

                this.knob.y = per;
                //console.log(per);
                this.zc.setVol(this.type, per);
                //  this.callback(per);
            }
        }
    }

}
