export class NumUtil
{
    constructor()
    {

    }
    /**
     * 
     * @param value the number in string format
     * @returns a two decimal number
     */
    public static convertAndRound(value:string)
    {
        let num:number=parseFloat(value);
        num=Math.round(num*1000)/1000;
        return num;
    }
    /**
     * 
     * @param num
     * @returns a two decimal number
     * example 12.34368 = 12.34
     */
    public static roundDec(num:number)
    {
        num=Math.round(num*1000)/1000;
        return num;
    }
}