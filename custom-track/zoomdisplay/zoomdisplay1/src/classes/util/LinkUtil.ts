export class LinkUtil {
    constructor() {

    }
    public static makeLink(buttonID: string, callback: Function, action: string, params: string) {
        //let v: HTMLElement | null = document.getElementById(buttonID);
        let v: HTMLElement | null = document.getElementsByClassName(buttonID)[0] as HTMLElement;
        
        if (v) {
            v.addEventListener("click",() => { 
                ////console.log("click");
                callback(action, params)});
            ////console.log(v);
        }
    }
}