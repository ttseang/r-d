import { HtmlObj } from "@teachingtextbooks/svggame";
import { LinkUtil } from "./util/LinkUtil";
import { ZoomyController } from "./ZoomyController";
import { ZoomyModel } from "./ZoomyModel";

export class Gadgets {
    private zc: ZoomyController = ZoomyController.getInstance();
    private zm: ZoomyModel = ZoomyModel.getInstance();

    private volbar1: HtmlObj;
    private volbar2: HtmlObj;
    private speedBox: HtmlObj;
    private btnMute: HtmlObj;
    private btnPlay: HtmlObj;
    private btnAuto: HtmlObj;
    private btnListen: HtmlObj;
    private btnRead: HtmlObj;
    private btnMusic: HtmlObj;
    private btnVol: HtmlObj;
    private btnSpeed: HtmlObj;
    private textHighlightSwitch: HtmlObj;
    private btnCopyright: HtmlObj;

    //TODO:change play speed
    //TODO:change volume
    //TODO:change audio speed
    //TODO:change mode to listen
    //TODO:change mode to read
    //TODO:change mode to auto
    //TODO:change mode to manual

    constructor() {
        this.volbar1 = new HtmlObj("vol1");
        this.volbar2 = new HtmlObj("vol2");
        this.speedBox = new HtmlObj("speedBox");
        this.btnMute = new HtmlObj("btnMute");
        this.btnPlay = new HtmlObj("btnPlay");
        this.btnAuto = new HtmlObj("btnAuto");
        this.btnListen = new HtmlObj("btnListen");
        this.btnRead = new HtmlObj("btnRead");
        this.btnMusic = new HtmlObj("btnVolumeMusic");
        this.btnVol = new HtmlObj("btnVolume");
        this.btnSpeed = new HtmlObj("btnSpeed");
        this.textHighlightSwitch = new HtmlObj("highlightSwitch");
        this.btnCopyright = new HtmlObj("btnCopyright");

        this.volbar1.visible = false;
        this.volbar2.visible = false;
        this.speedBox.visible = false;

        LinkUtil.makeLink("btnVolume", this.doAction.bind(this), "toggle", "vol2");
        LinkUtil.makeLink("btnVolumeMusic", this.doAction.bind(this), "toggle", "vol1");
        LinkUtil.makeLink("btnMute", this.doAction.bind(this), "toggleaudio", "");
        LinkUtil.makeLink("btnSpeed", this.doAction.bind(this), "toggle", "speed");

        LinkUtil.makeLink("fastrow", this.doAction.bind(this), "speed", "fast");
        LinkUtil.makeLink("normalrow", this.doAction.bind(this), "speed", "normal");
        LinkUtil.makeLink("slowrow", this.doAction.bind(this), "speed", "slow");

        LinkUtil.makeLink("btnPlay", this.doAction.bind(this), "toggle", "play");
        LinkUtil.makeLink("btnAuto", this.doAction.bind(this), "mode", "auto");
        LinkUtil.makeLink("btnListen", this.doAction.bind(this), "mode", "listen");
        LinkUtil.makeLink("btnRead", this.doAction.bind(this), "mode", "read");

        LinkUtil.makeLink("highlightSwitch", this.doAction.bind(this), "toggle", "highlight");
        LinkUtil.makeLink("btnCopyright", this.doAction.bind(this), "toggle", "copyright");

        this.zc.hideGadgets = this.hideGadgets.bind(this);

        this.zc.setVol = this.setVol.bind(this);
    }
    setVol(type: string, per: number) {
        //console.log(type + " " + per);

        if (type == "music") {
            this.zm.mvol = 100 - (per / 75) * 100;
        }
        else {
            this.zm.svol = 100 - (per / 75) * 100;
        }

    }
    hideGadgets() {
        this.volbar1.visible = false;
        this.volbar2.visible = false;
        this.speedBox.visible = false;
    }
    doAction(action: string, params: string) {
        let value: boolean = false;

        switch (action) {
            case "toggle":
                if (params === "vol1") {
                    value = !this.volbar1.visible;
                    this.hideGadgets();
                    this.volbar1.visible = value;
                }
                if (params === "vol2") {
                    if (this.zm.mode !== "read") {
                        value = !this.volbar2.visible;
                        this.hideGadgets();
                        this.volbar2.visible = value;
                    }
                }
                if (params === "speed") {
                    if (this.zm.mode !== "read") {
                        value = !this.speedBox.visible;
                        this.hideGadgets();
                        this.speedBox.visible = value;
                    }
                }
                if (params === "play") {
                    if (this.zm.mode !== "read") {

                        this.zm.paused = !this.zm.paused;
                        this.zc.playPause();
                        console.log(this.zm.paused);

                        if (this.zm.paused === true) {
                            console.log("PAUSE!");
                            this.btnPlay.el?.classList.remove("paused");
                            // this.zc.pauseAudio();
                        }
                        else {
                            console.log("UNPAUSED");
                            this.btnPlay.el?.classList.add("paused");
                            //  this.zc.playAudio();
                        }
                    }
                }

                if (params === "highlight") {
                    if (this.zm.textHighlights) {
                        this.zm.textHighlights = false;
                        this.zm.textHighlightToggle();
                    } else {
                        this.zm.textHighlights = true;
                        this.zm.textHighlightToggle();
                    }
                }

                if (params === "copyright") {
                    //do action here
                    this.zm.toggleCopyrightPanel();
                }

                break;

            case "toggleaudio":

                this.zm.mute = !this.zm.mute;
                //console.log(this.btnMute);

                if (this.btnMute) {
                    if (this.btnMute.el) {
                        //console.log(this.zm.mute);

                        if (this.zm.mute === true) {
                            this.btnMute.el.classList.remove("unmuted");
                            this.btnMute.el.classList.add("muted");
                        }
                        else {
                            this.btnMute.el.classList.add("unmuted");
                            this.btnMute.el.classList.remove("muted");
                        }
                    }
                }

            case "speed":

                this.speedBox.visible = false;

                switch (params) {
                    case "fast":
                        this.zm.speed = 2;
                        break;

                    case "normal":
                        this.zm.speed = 1;
                        break;

                    case "slow":
                        this.zm.speed = 0.5;
                        break;
                }
                break;

            case "mode":

                this.hideGadgets();
                this.zm.mode = params;
                this.updateModeButtons();
        }

    }
    public audioStarted() {
        this.btnPlay.el?.classList.add("paused");
    }
    public audioEnded() {
        if (this.zm.mode === "listen") {
            this.zm.paused = false;
            this.btnPlay.el?.classList.remove("paused");
        }
    }
    updateModeButtons() {
        console.log(this.btnListen);


        if (this.btnAuto.el) {
            if (this.zm.mode === "auto") {
                this.btnAuto.el.classList.remove("autoToggledOff");
                this.btnAuto.el.classList.add("autoToggledOn");
            }
            else {
                this.btnAuto.el.classList.add("autoToggledOff");
                this.btnAuto.el.classList.remove("autoToggledOn");
            }

        }


        if (this.btnListen.el) {
            if (this.zm.mode === "listen") {
                this.btnListen.el.classList.add("listenToggledOn");
                this.btnListen.el.classList.remove("listenToggledOff");
            }
            else {
                this.btnListen.el.classList.remove("listenToggledOn");
                this.btnListen.el.classList.add("listenToggledOff");
            }
        }
        if (this.btnRead.el) {
            if (this.zm.mode === "read") {
                this.btnRead.el.classList.add("btnReadToggledOn");
                this.btnRead.el.classList.remove("btnToggledOff");
                if (this.btnPlay.el) {
                    this.btnPlay.el.classList.add("greyout");
                }
                if (this.btnVol.el) {
                    this.btnVol.el.classList.add("greyout");
                }
                if (this.btnSpeed.el) {
                    this.btnSpeed.el.classList.add("greyout");
                }
            }
            else {
                this.btnRead.el.classList.remove("btnReadToggledOn");
                this.btnRead.el.classList.add("btnToggledOff");
                if (this.btnPlay.el) {
                    this.btnPlay.el.classList.remove("greyout");
                }
                if (this.btnVol.el) {
                    this.btnVol.el.classList.remove("greyout");
                }
                if (this.btnSpeed.el) {
                    this.btnSpeed.el.classList.remove("greyout");
                }
            }
        }
    }

}