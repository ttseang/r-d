import { PageElementVo } from "../dataObjs/PageElementVo";
import { TextStyleVo } from "../dataObjs/TextStyleVo";
import { ZoomPresentationVo } from "../dataObjs/ZoomPresentationVo";
import { ZoomyController } from "./ZoomyController";
import { ProgressSlider } from "./ProgressSlider";

export class ZoomyModel {
    public styleDiv: HTMLStyleElement | null = null;

    private static instance: ZoomyModel | null = null;
    public pageH: number = 0;
    public pageW: number = 0;
    public attribution: string = "not set";
    public bgColor: string = "#eeeeee";

    private zc: ZoomyController = ZoomyController.getInstance();

    public presentation: ZoomPresentationVo = new ZoomPresentationVo();
    public preview: ZoomPresentationVo = new ZoomPresentationVo();
    public styleMap: Map<number, TextStyleVo> = new Map<number, TextStyleVo>();

    public currentStep: number = 0;
    public previewStep: number = 0;
    private _speed: number = 1;

    //music volume
    private _mvol: number = 10;

    //speaking volume
    private _svol: number = 80;

    private _mute: boolean = false;
    private _mode: string = "auto";

    //  public playMode:"auto"|"manual"="auto";
    public paused: boolean = false;

    //text highlighting is off or on
    public textHighlights: boolean = false;

    //create an instance for the Nav Progress Slider
    public navProgSlider: ProgressSlider;

    //is Copyright panel showing
    public copyRightShowing: boolean = false;

    //position of zoom 
    public posString: string = "";

    constructor() {
        this.styleDiv = document.getElementById("elementStyles") as HTMLStyleElement;

        (window as any).zm = this;

        this.navProgSlider = new ProgressSlider(document.getElementById("progressSlider") as HTMLInputElement,
            document.getElementById("selector") as HTMLElement, document.getElementById("progressBar") as HTMLElement,
            document.getElementById("sectionNumber") as HTMLElement, document.getElementById("sectionMarksContainer") as HTMLElement,
            document.getElementById("knob") as HTMLElement, document.getElementById("selectionPreview") as HTMLElement,
            ZoomyController.getInstance());

    }
    public static getInstance() {
        if (this.instance === null) {
            this.instance = new ZoomyModel();
        }
        return this.instance;
    }
    public set mute(val: boolean) {
        this._mute = val;
        this.zc.muteChanged();
    }
    public get mute() {
        return this._mute;
    }
    public set mvol(val: number) {
        this._mvol = val;
        this.zc.audioUpdated();
    }
    public get mvol() {
        return this._mvol;
    }
    public set svol(val: number) {
        this._svol = val;
        this.zc.audioUpdated();
    }
    public get svol() {
        return this._svol;
    }
    public set speed(val: number) {
        this._speed = val;
        this.zc.audioUpdated();
    }
    public get speed() {
        return this._speed;
    }
    public set mode(val: string) {
        //console.log("set mode "+val);
        this._mode = val;
        this.zc.modeChanged();
    }
    public get mode() {
        return this._mode;
    }
    getFullPath(path: string) {
        return "https://ttv5.s3.amazonaws.com/william/images/bookimages/" + path;
    }

    getAudioPath(path: string) {

        return "https://ttv5.s3.amazonaws.com/" + path;
    }
    getBackgroundAudioPath(path: string) {
        return "https://ttv5.s3.amazonaws.com/" + path;
    }
    addStyle(elementVo: PageElementVo, style: CSSStyleDeclaration) {
        if (this.styleDiv) {

            let cRule = "#" + elementVo.id + "{" + style.cssText + "}";

            this.styleDiv.append(cRule);
        }
    }
    addStyleByID(elementID: string, style: CSSStyleDeclaration) {
        if (this.styleDiv) {
            let cRule = "#" + elementID + "{" + style.cssText + "}";
            this.styleDiv.append(cRule);
        }
    }

    textHighlightToggle() {
        var NAME = document.getElementById("highlightSwitch")!;
        if (this.textHighlights) {
            NAME.className = "toggleSwitchOn";
        } else {
            NAME.className = "toggleSwitch";
        }
    }

    setAttribution() {
        var attributeText: HTMLElement = document.getElementById("copyrightText") as HTMLElement;
        attributeText.innerText = this.presentation.attribution;
    }

    toggleCopyrightPanel() {
        var copyPanel = document.getElementById("copyrightOverlay")!;
        if (!this.copyRightShowing) {
            copyPanel.className = "visibleOn";
            this.copyRightShowing = true;
            this.setAttribution();
        } else {
            copyPanel.className = "visibleOff";
            this.copyRightShowing = false;
            console.log("Copyright Showing: " + this.copyRightShowing.toString());
        }
    }
    public getCurvePath(index: number) {
        switch (index) {
            case 0:
                return "";
            case 1:
                return "M73.2,148.6c4-6.1,65.5-96.8,178.6-95.6c111.3,1.2,170.8,90.3,175.1,97";
            case 2:
                return "M 0 0 C 37.5 56.25 306.25 156.25 612.5 -37.5";
        }
        return "";
    }
}