import { HtmlObj, IScreen } from "@teachingtextbooks/svggame";
import { ProgBar } from "./ProgBar";
import { ToggleSwitch } from "./ToggleSwitch";
import { ZoomyController } from "./ZoomyController";
import { ZoomyModel } from "./ZoomyModel";


export class BottomBar extends HtmlObj {
    public progBar: ProgBar = new ProgBar();
    public toggleSwitch: ToggleSwitch;
    private screen: IScreen;
    private zm: ZoomyModel = ZoomyModel.getInstance();
    private zc: ZoomyController = ZoomyController.getInstance();

    constructor(screen: IScreen, progCallback: Function) {
        super("bottomBar");
        this.screen = screen;
        this.progBar.callback = progCallback;
        this.toggleSwitch = new ToggleSwitch("highlightSwitch");

    }
    private updatePage() {

    }
    public close() {
        //  this.y=100;
        // this.y=this.screen.gh*.5;
        if (this.el) {
            //  this.el.style.top = "18vh";
            this.el.classList.add("bottomBarClosed");
            this.el.classList.remove("bottomBarOpen");
        }
        this.zc.hideGadgets();
    }
    public open() {
        // this.y=86;
        // this.y=0;
        if (this.el) {
            // this.el.style.top = "10%";
            this.el.classList.remove("bottomBarClosed");
            this.el.classList.add("bottomBarOpen");
        }
    }

}