import { HtmlObj, IScreen, SoundObj } from "@teachingtextbooks/svggame";
import { DisplayElementVo } from "../dataObjs/DisplayElementVo";
import { PageElementVo } from "../dataObjs/PageElementVo";
import { StepVo } from "../dataObjs/StepVo";
import { ZoomPresentationVo } from "../dataObjs/ZoomPresentationVo";
import { ZoomVo } from "../dataObjs/ZoomVo";
import { LineElement } from "./Elements/LineElement";
import { ZoomyController } from "./ZoomyController";
import { ZoomyModel } from "./ZoomyModel";

export class PreviewPage {
    private screen: IScreen;
    private audioHasEnded: boolean = false;

    private step: StepVo = new StepVo(0, []);

    //a map of all the elements currently on the page
    private currentElements: Map<string, DisplayElementVo> = new Map<string, DisplayElementVo>();

    //the svg element previewArea
    public svg: SVGSVGElement | null | undefined;

    //the foreignObject id=prevblank child of previewArea where all the dynamic content is placed
    public mainParent: HTMLElement | null = null;

    private zm: ZoomyModel = ZoomyModel.getInstance();
    private zc: ZoomyController = ZoomyController.getInstance();

    //record the old zoom coodinates to use in the zoom animation
    private oldZoom: string = "";

    private audioPlayer: SoundObj | null = null;
    private backgroundPlayer: SoundObj | null = null;
    private lastBackgroundSound: string = "";

    constructor(screen: IScreen) {
        this.screen = screen;

        this.mainParent = document.getElementById("prevblank");
        this.svg = (document.getElementById("previewArea") as unknown) as SVGSVGElement;
        //
        //
        //bind the step functions to the main controller
        this.zc.previewJump = this.previewJump.bind(this);

        (window as any).zp = this;

        window.onresize = () => {
            this.zm.pageH = window.innerHeight;
            this.zm.pageW = window.innerWidth;

            if (this.mainParent) {
                this.mainParent.setAttribute("width", "140px");
                this.mainParent.setAttribute("height", "100px");
            }

            this.setZoom(true);
        };
    }
    setData(zoomPres: ZoomPresentationVo) {

        let w: number = zoomPres.pageW;
        let h: number = zoomPres.pageH;

        console.log(w + "x" + h);

        if (this.mainParent) {
            this.mainParent.setAttribute("width", "140px");
            this.mainParent.setAttribute("height", "100px");
        }


        this.zm.pageH = h;
        this.zm.pageW = w;

        this.step = zoomPres.steps[0];
        // //console.log(this.step);
        // this.buildFrame();
    }

    previewJump(silderValue: number | string) {

        var newStep = Number(silderValue);
        if (this.zm.previewStep != newStep) {
            this.zm.previewStep = newStep;
            this.pageChanged();
        }

    }

    private pageChanged() {
        this.step = this.zm.preview.steps[this.zm.previewStep];
        this.buildFrame();
    }

    setZoom(hard: boolean = false) {

        let pageW = this.zm.pageW;
        let pageH = this.zm.pageH;

        let zoomVo: ZoomVo = this.step.zoom;
        ////console.log(zoomVo);

        //turn percentages into pixels
        let xx: number = (zoomVo.x1 / 100) * pageW;
        let yy: number = (zoomVo.y1 / 100) * pageH;

        //measure the distance between the points
        let distX: number = (zoomVo.x2 - zoomVo.x1) / 100;
        let distY: number = (zoomVo.y2 - zoomVo.y1) / 100;

        //cut off the extra decimal places past 2 points
        distX = Math.round(distX * 100) / 100;
        distY = Math.round(distY * 100) / 100;

        let zoomX: number = (pageW * distX);
        let zoomY: number = (pageH * distY);

        let posString = xx.toString() + " " + yy.toString();
        posString += " " + zoomX + " " + zoomY;


        console.log(posString);

        if (this.svg) {
            //
            if (this.oldZoom === "") {
                this.svg.setAttribute("viewBoxB", posString);
            }
            else {
                let zoomAnimation: SVGAnimateElement = (document.getElementById("zoomA") as unknown) as SVGAnimateElement;
                if (hard === false) {
                    zoomAnimation.setAttribute("values", this.oldZoom + ";" + posString);
                }
                else {
                    zoomAnimation.setAttribute("values", posString + ";" + posString);
                }

                zoomAnimation.beginElement();
            }

        }
        //record the position for animation
        this.oldZoom = posString;
    }
    deleteUnneeded() {
        this.currentElements.forEach((el: DisplayElementVo) => {
            if (el.delFlag === true) {
                if (el.displayElement && this.mainParent) {
                    this.mainParent.removeChild(el.displayElement);
                    this.currentElements.delete(el.eid);
                }
            }
        });
    }

    buildFrame(playTheAudio: boolean = true) {
        this.currentElements.forEach((el: DisplayElementVo) => { el.delFlag = true; });
        // console.log("build frame");

        let elements: PageElementVo[] = this.step.elements;

        ////console.log(elements);
        for (let i: number = 0; i < elements.length; i++) {
            let element: PageElementVo = elements[i];
            if (this.mainParent) {

                /**
                 * does this element already exsist?
                 * if not add it
                 * if so update position and content
                 */

                if (!this.currentElements.has(element.id)) {

                    /**
                     *create the HTMLElement from the data obect
                     */
                    let displayElement: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement = element.getHtml();

                    /**
                     *add the data object to the current element map
                     */
                    this.currentElements.set(element.eid, new DisplayElementVo(element.eid, displayElement));

                    /**
                     * add the display object to the document
                     */
                    this.mainParent.append(displayElement);
                    // this.svg?.append(displayElement);

                    let cssStyle: CSSStyleDeclaration = element.getCSS();
                    this.zm.addStyle(element, cssStyle);
                }


                else {

                    let displayElementVo: DisplayElementVo | undefined = this.currentElements.get(element.eid);

                    ////console.log(displayElementVo);
                    if (displayElementVo) {

                        displayElementVo.delFlag = false;

                        if (element.type === PageElementVo.TYPE_LINE) {
                            //   //console.log("update line "+element.eid+" "+element.id);

                            LineElement.updateLine(displayElementVo.displayElement as SVGAElement, element);
                        }
                        else {
                            if (element.type === PageElementVo.TYPE_CARD) {
                                if (element.subType === PageElementVo.SUB_TYPE_WHITE_CARD) {
                                    let textID: string = element.eid + "-text";

                                    let textStyle: CSSStyleDeclaration = element.getTextCSS();

                                    //Testing highlight
                                    if (!this.zm.textHighlights) {
                                        textStyle.color = '#000';
                                    }

                                    this.zm.addStyleByID(textID, textStyle);

                                    let textDiv: HtmlObj = new HtmlObj(textID);

                                    if (textDiv.el) {
                                        textDiv.el.innerText = element.content[0];
                                    }
                                }
                                if (element.subType === PageElementVo.SUB_TYPE_GREEN_CARD) {
                                    let textID: string = element.eid + "-text";
                                    let titleID: string = element.eid + "-title";

                                    let textDiv: HtmlObj = new HtmlObj(textID);
                                    let titleDiv: HtmlObj = new HtmlObj(titleID);

                                    if (titleDiv.el) {
                                        titleDiv.el.innerText = element.content[0];
                                    }
                                    if (textDiv.el) {
                                        textDiv.el.innerText = element.content[1];
                                    }
                                }
                            }
                            let css: CSSStyleDeclaration = element.getCSS();

                            displayElementVo.displayElement.setAttribute("style", css.cssText);



                        }
                    }
                }
            }
        }
        this.deleteUnneeded();
        this.setZoom();

    }
}