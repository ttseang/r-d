export class ZoomyController
{
    private static instance:ZoomyController | null=null;

    public nextStep:Function=()=>{};
    public prevStep:Function=()=>{};
    public jumpStep:Function=(value:number|string)=>{};
    public previewJump:Function=(value:number|string)=>{};

    public documentMove:Function=(e: PointerEvent)=>{};
    public documentUp:Function=(e:PointerEvent)=>{};

    public hideGadgets:Function=()=>{};
    public setVol:Function=(type:string,per:number)=>{};
    public audioUpdated:Function=()=>{};
    
    public setSpeed:Function=(speed:string)=>{};

    public audioEnded:Function=()=>{};
    public audioStarted:Function=()=>{};
    public muteChanged:Function=()=>{};
    public modeChanged:Function=()=>{};

    public playPause:Function=()=>{};
    public pauseAudio:Function=()=>{};
    public playAudio:Function=()=>{};

    public textHighlightToggle:Function=()=>{};
    public toggleCopyright:Function =()=>{};

    constructor()
    {
        (window as any).zc=this;
    }
    public static getInstance()
    {
        if (this.instance===null)
        {
            this.instance=new ZoomyController();
        }
        return this.instance;
    }
}