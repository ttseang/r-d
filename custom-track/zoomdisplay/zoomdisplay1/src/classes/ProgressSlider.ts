import { position } from "html2canvas/dist/types/css/property-descriptors/position";
import { ZoomyController } from "./ZoomyController";

export class ProgressSlider{
    public progressSilderOBJ: HTMLInputElement;
    public slider:HTMLElement;
    public sliderKnob:HTMLElement;
    public progressBar:HTMLElement;
    private totalPresSteps: number = 0;
    private singleStepSection:number = 0;
    private sectionPreview:HTMLElement;
    private sectionNumber:HTMLElement;
    private sectionTickContainer:HTMLElement;
    private controller:ZoomyController;
    


    constructor(progSlider:HTMLInputElement, knob:HTMLElement, barFill:HTMLElement, previewText:HTMLElement, 
        tickContainer:HTMLElement, knobButton:HTMLElement, previewPanel:HTMLElement, zoomController:ZoomyController ){
        this.progressSilderOBJ = progSlider;
        this.slider = knob;
        this.sliderKnob = knobButton;
        this.progressBar = barFill;
        this.sectionNumber = previewText;
        this.sectionPreview = previewPanel;
        this.sectionTickContainer = tickContainer;
        this.controller = zoomController;
        this.progressSilderOBJ.addEventListener("input",()=>{
            this.setSectionPreview(true, this.getNewStep(this.progressSilderOBJ.value));
            this.setSliderPosition((Number(this.progressSilderOBJ.value)/this.singleStepSection));
        });
        this.progressSilderOBJ.addEventListener("mousedown", ()=>{
            this.setSectionPreview(true, this.getNewStep(this.progressSilderOBJ.value));
        })
        this.progressSilderOBJ.addEventListener("mouseup", ()=>{
            this.setSectionPreview(false, 0);
           
        })
        
    }

    printSliderValue(){
        console.log("Slider Value NOW: " + this.progressSilderOBJ.value);
    }

    getNewStep(silderValue:number|string){
        return Number(silderValue)/this.singleStepSection;
    }

    setTotalPresentationSteps(numberOfSteps:number){
        this.totalPresSteps = numberOfSteps;
        //console.log("number of presentation steps: " + this.totalPresSteps);
        this.setSliderSteps(this.totalPresSteps-1);
        this.setSectionText(0);
        this.createSectionMarks(this.sectionTickContainer);
    }

    setSliderSteps(totalSteps:number){
        this.singleStepSection = Math.round(100/totalSteps);
        this.progressSilderOBJ.step = String(this.singleStepSection);

    }

    setSliderPosition(currentPosition:string|number){
        currentPosition = Number(currentPosition);
        //console.log("This is the current value of the slider: " + (Number(currentPosition)*this.singleStepSection));
        this.progressSilderOBJ.value = String((Number(currentPosition)*this.singleStepSection));
        this.sectionPreview.style.left = this.progressSilderOBJ.value + '%'
        if(currentPosition != 0){
            this.sectionPreview.style.translate = '-' + ((Number(this.progressSilderOBJ.value)/this.singleStepSection)*7) + 'px';
        }else{
            this.sectionPreview.style.translate = '-0px';
        }
        this.progressSilderOBJ.style.background = 'linear-gradient(to right, #5B8ECC 0%, #5B8ECC '+ this.progressSilderOBJ.value + '%, #fff ' + this.progressSilderOBJ.value + '%, #fff 100%)';
        this.setSectionText(currentPosition);
        this.updateMarks(this.progressSilderOBJ.value);
    }

    setSectionText(currentPosition:number){
        this.sectionNumber.innerHTML = String(currentPosition+1) + "/" + String(this.totalPresSteps);
    }

    createSectionMarks(tickMarkDiv:HTMLElement){
        for(var i = 0; i < this.totalPresSteps; i++){
            const div = document.createElement("div");
            var sectionName = "section" + i.toString();
            div.id = sectionName;
            div.classList.add("sectionMarks");
            div.dataset.step = i.toString();
            if(i == this.totalPresSteps-1){
                div.style.right = `0%`;
                div.style.float = 'right';
            }else{
                div.style.left = `${(this.singleStepSection * i)}%`;
                if(i != 0){
                    div.style.translate = '-'+(i*(i*4))+'px';
                    //this.slider.style.translate = '-'+(i*4)+'px';
                    //console.log('Translation: ' + ((i+1)*44));
                }
                else{
                    div.style.visibility = 'hidden';
                }
            }

            div.addEventListener("mousedown",()=>{
                //console.log("Clicked: " + div.id);
                this.setSliderPosition(Number(div.id.substring(7, div.id.length)));
                this.controller.jumpStep(Number(div.id.substring(7, div.id.length))*this.singleStepSection);

             });

            tickMarkDiv.appendChild(div);
        }
    }

    updateMarks(sliderValue:string|number){
        for(var i=0; i < this.totalPresSteps; i++){
            var idName:string = 'section'+i;
            var markToChange:HTMLElement = document.getElementById(idName)!;
            if(i == 0){
                markToChange.style.backgroundColor = "#ffffff";
            };

            if(Number(sliderValue) > (i*this.singleStepSection)){
                markToChange.style.backgroundColor ="#ffffff";
                markToChange.style.visibility = "visible";
            } else if(Number(sliderValue) == (i*this.singleStepSection)){
                markToChange.style.visibility = "hidden";
            }
            else{
                markToChange.style.backgroundColor = "#598CC9";
                markToChange.style.visibility = "visible";
            }
        }
    }

    public setSectionPreview(show:boolean, step:number){
        if(show){
            this.sectionPreview.style.visibility = "visible";
            this.controller.previewJump(step);
        } else {
            this.sectionPreview.style.visibility = "hidden";
        }
    }
    
}

