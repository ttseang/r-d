import { ZoomPage } from "./ZoomPage";
import { ZoomPresentationVo } from "./dataObjs/ZoomPresentationVo";
import { ClickManager } from "./managers/ClickManager";
import { ZoomyModel } from "./mc/ZoomyModel";
import { ZoomPresLoader } from "./util/ZoomPresLoader";

export class ZoomDisplay {
    private loader: ZoomPresLoader;
    private zoomPage: ZoomPage;
    private clickManager: ClickManager = new ClickManager();
    private zm: ZoomyModel = ZoomyModel.getInstance();
    private compilePreview: boolean = false;
    constructor() {
        const pageContainer: SVGAElement = document.getElementsByClassName("pageContainer")[0] as SVGAElement;
        const styleContainer: HTMLStyleElement = document.getElementsByClassName("elementStyles")[0] as HTMLStyleElement;

        this.zoomPage = new ZoomPage(pageContainer, styleContainer);
        this.loader = new ZoomPresLoader(this.presLoaded.bind(this));
        if (this.compilePreview === true) {

            let bookContentDiv: HTMLDivElement | null | undefined = window.parent.document.getElementById("bookData") as HTMLDivElement;


            if (bookContentDiv) {
                let content: string = bookContentDiv.innerText;
                this.loader.loadFromString(content);
            }
            return;
        }

        //this.loader.loadFile("./assets/files/TTZZ.json");
        // this.loader.loadFile("./assets/files/ROME.json");
        this.loader.loadFile("./assets/files/shapes.json");
    }


    private presLoaded(data: any) {

        let zoomPres: ZoomPresentationVo = new ZoomPresentationVo([], 300, 300);
        zoomPres.fromObj(data);

        //set the css variables
        document.documentElement.style.setProperty('--wratio', zoomPres.scaleW.toString());

        this.zm.presentation = zoomPres;
        this.zoomPage.setData(zoomPres);
        this.zoomPage.setPage(0);
    }

}