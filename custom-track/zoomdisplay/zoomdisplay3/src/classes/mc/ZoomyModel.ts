import { ZoomPresentationVo } from "../dataObjs/ZoomPresentationVo";

export class ZoomyModel
{
    private static _instance:ZoomyModel;

    public presentation:ZoomPresentationVo|null=null;

    constructor()
    {
        (window as any).zoomyModel=this;
    }
    public static getInstance():ZoomyModel
    {
        if(ZoomyModel._instance==null)
        {
            ZoomyModel._instance=new ZoomyModel();
        }
        return ZoomyModel._instance;
    }
}