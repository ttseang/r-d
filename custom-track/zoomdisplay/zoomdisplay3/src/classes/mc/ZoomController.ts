export class ZoomController
{
    private static _instance:ZoomController;

    constructor()
    {
        (window as any).zoomController=this;
    }
    public static getInstance():ZoomController
    {
        if(ZoomController._instance==null)
        {
            ZoomController._instance=new ZoomController();
        }
        return ZoomController._instance;
    }
    public nextPage:Function=()=>{};
    public prevPage:Function=()=>{};
    public firstPage:Function=()=>{};
    public lastPage:Function=()=>{};
    public goToPage:Function=(pageNum:number)=>{};
    
}