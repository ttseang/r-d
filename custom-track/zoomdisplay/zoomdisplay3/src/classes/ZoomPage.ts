
import { DisplayElementVo } from "./dataObjs/DisplayElementVo";
import { PageElementTypes, PageElementVo } from "./dataObjs/PageElementVo";
import { StepVo } from "./dataObjs/StepVo";
import { ZoomPresentationVo } from "./dataObjs/ZoomPresentationVo";
import { ElementFactory } from "./util/ElementFactory";
import { CSSManager } from "./managers/CssManager";
import { CodeVo } from "./dataObjs/CodeVo";
import { ZoomVo } from "./dataObjs/ZoomVo";
import { ZoomController } from "./mc/ZoomController";
import { LineElement } from "./elements/LineElement";
import { TextElement } from "./elements/TextElement";

export class ZoomPage {

    //the page container is an svg element and is the parent of the element container
    //and the zoom animation
    private pageContainer: SVGAElement;

    //the element container is a foreignObject element and is the parent of all the elements on the page
    public elementContainer: SVGSVGElement | null | undefined;

    //the line container is for svg line elements that must be on top of all other elements
    //we need this because svg line elements do not work in foreignObject elements
    //use the top svg element for lines
    public lineContainer: SVGSVGElement | null | undefined;

    //this controls the zoom animations
    private zoomElement: SVGAnimateElement | null = null;

    //the current page number
    public currentPageNumber: number = 0;

    //this is all the data for all the pages
    public zoomData: ZoomPresentationVo | null = null;

    //this is the data for the current page that contains the page elements
    public currentStep: StepVo | null = null;

    //this is the data for the current page elements
    public currentElements: PageElementVo[] = [];

    //used for text elements and images
    private origins: string[] = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];

    //this map is used to keep track of the elements that are currently on the page
    private elementMap: Map<string, DisplayElementVo> = new Map<string, DisplayElementVo>();

    //css manager keeps track of the css for each element and cleans up the css when the element is removed
    //or updates it when the element is updated
    private cssManager: CSSManager;

    //the previous zoom level
    private oldZoom: string = "";

    private pageW: number = 0;
    private pageH: number = 0;
    private useAnimation: boolean = true;

    private zoomController: ZoomController = ZoomController.getInstance();

    constructor(pageContainer: SVGAElement, styleContainer: HTMLStyleElement) {
        this.pageContainer = pageContainer;

        //for debugging - make the zoomPage object available in the console
        //remove before production
        (window as any).zoomPage = this;

        //find the element container
        if (this.pageContainer) {
            this.elementContainer = this.pageContainer.querySelector(".elementContainer") as SVGSVGElement;
            this.zoomElement = this.pageContainer.querySelector(".zoom1") as SVGAnimateElement;
        }

        this.lineContainer = document.querySelector(".lineContainer") as SVGSVGElement;

        this.cssManager = new CSSManager(styleContainer);

        //add events to the zoomController
        this.zoomController.nextPage = this.nextPage.bind(this);
        this.zoomController.prevPage = this.prevPage.bind(this);
        this.zoomController.firstPage = this.firstPage.bind(this);
        this.zoomController.lastPage = this.lastPage.bind(this);
        this.zoomController.goToPage = this.goToPage.bind(this);
    }
    nextPage() {
        if (this.zoomData) {
            if (this.currentPageNumber < this.zoomData.steps.length - 1) {
                this.useAnimation = true;
                this.setPage(this.currentPageNumber + 1);
            }
        }
    }
    prevPage() {
        if (this.currentPageNumber > 0) {
            this.useAnimation = true;
            this.setPage(this.currentPageNumber - 1);
        }
    }
    firstPage() {
        this.useAnimation = false;
        this.setPage(0);
    }
    lastPage() {
        if (this.zoomData) {
            this.useAnimation = false;
            this.setPage(this.zoomData.steps.length - 1);
        }
    }
    goToPage(pageNum: number) {
        if (this.zoomData) {
            this.useAnimation = false;
            if (pageNum >= 0 && pageNum < this.zoomData.steps.length) {
                this.setPage(pageNum);
            }
        }
    }

    setData(zoomData: ZoomPresentationVo) {

        this.zoomData = zoomData;

        //get the size data from the zoomData object

        let w: number = zoomData.pageW;
        let h: number = zoomData.pageH;

        //get page width to height ratio
        let ratio: number = w / h;

        //set the page width to the window width and the height to the window width divided by the ratio
        w = window.innerWidth;
        h = w / ratio;

        //set the page container size
        if (this.elementContainer) {
            this.elementContainer.setAttribute("width", w.toString());
            this.elementContainer.setAttribute("height", h.toString());
        }
        /*    this.pageContainer.setAttribute("width", w.toString());
           this.pageContainer.setAttribute("height", h.toString()); */

        this.pageW = w;
        this.pageH = h;

    }
    public setPage(currentPageNumber: number) {
        //update the current page number
        this.currentPageNumber = currentPageNumber;
       

        if (this.zoomData) {
            //update the current step and current elements
            this.currentStep = this.zoomData.steps[currentPageNumber];
            this.currentElements = this.currentStep.elements;
        }

        //add and remove elements from the page
        this.updatePage();
    }
    private updatePage() {
        console.log("updatePage");
        this.elementMap.forEach((value: DisplayElementVo, key: string) => { value.delFlag = true; });

        //loop through the current elements and add them to the page
        for (let i: number = 0; i < this.currentElements.length; i++) {
            let element: PageElementVo = this.currentElements[i];
            /**
                 * does this element already exsist?
                 * if not add it
                 * if so update position and content
                 */
            if (!this.elementMap.has(element.eid)) {
                /**
                 *create the HTMLElement from the data obect
                 */

                let codeVo: CodeVo | null = ElementFactory.getElement(element);

                if (codeVo === null) {
                    console.log("codeVo is null");
                    continue;
                    // return;
                }

                let displayElement: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement | null = codeVo.element;
                /**
                *add the data object to the current element map
                */
                if (displayElement === null) {
                    console.log("displayElement is null");
                    return;
                }

                this.elementMap.set(element.eid, new DisplayElementVo(element.eid, displayElement, element.type));
                //add the element to the page
                //if a line element add it to the page container
                //all other elements are added to the element container
                console.log("element.type", element.type);
                if (element.type === PageElementTypes.LINE) {
                    if (this.lineContainer) {
                        this.lineContainer.prepend(displayElement);
                    }
                }
                else {
                    if (this.elementContainer) {
                        this.elementContainer.appendChild(displayElement);
                    }
                }
                //set the element position by setting the style
                let css: CSSStyleDeclaration = codeVo.css;
                if (css) {
                    this.cssManager.addStyle(element.eid, css);
                }

            }
            else {
                //already exsists so update the position and content
                let displayElementVo: DisplayElementVo | undefined = this.elementMap.get(element.eid);
                if (displayElementVo) {
                    let displayElement: HTMLElement | SVGAElement | SVGLineElement | SVGSVGElement | null = displayElementVo.displayElement;
                    displayElementVo.delFlag = false;
                    if (displayElement) {
                        //let css: CSSStyleDeclaration = displayElement.style;

                        let codeVo: CodeVo | null = ElementFactory.getElement(element);

                        //update content if needed
                        
                        ElementFactory.update(displayElement, element, this.useAnimation);

                        let css: CSSStyleDeclaration = displayElement.style;
                        if (codeVo) {
                            css = codeVo.css;
                        }
                        //update the element position by updating the style
                        css.left = element.x + "%";
                        css.top = element.y + "%";
                        if (element.w > 0) {
                            //calculate width with calc(var(--vw) * 20))
                           // css.width = "calc(var(--vw) * " + element.w + ")";
                            css.width = element.w + "%";
                        }
                        this.cssManager.addStyle(element.eid, css);
                    }
                }
            }
        }
        this.removeDeletedElements();
        this.setZoom();
    }
    removeDeletedElements() {
        //loop through the element map and remove any elements that have been flagged for deletion
        this.elementMap.forEach((value: DisplayElementVo, key: string) => {
            if (value.delFlag) {
                //remove the element from the page
                //if a line element remove it from the line container
                //all other elements are removed from the element container
                if (value.type === PageElementTypes.LINE) {
                    if (this.lineContainer) {
                        this.lineContainer.removeChild(value.displayElement);
                    }
                }
                else {

                    if (this.elementContainer) {
                        this.elementContainer.removeChild(value.displayElement);
                    }
                }
                //remove the element from the element map
                this.elementMap.delete(key);
                //remove the element css
                this.cssManager.removeStyle(key);
            }
        });
    }
    private setZoom() {
        console.log("setZoom");
        if (!this.currentStep) {
            console.log("no current step");
            return;
        }
        let zoomVo: ZoomVo = this.currentStep.zoom;

        //turn percentages into pixels
        let xx: number = (zoomVo.x1 / 100) * this.pageW;
        let yy: number = (zoomVo.y1 / 100) * this.pageH;

        //round to 2 decimal places
        xx = Math.round(xx * 100) / 100;
        yy = Math.round(yy * 100) / 100;

        //measure the distance between the points
        let distX: number = (zoomVo.x2 - zoomVo.x1) / 100;
        let distY: number = (zoomVo.y2 - zoomVo.y1) / 100;

        //cut off the extra decimal places past 2 points
        distX = Math.round(distX * 100) / 100;
        distY = Math.round(distY * 100) / 100;

        //multiply the distance by the page width and height to get the zoom width and height
        let zoomX: number = (this.pageW * distX);
        let zoomY: number = (this.pageH * distY);

        //round to 2 decimal places
        zoomX = Math.round(zoomX * 100) / 100;
        zoomY = Math.round(zoomY * 100) / 100;

        //set the zoom animation
        let posString = xx.toString() + " " + yy.toString();
        posString += " " + zoomX + " " + zoomY;

        console.log(posString);

        if (this.zoomElement)
            if (this.oldZoom === "") {
                this.pageContainer.setAttribute("viewBox", posString);
            }
            else {

                if (this.oldZoom !== posString) {
                    this.zoomElement.setAttribute("values", this.oldZoom + ";" + posString);
                    this.zoomElement.beginElement();
                }

            }


        this.oldZoom = posString;
    }

}
