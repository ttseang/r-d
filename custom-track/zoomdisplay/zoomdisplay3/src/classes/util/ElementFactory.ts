import { CodeVo } from "../dataObjs/CodeVo";
import { CardTypes, PageElementTypes, PageElementVo } from "../dataObjs/PageElementVo";
import { GreenCardElement } from "../elements/cards/GreenCardElement";
import { WhiteCardElement } from "../elements/cards/WhiteCardElement";
import { ImageElement } from "../elements/ImageElement";
import { LineElement } from "../elements/LineElement";
import { ShapeElement } from "../elements/ShapeElement";
import { TextElement } from "../elements/TextElement";
import { iElement } from "../interfaces/iElement";

export class ElementFactory {
    public static getElement(pageElement: PageElementVo): CodeVo | null {

        let element: iElement | null = null;

        switch (pageElement.type) {
            case PageElementTypes.IMAGE:
                element = new ImageElement(pageElement);

                return new CodeVo(element.getHTML(), element.getCss());

            case PageElementTypes.LINE:

                element = new LineElement(pageElement);

                return new CodeVo(element.getHTML(), element.getCss());

            case PageElementTypes.TEXT:

                element = new TextElement(pageElement);
                return new CodeVo(element.getHTML(), element.getCss());

            case PageElementTypes.SHAPE:
                element=new ShapeElement(pageElement);
                return new CodeVo(element.getHTML(), element.getCss());

            case PageElementTypes.CARD:

                switch (pageElement.subType) {
                    case CardTypes.GREEN:
                        element = new GreenCardElement(pageElement);
                        return new CodeVo(element.getHTML(), element.getCss());

                    default:
                        element = new WhiteCardElement(pageElement);
                        return new CodeVo(element.getHTML(), element.getCss());
                }

        }

        return null;
    }
    public static update(displayElement: HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement, pageElement: PageElementVo, useAnimation: boolean): void {

        switch (pageElement.type) {


            case PageElementTypes.LINE:
                //update line
                if (displayElement instanceof SVGLineElement) {
                    LineElement.updateLine(displayElement, pageElement.x, pageElement.y, pageElement.w, pageElement.h, useAnimation);
                }
                break;

            case PageElementTypes.TEXT:
                //update text
                if (displayElement instanceof HTMLElement) {
                    TextElement.update(displayElement, pageElement);
                }

                break;
            case PageElementTypes.IMAGE:
                //update image
                if (displayElement instanceof HTMLImageElement) {
                    ImageElement.update(displayElement, pageElement);
                }

                break;
            
            case PageElementTypes.SHAPE:
                //update shape
                if (displayElement instanceof SVGSVGElement) {
                    ShapeElement.update(displayElement, pageElement);
                }
            case PageElementTypes.CARD:

                switch (pageElement.subType) {
                    case CardTypes.GREEN:
                        //update green card
                        if (displayElement instanceof HTMLElement) {
                            GreenCardElement.update(displayElement, pageElement);
                        }
                        break;

                    case CardTypes.WHITE:
                        //update white card
                        if (displayElement instanceof HTMLElement) {
                            WhiteCardElement.update(displayElement, pageElement);
                        }
                        break;
                    }
                break;
                
        }



    }
}