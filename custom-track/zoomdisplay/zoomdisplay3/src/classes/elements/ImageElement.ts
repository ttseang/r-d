import { ElementExtrasVo } from "../dataObjs/ElementExtrasVo";
import { PageElementVo } from "../dataObjs/PageElementVo";
import { iElement } from "../interfaces/iElement";
import { CSSManager } from "../managers/CssManager";
import { Paths } from "../util/Paths";

export class ImageElement implements iElement {

    private pageElement: PageElementVo;
    constructor(pageElement: PageElementVo) {
        this.pageElement = pageElement;
    }

    getHTML(): HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement {
        const img: HTMLImageElement = document.createElement("img");
        img.id = this.pageElement.eid;
        let content: string = this.pageElement.content[0];
        img.src = Paths.FULL_PATH + content;
        return img;
    }
    getCss(): CSSStyleDeclaration {
        let css: CSSStyleDeclaration = document.createElement("div").style;
        css.position = "absolute";
        css.left = this.pageElement.x + "%";
        css.top = this.pageElement.y + "%";

         //add transition to left and top
         css.transition = "left 0.5s, top 0.5s";

        if (this.pageElement.w > 0) {
            css.width = this.pageElement.w + "%";
        }
        if (this.pageElement.extras)
        {
            let extras:ElementExtrasVo=this.pageElement.extras;
            let transformString: string = "translate(" + CSSManager.getOrientation(extras.orientation) + ")";
            css.transform = transformString;
        }
        return css;
    }
    public static update(element:HTMLElement,pageElement:PageElementVo): void {
        let content: string = pageElement.content[0];
        (element as HTMLImageElement).src = Paths.FULL_PATH + content;
    }
}