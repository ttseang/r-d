import { PageElementVo } from "../dataObjs/PageElementVo";
import { iElement } from "../interfaces/iElement";

export class ShapeElement implements iElement {
    private pageElement: PageElementVo;
    constructor(pageElement: PageElementVo) {
        this.pageElement = pageElement;
    }

    getHTML(): HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement {
        //make an svg element
        let svg: SVGSVGElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");

        //set the svg's id attribute to the eid of the pageElementVo
        svg.id = "svg" + this.pageElement.eid.toString();

        //place in absolute position
        svg.classList.add("fullLine");

        //make a path element
        let path: SVGPathElement = document.createElementNS("http://www.w3.org/2000/svg", "path");

        //set the path's d attribute to the 1st element of the content array of the pageElementVo
        path.setAttribute("d", this.pageElement.content[0]);

        //set the path's fill attribute to the fill color of the pageElementVo
        path.setAttribute("fill", this.pageElement.extras.backgroundColor);

        //set the path's stroke attribute to the border color of the pageElementVo
        path.setAttribute("stroke", this.pageElement.extras.borderColor);

        //set the path's stroke-width attribute to the border thickness of the pageElementVo
        path.setAttribute("stroke-width", this.pageElement.extras.borderThick.toString());

        path.setAttribute("style",this.getCss().cssText);

        svg.appendChild(path);

        return svg;
    }
    getCss(): CSSStyleDeclaration {
        return ShapeElement.getStyle(this.pageElement);
    }
    private static getStyle(pageElement:PageElementVo):CSSStyleDeclaration
    {
        let cssStyle: CSSStyleDeclaration = document.createElement("div").style;
        cssStyle.transform = ShapeElement.getTransform(pageElement);
        cssStyle.transitionProperty = "all";
        cssStyle.transitionDuration = "2s";

        //opacity
        let opacity: number = pageElement.extras.alpha / 100;
        cssStyle.opacity = opacity.toString();

        return cssStyle;
    }
    private static getTransform(pageElementVo: PageElementVo) {

        let transformString: string = "";
        
        //add position
        let x: number = pageElementVo.x;
        let y: number = pageElementVo.y;
        transformString += " translate(" + x.toString() + "%," + y.toString() + "%)";

        //add rotation
        let angle: number = ((pageElementVo.extras.rotation) / 100) * 360;
        angle = Math.floor(angle);
        transformString += " rotate(" + angle.toString() + "deg)";

        //set the shape to the width of the screen
        //get the width of the screen
        let screenWidth: number = window.innerWidth;

        //the width of the shape is 100px
        let shapeWidth: number = 100;

        //the scale is the width of the screen divided by the width of the shape
        let scale: number = screenWidth / shapeWidth;

        //and then adjust that scale by the width of the pageElementVo
        scale = scale * (pageElementVo.w / 100);

        //set scale
        //let scale: number = ((pageElementVo.w) / 100);
        if (isNaN(scale) === true) {
            scale = 1;
        }
        transformString += " scale(" + scale.toString() + ")";

        if (pageElementVo.extras.skewY !== 0) {
            let skewAngleY: number = ((pageElementVo.extras.skewY) / 100) * 360;
            skewAngleY = Math.floor(skewAngleY);
            transformString += " skewY(" + skewAngleY.toString() + "deg)";
        }
        if (pageElementVo.extras.skewX !== 0) {
            let skewAngleX: number = ((pageElementVo.extras.skewX) / 100) * 360;
            skewAngleX = Math.floor(skewAngleX);
            transformString += " skewX(" + skewAngleX.toString() + "deg)";
        }
        if (pageElementVo.extras.flipV === true) {
            transformString += " scaleY(-1)";
        }
        if (pageElementVo.extras.flipH === true) {
            transformString += " scaleX(-1)";
        }

        return transformString;
    }
  
    public static update(element:SVGAElement | SVGSVGElement,pageElement:PageElementVo): void {     
        console.log("update shape"); 
        let cssText:string=ShapeElement.getStyle(pageElement).cssText; 
        const path:SVGPathElement=element.getElementsByTagName("path")[0];
        path.setAttribute("style",cssText);
    //    console.log("cssText: "+cssText);
       // element.setAttribute("style",cssText);
    }
}