import { PageElementVo } from "../../dataObjs/PageElementVo";
import { iElement } from "../../interfaces/iElement";
import { Paths } from "../../util/Paths";

export class GreenCardElement implements iElement {

    private pageElement: PageElementVo;
    constructor(pageElement: PageElementVo) {
        this.pageElement = pageElement;
    }

    getHTML(): HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement {
        const div: HTMLDivElement = document.createElement("div");
        div.id = this.pageElement.eid;
        div.className = "greenCard";

        let title: string = this.pageElement.content[0];
        let text: string = this.pageElement.content[1];

        let h4: HTMLHeadingElement = document.createElement("h4");
        let p: HTMLParagraphElement = document.createElement("p");
        h4.innerText = title;
        p.innerText = text;

        div.appendChild(h4);
        div.appendChild(p);

        return div;
    }
    getCss(): CSSStyleDeclaration {
        let css: CSSStyleDeclaration = document.createElement("div").style;
        css.position = "absolute";
        css.left = this.pageElement.x + "%";
        css.top = this.pageElement.y + "%";

        //add transition to left and top
        css.transition = "left 0.5s, top 0.5s";

        if (this.pageElement.w > 0) {
            //calculate width with calc(var(--vw) * 20))
            css.width = "calc(var(--vw) * " + this.pageElement.w + ")";
           // css.width = this.pageElement.w + "%";
        }
        return css;
    }
    public static update(element: HTMLElement, pageElement: PageElementVo): void {
        let title: string = pageElement.content[0];
        let text: string = pageElement.content[1];

        let h4: HTMLHeadingElement | null = element.querySelector("h4");
        let p: HTMLParagraphElement | null = element.querySelector("p");
        if (h4 && p) {
            h4.innerText = title;
            p.innerText = text;
        }
    }
}