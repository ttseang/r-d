import { PageElementVo } from "../dataObjs/PageElementVo";
import { iElement } from "../interfaces/iElement";
import { NumUtil } from "../util/NumUtil";

export class LineElement implements iElement {

    private pageElement: PageElementVo;
    constructor(pageElement: PageElementVo) {
        this.pageElement = pageElement;
    }
    getHTML(): HTMLElement | SVGAElement | SVGSVGElement | SVGLineElement {


        let x1: number = NumUtil.roundDec(this.pageElement.x);
        let y1: number = NumUtil.roundDec(this.pageElement.y);
        let x2: number = NumUtil.roundDec(this.pageElement.w);
        let y2: number = NumUtil.roundDec(this.pageElement.h);


        let line: SVGLineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");


        line.setAttribute("id", this.pageElement.eid);
        line.setAttribute("x1", x1.toString() + "%");
        line.setAttribute("y1", y1.toString() + "%");
        line.setAttribute("x2", x2.toString() + "%");
        line.setAttribute("y2", y2.toString() + "%");


        //set up the animations

        //animate the x1
        let anim1: SVGAnimateElement = document.createElementNS("http://www.w3.org/2000/svg", "animate");
        anim1.setAttribute("id", "movelinex1");
        anim1.setAttribute("attributeName", "x1");
        anim1.setAttribute("begin", "indefinite");
        anim1.setAttribute("dur", "0.5s");
        anim1.setAttribute("fill", "freeze");

        //animate the x2
        let anim2: SVGAnimateElement = document.createElementNS("http://www.w3.org/2000/svg", "animate");
        anim2.setAttribute("id", "movelinex2");
        anim2.setAttribute("attributeName", "x2");
        anim2.setAttribute("begin", "indefinite");
        anim2.setAttribute("dur", "0.5s");
        anim2.setAttribute("fill", "freeze");

        //animate the y1
        let anim3: SVGAnimateElement = document.createElementNS("http://www.w3.org/2000/svg", "animate");
        anim3.setAttribute("id", "moveliney1");
        anim3.setAttribute("attributeName", "y1");
        anim3.setAttribute("begin", "indefinite");
        anim3.setAttribute("dur", "0.5s");
        anim3.setAttribute("fill", "freeze");

        //animate the y2
        let anim4: SVGAnimateElement = document.createElementNS("http://www.w3.org/2000/svg", "animate");
        anim4.setAttribute("id", "moveliney2");
        anim4.setAttribute("attributeName", "y2");
        anim4.setAttribute("begin", "indefinite");
        anim4.setAttribute("dur", "0.5s");
        anim4.setAttribute("fill", "freeze");

        //add the animations to the line
        line.appendChild(anim1);
        line.appendChild(anim2);
        line.appendChild(anim3);
        line.appendChild(anim4);

        return line;
    }
    getCss(): CSSStyleDeclaration {
        let css: CSSStyleDeclaration = document.createElement("div").style;
        let extras = this.pageElement.extras;

        css.stroke = extras.borderColor;
        css.strokeWidth = extras.borderThick.toString() + "px";
        return css;
    }
    public update(): void {
        //do nothing because the update is handled by the updateLine method
    }
    public static updateLine(line: SVGLineElement, x1: number, y1: number, x2: number, y2: number, useAnimation: boolean) {


        let oldX1: string = line.getAttribute("x1") || "0";
        let oldX2: string = line.getAttribute("x2") || "0";
        let oldY1: string = line.getAttribute("y1") || "0";
        let oldY2: string = line.getAttribute("y2") || "0";

        // console.log(oldX1+" "+x1);
        if (useAnimation) {
            let lineAnimation: SVGAnimateElement = (line.querySelector("#movelinex1") as unknown) as SVGAnimateElement;
            lineAnimation.setAttribute("values", oldX1 + ";" + x1.toString() + "%");
            lineAnimation.beginElement();

            let lineAnimation2: SVGAnimateElement = (line.querySelector("#movelinex2") as unknown) as SVGAnimateElement;
            lineAnimation2.setAttribute("values", oldX2 + ";" + x2.toString() + "%");
            lineAnimation2.beginElement();

            let lineAnimation3: SVGAnimateElement = (line.querySelector("#moveliney1") as unknown) as SVGAnimateElement;
            lineAnimation3.setAttribute("values", oldY1 + ";" + y1.toString() + "%");
            lineAnimation3.beginElement();

            let lineAnimation4: SVGAnimateElement = (line.querySelector("#moveliney2") as unknown) as SVGAnimateElement;
            lineAnimation4.setAttribute("values", oldY2 + ";" + y2.toString() + "%");
            lineAnimation4.beginElement();

            //half second delay for the animation
            setTimeout(() => {
                line.setAttribute("x1", x1.toString() + "%");
                line.setAttribute("y1", y1.toString() + "%");
                line.setAttribute("x2", x2.toString() + "%");
                line.setAttribute("y2", y2.toString() + "%");
            }, 500);
        }
        else
        {
            line.setAttribute("x1", x1.toString() + "%");
            line.setAttribute("y1", y1.toString() + "%");
            line.setAttribute("x2", x2.toString() + "%");
            line.setAttribute("y2", y2.toString() + "%");
        }
    }

}