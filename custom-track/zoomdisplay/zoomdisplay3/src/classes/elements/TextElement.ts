import { ElementExtrasVo } from "../dataObjs/ElementExtrasVo";
import { PageElementVo } from "../dataObjs/PageElementVo";
import { iElement } from "../interfaces/iElement";
import { CSSManager } from "../managers/CssManager";

export class TextElement implements iElement
{
    private pageElement: PageElementVo;
    constructor(pageElement: PageElementVo) {
        this.pageElement = pageElement;
    }
    getHTML(): HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement {
        let element:HTMLElement=document.createElement("p");
        element.id=this.pageElement.eid;
        element.innerText=this.pageElement.content[0];
        element.classList.add(this.getSizeClass());
        return element;
    }
    getCss(): CSSStyleDeclaration {
        let css: CSSStyleDeclaration = document.createElement("div").style;
        css.position = "absolute";
        css.left = this.pageElement.x + "%";
        css.top = this.pageElement.y + "%";

        //add transition to left and top
        css.transition = "left 0.5s, top 0.5s";

        if (this.pageElement.w > 0) {
            //calculate width with calc(var(--vw) * 20))
            css.width = "calc(var(--vw) * "+this.pageElement.w+")";
           
        }
        if (this.pageElement.extras)
        {
            let extras:ElementExtrasVo=this.pageElement.extras;
            let transformString: string = "translate(" + CSSManager.getOrientation(extras.orientation) + ")";
            css.transform = transformString;
            css.color=extras.fontColor;
            css.fontSize=extras.fontSize+"px";
        }
        return css;
    }
    private getSizeClass():string
    {
        let sizes="cornerText,fullHalf,towerText,centerText";
        let sizeArray:string[]=sizes.split(",");
        return sizeArray[this.pageElement.extras.textSizeID];
    }
    public static update(element:HTMLElement,pageElement:PageElementVo): void {
        element.innerText=pageElement.content[0];
    }
}
/* .cornerText
{
    width: 25%!important;
    height: 25%;
    overflow: hidden;
}
.centerText
{
    margin: auto;
    width: 50%!important;
    height: 50%;
    overflow: hidden;
}
.towerText
{
    width: 25%!important;
    height: 100%;
    overflow: hidden;
}
.fullHalf
{
    width: 50%!important;
    height: 100%;
    overflow: hidden;
} */