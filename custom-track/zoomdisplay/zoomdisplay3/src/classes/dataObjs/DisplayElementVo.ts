export class DisplayElementVo
{
    public eid:string;
    public type:string;
    public displayElement:HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement;
    public delFlag:boolean=false;

    constructor(eid:string,displayElement:HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement, type:string)
    {
        this.eid=eid;
        this.displayElement=displayElement;
        this.type=type;
    }
  
}