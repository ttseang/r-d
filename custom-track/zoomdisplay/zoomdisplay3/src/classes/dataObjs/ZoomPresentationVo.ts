import { parse } from "path";
import { StepVo } from "./StepVo";


export class ZoomPresentationVo {
    public steps: StepVo[];
    public pageW: number;
    public pageH: number;
    public attribution: string;
    public bgColor: string;
    public scaleW: number = 1;
    public scaleH: number = 1;

    constructor(steps: StepVo[] = [], pageW: number = 0, pageH: number = 0, backgroundColor: string = "#eeeeee") {
        this.steps = steps;
        this.pageW = pageW;
        this.pageH = pageH;
        this.attribution = "not set";
        this.bgColor = backgroundColor;
    }
    fromObj(obj: any) {
        console.log("fromObj", obj);

        const orignalW: number = parseInt(obj.pageW);
        const orignalH: number = parseInt(obj.pageH);


        const ratio: number = 96 / 55;
        let fullW: number = window.innerWidth;
        let fullH: number = fullW / ratio;

        this.scaleW = fullW / orignalW;
        this.scaleH = fullH / orignalH;

        console.log("scaleW", this.scaleW);
        console.log("orignalW", orignalW);
        console.log("fullW", fullW);

        this.pageW = fullW;
        this.pageH = fullH;
        this.attribution = obj.attribution;
        this.bgColor = obj.bgColor;

        for (let i: number = 0; i < obj.steps.length; i++) {
            let stepData: any = obj.steps[i];
            let stepVo: StepVo = new StepVo();
            stepVo.fromObj(stepData);
            this.steps.push(stepVo);
        }
    }
}