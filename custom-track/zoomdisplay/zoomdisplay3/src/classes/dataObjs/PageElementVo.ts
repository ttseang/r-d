import { ElementExtrasVo } from "./ElementExtrasVo";

export class PageElementVo {


    /* public static TYPE_IMAGE: string = "IMAGE";
    public static TYPE_TEXT: string = "TEXT";
    public static TYPE_BACK_IMAGE = "BACK_IMAGE";
    public static TYPE_CARD: string = "CARD";
    public static TYPE_LINE: string = "LINE";
    public static TYPE_SHAPE: string = "SHAPE";


    public static SUB_TYPE_GREEN_CARD: string = "greenCard";
    public static SUB_TYPE_WHITE_CARD: string = "whiteCard"; */


    public static elementID: number = 0;

    public id: string = "";
    public eid: string;
    public type: string;
    public subType: string;
    public classes: string[];
    public content: string[];
    public x: number;
    public y: number;
    public w: number;
    public h: number;
    public textStyle: number = -1;
    
    public extras: ElementExtrasVo;
   

    //set default values for each property
    constructor(type: string = "", subType: string = "", classes: string[] = [], content: string[] = [], x: number = 0, y: number = 0, w: number = 0, h: number = 0) {

        this.type = type;
        this.subType = subType;
        this.classes = classes;
        this.content = content;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        //temporary eid. It will be set later
        this.eid = "instance0";
        this.extras = new ElementExtrasVo();
    }

    fromObj(obj: any) {
        PageElementVo.elementID++;
        this.id = "bookElement" + PageElementVo.elementID.toString();

        this.eid = obj.eid;
        this.type = obj.type;
        this.subType = obj.subType;
        this.classes = obj.classes;
        this.content = obj.content;
        this.x = obj.x;
        this.y = obj.y;
        this.w = obj.w;
        this.h = obj.h;

        this.extras.type = this.type;
        this.extras.fromObj(obj.extras);
    }
}

export enum PageElementTypes {
    IMAGE = "IMAGE",
    TEXT = "TEXT",
    BACK_IMAGE = "BACK_IMAGE",
    CARD = "CARD",
    LINE = "LINE",
    SHAPE = "SHAPE"
}

export enum CardTypes {
    GREEN = "greenCard",
    WHITE = "whiteCard"
}