import { UIVo } from "../dataObjs/UIVo";



export class UIManager {
    private static _instance: UIManager;
    public uiList: UIVo[] = [];
    public barsOpen: boolean = true;

    constructor() {

        this.setUp();
        (window as any).uiManager = this;
    }
    public static getInstance(): UIManager {
        if (UIManager._instance == null) {
            UIManager._instance = new UIManager();
        }
        return UIManager._instance;
    }
    private setUp() {

        this.addUi("darkScreen", "darkScreen", null);
        //topBar bottomBar
        this.addUi("topBar", "bars", null);
        this.addUi("bottomBar", "bars", null);

        //tbButtons btnAuto,btnListen,btnRead topbar buttons
        this.addUi("toggleButton autoPlay", "tbButtons", null);
        this.addUi("toggleButton listen", "tbButtons", null);
        this.addUi("toggleButton read", "tbButtons", null);

        //bubbleGrid btnAutoStart,btnListenStart,btnReadStart
        this.addUi("bubbleGrid", "bubbleGrid", null);
        this.addUi("btnAutoStart", "bubbleGrid", null);
        this.addUi("btnListenStart", "bubbleGrid", null);
        this.addUi("btnReadStart", "bubbleGrid", null);

        this.addUi("gadgets", "gadgetParent", null);
        this.addUi('volumeSlider', 'gadgets', null);
        this.addUi('speedBalloon', 'gadgets', null);
        this.addUi('speechSlider', 'gadgets', null);

        this.addUi("mobileVolume", "gadgets", null);

        //nav buttons
        //btnNavDown
        this.addUi("btnNavDown", "navButtons", null);



        this.addUi("speedSlow", "playspeed", null);
        this.addUi("speedNormal", "playspeed", null);
        this.addUi("speedFast", "playspeed", null);

        //highlight switch
        this.addUi("hknob", "highlight", null);
        this.addUi("highlightSwitch", "highlight", null);

        //pageSliderNav
        this.addUi("pageSliderNav", "pageSliderParent", null);
        this.addUi("zoomySlider", "pageSlider", null);
        this.addUi("tickmarks", "pageSlider", null);
        this.addUi("previewHolder", "pageSlider", null);

        //btnNext and btnPrev
        this.addUi("btnNext", "navButtons2", null);
        this.addUi("btnPrev", "navButtons2", null);
    }
    public addUi(id: string, group: string, element: HTMLElement | null): void {

        if (element == null) {
            element = document.getElementsByClassName(id)[0] as HTMLElement;
            if (element == null) {
                element = document.getElementById(id) as HTMLElement;
            }
        }
        if (element == null) {
            console.log(id + " not found");
            return;
        }

        this.uiList.push(new UIVo(id, group, element));

    }
    public getElements(group: string): HTMLElement[] {
        let elements: HTMLElement[] = [];
        for (let i = 0; i < this.uiList.length; i++) {
            if (this.uiList[i].group == group) {
                elements.push(this.uiList[i].element);
            }
        }
        return elements;
    }
    public getSingleElement(elementName: string) {
        for (let i = 0; i < this.uiList.length; i++) {
            if (this.uiList[i].id == elementName) {
                return this.uiList[i].element;
            }
        }
        return null;
    }
    private getUIVo(id: string): UIVo | null {
        for (let i = 0; i < this.uiList.length; i++) {
            if (this.uiList[i].id == id) {
                return this.uiList[i];
            }
        }
        return null;
    }
    public changeElementClass(id: string, className: string = "", removeClass: string = ""): void {
        let uiVo: UIVo | null = this.getUIVo(id);
        if (uiVo == null) throw new Error("UIManager: changeElementClass: uiVo is null");
        if (removeClass != "") {
            uiVo.element.classList.remove(removeClass);
        }
        if (className != "") {
            uiVo.element.classList.add(className);
        }
    }
    public changeGroupClass(group: string, className: string = "", removeClass: string = "", exception: string = ""): void {
        let elements: HTMLElement[] = this.getElements(group);

        for (let i = 0; i < elements.length; i++) {
            if (removeClass != "") {
                elements[i].classList.remove(removeClass);
            }
            if (className != "") {
                elements[i].classList.add(className);
            }
        }
    }
    public addGroupClass(group: string, className: string): void {
        let elements: HTMLElement[] = this.getElements(group);
        for (let i = 0; i < elements.length; i++) {
            elements[i].classList.add(className);
        }
    }
    public removeGroupClass(group: string, className: string): void {
        let elements: HTMLElement[] = this.getElements(group);
        for (let i = 0; i < elements.length; i++) {
            elements[i].classList.remove(className);
        }
    }
    public groupHasClass(group: string, className: string): boolean {
        let elements: HTMLElement[] = this.getElements(group);
        for (let i = 0; i < elements.length; i++) {
            if (elements[i].classList.contains(className)) {
                return true;
            }
        }
        return false;
    }
    public allGroupHasClass(group: string, className: string): boolean {
        let elements: HTMLElement[] = this.getElements(group);
        for (let i = 0; i < elements.length; i++) {
            if (!elements[i].classList.contains(className)) {
                return false;
            }
        }
        return true;
    }
    public getUIListNames(): string[] {
        let names: string[] = [];
        for (let i = 0; i < this.uiList.length; i++) {
            names.push(this.uiList[i].id);
        }
        return names;
    }
    public toggleElementClass(id: string, className: string): void {
        let uiVo: UIVo | null = this.getUIVo(id);
        if (uiVo == null) throw new Error("UIManager: changeElementClass: uiVo is null");
        uiVo.element.classList.toggle(className);

    }
    public addElementClass(id: string, className: string): void {
        let uiVo: UIVo | null = this.getUIVo(id);
        if (uiVo == null) throw new Error("UIManager: changeElementClass: uiVo is null");
        uiVo.element.classList.add(className);
    }
    public removeElementClass(id: string, className: string): void {
        let uiVo: UIVo | null = this.getUIVo(id);
        if (uiVo == null) throw new Error("UIManager: changeElementClass: uiVo is null");
        uiVo.element.classList.remove(className);
    }
    public toggleGroupClass(group: string, className: string): void {
        let elements: HTMLElement[] = this.getElements(group);

        for (let i = 0; i < elements.length; i++) {
            elements[i].classList.toggle(className);
        }
    }
    public toggleBars() {
        if (this.barsOpen) {
            this.closeBars();
        }
        else {
            this.openBars();
        }
    }
    public openBars() {
        // this.uiManager.changeElementClass("topBar","open","closed");
        this.changeGroupClass("bars", "open", "closed");
        this.barsOpen = true;
    }
    public closeBars() {
        //this.changeElementClass("topBar","closed","open");
        this.changeGroupClass("bars", "closed", "open");
        this.barsOpen = false;
    }
    public hide(elementId: string) {
        this.changeElementClass(elementId, "hid", "");
    }
    public show(elementId: string) {
        this.changeElementClass(elementId, "", "hid");
    }
    public hideGroup(group: string) {
        this.changeGroupClass(group, "hid", "");
    }
    public showGroup(group: string) {
        this.changeGroupClass(group, "", "hid");
    }
    public toggle(elementId: string) {
        this.toggleElementClass(elementId, "hid");
    }
    public toggleGroup(group: string) {
        this.toggleGroupClass(group, "hid");
    }

}