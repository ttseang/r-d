import { PageElementVo } from "../dataObjs/PageElementVo";

export class CSSManager {
    private styleDiv: HTMLStyleElement | null = null;
    constructor(styleDiv: HTMLStyleElement) {
        this.styleDiv = styleDiv;
        (window as any).cssManager = this;
    }

    public addStyle(id: string, style: CSSStyleDeclaration) {
        
        let ruleIndex = -1;
        if (this.styleDiv) {
            //if the style is already there, remove it

            if (this.styleDiv.sheet != null) {
                //if the rule is already there, remove it
                this.removeStyle(id);
                let cRule = "#" + id + "{" + style.cssText + "}";
          
                //create a css rule
                ruleIndex = this.styleDiv.sheet.insertRule(cRule);
              //  const rule = this.styleDiv.sheet.cssRules[ruleIndex] as CSSStyleRule;
            }
            
        }
        return ruleIndex;
    }
    public removeStyle(id: string) {
        if (this.styleDiv) {
            //if the style is there, remove it

            if (this.styleDiv.sheet) {
               //loop through the rules and find the one with the id
                for (let i = 0; i < this.styleDiv.sheet.cssRules.length; i++) {
                    const rule = this.styleDiv.sheet.cssRules[i] as CSSStyleRule;
                  //  console.log(rule.selectorText);
                    if (rule.selectorText == "#" + id) {
                        this.styleDiv.sheet.deleteRule(i);
                        break;
                    }
                }
            }
        }
    }
    public static getOrientation(index:number)
    {
        let origins:string[] = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
        return origins[index];
    }
}