export interface iElement
{
    getHTML():HTMLElement | SVGLineElement | SVGAElement | SVGSVGElement;
    getCss():CSSStyleDeclaration;
}