# README

## What is this repository for?

TT Custom Track is an administrative tool for creating Custom Track presentations. It leverages Create React App as a
primary framework.

## How do I get set up?

### Clone the repository

```
git clone git@bitbucket.org:teachingtextbooks/custom-track-admin.git
```

### Access the repo directory and install dependencies

```
cd custom-track-admin
npm i
```

### Spin up the project

```
npm run start
```

## Who do I talk to?

* William Clarkson
* Randy Kinsley