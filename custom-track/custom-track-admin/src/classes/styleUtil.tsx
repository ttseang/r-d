import { FontVo } from "./dataObjs/FontVo";
import { StyleVo } from "./dataObjs/StyleVo";
import { TextStyleVo } from "./dataObjs/TextStyleVo";
import { GridVo } from "./GridVo";
import { ImgBorderStyleVo } from "./dataObjs/ImgBorderStyleVo";
import { TextPathVo } from "./dataObjs/TextPathVo";
import { TextSizeVo } from "./dataObjs/TextSizeVo";
import { PropActions } from "../screens/PageEditScreen";


export class StyleUtil
{
    public textSizes:TextSizeVo[]=[];
    public styles:StyleVo[]=[];
    public orientations:GridVo[]=[];
    public aligns:GridVo[]=[];
    public textStyles:TextStyleVo[]=[];
    public imgBorders:ImgBorderStyleVo[]=[];

    public fonts:FontVo[]=[];
    public textPath:TextPathVo[]=[];

    constructor()
    {
        //TO DO move to JSON        
        this.styles.push(new StyleVo(1,"top left",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"1.jpg",0,0,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(2,"top center",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"2.jpg",50,0,0,StyleVo.ADJUST_HALF,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(3,"top right",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"3.jpg",100,0,0,StyleVo.ADJUST_FULL,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(4,"center left",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"4.jpg",0,50,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_HALF));
        this.styles.push(new StyleVo(5,"center",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,["transformCenter"],"5.jpg",50,50,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_NONE));
        this.styles.push(new StyleVo(6,"center right",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"6.jpg",100,50,0,StyleVo.ADJUST_FULL,StyleVo.ADJUST_HALF));
        this.styles.push(new StyleVo(7,"bottom left",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"7.jpg",0,100,0,StyleVo.ADJUST_NONE,StyleVo.ADJUST_FULL));
        this.styles.push(new StyleVo(8,"bottom center",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"8.jpg",50,100,0,StyleVo.ADJUST_HALF,StyleVo.ADJUST_FULL));
        this.styles.push(new StyleVo(9,"bottom right",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_POSITION,[],"9.jpg",100,100,0,StyleVo.ADJUST_FULL,StyleVo.ADJUST_FULL));


        this.styles.push(new StyleVo(10,"size 1",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size1.jpg",0,0,10));
        this.styles.push(new StyleVo(11,"size 2",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size2.jpg",0,0,20));
        this.styles.push(new StyleVo(12,"size 3",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size3.jpg",0,0,30));
        this.styles.push(new StyleVo(13,"size 4",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size4.jpg",0,0,40));
        this.styles.push(new StyleVo(14,"size 5",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size5.jpg",0,0,50));
        this.styles.push(new StyleVo(15,"size 6",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size6.jpg",0,0,60));
        this.styles.push(new StyleVo(16,"size 7",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size7.jpg",0,0,70));
        this.styles.push(new StyleVo(17,"size 8",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size8.jpg",0,0,80));
        this.styles.push(new StyleVo(18,"size 9",StyleVo.TYPE_IMAGE,StyleVo.SUBTYPE_SIZE,[],"size9.jpg",0,0,90));


        //
        //
        //
        this.imgBorders.push(new ImgBorderStyleVo('None',['0','0']));
        this.imgBorders.push(new ImgBorderStyleVo('Arrow-Left',['polygon(40% 0%, 40% 20%, 100% 20%, 100% 80%, 40% 80%, 40% 100%, 0% 50%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Arrow-Right',['polygon(0% 20%, 60% 20%, 60% 0%, 100% 50%, 60% 100%, 60% 80%, 0% 80%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Ellipse',['ellipse(40% 45% at 50% 50%)','2']));
        this.imgBorders.push(new ImgBorderStyleVo('Hexagon',['polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Message',['polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 75% 100%, 50% 75%, 0% 75%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Pentagon',['polygon(50% 0%, 100% 38%, 82% 100%, 18% 100%, 0% 38%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Point-Left',['polygon(25% 0%, 100% 1%, 100% 100%, 25% 100%, 0% 50%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Point-Right',['polygon(0% 0%, 75% 0%, 100% 50%, 75% 100%, 0% 100%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Rhombus',['polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Star',['polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Triangle',['polygon(50% 0%, 0% 100%, 100% 100%)']));
        this.imgBorders.push(new ImgBorderStyleVo('Trapezoid',['polygon(20% 0%, 80% 0%, 100% 100%, 0% 100%)']));
        //
        //
        //

        this.orientations.push(new GridVo(0,"top left","orientations/00.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(1,"top center","orientations/05.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(2,"top right","orientations/01.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(3,"center left","orientations/50.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(4,"center","orientations/55.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(5,"center right","orientations/51.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(6,"bottom left","orientations/10.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(7,"bottom center","orientations/15.png",PropActions.ORIENTATION));
        this.orientations.push(new GridVo(8,"bottom right","orientations/11.png",PropActions.ORIENTATION));
        //
        //
        //OLD CONFIGURATIONS
        /*this.aligns.push(new GridVo(0,"top left","aligns/0.jpg",3));
        this.aligns.push(new GridVo(1,"top center","aligns/1.jpg",3));
        this.aligns.push(new GridVo(2,"top right","aligns/2.jpg",3));
        this.aligns.push(new GridVo(3,"center left","aligns/3.jpg",3));
        this.aligns.push(new GridVo(4,"center","aligns/4.jpg",3));
        this.aligns.push(new GridVo(5,"center right","aligns/5.jpg",3));
        this.aligns.push(new GridVo(6,"bottom left","aligns/6.jpg",3));
        this.aligns.push(new GridVo(7,"bottom center","aligns/7.jpg",3));
        this.aligns.push(new GridVo(8,"bottom right","aligns/8.jpg",3));
        */
        //NEW CONFIGURATION
        this.aligns.push(new GridVo(0,"top left","fas fa-level-up",PropActions.ALIGN));
        this.aligns.push(new GridVo(1,"top center","fas fa-arrow-to-top",PropActions.ALIGN));
        this.aligns.push(new GridVo(2,"top right","fas fa-level-up",PropActions.ALIGN));
        this.aligns.push(new GridVo(3,"center left","fas fa-arrow-to-left",PropActions.ALIGN));
        this.aligns.push(new GridVo(4,"center","far fa-compress-arrows-alt",PropActions.ALIGN));
        this.aligns.push(new GridVo(5,"center right","fas fa-arrow-to-right",PropActions.ALIGN));
        this.aligns.push(new GridVo(6,"bottom left","fas fa-level-down",PropActions.ALIGN));
        this.aligns.push(new GridVo(7,"bottom center","fas fa-arrow-to-bottom",PropActions.ALIGN));
        this.aligns.push(new GridVo(8,"bottom right","fas fa-level-down",PropActions.ALIGN));
        //
        //
        //
        this.fonts.push(new FontVo("Arial","Arial"));
        this.fonts.push(new FontVo("Times New Roman","Times New Roman"));
        this.fonts.push(new FontVo("Cursive","Cursive"));
        this.fonts.push(new FontVo("Andika","Andika"));
        //
        //
        //
        //Name, Path, Anchor Point X
       /*  this.textPath.push(new TextPathVo("None","",0));
        this.textPath.push(new TextPathVo("Curve","M73.2,148.6c4-6.1,65.5-96.8,178.6-95.6c111.3,1.2,170.8,90.3,175.1,97",210));
        this.textPath.push(new TextPathVo("Curve-Down","M 0 0 C 37.5 56.25 306.25 156.25 612.5 -37.5",250)); */

        this.textPath.push(new TextPathVo("None",0,0));
        this.textPath.push(new TextPathVo("Curve",1,210));
        this.textPath.push(new TextPathVo("Curve-Down",2,250));

        this.textSizes.push(new TextSizeVo(0,"corner","cornerText"));
        this.textSizes.push(new TextSizeVo(1,"full half","fullHalf"));
        this.textSizes.push(new TextSizeVo(2,"tower","towerText"));
        this.textSizes.push(new TextSizeVo(3,"center","centerText"));
    }

    public filterStyles(type:string,subtype:string)
    {
        let fstyles:StyleVo[]=[];
        for (let i:number=0;i<this.styles.length;i++)
        {
            if (this.styles[i].type===type && this.styles[i].subType===subtype)
            {
                fstyles.push(this.styles[i]);
            }            
        }
        return fstyles;
    }
}