import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { GridVo } from '../GridVo';

import GridItem from './GridItem';
interface MyProps {gridItems:GridVo[],callback:Function}
interface MyState {gridItems:GridVo[]}
class  StyleGrid extends Component <MyProps, MyState>
{
    constructor(props:MyProps){
super(props);
        this.state={gridItems:this.props.gridItems}
}
componentDidUpdate(oldProps:MyProps)
{
    if (oldProps!==this.props)
    {
        this.setState({gridItems:this.props.gridItems});
    }
}
getGrid()
{
    let items:JSX.Element[]=[];
    for (let i:number=0;i<this.state.gridItems.length;i++)
    {
        let key:string="gridItem"+i.toString();

        items.push(<Col sm={4} key={key}><GridItem gridVo={this.state.gridItems[i]} callback={this.props.callback}></GridItem></Col>)
    }
    return (<Row>{items}</Row>);
}
render()
{
    if(this.props.gridItems[0].action === 3){
        return (
            <div className='toolPanel' id={"alignPanel"} style={{display:'none'}}>
                <div  className='padLeft'>
                <label style={{fontSize:'14px'}}><strong>Align to stage:</strong></label>
                {this.getGrid()}
                </div>
            </div>
            )
    } else{
        return(
            <div className='toolPanel' id={"orientationPanel"} style={{display:'none'}}>
            {this.getGrid()}
            </div>
        )
    }
}
}
export default StyleGrid;