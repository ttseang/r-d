import React, { Component } from 'react';
import { GridVo } from '../GridVo';

import MainStorage from '../MainStorage';
interface MyProps { gridVo: GridVo, callback: Function }
interface MyState { gridVo: GridVo }
class GridItem extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { gridVo: this.props.gridVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ gridVo: this.props.gridVo });
        }
    }
    getImage() {
        return this.ms.getImagePath(this.state.gridVo.image, 2);
    }

    getIcons(){
        return <i className= {this.state.gridVo.image} id='toolIcons'></i>;
    }

    render() {
        if(this.props.gridVo.action !== 3){
            return (
            <div>
                <img onClick={()=>{this.props.callback(this.props.gridVo.action,this.props.gridVo.id)}} src={this.getImage()} width="50" alt={this.state.gridVo.text} title={this.state.gridVo.text} />
            </div>)
        } else {
            return(
            <div>
                <button className="toolButton" onClick={event => this.props.callback(this.props.gridVo.action,this.props.gridVo.id)}>{this.getIcons()}</button>
            </div>
            )
        }
    }
}
export default GridItem;