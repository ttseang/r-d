import React, { ChangeEvent, Component } from "react";
import { Row, Col, Button } from "react-bootstrap";

interface MyProps {
  callback: Function;
  buttonText: string;
  text: string;
  clearOnEnter: boolean;
}
interface MyState {
  text: string;
}
class TextInputButton extends Component<MyProps, MyState> {
  static defaultProps = { text: "", clearOnEnter: true };
  constructor(props: MyProps) {
    super(props);
    this.state = { text: this.props.text };
  }
  onChange(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ text: e.target.value });
  }
  setText() {
    this.props.callback(this.state.text);
  }
  doInput() {
    if (this.state.text === "") {
      return;
    }
    this.props.callback(this.state.text);
    if (this.props.clearOnEnter === true) {
      this.setState({ text: "" });
    }
  }
  enterPressed(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === "Enter") {
      this.doInput();
    }
  }
  getInput() {
    return (
      <Row>
        <Col sm="12" md="8">
          <input
            type="text"
            className="form-control"
            placeholder=""
            onChange={this.onChange.bind(this)}
            onKeyDown={this.enterPressed.bind(this)}
            value={this.state.text}
          />
        </Col>
        <Col className="searchButton" sm="12" md="2">
          <Button className="searchButton" onClick={this.doInput.bind(this)}>{this.props.buttonText}</Button>
        </Col>
      </Row>
    );
  }
  render() {
    return (
      <div>
        {this.getInput()}
      </div>
    )
  }
}
export default TextInputButton;
