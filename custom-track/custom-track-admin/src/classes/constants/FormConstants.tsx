import { FormElementVo } from "../dataObjs/FormElementVo";

export class FormConstants
{
    public static GREEN_CARD_FORM:FormElementVo[]=[new FormElementVo("Title",FormElementVo.TYPE_STRING),new FormElementVo("Content",FormElementVo.TYPE_BLOB)]
    public static WHITE_CARD_FORM:FormElementVo[]=[new FormElementVo("Content",FormElementVo.TYPE_BLOB)]
    
}