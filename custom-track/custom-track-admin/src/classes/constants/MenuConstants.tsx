import { GenVo } from "../dataObjs/GenVo";

export class MenuConstants
{
    static cardSelectMenu:GenVo[]=[new GenVo("Green Card","images/pageImages/png/gcard.png",0,"Select","primary",false),new GenVo("White Card","images/pageImages/png/wcard.png",1,"Select","primary",false)]
}