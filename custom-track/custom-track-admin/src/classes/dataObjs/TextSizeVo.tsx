export class TextSizeVo
{
    public id:number;
    public name:string;
    public classString:string;
    constructor(id:number,name:string,classString:string)
    {
        this.id=id;
        this.name=name;
        this.classString=classString;
    }
}