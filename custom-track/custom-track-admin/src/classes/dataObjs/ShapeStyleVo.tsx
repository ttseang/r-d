export class ShapeStyleVo
{
    public styleName:string;
    public strokeColor:string;
    public strokeWidth:number;
    public fillColor:string;    

    constructor(styleName:string,strokeColor:string,strokeWidth:number,fillColor:string)
    {
        this.styleName=styleName;
        this.strokeColor=strokeColor;
        this.strokeWidth=strokeWidth;
        this.fillColor=fillColor;

    }
}