export class ToolVo
{
    public toolName:string="";
    public icon:string="";
    constructor(toolName:string,icon:string)
    {
        this.toolName=toolName;
        this.icon=icon;
    }
}