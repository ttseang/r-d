import { ShapeStyleVo } from "./ShapeStyleVo";
import { ImgBorderStyleVo } from "./ImgBorderStyleVo";
import { TextPathVo } from "./TextPathVo";

export class ExtrasVo {
    public alpha: number;
    public rotation: number;
    public flipH: boolean;
    public flipV: boolean;
    public orientation: number;
    public skewX: number;
    public skewY: number;

    public fade: number;

    public borderColor: string;
    public borderThick: number;
    public borderRadius: number;
    public borderStyle:ImgBorderStyleVo;
    public fontSize: number;
    public fontColor: string;
    public fontName: string;

    public backgroundPosX: number;
    public backgroundPosY: number;
    public backgroundSizeW: number;
    public backgroundSizeH: number;

    public textCurve:TextPathVo;

    public textSizeID: number = 0;
    public textSizeName: string = "default";
    //red and white hex
    public shapeStyle: ShapeStyleVo = new ShapeStyleVo("default", "#ff0000", 1, "#ffffff");


    public popupID: number = -1;

    public backgroundColor: string = "#ff00ff";

    constructor() {
        this.alpha = 100;
        this.rotation = 0;
        this.flipH = false;
        this.flipV = false;
        this.orientation = 0;
        this.borderColor = "#000000";
        this.borderThick = 0;
        this.borderRadius = 0;
        this.borderStyle = new ImgBorderStyleVo('None',['']);
        this.skewX = 0;
        this.skewY = 0;

        this.fade = 0;

        this.fontSize = 16;
        this.fontColor = "#000000";
        this.fontName = "Arial";
        this.textCurve = new TextPathVo('None',0,0);

        this.backgroundPosX = 0;
        this.backgroundPosY = 0;
        this.backgroundSizeH = 100;
        this.backgroundSizeW = 100;
        
    }
    getExport() {
        //include all properties except the shapeStyle
        let obj: any = {};
        for (let prop in this) {
            if (prop !== "shapeStyle") {
                obj[prop] = this[prop];
            }

        }
        //console.log("get export");
        //console.log(obj);
        return obj as ExtrasVo;
    }
    fromObj(obj: any) {
        //console.log("Extra");
        //console.log(obj);
        this.alpha = parseFloat(obj.alpha);
        this.rotation = parseFloat(obj.rotation);
        this.flipH = (obj.flipH === true || obj.flipH === "true") ? true : false;
        this.flipV = (obj.flipV === true || obj.flipV === "true") ? true : false;
        this.orientation = parseInt(obj.orientation);
        this.borderColor = obj.borderColor;
        this.borderThick = parseFloat(obj.borderThick);
        this.borderRadius = parseFloat(obj.borderRadius);
        if (obj.borderStyle)
        {
           // this.borderStyle=new ImgBorderStyleVo(obj.borderStyle.label,obj.borderStyle.dashArray);
        }
       
        this.skewX = parseFloat(obj.skewX);
        this.skewY = parseFloat(obj.skewY);

        this.fade = parseFloat(obj.fade);

        this.fontSize = parseFloat(obj.fontSize);
        this.fontColor = obj.fontColor;
        this.fontName = obj.fontName;

        this.backgroundPosX = parseFloat(obj.backgroundPosX);
        this.backgroundPosY = parseFloat(obj.backgroundPosY);
        this.backgroundSizeH = parseFloat(obj.backgroundSizeH);
        this.backgroundSizeW = parseFloat(obj.backgroundSizeW);

        this.popupID = parseInt(obj.popupID);
        this.textSizeID = parseInt(obj.textSizeID);
        if (obj.textCurve)
        {
            this.textCurve=new TextPathVo(obj.textCurve.type,parseInt(obj.textCurve.pathIndex),parseInt(obj.textCurve.anchorX));
        }
       // this.textCurve=new TextPathVo(obj.textCurve.type,parseInt(obj.textCurve.pathIndex),parseInt(obj.textCurve.anchorX));
    }
   
}