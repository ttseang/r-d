export class FontVo
{
    public label:string;
    public fontName:string;

    constructor(label:string,fontName:string)
    {
        this.label=label;
        this.fontName=fontName;
    }
}