export class ImgBorderStyleVo
{
    public label:string;
    public params:string[];

    constructor(label:string,params:string[])
    {
        this.label=label;
        this.params = params;
    }
}