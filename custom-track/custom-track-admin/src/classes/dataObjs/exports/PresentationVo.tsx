import { StepExportVo } from "./StepExportVo";

export class PresentationVo
{
    public steps:StepExportVo[];
    public pageW:number;
    public pageH:number;
    public attribution:string;
    public bgColor:string;

    constructor(steps:StepExportVo[],pageW:number,pageH:number, attribution:string, colorBG:string)
    {
        this.steps=steps;
        this.pageW=pageW;
        this.pageH=pageH;
        this.attribution = attribution;
        this.bgColor = colorBG;
    }
}