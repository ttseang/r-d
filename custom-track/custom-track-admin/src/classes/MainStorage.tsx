import { ElementVo } from "./dataObjs/ElementVo";
import { PresentationVo } from "./dataObjs/exports/PresentationVo";
import { StepSaveVo } from "./dataObjs/exports/saver/StepSaveVo";
import { ZoomSaveVo } from "./dataObjs/exports/saver/ZoomSaveVo";
import { StepExportVo } from "./dataObjs/exports/StepExportVo";
import { FileVo } from "./dataObjs/FileVo";
import { FormElementVo } from "./dataObjs/FormElementVo";
import { GenVo } from "./dataObjs/GenVo";
import { HistoryVo } from "./dataObjs/HistoryVo";
import { LibItemVo } from "./dataObjs/LibItemVo";
import { PopUpVo } from "./dataObjs/PopUpVo";
import { ScreenSizeVo } from "./dataObjs/ScreenSizeVo";
import { ShapeStyleVo } from "./dataObjs/ShapeStyleVo";
import { StepVo } from "./dataObjs/StepVo";
import { StyleVo } from "./dataObjs/StyleVo";
import { TextStyleVo } from "./dataObjs/TextStyleVo";
import { ZoomVo } from "./dataObjs/ZoomVo";
import { LibUtil } from "./libUtil";
import { StyleUtil } from "./styleUtil";

export class MainStorage {
  private static instance: MainStorage | null = null;

  public styleMap: Map<number, TextStyleVo> = new Map<number, TextStyleVo>();
  public steps: StepVo[] = [];
  private _currentStep: number = 0;

  public selectedStep: StepVo;

  public selectedElement: ElementVo | null = null;
  public pageW: number = 480;
  public pageH: number = 270;
  public pageTop: number = 27;
  public pageLeft: number = 33;

  public attribution: string = "";
  public bgColor: string = "#cccccc";

  public selectedTool:string="";

  public eidIndex: number = 0;
  private _eid: string = "instance0";

  public libUtil: LibUtil;
  public library: LibItemVo[] = [];
  public suggested: string = "";
  public zoomLocked: boolean = true;

  public zoomVo: ZoomVo = new ZoomVo(0, 0, 100, 100);

  public linkedCount: number = 0;

  public popups: PopUpVo[] = [];
  public styles: StyleVo[] = [];
  public styleUtil: StyleUtil;

  public editText: string = "";
  public editBGSettings: string = "";
  public tempID: number = 0;

  public menu: GenVo[] = [];
  public formData: FormElementVo[] = [];

  public exportCode: string = "";
  public saveCode: string = "";

  private historyIndex: number = 0;
  private history: HistoryVo[] = [];

  public keyLock: boolean = false;

  public copyElement: ElementVo | null = null;
  public LtiFileName: string = "";
  public fileName: string = "";
  public files: FileVo[] = [];
  public appType: string = "zoomy";

  public framePanelIndex: number = 0;
  public backgroundAudioSwitch: boolean = false;
  public screenSizes: ScreenSizeVo[] = [];

  public shapeStyles: ShapeStyleVo[] = [];

  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor() {
    //media items
    this.libUtil = new LibUtil();
    this.library = this.libUtil.library;

    this.styleUtil = new StyleUtil();

    this.styles = this.styleUtil.styles;

    this.steps.push(new StepVo(0, []));
    this.selectedStep = this.steps[0];

    (window as any).ms = this;
    this.makeShapeStyles();
  }
  public getCurvePath(index:number)
  {
     switch(index)
     {
        case 0:
          return "";
        case 1:
          return "M73.2,148.6c4-6.1,65.5-96.8,178.6-95.6c111.3,1.2,170.8,90.3,175.1,97";
        case 2:
          return "M 0 0 C 37.5 56.25 306.25 156.25 612.5 -37.5";
     }
     return "";
  }
  private makeShapeStyles() {
     //red stroke green fill
      this.shapeStyles.push(new ShapeStyleVo("red stroke green fill", "#ff0000", 1, "#00ff00"));
      //red stroke blue fill
      this.shapeStyles.push(new ShapeStyleVo("red stroke blue fill", "#ff0000", 1, "#0000ff"));
      //red stroke yellow fill
      this.shapeStyles.push(new ShapeStyleVo("red stroke yellow fill", "#ff0000", 1, "#ffff00"));
      //red stroke orange fill
      this.shapeStyles.push(new ShapeStyleVo("red stroke orange fill", "#ff0000", 1, "#ff8000"));
      //red stroke purple fill
      this.shapeStyles.push(new ShapeStyleVo("red stroke purple fill", "#ff0000", 1, "#800080"));
      //red stroke black fill
      this.shapeStyles.push(new ShapeStyleVo("red stroke black fill", "#ff0000", 1, "#000000"));
      //red stroke white fill
      this.shapeStyles.push(new ShapeStyleVo("red stroke white fill", "#ff0000", 1, "#ffffff"));
      //green stroke red fill
      this.shapeStyles.push(new ShapeStyleVo("green stroke red fill", "#00ff00", 1, "#ff0000"));
      //green stroke blue fill
      this.shapeStyles.push(new ShapeStyleVo("green stroke blue fill", "#00ff00", 1, "#0000ff"));
      //green stroke yellow fill
      this.shapeStyles.push(new ShapeStyleVo("green stroke yellow fill", "#00ff00", 1, "#ffff00"));
      //green stroke orange fill
      this.shapeStyles.push(new ShapeStyleVo("green stroke orange fill", "#00ff00", 1, "#ff8000"));
      //green stroke purple fill
      this.shapeStyles.push(new ShapeStyleVo("green stroke purple fill", "#00ff00", 1, "#800080"));
      //green stroke black fill
      this.shapeStyles.push(new ShapeStyleVo("green stroke black fill", "#00ff00", 1, "#000000"));
      //green stroke white fill
      this.shapeStyles.push(new ShapeStyleVo("green stroke white fill", "#00ff00", 1, "#ffffff"));
      //blue stroke red fill
      this.shapeStyles.push(new ShapeStyleVo("blue stroke red fill", "#0000ff", 1, "#ff0000"));
      //blue stroke green fill thick stroke
      this.shapeStyles.push(new ShapeStyleVo("blue stroke green fill thick stroke", "#0000ff", 3, "#00ff00"));
      //blue stroke yellow fill thick stroke
      this.shapeStyles.push(new ShapeStyleVo("blue stroke yellow fill thick stroke", "#0000ff", 3, "#ffff00"));
      //blue stroke orange fill thick stroke
      this.shapeStyles.push(new ShapeStyleVo("blue stroke orange fill thick stroke", "#0000ff", 3, "#ff8000"));
      //blue stroke purple fill thick stroke
      this.shapeStyles.push(new ShapeStyleVo("blue stroke purple fill thick stroke", "#0000ff", 3, "#800080"));

  }
  public getPreviewSize(multiplier:number=1): object {
    //get css variable --vw
   /*  let vw:number=parseFloat(getComputedStyle(document.documentElement).getPropertyValue("--vw"));
    let px:number=vw/9.6;
    //console.log("vw", vw);
    //console.log("px", px);
 */
    //get the screen width -this is temporary
  //  let screenWidth:number=window.innerWidth;

   // let ww:number=screenWidth*multiplier;
   //console.log("pageW", this.pageW);
   //console.log("multiplier", multiplier);
    let ww:number=this.pageW*multiplier;
    let hh:number=ww/2;

    return {
      w: ww,
      h: hh
    };
  }
  public get eid(): string {
    this._eid = "instance" + this.eidIndex.toString();
    return this._eid;
  }
  public get currentStep(): number {
    return this._currentStep;
  }
  public set currentStep(value: number) {
    this._currentStep = value;
    this.selectedStep = this.steps[this._currentStep];
  }
  public addHistory() {
    if (this.historyIndex !== this.history.length - 1) {
      this.history = this.history.slice(0, this.historyIndex);
    }
    this.history.push(new HistoryVo(this.currentStep, this.steps));
    this.historyIndex = this.history.length - 1;
  }
  public goBack() {
    if (this.historyIndex > 0) {
      this.historyIndex--;
      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }
  public goForward() {
    if (this.historyIndex < this.history.length - 1) {
      this.historyIndex++;

      let historyVo: HistoryVo = this.history[this.historyIndex];
      this.steps = historyVo.steps;
      this.currentStep = historyVo.currentStep;
    }
  }
  getAudioPath(audio: string) {
    return "https://ttv5.s3.amazonaws.com/william/audio/" + audio;
  }
  getPageH() {
    return this.pageH;
  }
  getPageW() {
    return this.pageW;
  }
  public static getInstance(): MainStorage {
    if (this.instance === null) {
      this.instance = new MainStorage();
    }
    return this.instance;
  }
  getStepIndex(id: number) {
    for (let i: number = 0; i < this.steps.length; i++) {
      let step: StepVo = this.steps[i];
      if (step.id === id) {
        return i;
      }
    }
    return -1;
  }

  getImagePath(image: string, path: number = 0) {

    if (path === 1) {
      var test = image.substring(0,5);
      if(test === "https"){
        return image;
      } else{
        return "https://ttv5.s3.amazonaws.com/william/images/bookimages/" + image;
      }
    } else if(path === 2){
      return "https://ttv5.s3.amazonaws.com/william/images/appImages/" + image;
    }
    return "https://ttv5.s3.amazonaws.com/william/images/bookimages/" + image;
  }

  getPopUpByID(id: number) {
    for (let i: number = 0; i < this.popups.length; i++) {
      if (this.popups[i].id === id) {
        return this.popups[i];
      }
    }
    return this.popups[0];
  }
  getExport() {
    let allSteps: StepExportVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }

    let presentationVo: PresentationVo = new PresentationVo(allSteps, this.pageW, this.pageH, this.attribution, this.bgColor);

    ////console.log(presentationVo);
    this.exportCode = JSON.stringify(presentationVo);
    return this.exportCode;
  }
  getFullScreenPreview() {
    let allSteps: StepExportVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toExport())
    }

    let presentationVo: PresentationVo = new PresentationVo(allSteps, this.pageW, this.pageH, this.attribution, this.bgColor);

    ////console.log(presentationVo);
    return JSON.stringify(presentationVo);

  }
  saveStageToLocalStorage(pageLeft: number, pageTop: number, pageW: number, pageH: number) {
    localStorage.setItem('pageLeft', pageLeft.toString());
    localStorage.setItem('pageTop', pageTop.toString());
    localStorage.setItem('pageW', pageW.toString());
    localStorage.setItem('pageH', pageH.toString());
    
}
restoreStageFromLocalStorage() {
    let pageLeft: number = parseInt(localStorage.getItem('pageLeft') || '33');
    let pageTop: number = parseInt(localStorage.getItem('pageTop') || '27');
    let pageW: number = parseInt(localStorage.getItem('pageW') || '480');
    let pageH: number = parseInt(localStorage.getItem('pageH') || '200');
    this.pageLeft = pageLeft;
    this.pageTop = pageTop;
    this.pageW = pageW;
    this.pageH = pageH;
}
  saveZoom() {
    let allSteps: StepSaveVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toSave())
    }
    let zoomSave: ZoomSaveVo = new ZoomSaveVo(allSteps, this.pageW, this.pageH);
    this.saveCode = JSON.stringify(zoomSave);
    return this.saveCode;
  }
  saveZoom2() {
    let allSteps: StepSaveVo[] = [];
    for (let i: number = 0; i < this.steps.length; i++) {
      allSteps.push(this.steps[i].toSave())
    }
    let zoomSave: ZoomSaveVo = new ZoomSaveVo(allSteps, this.pageW, this.pageH);
    return zoomSave;
  }
  loadZoom(data: any) {

    ////console.log(data);
    this.steps = [];

    this.pageW = parseInt(data.pageW);
    this.pageH = parseInt(data.pageH);

    let steps: any[] = data.steps;

    for (let i: number = 0; i < steps.length; i++) {
      let stepVo: StepVo = new StepVo();
      stepVo.fromObj(steps[i]);
      this.steps.push(stepVo);
    }

    this.selectedStep = this.steps[0];
  }
  /* showEid() {
    for (let i: number = 0; i < this.steps.length; i++) {
      let stepVo: StepVo = this.steps[i];
     // //console.log("step " + i.toString());
      for (let j: number = 0; j < stepVo.elements.length; j++) {
        let elementVo = stepVo.elements[j];
        //console.log(elementVo.type + " " + elementVo.eid + " id=" + elementVo.id.toString());

      }

    }
  } */
}
export default MainStorage;