export class ScreenUtil {
    public static convertVWtoPixel(vw: number): number {
        let px:number= vw * window.innerWidth / 100;
        //round to 2 decimal places
        px=Math.round(px*100)/100;
        return px;
    }
    public static convertVHtoPixel(vh: number): number {
        let px:number= vh * window.innerHeight / 100;
        //round to 2 decimal places
        px=Math.round(px*100)/100;
        return px;
    }
    public static convertPixeltoVW(px: number): number {
        let vw:number= px * 100 / window.innerWidth;
        //round to 2 decimal places
        vw=Math.round(vw*100)/100;
        return vw;
    }
    public static convertPixeltoVH(px: number): number {
        let vh:number= px * 100 / window.innerHeight;
        //round to 2 decimal places
        vh=Math.round(vh*100)/100;
        return vh;
    }
    public static convertVWtoVH(vw: number): number {
        return vw * window.innerHeight / window.innerWidth;
    }
    public static convertVHtoVW(vh: number): number {
        return vh * window.innerWidth / window.innerHeight;
    }
    //get percentage of screen width when given a vw value
    public static getPercentWidth(vw: number): number {
        return vw * window.innerWidth / 100;
    }
    //get percentage of screen height when given a vh value
    public static getPercentHeight(vh: number): number {
        return vh * window.innerHeight / 100;
    }
}