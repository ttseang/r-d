import React, { Component } from 'react';
import { ButtonIconVo } from '../classes/dataObjs/ButtonIconVo';
import AddBox from './AddBox';
interface MyProps { addElements: ButtonIconVo[], addNewElement: Function }
interface MyState { addElements: ButtonIconVo[] }
class AddBoxes extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = { addElements: this.props.addElements }
        }
    getAddBoxes() {
        //     let addElements: string[] = ["Image", "Text","Gutter Image","Special"];    

        let addRows: JSX.Element[] = [];
        for (let i: number = 0; i < this.state.addElements.length; i++) {
            let key: string = "addBox" + i.toString();
            addRows.push(<AddBox key={key} buttonData={this.state.addElements[i]} callback={() => { this.props.addNewElement(this.state.addElements[i].action) }}></AddBox>)
        }
        return (<div>{addRows}</div>)
    }

    render() {
        return (<div>{this.getAddBoxes()}</div>)
    }
}
export default AddBoxes;