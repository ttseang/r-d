import React, { Component } from 'react';
import Card from 'react-bootstrap/esm/Card';
import { StepVo } from '../classes/dataObjs/StepVo';
import MainStorage from '../classes/MainStorage';
interface MyProps { stepVo: StepVo,selectCallback:Function }
interface MyState { stepVo: StepVo }
class StepBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = { stepVo: this.props.stepVo };

        }
        componentDidUpdate(oldProps:MyProps)
        {
            if (this.props!==oldProps)
            {
                this.setState({stepVo:this.props.stepVo});
            }
        }
    render() {

        let classString:string="stepCard";

        if (this.ms.selectedStep)
        {
            if (this.ms.selectedStep.id===this.props.stepVo.id)
            {
                classString="stepCard selected";
            }
        }

        return (<Card className={classString} onClick={()=>{this.props.selectCallback(this.state.stepVo)}}>{this.state.stepVo.label}</Card>)
    }
}
export default StepBox;