import React, { Component } from 'react';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { TextStyleVo } from '../classes/dataObjs/TextStyleVo';
import MainStorage from '../classes/MainStorage';
import { TextPathVo } from '../classes/dataObjs/TextPathVo';
import { TextSizeVo } from '../classes/dataObjs/TextSizeVo';
interface MyProps { elementVo: ElementVo, callback: Function, onUp: Function, lineUp: Function, lineDown: Function, preview: boolean }
interface MyState { }
class BookElement extends Component<MyProps, MyState>
{
    public ref: React.RefObject<any>;
    private ms: MainStorage = MainStorage.getInstance();
    private origins: string[];

    constructor(props: MyProps) {
        super(props);
        this.state = {};
        this.ref = React.createRef();
        this.props.elementVo.el = this;

        this.origins = ["0,0", "-50%,0", "-100%,0", "0,-50%", "-50%,-50%", "-100%,-50%", "0,-100%", "-50%,-100%", "-100%,-100%"];
    }

    getStyle(elementVo: ElementVo) {
        let style: React.CSSProperties = {};
        if (elementVo.id === this.ms.selectedElement?.id) {
            style.outline = "cyan dashed 2px";
        }

        if (elementVo.type !== ElementVo.TYPE_SHAPE) {
            style.left = elementVo.x.toString() + "%";

            //left var vw 
            //style.left = "calc(var(--vw) * "+elementVo.x.toString()+")";

            style.top = elementVo.y.toString() + "%";

         //   style.width = elementVo.w.toString() + "%";
            //calc var vw
         //   style.width = "calc(var(--vw) * "+elementVo.w.toString()+")";
            //use %
            style.width = elementVo.w.toString() + "%";

        }
        else {
            style.width = "100%";
            style.height = "100%";
            style.left = "0";
            style.top = "0";

            return style;
        }
        if (elementVo.type === ElementVo.TYPE_BACK_IMAGE) {
            let image: string = "url(" + this.ms.getImagePath(this.props.elementVo.content[0]) + ")";
            style.backgroundImage = image;
            style.backgroundPositionX = elementVo.extras.backgroundPosX + "px";
            style.backgroundPositionY = elementVo.extras.backgroundPosY + "px";



            /* if (elementVo.subType === ElementVo.SUB_TYPE_GUTTER) { 
                let ww: number = elementVo.extras.backgroundSizeW;

                let bgs: string = ww.toString() + "px";
                style.backgroundSize = bgs;
                style.width = elementVo.w.toString() + "px";
                style.height = elementVo.h.toString() + "px";

            } */
        }

        /*  if (elementVo.type===ElementVo.TYPE_CARD)
         {
             let cw:number=elementVo.w;
             style.width=cw.toString()+"vw";
             style.height=(cw/2.5).toString()+"vw";
 
         } */
        if (elementVo.type === ElementVo.TYPE_TEXT && elementVo.textStyle === -1) {

            style.color = elementVo.extras.fontColor;
            style.fontFamily = elementVo.extras.fontName;
        }

        if (elementVo.type === ElementVo.TYPE_TEXT) {
            style.fontSize = elementVo.extras.fontSize;
        }

        if (elementVo.extras.alpha !== 100) {
            style.opacity = elementVo.extras.alpha.toString() + "%";
        }
        if (elementVo.type === ElementVo.TYPE_SHAPE) {

            return style;
        }
        let transformString: string = "translate(" + this.origins[elementVo.extras.orientation] + ")";

        if (elementVo.extras.rotation !== 0) {
            let angle: number = ((elementVo.extras.rotation) / 100) * 360;
            angle = Math.floor(angle);
            ////////console.log(angle);
            transformString += " rotate(" + angle.toString() + "deg)";
        }
        if (elementVo.extras.skewY !== 0) {
            let skewAngleY: number = ((elementVo.extras.skewY) / 100) * 360;
            skewAngleY = Math.floor(skewAngleY);
            ////////console.log(skewAngleY);
            transformString += " skewY(" + skewAngleY.toString() + "deg)";
        }
        if (elementVo.extras.skewX !== 0) {
            let skewAngleX: number = ((elementVo.extras.skewX) / 100) * 360;
            skewAngleX = Math.floor(skewAngleX);
            ////////console.log(skewAngleX);
            transformString += " skewX(" + skewAngleX.toString() + "deg)";
        }
        if (elementVo.extras.flipV === true) {
            transformString += " scaleY(-1)";
        }
        if (elementVo.extras.flipH === true) {
            transformString += " scaleX(-1)";
        }

        style.transform = transformString;
        //console.log(elementVo.extras.borderStyle);

        if (elementVo.extras.borderThick > 0 || elementVo.extras.borderRadius > 0 || elementVo.extras.borderStyle.label !== "None") {
            style.border = "solid";
            style.borderWidth = elementVo.extras.borderThick;
            style.borderColor = elementVo.extras.borderColor;
            if (elementVo.extras.borderStyle.label === "None") {

                style.borderRadius = elementVo.extras.borderRadius;
            } else {
                style.clipPath = elementVo.extras.borderStyle.params[0];
            }
        }

        if (elementVo.extras.fade > 0) {
            var amount: number = 100 - elementVo.extras.fade;
            style.WebkitMaskImage = 'linear-gradient(180deg, #000000 0%, rgba(0,0,0,0.0) ' + amount + '%)';
        }


        return style;
    }
    getTransform(elementVo: ElementVo) {
        let transformString: string = "";

        //add position
        let x: number = elementVo.x;
        let y: number = elementVo.y;
        transformString += " translate(" + x.toString() + "%," + y.toString() + "%)";

        let angle: number = ((elementVo.extras.rotation) / 100) * 360;
        angle = Math.floor(angle);
        ////////console.log(angle);
        transformString += " rotate(" + angle.toString() + "deg)";

        //set scale

        //set the shape width to the same size as the stage 480 px
        //shape original width is 100px
        //so the scale is 480/100=4.8
        //and then we can use the elementVo.w to scale the shape

        //get the css variable --stageWidth
        let stageWidth: number = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--stageWidth')) || 480;

        let scale: number = stageWidth / 100;
        scale = scale * elementVo.w / 100;



        //let scale: number = ((elementVo.w) / 100);
        if (isNaN(scale) === true) {
            scale = 1;
        }
        transformString += " scale(" + scale.toString() + ")";




        if (elementVo.extras.skewY !== 0) {
            let skewAngleY: number = ((elementVo.extras.skewY) / 100) * 360;
            skewAngleY = Math.floor(skewAngleY);
            //   //console.log(skewAngleY);
            transformString += " skewY(" + skewAngleY.toString() + "deg)";
        }
        if (elementVo.extras.skewX !== 0) {
            let skewAngleX: number = ((elementVo.extras.skewX) / 100) * 360;
            skewAngleX = Math.floor(skewAngleX);
            //  //console.log(skewAngleX);
            transformString += " skewX(" + skewAngleX.toString() + "deg)";
        }
        if (elementVo.extras.flipV === true) {
            transformString += " scaleY(-1)";
        }
        if (elementVo.extras.flipH === true) {
            transformString += " scaleX(-1)";
        }


        return transformString;
    }
    getShapeStyle(elementVo: ElementVo) {
        let style: React.CSSProperties = {};
        style.fill = elementVo.extras.backgroundColor;
        style.stroke = elementVo.extras.borderColor;
        style.strokeWidth = elementVo.extras.borderThick;
        style.opacity = elementVo.extras.alpha.toString() + "%";
        style.transform = this.getTransform(elementVo);
        if (elementVo.id === this.ms.selectedElement?.id) {
            style.outline = "cyan dashed 2px";
        }

        return style;
    }
    getHtml() {
        let ukey: string = "element" + this.props.elementVo.id.toString();
        let elementVo: ElementVo = this.props.elementVo;
        let style: React.CSSProperties = this.getStyle(elementVo);

        let classArray: string[] = this.props.elementVo.classes.slice();

        if (elementVo.visible === false) {
            classArray.push("hid");
        }
        if (elementVo.locked === true) {
            classArray.push("locked");
        }

        if (elementVo.textStyle !== -1) {

            if (this.ms.styleMap.has(elementVo.textStyle)) {
                let textStyleVo: TextStyleVo | undefined = this.ms.styleMap.get(elementVo.textStyle);
                ////console.log(textStyleVo);

                if (textStyleVo) {
                    for (let i: number = 0; i < textStyleVo.classes.length; i++) {
                        classArray.push(textStyleVo.classes[i]);
                    }
                }
            }
        }


        if (this.props.elementVo.type === ElementVo.TYPE_CARD) {

          //  classArray.push("infoCard");
            switch (this.props.elementVo.subType) {
                case ElementVo.SUB_TYPE_GREEN_CARD:
                    classArray.push("greenCard");
                    break;

                case ElementVo.SUB_TYPE_WHITE_CARD:
                    classArray.push("whiteCard");
                    break;
            }

        }

        let classes: string = classArray.concat(['abPos']).join(" ");


        if (this.props.elementVo.type === ElementVo.TYPE_LINE) {
            let x1: number = (elementVo.x / 100) * this.ms.pageW;
            let y1: number = (elementVo.y / 100) * this.ms.pageH;
            let x2: number = (elementVo.w / 100) * this.ms.pageW;
            let y2: number = (elementVo.h / 100) * this.ms.pageH;

            let x3: number = x1 + ((x2 - x1) / 2);
            let y3: number = y1 + ((y2 - y1) / 2);

            let selected: string;

            if (elementVo.id === this.ms.selectedElement?.id) {
                selected = "cyan dashed 2px";
            } else {
                selected = "";
            }


            if (elementVo !== this.ms.selectedElement || this.props.preview === true) {
                return (<svg className='noPoint' key={ukey} x={0} y={0} width={this.ms.pageW} height={this.ms.pageH} style={{ position: "relative" }}>

                    <line x1={x1} y1={y1} x2={x2} y2={y2} style={{ stroke: elementVo.extras.borderColor, strokeWidth: elementVo.extras.borderThick.toString() + "px" }} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }}></line>
                </svg>)
            }

            return (<svg key={ukey} x={0} y={0} width={this.ms.pageW} height={this.ms.pageH} className="linePos">

                <line x1={x1} y1={y1} x2={x2} y2={y2} style={{ stroke: elementVo.extras.borderColor, strokeWidth: elementVo.extras.borderThick.toString() + "px", outline: selected }}></line>
                <circle cx={x1} cy={y1} r={10} className="dragCircle" onPointerUp={() => { this.props.lineUp(this.props.elementVo, 0) }} onPointerDown={() => { this.props.lineDown(this.props.elementVo, 0) }}></circle>
                <circle cx={x2} cy={y2} r={10} className="dragCircle" onPointerUp={() => { this.props.lineUp(this.props.elementVo, 1) }} onPointerDown={() => { this.props.lineDown(this.props.elementVo, 1) }}></circle>
                <circle cx={x3} cy={y3} r={10} className="dragCircle" onPointerUp={() => { this.props.lineUp(this.props.elementVo, 2) }} onPointerDown={() => { this.props.lineDown(this.props.elementVo, 2) }}></circle>

            </svg>)
        }

        if (this.props.elementVo.type === ElementVo.TYPE_CARD) {

            style.minHeight = elementVo.h.toString() + "%";
          //  style.width = elementVo.w.toString() + "%";
          //calc var vw
          style.width = "calc(var(--vw) * "+elementVo.w.toString()+")";

            let textStyle: React.CSSProperties = {};
            textStyle.fontSize = elementVo.extras.fontSize.toString() + "%";
            textStyle.fontFamily = elementVo.extras.fontName;
            textStyle.color = elementVo.extras.fontColor;

            let textStyle2: React.CSSProperties = {};
            let fs2: number = elementVo.extras.fontSize + 2;
            textStyle2.fontSize = fs2.toString() + "%";
            textStyle2.fontFamily = elementVo.extras.fontName;
            textStyle2.color = elementVo.extras.fontColor;
            textStyle2.fontWeight = "bold";


            switch (this.props.elementVo.subType) {
                case ElementVo.SUB_TYPE_GREEN_CARD:
                    
                    return (<div className={classes} style={style} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} >
                        <h4 id="title" style={textStyle2}>{this.props.elementVo.content[0]}</h4>
                        <p id="content" style={textStyle}>{this.props.elementVo.content[1]}</p>
                    </div>)
                case ElementVo.SUB_TYPE_WHITE_CARD:
                    return (<div className={classes} style={style} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} >
                        <p id="content" style={textStyle}>{this.props.elementVo.content[0]}</p>
                    </div>)
            }
        }
        if (this.props.elementVo.type === ElementVo.TYPE_TEXT) {

           
            if (this.props.elementVo.extras.textCurve.label !== "None") {
                //get css variable --px
                let px: number = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--px'));
                let fontSize: number = elementVo.extras.fontSize * px;

                style.fontSize = fontSize.toString() + "px";

                const textCurve: TextPathVo = this.props.elementVo.extras.textCurve;
                const curvePath: string = this.ms.getCurvePath(textCurve.pathIndex);

                return (
                    // <article ref={this.ref} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} style={style}>
                    <svg viewBox="0 0 500 500" ref={this.ref} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} style={style}>
                        <path id={elementVo.id + "path"} stroke="none" fill="transparent" d={curvePath} />
                        <text textAnchor="middle" x={this.props.elementVo.extras.textCurve.anchorX} y="50">
                            <textPath href={"#" + elementVo.id + "path"}>
                                {this.props.elementVo.content[0]}
                            </textPath>
                        </text>
                    </svg>
                    // </article>
                )
            } else {

                const textSizes:TextSizeVo[]=this.ms.styleUtil.textSizes;
                let textSize:TextSizeVo=textSizes[elementVo.extras.textSizeID];
                console.log(textSize.classString);
                
                classes+=" "+textSize.classString;

                return (<p ref={this.ref} key={ukey} id={ukey} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} style={style}>{this.props.elementVo.content[0]}</p>);
            }
        }
        if (this.props.elementVo.type === ElementVo.TYPE_BACK_IMAGE) {

            if (elementVo.subType !== ElementVo.SUB_TYPE_GUTTER) {
                return (<div key={ukey} id={ukey} ref={this.ref} style={style} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} ></div>)
            }
            else {
                return (<div key={ukey} id={ukey} ref={this.ref} style={style} onPointerUp={() => { this.props.onUp(this.props.elementVo) }} onPointerDown={() => { this.props.callback(this.props.elementVo) }} className={classes} ></div>)
            }
        }

        if (this.props.elementVo.type === ElementVo.TYPE_SHAPE) {

            return (<svg key={ukey} style={style} className={classes} onPointerDown={() => { this.props.callback(this.props.elementVo) }}>
                <path d={this.props.elementVo.content[0]} style={this.getShapeStyle(this.props.elementVo)} />
            </svg>)

        }

        return (<img key={ukey} id={ukey} ref={this.ref} className={classes} style={style} onPointerDown={() => { this.props.callback(this.props.elementVo) }} src={this.ms.getImagePath(this.props.elementVo.content[0], 1)} alt="bookpart" />)
    }

    render() {
        return (this.getHtml())
    }
}
export default BookElement;