import React, { Component } from 'react';
import { StepVo } from '../classes/dataObjs/StepVo';
import MainStorage from '../classes/MainStorage';
import { TemplateData } from '../classes/TemplateData';
interface MyProps { stepVo: StepVo | null,editCallback:Function,onUp:Function,onMove:Function,lineUp:Function,lineDown:Function,w:number,h:number}
interface MyState { stepVo: StepVo | null,w:number,h:number}
class PreviewPage extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private resizeListener: any = null;
    private size:number=1;
    constructor(props: MyProps) {
        super(props);
       /*  let psize:any=this.ms.getPreviewSize(this.size); */
        this.state = { stepVo: this.props.stepVo,w:this.props.w,h:this.props.h };
    }
    componentDidMount(): void {
        this.resizeListener = window.addEventListener("resize", () => {
            let psize:any=this.ms.getPreviewSize(this.size);
            this.setState({w:psize.w,h:psize.h});
        });
    }
    componentWillUnmount(): void {
        window.removeEventListener("resize", this.resizeListener);
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ stepVo: this.props.stepVo,w:this.props.w,h:this.props.h });
        }
    }
    getPath(image: string) {
        return "./images/bookimages/" + image;
    }

    getPage() {       

        let td: TemplateData = new TemplateData();       
        let w:number=this.ms.pageW;
        let h:number=this.ms.pageH;
        
        return td.getPage2(this.state.stepVo,this.props.editCallback,this.props.onUp,this.props.lineUp,this.props.lineDown,w,h);
       // return td.getPage(pageVo, texts, images,left);
    }


    render() {
/* 
        let obj:any=this.ms.getPreviewSize(2.5);
        
        //TODO RANDY SCREEN SIZE 3440 x 1440
        let vb:string="0 0 "+obj.w.toString()+" "+obj.h.toString(); */
         let w:number=this.ms.pageW;
        let h:number=this.ms.pageH;
       /*  let w:string="calc(var(--pvw)*100)";
        let h:string="calc(var(--pvh)*100)"; */
        let vb:string="0 0 "+w.toString()+" "+h.toString();

        return (<svg viewBox={vb} height={h} width={w} onPointerMove={(e:React.PointerEvent<SVGElement>)=>{this.props.onMove(e)}}>
            {this.getPage()}           
            </svg>)
    }
}
export default PreviewPage;