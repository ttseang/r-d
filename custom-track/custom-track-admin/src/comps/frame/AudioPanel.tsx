import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

import { StepVo } from '../../classes/dataObjs/StepVo';
import { MainController } from '../../classes/MainController';
interface MyProps { stepVo: StepVo | null,useBackground:boolean }
interface MyState { stepVo: StepVo | null }
class AudioPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepVo: this.props.stepVo };
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({stepVo:this.props.stepVo});
        }
    }
    getAudioName()
    {
        if (this.state.stepVo)
        {
            let audio:string=this.state.stepVo.audioName;
            if (this.props.useBackground===true)
            {
                audio=this.state.stepVo.backgroundAudioName;
            }
            if (audio==="")
            {
                return "No Audio Set";
            }
            let nameArray:string[]=audio.split("/");
            let name:string=nameArray[nameArray.length-1];
            name=name.replace(".mp3","");
            return name;
        }
        return "";
    }
    openAudio()
    {
        if (this.props.useBackground===false)
        {
            this.mc.changeScreen(5);
        }
        else
        {
            this.mc.changeScreen(6);
        }
    }
    render() {
        return (<div className='tac'><h6 className='mt10'>{this.getAudioName()}</h6><div className='m20'><Button onClick={()=>{this.openAudio()}}>Set Audio</Button></div></div>)
    }
}
export default AudioPanel;