import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

import { StepVo } from '../../classes/dataObjs/StepVo';
import { MainController } from '../../classes/MainController';
import MainStorage from '../../classes/MainStorage';
interface MyProps { stepVo: StepVo | null,useBackground:boolean }
interface MyState { stepVo: StepVo | null }
class AttributionPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();
    private ms:MainStorage=MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { stepVo: this.props.stepVo };
    }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({stepVo:this.props.stepVo});
        }
    }
    getAttributionState()
    {
        if (this.state.stepVo)
        {
            let attribution:string=this.state.stepVo.audioName;
            if (this.props.useBackground===true)
            {
                attribution=this.ms.attribution;
            }
            if (attribution==="")
            {
                return "No Attribution Entered";
            }
            
            return attribution;
        }
        return "";
    }
    openAttribution()
    {
            this.mc.changeScreen(4);
    }
    render() {
        return (<div className='tac'><h6 className='mt10'>{this.getAttributionState()}</h6><div className='m20'><Button onClick={()=>{this.openAttribution()}}>Set Attribution</Button></div></div>)
    }
}
export default AttributionPanel;