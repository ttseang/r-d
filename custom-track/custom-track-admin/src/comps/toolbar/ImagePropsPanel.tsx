import { MainController } from '../../classes/MainController';

export default function ImagePropsPanel() {
    const mc = MainController.getInstance();
  return (
    <div>
    <button className="btn btn-success btn-sm" style={{width:'100%' ,margin:'10px 0px 0px 0px',display:'block'}} onClick={() => mc.addNewElement(0)}>Add New</button>
       {mc.getToolProps('imagePropControls')}
    </div>
  );
}
