import React, { Component } from 'react';
import { MainController } from '../../classes/MainController';
import BGSettingsPanel from '../toolpanels/BGSettingsPanel';
import ImagePropsPanel from './ImagePropsPanel';
import ZoomPropPanel from './ZoomPropPanel';
import { ToolVo } from '../../classes/dataObjs/ToolVo';

interface MyProps { zoomLocked: boolean; }
interface MyState { selectedTool: string; zoomLocked: boolean; }

enum ToolNames {
  settingsTool = 'settingsTool',
  imageTool = 'imageTool',
  textTool = 'textTool',
  cardTool = 'cardTool',
  lineTool = 'lineTool',
  shapeTool = 'shapeTool',
  zoomTool = 'zoomTool'
}

class ToolBar extends Component<MyProps, MyState>
{
  //private toolNames = [ToolNames.settingsTool, ToolNames.imageTool, ToolNames.textTool, ToolNames.cardTool, ToolNames.lineTool, ToolNames.shapeTool, ToolNames.zoomTool];
  private tools: ToolVo[] = [];
  private mc: MainController = MainController.getInstance();

  constructor(props: MyProps) {

    super(props);
    this.state = { selectedTool: 'settingsTool', zoomLocked: this.props.zoomLocked };
    this.mc.elementSelected = this.elementSelected.bind(this);
    this.mc.getSelectedTool = this.getSelectedTool.bind(this);

    this.tools.push(new ToolVo(ToolNames.settingsTool, 'fas fa-info-circle fa-lg'));
    this.tools.push(new ToolVo(ToolNames.imageTool, 'fas fa-image'));
    this.tools.push(new ToolVo(ToolNames.textTool, 'fas fa-font'));
    this.tools.push(new ToolVo(ToolNames.cardTool, 'far fa-address-card'));
    this.tools.push(new ToolVo(ToolNames.lineTool, 'fas fa-horizontal-rule'));
    this.tools.push(new ToolVo(ToolNames.shapeTool, 'fas fa-star'));
    this.tools.push(new ToolVo(ToolNames.zoomTool, 'fas fa-search-plus'));

  }


  componentDidUpdate(oldProps: MyProps) {
    if (oldProps !== this.props) {
      this.setState({ zoomLocked: this.props.zoomLocked });
      if (this.props.zoomLocked === false) {
        // this.setState({ selectedTool: 'zoomTool' });
        this.autoSwapTool(ToolNames.zoomTool);
      }

    }
  }
  /*  public getToolIndex(toolName: ToolNames) {
     return this.toolNames.indexOf(toolName) || -1;
   }
   public getToolByIndex(index: number) {
     return this.toolNames[index] || '';
   } */
  public autoSwapTool(toolName: ToolNames) {
    // if (this.toolNames.indexOf(toolName) !== -1) {
    this.setState({ selectedTool: toolName }, this.getToolProperties);
    // }
  }

  private getToolProperties() {


    switch (this.state.selectedTool) {
      case ToolNames.settingsTool:
        return (<BGSettingsPanel></BGSettingsPanel>);
      case ToolNames.imageTool:
        return (<ImagePropsPanel></ImagePropsPanel>);
      case ToolNames.textTool:
        return <div>
          <button className="btn btn-success btn-sm" style={{ width: '100%', margin: '10px 0px 0px 0px', display: 'block' }} onClick={() => this.mc.addNewElement(1)}>Add New Text</button>
          <div>{this.mc.getToolProps('textPropControls')}</div>
        </div>;
      case ToolNames.cardTool:
        return <div><button className="btn btn-success btn-sm" style={{ margin: '10px 0', display: 'block' }} onClick={() => this.mc.addNewElement(5)}>Add New Card</button>
          <div>{this.mc.getToolProps('Card Tool Properties')}</div>
        </div>;
      case ToolNames.lineTool:
        return <div>
          <button className="btn btn-success btn-sm" style={{ margin: '10px 0', display: 'block' }} onClick={() => this.mc.addNewElement(6)}>Add New Line</button>
          <div>{this.mc.getToolProps('Line Tool Properties')}</div>
        </div>;
      case ToolNames.shapeTool:
        return <div>
          <button className="btn btn-success btn-sm" style={{ margin: '10px 0', display: 'block' }} onClick={() => this.mc.addNewElement(7)}>Add New Shape</button>
          {this.mc.getToolProps('Shape Tool Properties')}
        </div>;
      case ToolNames.zoomTool:
        return <ZoomPropPanel></ZoomPropPanel>
    }
  }
  private handleToolSelection(event: React.MouseEvent<HTMLButtonElement, MouseEvent>, buttonName: string) {
    this.setState({ selectedTool: buttonName }, this.getToolProperties);
  }

  //Needed for switching tools when an element is added or selected on stage
  private elementSelected(selectedItem: string) {
    switch (selectedItem) {
      case 'IMAGE':
        this.autoSwapTool(ToolNames.imageTool);
        break;
      case 'TEXT':
        this.autoSwapTool(ToolNames.textTool);
        break;
      case 'CARD':
        this.autoSwapTool(ToolNames.cardTool);
        break;
      case 'LINE':
        this.autoSwapTool(ToolNames.lineTool);
        break;
      case 'SHAPE':
        this.autoSwapTool(ToolNames.shapeTool)
        break;
      default:
        this.autoSwapTool(ToolNames.settingsTool);
        break;
    }
  }
  private getSelectedTool() {
    return this.state.selectedTool;
  }
  getToolButtons() {
    let buttons: any[] = [];
    for (let i = 0; i < this.tools.length; i++) {
      let classNames: string[] = ['toolButton'];
      if (this.tools[i].toolName === this.state.selectedTool) {
        classNames.push('selectedTool');
      }
      let key: string = this.tools[i].toolName + 'Button';
      buttons.push(<button key={key} name={this.tools[i].toolName} className={classNames.join(" ")} onClick={event => this.handleToolSelection(event, this.tools[i].toolName)}><i className={this.tools[i].icon}></i></button>);
    }
    return buttons;
  }
  render() {
    return (<div className="toolContainer">
      <div className="toolButtonRack">
        {this.getToolButtons()}
      </div>
      <div className="toolProperties">{this.getToolProperties()}</div>
    </div>)
  }
}
export default ToolBar;