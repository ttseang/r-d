import React, { Component } from 'react';
import MainStorage from '../../classes/MainStorage';
import { MainController } from '../../classes/MainController';
import { ZoomVo } from '../../classes/dataObjs/ZoomVo';
import { Button } from 'react-bootstrap';
interface MyProps { }
interface MyState { x1: number, y1: number, x2: number, y2: number, pageW: number, pageH: number, pageLeft: number, pageTop: number, lockAspect: boolean }
class ZoomPropPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { x1: 0, y1: 0, x2: 100, y2: 100, pageW: this.ms.pageW, pageH: this.ms.pageH, pageLeft: this.ms.pageLeft, pageTop: this.ms.pageTop, lockAspect: false };
        this.mc.updateZoomProps = this.updateZoomProps.bind(this);
    }
    /* componentDidMount() {
        
    } */
    updateZoomProps(zoomVo: ZoomVo) {
        //trim the values to 2 decimal places
        let x1: number = Math.floor(zoomVo.x1 * 100) / 100;
        let y1: number = Math.floor(zoomVo.y1 * 100) / 100;
        let x2: number = Math.floor(zoomVo.x2 * 100) / 100;
        let y2: number = Math.floor(zoomVo.y2 * 100) / 100;

        this.setState({ x1: x1, y1: y1, x2: x2, y2: y2 });
    }
    updateStageSize() {
        this.ms.pageW = this.state.pageW;
        this.ms.pageH = this.state.pageH;
        //set the css variables
        document.documentElement.style.setProperty('--stageHeight', this.ms.pageH.toString() + "px");
        document.documentElement.style.setProperty('--stageWidth', this.ms.pageW.toString() + "px");
        document.documentElement.style.setProperty('--stageLeft', this.state.pageLeft.toString());
        document.documentElement.style.setProperty('--stageTop', this.state.pageTop.toString());
        this.mc.stageSizeUpdated();
        this.ms.saveStageToLocalStorage(this.state.pageLeft, this.state.pageTop, this.state.pageW, this.state.pageH)
    }
    private handleZoomChange(event: React.ChangeEvent<HTMLInputElement>) {
        const target: HTMLInputElement = event.currentTarget;

        switch (target.name) {
            case 'zoomX':
                this.setState({ x1: parseInt(target.value) }, this.updateZoom.bind(this));
                break;
            case 'zoomY':
                this.setState({ y1: parseInt(target.value) }, this.updateZoom.bind(this));
                break;
            case 'zoomX2':
                this.setState({ x2: parseInt(target.value) }, this.updateZoom.bind(this));
                break;
            case 'zoomY2':
                this.setState({ y2: parseInt(target.value) }, this.updateZoom.bind(this));
                break;
        }

    }
    private handleStagePropsChange(event: React.ChangeEvent<HTMLInputElement>) {
        const target: HTMLInputElement = event.currentTarget;

        switch (target.name) {
            case 'pageW':
                let origW: number = this.state.pageW;
                let origH: number = this.state.pageH;
                let newW: number = parseInt(target.value);
                let newH: number = origH;
                if (this.state.lockAspect) {
                    //calculate the new height
                    newH = (origH / origW) * newW;
                    //round to 2 decimal places
                    newH = Math.floor(newH * 100) / 100;
                }
                if (isNaN(newH)) {
                    newH = origH;
                }
                this.setState({ pageW: parseInt(target.value), pageH: newH }, this.updateStageSize.bind(this));
                break;
            case 'pageH':
                let origW2: number = this.state.pageW;
                let origH2: number = this.state.pageH;
                let newW2: number = origW2;
                let newH2: number = parseInt(target.value);
                if (this.state.lockAspect) {
                    //calculate the new height
                    newW2 = (origW2 / origH2) * newH2;
                    //round to 2 decimal places
                    newW2 = Math.floor(newW2 * 100) / 100;

                    //newW2=Math.floor(newW2);
                }
                if (isNaN(newW2)) {
                    newW2 = origW2;
                }
                this.setState({ pageH: parseInt(target.value), pageW: newW2 }, this.updateStageSize.bind(this));
                break;
            case 'pageLeft':
                this.setState({ pageLeft: parseInt(target.value) }, this.updateStageSize.bind(this));
                break;
            case 'pageTop':
                this.setState({ pageTop: parseInt(target.value) }, this.updateStageSize.bind(this));
                break;
        }

    }
    private moveUp() {
        let y1: number = this.state.y1;
        let y2: number = this.state.y2;

        y1--;
        y2--;
        this.setState({ y1: y1, y2: y2 }, this.updateZoom.bind(this));
    }
    private moveDown() {
        let y1: number = this.state.y1;
        let y2: number = this.state.y2;

        y1++;
        y2++;
        this.setState({ y1: y1, y2: y2 }, this.updateZoom.bind(this));
    }
    private moveLeft() {
        let x1: number = this.state.x1;
        let x2: number = this.state.x2;

        x1--;
        x2--;
        this.setState({ x1: x1, x2: x2 }, this.updateZoom.bind(this));
    }
    private moveRight() {
        let x1: number = this.state.x1;
        let x2: number = this.state.x2;

        x1++;
        x2++;
        this.setState({ x1: x1, x2: x2 }, this.updateZoom.bind(this));
    }
    updateZoom() {
        const zoomVo: ZoomVo = new ZoomVo(this.state.x1, this.state.y1, this.state.x2, this.state.y2);
        //console.log('zoomVo', zoomVo);
        this.mc.updateZoomControls(zoomVo);
        this.mc.updateZoomPreview(zoomVo);
    }
    setLockAspect(e: React.ChangeEvent<HTMLInputElement>) {
        const target: HTMLInputElement = e.currentTarget;
        console.log('target', target.checked);
        this.setState({ lockAspect: target.checked });
    }
    getStagePropsInput() {
        return (<div className='zoomProps'>
            <div className='zoomPropsCol'>
                <label htmlFor='pageW'>W:</label>
                <input type='number' id='pageW' name='pageW' min='0' max='2000' step='1' value={this.state.pageW} onChange={this.handleStagePropsChange.bind(this)} />
                <label htmlFor='pageH'>H:</label>
                <input type='number' id='pageH' name='pageH' min='0' max='2000' step='1' value={this.state.pageH} onChange={this.handleStagePropsChange.bind(this)} />
            </div>
            <br />
            <div className='zoomPropsCol'>
                <label htmlFor='pageLeft'>X:</label>
                <input type='number' id='pageLeft' name='pageLeft' min='0' max='200' step='1' value={this.state.pageLeft} onChange={this.handleStagePropsChange.bind(this)} />
                <label htmlFor='pageTop'>Y:</label>
                <input type='number' id='pageTop' name='pageTop' min='0' max='200' step='1' value={this.state.pageTop} onChange={this.handleStagePropsChange.bind(this)} />
            </div>
            <div className='zoomRow'>
                <div className='zoomPropsCol'>
                    <label htmlFor='lockAspect'><i className='fa fa-lock'></i></label>
                    <input type='checkbox' className='lockAspect' name='lockAspect' onChange={this.setLockAspect.bind(this)} checked={this.state.lockAspect} />
                </div>
                <div className='zoomPropsCol'>
                    <Button onClick={this.resetSize.bind(this)}>Reset</Button>
                </div>
            </div>
        </div>);

    }
    resetSize() {
        this.setState({ pageW: 480, pageH: 270, pageLeft: 33, pageTop: 27 }, this.updateStageSize.bind(this));

    }

    render() {
        //x y w h input boxes
        return (<div>
            <h6 className='tac'>Zoom Size & Position</h6>
            <div className='zoomProps'>
                <div className='zoomPropsCol'>
                    <div>
                        <label htmlFor='zoomX'>X1</label>
                        <input type='number' id='zoomX' name='zoomX' min='0' max='100' step='1' value={this.state.x1} onChange={this.handleZoomChange.bind(this)} />
                    </div>
                    <div>
                        <label htmlFor='zoomY'>Y1</label>
                        <input type='number' id='zoomY' name='zoomY' min='0' max='100' step='1' value={this.state.y1} onChange={this.handleZoomChange.bind(this)} />
                    </div>
                </div>
                <div className='zoomPropsCol'><div>
                    <label htmlFor='zoomX2'>X2</label>
                    <input type='number' id='zoomX2' name='zoomX2' min='0' max='100' step='1' value={this.state.x2} onChange={this.handleZoomChange.bind(this)} />
                    <label htmlFor='zoomY2'>Y2</label>
                    <input type='number' id='zoomY2' name='zoomY2' min='0' max='100' step='1' value={this.state.y2} onChange={this.handleZoomChange.bind(this)} />
                </div>
                </div>
            </div>
            <hr />
            <h6 className='tac'>Move Zoom</h6>
            <div className="arrowContainer">
                <div></div>
                <div className="button" onClick={this.moveUp.bind(this)}><span className="arrow">&#8593;</span></div>
                <div></div>
                <div className="button" onClick={this.moveLeft.bind(this)}><span className="arrow">&#8592;</span></div>
                <div></div>

                <div className="button" onClick={this.moveRight.bind(this)}><span className="arrow">&#8594;</span></div>
                <div></div>

                <div className="button" onClick={this.moveDown.bind(this)}><span className="arrow">&#8595;</span></div>
            </div>
            <hr />
            <h6 className='tac'>Stage Properties</h6>
            {this.getStagePropsInput()}
        </div>

        )
    }
}
export default ZoomPropPanel;