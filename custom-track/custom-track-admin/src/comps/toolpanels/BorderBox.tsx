import React, { ChangeEvent, Component } from 'react';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../../classes/dataObjs/ExtrasVo';
import { ImgBorderStyleVo } from '../../classes/dataObjs/ImgBorderStyleVo';
import ColorPicker from '../../classes/ui/ColorPicker';
import ValInput from './ValInput';
import MainStorage from '../../classes/MainStorage';
interface MyProps {elementVo:ElementVo,callback:Function}
interface MyState {elementVo:ElementVo}
class BorderBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {elementVo:this.props.elementVo};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({elementVo:this.props.elementVo});
        }
    }
    colorCallback(color:string)
    {
        ////console.log(color);
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.borderColor=color;
        this.props.callback(extras);
    }
    sizeCallback(val:number)
    {
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.borderThick=val;
        this.props.callback(extras);
    }

    radiusCallback(val:number)
    {
        let extras:ExtrasVo=this.state.elementVo.extras;
        extras.borderRadius=val;
        this.props.callback(extras);
    }

    borderStyleChanged(e:ChangeEvent<HTMLSelectElement>)
    {
        let extras:ExtrasVo=this.state.elementVo.extras;
        for(let i:number = 0; i <this.ms.styleUtil.imgBorders.length;i++){
            if(e.currentTarget.value === this.ms.styleUtil.imgBorders[i].label){
                extras.borderStyle=this.ms.styleUtil.imgBorders[i];
                break;
            } else {
                extras.borderStyle=this.ms.styleUtil.imgBorders[0];
            }
        }

        this.props.callback(extras);
    }

    getBorderStyleDropDown()
    {
        let borders:ImgBorderStyleVo[] = this.ms.styleUtil.imgBorders;
        let borderArray:JSX.Element[]=[];
        for(let i:number=0; i<borders.length; i++){
            let key:string="bs"+i.toString();
            borderArray.push(<option key={key}>{borders[i].label}</option>)
        }

        return (<select onChange={this.borderStyleChanged.bind(this)} value={this.state.elementVo.extras.borderStyle.label}>{borderArray}</select>)
    }

    render() {
        return (<div className='toolPanel' id={"borderPanel"} style={{display:'none'}}>
            <div>
            <label style={{fontSize:'14px', margin:'0px 10px 0 0'}}><strong>Border:</strong></label>
            <ColorPicker myColor={this.state.elementVo.extras.borderColor} callback={this.colorCallback.bind(this)}></ColorPicker></div>
            <div><ValInput val={this.state.elementVo.extras.borderThick} min={0} max={30} callback={this.sizeCallback.bind(this)} label="Size" multiline={false}></ValInput></div>
            <div><ValInput val={this.state.elementVo.extras.borderRadius} min={0} max={100} callback={this.radiusCallback.bind(this)} label="Corner" multiline={false}></ValInput></div>
            <div className='padLeft'>{this.getBorderStyleDropDown()}</div>
        </div>)
    }
}
export default BorderBox;