import React, { Component } from 'react';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { ShapeStyleVo } from '../../classes/dataObjs/ShapeStyleVo';
import MainStorage from '../../classes/MainStorage';
import ValBox from './ValBox';
interface MyProps { callback: Function,styleCallback:Function, elementVo: ElementVo }
interface MyState { elementVo: ElementVo, selectedShape: string, selectedStyle: ShapeStyleVo}
class ShapeBox extends Component<MyProps, MyState>
{
    private shapes: string[] = ["heart", "star", "circle", "square", "triangle", "stop","arrow"];
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        let shape: string = this.props.elementVo.content[1] || "heart";
        this.state = { selectedShape: shape, elementVo: this.props.elementVo, selectedStyle: this.props.elementVo.extras.shapeStyle};
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            let shape: string = this.props.elementVo.content[1] || "heart";
            this.setState({ selectedShape: shape, elementVo: this.props.elementVo, selectedStyle: this.props.elementVo.extras.shapeStyle});
          }
    }
    onStyleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState({ selectedStyle: this.ms.shapeStyles[e.target.selectedIndex] });
        this.props.styleCallback(this.ms.shapeStyles[e.target.selectedIndex]);
    }

    onChangeStroke = (value:string) => {
        this.props.elementVo.extras.shapeStyle.strokeColor = value;
        this.setState({selectedStyle: this.props.elementVo.extras.shapeStyle});
        this.props.styleCallback(this.state.selectedStyle);
    }

    onChangeFill = (value:string) => {
        this.props.elementVo.extras.shapeStyle.fillColor = value;
        this.setState({selectedStyle: this.props.elementVo.extras.shapeStyle});
        this.props.styleCallback(this.state.selectedStyle);
    }

    onShapeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState({ selectedShape: e.target.value });
        let shapeName: string = e.target.value;

        //clone the svg tag with the id of heart
        let svg: SVGSVGElement | null = document.getElementById(shapeName) as unknown as SVGSVGElement;

        if (svg != null) {

            //get the path child of the svg
            let path: SVGPathElement | null = svg.querySelector("path") as unknown as SVGPathElement;

            if (path != null) {
                //get the svg as a string
                let dString: string = path.getAttribute("d") as string;

                //console.log("shapeName=" + shapeName);
                //console.log("dString=" + dString);
                this.props.callback([dString, shapeName]);
            }
        }
    }

    //make shape dropdown
    getDropdown() {
        return (<select value={this.state.selectedShape} onChange={this.onShapeChange}>
            {this.shapes.map((shape) => <option key={shape} value={shape}>{shape}</option>)}
        </select>)
    } 
    getStyleDropdown() {
        return (<select value={this.state.selectedStyle.styleName} onChange={this.onStyleChange}>
            {this.ms.shapeStyles.map((style) => <option key={style.styleName} value={style.styleName}>{style.styleName}</option>)}
        </select>)
    }

    sizeCallback(val:number)
    {
        this.props.elementVo.extras.shapeStyle.strokeWidth = val;
        this.setState({selectedStyle: this.props.elementVo.extras.shapeStyle});
        this.props.styleCallback(this.state.selectedStyle);
    }

    getStyleControls(){
        return(
            <div>
            <label>Stroke:</label><br/>
            <input onChange={(e) => this.onChangeStroke(e.target.value)} type="color" id="strokeColor" name="strokeColor" value={this.state.elementVo.extras.shapeStyle.strokeColor}/><br />
            <label>Fill:</label><br/>
            <input onChange={(e) => this.onChangeFill(e.target.value)} type="color" id="fillColor" name="fillColor" value={this.state.elementVo.extras.shapeStyle.fillColor}/>
            <ValBox label="Stroke Size" min={0} max={10} val={this.state.elementVo.extras.shapeStyle.strokeWidth} callback={this.sizeCallback.bind(this)} multiline={true}></ValBox>
            </div>
        )
    }

    render() {
        return (<div>
            <hr className='toolPropDivide' />
            <div className='controlGrid'>
            {this.getDropdown()}<br />
            {this.getStyleControls()}
        </div>
        </div>)
    }
}
export default ShapeBox; 