import { ChangeEvent, Component } from 'react';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../../classes/dataObjs/ExtrasVo';
import { FontVo } from '../../classes/dataObjs/FontVo';
import { TextPathVo } from '../../classes/dataObjs/TextPathVo';

import MainStorage from '../../classes/MainStorage';
import ColorPicker from '../../classes/ui/ColorPicker';
import ValBox from './ValBox';
import { TextSizeVo } from '../../classes/dataObjs/TextSizeVo';
interface MyProps { elementVo: ElementVo, callback: Function }
interface MyState { elementVo: ElementVo }
class FontPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { elementVo: this.props.elementVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (this.props !== oldProps) {
            this.setState({ elementVo: this.props.elementVo });
        }
    }
    fontChanged(e: ChangeEvent<HTMLSelectElement>) {
        let extras: ExtrasVo = this.state.elementVo.extras;
        extras.fontName = e.currentTarget.value;
        this.props.callback(extras);
    }
    sizeSelected(e: ChangeEvent<HTMLSelectElement>) {
        let extras: ExtrasVo = this.state.elementVo.extras;
        for (let i: number = 0; i < this.ms.styleUtil.textSizes.length; i++) {
            if (e.currentTarget.value === this.ms.styleUtil.textSizes[i].name) {
                extras.textSizeID = this.ms.styleUtil.textSizes[i].id;
                extras.textSizeName = this.ms.styleUtil.textSizes[i].name;
                break;
            } else {
                extras.textSizeID = this.ms.styleUtil.textSizes[0].id;
                extras.textSizeName = this.ms.styleUtil.textSizes[0].name;
            }
        }
        this.props.callback(extras);
    }
    getFontDropDown() {
        let fonts: FontVo[] = this.ms.styleUtil.fonts;
        let ddArray: JSX.Element[] = [];
        for (let i: number = 0; i < fonts.length; i++) {
            let key: string = "fs" + i.toString();
            //  let selected:boolean=(fonts[i].fontName===this.state.elementVo.extras.fontName)?true:false;

            ddArray.push(<option key={key}>{fonts[i].label}</option>)
        }
        return (<select onChange={this.fontChanged.bind(this)} value={this.state.elementVo.extras.fontName}>{ddArray}</select>)
    }
    getSizesDropDown() {
        let sizes: TextSizeVo[] = this.ms.styleUtil.textSizes;
        let ddArray: JSX.Element[] = [];
        let selectedTextSize: TextSizeVo = this.ms.styleUtil.textSizes[0];
        for (let i: number = 0; i < sizes.length; i++) {
            let key: string = "fs" + i.toString();
            ddArray.push(<option key={key}>{sizes[i].name}</option>)
            if (sizes[i].id === this.state.elementVo.extras.textSizeID) {
                selectedTextSize = sizes[i];
            }
        }
        return (<select onChange={this.sizeSelected.bind(this)} value={selectedTextSize.name}>{ddArray}</select>)
    }
    pathSelected(e: ChangeEvent<HTMLSelectElement>) {
        let extras: ExtrasVo = this.state.elementVo.extras;
        for (let i: number = 0; i < this.ms.styleUtil.textPath.length; i++) {
            if (e.currentTarget.value === this.ms.styleUtil.textPath[i].label) {
                extras.textCurve = this.ms.styleUtil.textPath[i];
                break;
            } else {
                extras.textCurve = this.ms.styleUtil.textPath[0];
            }
        }
        this.props.callback(extras);
    }

    getPathDropDown() {
        let paths: TextPathVo[] = this.ms.styleUtil.textPath;
        let pathArray: JSX.Element[] = [];
        for (let i: number = 0; i < paths.length; i++) {
            let key: string = "fs" + i.toString();
            pathArray.push(<option key={key}>{paths[i].label}</option>)
        }
        return (<select onChange={this.pathSelected.bind(this)} value={this.state.elementVo.extras.textCurve.label}>{pathArray}</select>)
    }

    colorCallback(color: string) {
        // ////console.log(color);
        let extras: ExtrasVo = this.state.elementVo.extras;
        extras.fontColor = color;
        this.props.callback(extras);
    }
    sizeCallback(val: number) {
        let extras: ExtrasVo = this.state.elementVo.extras;
        extras.fontSize = val;
        this.props.callback(extras);
    }
    render() {
        return (<div>
            <hr className='toolPropDivide'></hr>
            <div className='tac'>{this.getFontDropDown()}</div>
            
            <div className='tac'><ColorPicker myColor={this.state.elementVo.extras.fontColor} callback={this.colorCallback.bind(this)}></ColorPicker></div>
            <div><ValBox label="Font Size" min={0} max={100} val={this.state.elementVo.extras.fontSize} callback={this.sizeCallback.bind(this)} multiline={true}></ValBox></div>
            <label>Path:</label>
            <div className='tac'>{this.getPathDropDown()}</div>
            <label>Box Size:</label>
            <div className='tac'>{this.getSizesDropDown()}</div>
        </div>)
    }
}
export default FontPanel;