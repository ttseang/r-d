 import React, { Component } from 'react';
import { ButtonConstants } from '../../classes/constants/ButtonConstants';

import ButtonLine from '../../classes/ui/ButtonLine';
interface MyProps { actionCallback: Function }
interface MyState { }
class FlipPanel extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    
    render() {
        return (
            <div className='toolPanel' id={"flipPanel"} style={{display:'none'}}>
            <ButtonLine buttonArray={ButtonConstants.FLIP_BUTTONS} actionCallback={this.props.actionCallback} useVert={false}></ButtonLine>
            </div>
        )
    }
}
export default FlipPanel;