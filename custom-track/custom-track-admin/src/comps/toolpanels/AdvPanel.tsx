import React, { Component } from 'react';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import ValInput from './ValInput';
interface MyProps { elementVo: ElementVo, callback: Function };
interface MyState { elementVo: ElementVo};
class AdvPanel extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { elementVo: this.props.elementVo}; 
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ elementVo: this.props.elementVo });
        }
    }
    onAlphaChange(alpha: number) {
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.alpha = alpha;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }
    onRotationChange(rot: number) {
        // ////console.log(rot);
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.rotation = rot;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }
    onSkewYChange(skew: number) {
        // ////console.log(rot);
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.skewY = skew;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }
    onSkewXChange(skew: number) {
        // ////console.log(rot);
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.skewX = skew;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }

    //Inital Testing to add Gradient Fade for images
    onFadeChange(fade: number) {
        let elementVo: ElementVo = this.props.elementVo;
        elementVo.extras.fade = fade;
        this.setState({ elementVo: elementVo });
        this.props.callback(elementVo.extras);
    }
    

    render() {
        if(!ElementVo.TYPE_SHAPE){
        return (<div>
                <hr className='toolPropDivide'></hr>
                <label style={{fontSize:'14px'}}><strong>Advanced:</strong></label>
                <div className='padLeft'><ValInput val={this.state.elementVo.extras.alpha} min={0} max={100} callback={this.onAlphaChange.bind(this)} label="Alpha" multiline={false}></ValInput>
                    <ValInput val={this.state.elementVo.extras.rotation} min={-100} max={100} callback={this.onRotationChange.bind(this)} label="Angle" multiline={false}></ValInput>
                </div>
                <div className='padLeft'>
                    <label style={{fontSize:'14px'}}>Skew:</label><br/>    
                    <ValInput val={this.state.elementVo.extras.skewX} min={-100} max={100} callback={this.onSkewXChange.bind(this)} label="X" multiline={false}></ValInput>
                    <ValInput val={this.state.elementVo.extras.skewY} min={-100} max={100} callback={this.onSkewYChange.bind(this)} label="Y" multiline={false}></ValInput>
                </div>
                <div className='padLeft'>
                <label style={{fontSize:'14px'}}>Fade:</label><br/>
                    <ValInput val={this.state.elementVo.extras.fade} min={0} max={100} callback={this.onFadeChange.bind(this)} label="Fade" multiline={false}></ValInput>
                </div>    
        </div>)
        } else {
            return (
                <div className='toolPanel' id={"advancedPanel"} style={{display:'none'}}>
                <div className='padLeft'><ValInput val={this.state.elementVo.extras.alpha} min={0} max={100} callback={this.onAlphaChange.bind(this)} label="Alpha" multiline={false}></ValInput>
                    <ValInput val={this.state.elementVo.extras.rotation} min={-100} max={100} callback={this.onRotationChange.bind(this)} label="Angle" multiline={false}></ValInput>
                </div>
                <div className='padLeft'>
                    <label style={{fontSize:'14px'}}>Skew:</label><br/>    
                    <ValInput val={this.state.elementVo.extras.skewX} min={-100} max={100} callback={this.onSkewXChange.bind(this)} label="X" multiline={false}></ValInput>
                    <ValInput val={this.state.elementVo.extras.skewY} min={-100} max={100} callback={this.onSkewYChange.bind(this)} label="Y" multiline={false}></ValInput>
                </div>
                </div>
            )
        }
    }
}
export default AdvPanel;