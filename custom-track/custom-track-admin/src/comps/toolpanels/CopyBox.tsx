import React, { Component } from 'react';
import { ButtonConstants } from '../../classes/constants/ButtonConstants';
import ButtonIconLine from '../../classes/ui/ButtonIconLine';

interface MyProps {actionCallback:Function}
interface MyState { }
class CopyBox extends Component<MyProps, MyState>
{
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    render() {
            return (<div><ButtonIconLine buttonArray={ButtonConstants.ICON_COPY_BUTTONS} 
                actionCallback={this.props.actionCallback} useVert={false} useText={false}></ButtonIconLine></div>)

    }
}
export default CopyBox;

