import { Component } from 'react';
import { Card } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import MainStorage from '../../classes/MainStorage';
import ValBox from './ValBox';
import ValInput from './ValInput';
interface MyProps {elementVo:ElementVo,callback:Function}
interface MyState {elementVo:ElementVo}
class PosBox extends Component<MyProps, MyState>
{
    private ms:MainStorage=MainStorage.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {elementVo:this.props.elementVo};
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({elementVo:this.props.elementVo});
        }
    }
    onXChange(xx:number)
    {
        let elementVo:ElementVo=this.props.elementVo;
        elementVo.x=xx;
        this.setState({elementVo:elementVo});
        this.props.callback(elementVo.x,elementVo.y,elementVo.w,elementVo.h);
       
    }
    onYChange(yy:number)
    {
        let elementVo:ElementVo=this.props.elementVo;
        elementVo.y=yy;
        this.setState({elementVo:elementVo});
        this.props.callback(elementVo.x,elementVo.y,elementVo.w,elementVo.h);
    }
    onWChange(ww:number)
    {
        let elementVo:ElementVo=this.props.elementVo;
        elementVo.w=ww;
        this.setState({elementVo:elementVo});
        this.props.callback(elementVo.x,elementVo.y,elementVo.w,elementVo.h);
    }
    render() {
        return (<div>
            <hr className='toolPropDivide'></hr>
                    <div><ValInput val={this.state.elementVo.x} min={-100} max={100} callback={this.onXChange.bind(this)} label="X" multiline={false}></ValInput>
                    <ValInput val={this.state.elementVo.y} min={-100} max={100} callback={this.onYChange.bind(this)} label="Y" multiline={false}></ValInput></div>
                    <ValInput val={this.state.elementVo.w} min={0} max={200} callback={this.onWChange.bind(this)} label="W" multiline={false}></ValInput>
        </div>)
    }
}
export default PosBox;