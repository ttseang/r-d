import React, { ChangeEvent, Component } from 'react';
import { Row, Col } from 'react-bootstrap';
interface MyProps {label:string,min:number,max:number, val: number,callback:Function,multiline:boolean }
interface MyState { val: number }

class ValInput extends Component<MyProps, MyState>
{
    public static defaultProps={min:0,max:100};
    
        constructor(props: MyProps) {
            super(props);
            this.state = { val: this.props.val };
        }
    componentDidUpdate(oldProps:MyProps)
    {
        if (oldProps!==this.props)
        {
            this.setState({val:this.props.val});
        }
    }
    onChange(e:ChangeEvent<HTMLInputElement>)
    {
        let val:number=parseFloat(e.currentTarget.value);
        if (isNaN(val))
        {
            val=0;
        }
       // this.setState({val:val});
        this.props.callback(val);
    }

    getInputBox() {
        return (<input className='valueInput' type='number' min={this.props.min} max={this.props.max} value={this.state.val} onChange={this.onChange.bind(this)}></input>)
    }
    render() {
        return ( 
           <span><label style={{fontSize:'12px', padding:'0px 4px 10px'}}>{this.props.label + ': '}</label>{this.getInputBox()}</span>
        )
    }
}
export default ValInput;