import { useEffect, useState} from 'react';
import { MainController } from '../../classes/MainController';
import MainStorage from '../../classes/MainStorage';

export default function BGSettingsPanel(this:any) {
    const ms:MainStorage=MainStorage.getInstance();
    const mc:MainController=MainController.getInstance();
    const [bgColor, setbgColor] = useState(ms.bgColor);
    const stage:HTMLElement|null = document.getElementById('stage');

    useEffect(() => {
            changeStageColor();
            saveColorChange();
    });

    const saveColorChange = () =>
    {
        ms.bgColor = bgColor;
    }

    const changeStageColor = () =>
    {
        if(stage){
           stage.style.backgroundColor = bgColor;
        }
    }

    return(
        <div className='padLeft'>
            <label>Stage Color:</label><br></br>
            <input onChange={(e) => setbgColor(e.target.value)} type="color" id="bgColor" name="bgColor" value={bgColor}/> 
            {mc.getToolProps('audioControls')}
        </div>
    );    
}