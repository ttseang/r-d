import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import MainStorage from '../../classes/MainStorage';
import { PropActions } from '../../screens/PageEditScreen';

interface MyProps { elementVo: ElementVo, callback: Function }
interface MyState { elementVo: ElementVo }
class ContentPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { elementVo: this.props.elementVo };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ elementVo: this.props.elementVo });
        }
    }
    getContent() {
        if (this.state.elementVo.type === ElementVo.TYPE_TEXT) {
            let text: string = this.state.elementVo.content[0].substring(0, 30);
            if (this.state.elementVo.content.length > 30) {
                text += "...";
            }
            return text;
        }
        if (this.state.elementVo.type === ElementVo.TYPE_IMAGE || this.state.elementVo.type === ElementVo.TYPE_BACK_IMAGE) {
            let image: string = this.ms.getImagePath(this.state.elementVo.content[0], 1);
            return (<img src={image} alt='book' className='contentThumb' />)
        }
    }
    render() {
        return (
            <div >
                <hr className='toolPropDivide'></hr>
                <div className='elementPreview'>{this.getContent()}
                    <Button size="sm" onClick={() => { this.props.callback(PropActions.EDIT) }}>Change</Button>
                </div>
            </div>
        )
    }
}
export default ContentPanel;