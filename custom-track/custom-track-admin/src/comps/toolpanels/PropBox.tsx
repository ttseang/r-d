import { Component } from 'react';
import { Button, Tab, Tabs } from 'react-bootstrap';
import { ButtonConstants } from '../../classes/constants/ButtonConstants';

import { ButtonVo } from '../../classes/dataObjs/ButtonVo';
import { ElementVo } from '../../classes/dataObjs/ElementVo';
import { StepVo } from '../../classes/dataObjs/StepVo';
import { MainController } from '../../classes/MainController';

import MainStorage from '../../classes/MainStorage';
import StyleGrid from '../../classes/ui/StyleGrid';
import FramePropBox from '../frame/FramePropBox';
import AdvPanel from './AdvPanel';
import BgPosBox from './BgPosBox';
import BorderBox from './BorderBox';
import ContentPanel from './ContentPanel';
import CopyBox from './CopyBox';
import FlipPanel from './FlipPanel';
import FontPanel from './FontPanel';
import InstanceBox from './InstanceBox';
import LinePosBox from './LinePosBox';
import LineStylebox from './LineStyleBox';
import PopUpSelect from './PopUpSelect';
import PosBox from './PosBox';
import PosBox2 from './PosBox2';
import ShapeBox from './ShapeBox';
import TextStylePanel from './TextStylePanel';
interface MyProps {id:string, elementVo: ElementVo | null, step: StepVo | null, callback: Function, actionCallback: Function, updateContent: Function, updateExtras: Function, updateBackgroundProps: Function, updateLine: Function, updateInstance: Function, updateShapeStyle: Function }
interface MyState { elementVo: ElementVo | null, step: StepVo | null, panelIndex: number }
class PropBox extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { elementVo: this.props.elementVo, step: this.props.step, panelIndex: 0 };
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ elementVo: this.props.elementVo, step: this.props.step });
        }
    }

    getPanel(index: number): JSX.Element {
        if (this.state.elementVo === null) {
            return (<div>Loading</div>);
        }
        // ////console.log("panelIndex=" + this.state.panelIndex);

        switch (index) {
            case 0:
                return <ContentPanel key="contentPanel" elementVo={this.state.elementVo} callback={this.props.actionCallback}></ContentPanel>

            case 1:

                return this.getPropBox();

            case 2:
                return (<div key={'panel' + index}>
                    <button className="accordion" onClick={(event) => this.propPanelToggle('align')}>Alignment</button>
                    <StyleGrid key="alignGrid" gridItems={this.ms.styleUtil.aligns} callback={this.props.actionCallback}></StyleGrid>
                    </div>
                )

            case 3:
                return (<div key={'panel' + index}>
                    <button className="accordion" onClick={(event) => this.propPanelToggle('flip')}>Flip</button>
                    <FlipPanel key="flipPanel" actionCallback={this.props.actionCallback}></FlipPanel>
                    </div>
                )

            case 4:
                return (<div key={'panel' + index}>
                    <button className="accordion" onClick={(event) => this.propPanelToggle('orientation')}>Orientation</button>
                    <StyleGrid key="orgrid" gridItems={this.ms.styleUtil.orientations} callback={this.props.actionCallback}></StyleGrid>
                    </div>)

            case 5:
                return (<div key={'panel' + index}>
                    <button className="accordion" onClick={(event) => this.propPanelToggle('border')}>Border</button>
                    <BorderBox key="borderBox" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></BorderBox>
                    </div>
                )

            case 6:
                return (<div key={'panel' + index}>
                    <button className="accordion" onClick={(event) => this.propPanelToggle('advanced')}>Advanced</button>
                    <AdvPanel key="advancedPanel" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></AdvPanel>
                    </div>
                )

            case 7:
                return <FontPanel key="fontPanel" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></FontPanel>
            case 8:
                return (<BgPosBox key="backgroundPosBox" elementVo={this.state.elementVo} callback={this.props.updateBackgroundProps.bind(this)}></BgPosBox>);

            case 14:
                return (<PopUpSelect key="popupselect" element={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></PopUpSelect>)

            case 15:
                return (<LinePosBox key="lineposbox" elementVo={this.state.elementVo} callback={this.props.updateLine}></LinePosBox>)

            case 16:
                return (<LineStylebox key="linestylebox" elementVo={this.state.elementVo} callback={this.props.updateExtras.bind(this)}></LineStylebox>)

            //instance box
            case 17:
                return <InstanceBox key="instanceBox" element={this.state.elementVo} callback={this.props.updateInstance}></InstanceBox>

            case 19:
                return <TextStylePanel key="textStylePanel"></TextStylePanel>

            case 20:
                return this.getPropBox2();

            case 21:
                return this.getShapeBox();
        }
        return (<div key="no panel">Panel Here</div>);
    }
    doFrameActions() {

    }

    propPanelToggle(panelName:string){
        let panel:HTMLElement|null = document.getElementById(panelName+"Panel");
        if(panel !== null){
            panel.style.display = panel.style.display === 'none' ? 'block' : 'none';
        }
    }

    getButtonData(): ButtonVo[] {
        let buttonData: ButtonVo[] = ButtonConstants.PANEL_BUTTONS;
        if (this.state.elementVo === null) {
            return buttonData;
        }
        if (this.state.elementVo.type === ElementVo.TYPE_TEXT) {
            buttonData = ButtonConstants.TEXT_PANEL_BUTTONS;
        }
        /*  if (this.state.elementVo.subType===ElementVo.SUB_TYPE_GUTTER)
         {
             buttonData=ButtonConstants.BGIMG_PANEL_BUTTONS;
         } */
        if (this.state.elementVo.type === ElementVo.TYPE_CARD) {
            buttonData = ButtonConstants.CARD_PANEL_BUTTONS;
        }
        if (this.state.elementVo.subType === ElementVo.SUB_TYPE_POPUP_LINK) {
            buttonData = ButtonConstants.POPUP_PANEL_BUTTONS;
        }
        if (this.state.elementVo.type === ElementVo.TYPE_LINE) {
            buttonData = ButtonConstants.LINE_PANEL_BUTTONS;
        }
        if (this.state.elementVo.type === ElementVo.TYPE_SHAPE) {
            buttonData = ButtonConstants.SHAPE_PANEL_BUTTONS;
        }
        return buttonData;
    }

    getButtons() {
        if (this.state.elementVo === null) {
            // return "Select an Element";
            return <FramePropBox stepVo={this.state.step} callback={() => { }} actionCallback={this.doFrameActions.bind(this)}></FramePropBox>
        }
        let buttonData: ButtonVo[] = this.getButtonData();


        let buttons: JSX.Element[] = [];

        for (let i: number = 0; i < buttonData.length; i++) {
            let key: string = "propButton" + i.toString();

            buttons.push(<Button key={key} variant={buttonData[i].variant} className="fw" onClick={() => { this.changePanel(buttonData[i].action) }}>{buttonData[i].text}</Button>)
            if (buttonData[i].action === this.state.panelIndex) {
                buttons.push(this.getPanel(this.state.panelIndex));
            }
        }
        return buttons;
    }
    getTabs() {
        if (this.state.elementVo === null) {
            // return "Select an Element";
            return <FramePropBox stepVo={this.state.step} callback={() => { }} actionCallback={this.doFrameActions.bind(this)}></FramePropBox>
        }
        let buttonData: ButtonVo[] = this.getButtonData();
        let tabs: JSX.Element[] = [];
        for (let i: number = 0; i < buttonData.length; i++) {
            let key: string = "propButton" + i.toString();
            let tab: JSX.Element = <Tab key={key} eventKey={buttonData[i].action} title={buttonData[i].text}>{this.getPanel(buttonData[i].action)}</Tab>
            tabs.push(tab);
        }
        return <div className="propTabs">
            <Tabs defaultActiveKey={buttonData[0].action}>
                {tabs}
            </Tabs>
        </div>

    }

    getProperties(){
        if (this.state.elementVo === null) {
            if(this.mc.getSelectedTool() === "settingsTool"){
                return <FramePropBox stepVo={this.state.step} callback={() => { }} actionCallback={this.doFrameActions.bind(this)}></FramePropBox> 
            }
             else {
                return "Select an Element";
             }
            }
            
        let buttonData: ButtonVo[] = this.getButtonData();

       let panels:JSX.Element[] = [];
       
       for(let i:number = 0; i < buttonData.length; i++){
            //let key: string = "propPanel" + i.toString();
            let panel: JSX.Element = this.getPanel(buttonData[i].action);
            panels.push(panel);
       }
       return <div id='propertyPanels'>
        {panels}
       </div>
    }

    changePanel(panelIndex: number) {
        if (this.state.panelIndex === panelIndex) {
            panelIndex = -1;
        }
        this.setState({ panelIndex: panelIndex })
    }
    getContent() {
        if (this.state.elementVo) {
            return this.state.elementVo.content + " " + this.state.elementVo.type;
        }
        return "";
    }
    getPropBox() {
        if (this.state.elementVo) {
            return (<PosBox key="posbox" elementVo={this.state.elementVo} callback={this.props.callback}></PosBox>)
        }
        return (<div>Loading</div>);
    }
    /**
     * 
     * Includes height box
     */
    getPropBox2() {
        if (this.state.elementVo) {
            return (<PosBox2 key="posbox2" elementVo={this.state.elementVo} callback={this.props.callback}></PosBox2>)
        }
        return (<div>Loading</div>);
    }
    getCopyBox() {
        if (this.state.elementVo) {
            return (<CopyBox key="copybox" actionCallback={this.props.actionCallback}></CopyBox>)
        }
        return "";
    }
    getShapeBox() {
        if (this.state.elementVo) {
            return (<ShapeBox key="shapebox" elementVo={this.state.elementVo} callback={this.props.updateContent} styleCallback={this.props.updateShapeStyle}></ShapeBox>)
        }
        return <div></div>
    }

    render() {

        return (
        <div id={this.props.id} className='toolPropPanel'>
            {this.getCopyBox()}

            {this.getProperties()}

        </div>)
    }
}
export default PropBox;