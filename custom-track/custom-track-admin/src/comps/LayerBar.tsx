import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import DragList from './dragListComps/DragList';
import ElementRow from './ElementRow';
import { MainController } from '../classes/MainController';
interface MyProps { selectElement: Function, elements: ElementVo[], selectedElement: ElementVo | null, toggleVis: Function, toggleLock: Function, updateElementRows: Function }
interface MyState { elements: ElementVo[], selectedElement: ElementVo | null, x: number, y: number ,showMe:boolean}
class LayerBar extends Component<MyProps, MyState>
{
    private dragFunction: any;
    private startDragFunction: any = this.startDrag.bind(this);
    private stopDragFunction: any = this.stopDrag.bind(this);
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { elements: this.props.elements, selectedElement: this.props.selectedElement, x: 300, y: 100 ,showMe:false};
        this.dragFunction = this.onDrag.bind(this);

        this.mc.showLayers=this.showLayers.bind(this);
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ elements: this.props.elements, selectedElement: this.props.selectedElement });
        }
    }
    showLayers()
    {
        this.setState({showMe:true});
    }
    getElementRows() {

        // let allElements: ElementVo[] = (this.state.showLeft === true) ? this.state.pages.left.elements : this.state.pages.right.elements;
        let allElements: ElementVo[] = this.state.elements;
        let editRows: JSX.Element[] = [];

        let usedLinks: string[] = [];

        for (let j: number = 0; j < allElements.length; j++) {
            let key: string = "elvo" + j.toString();
            let selected: boolean = false;
            if (this.state.selectedElement) {
                if (this.state.selectedElement.id === allElements[j].id) {
                    selected = true;
                }
            }
            let skip: boolean = false;

            if (allElements[j].linkID !== "" && usedLinks.includes(allElements[j].linkID)) {
                skip = true;
            }

            if (skip === false) {
                if (selected === true) {
                    editRows.push(<div key={key}><ElementRow callback={this.props.selectElement} selected={true} index={j} elementVo={allElements[j]} toggleVis={this.props.toggleVis} toggleLock={this.props.toggleLock}></ElementRow></div>)
                }
                else {
                    editRows.push(<div key={key}><ElementRow callback={this.props.selectElement} selected={false} index={j} elementVo={allElements[j]} toggleVis={this.props.toggleVis} toggleLock={this.props.toggleLock}></ElementRow></div>)
                }
                if (allElements[j].linkID !== "") {
                    usedLinks.push(allElements[j].linkID);
                }
            }

        }
        // //////console.log(this.selectedRef);

        return (
            <div>
                <Card className='rsidePanel'>
                    <div className='sideScroll'>
                        <div id="listHolder"><DragList itemHeight={60} onUpdate={this.props.updateElementRows}>{editRows}</DragList></div>
                    </div>
                </Card>
            </div>);
    }
    startDrag(e: React.MouseEvent) {
        window.addEventListener("mousemove", this.dragFunction);
        window.addEventListener("mouseup", this.stopDragFunction)
    }
    stopDrag(e: MouseEvent) {
        window.removeEventListener("mousemove", this.dragFunction);
        window.removeEventListener("mouseup", this.stopDragFunction);
    }
    onDrag(e: MouseEvent) {
        //get the bounding box of the target
        let target: HTMLElement = e.target as HTMLElement;
        let rect: DOMRect = target.getBoundingClientRect();
        //get the center of the target
        let centerX: number = rect.left + (rect.width / 2);
        let centerY: number = rect.top + (rect.height / 2);

        console.log(centerX, centerY);

        //get the change in mouse position
        let x: number = e.clientX;
        let y: number = e.clientY;

        //get the change in mouse position from the center of the target
        x = x - 20;
        y = y - 20;

        this.setState({ x: x, y: y });
    }
    render() {
        if (this.state.showMe===false)
        {
            return "";
        }
        return (<Card className='layerBar' onMouseDown={this.startDrag.bind(this)} style={{ position: "absolute", left: this.state.x, top: this.state.y,zIndex:100 }}>
            <Card.Header><h2>Layers</h2><button className='btn-close' onClick={()=>{this.setState({showMe:false})}}></button></Card.Header>
            <Card.Body>
            
            {this.getElementRows()}
            </Card.Body>
        </Card>)
    }
}
export default LayerBar;