import React, { Component } from 'react';
import { AlignUtil } from '../classes/AlignUtil';
import { FormConstants } from '../classes/constants/FormConstants';
import { ButtonIconVo } from '../classes/dataObjs/ButtonIconVo';
import { ElementVo } from '../classes/dataObjs/ElementVo';
import { ExtrasVo } from '../classes/dataObjs/ExtrasVo';
import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { ShapeStyleVo } from '../classes/dataObjs/ShapeStyleVo';
import { StepVo } from '../classes/dataObjs/StepVo';
import { ZoomVo } from '../classes/dataObjs/ZoomVo';
import { MainController } from '../classes/MainController';
import { MainStorage } from '../classes/MainStorage';
import AddBoxes from '../comps/AddBoxes';
import ElementBar from '../comps/ElementBar';
import LayerBar from '../comps/LayerBar';
import PreviewPage from '../comps/PreviewPage';
import PropBox from '../comps/toolpanels/PropBox';
import StepPanel from '../comps/StepPanel';
import ToolBar from '../comps/toolbar/ToolBar';
import TopBar from '../comps/TopBar';
import ZoomPreviewPage from '../comps/ZoomPreviewPage';
import ZoomFrame from '../comps/ZoomFrame';
interface MyProps { steps: StepVo[], doneCallback: Function }
interface MyState { steps: StepVo[], zoomVo: ZoomVo, zoomLocked: boolean, sideMode: number, step: StepVo | null, selectedElement: ElementVo | null, activeRightTab: string, w: number, h: number }

enum SideModes {
    ELEMENTS = 0,
    LAYERS = 1,
    ZOOM = 2
}
enum RightTabs {
    PROPS = "props",
    LAYERS = "layers",
    ZOOM = "zoom"
}

export enum PropActions
{
    CHANGE_PAGE=0,
    FLIP_H=1,
    FLIP_V=2,
    ALIGN=3,
    ORIENTATION=4,
    EDIT=5,
    COPY=12,
    DELETE=13,
    NONE=99
}

class PageEditScreen extends Component<MyProps, MyState>
{
    private addElements: ButtonIconVo[] = [];
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private editElement: ElementVo | null = null;
    private isDown: boolean = false;
    private lineCircleIndex: number = 0;
    private changeFlag: boolean = false;

    private isCompMounted: boolean = false;

    constructor(props: MyProps) {
        super(props);
        this.addElements.push(new ButtonIconVo("Image", "fas fa-image", "light", 0, "sm"));
        this.addElements.push(new ButtonIconVo("Text", "fas fa-font", "light", 1, "sm"));
        this.addElements.push(new ButtonIconVo("Card", "far fa-address-card", "light", 5, "sm"));
        this.addElements.push(new ButtonIconVo("Line", "fas fa-horizontal-rule", "light", 6, "sm"));
        this.addElements.push(new ButtonIconVo("Shape", "fas fa-heart", "light", 7, "sm"));

        this.mc.lockZoom = this.setZoomLock.bind(this);

        this.mc.undo = this.goBack.bind(this);
        this.mc.redo = this.goForward.bind(this);

        this.mc.copyElement = this.copyElementToClipboard.bind(this);
        this.mc.pasteElement = this.pasteElement.bind(this);
        this.mc.selectElement = this.selectElement.bind(this);
        this.mc.deselectElement = this.deselectElement.bind(this);

        this.mc.toggleZoom = this.toggleZoomLock.bind(this);

        this.mc.doKeyboardShortCut = this.doShortCut.bind(this);

        this.mc.getToolProps = this.getPropBox.bind(this);
        this.mc.updateZoomPreview = this.updateZoomPreview.bind(this);

        this.mc.stageSizeUpdated=this.stageSizeUpdated.bind(this);

        let firstStep: StepVo | null = null;
        if (this.props.steps.length > 0) {
            firstStep = this.props.steps[0];
        }

        let selectedStep: StepVo | null = this.ms.selectedStep;

        if (selectedStep === null) {
            selectedStep = firstStep;
        }

        this.state = { steps: this.props.steps, step: selectedStep, zoomVo: this.ms.zoomVo, zoomLocked: this.ms.zoomLocked, sideMode: 0, selectedElement: null, activeRightTab: "props", h: this.ms.pageH, w: this.ms.pageW };
    }
    componentDidMount() {
        if (this.ms.selectedElement !== null) {
            this.setState({ selectedElement: this.ms.selectedElement });
            this.mc.elementSelected(this.ms.selectedElement?.type);
        }
        this.mc.updateZoomControls(this.ms.selectedStep.zoom);
        this.updateZoom(this.ms.selectedStep.zoom);
        this.isCompMounted = true;

    }
    componentWillUnmount() {
        this.isCompMounted = false;
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ steps: this.props.steps });
        }
    }

    copyElementToClipboard() {
        if (this.ms.selectedElement) {
            this.ms.copyElement = this.ms.selectedElement.clone2();
        }
    }
    pasteElement() {
        if (this.ms.copyElement) {
            let pasteElement: ElementVo = this.ms.copyElement.clone2();

            if (this.pageHasElement(this.ms.copyElement.eid)) {
                this.ms.eidIndex++;
                pasteElement.eid = this.ms.eid;
            }

            pasteElement.id = ElementVo.getNewID();

            let stepVo: StepVo = this.ms.steps[this.ms.currentStep];
            stepVo.addElement(pasteElement);

            this.ms.addHistory();


            this.ms.selectedElement = pasteElement;
            this.selectElement(this.ms.selectedElement);
            this.setState({ step: this.ms.steps[this.ms.currentStep] });
        }
    }
    pageHasElement(eid: string) {
        if (this.state.step) {

            for (let i: number = 0; i < this.state.step.elements.length; i++) {
                let element: ElementVo = this.state.step.elements[i];
                if (element.eid === eid) {
                    return true;
                }
            }
        }
        return false;

    }
    doShortCut(key: string, ctr: boolean, alt: boolean) {
        if (this.isCompMounted === false) {
            return;
        }
        let cString: string = (ctr === false) ? "0" : "1";
        let aString: string = (alt === false) ? "0" : "1";

        let keyString: string = cString + aString + key;
        ////console.log(keyString);

        switch (keyString) {
            case "10z":
                this.goBack();
                break;

            case "10y":
                this.goForward();
                break;

            case "10c":
                this.copyElementToClipboard();
                break;
            case "10v":
                this.pasteElement();
                break;
            case "10d":
                this.toggleZoomLock();
                break;

            case "00Delete":
                if (this.state.selectedElement) {
                    this.deleteElement(this.state.selectedElement);
                    this.ms.addHistory();
                    this.setState({ selectedElement: null });
                }

                break;
        }


    }
    onMove(e: React.PointerEvent<SVGElement>) {
        e.preventDefault();

        if (this.isDown === true && this.state.selectedElement) {

            let selected: ElementVo = this.state.selectedElement;

            /**
             * convert movement in pixels to percentages
             */
            let perMoveX: number = Math.floor((e.movementX / this.ms.pageW) * 10000) / 100;
            let perMoveY: number = Math.floor((e.movementY / this.ms.pageH) * 10000) / 100;


            if (selected.type === ElementVo.TYPE_LINE) {
                this.moveLine(perMoveX, perMoveY);
                return;
            }

            /**
             * update position
             */
            selected.x += perMoveX;
            selected.y += perMoveY;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }

    }
    releaseElement(elementVo: ElementVo) {
        this.isDown = false;
        ////console.log("element released");
    }
    onElementDown(elementVo: ElementVo) {

        this.selectElement(elementVo);
        this.isDown = true;
    }
    onLineDown(elementVo: ElementVo, index: number) {
        ////console.log("line=" + index);
        this.selectElement(elementVo);
        this.isDown = true;
        this.lineCircleIndex = index;
    }
    moveLine(perMoveX: number, perMoveY: number) {
        let selected: ElementVo | null = this.state.selectedElement;
        if (selected) {
            switch (this.lineCircleIndex) {
                case 0:
                    selected.x += perMoveX;
                    selected.y += perMoveY;
                    break;

                case 1:
                    selected.w += perMoveX;
                    selected.h += perMoveY;
                    break;

                case 2:
                    selected.x += perMoveX;
                    selected.y += perMoveY;
                    selected.w += perMoveX;
                    selected.h += perMoveY;

            }
            this.changeFlag = true;
            this.setState({ selectedElement: selected });
        }
    }
    onLineUp(elementVo: ElementVo, index: number) {
        this.isDown = false;
        //  ////console.log("line up");
    }
    toggleVis(elementVo: ElementVo) {
        elementVo.visible = !elementVo.visible;
        this.ms.selectedElement = elementVo;

        this.setState({ selectedElement: elementVo });
    }
    toggleLock(elementVo: ElementVo) {
        elementVo.locked = !elementVo.locked;
        this.ms.selectedElement = elementVo;

        this.setState({ selectedElement: elementVo });
    }
    selectElement(elementVo: ElementVo) {
        console.log('Select Element Function: ' + elementVo.type);
        this.ms.suggested = elementVo.suggested;
        this.ms.selectedElement = elementVo;
        this.setState({ selectedElement: elementVo, sideMode: 0 });
        this.mc.elementSelected(elementVo.type);

    }

    deselectElement(elementVo: ElementVo) {
        this.setState({ selectedElement: null, sideMode: 0 });
    }

    editTheElement(elementVo: ElementVo | null) {
        if (elementVo === null) {
            return;
        }

        this.editElement = elementVo;
        if (this.editElement.type === ElementVo.TYPE_IMAGE || this.editElement.type === ElementVo.TYPE_BACK_IMAGE) {
            this.mc.mediaChangeCallback = this.updateElementImage.bind(this);
            this.mc.openImageBrowse();
        }
        if (this.editElement.type === ElementVo.TYPE_TEXT) {
            this.ms.editText = this.editElement.content[0];
            this.mc.textEditCallback = this.updateElementText.bind(this);
            this.mc.openTextEdit();
        }

        if (this.editElement.type === ElementVo.TYPE_CARD) {
            switch (this.editElement.subType) {
                case ElementVo.SUB_TYPE_GREEN_CARD:

                    this.mc.openForm(FormConstants.GREEN_CARD_FORM);
                    break;

                case ElementVo.SUB_TYPE_WHITE_CARD:
                    this.mc.openForm(FormConstants.WHITE_CARD_FORM);

            }

        }
    }
    updateElementImage(libItem: LibItemVo) {
        if (this.editElement) {
            this.editElement.content[0] = libItem.image;
            this.mc.closeImageBrowse();
            this.ms.addHistory();
        }
    }
    updateElementText(text: string) {
        if (this.editElement) {
            this.editElement.content[0] = text;
            this.mc.closeTextEdit();
            this.ms.addHistory();
        }
    }
    updateProps(xx: number, yy: number, ww: number, hh: number) {


        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.x = xx;
            selected.y = yy;
            selected.w = ww;
            selected.h = hh;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    updateLine(xx: number, yy: number, ww: number, hh: number) {


        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.x = xx;
            selected.y = yy;
            selected.w = ww;
            selected.h = hh;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    updateBackgroundProps(xx: number, yy: number, ww: number, hh: number) {
        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.extras.backgroundPosX = xx;
            selected.y = yy;
            selected.extras.backgroundSizeW = ww;

            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    updateInstanceName(eid: string) {
        if (this.state.selectedElement) {
            let selected: ElementVo = this.state.selectedElement;
            selected.eid = eid;
            this.setState({ selectedElement: selected });
            this.changeFlag = true;
        }
    }
    getSideContent() {

        let allElements: ElementVo[] = [];
        if (this.state.step) {
            allElements = this.state.step.elements;
        }
        return (<ElementBar addElements={this.addElements} zoomLocked={this.state.zoomLocked} sideMode={this.state.sideMode}
            elements={allElements}
            selectedElement={this.state.selectedElement} addNewElement={this.mc.addNewElement}
            updateElementRows={this.updateElementRows.bind(this)} selectElement={this.selectElement.bind(this)}
            toggleVis={this.toggleVis.bind(this)} toggleLock={this.toggleLock.bind(this)}
            toggleZoomLock={this.toggleZoomLock.bind(this)}></ElementBar>
        );
    }

    getToolBar() {
        return (
            <ToolBar zoomLocked={this.state.zoomLocked}></ToolBar>
        )
    }

    setZoomLock(lock: boolean) {
        this.ms.zoomLocked = lock;
        this.setState({ zoomLocked: lock });
    }
    toggleZoomLock() {

        this.ms.zoomLocked = !this.state.zoomLocked;
        this.setState({ zoomLocked: !this.state.zoomLocked });
    }
    updateElementRows(pos: number[]) {


        let allElements: ElementVo[] = [];
        if (this.state.step) {
            allElements = this.state.step.elements;
        }
        let nElements: ElementVo[] = [];
        for (let i: number = 0; i < allElements.length; i++) {
            nElements.push(allElements[pos[i]]);
        }

        this.ms.steps[this.ms.currentStep].elements = nElements;

        this.ms.addHistory();

    }
    doPropAction(action: number, param: number = 0) {


        if (this.state.selectedElement) {

            let elementVo: ElementVo | null = this.state.selectedElement;

            switch (action) {
                case PropActions.CHANGE_PAGE:
                    //  this.changeElementPage();
                    break;
                case PropActions.FLIP_H:
                    let flippedH: boolean = !elementVo.extras.flipH;
                    elementVo.extras.flipH = flippedH;
                    this.ms.addHistory();
                    break;

                case PropActions.FLIP_V:
                    let flippedV: boolean = !elementVo.extras.flipV;
                    elementVo.extras.flipV = flippedV;
                    this.ms.addHistory();
                    break;

                case PropActions.ALIGN:
                    let alignUtil: AlignUtil = new AlignUtil();
                    alignUtil.alignElement(param, elementVo);
                    this.ms.addHistory();
                    break;

                case PropActions.ORIENTATION:
                    elementVo.extras.orientation = param;
                    this.ms.addHistory();
                    break;

                case PropActions.EDIT:
                    this.editTheElement(this.state.selectedElement);
                    break;

                case PropActions.COPY:
                    elementVo = this.copyElement(elementVo);
                    this.ms.addHistory();
                    break;
                case PropActions.DELETE:
                    this.deleteElement(elementVo);
                    elementVo = null;
                    this.ms.addHistory();
                    break;
            }

            if (action > 0) {
                this.setState({ selectedElement: elementVo });
            }
        }
    }
    deleteElement(delElement: ElementVo) {
        let stepVo: StepVo = this.ms.steps[this.ms.currentStep];

        let elements: ElementVo[] = stepVo.deleteElement(delElement);
        this.ms.steps[this.ms.currentStep].elements = elements;

    }
    copyElement(copyElementVo: ElementVo) {
        let elementVo: ElementVo = copyElementVo.clone();

        if (this.pageHasElement(elementVo.eid)) {
            this.ms.eidIndex++;
            elementVo.eid = this.ms.eid;
        }

        elementVo.id = ElementVo.getNewID();

        let stepVo: StepVo = this.ms.steps[this.ms.currentStep];

        stepVo.addElement(elementVo);

        this.ms.selectedElement = elementVo;
        return elementVo;
    }
    updateExtras(extrasVo: ExtrasVo) {
        if (this.state.selectedElement) {
            this.changeFlag = true;
            let elementVo: ElementVo = this.state.selectedElement;
            elementVo.extras = extrasVo;
            this.setState({ selectedElement: elementVo });
        }
    }
    updateContent(content: string[]) {
        if (this.state.selectedElement) {

            let elementVo: ElementVo = this.state.selectedElement;
            elementVo.content = content;
            this.setState({ selectedElement: elementVo });
        }
    }
    updateShapeStyle(style: ShapeStyleVo) {
        if (this.state.selectedElement) {
            let elementVo: ElementVo = this.state.selectedElement;
            elementVo.extras.backgroundColor = style.fillColor;
            elementVo.extras.borderColor = style.strokeColor;
            elementVo.extras.borderThick = style.strokeWidth;
            elementVo.extras.shapeStyle = style;
            this.setState({ selectedElement: elementVo });
        }
    }
    getPropBox(id: string) {
        return (<PropBox id={id} key="propBox" elementVo={this.state.selectedElement} step={this.state.step} updateContent={this.updateContent.bind(this)} updateLine={this.updateLine.bind(this)} callback={this.updateProps.bind(this)} actionCallback={this.doPropAction.bind(this)} updateExtras={this.updateExtras.bind(this)} updateBackgroundProps={this.updateBackgroundProps.bind(this)} updateInstance={this.updateInstanceName.bind(this)} updateShapeStyle={this.updateShapeStyle.bind(this)}></PropBox>)
    }
    getAddBoxes() {
        return <AddBoxes addElements={this.addElements} addNewElement={this.mc.addNewElement}></AddBoxes>
    }
    getPreview() {
        //let psize:any=this.ms.getPreviewSize(1);
        return (<div className="preview">
            <PreviewPage editCallback={this.onElementDown.bind(this)} onUp={this.releaseElement.bind(this)} stepVo={this.state.step} onMove={this.onMove.bind(this)} lineDown={this.onLineDown.bind(this)} lineUp={this.onLineUp.bind(this)} w={this.state.w} h={this.state.h}></PreviewPage>
        </div>)
    }
    stageSizeUpdated()
    {
        this.setState({w:this.ms.pageW,h:this.ms.pageH});
    }
    getZoomedPreview() {
        //let zoomVo:ZoomVo=new ZoomVo()
        return (<div className="zoomPreview">
            <ZoomPreviewPage stepVo={this.state.step} zoomVo={this.state.zoomVo}></ZoomPreviewPage>
        </div>)
    }
    updateZoom(zoomVo: ZoomVo) {
        //////console.log(zoomVo.x1);
        this.changeFlag = true;
        this.ms.selectedStep.zoom = zoomVo;
        this.mc.updateZoomProps(zoomVo);
        this.setState({ zoomVo: zoomVo });
    }
    updateZoomPreview(zoomVo: ZoomVo) {
        this.changeFlag = true;
        this.ms.selectedStep.zoom = zoomVo;
        //  this.mc.updateZoomProps(zoomVo);
        this.setState({ zoomVo: zoomVo });
    }
    selectStep(stepVo: StepVo) {
        this.setState({ step: this.ms.selectedStep, zoomVo: this.ms.selectedStep.zoom, selectedElement: null });
        this.mc.updateZoomControls(this.ms.selectedStep.zoom);
    }
    goBack() {
        this.ms.goBack();
        this.setState({ step: this.ms.selectedStep, zoomVo: this.ms.selectedStep.zoom, selectedElement: null });
        this.mc.updateZoomControls(this.ms.selectedStep.zoom);
        this.mc.stepsUpdated();
    }
    goForward() {
        this.ms.goForward();
        this.setState({ step: this.ms.selectedStep, zoomVo: this.ms.selectedStep.zoom, selectedElement: null });
        this.mc.updateZoomControls(this.ms.selectedStep.zoom);
        this.mc.stepsUpdated();
    }
    doSaveAction(action: number) {
        switch (action) {
            case 1:
                // alert("TODO:Save");
                this.mc.saveZoom();
                break;

            case 0:
                // alert("TODO:Load");
                this.mc.openZoom();
                break;
            case 2:
                this.mc.changeScreen(10);
                break;

            case 3:
                this.mc.exportFile();
                break;

            case 10:
                this.mc.duplicateFrame();
                this.ms.addHistory();
                break;

            case 11:
                this.mc.addBlankFrame();
                this.ms.addHistory();
                break;

            case 12:
                this.mc.delFrame();
                this.ms.addHistory();
                break;

            case 20:
                this.mc.undo();
                break;

            case 21:
                this.mc.redo();
                break;

            case 22:
                this.copyElementToClipboard();
                break;
            case 23:
                this.pasteElement();
                break;

            case 30:
                //add an image
                this.mc.addNewElement(0);
                break;
            case 31:
                //add a text box
                this.mc.addNewElement(1);
                break;
            case 32:
                //add a card
                this.mc.addNewElement(5);
                break;
            case 33:
                //add a line
                this.mc.addNewElement(6);
                break;
            case 34:
                //add a shape
                this.mc.addNewElement(7);
                break;

            case 40:
                this.mc.showLayers();
                break;
        }
    }
    onUp() {
        this.isDown = false;
        ////console.log("on up");
        if (this.changeFlag === true) {
            this.changeFlag = false;
            this.ms.addHistory();
        }
    }
    setRightTab(tab: string | null) {
        if (tab) {
            this.setState({ activeRightTab: tab });
        }
    }
    getRightTabs() {
        let allElements: ElementVo[] = [];
        if (this.state.step) {
            allElements = this.state.step.elements;
        }

        return <div>
            <LayerBar elements={allElements}
                selectedElement={this.state.selectedElement}
                updateElementRows={this.updateElementRows.bind(this)} selectElement={this.selectElement.bind(this)}
                toggleVis={this.toggleVis.bind(this)} toggleLock={this.toggleLock.bind(this)}></LayerBar>
        </div>
    }
    /*  <div className='pholder'>
     {this.getPreview()}
     <ZoomFrame updateZoom={this.updateZoom.bind(this)} locked={this.state.zoomLocked}></ZoomFrame>
     </div> */
    render() {
        return (<div className='pageEditScreen' onPointerUp={(e: React.PointerEvent<HTMLDivElement>) => { this.onUp() }}>
            <div className='threeSpan'>
                <TopBar callback={this.doSaveAction.bind(this)} zoomLocked={this.state.zoomLocked}></TopBar>
            </div>
            <div>{this.getToolBar()}</div>
            <div>
                <StepPanel selectCallback={this.selectStep.bind(this)}></StepPanel>
                <div><ZoomFrame updateZoom={this.updateZoom.bind(this)} locked={this.state.zoomLocked}></ZoomFrame>
                    {this.getPreview()}
                </div>
                {this.getRightTabs()}
            </div>
            <div>
                {this.getZoomedPreview()}

            </div>
        </div>)
    }

}
export default PageEditScreen;