import React, { useState } from 'react';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';


export default function AttributionScreen(this: any) {

    const ms: MainStorage = MainStorage.getInstance();
    const mc:MainController=MainController.getInstance();
    const [attributionText, setAttribution] = useState(ms.attribution);

    const handleChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
      setAttribution(event.target.value);
    }

    const saveAttribution = () => {
      ms.attribution = attributionText;
    }

  return (
    <div className='attributionCard'>
         <textarea onChange={handleChange} value = {attributionText} id="attributionField" name="text_field_att" cols={60} rows={6} placeholder="Enter attribution text here..."></textarea>
         <br></br>
        <button onClick={()=> mc.changeScreen(0)}> Return</button>
        <button onClick={()=> saveAttribution()}> Save</button>
    </div>
  );
}
