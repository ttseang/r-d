import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { StyleVo } from '../classes/dataObjs/StyleVo';
import StyleBox from '../comps/StyleBox';
interface MyProps { styles: StyleVo[] }
interface MyState { styles: StyleVo[] }

class StyleScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { styles: this.props.styles }
    }
    componentDidUpdate(oldProps: MyProps) {
        if (oldProps !== this.props) {
            this.setState({ styles: this.props.styles });
        }
    }
    getBoxes() {
        let boxes: JSX.Element[] = [];
        for (let i: number = 0; i < this.props.styles.length; i++) {
            let key: string = "styleBox" + i.toString();
            boxes.push(<Col sm={4}><StyleBox key={key} styleVo={this.props.styles[i]}></StyleBox></Col>)
        }
        return (<Row>{boxes}</Row>);
    }
    render() {
        return (<div>{this.getBoxes()}</div>)
    }
}
export default StyleScreen;