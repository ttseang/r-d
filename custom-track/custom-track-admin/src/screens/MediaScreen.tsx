import { Component } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { ButtonConstants } from '../classes/constants/ButtonConstants';

import { LibItemVo } from '../classes/dataObjs/LibItemVo';
import { MainController } from '../classes/MainController';
import MainStorage from '../classes/MainStorage';
import PicBox from '../classes/PicBox';
import ButtonLine from '../classes/ui/ButtonLine';

interface MyProps { library: LibItemVo[], closeCallback: Function }
interface MyState { library: LibItemVo[],filteredLib:LibItemVo[],showFiltered:boolean }
class MediaScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props)
        this.state = { library: this.props.library,filteredLib:[],showFiltered:false};
    }
    componentDidMount()
    {
        let libItemVo:LibItemVo[]=this.getFilter();
        let show:boolean=true;
        if (libItemVo.length===0)
        {
            show=false;
        }
        this.setState({filteredLib:libItemVo,showFiltered:show});
    }
    componentDidUpdate(oldProps: MyProps) {
        if (this.props !== oldProps) {
            this.setState({ library: this.props.library });
        }
    }
    private getFilter()
    {        
        return this.ms.libUtil.getFilter(this.ms.suggested);
    }
    private showFilter()
    {
        this.setState({showFiltered:true});
    }
    private showAll()
    {
        this.setState({showFiltered:false});
    }
    private getCards() {
        let cardArray: JSX.Element[] = [];
        let items:LibItemVo[]=this.state.library;
        if (this.state.showFiltered===true)
        {
            items=this.state.filteredLib;
        }
        for (let i: number = 0; i < items.length; i++) {
            let key:string="mediaImage"+i.toString();

            cardArray.push(<Col key={key} sm={6} md={3} lg={2} ><PicBox libItem={items[i]}></PicBox></Col>);
        }
        return cardArray;
    }
    buttonActions(action:number,param:number)
    {
        switch(action)
        {
            case 0:
                this.setState({showFiltered:true});
                break;
            case 1:
                this.setState({showFiltered:false});
        }
    }
    getButtons()
    {
        if (this.state.filteredLib.length===0)
        {
           return "";
        }
        return <ButtonLine buttonArray={ButtonConstants.LIB_PANEL_BUTTONS} actionCallback={this.buttonActions.bind(this)} useVert={false}></ButtonLine>
    }
    render() {
        return (<div>
            <Card id="mediaLib">
                <Card.Header className="headTitle">Media Library<Button className="btnClose" onClick={() => { this.props.closeCallback() }}>X</Button></Card.Header>
                <Card.Body>
                    {this.getButtons()}
                    <div className='scroll1'>
                        <Row>
                            {this.getCards()}
                        </Row>
                    </div>
                </Card.Body>
            </Card>
        </div>)
    }
}
export default MediaScreen;