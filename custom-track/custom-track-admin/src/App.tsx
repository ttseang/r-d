import React from 'react';
import './App.css';
import './styles/main.css';
import './styles/pageEdit.css';
import './styles/stepbar.css';
import './styles/toolbar.css';
import './styles/Zoomy.css';
import './styles/cards.css';
import './styles/text.css';

import './TextAnimationStyles.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import BaseScreen from './screens/BaseScreen';
function App() {
  return (
    <BaseScreen></BaseScreen>
  );
}
export default App;
