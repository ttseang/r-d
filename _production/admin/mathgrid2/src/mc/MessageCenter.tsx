import EventEmitter from "events";

export class MessageCenter
{
    public static instance: MessageCenter;
    public emitter:EventEmitter=new EventEmitter();

    public static EVENT_UPDATE_SELECTED_CELL:string='updateSelectedCell';
    public static EVENT_UPDATE_STYLE:string='updateStyle';
    public static EVENT_UPDATE_ADJUST:string='updateAdjust';
    public static EVENT_FORMAT_CELL:string='formatCell';
    public static EVENT_SET_TEXT:string='setText';
    constructor()
    {
        (window as any).messageCenter = this;
    }
    public static getInstance(): MessageCenter
    {
        if (MessageCenter.instance == null)
        {
            MessageCenter.instance = new MessageCenter();
        }
        return MessageCenter.instance;
    }
    public emit(event:string,...args:any[])
    {
        this.emitter.emit(event,args);
    }
    public on(event:string,listener:Function)
    {
        this.emitter.on(event,listener as any);
    }
}