import { ButtonGroupVo } from "../dataObjs/ButtonGroupVo";
import { ButtonVo } from "../dataObjs/ButtonVo";
import { HistoryVo } from "../dataObjs/HistoryVo";
import { OverScoreVo } from "../dataObjs/OverScoreVo";
import { PanelPosVo } from "../dataObjs/PanelPosVo";
import { StyleVo } from "../dataObjs/StyleVo";
import { TCellExportVo } from "../dataObjs/TCellExportVo";
import { TCellVo } from "../dataObjs/TCellVo";
import { TGridExport } from "../dataObjs/TGridExportVo";
import { TGridVo } from "../dataObjs/TGridVo";
import { GridUtil } from "../util/GridUtil";
import { CellType, StyleType, TempType } from "./Constants";

export class MainStorage {
    public static instance: MainStorage;
    public styles: StyleVo[] = [];
    public buttonGroups: ButtonGroupVo[] = [];
    public mathSymbols: string[] = [];
    public overlays: string[] = [];
    public overscores:OverScoreVo[]=[];
    public steps: TGridVo[] = [];
    
    public history:HistoryVo[]=[];
    public historyIndex: number = 0;

    public currentStep: number = 0;

    public LtiFileName: string = "";
    public fileName: string = "";
    public saveString: string = "";

    public applyAdjustToCell: boolean = false;
    public advanceOnBorder: boolean = false;

    public panelMap: Map<string, PanelPosVo> = new Map<string, PanelPosVo>();
    public scrollToBottomStep:boolean=false;
    public copyCell:TCellVo|null=null;
    public scratchPad:TCellVo[]=[];

    public static getInstance(): MainStorage {
        if (MainStorage.instance == null) {
            MainStorage.instance = new MainStorage();
        }
        return MainStorage.instance;
    }
    //constructor
    private constructor() {
        this.loadPanelPosFromStorage();
        this.makeStyles();
        this.makeTemplateButtons();
        this.makeMathSymbols();
        this.getScratchPad();
        (window as any).mainStorage = this;
    }
    public addHistory() {
        if (this.historyIndex !== this.history.length - 1) {
          this.history = this.history.slice(0, this.historyIndex);
        }
        this.history.push(new HistoryVo(this.currentStep, this.steps));
        this.historyIndex = this.history.length - 1;
        this.saveToLocalStorage();
      }
      public undoHistory() {
        if (this.historyIndex > 0) {
          this.historyIndex--;
          let historyVo: HistoryVo = this.history[this.historyIndex];
          this.steps = historyVo.steps;
          this.currentStep = historyVo.currentStep;
        }
        else{
            this.steps=[];
            this.steps.push(GridUtil.getEmptyGrid());
        }
        this.saveToLocalStorage();
      }
      public redoHistory() {
        if (this.historyIndex < this.history.length - 1) {
          this.historyIndex++;
    
          let historyVo: HistoryVo = this.history[this.historyIndex];
          this.steps = historyVo.steps;
          this.currentStep = historyVo.currentStep;
        }
        this.saveToLocalStorage();
      }
    updatePanelPos(type: string, posX: number, posY: number, open: boolean = false) {
        //check if the panel is already in the map
        let panelPos: PanelPosVo | undefined = this.panelMap.get(type);
        if (panelPos) {
            panelPos.posX = posX;
            panelPos.posY = posY;
            panelPos.open = open;
        }
        else {
            panelPos = new PanelPosVo(type, posX, posY, open);
            this.panelMap.set(type, panelPos);
        }
        //save the panel map to local storage
        //get only the value from the map
        let panelMapString: string = JSON.stringify(Array.from(this.panelMap.values()));
        localStorage.setItem("panelMap", panelMapString);
      
       
        
    }
    getAllOpenPanels(): PanelPosVo[] {
        let panels: PanelPosVo[] = [];
        this.panelMap.forEach((panel: PanelPosVo) => {
            if (panel.open) {
                panels.push(panel);
            }
        });
        return panels;
    }
    getPanelPos(type: string): PanelPosVo {

        let panelPos: PanelPosVo | undefined = this.panelMap.get(type);
        if (!panelPos) {
            panelPos = new PanelPosVo(type, 100,100, true);
            this.panelMap.set(type, panelPos);
        }
        return panelPos;

    }
    loadPanelPosFromStorage() {
        let panelMapString: string | null = localStorage.getItem("panelMap");
        if (panelMapString) {
            let panelMap: PanelPosVo[] = JSON.parse(panelMapString);
            for (let i: number = 0; i < panelMap.length; i++) {
                this.panelMap.set(panelMap[i].type, panelMap[i]);
            }
        }
    }
    prepSave(): string {
        let exports: TGridExport[] = [];
        for (let i: number = 0; i < this.steps.length; i++) {

            let grid: TGridVo = this.steps[i];
            let exportGrid: TGridExport = grid.getExport();
            exports.push(exportGrid);

        }
        this.saveString = JSON.stringify(exports);
        return this.saveString;
    }
    saveToLocalStorage() {
        let saveData:string=this.prepSave();
        localStorage.setItem("saveData", saveData);
    }
    getFromLocalStorage():string{
        let saveData:string|null=localStorage.getItem("saveData");
        if (saveData)
        {
            return saveData;
        }
        return "";
    }
    public addToScratchPad(cell:TCellVo)
    {
        let exportCell:TCellExportVo= cell.getExport();
        let exportString:string=JSON.stringify(exportCell);
        let scratchPad:string|null=localStorage.getItem("scratchPad");
       
        if (scratchPad)
        {
            let scratchPadArray:string[]=JSON.parse(scratchPad);
            scratchPadArray.push(exportString);
            localStorage.setItem("scratchPad",JSON.stringify(scratchPadArray));
        }
        else{
            let scratchPadArray:string[]=[];
            scratchPadArray.push(exportString);
            let obj:string=JSON.stringify(scratchPadArray);
            localStorage.setItem("scratchPad",obj);
        }
        this.scratchPad.push(cell.clone());
    }
    public setScratchPad(scratchPad:TCellVo[])
    {
        this.scratchPad=scratchPad;
        let scratchPadArray:string[]=[];
        for (let i:number=0;i<scratchPad.length;i++)
        {
            let exportCell:TCellExportVo= scratchPad[i].getExport();
            let exportString:string=JSON.stringify(exportCell);
            scratchPadArray.push(exportString);
        }
        localStorage.setItem("scratchPad",JSON.stringify(scratchPadArray));

    }
    public getScratchPad():TCellVo[]
    {
        let scratchPad:string|null=localStorage.getItem("scratchPad");
        if (scratchPad)
        {
            let scratchPadArray:string[]=JSON.parse(scratchPad);
            let cells:TCellVo[]=[];
            for (let i:number=0;i<scratchPadArray.length;i++)
            {
                let cellData:TCellExportVo=JSON.parse(scratchPadArray[i]);

                let text:string[]=cellData.text;
                let cellClasses:string[]=cellData.cellClasses;
                let textClasses:string[][]=cellData.textClasses;
                let rowSize:number=cellData.rowSize;
                let colSize:number=cellData.colSize;
                let type:string=cellData.type;
                let adjustX:number[]=cellData.adjustX;
                let adjustY:number[]=cellData.adjustY;
                let cellType:CellType=this.stringToConstants(type);

                let cell:TCellVo=new TCellVo(text,colSize,rowSize,textClasses,cellClasses,cellType);
                cell.adjustX=adjustX;
                cell.adjustY=adjustY;
                cells.push(cell);
            }
            this.scratchPad=cells;
        }
        return [];
    }
    public clearScratchPad()
    {
        localStorage.removeItem("scratchPad");
    }
    private makeMathSymbols() {

        this.overlays.push('square-root');
        this.overlays.push('square-root2');
        this.overlays.push('arrow-left');
        this.overlays.push('arrow-right');
        this.overlays.push('arrow-up');
        this.overlays.push('arrow-down');

        this.overscores.push(new OverScoreVo("U+2212","AB","ovrscr seg"));
        this.overscores.push(new OverScoreVo("U+2192","AB","ovrscr ray"));
        this.overscores.push(new OverScoreVo("U+2194","AB","ovrscr lin"));
        this.overscores.push(new OverScoreVo("U+2221","AB","ovrscr arc"));
        this.overscores.push(new OverScoreVo("U+00F8","AB","ovrscr avg"));
        this.overscores.push(new OverScoreVo("U+2032","AB","ovrscr pav"));
        
/* 
        {
            "description": "Ray",
            "symbol": "U+2192",
            "latex": "\\class{ovrscr ray}{AB}"
        },
        {
            "description": "Line",
            "symbol": "U+2194",
            "latex": "\\class{ovrscr lin}{AB}"
        },
        {
            "description": "Arc",
            "symbol": "U+2221",
            "latex": "\\class{ovrscr arc}{AB}"
        },
        {
            "description": "Average",
            "symbol": "U+00F8",
            "latex": "\\class{ovrscr avg}{AB}"
        },
        {
            "description": "Prime Average",
            "symbol": "U+2032",
            "latex": "\\class{ovrscr pav}{AB}"
        },
        {
            "description": "Prime Average Low",
            "symbol": "U+2032",
            "latex": "\\class{ovrscr pav low}{AB}"
        } */

        //plus symbols
        this.mathSymbols.push('+');
        this.mathSymbols.push('−');
        this.mathSymbols.push('×');
        this.mathSymbols.push('÷');
        this.mathSymbols.push('±');
        this.mathSymbols.push('√');
        this.mathSymbols.push('∛');
        this.mathSymbols.push('∜');
        this.mathSymbols.push('∑');
        this.mathSymbols.push('∏');
        this.mathSymbols.push('∫');
        this.mathSymbols.push('∂');
        this.mathSymbols.push('∆');
        this.mathSymbols.push('∞');
        this.mathSymbols.push('∈');
        this.mathSymbols.push('∉');
        this.mathSymbols.push('∋');
        this.mathSymbols.push('∌');
        this.mathSymbols.push('∝');
        this.mathSymbols.push('∟');
        this.mathSymbols.push('∠');
        this.mathSymbols.push('∡');
        this.mathSymbols.push('∢');
        this.mathSymbols.push('∣');
        this.mathSymbols.push('∤');
        this.mathSymbols.push('∥');
        this.mathSymbols.push('∦');
        this.mathSymbols.push('∧');
        this.mathSymbols.push('∨');
        this.mathSymbols.push('∩');
        this.mathSymbols.push('∪');
        this.mathSymbols.push('∬');
        this.mathSymbols.push('∭');
        this.mathSymbols.push('∮');
        this.mathSymbols.push('∯');
        this.mathSymbols.push('∰');
        this.mathSymbols.push('∱');
        this.mathSymbols.push('∲');
        this.mathSymbols.push('∳');
        this.mathSymbols.push('∴');
        this.mathSymbols.push('∵');
        this.mathSymbols.push('∶');
        this.mathSymbols.push('∷');
        this.mathSymbols.push('∸');
        this.mathSymbols.push('∹');
        this.mathSymbols.push('∺');
        this.mathSymbols.push('∻');
        this.mathSymbols.push('∼');
        this.mathSymbols.push('∽');
        this.mathSymbols.push('∾');
        this.mathSymbols.push('∿');
        this.mathSymbols.push('≀');
        this.mathSymbols.push('≁');
        this.mathSymbols.push('≂');
        this.mathSymbols.push('≃');
        this.mathSymbols.push('≄');
        this.mathSymbols.push('≅');
        this.mathSymbols.push('≆');
        this.mathSymbols.push('≇');
        this.mathSymbols.push('≈');
        this.mathSymbols.push('≉');
        this.mathSymbols.push('≊');
        this.mathSymbols.push('≋');

        //arrows
        this.mathSymbols.push('←');
        this.mathSymbols.push('↑');
        this.mathSymbols.push('→');
        this.mathSymbols.push('↓');
        this.mathSymbols.push('↔');
        this.mathSymbols.push('↕');
        this.mathSymbols.push('↖');
        this.mathSymbols.push('↗');
        this.mathSymbols.push('↘');
        this.mathSymbols.push('↙');
        this.mathSymbols.push('↚');
        this.mathSymbols.push('↛');
        this.mathSymbols.push('↜');
        this.mathSymbols.push('↝');
        this.mathSymbols.push('↞');
        this.mathSymbols.push('↟');
        this.mathSymbols.push('↠');
        this.mathSymbols.push('↡');


    }
    private makeTemplateButtons() {
        let columnGroup: ButtonGroupVo = new ButtonGroupVo('Cell Templates', 'column', []);
        // columnGroup.buttons.push(new ButtonVo('|÷',CellType.Divide,TempType.Any));
        // columnGroup.buttons.push(new ButtonVo('||÷',CellType.Divide2Col,TempType.Any));
        columnGroup.buttons.push(new ButtonVo('x.', CellType.Whole, TempType.Decimal));
        columnGroup.buttons.push(new ButtonVo('|', CellType.Whole, TempType.Any));
        columnGroup.buttons.push(new ButtonVo('||', CellType.Column, TempType.TwoCol));
        columnGroup.buttons.push(new ButtonVo('|||', CellType.Column, TempType.ThreeCol));
        columnGroup.buttons.push(new ButtonVo('||||', CellType.Column, TempType.FourCol));
        columnGroup.buttons.push(new ButtonVo('2 rows', CellType.Row, TempType.TwoRows));
        columnGroup.buttons.push(new ButtonVo('3 rows', CellType.Row, TempType.ThreeRows));
        //  columnGroup.buttons.push(new ButtonVo('_4',CellType.Row,TempType.FourRows));

        //let fractionGroup: ButtonGroupVo = new ButtonGroupVo('fraction', 'fraction', []);
        columnGroup.buttons.push(new ButtonVo('x/y', CellType.Fraction, TempType.Fraction1x1));
        //2x2
        columnGroup.buttons.push(new ButtonVo('xx/yy', CellType.Fraction, TempType.Fraction2x2));
        //3x3
        columnGroup.buttons.push(new ButtonVo('xxx/yyy', CellType.Fraction, TempType.Fraction3x3));
        //4x4
        columnGroup.buttons.push(new ButtonVo('xxxx/yyyy', CellType.Fraction, TempType.Fraction4x4));

        //grids
        //2x2
        columnGroup.buttons.push(new ButtonVo('2x2', CellType.Grid, TempType.Grid2x2));
        //3x2
        columnGroup.buttons.push(new ButtonVo('3x2', CellType.Grid, TempType.Grid3x2));
        //4x2
        columnGroup.buttons.push(new ButtonVo('4x2', CellType.Grid, TempType.Grid4x2));


        // let powerGroup: ButtonGroupVo = new ButtonGroupVo('power', 'power', []);
        columnGroup.buttons.push(new ButtonVo('x^y', CellType.Power, TempType.Power1x1));
        columnGroup.buttons.push(new ButtonVo('xx^yy', CellType.Power, TempType.Power2x2));
        columnGroup.buttons.push(new ButtonVo('xxx^yyy', CellType.Power, TempType.Power3x3));


        // let carryGroup: ButtonGroupVo = new ButtonGroupVo('misc', 'carry', []);
        columnGroup.buttons.push(new ButtonVo('carry', CellType.Carry, TempType.Any));
        columnGroup.buttons.push(new ButtonVo('borrow-top', CellType.Borrow, TempType.BorrowTop));
        columnGroup.buttons.push(new ButtonVo('borrow-side', CellType.Borrow, TempType.BorrowSide));
        columnGroup.buttons.push(new ButtonVo('√', CellType.SquareRoot, TempType.Square1));

        let symbolGroup: ButtonGroupVo = new ButtonGroupVo('symbols', 'symbols', []);

        this.buttonGroups.push(columnGroup);
        //this.buttonGroups.push(fractionGroup);
        //  this.buttonGroups.push(powerGroup);
        //  this.buttonGroups.push(carryGroup);
        this.buttonGroups.push(symbolGroup);

    }
    private makeStyles(): void {
        this.styles.push(new StyleVo('bottom line', 'bottomline', true, StyleType.cell));
        this.styles.push(new StyleVo('top line', 'topline', true, StyleType.cell));
        this.styles.push(new StyleVo('left line', 'leftline', true, StyleType.cell));
        this.styles.push(new StyleVo('right line', 'rightline', true, StyleType.cell));



        this.styles.push(new StyleVo('Size 1', 'fs1', false, StyleType.size));
        this.styles.push(new StyleVo('Size 2', 'fs2', false, StyleType.size));
        this.styles.push(new StyleVo('Size 3', 'fs3', false, StyleType.size));
        this.styles.push(new StyleVo('Size 4', 'fs4', false, StyleType.size));
        this.styles.push(new StyleVo('Size 5', 'fs5', false, StyleType.size));
        this.styles.push(new StyleVo('Size 6', 'fs6', false, StyleType.size));
        this.styles.push(new StyleVo('Size 7', 'fs7', false, StyleType.size));
        this.styles.push(new StyleVo('Size 8', 'fs8', false, StyleType.size));
        this.styles.push(new StyleVo('Size 9', 'fs9', false, StyleType.size));
        this.styles.push(new StyleVo('Size 10', 'fs10', false, StyleType.size));


        this.styles.push(new StyleVo('bold text', 'boldText', false, StyleType.font));
        this.styles.push(new StyleVo('italic text', 'italicText', false, StyleType.font));
        this.styles.push(new StyleVo('borrowed', 'strikethrough', false, StyleType.font));

        this.styles.push(new StyleVo('half-circle', 'halfCircle', true, StyleType.font));

        //colors
        this.styles.push(new StyleVo('', 'redText', false, StyleType.color, ["squareButton", "buttonRed"]));
        this.styles.push(new StyleVo('', 'blueText', false, StyleType.color, ["squareButton", "buttonBlue"]));
        this.styles.push(new StyleVo('', 'greenText', false, StyleType.color, ["squareButton", "buttonGreen"]));
        this.styles.push(new StyleVo('', 'yellowText', false, StyleType.color, ["squareButton", "buttonYellow"]));
        this.styles.push(new StyleVo('', 'purpleText', false, StyleType.color, ["squareButton", "buttonPurple"]));
        this.styles.push(new StyleVo('', 'orangeText', false, StyleType.color, ["squareButton", "buttonOrange"]));
        this.styles.push(new StyleVo('', 'pinkText', false, StyleType.color, ["squareButton", "buttonPink"]));
        this.styles.push(new StyleVo('', 'brownText', false, StyleType.color, ["squareButton", "buttonBrown"]));
        this.styles.push(new StyleVo('', 'blackText', false, StyleType.color, ["squareButton", "buttonBlack"]));
        this.styles.push(new StyleVo('', 'whiteText', false, StyleType.color, ["squareButton", "buttonWhite"]));
        this.styles.push(new StyleVo('', 'grayText', false, StyleType.color, ["squareButton", "buttonGray"]));
        this.styles.push(new StyleVo('', 'lightBlueText', false, StyleType.color, ["squareButton", "buttonLightBlue"]));
        this.styles.push(new StyleVo('', 'lightGreenText', false, StyleType.color, ["squareButton", "buttonLightGreen"]));
        //gold
        this.styles.push(new StyleVo('', 'goldText', false, StyleType.color, ["squareButton", "buttonGold"]));
    }
    public getStyleByName(name: string): StyleVo | null {
        for (let i = 0; i < this.styles.length; i++) {
            if (this.styles[i].styleName === name) {
                return this.styles[i];
            }
        }
        return null;
    }
    public getStyleByType(type: StyleType): StyleVo[] {
        let styles: StyleVo[] = [];
        for (let i = 0; i < this.styles.length; i++) {
            if (this.styles[i].type === type) {
                styles.push(this.styles[i]);
            }
        }
        return styles;
    }
    public stringToConstants(str: string): CellType {

        let types: CellType[] = [CellType.Whole, CellType.Column, CellType.Row, CellType.Fraction, CellType.Power, CellType.SquareRoot, CellType.Carry, CellType.Borrow];
        for (let i: number = 0; i < types.length; i++) {
            if (str === types[i]) {
                return types[i];
            }
        }
        return CellType.Whole;
    }
}