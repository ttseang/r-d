
export class MainController
{
    public static instance: MainController;
    public doGridAction:Function=()=>{};
    public updateSelectedCell:Function=()=>{};
    public updateStyle:Function=()=>{};
    public updateAdjust:Function=()=>{};
    public formatCell:Function=()=>{};
    public setText:Function=()=>{};
    //step functions
    public addStep:Function=()=>{};
    public moveStepUp:Function=()=>{};
    public moveStepDown:Function=()=>{};
    public cloneStep:Function=()=>{};
    public changeStep:Function=()=>{};
    public removeStep:Function=()=>{};
    public changeScreen:Function=()=>{};
    
    public togglePreview:Function=()=>{};
    public showGuideLines:Function=()=>{};
    public changeColor:Function=()=>{};
    public clearCell:Function=()=>{};

    public addPanel:Function=()=>{};
    public closePanel:Function=()=>{};

    public updateDebug:Function=()=>{};
    public undo:Function=()=>{};
    public redo:Function=()=>{};
    public copy:Function=()=>{};
    public cut:Function=()=>{};
    public paste:Function=()=>{};
    public clipboardChanged:Function=()=>{};
    public scratchPadChanged:Function=()=>{};
    public setCell:Function=()=>{};
    public addScratch:Function=()=>{};
    
    public toggleOverlay:Function=()=>{};
    public setOverlay:Function=()=>{};
    public addOverlay:Function=()=>{};
    public addOverScore:Function=()=>{};
    
    //constructor
    constructor()
    {
        (window as any).mc = this;
    }
    public static getInstance(): MainController
    {
        if (MainController.instance == null)
        {
            MainController.instance = new MainController();
        }
        return MainController.instance;
    }
}