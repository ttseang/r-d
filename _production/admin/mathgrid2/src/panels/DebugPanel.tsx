import React, { Component } from 'react';
import { TCellVo } from '../dataObjs/TCellVo';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState { cell: TCellVo | null }
class DebugPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { cell: null };
        this.mc.updateDebug = this.updateCell.bind(this);
    }
    updateCell(cell: TCellVo) {
        this.setState({ cell: cell });
    }
    getInfo() {
        if (this.state.cell) {
            return (<div className='debugInfo'>
                <div>UID:{this.state.cell.myIndex}</div>
                <div>Row:{this.state.cell.row}</div>
                <div>Col:{this.state.cell.col}</div>
                <div>Index:{this.state.cell.selectedIndex}</div>
            </div>);
        }
        return "no cell selected";
    }
    render() {
        return (<div>{this.getInfo()}</div>)
    }
}
export default DebugPanel;