import React, { Component } from 'react';
import { StyleVo } from '../dataObjs/StyleVo';
import { MainController } from '../mc/MainController';
import { MainStorage } from '../mc/MainStorage';
import { GridActions, StyleType } from '../mc/Constants';
interface MyProps { }
interface MyState { advance: boolean }
class BorderPanel extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { advance: this.ms.advanceOnBorder };
    }
    onStyleClick(index: number) {
        let styleVo: StyleVo = this.ms.getStyleByType(StyleType.cell)[index];
        this.mc.updateStyle(styleVo);
        if (this.state.advance) {
            this.mc.doGridAction(GridActions.GoRight);
        }
    }
    onAdvanceClick() {
        let advance: boolean = !this.state.advance;
        this.setState({ advance: advance });
        this.ms.advanceOnBorder = advance;
    }
    render() {
        return (<div className="borderPanel">
            <div></div>
            <div><button onClick={() => { this.onStyleClick(1) }} className="btn btn-light"><img src='./icons/top-border.png' alt='border top'></img></button></div>
            <div></div>
            <div><button onClick={() => { this.onStyleClick(2) }} className="btn btn-light"><img src='./icons/border-left.png' alt='border left'></img></button></div>
            <div></div>
            <div><button onClick={() => { this.onStyleClick(3) }} className="btn btn-light"><img src='./icons/border-right.png' alt='border right'></img></button></div>
            <div></div>
            <div><button onClick={() => { this.onStyleClick(0) }} className="btn btn-light"><img src='./icons/bottom-border.png' alt='border bottom'></img></button></div>
            <div></div>
            <div></div>
            <div><input type='checkbox' onClick={this.onAdvanceClick.bind(this)} />Advance</div>
            <div></div>
        </div>)
    }
}
export default BorderPanel;