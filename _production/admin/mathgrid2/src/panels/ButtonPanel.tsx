import React, { Component } from 'react';
import { GridActions, PanelActions } from '../mc/Constants';
import { MainController } from '../mc/MainController';
interface MyProps { panelAction: PanelActions }
interface MyState { }
class ButtonPanel extends Component<MyProps, MyState>
{
    private mc:MainController=MainController.getInstance();

        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    onClick(index:number)
    {
        switch(index)
        {
            case 0:
            this.mc.doGridAction(GridActions.DeleteRow);
            break;
            case 1:
            this.mc.doGridAction(GridActions.DeleteCol);
            break;
        }
    }
    render() {
        return (<div className='buttonPanel'>
            <div className='longButton3 danger' onClick={()=>{this.onClick(0)}}>Delete Row</div>
            <div className='longButton3 danger' onClick={()=>{this.onClick(1)}}>Delete Column</div>
        </div>)
    }
}
export default ButtonPanel;