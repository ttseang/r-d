import React, { Component } from 'react';
import MovePanel from './MovePanel';
import { PanelActions } from '../mc/Constants';
import ButtonPanel from './ButtonPanel';
import StylePanel from './StylePanel';
import TemplateButtons from '../comps/TemplateButtons';
interface MyProps { }
interface MyState { panelIndex: number }
class SidePanel extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { panelIndex: 0 };
    }
    selectionChanged(event: React.ChangeEvent<HTMLSelectElement>) {

        let index = event.target.selectedIndex;
        this.setState({ panelIndex: index });

    }
    getSelector(): JSX.Element {
        return (<select className='sideSelect' onChange={this.selectionChanged.bind(this)}>
            <option value="Style">Style</option>
            <option value="Adjust">Adjust</option>
            <option value="Move">Move</option>
            <option value="Shift">Shift</option>
            <option value="Insert">Insert</option>
            <option value="Delete">Delete</option>

        </select>);
    }
    getPanel() {
        switch (this.state.panelIndex) {
            case 0:
                return (<StylePanel></StylePanel>);
            case 1:
                return (<MovePanel panelAction={PanelActions.Adjust} />);
            case 2:
                return (<MovePanel panelAction={PanelActions.Move} />);
            case 3:
                return (<MovePanel panelAction={PanelActions.Shift} />);
            case 4:
                return (<MovePanel panelAction={PanelActions.Insert} />);
            case 5:
                return (<ButtonPanel panelAction={PanelActions.Delete}></ButtonPanel>)


        }
    }

    getBottomPanel() {
        return(<div className='templateButtons'>
            <TemplateButtons></TemplateButtons>
        </div>);

    }
    render() {
        return (<div className='sidePanel'>
            {this.getSelector()}
            {this.getPanel()}
            {this.getBottomPanel()}
        </div>)
    }
}
export default SidePanel;