import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';

import { StyleVo } from '../dataObjs/StyleVo';
import { StyleType } from '../mc/Constants';
import { TCellVo } from '../dataObjs/TCellVo';
import { MessageCenter } from '../mc/MessageCenter';
interface MyProps {panelType:StyleType}
interface MyState { selectedCell: TCellVo | null, selectedIndex: number }
class StylePanel2 extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private messages:MessageCenter=MessageCenter.getInstance();
    
    constructor(props: MyProps) {
        super(props);
        this.state = { selectedCell: null, selectedIndex: 0 };
        this.messages.on(MessageCenter.EVENT_UPDATE_SELECTED_CELL,(data:any)=>{
            this.cellSelected(data[0]);
        });
    }
    onStyleClick(styleVo: StyleVo) {

        this.mc.updateStyle(styleVo);
    }
    cellSelected(cellVo: TCellVo) {
        this.setState({ selectedCell: cellVo, selectedIndex: cellVo.selectedIndex });
    }
    getButtons(style: StyleType) {
        let styles: StyleVo[] = this.ms.getStyleByType(style);

        let buttons: JSX.Element[] = [];
        for (let i = 0; i < styles.length; i++) {
            let key: string = styles[i].styleName;
            let name: string = styles[i].styleName;
            if (key==='' || key===undefined || key===null)
            {
                key = styles[i].styleValue;
            }
            let style: string = styles[i].styleValue;
            //this is the class to apply to the button
            let buttonStyle: string = styles[i].buttonStyle.join(' ');

            if (this.state.selectedCell) {
                //if the selected cell has the same style as the button, then add a checkmark
                let selectedIndex:number=this.state.selectedCell.selectedIndex;
                let styles: string[] = this.state.selectedCell.textClasses[selectedIndex];
                if (!styles) {
                    styles = [];
                }

                if (styles.includes(style)) {
                    name += ' ✓';
                }
               /*  else {
                    name += ' ✗';
                } */

            }
            if (buttonStyle !== "") {
                buttons.push(<button key={key} className={buttonStyle} onClick={() => {
                    this.onStyleClick(styles[i]);
                }}>{name}</button>)
            }
            else {
                buttons.push(<button key={key} className='longButton2' onClick={() => {
                    this.onStyleClick(styles[i]);
                }}>{name}</button>)
            }
        }
        if (style===StyleType.color)
        {
            return <div className='colorButtons'>{buttons}</div>
        }
        return <div className='styleButtons'>{buttons}</div>
    }
    getPanels() {
        return (<div>
            {this.getButtons(this.props.panelType)}
        </div>
        )
    }
    render() {
        return (<div className='stylePanel'>
            {this.getPanels()}
        </div>)
    }
}
export default StylePanel2;