import React, { Component } from 'react';
import { GridActions, PanelActions } from '../mc/Constants';
import { MainController } from '../mc/MainController';
import { MessageCenter } from '../mc/MessageCenter';
import { TCellVo } from '../dataObjs/TCellVo';
interface MyProps { panelAction: PanelActions }
interface MyState {cell:TCellVo|null}
class MovePanel extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    private messages:MessageCenter=MessageCenter.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {cell:null};
        this.messages.on(MessageCenter.EVENT_UPDATE_SELECTED_CELL,(data:any)=>{
            //this.cellSelected(data[0]);
            this.setState({cell:data[0]});
        });
    }
    onClick(index: number) {
        switch (this.props.panelAction) {
            case PanelActions.Move:
                switch (index) {
                    case 0:
                        this.mc.doGridAction(GridActions.MoveUp);
                        break;
                    case 1:
                        this.mc.doGridAction(GridActions.MoveLeft);
                        break;
                    case 2:
                        this.mc.doGridAction(GridActions.MoveRight);
                        break;
                    case 3:
                        this.mc.doGridAction(GridActions.MoveDown);
                        break;
                }
                break;
            case PanelActions.Shift:
                switch (index) {
                    case 0:
                        this.mc.doGridAction(GridActions.ShiftColUp);
                        break;
                    case 1:
                        this.mc.doGridAction(GridActions.ShiftRowLeft);
                        break;
                    case 2:
                        this.mc.doGridAction(GridActions.ShiftRowRight);
                        break;
                    case 3:
                        this.mc.doGridAction(GridActions.ShiftColDown);
                        break;
                }
                break;
            case PanelActions.Insert:
                switch (index) {
                    case 0:
                        this.mc.doGridAction(GridActions.InsertRowAbove);
                        break;
                    case 1:
                        this.mc.doGridAction(GridActions.InsertColLeft);
                        break;
                    case 2:
                        this.mc.doGridAction(GridActions.InsertColRight);
                        break;
                    case 3:
                        this.mc.doGridAction(GridActions.InsertRowBelow);
                        break;

                }

                break;
            case PanelActions.Adjust:
                switch (index) {
                    case 0:
                        this.mc.updateAdjust(0, -0.05);
                        break;
                    case 1:
                        this.mc.updateAdjust(-0.05, 0);
                        break;
                    case 2:
                        this.mc.updateAdjust(0.05, 0);
                        break;
                    case 3:
                        this.mc.updateAdjust(0, 0.05);
                        break;

                }
        }
    }
    getLabel() {
        switch (this.props.panelAction) {
            case PanelActions.Move:
                return "Move";
            case PanelActions.Shift:
                return "Shift";
            case PanelActions.Insert:
                return "Insert";
        }
    }
    getButtonLabel(index: number) {
        let labels: string[] = [];
        switch (this.props.panelAction) {
            case PanelActions.Move:
            case PanelActions.Adjust:
                labels = ["UP", "LEFT", "RIGHT", "DOWN"];
                break;
            case PanelActions.Shift:
                labels = ["COL UP", "ROW LEFT", "ROW RIGHT", "COL DOWN"];
                break;
            case PanelActions.Insert:
                labels = ["Row Above", "Col Left", "Col Right", "Row Below"];

        }
        if (index >= 0 && index < labels.length) {
            return labels[index];
        }
        return "";


    }
    getInfo() {
        if (!this.state.cell)
        {
            return (<div></div>);
        }
        switch (this.props.panelAction) {
            case PanelActions.Adjust:
                return (<div className='panelInfo'>
                    <div>X:{this.state.cell.getAdjustX()}</div>
                    <div>Y:{this.state.cell.getAdjustY()}</div>
                </div>);
            /* case PanelActions.Move:
                return (<div>
                    <div>X:{this.state.cell.row}</div>
                    <div>Y:{this.state.cell.col}</div>
                </div>); */

            default:
                return (<div></div>);

        }
    }
    render() {

        return (<div>
            {this.getInfo()}
            <div className='movePanel'>
            <div></div>
            <div className='cellButton primary' onClick={() => { this.onClick(0) }}>{this.getButtonLabel(0)}</div>
            <div></div>
            <div className='cellButton primary' onClick={() => { this.onClick(1) }}>{this.getButtonLabel(1)}</div>
            <div className='centerLabel'>{this.getLabel()}</div>
            <div className='cellButton primary' onClick={() => { this.onClick(2) }}>{this.getButtonLabel(2)}</div>
            <div></div>
            <div className='cellButton primary' onClick={() => { this.onClick(3) }}>{this.getButtonLabel(3)}</div>
            <div></div>
        </div></div>)
    }
}
export default MovePanel;