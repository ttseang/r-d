import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState { }
class SymbolPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    getButtons() {
        let symbols: string[] = this.ms.mathSymbols;
        let buttons: JSX.Element[] = [];
        for (let i = 0; i < symbols.length; i++) {
            let key: string = 'symbol_' + symbols[i];
            buttons.push(<button key={key} className='cellButton' onClick={() => {
                this.mc.setText(symbols[i]);
            }}>{symbols[i]}</button>)
        }
        return buttons;
    }
    render() {
        return (<div className='symButtons'>
            {this.getButtons()}
        </div>)
    }
}
export default SymbolPanel;