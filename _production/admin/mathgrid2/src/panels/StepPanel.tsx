import React, { Component } from 'react';
import { TGridVo } from '../dataObjs/TGridVo';
import StepCard from '../comps/StepCard';
import { Button, ButtonGroup } from 'react-bootstrap';
import { MainController } from '../mc/MainController';
import { MainStorage } from '../mc/MainStorage';
interface MyProps { steps: TGridVo[] }
interface MyState { steps: TGridVo[] }
class StepPanel extends Component<MyProps, MyState>
{
    private mc:MainController = MainController.getInstance();
    private ms:MainStorage = MainStorage.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = { steps: props.steps };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.steps !== this.props.steps) {

            if (this.ms.scrollToBottomStep)
            {
                this.setState({ steps: this.props.steps },this.scrollToBottom.bind(this));
                this.ms.scrollToBottomStep=false;
                return;
            }
            this.setState({ steps: this.props.steps });
        }
    }
    getSteps() {
        let cards: JSX.Element[] = [];
        this.state.steps.forEach((step: TGridVo, index: number) => {
            let key: string = "step" + index.toString();
            let selected: boolean = (index === this.ms.currentStep);
            cards.push(<StepCard key={key} index={index} grid={step} selected={selected} />);
        });
        return cards;
    }
    
    scrollToBottom()
    {
        let scrollEl:HTMLElement = document.getElementsByClassName("gridSteps")[0] as HTMLElement;
        scrollEl.scrollTop = scrollEl.scrollHeight;
    }
    
            
    render() {
        return (
            <div className='stepPanel'>
                <div className='stepButtonPanel'>
                    <ButtonGroup>
                     <Button variant="danger" title='delete step' size="sm" onClick={()=>{this.mc.removeStep()}}><i className="fas fa-trash"></i></Button>
                        <Button variant="primary" title='move step back' size="sm" onClick={()=>{this.mc.moveStepDown()}}><i className="fas fa-arrow-up"></i></Button>
                        <Button variant="primary" size="sm" title='move step forward' onClick={()=>{this.mc.moveStepUp()}}><i className="fas fa-arrow-down"></i></Button>
                        <Button variant="primary" size="sm" title='copy step' onClick={()=>{this.mc.cloneStep()}}><i className="fas fa-clone"></i></Button>
                        <Button variant="success" size="sm" onClick={()=>{this.mc.addStep()}}>+</Button>
                    </ButtonGroup>
                </div>
                <div className='gridSteps'>
                    {this.getSteps()}
                </div>
            </div>)
    }
}
export default StepPanel;