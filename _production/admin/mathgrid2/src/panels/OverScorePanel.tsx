import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import { OverScoreVo } from '../dataObjs/OverScoreVo';
import { StyleUtil } from '../util/StyleUtil';
interface MyProps { }
interface MyState { }
class OverScorePanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }

    getButtons() {
        let overscores: OverScoreVo[] = this.ms.overscores;
        let buttons: JSX.Element[] = [];
        for (let i = 0; i < overscores.length; i++) {
            let key: string = 'overscore_' +i;
            let buttonText: string = overscores[i].buttonText;
           // let text: string = overscores[i].elementText;
          //  let classText: string = overscores[i].classText;
            //convert unicode to html entity
           

            if (buttonText.indexOf('U+') !== -1) {

                buttonText = StyleUtil.convertUnicodeToHtml(buttonText);
                // console.log(text);
            }

            buttons.push(<button key={key} className='cellButton' onClick={() => {
                this.mc.addOverScore(overscores[i]);
            }}>
                <span dangerouslySetInnerHTML={{ __html: buttonText }}></span>
            </button>)
        }
        return buttons;
    }
    render() {
        return (<div className='symButtons'>
            {this.getButtons()}
        </div>)
    }
}
export default OverScorePanel;