import React, { Component } from 'react';
import { TCellVo } from '../dataObjs/TCellVo';
import { TGridVo } from '../dataObjs/TGridVo';
import { PosVo } from '../dataObjs/PosVo';
import { GridUtil } from '../util/GridUtil';
interface MyProps { grid: TGridVo, selectCallback: Function, startDragCallback: Function, overCallback: Function, currentRow: number, currentCol: number, selectedIndex: number, preview: number, multi: PosVo[], startDrag: PosVo, overDrag: PosVo }
interface MyState { grid: TGridVo, currentRow: number, currentCol: number, selectedIndex: number, preview: number, multi: PosVo[], startDrag: PosVo, overDrag: PosVo }
class GrowGrid extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        (window as any).gg = this;
        this.state = { grid: this.props.grid, currentRow: props.currentRow, currentCol: props.currentCol, selectedIndex: props.selectedIndex, preview: props.preview, multi: props.multi, startDrag: props.startDrag, overDrag: props.overDrag };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.grid !== this.props.grid) {
            this.setState({ grid: this.props.grid });
        }
        if (prevProps.currentRow !== this.props.currentRow) {
            this.setState({ currentRow: this.props.currentRow });
        }
        if (prevProps.currentCol !== this.props.currentCol) {
            this.setState({ currentCol: this.props.currentCol });
        }
        if (prevProps.selectedIndex !== this.props.selectedIndex) {
            this.setState({ selectedIndex: this.props.selectedIndex });
        }
        if (prevProps.preview !== this.props.preview) {
            this.setState({ preview: this.props.preview });
        }
        if (prevProps.multi !== this.props.multi) {
            this.setState({ multi: this.props.multi });
        }
        if (prevProps.startDrag !== this.props.startDrag) {
            this.setState({ startDrag: this.props.startDrag });
        }
        if (prevProps.overDrag !== this.props.overDrag) {
            this.setState({ overDrag: this.props.overDrag });
        }
    }
    private checkMulti(row: number, col: number): boolean {
        let multi: PosVo[] = this.state.multi;
        let count: number = multi.length;
        for (let i: number = 0; i < count; i++) {
            let pos: PosVo = multi[i];
            if (pos.row === row && pos.col === col) {
                return true;
            }
        }
        return false;
    }
    getGridHtml() {
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData;
        let colCount = gridData.length;
        let rowHtml: JSX.Element[] = [];
        let gridHtml: JSX.Element[] = [];
        for (let i: number = 0; i < colCount; i++) {
            let col = gridData[i];
            rowHtml = [];
            let rowCount = col.length;
            for (let j: number = 0; j < rowCount; j++) {
                let key: string = "grid_" + i.toString() + '_' + j.toString();
                let cell: TCellVo = col[j];
                cell.row = j;
                cell.col = i;
                cell.selected = false;
                cell.selectedIndex = this.state.selectedIndex;
                if (j === this.state.currentRow && i === this.state.currentCol && this.state.multi.length === 0) {
                    cell.selected = true;
                }
                if (cell.selected === false && this.checkMulti(j, i)) {
                    cell.selected = true;
                    cell.selectedIndex = 0;
                }

                
                //check if the cell is between the start and end drag cells
                //regarless of the direction of the drag
                cell.dragOver = GridUtil.checkCellInRange(this.state.startDrag.row, this.state.startDrag.col, this.state.overDrag.row, this.state.overDrag.col, j, i);
                
                


                let cellHtml: JSX.Element = cell.getHtml(key, this.props.selectCallback, this.props.startDragCallback, this.props.overCallback, this.state.preview);

                //cellHtml.props.onClick = () => { this.props.selectCallback(j,i) };
                rowHtml.push(cellHtml);
            }
            let rowKey: string = "row_" + i.toString();
            gridHtml.push(<div key={rowKey}>{rowHtml}</div>);
        }

        return gridHtml;
    }

    render() {
        
        return (
            <div className="growgrid" id="growgrid">
                {this.getGridHtml()}
            </div>
        )
    }
}
export default GrowGrid;