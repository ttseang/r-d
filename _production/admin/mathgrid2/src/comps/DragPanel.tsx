import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import StylePanel2 from '../panels/StylePanel2';
import { PanelActions, PanelType, StyleType } from '../mc/Constants';
import { MainController } from '../mc/MainController';
import { MainStorage } from '../mc/MainStorage';
import MovePanel from '../panels/MovePanel';
import Templates from './Templates';
import SymbolPanel from '../panels/SymbolPanel';
import ButtonPanel from '../panels/ButtonPanel';
import BorderPanel from '../panels/BorderPanel';
import DebugPanel from '../panels/DebugPanel';
import ClipPanel from './ClipPanel';
import ScratchPanel from './ScratchPanel';
import OverlayPanel from '../panels/OverlayPanel';
import OverScorePanel from '../panels/OverScorePanel';
interface MyProps { posX: number; posY: number, width: number, height: number, type: string, index: number, callback: Function }
interface MyState { posX: number; posY: number, width: number, height: number, index: number }
class DragPanel extends Component<MyProps, MyState>
{
    private moveFunc: any;
    private offsetLeft: number = 0;
    private offsetTop: number = 0;
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props)
        this.state = { posX: props.posX, posY: props.posY, width: props.width, height: props.height, index: props.index }
        this.moveFunc = this.updatePos.bind(this);
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.posX !== this.props.posX) {
            this.setState({ posX: this.props.posX });
        }
        if (prevProps.posY !== this.props.posY) {
            this.setState({ posY: this.props.posY });
        }
        if (prevProps.width !== this.props.width) {
            this.setState({ width: this.props.width });
        }
        if (prevProps.height !== this.props.height) {
            this.setState({ height: this.props.height });
        }
        if (prevProps.index !== this.props.index) {
            this.setState({ index: this.props.index });
        }
    }
    onMouseDown(e: React.MouseEvent<HTMLDivElement>) {       

        this.props.callback(this.props.type);
        //get the mouse position relative to the drag panel
        let parent: HTMLDivElement = e.currentTarget.parentElement as HTMLDivElement;
        if (!parent) {
            return;
        }
        this.offsetLeft = e.clientX - parent.offsetLeft;
        this.offsetTop = e.clientY - parent.offsetTop;

        document.addEventListener("mousemove", this.moveFunc);
        document.addEventListener("mouseup", this.onMouseUp.bind(this));
    }
    onMouseUp() {
        document.removeEventListener("mousemove", this.moveFunc);
    }
    updatePos(e: React.MouseEvent<HTMLDivElement>) {
        let mouseX: number = e.clientX;
        let mouseY: number = e.clientY;
        mouseX -= this.offsetLeft;
        mouseY -= this.offsetTop;

        this.ms.updatePanelPos(this.props.type, mouseX, mouseY, true);

        //set the position of the drag panel
        this.setState({ posX: mouseX, posY: mouseY });
    }
    getPanel() {
        switch (this.props.type) {
            case PanelType.colors:
                return <StylePanel2 panelType={StyleType.color}></StylePanel2>
            case PanelType.fonts:
                return <StylePanel2 panelType={StyleType.font}></StylePanel2>
            case PanelType.sizes:
                return <StylePanel2 panelType={StyleType.size}></StylePanel2>
            case PanelType.lines:
                return <BorderPanel></BorderPanel>
            //  return <StylePanel2 panelType={StyleType.cell}></StylePanel2>
            case PanelType.move:
                return <MovePanel panelAction={PanelActions.Move}></MovePanel>
            case PanelType.shift:
                return <MovePanel panelAction={PanelActions.Shift}></MovePanel>
            case PanelType.insert:
                return <MovePanel panelAction={PanelActions.Insert}></MovePanel>
            case PanelType.delete:
                return <ButtonPanel panelAction={PanelActions.Delete}></ButtonPanel>
            case PanelType.adjust:
                return <MovePanel panelAction={PanelActions.Adjust}></MovePanel>
            case PanelType.templates:
                return <Templates></Templates>
            case PanelType.symbols:
                return <SymbolPanel></SymbolPanel>
            case PanelType.debug:
                return <DebugPanel></DebugPanel>
            case PanelType.clipboard:
                return <ClipPanel></ClipPanel>
            case PanelType.scratchpad:
                return <ScratchPanel></ScratchPanel>

            case PanelType.overlays:
                return <OverlayPanel></OverlayPanel>
            case PanelType.overscores:
                return <OverScorePanel></OverScorePanel>
            default:
                return "no panel";

        }
    }
    getTitle() {
        switch (this.props.type) {
            case PanelType.colors:
                return "Colors";
            case PanelType.fonts:
                return "Fonts";
            case PanelType.sizes:
                return "Sizes";

            case PanelType.lines:
                return "Borders";
            case PanelType.move:
                return "Move";
            case PanelType.shift:
                return "Shift";
            case PanelType.insert:
                return "Insert";
            case PanelType.delete:
                return "Delete";
            case PanelType.adjust:
                return "Adjust";

            case PanelType.templates:
                return "Templates";

            case PanelType.symbols:
                return "Symbols";

            case PanelType.debug:
                return "Debug";

            case PanelType.clipboard:
                return "Clipboard";

            case PanelType.scratchpad:
                return "Scratchpad";

            case PanelType.overlays:
                return "Overlay Images";

            default:
                return "Drag Panel";
        }
    }
    closeMe() {
        this.mc.closePanel(this.props.type);
    }
    render() {
        let zIndex: number = 1000 + this.state.index;
        let posStyle: React.CSSProperties = { position: "absolute", left: this.state.posX, top: this.state.posY, zIndex: zIndex };
        posStyle.width = this.state.width + "vw";
        posStyle.height = this.state.height + "vh";

        return (<Card className='dragPanel' style={posStyle}>
            <Card.Header onMouseDown={this.onMouseDown.bind(this)} className='dragPanelHeader'>{this.getTitle()} <i className="btnClose fas fa-times" onClick={this.closeMe.bind(this)}></i></Card.Header>
            <Card.Body>
                {this.getPanel()}
            </Card.Body>
        </Card>
        )
    }
}
export default DragPanel;