import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { TCellVo } from '../dataObjs/TCellVo';
import { MainController } from '../mc/MainController';
import { Button, ListGroup, ListGroupItem } from 'react-bootstrap';
interface MyProps { }
interface MyState { scratchPad: TCellVo[] | null, mode: number }
class ScratchPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { scratchPad: this.ms.scratchPad, mode: 0 };
        this.mc.scratchPadChanged = this.scratchPadChanged.bind(this);
    }
    scratchPadChanged() {
        this.setState({ scratchPad: this.ms.scratchPad });
    }
    doPaste(index: number) {
        let cell: TCellVo = this.state.scratchPad![index];
        this.mc.setCell(cell);
    }
    getCells() {
        if (this.state.mode === 1) {
            return this.getEditCells();
        }
        let scratchCells: TCellVo[] | null = this.state.scratchPad;
        if (scratchCells) {
            let cells: JSX.Element[] = [];
            for (let i: number = 0; i < scratchCells.length; i++) {
                cells.push(scratchCells[i].getHtml("scratch" + i.toString(), () => { this.doPaste(i) }, () => { }, () => { }, 0));
            }
            return (<div>               
                <div className='scratchPad'>
                    {cells}
                </div>
                <Button size="sm" variant='primary' onClick={() => { this.setState({ mode: 1 }) }}>Edit</Button>
            </div>)
        }
        return "No cells have been saved";
    }
    deleteScratch(index: number) {
    
        let scratchCells: TCellVo[] | null = this.state.scratchPad;
        if (scratchCells) {
            scratchCells.splice(index, 1);
            this.setState({ scratchPad: scratchCells });
            this.ms.setScratchPad(scratchCells);
        }
    }
    getEditCells() {
        let scratchCells: TCellVo[] | null = this.state.scratchPad;
        let bars: JSX.Element[] = [];
        if (scratchCells) {
            for (let i: number = 0; i < scratchCells.length; i++) {
                let key: string = "scratch" + i.toString();
                bars.push(
                    <ListGroupItem  key={key}>
                        <div className='scratchPadRow'>
                            {scratchCells[i].getHtml("scratch" + i.toString(), () => { this.doPaste(i) }, () => { }, () => { }, 0)}
                            <Button size="sm" variant='danger' onClick={() => { this.deleteScratch(i) }}>X</Button>
                        </div>
                    </ListGroupItem>);
            }
            return (<div>
                <ListGroup className='scratchGroup'>
                    {bars}
                </ListGroup>
                <Button size="sm" variant='primary' onClick={() => { this.setState({ mode: 0 }) }}>Done</Button>
            </div>)
        }
    }
    render() {
        return (<div>
            {this.getCells()}
        </div>)
    }
}
export default ScratchPanel;