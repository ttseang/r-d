import React, { Component } from 'react';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
import { MainController } from '../mc/MainController';
import { MainStorage } from '../mc/MainStorage';
import { PanelType } from '../mc/Constants';
interface MyProps { }
interface MyState { }
class TopMenu extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    private doAction(menuIndex: number, actionIndex: number) {
        
        switch (actionIndex) {
            case 0:
                
                localStorage.setItem("saveData", "");
                (window as any).location.reload();
                break;

            case 2:
                
                this.mc.changeScreen(2);
                break;
            case 1:
                
                this.mc.changeScreen(1);
                break;
        }
    }
    clickCheck(e: React.ChangeEvent<HTMLInputElement>) {
        let checked: boolean = e.target.checked;
       
        this.ms.applyAdjustToCell = checked;
       
    }
    showOverlays()
    {
        this.mc.addPanel(PanelType.overlays);
        this.mc.toggleOverlay();
    }
    showOverScores()
    {
        this.mc.addPanel(PanelType.overscores);
    }
    render() {
        return (<div className='topMenu'>
            <ButtonGroup>
                <Dropdown>
                    <Dropdown.Toggle variant="text" id="dropdown-basic">
                        File
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => { this.doAction(0, 0) }}>New</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 1) }}>Save</Dropdown.Item>
                        <Dropdown.Item onClick={() => { this.doAction(0, 2) }}>Open</Dropdown.Item>
                    </Dropdown.Menu>
                    <button className='btnEye' onClick={() => { this.mc.togglePreview() }}><i className="fas fa-eye"></i></button>
                    <button className='btnEye' onClick={() => { this.mc.showGuideLines(2) }}><i className="fas fa-grip-horizontal"></i></button>
                    <button className='btnEye' onClick={() => { this.mc.showGuideLines(3) }}><i className="fas fa-grip-vertical"></i></button>
                    <button className='btnEye' onClick={() => { this.mc.showGuideLines(4) }}><i className="fas fa-border-all"></i></button>
                    <span>||</span>
                    <span><input type='checkbox' onChange={this.clickCheck.bind(this)} />Cell</span>
                    <button className='btnEye' title='adjust left' onClick={() => { this.mc.updateAdjust(-0.05, 0); }}><i className="fas fa-chevron-left"></i></button>
                    <button className='btnEye' title='adjust right' onClick={() => { this.mc.updateAdjust(0.05, 0); }}><i className="fas fa-chevron-right"></i></button>
                    <button className='btnEye' title='adjust up' onClick={() => { this.mc.updateAdjust(0, -0.05); }}><i className="fas fa-chevron-up"></i></button>
                    <button className='btnEye' title='adjust down' onClick={() => { this.mc.updateAdjust(0, 0.05); }}><i className="fas fa-chevron-down"></i></button>
                    <span>||</span>
                    <button className='btnEye' title='clear cell' onClick={() => { this.mc.clearCell() }}><i className="fas fa-broom"></i></button>
                    <button className='btnEye' title='colors' onClick={() => { this.mc.addPanel(PanelType.colors) }}><i className="fas fa-palette"></i></button>
                    <button className='btnEye' title='font' onClick={() => { this.mc.addPanel(PanelType.fonts) }}><i className="fas fa-font"></i></button>
                    <button className='btnEye' title='sizes' onClick={() => { this.mc.addPanel(PanelType.sizes) }}><i className="fas fa-text-height"></i></button>

                    <button className='btnEye' title='borders' onClick={() => { this.mc.addPanel(PanelType.lines) }}><i className="fas fa-border-all"></i></button>
                    <button className='btnEye' title='move' onClick={() => { this.mc.addPanel(PanelType.move) }}><i className="fas fa-arrows-alt"></i></button>
                    <button className='btnEye' title='adjust' onClick={() => { this.mc.addPanel(PanelType.adjust) }}><i className="fas fa-screwdriver"></i></button>

                    <button className='btnEye' title='shift' onClick={() => { this.mc.addPanel(PanelType.shift) }}><i className="far fa-hand-point-right"></i></button>
                    <button className='btnEye' title='insert' onClick={() => { this.mc.addPanel(PanelType.insert) }}><i className="fas fa-plus-circle"></i></button>
                    <button className='btnEye' title='delete' onClick={() => { this.mc.addPanel(PanelType.delete) }}><i className="fas fa-minus-circle"></i></button>


                    <button className='btnEye' title='clipboard' onClick={() => { this.mc.addPanel(PanelType.clipboard) }}><i className="fas fa-clipboard"></i></button>
                    <button className='btnEye' title='cell templates' onClick={() => { this.mc.addPanel(PanelType.templates) }}><i className="fas fa-file-alt"></i></button>
                    <button className='btnEye' title='symbols' onClick={() => { this.mc.addPanel(PanelType.symbols) }}><i className="fas fa-square-root-alt"></i></button>
                    <button className='btnEye' title='debug' onClick={() => { this.mc.addPanel(PanelType.debug) }}><i className="fas fa-bug"></i></button>
                    <span>||</span>

                
                    <button className='btnEye' title='cut' onClick={() => { this.mc.cut() }}><i className="fas fa-cut"></i></button>
                    <button className='btnEye' title='copy' onClick={() => { this.mc.copy() }}><i className="fas fa-copy"></i></button>
                    <button className='btnEye' title='paste' onClick={() => { this.mc.paste() }}><i className="fas fa-paste"></i></button>
                    <button className='btnEye' title='undo' onClick={() => { this.mc.addPanel(PanelType.scratchpad) }}><img src='./icons/scratchpad.png' alt='scratchpad' title='scratch pad' /></button>
                    <button className='btnEye' title='undo' onClick={() => { this.mc.addScratch() }}><img src='./icons/addpad.png' alt='scratchpad' title='add to scratch pad' /></button>
                    <span>||</span>
                    <button className='btnEye' title='undo' onClick={() => { this.mc.undo() }}><i className="fas fa-undo"></i></button>
                    <button className='btnEye' title='redo' onClick={() => { this.mc.redo() }}><i className="fas fa-redo"></i></button>
                    <span>||</span>
                    <button className='overscores' title='overscores' onClick={() => { this.mc.addPanel(PanelType.overscores) }}><i className="fas fa-underline"></i></button>
                   {/*  <span>||</span> */}
                   
                    {/* <button className='btnEye' title='overlay' onClick={() => { this.showOverlays() }}><i className="fas fa-image"></i></button> */}
                </Dropdown>
                <Dropdown>
                </Dropdown>
            </ButtonGroup>
        </div>)
    }
}
export default TopMenu;