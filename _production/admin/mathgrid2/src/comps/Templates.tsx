import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { ButtonGroupVo } from '../dataObjs/ButtonGroupVo';
import { ButtonVo } from '../dataObjs/ButtonVo';
import { MainController } from '../mc/MainController';
import { TCellVo } from '../dataObjs/TCellVo';
import { DefUtil } from '../util/DefUtil';
interface MyProps { }
interface MyState { selectedButtonType: string }
class Templates extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { selectedButtonType: ''};
        this.mc.updateSelectedCell = this.cellSelected.bind(this);
    }
    cellSelected(cell: TCellVo) {
        console.log('type', cell);
        let defUtil:DefUtil = DefUtil.getInstance();
        let lastClass:string = cell.cellClasses[cell.cellClasses.length-1];
        let type:string = defUtil.getTempType(lastClass);
        console.log('type', type);
        this.setState({ selectedButtonType: cell.type as string });
    }

    getButtons() {
        let group: ButtonGroupVo = this.ms.buttonGroups[0];
        let buttons: JSX.Element[] = [];
        for (let j = 0; j < group.buttons.length; j++) {
            let button: ButtonVo = group.buttons[j];
            let cls: string = 'longButton';
          //  console.log('getButtons',button.cellType,button.tempType);
            if (button.cellType === this.state.selectedButtonType) {
                cls = 'longButton selected';
            }

            let key: string = group.groupName + '_' + button.text;
            buttons.push(<button key={key} className={cls} onClick={() => {
                //  this.props.callback(button.cellType,button.tempType);
                this.mc.formatCell(button.cellType, button.tempType);
            }}>{button.text}</button>)
        }
        return buttons;
    }
    render() {
        return (<div className='editButtons'>
            {
                this.getButtons()
            }
        </div>)
    }
}
export default Templates;