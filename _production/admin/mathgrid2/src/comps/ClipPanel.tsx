import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { TCellVo } from '../dataObjs/TCellVo';
import { MainController } from '../mc/MainController';
import { Button } from 'react-bootstrap';
interface MyProps { }
interface MyState {copyCell:TCellVo | null }
class ClipPanel extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController=MainController.getInstance();
    constructor(props: MyProps) {
        super(props);
        this.state = {copyCell:this.ms.copyCell};
        this.mc.clipboardChanged=this.clipboardChanged.bind(this);
    }
    clipboardChanged() {
        this.setState({copyCell:this.ms.copyCell});
    }
    doPaste()
    {
        this.mc.paste();
    }
    doCopy()
    {
        this.mc.copy();
    }
    saveToScratchPad()
    {
        if (this.ms.copyCell)
        {
            this.ms.addToScratchPad(this.ms.copyCell);
            this.mc.scratchPadChanged();
        }       
    }
    getContent() {
        if (this.state.copyCell) {
            return this.state.copyCell.getHtml("copypreview", () => {this.doPaste() }, () => { }, () => { }, 0);
        }
        return "no content in clipboard";
    }
    render() {
        return (
            <div className="clipPanel">
                {this.getContent()}
                <Button size='sm' onClick={this.saveToScratchPad.bind(this)}>Save To Scratch Pad</Button>
                <Button size='sm' onClick={this.doCopy.bind(this)}>Copy Current Cell</Button>
            </div>)
    }
}
export default ClipPanel;