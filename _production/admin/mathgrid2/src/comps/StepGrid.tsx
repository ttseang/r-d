import React, { Component } from 'react';
import { TCellVo } from '../dataObjs/TCellVo';
import { TGridVo } from '../dataObjs/TGridVo';
interface MyProps { grid: TGridVo,gridClass:string}
interface MyState { grid: TGridVo }
class StepGrid extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        (window as any).gg = this
        this.state = {grid:this.props.grid };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
         if (prevProps.grid !== this.props.grid) {
            this.setState({ grid: this.props.grid });
        }
    
    }
    getGridHtml() {
        let grid: TGridVo= this.state.grid;
        let gridData =grid.gridData;
        let rowCount = gridData.length;
        let rowHtml: JSX.Element[] = [];
        let gridHtml: JSX.Element[] = [];
        for (let i: number = 0; i < rowCount; i++) {
            let row = gridData[i];
            rowHtml = [];
            let colCount = row.length;
            for (let j: number = 0; j < colCount; j++) {
                let key: string ="grid_"+ i.toString() + '_' + j.toString();
                let cell: TCellVo = row[j];
                cell.row=j;
                cell.col=i;

                let cellHtml: JSX.Element = cell.getStepHtml(key);
               
                //cellHtml.props.onClick = () => { this.props.selectCallback(j,i) };
                rowHtml.push(cellHtml);
            }
            let rowKey: string = "row_" + i.toString();
            gridHtml.push(<div key={rowKey}>{rowHtml}</div>);
        }

        return gridHtml;
    }
    render() {
        //StepGrid
        return (<div>            
            <div className="stepGrid">

                {this.getGridHtml()}

            </div>
        </div>)
    }
}
export default StepGrid;