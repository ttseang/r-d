import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { TGridVo } from '../dataObjs/TGridVo';
import { MainController } from '../mc/MainController';
import StepGrid from './StepGrid';

interface MyProps { grid: TGridVo, index: number,selected:boolean }
interface MyState { grid: TGridVo,selected:boolean,index:number}
class StepCard extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { grid: props.grid,selected:props.selected,index:props.index };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.grid !== this.props.grid) {
            this.setState({ grid: this.props.grid });
        }
        if (prevProps.selected !== this.props.selected) {
            this.setState({ selected: this.props.selected });
        }
        if (prevProps.index !== this.props.index) {
            this.setState({ index: this.props.index });
        }
    }
    render() {
        let classes:string = 'stepCard';
        if(this.state.selected)
        {
            classes += ' selected';
        }
        return (<Card className={classes} onClick={() => { this.mc.changeStep(this.props.index) }}>
           {<Card.Header>Step {this.state.index + 1}</Card.Header>}
            <StepGrid grid={this.state.grid} gridClass={'stepGrid'} />
          
        </Card>)
    }
}
export default StepCard;