import React, { Component } from 'react';
import { OverlayVo } from '../dataObjs/OverlayVo';
import { MainController } from '../mc/MainController';
interface MyProps {active: boolean;}
interface MyState {active: boolean,overlays:OverlayVo[]}
class ImageOverlay extends Component<MyProps, MyState>
{
       private currentOverlay:OverlayVo | null=null;
       private moveListener:any=this.mouseMove.bind(this);
       private mouseUpListener:any=this.mouseUp.bind(this);
       private mc:MainController=MainController.getInstance();

        constructor(props: MyProps) {
            super(props);

            let startOverlays:OverlayVo[]=[];
           // startOverlays.push(new OverlayVo(0,0,10,10,"square-root"));

            this.state = {active: this.props.active,overlays:startOverlays};

            this.mc.addOverlay=this.addOverlay.bind(this);
        }

        componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
            if (prevProps.active !== this.props.active) {
                this.setState({active: this.props.active});
            }
        }
        addOverlay(path:string)
        {
            let overlays:OverlayVo[]=this.state.overlays;
            overlays.push(new OverlayVo(0,0,10,10,path));
            this.setState({overlays:overlays});
            this.mc.setOverlay(true);
        }
        overlayClick(index:number)
        {
            console.log("overlay click "+index);
            this.currentOverlay=this.state.overlays[index];
            document.addEventListener("mousemove", this.moveListener);
            document.addEventListener("mouseup", this.mouseUpListener);
        }
        mouseMove(e:React.MouseEvent<SVGSVGElement, MouseEvent>)
        {
           if (this.currentOverlay !== null) {
                //let rect = (e.target as any).getBoundingClientRect();
                let parentRect = (e.target as any).parentElement.getBoundingClientRect();
                let moveX:number = e.movementX;
                let moveY:number = e.movementY;

                console.log("moveX "+moveX+" moveY "+moveY);
                
                let x:number = this.currentOverlay.x + (moveX / parentRect.width * 100);
                let y:number = this.currentOverlay.y + (moveY / parentRect.height * 100);


                 this.currentOverlay.x = x;
                this.currentOverlay.y = y;
                this.setState({overlays:this.state.overlays});
           }
        }
        mouseUp(e:React.MouseEvent<SVGSVGElement, MouseEvent>)
        {
            this.currentOverlay=null;
            document.removeEventListener("mousemove", this.moveListener);
            document.removeEventListener("mouseup", this.mouseUpListener);
        }
        getOverlays()
        {
            let overlays:JSX.Element[]=[];
            for(let i=0;i<this.state.overlays.length;i++)
            {
                let key:string="overlay"+i;
                overlays.push(this.state.overlays[i].getHtml(i,key,this.overlayClick.bind(this)));
            }
            return overlays;
        }
    render() {

        //set pointer events to none if not active or pointer events to all if active
        let pointerEvents = this.state.active ? "all" : "none";
        let style:any={"pointerEvents": pointerEvents};
        //background color
        //light green if active, transparent if not
        style.backgroundColor = this.state.active ? "rgba(0,255,0,0.2)" : "transparent";
        return (
            <svg width="100%" height="100%" style={style} viewBox="0 0 100 100" preserveAspectRatio="none" className='floatingSymbol'>
                {this.getOverlays()}
            </svg>
        )
    }
}
export default ImageOverlay;