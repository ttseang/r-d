import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import { ButtonGroupVo } from '../dataObjs/ButtonGroupVo';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import { ButtonVo } from '../dataObjs/ButtonVo';
import 'react-tabs/style/react-tabs.css';
import SymbolPanel from '../panels/SymbolPanel';

interface MyProps { }
interface MyState { }
class TemplateButtons extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();
    private tabNames: string[] = [];
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    getTabs() {
        let buttonGroups: ButtonGroupVo[] = this.ms.buttonGroups;
        let tabs: JSX.Element[] = [];
        let buttons: JSX.Element[] = [];
        let tabPanels: JSX.Element[] = [];
        //use react-tabs
        for (let i = 0; i < buttonGroups.length; i++) {
            let group: ButtonGroupVo = buttonGroups[i];
            let tabKey: string = 'tab_' + group.groupName;
            tabs.push(<Tab key={tabKey}>{group.groupName}</Tab>)

            buttons = [];
            for (let j = 0; j < group.buttons.length; j++) {
                let button: ButtonVo = group.buttons[j];

                let key: string = group.groupName + '_' + button.text;
                buttons.push(<button key={key} className='longButton' onClick={() => {
                    //  this.props.callback(button.cellType,button.tempType);
                    this.mc.formatCell(button.cellType, button.tempType);
                }}>{button.text}</button>)
            }
            let panelKey: string = 'panel_' + group.groupName;

            if (group.groupName === 'symbols') {
                tabPanels.push(<TabPanel key={panelKey}>
                    <SymbolPanel />
                </TabPanel>);
            }
            else {


                tabPanels.push(<TabPanel key={panelKey}>
                    <div className='editButtons'>
                        {buttons}
                    </div>
                </TabPanel>);
            }
        }
        /*
        return (
            <Tabs>
                <TabList>
                    {tabs}
                </TabList>
                {tabPanels}
            </Tabs>
        ) */
        return <Tabs>
            <TabList>
                {tabs}
            </TabList>

            {tabPanels}
        </Tabs>
    }
    render() {
        return (<div>
            {
                this.getTabs()
            }
        </div>)
    }
}
export default TemplateButtons;