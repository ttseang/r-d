import { CellType } from "../mc/Constants";
import { MainStorage } from "../mc/MainStorage";
import { TCellExportVo } from "./TCellExportVo";
import { TCellVo } from "./TCellVo";
import { TGridExport } from "./TGridExportVo";

//gridData TCellVo[][]
export class TGridVo {
    public gridData: TCellVo[][];
    public rowCount: number;
    public colCount: number;
    public name: string = "step name";

    constructor(gridData: TCellVo[][]) {
        //gridData is a 2d array of TCellVo
        //each row is an array of TCellVo
        //columns are in the first dimension
        //rows are in the second dimension
        this.gridData = gridData;
        if (gridData.length===0)
        {
            this.rowCount=0;
            this.colCount=0;
        }
        else
        {
            this.rowCount = gridData[0].length;
            this.colCount = gridData.length;
        }
    }
    //clone
    public clone(): TGridVo {
        let gridData: TCellVo[][] = [];
        for (let i: number = 0; i < this.gridData.length; i++) {
            gridData.push([]);
            for (let j: number = 0; j < this.gridData[i].length; j++) {
                gridData[i].push(this.gridData[i][j].clone());
            }
        }
        return new TGridVo(gridData);
    }
    getRow(rowIndex: number): TCellVo[] {
        let row: TCellVo[] = [];
        for (let i: number = 0; i < this.gridData.length; i++) {
            row.push(this.gridData[i][rowIndex]);
        }
        return row;
    }
    //get cell
    public getCell(row: number, col: number): TCellVo {
        return this.gridData[col][row];
    }
    //set cell
    public setCell(row: number, col: number, cell: TCellVo): void {
        this.gridData[col][row] = cell;
    }
    //get row count
    public getRowCount(): number {
        return this.gridData.length;
    }
    //get col count
    public getColCount(): number {
        return this.gridData[0].length;
    }

    addRow(atStart: boolean = false) {
        //add a row to the grid
        //if atStart is true, add the row to the start of the grid
        //otherwise add the row to the end of the grid
        if (atStart) {
            this.insertRowAbove(0);
        }
        else {
            this.insertRowBelow(this.rowCount - 1);
        }
    }
    addColumn(atStart: boolean = false) {
        if (atStart) {
            this.insertColumnLeft(0);
        }
        else {
            this.insertColumnRight(this.colCount - 1);
        }
    }
    
    shiftUp() {
        //move all rows up one
        //the columns are in the first index
        //the rows are in the second index
        //so we need to move the rows up one
        for (let i: number = 0; i < this.gridData.length; i++) {
            //move the rows up one
            for (let j: number = 0; j < this.gridData[i].length - 1; j++) {
                let cell = this.gridData[i].pop();
                if (cell != null) {
                    this.gridData[i].unshift(cell);
                }
            }
        }

    }
    shiftDown() {
        //move all rows down one
        //the columns are in the first index
        //the rows are in the second index
        //so we need to move the rows down one
        for (let i: number = 0; i < this.gridData.length; i++) {
            //move the rows down one
            for (let j: number = 0; j < this.gridData[i].length - 1; j++) {
                let cell = this.gridData[i].shift();
                if (cell != null) {
                    this.gridData[i].push(cell);
                }
            }
        }
    }
    shiftLeft() {
        //move all columns left one
        //the columns are in the first index
        //the rows are in the second index
        //so we need to move the columns left one
        let firstCol = this.gridData.shift();
        if (firstCol != null) {
            this.gridData.push(firstCol);
        }
    }
    shiftRight() {
        //move all columns right one
        //the columns are in the first index
        //the rows are in the second index
        //so we need to move the columns right one
        let lastCol = this.gridData.pop();
        if (lastCol != null) {
            this.gridData.unshift(lastCol);
        }
    }
    shiftColumnUp(column: number) {
        let columnData = this.gridData[column];
        let firstCell = columnData.shift();
        if (firstCell != null) {
            columnData.push(firstCell);
        }
    }
    shiftColumnDown(column: number) {
        let columnData = this.gridData[column];
        let lastCell = columnData.pop();
        if (lastCell != null) {
            columnData.unshift(lastCell);
        }

    }
    shiftRowLeft(row: number) {
        let rowData = this.getRow(row);
        let firstCell = rowData.shift();
        if (firstCell != null) {
            rowData.push(firstCell);
        }
        for (let i: number = 0; i < this.gridData.length; i++) {
            this.gridData[i][row] = rowData[i];
        }


    }
    shiftRowRight(row: number) {
        let rowData = this.getRow(row);
        let lastCell = rowData.pop();
        if (lastCell != null) {
            rowData.unshift(lastCell);
        }
        for (let i: number = 0; i < this.gridData.length; i++) {
            this.gridData[i][row] = rowData[i];
        }
    }

    insertRowAbove(row: number) {
        for (let i: number = 0; i < this.colCount; i++) {
            let cell = TCellVo.createEmptyCell();
            this.gridData[i].splice(row, 0, cell);
        }
        this.rowCount++;
    }
    insertRowBelow(row: number) {
        for (let i: number = 0; i < this.colCount; i++) {
            let cell = TCellVo.createEmptyCell();
            this.gridData[i].splice(row + 1, 0, cell);
        }
        this.rowCount++;
    }
    insertColumnLeft(col: number) {

        let columnData = [];
        for (let i: number = 0; i < this.rowCount; i++) {
            let cell = TCellVo.createEmptyCell();
            columnData.push(cell);
        }
        this.gridData.splice(col, 0, columnData);
        this.colCount++;
    }
    insertColumnRight(col: number) {
        let columnData = [];
        for (let i: number = 0; i < this.rowCount; i++) {
            let cell = TCellVo.createEmptyCell();
            columnData.push(cell);
        }
        this.gridData.splice(col + 1, 0, columnData);
        this.colCount++;
    }
    deleteColumn(col: number) {
        console.log("delete col="+col);
        console.log("col count="+this.colCount);
        //delete a col from the grid based on the col index
        //if the col index is out of bounds, do nothing
        if (col < 0 || col >= this.colCount) {
            return;
        }
        //remove the col from the grid
        this.gridData.splice(col, 1);
        this.colCount--;

    }
    deleteRow(row: number) {
        console.log("delete row="+row);
        //delete a row from the grid based on the row index
        //if the row index is out of bounds, do nothing
        if (row < 0 || row >= this.rowCount) {
            return;
        }
        //remove the column from the grid
        for (let i: number = 0; i < this.gridData.length; i++) {
            this.gridData[i].splice(row, 1);
        }
        this.rowCount--;
    }
    
    getExport()
    {
        let grid2:TCellExportVo[][]=[];
        for (let i:number=0;i<this.gridData.length;i++)
        {
            grid2[i]=[];
            for (let j:number=0;j<this.gridData[i].length;j++)
            {
                let cell:TCellVo = this.gridData[i][j];
                let xCell:TCellExportVo=cell.getExport();
                grid2[i][j]=xCell;
            }
        }
        let gridExport:TGridExport = new TGridExport(grid2);
        return gridExport;
    }
    loadFromData(data:any)
    {
        let ms:MainStorage=MainStorage.getInstance();

        let gridData:TCellVo[][]=[];
        for (let i:number=0;i<data.length;i++)
        {
            gridData[i]=[];
            for (let j:number=0;j<data[i].length;j++)
            {
                let cellData:any=data[i][j];
                let text:string[]=cellData.text;
                let cellClasses:string[]=cellData.cellClasses;
                let textClasses:string[][]=cellData.textClasses;
                let rowSize:number=cellData.rowSize;
                let colSize:number=cellData.colSize;
                let type:string=cellData.type;
                let adjustX:number[]=cellData.adjustX;
                let adjustY:number[]=cellData.adjustY;
                let cellType:CellType=ms.stringToConstants(type);
                let cell:TCellVo=new TCellVo(text,colSize,rowSize,textClasses,cellClasses,cellType);
                cell.adjustX=adjustX;
                cell.adjustY=adjustY;

                if (cellData.template!==undefined)
                {
                    cell.template=cellData.template;
                }
               
                gridData[i][j]=cell;
            }
        }
        this.gridData=gridData;
        this.rowCount = gridData[0].length;
        this.colCount = gridData.length;
    }
    
}