export class OverlayVo
{
    public x:number;
    public y:number;
    public width:number;
    public height:number;
    public src:string;

    constructor(x:number,y:number,width:number,height:number,src:string)
    {
        this.x=x;
        this.y=y;
        this.width=width;
        this.height=height;
        this.src=src;
    }
    public getHtml(index:number,key:string,callback:Function):JSX.Element
    {
        //return a svg image with a click handler
        let src="overlay/"+this.src+".svg";
        let posX=this.x+"%";
        let posY=this.y+"%";
        let width=this.width+"%";
        let height=this.height+"%";
        return (
            
            <image key={key} onMouseDown={()=>{callback(index)}} href={src} x={posX} y={posY} height={height} width={width} preserveAspectRatio="none"></image>
        )
        
    }
}