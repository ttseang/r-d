import { TGridVo } from "./TGridVo";

export class HistoryVo {
    public currentStep: number;
    public steps: TGridVo[] = new Array<TGridVo>();

    constructor(currentStep: number, steps: TGridVo[]) {
        this.currentStep = currentStep;
        for (let i = 0; i < steps.length; i++) {
            this.steps.push(steps[i].clone());
        }
    }
}