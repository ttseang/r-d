import { CellType } from "../mc/Constants";
import { TCellExportVo } from "./TCellExportVo";


export class TCellVo {
    public text: string[];
    public rowSize: number;
    public colSize: number;
    public textClasses: string[][];
    public cellClasses: string[] = [];
    public row: number = 0;
    public col: number = 0;
    public selected: boolean = false;
    public dragOver: boolean = false;
    public selectedIndex: number = 0;
    public selectedIndexes: number[] = [];
    public type: CellType;
    public adjustX: number[] = []
    public adjustY: number[] = [];

    public myIndex: number = 0;
    public static cellIndex: number = 0;
    public template: string = "";

    constructor(text: string[], colSize: number, rowSize: number, textClasses: string[][] = [], cellClasses: string[] = [], type: CellType = CellType.Whole) {
        this.text = text;
        this.rowSize = rowSize;
        this.colSize = colSize;
        this.textClasses = textClasses;
        this.cellClasses = cellClasses;
        this.type = type;
        this.myIndex = TCellVo.cellIndex;
        TCellVo.cellIndex++;
    }
    static createEmptyCell(): TCellVo {
        const mt:TCellVo= new TCellVo([""], 1, 1, [["whole"]], [], CellType.Whole);
        mt.template="any";
        return mt;
    }
    public isEmpty(): boolean {
        if (this.text.length === 1 && this.text[0] === "") {
            return true;
        }
        return false;
    }
    public addSelectedIndex(index: number): void {
        if (this.selectedIndexes.indexOf(index) < 0) {
            this.selectedIndexes.push(index);
        }
        else
        {
            //remove it
            let i:number = this.selectedIndexes.indexOf(index);
            this.selectedIndexes.splice(i,1);
        }
    }
    public getAdjustX(): number {
        let val: number = this.adjustX[this.selectedIndex];
        if (isNaN(val) || val === undefined) {
            return 0;
        }
        return val;
    }
    public getAdjustY(): number {
        let val: number = this.adjustY[this.selectedIndex];
        if (isNaN(val) || val === undefined) {
            return 0;
        }
        return val;
    }
    public setAdjustX(val: number): void {
        if (val < -1 || val > 1) {
            return;
        }
        //round to 3 decimal places
        val = Math.round(val * 1000) / 1000;
        this.adjustX[this.selectedIndex] = val;
    }
    public setAdjustY(val: number): void {
        if (val < -1 || val > 1) {
            return;
        }
        //round to 3 decimal places
        val = Math.round(val * 1000) / 1000;
        this.adjustY[this.selectedIndex] = val;
    }

    public getAdjustX2(index:number): number {
        let val: number = this.adjustX[index];
        if (isNaN(val) || val === undefined) {
            return 0;
        }
        return val;
    }
    public getAdjustY2(index:number): number {
        let val: number = this.adjustY[index];
        if (isNaN(val) || val === undefined) {
            return 0;
        }
        return val;
    }
    public setAdjustX2(index:number,val: number): void {
        if (val < -1 || val > 1) {
            return;
        }
        //round to 3 decimal places
        val = Math.round(val * 1000) / 1000;
        this.adjustX[index] = val;
    }
    public setAdjustY2(index:number,val: number): void {
        if (val < -1 || val > 1) {
            return;
        }
        //round to 3 decimal places
        val = Math.round(val * 1000) / 1000;
        this.adjustY[index] = val;
    }


    getText(baseClass: string = "",callback:Function,downCallback:Function,overCallback:Function) {

        let textSpans: JSX.Element[] = [];
        for (let i: number = 0; i < this.text.length; i++) {
            let cellClass: string = baseClass + "_" + i.toString();
            if (this.selected && this.selectedIndex === i && this.type !== CellType.SquareRoot && this.selectedIndexes.length < 1) {
                cellClass += " currentCell";
            }
            if (this.selectedIndexes.indexOf(i) >= 0) {
                cellClass += " currentCell";
            }
            
            if (this.textClasses.length > i) {
                let tc: string[] = this.textClasses[i];
                if (!tc) {
                    tc = [];
                    this.textClasses[i] = tc;
                }
                let tcString: string = tc.join(" ");
                cellClass += " " + tcString;
            }
            let adjustX: number = 0;
            let adjustY: number = 0;

            if (this.adjustX.length > i) {
                adjustX = this.adjustX[i];
                if (isNaN(adjustX) || adjustX === undefined || adjustX === null) {
                    adjustX = 0;
                }
            }
            if (this.adjustY.length > i) {
                adjustY = this.adjustY[i];
                if (isNaN(adjustY) || adjustY === undefined || adjustY === null) {
                    adjustY = 0;
                }
            }

            //make a relative position style for the cell
            //use --cellHeight and --cellWidth to set the size of the cell 
            //multiply by the adjustX and adjustY to set the position
            //make css object

            let style: any = {};
            style.position = "relative";
            style.left = "calc(var(--cellWidth) * " + adjustX.toString() + ")";
            style.top = "calc(var(--cellHeight) * " + adjustY.toString() + ")";

            let key: string = "cell_" + i.toString();
            let text: string = this.text[i];
            if (text === " " && this.selected===true && this.selectedIndex===i) {
                text = "?";
            }
            if (adjustX !== 0 || adjustY !== 0) {
                textSpans.push(<span key={key} style={style} className={cellClass} onMouseDown={()=>{downCallback(this.row,this.col)}} onMouseUp={() => { callback(this.row, this.col,i) }} onMouseOver={()=>{overCallback(this.row,this.col)}}>{text}</span>);
            }
            else {
                textSpans.push(<span key={key} className={cellClass} onMouseDown={()=>{downCallback(this.row,this.col)}} onMouseUp={() => { callback(this.row, this.col,i) }} onMouseOver={()=>{overCallback(this.row,this.col)}}>{text}</span>);
            }
        }
        return textSpans;
    }
    addTextClass(textClass: string) {
        let tc: string[] = this.textClasses[this.selectedIndex];
        if (!tc) {
            tc = [];
            this.textClasses[this.selectedIndex] = tc;
        }
        if (tc.indexOf(textClass) < 0) {
            tc.push(textClass);
        }
    }
    addTextClassAt( index: number,textClass: string) {
        let tc: string[] = this.textClasses[index];
        if (!tc) {
            tc = [];
            this.textClasses[index] = tc;
        }
        if (tc.indexOf(textClass) < 0) {
            tc.push(textClass);
        }
    }

    removeTextClass(textClass: string) {
        let tc: string[] = this.textClasses[this.selectedIndex];
        if (tc) {
            let index: number = tc.indexOf(textClass);
            if (index >= 0) {
                tc.splice(index, 1);
            }
        }
    }
    addCellClass(cellClass: string) {
        if (this.cellClasses.indexOf(cellClass) < 0) {
            this.cellClasses.push(cellClass);
        }
    }
    removeCellClass(cellClass: string) {
        let index: number = this.cellClasses.indexOf(cellClass);
        if (index >= 0) {
            this.cellClasses.splice(index, 1);
        }
    }
    toggleCellClass(cellClass: string) {
        let index: number = this.cellClasses.indexOf(cellClass);
        if (index < 0) {
            this.cellClasses.push(cellClass);
        }
        else {
            this.cellClasses.splice(index, 1);
        }
    }
    toggleTextClass(textClass: string) {

        let tc: string[] = this.textClasses[this.selectedIndex];
        if (!tc) {
            tc = [];
            this.textClasses[this.selectedIndex] = tc;
        }
    
        let index: number = tc.indexOf(textClass);
        if (index < 0) {
            tc.push(textClass);
        }
        else {
            tc.splice(index, 1);
        }
    }
    clone(): TCellVo {
        let textCopy: string[] = this.text.slice();
        let textClassesCopy: string[][]=[];
        //make a copy of the textClasses
        for (let i: number = 0; i < this.textClasses.length; i++) {
            let tc: string[] = this.textClasses[i];
            if (tc) {
                let tcCopy: string[] = tc.slice();
                textClassesCopy.push(tcCopy);

            }
        }


        let cellClassesCopy: string[] = this.cellClasses.slice();
        let copyCell: TCellVo = new TCellVo(textCopy, this.colSize, this.rowSize, textClassesCopy, cellClassesCopy, this.type);
        copyCell.adjustX = this.adjustX.slice();
        copyCell.adjustY = this.adjustY.slice();
        copyCell.template = this.template;
        return copyCell;
    }
    getStepHtml(key: string) {
        let size: string = this.colSize.toString() + "_" + this.rowSize.toString();
        let classCopy: string[] = this.cellClasses.slice();
        let baseClass: string = "cell_" + size;
        let text: JSX.Element[] = this.getText(baseClass,()=>{},()=>{},()=>{});
        classCopy.push("stepCell");
        classCopy.push(baseClass);
        let classString = classCopy.join(" ");
        return <div key={key} className={classString}>{text}</div>
    }
    getHtml(key: string, callback: Function,downCallback:Function,overCallback:Function, preview: number) {
        
        let size: string = this.colSize.toString() + "_" + this.rowSize.toString();
        let classCopy: string[] = this.cellClasses.slice();
        let baseClass: string = "cell_" + size;
        let text: JSX.Element[] = this.getText(baseClass,callback,downCallback,overCallback);
       
        switch (preview) {
            case 0:
                classCopy.push("cell");
                break;
            case 1:
                classCopy.push("cellNoBorder");
                break;
            case 2:
                classCopy.push("cellGuideH");
                break;
            case 3:
                classCopy.push("cellGuideV");
                break;               
            case 4:
                classCopy.push("cellGuideHV");
                break;

        }

        /*  if (preview === false) {
             classCopy.push("cell");
         }
         else {
             classCopy.push("cellNoBorder");
         } */
        classCopy.push(baseClass);

        if (this.selected && this.text.length < 2 && preview!==1) {
            classCopy.push("currentCell");
        }
        if (this.dragOver && preview!==1) {
            classCopy.push("dragCell");
        }
        let classString = classCopy.join(" ");
        if (this.isEmpty())
        {
            return <div key={key} onMouseUp={() => { callback(this.row, this.col) }} onMouseDown={()=>{downCallback(this.row,this.col)}} onMouseOver={()=>{overCallback(this.row,this.col)}} className={classString}>{text}</div>
        }
        return <div key={key} className={classString}>{text}</div>
       /*  return <div key={key} onClick={() => { callback(this.col, this.row) }} className={classString}>{text}</div> */
        /* return <td className={this.classes.join(" ")} rowSpan={this.rowSize} colSpan={this.colSize}>{this.text}</td> */
    }
    getExport() {
        let exportCell: TCellExportVo = new TCellExportVo(this.text, this.colSize, this.rowSize, this.textClasses, this.cellClasses, this.type);
        exportCell.template = this.template;
        exportCell.adjustX = this.adjustX.slice();
        exportCell.adjustY = this.adjustY.slice();
        return exportCell;
    }

}