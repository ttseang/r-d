export class TCellExportVo {
    public text: string[];
    public rowSize: number;
    public colSize: number;
    public textClasses: string[][];
    public cellClasses: string[] = [];
    public type: string;
    public adjustX: number[] = [];
    public adjustY: number[] = [];
    public template: string = "";
    constructor(text: string[], colSize: number, rowSize: number, textClasses: string[][] = [], cellClasses: string[] = [], type: string = "text") {
        this.text = text;
        this.rowSize = rowSize;
        this.colSize = colSize;
        this.textClasses = textClasses;
        this.cellClasses = cellClasses;
        this.type = type;
    }
}