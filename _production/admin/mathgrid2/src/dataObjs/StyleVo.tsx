import { StyleType } from "../mc/Constants";

export class StyleVo {
    public styleName: string;
    public styleValue: string;
    public applyToCell: boolean;
    public type: StyleType;
    public buttonStyle: string[] = [];
    constructor(styleName: string, styleValue: string, applyToCell: boolean, type: StyleType, buttonStyle: string[] = []) {
        this.styleName = styleName;
        this.styleValue = styleValue;
        this.applyToCell = applyToCell;
        this.type = type;
        this.buttonStyle = buttonStyle;
    }
}