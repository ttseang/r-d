

export class FileVo
{
    public lti:string;
    
    public type:string;
    public eqstr:string;
  
    public data:any[];

    constructor(lti:string,type:string,eqstr:string,data:any[])
    {
        this.lti=lti;
        this.type=type;
        this.eqstr=eqstr;
        this.data=data;
    }
}