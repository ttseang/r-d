import { ButtonVo } from "./ButtonVo";

export class ButtonGroupVo
{
    public groupName:string;
    public groupKey:string;
    public buttons:ButtonVo[];
    constructor(groupName:string,groupKey:string,buttons:ButtonVo[])
    {
        this.groupName=groupName;
        this.groupKey=groupKey;
        this.buttons=buttons;
    }
}