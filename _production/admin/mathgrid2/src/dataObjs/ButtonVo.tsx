import { CellType, TempType } from "../mc/Constants";

export class ButtonVo
{
    public text:string;
    public cellType:CellType;
    public tempType:TempType;
    constructor(text:string,cellType:CellType,tempType:TempType)
    {
        this.text=text;
        this.cellType=cellType;
        this.tempType=tempType;
    }
}