import { TCellExportVo } from "./TCellExportVo";

export class TGridExport
{
    public gridData: TCellExportVo[][];
    public rowCount: number;
    public colCount: number;
    public name: string = "step name";

    constructor(gridData: TCellExportVo[][])
    {
        //gridData is a 2d array of TCellExportVo
        //each row is an array of TCellExportVo
        //columns are in the first dimension
        //rows are in the second dimension
        this.gridData = gridData;
        this.rowCount = gridData.length;
        this.colCount = gridData[0].length;
        
    }
}