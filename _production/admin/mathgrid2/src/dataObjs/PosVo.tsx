export class PosVo
{
    public row:number;
    public col:number;
    public index:number;
    constructor(row:number,col:number,index:number)
    {
        this.row = row;
        this.col = col;
        this.index = index;
    }
    isEqual(pos:PosVo):boolean
    {
        if(pos.row === this.row && pos.col === this.col && pos.index === this.index)
        {
            return true;
        }
        return false;
    }
}