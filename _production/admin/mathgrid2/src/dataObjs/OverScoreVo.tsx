export class OverScoreVo
{
    public buttonText:string;
    public elementText:string;
    public classText:string;

    constructor(buttonText:string,elementText:string,classText:string)
    {
        this.buttonText=buttonText;
        this.elementText=elementText;
        this.classText=classText;
    }
}