import { ChangeEvent, Component } from 'react';
import { Alert, Button, Card, Col, Row } from 'react-bootstrap';

import ApiConnect from '../util/ApiConnect';
import { MainStorage } from '../mc/MainStorage';
interface MyProps { callback: Function, cancelCallback: Function }
interface MyState { fileName: string, LtiFileName: string, msg: string, confirmFlag: boolean }
class FileNameScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { "LtiFileName": this.ms.LtiFileName, "fileName": this.ms.fileName, "msg": "", confirmFlag: false }
    }
    componentDidMount(): void {
        this.setState({ LtiFileName: this.ms.LtiFileName});
    }
    onChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ LtiFileName: e.currentTarget.value });
    }
    onChangeName(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ fileName: e.currentTarget.value });
    }
    getInput() {
        return (<Row><Col sm={2}></Col><Col className="tar" sm={3}>LTI:</Col><Col sm={5}><input type='text' value={this.state.LtiFileName} onChange={this.onChange.bind(this)}></input></Col><Col sm={2}></Col></Row>)
    }
    /*  getInputName() {
         return (<Row><Col sm={2}></Col><Col className='tar' sm={3}>File Name</Col><Col sm={5}><input type='text' value={this.state.fileName} onChange={this.onChangeName.bind(this)}></input></Col><Col sm={2}></Col></Row>)
     } */
    setName() {
        if (this.state.LtiFileName === "") {
            this.setState({ msg: "LTI can not be blank" });
            return;
        }

        /*  if (this.state.fileName==="")
         {
             this.setState({msg:"File Name can not be blank"});
             return;
         } */

        if (this.ms.LtiFileName === "") {
            //check for existing name
            this.checkForAlready();
        }
        else {
            this.confirm();
        }
        //   this.ms.fileName = this.state.fileName;



        //this.props.callback();
    }
    checkForAlready() {
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getFileList(this.checked.bind(this), this.state.LtiFileName);

    }
    checked(data: any) {
        //////////console.log(data.length);

        if (data.length === 0) {
            this.confirm();
        }
        else {
            this.setState({ confirmFlag: true });
        }
    }
    confirm() {
        this.ms.LtiFileName = this.state.LtiFileName;
        this.props.callback();
    }
    deny() {
        this.setState({ LtiFileName: "",confirmFlag:false });
    }
    getMessage() {
        if (this.state.msg === "") {
            return "";
        }
        return <Alert variant='warning' className='tac'>{this.state.msg}</Alert>
    }
    getYesNoBox() {

        return (<Card>
            <Card.Header>
                Overwrite?
            </Card.Header>
            <Card.Body>
                <Row><Col className='tac'><h3>{this.state.LtiFileName} Already Exists. </h3> <p>Do you want to overwrite?</p></Col></Row>
                <Row><Col sm={3}></Col><Col sm={3}><Button variant='danger' onClick={()=>{this.confirm()}}>Yes</Button></Col><Col sm={3}></Col><Button onClick={()=>{this.deny()}}>No</Button><Col sm={3}></Col></Row>
            </Card.Body>
        </Card>)

    }
    render() {
        if (this.state.confirmFlag === true) {
            return <Row><Col>{this.getYesNoBox()}</Col></Row>
        }
        return (<Card>
            <Card.Body>
                <Row><Col>{this.getMessage()}</Col></Row>
                {this.getInput()}
                <br />
                <Row><Col className='tac'><Button onClick={this.setName.bind(this)}>Done</Button></Col><Col className='tac'><Button variant='danger' onClick={() => { this.props.cancelCallback() }}>Cancel</Button></Col></Row>
            </Card.Body>
        </Card>)
    }
}
export default FileNameScreen;