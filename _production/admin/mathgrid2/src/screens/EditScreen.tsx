import { Component } from 'react';
import { TCellVo } from '../dataObjs/TCellVo';
import GrowGrid from '../comps/GrowGrid';
import { TGridVo } from '../dataObjs/TGridVo';
import { CellType, GridActions, PanelType, TempType } from '../mc/Constants';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import { StyleVo } from '../dataObjs/StyleVo';
import { DefUtil } from '../util/DefUtil';
import StepPanel from '../panels/StepPanel';
import { GridUtil } from '../util/GridUtil';
import TopMenu from '../comps/TopMenu';
import DragPanel from '../comps/DragPanel';
import { PanelPosVo } from '../dataObjs/PanelPosVo';
import { MessageCenter } from '../mc/MessageCenter';
import { PosVo } from '../dataObjs/PosVo';
import { StyleUtil } from '../util/StyleUtil';
import ImageOverlay from '../comps/ImageOverlay';
import { OverScoreVo } from '../dataObjs/OverScoreVo';
interface MyProps { steps: TGridVo[] }
interface MyState { steps: TGridVo[], grid: TGridVo, panels: JSX.Element[], currentRow: number, currentCol: number, cellIndex: number, preview: number, multiSelect: boolean, multiPos: PosVo[], startPos: PosVo, overPos: PosVo, currentStep: number, editOverlay: boolean }
class EditScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private messages: MessageCenter = MessageCenter.getInstance();

    private defUtil: DefUtil = DefUtil.getInstance();
    private keyPressFunc: any = this.onKeyPress.bind(this);
    private keyUpFunc: any = this.onKeyUp.bind(this);
    private isMountedFlag: boolean = false;

    private startRow: number = -1;
    private startCol: number = -1;
    private overRow: number = 0;
    private overCol: number = 0;

    constructor(props: MyProps) {
        super(props);
        let startPos: PosVo = new PosVo(-1, -1, 0);
        let overPos: PosVo = new PosVo(-1, -1, 0);

        this.state = { steps: this.props.steps, grid: this.props.steps[this.ms.currentStep], panels: [], currentRow: 0, currentCol: 0, cellIndex: 0, preview: 0, multiSelect: false, multiPos: [], startPos: startPos, overPos: overPos, currentStep: this.ms.currentStep, editOverlay: false };
    }
    componentDidMount(): void {
        //remove the keydown listener from the document
        document.removeEventListener('keydown', this.keyPressFunc);
        //add the keydown listener to the document
        document.addEventListener('keydown', this.keyPressFunc);
        document.removeEventListener('keyup', this.keyUpFunc);
        //add the keydown listener to the document
        document.addEventListener('keyup', this.keyUpFunc);


        this.mc.doGridAction = this.doGridAction.bind(this);
        this.mc.updateStyle = this.updateStyle.bind(this);
        this.mc.updateAdjust = this.updateAdjust.bind(this);
        this.mc.formatCell = this.formatCell.bind(this);
        this.mc.setText = this.setText.bind(this);
        //step functions
        this.mc.addStep = this.addStep.bind(this);
        this.mc.cloneStep = this.cloneStep.bind(this);
        this.mc.changeStep = this.changeStep.bind(this);
        this.mc.moveStepUp = this.moveRight.bind(this);
        this.mc.moveStepDown = this.moveLeft.bind(this);
        this.mc.removeStep = this.removeStep.bind(this);
        this.mc.togglePreview = this.togglePreview.bind(this);
        this.mc.showGuideLines = this.showGuideLines.bind(this);
        this.mc.addPanel = this.addPanel.bind(this);
        this.mc.closePanel = this.removePanel.bind(this);
        this.mc.clearCell = this.clearCurrentCell.bind(this);

        this.mc.undo = this.undoHistory.bind(this);
        this.mc.redo = this.redoHistory.bind(this);

        this.mc.copy = this.copyCell.bind(this);
        this.mc.cut = this.cutCell.bind(this);
        this.mc.paste = this.pasteCell.bind(this);
        this.mc.setCell = this.setCell.bind(this);
        this.mc.addScratch = this.saveCurrentToScratch.bind(this);
        this.mc.toggleOverlay = this.toggleOverlay.bind(this);
        this.mc.setOverlay = this.setOverlay.bind(this);
        this.mc.addOverScore = this.addOverScore.bind(this);

        this.isMountedFlag = true;
        this.reopenPanels();
    }
    componentWillUnmount(): void {
        //remove the keydown listener from the document
        document.removeEventListener('keydown', this.keyPressFunc);
        this.isMountedFlag = false;
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (this.isMountedFlag) {
            if (prevProps.steps !== this.props.steps) {
                this.setState({ steps: this.props.steps, grid: this.props.steps[this.ms.currentStep] });
            }
        }
    }
    undoHistory() {
        this.ms.undoHistory();
        this.setState({ steps: this.ms.steps, grid: this.ms.steps[this.ms.currentStep] });
    }
    redoHistory() {
        this.ms.redoHistory();
        this.setState({ steps: this.ms.steps, grid: this.ms.steps[this.ms.currentStep] });
    }
    copyCell() {
        let cell: TCellVo = this.getCurrentCell();
        if (cell.isEmpty()) {
            return;
        }
        this.ms.copyCell = cell.clone();
        this.mc.clipboardChanged();
    }
    cutCell() {
        let cell: TCellVo = this.getCurrentCell();
        if (cell.isEmpty()) {
            return;
        }
        this.ms.copyCell = cell.clone();
        this.clearCurrentCell();
        this.mc.clipboardChanged();
    }
    pasteCell() {
        if (this.ms.copyCell) {
            this.setCell(this.ms.copyCell);
        }
    }
    saveCurrentToScratch() {
        let cell: TCellVo = this.getCurrentCell();
        if (cell.isEmpty()) {
            return;
        }
        this.ms.addToScratchPad(cell.clone());
        this.mc.scratchPadChanged();
    }
    setCell(cell: TCellVo) {

        let copyCell: TCellVo = cell.clone();
        //  let copyCell: TCellVo = new TCellVo(cell.text.slice(),cell.colSize,cell.rowSize,cell.textClasses.slice(),cell.cellClasses.slice(),cell.type);



        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;


        gridData[currentCol][currentRow] = copyCell;

        grid.gridData = gridData;
        this.setState({ grid: grid });
        this.ms.addHistory();
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, copyCell);
    }
    addStep() {
        let steps: TGridVo[] = this.state.steps.slice();
        let grid: TGridVo = GridUtil.getEmptyGrid();
        grid.name = 'Step ' + (steps.length + 1).toString();
        steps.push(grid);
        this.ms.currentStep = steps.length - 1;
        this.setState({ steps: steps, grid: grid });
        this.ms.steps = steps;
        this.ms.addHistory();
        this.ms.scrollToBottomStep = true;
        this.changeStep(this.ms.currentStep);
    }
    cloneStep() {
        let steps: TGridVo[] = this.state.steps.slice();
        let grid: TGridVo = steps[this.ms.currentStep];
        let newGrid: TGridVo = grid.clone();
        newGrid.name = 'Step ' + (steps.length + 1).toString();
        steps.push(newGrid);
        this.setState({ steps: steps });
        this.ms.steps = steps;
        this.ms.addHistory();
    }
    changeStep(index: number) {
        this.ms.currentStep = index;
        this.setState({ grid: this.ms.steps[index], currentStep: index });
    }
    moveLeft() {

        let steps: TGridVo[] = this.state.steps.slice();
        let index: number = this.ms.currentStep;
        if (index > 0) {
            let step: TGridVo = steps[index];
            steps.splice(index, 1);
            steps.splice(index - 1, 0, step);
            this.ms.currentStep = index - 1;
            this.ms.steps = steps;
            this.setState({ steps: steps, grid: step });
            this.ms.addHistory();
        }
    }
    moveRight() {

        let steps: TGridVo[] = this.state.steps.slice();
        let index: number = this.ms.currentStep;
        if (index < steps.length - 1) {
            let step: TGridVo = steps[index];
            steps.splice(index, 1);
            steps.splice(index + 1, 0, step);
            this.ms.currentStep = index + 1;
            this.ms.steps = steps;
            this.setState({ steps: steps, grid: step });
            this.ms.addHistory();
        }
    }
    removeStep() {
        let steps: TGridVo[] = this.state.steps.slice();
        let index: number = this.ms.currentStep;

        if (index > 0) {
            steps.splice(index, 1);
            this.ms.currentStep = index - 1;
            if (steps.length === 0) {
                steps = [GridUtil.getEmptyGrid()];
            }
            this.ms.steps = steps;
            this.setState({ steps: steps, grid: steps[this.ms.currentStep] });
            this.ms.addHistory();
        }
        else {
            steps.splice(index, 1);
            this.ms.currentStep = 0;
            if (steps.length === 0) {
                let grid: TGridVo = GridUtil.getEmptyGrid();
                grid.name = 'Step 1';
                steps = [grid];
            }
            this.ms.steps = steps;
            this.setState({ steps: steps, grid: steps[0] });
            this.ms.addHistory();
        }
    }
    onKeyUp(e: KeyboardEvent) {
        let ctrlDown: boolean = e.ctrlKey;
        if (!ctrlDown) {
            this.setState({ multiSelect: false });
        }
    }
    onKeyPress(e: KeyboardEvent) {

        //unless the key starts with a capital f then prevent default
        //get the first character of the key
        let firstChar = e.key.substring(0, 1);
        if (firstChar !== 'F') {
            e.preventDefault();
        }

        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData;
        let rowCount = gridData.length;
        let colCount = gridData[0].length;
        let cellIndex = this.state.cellIndex;
        let cell: TCellVo = gridData[currentCol][currentRow];
        let key = e.key;


        //let textLength: number = text.length;
        let shiftDown: boolean = e.shiftKey;
        let ctrlDown: boolean = e.ctrlKey;
        let altDown: boolean = e.altKey;

        //if ctrl then look for multi select
        if (ctrlDown) {
            this.setState({ multiSelect: true });
        }
        if (ctrlDown && key.toLowerCase() === "z") {
            this.undoHistory();
            return;
        }
        if (ctrlDown && key.toLowerCase() === "y") {
            this.redoHistory();
            return;
        }
        //if f5 then reload
        if (key === 'F5') {
            window.location.reload();
            return;
        }
        if (altDown) {
            if (key === 'f') {
                this.makeCell(cell, CellType.Fraction, TempType.Fraction1x1);
                return;
            }
            if (key === 's') {
                this.makeCell(cell, CellType.SquareRoot, TempType.Square1);
                return;
            }
            if (key === 'p') {
                this.makeCell(cell, CellType.Power, TempType.Any);
                return;
            }
            //whole number
            if (key === 'w') {
                this.makeCell(cell, CellType.Whole, TempType.Any);
                return;
            }
            if (key === 'u') {
                cell.toggleCellClass('bottomline');
                gridData[currentCol][currentRow] = cell;
                grid.gridData = gridData;
                this.setState({ grid: grid });

                this.forwardOne();
                return;
            }
            if (key === 't') {
                cell.toggleCellClass('topline');
                gridData[currentCol][currentRow] = cell;
                grid.gridData = gridData;
                this.setState({ grid: grid });
                this.forwardOne();
                return;
            }
        }


        switch (key) {
            case 'ArrowUp':

                if (shiftDown) {
                    this.doGridAction(GridActions.MoveUp);
                    return;
                }
                if (altDown) {
                    this.doGridAction(GridActions.ShiftColUp);
                    return;
                }
                this.doGridAction(GridActions.GoUp);

                return;
            case 'ArrowDown':
                if (shiftDown) {
                    this.doGridAction(GridActions.MoveDown);
                    return;
                }
                if (altDown) {
                    this.doGridAction(GridActions.ShiftColDown);
                    return;
                }
                this.doGridAction(GridActions.GoDown);

                return;
            case 'ArrowLeft':
                ////////console.log('arrow left');
                if (shiftDown) {
                    this.doGridAction(GridActions.MoveLeft);
                    return;
                }
                if (altDown) {
                    this.doGridAction(GridActions.ShiftRowLeft);
                    return;
                }
                this.doGridAction(GridActions.SectionLeft);
                return;



            case 'ArrowRight':
                ////////console.log('arrow right');
                if (shiftDown) {
                    this.doGridAction(GridActions.MoveRight);
                    return;
                }
                if (altDown) {
                    this.doGridAction(GridActions.ShiftRowRight);
                    return;
                }
                this.doGridAction(GridActions.SectionRight);
                return;

            case 'Backspace':
                if (cell.text[cellIndex] !== '?') {

                    switch (cell.type) {
                        case CellType.Fraction:
                        case CellType.Power:
                            cell.text[cellIndex] = '?';
                            break;
                        default:
                            cell.text[cellIndex] = '';

                    }
                }
                this.goBackOneSection();
                return;
            case 'Delete':
                cell.text[cellIndex] = '';
                return;
            case 'Enter':
                currentCol = 0;
                currentRow++;
                if (currentRow >= rowCount) {
                    currentRow = 0;
                }
                break;
            case 'Tab':
                if (shiftDown) {
                    this.goBackOneSection();
                } else {
                    this.forwardOne();
                }
                return;

            default:
                if (key.length === 1) {
                    let gridData = grid.gridData;
                    let cell: TCellVo = gridData[currentCol][currentRow];

                    cell.text[cellIndex] = key;

                    cellIndex++;
                    if (cellIndex >= cell.text.length) {
                        cellIndex = 0;
                        currentCol++;
                        if (currentCol >= colCount) {
                            currentCol = 0;
                            currentRow++;
                            if (currentRow >= rowCount) {
                                currentRow = 0;
                            }
                        }
                    }
                }

        }
        //if the key is a number, set the cell value to that number

        (window as any).cell = cell;
        this.mc.updateDebug(cell);
        this.setState({ grid: grid, cellIndex: cellIndex, currentCol: currentCol, currentRow: currentRow });
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
        this.ms.addHistory();
    }
    setText(text: string) {
        ////////console.log('setText');
        let currentCell: TCellVo = this.getCurrentCell();
        ////////console.log(currentCell);
        currentCell.text[currentCell.selectedIndex] = text;
        this.updateGrid(currentCell);
        this.mc.updateDebug(currentCell);
        this.forwardOne();
        this.ms.addHistory();
    }
    getCurrentCell(): TCellVo {
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData;
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let cell: TCellVo = gridData[currentCol][currentRow];
        return cell;
    }
    updateGrid(cell: TCellVo) {
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData;
        let currentRow = cell.row;
        let currentCol = cell.col;
        gridData[currentCol][currentRow] = cell;
        this.setState({ grid: grid });
    }
    clearCurrentCell() {
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData;
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        gridData[currentCol][currentRow] = new TCellVo([""], 1, 1, [], ["whole"], CellType.Whole);
        grid.gridData = gridData;
        this.setState({ grid: grid });
        this.ms.addHistory();
    }
    updateAdjust(adjustX: number, adjustY: number) {
        if (this.ms.applyAdjustToCell === true) {
            this.updateAdjustCell(adjustX, adjustY);
            return;
        }
        if (this.state.multiPos.length > 0) {
            this.updateAdjustMulti(adjustX, adjustY);
            return;
        }
        let cell: TCellVo = this.getCurrentCell();
        let currentAdjustX = cell.getAdjustX();
        let currentAdjustY = cell.getAdjustY();
        currentAdjustX += adjustX;
        currentAdjustY += adjustY;
        cell.setAdjustX(currentAdjustX);
        cell.setAdjustY(currentAdjustY);
        this.updateGrid(cell);
        this.ms.addHistory();
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
    }
    updateAdjustMulti(adjustX: number, adjustY: number) {
        let postions: PosVo[] = this.state.multiPos.slice();
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        for (let i: number = 0; i < postions.length; i++) {

            let col: number = postions[i].col;
            let row: number = postions[i].row;
            if (col < 0 || row < 0) continue;

            let cell: TCellVo = gridData[col][row];
            let numberOfSubCells: number = cell.selectedIndexes.length;
            for (let j: number = 0; j < numberOfSubCells; j++) {
                let index: number = cell.selectedIndexes[j];
                let currentAdjustX = cell.getAdjustX2(index);
                let currentAdjustY = cell.getAdjustY2(index);
                currentAdjustX += adjustX;
                currentAdjustY += adjustY;
                cell.setAdjustX2(index, currentAdjustX);
                cell.setAdjustY2(index, currentAdjustY);
            }
            gridData[postions[i].col][postions[i].row] = cell;
        }
        grid.gridData = gridData;
        this.setState({ grid: grid });

    }
    updateAdjustCell(adjustX: number, adjustY: number) {
        let cell: TCellVo = this.getCurrentCell();
        let numberOfSubCells: number = cell.text.length;
        for (let i: number = 0; i < numberOfSubCells; i++) {
            let currentAdjustX = cell.getAdjustX2(i);
            let currentAdjustY = cell.getAdjustY2(i);
            currentAdjustX += adjustX;
            currentAdjustY += adjustY;
            cell.setAdjustX2(i, currentAdjustX);
            cell.setAdjustY2(i, currentAdjustY);
        }
        this.updateGrid(cell);
        this.ms.addHistory();
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
    }
    updateStyle(style: StyleVo) {
        if (this.state.multiPos.length === 0) {
            this.updateSingleStyle(style);
            return;
        }
        this.updateMultiStyle(style);
    }
    updateMultiStyle(style: StyleVo) {

        let postions: PosVo[] = this.state.multiPos.slice();
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        for (let i: number = 0; i < postions.length; i++) {
            //console.log(postions[i]);
            let cell: TCellVo = gridData[postions[i].col][postions[i].row];
            cell.selectedIndex = postions[i].index;
            /*  cell.selectedIndexes.push(postions[i].index); */

            StyleUtil.removeStyle(style, style.type, cell);
            //
            //toggle the style
            //
            if (style.applyToCell) {
                cell.toggleCellClass(style.styleValue);
            }
            else {
                cell.toggleTextClass(style.styleValue);
            }
            gridData[postions[i].col][postions[i].row] = cell;

        }
        grid.gridData = gridData;
        this.setState({ grid: grid });
    }
    updateSingleStyle(style: StyleVo) {
        console.log('update single style');
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();


        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let cell: TCellVo = this.getCurrentCell();
        //console.log(cell);

        if (cell.text.length === 0) {
            return;
        }
        StyleUtil.removeStyle(style, style.type, cell);

        if (style.applyToCell) {
            cell.toggleCellClass(style.styleValue);
        }
        else {
            cell.toggleTextClass(style.styleValue);
        }
        gridData[currentCol][currentRow] = cell;
        grid.gridData = gridData;
        this.setState({ grid: grid });
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
        this.ms.addHistory();
    }
    addOverScore(overscore: OverScoreVo) {
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let cell: TCellVo = this.getCurrentCell();
        let classes: string[] = overscore.classText.split(' ');
        // let textLen:number = cell.text.length;
        let overscores: OverScoreVo[] = this.ms.overscores;
        for (let i:number=0;i<overscores.length;i++)
        {
            let classes2:string[] = overscores[i].classText.split(' ');
            for (let j:number=0;j<classes2.length;j++)
            {
                cell.removeCellClass(classes2[j]);
            }
        }

        for (let i: number = 0; i < classes.length; i++) {

            cell.addCellClass(classes[i]);
            /*  for (let j:number=0;j<textLen;j++)
             {
                 cell.addTextClassAt(j,classes[i]);
             } */
        }
        //cell.text=[overscore.elementText];
        gridData[currentCol][currentRow] = cell;
        grid.gridData = gridData;
        this.setState({ grid: grid });
    }
    makeCell(cell: TCellVo, type: CellType, template: TempType) {
        ////console.log('make cell');
        ////console.log(cell);

        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let textSlice: string[] = cell.text.slice();
        cell = this.defUtil.getDefiniton(type, template, textSlice);
        gridData[currentCol][currentRow] = cell;
        grid.gridData = gridData;
        this.setState({ grid: grid });
        this.ms.addHistory();
        return;
    }


    fixTextSlice(textSlice: string[], minLen: number) {
        while (textSlice.length < minLen) {
            textSlice.push('?');
        }
        for (let i = 0; i < textSlice.length; i++) {
            if (textSlice[i] === '') {
                textSlice[i] = '?';
            }
        }
        return textSlice;
    }
    forwardOne() {
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        let rowCount = gridData.length;
        let colCount = gridData[0].length;
        let cellIndex = this.state.cellIndex;
        let cell: TCellVo = gridData[this.state.currentCol][this.state.currentRow];

        cellIndex++;
        if (cellIndex >= cell.text.length) {
            cellIndex = 0;
            currentCol++;
            if (currentCol >= colCount) {
                currentCol = 0;
                currentRow++;
                if (currentRow >= rowCount) {
                    currentRow = 0;
                }
            }
        }
        cell.selectedIndex = cellIndex;
        this.mc.updateSelectedCell(cell);
        this.mc.updateDebug(cell);

        this.setState({ grid: grid, cellIndex: cellIndex, currentCol: currentCol, currentRow: currentRow });
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
    }
    goBackOneSection() {
        ////////////console.log('go back one section');
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        let cellIndex = this.state.cellIndex;
        let cell: TCellVo = gridData[this.state.currentCol][this.state.currentRow];
        ////////////console.log(cell.text);
        if (cell.text.length === 1) {
            ////////////console.log('only one section');
            this.goBackOne();
            return;
        }
        cellIndex--;
        if (cellIndex < 0) {
            this.goBackOne();
            return;
        }

        cell.selectedIndex = cellIndex;
        this.mc.updateSelectedCell(cell);
        this.mc.updateDebug(cell);
        this.setState({ cellIndex: cellIndex });
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
    }
    goBackOne() {
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData.slice();
        let rowCount = gridData.length;

        currentCol--;
        if (currentCol < 0) {
            currentCol = 0;
            currentRow--;
            if (currentRow < 0) {
                currentRow = 0;
                if (currentRow >= rowCount) {
                    currentRow = 0;
                }
                if (currentRow < 0) {
                    currentRow = rowCount - 1;
                }
            }
        }
        let cell: TCellVo = gridData[currentCol][currentRow];
        let cellIndex = cell.text.length - 1;
        cell.selectedIndex = cellIndex;
        this.mc.updateSelectedCell(cell);
        this.mc.updateDebug(cell);
        this.setState({ grid: grid, cellIndex: cellIndex, currentCol: currentCol, currentRow: currentRow });
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
    }
    selectCell(row: number, column: number, index: number = 0) {
        ////////console.log('select cell');
        //console.log('select cell');
        //console.log(column, row, index);
        if (row !== this.startRow || column !== this.startCol) {
            //console.log("DRAG?")
            //console.log(this.startRow, this.startCol);
            //console.log(row, column);
            this.selectCellsUnderDrag();
            this.clearDrag();
            return;
        }


        this.clearDrag();

        this.setState({ currentRow: row, currentCol: column, cellIndex: index });
        let grid: TGridVo = this.state.grid;
        let gridData = grid.gridData;
        let cell: TCellVo = gridData[column][row];
        cell.selectedIndex = index;
        this.mc.updateSelectedCell(cell);

        //let selectedCells:TCellVo[] = this.state.selectedCells.slice();

        if (this.state.multiSelect) {

            //if cell.selectedIndexes contains index then remove it otherwise add it
            cell.addSelectedIndex(index);

            let multiPos: PosVo[] = this.state.multiPos.slice();
            let pos: PosVo = new PosVo(row, column, index);
            let found: boolean = false;
            for (let i: number = 0; i < multiPos.length; i++) {
                if (multiPos[i].isEqual(pos)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (!cell.isEmpty()) {
                    multiPos.push(pos);
                    for (let i: number = 0; i < multiPos.length; i++) {
                        //console.log(multiPos[i]);
                    }
                }

                // selectedCells.push(cell);
            }
            else {
                for (let i: number = 0; i < multiPos.length; i++) {
                    if (multiPos[i].isEqual(pos)) {
                        multiPos.splice(i, 1);
                        //selectedCells.splice(i,1);
                        break;
                    }
                }
            }
            this.setState({ multiPos: multiPos });
            return;
        }
        else {
            
            let postions: PosVo[] = this.state.multiPos.slice();
            let grid: TGridVo = this.state.grid;
            let gridData = grid.gridData.slice();
            for (let i: number = 0; i < postions.length; i++) {
                let cell: TCellVo = gridData[postions[i].col][postions[i].row];
                cell.selectedIndexes = [];
            }

            this.setState({ multiPos: [] });
        }

        this.mc.updateSelectedCell(cell);
        this.mc.updateDebug(cell);
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, cell);
    }
    formatCell(cellType: CellType, template: TempType) {
        ////////console.log('format cell');
        let currentCell: TCellVo = this.state.grid.gridData[this.state.currentCol][this.state.currentRow];
        this.makeCell(currentCell, cellType, template);
        this.mc.updateSelectedCell(currentCell);
        this.mc.updateDebug(currentCell);
        (window as any).cell = currentCell;
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, currentCell);
    }
    doGridAction(action: GridActions) {
        let grid: TGridVo = this.state.grid;
        let currentRow = this.state.currentRow;
        let currentCol = this.state.currentCol;
        let currentCell: TCellVo = grid.gridData[currentCol][currentRow];
        let cellIndex = this.state.cellIndex;

        switch (action) {
            case GridActions.InsertRowAtStart:
                grid.addRow(true);
                break;
            case GridActions.InsertRowAtEnd:
                grid.addRow(false);
                break;
            case GridActions.InsertColAtStart:
                grid.addColumn(true);
                break;
            case GridActions.InsertColAtEnd:
                grid.addColumn(false);
                break;
            case GridActions.DeleteRow:
                grid.deleteRow(currentRow);
                break;
            case GridActions.DeleteCol:
                grid.deleteColumn(currentCol);
                break;
            case GridActions.InsertColLeft:
                grid.insertColumnLeft(currentCol);
                break;
            case GridActions.InsertColRight:
                grid.insertColumnRight(currentCol);
                break;
            case GridActions.InsertRowAbove:
                grid.insertRowAbove(currentRow);
                break;
            case GridActions.InsertRowBelow:
                grid.insertRowBelow(currentRow);
                break;
            case GridActions.MoveDown:
                grid.shiftDown();
                break;
            case GridActions.MoveUp:
                grid.shiftUp();
                break;
            case GridActions.MoveLeft:
                grid.shiftLeft();
                break;
            case GridActions.MoveRight:
                grid.shiftRight();
                break;
            case GridActions.ShiftColDown:
                grid.shiftColumnDown(currentCol);
                break;
            case GridActions.ShiftColUp:
                grid.shiftColumnUp(currentCol);
                break;
            case GridActions.ShiftRowLeft:
                grid.shiftRowLeft(currentRow);
                break;
            case GridActions.ShiftRowRight:
                grid.shiftRowRight(currentRow);
                break;
            case GridActions.GoUp:
                currentRow--;
                if (currentRow < 0) {
                    currentRow = grid.gridData.length - 1;
                }
                break;
            case GridActions.GoDown:
                currentRow++;
                if (currentRow >= grid.gridData.length) {
                    currentRow = 0;
                }
                break;
            case GridActions.GoLeft:
                currentCol--;
                if (currentCol < 0) {
                    currentCol = grid.gridData[0].length - 1;
                    currentRow--;
                    if (currentRow < 0) {
                        currentRow = grid.gridData.length - 1;
                    }
                }
                break;
            case GridActions.GoRight:
                currentCol++;
                if (currentCol >= grid.gridData[0].length) {
                    currentCol = 0;
                    currentRow++;
                    if (currentRow >= grid.gridData.length) {
                        currentRow = 0;
                    }
                }
                break;
            case GridActions.SectionLeft:
                ////////console.log('section left');
                cellIndex--;
                if (cellIndex < 0) {
                    // cellIndex = 0;
                    currentCol--;
                    if (currentCol < 0) {
                        currentCol = grid.gridData[0].length - 1;
                        currentRow--;
                        if (currentRow < 0) {
                            currentRow = grid.gridData.length - 1;
                        }
                    }
                    currentCell = grid.gridData[currentCol][currentRow];
                    cellIndex = currentCell.text.length - 1;

                }
                currentCell.selectedIndex = cellIndex;
                break;
            case GridActions.SectionRight:
                cellIndex++;
                if (cellIndex >= grid.gridData[currentCol][currentRow].text.length) {
                    cellIndex = 0;
                    currentCol++;
                    if (currentCol >= grid.gridData[0].length) {
                        currentCol = 0;
                        currentRow++;
                        if (currentRow >= grid.gridData.length) {
                            currentRow = 0;
                        }
                    }
                }
                currentCell = grid.gridData[currentCol][currentRow];
                currentCell.selectedIndex = cellIndex;

        }
        console.log('current row', currentRow);
        console.log('row count', grid.rowCount);

        console.log('column count', grid.colCount);
        console.log('current col', currentCol);
        
        if (currentCol>=grid.gridData.length)
        {
            currentCol = grid.gridData.length-1;
        }
        if (currentRow>=grid.rowCount)
        {
            currentRow = grid.rowCount-1;
        }
        console.log(grid.gridData);

        currentCell = grid.gridData[currentCol][currentRow];
        this.mc.updateSelectedCell(currentCell);
        this.mc.updateDebug(currentCell);
        this.setState({ grid: grid, currentRow: currentRow, currentCol: currentCol, cellIndex: cellIndex });
        this.messages.emit(MessageCenter.EVENT_UPDATE_SELECTED_CELL, currentCell);
    }

    togglePreview() {
        if (this.state.preview === 0) {
            this.setState({ preview: 1 });
        }
        else {
            this.setState({ preview: 0 });
        }
    }

    showGuideLines(lineGuide: number) {
        ////////console.log('show guide lines');
        if (this.state.preview === lineGuide) {
            this.setState({ preview: 0 });
            return;
        }
        this.setState({ preview: lineGuide });

    }
    removePanel(panelType: PanelType) {
        let panels: JSX.Element[] = this.state.panels.slice();
        let key: string = 'panel' + panelType.toString();

        let panelPos: PanelPosVo = this.ms.getPanelPos(panelType);
        panelPos.open = false;
        this.ms.updatePanelPos(panelPos.type, panelPos.posX, panelPos.posY, panelPos.open);

        for (let i: number = 0; i < panels.length; i++) {
            if (panels[i].key === key) {
                panels.splice(i, 1);
                this.setState({ panels: panels });
                return;
            }
        }

    }
    reopenPanels() {
        let panels: JSX.Element[] = [];
        let panelVo: PanelPosVo[] = this.ms.getAllOpenPanels();
        // ////////console.log(panelVo);
        for (let i: number = 0; i < panelVo.length; i++) {
            panels.push(this.getPanel(panelVo[i]));
        }
        this.setState({ panels: panels });
    }
    addPanel(panelType: string) {
        let panels: JSX.Element[] = this.state.panels.slice();
        let key: string = 'panel' + panelType;
        ////////console.log('add panel ' + key);
        //if the panel already exists then return
        for (let i: number = 0; i < panels.length; i++) {
            if (panels[i].key === key) {
                return;
            }
        }
        let panelPos: PanelPosVo = this.ms.getPanelPos(panelType);
        panelPos.open = true;
        let panel: JSX.Element = this.getPanel(panelPos);
        panels.push(panel);
        this.setState({ panels: panels });
    }
    getPanel(panelPos: PanelPosVo) {
        let key: string = 'panel' + panelPos.type;
        let width: number = 30;
        let height: number = 30;

        switch (panelPos.type) {
            case PanelType.adjust:
            case PanelType.move:
            case PanelType.insert:
            case PanelType.delete:
            case PanelType.shift:
            case PanelType.templates:
            case PanelType.colors:
            case PanelType.scratchpad:
                height = 40;
                break;

            case PanelType.lines:
            case PanelType.clipboard:

                height = 50;
        }
        let index: number = this.state.panels.length;
        this.ms.updatePanelPos(panelPos.type, panelPos.posX, panelPos.posY, true);
        //<DragPanel posX={20} posY={20} width={30} height={30} type={PanelType.colors}></DragPanel>
        let panel: JSX.Element = <DragPanel key={key} posX={panelPos.posX} posY={panelPos.posY} width={width} height={height} type={panelPos.type} callback={this.panelSelect.bind(this)} index={index}></DragPanel>;
        return panel;
    }
    panelSelect(panelType: PanelType) {
        let panels: JSX.Element[] = this.state.panels.slice();
        let key: string = 'panel' + panelType;
        let index: number = -1;
        //if the panel already exists then return
        for (let i: number = 0; i < panels.length; i++) {
            if (panels[i].key === key) {
                index = i;
                break;
            }
        }
        //move the panel to the end of the array
        if (index > -1) {
            let panel: JSX.Element = panels[index];
            panels.splice(index, 1);
            panels.push(panel);
            this.setState({ panels: panels });
        }
    }
    selectCellsUnderDrag() {

        let multiPos: PosVo[] = GridUtil.findCellsInRange(this.startRow, this.startCol, this.overRow, this.overCol);

        //remove empty cells
        for (let i: number = 0; i < multiPos.length; i++) {
            let rr: number = multiPos[i].row;
            let cc: number = multiPos[i].col;
            if (rr > 0 && cc > 0) {
                let cell: TCellVo = this.state.grid.gridData[cc][rr];
                if (cell.isEmpty()) {
                    multiPos.splice(i, 1);
                    i--;
                }
            }
        }
        let multiCopy: PosVo[] = multiPos.slice();
        for (let i: number = 0; i < multiCopy.length; i++) {
            let rr: number = multiCopy[i].row;
            let cc: number = multiCopy[i].col;
            if (rr > 0 && cc > 0) {
                let cell: TCellVo = this.state.grid.gridData[cc][rr];
                let indexLength: number = cell.text.length;
                for (let j: number = 1; j < indexLength; j++) {
                    let pos: PosVo = new PosVo(multiCopy[i].row, multiCopy[i].col, j);
                    multiPos.push(pos);

                }
                for (let j: number = 0; j < indexLength; j++) {
                    cell.selectedIndexes.push(j);
                }
            }
        }
        //console.log(multiPos);

        this.setState({ multiPos: multiPos });

    }
    clearDrag() {
        this.startRow = -1;
        this.startCol = -1;
        this.overRow = -1;
        this.overCol = -1;

        //clear the built in selection
        (window as any).getSelection().removeAllRanges()
        this.setState({ startPos: new PosVo(-1, -1, 0), overPos: new PosVo(-1, -1, 0) });
    }
    selectStartCell(row: number, col: number) {
        //console.log('select start cell');
        this.startRow = row;
        this.startCol = col;
        //console.log('start cell', row, col);
        let startPos: PosVo = new PosVo(row, col, 0);
        this.setState({ startPos: startPos });
    }
    overCell(row: number, col: number) {

        if (this.startCol === -1 || this.startRow === -1) {
            return;
        }
        this.overRow = row;
        this.overCol = col;

        let overCell: PosVo = new PosVo(this.overRow, this.overCol, 0);
        this.setState({ overPos: overCell });
        //clear the built in selection
        (window as any).getSelection().removeAllRanges()
    }
    toggleOverlay() {
        let editOverlay: boolean = !this.state.editOverlay;
        this.setState({ editOverlay: editOverlay });
    }
    setOverlay(show: boolean) {
        this.setState({ editOverlay: show });
    }
    render() {
        return (
            <div>
                <TopMenu></TopMenu>
                {this.state.panels}
                <div className='editScreen'>
                    <h3>Step:{this.state.currentStep + 1}</h3>
                    <div className='gridContainer'>


                        <GrowGrid grid={this.state.grid} preview={this.state.preview} selectCallback={this.selectCell.bind(this)} startDragCallback={this.selectStartCell.bind(this)} overCallback={this.overCell.bind(this)} currentRow={this.state.currentRow} currentCol={this.state.currentCol} selectedIndex={this.state.cellIndex} multi={this.state.multiPos} startDrag={this.state.startPos} overDrag={this.state.overPos} />

                        <ImageOverlay active={this.state.editOverlay}></ImageOverlay>
                    </div>
                </div>
                <StepPanel steps={this.state.steps}></StepPanel>
            </div>
        )
    }
}
export default EditScreen;