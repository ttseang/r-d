import { Component } from 'react';
import EditScreen from './EditScreen';
import { TGridVo } from '../dataObjs/TGridVo';
import { MainStorage } from '../mc/MainStorage';
import { GridUtil } from '../util/GridUtil';
import { MainController } from '../mc/MainController';
import FileNameScreen from './FileNameScreen';
//import { FileVo } from '../dataObjs/FileVo';
import ApiConnect from '../util/ApiConnect';
import FileOpenScreen from './FileOpenScreen';
import { Payload } from '../dataObjs/Payload';
interface MyProps { }
interface MyState { mode: number, steps: TGridVo[] }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);

        let mtGrid: TGridVo = GridUtil.getEmptyGrid();
        mtGrid.name = "Step 1";
        this.ms.steps.push(mtGrid);
        this.state = { mode: 0, steps: this.ms.steps };
        this.mc.changeScreen = this.changeScreen.bind(this);
    }
    componentDidMount(): void {

        let lastData: string = this.ms.getFromLocalStorage();
        const params = new URLSearchParams(window.location.search);

        if (params.get("lti")) {
            this.openFile(params.get("lti") || "");
        }
        else if (lastData !== "") {
            this.loadFromString(lastData);
        }
    }
    private changeScreen(index: number) {
        this.setState({ mode: index });
    }
    getScreen() {
        switch (this.state.mode) {
            case 0:
                return <EditScreen steps={this.state.steps} />;
            case 1:
                return <FileNameScreen callback={this.setFileName.bind(this)} cancelCallback={this.cancelScreen.bind(this)} />;
            case 2:
                return <FileOpenScreen openCallback={this.openFile.bind(this)} goBack={this.cancelScreen.bind(this)} />;
        }
    }
    setFileName(name: string) {

        this.ms.fileName = name;
        this.ms.prepSave();
        const apiConnect: ApiConnect = new ApiConnect();

        let payload: Payload = new Payload(this.ms.LtiFileName, "addition", "1+1", this.ms.saveString);
        apiConnect.Save(JSON.stringify(payload), this.saveDone.bind(this));
    }
    saveDone(data: any) {


        this.setState({ mode: 0 });
        //   let loadData: any = this.ms.saveData.steps[0] || [];
        /*   setTimeout(() => {
              this.mc.loadFromData(loadData);
          }, 500); */
    }
    openFile(lti: string) {
        this.setState({ mode: 0 });
        this.ms.LtiFileName = lti;

        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.getFileContent(lti, this.loadDone.bind(this));
    }
    loadDone(data: any) {
        if (!data.error) {
            let loadString: string = data.data;
            this.loadFromString(loadString);
        }
        else {
            console.log("Couldn't load LTI?", this.ms.LtiFileName);
            // TODO - set this.ms.LtiFileName to ""?
        }
    }
    loadFromString(loadString: string) {
        let loadObj: any = JSON.parse(loadString);
        let steps: TGridVo[] = [];
        let stepCount: number = loadObj.length;
        for (let i: number = 0; i < stepCount; i++) {
            let stepData: any = loadObj[i].gridData;
            let tGrid: TGridVo = new TGridVo([]);
            tGrid.loadFromData(stepData);
            steps.push(tGrid);
        }
        this.ms.steps = steps;
        this.ms.history = [];
        this.ms.historyIndex = 0;
        this.ms.currentStep = 0;
        this.setState({ mode: 0, steps: steps });
    }
    cancelScreen() {
        this.setState({ mode: 0 });
    }
    render() {
        return (<div>
            {this.getScreen()}
        </div>)
    }
}
export default BaseScreen;