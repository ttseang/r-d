import { PosVo } from "../dataObjs/PosVo";
import { TCellVo } from "../dataObjs/TCellVo";
import { TGridVo } from "../dataObjs/TGridVo";
import { CellType } from "../mc/Constants";

export class GridUtil
{
    public static getEmptyGrid(): TGridVo
    {
        let initalGridRows:number = 10;
        let initalGridCols:number = 12;
        let gridData:TCellVo[][] = [];
        //make a 10 x 10 grid of random numbers and math operators and symbols
        //make the rows and colums sizes random
        for(let i:number=0;i<initalGridCols;i++)
        {
            gridData[i] = [];
            for(let j:number=0;j<initalGridRows;j++)
            {
                gridData[i][j] = new TCellVo([""],1,1,[['fs8']],["whole"],CellType.Whole);
                gridData[i][j].template="any";
            }
        }
        let grid:TGridVo = new TGridVo(gridData);
        return grid;
    }
    public static findCellsInRange(startRow:number,startCol:number,endRow:number,endCol:number):PosVo[]
    {
        let cellsInRange:PosVo[] = [];

        if (startRow > endRow)
        {
            let temp:number = startRow;
            startRow = endRow;
            endRow = temp;
        }
        if (startCol > endCol)
        {
            let temp:number = startCol;
            startCol = endCol;
            endCol = temp;
        }

        let row:number = startRow;
        let col:number = startCol;
        while(row <= endRow)
        {
            while(col <= endCol)
            {
                cellsInRange.push(new PosVo(row,col,0));
                col++;
            }
            col = startCol;
            row++;
        }
        return cellsInRange;
    }
    public static checkCellInRange(startRow:number,startCol:number,endRow:number,endCol:number,row:number,col:number):boolean
    {
        if (startRow===-1 || startCol===-1 || endRow===-1 || endCol===-1)
        {
            return false;
        }
        if (startRow > endRow)
        {
            let temp:number = startRow;
            startRow = endRow;
            endRow = temp;
        }
        if (startCol > endCol)
        {
            let temp:number = startCol;
            startCol = endCol;
            endCol = temp;
        }
        if(row >= startRow && row <= endRow && col >= startCol && col <= endCol)
        {
            return true;
        }
        return false;
    }
}