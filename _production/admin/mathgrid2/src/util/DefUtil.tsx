import { TCellVo } from "../dataObjs/TCellVo";
import { CellType, TempType } from "../mc/Constants";

export class DefUtil {
    //make a map indexed by CellType and TempType
    private defMap: Map<string, TCellVo> = new Map<string, TCellVo>();
    private tempMap:Map<string,string>=new Map<string,string>();

    private static instance: DefUtil;
    constructor() {
        this.makeDefMap();
        (window as any).defMap = this.defMap;
        (window as any).tempMap = this.tempMap;
    }
    public static getInstance(): DefUtil {
        if (this.instance === undefined) {
            this.instance = new DefUtil();
        }
        return this.instance;
    }
    private makeDefMap(): void {

        //Whole Number
        this.makeCell([], 1, CellType.Whole, TempType.Any, 1, 1, ['whole'], []);

        //Whole number with decimal
        this.makeCell([], 2, CellType.Whole, TempType.Decimal, 2, 1, ['decimal'], []);
        //columns
        //1x2
        this.makeCell([], 2, CellType.Column, TempType.TwoCol, 1, 2, ['twoCol']);
        //1x3
        this.makeCell([], 3, CellType.Column, TempType.ThreeCol, 1, 3, ['threeCol']);
        //1x4
        this.makeCell([], 4, CellType.Column, TempType.FourCol, 1, 4, ['fourCol']);

        //rows
        //2x1
        this.makeCell([], 2, CellType.Row, TempType.TwoRows, 2, 1, ['twoRows']);
        //3x1
        this.makeCell([], 3, CellType.Row, TempType.ThreeRows, 3, 1, ['threeRows']);
        //4x1
        this.makeCell([], 4, CellType.Row, TempType.FourRows, 4, 1, ['fourRows']);

        //make a map of TCellvo indexed by CellType and TempType
        //fractions
        //1x1
        this.makeCell([], 2, CellType.Fraction, TempType.Fraction1x1, 1, 2, ['fraction']);
        //2x2
        this.makeCell([], 4, CellType.Fraction, TempType.Fraction2x2, 2, 2, ['fraction2x2']);
        //3x3
        this.makeCell([], 6, CellType.Fraction, TempType.Fraction3x3, 3, 3, ['fraction3x3']);
        //4x4
        this.makeCell([], 8, CellType.Fraction, TempType.Fraction4x4, 4, 4, ['fraction4x4']);

        //grids
        //2x2
        this.makeCell([], 4, CellType.Grid, TempType.Grid2x2, 2, 2, ['grid2x2']);
        //3x2
        this.makeCell([], 6, CellType.Grid, TempType.Grid3x2, 3, 2, ['grid3x2']);
        //4x2
        this.makeCell([], 8, CellType.Grid, TempType.Grid4x2, 4, 2, ['grid4x2']);

        //powers
        //1x1
        this.makeCell([], 2, CellType.Power, TempType.Power1x1, 2, 1, ['power']);
        //2x2
        this.makeCell([], 4, CellType.Power, TempType.Power2x2, 2, 2, ['power2x2']);
        //3x3
        this.makeCell([], 6, CellType.Power, TempType.Power3x3, 3, 3, ['power3x3']);

        //square roots
        //1
        this.makeCell(["√"], 1, CellType.SquareRoot, TempType.Square1, 1, 1, ['sqrt']);

        //carry and borrow
        // cell = new TCellVo(textSlice, 2, 1, [], ["carry"], type);
        this.makeCell([], 2, CellType.Carry, TempType.Any, 2, 1, ['carry']);
        this.makeCell([], 2, CellType.Borrow, TempType.BorrowTop, 1, 2, ['borrow-top']);
        this.makeCell([], 2, CellType.Borrow, TempType.BorrowSide, 1, 2, ['borrow-side']);
        //  cell = new TCellVo(textSlice, 1, 2, [['', 'redText']], ["fraction"],);

    }
    public getTempType(classString:string):string
    {
        console.log("getTempType",classString);
        return this.tempMap.get(classString) || TempType.Any;
    }
    private makeCell(textSlice: string[], textLen: number, cellType: string, tempType: string, w: number, h: number, cellClass: string[], textClasses: string[][] = []) {
        
        //get the last element of the cellClass array
        let classString=cellClass[cellClass.length-1];
     //   console.log("makeCell",classString);
        let key:string=cellType+classString;
        this.tempMap.set(key,tempType);
        textSlice = this.fixTextSlice(textSlice, textLen);
        /*  if (cellClass.length > 0) {
             //concatenate the cellClass to the cellType
             cellClass.push(cellType);
         }
         else {
             cellClass = [cellType];
         } */
        let cell: TCellVo = new TCellVo(textSlice, w, h, textClasses.slice(), cellClass.slice());
        cell.template=tempType;
       // console.log(cell);
        ////console.log("makeCell", cellType, tempType, cell);
        this.addDef(cellType, tempType, cell);
    }
    private addDef(cellType: string, tempType: string, def: TCellVo): void {
        let key: string = cellType + tempType;
        def.template = tempType;
        ////////console.log("addDef", key);
        this.defMap.set(key, def);
    }
    public getDefiniton(cellType: string, tempType: string, text: string[] = []): TCellVo {

        let key: string = cellType + tempType;
        ////////console.log("getDefiniton", key);
        let def: TCellVo | undefined = this.defMap.get(key);

        if (def === undefined) {
            def = this.defMap.get(CellType.Whole + TempType.Any);
        }
        if (def === undefined) {
            const ncell:TCellVo= new TCellVo(["1"], 1, 1, [], ["whole"], CellType.Whole);
            ncell.template=tempType;
            return ncell;
        }
        def = def.clone();     


        if (text.length === 0) {
            ////console.log("getDefiniton: text is empty");
            ////console.log(def);
            return def;
        }
        //check for text=['']

        let len: number = def.text.length;
        let text2: string[] = this.fixTextSlice(text, len);

        //custom text here
        if (cellType === CellType.SquareRoot) {
            //make the text2 element a square root symbol
            text2 = [];
            text2[0] = "√";
        }
        if (cellType === CellType.Whole && tempType === TempType.Decimal) {
            // text2=[];
            if (text2[0] === '' || text2[0] === undefined || text2[0] === null || text2[0] === '?') {
                text2[0] = "1";
            }
            text2[1] = ".";
        }

        def.text = text2;

        let mycell: TCellVo = new TCellVo(text2, def.colSize, def.rowSize, def.textClasses.slice(), def.cellClasses.slice(), def.type);
        mycell.template = tempType;
        //custom adjustments here
        //this should be in the definition
        //but there is a bug in the definition that causes
        //the text styles to be linked to all other cells with the same definition
        //so we have to do it here for now
        if (cellType === CellType.Whole && tempType === TempType.Decimal) {

            mycell.setAdjustY2(0, -0.15);
            mycell.setAdjustX2(0, .1);
            mycell.setAdjustY2(1, -0.15);
            mycell.setAdjustX2(1, .1);

            mycell.addTextClassAt(0, "fs8");
            mycell.addTextClassAt(1, "fs8");
        }
        //borrow
        if (cellType === CellType.Borrow && tempType === TempType.BorrowTop) {
            mycell.setAdjustY2(0, 0.25);
            mycell.setAdjustY2(1, -0.255);
            mycell.addTextClassAt(0, "fs4");
            mycell.addTextClassAt(1, "fs8");
        }
        if (cellType === CellType.Borrow && tempType === TempType.BorrowSide) {

            mycell.setAdjustX2(0, -0.3);
            mycell.setAdjustY2(0, 0.45);


            mycell.setAdjustX2(1, 0);
            mycell.setAdjustY2(1, -0.3);

            mycell.addTextClassAt(0, "fs4");
            mycell.addTextClassAt(1, "fs8");
        }
        if (cellType === CellType.Fraction) {
            //Fraction4x4

            switch (tempType) {

                case TempType.Fraction1x1:
                    mycell.addTextClassAt(0, "fs4");
                    mycell.addTextClassAt(1, "fs4");
                    break;

                case TempType.Fraction2x2:
                    this.setTextClass(mycell, "fs3",len);

                    mycell.setAdjustX2(0, 0.1);
                    mycell.setAdjustX2(1, -0.1);                  

                    mycell.setAdjustX2(2, 0.1);
                    mycell.setAdjustX2(3, -0.1);

                    break;

                case TempType.Fraction3x3:
                    this.setTextClass(mycell, "fs3",len);
                    break;
                case TempType.Fraction4x4:
                    this.setTextClass(mycell, "fs3",len);
                    break;
            }


        }
        if (cellType===CellType.Row)
        {
            switch (tempType) {
                case TempType.TwoRows:
                    this.setTextClass(mycell, "fs2",len);
                    break;
                case TempType.ThreeRows:
                    this.setTextClass(mycell, "fs1",len);
                    break;
            }
        }
        mycell.template = tempType;
        //
        return mycell
    }
    private setTextClass(cell: TCellVo, textClass:string,len:number): void {

        for (let i = 0; i < len; i++) {
            cell.addTextClassAt(i, textClass);
        }
    }
    private fixTextSlice(textSlice: string[], len: number) {
        // ////console.log(textSlice);

        while (textSlice.length < len) {
            textSlice.push('?');
        }
        while (textSlice.length > len) {
            textSlice.pop();
        }
        for (let i = 0; i < textSlice.length; i++) {
            if (textSlice[i] === '') {
                textSlice[i] = '?';
            }
        }
        //////console.log("fixTextSlice", textSlice);
        return textSlice;
    }
}