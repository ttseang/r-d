import { StyleVo } from "../dataObjs/StyleVo";
import { TCellVo } from "../dataObjs/TCellVo";
import { StyleType } from "../mc/Constants";
import { MainStorage } from "../mc/MainStorage";

export class StyleUtil {
    public static removeStyle(selectedStyle: StyleVo, type: StyleType, cell: TCellVo) {
        const ms: MainStorage = MainStorage.getInstance();
        const styles: StyleVo[] = ms.getStyleByType(type);
        for (let i: number = 0; i < styles.length; i++) {
            let style: StyleVo = styles[i];
            if (style.applyToCell) {
                if (style.styleValue !== selectedStyle.styleValue) {
                    cell.removeCellClass(style.styleValue);
                }
            }
            else {
                if (style.styleValue !== selectedStyle.styleValue) {
                    cell.removeTextClass(style.styleValue);
                }

            }
        }
    }
    public static convertUnicodeToHtml(text: string): string {
       
        //U+25B2
        //remove U+
          //get any text after the U+ and 4 characters
          let extra: string = text.substring(7);
          console.log(text);
          console.log(extra);
          if (text.length < 6) {
              extra = "";
          }
          let code: string = text.substring(2);
          let codeNum: number = parseInt(code, 16);
          let html: string = String.fromCharCode(codeNum)+extra;
          return html;

      }
}