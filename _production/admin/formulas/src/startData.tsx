import { CatVo } from "./dataObjs/CatVo";
import { FormulaVo } from "./dataObjs/FormulaVo";
import { MainStorage } from "./mc/MainStorage";

export class StartData
{
    private callback:Function;
    private ms:MainStorage=MainStorage.getInstance();
    private index:number=0;
    private currentCat:CatVo | null=null;
    constructor(callback:Function)
    {
        this.callback=callback;
    }
    public getCat()
    {
       this.currentCat=this.ms.cats[this.index];
       let file:string=this.currentCat.file;
       //console.log(file);
         fetch("./json/"+file+".json").then(response => response.json())
         .then(data => this.gotCat(data));
    }
    private gotCat(data:any)
    {
        //console.log(data);
        let len:number=data.length;
        for (let i = 0; i < len; i++) {
            let element = data[i];
            let symbol:string=element.symbol;
            let description:string=element.description;
            let latex:string=element.latex;
            let symbolVo:FormulaVo=new FormulaVo(description,symbol,latex,this.currentCat!.name);
            this.ms.addFormula(symbolVo);
        }
        if (this.index<this.ms.cats.length-1)
        {
            this.index++;
            this.getCat();
        }
        else
        {
           // this.callback();
           this.loadPText();
        }
    }
    public getStartData()
    {
        fetch("./symbols.json")
        .then(response => response.json())
        .then(data => this.gotStartData(data));
    }
    
    private gotStartData(data:any)
    {   
        
        
        //console.log(data);
        let greekLetters:any=data.Greek_Letters;
        this.addSymbols(greekLetters,"greek");
     
        let mathSymbols:any=data.Math_Symbols;
        
        let operators:any=mathSymbols.Basic_Operators;
        this.addSymbols(operators,"operators");

        let fractions:any=data.Fractions;
        //console.log(fractions);
        this.addSymbols(fractions,"fractions",true);

        
        let relations:any=mathSymbols.Relations;
        
        this.addSymbols(relations,"relation");

        let others:any=mathSymbols.Other;
        
        this.addSymbols(others,"other");
        this.callback();
    }
    private addSymbols(data:any,category:string,useDescription:boolean=false )
    {
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            //
            let symbol:string=element.symbol;
            let description:string=element.description;
            if (useDescription===false)
            {
                description=symbol;
            }
            let symbolVo:FormulaVo=new FormulaVo(description,symbol,category);
            this.ms.addFormula(symbolVo);
        }
    }
    public loadPText()
    {
        fetch("./txt/predict.txt")
        .then(response => response.text())
        .then(data => this.gotPText(data));
    }
    private gotPText(data:string)
    {
        //console.log(data);
        let lines:string[]=data.split("\n");
        this.ms.predictiveWords=lines;
        this.callback();
    }
}