export class FormulaVo
{
    public description:string;
    public laxtext:string;
    public symbol:string;
    public category:string;
    public useSurround:boolean=false;
    constructor(description:string,symbol:string,laxtext:string,category:string="",useSurround:boolean=false)
    {
        this.description=description;
        this.symbol=symbol;
        this.laxtext=laxtext;
        this.category=category;
        this.useSurround=useSurround;
    }
}