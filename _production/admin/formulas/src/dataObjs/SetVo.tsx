export class SetVo
{
    public content:string;
    public type:number;
    public startPos:number;
    public startChar:string;
    public endChar:string;

    constructor(content:string,type:number,startPos:number,startChar:string,endChar:string)
    {
        this.content=content;
        this.type=type;
        this.startPos=startPos;
        this.startChar=startChar;
        this.endChar=endChar;
    }
}
export enum SetType
{
    CURLY_BRACKETS=1,
    SQUARE_BRACKETS=2,
    PARENTHESES=3,
    POWERS=4,
    SUBSCRIPTS=5,
    FRACTIONS=6
}