export class EditButtonVo {
    public title:string;
    public symbol:string;
    public action:number;

    constructor(title:string,symbol:string,action:number) {
        this.title = title;
        this.symbol = symbol;
        this.action = action;
    }
}