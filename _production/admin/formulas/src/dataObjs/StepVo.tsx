export class StepVo
{
    public latex:string;
    public mml:string;
    constructor(latex:string,mml:string)
    {
        this.latex=latex;
        this.mml=mml;
    }
}