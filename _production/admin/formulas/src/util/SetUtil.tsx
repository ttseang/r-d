import { SetType, SetVo } from "../dataObjs/SetVo";

export class SetUtil
{
    public static getSets(text: string) {

        let sets: SetVo[] = [];
        let sets2: SetVo[] = [];
        sets2 = SetUtil.setScan(text, "{", "}", SetType.CURLY_BRACKETS);
        for (let i: number = 0; i < sets2.length; i++) {
            sets.push(sets2[i]);
        }
        sets2 = SetUtil.setScan(text, "(", ")", SetType.PARENTHESES);
        for (let i: number = 0; i < sets2.length; i++) {
            sets.push(sets2[i]);
        }
        sets2 = SetUtil.setScan(text, "[", "]", SetType.SQUARE_BRACKETS);
        for (let i: number = 0; i < sets2.length; i++) {
            sets.push(sets2[i]);
        }
      //  console.log(sets);
        //sort on startPos
        sets.sort((a, b) => {
            return a.startPos - b.startPos;
        });

       // this.ms.sets = sets;
        return sets;
    }
    public static setScan(text: string, startSymbol: string, endSymbol: string, type: number) {
   //     console.log("text", text, "startSymbol", startSymbol, "endSymbol", endSymbol, "type", type);
        //get all the text between each { and } put it in an array
        let sets: SetVo[] = [];
        let textArray: string[] = text.split("");
        let start: number = 0;
        let end: number = 0;
        let level: number = 0;
        for (let i: number = 0; i < textArray.length; i++) {
            if (textArray[i] === startSymbol) {
           //     console.log("start");
                if (level === 0) {
                    start = i;
                }
                level++;
            }
            if (textArray[i] === endSymbol) {
                //console.log("end");
                level--;
                if (level === 0) {
                    end = i;
                    let setText: string = text.substring(start + 1, end);
                    let setVo: SetVo = new SetVo(setText, type, start,startSymbol,endSymbol);

                    sets.push(setVo);


                }
            }
        }
        // console.log(sets);

        return sets;
    }
}