export class MMLUtil {
    public static getMMLFromLatex(text: string): string {
        let temml = (window as any).temml;
        //console.log(temml);
        if (!temml) {
            return "";
        }
        let options: any = { displayMode: false, trust: true };
        let mml = temml.renderToString(text, options);
        mml = MMLUtil.applyTweaks(mml);
        //  MMLUtil.applyTweaks(mml);
        return mml;
    }
    public static applyTweaks(mml: string) {

       // let colors:string[]=["ttpurple","ttblue","ttgreen","ttorange","ttblack"];

        //make an html element and add the mml to it
        let div = document.createElement("div");
        div.innerHTML = mml;
       // let inTag: boolean = false;
      //  let openTag: string = "";


        //loop through all the elements and apply the tweaks
        let elements = div.getElementsByTagName("*");
        for (let i = 0; i < elements.length; i++) {
            let element = elements[i];
            //console.log(element);

            /* if (openTag !== "" && element.tagName === openTag) {
                inTag = false;

            }
            if (inTag) {
                //add the mathvariant attribute and set it to normal
                element.setAttribute("mathvariant", "normal");
            }
            //if the element has a class of ovrscr
            if (element.classList.contains("ovrscr")) {
                //add the mathvariant attribute and set it to normal
                element.setAttribute("mathvariant", "normal");
                openTag = element.tagName;
                inTag = true;
            } */
            //check for colors in the class
           /*  for(let j=0;j<colors.length;j++){
                if(element.classList.contains(colors[j])){
                    element.setAttribute("mathvariant", "normal");
                }
            } */
            //check for mo tags with an = + - * / in them
            if(element.tagName==="mo"){
                let text:string | null=element.textContent;
                if(text==="="||text==="+"||text==="-"||text==="*"||text==="/"){
                    //add a class of ttblack
                    element.classList.add("ttfont");
                }
            }
            element.setAttribute("mathvariant", "normal");
        }
        //return the inner html of the div
        return div.innerHTML;

    }
    public static convertUnicodeToHtml(text: string): string {

        //U+25B2
        //remove U+
        //get any text after the U+ and 4 characters
        let extra: string = text.substring(7);
        //console.log(text);
        // console.log(extra);
        if (text.length < 6) {
            extra = "";
        }
        let code: string = text.substring(2);
        let codeNum: number = parseInt(code, 16);
        let html: string = String.fromCharCode(codeNum) + extra;
        return html;

    }
    public static cleanLatex(latex: string): string {
        //remove all line breaks
        latex = latex.replace(/\n/g, "");
        latex = latex.trim();
        if (latex.startsWith("$")) {
            latex = latex.substring(1);
        }
        if (latex.endsWith("$")) {
            latex = latex.substring(0, latex.length - 1);
        }
        //if latex starts with \[ remove it
        //if latex ends with \] remove it
        if (latex.startsWith("\\[")) {
            latex = latex.substring(2);
        }
        if (latex.endsWith("\\]")) {
            latex = latex.substring(0, latex.length - 2);
        }
        if (latex.includes(":")) {
            //split on the : and take the second part
            let parts: string[] = latex.split(":");
            if (parts.length > 1) {
                latex = parts[1];
            }
        }

        //\color{purple} to \class{ttpurple}
        //\color{blue} to \class{ttblue}
        //\color{green} to \class{ttgreen}
        //\color{orange} to \class{ttorange}
        //\color{black} to \class{ttblack}
        latex = latex.replace(/\\color{purple}/g, "\\class{ttpurple}");
        latex = latex.replace(/\\color{blue}/g, "\\class{ttblue}");
        latex = latex.replace(/\\color{green}/g, "\\class{ttgreen}");
        latex = latex.replace(/\\color{orange}/g, "\\class{ttorange}");
        latex = latex.replace(/\\color{black}/g, "\\class{ttblack}");
        

        return latex;
    }
    public static firstAid(latex: string): string {

         //remove the first \begin{equation} and the last \end{equation}
         if (latex.startsWith("\\begin{equation}")) {
            latex = latex.substring(16);
        }
        if (latex.endsWith("\\end{equation}")) {
            latex = latex.substring(0, latex.length - 14);
        }
        //remove the first \documentclass{article} and the last \end{document}
        if (latex.startsWith("\\documentclass{article}")) {
            latex = latex.substring(23);
        }
        if (latex.endsWith("\\end{document}")) {
            latex = latex.substring(0, latex.length - 14);
        }
        //remove the first \begin{align} and the last \end{align}
        if (latex.startsWith("\\begin{align}")) {
            latex = latex.substring(13);
        }
        //remove the first \( and the last \)
        if (latex.startsWith("\\(")) {
            latex = latex.substring(2);
        }
        if (latex.endsWith("\\)")) {
            latex = latex.substring(0, latex.length - 2);
        }
        //remove the first \[ and the last \]
        if (latex.startsWith("\\[")) {
            latex = latex.substring(2);
        }
        if (latex.endsWith("\\]")) {
            latex = latex.substring(0, latex.length - 2);
        }
        //remove the first $ and the last $
        if (latex.startsWith("$")) {
            latex = latex.substring(1);
        }
        if (latex.endsWith("$")) {
            latex = latex.substring(0, latex.length - 1);
        }
        
        //if the latex contains a colon : then remove everything before the colon and the colon
        if (latex.includes(":")) {
            //split on the : and take the second part
            let parts: string[] = latex.split(":");
            if (parts.length > 1) {
                latex = parts[1];
            }
        }
        //search for the word where and take away everything after it including the word where
        if (latex.includes("where")) {
            //split on the : and take the second part
            let parts: string[] = latex.split("where");
            if (parts.length > 1) {
                latex = parts[0];
            }
        }
        //are
        //search for the word are and take away everything after it including the word are
        if (latex.includes("are")) {
            //split on the : and take the second part
            let parts: string[] = latex.split("are");
            if (parts.length > 1) {
                latex = parts[0];
            }
        }

        return latex;

    }

}