export class MainController
{
   
    private static instance: MainController;
    private constructor() { }
    public static getInstance(): MainController {
        if (!MainController.instance) {
            MainController.instance = new MainController();
        }
        return MainController.instance;
    }

    public changeStep(index: number) {
       // console.log("changeStep", index);
    }
    public addStep:Function =()=>{};
    public cloneStep:Function =()=>{};
    public moveStepUp:Function =()=>{};
    public moveStepDown:Function =()=>{};
    public deleteStep:Function =()=>{};    
    public doSave:Function =()=>{};
    public doLoad:Function =()=>{};
    public doNew:Function =()=>{};
    public showSaveScreen:Function =()=>{};

    public redo:Function =()=>{};
    public undo:Function =()=>{};
    public clear:Function =()=>{};
    public setMessage:Function =()=>{};
    public setLatex:Function =()=>{};
    public setScratch:Function =()=>{};

    public surroundText:Function =()=>{};
    public insertText:Function =()=>{};
}