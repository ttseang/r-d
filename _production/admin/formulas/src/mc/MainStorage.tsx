import { CatVo } from "../dataObjs/CatVo";
import { ColorVo } from "../dataObjs/ColorVo";
import { FormulaVo } from "../dataObjs/FormulaVo";
import { SetVo } from "../dataObjs/SetVo";

export class MainStorage {

    public cats: CatVo[] = [];
    public steps: string[] = [""];
    public scrollToBottomStep: boolean = true;
    public currentStep: number = 0;
    public lti: string = "";

    public errorStr: string = "";
    public predictiveWords: string[] = [];

    public history: string[][] = [];
    public historyIndex: number = 0;
    public sets: SetVo[] = [];

    public needToSave: boolean = false;
    public static instance: MainStorage;

    public showAi: boolean = true;
    public instructionText: string = "";
    public tweakText: string = "";
    public currentLatex: string = "";

    public colors: ColorVo[] = [];
    public bookmarkToAdd: string = "";

    //ai text screen storage
    public aiText: string = "";
    public tweakAiText: string = "";
    public colorText: string = "";

    public static getInstance(): MainStorage {
        if (!MainStorage.instance) {
            MainStorage.instance = new MainStorage();
        }
        return MainStorage.instance;
    }
    constructor() {
        (window as any).ms = this;
        this.init();
    }
    public addHistory() {
        this.historyIndex++;
        this.history.splice(this.historyIndex, this.history.length - this.historyIndex);
        this.history.push(this.steps.slice());
        this.historyIndex = this.history.length - 1;
    }
    public undo() {
        if (this.historyIndex > 0) {
            this.historyIndex--;
            this.steps = this.history[this.historyIndex].slice();
            this.currentStep = this.steps.length - 1;
        }
        else {
            this.steps = [""];
            this.currentStep = 0;
        }
    }
    public redo() {
        if (this.historyIndex < this.history.length - 1) {
            this.historyIndex++;
            this.steps = this.history[this.historyIndex].slice();
            this.currentStep = this.steps.length - 1;
        }
    }
    private init() {

        //basic
        this.cats.push(new CatVo("Operators", "ops"));
        this.cats.push(new CatVo("Overscores", "overscores"));
        this.cats.push(new CatVo("Equations/Notations", "equations"));
        //colors
        this.cats.push(new CatVo("Colors", "colors"));
        //delimiters
        this.cats.push(new CatVo("Delimiters", "delimiters"));


        this.formulaList = [];
        /*     43718b
    5e7f57
    cc9249
    675a75 */
        //black
        this.colors.push(new ColorVo("black", "#000000"));
        this.colors.push(new ColorVo("blue", "#43718b"));
        this.colors.push(new ColorVo("green", "#5e7f57"));
        this.colors.push(new ColorVo("orange", "#cc9249"));
        this.colors.push(new ColorVo("purple", "#675a75"));
        
    }
    getColorByName(name: string): ColorVo | null {
        for (let i = 0; i < this.colors.length; i++) {
            let color: ColorVo = this.colors[i];
            if (color.name === name) {
                return color;
            }
        }
        return null;
    }
    public formulaList: FormulaVo[] = [];
    public addFormula(formula: FormulaVo) {
        this.formulaList.push(formula);
    }
    public getFormulaListByCategory(category: string): FormulaVo[] {

        let result: FormulaVo[] = [];
        for (let i = 0; i < this.formulaList.length; i++) {
            let formula: FormulaVo = this.formulaList[i];
            if (formula.category === category) {
                result.push(formula);
            }
        }
        return result;

    }
    public addStep(text: string) {
        this.steps.push(text);
        this.currentStep = this.steps.length - 1;
        this.scrollToBottomStep = true;
    }
    public getStep(step: number): string {
        return this.steps[step];
    }
    public getCurrentStep(): string {
        return this.steps[this.currentStep];
    }
    public deleteStep() {
        this.steps.splice(this.currentStep, 1);
        this.currentStep--;
        if (this.currentStep < 0) {
            this.currentStep = 0;
        }

    }
    public moveStepDown() {
        if (this.currentStep > 0) {

            let temp: string = this.steps[this.currentStep - 1];
            this.steps[this.currentStep - 1] = this.steps[this.currentStep];
            this.steps[this.currentStep] = temp;
            this.currentStep--;
        }
    }
    public moveStepUp() {
        if (this.currentStep < this.steps.length - 1) {

            let temp: string = this.steps[this.currentStep + 1];
            this.steps[this.currentStep + 1] = this.steps[this.currentStep];
            this.steps[this.currentStep] = temp;
            this.currentStep++;
        }

    }
    public cloneStep() {
        let text: string = this.getCurrentStep();
        this.addStep(text);
    }
    
}