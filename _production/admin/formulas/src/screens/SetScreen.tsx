import React, { Component } from 'react';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import { SetVo } from '../dataObjs/SetVo';
import { SetUtil } from '../util/SetUtil';
interface MyProps { latex: string }
interface MyState { textSets: SetVo[] }
class SetScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private latex: string = "";
    private orignalLatex: string = "";
    private orignalSets: SetVo[] = [];
    private mc: MainController = MainController.getInstance();
    private ignoreList: string[] = ["bmatrix", "ovrscr avg", "ovrscr sum", "ovrscr ray", "ovrscr seg", "ovrscr pav", "ovrscr pav low", "ovrscr lin", "ovrscr arc"];

    constructor(props: MyProps) {
        super(props);
        let sets: SetVo[] = SetUtil.getSets(this.props.latex);
        this.state = { textSets: sets };
        this.latex = this.props.latex;
        this.orignalLatex = this.props.latex;
    }
    public changeText(e: React.ChangeEvent<HTMLInputElement>, index: number) {
        //  console.log(index);
        let textSets: SetVo[] = this.state.textSets.slice();
        let value: string = e.target.value;
        //  let setString:string=textSets[index].content;
       
        this.setState({ textSets: textSets });
        this.changeSet(value, index);
        textSets[index].content = value;
        /*  setText[index] = value;
         this.setState({ setText: setText });
         this.ms.sets = setText;
         this.changeSet(value, index); */
    }
    public changeSet(textValue: string, index: number) {

        let sets: SetVo[] = this.state.textSets.slice();
        let textSet: SetVo = sets[index];
        let content: string = textSet.content;

        console.log("textValue: " + textValue);
        console.log("content: " + content);

        /* //find the nth occurence of { in the latex string. nth is index
        let index2 = 0;
        let index3 = 0;
        for (let i = 0; i < index + 1; i++) {
            index2 = this.latex.indexOf('{', index3);
            index3 = index2 + 1;
        } */
        let index2: number = this.state.textSets[index].startPos;
        console.log("start index: " + index2);

        let firstHalf:string="";
        let middle:string="";
        let secondHalf:string="";
        let newText:string="";

        if (textValue.length > content.length) {

            firstHalf= this.latex.substring(0, index2);
            middle = textValue;
            secondHalf = this.latex.substring(index2 + middle.length+1);
            newText = firstHalf + textSet.startChar + textValue + textSet.endChar + secondHalf;
            console.log("greater");
        } else {

            /* firstHalf = this.latex.substring(0, index2);
            middle = textValue;
            secondHalf = this.latex.substring(index2 + middle.length);
            newText = firstHalf + textSet.startChar + textValue + textSet.endChar + secondHalf; */
            let position:number=index2+textValue.length+1;
            //split the string at the position
            firstHalf=this.latex.substring(0,position);
            secondHalf=this.latex.substring(position+1);
            newText=firstHalf+secondHalf;
            console.log("less");
        }


        console.log("firstHalf: " + firstHalf);
        console.log("middle: " + middle);
        console.log("secondHalf: " + secondHalf);


         
        //console.log("newText: "+newText);

        this.latex = newText;
        this.mc.setLatex(newText);
        let nsets: SetVo[] = SetUtil.getSets(newText);
        this.setState({ textSets: nsets });
    }
    public showPositions() {
        this.orignalSets = this.state.textSets.slice();
        this.orignalLatex = this.latex;
        let latex2:string=this.latex;
        let nums: string = "①②③④⑤⑥⑦⑧⑨⑩";
        let j = -1;
        for (let i: number = 0; i < this.ms.sets.length; i++) {

            if (this.ignoreList.indexOf(this.state.textSets[i].content) > -1) continue;
            j++;
           // let set: string = nums.charAt(j) + this.state.textSets[i].content;
           // this.changeSet(set, i);
            latex2 = this.insertAtPostion(latex2, this.state.textSets[i].startPos+i, nums.charAt(j));
        }
        console.log(latex2);
        this.mc.setLatex(latex2);
        /* let nsets: SetVo[] = SetUtil.getSets(latex2);
        this.setState({ textSets: nsets }); */
    }
    public insertAtPostion(str: string, index: number, value: string) {
        return str.substring(0, index) + value + str.substring(index);

    }
    public hidePositions() {
        this.ms.sets = this.orignalSets.slice();
        this.latex = this.orignalLatex;
        this.mc.setLatex(this.latex);
        this.setState({ textSets: this.ms.sets });
    }
    getInputBoxes() {
        let boxes: JSX.Element[] = [];
        let j: number = -1;
        for (let i = 0; i < this.state.textSets.length; i++) {
            const element = this.state.textSets[i].content;
            let key: string = 'box_' + i;
            if (this.ignoreList.indexOf(element) > -1) continue;
            j++;
            boxes.push(<div key={key}>{j + 1}.<input key={'input_' + i} type='text' value={element} onChange={(e) => this.changeText(e, i)}></input></div>);
        }
        return boxes;
    }
    render() {
        return (<div>
            <div className='setsScroll'>{this.getInputBoxes()}</div>
            <div>
                <button onMouseDown={this.showPositions.bind(this)} onMouseUp={this.hidePositions.bind(this)} className='btn btn-light'>
                    <i className="fas fa-eye"></i>
                </button>
            </div>
        </div>)
    }
}
export default SetScreen;