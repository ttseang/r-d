import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import ApiConnect from '../util/ApiConnect';
interface MyProps { callback: Function,cancelCallback:Function }
interface MyState { text: string }
class LTIScreen extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = { text: "" };
    }

    enterLTI() {
        if (this.state.text.length === 0) {
            return;
        }
        this.checkLti(this.state.text);
       // this.props.callback(this.state.text);
        //console.log(this.state.text);
    }
    //check for exisiting lti
    checkLti(lti: string) {
        const api: ApiConnect = new ApiConnect();
        api.getFileList(this.gotSearch.bind(this), lti);
    }
    gotSearch(data: any) {
      //  console.log(data);
        let len: number = data.length;
        for (let i: number = 0; i < len; i++) {
            if (data[i].lti === this.state.text) {
                alert("LTI already exists");
                return;
            }
        }
        this.props.callback(this.state.text);
    }
    onKeyDown(e: any) {
        if (e.key === 'Enter') {
            this.enterLTI();
        }
    }
    onTextChanged(e:React.ChangeEvent<HTMLInputElement>)
    {
        let text:string = e.currentTarget.value;
        text = text.toUpperCase();
        this.setState({text:text});
    }
    //input textfield
    render() {
        return (
            <Card className='ltiScreen'>
                <Card.Body>
                    <h6>Please Provide an LTI</h6>
                    <input type="text" onChange={this.onTextChanged.bind(this)} value={this.state.text} onKeyDown={this.onKeyDown.bind(this)} />
                    <div><small>Example: TT.RD.M1.T1</small></div>
                    <Button variant='danger' onClick={() => { this.props.cancelCallback(); }}>Cancel</Button>
                    <Button onClick={this.enterLTI.bind(this)}>Submit</Button>

                </Card.Body>
            </Card>)
    }
}
export default LTIScreen;