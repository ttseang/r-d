import React, { Component } from 'react';
import { Button, ButtonGroup, Card } from 'react-bootstrap';
import ApiConnect from '../util/ApiConnect';
import { ChatVo } from '../dataObjs/ChatVo';
import { MainController } from '../mc/MainController';
import { MainStorage } from '../mc/MainStorage';
import SuggestInput from '../comps/SuggestInput';
import { MMLUtil } from '../util/MMLUtil';
import { ColorVo } from '../dataObjs/ColorVo';
import ColorBox from '../comps/ColorBox';
interface MyProps { latex: string, latexCallback: Function }
interface MyState { text: string, tweakText: string,colorText:string, mode: AiMode, predict: string[], response: string, latex: string, loading: boolean }

export enum AiMode {
    CREATE=1,
    TWEAK=2,
    COLORS=3
};

class AiScreen extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    private ms: MainStorage = MainStorage.getInstance();

    private preditiveText: string[] = [];
    constructor(props: MyProps) {
        super(props);
        this.state = { text: this.ms.aiText, tweakText: this.ms.tweakAiText,colorText:this.ms.colorText, mode: AiMode.CREATE, predict: [], response: "", latex: this.props.latex, loading: false };
        this.preditiveText = this.ms.predictiveWords;
        this.mc.setScratch=this.setScratch.bind(this);
    }

    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.latex !== this.props.latex) {
            this.setState({ latex: this.props.latex });
        }
    }

    /*  onLatexChange(e: any) {
         let latex: string = e.target.value;
         latex=MMLUtil.cleanLatex(latex);
         this.ms.currentLatex=latex;
 
         this.setState({ latex: latex });
     } */
     setScratch(text:string)
     {
        this.setState({text:text});
     }
    send(text: string) {

        let prompt: string = " Show me latex code for:" + text;

        //if prompt doesn't end with a period, add one
        if (prompt.charAt(prompt.length - 1) !== ".") {
            prompt += ".";
        }


        let systemChatVo: ChatVo = new ChatVo("system", "Only return latex code. Don't tell me what you are doing.");
        let chatVo: ChatVo = new ChatVo("user", prompt);

        let messages: ChatVo[] = [];
        messages.push(systemChatVo);
        messages.push(chatVo);

        let msgObj: any = JSON.stringify(messages);

        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.sendTT(msgObj, this.gotResponse.bind(this), this.gotError.bind(this));
        this.setState({ loading: true });
    }
    sendTweak(text: string) {
        if (this.state.latex === "") {
            this.mc.setMessage("No latex to tweak");
            return;
        }
        let prompt: string = "Make the following changes:" + text;

        if (prompt.charAt(prompt.length - 1) !== ".") {
            prompt += ".";
        }

        let systemChatVo: ChatVo = new ChatVo("system", "Only return latex code. Do not tell me what you are doing.");
        let laxtextChatVo: ChatVo = new ChatVo("user", "This is the current latex:" + this.state.latex);
        let chatVo: ChatVo = new ChatVo("user", prompt);

        let messages: ChatVo[] = [];
        messages.push(systemChatVo);
        messages.push(laxtextChatVo);
        messages.push(chatVo);

        let msgObj: any = JSON.stringify(messages);

        const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.sendTT(msgObj, this.gotResponse.bind(this), this.gotError.bind(this));
        this.setState({ loading: true });
    }
    sendColor(text: string) {
        if (this.state.latex === "") {
            this.mc.setMessage("No latex to color");
            return;
        }
        let prompt: string = text;
        
        if (prompt.charAt(prompt.length - 1) !== ".") {
            prompt += ".";
        }
        
        let systemChatVo: ChatVo = new ChatVo("system", "Only return latex code. Do not tell me what you are doing.");
        let laxtextChatVo: ChatVo = new ChatVo("user", "This is the current latex:" + this.state.latex);
        let colorChatVo: ChatVo = new ChatVo("user", "change the colors by adding a class with the same name to the element with a ttprefix. For example, to make the text red, add the class 'ttred' to the element. Do not use \textcolor but use class");
        let chatVo: ChatVo = new ChatVo("user", prompt);

        let messages: ChatVo[] = [];
        messages.push(systemChatVo);
        messages.push(laxtextChatVo);
        messages.push(colorChatVo);
        messages.push(chatVo);

        let msgObj: any = JSON.stringify(messages);

         const apiConnect: ApiConnect = new ApiConnect();
        apiConnect.sendTT(msgObj, this.gotResponse.bind(this), this.gotError.bind(this));
        this.setState({ loading: true });

    }
    gotError(err: any) {
        console.log(err);
    }
    gotResponse(data: any) {
        // console.log(data);
        let choices: any[] = data.choices;
        let first: any = choices[0];
        console.log(first);
        let message: any = first.message;
        console.log(message);

        let content: any = message.content;
        console.log(content);
        //remove all $
        let latex: string = content.replace(/\$/g, '');
        console.log(latex);
        latex = MMLUtil.cleanLatex(latex);
        this.props.latexCallback(latex);
        this.ms.currentLatex = latex;
        this.setState({ latex: latex, loading: false });
    }
    copy() {
        navigator.clipboard.writeText(this.state.latex);
        this.mc.setMessage("Copied to clipboard");
    }

    updateLatex(text: string) {
        this.setState({ latex: text });
    }
    setText(text: string) {
        //set the clipboard text
        navigator.clipboard.writeText(text);
        this.mc.setMessage("Copied to clipboard");
    }

    
    getRadio() {
        let variant1: string = "primary";
        let variant2: string = "primary";
        let variant3: string = "primary";

        if (this.state.mode===AiMode.CREATE) {
            variant1 = "outline-primary";

        }
        if (this.state.mode===AiMode.TWEAK) {
            variant2 = "outline-primary";
        }
        if (this.state.mode===AiMode.COLORS) {
            variant3 = "outline-primary";
        }
        return <ButtonGroup className='cbuttons'>
            <Button variant={variant1} disabled={this.state.loading} onClick={() => { this.setState({ mode: AiMode.CREATE }) }}>Create</Button>
            <Button variant={variant2} disabled={this.state.loading} onClick={() => { this.setState({ mode: AiMode.TWEAK }) }}>Tweak</Button>
            <Button variant={variant3} disabled={this.state.loading} onClick={() => { this.setState({ mode: AiMode.COLORS }) }}>Colors</Button>
        </ButtonGroup>
    }
    getInputBox() {
        switch (this.state.mode) {
            case AiMode.CREATE:
                return <SuggestInput key="instructionText" text={this.state.text} onChange={(text: string) => { this.setState({ text: text }); this.ms.bookmarkToAdd=text; this.ms.aiText=text;}} send={this.send.bind(this)} label={'Enter Instructions'} buttonText={'OK'} loading={this.state.loading} variant={'success'} placeHolder={'Enter Instructions'} />
            

            case AiMode.TWEAK:
                return <SuggestInput key="tweakText" text={this.state.tweakText} onChange={(text: string) => { this.setState({ tweakText: text }); this.ms.tweakAiText=text; }} send={this.sendTweak.bind(this)} label={'Enter Changes'} buttonText={'Tweak'} loading={this.state.loading} variant={'warning'} placeHolder={'What do you want to change?'} />
         

            case AiMode.COLORS:
                return (
                    <div>
                        {this.getColorBoxes()}
                        <SuggestInput key="colorText" text={this.state.colorText} onChange={(text: string) => { this.setState({ colorText: text }); this.ms.colorText=text; }} send={this.sendColor.bind(this)} label={'Enter Colors'} buttonText={'Update'} loading={this.state.loading} variant={'warning'} placeHolder={'Specify colors and items'} />
                        <hr/>
                        
                    </div>
                )
           
        }
        
    }
    getColorBoxes()
    {
        let boxes:JSX.Element[]=[];
        for (let i:number=0;i<this.ms.colors.length;i++)
        {
            let color:ColorVo=this.ms.colors[i];
            boxes.push(<ColorBox key={i} colorVo={color} callback={this.addColor.bind(this)} />);
        }
        return (<div className="colorBoxes">{boxes}</div>)
    }
    addColor(colorVo:ColorVo)
    {
        let text:string=this.state.colorText;
        text+=" "+colorVo.name;
        this.setState({colorText:text});
    }
   
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    {this.getInputBox()}
                    {this.getRadio()}
                </Card.Body>
            </Card>

            <hr />
        </div>)
    }
}
export default AiScreen;
