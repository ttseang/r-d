import React, { Component } from 'react';
import EqScreen from './EqScreen';
import { Card } from 'react-bootstrap';
import { StartData } from '../startData';
import StartScreen from './StartScreen';
import FileOpenScreen from './FileOpenScreen';
import LTIScreen from './LTIScreen';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
import { StepVo } from '../dataObjs/StepVo';
import { MMLUtil } from '../util/MMLUtil';
import { Payload } from '../dataObjs/Payload';
import ApiConnect from '../util/ApiConnect';
import { FileVo } from '../dataObjs/FileVo';
import TestScreen from './TestScreen';
interface MyProps { }
interface MyState { mode: number }
class BaseScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { mode: 0 };

        this.mc.doSave = this.doSave.bind(this);
        this.mc.doLoad = this.doLoad.bind(this);
        this.mc.doNew = this.doNew.bind(this);
    }
    componentDidMount(): void {
        let sd: StartData = new StartData(this.gotStartData.bind(this));
        sd.getCat();
    }
    gotStartData() {
        //console.log("gotStartData");
        this.setState({ mode:1 });
    }
    getScreen() {
        switch (this.state.mode) {

            case -1:
                return <TestScreen />;
            case 0:
                return "Loading";

            case 1:
                return <StartScreen callback={this.chooseStart.bind(this)} />;
            case 2:
                return <FileOpenScreen goBack={() => { this.setState({ mode: 1 }) }} openCallback={this.openFile.bind(this)} />;
            case 3:
                //name screen
                return <LTIScreen callback={this.setLTI.bind(this)} cancelCallback={()=>{this.setState({mode:1})}} />;
            case 4:
                return <EqScreen />;
        }
        return "screen not found";
    }
    doSave() {
      //  console.log("doSave");
        this.mc.showSaveScreen(true);
        let steps: StepVo[] = [];
        for (let i = 0; i < this.ms.steps.length; i++) {
            let mml: string = MMLUtil.getMMLFromLatex(this.ms.steps[i]);
            let step: StepVo = new StepVo(this.ms.steps[i], mml);
            steps.push(step);
        }
      //  console.log(steps);

        let saveString: string = JSON.stringify(steps);

        let payload: Payload = new Payload(this.ms.lti, "latex", this.ms.steps[0], saveString);
        let apiConnect: ApiConnect = new ApiConnect();
        apiConnect.Save(JSON.stringify(payload), this.saveDone.bind(this));

    }
    saveDone(data: string) {
        //console.log(data);
        this.ms.needToSave = false;
        this.mc.showSaveScreen(false);
    }
    doLoad() {
        this.setState({ mode: 2 });
    }
    doNew()
    {
        this.ms.steps = [];
        this.ms.lti = "";
        this.setState({ mode: 3 });
    }
    setLTI(lti: string) {
        //console.log(lti);
        this.ms.lti = lti;
        this.setState({ mode: 4 });
    }
    chooseStart(choice: string) {
        //console.log(choice);
        switch (choice) {
            case "load":
                this.setState({ mode: 2 });
                break;
            case "new":
                this.setState({ mode: 3 });
                break;
        }
    }
    openFile(file: FileVo) {
        const apiConnect: ApiConnect = new ApiConnect();
        this.ms.lti = file.lti;
        apiConnect.getFileContent(file.lti, this.loadDone.bind(this));
    }
    loadDone(data: any) {
        let loadString: string = data.data;
        //console.log(loadString);
        let dataObj: any = JSON.parse(loadString);
       // console.log(dataObj);
        this.ms.steps = [];
        for (let i:number = 0; i < dataObj.length; i++) {
            let step: StepVo = new StepVo(dataObj[i].latex, dataObj[i].mml);
            this.ms.steps.push(step.latex);
        }
        this.setState({ mode: 4 });
       // this.loadFromString(loadString);
    }
    render() {
        return (<div>
            <Card>
                <Card.Body>
                    {this.getScreen()}
                </Card.Body>
            </Card>
        </div>)
    }
}
export default BaseScreen;