import React, { Component } from 'react';
import InsertInput from '../comps/InsertInput';
import { Button, ButtonGroup } from 'react-bootstrap';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState {text:string }
class TestScreen extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = {text:""};
        }
    render() {
        return (<div>
            output:{this.state.text}
            <InsertInput callback={(text: string) => { this.setState({ text: text }); } } text={this.state.text} useTextArea={false} showDebug={true}/>
            <ButtonGroup>
                <Button variant="outline-primary" onClick={() => {this.mc.insertText("abc") }}>Insert ABC</Button>
                <Button variant="outline-primary" onClick={() => {this.mc.surroundText("(",")")}}>Surround with parentheses</Button>
            </ButtonGroup>
        </div>)
    }
}
export default TestScreen;