import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
interface MyProps {callback:Function}
interface MyState {}
class StartScreen extends Component <MyProps, MyState>
{constructor(props:MyProps){
super(props);
this.state={};
}
render()
{
    //two buttons one for new equation and one for load equation
return (<div>
    <div className="startScreen">
        <Button onClick={()=>{this.props.callback("new")}}>New Equation</Button>
        <Button onClick={()=>{this.props.callback("load")}}>Load Equation</Button>
        <Button onClick={()=>{this.props.callback("lti")}}>Help</Button>
    </div>
        
</div>)
}
}
export default StartScreen;