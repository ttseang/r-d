import { Component } from 'react';
import { Alert, Card } from 'react-bootstrap';
import { CodeBlock } from 'react-code-blocks';
import FormulaButtons from '../comps/FormulaButtons';
import StepPanel from '../comps/StepPanel';
import { MainStorage } from '../mc/MainStorage';
import { MainController } from '../mc/MainController';
//import TopMenu from '../comps/TopMenu';
import EditBar from '../comps/EditBar';
import { MMLUtil } from '../util/MMLUtil';
import ScratchPad from '../comps/ScratchPad';
//import AiScreen from './AiScreen';
import AiScreen from './AiScreen';
import InsertInput from '../comps/InsertInput';
interface MyProps { }
interface MyState { text: string, message: string,steps: string[], mml: any, err: boolean, showSaveScreen: boolean,  showCode: boolean, showScratch: boolean, showAi: boolean, fs: number }
class EqScreen extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc: MainController = MainController.getInstance();
    private textIndex: number = 0;

    constructor(props: MyProps) {
        super(props);
        this.state = { text: "", message: "", steps: this.ms.steps, mml: "", err: false, showSaveScreen: false, showCode: false, showScratch: false, showAi: true, fs: 32 };

        this.mc.addStep = this.addStep.bind(this);
        this.mc.cloneStep = this.cloneStep.bind(this);
        this.mc.deleteStep = this.deleteStep.bind(this);
        this.mc.moveStepUp = this.moveStepUp.bind(this);
        this.mc.moveStepDown = this.moveStepDown.bind(this);
        this.mc.changeStep = this.changeStep.bind(this);
        this.mc.showSaveScreen = (show: boolean) => {
            this.setState({ showSaveScreen: show });
        }
        this.mc.setMessage = this.setMessage.bind(this);
        this.mc.setLatex = this.setLatex.bind(this);
    }
    setLatex(latex: string) {
        this.setState({ text: latex });
        this.updateMML(latex);
        this.ms.steps[this.ms.currentStep] = latex;
    }
    setMessage(message: string) {
        console.log("setMessage:" + message);
        this.setState({ message: message });
        //in 3 seconds clear the message
        setTimeout(() => {
            this.setState({ message: "" });
        }, 3000);
    }
    componentDidMount(): void {
        if (this.ms.steps.length > 0) {
            this.updateMML(this.ms.steps[this.ms.currentStep]);
        }
        window.onbeforeunload = (e: any) => {
            if (this.ms.needToSave) {
                var dialogText = 'Dialog text here';
                e.returnValue = dialogText;
                return dialogText;
            }
        };

    }

    onTextChange(e: any) {

        let text: string = e.target.value;

        this.updateMML(text);
        this.ms.addHistory();
        this.ms.needToSave = true;
        this.setState({ text: text, steps: this.ms.steps});
        //this.getSets(text);
    }
    onTextChange2(text: string) {

        
        this.updateMML(text);
        this.ms.addHistory();
        this.ms.needToSave = true;
        this.setState({ text: text, steps: this.ms.steps});
    }
    updateMML(text: string) {
        this.ms.steps[this.ms.currentStep] = text;
        let mml: string = MMLUtil.getMMLFromLatex(text);

        //check for the word error in the mml
        let err: boolean = false;
        if (mml.indexOf("error") > -1) {
            this.ms.errorStr = mml;
            mml = this.state.mml;
            err = true;
        }
        this.setState({ text: text, mml: mml, err: err });
        //this.getSets(text);
    }

    getMML() {
        return <div dangerouslySetInnerHTML={{ __html: this.state.mml }}></div>;
    }
    getCode() {
        let formated: string = this.state.mml;
        //replace all > with > and a line break
        formated = formated.replace(/>/g, ">\n");
        //replace all < with < and a line break
        formated = formated.replace(/</g, "\n<");
        //remove all empty lines
        formated = formated.replace(/^\s*[\r\n]/gm, "");
        return <div className="cb"><h4>MathML Code</h4><CodeBlock text={formated} language="html" showLineNumbers={true} wrapLines={true} /></div>

    }
    
    addText(text: string) {
        let t: string = this.state.text;
        t += text;
        this.updateMML(t);
        this.ms.addHistory();
        this.ms.needToSave = true;
        this.setState({ text: t, steps: this.ms.steps });
       // //this.getSets(text);
    }
    getErrorText() {
        if (this.state.message !== "") {
            return <Alert variant="warning">{this.state.message}</Alert>
        }
        if (this.state.err) {
            return <Alert variant="danger">Error in equation
                <div dangerouslySetInnerHTML={{ __html: this.ms.errorStr }}></div>;
            </Alert>
        }
        return <Alert variant="success">Syntax is correct</Alert>
    }
    changeStep(step: number) {
        this.ms.currentStep = step;
        let text: string = this.ms.steps[this.ms.currentStep];
        this.updateMML(text);
        this.ms.currentLatex=text;
        this.setState({ text: text, steps: this.ms.steps });
       // //this.getSets(text);
    }
    addStep() {
        this.ms.addStep("");
        this.setState({ steps: this.ms.steps });
        this.ms.addHistory();
        this.ms.needToSave = true;
        this.mc.changeStep(this.ms.currentStep);
    }
    cloneStep() {
        this.ms.cloneStep();
        this.setState({ steps: this.ms.steps });
        this.ms.needToSave = true;
        this.ms.addHistory();
    }
    deleteStep() {
        this.ms.deleteStep();
        this.setState({ steps: this.ms.steps });
        this.ms.needToSave = true;
        this.ms.addHistory();
    }
    moveStepUp() {
        this.ms.moveStepUp();
        this.setState({ steps: this.ms.steps });
        this.ms.needToSave = true;
        this.ms.addHistory();
    }
    moveStepDown() {
        this.ms.moveStepDown();
        this.setState({ steps: this.ms.steps });
        this.ms.needToSave = true;
        this.ms.addHistory();
    }
    doEditAction(action: number) {
        switch (action) {
            case 0:
                //undo
                this.ms.undo();
                this.refresh();
                this.ms.needToSave = true;
                break;
            case 1:
                //redo
                this.ms.redo();
                this.refresh();
                this.ms.needToSave = true;
                break;
            case 2:
                //clear
                this.ms.steps[this.ms.currentStep] = "";
                this.refresh();
                this.ms.needToSave = true;
                break;
            case 3:
                //copy
                //console.log("copy");
                navigator.clipboard.writeText(this.state.text);
                this.setMessage("Copied to clipboard");
                break;
            case 4:
                //save
                this.mc.doSave();
                break;
            case 5:
                //toggle code
                this.setState({ showCode: !this.state.showCode, showScratch: false });
                break;
            case 6:
                //decresae font size
                if (this.state.fs > 8) {
                    this.setState({ fs: this.state.fs - 2 });
                }
                break;
            case 7:
                //increase font size
                if (this.state.fs < 64) {
                    this.setState({ fs: this.state.fs + 2 });
                }
                break;
            case 8:
                //toggle scratch
                this.setState({ showScratch: !this.state.showScratch, showCode: false});
                break;
            case 9:
                //toggle ai
                this.ms.showAi=!this.ms.showAi;
                this.setState({ showAi: !this.state.showAi, showCode: false, showScratch: false});
                break;          

                case 11:
                    this.setState({ showScratch: !this.state.showScratch, showCode: false});
                break;

            case 12:
                console.log(this.state.text);
                //apply first aid
                let text:string=this.state.text;
                text=MMLUtil.firstAid(text);
                console.log("after="+text);
                this.setState({text:text},()=>{
                    this.updateMML(text);
                }
                );
        }
    }
    refresh() {
        this.setState({ steps: this.ms.steps }, () => {
            this.updateMML(this.ms.steps[this.ms.currentStep])
        });
    }
    getMiddleUI() {
       
        if (this.state.showCode) {
            return this.getCode();
        }
        if (this.state.showScratch) {

            return <ScratchPad callback={(text:string)=>{this.mc.setScratch(text)}} />
        }
       /*  if (this.state.showAi) {
            return <AiScreen closeCallback={() => { this.setState({ showAi: false }) }} />
        } */
         if (this.state.showAi)
        {
            return "";
        }
       
        return (
            <FormulaButtons  />)
    }
    getEditBar() {
        /* if (this.state.showAi) {
            return "";
        } */
        return <EditBar callback={this.doEditAction.bind(this)}></EditBar>
    }
  
    getTop() {
        
        return (<div className='top'>
            <Card>
                <Card.Body>
                    <div className='outputArea' style={{ fontSize: this.state.fs }}>{this.getMML()}</div>
                </Card.Body>
            </Card>
           {this.getInputBox()}
        </div>)
    }
    getInputBox()
    {
        if (this.state.showAi === false) {
            return (<Card>
                <Card.Body>
                    <InsertInput callback={this.onTextChange2.bind(this)} text={this.state.text} useTextArea={true} showDebug={false} />
                   {/*  <input type="text" value={this.state.text} onChange={this.onTextChange.bind(this)} placeholder='Type Latex here' /> */}
                </Card.Body>
            </Card>)
        }
        return <AiScreen latex={this.state.text} latexCallback={(latex:string)=>{
            this.setState({text:latex});
            this.updateMML(latex);
            this.ms.addHistory();
        }} />
    }
    render() {
        if (this.state.showSaveScreen) {
            return (<div>
                SAVING
            </div>)
        }

        //input field for equation
        //out area for result
        return (<div>
            <div className='eqGroup'>
                <div className='steps'>
                    {this.getErrorText()}
                    <strong>{this.ms.lti}</strong>
                    <StepPanel steps={this.state.steps} />
                </div>
                <div className='eqScreen'>
                    {this.getTop()}
                    <Card>
                        <Card.Body>
                            {this.getEditBar()}
                            {this.getMiddleUI()}
                        </Card.Body>
                    </Card>
                    {/* {this.getCode()} */}
                </div></div>
        </div>
        )
    }
}
export default EqScreen;