import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

import { MainController } from '../mc/MainController';
import { MMLUtil } from '../util/MMLUtil';


interface MyProps { formula: string, index: number, selected: boolean }
interface MyState { formula: string, selected: boolean, index: number }
class StepCard extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { formula: props.formula, selected: props.selected, index: props.index };
    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {

        if (prevProps.formula !== this.props.formula) {
            this.setState({ formula: this.props.formula });
        }

        if (prevProps.selected !== this.props.selected) {
            this.setState({ selected: this.props.selected });
        }
        if (prevProps.index !== this.props.index) {
            this.setState({ index: this.props.index });
        }
    }

    getMML() {
        let mml: string = MMLUtil.getMMLFromLatex(this.state.formula);
        if (mml.indexOf("error") > -1) {
            mml = this.state.formula;
        }
        //return dangerouslySetInnerHTML={{ __html: this.convertToMML(this.state.formula) }}
        return <div dangerouslySetInnerHTML={{ __html: mml }}></div>;
    }
    render() {
        let classes: string = 'stepCard';
        if (this.state.selected) {
            classes += ' selected';
        }
        return (<Card className={classes} onClick={() => { this.mc.changeStep(this.props.index) }}>
            {<Card.Header>Step {this.state.index}</Card.Header>}
            <Card.Body>
                <div className='stepFormula'>
                    {this.getMML()}
                </div>
            </Card.Body>
        </Card>)
    }
}
export default StepCard;