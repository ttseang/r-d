import React, { Component } from 'react';
import { EditButtonVo } from '../dataObjs/EditButtonVo';
import { Button, ButtonGroup } from 'react-bootstrap';
import { MainStorage } from '../mc/MainStorage';
interface MyProps {callback:Function}
interface MyState { }
class EditBar extends Component<MyProps, MyState>
{
    private ms:MainStorage = MainStorage.getInstance();

    private editButtons:EditButtonVo[] = [];
        constructor(props: MyProps) {
            super(props);
            this.state = {};

            
            
        }
    makeButtons()
    {
        this.editButtons=[];
        this.editButtons.push(new EditButtonVo('undo','fa-undo',0));
            this.editButtons.push(new EditButtonVo('redo','fa-redo',1));
            //clear
            this.editButtons.push(new EditButtonVo('clear','fa-trash',2));
            //copy
            this.editButtons.push(new EditButtonVo('copy','fa-copy',3));
            //save
            this.editButtons.push(new EditButtonVo('save','fa-save',4));
            //code
            this.editButtons.push(new EditButtonVo('code','fa-code',5));
            //scratch pad
            this.editButtons.push(new EditButtonVo('scratch pad','fa-sticky-note',8));
            //bookmark <i class="far fa-bookmark"></i>
            this.editButtons.push(new EditButtonVo('bookmark','fa-bookmark',11));
            //font size decrease
            this.editButtons.push(new EditButtonVo('font size decrease','fa-search-minus',6));
            //font size increase
            this.editButtons.push(new EditButtonVo('font size increase','fa-search-plus',7));
            //<i class="fas fa-pencil-alt"></i>
         //   this.editButtons.push(new EditButtonVo('fill in blanks','fa-pencil-alt',10));
            //ai button <i class="fas fa-user-astronaut"></i>

            //<i class="fas fa-first-aid"></i>
            //quick fix
            this.editButtons.push(new EditButtonVo('quick fix','fa-first-aid',12));
            if (this.ms.showAi===true)
            {
                //wrench <i class="fas fa-wrench"></i>
                this.editButtons.push(new EditButtonVo('manual','fas fa-wrench',9));
            }
            else
            {
                this.editButtons.push(new EditButtonVo('ai assist','fa-user-astronaut',9));
            }
    }
    doAction(index:number)
    {
        this.props.callback(index);
    }
    getButtons()
    {
        this.makeButtons();
        let buttons:JSX.Element[] = [];
        for (let i = 0; i < this.editButtons.length; i++) {
            const btn:EditButtonVo = this.editButtons[i];
            let key:string = 'btn_' + i;
            let icon:string = 'fas ' + btn.symbol;
            buttons.push(<Button variant='light' key={key} title={btn.title} onClick={()=>this.doAction(btn.action)}><i className={icon}></i></Button>);
        }
        return <ButtonGroup>{buttons}</ButtonGroup>;
    }
    render() {
        return (<div className='editButtons'>
            {this.getButtons()}
        </div>)
    }
}
export default EditBar;