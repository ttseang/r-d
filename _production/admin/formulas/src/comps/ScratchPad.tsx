import { Component } from 'react';
import { Button, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import { MainStorage } from '../mc/MainStorage';
interface MyProps { callback: Function }
interface MyState { text: string, scratch: string[] }
class ScratchPad extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = { text: "", scratch: this.getFromStorage()};
    }
    componentDidMount(): void {
        if (this.ms.bookmarkToAdd !== "") {
            this.addScratch(this.ms.bookmarkToAdd);
            this.ms.bookmarkToAdd = "";
        }
    }
    getFromStorage() {
        let scratch: string[] = JSON.parse(localStorage.getItem('scratch') || '[]');
        return scratch;
    }
    storeLocally() {
        let scratch: string[] = this.state.scratch;
        localStorage.setItem('scratch', JSON.stringify(scratch));
    }
    scratchClick(text: string) {
        this.props.callback(text);
    }
    addScratch(text: string) {
        if (this.ms.showAi === false) {
            this.addScratchLatex(text);
            return;
        }
        let scratch: string[] = this.state.scratch;
        let textToAdd: string = "prompt:" + text;
        scratch.push(textToAdd);
        this.setState({ scratch: scratch, text: '' }, this.storeLocally);
    }
    addScratchLatex(text: string) {
        let scratch: string[] = this.state.scratch;
        let textToAdd: string = "latex:" + text;
        scratch.push(textToAdd);
        this.setState({ scratch: scratch, text: '' }, this.storeLocally);
    }
    removeScratch(index: number) {
        let scratch: string[] = this.state.scratch;
        scratch.splice(index, 1);
        this.setState({ scratch: scratch }, this.storeLocally);
    }
    getList() {
        let scratch: string[] = this.state.scratch;
        console.log(scratch);
        let list: JSX.Element[] = [];
        for (let i = 0; i < scratch.length; i++) {
            const element = scratch[i];
            //if mode is 0, then it is prompt, if 1 then it is latex
            if ((element.indexOf("prompt:") === 0 && this.ms.showAi===true) || (element.indexOf("latex:") === 0 && this.ms.showAi===false)) {
                //  const mml: string = MMLUtil.getMMLFromLatex(element);

                //remove prompt: or latex: from the string
                let text: string = element.substring(element.indexOf(":") + 1);


                list.push(<ListGroupItem key={'scratch_' + i} onClick={() => this.scratchClick(text)}>
                    <Row>
                        <Col xs={10}>{text}</Col>
                        <Col xs={2}><Button variant='light' onClick={() => this.removeScratch(i)}>X</Button></Col>
                    </Row>

                </ListGroupItem>);
            }
        }
        return list;
    }
    getMML(mml: string) {
        return <div dangerouslySetInnerHTML={{ __html: mml }}></div>;
    }
    render() {
        return (<div>
            <h3>Scratch Pad</h3>
            <ListGroup className='scroll3'>
                {this.getList()}
            </ListGroup>
            <input type='text' value={this.state.text} onChange={(e) => this.setState({ text: e.target.value })}></input>
            <Button variant='light' onClick={() => this.addScratch(this.state.text)}>Add</Button>
        </div>)
    }
}
export default ScratchPad;