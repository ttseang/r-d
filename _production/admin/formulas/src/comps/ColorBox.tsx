import React, { Component } from 'react';
import { ColorVo } from '../dataObjs/ColorVo';
interface MyProps { colorVo: ColorVo, callback: Function }
interface MyState { }
class ColorBox extends Component<MyProps, MyState>
{
    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div className='colorBox'>
            <div className='colorBoxColor' style={{ backgroundColor: this.props.colorVo.color }} onClick={() => { this.props.callback(this.props.colorVo) }}></div>
        </div>)
    }
}
export default ColorBox;