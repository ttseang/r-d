import React, { Component } from 'react';
import { MainController } from '../mc/MainController';
interface MyProps {text:string,callback:Function,useTextArea:boolean,showDebug:boolean}
interface MyState { text: string,selectStart:number,selectEnd:number,selection:string,startText:string,endText:string }
class InsertInput extends Component<MyProps, MyState>
{

    private mc: MainController = MainController.getInstance();
    private inputRef: any;
    constructor(props: MyProps) {
        super(props);
        this.state = { text: this.props.text,selectStart:0,selectEnd:0,selection:"",startText:"",endText:"" };
        //make a ref to the input element
        this.inputRef = React.createRef();

    }
    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any): void {
        if (prevProps.text !== this.props.text) {
            this.setState({ text: this.props.text });
        }
    }
    componentDidMount(): void {
        this.mc.insertText = this.insertText.bind(this);
        this.mc.surroundText = this.surroundText.bind(this);
    }
    setCaretPos(e: any) {
       // console.log("setCaretPos");
        let caretPos = this.inputRef.current.selectionStart;
        let caretPosEnd = this.inputRef.current.selectionEnd;

        let startText: string = this.inputRef.current.value.substring(0, caretPos);
        let endText: string = this.inputRef.current.value.substring(caretPosEnd);


        this.setState({selectStart:caretPos,selectEnd:caretPosEnd,selection:this.inputRef.current.value.substring(caretPos,caretPosEnd),startText:startText,endText:endText});

      //  console.log(this.inputRef.current.selectionStart);

    }
    afterUpdate() {
        console.log("afterUpdate");
        this.props.callback(this.state.text);
        this.inputRef.current.focus();
        this.inputRef.current.selectionStart = this.state.selectStart;
        this.inputRef.current.selectionEnd = this.state.selectStart;

    }
    insertText(text: string) {
        console.log("insertText");
        let newText: string = this.state.text.substring(0, this.state.selectStart) + text + this.state.text.substring(this.state.selectEnd);
        this.setState({ text: newText },this.afterUpdate.bind(this));
    }
    surroundText(start: string, end: string) {
        let newText: string = this.state.startText + start + this.state.selection + end + this.state.endText;
        this.setState({ text: newText },this.afterUpdate.bind(this));
    }
    onTextChanged(e: any) {
        let caretPos = e.target.selectionStart;
        this.setState({ text: e.target.value,selectStart:caretPos,selectEnd:caretPos },this.afterUpdate.bind(this));
    }
    getDebugInfo() {
        if (!this.props.showDebug) return null;
        return (<div> <small>caretPos={this.state.selectStart}</small>
        <small> caretPosEnd={this.state.selectEnd}</small>
        <small> startText={this.state.startText}</small>
        <small> middleText={this.state.selection}</small>
        <small> endText={this.state.endText}</small>                
        </div>)
    }
    getInput()
    {
        if (this.props.useTextArea)
        {
            return <textarea ref={this.inputRef}  value={this.state.text} onKeyUp={this.setCaretPos.bind(this)} onMouseUp={this.setCaretPos.bind(this)} onChange={this.onTextChanged.bind(this)} />
        }
        else
        {
             return(<input type="text" ref={this.inputRef}  value={this.state.text} onKeyUp={this.setCaretPos.bind(this)} onMouseUp={this.setCaretPos.bind(this)} onChange={this.onTextChanged.bind(this)} />);
        }
    }   
    render() {
        return (<div className='insertInput'>            
            {this.getInput()}
            {this.getDebugInfo()}
        </div>)
    }
}
export default InsertInput;