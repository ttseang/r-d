import React, { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { MainStorage } from '../mc/MainStorage';
interface MyProps { text: string, onChange: Function, send: Function, label: string, buttonText: string, variant: string, loading: boolean, placeHolder: string }
interface MyState { text: string, loading: boolean, predict: string[] }
class SuggestInput extends Component<MyProps, MyState>
{
    private preditiveText: string[] = [];
    private ms: MainStorage = MainStorage.getInstance();
    private textRef: React.RefObject<HTMLInputElement>;
    constructor(props: MyProps) {
        super(props);
        this.state = { text: this.props.text, loading: false, predict: [] };
        this.preditiveText = this.ms.predictiveWords;
        this.textRef = React.createRef();
    }
    componentDidUpdate(prevProps: MyProps) {
        if (prevProps.text !== this.props.text) {
            let text: string = this.props.text;
            //remomve double spaces
            text = text.replace(/  +/g, ' ');
            this.setState({ text: text });
        }
        //loading
        if (prevProps.loading !== this.props.loading) {
            this.setState({ loading: this.props.loading });
        }
    }
    onTextChange(e: any) {
        let text: string = e.target.value;
        //remomve double spaces
        text = text.replace(/  +/g, ' ');
        this.setState({ text: text }, this.findPredictiveText.bind(this));
        this.props.onChange(text);
    }
    checkForEnter(e: any) {
        if (e.keyCode === 13) {
            this.send();
        }
        if (e.code === "Escape") {
            this.setState({ text: "" });
        }
        //tab
        if (e.code === "Tab") {
            e.preventDefault();

            let predict: string = this.state.predict[0];

            if (predict) {
                this.addPredit(predict);
            }
        }
        //f1
        if (e.code === "F1") {
            e.preventDefault();
            let second: string = this.state.predict[1];
            if (second) {
                this.addPredit(second);
            }
        }
        //f2
        if (e.code === "F2") {
            e.preventDefault();
            let third: string = this.state.predict[2];
            if (third) {
                this.addPredit(third);
            }
        }
    }
    send() {
        this.props.send(this.state.text);
    }
    findPredictiveText() {
        let text: string = this.state.text;

        //try to predict the completion of the last word
        let words: string[] = text.split(" ");
        let lastWord: string = words[words.length - 1];
        let lastWordLower: string = lastWord.toLowerCase();
        let found: string[] = [];
        for (let i = 0; i < this.preditiveText.length; i++) {
            let pred: string = this.preditiveText[i];
            let predLower: string = pred.toLowerCase();
            if (predLower.startsWith(lastWordLower)) {
                if (lastWordLower !== " " && lastWordLower !== "") {
                    found.push(pred);
                }
            }
        }
        if (found.length > 0) {
            this.setState({ predict: found });
        }
        else {
            this.setState({ predict: [] });
        }
    }
    getPredictBox() {
        if (this.state.predict.length === 0) {
            return <div className='predictBox'><small>no suggestions</small></div>;
        }

        let keyStrings: string[] = ["Tab", "F1", "F2"];
        let words: JSX.Element[] = [];
        let len: number = this.state.predict.length;
        if (len > 3) {
            len = 3;
        }
        for (let i = 0; i < len; i++) {
            let key: string = keyStrings[i];
            let word: string = this.state.predict[i];
            words.push(<small key={key}>{key}:{word}</small>);
        }
        return <div className='predictBox'>{words}</div>;
    }
    addPredit(ptext: string) {
        let text: string = this.state.text;

        //add the predict to the text
        //take off the last word
        let words: string[] = text.split(" ");
        words.pop();
        let newText: string = words.join(" ");
        newText += " " + ptext + " ";

        this.setState({ text: newText, predict: [] });

        //set the cursor to the end
        let textRef: HTMLInputElement = this.textRef.current as HTMLInputElement;
        textRef.focus();
        textRef.selectionStart = newText.length;
        textRef.selectionEnd = newText.length;
        textRef.setSelectionRange(newText.length, newText.length);
        //fire the change event
        this.props.onChange(newText);
        //fake a key press of space
        
    }
    render() {
        return (<div>
            <div className='uiGroupRow'>
            <input type="text" ref={this.textRef} disabled={this.state.loading} value={this.state.text} onKeyDown={this.checkForEnter.bind(this)} onChange={this.onTextChange.bind(this)} placeholder={this.props.placeHolder} />
            <ButtonGroup vertical>
                <Button variant={this.props.variant} disabled={this.state.loading} onClick={this.send.bind(this)}>{this.props.buttonText}</Button>
                <Button variant='danger' disabled={this.state.loading} onClick={() => { this.setState({ text: "" }); this.props.onChange(""); }}>Clear</Button>
            </ButtonGroup>
            <div>                
            </div>
        </div>
            {this.getPredictBox()}
        </div>)
    }
}
export default SuggestInput;