import React, { Component } from 'react';

import { MainStorage } from '../mc/MainStorage';
import { TabList, TabPanel, Tab, Tabs } from 'react-tabs';
import { Button } from 'react-bootstrap';
import { CatVo } from '../dataObjs/CatVo';
import { FormulaVo } from '../dataObjs/FormulaVo';
import { MMLUtil } from '../util/MMLUtil';
import { MainController } from '../mc/MainController';
import ColorBox from './ColorBox';
import { ColorVo } from '../dataObjs/ColorVo';
interface MyProps { }
interface MyState { }
class FormulaButtons extends Component<MyProps, MyState>
{
    private ms: MainStorage = MainStorage.getInstance();
    private mc:MainController = MainController.getInstance();

    constructor(props: MyProps) {
        super(props);
        this.state = {};
    }
    buttonClicked(text: string,useSurround:boolean) {
        //console.log(text);
        let hasPipe:boolean = text.indexOf('|') > -1;

        if (useSurround || hasPipe) {
            //split text on |pipe|
            let parts: string[] = text.split('|pipe|');
            console.log(parts);
            this.mc.surroundText(parts[0], parts[1]);
          //  this.props.surroundCallback(text);
        }
        else {
           //  this.props.callback(text);
            this.mc.insertText(text);
        }
    }
    private getPanels() {
        let panels: JSX.Element[] = [];
        let categories: CatVo[] = this.ms.cats;
        for (let i = 0; i < categories.length; i++) {
            const cat: CatVo = categories[i];
            let buttons: JSX.Element[] = this.getButtons(cat.name);
            let key: string = 'panel_' + i;
            panels.push(<TabPanel key={key}><div className='formulaButtons'>{buttons}</div></TabPanel>);
        }
        return panels;
    }
    private getTabs() {
        let tabs: JSX.Element[] = [];
        let categories: CatVo[] = this.ms.cats;
        for (let i = 0; i < categories.length; i++) {
            const cat: CatVo = categories[i];
            let key: string = 'tab_' + i;
            tabs.push(<Tab key={key}>{cat.name}</Tab>);
        }
        return tabs;
    }
    private getUI() {
        return (<div className='eqUI'>
            <Tabs>
                <TabList>
                    {this.getTabs()}
                </TabList>
                {this.getPanels()}
            </Tabs>
        </div>
        )
    }
    convertToMML(text: string) {
        let mml: string = MMLUtil.getMMLFromLatex(text);
        return mml;
    }
    private getButtons(category: string) {
        let buttons: JSX.Element[] = [];
        let formulas: FormulaVo[] = this.ms.getFormulaListByCategory(category);
        //console.log(formulas);
        for (let i = 0; i < formulas.length; i++) {
            const element = formulas[i];
            let symbol: string = element.symbol;
            let description: string = element.description;
            if (symbol === '') {
                //skip
                continue;
            }
            let laxtext: string = element.laxtext;
            let key: string = 'formula_' + i;
            let useHTML:boolean=false;
            let useImage:boolean=false;
            //check for unicode in text
            if (symbol.indexOf('U+') !== -1) {

                symbol = MMLUtil.convertUnicodeToHtml(symbol);
                // console.log(text);
            }
           
            //check for <i in text
            if (symbol.indexOf('<i') !== -1) {
                useHTML=true;
            }
            if (symbol.indexOf('img-') !== -1) {
                useImage=true;
            }

            
          //  console.log(laxtext);
          switch (category) {
             case "Colors":
            
                let color:ColorVo | null=this.ms.getColorByName(symbol);
                if (color===null)
                {
                    color=new ColorVo("black","#000000");
                }
                buttons.push(<ColorBox key={key} colorVo={color} callback={()=>{this.buttonClicked(laxtext,true)}} />);
            break;
            case "Overscores":
                symbol = this.convertToMML(laxtext);
                buttons.push(<Button variant='light' size='lg' key={key} title={description} onClick={() => { this.buttonClicked(laxtext,element.useSurround) }} dangerouslySetInnerHTML={{ __html: symbol }}></Button>);
                break;

            default:
                if (useImage)
                {
                    //remove img-
                    symbol=symbol.substring(4);
                    let iconPath:string="./assets/icons/"+symbol+".png";
                    buttons.push(<Button variant='light' size='lg' key={key} title={description} onClick={() => { this.buttonClicked(laxtext,element.useSurround) }}><img className="buttonIcon" src={iconPath} alt={symbol} /></Button>);
                    break;
                }
                if (useHTML)
                {
                    buttons.push(<Button variant='light' size='lg' key={key} title={description} onClick={() => { this.buttonClicked(laxtext,element.useSurround) }} dangerouslySetInnerHTML={{ __html: symbol }}></Button>);
                    break;
                }
                buttons.push(<Button variant='light' size='lg' key={key} title={description} onClick={() => { this.buttonClicked(laxtext,element.useSurround) }}>{symbol}</Button>);
                break;
           
        }
        }
        return buttons;
    }
    render() {
        return (
            this.getUI() 
        )
    }
}
export default FormulaButtons;