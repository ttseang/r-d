import React, { Component } from 'react';
import { Dropdown } from 'react-bootstrap';
import { MainController } from '../mc/MainController';
interface MyProps { }
interface MyState { }
class TopMenu extends Component<MyProps, MyState>
{
    private mc: MainController = MainController.getInstance();
    
        constructor(props: MyProps) {
            super(props);
            this.state = {};
        }
    doAction(menu: number, action: number) {
        switch(action)
        {
            case 0:
                this.mc.doNew();
            break;
            case 1:
                this.mc.doSave();
            break;
            case 2:
                this.mc.doLoad();
            break;
        }
    }
    render() {
        return (<div className='topMenu'>
            <Dropdown>
                <Dropdown.Toggle variant="text" id="dropdown-basic">
                    File
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item onClick={() => { this.doAction(0, 0) }}>New</Dropdown.Item>
                    <Dropdown.Item onClick={() => { this.doAction(0, 1) }}>Save</Dropdown.Item>
                    <Dropdown.Item onClick={() => { this.doAction(0, 2) }}>Open</Dropdown.Item>
                </Dropdown.Menu>

            </Dropdown>


        </div>)
    }
}
export default TopMenu;