import './App.css';
import BaseScreen from './screens/BaseScreen';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-tabs/style/react-tabs.css';
function App() {
  return (
    <div className="App">
      <BaseScreen />
    </div>
  );
}

export default App;
