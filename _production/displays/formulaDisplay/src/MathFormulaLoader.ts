export class MathFormulaLoader {
    constructor() {

    }
    public getFileContent(lti: string, callback: Function) {
        console.log("getFileContent");
        //https://tthq.me/api/pr/getequation/1
        //https://tthq.me/api/pr/getequation/X.TEST.01
        let url: string = "https://tthq.me/api/pr/getequation/" + lti;
        fetch(url, {
            method: "post"
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {       
                        callback(json);
                    });
                }
            })
    }
}