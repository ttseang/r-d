import { MathFormulaLoader } from "./MathFormulaLoader";

export class MathFormulaDisplay {
    static editorURL = "https://research.teachingtextbooksapp.com/w/mathGridAdmin/";
    static endPointURL = "https://tthq.me/api/pr/getequation/";

    private lti: string;
    private el: HTMLElement;
    private onReady: Function | null;
    public stepIndex: number = 0;
    private steps: Array<any> = [];

    constructor(lti: string, el: HTMLElement, onReady: Function | null = null, stepIndex: number = -1) {
        this.lti = lti;
        this.el = el;
        this.onReady = onReady;
        this.stepIndex = stepIndex;

        if (this.lti) {
            this.loadContent();
        }
    }
    private loadContent() {
        const loader: MathFormulaLoader = new MathFormulaLoader();
        loader.getFileContent(this.lti, this.onFileContentLoaded.bind(this));

    }
    onFileContentLoaded(json: any) {
        // console.log(json);
        let data: any = json.data;
        // console.log(data);
        let dataObj: any = JSON.parse(data);
        if (dataObj.length > 0) {
            this.steps = dataObj;
            this.showStep(this.stepIndex);
            this.onReady && this.onReady();
        }
    }

    showStep(stepIndex: number) {
        if (!this.el) {
            console.log("no el");
            return;
        }

        this.stepIndex = stepIndex >= 0 ? stepIndex : this.stepIndex;
        const data = this.steps[this.stepIndex];

        if (data) {
            this.el.innerHTML = data.mml;
        }
    }
}