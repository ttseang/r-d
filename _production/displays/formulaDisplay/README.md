# Teaching Textbooks Formula Display

This is a simple npm package that displays the formulas used in the Teaching Textbooks math curriculum. It is intended to be used in conjunction with the Formula Admin App, which is a separate project.

## Installation

npm install @teachingtextbooks/math-formula-display

## Usage

    import { MathFormulaDisplay } from "@teachingtextbooks/math-formula-display/dist/mathFormulaDisplay.js";

    window.onload = function () {
    //get the HTML element that will contain the formula
    const el: HTMLElement = document.getElementsByClassName("mathFormula")[0] as HTMLElement;

       //make sure the element exists
       if (el) {

          //create a new instance of the MathFormulaDisplay class and in the constructor
           //pass in the LTI, the HTML element, and a callback function

          const forumlaDisplay: MathFormulaDisplay = new MathFormulaDisplay("TT.RD.TEST1012", el, () => {
            console.log("math formula loaded");
    });
    }
    };
