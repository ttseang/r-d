import { CellType, TCellVo } from "./TCellVo";


//gridData TCellVo[][]
export class TGridVo {
    public gridData: TCellVo[][];
    public rowCount: number;
    public colCount: number;
    public name: string = "step name";

    constructor(gridData: TCellVo[][]) {
        //gridData is a 2d array of TCellVo
        //each row is an array of TCellVo
        //columns are in the first dimension
        //rows are in the second dimension
        this.gridData = gridData;
        if (gridData.length===0)
        {
            this.rowCount=0;
            this.colCount=0;
        }
        else
        {
            this.rowCount = gridData[0].length;
            this.colCount = gridData.length;
        }
    }
    public getColumn(col:number)
    {
        let column:TCellVo[]=[];
        for (let i:number=0;i<this.rowCount;i++)
        {
            column.push(this.getCellAt(col,i));
        }
        return column;
    }
    public getRow(index:number)
    {
        let row:TCellVo[]=[];
        for (let i:number=0;i<this.colCount;i++)
        {
            row.push(this.getCellAt(i,index));
        }
        return row;
    }
    public getColumnText(index:number)
    {
        let column:TCellVo[]=this.getColumn(index);
        let text:string="";
        for (let i:number=0;i<column.length;i++)
        {
            text+=column[i].text;
        }
        return text;
    }
    public getRowText(index:number)
    {
        let row:TCellVo[]=this.getRow(index);
        let text:string="";
        for (let i:number=0;i<row.length;i++)
        {
            text+=row[i].text;
        }
        return text;
    }
   
    public checkIsRowEmpty(row:number):boolean
    {
        for (let i:number=0;i<this.colCount;i++)
        {
            let cell:TCellVo=this.getCellAt(i,row);
            if (!cell.isEmpty)
            {
                return false;
            }
        }
        return true;
    }
    public checkIsColumnEmpty(col:number):boolean
    {
    
        for (let i:number=0;i<this.rowCount;i++)
        {
            let cell:TCellVo=this.getCellAt(col,i);

            if (!cell.isEmpty)
            {
                return false;
            }
        }
        return true;
    }
    public findTopRow():number
    {
        //find the first row that is not empty
        for (let i:number=0;i<this.rowCount;i++)
        {
            if (!this.checkIsRowEmpty(i))
            {
                return i;
            }
        }
        return -1;
    }
    public findBottomRow():number
    {
        //find the first row that is not empty
        for (let i:number=this.rowCount-1;i>=0;i--)
        {
            if (!this.checkIsRowEmpty(i))
            {
                return i;
            }
        }
        return -1;
    }
    public findLeftColumn():number
    {
        //find the first row that is not empty
        for (let i:number=0;i<this.colCount;i++)
        {
            if (!this.checkIsColumnEmpty(i))
            {
                return i;
            }
        }
        return -1;
    }
    public findRightColumn():number
    {
        //find the first row that is not empty
        for (let i:number=this.colCount-1;i>=0;i--)
        {
            if (!this.checkIsColumnEmpty(i))
            {
                return i;
            }
        }
        return -1;
    }
    public getCellAt(col:number,row:number)
    {
        return this.gridData[col][row];
    }
    public getValueAt(col:number,row:number)
    {
        return this.gridData[col][row].text;
    }
    public loadFromData(data:any)
    {
        this.colCount=data.length;
        this.rowCount=data[0].length;
        let gridData:TCellVo[][]=[];
        for (let i:number=0;i<data.length;i++)
        {
            gridData[i]=[];
            for (let j:number=0;j<data[i].length;j++)
            {
                let cellData:any=data[i][j];
                let text:string[]=cellData.text;
                let cellClasses:string[]=cellData.cellClasses;
                let textClasses:string[][]=cellData.textClasses;
                let rowSize:number=cellData.rowSize;
                let colSize:number=cellData.colSize;
                let type:string=cellData.type;
                let adjustX:number[]=cellData.adjustX;
                let adjustY:number[]=cellData.adjustY;
                let cellType:CellType=TCellVo.stringToConstants(type);
                let cell:TCellVo=new TCellVo(text,colSize,rowSize,textClasses,cellClasses,cellType);
                cell.adjustX=adjustX;
                cell.adjustY=adjustY;
               
                gridData[i][j]=cell;
            }
        }
        
        this.gridData=gridData;
        (window as any).tGridVo=this;
       // this.removeEmptyColumns();
      //  this.removeEmptyRows();
       
    }
   
}