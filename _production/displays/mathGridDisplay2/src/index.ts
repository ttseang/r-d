import { MathGrid } from "./MathGrid";
import { MathGridLoader } from "./MathGridLoader";
import { TCellVo } from "./dataObjs/TCellVo";
import { TGridVo } from "./dataObjs/TGridVo";


/*
window.onload = function () {
    //     //TT.RD.SAMPLES
    //     //TT.RD.SIMPLE

   // let lti = "Rachel.Addition.8.3.23";
   let lti = "Rachel.Multiplication.8.3.23";
   // let lti = "Rachel.Division.8.4.23";

    MathGrid.injectCSS();
    let mathGrid: MathGrid = new MathGrid(lti, document.getElementsByClassName("mathGrid")[0] as HTMLDivElement,0,()=>{},true);

    (window as any).mathGrid = mathGrid;
    document.addEventListener("keydown", (e) => {
        if (e.key.toLowerCase() === "arrowleft") {
            mathGrid.showStep(mathGrid.stepIndex - 1);
        }
        else {
            mathGrid.showStep(mathGrid.stepIndex + 1);
        }
    })
}
*/


export { MathGrid, MathGridLoader, TCellVo, TGridVo };