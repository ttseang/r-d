import { CacheManager } from "@teachingtextbooks/ttcachemanager";
import { MathGridLoader } from "./MathGridLoader";
import { TCellVo } from "./dataObjs/TCellVo";
import { TGridVo } from "./dataObjs/TGridVo";


const cm = CacheManager.getInstance();
const cachePrefix = "mathGrid-";

export class MathGrid {
    static editorURL = "https://research.teachingtextbooksapp.com/w/mathGridAdmin/";
    static endPointURL = "https://tthq.me/api/pr/getequation/";

    public lti: string;
    private el: HTMLElement;
    private onReady: Function | null;
    private steps: TGridVo[] = [];
    public stepIndex: number = 0;
    private useTrim: boolean;

    private top: number = 100;
    private bottom: number = 0;
    private left: number = 100;
    private right: number = 0;

    constructor(lti: string, el: HTMLElement, stepIndex: number = -1, onReady: Function | null = null, useTrim: boolean = true) {
        (window as any).mathGrid = this;

        this.lti = lti;
        this.el = el;
        this.stepIndex = stepIndex;
        this.onReady = onReady;
        this.useTrim = useTrim;

        if (this.useTrim===false)
        {
            this.top=0;
            this.bottom=100;
            this.left=0;
            this.right=100;
        }

        if (this.lti) {
            const cachedData: string = cm.getFromCache(`${cachePrefix}${this.lti}`, "");
            // console.log("cache", `${cachePrefix}${this.lti}`, cachedData.length);
            if (cachedData) {
                this.onFileContentLoaded(JSON.parse(cachedData));
            }
            else {
                this.loadContent();
            }
        }
    }

    static removeFromCache(lti: string) {
        // console.log("removing...", lti);
        cm.removeFromCache(`${cachePrefix}${lti}`);
    }

    loadContent() {
        let mathGridLoader: MathGridLoader = new MathGridLoader();
        mathGridLoader.getFileContent(this.lti, (data: any) => {
            // console.log("success", JSON.parse(data.data).gridData);
            cm.saveToCache(`${cachePrefix}${this.lti}`, JSON.stringify(data), 60000);
            this.onFileContentLoaded(data);
        });
    }

    onFileContentLoaded(data: any) {
        // console.log(data.data);
        let loadString: string = data.data;
        let loadObj: any = JSON.parse(loadString);
        let steps: TGridVo[] = [];
        let stepCount: number = loadObj.length;


        for (let i: number = 0; i < stepCount; i++) {
            let stepData: any = loadObj[i].gridData;
            let tGrid: TGridVo = new TGridVo([]);
            tGrid.loadFromData(stepData);
            
            if (this.useTrim) {
            let top2: number = tGrid.findTopRow();
            let bottom2: number = tGrid.findBottomRow();
            let left2: number = tGrid.findLeftColumn();
            let right2: number = tGrid.findRightColumn();

            if (top2 < this.top) {
                this.top = top2;
            }
            if (bottom2 > this.bottom) {
                this.bottom = bottom2;
            }
            if (left2 >= 0) {
                this.left = Math.min(this.left, left2);
            }
            if (right2 >= 0) {
                this.right = Math.max(this.right, right2);
            }
            }
            steps.push(tGrid);
        }

        // console.log("top", this.top);
        // console.log("bottom", this.bottom);

        this.steps = steps;
        this.showStep(this.stepIndex);

        this.onReady && this.onReady();
    }

    showStep(stepIndex: number) {
        this.stepIndex = stepIndex >= 0 ? stepIndex : this.stepIndex;
        // console.log("to HTML", this.getGridHtml());
        if (this.el) {
            this.el.innerHTML = this.getGridHtml();
        }
        else {
            console.log("no el");
        }
    }

    getTestCell() {
        let grid: TGridVo = this.steps[this.stepIndex];
        let firstCell: TCellVo = grid.gridData[0][0];
        return firstCell;
    }

    getGridHtml() {
        let grid: TGridVo = this.steps[this.stepIndex];
        if (!grid) {
            return "";
        }

        (window as any).grid = grid;

        let gridData = grid.gridData;
        let colCount = gridData.length;

        let rowCount = 0;
        if (colCount > 0) {
            rowCount = gridData[0].length;
        }

        let rowHtml: string[] = [];
        let gridHtml: string[] = [];

        for (let col: number = 0; col < colCount; col++) {
            if (col > this.left-1 && col < this.right + 1) {
                for (let row: number = 0; row < rowCount; row++) {
                    if (row > this.top-1 && row < this.bottom + 1) {
                        let cell: TCellVo = grid.getCellAt(col, row);
                        rowHtml.push(cell.getHtml());
                    }                   
                }
                let rowString: string = rowHtml.join("");
                rowString = "<div>" + rowString + "</div>";
                gridHtml.push(rowString);
                rowHtml = [];
            }
          
        }
        return gridHtml.join("");

    }

    public static injectCSS() {
        const cssId = "mathGridStyles";
        if (document.querySelector(`#${cssId}`)) {
            return;
        }

        let cssString = `.mathGrid {
            --defaultSize: 10rem;
            --cellWidth: calc(var(--defaultSize) / 2.5);
            --cellHeight: calc(var(--defaultSize) / 2.4); /* not 2.5, 2.4 gives a little more space */
            --halfCellWidth: calc(var(--cellWidth) / 2);
            --halfCellHeight: calc(var(--cellHeight) / 2);
        
            display: flex;
            font-size: var(--defaultSize);
        }
        
        .mathGrid .cell {
            width: var(--cellWidth);
            height: var(--cellHeight);
            position: relative;
        
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
        }
        
        .mathGrid .bottomline::after,
        .mathGrid .topline::after {
            content: "";
            position: absolute;
            width: 110%;
            height: calc(var(--cellHeight) / 8);
            border-radius: calc(var(--cellHeight) / 8);
            background: #000;
            top: var(--cellHeight);
            left: -5%;
        }
        .mathGrid .topline::after {
            top: 0;
        }
        
        .mathGrid .rightline::after,
        .mathGrid .leftline::after {
            content: "";
            position: absolute;
            width: calc(var(--cellWidth) / 8);
            height: 110%;
            border-radius: calc(var(--cellWidth) / 8);
            background: #000;
            left: calc(var(--cellWidth) - var(--cellWidth) / 16);
        }
        .mathGrid .leftline::after {
            left: calc(var(--cellWidth) / -16);
        }
        
        .mathGrid .halfDown .cell_1_1_0 {
            position: relative;
            top: 50%
        }
        
        .mathGrid .cell_1_1 {
            display: flex;
            flex-wrap: nowrap;
            align-content: flex-start;
            justify-content: center;
            flex-direction: column;
            font-size: calc(var(--defaultSize) / 2);
            width: var(--cellWidth);
            height: var(--cellHeight);
            position: relative;
            align-items: center
        }
        
        .mathGrid .whole.cell_1_1 {
            font-size: calc(var(--defaultSize) / 3)
        }
        
        .mathGrid .operator.cell_1_1 {
            font-size: calc(var(--defaultSize) / 3)
        }
        
        /* math */
        .mathGrid .underlineRow {
            border-bottom: 2px solid black !important
        }
        
        .mathGrid .smallText {
            font-size: .8em
        }
        
        .mathGrid .largeText {
            font-size: 1.2em
        }
        
        .mathGrid .redText {
            color: red
        }
        
        .mathGrid .greenText {
            color: green
        }
        
        .mathGrid .blueText {
            color: blue
        }
        
        .mathGrid .yellowText {
            color: yellow
        }
        
        .mathGrid .purpleText {
            color: purple
        }
        
        .mathGrid .orangeText {
            color: orange
        }
        
        .mathGrid .pinkText {
            color: pink
        }
        
        .mathGrid .brownText {
            color: brown
        }
        
        .mathGrid .blackText {
            color: black
        }
        
        .mathGrid .whiteText {
            color: white
        }
        
        .mathGrid .grayText {
            color: gray
        }
        
        .mathGrid .lightBlueText {
            color: lightblue
        }
        
        .mathGrid .lightGreenText {
            color: lightgreen
        }
        
        .mathGrid .boldText {
            font-weight: bold
        }
        
        .mathGrid .italicText {
            font-style: italic
        }
        
        .mathGrid .tableBorder {
            border: solid;
            padding: 18px;
            width: 150px;
            height: fit-content;
            display: flex;
            justify-content: center;
            align-items: flex-start
        }
        
        .mathGrid .strikethrough {
            position: relative;
            font-size: .8rem
        }
        
        .mathGrid .strikethrough:before {
            content: "";
            position: absolute;
            background: #000;
            top: 20%;
            left: 40%;
            width: calc(var(--cellWidth) / 8);
            height: 70%;
            border-radius: calc(var(--cellWidth) / 8);
            transform: rotate(30deg);
        }
        
        .mathGrid .halfCircle::after {
            content: "";
            position: absolute;
            top: 0;
            left: 75%;
            width: 50%;
            height: 100%;
            display: inline-block;
            background-image: url('data:image/svg+xml,<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 19 69" style="enable-background:new 0 0 19 69;" xml:space="preserve"><path d="M11.8,8.1c14.8,24.5,1,51.6-4.6,59.7c-1.6,2.5-6.3,0.3-4.2-3C13.4,48.2,16.9,22.5,2.6,6.4C-0.5,3.3,2.9-0.1,5.1,0H19v8.1H11.8z"/></svg>');
            background-size: contain;
            background-repeat: no-repeat;
        }
        
        .mathGrid .division-line {
            border-top: 2px solid black;
            width: 50px;
            margin: 0 10px
        }
        
        /* end math */
        
        /* cells */
        
        .mathGrid .sqrt .cell_1_1_0 {
            position: relative;
            left: calc(var(--cellWidth) / 3.3);
            top: 0;
            font-size: calc(var(--defaultSize) / 3)
        }
        
        .mathGrid .cell_1_2 {
            display: flex;
            flex-wrap: nowrap;
            align-content: flex-start;
            justify-content: center;
            flex-direction: column;
            font-size: calc(var(--defaultSize) / 3);
            width: var(--cellWidth);
            height: var(--cellHeight);
            align-items: center
        }
        
        .mathGrid .cell_1_2_0 {
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .cell_1_2_1 {
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .cell_2_1 {
            display: flex;
            flex-wrap: nowrap;
            align-content: flex-start;
            justify-content: center;
            flex-direction: row;
            font-size: calc(var(--defaultSize) / 3);
            width: var(--cellWidth);
            height: var(--cellHeight)
        }
        
        .mathGrid .cell_2_1_0 {
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .power .cell_2_1_0 {
            font-size: calc(var(--defaultSize) / 6);
            position: relative;
            top: calc(var(--cellHeight) / 3)
        }
        
        .mathGrid .power .cell_2_1_1 {
            font-size: calc(var(--defaultSize) / 6);
            position: relative;
            top: calc(var(--cellHeight) / 3)
        }
        
        .mathGrid .power2x2 {
            display: flex;
            flex-wrap: nowrap;
            align-content: flex-start;
            justify-content: center;
            flex-direction: row;
            width: var(--cellWidth);
            height: var(--cellHeight)
        }
        
        .mathGrid .power2x2 .cell_2_2_0 {
            position: relative;
            top: calc(var(--cellHeight) / 3);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .power2x2 .cell_2_2_1 {
            position: relative;
            top: calc(var(--cellHeight) / 3);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .power2x2 .cell_2_2_2 {
            font-size: calc(var(--defaultSize) / 8)
        }
        
        .mathGrid .power2x2 .cell_2_2_3 {
            font-size: calc(var(--defaultSize) / 8)
        }
        
        .mathGrid .power3x3 {
            display: flex;
            flex-wrap: nowrap;
            align-content: flex-start;
            justify-content: center;
            flex-direction: row;
            width: var(--cellWidth);
            height: var(--cellHeight)
        }
        
        .mathGrid .power3x3 .cell_3_3_0 {
            position: relative;
            top: calc(var(--cellHeight) / 3);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .power3x3 .cell_3_3_1 {
            position: relative;
            top: calc(var(--cellHeight) / 3);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .power3x3 .cell_3_3_2 {
            position: relative;
            top: calc(var(--cellHeight) / 3);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .power3x3 .cell_3_3_3 {
            font-size: calc(var(--defaultSize) / 8)
        }
        
        .mathGrid .power3x3 .cell_3_3_4 {
            font-size: calc(var(--defaultSize) / 8)
        }
        
        .mathGrid .power3x3 .cell_3_3_5 {
            font-size: calc(var(--defaultSize) / 8)
        }
        
        .mathGrid .cell_1_3 {
            display: flex;
            flex-wrap: nowrap;
            align-content: flex-start;
            justify-content: center;
            flex-direction: row;
            font-size: calc(var(--defaultSize) /4);
            width: var(--cellWidth);
            height: var(--cellHeight)
        }
        
        .mathGrid .power .cell_2_1_1 {
            font-size: calc(var(--defaultSize) / 6);
            position: relative;
            top: 0
        }
        
        .mathGrid .divide .cell_1_1_0 {
            display: inline;
            border-right: 2px black solid;
            border-radius: 0 0 16px 0;
            text-align: right;
            padding-right: 5px;
            font-size: calc(var(--defaultSize) /4)
        }
        
        .mathGrid .divide2Col {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            height: var(--cellHeight)
        }
        
        .mathGrid .divide2Col .cell_2_1_0 {
            display: inline;
            text-align: right;
            font-size: calc(var(--defaultSize) /4)
        }
        
        .mathGrid .divide2Col .cell_2_1_1 {
            display: inline;
            border-right: 2px black solid;
            border-radius: 0 0 16px 0;
            text-align: right;
            padding-right: 5px;
            font-size: calc(var(--defaultSize) /4)
        }
        
        .mathGrid .decimal {
            position: relative;
        }
        .mathGrid .decimal .cell_2_1_0 {
            position: absolute !important;
            top: calc(var(--cellHeight) * -0.15);
            left: inherit !important;
        }
        .mathGrid .decimal .cell_2_1_1 {
            position: absolute !important;
            top: calc(var(--cellHeight) * -0.15);
            left: var(--cellWidth) !important;
            transform: translateX(-50%) !important;
        }
        
        .mathGrid .carry {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            height: var(--cellHeight)
        }
        
        .mathGrid .carry .cell_1_1_0 {
            display: inline;
            position: relative;
            top: calc(var(--cellHeight) / 4);
            left: 0;
            font-size: calc(var(--defaultSize) /6)
        }
        
        .mathGrid .borrow, .mathGrid .borrow-top, .mathGrid .borrow-side {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            height: var(--cellHeight)
        }
        
        /* end cells */
        
        /* columns */
        
        .mathGrid .twoCol {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            flex-direction: row;
        }
        
        .mathGrid .twoCol .cell_2_1_0 {
            display: inline;
            text-align: right;
            font-size: calc(var(--defaultSize) /4)
        }
        
        .mathGrid .twoCol .cell_2_1_1 {
            display: inline;
            text-align: left;
            font-size: calc(var(--defaultSize) /4)
        }
        
        .mathGrid .threeCol {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            height: var(--cellHeight)
        }
        
        .mathGrid .threeCol .cell_3_1_0 {
            display: inline;
            text-align: right;
            font-size: calc(var(--defaultSize) /5)
        }
        
        .mathGrid .threeCol .cell_3_1_1 {
            display: inline;
            text-align: center;
            font-size: calc(var(--defaultSize) /5)
        }
        
        .mathGrid .threeCol .cell_3_1_2 {
            display: inline;
            text-align: left;
            font-size: calc(var(--defaultSize) /5)
        }
        
        .mathGrid .fourCol {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            height: var(--cellHeight)
        }
        
        .mathGrid .fourCol .cell_1_4_0 {
            display: inline;
            text-align: right;
            font-size: calc(var(--defaultSize) /6)
        }
        
        .mathGrid .fourCol .cell_1_4_1 {
            display: inline;
            text-align: center;
            font-size: calc(var(--defaultSize) /6)
        }
        
        .mathGrid .fourCol .cell_1_4_2 {
            display: inline;
            text-align: center;
            font-size: calc(var(--defaultSize) /6)
        }
        
        .mathGrid .fourCol .cell_1_4_3 {
            display: inline;
            text-align: left;
            font-size: calc(var(--defaultSize) /6)
        }
        
        .mathGrid .twoRows {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            flex-direction: column;
            font-size: calc(var(--defaultSize) /6)
        }
        
        .mathGrid .threeRows {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            flex-direction: column;
            font-size: calc(var(--defaultSize) /9)
        }
        
        .mathGrid .fourRows {
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            flex-direction: column;
            font-size: calc(var(--defaultSize) /6)
        }
        
        /* end columns */
        
        /* fractions */
        
        .mathGrid .fraction {
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .fraction .cell_1_2_0 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction2x2 {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .fraction2x2 .cell_2_2_0 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction2x2 .cell_2_2_1 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction3x3 {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .fraction3x3 .cell_3_3_0 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction3x3 .cell_3_3_1 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction3x3 .cell_3_3_2 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction4x4 {
            display: grid;
            grid-template-columns: repeat(4, 1fr);
            font-size: calc(var(--defaultSize) / 6)
        }
        
        .mathGrid .fraction4x4 .cell_4_4_0 {
            border-bottom: solid 2px black
        }
        
        .fraction4x4 .cell_4_4_1 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction4x4 .cell_4_4_2 {
            border-bottom: solid 2px black
        }
        
        .mathGrid .fraction4x4 .cell_4_4_3 {
            border-bottom: solid 2px black
        }
        
        /* end fractions */
        
        /* font sizes */
        
        .mathGrid .fs1 {
            font-size: calc(var(--defaultSize) /10) !important
        }
        
        .mathGrid .fs2 {
            font-size: calc(var(--defaultSize) /9) !important
        }
        
        .mathGrid .fs3 {
            font-size: calc(var(--defaultSize) /8) !important
        }
        
        .mathGrid .fs4 {
            font-size: calc(var(--defaultSize) /7) !important
        }
        
        .mathGrid .fs5 {
            font-size: calc(var(--defaultSize) /6) !important
        }
        
        .mathGrid .fs6 {
            font-size: calc(var(--defaultSize) /5) !important
        }
        
        .mathGrid .fs7 {
            font-size: calc(var(--defaultSize) /4) !important
        }
        
        .mathGrid .fs8 {
            font-size: calc(var(--defaultSize) /3) !important
        }
        
        .mathGrid .fs9 {
            font-size: calc(var(--defaultSize) /2) !important
        }
        
        .mathGrid .fs10 {
            font-size: calc(var(--defaultSize))
        }
        
        /* end front sizes */`;

        const style: HTMLStyleElement = document.createElement("style");
        style.id = cssId;
        style.innerHTML = cssString;
        document.head.appendChild(style);
    }
}