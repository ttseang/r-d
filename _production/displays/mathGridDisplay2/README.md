# @teachingtextbooks/mathgriddisplay

This is a library for displaying math grids that are created by the
MathGrid App.  It is used to display the grids in various other 
Teaching Textbooks applications and lessons.

## Installation

npm i @teachingtextbooks/mathgriddisplay

## styling

The code has default styling that can be used.  If you want to use your own styling, have a look at
the mathgrid.css file in the dist folder to see what classes are used.

https://www.npmjs.com/package/@teachingtextbooks/mathgriddisplay?activeTab=code

## Usage - Typescript


```import { MathGrid } from "@teachingtextbooks/mathgriddisplay/dist/MathGrid";

window.onload = function () {
    //teachtextbooks mathgrid

    //inject the standard mathgrid css into the page
    //only use this if you are not providing your own mathgrid css
    MathGrid.injectCSS();

    //get the element that will contain the mathgrid
    //it can be any element that you can get a reference to
    
    let el: HTMLElement = document.getElementsByClassName("mathGrid")[0] as HTMLElement;
    if (el) {
        const lti: string = "TT.RD.SIMPLE";
        //OTHER LTI OPTIONS FOR TESTING
        //const lti:string = "TT.RD.SAMPLES"; 

        //pass in the lti, the element, and a callback function, trim is optional

        //trim will remove the extra space around the mathgrid
        const useTrim:boolean = true;

        const md: MathGrid = new MathGrid(lti, el, () => {
            //this callback is called when the mathgrid is ready to be used
            //you can use this to show a specific step
            // md.showStep(0);
        },useTrim);
    }
};
```