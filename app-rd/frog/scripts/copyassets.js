const fse = require('fs-extra');

const srcDir = './public/assets';
const destDir = './android/app/src/main/assets/public/assets';
                              
// To copy a folder or file  
fse.copySync(srcDir, destDir,{ overwrite: true }, function (err) {
  if (err) {                 
    console.error(err);       // add if you want to replace existing folder or file with same name
  } else {
    console.log("success!");
  }
});