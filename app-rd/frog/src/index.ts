import * as Phaser from "phaser";
import "./style.css";
import { GM } from "./classes/GM";
//import 'phaser/plugins/spine/dist/SpinePlugin';
//import "phaser/plugins/spine/src/gameobject/SpineGameObject";
import { SceneMain } from "./scenes/SceneMain";
import { SceneLoad } from "./scenes/SceneLoad";

// Constants
const cspMeta = document.createElement("meta");
const gameDiv: HTMLElement = document.createElement("div");
gameDiv.id = "phaser-game";

// Document Head
cspMeta.httpEquiv = "Content-Security-Policy";
cspMeta.content = "script-src 'self' 'unsafe-inline';";
document.head.appendChild(cspMeta);

// Document Body
document.body.appendChild(gameDiv);


let gm: GM = GM.getInstance();
let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad = navigator.userAgent.indexOf("iPad");

if (isTablet != -1 || isIpad != -1) {
    gm.isTablet = true;
    isMobile = 1;
}
if (isMobile != -1) {
    gm.isMobile = true;
} 
let w = window.innerWidth;
let h = window.innerHeight;
//
//

if (w < h) {
    gm.isPort = true;
}
const config: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    backgroundColor: 'cccccc',
    parent: 'phaser-game',
    scene: [SceneLoad,SceneMain]
};
/* ,
    plugins: {
        scene: [
            { key: 'SpinePlugin', plugin: window.SpinePlugin, mapping: 'spine' }
        ]
    } */
new Phaser.Game(config);