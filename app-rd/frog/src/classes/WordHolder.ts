import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";

export class WordHolder extends Phaser.GameObjects.Container
{
    public scene:Phaser.Scene;
    private id:number=0;
    private image:Phaser.GameObjects.Image;
    private textObj:Phaser.GameObjects.Text;
    public callback:Function=()=>{};
    public index:number=0;

    constructor(id:number,word:string,bscene:IBaseScene)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();
        this.id=id;
        
        let back:Phaser.GameObjects.Image=this.scene.add.image(0,0,"holder");
        Align.scaleToGameW(back,0.2,bscene);
        back.setTint(0xffffff);

        this.image=this.scene.add.image(0,0,"listimage"+id.toString());
        Align.scaleToGameW(this.image,0.15,bscene);

        this.textObj=this.scene.add.text(0,0,word,{color:"#000000",backgroundColor:"#ffffff"}).setOrigin(0.5,0.5);
        this.textObj.y=this.image.displayHeight/2;


        let sb=this.scene.add.image(0,0,"sb");
        sb.y=back.displayHeight;
        sb.setInteractive();
        sb.on('pointerdown',this.playSound.bind(this));

        this.add(back);
        this.add(this.image);
        this.add(this.textObj);
        this.add(sb);

        this.setSize(back.displayWidth,back.displayHeight);
        this.scene.add.existing(this);
        this.image.setInteractive();
        this.image.on('pointerdown',()=>{this.callback(this.index)});
       // this.on('pointerdown',this.onClick.bind(this));
    }
    playSound()
    {
        let sound:Phaser.Sound.BaseSound=this.scene.sound.add("listaudio"+this.id.toString());
        sound.play();
    }
}