import { BrowserWindow } from "electron";
import { GameObjects } from "phaser";
import { Button } from "../classes/comps/Button";
import { FallingObj } from "../classes/FallingObj";
import { ResizeManager } from "../classes/resizeManager";
import { SpineAnim2 } from "../classes/SpinAnim2";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { PosVo } from "../dataObjs/PosVo";
//import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { BaseScene } from "./BaseScene";
//import { SpineAnim2 } from "../classes/SpinAnim2";
//import { GameObjects } from "phaser";
//import { FallingObj } from "../classes/FallingObj";


/* import face from "../../public/assets/face.png";
import button from "../../public/assets/ui/buttons/1/1.png";
import starfish from "../../public/assets/starfish.png";
//import seahorse from "../../public/assets/seahorse.png";

import coralAtlas from "../../public/assets/coral reef.atlas";
import cr1 from "../../public/assets/coral reef.png";
import cr2 from "../../public/assets/coral reef2.png";
import cr3 from "../../public/assets/coral reef3.png"; */

export class SceneStart extends BaseScene implements IBaseScene {
    private button1: Button;
    private resizeManager: ResizeManager;
    private coral: SpineAnim2;
    
    constructor() {
        super("SceneStart");
    }
    preload() {
       // this.load.setPath("../public/assets/");

       // let imageMap:Map<string,NodeRequire>=new Map();

        /* let imageArray:string[]=['seahorse','starfish'];
        for (let i:number=0;i<imageArray.length;i++)
        {
            require("../../public/assets/"+imageArray[i]+".png");
            //imageMap.set(imageArray[i],req);
        } */

       // const coralJson=require("../../public/assets/coral reef.json");

      //  let req2:any[]=[coralJson,coralAtlas,cr1,cr2,cr3];

        this.load.image("button", "assets/ui/buttons/1/1.png");
        this.load.image("face","assets/face.png");

        this.load.spine("coral", "/assets/coral reef.json", ['/assets/coral reef.atlas'], false);
        this.load.image("starfish", "assets/starfish.png");
        this.load.image("seahorse", "assets/seahorse.png");

    }
    
    create() {
        super.create();
        this.makeGrid(22, 22);
     //   this.grid.showPos();

        console.log("scene start");        

        this.resizeManager = ResizeManager.getInstance(this);

      //  window['scene'] = this;

        this.makeChildren();
        this.definePos();
        this.placeItems();
    }
    makeChildren() {
       
      
       this.coral = new SpineAnim2(this, "coral","coral");
       this.coral.cover=true;
       this.coral.setReal(1666.79, 1048.97);
      this.coral.setOffSet(785, -1040);
      this.coral.play("animation",true);

      this.button1 = new Button(this, "button", "Start");
      this.button1.setCallback(()=>{
          this.makeChildWindow();
      })
    }
    makeChildWindow()
    {
       /*  const path = require("path");

        let win2:BrowserWindow=new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
              preload: path.join(__dirname, "preload.js"),
            },
          }); */
    }
    definePos() {
        let keys:string[]=[];
        //
        //
        //
        let buttonLayout: LayoutVo = this.resizeManager.addItem("button1", this.button1);
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", 10, 10, 0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_LANDSCAPE,10,19,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_MOBILE,ResizeVo.ORIENTATION_PORTRAIT,10,3,0.2));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_PORTRAIT,19,19,0.4));
        buttonLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_TABLET,ResizeVo.ORIENTATION_LANDSCAPE,2,2,.1));

        keys.push("button1");

        let deck: PosVo[] = this.makeDeck();

        for (let i: number = 0; i < 10; i++) {
            let face: GameObjects.Sprite = this.add.sprite(0, 0, "starfish");
            let faceLayout: LayoutVo = this.resizeManager.addItem("starfish" + i, face);
            keys.push("starfish" + i);
            let scale: number = Math.floor((Math.random() * 50)) / 500;
            // console.log(scale);
            faceLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", deck[i].x, deck[i].y, scale));
        }

        for (let i: number = 0; i < 5; i++) {
            let fallingObj: FallingObj = new FallingObj(this,"seahorse");
            let fallLayout: LayoutVo = this.resizeManager.addItem("falling" + i.toString(), fallingObj);
            fallLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP, "", i * 4, 0, 0.05));
            this.grid.placeAt(i * 4, 0, fallingObj);
            fallingObj.pos = fallLayout.getDefinition(ResizeVo.DEVICE_DESKTOP, "");

            keys.push("falling" + i.toString());
        }

        let coralLayout:LayoutVo=this.resizeManager.addItem("coral",this.coral);
        coralLayout.setDefintion(new ResizeVo(ResizeVo.DEVICE_DESKTOP,"",-0.5,-0.5,1));
        keys.push("coral");

        this.resizeManager.setKeys(keys);

        //this.resizeManager.setKeys(['button1','face']);

    }
    placeItems() {
        this.resizeManager.placeChildren();
    }
    makeDeck() {
        let deck: PosVo[] = [];

        for (let i: number = 0; i < 22; i++) {
            for (let j: number = 0; j < 22; j++) {
                deck.push(new PosVo(i, j));
            }
        }
        return this.shuffleArray(deck);
    }
    shuffleArray(deck: PosVo[]) {
        for (let i: number = 0; i < 500; i++) {
            let t1: number = Math.floor(Math.random() * deck.length);
            let t2: number = Math.floor(Math.random() * deck.length);

            let temp: PosVo = deck[t1];
            deck[t1] = deck[t2];
            deck[t2] = temp;

        }
        return deck;
    }
}