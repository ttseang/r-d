import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene {
    constructor() {
        super("SceneMain");
    }
    preload() {
        this.load.image("face", "./assets/face.png");
    }
    create() {
        super.create();
        this.makeGrid(11,11);

        console.log("create!");
        let face:Phaser.GameObjects.Image=this.add.image(100, 200, "face");

        Align.scaleToGameW(face,0.25,this);

        this.grid.showNumbers();
        this.grid.placeAtIndex(60,face);
    }
}