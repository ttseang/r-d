import { IGameObj } from "../interfaces/IGameObj";
import { ResizeVo } from "./ResizeVo";

export class LayoutVo
{
    private resizeDef:Map<string,ResizeVo>=new Map<string,ResizeVo>();
    public defaultResize:ResizeVo | null=null;

    public obj:IGameObj;

    constructor(obj:IGameObj)
    {
        this.obj=obj;
    }
    public setDefintion(resizeVo:ResizeVo)
    {
        let key:string=resizeVo.device+"_"+resizeVo.orientation;
        this.resizeDef.set(key,resizeVo);

        if (this.defaultResize===null)
        {
            this.defaultResize=resizeVo;
        }
    }
    public getDefinition(device:string,orientation:string)
    {
        let key:string=device+"_"+orientation;
        if (this.resizeDef.has(key))
        {
            return this.resizeDef.get(key);
        }
        return this.defaultResize;
    }
    public setDefault(resizeVo:ResizeVo)
    {
        this.defaultResize=resizeVo;
    }
}