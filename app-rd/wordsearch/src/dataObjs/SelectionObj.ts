import { LetterBox } from "../classes/LetterBox";
import { LineVo } from "./LineVo";

export class SelectionObj
{
    public startX:number=0;
    public startY:number=0;

    public screenX:number=0;
    public screenY:number=0;

    public endX:number=0;
    public endY:number=0;

    public selectArray:LetterBox[]=[];
    public lines:LineVo[]=[];

    constructor(startX:number,startY:number)
    {
        this.startX=startX;
        this.startY=startY;
    }
    get diffX()
    {
        return Math.abs(this.startX-this.endX);
    }
    get diffY()
    {
        return Math.abs(this.startY-this.endY);
    }
    
    public addLine()
    {
        this.lines.push(new LineVo(this.startX,this.startY,this.endX,this.endY));
    }
}