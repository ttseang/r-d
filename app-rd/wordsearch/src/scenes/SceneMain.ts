import { GameObjects } from "phaser";
import { LetterBox } from "../classes/LetterBox";
import { WordList } from "../classes/WordList";
import { BoardObj } from "../dataObjs/BoardObj";
import { FlipVo } from "../dataObjs/flipVo";
import { LineVo } from "../dataObjs/LineVo";
import { SelectionObj } from "../dataObjs/SelectionObj";
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneMain extends BaseScene implements IBaseScene {
    private board: BoardObj;
    private wordList: WordList;
    private flipVo: FlipVo;
    private selectionObj: SelectionObj = new SelectionObj(0, 0);
    private dragGraphics: GameObjects.Graphics;
    private lineGraphics:GameObjects.Graphics;


    constructor() {
        super("SceneMain");
    }
    preload() {
        
    }
    create() {
        super.create();
        this.makeGrid(22, 22);
        //this.grid.show();
        let initOr: string = this.scale.orientation.toString();
        //console.log(initOr);
        
        

        this.flipVo = new FlipVo(this.gw, this.gh, initOr);

        ////console.log(this.flipVo);

        fetch('assets/words/level1.json')
            .then(response => response.json())
            .then(data => this.setUp(data));
    }
    setUp(data: any) {


        let wordArray: string[] = data.level.words.split(",");
        let boardArray: string[] = data.level.board.split("|");

        let rows: number = parseInt(data.level.rows);
        let cols: number = parseInt(data.level.cols);
        this.board = new BoardObj(boardArray, wordArray, rows, cols);
        this.getPlaySize();
        //
        //
        //
        this.makeWordList();
        this.makeBoard();

        if (this.scale.orientation === Phaser.Scale.Orientation.PORTRAIT) {
            this.grid.placeAt(1, 0, this.wordList);
        }
        else {
            this.grid.placeAt(1, 1, this.wordList);
        }

        this.dragGraphics = this.add.graphics();
        this.lineGraphics=this.add.graphics();

       // this.scale.on('orientationchange', this.onFlipped.bind(this));
        window.addEventListener("orientationchange",this.onFlipped.bind(this), false);
        this.input.on('gameobjectdown', this.selectStart.bind(this));
    }
    selectStart(pointer: Phaser.Input.Pointer, obj: LetterBox) {
        //console.log('down');
        if (obj.name.substr(0, 3) != "let") {
            return;
        }
        //console.log(obj.name);



        let realObj: LetterBox = this.getRealObj(obj);
        //  realObj.alpha=.5;
        //console.log(realObj.col,realObj.row);
        
        this.selectionObj.startX = realObj.col;
        this.selectionObj.startY = realObj.row;
        this.selectionObj.screenX = realObj.x;
        this.selectionObj.screenY = realObj.y;

        this.input.once("gameobjectup", this.selectEnd.bind(this));
        this.input.on('pointermove', this.selectMove.bind(this));
    }
    getRealObj(obj:LetterBox) {
        let location: string[] = obj.name.split("_");

        let row: number = parseInt(location[2]);
        let col: number = parseInt(location[1]);

        let realObj: LetterBox = this.board.letterBoxes[col][row];
        return realObj;
    }
    selectMove() {
        this.dragGraphics.clear();
        this.dragGraphics.lineStyle(this.gw/25, 0x2ecc71, 0.8);
        this.dragGraphics.moveTo(this.selectionObj.screenX, this.selectionObj.screenY);
        this.dragGraphics.lineTo(this.input.activePointer.x, this.input.activePointer.y);
        this.dragGraphics.strokePath();
    }
    drawLines()
    {
        this.lineGraphics.clear();
        this.lineGraphics.lineStyle(this.gw/25, 0x9BADB7, 0.7);
        for (let i:number=0;i<this.selectionObj.lines.length;i++)
        {
        let line:LineVo=this.selectionObj.lines[i];
        let startBlock:LetterBox=this.board.letterBoxes[line.startY][line.startX];
        let endBlock:LetterBox=this.board.letterBoxes[line.endY][line.endX];
        if (line.startY==line.endY)
        {
            this.lineGraphics.moveTo(startBlock.x-this.cw/2,startBlock.y);
            this.lineGraphics.lineTo(endBlock.x+this.cw/2,endBlock.y);
        }
        else
        {
            if (line.startX==line.endX)
            {
                this.lineGraphics.moveTo(startBlock.x,startBlock.y-this.ch/2);
                this.lineGraphics.lineTo(endBlock.x,endBlock.y+this.ch);
            }
            else
            {
                this.lineGraphics.moveTo(startBlock.x-this.cw/2,startBlock.y-this.ch/2);
                this.lineGraphics.lineTo(endBlock.x+this.cw/2,endBlock.y+this.ch/2);
            }
        }
        
       
        }
        this.lineGraphics.strokePath();
    }
    selectEnd(pointer: Phaser.Input.Pointer, obj: LetterBox) {
        this.input.off('pointermove');

        this.dragGraphics.clear();

        let realObj: LetterBox = this.getRealObj(obj);
        //  realObj.alpha=.5;
        this.selectionObj.endX = realObj.col;
        this.selectionObj.endY = realObj.row;

        //console.log(this.selectionObj);

        let word: string = "";
        this.selectionObj.selectArray = [];

        //console.log(this.selectionObj.diffX, this.selectionObj.diffY);

        if (this.selectionObj.diffX === 0 && this.selectionObj.diffY !== 0) {
            word = this.getColSlice(this.selectionObj.startX, this.selectionObj.startY, this.selectionObj.endY);
        }
        if (this.selectionObj.diffY === 0 && this.selectionObj.diffX !== 0) {

            word = this.getRowSlice(this.selectionObj.startY, this.selectionObj.startX, this.selectionObj.endX);
        }
        if (this.selectionObj.diffX === this.selectionObj.diffY) {

            word = this.getDiagSlice(this.selectionObj.startX, this.selectionObj.startY, this.selectionObj.endY, this.selectionObj.endX);
        }
        //console.log(word);

        if (this.board.checkWord(word)==true)
        {
            this.wordList.strike(word);
            this.selectionObj.addLine();
            this.drawLines();
        }
    }
    getRowSlice(row: number, start: number, end: number) {
        if (end < start) {
            let temp: number = end;
            end = start;
            start = temp;
        }
        //console.log(row, start, end);

        let word: string = "";
        for (let i: number = start; i < end + 1; i++) {
            //this.board.letterBoxes[row][i].alpha = .5;
            word += this.board.letters[i][row];
            this.selectionObj.selectArray.push(this.board.letterBoxes[i][row]);
        }

        return word;
    }
    getColSlice(col: number, start: number, end: number) {
        if (end < start) {
            let temp: number = end;
            end = start;
            start = temp;
        }
        let word: string = "";
        for (let i: number = start; i < end + 1; i++) {
            word += this.board.letters[col][i];
            this.selectionObj.selectArray.push(this.board.letterBoxes[col][i]);
        }
        return word;
    }
    getDiagSlice(startCol: number, startRow: number, endRow: number, endCol: number) {
       
        let yDir:number=1;
        let xDir:number=1;

        if (endRow < startRow) {
           // let temp: number = endRow;
         //   endRow = startRow;
            //startRow = temp;
            yDir=-1;
        }

        if (endCol < startCol) {
            //let temp2: number = endCol;
          //  endCol = startCol;
           // startCol = temp2;
            xDir=-1;
        }


        let len: number = Math.abs(startRow - endRow) + 1;
        let word: string = "";

        //console.log(startCol,endCol);
        //console.log(startRow,endRow);
        //console.log(xDir,yDir);

        let row:number=startRow;
        let col:number=startCol;

        for (let i: number = 0; i < len; i++) {
            /* let xx: number = startRow + i*xDir;
            let yy: number = startCol + i*yDir; */
            
            word += this.board.letters[col][row];
            this.selectionObj.selectArray.push(this.board.letterBoxes[row][col]);
            row+=yDir;
            col+=xDir;
            
            
        }
        return word;

    }
    onFlipped(orientation:string) {
        console.log("flipped");

        if (orientation == Phaser.Scale.LANDSCAPE) {
            this.scale.resize(this.flipVo.lW, this.flipVo.lH);
            this.grid.resetDim(this.flipVo.lW, this.flipVo.lH);
            this.grid.placeAt(1, 1, this.wordList);
            this.gw = this.flipVo.lW;
            this.gh = this.flipVo.lH;
        }
        else {
            this.grid.placeAt(1, 0, this.wordList);
            this.scale.resize(this.flipVo.pW, this.flipVo.pH);
            this.grid.resetDim(this.flipVo.pW, this.flipVo.pH);
            this.gw = this.flipVo.pW;
            this.gh = this.flipVo.pH;
        }
        this.wordList.resizeMe();
        this.grid.hide();
        this.makeGrid(22, 22);
        //this.grid.show();
        this.getPlaySize();
        this.alignBoard();
        this.drawLines();
    }

    private makeBoard() {

        let letterBox: LetterBox;

        for (let i: number = 0; i < this.board.rows; i++) {
            //  let letters: string[] = this.board.board[i].split("");

            for (let j: number = 0; j < this.board.cols; j++) {
                let letter = this.board.letters[j][i];
                letterBox = new LetterBox(this, letter);

                letterBox.resize(this.board.wordCellW, this.board.wordCellH);

                this.board.letterBoxes[i][j] = letterBox;

            }

        }
        this.alignBoard();
    }
    private alignBoard() {
        let xx: number = 0;
        let yy: number = 0;
        let letterBox: LetterBox;

        for (let i: number = 0; i < this.board.rows; i++) {

            for (let j: number = 0; j < this.board.cols; j++) {
                letterBox = this.board.letterBoxes[i][j];
                letterBox.x = this.board.gridStartX + xx;
                letterBox.y = this.board.gridStartY + yy;
                letterBox.row = i;
                letterBox.col = j;
                letterBox.back.name = "letterbox_" + i.toString() + "_" + j.toString();
                //letterBox.setSize(this.board.wordCellW,this.board.wordCellH);                
                letterBox.resize(this.board.wordCellW, this.board.wordCellH);
                xx += letterBox.back.displayWidth;

            }
            xx = 0;
            yy += letterBox.back.displayHeight;
        }
    }
    private getPlaySize() {
        let playSizeH: number;
        let playSizeW: number;
        ////console.log(this.gw);
        //  //console.log(this.board.cols);

        if (this.scale.orientation.toString() === Phaser.Scale.LANDSCAPE) {
            //console.log("landscape");

            playSizeH = this.gh * 0.8;
            playSizeW = this.gw * 0.55;
            this.board.gridStartY = this.gh * 0.1;
            this.board.gridStartX = this.gw * 0.45;
        }
        else {
            //console.log("port");
            playSizeH = this.gh * 0.5;
            playSizeW = this.gw * 0.9;
            this.board.gridStartY = this.gh * 0.25;
            this.board.gridStartX = this.gw * 0.1;
        }

        //console.log(playSizeW, playSizeH);

        let wordCellH: number = (Math.floor(playSizeH / this.board.rows) * 100) / 100;
        let wordCellW: number = (Math.floor(playSizeW / this.board.cols) * 100) / 100;

        //console.log(wordCellW, wordCellH);

        this.board.wordCellH = wordCellH;
        this.board.wordCellW = wordCellW;

        //console.log(this.board);

        //  this.gridStartX-=this.wordCellW;

    }
    makeWordList() {
        this.wordList = new WordList(this, this.board.words);
    }
}








