declare interface Window
{
	SpinePlugin: any
}
// @types/phaser/index.d.ts
/// <reference path="../../node_modules/phaser/types/SpineGameObject.d.ts" />
/// <reference path="../../node_modules/phaser/types/SpinePlugin.d.ts" />

declare module '*.png';
declare module '*.jpg';
declare module '*.json';
declare module '*.atlas';
