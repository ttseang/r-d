import { FontLoaderConfig } from "../fonts/FontLoaderConfig";

export class FontUtil {
    private font: string = null;
    private callback:Function=()=>{};

    constructor(callback:Function=()=>{})
    {
        this.callback=callback;
    }
    public init() {
      var script = document.createElement("script");
      
      if( document.location.protocol === "https:")
        script.src = "https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js";
       
      else
        script.src = "https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js";
       
       script.type = 'text/javascript';
       script.async = false;
       script.onload=()=>{
           console.log("LOADED");
           this.callback();
       }
       var firstScript = document.getElementsByTagName('script')[0];
       firstScript.parentNode.insertBefore(script, firstScript);
    }
  
    public onFontsActive = () => {
     console.log("Font active");
    }
    
    public onFontsInactive = () => {
      
    }
    
    
  }
  
 