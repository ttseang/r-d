import { GameObjects } from "phaser";
import IBaseScene from "../interfaces/IBaseScene";
import UIBlock from "../util/UIBlock";

export class LetterBox extends Phaser.GameObjects.Container
{
    public letter:string="";
    private text1:GameObjects.Text;
    public back:GameObjects.Image;
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    public row:number=0;
    public col:number=0;
    public next:LetterBox;

    constructor(bscene:IBaseScene,letter:string)
    {
        super(bscene.getScene());
        this.scene=bscene.getScene();

        this.letter=letter;       
       

        this.back=bscene.getScene().add.image(0,0,"holder");
        this.add(this.back);

        this.back.displayWidth=bscene.cw;
        this.back.displayHeight=bscene.ch;
        this.back.setTint(0xCCCCCC);
        this.back.alpha=0.9;

        

        let fs:number=bscene.getW()/20;

        if (this.scene.scale.orientation.toString()===Phaser.Scale.PORTRAIT)
        {
            fs=bscene.getW()/10;
        }
       // this.text1=this.scene.add.bitmapText(0,0,"font1",letter,fs);
       this.text1=this.scene.add.text(0,0,letter,{color:"#000000",fontFamily:"font1",fontSize:fs.toString()+"px"}).setOrigin(0.5,0.5);
        this.add(this.text1);

        this.setSize(this.back.displayWidth,this.back.displayHeight);
        this.back.setInteractive();
       this.scene.add.existing(this); 
    }
    
    resize(w:number,h:number)
    {
        this.back.displayWidth=w;
        this.back.displayHeight=h;
        this.setSize(this.back.displayWidth,this.back.displayHeight);
       // this.setInteractive();
    }
}