import { LetterBox } from "../ui/LetterBox";

export class BoxCollection
{
    public word:string;
    public boxes:LetterBox[]=[];
    public used:boolean=false;
    
    constructor(word:string)
    {
        this.word=word;
    }
}