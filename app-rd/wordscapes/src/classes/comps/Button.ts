import { GameObjects } from "phaser";
import IBaseScene from "../../interfaces/IBaseScene";
import { IGameObj } from "../../interfaces/IGameObj";
import Align from "../../util/align";
import UIBlock from "../../util/UIBlock";

export class Button extends UIBlock implements IGameObj {
    public text1: GameObjects.Text;
    private bscene: IBaseScene;
    private back: GameObjects.Image;

    private callback: Function = () => { };

    public type:string="button";
    public scene: Phaser.Scene;

    constructor(bscene: IBaseScene, buttonKey: string, text: string) {
        super();
        this.bscene = bscene;
        this.scene = bscene.getScene();
        //
        //
        //
        this.back = this.scene.add.image(0, 0, buttonKey);
        this.text1 = this.scene.add.text(0, 0, text, { "color": "#000000", "fontSize": "26px" }).setOrigin(0.5, 0.5);
        Align.scaleToGameW(this.back,0.3,this.bscene);
        
        //
        //
        //
        this.back.setScrollFactor(0, 0);
        this.text1.setScrollFactor(0, 0);
        //
        //
        //
        this.add(this.back);
        this.add(this.text1);
        //this.scene.add.existing(this);
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        //
        //
        //

    }
    scaleX: number;
    scaleY: number;
    setDepth(d:number)
    {
        this.back.setDepth(d+1);
        this.text1.setDepth(d+2);
        
    }
    setCallback(cb: Function) {

        this.callback = cb;
        this.back.setInteractive();
        this.back.on('pointerdown', () => {
            this.callback();
        })
    }
    update()
    {
        //console.log("update button");
        Align.scaleToGameW(this.back,0.3,this.bscene);
    }
}