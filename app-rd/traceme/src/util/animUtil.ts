export class AnimationUtil {
    AnimationUtil() {}
    /* makeChar(scene:Phaser.Scene, key:string, char:string) {
        console.log(char);
        if (char.indexOf("_fairy") > -1) {
            this.makeFairy(scene, key);
            return;
        }
        if (char.indexOf("_wizard") > -1) {
            this.makeWizard(scene, key);
            return;
        }
        if (char.indexOf("_elf") > -1) {
            let findex = char.split("_elf")[1];
            this.makeElf(scene, key, findex);
            return;
        }
        if (char.indexOf("_archer") > -1) {
            let aindex = parseInt(char.split("_archer")[1]) - 1;
            this.makeArcher(scene, key, aindex);
        }
        if (char.indexOf("_dwarf") > -1) {
            let dindex = parseInt(char.split("_dwarf")[1]);
            this.makeDwarf(scene, key, dindex);
        }
    } 
         makeEnemy(scene:Phaser.Scene, key:string, char:string) {
        console.log(char);
        if (char.indexOf("_shamans") > -1) {
         //   this.makeShamans(scene, key);
        }
        if (char.indexOf("assassin")>-1)
        {
            
            let aindex = parseInt(char.split("assassin")[1]);
            this.makeAssassin(scene:Phaser.Scene,key,aindex);
        }
        if (char.indexOf("demon")>-1)
        {
           let aindex2 = parseInt(char.split("demon")[1]);
            this.makeDemon(scene:Phaser.Scene,key,aindex2);   
        }
        if (char.indexOf('monster')>-1)
        {
            this.makeMonster(scene:Phaser.Scene,key);
        }
    }
    makeAnims(scene:Phaser.Scene, key:string) {
        console.log("anim key=" + key);
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Attack__',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: 0
        });
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'slide',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Slide__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpAttack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Attack__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpThrow',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Throw__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Idle__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Dead__',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'run',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Run__',
                suffix: '.png'
            }),
            frameRate: 16,
            repeat: -1
        });
    }
    makeAnims2(scene:Phaser.Scene, key:string) {
        //console.log("anim key=" + key);
        scene.anims.create({
            key: 'attack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Attack (',
                suffix: ').png'
            }),
            frameRate: 32,
            repeat: -1
        });
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'slide',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Slide (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpAttack',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'Jump_Attack (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'jumpThrow',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 0,
                prefix: 'JumpShoot (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 0,
                prefix: 'Idle (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        scene.anims.create({
            key: 'dead',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 0,
                prefix: 'Dead (',
                suffix: ').png'
            }),
            frameRate: 8,
            repeat: -1
        });
        let frames = scene.anims.generateFrameNames(key, {
            start: 0,
            end: 10,
            zeroPad: 0,
            prefix: 'Run (',
            suffix: ').png'
        });
        //console.log(frames);
        scene.anims.create({
            key: 'run',
            frames: frames,
            frameRate: 8,
            repeat: -1
        });
    } */
    makeDog(scene:Phaser.Scene, key:string) {
        console.log("anim key=" + key);
        
        scene.anims.create({
            key: 'jump',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 7,
                zeroPad: 3,
                prefix: 'jump_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
        scene.anims.create({
            key: 'walk',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'walk_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0
        });
       /*  scene.anims.create({
            key: 'slide',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'slide_',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        }); */
        
        scene.anims.create({
            key: 'idle',
            frames: scene.anims.generateFrameNames(key, {
                start: 0,
                end: 9,
                zeroPad: 3,
                prefix: 'idle',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: -1
        });
      
    }
   
}