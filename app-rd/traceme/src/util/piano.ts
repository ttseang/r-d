export class Piano
{
    private notes:number[];
    private scene:Phaser.Scene;
    private index:number=-1;

    constructor(scene:Phaser.Scene)
    {
        this.scene=scene;
        this.notes=[1,1,5,5,6,6,5];
    }
    public test()
    {
        setInterval(() => {
            this.playNext();
        }, 500);
        
    }
    public playNext()
    {
        this.index++;
        if (this.index>this.notes.length-1)
        {
            this.index=0;
        }
        let note:string="note"+this.notes[this.index].toString();
        console.log(note);
        let sound:Phaser.Sound.BaseSound=this.scene.sound.add(note);
        sound.play();
    }
}