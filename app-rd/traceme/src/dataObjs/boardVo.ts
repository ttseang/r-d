import { DrawVo } from "./drawVo";

export class BoardVo
{
    public imageKey:string;
    public col:number;
    public row:number;
    public scale:number;
    
    public draws:DrawVo[]=[];

    constructor(imageKey:string,col:number,row:number,scale:number)
    {
        this.imageKey=imageKey;
        this.row=row;
        this.col=col;
        this.scale=scale;
    }
}