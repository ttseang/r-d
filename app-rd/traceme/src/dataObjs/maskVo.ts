export class MaskVo
{
    public key:string;
    public col:number;
    public row:number;
    public scale:number;

    constructor(key:string,col:number,row:number,scale:number)
    {
        this.key=key;
        this.col=col;
        this.row=row;
        this.scale=scale;
    }
}