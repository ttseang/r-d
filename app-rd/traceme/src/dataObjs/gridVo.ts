export class GridVo
{
    public row:number;
    public col:number;
    
    //square height and width;

    public sh:number;
    public sw:number;

    public arrowAngle:number=-1;
    public arrowX:number=-1;
    public arrowY:number=-1;

    public debug:boolean=false;

    constructor(col:number,row:number,sw:number,sh:number,arrowAngle:number=-1,arrowX:number=0,arrowY:number=0,debug:boolean=false)
    {
        this.debug=debug;
        this.row=row;
        this.col=col;
        this.sh=sh;
        this.sw=sw;
        this.arrowAngle=arrowAngle;
        this.arrowX=arrowX;
        this.arrowY=arrowY;
    }
    toString()
    {
        let data:number[]=[this.row,this.col,this.sh,this.sw];

        return data.join(",");
    }
}