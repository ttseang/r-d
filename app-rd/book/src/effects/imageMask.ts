
import IBaseScene from "../interfaces/IBaseScene";
import Align from "../util/align";
import UIBlock from "../util/UIBlock";

export class ImageMask extends UIBlock {
    // private text:string;
    //private tf:Phaser.GameObjects.Text;
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    public holder: Phaser.GameObjects.Sprite;
    public ox: number = 0;
    public oy: number = 0;
    public image: Phaser.GameObjects.Sprite;
    public scaleX:number=1;
    public scaleY:number=1;
    
    constructor(bscene: IBaseScene, key: string, imageKey: string, scale: number = 0.2) {
        super();
        // this.text=tf2.text;
        this.bscene = bscene;
        this.scene = bscene.getScene();
        //  this.tf=this.scene.add.text(0,0,this.text);
        // this.tf.setColor(tf2.style.color);
        // this.tf.setFont(tf2.style.fontSize);
        // this.tf.setFontFamily(tf2.style.fontFamily);
        //
        //
        //

        this.image = this.scene.add.sprite(0, 0, imageKey);
        this.holder = this.scene.add.sprite(0, 0, key);
        //  this.image.alpha=0.2;
        Align.scaleToGameW(this.image, scale, bscene);
          //this.holder.alpha=0.5;

        this.add(this.image);
        this.add(this.holder);
    }
  
    setMask(index: number) {
        this.holder.displayWidth = this.image.displayWidth;
        this.holder.displayHeight = this.image.displayHeight / 3;
        this.holder.y = (this.image.displayHeight / 3) * index
        this.holder.visible = false;

        this.add(this.image);
        this.add(this.holder);
        this.image.setMask(new Phaser.Display.Masks.BitmapMask(this.scene, this.holder));
    }
    setDims(w:number,h:number)
    {
        this.holder.displayWidth = w;
        this.holder.displayHeight = h;
        this.image.displayWidth=w;
        this.image.displayHeight=h;
    }
    matchDims() {
        this.holder.displayWidth = this.image.displayWidth;
        this.holder.displayHeight = this.image.displayHeight;
    }
    setFullMask() {

        // this.holder.y=(this.image.displayHeight/3)*index
        this.holder.visible = false;

      //  this.add(this.image);
      //  this.add(this.holder);
        this.image.setMask(new Phaser.Display.Masks.BitmapMask(this.scene, this.holder));
    }

    destroy() {
        this.image.destroy();
        this.holder.destroy();
    }
}