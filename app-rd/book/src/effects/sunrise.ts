
import IBaseScene from "../interfaces/IBaseScene";
import { ImageMask } from "./imageMask";
import { TextMask } from "./textMask";

export class Sunrise
{
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private scale: number = 0;
    private dir: number = 1;
    private count: number = 0;
    private blocks: TextMask[] = [];
    //   private corners: Phaser.GameObjects.Sprite[] = [];
    private timer1: Phaser.Time.TimerEvent | undefined;
    private dropIndex: number = 3;
    public callback: Function = () => { };
    private animationName: string;
    private t: Phaser.GameObjects.Text;
    private tm:ImageMask;

    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text, animationName: string) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.animationName = animationName;
       
        this.t = t;

        this.tm=new ImageMask(bscene,"holder","sunrise");
     
        this.t.visible=true;

        this.tm.image.displayWidth=this.t.displayWidth;
        this.tm.image.scaleY=this.tm.image.scaleX;

        this.tm.holder.displayWidth=this.tm.image.displayWidth;
        this.tm.holder.displayHeight=this.tm.image.displayHeight;


        this.tm.x=t.x+t.displayWidth/2;
        this.tm.y=t.y-t.displayHeight/2;

        this.tm.image.alpha=0.9;

        this.tm.image.y=this.tm.holder.y+this.tm.holder.displayHeight;
        this.tm.setFullMask();
        this.animate();
        (window as any).tm=this;
    }
    animate()
    {
       // this.tm.holder.setScale(1,1);
        //   this.timer1=this.scene.time.addEvent({ delay: 50, callback:()=>{this.tm.image.alpha-=.05}, loop: true });
          // this.scene.tweens.add({targets: this.tm.holder,duration: 3000,scaleX:2,scaleY:2,yoyo:false,onComplete:this.done.bind(this)});;
      //  let osx:number=this.holder.scaleX;
      //  let osy:number=this.holder.scaleY;
      //  this.tm.holder.setScale(0.01,0.01);
     //   this.timer1=this.scene.time.addEvent({ delay: 50, callback:()=>{this.tm.image.alpha-=.05}, loop: true });
       this.scene.tweens.add({targets: this.tm.image,duration: 5000,y:this.tm.holder.y,yoyo:false,onComplete:this.done.bind(this)});;
    }
    done()
    {
        this.callback();
        setTimeout(() => {
          this.tm.destroy();
        }, 2000);
      //  this.timer1=this.scene.time.addEvent({ delay: 2000, callback:()=>this.destroyItems.bind(this), loop: false });
      //  
    }
    
}