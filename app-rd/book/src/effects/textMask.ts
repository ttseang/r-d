
import IBaseScene from "../interfaces/IBaseScene";
import UIBlock from "../util/UIBlock";

export class TextMask extends UIBlock
{
    private text:string;
    private tf:Phaser.GameObjects.Text;
    private bscene:IBaseScene;
    private scene:Phaser.Scene;
    public holder:Phaser.GameObjects.Sprite;
    public ox:number=0;
    public oy:number=0;
    public index:number=0;
    
    constructor(bscene:IBaseScene,tf2:Phaser.GameObjects.Text,key:string)
    {
        super();
        this.text=tf2.text;
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.tf=this.scene.add.text(0,0,this.text);
        this.tf.setColor(tf2.style.color);
      // this.tf.setColor("#ff00ff");
        this.tf.setFont(tf2.style.fontSize);
        this.tf.setFontFamily(tf2.style.fontFamily);
        //
        //
        //
        this.holder=this.scene.add.sprite(0,0,key).setOrigin(0,0);
     //   this.holder.alpha=0.5;

       
    }
    setMask(index:number)
    {
        this.holder.displayWidth=this.tf.displayWidth;
        this.holder.displayHeight=this.tf.displayHeight/3;
        this.holder.y=(this.tf.displayHeight/3)*index
        this.holder.visible=false;

        this.add(this.tf);
        this.add(this.holder);
        this.tf.setMask(new Phaser.Display.Masks.BitmapMask(this.scene,this.holder));
    }
    setFullMask()
    {
        this.holder.displayWidth=this.tf.displayWidth;
        this.holder.displayHeight=this.tf.displayHeight;
       // this.holder.y=(this.image.displayHeight/3)*index
        this.holder.visible=false;

        this.add(this.tf);
        this.add(this.holder);
        this.tf.setMask(new Phaser.Display.Masks.BitmapMask(this.scene,this.holder));
    }
    destroy()
    {
        this.tf.destroy();
        this.holder.destroy();
    }
}