import IBaseScene from "../interfaces/IBaseScene";


export class BoxDraw {
    private t: Phaser.GameObjects.Text;
    private bscene: IBaseScene;
    private scene: Phaser.Scene;
    private north: Phaser.GameObjects.Line | undefined;
    private south: Phaser.GameObjects.Line | undefined;
    private east: Phaser.GameObjects.Line | undefined;
    private west: Phaser.GameObjects.Line | undefined;
    private per: number = 0;
    private callback: Function = () => { };
    private timer1:Phaser.Time.TimerEvent | undefined;
    
    constructor(bscene: IBaseScene, t: Phaser.GameObjects.Text) {
        this.bscene = bscene;
        this.scene = bscene.getScene();
        this.t = t;
        this.drawLines();
    }


    drawLines() {
        var top = this.t.y;//-this.t.displayHeight/2;
        var bottom = top + this.t.displayHeight;
        var left = this.t.x;// - this.t.displayWidth / 2;
        var right = left + this.t.displayWidth;
        console.log(left);
        //
        //
        //
        this.north = this.scene.add.line(left, top, 0, 0, this.t.displayWidth, 0, 0XFF0000).setOrigin(0, 0);
        this.south = this.scene.add.line(right, bottom, 0, 0, this.t.displayWidth, 0, 0XFF0000).setOrigin(1, 0);
        this.east = this.scene.add.line(right, top, 0, this.t.displayHeight, 0, 0, 0XFF0000).setOrigin(0, 0);
        this.west = this.scene.add.line(left, bottom, 0, this.t.displayHeight, 0, 0, 0XFF0000).setOrigin(0, 1);
        this.per = 0;
        this.draw(0);
    }

    draw(per: number) {
        if (per >100) {
            this.stop();
            if (this.callback) {
                this.callback();
            }
            return;
        }
        var ns = 1;
        var ss = 1;
        var es = 1;
        var ws = 1;
        //
        //
        //
        if (per > 75) {
            ns = (per - 75) / 25;
        } else {
            ns = 0;
        }
        if (per > 50 && per < 76) {
            ws = (per - 50) / 25;
        }
        if (per < 51) {
            ws = 0;
        }
        if (per > 25 && per < 51) {
            ss = (per - 25) / 25;
        }
        if (per < 26) {
            ss = 0;
            es = per / 25;
        }
        if (this.north) {
            this.north.scaleX = ns;
        }
        if (this.south) {
            this.south.scaleX = ss;
        }
        if (this.west) {
            this.west.scaleY = ws;
        }
        if (this.east) {
            this.east.scaleY = es;
        }


    }
    tick() {
        this.per+= 5;
        this.draw(this.per);
    }
    reset() {
        this.stop();
        this.per = 100;
        this.start();
    }
    start() {
        this.timer1 = this.scene.time.addEvent({
            delay: 125,
            callback: this.tick,
            callbackScope: this,
            loop: true
        });
    }
    stop() {
        if (this.timer1)
        {
            this.timer1.remove();
        }      
    }
}
