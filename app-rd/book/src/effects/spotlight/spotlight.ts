//import { SpotlightOverlay } from "./spotlightOverlay";

import { SpotlightOverlay } from "./spotlightoverlay";

export class Spotlight
{
    private so:SpotlightOverlay;
    private defaultDuration:number=2000;
    constructor()
    {
        this.so=new SpotlightOverlay();
       // window.spot=this;
    }

    run(rectangle:ClientRect, duration:number)
    {
        /*
		This is it, the main API of this whole class.
		All you gotta do is call SpotlightEffect.run() to make the effect happen!
		You pass it the bounding rectangle of the thing you want spotlighted,
		and the duration (in milliseconds) you want the effect to last.
		*/
		var cx, cy, r;
        
		if (duration === undefined) duration = this.defaultDuration;
		cx = Math.round(rectangle.left + rectangle.width/2);
		cy = Math.round(rectangle.top + rectangle.height/2);
		r = Math.round(Math.hypot(rectangle.width,rectangle.height)/2);
		this.so.setBackground(cx, cy, r);
		this.so.fadeIn();
		setTimeout(this.doFade.bind(this), duration);
       
    }
    doFade()
    {
        this.so.fadeOut();
    }
    
}
export default Spotlight;