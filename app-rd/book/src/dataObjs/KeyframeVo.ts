
import { IGameObj } from "../interfaces/IGameObj";
import { AttsVo } from "./AttsVo";
//import { EffectsAttsVo } from "./effectAttsVo";
import { HighlightVo } from "./HighlightVo";
//import { TextClasses } from "./TextClasses";

export class KeyframeVo
{
    public itemName:string="";
    public start:number;
    public atts:AttsVo | undefined;
    
  //  public effectAtts:EffectsAttsVo|undefined;
   // public textClasses:TextClasses|undefined;
    public cssClass:string="";  
    public cssSize:string="f1";
//public effectName:string="";
    //public duration:number=0;

    public highlights:HighlightVo[]=[];
    public active:boolean=true;
   // public centerText:boolean=false;
    
    //public type:string="";

    //update take
    public eventType:string="";

    public item:IGameObj | undefined;
    public useTween:boolean=false;
    
    constructor(itemName:string,start:number,eventType:string)
    {
        this.itemName=itemName;
        this.start=start;        
        this.eventType=eventType;
    }
}