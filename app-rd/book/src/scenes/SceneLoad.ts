
//import { Sound } from "phaser";
import MainStorage from "../classes/MainStorage";
import Align from "../util/align";
import { BaseScene } from "./BaseScene";

export class SceneLoad extends BaseScene {

    private loadingText: Phaser.GameObjects.Text | undefined;
    private ms: MainStorage = MainStorage.getInstance();

    constructor() {
        super("SceneLoad");

    }
    preload() {
        super.create();

        this.sound.once(Phaser.Sound.Events.DECODED_ALL, this.soundDecoded.bind(this));


        this.loadingText = this.add.text(0, 0, "0%", { fontSize: "80px", color: "#00000" }).setOrigin(0.5, 0.5);
        Align.center(this.loadingText, this);
        this.load.on('progress', this.onProgress.bind(this));
        // this.load.once(Phaser.Sound.Events.DECODED_ALL,this.soundDecoded.bind(this));

        this.load.path="assets/";
        
        this.load.image("holder", "common/holder.jpg");
        this.load.image("rect", "common/rect.jpg");
        this.load.image("face", "common/face.png");
        this.load.image("bracket", "common/corner.png");
        this.load.image("hand", "common/hand.png");
        this.load.image("punch", "common/punch.png");
        this.load.image("thumbsUp", "common/thumbsUp.png");
        this.load.image("circle", "common/circle.png");
        this.load.image("heart", "common/heart.png");
        this.load.image("balloon", "common/balloon.png");
        this.load.image("rocket", "common/rocket2.png");
        this.load.image("rays3", "common/rays3.png");
        this.load.image("donut", "common/donut.png");
        this.load.image("sunrise", "common/sunrise.png");

        this.load.atlas("halfRays", "common/rays_half.png", "common/rays_half.json");
        this.load.atlas("brackets", "common/bracket.png", "common/bracket.json");
        this.load.atlas("bubblePop", "common/bubblePop.png", "common/bubblePop.json");
        this.load.atlas("starBurst", "common/starburst.png", "common/starburst.json");
        this.load.atlas("fullRays", "common/rays.png", "common/rays.json");
        this.load.atlas("raysPaper", "common/raysPaper.png", "common/raysPaper.json");
        this.load.atlas("sparks", "common/sparks.png", "common/sparks.json");
        this.load.atlas("radial", "common/radial.png", "common/radial.json");

        this.load.image("paperTop", "common/paperTop.png");
        this.load.image("paperBottom", "common/paper1.png");

        if (this.ms.loadPageAudio == true) {
            for (let i: number = 1; i < this.ms.numberOfPages + 1; i++) {
                this.load.audio("paudio"+i.toString(), "pageAssets/" + this.ms.book + "/pageAudio/" + i.toString() + ".mp3");
            }
        }
        if (this.ms.loadPageImages == true) {
            for (let i: number = 1; i < this.ms.numberOfPages + 1; i++) {
                this.load.image("pimage" + i.toString(), "pageAssets/" + this.ms.book + "/pageImages/" + i.toString() + ".jpg");
            }
        }
        for (let i:number=0;i<this.ms.customImages.length;i++)
        {
            this.load.image(this.ms.customImages[i].key,"pageAssets/" + this.ms.book + "/images/"+this.ms.customImages[i].path);
        }
        for (let i:number=0;i<this.ms.customAudio.length;i++)
        {
            console.log(this.ms.customAudio[i]);

            this.load.audio(this.ms.customAudio[i].key,"pageAssets/" + this.ms.book + "/audio/"+this.ms.customAudio[i].path);
        }
        
        //this.load.audio("FreshAndCool", "pageAssets/icecream/audio/backgroundMusic/FreshAndCool.mp3");

        //this.load.image("bg1", "./common/bg1.jpg");
    }
    onProgress(per: number) {
        per = Math.floor(per * 100);
        if (this.loadingText) {
            this.loadingText.setText(per.toString() + "%");
        }

    }
    create() {
        //let audio1:Sound.BaseSound=this.sound.add("audio1");
        ////  audio1.once(Phaser.Sound.Events.DECODED, this.soundDecoded.bind(this));
        // let am:Sound.WebAudioSoundManager=new Sound.WebAudioSoundManager(this.sys.game);
        // am.once(Phaser.Sound.Events.DECODED, this.soundDecoded.bind(this));

        setTimeout(() => {
            this.scene.start("SceneMain");
        }, 3000);
    }
    soundDecoded() {

    }
    update() {

    }
}
export default SceneLoad;