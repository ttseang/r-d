
import { EffectDataVo } from "../dataObjs/EffectDataVo";

export class EffectsDef {
    public sent: string;
    private static instance: EffectsDef;
    public gameConfig: any;
    public game:Phaser.Game | undefined=undefined;
    //
    //

    public selectedFile: number = 0;
    public prevData: string = "";

    public makeNewFlag: boolean = true;
    public prevMode: boolean = false;

    public effectKeys:string[]=[];

    //public effectData: Map<string, EffectDataVo> = new Map();
    public effectArray:Array<EffectDataVo>=[];

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        this.sent = "";
        //this.addEffectData(new EffectDataVo(key,name,cssClassForMain,cssClassForHighLight,phaserEffect,duration);
        
        this.addEffectData(new EffectDataVo("dropText", "Drop Slices", "noEffect", "noEffect", "dropText", 1));


        this.addEffectData(new EffectDataVo("sliceRight", "Slices Right", "noEffect", "noEffect", "sliceRight", 1));
        this.addEffectData(new EffectDataVo("sliceLeft", "Slices Left", "noEffect", "noEffect", "sliceLeft", 1));
        this.addEffectData(new EffectDataVo("sliceMix", "Slices Mix", "noEffect", "noEffect", "mixSlice", 1));

        this.addEffectData(new EffectDataVo("rays", "Rays", "noEffect", "noEffect", "rays", 1));
        
        this.addEffectData(new EffectDataVo("sunrise", "Sunrise", "noEffect", "noEffect", "sunrise", 1));
        this.addEffectData(new EffectDataVo("redBox", "Red Box", "noEffect", "noEffect", "redBox", 1));
        this.addEffectData(new EffectDataVo("halfRays", "Half Rays", "noEffect", "noEffect", "halfRays", 1));
        this.addEffectData(new EffectDataVo("fullRays", "Full Rays", "noEffect", "noEffect", "fullRays", 1));
        this.addEffectData(new EffectDataVo("paperRays", "Paper Rays", "noEffect", "noEffect", "raysPaper", 1));
        this.addEffectData(new EffectDataVo("spotlight", "Spotlight", "noEffect", "noEffect", "spotlight", 5000));

        this.addEffectData(new EffectDataVo("revealLeft", "Reveal Left", "noEffect", "noEffect", "revealLeft", 1));
        this.addEffectData(new EffectDataVo("revealRight", "Reveal Right", "noEffect", "noEffect", "revealRight", 1));

        this.addEffectData(new EffectDataVo("revealUp", "Reveal Up", "noEffect", "noEffect", "revealUp", 1));
        this.addEffectData(new EffectDataVo("revealDown", "Reveal Down", "noEffect", "noEffect", "revealDown", 1));

        this.addEffectData(new EffectDataVo("thumbsUp", "Thumbs Up", "noEffect", "noEffect", "thumb", 1));
        this.addEffectData(new EffectDataVo("bob", "Bobbing", "noEffect", "noEffect", "bob", 0.5));
        this.addEffectData(new EffectDataVo("punch", "Punch The Word", "noEffect", "noEffect", "punch", 1));
        this.addEffectData(new EffectDataVo("pointer", "fly and point", "noEffect", "noEffect", "point", 2));
        this.addEffectData(new EffectDataVo("brackets", "add brackets", "noEffect", "noEffect", "brackets", 4));
        this.addEffectData(new EffectDataVo("fly", "fly to place", "noEffect", "noEffect", "fly", 4));
        this.addEffectData(new EffectDataVo("bubbles", "Bubbles", "noEffect", "noEffect", "bubbles", 4));
        this.addEffectData(new EffectDataVo("hearts", "Hearts", "noEffect", "noEffect", "hearts", 4));
        this.addEffectData(new EffectDataVo("faces", "Faces", "noEffect", "noEffect", "faces", 2));
        this.addEffectData(new EffectDataVo("balloons", "Balloons", "noEffect", "noEffect", "balloons", 4));
        this.addEffectData(new EffectDataVo("rockets", "Rockets", "noEffect", "noEffect", "rockets", 4));
        
        this.addEffectData(new EffectDataVo("strike", "Cross Out", "noEffect", "strike", "none"));
        this.addEffectData(new EffectDataVo("strikeall", "Cross Out All",  "strike","noEffect", "none"));
        this.addEffectData(new EffectDataVo("blink", "Flashing Red Highlight", "noEffect", "blink_me", "none"));        
        this.addEffectData(new EffectDataVo("glow", "Flashing Red Glow", "noEffect", "glow", "none"));
       
        this.addEffectData(new EffectDataVo("flip_in_place", "flip in place", "noEffect", "noEffect", "flipInPlace", 4));
        this.addEffectData(new EffectDataVo("grow", "grow in place", "noEffect", "noEffect", "grow", 2));
        this.addEffectData(new EffectDataVo("spinGrow", "spin and grow", "noEffect", "noEffect", "spinGrow", 4));
        this.addEffectData(new EffectDataVo("shake", "shake in place", "noEffect", "noEffect", "shake", 0.25));
        this.addEffectData(new EffectDataVo("shakeZoom", "shake & zoom", "noEffect", "noEffect", "shakeZoom", 0.25));
        this.addEffectData(new EffectDataVo("slowglow","slow glow","noEffect","slowglow","none",0));
        this.addEffectData(new EffectDataVo("fullslowglow","test","slowglow","highlight2","none",0));
        this.addEffectData(new EffectDataVo("fastflash","fast flash","noEffect","fastflash","none",0));
        this.addEffectData(new EffectDataVo("fineshine","fine shine","noEffect","fineshine","none",0));
        this.addEffectData(new EffectDataVo("rainbow","rainbow","noEffect","rainbow","none",0));
        this.addEffectData(new EffectDataVo("starBurst","StarBurst","noEffect","noEffect","starBurst",0));
        this.addEffectData(new EffectDataVo("enlarge","Enlarge","noEffect","noEffect","enlarge",1));
        this.addEffectData(new EffectDataVo("paperReveal","Paper Reveal","noEffect","noEffect","paperReveal",0));
        this.addEffectData(new EffectDataVo("sparkles","Sparkles","noEffect","noEffect","sparks",0));
        //paperReveal
       this.addEffectData(new EffectDataVo("bubblePop","bubble pop","noEffect","noEffect","bubblePop",0));
      // this.addEffectData(new EffectDataVo("radial","Raidal","noEffect","noEffect","radial",4));
        
        //this.addEffectData(new EffectDataVo("test2","test 2","text","word wisteria","none",0));
      //  this.addEffectData(new EffectDataVo("bounce","Bounce","animate__animated animate__bounce","highlight2","none",0));
        //slide-fwd-center
    }
    static getInstance(): EffectsDef {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new EffectsDef();
        }
        return this.instance;
    }
    public addEffectData(effectData: EffectDataVo) {
        //this.effectData.set(effectData.key, effectData);
        effectData.name=effectData.name.toUpperCase();
        this.effectArray.push(effectData);
       // this.effectKeys.push(effectData.key.toLowerCase());
        /* this.effectArray.sort((a:EffectDataVo,b:EffectDataVo)=>{
            return (a.name<b.name)?1:0
        }); */
       // console.log(this.effectArray);
    }
    public getEffectData(key: string) {
       /*  if (this.effectData.has(key)) {
            return this.effectData.get(key);
        } */

        for (let i:number=0;i<this.effectArray.length;i++)
        {
            if (this.effectArray[i].key===key)
            {
                return this.effectArray[i]
            }
        }

        return new EffectDataVo("blank", "blank", "noEffect", "noEffect", "none");
    }
}
export default EffectsDef;