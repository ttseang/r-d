import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { Align } from "../util/align";

export class MaskedImage implements IGameObj
{
    private bscene:IBaseScene;
    private scene:Phaser.Scene;
    public image:Phaser.GameObjects.Sprite;
    public mask:Phaser.GameObjects.Sprite;
    public visible:boolean=true;
    public alpha:number=1;
    public displayWidth:number=0;
    public displayHeight:number=0;
    public scaleX:number=1;
    public scaleY:number=1;
    public x:number=0;
    public y:number=0;
    public inPlace:boolean=false;
    public type:string="";

    constructor(bscene: IBaseScene, key: string, imageKey: string, scale: number = 0.2)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        console.log("Mask="+key);
        
        this.image=this.scene.add.sprite(0,0,imageKey);
        this.mask=this.scene.add.sprite(0,0,key);
        Align.scaleToGameW(this.image,scale,bscene);
        Align.scaleToGameW(this.mask,scale*0.7,bscene);
       // this.mask.alpha=0.5;
        this.mask.visible=false;
        this.image.setMask(new Phaser.Display.Masks.BitmapMask(this.scene, this.mask));
    }
    
    update(): void {
        
    }
    resize(scale:number)
    {
        Align.scaleToGameW(this.image, scale, this.bscene);
        Align.scaleToGameW(this.mask,scale*0.75,this.bscene);
    }
}