import IBaseScene from "../interfaces/IBaseScene";
let instance:AudioPlayer | null=null;

export class AudioPlayer
{
    private bscene:IBaseScene;
    private scene:Phaser.Scene;
    private isPlaying:boolean=false;
    public sound:Phaser.Sound.BaseSound | undefined;
    private backgroundName:string="";
    public backgroundSound:Phaser.Sound.BaseSound | undefined;
    
    constructor(bscene:IBaseScene)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();

    }
    public static getAudioPlayer(bscene:IBaseScene)
    {
        if (instance===null)
        {
            instance=new AudioPlayer(bscene);
        }
        return instance;
    }
    public playSound(key:string)
    {
        console.log(key);
        if (this.isPlaying===false)
        {
            this.sound=this.scene.sound.add(key);
            this.sound.play();
            this.sound.once(Phaser.Sound.Events.COMPLETE,()=>{this.isPlaying=false;
            this.sound=undefined});

            
        }
        else
        {
            if (this.sound)
            {
                this.sound.resume();
            }
        }
    }
    public resume()
    {
        if (this.sound)
        {
            this.sound.resume();
        }
    }
    public pause()
    {
        if (this.isPlaying===true)
        {
            if (this.sound)
            {
                this.sound.pause();
            }
        }
    }
    public setBackgroundMusic(music: string) {
        if (music === this.backgroundName) {
            return;
        }
        if (music === "") {
            return;
        }
        this.backgroundName = music;
        if (this.backgroundSound) {
            this.backgroundSound.stop();

        }
        this.backgroundSound = this.scene.sound.add(music, { volume: 0.1, loop: true });
        if (this.backgroundSound)
        {
            this.backgroundSound.play();
        }
        
    }
}