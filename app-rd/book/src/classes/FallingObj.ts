import { GameObjects } from "phaser";
import { PosVo } from "../dataObjs/PosVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { BaseScene } from "../scenes/BaseScene";
import { AlignGrid } from "../util/alignGrid";

export class FallingObj extends GameObjects.Sprite implements IGameObj
{
    private grid:AlignGrid;
    public pos:ResizeVo;
    private bscene:IBaseScene;

    constructor(bscene:IBaseScene,key:string)
    {
        super(bscene.getScene(),0,0,key);
        this.grid=bscene.getGrid();
        bscene.getScene().add.existing(this);
        this.bscene=bscene;
        this.type="fallingObj";
        
         setInterval(() => {
            this.updatePos();
        }, 50);
    }
    updatePos()
    {
        this.y+=this.grid.ch/50;
        this.pos.posY+=0.02;
        if (this.y>this.bscene.getH())
        {
            this.pos.posY=0;
            this.y=0;
        }
    }
    update()
    {
        console.log("update parachute");
        this.grid=this.bscene.getGrid();
        this.grid.placeAt(this.pos.posX,this.pos.posY,this);
    }
}