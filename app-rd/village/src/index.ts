import * as Phaser from "phaser";
import "./style.css";
import { GM } from "./classes/GM";
import { SceneMap } from "./village/scenes/SceneMap";
import { SceneInside } from './village/scenes/SceneInside';
import { SceneData } from './scenes/SceneData';
import { SceneCookieData } from './minigames/cookies/scenes/SceneCookieData';
import { SceneCookieLoad } from './minigames/cookies/scenes/SceneCookieLoad';
import { SceneCookie } from './minigames/cookies/scenes/SceneCookie';
import { SceneCutter } from './minigames/cookies/scenes/SceneCutter';
import SceneFont from './scenes/SceneFont';
import SceneLoadMovie from './theater/scenes/SceneLoadMovie';
import { SceneMovieData } from './theater/scenes/SceneMovieData';
import SceneMovie from './theater/scenes/SceneMovie';

//import 'phaser/plugins/spine/dist/SpinePlugin';
//import "phaser/plugins/spine/src/gameobject/SpineGameObject";


// Constants
const cspMeta = document.createElement("meta");
const gameDiv: HTMLElement = document.createElement("div");
gameDiv.id = "phaser-game";

// Document Head
cspMeta.httpEquiv = "Content-Security-Policy";
cspMeta.content = "script-src 'self' 'unsafe-inline';";
document.head.appendChild(cspMeta);

// Document Body
document.body.appendChild(gameDiv);


let gm: GM = GM.getInstance();
let isMobile = navigator.userAgent.indexOf("Mobile");
let isTablet = navigator.userAgent.indexOf("Tablet");
let isIpad = navigator.userAgent.indexOf("iPad");

if (isTablet != -1 || isIpad != -1) {
    gm.isTablet = true;
    isMobile = 1;
}
if (isMobile != -1) {
    gm.isMobile = true;
} 
let w = window.innerWidth;
let h = window.innerHeight;
//
//

if (w < h) {
    gm.isPort = true;
}
const config: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    backgroundColor: 'ff0000',
    parent: 'phaser-game',
    physics:{
        default:"arcade",
        arcade:{debug:false}
    },
    scene: [SceneData,SceneMap,SceneInside,SceneCookieData,SceneCookieLoad,SceneCutter,SceneCookie,SceneLoadMovie,SceneMovieData,SceneMovie]
};
/* ,
    plugins: {
        scene: [
            { key: 'SpinePlugin', plugin: window.SpinePlugin, mapping: 'spine' }
        ]
    } */
console.log("INDEX");

new Phaser.Game(config);