import { Game, GameObjects, Physics, Tilemaps } from "phaser";
import { MessageBox } from "../../classes/comps/MessageBox";
import { GM } from "../../classes/GM";
import { LocVo } from "../../dataObjs/LocVo";
import Align from "../../util/align";
import { BaseScene } from "../../scenes/BaseScene";
import { PosVo } from "../../dataObjs/PosVo";
import { BuildingVo } from "../../dataObjs/BuildingVo";

export class SceneMap extends BaseScene {
    private controls: any;
    private map: Phaser.Tilemaps.Tilemap;
    private layer1: Phaser.Tilemaps.TilemapLayer;
    private gm: GM = GM.getInstance();
    private clickLock: boolean = false;
    private mb: MessageBox;
    // private cursors: Phaser.Types.Input.Keyboard.CursorKeys;
    private scrollX: number = 0;
    private scrollY: number = 0;
    private avatar: Physics.Arcade.Sprite;
    private offMap: Phaser.GameObjects.Group;

    private buildingsGroup: Phaser.Physics.Arcade.Group;
    private moveGroup: Phaser.Physics.Arcade.Group;
    private pickupGroup: Phaser.Physics.Arcade.Group;

    private avatarFace: number = 0;
    private isWalking: boolean = false;
    private testBuilding: GameObjects.Sprite;
    private block: GameObjects.Image;

    private avatarTarget: PosVo = new PosVo(-1, -1);
    private lastVY: number = 0;
    private lastVX: number = 0;

    constructor() {
        super("SceneMap");
    }
    preload() {


    }
    create() {
        super.create();
        this.makeGrid(11, 11);
        this.offMap = new Phaser.GameObjects.Group(this);
        this.buildingsGroup = this.physics.add.group();
        this.moveGroup = this.physics.add.group();
        this.pickupGroup = this.physics.add.group();

        this.makeAvatar();
        this.makeAnim("fountain");

        //
        //
        //
        this.buildWorld();
        //
        //
        //
        //  this.cursors = this.input.keyboard.createCursorKeys();

        this.avatar = this.physics.add.sprite(0, 0, "avatar");
        Align.scaleToGameW(this.avatar, 0.02, this);
        // Align.center(this.avatar,this);    
        this.avatar.setFrame("idle_right_up.png");
        this.placeOnMap(this.gm.startX, this.gm.startY, this.avatar);
        this.avatar.setBounce(0.1, 0.1);


        this.cameras.main.startFollow(this.avatar);

        this.physics.add.collider(this.avatar, this.buildingsGroup, null, this.hitSomething.bind(this));
        this.physics.add.collider(this.avatar, this.moveGroup);
        this.physics.add.collider(this.avatar, this.pickupGroup, null, this.pickUpTrash.bind(this));
        this.physics.add.collider(this.buildingsGroup, this.moveGroup);
        this.physics.add.collider(this.moveGroup, this.moveGroup);

        this.block = this.add.image(0, 0, "holder");
        this.block.setTint(0xff0000);
        this.block.visible = false;
        //
        //
        //
        this.mb = new MessageBox(this);
        this.mb.closeCallback = this.messageClose.bind(this);
    }
    pickUpTrash(avatar: Phaser.Physics.Arcade.Sprite, obj: Phaser.Physics.Arcade.Sprite) {
        obj.destroy();
    }
    hitSomething() {
        let currentVX: number = this.getSgn(this.avatar.body.velocity.x);
        let currentVY: number = this.getSgn(this.avatar.body.velocity.y);
        if (currentVY === 0 && currentVX == 0) {
            currentVY = this.lastVY;
            currentVX = this.lastVX;
        }
        this.lastVX = currentVX;
        this.lastVY = currentVY;
        //  //console.log(currentVX,currentVY);

        //this.avatar.setVelocity(-this.avatar.body.velocity.x*0.5,-this.avatar.body.velocity.y*0.5)
        this.avatar.setVelocity(0, 0);

        let tx: number = this.avatar.x - currentVX * 50;
        let ty: number = this.avatar.y - currentVY * 50;

        this.tweens.add({ duration: 250, targets: [this.avatar], x: tx, y: ty, onComplete: this.hitDone.bind(this) });
        this.clickLock = true;

        this.avatarIdle();
        this.isWalking = false;
    }
    hitDone() {
        this.clickLock = false;
    }
    makeAvatar() {
        this.anims.create({
            key: 'walkleft',
            frames: [
                { key: 'avatar', frame: "walk_left_1.png" },
                { key: 'avatar', frame: "walk_left_2.png" }
            ],
            frameRate: 8,
            repeat: -1
        });
        this.anims.create({
            key: 'walkright',
            frames: [
                { key: 'avatar', frame: "walk_right_1.png" },
                { key: 'avatar', frame: "walk_right_2.png" }
            ],
            frameRate: 8,
            repeat: -1
        });
        this.anims.create({
            key: 'walkleftup',
            frames: [
                { key: 'avatar', frame: "walk_up_left_1.png" },
                { key: 'avatar', frame: "walk_up_left_2.png" }
            ],
            frameRate: 8,
            repeat: -1
        });
        this.anims.create({
            key: 'walkrightup',
            frames: [
                { key: 'avatar', frame: "walk_up_right_1.png" },
                { key: 'avatar', frame: "walk_up_right_2.png" }
            ],
            frameRate: 8,
            repeat: -1
        });
    }
    makeAnim(key: string) {
        //let frameNames:any[]= this.textures.get(key).getFrameNames();
        // //////console.log(frameNames);
        let animConfig: any = {
            key: key + 'play',
            frames: this.anims.generateFrameNames(key),
            frameRate: 8,
            repeat: -1
        }
        //////console.log(animConfig);

        this.anims.create(animConfig);
    }

    makeLocks() {
        this.gm.buildings.forEach((locVo: LocVo) => {
            //////console.log(locVo);
            if (locVo.locked == true) {
                let lock: GameObjects.Image = this.placeItem(locVo.x, locVo.y, "lock");
                lock.setDepth(200000);
                let tile: Phaser.Tilemaps.Tile = this.layer1.getTileAt(locVo.x, locVo.y);

                /* if (tile) {
                    tile.tint = 0xbbbbbb;
                } */
            }
            if (locVo.name === this.gm.lastUnlocked.name) {
                let hand: GameObjects.Image = this.placeItem(locVo.x, locVo.y, "hand");
                Align.scaleToGameW(hand, 0.05, this);
                hand.y -= hand.displayHeight;
                hand.x -= hand.displayWidth;
                hand.setDepth(100000);
                hand.angle = 300;
            }
        });
    }

    messageClose() {
        this.clickLock = false;
        //////console.log("message close");
    }
    buildWorld() {
        this.map = this.add.tilemap('map');
        var tileset1: Phaser.Tilemaps.Tileset = this.map.addTilesetImage('village', 'village');
        var tileset2: Phaser.Tilemaps.Tileset = this.map.addTilesetImage("village2", "village2");
        let tileset3: Phaser.Tilemaps.Tileset = this.map.addTilesetImage("roadtiles2", "roadtiles2");
        let gardenTileset: Phaser.Tilemaps.Tileset = this.map.addTilesetImage("park", "park");

        this.layer1 = this.map.createLayer('Tile Layer 1', [tileset1, tileset2, tileset3, gardenTileset]);


        /*  let layer2=this.map.createFromObjects("shops",[]);
         //////console.log(layer2); */

      //  window['scene'] = this;
        //   this.cameras.main.setPosition(0,0);
        //this.cameras.main.setZoom(2);


        this.input.on('pointerdown', this.worldClicked.bind(this));
        this.cameras.main.setScroll(-1900, 900);
        this.cameras.main.setZoom(1);

        

        this.placeAnim(4, 7, "fountain");
        for (let i: number = 0; i < 12; i++) {
            let ball: Phaser.Physics.Arcade.Sprite = this.placeMoveObj(5, 6, 0.01, "ball");
            ball.x += Phaser.Math.Between(-60, 60);
            ball.y += Phaser.Math.Between(-60, 60);
        }

        let trashTiles:number[]=[29,30,50,51];

        for (let i: number = 0; i < 60; i++) {
            let tx: number = Phaser.Math.Between(1, 11);
            let ty: number = Phaser.Math.Between(1, 11);
            let tile: Phaser.Tilemaps.Tile = this.layer1.getTileAt(tx, ty);
            
            if (trashTiles.includes(tile.index)) {
                console.log(tx,ty,tile.index);
                let trash: Phaser.Physics.Arcade.Sprite = this.placePickUp(tx+1, ty, 0.015, "trash");
               // trash.x += Phaser.Math.Between(-30, 30);
               // trash.y += Phaser.Math.Between(-30, 30);
                let frame:string=Phaser.Math.Between(1,5).toString()+".png";
                trash.setFrame(frame);
            }
        }
        this.makeLocks();
        this.makeBuildings();
    }
    makeBuildings() {
        this.layer1.forEachTile((tile) => {
            // ////console.log(tile.index);
            //console.log(tile.tileset.name);

            let index: number = tile.index;
            if (tile.tileset.name === "village2") {
                index += 100;
            }
            if (tile.tileset.name === "park") {
                index += 200;
            }
            tile.width = 514;
            tile.height = 258;
            //
            //
            //
            if (this.gm.mapSprites.has(index)) {
                let buildingVo: BuildingVo = this.gm.mapSprites.get(index);
                let frame: string = buildingVo.key;
                tile.index = buildingVo.tile;
                let shop: Phaser.Physics.Arcade.Sprite = this.placeBuilding(tile, buildingVo);
                if (this.gm.buildings.has(frame)) {
                    let locVo: LocVo = this.gm.buildings.get(frame);
                    if (locVo.locked === true) {
                        shop.setTint(0xbbbbbb);
                    }
                }

                /*  shop.y+=buildingVo.offsetY*this.gh;
                 shop.x+=buildingVo.offsetX*this.gw; */


                this.testBuilding = shop;
            }
            /* if (tile.index===1)
            {
                tile.index=7;
                this.testBuilding=this.placeBuilding(tile.x,tile.y,"buildings","bakery.png");
            } */

            /*   if (tile.index === 18) {
                  tile.index = 7;
                  let tree: GameObjects.Sprite = this.placeBuilding(tile.x, tile.y, "buildings", "tree.png");
                  tree.setOrigin(0, 1);
              } */

        })
    }
    worldClicked(p: Phaser.Input.Pointer) {
        if (this.clickLock === true) {
            return;
        }
        const { worldX, worldY } = p

        //const startVec = this.layer1.worldToTileXY(this.layer1.x, this.layer1.y)
        const targetVec = this.layer1.worldToTileXY(worldX, worldY);
        ////////console.log(targetVec);

        let xx: number = Math.round(targetVec.x) - 1;
        let yy: number = Math.round(targetVec.y);

        let tile = this.layer1.getTileAt(xx, yy);
        console.log(tile);

        //   tile.alpha=0.5;
        //////console.log(xx, yy);
        let key: string = xx.toString() + "-" + yy.toString();

        if (this.gm.locations.has(key)) {
            let locVo: LocVo = this.gm.locations.get(key);

            let locName: string = locVo.name;
            ////console.log(locName);

            /* let locVo2:LocVo=this.gm.buildings.get(locName);
            //////console.log(locVo2); */
            if (locVo.locked === true) {
                this.clickLock = true;
                this.mb.showMessage("That building is locked!");
            }
            else {
                this.gm.currentBuilding = locName;
                this.gm.currentLoc = locVo;
                this.fadeScene();
                return;
            }
        }
        else {
            //////console.log("nothing");

            let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(tile.x - 1, tile.y);
            this.avatarTarget = new PosVo(p.x, p.y);
            this.doWalk();
            //////console.log(pos);
            // this.cameras.main.setScroll(this.gw/2+pos.x,this.gh/2+pos.y);
            // this.layer1.setPosition(pos.x,pos.y,0,0);
        }
    }
    public placeItem(xx: number, yy: number, key: string) {
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx + 1, yy);

        let item: GameObjects.Image = this.add.image(pos.x, pos.y, key);
        this.offMap.add(item);
        return item;
    }
    public placeBuilding(tile: Phaser.Tilemaps.Tile, buildingVo: BuildingVo) {
        let xx: number = tile.x;
        let yy: number = tile.y;
        let key: string = buildingVo.key;
        //console.log("key=" + key);

        let scale: number = buildingVo.scale;
        let frame: string = buildingVo.frame + ".png";
        //
        //
        //
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx + 1, yy);
        let item: Phaser.Physics.Arcade.Sprite = this.physics.add.sprite(pos.x, pos.y, key);

        item.y += buildingVo.offsetY * this.gh;
        item.x += buildingVo.offsetX * this.gw;

        let ww: number = this.layer1.tilemap.tileWidth * buildingVo.scale;

        item.displayWidth = ww;
        item.scaleY = item.scaleX;
        item.body.bounce.x = 1;
        item.body.bounce.y = 1;

        // Align.scaleToGameW(item, 0.2, this);
        item.setFrame(frame);
        //this.offMap.add(item);
        this.buildingsGroup.add(item);
        item.setImmovable(true);
        item.body.setSize(item.displayWidth * buildingVo.collideWidth, item.displayHeight * buildingVo.collideHeight);
        item.setDepth(item.y);
        return item;
    }
    private placeAnim(xx: number, yy: number, key: string) {
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx, yy);
        let tile: Phaser.Tilemaps.Tile = this.layer1.getTileAt(xx, yy);

        let item: Phaser.Physics.Arcade.Sprite = this.physics.add.sprite(pos.x - tile.width / 2, pos.y + tile.height / 3, key);//.setOrigin(0,0);
        Align.scaleToGameW(item, 0.15, this);
        this.buildingsGroup.add(item);

        let animKey: string = key + "play";
        item.play(animKey);
        item.body.setSize(item.displayWidth / 2, item.displayHeight / 2);
        item.setDepth(item.y);
        item.setImmovable(true);
        //this.offMap.add(item);

        return item;
    }
    private placeMoveObj(xx: number, yy: number, scale: number, key: string) {
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx, yy);
        let tile: Phaser.Tilemaps.Tile = this.layer1.getTileAt(xx, yy);

        let item: Phaser.Physics.Arcade.Sprite = this.physics.add.sprite(pos.x - tile.width / 2, pos.y + tile.height / 3, key);//.setOrigin(0,0);
        Align.scaleToGameW(item, scale, this);

        item.setBounce(1.2, 1.2);
        item.setCollideWorldBounds(true, 1, 1);
        // item.setGravity(0.1,0.1);

        item.body.setSize(item.displayWidth / 2, item.displayHeight / 2);
        item.setDepth(item.y);
        this.moveGroup.add(item);
        return item;
    }
    private placePickUp(xx: number, yy: number, scale: number, key: string) {
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx, yy);
        let tile: Phaser.Tilemaps.Tile = this.layer1.getTileAt(xx, yy);

        //let item: Phaser.Physics.Arcade.Sprite = this.physics.add.sprite(pos.x - tile.width / 2, pos.y + tile.height / 3, key);//.setOrigin(0,0);
        let item: Phaser.Physics.Arcade.Sprite = this.physics.add.sprite(pos.x, pos.y, key);//.setOrigin(0,0);
       
        Align.scaleToGameW(item, scale, this);


        item.body.setSize(item.displayWidth / 2, item.displayHeight / 2);
        item.setDepth(item.y);
        this.pickupGroup.add(item);
        return item;
    }
    placeOnMap(xx: number, yy: number, item: GameObjects.Sprite) {
        let pos: Phaser.Math.Vector2 = this.layer1.tileToWorldXY(xx, yy);
        //let tile:Phaser.Tilemaps.Tile=this.layer1.getTileAt(xx,yy);
        item.x = pos.x;
        item.y = pos.y;

    }
    getBuildingPosition(building: string) {
        if (!this.gm.buildings.has(building)) {
            return new LocVo(0, 0, 0, "", "");
        }
        let locVo: LocVo = this.gm.buildings.get("building");
        return locVo;
    }
    fadeScene() {
        this.offMap.setVisible(false);
        this.buildingsGroup.setAlpha(0.1);

        this.tweens.add({ targets: [this.layer1], duration: 1000, alpha: 0.1, onComplete: this.fadeDone.bind(this) });
    }
    fadeDone() {
        const avatarPos = this.layer1.worldToTileXY(this.avatar.x, this.avatar.y);
        //console.log(avatarPos);
        let xx: number = Math.round(avatarPos.x);
        let yy: number = Math.round(avatarPos.y);

        this.gm.startX = xx;
        this.gm.startY = yy;

        this.scene.start("SceneInside");
    }


    avatarWalk() {
        if (this.isWalking === true) {
            return;
        }
        this.isWalking = true;
        //
        //
        //
        switch (this.avatarFace) {
            case 1:
                this.avatar.play("walkleftup");
                break;

            case 2:
                this.avatar.play("walkright");
                break;

            case 3:
                this.avatar.play("walkleft");
                break;

            case 4:
                this.avatar.play("walkrightup");
                break;
        }
    }
    avatarIdle() {
        this.avatar.stop();
        switch (this.avatarFace) {
            case 1:
                this.avatar.setFrame("idle_left_up.png");
                break;

            case 2:
                this.avatar.setFrame("idle_right.png");
                break;

            case 3:
                this.avatar.setFrame("idle_left.png");
                break;

            case 4:
                this.avatar.setFrame("idle_right_up.png");
                break;
        }
        //  this.avatar.setVelocity(0, 0);
    }

    doWalk() {
        if (this.avatarTarget) {
            if (this.avatarTarget.x != -1 && this.avatarTarget.y != -1) {
                let avx2: number = this.avatar.x - this.cameras.main.scrollX;
                let avy2: number = this.avatar.y - this.cameras.main.scrollY;

                ////console.log(avx2,avy2);

                this.block.x = this.avatarTarget.x + this.cameras.main.scrollX;
                this.block.y = this.avatarTarget.y + this.cameras.main.scrollY;

                let xDir: number = ((this.avatarTarget.x - avx2) < 0) ? -1 : 1;
                let yDir: number = ((this.avatarTarget.y - avy2) < 0) ? -1 : 1;

                if (xDir == -1 && yDir == -1) {
                    //////console.log("left");
                    this.avatarFace = 1;
                }
                if (xDir == 1 && yDir == 1) {
                    //////console.log("right");
                    this.avatarFace = 2;
                }
                if (xDir == -1 && yDir == 1) {
                    this.avatarFace = 3;
                }
                if (xDir == 1 && yDir == -1) {
                    this.avatarFace = 4;
                }

                //console.log(xDir, yDir);
                this.isWalking = false;
                this.avatarWalk();
                this.physics.moveToObject(this.avatar, this.block, 500);

            }
        }
    }
    checkPos() {
        if (this.avatarTarget) {
            if (this.avatarTarget.x != -1 && this.avatarTarget.y != -1) {
                let avx2: number = this.avatar.x - this.cameras.main.scrollX;
                let avy2: number = this.avatar.y - this.cameras.main.scrollY;
                //
                //
                //
                let distX: number = Math.abs(this.avatar.x - this.block.x);
                let distY: number = Math.abs(this.avatar.y - this.block.y);
                // //console.log(distX, distY);

                if (distX < 5 && distY < 5) {
                    this.avatarTarget.x = -1;
                    this.avatar.setVelocity(0, 0);
                    this.avatarIdle();
                    this.isWalking = false;
                }
            }

        }
    }
    private getSgn(num: number) {
        if (num === 0) {
            return 0;
        }
        if (num < -1) {
            return -1;
        }
        return 1;
    }
    update(time: number, delta: number) {

        this.checkPos();
        this.avatar.setDepth(this.avatar.y);

    }
}