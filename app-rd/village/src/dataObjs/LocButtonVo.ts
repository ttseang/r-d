export class LocButtonVo
{
    public key:string;
    public type:string;
    public file:string;
    
    constructor(key:string,type:string,file:string)
    {
        this.key=key;
        this.type=type;
        this.file=file;
    }
}