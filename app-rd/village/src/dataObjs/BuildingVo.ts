export class BuildingVo
{
    public key:string;
    public frame:string;
    public offsetX:number;
    public offsetY:number;
    public scale:number;
    public tile:number;
    public collideWidth:number;
    public collideHeight:number;

    constructor(key:string,frame:string,offsetX:number,offsetY:number,scale:number=0.7,tile:number=17,collideWidth:number=1,collideHeight:number=1)
    {
        this.key=key;
        this.frame=frame;
        this.offsetX=offsetX;
        this.offsetY=offsetY;
        this.scale=scale;
        this.tile=tile;
        this.collideWidth=collideWidth;
        this.collideHeight=collideHeight;
    }
}