import { BuildingVo } from "../dataObjs/BuildingVo";
import { LocVo } from "../dataObjs/LocVo";

let instance: GM = null;

export class GM {
    public isMobile: boolean = false;
    public isPort: boolean = false;
    public isTablet: boolean = false;

    public locations: Map<string, LocVo> = new Map<string, LocVo>();
    public buildings: Map<string, LocVo> = new Map<string, LocVo>();
    public mapSprites:Map<number,BuildingVo>=new Map<number,BuildingVo>();

    public currentBuilding:string="";
    public currentLoc:LocVo;
    public lastUnlocked:LocVo;

    public startX:number=3;
    public startY:number=7;
    public device:string="desktop";
    public orientation:string="";
    
    constructor() {
      
        this.mapSprites.set(1,new BuildingVo("buildings","bakery",0,-0.07,0.75,17,0.8,0.6));
        this.mapSprites.set(11,new BuildingVo("buildings","pizza1",0,-0.05,0.9,17,0.8,0.5));
        this.mapSprites.set(3,new BuildingVo("buildings","food",-0.01,-0.05,0.6,17,0.9,0.8));


        this.mapSprites.set(2,new BuildingVo("buildings","coffee",0,-0.07,0.7,17,0.8,0.6));
        this.mapSprites.set(15,new BuildingVo("buildings","cola",0,-0.025,0.7,17,0.5,0.5));
        this.mapSprites.set(14,new BuildingVo("buildings","bshop",0.03,-0.05,0.7,17,0.8,0.6));
        this.mapSprites.set(5,new BuildingVo("buildings","gen_store",0.0,-0.05,0.7,17,0.8,0.5));
        this.mapSprites.set(12,new BuildingVo("buildings","pizza2",-0.025,-0.06,0.7,17,0.8,0.5));
      


        this.mapSprites.set(18,new BuildingVo("buildings","tree",0,0,0.8,7,0.5,0.5));

       
        this.mapSprites.set(123,new BuildingVo("buildings","garage",0,-0.1,0.7,17,0.5,0.5));
        this.mapSprites.set(124,new BuildingVo("buildings","hospital",0,-0.1,1,17,0.5,0.5));
        this.mapSprites.set(122,new BuildingVo("buildings","church",0.04,-0.05,0.7,17,0.8,0.7));
        this.mapSprites.set(121,new BuildingVo("buildings","autoparts",0.04,-0.05,0.7,17,0.6,0.7));
        this.mapSprites.set(125,new BuildingVo("buildings","movies",0,-0.08,0.8,17,0.6,0.7));
        this.mapSprites.set(128,new BuildingVo("buildings","snackshack",-0.04,-0.1,0.5,17,0.8,0.7));
        this.mapSprites.set(126,new BuildingVo("buildings","library",0,-0.1,0.7,17,0.6,0.7));
        this.mapSprites.set(127,new BuildingVo("buildings","police",0,-0.1,0.7,17,0.6,0.7));

        //
        //
        //
        this.mapSprites.set(247,new BuildingVo("parkbuildings","forest",0,-0.1,0.7,7,0.5,0.5));
        this.mapSprites.set(246,new BuildingVo("parkbuildings","forest2",0,-0.1,0.7,7,0.5,0.5));
        this.mapSprites.set(258,new BuildingVo("parkbuildings","wat",0,-0.1,0.7,59,0.25,0.25));
        this.mapSprites.set(244,new BuildingVo("parkbuildings","bandstand",0,-0.2,1.2,7,0.1,0.1));
        this.mapSprites.set(257,new BuildingVo("parkbuildings","planter",0,-0.1,1,7,0.5,0.2));
        this.mapSprites.set(239,new BuildingVo("parkbuildings","angel",0,-0.1,0.8,48,2,0.5));


       // this.mapSprites.set(259,new BuildingVo("parkbuildings","water",0,-0.1,0.7,7,0.5,0.5));


    }
    static getInstance() {
        if (instance === null) {
            instance = new GM();
        }
        return instance;
    }

    public unlockBuilding(building: string) {
        console.log("unlock "+building);
        if (this.buildings.has(building)) {
            let locVo: LocVo = this.buildings.get(building);
            let key: string = locVo.x.toString() + "-" + locVo.y.toString();

            locVo.locked = false;

            this.locations.set(key, locVo);
            this.buildings.set(building, locVo);
            this.lastUnlocked=locVo;
        }
    }
}