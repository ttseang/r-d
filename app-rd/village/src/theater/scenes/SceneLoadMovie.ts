
//import { Sound } from "phaser";
import MainStorage from "../classes/MainStorage";
import Align from "../../util/align";
import { BaseScene } from "../../scenes/BaseScene";

export class SceneLoadMovie extends BaseScene {

    private loadingText: Phaser.GameObjects.Text | undefined;
    private ms: MainStorage = MainStorage.getInstance();

    constructor() {
        super("SceneLoadMovie");
    }
    preload() {
        super.create();

        this.sound.once(Phaser.Sound.Events.DECODED_ALL, this.soundDecoded.bind(this));


        this.loadingText = this.add.text(0, 0, "Loading Media 0%", { fontSize: "80px", color: "#ffffff" }).setOrigin(0.5, 0.5);
        Align.center(this.loadingText, this);
        this.load.on('progress', this.onProgress.bind(this));
        // this.load.once(Phaser.Sound.Events.DECODED_ALL,this.soundDecoded.bind(this));

        //this.load.image("holder", "common/holder.jpg");
        this.load.image("tscreen","./assets/movies/common/tscreen.jpg");
        this.load.image("rect", "./assets/movies/common/rect.jpg");
        this.load.image("face", "./assets/movies/common/face.png");
        this.load.image("bracket", "./assets/movies/common/corner.png");
        this.load.image("hand", "./assets/movies/common/hand.png");
        this.load.image("punch", "./assets/movies/common/punch.png");
        this.load.image("thumbsUp", "./assets/movies/common/thumbsUp.png");
        this.load.image("circle", "./assets/movies/common/circle.png");
        this.load.image("heart", "./assets/movies/common/heart.png");
        this.load.image("balloon", "./assets/movies/common/balloon.png");
        this.load.image("rocket", "./assets/movies/common/rocket2.png");
        this.load.image("rays3", "./assets/movies/common/rays3.png");
        this.load.image("donut", "./assets/movies/common/donut.png");
        this.load.image("sunrise", "./assets/movies/common/sunrise.png");

        this.load.atlas("halfRays", "./assets/movies/common/rays_half.png", "./assets/movies/common/rays_half.json");
        this.load.atlas("brackets", "./assets/movies/common/bracket.png", "./assets/movies/common/bracket.json");
        this.load.atlas("bubblePop", "./assets/movies/common/bubblePop.png", "./assets/movies/common/bubblePop.json");
        this.load.atlas("starBurst", "./assets/movies/common/starburst.png", "./assets/movies/common/starburst.json");
        this.load.atlas("fullRays", "./assets/movies/common/rays.png", "./assets/movies/common/rays.json");
        this.load.atlas("raysPaper", "./assets/movies/common/raysPaper.png", "./assets/movies/common/raysPaper.json");
        this.load.atlas("sparks", "./assets/movies/common/sparks.png", "./assets/movies/common/sparks.json");
        this.load.atlas("radial", "./assets/movies/common/radial.png", "./assets/movies/common/radial.json");

        this.load.image("paperTop", "./assets/movies/common/paperTop.png");
        this.load.image("paperBottom", "./assets/movies/common/paper1.png");

        if (this.ms.loadPageAudio == true) {
            for (let i: number = 1; i < this.ms.numberOfPages + 1; i++) {
                this.load.audio("paudio"+i.toString(), "assets/movies/pageAssets/" + this.ms.book + "/pageAudio/" + i.toString() + ".mp3");
            }
        }
        if (this.ms.loadPageImages == true) {
            for (let i: number = 1; i < this.ms.numberOfPages + 1; i++) {
                this.load.image("pimage" + i.toString(), "assets/movies/pageAssets/" + this.ms.book + "/pageImages/" + i.toString() + ".jpg");
            }
        }
        for (let i:number=0;i<this.ms.customImages.length;i++)
        {
            this.load.image(this.ms.customImages[i].key,"assets/movies/pageAssets/" + this.ms.book + "/images/"+this.ms.customImages[i].path);
        }
        for (let i:number=0;i<this.ms.customAudio.length;i++)
        {
            console.log(this.ms.customAudio[i]);

            this.load.audio(this.ms.customAudio[i].key,"assets/movies/pageAssets/" + this.ms.book + "/audio/"+this.ms.customAudio[i].path);
        }
        
        //this.load.audio("FreshAndCool", "assets/movies/pageAssets/icecream/audio/backgroundMusic/FreshAndCool.mp3");

        //this.load.image("bg1", "./common/bg1.jpg");
    }
    onProgress(per: number) {
        per = Math.floor(per * 100);
        if (this.loadingText) {
            this.loadingText.setText(per.toString() + "%");
        }

    }
    create() {
        //let audio1:Sound.BaseSound=this.sound.add("audio1");
        ////  audio1.once(Phaser.Sound.Events.DECODED, this.soundDecoded.bind(this));
        // let am:Sound.WebAudioSoundManager=new Sound.WebAudioSoundManager(this.sys.game);
        // am.once(Phaser.Sound.Events.DECODED, this.soundDecoded.bind(this));

        setTimeout(() => {
            this.scene.start("SceneMovie");
        }, 3000);
    }
    soundDecoded() {

    }
    update() {

    }
}
export default SceneLoadMovie;