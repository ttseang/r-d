
import { GameObjects } from "phaser";
import { AudioPlayer } from "../classes/AudioPlayer";
import { LayoutClock } from "../classes/LayoutClock";
import { MovieLayoutManager } from "../classes/MovieLayoutManager";
import MainStorage from "../classes/MainStorage";
//import { TestFile } from "../classes/testFile";
import { AttsVo } from "../mdataObjs/AttsVo";
import { KeyframeVo } from "../mdataObjs/KeyframeVo";
import Align from "../../util/align";
import { PageLoader } from "../../util/pageLoader";
import { BaseScene } from "../../scenes/BaseScene";
import { Button } from "../../classes/comps/Button";

export class SceneMovie extends BaseScene {

    private ms: MainStorage = MainStorage.getInstance();
    private layoutClock: LayoutClock | undefined;
    private layoutManager: MovieLayoutManager | undefined;
    private audioPlayer: AudioPlayer;
    private bgImage: GameObjects.Sprite | undefined;
    private testImage: GameObjects.Sprite | undefined;
    private btnStart: Button;

    constructor() {
        super("SceneMovie");

        //load file
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        //let tf:TestFile=new TestFile();       
        this.audioPlayer = AudioPlayer.getAudioPlayer(this);
    }

    preload() {

    }
    create() {
        super.create();
        this.makeGrid(44, 44);
       

        this.game.canvas.id = "thecanvas";

        this.game.events.removeAllListeners();


        this.game.events.addListener(Phaser.Core.Events.BLUR, this.onBlur.bind(this));
        this.game.events.addListener(Phaser.Core.Events.CONTEXT_LOST, this.onBlur.bind(this));
        this.game.events.addListener(Phaser.Core.Events.FOCUS, this.onFocus.bind(this));
        this.game.events.addListener(Phaser.Core.Events.CONTEXT_RESTORED, this.onFocus.bind(this));

        this.layoutManager = new MovieLayoutManager(this);
        this.layoutClock = LayoutClock.getInstance(this, this.layoutManager);
        this.layoutClock.pageCallback = this.nextPage.bind(this);
        this.layoutClock.reset();




        this.bgImage = this.add.sprite(0, 0, "tscreen");
        Align.scaleToGameW(this.bgImage, 1, this);
        //this.grid.placeAtIndex(60,this.bgImage,true);
        Align.center(this.bgImage, this);
       // this.grid.showNumbers();

        // this.testImage=this.add.sprite(0,0,"circle");
        //this.grid.placeAt(28,0.5,this.testImage);

        let pl: PageLoader = new PageLoader(this.placeStatics.bind(this));
        pl.loadPage(this.ms.currentPage + ".json");
        
        let btnExit:GameObjects.Image=this.add.image(0,0,"btnExit");
        Align.scaleToGameW(btnExit,0.05,this);
        this.grid.placeAtIndex(1500,btnExit);
        btnExit.setInteractive();
        btnExit.on('pointerdown',()=>{
            this.layoutManager.clearAll();
            this.audioPlayer.sound.stop();
            this.audioPlayer.backgroundSound.stop();
            this.scene.start("SceneMap");
        });
        // this.placeImage("face",60,0.1);

        // this.startClock();

        window.onresize = this.onResize.bind(this);
       // window['scene'] = this;
    }
    onResize() {
        let width: number = window.innerWidth / window.devicePixelRatio;
        let height: number = window.innerHeight / window.devicePixelRatio;

        console.log(width, height);

        this.scale.resize(width, height);
        // this.scene.game.scale.resize(width, height);
        this.cameras.resize(width, height);
        this.gw = width;
        this.gh = height;

        //  this.sys.game.config.width = width;
        //  this.sys.game.config.height = height;
        this.grid.hide();
        this.makeGrid(44, 44);
        // this.grid.show();
        //  this.grid.placeAt(28,0.5,this.testImage);
        //   console.log(this.grid.cw,this.ch);
        this.layoutManager.onResize();
    }
    placeStatics() {
        console.log(this.ms.statics);

        for (let i: number = 0; i < this.ms.statics.length; i++) {
            let attVo: AttsVo = this.ms.statics[i];
            let kf: KeyframeVo = new KeyframeVo("static" + i.toString(), 0, "update");
            kf.atts = attVo;
            // console.log(kf);
            this.layoutManager.placeObj(kf);
        }
        if (this.ms.movieStarted == true) {
            this.startClock();
        }
        else {
            this.btnStart = new Button(this, "button", "Start Movie");
            this.btnStart.setCallback(this.startClock.bind(this));
            Align.center(this.btnStart, this);
            this.ms.movieStarted=true;
        }
    }
    startClock() {
        if (this.btnStart) {
            this.btnStart.visible = false;
        }
        if (this.ms.backgroundMusic) {
            this.audioPlayer.setBackgroundMusic(this.ms.backgroundMusic);
        }
        if (this.layoutClock) {
            this.layoutClock.start();
        }
    }
    onBlur() {
        console.log("blurred");

        let audioPlayer: AudioPlayer = AudioPlayer.getAudioPlayer(this);
        audioPlayer.pause();
        if (this.layoutClock) {
            this.layoutClock.paused = true
        }
    }
    onFocus() {
        console.log("focus")
        let audioPlayer: AudioPlayer = AudioPlayer.getAudioPlayer(this);
        audioPlayer.resume();
        if (this.layoutClock) {
            this.layoutClock.paused = false;
        }
    }
    nextPage() {
        this.layoutManager.clearAll();
        this.scene.restart();
    }
    update(time: number, delta: number) {
        if (this.layoutClock) {
            this.layoutClock.tick(time, delta);
        }
    }
}
export default SceneMovie;