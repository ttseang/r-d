import IBaseScene from "../../interfaces/IBaseScene";
import { Align } from "../../util/align";

export class Brackets
{
    public scene:Phaser.Scene;
    private bscene:IBaseScene;
    private scale:number=0;
    private dir:number=1;
    private count:number=0;

    private corners: Phaser.GameObjects.Sprite[] = [];
    private timer1:Phaser.Time.TimerEvent | undefined;
    
    private t:Phaser.GameObjects.Text;
    constructor(bscene:IBaseScene,t:Phaser.GameObjects.Text)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.t=t;      
    }
   public makeBrackets()
    {
        
        let t:Phaser.GameObjects.Text=this.t;

        for (let i: number = 0; i < 4; i++) {
            let corner: Phaser.GameObjects.Sprite = this.scene.add.sprite(0, 0, "brackets");
            Align.scaleToGameW(corner, 0.025, this.bscene);
            // this.lineUpSpriteToEl(bracket,el);
            this.corners.push(corner);
        }

        /* let bufferX:number=t.displayWidth*0.125;
        let bufferY:number=t.displayHeight*0.01; */

        let bufferX:number=5;
        let bufferY:number=0;

        this.corners[0].x = t.x+bufferX;
        this.corners[0].y = t.y+bufferY;
        //
        //
        this.corners[1].x = t.x + t.displayWidth-bufferX;
        this.corners[1].y = t.y+bufferY;
        this.corners[1].flipX = true;
        //
        //
        this.corners[2].x = t.x+bufferX;
        this.corners[2].y = t.y + t.displayHeight-bufferY;
        this.corners[2].flipY = true;
        //
        //
        this.corners[3].x = t.x + t.displayWidth-bufferX;
        this.corners[3].y = t.y + t.displayHeight-bufferY;
        this.corners[3].flipX = true;
        this.corners[3].flipY = true;
    }
    animate()
    {
        for (let i:number=0;i<4;i++)
        {
            this.corners[i].play('bracketsPlay');
            
        }
        //this.timer1=this.scene.time.addEvent({ delay: 200, callback: this.pulse.bind(this), loop: true });
       //this.scene.tweens.add({targets: this.corners[i],duration: 1500,scaleY:0.2,scaleX:0.2,yoyo:true,onComplete:this.destroy.bind(this)});
    }
    pulse()
    {
        for (let i:number=0;i<4;i++)
        {
            this.corners[i].scaleX-=(0.03*this.dir);
            this.corners[i].scaleY-=(0.03*this.dir);
        }
        this.count++;
        if (this.count/3===Math.floor(this.count/3))
        {
            this.dir=-this.dir;
            if (this.count===30)
            {
                this.destroy();
            }
        }
    }
    destroy()
    {
        for (let i:number=0;i<4;i++)
        {
            this.corners[i].destroy();
        }
    }
}