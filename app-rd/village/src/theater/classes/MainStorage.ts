import { AttsVo } from "../mdataObjs/AttsVo";
import { FileVo } from "../mdataObjs/FileVo";
import { KeyframeVo } from "../mdataObjs/KeyframeVo";



export class MainStorage {
   
    private static instance: MainStorage;
    public gameConfig: any;
    //
    //

    public selectedFile: number = 0;
    public prevData: string = "";

    public makeNewFlag: boolean = true;
    public prevMode: boolean = false;
    public keyFrames: KeyframeVo[] = [];
    public statics:AttsVo[]=[];
    public currentKeyFrame: KeyframeVo | undefined;
    public currentPage: string = "icecream1";
    public backgroundMusic: string = "";
    public backgroundImage: string = "";
    public syncToAudio:boolean=false;
    public book:string="icecream";
    public numberOfPages:number=0;
    public loadPageImages:boolean=false;
    public loadPageAudio:boolean=false;

    public customImages:FileVo[]=[];
    public customAudio:FileVo[]=[];
    
    public movieStarted:boolean=false;

    public assetFolder:string="./assets/movies/";

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor() {
        
    }
    static getInstance(): MainStorage {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new MainStorage();
        }
        return this.instance;
    }
    public reset()
    {
        
    }
    public getNextFrame() {
        this.currentKeyFrame = this.keyFrames.shift();
        return this.currentKeyFrame;
    }
    public addKeyFrame(k: KeyframeVo) {
        this.keyFrames.push(k);
        // this.keyFrames.sort((a:KeyframeVo,b:KeyframeVo)=>(a.start<b.start)?1:0);
    }
}
export default MainStorage;