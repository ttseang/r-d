import { PopperVo } from "../mdataObjs/PopperVo";
import IBaseScene from "../../interfaces/IBaseScene";

export class Popper extends Phaser.GameObjects.Sprite
{
    public scene:Phaser.Scene;
    public type2:string;
    public callback:Function=()=>{};
    public clickCount:number=0;
    public dataProvider:PopperVo | undefined;
    
    constructor(bscene:IBaseScene,key:string,type2:string,callback:Function)
    {
        super(bscene.getScene(),0,0,key);
        this.scene=bscene.getScene();
        this.scene.add.existing(this);
        this.callback=callback;
        this.type2=type2;

        this.setInteractive();
        this.on('pointerdown',this.clickMe.bind(this));
    }
    clickMe()
    {
        this.callback(this);
    }
}