export class FileVo
{
    public key:string;
    public path:string;

    constructor(key:string,path:string)
    {
        this.key=key;
        this.path=path;
    }
}