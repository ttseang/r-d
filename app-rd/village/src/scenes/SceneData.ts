import { GM } from "../classes/GM";
import { LocButtonVo } from "../dataObjs/LocButtonVo";
import { LocVo } from "../dataObjs/LocVo";
import { BaseScene } from "./BaseScene";



export class SceneData extends BaseScene
{
    private gm:GM=GM.getInstance();

    constructor()
    {
        super("SceneData");
    }
    preload()
    {     

        let folder:string="./assets/village/";

        this.load.image("roadtiles2",folder+"roadtiles2.png");
        this.load.image("lock", folder+"lock.png");
        this.load.image("mback", folder+"ui/mbback.jpg");
        this.load.image("holder", folder+"ui/holder.jpg");
        this.load.image('button', folder+"ui/buttons/1/1.png");
        this.load.image("village", folder+"village.png");
        this.load.image("village2", folder+"village2.png");
        
        this.load.atlas("buildings",folder+"buildings/buildings2.png",folder+"buildings/buildings2.json");
        this.load.atlas("parkbuildings", folder+"buildings/parkbuildings.png",folder+"buildings/parkbuildings.json");
        

        this.load.image("park",folder+"park.png");

        this.load.tilemapTiledJSON('map', folder+'village.json');
        this.load.image("bakery", folder+"inside/bakery.png");
        this.load.image("coffee", folder+"inside/coffee.png");
        this.load.image("pizza", folder+"inside/pizza.png");
        this.load.image("movies", folder+"inside/movies.png");
        this.load.image("food", folder+"inside/food.png");
        this.load.image("bshop", folder+"inside/bshop.png");
        this.load.atlas("fountain",folder+"animations/fountain1.png",folder+"animations/fountain1.json");
        this.load.image("hand",folder+"hand.png");

        this.load.atlas("avatar",folder+"avatars/avatar1.png",folder+"avatars/avatar1.json");
        this.load.atlas("trash",folder+"trash.png",folder+"trash.json");
        
       
        this.load.image("greycity",folder+"inside/greycity.jpg");
        this.load.image("ball",folder+"ball.png");
        //
        //
        //
        this.load.image("btnCookie",folder+"buttons/btnCookie2.png");
        this.load.image("btnPizza",folder+"buttons/btnPizza.png");
        this.load.image("btnMovie",folder+"buttons/btnMovie.png");
        this.load.image("btnExit",folder+"buttons/btnExit.png");

        //
        //
        //
        let faceFolder:string="./assets/face/";
        this.load.image("face", faceFolder+"face3.png");
        this.load.image("eye", faceFolder+"eye.png");
        this.load.image("closed", faceFolder+"closed.png");
        this.load.image("eyeshade",faceFolder+"eyeshade.png");
        //
        //
        //
        this.load.image("lbrow", faceFolder+"lbrow.png");
        this.load.image("rbrow", faceFolder+"rbrow.png");

        this.load.atlas("mouth", faceFolder+"mouth.png", faceFolder+"mouth.json");

        this.load.image("hand1", faceFolder+"hand1.png");
        this.load.image("hand2", faceFolder+"hand2.png");

        this.load.atlas("hands", faceFolder+"hands.png", faceFolder+"hands.json");
       
        //
        //
        //
        this.load.image("bubbletop",faceFolder+"bubbletop.png");
        
    }
    create()
    {
        
        this.getLocations();
    }
    
    getLocations()
    {
        fetch("./assets/village/locations.json")
        .then(response => response.json())
        .then(data => this.gotLocations({ data }));
    }
    gotLocations(data:any)
    {
        console.log(data);
        let locations:any=data.data.locations;
        console.log(locations);
        for (let i:number=0;i<locations.length;i++)
        {
            let loc:any=locations[i];
        
            let locVo:LocVo=new LocVo(parseInt(loc.level),parseInt(loc.x),parseInt(loc.y),loc.name,loc.unlock,loc.message);

            if (loc['button'])
            {
                let button:any=loc.button;
                let locButton:LocButtonVo=new LocButtonVo(button.key,button.type,button.file);
                locVo.button=locButton;
            }

            let key:string=locVo.x.toString()+"-"+locVo.y.toString();
            this.gm.locations.set(key,locVo);            
            this.gm.buildings.set(locVo.name,locVo);
        }

        this.gm.unlockBuilding("movies");
        this.gm.unlockBuilding("bakery");
        
        this.scene.start("SceneMap");
    }
}