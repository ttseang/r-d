export class SposVo
{
    public dkey:string;
    public x:number;
    public y:number;
    public scale:number;

    constructor(dkey:string,x:number,y:number,scale:number)
    {
        this.dkey=dkey;
        this.x=x;
        this.y=y;
        this.scale=scale;
    }
}