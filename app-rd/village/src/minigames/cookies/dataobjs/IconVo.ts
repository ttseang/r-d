export class IconVo
{
    public name:string;
    public frame:string;
    public action:string;
    public scales:string;

    constructor(name:string,frame:string,action:string="",scales:string="")
    {
        this.name=name;
        this.frame=frame;
        this.action=action;
        this.scales=scales;
    }
}