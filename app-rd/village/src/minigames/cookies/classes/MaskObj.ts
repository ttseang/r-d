import { GameObjects } from "phaser";
import IBaseScene from "../../../interfaces/IBaseScene";

export class MaskObj
{
    private bscene:IBaseScene;
    public scene:Phaser.Scene;
    public backImage:GameObjects.Sprite;
    public maskImage:GameObjects.Sprite;

    constructor(bscene:IBaseScene,backKey:string,maskKey:string)
    {
     //   super(bscene.getScene());
        
        this.bscene=bscene;
        this.scene=bscene.getScene();

        this.backImage=this.scene.add.sprite(0,0,backKey);
        this.maskImage=this.scene.add.sprite(0,0,maskKey);
        
     

        //this.add(this.backImage);
      //  this.add(this.maskImage);

       //
        //this.maskImage.visible=false;

       // this.scene.add.existing(this);

       // window['mo']=this;
    }
    public setPos(xx:number,yy:number)
    {
        this.maskImage.x=xx;
        this.maskImage.y=yy;
        this.backImage.x=xx;
        this.backImage.y=yy;
    }
    doMask()
    {
        this.backImage.setMask(new Phaser.Display.Masks.BitmapMask(this.scene, this.maskImage));
        this.maskImage.visible=false;
    }
}