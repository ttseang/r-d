import { GameObjects } from "phaser";
import IBaseScene from "../../../interfaces/IBaseScene";
import Align from "../../../util/align";
import { Controller } from "./Controller";
import { CookieModel } from "./CookieModel";
import { Dragger } from "./Dragger";
import { LayoutBuilder } from "./layoutBuilder";
import { LayoutManager } from "./layoutManager";
import { ScratchCard } from "./ScratchCard";

export class Table 
{
    private bscene:IBaseScene;
    private scene:Phaser.Scene;

    private baseObj:GameObjects.Sprite;
    public baseBack:GameObjects.Image;

    private lm:LayoutManager;
    private lastScratch:ScratchCard;

    private menuButton:GameObjects.Sprite;
    private controller:Controller=Controller.getInstance();
    private gm:CookieModel=CookieModel.getInstance();

    constructor(bscene:IBaseScene,lm:LayoutManager,baseKey:string,frame:string)
    {
        this.bscene=bscene;
        this.scene=bscene.getScene();
        this.lm=lm;

       /*  let layoutBuilder:LayoutBuilder=new LayoutBuilder(this.bscene,this.lm);
        layoutBuilder.build("table"); */
         this.baseBack=this.scene.add.image(0,0,"circle");
         let scale2:number=this.lm.getDynamicScale(this.gm.baseBackScales);
       /*   scale2=Math.floor(scale2*100)/100;
         console.log(scale2); */
        Align.scaleToGameW(this.baseBack,scale2,this.bscene);
      //  Align.scaleToGameW(this.baseBack,this.lm.getScale("baseBack"),this.bscene);
        this.baseObj=this.scene.add.sprite(0,0,baseKey);
        if (frame!=="")
        {
            this.baseObj.setFrame(frame);
        }
       
        Align.scaleToGameW(this.baseObj,this.lm.getDynamicScale(this.gm.baseScales),this.bscene);
        
        this.bscene.getGrid().placeAt(5,3,this.baseBack);
        this.bscene.getGrid().placeAt(5,3,this.baseObj);

       
        //
        //
        //
        this.controller.addScratch=this.addScratch.bind(this);
        this.controller.addDrag=this.addDragger.bind(this);
    }
    public addScratch(key:string,frame:string,color:number)
    {
        if (this.lastScratch)
        {
            this.lastScratch.removeInteractive();
        }
        let scratchCard=new ScratchCard(this.bscene,key,frame,color);
        scratchCard.displayWidth=this.baseObj.displayWidth*0.95;
        scratchCard.displayHeight=this.baseObj.displayHeight*0.95;

        scratchCard.x=this.baseObj.x;
        scratchCard.y=this.baseObj.y;
        //
        //
        //
        this.lastScratch=scratchCard;
    }
    public addDragger(key:string,frame:string,scales:string)
    {
        console.log(key,frame,scales);

        let dragger:Dragger=new Dragger(this.bscene,this.lm,this.baseObj.x,this.baseObj.y,key,frame,"circle",scales);
        

    }
}