import { GameObjects } from "phaser";
import { IconVo } from "../../dataObjs/IconVo";
import { PosVo } from "../../../../dataObjs/PosVo";
import IBaseScene from "../../../../interfaces/IBaseScene";
import Align from "../../../../util/align";
import { AlignGrid } from "../../../../util/alignGrid";
import { Controller } from "../Controller";
import { CookieModel } from "../CookieModel";
import { LayoutManager } from "../layoutManager";
import { TabButton } from "./tabButton";
import { TabIcon } from "./tabIcon";

export class TabLine extends Phaser.GameObjects.Container {
    public scene: Phaser.Scene;
    private bscene: IBaseScene;
    private tabs: TabButton[] = [];
    private icons: TabIcon[] = [];
    private back: GameObjects.Image;
    private lm: LayoutManager;
    private gm: CookieModel = CookieModel.getInstance();
    private grid: AlignGrid;
    private doorXDir:number=0;
    private targetX: number=0;

    private controller: Controller = Controller.getInstance();
    

    constructor(bscene: IBaseScene, lm: LayoutManager) {
        super(bscene.getScene());
        this.scene = bscene.getScene();
        this.bscene = bscene;
        this.lm = lm;

        this.back = this.scene.add.image(0, 0, "shelf").setOrigin(0, 0);
        this.back.displayHeight = this.bscene.getH();
        this.back.displayWidth = this.bscene.getW() * lm.getScale("shelfW");
        // Align.scaleToGameH(this.back,1,this.bscene);

        this.back.setTint(0xFFE000);
        //  this.back.alpha=0.5;
        this.add(this.back);
        this.setSize(this.back.displayWidth, this.back.displayHeight);
        console.log(this.displayWidth, this.displayHeight);


      
        //dock to the bottom        
        //this.y=this.bscene.getH()*0.75;
        this.y = this.bscene.getH() / 2 - this.displayHeight / 2;
        this.x = 0;
        this.grid = new AlignGrid(this.bscene, 11, 11, this.back.displayWidth, this.back.displayHeight);
        // this.grid.showPos();


        let arrow: GameObjects.Sprite = this.scene.add.sprite(0, 0, "arrow");
        Align.scaleToGameW(arrow, this.lm.getScale("arrow"), this.bscene);
        arrow.flipX = true;
        arrow.setTint(0xffcc00);

        this.add(arrow);
        this.grid.placeAt(9, 0, arrow);

        arrow.setInteractive();
        arrow.on('pointerdown', () => { this.close() })

        this.scene.add.existing(this);
       // window['shelf'] = this;
        this.x = -this.displayWidth;
        this.targetX=this.x;
        //
        //set controller functions
        //
        this.controller.openSide = this.resetMenu.bind(this);
        this.controller.closeSide = this.close.bind(this);

    }
    public open() {
        //console.log("open");
        this.scene.children.bringToTop(this);
       // this.scene.tweens.add({ targets: this, duration: 1000, x: 0 });
       this.targetX=-1;
       this.doorXDir=1;
    }
    public close() {
        this.targetX=-this.displayWidth;
        this.doorXDir=-1;
       // this.scene.tweens.add({ targets: this, duration: 1000, x: -this.displayWidth });
    }
    private tabPressed(dp: IconVo) {
        let menuArray: string[] = dp.action.split("-");
        console.log(dp.action);
        if (dp.action.startsWith("menu-")) {

            let items: IconVo[] = this.gm.menus.get(menuArray[1]).items;
           // this.close();
           this.makeTabs(items);
                this.open();
           /*  setTimeout(() => {
                this.makeTabs(items);
                this.open();
            }, 1000); */
            return;
        }

        if (dp.action.startsWith("addscratch")) {
            console.log(menuArray[1]);
            let icingFrame:string=this.gm.cookieIndex.toString()+".png";
            this.controller.addScratch(this.gm.scratchKey, icingFrame, menuArray[1]);
            this.close();
            //this.resetMenu();
            return;
        }
        if (dp.action.startsWith("adddrag"))
        {
            this.controller.addDrag(menuArray[1],menuArray[2],dp.scales);
        }
        this.close();

    }
    public resetMenu() {
        this.controller.closeEdit();

        let items: IconVo[] = this.gm.menus.get('main').items;
        this.makeTabs(items);
        this.open();
    }
    public makeTabs(tabArray: IconVo[]) {
        let pos: PosVo[] = this.lm.getTabPos();
        while (this.tabs.length > 0) {
            this.tabs.pop().destroy();
        }
        for (let i: number = 0; i < tabArray.length; i++) {
            let tabButton: TabButton = new TabButton(this.bscene,"icons", this.lm, tabArray[i]);
            tabButton.callback = this.tabPressed.bind(this);
            this.add(tabButton);
            this.tabs.push(tabButton);

            let pos2: PosVo = pos[i];
            this.grid.placeAt(pos2.x, pos2.y, tabButton);
        }
        //this.bringToTop(this.back);
    }
    public updateMe()
    {
        if (this.doorXDir!==0)
        {
            this.x+=this.doorXDir*20;
            if (this.doorXDir==-1)
            {
                if (this.x<this.targetX)
                {
                    this.doorXDir=0;
                }
            }
            else
            {
                if (this.x>this.targetX)
                {
                    this.doorXDir=0;
                }
            }
        }
        else
        {
            this.x=this.targetX;
        }
    }
}