import { CookieModel } from "../classes/CookieModel";
import { FileVo } from "../dataObjs/fileVo";
import { BaseScene } from "../../../scenes/BaseScene";

export class SceneCookieLoad extends BaseScene {

    private cm:CookieModel=CookieModel.getInstance();

    constructor()
    {
        super("SceneCookieLoad");
    }
    preload()
    {
        console.log("cookie load");
        super.create();

        
        for (let i: number = 0; i < this.cm.jsonAssets.length; i++) {
            
            let key: string = this.cm.jsonAssets[i];
            if (this.textures.exists(key))
            {
                this.textures.remove(key);
            }
            let path:string=this.cm.assetFolder + key + ".png";
            let jpath:string=this.cm.assetFolder+key+".json";         

            console.log(key);
            this.load.atlas(key,path , jpath);
        }

        for (let i:number=0;i<this.cm.images.length;i++)
        {
            let image:FileVo=this.cm.images[i];
           
            if (this.textures.exists(image.key))
            {
                this.textures.remove(image.key);
            }
            console.log("load "+image.key);

            this.load.image(image.key,this.cm.assetFolder+image.file);
        }
        for (let i:number=0;i<this.cm.audios.length;i++)
        {
            let audio:FileVo=this.cm.audios[i];
            let path:string=this.cm.assetFolder+audio.file;
            console.log(path);

            this.load.audio(audio.key,path);
        }

        /* this.load.image("holder", "./assets/holder.jpg");
        this.load.image("circle", "./assets/circle.png");
        this.load.image('background', "./assets/tabletop.jpg");
        this.load.image("shelf", "./assets/shelf.png");
        this.load.image("arrow", "./assets/arrow.png");
        this.load.image("dough", "./assets/dough.png");
        this.load.image("oven","./assets/oven.png");
        this.load.audio("ovenSound","./assets/microwave.wav");
        this.load.image("saveButton","./assets/disk.png"); */
    }
    create()
    {
       this.scene.start(this.cm.afterLoadScene);
    }
}