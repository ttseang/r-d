import { GameObjects } from "phaser";
import { LayoutVo } from "../dataObjs/LayoutVo";
import { ResizeVo } from "../dataObjs/ResizeVo";
import IBaseScene from "../interfaces/IBaseScene";
import { IGameObj } from "../interfaces/IGameObj";
import { Align } from "../util/align";
import { GM } from "./GM";
import { ResizeManager } from "./resizeManager";


export class SpineAnim2 implements IGameObj {
    public scene: Phaser.Scene;
    public bscene: IBaseScene;
    private _realWidth: number = -1;
    private _realHeight: number = -1;


    private graphics: GameObjects.Graphics;
    public spineObj: SpineGameObject;
    public type: string = "spine";
    public resizeKey: string;
    private gm: GM = GM.getInstance();
    public cover:boolean=false;

    constructor(bscene: IBaseScene, key: string, resizeKey: string) {
        this.bscene = bscene;
        this.scene = bscene.getScene();

        this.spineObj = this.scene.add.spine(0, 0, key);

        this._realWidth = -1;
        this._realHeight = -1;

        this.resizeKey = resizeKey;
    }
    set displayWidth(val: number) {
        this.spineObj.displayWidth = val;
    }
    get displayWidth() {
        return this.spineObj.displayWidth;
    }
    set displayHeight(val: number) {
        this.spineObj.displayHeight = val;
    }
    get displayHeight() {
        return this.spineObj.displayHeight;
    }
    set visible(val: boolean) {
        this.spineObj.visible = val;
    }
    get visible() {
        return this.spineObj.visible;
    }
    set alpha(val: number) {
        this.spineObj.alpha = val;
    }
    get alpha() {
        return this.spineObj.alpha;
    }
    set x(val: number) {
        this.spineObj.x = val;
    }
    get x() {
        return this.spineObj.x;
    }
    set y(val: number) {
        this.spineObj.y = val;
    }
    get y() {
        return this.spineObj.y;
    }
    set scaleX(val: number) {
        this.spineObj.scaleX = val;
    }
    get scaleX() {
        return this.spineObj.scaleX;
    }
    set scaleY(val: number) {
        this.spineObj.scaleY = val;
    }
    get scaleY() {
        return this.spineObj.scaleY;
    }

    update() {
        console.log("update coral");
        if (this.cover===true)
        {
            this.scaleToGameW(1);
            if (this.realHeight<this.bscene.getH())
            {
                console.log("fix");
               this.scaleToGameH(1);
               Align.centerSpine(this,this.bscene);
               // this.spineObj.displayHeight=this.bscene.getH();
                //this.spineObj.scaleX=this.spineObj.scaleY;
            }
            
            return;
        }
        let rm: ResizeManager = ResizeManager.getInstance(null);
        let resizeVo: ResizeVo = rm.getLayout(this.resizeKey);
        this.scaleToGameW(resizeVo.scale);
        //throw new Error("Method not implemented.");
    }
    play(key: string, loop: boolean) {
        this.spineObj.play(key, loop);
    }
    scaleToGameW(per:number) {
        let offsetScale = this.spineObj.displayWidth / this.realWidth;
        this.spineObj.displayWidth = this.bscene.getW() * per * offsetScale;
        this.spineObj.scaleY = this.spineObj.scaleX;
        //1440/679
    }
    scaleToGameH(per:number)
    {
        let offsetScale = this.spineObj.displayHeight / this.realHeight;
        this.spineObj.displayHeight = this.bscene.getH() * per * offsetScale;
        this.spineObj.scaleX = this.spineObj.scaleY;
    }
    setReal(w:number, h:number) {
        this._realWidth = w;
        this._realHeight = h;
    }
    get realHeight() {
        if (this._realHeight != -1) {
            return this._realHeight * this.spineObj.scaleY;
        }
        return this.spineObj.displayHeight;
    }
    get realWidth() {
        if (this._realWidth != -1) {
            return this._realWidth * this.spineObj.scaleX;
        }
        return this.spineObj.displayWidth;
    }

    setOffSet(xx: number, yy: number) {
        let root = this.spineObj.getRootBone();
        root.x = xx;
        root.y = yy;
    }
    fixCenter() {
        this.spineObj.x -= this.spineObj.displayWidth / 2;
        this.spineObj.y -= this.spineObj.displayHeight / 2;
    }
    resetPos() {
        this.spineObj.x = 0;
        this.spineObj.y = 0;
    }
    centerMe() {
        Align.centerSpine(this, this.bscene);
    }
    showDebugPos() {
        if (!this.graphics) {
            this.graphics = this.scene.add.graphics();

            this.graphics.fillStyle(0xffffff, 0.4);
        }
        else {
            this.graphics.clear();
            this.graphics.fillStyle(0xffffff, 0.4);
        }

        this.graphics.fillRect(this.spineObj.x, this.spineObj.y, this.spineObj.displayWidth, this.spineObj.displayHeight);
    }
}