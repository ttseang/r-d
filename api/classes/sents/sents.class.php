<?php
class sents_sents
{
    public function __construct()
    {

    }
    public function getAllSents()
    {
        $db = new db_Database();
        $db->query("select id,sentence from sentence_diagram");

        $set = $db->resultset();

        $sents = array();
        $len   = $db->rowcount();

        for ($i = 0; $i < $len; $i++) {
            array_push($sents, $this->obscure($set[$i]));
        }

        return $sents;
    }
    private function getHash($id)
    {
        return md5('pepperandsalt' . $id);
    }
    private function obscure($obj)
    {
        $hash = $this->getHash($obj['id']);

        $key = $hash . "_" . $obj['id'];
        return array('key' => $key, 'sent' => $obj['sentence']);
    }
    private function testKey($key)
    {
        $keyArray = explode("_", $key);
        if (count($keyArray) < 2) {
            return false;
        }
        $hash = $this->getHash($keyArray[1]);
        if ($hash != $keyArray[0]) {
            return false;
        }
        return true;
    }
    public function saveSent($key, $data)
    {
        //test key
    }
}
